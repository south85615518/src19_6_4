﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;

namespace Gauge.BLL
{
    public class smos_module_water_gauge_data
    {
        public Gauge.DAL.smos_module_water_gauge_data dal = new Gauge.DAL.smos_module_water_gauge_data();
        public bool PointLoad(int xmno,out List<string> pointlist,out string mssg)
        {
            mssg = "";
            pointlist = new List<string>();
            try
            {
                if (dal.PointLoad(xmno, out pointlist))
                {
                    mssg = string.Format("加载项目编号{0}的库水位点名数量为{1}", xmno, pointlist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("加载项目编号{0}的库水位点名失败", xmno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目编号{0}的库水位点名出错，错误信息：" + ex.Message, xmno);
                return false;
            }
        }

        //public GPS.DAL.smos_module_gnss_data dal = new DAL.smos_module_gnss_data();
        public bool MaxTime(string pointnamestr, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.MaxTime(pointnamestr, out maxTime))
                {
                    mssg = string.Format("获取点列{0}库水位测量数据的最大日期{1}成功", pointnamestr, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取点列{0}库水位测量数据的最大日期失败", pointnamestr);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取点列{0}库水位测量数据的最大日期出错，错误信息:" + ex.Message, pointnamestr);
                return false;
            }
        }


    }
}
