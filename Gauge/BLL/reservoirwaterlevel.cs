﻿/**  版本信息模板在安装目录下，可自行修改。
* reservoirwaterlevel.cs
*
* 功 能： N/A
* 类 名： reservoirwaterlevel
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:27   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace Gauge.BLL
{
	/// <summary>
	/// reservoirwaterlevel
	/// </summary>
	public partial class reservoirwaterlevel
	{
		private readonly Gauge.DAL.reservoirwaterlevel dal=new Gauge.DAL.reservoirwaterlevel();
		public reservoirwaterlevel()
		{}
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        //public bool Exists(decimal sid, string name)
        //{
        //    return dal.Exists(sid, name);
        //}

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Gauge.Model.reservoirwaterlevel model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目{0}库水位的预警参数名{1}成功", model.xmno, model.name);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目{0}库水位的预警参数名{1}失败", model.xmno, model.name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目{0}库水位的预警参数名{1}出错，错误信息：" + ex.Message, model.xmno, model.name);
                return false;
            }




            //return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Gauge.Model.reservoirwaterlevel model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新库水位的预警参数名{1}成功", model.xmno, model.name);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新库水位的预警参数名{1}失败", model.xmno, model.name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新库水位的预警参数名{1}出错，错误信息：" + ex.Message, model.xmno, model.name);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int xmno, string name, out string mssg)
        {

            try
            {
                if (dal.Delete(xmno, name))
                {
                    mssg = string.Format("删除库水位预警名称{0}成功", name);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除库水位预警名称{0}失败", name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除库水位预警名称{0}出错，错误信息：" + ex.Message, name);
                return false;
            }
            //return dal.Delete(sid,name);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string name, int xmno, out Gauge.Model.reservoirwaterlevel model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(name, xmno, out model))
                {
                    mssg = string.Format("获取项目{0}库水位预警名称{1}实体成功", xmno, name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}库水位预警名称{1}实体失败", xmno, name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}库水位预警名称{1}实体出错，错误信息:" + ex.Message, xmno, name);
                return false;
            }

            //return dal.GetModel(sid,name);
        }

        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out  dt))
                {
                    mssg = string.Format("加载项目{0}的库水位预警参数{1}条成功", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("加载项目{0}的库水位预警参数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目{0}的库水位预警参数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TableRowsCount(string seachstring, int xmno, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TableRowsCount(seachstring, xmno, out totalCont))
                {
                    mssg = string.Format("加载项目{0}库水位预警参数表记录数{1}成功!", xmno, totalCont);
                    return true;

                }
                else
                {
                    mssg = string.Format("加载项目{0}库水位预警参数表记录数失败!", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载项目{0}库水位预警参数表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        ///// <summary>
        ///// 获取所有的预警参数
        ///// </summary>
        ///// <param name="xmno"></param>
        ///// <param name="dt"></param>
        ///// <returns></returns>
        //public bool AlarmValueSelect(int xmno, out DataTable dt)
        //{



        //}
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(string alarmname, int xmno, out string mssg)
        {
            try
            {
                if (dal.PointAlarmValueDelCasc(alarmname, xmno))
                {
                    mssg = string.Format("项目{0}库水位预警参数级联删除成功！", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}库水位预警参数级联删除失败！", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目{0}库水位预警参数级联删除出错，出错信息" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Gauge.Model.reservoirwaterlevel> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Gauge.Model.reservoirwaterlevel> DataTableToList(DataTable dt)
        {
            List<Gauge.Model.reservoirwaterlevel> modelList = new List<Gauge.Model.reservoirwaterlevel>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Gauge.Model.reservoirwaterlevel model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }
        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno, out string alarmValueNameStr, out string mssg)
        {
            alarmValueNameStr = "";
            try
            {
                if (dal.AlarmValueNameGet(xmno, out alarmValueNameStr))
                {
                    mssg = string.Format("获取项目{0}的库水位狱警名称{1}成功", xmno, alarmValueNameStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的库水位狱警名称失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的库水位狱警名称出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
	}
}

