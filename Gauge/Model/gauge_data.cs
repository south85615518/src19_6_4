﻿/**  版本信息模板在安装目录下，可自行修改。
* smos_module_displacement_gauge_data.cs
*
* 功 能： N/A
* 类 名： smos_module_displacement_gauge_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/31 11:17:00   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Gauge.Model
{
	/// <summary>
	/// smos_module_displacement_gauge_data:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gauge_data
	{
		public gauge_data()
		{}
        #region Model
        private int _id;
        private string _platform_id;
        private DateTime _time;
        private string _name;
        private double _distance_s;
        private double _elevation_h;
        private double _depth;
        private string _flag;
        private int _statistics_type;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string platform_id
        {
            set { _platform_id = value; }
            get { return _platform_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime time
        {
            set { _time = value; }
            get { return _time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string name
        {
            set { _name = value; }
            get { return _name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double distance_S
        {
            set { _distance_s = value; }
            get { return _distance_s; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double elevation_h
        {
            set { _elevation_h = value; }
            get { return _elevation_h; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double depth
        {
            set { _depth = value; }
            get { return _depth; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string flag
        {
            set { _flag = value; }
            get { return _flag; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int statistics_type
        {
            set { _statistics_type = value; }
            get { return _statistics_type; }
        }
        #endregion Model

	}
}

