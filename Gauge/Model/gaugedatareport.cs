﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gauge.Model
{
   /// <summary>
	/// gaugedatareport:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gaugedatareport
	{
		public gaugedatareport()
		{}
		#region Model
		private string _point_name;
		private double? _holedepth;
		private double? _deep;
		private double? _predeep;
		private int _thisdeep;
		private int _acdeep;
		private int _rap;
		private string _xmname= "0";
		private string _remark;
		private int _times;
		private DateTime _time= new DateTime();
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? holedepth
		{
			set{ _holedepth=value;}
			get{return _holedepth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? deep
		{
			set{ _deep=value;}
			get{return _deep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? predeep
		{
			set{ _predeep=value;}
			get{return _predeep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int thisdeep
		{
			set{ _thisdeep=value;}
			get{return _thisdeep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int acdeep
		{
			set{ _acdeep=value;}
			get{return _acdeep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int rap
		{
			set{ _rap=value;}
			get{return _rap;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xmname
		{
			set{ _xmname=value;}
			get{return _xmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int times
		{
			set{ _times=value;}
			get{return _times;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		#endregion Model
    }
}
