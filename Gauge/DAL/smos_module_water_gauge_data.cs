﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Data;

namespace Gauge.DAL
{
    public class smos_module_water_gauge_data
    {
        public static database db = new database();
        public bool PointLoad(int xmno,out List<string> pointlist)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select DISTINCT(point_name) from gaugepoint where  xmno = " + xmno + "";
            pointlist = querysql.querystanderlist(sql, conn);
            return true;
        }
        public bool MaxTime(string pointnamestr, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from  gaugedatareport  where  point_name in " + string.Format("('{0}')", pointnamestr.Replace(",", "','")));

            //OdbcParameter[] parameters = { new OdbcParameter("@pointnamestr",OdbcType.VarChar,200) };

            //parameters[0].Value = string.Format("{0}'", pointnamestr.Replace(",","','"));
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }

       




    }
}
