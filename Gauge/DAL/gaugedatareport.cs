﻿/**  版本信息模板在安装目录下，可自行修改。
* smos_module_displacement_gauge_data.cs
*
* 功 能： N/A
* 类 名： smos_module_displacement_gauge_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/31 11:17:00   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using SqlHelpers;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.Data;
using Tool;
namespace Gauge.DAL
{
    /// <summary>
    /// smos_module_displacement_gauge_data:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class gaugedatareport
    {
        public static database db = new database();
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(List<string> pointnamelist, out List<Gauge.Model.gaugedatareport> li)
        {
            li = new List<Gauge.Model.gaugedatareport>();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            strSql.AppendFormat(@"select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmname,remark,times,time from gaugedatareport
where point_name in ('" + string.Join("','", pointnamelist) + @"')
order by time desc,point_name desc  limit 0, {0}", pointnamelist.Count + 5);

           
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;

            List<string> pointhashtable = new List<string>();
            while (i < ds.Tables[0].Rows.Count && i < pointnamelist.Count)
            {
                li.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
                if (!pointhashtable.Contains(ds.Tables[0].Rows[i]["point_name"].ToString()))
                    pointhashtable.Add(ds.Tables[0].Rows[i]["point_name"].ToString());
                else
                    break;
            }
            return true;
        }

        public bool GetAlarmTableCont(List<string> pointnamelist, out int cont)
        {
            cont = 0;

            //StringBuilder strSql = new StringBuilder();
            //OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            //strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
            //strSql.Append(" FROM dtudata_tmp where xmno = @xmno ");
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            strSql.AppendFormat(@"select count(1) from gaugedatareport
where point_name in ('" + string.Join("','", pointnamelist) + @"')
order by time desc,point_name desc  limit 0, {0}", pointnamelist.Count + 5);
            //OdbcParameter[] parameters = {

            //        new OdbcParameter("@xmno", OdbcType.Int,11)};

            //parameters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            //DataTable dt  = querybkgsql.
            if (obj == null) return false;
            cont = int.Parse(obj.ToString());
            return true;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Gauge.Model.gaugedatareport DataRowToModel(DataRow row)
        {
            Gauge.Model.gaugedatareport model = new Gauge.Model.gaugedatareport();
            if (row != null)
            {
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                model.predeep = double.Parse(row["predeep"].ToString());
                model.deep = double.Parse(row["deep"].ToString());
                model.acdeep = int.Parse(row["acdeep"].ToString());
                model.rap = int.Parse(row["rap"].ToString());
                model.time = DateTime.Parse(row["time"].ToString());


            }
            return model;
        }

        public bool Delete(string pointname,DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            string sql = "delete a,b from gaugedatareport a,smos_module_water_gauge_data b where a.point_name = b.name and a.time = b.time and a.point_name = '" + pointname + "' and  a.time='"+dt+"' ";
            ExceptionLog.DTUPortInspectionWrite(sql);
            int cont = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,sql);
            return cont > 0 ? true : false;

        }

        public bool PointNewestDateTimeGet(string pointname, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from gaugedatareport  where  point_name = @point_name  ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }

        public bool GetModel(string pointname, DateTime dt, out Gauge.Model.gaugedatareport model)
        {
            model = null;
            StringBuilder strSql = new StringBuilder(256);
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            strSql.AppendFormat(@"select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmname,remark,times,time from gaugedatareport
where point_name = '"+pointname+"' and  time ='"+dt+"' ");
         
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }


    }
}

