﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmsplitondate.cs
*
* 功 能： N/A
* 类 名： alarmsplitondate
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/24 9:28:56   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;

namespace AuthorityAlarm.BLL
{
    /// <summary>
    /// alarmsplitondate
    /// </summary>
    public partial class alarmsplitondate
    {
        private readonly AuthorityAlarm.DAL.alarmsplitondate dal = new AuthorityAlarm.DAL.alarmsplitondate();
        public alarmsplitondate()
        { }
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(AuthorityAlarm.Model.alarmsplitondate model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}{1}{2}点测量时间{3}的数据自检日期分包记录成功", model.xmno, model.jclx, model.pointName, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}{1}{2}点测量时间{3}的数据自检日期分包记录失败", model.xmno, model.jclx, model.pointName, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}{1}{2}点测量时间{3}的数据自检日期分包记录出错,错误信息：" + ex.Message, model.xmno, model.jclx, model.pointName, model.time);
                return false;
            }
            //return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(AuthorityAlarm.Model.alarmsplitondate model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int xmno, string jclx, string pointName, DateTime time, out string mssg)
        {
            try
            {
                if (dal.Delete(xmno, jclx, pointName, time))
                {
                    mssg = string.Format("删除项目编号{0}{1}{2}点测量时间{3}的数据自检日期分包记录成功", xmno, jclx, pointName, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}{1}{2}点测量时间{3}的数据自检日期分包记录失败", xmno, jclx, pointName, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}{1}{2}点测量时间{3}的数据自检日期分包记录出错，错误信息：" + ex.Message, xmno, jclx, pointName, time);
                return false;
            }



            //该表无主键信息，请自定义主键/条件字段
            //return dal.Delete();
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno,DateTime startTime,DateTime endTime,string keyWord ,out List<AuthorityAlarm.Model.alarmsplitondate> ls, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            //return dal.GetModelList(dt,xmno,out ls);
            ls = null;
            try
            {
                if (dal.GetModelList(xmno,startTime,endTime,keyWord,out ls))
                {
                    mssg = string.Format("成功获取到项目编号{0}从{1}到{2}包含关键字{3}的数据自检日期分包记录{4}条", xmno,startTime,endTime,keyWord,ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}从{1}到{2}包含关键字{3}的数据自检日期分包记录失败", xmno, startTime, endTime, keyWord);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}从{1}到{2}包含关键字{3}的数据自检日期分包记录出错,错误信息：" + ex.Message, xmno, startTime, endTime, keyWord);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno, DateTime dt,string keyWord,out List<AuthorityAlarm.Model.alarmsplitondate> ls, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            //return dal.GetModelList(dt,xmno,out ls);
            ls = null;
            try
            {
                if (dal.GetModelList(xmno,dt,keyWord,out ls))
                {
                    mssg = string.Format("成功获取到项目编号{0}{1}的数据预警日期分包记录{2}条", xmno,dt,ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}{1}的数据预警日期分包记录失败", xmno,dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}{1}的数据自检预警分包记录出错,错误信息：" + ex.Message, xmno,dt);
                return false;
            }
        }
        public bool GetModel(int xmno, string alarmno, out AuthorityAlarm.Model.alarmsplitondate model,out string mssg)
        {

            model = null;
            try
            {
                if (dal.GetModel(xmno, alarmno,out model))
                {
                    mssg = string.Format("成功获取到项目编号{0}{1}的数据自检实体对象成功", xmno,alarmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}{1}的数据自检日期分包记录失败", xmno,alarmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}{1}的数据自检日期分包记录出错,错误信息：" + ex.Message, xmno, alarmno);
                return false;
            }
         

        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<AuthorityAlarm.Model.alarmsplitondate> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<AuthorityAlarm.Model.alarmsplitondate> DataTableToList(DataTable dt)
        {
            List<AuthorityAlarm.Model.alarmsplitondate> modelList = new List<AuthorityAlarm.Model.alarmsplitondate>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                AuthorityAlarm.Model.alarmsplitondate model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        public bool alarmcgsplitondatereaded(int xmno, DateTime dt, out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.alarmcgsplitondatereaded(xmno, dt))
                {
                    mssg = string.Format("标记项目编号{0}{1}的自检为已读成功", xmno, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("标记项目编号{0}{1}的自检为已读失败", xmno, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("标记项目编号{0}{1}的自检为已读出错，错误信息:" + ex.Message, xmno, dt);
                return false;
            }
        }

        public bool alarmcgsplitondateconfirm(int xmno, string alarmno, out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.alarmcgsplitondateconfirm(xmno, alarmno))
                {
                    mssg = string.Format("确认项目编号{0}狱警编号{1}的预警信息成功", xmno, alarmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("确认项目编号{0}狱警编号{1}的预警信息失败", xmno, alarmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("确认项目编号{0}狱警编号{1}的预警信息出错，错误信息:" + ex.Message, xmno, alarmno);
                return false;
            }
        }


        public bool alarmcgsplitondatedelete(int xmno, string alarmid,out string mssg)
        {
            try
            {
                if (dal.alarmcgsplitondatedelete(xmno, alarmid))
                {
                    mssg = string.Format("删除项目编号{0}预警编号{1}预警信息成功", xmno, alarmid);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}预警编号{1}预警信息失败", xmno, alarmid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}预警编号{1}预警信息出错,错误信息:"+ex.Message, xmno, alarmid);
                return false;
            }
        }
        public bool alarmcgsplitondatedelete(int xmno, DateTime date,out string mssg)
        {
            try
            {
                if (dal.alarmcgsplitondatedelete(xmno, date))
                {
                    mssg = string.Format("删除项目编号{0}{1}预警信息成功", xmno, date);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}{1}预警信息失败", xmno, date);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}{1}预警信息出错,错误信息:" + ex.Message, xmno, date);
                return false;
            }
        }


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}
        public bool UnitDayAlarmListLoad(string unitname, DateTime dt ,int timeunit ,out List<AuthorityAlarm.Model.alarmsplitondate> ls,out string mssg)
        {
            ls = null;
            try
            {
                if (dal.UnitDayAlarmListLoad(unitname,dt,timeunit,  out ls))
                {
                    mssg = string.Format("成功获取到最新预警条数据{0}条", ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取最新预警条数失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取最新预警条数出错,错误信息：" + ex.Message);
                return false;
            }

        }
        public bool XmDayAlarmListLoad(int xmno, DateTime dt, int timeunit, out List<AuthorityAlarm.Model.alarmsplitondate> ls,out string mssg)
        {
            ls = null;
            try
            {
                if (dal.XmDayAlarmListLoad(xmno, dt, timeunit, out ls))
                {
                    mssg = string.Format("成功获取到项目{0}最新预警条数据{1}条",xmno ,ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}最新预警条数失败",xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}最新预警条数出错,错误信息：" + ex.Message,xmno);
                return false;
            }
        }

        public bool AlarmListGetByCycList(int xmno, data.Model.gtsensortype datatype, List<string> cyclist, out List<AuthorityAlarm.Model.alarmsplitondate> alarmsplitondatelist,out string mssg)
        {
            mssg = "";
            alarmsplitondatelist = null;
            try
            {
                if (dal.AlarmListGetByCycList(xmno, datatype, cyclist, out alarmsplitondatelist))
                {
                    mssg = string.Format("获取项目编号{0}{1}{2}周期的分包预警记录数{3}条", xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype), string.Join(",", cyclist), alarmsplitondatelist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}{2}周期的分包预警记录数失败", xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype), string.Join(",", cyclist));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}{2}周期的分包预警记录数出错,错误信息:"+ex.Message, xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype), string.Join(",", cyclist));
                return false;
            }
        }

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

