﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmsplitondate.cs
*
* 功 能： N/A
* 类 名： alarmsplitondate
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/24 9:28:56   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;

namespace AuthorityAlarm.BLL
{
    /// <summary>
    /// alarmsplitondate
    /// </summary>
    public partial class alarmsplitondate
    {
       
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool CgAdd(AuthorityAlarm.Model.alarmsplitondate model, out string mssg)
        {
            try
            {
                if (dal.CgAdd(model))
                {
                    mssg = string.Format("添加项目编号{0}{1}{2}点测量时间{3}的成果数据预警日期分包记录成功", model.xmno, model.jclx, model.pointName, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}{1}{2}点测量时间{3}的成果数据预警日期分包记录失败", model.xmno, model.jclx, model.pointName, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}{1}{2}点测量时间{3}的成果数据预警日期分包记录出错,错误信息：" + ex.Message, model.xmno, model.jclx, model.pointName, model.time);
                return false;
            }
            //return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool CgUpdate(AuthorityAlarm.Model.alarmsplitondate model)
        {
            return dal.CgUpdate(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool CgDelete(int xmno, string jclx, string pointName, DateTime time, out string mssg)
        {
            try
            {
                if (dal.CgDelete(xmno, jclx, pointName, time))
                {
                    mssg = string.Format("删除项目编号{0}{1}{2}点测量时间{3}的成果数据预警日期分包记录成功", xmno, jclx, pointName, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}{1}{2}点测量时间{3}的成果数据预警日期分包记录失败", xmno, jclx, pointName, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}{1}{2}点测量时间{3}的成果数据预警日期分包记录出错，错误信息：" + ex.Message, xmno, jclx, pointName, time);
                return false;
            }



            //该表无主键信息，请自定义主键/条件字段
            //return dal.Delete();
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgGetModelList(int xmno,DateTime startTime,DateTime endTime,string keyWord ,out List<AuthorityAlarm.Model.alarmsplitondate> ls, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            //return dal.GetModelList(dt,xmno,out ls);
            ls = null;
            try
            {
                if (dal.CgGetModelList(xmno,startTime,endTime,keyWord,out ls))
                {
                    mssg = string.Format("成功获取到项目编号{0}从{1}到{2}包含关键字{3}的成果数据预警日期分包记录{4}条", xmno,startTime,endTime,keyWord,ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}从{1}到{2}包含关键字{3}的成果数据预警日期分包记录失败", xmno, startTime, endTime, keyWord);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}从{1}到{2}包含关键字{3}的成果数据预警日期分包记录出错,错误信息：" + ex.Message, xmno, startTime, endTime, keyWord);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgGetModelList(int xmno, DateTime dt,string keyWord,out List<AuthorityAlarm.Model.alarmsplitondate> ls, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            //return dal.GetModelList(dt,xmno,out ls);
            ls = null;
            try
            {
                if (dal.CgGetModelList(xmno,dt,keyWord,out ls))
                {
                    mssg = string.Format("成功获取到项目编号{0}{1}的成果数据预警日期分包记录{2}条", xmno,dt,ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}{1}的成果数据预警日期分包记录失败", xmno,dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}{1}的成果数据预警日期分包记录出错,错误信息：" + ex.Message, xmno,dt);
                return false;
            }
        }


     

        public bool cgalarmcgsplitondatereaded(int xmno, DateTime dt, out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.cgalarmcgsplitondatereaded(xmno, dt))
                {
                    mssg = string.Format("标记项目编号{0}{1}的自检为已读成功", xmno, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("标记项目编号{0}{1}的自检为已读失败", xmno, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("标记项目编号{0}{1}的自检为已读出错，错误信息:" + ex.Message, xmno, dt);
                return false;
            }
        }
       

        
        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

