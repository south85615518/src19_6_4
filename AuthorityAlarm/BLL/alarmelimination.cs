﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmelimination.cs
*
* 功 能： N/A
* 类 名： alarmelimination
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/2 13:49:49   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
namespace AuthorityAlarm.BLL
{
	/// <summary>
	/// 数据访问类:alarmelimination
	/// </summary>
	public partial class alarmelimination
	{
        public static database db = new database();
        private readonly AuthorityAlarm.DAL.alarmelimination dal = new AuthorityAlarm.DAL.alarmelimination();
		public alarmelimination()
		{}
		#region  BasicMethod

		
		


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.alarmelimination model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加监测员{0}项目编号{1}{2}点{3}时间{4}的消警记录成功", model.EliminationMonitorID, model.xmno, model.jclx, model.point_name,model.alarmtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加监测员{0}项目编号{1}{2}点{3}时间{4}的消警记录失败", model.EliminationMonitorID, model.xmno, model.jclx, model.point_name,model.alarmtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加监测员{0}项目编号{1}{2}点{3}时间{4}的消警记录出错,错误信息:"+ex.Message, model.EliminationMonitorID, model.xmno, model.jclx, model.point_name,model.alarmtime);
                return false;
            }
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.alarmelimination model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新监测员{0}项目编号{1}{2}点{3}的消警记录成功", model.EliminationMonitorID, model.xmno, model.jclx, model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新监测员{0}项目编号{1}{2}点{3}的消警记录失败", model.EliminationMonitorID, model.xmno, model.jclx, model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新监测员{0}项目编号{1}{2}点{3}的消警记录出错,错误信息:" + ex.Message, model.EliminationMonitorID, model.xmno, model.jclx, model.point_name);
                return false;
            }
		}

       

		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool Delete(int id)
        //{
			
			
        //}



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int id, string unitname, out AuthorityAlarm.Model.alarmelimination model,out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(id, unitname, out model))
                {
                    mssg = string.Format("获取机构{0}消警编号{1}的对象成功", unitname, id);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取机构{0}消警编号{1}的对象失败", unitname, id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取机构{0}消警编号{1}的对象出错,错误信息："+ex.Message, unitname, id);
                return false;
            }
        }


        public bool AlarmEliminationTableLoad(string unitname,string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword, int startPageIndex, int pageSize, string sordname, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.AlarmEliminationTableLoad(unitname,monitorID, xmno, jclx, pointname, starttime, endtime, keyword,startPageIndex,pageSize,sordname,sord, out dt))
                {
                    mssg = string.Format("加载监测员编号{0}项目编号{1}{2}点名{3}预警日期在{4}-{5}之间的消警记录{6}条成功", monitorID, xmno, jclx, pointname, starttime, endtime, keyword, dt.Rows.Count);
                    return true;
                }
                else
                {

                    mssg = string.Format("未获取到任何监测员编号{0}项目编号{1}{2}点名{3}预警日期在{4}-{5}之间的消警记录", monitorID, xmno, jclx, pointname, starttime, endtime, keyword);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("未获取到任何监测员编号{0}项目编号{1}{2}点名{3}预警日期在{4}-{5}之间的消警记录出错,错误信息:"+ex.Message, monitorID, xmno, jclx, pointname, starttime, endtime, keyword);
                return false;
            }
        }



        public bool AlarmEliminationTableLoadCount(string unitname, string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword, out int cont,out string mssg)
        {
            cont = 0;
            mssg = "";
            try
            {
                if (dal.AlarmEliminationTableLoadCount( unitname,  monitorID,  xmno,  jclx,  pointname,  starttime,  endtime,  keyword, out  cont))
                {
                    mssg = string.Format("加载监测员编号{0}项目编号{1}{2}点名{3}预警日期在{4}-{5}之间包含'{6}'的消警记录{7}条成功", monitorID, xmno, jclx, pointname, starttime, endtime, keyword, cont);
                    return true;
                }
                else
                {

                    mssg = string.Format("未获取到任何监测员编号{0}项目编号{1}{2}点名{3}预警日期在{4}-{5}的之间的包含'{6}'的消警记录", monitorID, xmno, jclx, pointname, starttime, endtime, keyword);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取任何监测员编号{0}项目编号{1}{2}点名{3}预警日期在{4}-{5}之间的包含'{6}'消警记录出错,错误信息:" + ex.Message, monitorID, xmno, jclx, pointname, starttime, endtime, keyword);
                return false;
            }
        }
	

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

