﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoralarm.cs
*
* 功 能： N/A
* 类 名： monitoralarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/7/26 14:41:30   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using AuthorityAlarm.Model;
namespace AuthorityAlarm.BLL
{
	/// <summary>
	/// monitoralarm
	/// </summary>
	public partial class monitoralarm
	{
		private readonly AuthorityAlarm.DAL.monitoralarm dal=new AuthorityAlarm.DAL.monitoralarm();
		public monitoralarm()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int mid)
		{
			return dal.Exists(mid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.monitoralarm model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("{0}添加监测员{1}的预警通知成功", model.time, model.unitmember);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}添加监测员{1}的预警通知失败", model.time, model.unitmember);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}添加监测员{1}的预警通知出错，错误信息:"+ex.Message, model.time, model.unitmember);
                return false;
            }





			
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.monitoralarm model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("{0}更新监测员{1}的预警通知成功", model.time, model.unitmember);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}更新监测员{1}的预警通知失败", model.time, model.unitmember);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}更新监测员{1}的预警通知出错，错误信息:" + ex.Message, model.time, model.unitmember);
                return false;
            }
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int mid,out string mssg)
		{

            try
            {
                if (dal.Delete(mid))
                {
                    mssg = string.Format("删除预警编号{0}成功",mid);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除预警编号{0}失败", mid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除预警编号{0}出错,错误信息："+ex.Message, mid);
                return false;
            }
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string midlist )
		{
			return dal.DeleteList(midlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool  GetModel(int mid,out AuthorityAlarm.Model.monitoralarm model,out string mssg)
		{
            model = null;
            try
            {
                if (dal.GetModel(mid, out model))
                {
                    mssg =string.Format("获取监测员预警编号为{0}的信息成功",mid);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取监测员预警编号为{0}的信息失败", mid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取监测员预警编号为{0}的信息出错，错误信息："+ex.Message, mid);
                return false;
            }
			 
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<AuthorityAlarm.Model.monitoralarm> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<AuthorityAlarm.Model.monitoralarm> DataTableToList(DataTable dt)
		{
			List<AuthorityAlarm.Model.monitoralarm> modelList = new List<AuthorityAlarm.Model.monitoralarm>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				AuthorityAlarm.Model.monitoralarm model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}


        public bool LastSendTime(int xmno, out DateTime dt,out string mssg)
        {
            dt = new DateTime();
            mssg = "";
            try
            {
                if (dal.LastSendTime(xmno, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的最后预警邮件发送时间为{1}", xmno, dt);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目编号{0}的最后预警邮件发送时间失败",xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的最后预警邮件发送时间出错，错误信息："+ex.Message, xmno);
                return false;
            }
        }

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

        public bool MonitorAlarmTableLoad(int xmno, int unitmemberno, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.MonitorAlarmTableLoad(xmno, unitmemberno, out dt))
                {
                    mssg = string.Format("获取项目编号{0}监测员{1}的预警信息成功", xmno, unitmemberno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}监测员{1}的预警信息失败", xmno, unitmemberno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}监测员{1}的预警信息出错，错误信息:" + ex.Message, xmno, unitmemberno);
                return false;
            }
        }
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

