﻿/**  版本信息模板在安装目录下，可自行修改。
* pointcheck.cs
*
* 功 能： N/A
* 类 名： pointcheck
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/9 10:01:27   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using AuthorityAlarm.Model;
using Tool;
using System.Text;
namespace AuthorityAlarm.BLL
{
    /// <summary>
    /// pointcheck
    /// </summary>
    public partial class pointcheck
    {
        public  AuthorityAlarm.DAL.pointcheck dal = new AuthorityAlarm.DAL.pointcheck();
        public pointcheck()
        { }
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {

            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}自检编号{3}的自检记录添加成功", model.xmno, model.type, model.point_name, model.pid);
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}自检编号{3}的自检记录添加失败", model.xmno, model.type, model.point_name, model.pid);
                    ExceptionLog.ExceptionWrite(mssg);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}监测类型{1}点名{2}自检编号{3}的自检记录添加出错,错误信息:" + ex.Message, model.xmno, model.type, model.point_name, model.pid);
                ExceptionLog.ExceptionWrite(mssg);
                return false;
            }


        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}自检编号{3}的自检记录更新成功", model.xmno, model.type, model.point_name, model.pid);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}自检编号{3}的自检记录更新失败", model.xmno, model.type, model.point_name, model.pid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}监测类型{1}点名{2}自检编号{3}的自检记录更新出错,错误信息:" + ex.Message, model.xmno, model.type, model.point_name, model.pid);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("删除项目编号{0}{1}{2}点{3}的数据成功",model.xmno,model.type,model.point_name,model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}{1}{2}点{3}的数据失败", model.xmno, model.type, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}{1}{2}点{3}的数据出错,错误信息：" + ex.Message, model.xmno, model.type, model.point_name, model.time);
                return false;
            }
           
        }

     


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<AuthorityAlarm.Model.pointcheck> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<AuthorityAlarm.Model.pointcheck> DataTableToList(DataTable dt)
        {
            List<AuthorityAlarm.Model.pointcheck> modelList = new List<AuthorityAlarm.Model.pointcheck>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                AuthorityAlarm.Model.pointcheck model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}


        public bool XmHighestAlarm(int xmno,out int alarm,out string mssg)
        {
            alarm = -1;
            try
            {
                if (dal.XmHighestAlarm(xmno, out alarm))
                {
                    mssg = string.Format("获取项目编号{0}的最高级别预警为{1}成功", xmno, alarm);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的最高级别预警为{1}失败", xmno, alarm);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的最高级别预警出错,错误信息:"+ex.Message, xmno);
                return false;
            }
        }
        public bool XmAlarmCount(int xmno, out int cont,out string mssg)
        {
            cont = -1;
            try
            {
                if (dal.XmAlarmCount(xmno, out cont))
                {
                    mssg = string.Format("获取项目编号{0}的预警点数为{1}成功", xmno, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的预警点数为{1}失败", xmno, cont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的预警点数出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno, out List<AuthorityAlarm.Model.pointcheck> modelList,out string mssg)
        {
            modelList = null;
            try
            {
                if (dal.GetModelList(xmno, out modelList))
                {
                    mssg = string.Format("获取项目{0}的预警汇总{1}条成功",xmno,modelList.Count) ;
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的预警汇总{1}条失败", xmno, modelList.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的预警汇总出错，错误信息:"+ex.Message, xmno, modelList.Count);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, string jclx, out AuthorityAlarm.Model.pointcheck model,out string mssg)
        {
            mssg = "";
            model = null;

            try
            {

                if (dal.GetModel(xmno, pointname, jclx,out model))
                {
                    mssg = string.Format("获取项目编号{0}{1}{2}点的自检记录成功",xmno,jclx,pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}{2}点的自检记录失败", xmno, jclx, pointname);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}{2}点的自检记录出错,错误信息："+ex.Message, xmno, jclx, pointname);
                return false;
            }
        }

        public bool XmAlarmCount(int xmno, string type, out int cont,out string mssg)
        {
            mssg = "";
            cont = 0;
            try
            {
                if (dal.XmAlarmCount(xmno, type, out cont))
                {
                    mssg = string.Format("获取项目编号{0}{1}的预警点数{2}个",xmno,type,cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}的预警点数失败",xmno,type);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}的预警点数出错，错误信息:"+ex.Message,xmno,type);
                return false;
            }

            //return false;
        }

        public bool MaxAlarm(int xmno, string type, out int alarm,out string mssg)
        {
            alarm = 0;
            mssg = "";
            try
            {
                if (dal.MaxAlarm(xmno, type, out alarm))
                {
                    mssg = string.Format("获取项目编号{0}{1}的最高预警级别{2}成功", xmno, type, alarm);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}的最高预警级别失败", xmno, type);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}的最高预警级别失败", xmno, type);
                return false;
            }
            //return false;
        }

        public bool AlarmContLayout(string unitname,out string alarmlaystr, out string mssg)
        {
            mssg = "";
            alarmlaystr = "";
            try
            {
                if (dal.AlarmLayoutCont(unitname, out alarmlaystr))
                {
                    mssg = string.Format("成功获取在线点情况为{0}", alarmlaystr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取在线点情况失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取在线点情况出错,错误信息:" + ex.Message);
                return false;
            }
        }

        public bool AlarmContLayout(int xmno, out string alarmlaystr, out string mssg)
        {
            mssg = "";
            alarmlaystr = "";
            try
            {
                if (dal.AlarmLayoutCont(xmno, out alarmlaystr))
                {
                    mssg = string.Format("成功获取在线点情况为{0}", alarmlaystr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取在线点情况失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取在线点情况出错,错误信息:" + ex.Message);
                return false;
            }
        }
        public bool AlarmPointLoad(int xmno,  out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.AlarmPointLoad(xmno, out dt))
                {
                    mssg = string.Format("成功获取到项目编号{0}的自检记录{1}条", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("未获取到项目编号{0}的自检记录", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("未获取到项目编号{0}的自检记录出错,错误信息:"+ex.Message, xmno);
                return false;
            }
        }

        public bool AlarmXmnoGet(string unitname, out List<string> xmnolist,out string mssg)
        {
            xmnolist = null;
            mssg = "";
            try
            {
                if (dal.AlarmXmnoGet(unitname, out xmnolist))
                {
                    mssg = string.Format("{0}有{1}个项目预警", unitname, xmnolist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}没有有项目预警", unitname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}项目预警情况出错,错误信息："+ex.Message, unitname);
                return false;
            }
        }


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

