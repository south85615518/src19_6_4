﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoreditlog.cs
*
* 功 能： N/A
* 类 名： monitoreditlog
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/13 10:00:19   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
namespace AuthorityAlarm.BLL
{
	/// <summary>
	/// 数据访问类:monitoreditlog
	/// </summary>
	public partial class monitoreditlog
	{
		public monitoreditlog()
		{}
		#region  BasicMethod


        public AuthorityAlarm.DAL.monitoreditlog dal = new DAL.monitoreditlog();
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(int ID)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from monitoreditlog");
        //    strSql.Append(" where ID=@ID");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@ID", OdbcType.Int)
        //    };
        //    parameters[0].Value = ID;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add( AuthorityAlarm.Model.monitoreditlog model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加监测员{0}项目{1}{2}操作记录成功", model.username, model.xmname, model.activetime);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加监测员{0}项目{1}{2}操作记录失败", model.username, model.xmname, model.activetime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加监测员{0}项目{1}{2}操作记录出错,错误信息:"+ex.Message, model.username, model.xmname, model.activetime);
                return true;
            }

		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update( AuthorityAlarm.Model.monitoreditlog model,out string mssg)
		{
            mssg = "";
            return true;
		}
        
        public bool MonitorEditLogTableLoad(string unitname, string monitorID, int xmno, string jclx,  DateTime starttime, DateTime endtime, string keyword, int startPageIndex, int pageSize, string sordname, string sord, out DataTable dt,out string mssg)
        {

            dt = null;
            mssg = "";
            try
            {
                if (dal.MonitorEditLogTableLoad(unitname, monitorID, xmno, jclx,  starttime, endtime, keyword, startPageIndex, pageSize, sordname, sord, out dt))
                {
                    mssg = string.Format("加载监测员编号{0}项目编号{1}{2}在{3}-{4}之间的操作记录{5}条成功", monitorID, xmno, jclx,starttime, endtime, dt.Rows.Count);
                    return true;
                }
                else
                {

                    mssg = string.Format("未获取到任何监测员编号{0}项目编号{1}{2}在{3}-{4}之间的操作记录", monitorID, xmno, jclx, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("未获取到任何监测员编号{0}项目编号{1}{2}预警日期在{3}-{4}之间的操作记录出错,错误信息:" + ex.Message, monitorID, xmno, jclx,starttime, endtime);
                return false;
            }
        }


        public bool MonitorEditLogTableCountLoad(string unitname, string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword, out int cont,out string mssg)
        {
            cont = -1;
            mssg = "";
            try
            {
                if (dal.MonitorEditLogTableCountLoad(unitname, monitorID, xmno, jclx, starttime, endtime, keyword, out cont))
                {
                    mssg = string.Format("加载监测员编号{0}项目编号{1}{2}在{3}-{4}之间的操作记录{5}条成功", monitorID, xmno, jclx, starttime, endtime, cont);
                    return true;
                }
                else
                {

                    mssg = string.Format("未获取到任何监测员编号{0}项目编号{1}{2}在{3}-{4}之间的操作记录", monitorID, xmno, jclx, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("未获取到任何监测员编号{0}项目编号{1}{2}预警日期在{3}-{4}之间的操作记录出错,错误信息:" + ex.Message, monitorID, xmno, jclx, starttime, endtime);
                return false;
            }
        }


		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from monitoreditlog ");
			strSql.Append(" where ID=@ID");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int)
			};
			parameters[0].Value = ID;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public  bool  GetModel(string unitname,int id,out AuthorityAlarm.Model.monitoreditlog model,out string mssg)
		{
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel( unitname,id, out model))
                {
                    mssg = string.Format("获取机构{0}操作日志编号{1}的对象成功", unitname, id);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取机构{0}操作日志编号{1}的对象失败", unitname, id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取机构{0}操作日志编号{1}的对象出错,错误信息：" + ex.Message, unitname, id);
                return false;
            }
		}


	

		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

