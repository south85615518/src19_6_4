﻿/**  版本信息模板在安装目录下，可自行修改。
* pointalarm.cs
*
* 功 能： N/A
* 类 名： pointalarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/9 10:55:22   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using AuthorityAlarm.Model;
namespace AuthorityAlarm.BLL
{
    /// <summary>
    /// pointalarm
    /// </summary>
    public partial class pointalarm
    {
        private readonly AuthorityAlarm.DAL.pointalarm dal = new AuthorityAlarm.DAL.pointalarm();
        public pointalarm()
        { }
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(AuthorityAlarm.Model.pointalarm model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}预警编号{3}的预警记录添加成功", model.xmno, model.type, model.point_name, model.pid);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}预警编号{3}的预警记录添加失败", model.xmno, model.type, model.point_name, model.pid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}监测类型{1}点名{2}预警编号{3}的预警记录添加出错,错误信息:" + ex.Message, model.xmno, model.type, model.point_name, model.pid);
                return false;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(AuthorityAlarm.Model.pointalarm model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}预警编号{3}的预警记录更新成功", model.xmno, model.type, model.point_name, model.pid);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}预警编号{3}的预警记录更新失败", model.xmno, model.type, model.point_name, model.pid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}监测类型{1}点名{2}预警编号{3}的预警记录更新出错,错误信息:" + ex.Message, model.xmno, model.type, model.point_name, model.pid);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(AuthorityAlarm.Model.pointalarm model, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("解除项目编号{0}{1}{2}点{3}的数据预警成功", model.xmno, model.type, model.point_name, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("解除项目编号{0}{1}{2}点{3}的数据预警失败", model.xmno, model.type, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("解除项目编号{0}{1}{2}点{3}的数据预警出错,错误信息：" + ex.Message, model.xmno, model.type, model.point_name, model.time);
                return false;
            }

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, string jclx, out AuthorityAlarm.Model.pointalarm model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, jclx, out model))
                {
                    mssg = string.Format("获取项目{0}{1}{2}的成果预警成功", xmno, jclx, pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}{2}的成果预警失败", xmno, jclx, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}{2}的成果预警出错，错误信息：" + ex.Message, xmno, jclx, pointname);
                return false;
            }


            //该表无主键信息，请自定义主键/条件字段
            //return dal.GetModel(xmno,pointname,jclx);
        }



        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<AuthorityAlarm.Model.pointalarm> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<AuthorityAlarm.Model.pointalarm> DataTableToList(DataTable dt)
        {
            List<AuthorityAlarm.Model.pointalarm> modelList = new List<AuthorityAlarm.Model.pointalarm>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                AuthorityAlarm.Model.pointalarm model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        public bool XmHighestAlarm(int xmno, out int alarm, out string mssg)
        {
            alarm = -1;
            try
            {
                if (dal.XmHighestAlarm(xmno, out alarm))
                {
                    mssg = string.Format("获取项目编号{0}的最高级别成果预警为{1}成功", xmno, alarm);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的最高级别成果预警为{1}失败", xmno, alarm);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的最高级别成果预警出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno, out List<AuthorityAlarm.Model.pointalarm> modelList, out string mssg)
        {
            modelList = null;
            try
            {
                if (dal.GetModelList(xmno, out modelList))
                {
                    mssg = string.Format("获取项目{0}的成果预警汇总{1}条成功", xmno, modelList.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的成果预警汇总{1}条失败", xmno, modelList.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的成果预警汇总出错，错误信息:" + ex.Message, xmno, modelList.Count);
                return false;
            }
        }

        public bool XmAlarmCount(int xmno, out int cont, out string mssg)
        {
            cont = -1;
            try
            {
                if (dal.XmAlarmCount(xmno, out cont))
                {
                    mssg = string.Format("获取项目编号{0}的成果预警点数为{1}成功", xmno, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的成果预警点数为{1}失败", xmno, cont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的成果预警点数出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

