﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmdatemailsendrecord.cs
*
* 功 能： N/A
* 类 名： alarmdatemailsendrecord
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/24 10:36:30   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace AuthorityAlarm.BLL
{
	/// <summary>
	/// alarmdatemailsendrecord
	/// </summary>
	public partial class alarmdatemailsendrecord
	{
		private readonly AuthorityAlarm.DAL.alarmdatemailsendrecord dal=new AuthorityAlarm.DAL.alarmdatemailsendrecord();
		public alarmdatemailsendrecord()
		{}
		#region  BasicMethod
		

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.alarmdatemailsendrecord model,out string mssg)
		{
            mssg = "";
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}{1}的预警邮件日期包发送记录成功", model.xmno, model.sdate);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}{1}的预警邮件日期包发送记录失败", model.xmno, model.sdate);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}{1}的预警邮件日期包发送记录出错，错误信息："+ex.Message, model.xmno, model.sdate);
                return false;
            }
            


			//return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.alarmdatemailsendrecord model,out string mssg)
		{
            mssg = "";
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}{1}的预警邮件日期包发送记录成功", model.xmno, model.sdate);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}{1}的预警邮件日期包发送记录失败", model.xmno, model.sdate);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}{1}的预警邮件日期包发送记录出错，错误信息：" + ex.Message, model.xmno, model.sdate);
                return false;
            }
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string did)
		{
			
			return dal.Delete(did);
		}
		

		

		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

