﻿/**  版本信息模板在安装目录下，可自行修改。
* adminalarm.cs
*
* 功 能： N/A
* 类 名： adminalarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/7/26 14:41:30   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using AuthorityAlarm.Model;
namespace AuthorityAlarm.BLL
{
	/// <summary>
	/// adminalarm
	/// </summary>
	public partial class adminalarm
	{
		private readonly AuthorityAlarm.DAL.adminalarm dal=new AuthorityAlarm.DAL.adminalarm();
		public adminalarm()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int aid)
		{
			return dal.Exists(aid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.adminalarm model,out string mssg)
		{
            mssg ="";
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("{0}添加管理员{1}的预警通知成功", model.sendTime, model.adminno);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}添加管理员{1}的预警通知失败", model.sendTime, model.adminno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}添加管理员{1}的预警通知出错，错误信息:" + ex.Message,  model.sendTime, model.adminno);
                return false;
            }

		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.adminalarm model,out string mssg)
		{
			 mssg ="";
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("{0}更新管理员{1}的预警通知成功", model.sendTime, model.adminno);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}更新管理员{1}的预警通知失败", model.sendTime, model.adminno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}更新管理员{1}的预警通知出错，错误信息:" + ex.Message, model.sendTime, model.adminno);
                return false;
            }

		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int aid)
		{
			
			return dal.Delete(aid);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string aidlist )
		{
			return dal.DeleteList(aidlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public AuthorityAlarm.Model.adminalarm GetModel(int aid)
		{
			
			return dal.GetModel(aid);
		}

		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<AuthorityAlarm.Model.adminalarm> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<AuthorityAlarm.Model.adminalarm> DataTableToList(DataTable dt)
		{
			List<AuthorityAlarm.Model.adminalarm> modelList = new List<AuthorityAlarm.Model.adminalarm>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				AuthorityAlarm.Model.adminalarm model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

        public bool AdministratorAlarmTableLoad(int xmno, int adminno, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.AdministratorAlarmTableLoad(xmno, adminno, out dt))
                {
                    mssg = string.Format("获取项目编号{0}管理员{1}的预警信息成功", xmno, adminno);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目编号{0}管理员{1}的预警信息失败", xmno, adminno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}管理员{1}的预警信息出错，错误信息:"+ex.Message, xmno, adminno);
                return false;
            }
        }



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

