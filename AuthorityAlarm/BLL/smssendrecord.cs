﻿/**  版本信息模板在安装目录下，可自行修改。
* smssendrecord.cs
*
* 功 能： N/A
* 类 名： smssendrecord
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/2 13:49:49   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
namespace AuthorityAlarm.BLL
{
	/// <summary>
	/// 数据访问类:smssendrecord
	/// </summary>
	public partial class smssendrecord
	{
        public static database db = new database();
        private readonly AuthorityAlarm.DAL.smssendrecord dal = new AuthorityAlarm.DAL.smssendrecord();
		public smssendrecord()
		{}
		#region  BasicMethod

		
		


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.smssendrecord model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}发送时间{1}的短信发送记录成功",  model.xmno,model.sendtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}发送时间{1}的短信发送记录失败", model.xmno, model.sendtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}发送时间{1}的短信发送记录出错，错误信息:"+ex.Message, model.xmno, model.sendtime);
                return false;
            }
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        //public bool Update(AuthorityAlarm.Model.smssendrecord model,out string mssg)
        //{
        //    try
        //    {
        //        if (dal.Update(model))
        //        {
        //            mssg = string.Format("更新监测员{0}项目编号{1}{2}点{3}的短信发送记录成功", model.EliminationMonitorID, model.xmno, model.jclx, model.point_name);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("更新监测员{0}项目编号{1}{2}点{3}的短信发送记录失败", model.EliminationMonitorID, model.xmno, model.jclx, model.point_name);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("更新监测员{0}项目编号{1}{2}点{3}的短信发送记录出错,错误信息:" + ex.Message, model.EliminationMonitorID, model.xmno, model.jclx, model.point_name);
        //        return false;
        //    }
        //}

       

		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool Delete(int id)
        //{
			
			
        //}



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int id, string unitname, out AuthorityAlarm.Model.smssendrecord model,out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(id, unitname, out model))
                {
                    mssg = string.Format("获取机构{0}短信发送记录编号{1}的对象成功", unitname, id);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取机构{0}短信发送记录编号{1}的对象失败", unitname, id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取机构{0}短信发送记录编号{1}的对象出错,错误信息："+ex.Message, unitname, id);
                return false;
            }
        }


        public bool smssendrecordTableLoad(string unitname,int xmno,DateTime starttime,DateTime endtime,string keyword, int startPageIndex, int pageSize,string sordname,string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.smssendrecordTableLoad( unitname, xmno, starttime, endtime, keyword,  startPageIndex,  pageSize, sordname, sord, out  dt))
                {
                    mssg = string.Format("加载项目编号{0}发送日期日期在{1}-{2}之间的短信发送记录{3}条成功",xmno,starttime,endtime,dt.Rows.Count );
                    return true;
                }
                else
                {

                    mssg = string.Format("加载项目编号{0}发送日期日期在{1}-{2}之间的短信发送记录失败", xmno, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目编号{0}发送日期日期在{1}-{2}之间的短信发送记录出错,错误信息:"+ex.Message, xmno, starttime, endtime);
                return false;
            }
        }



        public bool smssendrecordTableLoadCount(string unitname, int xmno, DateTime starttime, DateTime endtime, string keyword, out int cont,out string mssg)
        {
            cont = 0;
            mssg = "";
            try
            {
                if (dal.smssendrecordTableLoadCount(  unitname,  xmno,  starttime,  endtime,  keyword, out  cont))
                {
                    mssg = string.Format("加载项目编号{0}发送日期日期在{1}-{2}之间的短信发送记录{3}条成功", xmno, starttime, endtime, cont);
                    return true;
                }
                else
                {

                    mssg = string.Format("加载项目编号{0}发送日期日期在{1}-{2}之间的短信发送记录失败", xmno, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目编号{0}发送日期日期在{1}-{2}之间的短信发送记录出错,错误信息:" + ex.Message, xmno, starttime, endtime);
                return false;
            }
        }
	

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

