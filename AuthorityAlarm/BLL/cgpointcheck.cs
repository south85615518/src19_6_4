﻿/**  版本信息模板在安装目录下，可自行修改。
* pointcheck.cs
*
* 功 能： N/A
* 类 名： pointcheck
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/9 10:01:27   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using AuthorityAlarm.Model;
using Tool;
using System.Text;
namespace AuthorityAlarm.BLL
{
    /// <summary>
    /// pointcheck
    /// </summary>
    public partial class pointcheck
    {
     
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool CgAdd(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {

            try
            {
                if (dal.CgAdd(model))
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}成果预警编号{3}的成果预警记录添加成功", model.xmno, model.type, model.point_name, model.pid);
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}成果预警编号{3}的成果预警记录添加失败", model.xmno, model.type, model.point_name, model.pid);
                    ExceptionLog.ExceptionWrite(mssg);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}监测类型{1}点名{2}成果预警编号{3}的成果预警记录添加出错,错误信息:" + ex.Message, model.xmno, model.type, model.point_name, model.pid);
                ExceptionLog.ExceptionWrite(mssg);
                return false;
            }


        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool CgUpdate(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            try
            {
                if (dal.CgUpdate(model))
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}成果预警编号{3}的成果预警记录更新成功", model.xmno, model.type, model.point_name, model.pid);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}监测类型{1}点名{2}成果预警编号{3}的成果预警记录更新失败", model.xmno, model.type, model.point_name, model.pid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}监测类型{1}点名{2}成果预警编号{3}的成果预警记录更新出错,错误信息:" + ex.Message, model.xmno, model.type, model.point_name, model.pid);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool CgDelete(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            try
            {
                if (dal.CgDelete(model))
                {
                    mssg = string.Format("删除项目编号{0}{1}{2}点{3}的成果预警数据成功",model.xmno,model.type,model.point_name,model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}{1}{2}点{3}的成果预警数据失败", model.xmno, model.type, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}{1}{2}点{3}的成果预警数据出错,错误信息：" + ex.Message, model.xmno, model.type, model.point_name, model.time);
                return false;
            }
           
        }

        public bool CgXmHighestAlarm(int xmno,out int alarm,out string mssg)
        {
            alarm = -1;
            try
            {
                if (dal.CgXmHighestAlarm(xmno, out alarm))
                {
                    mssg = string.Format("获取项目编号{0}成果数据的最高级别预警为{1}成功", xmno, alarm);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}成果数据的最高级别预警为{1}失败", xmno, alarm);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}成果数据的最高级别预警出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        public bool CgXmAlarmCount(int xmno, out int cont,out string mssg)
        {
            cont = -1;
            try
            {
                if (dal.CgXmAlarmCount(xmno, out cont))
                {
                    mssg = string.Format("获取项目编号{0}的成果数据预警点数为{1}成功", xmno, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的成果数据预警点数为{1}失败", xmno, cont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的成果预警点数出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgGetModelList(int xmno, out List<AuthorityAlarm.Model.pointcheck> modelList,out string mssg)
        {
            modelList = null;
            try
            {
                if (dal.CgGetModelList(xmno, out modelList))
                {
                    mssg = string.Format("获取项目{0}的成果预警汇总{1}条成功",xmno,modelList.Count) ;
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的成果预警汇总{1}条失败", xmno, modelList.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的成果预警汇总出错，错误信息:" + ex.Message, xmno, modelList.Count);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgGetModel(int xmno, string pointname, string jclx, out AuthorityAlarm.Model.pointcheck model,out string mssg)
        {
            mssg = "";
            model = null;

            try
            {

                if (dal.CgGetModel(xmno, pointname, jclx,out model))
                {
                    mssg = string.Format("获取项目编号{0}{1}{2}点的成果预警记录成功",xmno,jclx,pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}{2}点的成果预警记录失败", xmno, jclx, pointname);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}{2}点的成果预警记录出错,错误信息："+ex.Message, xmno, jclx, pointname);
                return false;
            }
        }

        public bool CgXmAlarmCount(int xmno, string type, out int cont,out string mssg)
        {
            mssg = "";
            cont = 0;
            try
            {
                if (dal.CgXmAlarmCount(xmno, type, out cont))
                {
                    mssg = string.Format("获取项目编号{0}{1}的成果预警点数{2}个",xmno,type,cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}的成果预警点数失败",xmno,type);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}的成果预警点数出错，错误信息:"+ex.Message,xmno,type);
                return false;
            }

            //return false;
        }

        public bool CgMaxAlarm(int xmno, string type, out int alarm,out string mssg)
        {
            alarm = 0;
            mssg = "";
            try
            {
                if (dal.CgMaxAlarm(xmno, type, out alarm))
                {
                    mssg = string.Format("获取项目编号{0}{1}的最高预警级别{2}成功", xmno, type, alarm);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}的最高预警级别失败", xmno, type);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}的最高预警级别失败", xmno, type);
                return false;
            }
        }
        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

