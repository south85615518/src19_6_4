﻿/**  版本信息模板在安装目录下，可自行修改。
* supervisealarm.cs
*
* 功 能： N/A
* 类 名： supervisealarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/7/26 14:41:30   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;

namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:supervisealarm
	/// </summary>
	public partial class supervisealarm
	{
		public supervisealarm()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int sid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from supervisealarm");
			strSql.Append(" where sid=@sid ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@sid",OdbcType.Decimal,8)			};
			parameters[0].Value = sid;

            return false;//OdbcSQLHelper.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.supervisealarm model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into supervisealarm(");
			strSql.Append("sid,xmno,sendTime,context,id)");
			strSql.Append(" values (");
			strSql.Append("@sid,@xmno,@sendTime,@context,@id)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@sid",OdbcType.Decimal,8),
					new OdbcParameter("@xmno",OdbcType.Decimal,8),
					new OdbcParameter("@sendTime",OdbcType.Timestamp),
					new OdbcParameter("@context",OdbcType.VarChar,2000),
					new OdbcParameter("@id",OdbcType.Decimal,8)};
			parameters[0].Value = model.sid;
			parameters[1].Value = model.xmno;
			parameters[2].Value = model.sendTime;
			parameters[3].Value = model.context;
			parameters[4].Value = model.id;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.supervisealarm model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update supervisealarm set ");
			strSql.Append("xmno=@xmno,");
			strSql.Append("context=@context,");
			strSql.Append("id=@id");
			strSql.Append(" where sid=@sid ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Decimal,8),
					new OdbcParameter("@context",OdbcType.VarChar,2000),
					new OdbcParameter("@id",OdbcType.Decimal,8),
					new OdbcParameter("@sid",OdbcType.Decimal,8)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.context;
			parameters[2].Value = model.id;
			parameters[3].Value = model.sid;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int sid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from supervisealarm ");
			strSql.Append(" where sid=@sid ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@sid",OdbcType.Decimal,8)			};
			parameters[0].Value = sid;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string sidlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from supervisealarm ");
			strSql.Append(" where sid in ("+sidlist + ")  ");
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public AuthorityAlarm.Model.supervisealarm GetModel(int sid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select sid,xmno,sendTime,context,id from supervisealarm ");
			strSql.Append(" where sid=@sid ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@sid",OdbcType.Decimal,8)			};
			parameters[0].Value = sid;

			AuthorityAlarm.Model.supervisealarm model=new AuthorityAlarm.Model.supervisealarm();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public AuthorityAlarm.Model.supervisealarm DataRowToModel(DataRow row)
		{
			AuthorityAlarm.Model.supervisealarm model=new AuthorityAlarm.Model.supervisealarm();
			if (row != null)
			{
				if(row["sid"]!=null && row["sid"].ToString()!="")
				{
					model.sid=int.Parse(row["sid"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["sendTime"]!=null && row["sendTime"].ToString()!="")
				{
					model.sendTime=DateTime.Parse(row["sendTime"].ToString());
				}
				if(row["context"]!=null)
				{
					model.context=row["context"].ToString();
				}
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select sid,xmno,sendTime,context,id ");
			strSql.Append(" FROM supervisealarm ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		
		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName",OdbcType.VarChar, 255),
					new OdbcParameter("@fldName",OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize",OdbcType.Int32),
					new OdbcParameter("@PageIndex",OdbcType.Int32),
					new OdbcParameter("@IsReCount",OdbcType.Bit),
					new OdbcParameter("@OrderType",OdbcType.Bit),
					new OdbcParameter("@strWhere",OdbcType.VarChar,1000),
					};
			parameters[0].Value = "supervisealarm";
			parameters[1].Value = "sid";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

