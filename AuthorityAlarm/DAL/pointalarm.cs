﻿/**  版本信息模板在安装目录下，可自行修改。
* pointalarm.cs
*
* 功 能： N/A
* 类 名： pointalarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/9 10:55:22   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:pointalarm
	/// </summary>
	public partial class pointalarm
	{
		public pointalarm()
		{}
		#region  BasicMethod

        public static database db = new database();

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.pointalarm model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert ignore into pointalarm(");
			strSql.Append("pid,point_name,time,type,alarm,atime,xmno,readed)");
			strSql.Append(" values (");
			strSql.Append("@pid,@point_name,@time,@type,@alarm,@atime,@xmno,@readed)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@pid", OdbcType.VarChar,100),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@type", OdbcType.VarChar,200),
					new OdbcParameter("@alarm", OdbcType.Int,11),
					new OdbcParameter("@atime", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@readed", OdbcType.TinyInt,6)};
			parameters[0].Value = model.pid;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.time;
			parameters[3].Value = model.type;
			parameters[4].Value = model.alarm;
			parameters[5].Value = model.atime;
			parameters[6].Value = model.xmno;
			parameters[7].Value = false;
            OdbcSQLHelper.Conn = db.GetStanderCgConn(model.xmno);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.pointalarm model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update pointalarm set ");
			strSql.Append("pid=@pid,");
			strSql.Append("point_name=@point_name,");
			strSql.Append("time=@time,");
			strSql.Append("type=@type,");
			strSql.Append("alarm=@alarm,");
			strSql.Append("atime=@atime,");
			strSql.Append("xmno=@xmno,");
			strSql.Append("readed=@readed");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@pid", OdbcType.VarChar,100),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@type", OdbcType.VarChar,200),
					new OdbcParameter("@alarm", OdbcType.Int,11),
					new OdbcParameter("@atime", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@readed", OdbcType.TinyInt,6)};
			parameters[0].Value = model.pid;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.time;
			parameters[3].Value = model.type;
			parameters[4].Value = model.alarm;
			parameters[5].Value = model.atime;
			parameters[6].Value = model.xmno;
			parameters[7].Value = model.readed;
            OdbcSQLHelper.Conn = db.GetStanderCgConn(model.xmno);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(AuthorityAlarm.Model.pointalarm model)
        {
            OdbcConnection conn = db.GetStanderCgConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from pointalarm ");
            strSql.Append(" where    xmno=@xmno    and  point_name=@point_name  and  type = @type   and    time <=  @time   ");
            OdbcParameter[] parameters = {

                    new OdbcParameter("@xmno",OdbcType.Int),
					new OdbcParameter("@point_name",OdbcType.VarChar,100),
                    new OdbcParameter("@type",OdbcType.VarChar,50),
                    new OdbcParameter("@time",OdbcType.DateTime)
			};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.type;
            parameters[3].Value = model.time;
            ExceptionLog.ExceptionWrite(string.Format("解除项目编号{0}{1}{2}点{3}的预警成功", model.xmno, model.type, model.point_name, model.time));
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public bool GetModel(int xmno, string pointname, string jclx, out AuthorityAlarm.Model.pointalarm  model)
		{
			//该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pid,point_name,time,type,alarm,atime,xmno,readed from pointalarm ");
			strSql.Append(" where    xmno=@xmno    ");
            strSql.Append(" and      point_name=@point_name    ");
            strSql.Append(" and      type=@type    ");
			OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@point_name", OdbcType.VarChar,100),
                    new OdbcParameter("@type", OdbcType.VarChar,100)

			};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            parameters[2].Value = jclx;
            model = null;
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno, out List<AuthorityAlarm.Model.pointalarm> modelList)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select pid,point_name,time,type,alarm,atime,xmno,readed from pointalarm ");
            strSql.Append("  where    xmno = @xmno    and   readed = 0   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno",OdbcType.Int,11)
			};
            parameters[0].Value = xmno;
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            modelList = new List<AuthorityAlarm.Model.pointalarm>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                modelList.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public AuthorityAlarm.Model.pointalarm DataRowToModel(DataRow row)
		{
			AuthorityAlarm.Model.pointalarm model=new AuthorityAlarm.Model.pointalarm();
			if (row != null)
			{
				if(row["pid"]!=null)
				{
					model.pid=row["pid"].ToString();
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
				if(row["type"]!=null)
				{
					model.type=row["type"].ToString();
				}
				if(row["alarm"]!=null && row["alarm"].ToString()!="")
				{
					model.alarm=int.Parse(row["alarm"].ToString());
				}
				if(row["atime"]!=null && row["atime"].ToString()!="")
				{
					model.atime=DateTime.Parse(row["atime"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["readed"]!=null && row["readed"].ToString()!="")
				{
                    model.readed = row["readed"].ToString() == "0" ? false : true;
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pid,point_name,time,type,alarm,atime,xmno,readed ");
			strSql.Append(" FROM pointalarm ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from pointalarm T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        public bool XmHighestAlarm(int xmno, out int alarm)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            strSql.Append("select   max(alarm)   from pointalarm ");
            strSql.Append(" where     xmno=@xmno     and     readed = 0     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10)			};
            parameters[0].Value = xmno;
            alarm = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null && obj.ToString() != string.Empty )
            {
                alarm = (int)obj;
                return true;
            }
            return false;


        }


        public bool XmAlarmCount(int xmno, out int cont)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            strSql.Append("select   count(1)   from pointalarm ");
            strSql.Append(" where     xmno=@xmno     and     readed = 0     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10)			};
            parameters[0].Value = xmno;
            cont = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null && obj.ToString() != string.Empty)
            {

                cont = Convert.ToInt32(obj);
                return true;
            }
            return false;
        }

        



		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "pointalarm";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

