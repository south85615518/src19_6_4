﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmsplitondate.cs
*
* 功 能： N/A
* 类 名： alarmsplitondate
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/24 9:28:56   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:alarmsplitondate
	/// </summary>
	public partial class alarmsplitondate
	{
        public static database db = new database();
		public alarmsplitondate()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.alarmsplitondate model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into alarmsplitondate(");
			strSql.Append("dno,jclx,pointName,xmno,time,itime,alarmContext,adate,alarm,cyc)");
			strSql.Append(" values (");
			strSql.Append("@dno,@jclx,@pointName,@xmno,@time,sysdate(),@alarmContext,@adate,@alarm,@cyc)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@dno", OdbcType.VarChar,100),
					new OdbcParameter("@jclx", OdbcType.VarChar,100),
					new OdbcParameter("@pointName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@alarmContext", OdbcType.VarChar,1000),
					new OdbcParameter("@adate", OdbcType.Date),
                    new OdbcParameter("@alarm", OdbcType.Int),
                     new OdbcParameter("@cyc", OdbcType.Int)
                                         };
			parameters[0].Value = model.dno;
			parameters[1].Value = model.jclx;
			parameters[2].Value = model.pointName;
			parameters[3].Value = model.xmno;
			parameters[4].Value = model.time;
			parameters[5].Value = model.alarmContext;
			parameters[6].Value = model.adate;
            parameters[7].Value = model.alarm;
            parameters[8].Value = model.cyc;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.alarmsplitondate model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update alarmsplitondate set ");
			strSql.Append("dno=@dno,");
			strSql.Append("jclx=@jclx,");
			strSql.Append("pointName=@pointName,");
			strSql.Append("xmno=@xmno,");
			strSql.Append("time=@time,");
            strSql.Append("itime=sysdate(),");
			strSql.Append("alarmContext=@alarmContext,");
			strSql.Append("adate=@adate");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@dno", OdbcType.VarChar,100),
					new OdbcParameter("@jclx", OdbcType.VarChar,100),
					new OdbcParameter("@pointName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@alarmContext", OdbcType.VarChar,1000),
					new OdbcParameter("@adate", OdbcType.Date)};
			parameters[0].Value = model.dno;
			parameters[1].Value = model.jclx;
			parameters[2].Value = model.pointName;
			parameters[3].Value = model.xmno;
			parameters[4].Value = model.time;
			parameters[5].Value = model.alarmContext;
			parameters[6].Value = model.adate;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string jclx,string pointName,DateTime time)
		{
			//该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from alarmsplitondate ");
			strSql.Append(" where       xmno = @xmno   ");
            strSql.Append(" and      jclx = @jclx   ");
            strSql.Append(" and      pointName = @pointName   ");
            strSql.Append(" and      time = @time   ");
			OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@jclx", OdbcType.VarChar,100),
					new OdbcParameter("@pointName", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)
			};
           
            parameters[0].Value = xmno;
            parameters[1].Value = jclx;
            parameters[2].Value = pointName;
            parameters[3].Value = time;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        /// <summary>
        /// 得到一个对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, DateTime startTime, DateTime endTime,string keyWord,out List<AuthorityAlarm.Model.alarmsplitondate> ls)
        {


            startTime = startTime == new DateTime() ? Convert.ToDateTime("1970-1-1 00:00") : startTime ;
            endTime = endTime == new DateTime() ? Convert.ToDateTime("9999-1-1 00:00") :endTime;
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder(256);
            strSql.AppendFormat("select dno,jclx,pointName,alarmsplitondate.xmno as xmno,time,itime,alarmContext,adate,alarmsplitondate.readed as readed,confirm  from  alarmsplitondate  where   alarmsplitondate.xmno=@xmno   and   adate >= @startTime  and  adate <= @endTime  and  alarmContext  like  '%{0}%'     order  by   alarmsplitondate.adate  desc   ",keyWord);
            OdbcParameter[] parameters = {
            new OdbcParameter("@xmno", OdbcType.Int,11),
            new OdbcParameter("@startTime", OdbcType.DateTime),
            new OdbcParameter("@endTime", OdbcType.DateTime)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = startTime;
            parameters[2].Value = endTime;
            ls = new List<AuthorityAlarm.Model.alarmsplitondate>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;

        }


        /// <summary>
        /// 得到一个对象实体列表
        /// </summary>
        public bool GetModel(int xmno,string alarmno, out AuthorityAlarm.Model.alarmsplitondate model)
        {


            model = null;
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder(256);
            strSql.AppendFormat("select dno,jclx,pointName,alarmsplitondate.xmno as xmno,time,itime,alarmContext,adate,alarmsplitondate.readed as readed,confirm  from  alarmsplitondate  where   dno = '{0}' ", alarmno);
           
            
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                model = DataRowToModel(ds.Tables[0].Rows[i]);
                return true;
            }
            return false;

        }

        /// <summary>
        /// 得到一个对象实体列表
        /// </summary>
        public bool GetModelList(int xmno,DateTime dt ,string keyWord,out List<AuthorityAlarm.Model.alarmsplitondate> ls)
        {
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);

            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select dno,jclx,pointName, xmno,time,itime,alarmContext,adate, readed,alarm,confirm   from  alarmsplitondate  where   xmno={0}    and     adate='{1}'    and   alarmContext  like  '%{2}%'     ",xmno,dt.Date,keyWord);
            //string sql = "select * from alarmsplitondate";
            ExceptionLog.ExceptionWrite("库:" + OdbcSQLHelper.Conn.Database + "|" +/*strSql.ToString()*/strSql.ToString());
            //OdbcParameter[] parameters = {
            //new OdbcParameter("@xmno", OdbcType.Int,11),
            //new OdbcParameter("@adate", OdbcType.DateTime)
            //};
            //parameters[0].Value = xmno;
            //parameters[1].Value = dt;
            ls = new List<AuthorityAlarm.Model.alarmsplitondate>();
            DataTable table = querysql.querystanderdb(/*strSql.ToString()*/strSql.ToString(), OdbcSQLHelper.Conn);
            ExceptionLog.ExceptionWrite(table.Rows.Count.ToString());
            if (table == null) return false;
            int i = 0;
            while (i < table.Rows.Count)
            {
                ls.Add(DataRowToModel(table.Rows[i]));
                i++;
            }
            return true;

        }

       


        
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public AuthorityAlarm.Model.alarmsplitondate DataRowToModel(DataRow row)
        {
            AuthorityAlarm.Model.alarmsplitondate model = new AuthorityAlarm.Model.alarmsplitondate();
            if (row != null)
            {
                if (row["dno"] != null)
                {
                    model.dno = row["dno"].ToString();
                }
                if (row["jclx"] != null)
                {
                    model.jclx = row["jclx"].ToString();
                }
                if (row["pointName"] != null)
                {
                    model.pointName = row["pointName"].ToString();
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
                if (row["itime"] != null && row["itime"].ToString() != "")
                {
                    model.itime = DateTime.Parse(row["itime"].ToString());
                }
                if (row["alarmContext"] != null)
                {
                    model.alarmContext = row["alarmContext"].ToString();
                }
                if (row["adate"] != null && row["adate"].ToString() != "")
                {
                    model.adate = DateTime.Parse(row["adate"].ToString());
                }
                if (row["readed"] != null && row["readed"].ToString() != "")
                {
                    model.readed = Convert.ToBoolean(row["readed"].ToString() == "0" ? false : true);
                }
                if (row["confirm"] != null && row["confirm"].ToString() != "")
                {
                    model.confirm = Convert.ToBoolean(row["confirm"].ToString() == "0" ? false : true);
                }
            }
            return model;
        }

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select dno,jclx,pointName,xmno,time,alarmContext,adate ");
			strSql.Append(" FROM alarmsplitondate ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from alarmsplitondate T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        public bool alarmcgsplitondatereaded(int xmno, DateTime dt)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("update alarmsplitondate set readed = 1 where     xmno=@xmno     and      adate = @adate   ");
            OdbcParameter[] parameters = {
            new OdbcParameter("@xmno", OdbcType.Int,11),
            new OdbcParameter("@adate", OdbcType.DateTime)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = dt;
            int row = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (row > 0)
            {
                return true;
            }
            return true;
        }
        public bool alarmcgsplitondateconfirm(int xmno,string alarmid)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("update alarmsplitondate set confirm = @confirm,readed=0  where     dno = @dno   ");
            OdbcParameter[] parameters = {
            new OdbcParameter("@confirm", OdbcType.TinyInt,2),
            new OdbcParameter("@dno", OdbcType.VarChar,100)
			};
            parameters[0].Value = true;
            parameters[1].Value = alarmid;
            int row = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (row > 0)
            {
                return true;
            }
            return true;
        }
        public bool alarmcgsplitondatedelete(int xmno, string alarmid)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from alarmsplitondate  where     dno = @dno   ");
            OdbcParameter[] parameters = {
            new OdbcParameter("@dno", OdbcType.VarChar,100)
			};
            parameters[0].Value = alarmid;
            int row = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (row > 0)
            {
                return true;
            }
            return true;
        }
        public bool alarmcgsplitondatedelete(int xmno, DateTime date)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete  from alarmsplitondate  where   xmno=@xmno  and  adate = @date   ");
            OdbcParameter[] parameters = {
            new OdbcParameter("@xmno", OdbcType.Int,100),
            new OdbcParameter("@date", OdbcType.Date)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = date;
            int row = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (row > 0)
            {
                return true;
            }
            return true;
        }
		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "alarmsplitondate";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/
        public bool UnitDayAlarmListLoad(string unitname,DateTime lasttime,int timeunit , out List<AuthorityAlarm.Model.alarmsplitondate> ls)
        {
            //SingleTonOdbcSQLHelper singleTonOdbcSQLHelper = new SingleTonOdbcSQLHelper();
            //OdbcConnection SurveyConn = db.GetUnitSurveyStanderConn(unitname);
            ls = new List<Model.alarmsplitondate>();
            OdbcConnection conn = db.GetUnitStanderConn(unitname);
            ExceptionLog.ExceptionWrite("库:" + conn.Database);
            if (conn == null ||conn.Database == "") return false;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select dno,jclx,pointName, xmno,time,itime,alarmContext,adate, readed,alarm,confirm   from  alarmsplitondate where  time between '{0}'  and  '{1}'  and  pointName in (select point_name from pointcheck  )   order by  alarm,itime  desc ", lasttime.AddHours(-timeunit), lasttime);
            //string sql = "select * from alarmsplitondate";
            //OdbcParameter[] parameters = {
            //new OdbcParameter("@xmno", OdbcType.Int,11),
            //new OdbcParameter("@adate", OdbcType.DateTime)
            //};
            //parameters[0].Value = xmno;
            //parameters[1].Value = dt;
            
            DataTable dt =querysql.querystanderdb(strSql.ToString(),conn); //querysql.querystanderdb(/*strSql.ToString()*/strSql.ToString(), OdbcSQLHelper.Conn);
            //ExceptionLog.ExceptionWrite(ds.Tables[0].Rows.Count.ToString());
            if (dt == null) return false;
            int i = 0;
            while (i < dt.Rows.Count)
            {
                ls.Add(DataRowToModel(dt.Rows[i]));
                i++;
            }
            return true;
        }
        public bool XmDayAlarmListLoad(int xmno, DateTime lasttime, int timeunit, out List<AuthorityAlarm.Model.alarmsplitondate> ls)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);

            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select dno,jclx,pointName, xmno,time,itime,alarmContext,adate, readed,alarm,confirm   from  alarmsplitondate  where xmno = {0}  and  time between '{1}'  and  '{2}'  and  pointName in (select point_name from pointcheck  )    order by alarm, itime  desc ", xmno, lasttime.AddHours(-timeunit), lasttime);
            //string sql = "select * from alarmsplitondate";
            ExceptionLog.ExceptionWrite("库:" + OdbcSQLHelper.Conn.Database + "|" +/*strSql.ToString()*/strSql.ToString());
            //OdbcParameter[] parameters = {
            //new OdbcParameter("@xmno", OdbcType.Int,11),
            //new OdbcParameter("@adate", OdbcType.DateTime)
            //};
            //parameters[0].Value = xmno;
            //parameters[1].Value = dt;
            ls = new List<AuthorityAlarm.Model.alarmsplitondate>();
            DataTable table = querysql.querystanderdb(/*strSql.ToString()*/strSql.ToString(), OdbcSQLHelper.Conn);
            ExceptionLog.ExceptionWrite(table.Rows.Count.ToString());
            if (table == null) return false;
            int i = 0;
            while (i < table.Rows.Count)
            {
                ls.Add(DataRowToModel(table.Rows[i]));
                i++;
            }
            return true;
        }

        public bool AlarmListGetByCycList(int xmno, data.Model.gtsensortype datatype, List<string> cyclist, out List<AuthorityAlarm.Model.alarmsplitondate> alarmsplitondatelist)
        {
            StringBuilder strsql = new StringBuilder(255);
            strsql.AppendFormat("select dno,jclx,pointName, xmno,time,itime,alarmContext,adate, readed,alarm,confirm from  alarmsplitondate where xmno = {0} and jclx='{1}' and  cyc in ({2}) ", xmno, data.DAL.gtsensortype.GTSensorTypeToAlarmString(datatype), string.Join(",", cyclist));
            ExceptionLog.ExceptionWrite(strsql.ToString());
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
            alarmsplitondatelist = new List<Model.alarmsplitondate>();
            if(ds.Tables[0].Rows.Count == 0) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                alarmsplitondatelist.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

