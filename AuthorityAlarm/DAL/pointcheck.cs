﻿/**  版本信息模板在安装目录下，可自行修改。
* pointcheck.cs
*
* 功 能： N/A
* 类 名： pointcheck
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/9 10:01:27   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;

namespace AuthorityAlarm.DAL
{
    /// <summary>
    /// 数据访问类:pointcheck
    /// </summary>
    public partial class pointcheck
    {
        public static database db = new database();
        public pointcheck()
        { }
        #region  BasicMethod



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(AuthorityAlarm.Model.pointcheck model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("replace into pointcheck(");
            strSql.Append("pid,point_name,time,type,alarm,atime,xmno,readed)");
            strSql.Append(" values (");
            strSql.Append("@pid,@point_name,@time,@type,@alarm,@atime,@xmno,@readed)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@pid",OdbcType.VarChar,100),
					new OdbcParameter("@point_name",OdbcType.VarChar,100),
					new OdbcParameter("@time",OdbcType.DateTime),
					new OdbcParameter("@type",OdbcType.VarChar,200),
					new OdbcParameter("@alarm",OdbcType.Int,11),
					new OdbcParameter("@atime",OdbcType.DateTime),
					new OdbcParameter("@xmno",OdbcType.Int,11),
					new OdbcParameter("@readed",OdbcType.TinyInt,6)
                                         };
            parameters[0].Value = model.pid;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.time;
            parameters[3].Value = model.type;
            parameters[4].Value = model.alarm;
            parameters[5].Value = model.atime;
            parameters[6].Value = model.xmno;
            parameters[7].Value = false;
            int rows = 0;
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(AuthorityAlarm.Model.pointcheck model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update pointcheck set ");
            strSql.Append("alarm=@alarm,");
            strSql.Append("time=@time,");
            strSql.Append("atime=sysdate()");
            strSql.Append(" where   point_name=@point_name    and     time<=@_time    and     xmno = @xmno    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@alarm",OdbcType.Int),
					new OdbcParameter("@time",OdbcType.DateTime),
                    new OdbcParameter("@point_name",OdbcType.VarChar,50),
                    new OdbcParameter("@_time",OdbcType.DateTime),
                    new OdbcParameter("@xmno",OdbcType.Int)
                                         };
            parameters[0].Value = model.alarm;
            parameters[1].Value = model.time;
            parameters[2].Value = model.point_name;
            parameters[3].Value = model.time;
            parameters[4].Value = model.xmno;
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public bool UpdateAlarm(int xmno,DateTime dt,string pointName)


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(AuthorityAlarm.Model.pointcheck model)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from pointcheck ");
            strSql.Append(" where    xmno=@xmno    and  point_name=@point_name  and  type = @type   and    time <=  @time   ");
            OdbcParameter[] parameters = {

                    new OdbcParameter("@xmno",OdbcType.Int),
					new OdbcParameter("@point_name",OdbcType.VarChar,100),
                    new OdbcParameter("@type",OdbcType.VarChar,50),
                    new OdbcParameter("@time",OdbcType.DateTime)
			};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.type;
            parameters[3].Value = model.time;
            ExceptionLog.ExceptionWrite(string.Format("删除项目编号{0}{1}{2}点{3}的数据成功",model.xmno,model.type,model.point_name,model.time));
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool  GetModel(int xmno ,string pointname ,string jclx, out AuthorityAlarm.Model.pointcheck model )
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select pid,point_name,time,type,alarm,atime,xmno,readed from pointcheck ");
            strSql.Append(" where ");
            strSql.Append("    point_name=@point_name    and     xmno = @xmno    and    type=@type    and    readed = 0     ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@point_name",OdbcType.VarChar,50),
                    new OdbcParameter("@xmno",OdbcType.Int,10),
                    new OdbcParameter("@type",OdbcType.VarChar,50)

			};
            parameters[0].Value = pointname;
            parameters[1].Value = xmno;
            parameters[2].Value = jclx;
            model = null;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool  GetModelList(int xmno,out List<AuthorityAlarm.Model.pointcheck> modelList)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select pid,point_name,time,type,alarm,atime,xmno,readed from pointcheck ");
            strSql.Append("  where    xmno = @xmno    and   readed = 0   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno",OdbcType.Int,11)
			};
            parameters[0].Value = xmno;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            modelList = new List<AuthorityAlarm.Model.pointcheck>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                modelList.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        public bool AlarmXmnoGet(string unitname,out List<string> xmnolist )
        {
            OdbcSQLHelper.Conn = db.GetUnitStanderConn(unitname);
            string sql = "select distinct(xmno) from pointcheck ";
            xmnolist = querysql.querystanderlist(sql,OdbcSQLHelper.Conn);
            return true;
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public AuthorityAlarm.Model.pointcheck DataRowToModel(DataRow row)
        {
            AuthorityAlarm.Model.pointcheck model = new AuthorityAlarm.Model.pointcheck();
            if (row != null)
            {
                if (row["pid"] != null)
                {
                    model.pid = row["pid"].ToString();
                }
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
                if (row["type"] != null)
                {
                    model.type = row["type"].ToString();
                }
                if (row["alarm"] != null && row["alarm"].ToString() != "")
                {
                    model.alarm = int.Parse(row["alarm"].ToString());
                }
                if (row["atime"] != null && row["atime"].ToString() != "")
                {
                    model.atime = DateTime.Parse(row["atime"].ToString());
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["readed"] != null && row["readed"].ToString() != "")
                {
                    model.readed = row["readed"].ToString() == "0" ? false : true; ;
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select pid,point_name,time,type,alarm,atime,xmno,readed ");
            strSql.Append(" FROM pointcheck ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return OdbcSQLHelper.Query(strSql.ToString());
        }


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T. desc");
            }
            strSql.Append(")AS Row, T.*  from pointcheck T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        public bool XmHighestAlarm(int xmno,out int alarm)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select   max(alarm)   from pointcheck ");
            strSql.Append(" where     xmno=@xmno     and     readed = 0     ");
           OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10)			};
            parameters[0].Value = xmno;
            alarm = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
            if (obj != null)
            {

                alarm = (int)obj;
                return true;
            }
            return false;
           

        }

        public bool XmAlarmCount(int xmno, out int cont)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select   count(1)   from pointcheck ");
            strSql.Append(" where     xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10)			};
            parameters[0].Value = xmno;
            cont = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null&&obj.ToString()!=string.Empty)
            {

                cont = Convert.ToInt32(obj);
                return true;
            }
            return false;
        }

        public bool XmAlarmCount(int xmno,string type ,out int cont)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select   count(1)   from pointcheck ");
            strSql.Append(" where     xmno=@xmno    and type=@type     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10),
                    new OdbcParameter("@type",OdbcType.VarChar,50)

                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = type;
            cont = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null && obj.ToString() != string.Empty)
            {

                cont = Convert.ToInt32(obj);
                return true;
            }
            return false;
        }

        public bool MaxAlarm(int xmno, string type, out int alarm)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select   max(alarm)   from pointcheck ");
            strSql.Append(" where     xmno=@xmno    and type=@type     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10),
                    new OdbcParameter("@type",OdbcType.VarChar,50)

                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = type;
            alarm = 0;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null && obj.ToString() != string.Empty)
            {

                alarm = Convert.ToInt32(obj);
                return true;
            }
            return false;
        }

        public bool AlarmLayoutCont(string unitname,out string alarmlaystr)
        {
            alarmlaystr = "0/0/0";
            OdbcSQLHelper.Conn = db.GetUnitStanderConn(unitname);
            ExceptionLog.ExceptionWrite(OdbcSQLHelper.Conn.Database);
            string sql = "select (select count(distinct(xmno)) from pointcheck where alarm >1 ),(select count(distinct(xmno)) from pointcheck where alarm = 1 and  xmno not in (select distinct(xmno) from pointcheck where alarm >1) ),'项目总数' from pointcheck  ";
            DataSet ds = OdbcSQLHelper.Query(sql);
            if (ds == null) return false;
            DataTable dt = ds.Tables[0];
            alarmlaystr = string.Format("{0}/{1}/{2}", dt.Rows[0].ItemArray[0], dt.Rows[0].ItemArray[1], dt.Rows[0].ItemArray[2]);//querysql.querystanderstr(sql, 
            return true;
        }
        public bool AlarmLayoutCont(int xmno, out string alarmlaystr)
        {
            alarmlaystr = "0/0/0/0";
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            string sql =string.Format("select (select count(1) from pointcheck where alarm = 3 and xmno = {0} ),(select count(1) from pointcheck where alarm = 2  and xmno = {0}  ),(select count(1) from pointcheck where alarm = 1 and xmno = {0} ),(select count(1) from pointcheck where alarm = 0 ) from pointcheck  ",xmno);
            DataSet ds = OdbcSQLHelper.Query(sql);
            if (ds == null) return false;
            DataTable dt = ds.Tables[0];
            alarmlaystr = string.Format("{0}/{1}/{2}/{3}", dt.Rows[0].ItemArray[0], dt.Rows[0].ItemArray[1], dt.Rows[0].ItemArray[2], dt.Rows[0].ItemArray[3]);//querysql.querystanderstr(sql, 
            return true;
        }
        public bool AlarmPointLoad(int xmno,out DataTable dt)
        {
            dt = null;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            string sql = string.Format("select * from pointcheck where xmno = {0} order by point_name,type asc,alarm desc ", xmno);
            DataSet ds = OdbcSQLHelper.Query(sql);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;
        }
        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OdbcParameter[] parameters = {
                    new OdbcParameter("@tblName",OdbcType.VarChar, 255),
                    new OdbcParameter("@fldName",OdbcType.VarChar, 255),
                    new OdbcParameter("@PageSize",OdbcType.Int),
                    new OdbcParameter("@PageIndex",OdbcType.Int),
                    new OdbcParameter("@IsReCount",OdbcType.Bit),
                    new OdbcParameter("@OrderType",OdbcType.Bit),
                    new OdbcParameter("@strWhere",OdbcType.VarChar,1000),
                    };
            parameters[0].Value = "pointcheck";
            parameters[1].Value = "";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

