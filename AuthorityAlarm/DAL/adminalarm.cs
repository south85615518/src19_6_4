﻿/**  版本信息模板在安装目录下，可自行修改。
* adminalarm.cs
*
* 功 能： N/A
* 类 名： adminalarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/7/26 14:41:30   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
namespace AuthorityAlarm.DAL
{
    /// <summary>
    /// 数据访问类:adminalarm
    /// </summary>
    public partial class adminalarm
    {
        public static database db = new database();
        public adminalarm()
        { }
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int aid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from adminalarm");
            strSql.Append(" where aid=@aid ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@aid", OdbcType.Decimal,8)			};
            parameters[0].Value = aid;

            return false;//DbHelperMySQL.Exists(strSql.ToString(),parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(AuthorityAlarm.Model.adminalarm model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("insert into adminalarm(");
            strSql.Append("aid,xmno,sendTime,context,adminno,confirm,mess,mail,ForwardTime,dataType)");
            strSql.Append(" values (");
            strSql.Append("@aid,@xmno,@sendTime,@context,@adminno,@confirm,@mess,@mail,@ForwardTime,@dataType)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@aid", OdbcType.VarChar,200),
					new OdbcParameter("@xmno", OdbcType.Int,10),
					new OdbcParameter("@sendTime", OdbcType.DateTime),
					new OdbcParameter("@context", OdbcType.VarChar,2000),
					new OdbcParameter("@adminno", OdbcType.VarChar,100),
					new OdbcParameter("@confirm", OdbcType.TinyInt,2),
					new OdbcParameter("@mess", OdbcType.TinyInt,2),
					new OdbcParameter("@mail", OdbcType.TinyInt,2),
					new OdbcParameter("@ForwardTime", OdbcType.Int,11),
					new OdbcParameter("@dataType", OdbcType.TinyInt,2)};
            parameters[0].Value = model.aid;
            parameters[1].Value = model.xmno;
            parameters[2].Value = model.sendTime;
            parameters[3].Value = model.context;
            parameters[4].Value = model.adminno;
            parameters[5].Value = model.confirm;
            parameters[6].Value = model.mess;
            parameters[7].Value = model.mail;
            parameters[8].Value = model.ForwardTime;
            parameters[9].Value = model.dataType;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(AuthorityAlarm.Model.adminalarm model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("update adminalarm set ");
            strSql.Append("xmno=@xmno,");
            strSql.Append("context=@context,");
            strSql.Append("adminno=@adminno,");
            strSql.Append("confirm=@confirm,");
            strSql.Append("mess=@mess,");
            strSql.Append("mail=@mail,");
            strSql.Append("ForwardTime=@ForwardTime,");
            strSql.Append(" dataType=@dataType   ");
            strSql.Append(" where   aid=@aid   ");
            OdbcParameter[] parameters = {
                    
					new OdbcParameter("@xmno", OdbcType.Int,10),
					new OdbcParameter("@context", OdbcType.VarChar,2000),
					new OdbcParameter("@adminno", OdbcType.VarChar,100),
					new OdbcParameter("@confirm", OdbcType.TinyInt,2),
					new OdbcParameter("@mess", OdbcType.TinyInt,2),
					new OdbcParameter("@mail", OdbcType.TinyInt,2),
					new OdbcParameter("@ForwardTime", OdbcType.Int,11),
					new OdbcParameter("@dataType", OdbcType.TinyInt,2),
                    new OdbcParameter("@aid", OdbcType.VarChar,200)

                                         };
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.context;
            parameters[2].Value = model.adminno;
            parameters[3].Value = model.confirm;
            parameters[4].Value = model.mess;
            parameters[5].Value = model.mail;
            parameters[6].Value = model.ForwardTime;
            parameters[7].Value = model.dataType;
            parameters[8].Value = model.aid;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int aid)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from adminalarm ");
            strSql.Append(" where aid=@aid ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@aid", OdbcType.Decimal,8)			};
            parameters[0].Value = aid;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string aidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from adminalarm ");
            strSql.Append(" where aid in (" + aidlist + ")  ");
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public AuthorityAlarm.Model.adminalarm GetModel(int aid)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select aid,xmno,sendTime,context,adminno,confirm,mess,mail,ForwardTime,dataType from adminalarm ");
            strSql.Append(" where aid=@aid ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@aid", OdbcType.Decimal,8)			};
            parameters[0].Value = aid;

            AuthorityAlarm.Model.adminalarm model = new AuthorityAlarm.Model.adminalarm();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public AuthorityAlarm.Model.adminalarm DataRowToModel(DataRow row)
        {
            AuthorityAlarm.Model.adminalarm model = new AuthorityAlarm.Model.adminalarm();
            if (row != null)
            {
                if (row["aid"] != null && row["aid"].ToString() != "")
                {
                    model.aid = row["aid"].ToString();
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["sendTime"] != null && row["sendTime"].ToString() != "")
                {
                    model.sendTime = DateTime.Parse(row["sendTime"].ToString());
                }
                if (row["context"] != null)
                {
                    model.context = row["context"].ToString();
                }
                if (row["adminno"] != null && row["adminno"].ToString() != "")
                {
                    model.adminno = row["adminno"].ToString();
                }
                if (row["confirm"] != null && row["confirm"].ToString() != "")
                {
                    model.confirm = row["confirm"].ToString() == "1" ? true : false;
                }
                if (row["mess"] != null && row["mess"].ToString() != "")
                {
                    model.mess = row["mess"].ToString() == "1" ? true : false;
                }
                if (row["mail"] != null && row["mail"].ToString() != "")
                {
                    model.mail = row["mail"].ToString() == "1" ? true : false;
                }
                if (row["ForwardTime"] != null && row["ForwardTime"].ToString() != "")
                {
                    model.ForwardTime = int.Parse(row["ForwardTime"].ToString());
                }
                if (row["dataType"] != null && row["dataType"].ToString() != "")
                {
                    model.dataType = int.Parse(row["dataType"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select aid,xmno,sendTime,context,adminno,confirm,mess,mail,ForwardTime,dataType ");
            strSql.Append(" FROM adminalarm ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return OdbcSQLHelper.Query(strSql.ToString());
        }



        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OdbcParameter[] parameters = {
                    new OdbcParameter("@tblName", OdbcType.VarChar, 255),
                    new OdbcParameter("@fldName", OdbcType.VarChar, 255),
                    new OdbcParameter("@PageSize", OdbcType.Int32),
                    new OdbcParameter("@PageIndex", OdbcType.Int32),
                    new OdbcParameter("@IsReCount", OdbcType.Bit),
                    new OdbcParameter("@OrderType", OdbcType.Bit),
                    new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
                    };
            parameters[0].Value = "adminalarm";
            parameters[1].Value = "aid";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperMySQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        public bool AdministratorAlarmTableLoad(int xmno, int adminno, out DataTable dt)
        {
            dt = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select aid,xmno,sendTime,context,adminno,confirm,mess,mail,ForwardTime,dataType from adminalarm  ");
            strSql.Append("  where   xmno = @xmno   and  adminno = @adminno    ");
            OdbcParameter[] parameters = {
                                             new OdbcParameter("@xmno",OdbcType.Int,10),
                                             new OdbcParameter("@adminno",OdbcType.Int,50)
							
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = adminno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds != null)
            {
                dt = ds.Tables[0];
                return true;
            }
            return false;
        }


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

