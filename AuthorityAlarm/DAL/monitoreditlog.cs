﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoreditlog.cs
*
* 功 能： N/A
* 类 名： monitoreditlog
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/13 10:00:19   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:monitoreditlog
	/// </summary>
	public partial class monitoreditlog
	{
		public monitoreditlog()
		{}
		#region  BasicMethod


        public static database db = new database();
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(int ID)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from monitoreditlog");
        //    strSql.Append(" where ID=@ID");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@ID", OdbcType.Int)
        //    };
        //    parameters[0].Value = ID;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add( AuthorityAlarm.Model.monitoreditlog model)
		{
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into monitoreditlog(");
            strSql.Append("xmno,xmname,userID,username,jclx,activetype,beforeactive,afteractive,deletecontext,addcontext,activetime)");
			strSql.Append(" values (");
            strSql.Append("@xmno,@xmname,@userID,@username,@jclx,@activetype,@beforeactive,@afteractive,@deletecontext,@addcontext,@activetime)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@userID", OdbcType.VarChar,100),
					new OdbcParameter("@username", OdbcType.VarChar,100),
					new OdbcParameter("@jclx", OdbcType.VarChar,100),
					new OdbcParameter("@activetype", OdbcType.VarChar,100),
					new OdbcParameter("@beforeactive", OdbcType.VarChar,500),
					new OdbcParameter("@afteractive", OdbcType.VarChar,500),
					new OdbcParameter("@deletecontext", OdbcType.VarChar,500),
					new OdbcParameter("@addcontext", OdbcType.VarChar,500),
					new OdbcParameter("@activetime", OdbcType.DateTime)};
			parameters[0].Value = model.xmno;
            parameters[1].Value = model.xmname;
            parameters[2].Value = model.userID;
			parameters[3].Value = model.username;
			parameters[4].Value = model.jclx;
			parameters[5].Value = model.activetype;
            parameters[6].Value = model.beforeactive == "" ? "\\" : model.beforeactive;
            parameters[7].Value = model.afteractive == "" ? "\\" : model.afteractive;
            parameters[8].Value = model.deletecontext == "" ? "\\" : model.deletecontext;
            parameters[9].Value = model.addcontext == "" ? "\\" : model.addcontext;
			parameters[10].Value = model.activetime;

			int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update( AuthorityAlarm.Model.monitoreditlog model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update monitoreditlog set ");
			strSql.Append("xmno=@xmno,");
			strSql.Append("username=@username,");
			strSql.Append("jclx=@jclx,");
			strSql.Append("activetype=@activetype,");
			strSql.Append("beforeactive=@beforeactive,");
			strSql.Append("afteractive=@afteractive,");
			strSql.Append("deletecontext=@deletecontext,");
			strSql.Append("addcontext=@addcontext,");
			strSql.Append("activetime=@activetime");
			strSql.Append(" where ID=@ID");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@username", OdbcType.VarChar,100),
					new OdbcParameter("@jclx", OdbcType.VarChar,100),
					new OdbcParameter("@activetype", OdbcType.VarChar,100),
					new OdbcParameter("@beforeactive", OdbcType.VarChar,500),
					new OdbcParameter("@afteractive", OdbcType.VarChar,500),
					new OdbcParameter("@deletecontext", OdbcType.VarChar,500),
					new OdbcParameter("@addcontext", OdbcType.VarChar,500),
					new OdbcParameter("@activetime", OdbcType.DateTime),
					new OdbcParameter("@ID", OdbcType.Int,11)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.username;
			parameters[2].Value = model.jclx;
			parameters[3].Value = model.activetype;
            parameters[4].Value = model.beforeactive;
            parameters[5].Value = model.afteractive ;
            parameters[6].Value = model.deletecontext ;
            parameters[7].Value = model.addcontext;
			parameters[8].Value = model.activetime;
			parameters[9].Value = model.ID;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        
        public bool MonitorEditLogTableLoad(string unitname, string monitorID, int xmno, string jclx,  DateTime starttime, DateTime endtime, string keyword, int startPageIndex, int pageSize, string sordname, string sord, out DataTable dt)
        {
            
            dt = null;
            StringBuilder strsql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            string usernameStr = monitorID == "" ? "  1=1 " : string.Format("  username ='{0}'  ", monitorID);
            string beforeactiveStr = keyword == "" ? "  1=1  " : string.Format("beforeactive like  '%{0}%'", keyword);
            string afteractiveStr = keyword == "" ? "  1=1  " : string.Format("afteractive like  '%{0}%'", keyword);
            string deletecontext = keyword == "" ? "  1=1  " : string.Format("deletecontext like  '%{0}%'", keyword);
            string addcontext = keyword == "" ? "  1=1  " : string.Format("addcontext like  '%{0}%'", keyword);
            string jclxStr = jclx == "" ? "  1=1  " : string.Format("jclx like  '%{0}%'", jclx);
            string xmnostr = xmno == -1 ? "  1=1  " : string.Format("  xmno  =  {0}  ", xmno);

            strsql.AppendFormat(@"SELECT id,xmname,username,jclx,activetype,beforeactive,afteractive,deletecontext,addcontext,activetime FROM `monitoreditlog` where  {0}
 and {1} or {2} or {3} or {4}  and activetime  between   '{5}'  and  '{6}'   and  {7} and {8}    order  by   {9}  {10}  limit {11},{12}  ;", usernameStr, beforeactiveStr, afteractiveStr, deletecontext, addcontext, starttime, endtime, jclxStr,xmnostr , sordname, sord, startPageIndex, pageSize);
            
            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
            if (ds == null || ds.Tables[0].Rows.Count == 0) return false;
            dt = ds.Tables[0];
            return true;
        }


        public bool MonitorEditLogTableCountLoad(string unitname, string monitorID, int xmno, string jclx, DateTime starttime, DateTime endtime, string keyword, out int cont)
        {
            cont = -1;
            StringBuilder strsql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            string usernameStr = monitorID == "" ? "  1=1 " : string.Format("  username ='{0}'  ", monitorID);
            string beforeactiveStr = keyword == "" ? "  1=1  " : string.Format("beforeactive like  '%{0}%'", keyword);
            string afteractiveStr = keyword == "" ? "  1=1  " : string.Format("afteractive like  '%{0}%'", keyword);
            string deletecontext = keyword == "" ? "  1=1  " : string.Format("deletecontext like  '%{0}%'", keyword);
            string addcontext = keyword == "" ? "  1=1  " : string.Format("addcontext like  '%{0}%'", keyword);
            string jclxStr = jclx == "" ? "  1=1  " : string.Format("jclx like  '%{0}%'", jclx);
            string xmnostr = xmno == -1 ? "  1=1  " : string.Format("  xmno  =  {0}  ", xmno);

            strsql.AppendFormat(@"SELECT * FROM `monitoreditlog` where  {0}
 and {1} or {2} or {3} or {4}  and activetime  between   '{5}'  and  '{6}'   and  {7} and {8}  ;", usernameStr, beforeactiveStr, afteractiveStr, deletecontext, addcontext, starttime, endtime, jclxStr, xmnostr);

            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString());
            if (obj == null) return false;
            cont = Convert.ToInt32(obj);
            return true;
        }


		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from monitoreditlog ");
			strSql.Append(" where ID=@ID");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int)
			};
			parameters[0].Value = ID;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public  bool  GetModel(string unitname,int ID,out AuthorityAlarm.Model.monitoreditlog model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,xmno,username,jclx,activetype,beforeactive,afteractive,deletecontext,addcontext,activetime from monitoreditlog ");
			strSql.Append(" where ID=@ID");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int)
			};
			parameters[0].Value = ID;
			model=new  AuthorityAlarm.Model.monitoreditlog();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public  AuthorityAlarm.Model.monitoreditlog DataRowToModel(DataRow row)
		{
			 AuthorityAlarm.Model.monitoreditlog model=new  AuthorityAlarm.Model.monitoreditlog();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["username"]!=null)
				{
					model.username=row["username"].ToString();
				}
				if(row["jclx"]!=null)
				{
					model.jclx=row["jclx"].ToString();
				}
				if(row["activetype"]!=null)
				{
					model.activetype=row["activetype"].ToString();
				}
				if(row["beforeactive"]!=null)
				{
					model.beforeactive=row["beforeactive"].ToString();
				}
				if(row["afteractive"]!=null)
				{
					model.afteractive=row["afteractive"].ToString();
				}
				if(row["deletecontext"]!=null)
				{
					model.deletecontext=row["deletecontext"].ToString();
				}
				if(row["addcontext"]!=null)
				{
					model.addcontext=row["addcontext"].ToString();
				}
				if(row["activetime"]!=null && row["activetime"].ToString()!="")
				{
					model.activetime=DateTime.Parse(row["activetime"].ToString());
				}
			}
			return model;
		}

		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

