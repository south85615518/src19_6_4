﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoralarm.cs
*
* 功 能： N/A
* 类 名： monitoralarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/7/26 14:41:30   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;

namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:monitoralarm
	/// </summary>
	public partial class monitoralarm
	{
        public static database db = new database();
		public monitoralarm()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int mid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from monitoralarm");
			strSql.Append(" where mid=@mid ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@mid",OdbcType.Decimal,8)			};
			parameters[0].Value = mid;

            return false;//OdbcSQLHelper.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.monitoralarm model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into monitoralarm(");
			strSql.Append("mid,xmno,time,context,unitmember,confirm,mess,mail,ForwardTime,dataType)");
			strSql.Append(" values (");
			strSql.Append("@mid,@xmno,@time,@context,@unitmember,@confirm,@mess,@mail,@ForwardTime,@dataType)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@mid",OdbcType.VarChar,100),
					new OdbcParameter("@xmno",OdbcType.Int,8),
					new OdbcParameter("@time",OdbcType.DateTime),
					new OdbcParameter("@context",OdbcType.VarChar,2000),
					new OdbcParameter("@unitmember",OdbcType.Int,11),
					new OdbcParameter("@confirm",OdbcType.TinyInt,2),
					new OdbcParameter("@mess",OdbcType.TinyInt,2),
					new OdbcParameter("@mail",OdbcType.TinyInt,2),
					new OdbcParameter("@ForwardTime",OdbcType.Int,11),
					new OdbcParameter("@dataType",OdbcType.TinyInt,2)};
			parameters[0].Value = model.mid;
			parameters[1].Value = model.xmno;
			parameters[2].Value = model.time;
			parameters[3].Value = model.context;
			parameters[4].Value = model.unitmember;
			parameters[5].Value = model.confirm;
			parameters[6].Value = model.mess;
			parameters[7].Value = model.mail;
			parameters[8].Value = model.ForwardTime;
			parameters[9].Value = model.dataType;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.monitoralarm model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update monitoralarm set ");
			strSql.Append("xmno=@xmno,");
			strSql.Append("context=@context,");
			strSql.Append("unitmember=@unitmember,");
			strSql.Append("confirm=@confirm,");
			strSql.Append("mess=@mess,");
			strSql.Append("mail=@mail,");
			strSql.Append("ForwardTime=@ForwardTime,");
			strSql.Append("dataType=@dataType ");
			strSql.Append("   where  mid=@mid    ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,8),
					new OdbcParameter("@context",OdbcType.VarChar,2000),
					new OdbcParameter("@unitmember",OdbcType.Int,11),
					new OdbcParameter("@confirm",OdbcType.TinyInt,2),
					new OdbcParameter("@mess",OdbcType.TinyInt,2),
					new OdbcParameter("@mail",OdbcType.TinyInt,2),
					new OdbcParameter("@ForwardTime",OdbcType.Int,11),
					new OdbcParameter("@dataType",OdbcType.TinyInt,2),
					new OdbcParameter("@mid",OdbcType.VarChar,100)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.context;
			parameters[2].Value = model.unitmember;
			parameters[3].Value = model.confirm;
			parameters[4].Value = model.mess;
			parameters[5].Value = model.mail;
			parameters[6].Value = model.ForwardTime;
			parameters[7].Value = model.dataType;
			parameters[8].Value = model.mid;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int mid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from monitoralarm ");
			strSql.Append(" where mid=@mid ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@mid",OdbcType.Decimal,8)			};
			parameters[0].Value = mid;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string midlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from monitoralarm ");
			strSql.Append(" where mid in ("+midlist + ")  ");
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool  GetModel(int mid,out AuthorityAlarm.Model.monitoralarm model )
		{
            model = null;
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select mid,xmno,time,context,unitmember,confirm,mess,mail,ForwardTime,dataType from monitoralarm ");
			strSql.Append(" where mid=@mid ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@mid",OdbcType.Decimal,8)			};
			parameters[0].Value = mid;
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public AuthorityAlarm.Model.monitoralarm DataRowToModel(DataRow row)
		{
			AuthorityAlarm.Model.monitoralarm model=new AuthorityAlarm.Model.monitoralarm();
			if (row != null)
			{
				if(row["mid"]!=null && row["mid"].ToString()!="")
				{
					model.mid=row["mid"].ToString();
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
				if(row["context"]!=null)
				{
					model.context=row["context"].ToString();
				}
				if(row["unitmember"]!=null && row["unitmember"].ToString()!="")
				{
					model.unitmember=int.Parse(row["unitmember"].ToString());
				}
				if(row["confirm"]!=null && row["confirm"].ToString()!="")
				{
					model.confirm=Int32.Parse(row["confirm"].ToString()) == 1 ? true:false;
				}
				if(row["mess"]!=null && row["mess"].ToString()!="")
				{
                    model.mess = Int32.Parse(row["mess"].ToString()) == 1 ? true : false;
				}
				if(row["mail"]!=null && row["mail"].ToString()!="")
				{
                    model.mail = Int32.Parse(row["mail"].ToString()) == 1 ? true : false;
				}
				if(row["ForwardTime"]!=null && row["ForwardTime"].ToString()!="")
				{
					model.ForwardTime=int.Parse(row["ForwardTime"].ToString());
				}
				if(row["dataType"]!=null && row["dataType"].ToString()!="")
				{
					model.dataType=int.Parse(row["dataType"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select mid,xmno,time,context,unitmember,confirm,mess,mail,ForwardTime,dataType ");
			strSql.Append(" FROM monitoralarm ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        public bool LastSendTime(int xmno, out DateTime dt)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select   max(time)   from monitoralarm ");
            strSql.Append(" where     xmno=@xmno  and  mail =1    ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno",OdbcType.Int,10)

                                         };
            parameters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null && obj.ToString() != string.Empty)
            {

                dt = Convert.ToDateTime(obj);
                return true;
            }
            dt = new DateTime();
            return false;
        }
		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName",OdbcType.VarChar, 255),
					new OdbcParameter("@fldName",OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize",OdbcType.Int),
					new OdbcParameter("@PageIndex",OdbcType.Int),
					new OdbcParameter("@IsReCount",OdbcType.Bit),
					new OdbcParameter("@OrderType",OdbcType.Bit),
					new OdbcParameter("@strWhere",OdbcType.VarChar,1000),
					};
			parameters[0].Value = "monitoralarm";
			parameters[1].Value = "mid";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

        public bool MonitorAlarmTableLoad(int xmno, int unitmemberno, out DataTable dt)
        {
            dt = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select mid,xmno,time,context,unitmember,confirm,mess,mail,ForwardTime,dataType from monitoralarm  ");
            strSql.Append("  where   xmno = @xmno   and  unitmember = @unitmember    ");
            OdbcParameter[] parameters = {
                                             new OdbcParameter("@xmno",OdbcType.Int,10),
                                             new OdbcParameter("@unitmember",OdbcType.Int,50)
							
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = unitmemberno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds != null)
            {
                dt = ds.Tables[0];
                return true;
            }
            return false;
        }
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

