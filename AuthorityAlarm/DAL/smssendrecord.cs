﻿/**  版本信息模板在安装目录下，可自行修改。
* smssendrecord.cs
*
* 功 能： N/A
* 类 名： smssendrecord
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/2 13:49:49   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using Tool;
namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:smssendrecord
	/// </summary>
	public partial class smssendrecord
	{
        public static database db = new database();
		public smssendrecord()
		{}
		#region  BasicMethod

		
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from smssendrecord");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;

            return false;//OdbcSQLHelper.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.smssendrecord model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.Append("insert into smssendrecord(");
            strSql.Append("id,xmno,messtype,sendto,sendcontext,sendtime,result)");
            strSql.Append(" values (");
            strSql.Append("@id,@xmno,@messtype,@sendto,@sendcontext,@sendtime,@result)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@messtype", OdbcType.VarChar,200),
					new OdbcParameter("@sendto", OdbcType.VarChar,200),
                    new OdbcParameter("@sendcontext", OdbcType.VarChar,1000),
					new OdbcParameter("@sendtime", OdbcType.DateTime),
					new OdbcParameter("@result", OdbcType.VarChar,200)
                                         };
			parameters[0].Value = model.id;
            parameters[1].Value = model.xmno;
			parameters[2].Value = model.messtype;
            parameters[3].Value = model.sendto;
            parameters[4].Value = model.sendcontext;
			parameters[5].Value = DateTime.Now;
			parameters[6].Value = model.result;
           
			int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        //public bool Update(AuthorityAlarm.Model.smssendrecord model)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
        //    strSql.Append("update smssendrecord set ");
        //    strSql.Append("EliminationMonitorID=@EliminationMonitorID,");
        //    strSql.Append("EliminationContext=@EliminationContext,");
        //    strSql.Append("EliminationTime=@EliminationTime,");
        //    strSql.Append("xmno=@xmno,");
        //    strSql.Append("jclx=@jclx,");
        //    strSql.Append("point_name=@point_name ");
        //    strSql.Append(" where id=@id");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@EliminationMonitorID", OdbcType.VarChar,200),
        //            new OdbcParameter("@EliminationContext", OdbcType.VarChar,500),
        //            new OdbcParameter("@EliminationTime", OdbcType.DateTime),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@jclx", OdbcType.VarChar,200),
        //            new OdbcParameter("@point_name", OdbcType.VarChar,200),
        //            new OdbcParameter("@id", OdbcType.Int,11),

        //                                 };
        //    parameters[0].Value = model.EliminationMonitorID;
        //    parameters[1].Value = model.EliminationContext;
        //    parameters[2].Value = DateTime.Now;
        //    parameters[3].Value = model.xmno;
        //    parameters[4].Value = model.jclx;
        //    parameters[5].Value = model.point_name;
        //    parameters[6].Value = model.id;

        //    int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from smssendrecord ");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        public bool smssendrecordTableLoad(string unitname,int xmno,DateTime starttime,DateTime endtime,string keyword, int startPageIndex, int pageSize,string sordname,string sord,out DataTable dt)
        {
            dt = null;
            StringBuilder strsql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);

            string sendContextStr = keyword == "" ? "  1=1  " : string.Format("sendContext like  '%{0}%'", keyword);
            string xmnostr = xmno == -1 ? "  1=1  " : string.Format("  xmno  =  {0}  ", xmno);
            strsql.AppendFormat(@"SELECT * FROM `smssendrecord` where  {0}
 and sendtime  between   '{1}'  and  '{2}'   and  {3}   order  by   {4}  {5}  limit {6},{7}  ;", sendContextStr, starttime, endtime, xmnostr, sordname, sord, startPageIndex, pageSize);
            ExceptionLog.ExceptionWrite(strsql.ToString());
            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
            if (ds == null || ds.Tables[0].Rows.Count == 0) return false;
            dt = ds.Tables[0];
            return true;
        }

        public bool smssendrecordTableLoadCount(string unitname, int xmno, DateTime starttime, DateTime endtime, string keyword, out int cont)
        {
            cont = 0;
            StringBuilder strsql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            string sendContextStr = keyword == "" ? "  1=1  " : string.Format("sendContext like  '%{0}%'", keyword);
            string xmnostr = xmno == -1 ? "  1=1  " : string.Format("  xmno  =  {0}  ", xmno);
            strsql.AppendFormat(@"SELECT count(1) FROM `smssendrecord` where  {0}
 and sendtime  between   '{1}'  and  '{2}'   and  {3}  ;", sendContextStr, starttime, endtime, xmnostr);
            ExceptionLog.ExceptionWrite(strsql.ToString());
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString());
            if (obj == null) return false;
            cont = Convert.ToInt32(obj);
            return true;
        }


//        public bool smssendrecordTableLoad(string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword, out DataTable dt)
//        {
//            dt = null;
//            StringBuilder strsql = new StringBuilder();
//            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
//            string EliminationMonitorIDStr = monitorID == "" ? "  1=1 " : string.Format("  monitorID ='{0}'  ", monitorID);
//            string EliminationContextStr = keyword == "" ? "  1=1  " : string.Format("EliminationContext like  '%{0}%'", keyword);
//            string jclxStr = jclx == "" ? "  1=1  " : string.Format("  jclx ='{0}'  ", jclx);
//            string pointnamestr = pointname == "" ? "  1=1  " : string.Format("  pointname ='{0}'  ", pointname);

//            strsql.AppendFormat(@"SELECT * FROM `smssendrecord` where  {0}
// and {1} and EliminationTime  between   '{2}'  and  '{3}'   and  {4}  and {5} ;", EliminationMonitorIDStr, EliminationContextStr, starttime, endtime, jclxStr, pointnamestr);
//            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
//            if (ds == null || ds.Tables[0].Rows.Count == 0) return false;
//            dt = ds.Tables[0];
//            return true;
//        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool GetModel(int id,string unitname,out AuthorityAlarm.Model.smssendrecord model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select * from smssendrecord ");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;
			model = new AuthorityAlarm.Model.smssendrecord();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model =  DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public AuthorityAlarm.Model.smssendrecord DataRowToModel(DataRow row)
        {
            AuthorityAlarm.Model.smssendrecord model = new AuthorityAlarm.Model.smssendrecord();
            if (row != null)
            {
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.id = int.Parse(row["id"].ToString());
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["messtype"] != null)
                {
                    model.messtype = row["messtype"].ToString();
                }
                if (row["sendto"] != null)
                {
                    model.sendto = row["sendto"].ToString();
                }
                if (row["sendtime"] != null && row["sendtime"].ToString() != "")
                {
                    model.sendtime = DateTime.Parse(row["sendtime"].ToString());
                }
                if (row["result"] != null)
                {
                    model.result = row["result"].ToString();
                }
                if (row["sendcontext"] != null)
                {
                    model.sendcontext = row["sendcontext"].ToString();
                }
            }
            return model;
        }

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,EliminationMonitorID,EliminationContext,EliminationTime,xmno,jclx ");
			strSql.Append(" FROM smssendrecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

	
	

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

