﻿/**  版本信息模板在安装目录下，可自行修改。
* pointcheck.cs
*
* 功 能： N/A
* 类 名： pointcheck
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/9 10:01:27   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;

namespace AuthorityAlarm.DAL
{
    /// <summary>
    /// 数据访问类:pointcheck
    /// </summary>
    public partial class pointcheck
    {
        
        #region  BasicMethod



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool CgAdd(AuthorityAlarm.Model.pointcheck model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("replace into pointcheck(");
            strSql.Append("pid,point_name,time,type,alarm,atime,xmno,readed)");
            strSql.Append(" values (");
            strSql.Append("@pid,@point_name,@time,@type,@alarm,@atime,@xmno,@readed)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@pid",OdbcType.VarChar,100),
					new OdbcParameter("@point_name",OdbcType.VarChar,100),
					new OdbcParameter("@time",OdbcType.DateTime),
					new OdbcParameter("@type",OdbcType.VarChar,200),
					new OdbcParameter("@alarm",OdbcType.Int,11),
					new OdbcParameter("@atime",OdbcType.DateTime),
					new OdbcParameter("@xmno",OdbcType.Int,11),
					new OdbcParameter("@readed",OdbcType.TinyInt,6)
                                         };
            parameters[0].Value = model.pid;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.time;
            parameters[3].Value = model.type;
            parameters[4].Value = model.alarm;
            parameters[5].Value = model.atime;
            parameters[6].Value = model.xmno;
            parameters[7].Value = false;
            int rows = 0;
            OdbcSQLHelper.Conn = db.GetCgStanderConn(model.xmno);
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool CgUpdate(AuthorityAlarm.Model.pointcheck model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update pointcheck set ");
            strSql.Append("alarm=@alarm,");
            strSql.Append("time=@time,");
            strSql.Append("atime=sysdate()");
            strSql.Append(" where   point_name=@point_name    and     time<=@_time    and     xmno = @xmno    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@alarm",OdbcType.Int),
					new OdbcParameter("@time",OdbcType.DateTime),
                    new OdbcParameter("@point_name",OdbcType.VarChar,50),
                    new OdbcParameter("@_time",OdbcType.DateTime),
                    new OdbcParameter("@xmno",OdbcType.Int)
                                         };
            parameters[0].Value = model.alarm;
            parameters[1].Value = model.time;
            parameters[2].Value = model.point_name;
            parameters[3].Value = model.time;
            parameters[4].Value = model.xmno;
            OdbcSQLHelper.Conn = db.GetCgStanderConn(model.xmno);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public bool UpdateAlarm(int xmno,DateTime dt,string pointName)


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool CgDelete(AuthorityAlarm.Model.pointcheck model)
        {
            OdbcConnection conn = db.GetCgStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from pointcheck ");
            strSql.Append(" where    xmno=@xmno    and  point_name=@point_name  and  type = @type   and    time <=  @time   ");
            OdbcParameter[] parameters = {

                    new OdbcParameter("@xmno",OdbcType.Int),
					new OdbcParameter("@point_name",OdbcType.VarChar,100),
                    new OdbcParameter("@type",OdbcType.VarChar,50),
                    new OdbcParameter("@time",OdbcType.DateTime)
			};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.type;
            parameters[3].Value = model.time;
            ExceptionLog.ExceptionWrite(string.Format("删除项目编号{0}{1}{2}点{3}的数据成功",model.xmno,model.type,model.point_name,model.time));
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool  CgGetModel(int xmno ,string pointname ,string jclx, out AuthorityAlarm.Model.pointcheck model )
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            strSql.Append("select pid,point_name,time,type,alarm,atime,xmno,readed from pointcheck ");
            strSql.Append(" where ");
            strSql.Append("    point_name=@point_name    and     xmno = @xmno    and    type=@type    and    readed = 0     ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@point_name",OdbcType.VarChar,50),
                    new OdbcParameter("@xmno",OdbcType.Int,10),
                    new OdbcParameter("@type",OdbcType.VarChar,50)

			};
            parameters[0].Value = pointname;
            parameters[1].Value = xmno;
            parameters[2].Value = jclx;
            model = null;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool  CgGetModelList(int xmno,out List<AuthorityAlarm.Model.pointcheck> modelList)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select pid,point_name,time,type,alarm,atime,xmno,readed from pointcheck ");
            strSql.Append("  where    xmno = @xmno    and   readed = 0   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno",OdbcType.Int,11)
			};
            parameters[0].Value = xmno;
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            modelList = new List<AuthorityAlarm.Model.pointcheck>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                modelList.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }




      

       


        

        public bool CgXmHighestAlarm(int xmno,out int alarm)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            strSql.Append("select   max(alarm)   from pointcheck ");
            strSql.Append(" where     xmno=@xmno     and     readed = 0     ");
           OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10)			};
            parameters[0].Value = xmno;
            alarm = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
            if (obj != null)
            {

                alarm = (int)obj;
                return true;
            }
            return false;
           

        }

        public bool CgXmAlarmCount(int xmno, out int cont)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            strSql.Append("select   count(1)   from pointcheck ");
            strSql.Append(" where     xmno=@xmno     and     readed = 0     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10)			};
            parameters[0].Value = xmno;
            cont = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null&&obj.ToString()!=string.Empty)
            {

                cont = Convert.ToInt32(obj);
                return true;
            }
            return false;
        }

        public bool CgXmAlarmCount(int xmno,string type ,out int cont)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            strSql.Append("select   count(1)   from pointcheck ");
            strSql.Append(" where     xmno=@xmno    and type=@type     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10),
                    new OdbcParameter("@type",OdbcType.VarChar,50)

                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = type;
            cont = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null && obj.ToString() != string.Empty)
            {

                cont = Convert.ToInt32(obj);
                return true;
            }
            return false;
        }

        public bool CgMaxAlarm(int xmno, string type, out int alarm)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            strSql.Append("select   max(alarm)   from pointcheck ");
            strSql.Append(" where     xmno=@xmno    and type=@type     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10),
                    new OdbcParameter("@type",OdbcType.VarChar,50)

                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = type;
            alarm = 0;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null && obj.ToString() != string.Empty)
            {

                alarm = Convert.ToInt32(obj);
                return true;
            }
            return false;
        }

      

        

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

