﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmelimination.cs
*
* 功 能： N/A
* 类 名： alarmelimination
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/2 13:49:49   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using Tool;
namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:alarmelimination
	/// </summary>
	public partial class alarmelimination
	{
        public static database db = new database();
		public alarmelimination()
		{}
		#region  BasicMethod

		
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from alarmelimination");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;

            return false;//OdbcSQLHelper.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.alarmelimination model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("insert into alarmelimination(");
            strSql.Append("EliminationMonitorID,EliminationMonitorName,EliminationContext,EliminationTime,xmno,xmname,jclx,point_name)");
			strSql.Append(" values (");
            strSql.Append("@EliminationMonitorID,@EliminationMonitorName,@EliminationContext,@EliminationTime,@xmno,@xmname,@jclx,@point_name)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@EliminationMonitorID", OdbcType.VarChar,200),
                    new OdbcParameter("@EliminationMonitorName", OdbcType.VarChar,200),
					new OdbcParameter("@EliminationContext", OdbcType.VarChar,500),
					new OdbcParameter("@EliminationTime", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@xmname", OdbcType.VarChar,500),
					new OdbcParameter("@jclx", OdbcType.VarChar,200),
                    new OdbcParameter("@point_name", OdbcType.VarChar,200)
                                         };
			parameters[0].Value = model.EliminationMonitorID;
            parameters[1].Value = model.eliminationmonitorname;
			parameters[2].Value = model.EliminationContext;
			parameters[3].Value = DateTime.Now;
			parameters[4].Value = model.xmno;
            parameters[5].Value = model.xmname;
			parameters[6].Value = model.jclx;
            parameters[7].Value = model.point_name;
			int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.alarmelimination model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("update alarmelimination set ");
			strSql.Append("EliminationMonitorID=@EliminationMonitorID,");
			strSql.Append("EliminationContext=@EliminationContext,");
			strSql.Append("EliminationTime=@EliminationTime,");
			strSql.Append("xmno=@xmno,");
			strSql.Append("jclx=@jclx,");
            strSql.Append("point_name=@point_name ");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@EliminationMonitorID", OdbcType.VarChar,200),
					new OdbcParameter("@EliminationContext", OdbcType.VarChar,500),
					new OdbcParameter("@EliminationTime", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@jclx", OdbcType.VarChar,200),
                    new OdbcParameter("@point_name", OdbcType.VarChar,200),
					new OdbcParameter("@id", OdbcType.Int,11),

                                         };
			parameters[0].Value = model.EliminationMonitorID;
			parameters[1].Value = model.EliminationContext;
			parameters[2].Value = DateTime.Now;
			parameters[3].Value = model.xmno;
			parameters[4].Value = model.jclx;
            parameters[5].Value = model.point_name;
			parameters[6].Value = model.id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from alarmelimination ");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        public bool AlarmEliminationTableLoad(string unitname,string monitorID,int xmno,string jclx,string pointname,DateTime starttime,DateTime endtime,string keyword, int startPageIndex, int pageSize,string sordname,string sord,out DataTable dt)
        {
            //ExceptionLog.ExceptionWrite("起始页:" + startPageIndex + "行:" + pageSize);
            dt = null;
            StringBuilder strsql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            string EliminationMonitorIDStr = monitorID == "" ? "  1=1 " : string.Format("  EliminationMonitorID ='{0}'  ", monitorID);
            string EliminationContextStr = keyword == "" ? "  1=1  " : string.Format("EliminationContext like  '%{0}%'", keyword);
            string jclxStr = jclx == "" ? "  1=1  " : string.Format("EliminationContext like  '%{0}%'", jclx);
            string pointnamestr = pointname == "" ? "  1=1  " : string.Format("  point_name like  '%{0}%'  ", pointname);
            string xmnostr = xmno == -1 ? "  1=1  " : string.Format("  xmno  =  {0}  ", xmno);
            strsql.AppendFormat(@"SELECT id,EliminationMonitorName,xmname,jclx,point_name,EliminationContext,EliminationTime  FROM `AlarmElimination` where  {0}
 and {1} and EliminationTime  between   '{2}'  and  '{3}'   and  {4}  and {5} and xmno={6}  order  by   {7}  {8}  limit {9},{10}  ;", EliminationMonitorIDStr, EliminationContextStr, starttime, endtime, jclxStr, pointnamestr, xmno,sordname,sord,startPageIndex,pageSize);
            ExceptionLog.ExceptionWrite(strsql.ToString());
            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
            if (ds == null || ds.Tables[0].Rows.Count == 0) return false;
            dt = ds.Tables[0];
            return true;
        }

        public bool AlarmEliminationTableLoadCount(string unitname,string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword, out int cont)
        {
            cont = 0;
            StringBuilder strsql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            string EliminationMonitorIDStr = monitorID == "" ? "  1=1 " : string.Format("  EliminationMonitorID ='{0}'  ", monitorID);
            string EliminationContextStr = keyword == "" ? "  1=1  " : string.Format("EliminationContext like  '%{0}%'", keyword);
            string jclxStr = jclx == "" ? "  1=1  " : string.Format("EliminationContext like  '%{0}%'", jclx);
            string pointnamestr = pointname == "" ? "  1=1  " : string.Format("  point_name ='{0}'  ", pointname);
            starttime = starttime == new DateTime() ? Convert.ToDateTime("1990-1-1") : starttime;
            endtime = endtime == new DateTime() ? Convert.ToDateTime("2990-1-1") : endtime;
            strsql.AppendFormat(@"SELECT count(1) FROM `AlarmElimination` where  {0}
 and {1} and EliminationTime  between   '{2}'  and  '{3}'   and  {4}  and {5} ;", EliminationMonitorIDStr, EliminationContextStr, starttime, endtime, jclxStr, pointnamestr);
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strsql.ToString());
            if (obj == null) return false;
            cont = Convert.ToInt32(obj);
            return true;
        }


//        public bool AlarmEliminationTableLoad(string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword, out DataTable dt)
//        {
//            dt = null;
//            StringBuilder strsql = new StringBuilder();
//            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
//            string EliminationMonitorIDStr = monitorID == "" ? "  1=1 " : string.Format("  monitorID ='{0}'  ", monitorID);
//            string EliminationContextStr = keyword == "" ? "  1=1  " : string.Format("EliminationContext like  '%{0}%'", keyword);
//            string jclxStr = jclx == "" ? "  1=1  " : string.Format("  jclx ='{0}'  ", jclx);
//            string pointnamestr = pointname == "" ? "  1=1  " : string.Format("  pointname ='{0}'  ", pointname);

//            strsql.AppendFormat(@"SELECT * FROM `AlarmElimination` where  {0}
// and {1} and EliminationTime  between   '{2}'  and  '{3}'   and  {4}  and {5} ;", EliminationMonitorIDStr, EliminationContextStr, starttime, endtime, jclxStr, pointnamestr);
//            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
//            if (ds == null || ds.Tables[0].Rows.Count == 0) return false;
//            dt = ds.Tables[0];
//            return true;
//        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool GetModel(int id,string unitname,out AuthorityAlarm.Model.alarmelimination model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select * from alarmelimination ");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;
			model = new AuthorityAlarm.Model.alarmelimination();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model =  DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public AuthorityAlarm.Model.alarmelimination DataRowToModel(DataRow row)
		{
			AuthorityAlarm.Model.alarmelimination model=new AuthorityAlarm.Model.alarmelimination();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["EliminationMonitorID"]!=null)
				{
					model.EliminationMonitorID=row["EliminationMonitorID"].ToString();
				}
                if (row["EliminationMonitorName"] != null)
                {
                    model.eliminationmonitorname = row["EliminationMonitorName"].ToString();
                }
				if(row["EliminationContext"]!=null)
				{
					model.EliminationContext=row["EliminationContext"].ToString();
				}
				if(row["EliminationTime"]!=null && row["EliminationTime"].ToString()!="")
				{
					model.EliminationTime=DateTime.Parse(row["EliminationTime"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
                if (row["xmname"] != null && row["xmname"].ToString() != "")
                {
                    model.xmname = row["xmname"].ToString();
                }
				if(row["jclx"]!=null)
				{
					model.jclx=row["jclx"].ToString();
				}
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,EliminationMonitorID,EliminationContext,EliminationTime,xmno,jclx ");
			strSql.Append(" FROM alarmelimination ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

	
	

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

