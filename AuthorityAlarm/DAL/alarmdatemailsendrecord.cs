﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmdatemailsendrecord.cs
*
* 功 能： N/A
* 类 名： alarmdatemailsendrecord
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/24 10:36:30   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:alarmdatemailsendrecord
	/// </summary>
	public partial class alarmdatemailsendrecord
	{
		public alarmdatemailsendrecord()
		{}
		#region  BasicMethod



        public static database db = new database();
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AuthorityAlarm.Model.alarmdatemailsendrecord model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(model.xmno);
			strSql.Append("insert into alarmdatemailsendrecord(");
			strSql.Append("did,xmno,sdate,stime,scont)");
			strSql.Append(" values (");
			strSql.Append("@did,@xmno,@sdate,sysdate(),@scont)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append(" scont = scont+1,");
            strSql.Append(" stime = sysdate()");
			OdbcParameter[] parameters = {
					new OdbcParameter("@did", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@sdate", OdbcType.Date),
					new OdbcParameter("@scont", OdbcType.Int,11)
                                         };
			parameters[0].Value = model.did;
			parameters[1].Value = model.xmno;
			parameters[2].Value = model.sdate;
			parameters[3].Value = 1;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AuthorityAlarm.Model.alarmdatemailsendrecord model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update alarmdatemailsendrecord set ");
			strSql.Append("stime=@stime,");
			strSql.Append("scont=scont+1");
			strSql.Append(" where xmno=@xmno and sdate=@sdate ");
			OdbcParameter[] parameters = {
					
					new OdbcParameter("@stime", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@sdate", OdbcType.Date)
                                         };
			parameters[0].Value = DateTime.Now;
			parameters[1].Value = model.xmno;
			parameters[2].Value = model.sdate;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string did)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from alarmdatemailsendrecord ");
			strSql.Append(" where did=@did ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@did", OdbcType.VarChar,100)			
                                         };
			parameters[0].Value = did;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	


		

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public AuthorityAlarm.Model.alarmdatemailsendrecord DataRowToModel(DataRow row)
		{
			AuthorityAlarm.Model.alarmdatemailsendrecord model=new AuthorityAlarm.Model.alarmdatemailsendrecord();
			if (row != null)
			{
				if(row["did"]!=null)
				{
					model.did=row["did"].ToString();
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["sdate"]!=null && row["sdate"].ToString()!="")
				{
					model.sdate=DateTime.Parse(row["sdate"].ToString());
				}
				if(row["stime"]!=null && row["stime"].ToString()!="")
				{
					model.stime=DateTime.Parse(row["stime"].ToString());
				}
				if(row["scont"]!=null && row["scont"].ToString()!="")
				{
					model.scont=int.Parse(row["scont"].ToString());
				}
			}
			return model;
		}

		

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "alarmdatemailsendrecord";
			parameters[1].Value = "did";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperMySQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

