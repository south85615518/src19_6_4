﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmsplitondate.cs
*
* 功 能： N/A
* 类 名： alarmsplitondate
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/24 9:28:56   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace AuthorityAlarm.DAL
{
	/// <summary>
	/// 数据访问类:alarmsplitondate
	/// </summary>
    public partial class alarmsplitondate
	{
		#region  BasicMethod
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool CgAdd(AuthorityAlarm.Model.alarmsplitondate model)
		{
            OdbcSQLHelper.Conn = db.GetCgStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into alarmsplitondate(");
			strSql.Append("dno,jclx,pointName,xmno,time,itime,alarmContext,adate,alarm,cyc)");
			strSql.Append(" values (");
			strSql.Append("@dno,@jclx,@pointName,@xmno,@time,sysdate(),@alarmContext,@adate,@alarm,@cyc)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@dno", OdbcType.VarChar,100),
					new OdbcParameter("@jclx", OdbcType.VarChar,100),
					new OdbcParameter("@pointName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@alarmContext", OdbcType.VarChar,1000),
					new OdbcParameter("@adate", OdbcType.Date),
                    new OdbcParameter("@alarm", OdbcType.Int),
                    new OdbcParameter("@cyc", OdbcType.Int)
                                         };
			parameters[0].Value = model.dno;
			parameters[1].Value = model.jclx;
			parameters[2].Value = model.pointName;
			parameters[3].Value = model.xmno;
			parameters[4].Value = model.time;
			parameters[5].Value = model.alarmContext;
			parameters[6].Value = model.adate;
            parameters[7].Value = model.alarm;
            parameters[8].Value = model.cyc;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool CgUpdate(AuthorityAlarm.Model.alarmsplitondate model)
		{
            OdbcSQLHelper.Conn = db.GetCgStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update alarmsplitondate set ");
			strSql.Append("dno=@dno,");
			strSql.Append("jclx=@jclx,");
			strSql.Append("pointName=@pointName,");
			strSql.Append("xmno=@xmno,");
			strSql.Append("time=@time,");
            strSql.Append("itime=sysdate(),");
			strSql.Append("alarmContext=@alarmContext,");
			strSql.Append("adate=@adate");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@dno", OdbcType.VarChar,100),
					new OdbcParameter("@jclx", OdbcType.VarChar,100),
					new OdbcParameter("@pointName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@alarmContext", OdbcType.VarChar,1000),
					new OdbcParameter("@adate", OdbcType.Date)};
			parameters[0].Value = model.dno;
			parameters[1].Value = model.jclx;
			parameters[2].Value = model.pointName;
			parameters[3].Value = model.xmno;
			parameters[4].Value = model.time;
			parameters[5].Value = model.alarmContext;
			parameters[6].Value = model.adate;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool CgDelete(int xmno,string jclx,string pointName,DateTime time)
		{
			//该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from alarmsplitondate ");
			strSql.Append(" where       xmno = @xmno   ");
            strSql.Append(" and      jclx = @jclx   ");
            strSql.Append(" and      pointName = @pointName   ");
            strSql.Append(" and      time = @time   ");
			OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@jclx", OdbcType.VarChar,100),
					new OdbcParameter("@pointName", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)
			};
           
            parameters[0].Value = xmno;
            parameters[1].Value = jclx;
            parameters[2].Value = pointName;
            parameters[3].Value = time;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        /// <summary>
        /// 得到一个对象实体列表
        /// </summary>
        public bool CgGetModelList(int xmno, DateTime startTime, DateTime endTime,string keyWord,out List<AuthorityAlarm.Model.alarmsplitondate> ls)
        {


            startTime = startTime == new DateTime() ? Convert.ToDateTime("1970-1-1 00:00") : startTime ;
            endTime = endTime == new DateTime() ? Convert.ToDateTime("9999-1-1 00:00") :endTime;
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder(256);
            strSql.AppendFormat("select dno,jclx,pointName,alarmsplitondate.xmno as xmno,time,itime,alarmContext,adate,alarmsplitondate.readed as readed  from  alarmsplitondate  where   alarmsplitondate.xmno=@xmno   and   adate>@startTime  and  adate < @endTime  and  alarmContext  like  '%{0}%'     order  by   alarmsplitondate.adate  asc   ",keyWord);
            OdbcParameter[] parameters = {
            new OdbcParameter("@xmno", OdbcType.Int,11),
            new OdbcParameter("@startTime", OdbcType.DateTime),
            new OdbcParameter("@endTime", OdbcType.DateTime)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = startTime;
            parameters[2].Value = endTime;
            ls = new List<AuthorityAlarm.Model.alarmsplitondate>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;

        }
        /// <summary>
        /// 得到一个对象实体列表
        /// </summary>
        public bool CgGetModelList(int xmno,DateTime dt ,string keyWord,out List<AuthorityAlarm.Model.alarmsplitondate> ls)
        {
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select dno,jclx,pointName,alarmsplitondate.xmno as xmno,time,itime,alarmContext,adate,alarmsplitondate.readed as readed,alarm   from  alarmsplitondate  where   alarmsplitondate.xmno=@xmno    and     alarmsplitondate.adate=@adate    and   alarmContext  like  '%{0}%'    order  by   alarmsplitondate.adate  asc   ",keyWord);
            OdbcParameter[] parameters = {
            new OdbcParameter("@xmno", OdbcType.Int,11),
            new OdbcParameter("@adate", OdbcType.DateTime)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = dt;
            ls = new List<AuthorityAlarm.Model.alarmsplitondate>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;

        }

        public bool cgalarmcgsplitondatereaded(int xmno, DateTime dt)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            strSql.Append("update alarmsplitondate set readed = 1 where     xmno=@xmno     and      adate = @adate   ");
            OdbcParameter[] parameters = {
            new OdbcParameter("@xmno", OdbcType.Int,11),
            new OdbcParameter("@adate", OdbcType.DateTime)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = dt;
            int row = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (row > 0)
            {
                return true;
            }
            return true;
        }


	

       




		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

