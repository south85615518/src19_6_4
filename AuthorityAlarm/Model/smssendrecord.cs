﻿/**  版本信息模板在安装目录下，可自行修改。
* smssendrecord.cs
*
* 功 能： N/A
* 类 名： smssendrecord
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/19 14:19:24   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace AuthorityAlarm.Model
{
	/// <summary>
	/// smssendrecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class smssendrecord
	{
		public smssendrecord()
		{}
		#region Model
		private int _id=0;
		private int _xmno;
		private string _messtype;
		private string _sendto;
        private string _sendcontext;

        public string sendcontext
        {
            get { return _sendcontext; }
            set { _sendcontext = value; }
        }
		private DateTime _sendtime;
		private string _result;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string messtype
		{
			set{ _messtype=value;}
			get{return _messtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sendto
		{
			set{ _sendto=value;}
			get{return _sendto;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime sendtime
		{
			set{ _sendtime=value;}
			get{return _sendtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string result
		{
			set{ _result=value;}
			get{return _result;}
		}
		#endregion Model

	}
}

