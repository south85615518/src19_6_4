﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoreditlog.cs
*
* 功 能： N/A
* 类 名： monitoreditlog
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/13 10:00:19   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace AuthorityAlarm.Model
{
	/// <summary>
	/// monitoreditlog:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class monitoreditlog
	{
		public monitoreditlog()
		{}
		#region Model
		private int _id;
		private int _xmno;
        private int _xmname;

        public int xmname
        {
            get { return _xmname; }
            set { _xmname = value; }
        }
        private string _userID;

        public string userID
        {
            get { return _userID; }
            set { _userID = value; }
        }
		private string _username;
		private string _jclx;
		private string _activetype;
		private string _beforeactive;
		private string _afteractive;
		private string _deletecontext;
		private string _addcontext;
		private DateTime _activetime;

		/// <summary>
		/// auto_increment
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string username
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jclx
		{
			set{ _jclx=value;}
			get{return _jclx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string activetype
		{
			set{ _activetype=value;}
			get{return _activetype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string beforeactive
		{
			set{ _beforeactive=value;}
			get{return _beforeactive;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string afteractive
		{
			set{ _afteractive=value;}
			get{return _afteractive;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string deletecontext
		{
			set{ _deletecontext=value;}
			get{return _deletecontext;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string addcontext
		{
			set{ _addcontext=value;}
			get{return _addcontext;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime activetime
		{
			set{ _activetime=value;}
			get{return _activetime;}
		}
		#endregion Model

	}
}

