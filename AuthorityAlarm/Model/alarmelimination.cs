﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmelimination.cs
*
* 功 能： N/A
* 类 名： alarmelimination
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/2 13:49:49   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace AuthorityAlarm.Model
{
	/// <summary>
	/// alarmelimination:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class alarmelimination
	{
		public alarmelimination()
		{}
		#region Model
		private int _id;
		private string _eliminationmonitorid;
        private string _eliminationmonitorname;

        public string eliminationmonitorname
        {
            get { return _eliminationmonitorname; }
            set { _eliminationmonitorname = value; }
        }
		private string _eliminationcontext;
        private DateTime _alarmtime;

        public DateTime alarmtime
        {
            get { return _alarmtime; }
            set { _alarmtime = value; }
        }
		private DateTime _eliminationtime;
		private int _xmno;
        private string _xmname;

        public string xmname
        {
            get { return _xmname; }
            set { _xmname = value; }
        }
		private string _jclx;
        private string _point_name;

        public string point_name
        {
            get { return _point_name; }
            set { _point_name = value; }
        }
		/// <summary>
		/// auto_increment
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EliminationMonitorID
		{
			set{ _eliminationmonitorid=value;}
			get{return _eliminationmonitorid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EliminationContext
		{
			set{ _eliminationcontext=value;}
			get{return _eliminationcontext;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime EliminationTime
		{
			set{ _eliminationtime=value;}
			get{return _eliminationtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jclx
		{
			set{ _jclx=value;}
			get{return _jclx;}
		}
		#endregion Model

	}
}

