﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmdatemailsendrecord.cs
*
* 功 能： N/A
* 类 名： alarmdatemailsendrecord
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/24 10:36:30   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace AuthorityAlarm.Model
{
	/// <summary>
	/// alarmdatemailsendrecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class alarmdatemailsendrecord
	{
		public alarmdatemailsendrecord()
		{}
		#region Model
		private string _did;
		private int _xmno;
		private DateTime _sdate;
		private DateTime _stime;
		private int _scont;
		/// <summary>
		/// 
		/// </summary>
		public string did
		{
			set{ _did=value;}
			get{return _did;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime sdate
		{
			set{ _sdate=value;}
			get{return _sdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stime
		{
			set{ _stime=value;}
			get{return _stime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int scont
		{
			set{ _scont=value;}
			get{return _scont;}
		}
		#endregion Model

	}
}

