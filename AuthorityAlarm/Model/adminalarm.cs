﻿/**  版本信息模板在安装目录下，可自行修改。
* adminalarm.cs
*
* 功 能： N/A
* 类 名： adminalarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/7/26 14:41:29   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace AuthorityAlarm.Model
{
	/// <summary>
	/// adminalarm:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class adminalarm
	{
		public adminalarm()
		{}
		#region Model
		private string _aid;
		private int _xmno;
		private DateTime _sendtime= DateTime.Now;
		private string _context;
		private string _adminno;
        private bool _confirm;
        private bool _mess;
        private bool _mail;
		private int _forwardtime;
		private int _datatype;
		/// <summary>
		/// 
		/// </summary>
		public string aid
		{
			set{ _aid=value;}
			get{return _aid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// on update CURRENT_TIMESTAMP
		/// </summary>
		public DateTime sendTime
		{
			set{ _sendtime=value;}
			get{return _sendtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string context
		{
			set{ _context=value;}
			get{return _context;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string adminno
		{
			set{ _adminno=value;}
			get{return _adminno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool confirm
		{
			set{ _confirm=value;}
			get{return _confirm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool mess
		{
			set{ _mess=value;}
			get{return _mess;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool mail
		{
			set{ _mail=value;}
			get{return _mail;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ForwardTime
		{
			set{ _forwardtime=value;}
			get{return _forwardtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dataType
		{
			set{ _datatype=value;}
			get{return _datatype;}
		}
		#endregion Model

	}
}

