﻿/**  版本信息模板在安装目录下，可自行修改。
* pointalarm.cs
*
* 功 能： N/A
* 类 名： pointalarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/9 10:55:22   N/A    初版
*
* Copyright (c) 2012 AuthorityAlarm Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace AuthorityAlarm.Model
{
	/// <summary>
	/// pointalarm:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class pointalarm
	{
		public pointalarm()
		{}
		#region Model
		private string _pid;
		private string _point_name;
		private DateTime _time;
		private string _type;
		private int _alarm;
		private DateTime? _atime;
		private int _xmno;
		private bool _readed;
		/// <summary>
		/// 
		/// </summary>
		public string pid
		{
			set{ _pid=value;}
			get{return _pid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int alarm
		{
			set{ _alarm=value;}
			get{return _alarm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? atime
		{
			set{ _atime=value;}
			get{return _atime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool readed
		{
			set{ _readed=value;}
			get{return _readed;}
		}
		#endregion Model

	}
}

