﻿/**  版本信息模板在安装目录下，可自行修改。
* cgalarmsplitondate.cs
*
* 功 能： N/A
* 类 名： cgalarmsplitondate
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/8/24 9:28:56   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace AuthorityAlarm.Model
{
	/// <summary>
	/// cgalarmsplitondate:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cgalarmsplitondate
	{
		public cgalarmsplitondate()
		{}
		#region Model
		private string _dno;
		private string _jclx;
		private string _pointname;
		private int _xmno;
		private DateTime _time;
        private DateTime _itime;
        private int _alarm;
        private bool _readed;


		private string _alarmcontext;
		private DateTime _adate;
        private int _cont;
        public int alarm
        {
            get { return _alarm; }
            set { _alarm = value; }
        }
        public int cont
        {
            get { return _cont; }
            set { _cont = value; }
        }

        public bool readed
        {
            get { return _readed; }
            set { _readed = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string dno
		{
			set{ _dno=value;}
			get{return _dno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jclx
		{
			set{ _jclx=value;}
			get{return _jclx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pointName
		{
			set{ _pointname=value;}
			get{return _pointname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}

        public DateTime itime
        {
            get { return _itime; }
            set { _itime = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string alarmContext
		{
			set{ _alarmcontext=value;}
			get{return _alarmcontext;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime adate
		{
			set{ _adate=value;}
			get{return _adate;}
		}
		#endregion Model

	}
}

