﻿/**  版本信息模板在安装目录下，可自行修改。
* senor_data.cs
*
* 功 能： N/A
* 类 名： senor_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:15   N/A    初版
*
* Copyright (c) 2012 InclimeterCgDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace InclimeterCgDAL.DAL
{
	/// <summary>
	/// 数据访问类:senor_data
	/// </summary>
	public partial class senor_data
	{
        public static database db = new database();
		public senor_data()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(decimal id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from senor_data");
			strSql.Append(" where id=@id ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,8)			};
			parameters[0].Value = id;

            return false;//OdbcSQLHelper.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(InclimeterDAL.Model.senor_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into senor_data(");
			strSql.Append("id,point_name,region,this_disp,ac_disp,this_rap,time,deep,xmno)");
			strSql.Append(" values (");
			strSql.Append("@id,@point_name,@region,@this_disp,@ac_disp,@this_rap,@time,@deep,@xmno)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,8),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@region", OdbcType.VarChar,100),
					new OdbcParameter("@this_disp", OdbcType.Double),
					new OdbcParameter("@ac_disp", OdbcType.Double),
					new OdbcParameter("@this_rap", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime),
                    new OdbcParameter("@deep", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.region;
			parameters[3].Value = model.this_disp;
			parameters[4].Value = model.ac_disp;
			parameters[5].Value = model.this_rap;
			parameters[6].Value = model.time;
			parameters[7].Value = model.xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);  //OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(InclimeterDAL.Model.senor_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update senor_data set ");
			strSql.Append("point_name=@point_name,");
			strSql.Append("region=@region,");
			strSql.Append("this_disp=@this_disp,");
			strSql.Append("ac_disp=@ac_disp,");
			strSql.Append("this_rap=@this_rap,");
			strSql.Append("time=@time,");
			strSql.Append("xmno=@xmno");
			strSql.Append(" where id=@id ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@region", OdbcType.VarChar,100),
					new OdbcParameter("@this_disp", OdbcType.Double),
					new OdbcParameter("@ac_disp", OdbcType.Double),
					new OdbcParameter("@this_rap", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@id", OdbcType.Int,8)};
			parameters[0].Value = model.point_name;
			parameters[1].Value = model.region;
			parameters[2].Value = model.this_disp;
			parameters[3].Value = model.ac_disp;
			parameters[4].Value = model.this_rap;
			parameters[5].Value = model.time;
			parameters[6].Value = model.xmno;
			parameters[7].Value = model.id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(decimal id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from senor_data ");
			strSql.Append(" where id=@id ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,8)			};
			parameters[0].Value = id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from senor_data ");
			strSql.Append(" where id in ("+idlist + ")  ");
            int rows = 0;//OdbcSQLHelper.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.senor_data GetModel(decimal id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,point_name,region,this_disp,ac_disp,this_rap,time,xmno from senor_data ");
			strSql.Append(" where id=@id ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,8)			};
			parameters[0].Value = id;

			InclimeterDAL.Model.senor_data model=new InclimeterDAL.Model.senor_data();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters); //OdbcSQLHelper.Query(strSql.ToString(), parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.senor_data DataRowToModel(DataRow row)
		{
			InclimeterDAL.Model.senor_data model=new InclimeterDAL.Model.senor_data();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=decimal.Parse(row["id"].ToString());
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["region"]!=null)
				{
					model.region=row["region"].ToString();
				}
					//model.this_disp=row["this_disp"].ToString();
					//model.ac_disp=row["ac_disp"].ToString();
					//model.this_rap=row["this_rap"].ToString();
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,point_name,region,this_disp,ac_disp,this_rap,time,xmno ");
			strSql.Append(" FROM senor_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        public bool Senor_dataTableLoad(int xmno, string pointnamestr, DateTime st, DateTime ed, out DataTable dt)
        {
            dt = null;
            pointnamestr = pointnamestr.Replace(",", "'");

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select id,point_name,region,this_disp,ac_disp,this_rap,time,xmno ");
            strSql.Append(" FROM senor_data where xmno = @xmno and point_name in (@pointnamestr) and time between ('@st','@ed') order by point_name,time asc  ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@pointnamestr", OdbcType.Int,100),
                    new OdbcParameter("@st", OdbcType.DateTime),
                    new OdbcParameter("@xmno", OdbcType.DateTime)

                                         };

            parameters[0].Value = xmno;
            parameters[1].Value = string.Format("'{0}'", pointnamestr);
            parameters[2].Value = st;
            parameters[3].Value = ed;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            dt = ds.Tables[0];
            return true;
        }

        public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from senor_data  where   xmno=@xmno   and point_name = @point_name  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }

        public bool GetModel(int xmno, string pointname, DateTime dt, out InclimeterDAL.Model.senor_data model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderCgConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,point_name,region,this_disp,ac_disp,this_rap,time,xmno ");
            strSql.Append(" FROM senor_data where xmno = @xmno  and  point_name =@point_name and time = @time ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }
        public bool MaxTime(int xmno, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from senor_data  where   xmno = @xmno");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int)
            };
            paramters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool ResultDataReportPrint(string sql, string xmname, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            dt = querysql.querystanderdb(sql, conn);
            return true;
        }
        public bool SenordataTableLoad(DateTime starttime, DateTime endtime, int startPageIndex, int pageSize, int xmno, string pointname, string sord, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderCgConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select point_name,this_disp,previous_disp,ac_disp,this_rap,deep,previous_time,time,mtimes  from senor_data where      xmno=@xmno   and    {0}    and     time    between    @starttime      and    @endtime     order by {1}  asc ,deep desc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@starttime", OdbcType.DateTime),
					new OdbcParameter("@endtime", OdbcType.DateTime),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool SenorTableRowsCount(DateTime starttime, DateTime endtime, int xmno, string pointname, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from senor_data where xmno='{2}' and {3}  and time between '{0}' and '{1}'", starttime, endtime, xmno, searchstr);
            OdbcConnection conn = db.GetStanderCgConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }
        public bool PointNameCycListGet(int xmno, string pointname, out List<string> ls)
        {

            OdbcConnection conn = db.GetStanderCgConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(mtimes) from senor_data    where         xmno=@xmno     and      {0}   order by mtimes    asc  ", pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int)
							};
            parameters[0].Value = xmno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                i++;
            }
            return true;

        }


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

