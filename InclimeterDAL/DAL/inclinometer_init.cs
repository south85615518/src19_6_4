﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_init.cs
*
* 功 能： N/A
* 类 名： inclinometer_init
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:14   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;

namespace InclimeterDAL.DAL
{
	/// <summary>
	/// 数据访问类:inclinometer_init
	/// </summary>
	public partial class inclinometer_init
	{
		public inclinometer_init()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(InclimeterDAL.Model.inclinometer_init model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into inclinometer_init(");
			strSql.Append("idx,region,point_name,basevalue,xmno)");
			strSql.Append(" values (");
			strSql.Append("@idx,@region,@point_name,@basevalue,@xmno)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@idx", OdbcType.Int,4),
					new OdbcParameter("@region", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@basevalue", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.idx;
			parameters[1].Value = model.region;
			parameters[2].Value = model.point_name;
			parameters[3].Value = model.basevalue;
			parameters[4].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(InclimeterDAL.Model.inclinometer_init model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update inclinometer_init set ");
			strSql.Append("idx=@idx,");
			strSql.Append("region=@region,");
			strSql.Append("point_name=@point_name,");
			strSql.Append("basevalue=@basevalue,");
			strSql.Append("xmno=@xmno");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@idx", OdbcType.Int,4),
					new OdbcParameter("@region", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@basevalue", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.idx;
			parameters[1].Value = model.region;
			parameters[2].Value = model.point_name;
			parameters[3].Value = model.basevalue;
			parameters[4].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from inclinometer_init ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.inclinometer_init GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select idx,region,point_name,basevalue,xmno from inclinometer_init ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			InclimeterDAL.Model.inclinometer_init model=new InclimeterDAL.Model.inclinometer_init();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(), parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.inclinometer_init DataRowToModel(DataRow row)
		{
			InclimeterDAL.Model.inclinometer_init model=new InclimeterDAL.Model.inclinometer_init();
			if (row != null)
			{
				if(row["idx"]!=null && row["idx"].ToString()!="")
				{
					model.idx= Int32.Parse(row["idx"].ToString());
				}
				if(row["region"]!=null && row["region"].ToString()!="")
				{
					model.region=int.Parse(row["region"].ToString());
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
					//model.basevalue=row["basevalue"].ToString();
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select idx,region,point_name,basevalue,xmno ");
			strSql.Append(" FROM inclinometer_init ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.name desc");
			}
			strSql.Append(")AS Row, T.*  from inclinometer_init T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "inclinometer_init";
			parameters[1].Value = "name";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

