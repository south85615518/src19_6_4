﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_param.cs
*
* 功 能： N/A
* 类 名： inclinometer_param
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:14   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;

namespace InclimeterDAL.DAL
{
	/// <summary>
	/// 数据访问类:inclinometer_param
	/// </summary>
	public partial class inclinometer_param
	{
		public inclinometer_param()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(InclimeterDAL.Model.inclinometer_param model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into inclinometer_param(");
			strSql.Append("pid,pno,mkh,name,productionno,a,b,c,d,xmno)");
			strSql.Append(" values (");
			strSql.Append("@pid,@pno,@mkh,@name,@productionno,@a,@b,@c,@d,@xmno)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@pid", OdbcType.Int,11),
					new OdbcParameter("@pno", OdbcType.VarChar,100),
					new OdbcParameter("@mkh", OdbcType.VarChar,10),
					new OdbcParameter("@name", OdbcType.VarChar,10),
					new OdbcParameter("@productionno", OdbcType.VarChar,100),
					new OdbcParameter("@a", OdbcType.Double),
					new OdbcParameter("@b", OdbcType.Double),
					new OdbcParameter("@c", OdbcType.Double),
					new OdbcParameter("@d", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.pid;
			parameters[1].Value = model.pno;
			parameters[2].Value = model.mkh;
			parameters[3].Value = model.name;
			parameters[4].Value = model.productionno;
			parameters[5].Value = model.a;
			parameters[6].Value = model.b;
			parameters[7].Value = model.c;
			parameters[8].Value = model.d;
			parameters[9].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(InclimeterDAL.Model.inclinometer_param model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update inclinometer_param set ");
			strSql.Append("pid=@pid,");
			strSql.Append("pno=@pno,");
			strSql.Append("mkh=@mkh,");
			strSql.Append("name=@name,");
			strSql.Append("productionno=@productionno,");
			strSql.Append("a=@a,");
			strSql.Append("b=@b,");
			strSql.Append("c=@c,");
			strSql.Append("d=@d,");
			strSql.Append("xmno=@xmno");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@pid", OdbcType.Int,11),
					new OdbcParameter("@pno", OdbcType.VarChar,100),
					new OdbcParameter("@mkh", OdbcType.VarChar,10),
					new OdbcParameter("@name", OdbcType.VarChar,10),
					new OdbcParameter("@productionno", OdbcType.VarChar,100),
					new OdbcParameter("@a", OdbcType.Double),
					new OdbcParameter("@b", OdbcType.Double),
					new OdbcParameter("@c", OdbcType.Double),
					new OdbcParameter("@d", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.pid;
			parameters[1].Value = model.pno;
			parameters[2].Value = model.mkh;
			parameters[3].Value = model.name;
			parameters[4].Value = model.productionno;
			parameters[5].Value = model.a;
			parameters[6].Value = model.b;
			parameters[7].Value = model.c;
			parameters[8].Value = model.d;
			parameters[9].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from inclinometer_param ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.inclinometer_param GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pid,pno,mkh,name,productionno,a,b,c,d,xmno from inclinometer_param ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			InclimeterDAL.Model.inclinometer_param model=new InclimeterDAL.Model.inclinometer_param();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(), parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.inclinometer_param DataRowToModel(DataRow row)
		{
			InclimeterDAL.Model.inclinometer_param model=new InclimeterDAL.Model.inclinometer_param();
			if (row != null)
			{
				if(row["pid"]!=null && row["pid"].ToString()!="")
				{
					model.pid=int.Parse(row["pid"].ToString());
				}
				if(row["pno"]!=null)
				{
					model.pno=row["pno"].ToString();
				}
				if(row["mkh"]!=null)
				{
					model.mkh=row["mkh"].ToString();
				}
				if(row["name"]!=null)
				{
					model.name=row["name"].ToString();
				}
				if(row["productionno"]!=null)
				{
					model.productionno=row["productionno"].ToString();
				}
					//model.a=row["a"].ToString();
					//model.b=row["b"].ToString();
					//model.c=row["c"].ToString();
					//model.d=row["d"].ToString();
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pid,pno,mkh,name,productionno,a,b,c,d,xmno ");
			strSql.Append(" FROM inclinometer_param ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

	
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.name desc");
			}
			strSql.Append(")AS Row, T.*  from inclinometer_param T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "inclinometer_param";
			parameters[1].Value = "name";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

