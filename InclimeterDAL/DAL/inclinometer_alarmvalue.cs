﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_alarmvalue.cs
*
* 功 能： N/A
* 类 名： inclinometer_alarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:14   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace InclimeterDAL.DAL
{
	/// <summary>
	/// 数据访问类:inclinometer_alarmvalue
	/// </summary>
	public partial class inclinometer_alarmvalue
	{
        public static database db = new database();
		public inclinometer_alarmvalue()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(decimal sid,string name)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from inclinometer_alarmvalue");
			strSql.Append(" where sid=@sid and name=@name ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@sid", OdbcType.Int,4),
					new OdbcParameter("@name", OdbcType.VarChar,100)			};
			parameters[0].Value = sid;
			parameters[1].Value = name;

            return false;//OdbcSQLHelper.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(InclimeterDAL.Model.inclinometer_alarmvalue model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmname);
            strSql.Append("insert ignore into inclinometer_alarmvalue(");
			strSql.Append("sid,name,this_disp,ac_disp,this_rap)");
			strSql.Append(" values (");
			strSql.Append("@sid,@name,@this_disp,@ac_disp,@this_rap)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@sid", OdbcType.Int,4),
					new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@this_disp", OdbcType.Double),
					new OdbcParameter("@ac_disp", OdbcType.Double),
					new OdbcParameter("@this_rap", OdbcType.Double)};
			parameters[0].Value = model.sid;
			parameters[1].Value = model.name;
			parameters[2].Value = model.this_disp;
			parameters[3].Value = model.ac_disp;
			parameters[4].Value = model.this_rap;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(InclimeterDAL.Model.inclinometer_alarmvalue model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmname);
			strSql.Append("update inclinometer_alarmvalue set ");
			strSql.Append("this_disp=@this_disp,");
			strSql.Append("ac_disp=@ac_disp,");
			strSql.Append("this_rap=@this_rap  ");
			strSql.Append("    where    name=@name  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@this_disp", OdbcType.Double),
					new OdbcParameter("@ac_disp", OdbcType.Double),
					new OdbcParameter("@this_rap", OdbcType.Double),
					new OdbcParameter("@name", OdbcType.VarChar,100)};
			parameters[0].Value = model.this_disp;
			parameters[1].Value = model.ac_disp;
			parameters[2].Value = model.this_rap;
			parameters[3].Value = model.name;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select sid,name ,this_disp,ac_disp,this_rap from inclinometer_alarmvalue {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TableRowsCount(string xmname, out string totalCont)
        {
            string sql = string.Format("select count(*) from inclinometer_alarmvalue");
            OdbcConnection conn = db.GetStanderConn(xmname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }

        /// <summary>
        /// 获取所有的预警参数
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool AlarmValueSelect(string xmname, out DataTable dt)
        {

            string sql = "select * from inclinometer_alarmvalue ";
            OdbcConnection conn = db.GetStanderConn(xmname);

            dt = querysql.querystanderdb(sql, conn);
            return dt != null ? true : false;

        }
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(InclimeterDAL.Model.inclinometer_alarmvalue alarm, string xmname)
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update fmos_pointalarmvalue set firstAlarmName='' where taskName=@taskName and firstAlarmName=@firstAlarmName");
            //二级预警
            ls.Add("update fmos_pointalarmvalue set secondAlarmName='' where taskName=@taskName and secondAlarmName=@secondAlarmName");
            //三级预警
            ls.Add("update fmos_pointalarmvalue set thirdAlarmName='' where taskName=@taskName and thirdAlarmName=@thirdAlarmName");
            List<string> cascSql = ls;
            OdbcConnection conn = db.GetStanderConn(xmname);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@taskName", "'" + xmname + "'");
                temp = temp.Replace("@firstAlarmName", "'" + alarm.name + "'");
                temp = temp.Replace("@secondAlarmName", "'" + alarm.name + "'");
                temp = temp.Replace("@thirdAlarmName", "'" + alarm.name + "'");

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(temp, conn);


            }
            return true;
        }
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string xmname,string name)
		{
			
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
			strSql.Append("delete from inclinometer_alarmvalue ");
			strSql.Append(" where  name=@name ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@name", OdbcType.VarChar,100)			};
			parameters[0].Value = name;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(string xmname, out string alarmValueNameStr)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = "select distinct(name) from inclinometer_alarmvalue";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            alarmValueNameStr = string.Join(";", lsFormat);
            return true;
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool  GetModel(string name,string xmname,out InclimeterDAL.Model.inclinometer_alarmvalue model)
		{
            model = new Model.inclinometer_alarmvalue();
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
			strSql.Append("select sid,name,this_disp,ac_disp,this_rap from inclinometer_alarmvalue ");
			strSql.Append(" where name=@name ");
			OdbcParameter[] parameters = {
					
					new OdbcParameter("@name", OdbcType.VarChar,100)			};
			parameters[0].Value = name;

			//InclimeterDAL.Model.inclinometer_alarmvalue model=new InclimeterDAL.Model.inclinometer_alarmvalue();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(), parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.inclinometer_alarmvalue DataRowToModel(DataRow row)
		{
			InclimeterDAL.Model.inclinometer_alarmvalue model=new InclimeterDAL.Model.inclinometer_alarmvalue();
			if (row != null)
			{
				if(row["sid"]!=null && row["sid"].ToString()!="")
				{
					model.sid=Int32.Parse(row["sid"].ToString());
				}
                if (row["name"] != null && row["name"].ToString()!="")
				{
					model.name=row["name"].ToString();
				}
                if (row["this_disp"] != null && row["this_disp"].ToString() != "")
                {
                    model.this_disp = Convert.ToDouble(row["this_disp"].ToString());
                }
                if (row["ac_disp"] != null && row["ac_disp"].ToString() != "")
                {
                    model.ac_disp = Convert.ToDouble(row["ac_disp"].ToString());
                }
                if (row["this_rap"] != null && row["this_rap"].ToString() != "")
                {
                    model.this_rap = Convert.ToDouble(row["this_rap"].ToString());
                }
                //model.this_disp = row["this_disp"].ToString();
                //model.ac_disp = row["ac_disp"].ToString();
                //model.this_rap = row["this_rap"].ToString();
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select sid,name,this_disp,ac_disp,this_rap ");
			strSql.Append(" FROM inclinometer_alarmvalue ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.name desc");
			}
			strSql.Append(")AS Row, T.*  from inclinometer_alarmvalue T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

