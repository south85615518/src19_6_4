﻿/**  版本信息模板在安装目录下，可自行修改。
* senor_data.cs
*
* 功 能： N/A
* 类 名： senor_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:15   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace InclimeterDAL.Model
{
	/// <summary>
	/// senor_data:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class senor_data
	{
		public senor_data()
		{}
		#region Model
		private decimal _id;
		private string _point_name;
		private string _region;
		private double _this_disp;
		private double _ac_disp;
		private double _this_rap;
		private DateTime _time;
		private int _xmno;
        private double _deep;
        private DateTime _previous_time;
        private double _previous_disp;
        private int _mtimes;

        public int mtimes
        {
            get { return _mtimes; }
            set { _mtimes = value; }
        }
        public double previous_disp
        {
            get { return _previous_disp; }
            set { _previous_disp = value; }
        }

        public DateTime previous_time
        {
            get { return _previous_time; }
            set { _previous_time = value; }
        }

        public double deep
        {
            get { return _deep; }
            set { _deep = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public decimal id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string region
		{
			set{ _region=value;}
			get{return _region;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_disp
		{
			set{ _this_disp=value;}
			get{return _this_disp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_disp
		{
			set{ _ac_disp=value;}
			get{return _ac_disp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_rap
		{
			set{ _this_rap=value;}
			get{return _this_rap;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

