﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_param.cs
*
* 功 能： N/A
* 类 名： inclinometer_param
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:14   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace InclimeterDAL.Model
{
	/// <summary>
	/// inclinometer_param:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class inclinometer_param
	{
		public inclinometer_param()
		{}
		#region Model
		private int _pid;
		private string _pno;
		private string _mkh;
		private string _name;
		private string _productionno;
		private double _a;
		private double _b;
		private double _c;
		private double _d;
		private int _xmno;
		/// <summary>
		/// 
		/// </summary>
		public int pid
		{
			set{ _pid=value;}
			get{return _pid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pno
		{
			set{ _pno=value;}
			get{return _pno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mkh
		{
			set{ _mkh=value;}
			get{return _mkh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string productionno
		{
			set{ _productionno=value;}
			get{return _productionno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double a
		{
			set{ _a=value;}
			get{return _a;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double b
		{
			set{ _b=value;}
			get{return _b;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double c
		{
			set{ _c=value;}
			get{return _c;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double d
		{
			set{ _d=value;}
			get{return _d;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

