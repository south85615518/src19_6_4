﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_alarmvalue.cs
*
* 功 能： N/A
* 类 名： inclinometer_alarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:14   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace InclimeterDAL.Model
{
	/// <summary>
	/// inclinometer_alarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class inclinometer_alarmvalue
	{
		public inclinometer_alarmvalue()
		{}
		#region Model
		private decimal _sid;
		private string _name;
		private double _this_disp;
		private double _ac_disp;
		private double _this_rap;
        private string _xmname;

        public string xmname
        {
            get { return _xmname; }
            set { _xmname = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public decimal sid
		{
			set{ _sid=value;}
			get{return _sid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_disp
		{
			set{ _this_disp=value;}
			get{return _this_disp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_disp
		{
			set{ _ac_disp=value;}
			get{return _ac_disp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_rap
		{
			set{ _this_rap=value;}
			get{return _this_rap;}
		}
		#endregion Model

	}
}

