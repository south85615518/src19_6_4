﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_init.cs
*
* 功 能： N/A
* 类 名： inclinometer_init
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:14   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace InclimeterDAL.Model
{
	/// <summary>
	/// inclinometer_init:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class inclinometer_init
	{
		public inclinometer_init()
		{}
		#region Model
		private int _idx;
		private int _region;
		private string _point_name;
		private double _basevalue;
		private int _xmno;
		/// <summary>
		/// 
		/// </summary>
		public int idx
		{
			set{ _idx=value;}
			get{return _idx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int region
		{
			set{ _region=value;}
			get{return _region;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double basevalue
		{
			set{ _basevalue=value;}
			get{return _basevalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

