﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_alarmvalue.cs
*
* 功 能： N/A
* 类 名： inclinometer_alarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:14   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace InclimeterDAL.BLL
{
	/// <summary>
	/// inclinometer_alarmvalue
	/// </summary>
	public partial class inclinometer_alarmvalue
	{
		private readonly InclimeterDAL.DAL.inclinometer_alarmvalue dal=new InclimeterDAL.DAL.inclinometer_alarmvalue();
		public inclinometer_alarmvalue()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(decimal sid,string name)
		{
			return dal.Exists(sid,name);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(InclimeterDAL.Model.inclinometer_alarmvalue model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目{0}测斜的预警参数名{1}成功", model.xmname, model.name);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目{0}测斜的预警参数名{1}失败", model.xmname, model.name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目{0}测斜的预警参数名{1}出错，错误信息："+ex.Message, model.xmname, model.name);
                return false;
            }




			//return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(InclimeterDAL.Model.inclinometer_alarmvalue model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新测斜的预警参数名{1}成功", model.xmname, model.name);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新测斜的预警参数名{1}失败", model.xmname, model.name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新测斜的预警参数名{1}出错，错误信息：" + ex.Message, model.xmname, model.name);
                return false;
            }
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string xmname,string name,out string mssg)
		{

            try
            {
                if (dal.Delete(xmname,name))
                {
                    mssg = string.Format("删除测斜预警名称{0}成功", name);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除测斜预警名称{0}失败", name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除测斜预警名称{0}出错，错误信息："+ex.Message, name);
                return false;
            }
			//return dal.Delete(sid,name);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool  GetModel(string name,string xmname,out InclimeterDAL.Model.inclinometer_alarmvalue model ,out string mssg)
		{
            model = null;
            try
            {
                if (dal.GetModel(name, xmname, out model))
                {
                    mssg = string.Format("获取项目{0}深部位移预警名称{1}实体成功", xmname, name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}深部位移预警名称{1}实体失败", xmname, name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}深部位移预警名称{1}实体出错，错误信息:"+ex.Message, xmname, name);
                return false;
            }

			//return dal.GetModel(sid,name);
		}

        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, string xmname, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmname, colName, sord, out  dt))
                {
                    mssg = string.Format("加载项目{0}的深部位移预警参数{1}条成功", xmname, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("加载项目{0}的深部位移预警参数失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目{0}的深部位移预警参数出错，错误信息:"+ex.Message, xmname);
                return false;
            }
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TableRowsCount(string xmname, out string totalCont,out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.TableRowsCount(xmname, out totalCont))
                {
                    mssg = string.Format("加载项目{0}深部位移预警参数表记录数{1}成功!",xmname,totalCont);
                    return true;

                }
                else
                {
                    mssg = string.Format("加载项目{0}深部位移预警参数表记录数失败!", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载项目{0}深部位移预警参数表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        ///// <summary>
        ///// 获取所有的预警参数
        ///// </summary>
        ///// <param name="xmname"></param>
        ///// <param name="dt"></param>
        ///// <returns></returns>
        //public bool AlarmValueSelect(string xmname, out DataTable dt)
        //{

          

        //}
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(InclimeterDAL.Model.inclinometer_alarmvalue alarm, string xmname,out string mssg)
        {
            try
            {
                if (dal.PointAlarmValueDelCasc(alarm, xmname))
                {
                    mssg = string.Format("项目{0}深部位移预警参数级联删除成功！", xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}深部位移预警参数级联删除失败！", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目{0}深部位移预警参数级联删除出错，出错信息" + ex.Message;
                return false;
            }
        }
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<InclimeterDAL.Model.inclinometer_alarmvalue> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<InclimeterDAL.Model.inclinometer_alarmvalue> DataTableToList(DataTable dt)
		{
			List<InclimeterDAL.Model.inclinometer_alarmvalue> modelList = new List<InclimeterDAL.Model.inclinometer_alarmvalue>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				InclimeterDAL.Model.inclinometer_alarmvalue model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}
        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(string xmname, out string alarmValueNameStr, out string mssg)
        {
            alarmValueNameStr = "";
            try
            {
                if (dal.AlarmValueNameGet(xmname, out alarmValueNameStr))
                {
                    mssg = string.Format("获取项目{0}的深部位移测斜狱警名称{1}成功", xmname, alarmValueNameStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的深部位移测斜狱警名称失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的深部位移测斜狱警名称出错，错误信息:"+ex.Message, xmname);
                return false;
            }
        }
		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

