﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_init.cs
*
* 功 能： N/A
* 类 名： inclinometer_init
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:14   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using InclimeterDAL.Model;
namespace InclimeterDAL.BLL
{
	/// <summary>
	/// inclinometer_init
	/// </summary>
	public partial class inclinometer_init
	{
		private readonly InclimeterDAL.DAL.inclinometer_init dal=new InclimeterDAL.DAL.inclinometer_init();
		public inclinometer_init()
		{}
		#region  BasicMethod

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(InclimeterDAL.Model.inclinometer_init model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(InclimeterDAL.Model.inclinometer_init model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete();
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.inclinometer_init GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.GetModel();
		}

	
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<InclimeterDAL.Model.inclinometer_init> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<InclimeterDAL.Model.inclinometer_init> DataTableToList(DataTable dt)
		{
			List<InclimeterDAL.Model.inclinometer_init> modelList = new List<InclimeterDAL.Model.inclinometer_init>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				InclimeterDAL.Model.inclinometer_init model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

