﻿/**  版本信息模板在安装目录下，可自行修改。
* senor_data.cs
*
* 功 能： N/A
* 类 名： senor_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/10/26 14:52:15   N/A    初版
*
* Copyright (c) 2012 InclimeterDAL Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using InclimeterDAL.Model;
namespace InclimeterDAL.BLL
{
	/// <summary>
	/// senor_data
	/// </summary>
	public partial class senor_data
	{
		private readonly InclimeterDAL.DAL.senor_data dal=new InclimeterDAL.DAL.senor_data();
		public senor_data()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(decimal id)
		{
			return dal.Exists(id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(InclimeterDAL.Model.senor_data model,out string  mssg)
		{

            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("向测量库中添加项目编号{0}区域{1}点名{2}采集时间{3}的数据成功", model.xmno, model.region, model.point_name, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("向测量库中添加项目编号{0}区域{1}点名{2}采集时间{3}的数据失败", model.xmno, model.region, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("向测量库中添加项目编号{0}区域{1}点名{2}采集时间{3}的数据出错，错误信息:"+ex.Message, model.xmno, model.region, model.point_name, model.time);
                return false;
            }

		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(InclimeterDAL.Model.senor_data model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(decimal id)
		{
			
			return dal.Delete(id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			return dal.DeleteList(idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public InclimeterDAL.Model.senor_data GetModel(decimal id)
		{
			
			return dal.GetModel(id);
		}

		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public bool GetList(int xmno,out DataTable dt,out string mssg )
		{
            dt = null;
            try
            {
                if (dal.GetList(xmno, out dt))
                {
                    mssg = string.Format("获取项目编号{0}的测斜点数据{1}条成功", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的测斜点数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {

                mssg = string.Format("获取项目编号{0}的测斜点数据出错,错误信息:"+ex.Message, xmno);
                return false;
            }


			
		}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(int xmno, out List<InclimeterDAL.Model.senor_data> li, out string mssg)
        {
            li = null;
            try
            {
                if (dal.GetList(xmno, out li))
                {
                    mssg = string.Format("获取项目编号{0}的临时表测斜点数据{1}条成功", xmno, li.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的临时表测斜点数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {

                mssg = string.Format("获取项目编号{0}的临时表测斜点数据出错,错误信息:" + ex.Message, xmno);
                return false;
            }



        }
        public bool ResultDataReportPrint(string sql, string xmname, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultDataReportPrint(sql, xmname, out dt))
                {
                    mssg = "深部位移结果数据报表数据表生成成功";
                    return true;
                }
                else
                {
                    mssg = "深部位移结果数据报表数据表生成失败";
                    return false;
                }
            }
            catch (Exception ex)
            {

                mssg = "深部位移结果数据报表数据表生成出错，错误信息" + ex.Message;
                return false;

            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteSenorTmp(int xmno, out string mssg)
        {
            try
            {
                if (dal.DeleteTmp(xmno))
                {
                    mssg = string.Format("删除项目编号{0}临时表的数据成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}临时表的数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}临时表的数据出错,错误信息：" + ex.Message, xmno);
                return false;
            }

        }

        public bool MaxTime(int xmno, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno, out maxTime))
                {
                    mssg = string.Format("获取项目{0}深部位移测量数据的最大日期{1}成功", xmno, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}深部位移测量数据的最大日期失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}深部位移测量数据的最大日期出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }



		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<InclimeterDAL.Model.senor_data> GetModelList(string strWhere)
		{
            DataSet ds = null;// dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<InclimeterDAL.Model.senor_data> DataTableToList(DataTable dt)
		{
			List<InclimeterDAL.Model.senor_data> modelList = new List<InclimeterDAL.Model.senor_data>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				InclimeterDAL.Model.senor_data model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}
        public bool Senor_dataTableLoad(int xmno, string pointnamestr, DateTime st, DateTime ed, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.Senor_dataTableLoad(xmno, pointnamestr, st, ed, out dt))
                {
                    mssg = string.Format("获取项目编号{0}{1}时间范围{2}{3}深部位移的测量数据数据{4}条成功", xmno, pointnamestr, st, ed, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}时间范围{2}{3}深部位移的测量数据失败", xmno, pointnamestr, st, ed);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}时间范围{2}{3}深部位移的测量数据出错，错误信息:"+ex.Message, xmno, pointnamestr, st, ed);
                return false;
            }

        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        /// 

        public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt, out string mssg)
        {
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(xmno, pointname, out dt))
                {
                    mssg = string.Format("获取项目编号{0}点{1}深部位移的最新采集时间成功", xmno, pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点{1}深部位移的最新采集时间失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点{1}深部位移的最新采集时间出错,错误信息:" + ex.Message, xmno, pointname);
                return false;
            }



        }

        public bool GetModel(int xmno, string pointname, DateTime dt, out InclimeterDAL.Model.senor_data model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, dt, out model))
                {
                    mssg = string.Format("获取项目编号{0}点{1}测量时间{2}的深部位移数据记录成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点{1}测量时间{2}的深部位移数据记录失败", xmno, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点{1}测量时间{2}的深部位移数据记录出错,错误信息:" + ex.Message, xmno, pointname, dt);
                return false;
            }

        }

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}


        public bool SenordataTableLoad(DateTime starttime, DateTime endtime, int startPageIndex, int pageSize, int xmno, string pointname, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SenordataTableLoad(starttime, endtime, startPageIndex, pageSize, xmno, pointname, sord, out dt))
                {
                    if (starttime != new DateTime() && endtime != new DateTime())
                        mssg = string.Format("获取项目编号{0}从{1}至{2}测斜数据{3}条成功", xmno, starttime, endtime, dt.Rows.Count);
                    else
                    {
                        mssg = string.Format("获取项目编号{0}监测次数从{1}至{2}测斜数据{3}条成功", xmno,  dt.Rows.Count);
                    }
                    return true;
                }
                else
                {
                    if (starttime != new DateTime() && endtime != new DateTime())
                        mssg = string.Format("获取项目编号{0}从{1}至{2}测斜数据失败", xmno, starttime, endtime);
                    else
                    {
                        mssg = string.Format("获取项目编号{0}监测次数从{1}至{2}测斜数据失败", xmno);
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (starttime != new DateTime() && endtime != new DateTime())
                    mssg = string.Format("获取项目编号{0}从{1}至{2}测斜数据出错，错误信息："+ex.Message, xmno, starttime, endtime);
                else
                {
                    mssg = string.Format("获取项目编号{0}监测次数从{1}至{2}测斜数据出错，错误信息："+ex.Message, xmno);
                }
                return false;
            }

        }
        public bool PointNameCycListGet(int xmno, string pointname, out List<string> ls,out string mssg)
        {
            ls = new List<string>();
            try
            {
                if (dal.PointNameCycListGet(xmno, pointname, out ls))
                {
                    mssg = string.Format("获取项目编号{0}点名{1}的周期列表数量{2}成功", xmno, pointname, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点名{1}的周期列表失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点名{1}的周期列表出错,错误信息:" + ex.Message, xmno, pointname);
                return false;
            }

        }

        public bool SenorTableRowsCount(DateTime starttime, DateTime endtime, int xmno, string pointname, out string totalCont,out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.SenorTableRowsCount(starttime, endtime, xmno, pointname, out  totalCont))
                {
                    mssg = string.Format("获取项目编号{0}从{1}至{2}之间的数据{3}条成功", xmno, starttime, endtime, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}从{1}至{2}之间的数据失败", xmno, starttime, endtime, totalCont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}从{1}至{2}之间的数据出错，错误信息:"+ex.Message, xmno, starttime, endtime);
                return false;
            }
        }



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

