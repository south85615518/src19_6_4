﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Tool;
using System.Web.UI;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.UserProcess;
using NFnet_BLL.ProjectInfo;

namespace Aspect
{
    /// <summary>
    /// 填充头条控件值
    /// </summary>
    public class PageHeader : System.Web.UI.Page
    {

        /// <summary>
        /// 头部信息获取
        /// </summary>
        /// <param name="model">参数对象</param>
        /// <param name="mssg">执行结果</param>
        /// <returns>显示字符串</returns>
        public static bool IdentyTxtFill(ProcessTitleSetModel model, out string mssg)
        {
            mssg = "";

            switch (model.role)
            {
                case Role.superAdministratrorModel:
                    Authority.Model.member member = model.obj as Authority.Model.member;
                    model.title = string.Format("{0}:{1}", NFnetUIHelper.RoleToCHN.ToCHNString(model.role), member.userName);
                    model.surveybase = member.surveybase;
                    break;
                case Role.administratrorModel:
                    member = model.obj as Authority.Model.member;
                    model.title = string.Format("{0}:{1}", NFnetUIHelper.RoleToCHN.ToCHNString(model.role), member.userName);
                    model.surveybase = member.surveybase;
                    break;
                case Role.monitorModel:
                    Authority.Model.UnitMember unitmember = model.obj as Authority.Model.UnitMember;
                    model.title = string.Format("{0}:{1}", NFnetUIHelper.RoleToCHN.ToCHNString(model.role), unitmember._call);
                    model.surveybase = unitmember.surveybase;
                    break;
                case Role.superviseModel:
                    Authority.Model.MonitorMember monitormember = model.obj as Authority.Model.MonitorMember;
                    model.title = monitormember.name;
                    model.surveybase = monitormember.surveybase;
                    break;

            }
            return true;



        }

        /// <summary>
        /// 头部身份显示类
        /// </summary>
        public class ProcessTitleSetModel
        {
            /// <summary>
            /// 显示结果字符串
            /// </summary>
            public string title { get; set; }
            /// <summary>
            /// 用户名
            /// </summary>
            public string userId { get; set; }
            /// <summary>
            /// 用户角色
            /// </summary>
            public Role role { get; set; }
            /// <summary>
            /// 当前用户信息对象
            /// </summary>
            public bool surveybase { get; set; }
            public object obj { get; set; }
            public ProcessTitleSetModel(string title, string userId, Role role, object obj)
            {
                this.title = title;
                this.userId = userId;
                this.role = role;
                this.obj = obj;
            }
        }



        public static string mssg;
        /// <summary>
        /// 获取用户所在的单位名称
        /// </summary>
        /// <param name="userId">用户名</param>
        /// <param name="role">用户身份</param>
        /// <param name="obj">用户信息对象</param>
        /// <returns>单位名称</returns>
        public static string CurrentUserUnitNameGet(string userId, string role, object obj)
        {
            try
            {
                switch (role)
                {
                    case "系统管理员":
                        Authority.Model.member member = obj as Authority.Model.member;
                        return member.unitName;
                    case "管理员":
                        member = obj as Authority.Model.member;
                        return member.unitName;
                    case "监测员":
                        Authority.Model.UnitMember unitmember = obj as Authority.Model.UnitMember;
                        return unitmember.unitno;
                    case "":
                        Authority.Model.MonitorMember monitormember = obj as Authority.Model.MonitorMember;
                        var processSuperviseXmUnitModel = new ProcessSuperviseBLL.ProcessSuperviseXmUnitModel(monitormember.userNO);
                        if (superviseBLL.ProcessSuperviseXmUnit(processSuperviseXmUnitModel, out mssg))
                        {
                            return processSuperviseXmUnitModel.unitName;
                        }
                        else
                            return "";
                    default: return "";

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return "";
            }
        }



        public static bool CurrentRoleSmsReduce(Role role, object obj, out string mssg)
        {
            mssg = "";
            try
            {
                switch (role)
                {
                    case Role.superAdministratrorModel:
                    case Role.administratrorModel:
                        var member = obj as Authority.Model.member;
                        return administratorBLL.ProcessAdministratorsmsBLL(member.userId, out mssg);
                    case Role.monitorModel:
                        Authority.Model.UnitMember unitmember = obj as Authority.Model.UnitMember;
                        return monitorBLL.ProcessMonitorsmsBLL(unitmember.memberno, out mssg);
                    case Role.superviseModel:
                        Authority.Model.MonitorMember monitormember = obj as Authority.Model.MonitorMember;
                        return superviseBLL.ProcessSupervisesmsBLL(monitormember.userNO, out mssg);
                    default: return false;

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return false;
            }
        }

        public static bool RoleSurveyBaseExchange(Role role, object obj, out object currentobj,out string mssg)
        {
            mssg = "";
            currentobj = null;
            try
            {
                switch (role)
                {
                    case Role.superAdministratrorModel:
                    case Role.administratrorModel:
                        var member = obj as Authority.Model.member;
                        var administratormodel = new ProcessAdministratrorBLL.ProcessAdministratorSurveyBaseExchangeModel(member.userId, member.surveybase);
                        if (administratorBLL.ProcessAdministratorSurveyBaseExchange(administratormodel, out mssg))
                        {
                            administratormodel.surveybase = !administratormodel.surveybase;
                            currentobj = member;
                        }
                        return true;
                    case Role.monitorModel:
                        Authority.Model.UnitMember unitmember = obj as Authority.Model.UnitMember;
                        var monitormodel = new ProcessMonitorBLL.ProcessMonitorSurveyBaseExchangeModel(unitmember.userName, unitmember.surveybase);
                        if (monitorBLL.ProcessMonitorSurveyBaseExchange(monitormodel, out mssg))
                        {
                            monitormodel.surveybase = !monitormodel.surveybase;
                            currentobj = monitormodel;
                        }
                        return true;
                    case Role.superviseModel:
                        Authority.Model.MonitorMember monitormember = obj as Authority.Model.MonitorMember;

                        return true;
                    default: return false;

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return false;
            }
        }

        public static bool  CurrentRoleTel(Role role, object obj, out string tel,out string mssg)
        {
            mssg = "";
            tel = "";
            try
            {
                switch (role)
                {
                    case Role.superAdministratrorModel:
                    case Role.administratrorModel:
                        var member = obj as Authority.Model.member;
                        tel = member.tel;
                        return true;
                    case Role.monitorModel:
                        Authority.Model.UnitMember unitmember = obj as Authority.Model.UnitMember;
                        tel = unitmember.tel;
                        return true;
                    case Role.superviseModel:
                        Authority.Model.MonitorMember monitormember = obj as Authority.Model.MonitorMember;
                        tel = monitormember.tel;
                        return true;
                    default: return false;

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return false;
            }
        }


        public static bool recordNumSet(ProcessRecordNumSetModel model)
        {

            model.totalRecordText = model.totalCont.ToString();
            //如果是最后一页
            model.currentRecordText = model.pageIndex >= model.pageCount - 1 ? model.pageSize * model.pageIndex + 1 + "-" + model.totalCont.ToString() : model.pageSize * model.pageIndex + 1 + "-" + (model.pageSize * (model.pageIndex + 1)).ToString();
            return true;
        }


        public class ProcessRecordNumSetModel
        {
            public int pageSize { get; set; }
            public int pageIndex { get; set; }
            public int pageCount { get; set; }
            public int totalCont { get; set; }
            public string totalRecordText { get; set; }
            public string currentRecordText { get; set; }
            public ProcessRecordNumSetModel(int pageSize, int pageIndex, int pageCount, int totalCont)
            {
                this.pageSize = pageSize;
                this.pageIndex = pageIndex;
                this.pageCount = pageCount;
                this.totalCont = totalCont;
            }
            public ProcessRecordNumSetModel()
            {

            }
        }
        public static bool ProcessGridviewPageIndexChange(ProcessGridviewPageIndexChangeModel model, out string mssg)
        {

            mssg = "";
            if (model.commandName == "Page")
            {
                //首页
                if (model.commandArgument.ToString() == "First")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    model.txtNewPageIndex = "1";
                    model.pageIndex = 0;

                }
                else if (model.commandArgument.ToString() == "Next")
                {


                    model.pageIndex = (Int32.Parse(model.txtNewPageIndex) + 1) > model.pageCount ? model.pageCount : Int32.Parse(model.txtNewPageIndex) + 1;
                    model.txtNewPageIndex = model.pageIndex.ToString();

                }
                else if (model.commandArgument.ToString() == "-2")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    model.pageIndex = Int32.Parse(model.txtNewPageIndex) > model.pageCount ? model.pageCount : Int32.Parse(model.txtNewPageIndex) < 0 ? 0 : Int32.Parse(model.txtNewPageIndex);
                    model.txtNewPageIndex = model.pageIndex.ToString();


                }
                else if (model.commandArgument.ToString() == "Last")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    model.pageIndex = model.pageCount;
                    model.txtNewPageIndex = model.pageCount.ToString();

                }
                else if (model.commandArgument.ToString() == "Prev")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    model.pageIndex = (Int32.Parse(model.txtNewPageIndex) - 1) < 0 ? 0 : Int32.Parse(model.txtNewPageIndex) - 1;
                    model.txtNewPageIndex = model.pageIndex.ToString();

                }

                mssg = "成功";
                return true;
            }
            return false;

        }

        public class ProcessGridviewPageIndexChangeModel : ProcessRecordNumSetModel
        {
            public string txtNewPageIndex { get; set; }
            public string commandName { get; set; }
            public string commandArgument { get; set; }
            public ProcessGridviewPageIndexChangeModel()
            {

            }
            public ProcessGridviewPageIndexChangeModel(string txtNewPageIndex, string commandName, string commandArgument, int pageSize, int pageIndex, int pageCount, int totalCont)
            {
                this.txtNewPageIndex = (Int32.Parse(txtNewPageIndex) - 1).ToString();
                this.commandName = commandName;
                this.commandArgument = commandArgument;
                this.pageSize = pageSize;
                this.pageIndex = pageIndex;
                this.pageCount = pageCount;
                this.totalCont = totalCont;
            }
        }
        public static bool ProcessRowDataBound(ProcessRowDataBoundModel model, out string mssg)
        {

            if (model.e.Row.RowType == DataControlRowType.DataRow)
            {
                model.e.Row.Attributes.Add("id", model._i.ToString());
                model.e.Row.Attributes.Add("onKeyDown", "SelectRow();");
                model.e.Row.Attributes.Add("onMouseOver", "MarkRow(" + model._i.ToString() + ");");
                model.e.Row.Attributes.Add("onMouseLeave", "Markdel(" + model._i.ToString() + ");");
                if (model._i % 2 == 0)
                    model.e.Row.Style.Add("background-color", "#86e791");
                model._i++;
            }
            mssg = "成功";
            return true;

        }

        public class ProcessRowDataBoundModel
        {
            public GridViewRowEventArgs e { get; set; }
            public int _i { get; set; }
            public ProcessRowDataBoundModel(GridViewRowEventArgs e, int _i)
            {
                this.e = e;
                this._i = _i;
            }

        }
        public static ProcessAdministratrorBLL administratorBLL = new ProcessAdministratrorBLL();
        public static ProcessMonitorBLL monitorBLL = new ProcessMonitorBLL();
        public static ProcessSuperviseBLL superviseBLL = new ProcessSuperviseBLL();
        public static bool ProcessGetAuthoryXmList(ProcessGetAuthoryXmListModel md, out string mssg)
        {
            mssg = "";
            md.ls = new List<string>();
            var model = new ProcessPeopleXmnameListCondition(md.userId);
            switch (md.role)
            {

                case Role.administratrorModel:


                    if (!administratorBLL.ProcessAdministratorXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        md.ls = model.xmnameList;
                    }
                    break;
                case Role.monitorModel:
                    if (!monitorBLL.ProcessMonitorXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        md.ls = model.xmnameList;
                    }
                    break;
                case Role.superviseModel:
                    if (!superviseBLL.ProcessSuperviseXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        md.ls = model.xmnameList;
                    }
                    break;
            }
            return true;
        }

        public class ProcessGetAuthoryXmListModel
        {
            public string userId { get; set; }
            public Role role { get; set; }
            public List<string> ls { get; set; }
            public ProcessGetAuthoryXmListModel(string userId, Role role)
            {
                this.userId = userId;
                this.role = role;
            }
        }

        public static bool ProcessXmnameUpdate(string updatexmname,int xmno,out string mssg)
        {
            ProcessXmBLL processXmBLL = new ProcessXmBLL();
            var processXmnameUpdateModel = new ProcessXmBLL.ProcessXmnameUpdateModel(updatexmname,xmno);
            return processXmBLL.ProcessXmnameUpdate(processXmnameUpdateModel, out mssg);
        }


    }
}