﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnetUIHelper
{
    public static class RoleToCHN
    {
        public static string ToCHNString(this NFnet_BLL.LoginProcess.Role value)
        {
            switch (value)
            {
                case NFnet_BLL.LoginProcess.Role.superAdministratrorModel: return "系统管理员";
                case NFnet_BLL.LoginProcess.Role.administratrorModel: return "管理员";
                case NFnet_BLL.LoginProcess.Role.monitorModel: return "监测员";
                case NFnet_BLL.LoginProcess.Role.superviseModel: return "";
                default: return "";
            }
        }
        
    }
}
