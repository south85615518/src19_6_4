﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnetUIHelper
{
    public class AuthorySameName
    {
        public static Authority.BLL.UnitMember monitorbll = new Authority.BLL.UnitMember();
        public static Authority.BLL.MonitorMember supervisebll = new Authority.BLL.MonitorMember();
        public static bool MonitorName(ProcessSameNameCondition model,out string mssg)
        {
            return monitorbll.Exist(model.authorName,model.uerid,out mssg);
        }
        public static bool SuperviseName(ProcessSameNameCondition model, out string mssg)
        {
            return supervisebll.Exist(model.authorName,model.uerid, out mssg);
        }
        public class ProcessSameNameCondition
        {
            public string authorName { get; set; }
            public string uerid  { get; set; }
            public ProcessSameNameCondition(string authorName,object currentObj )
            {
                this.authorName = authorName;
                var unitMembermodel = currentObj as Authority.Model.UnitMember;
                var Membermodel = currentObj as Authority.Model.member;
                
                if (unitMembermodel == null)
                {
                    Membermodel = currentObj as Authority.Model.member;
                    this.uerid = Membermodel.userId;
                }
                else this.uerid = unitMembermodel.userName;
            }
        }
        
    }
}
