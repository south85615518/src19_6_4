﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
namespace Aspect
{
    /// <summary>
    /// 异常文档
    /// </summary>
    public class ExceptionLog
    {
        public static string defaultPath = "D:异常日志";

        public static void ExceptionWrite(Exception ex)
        {
            FileStream fs = null;
            try
            {
                
                string tx = string.Format("{0}\n{1}\n{2}\n", DateTime.Now.ToString(),ex.Message,ex.StackTrace.ToString());
                string realPath = defaultPath;
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ExceptionWrite(string sql)
        {
            string tx = string.Format("{0}\n{1}\n", DateTime.Now.ToString(),sql);
            string realPath = defaultPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
    }
}