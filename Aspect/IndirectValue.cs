﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.ProjectInfo;
using NFnet_Interface.UserProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using Authority.Model;
using NFnet_BLL.UserProcess;

namespace Aspect
{
    public class IndirectValue
    {
        public static Authority.BLL.xmconnect bll = new Authority.BLL.xmconnect();
        public static ProcessDTUPortXm processDTUPortXm = new ProcessDTUPortXm();
        public static ProcessDTUModuleBLL processDTUModuleBLL = new ProcessDTUModuleBLL();
        public static ProcessDTUPointModule processDTUPointModule = new ProcessDTUPointModule();
        public static string mssg = "";
        public static int GetXmnoFromXmname(string xmname)
        {
            string mssg = "";
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();
            if (bll.GetModel(xmname, out model, out mssg))
            {
                return model.xmno;
            }
            else
            {
                mssg = "不存在该项目名称的项目编号";
                return -1;
            }
        }
        public static ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public static Authority.Model.xmconnect GetXmconnectFromXmname(string xmname)
        {

            string mssg = "";
            var processXmInfoLoadModel = new ProcessXmBLL.ProcessXmInfoLoadModel(xmname);
            List<Authority.Model.xmconnect> lmx = new List<Authority.Model.xmconnect>();
            if (processXmBLL.ProcessXmInfoLoad(processXmInfoLoadModel, out mssg))
            {
                return processXmInfoLoadModel.model;
            }
            return new Authority.Model.xmconnect();
        }
        public static int GetDTUXmnoFromPort(int port)
        {
            return processDTUPortXm.DTUPortXm(port,out mssg).xmno;
        }
        public static bool  DTUModulePointName(int xmno,int port,string od,string addressno,out string pointname,out double line,out double holedepth)
        {
            DTU.Model.dtu model =  processDTUPointModule.DTUPointModule(xmno, port, od, addressno, out mssg);
            //ProcessPrintMssg.Print(mssg);
            pointname = model.point_name;
            line = model.line;
            holedepth = model.holedepth;
            return true;
            //return model.point_name;
        }
        public static bool DTUModulePointName(int xmno, int port, string addressno, out string pointname)
        {
            ProcessDTUPointModuleWhithoutOd processDTUPointModuleWhithoutOd = new ProcessDTUPointModuleWhithoutOd();
            DTU.Model.dtu model = processDTUPointModuleWhithoutOd.DTUPointModule(xmno, port,addressno, out mssg);
            //ProcessPrintMssg.Print(mssg);
            pointname = model.point_name;
            
            return true;
            //return model.point_name;
        }

        public static List<string> XmnameListLoad(out string mssg)
        {
            ProcessXmList processXmList = new ProcessXmList();
            return processXmList.XmList(out mssg);
        }

        public static member XmAdministratorLoad(string xmname,out string mssg)
        {
            ProcessXmAdministrator processXmAdministrator = new ProcessXmAdministrator();
            return processXmAdministrator.XmAdministrator(xmname,out mssg);
        }

        public static bool ExistsClient_Xmname(string userId, string client_xmname)
        {
            var model = new ProcessXmBLL.ExistsClient_XmnameModel(userId,client_xmname);
            return processXmBLL.ExistsClient_Xmname(model,out mssg);
        }
        public static bool ExistsXmname(string userId, string client_xmname)
        {
            var model = new ProcessXmBLL.ExistsXmnameModel(userId, client_xmname);
            return processXmBLL.ExistsXmname(model, out mssg);
        }

        public static Authority.Model.UnitMember MonitorInfoGetFromUserID(string userID, out string mssg)
        {
            ProcessMonitorBLL monitorBLL = new ProcessMonitorBLL();
            var processMonitorInfoLoadModel = new ProcessMonitorBLL.ProcessMonitorInfoLoadModel(userID);
            List<Authority.Model.UnitMember> lm = new List<Authority.Model.UnitMember>();
            if (monitorBLL.ProcessMonitorInfoLoadByUserID(processMonitorInfoLoadModel, out mssg))
            {
                return processMonitorInfoLoadModel.model;

            }
            return null;
        }


    }
}
