﻿/**  版本信息模板在安装目录下，可自行修改。
* cycdirnet.cs
*
* 功 能： N/A
* 类 名： cycdirnet
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Web.Script.Serialization;
using SqlHelpers;
namespace TotalStationCgDAL.BLL
{
    /// <summary>
    /// orgldata
    /// </summary>
    public partial class orgldata
    {
        private readonly TotalStationCgDAL.DAL.orgldata dal = new TotalStationCgDAL.DAL.orgldata();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static database db = new database();
        public orgldata()
        { }
        #region  BasicMethod
        
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStationCgDAL.Model.orgldata model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = "原始数据添加成功";
                    return true;
                }
                else
                {
                    mssg = "原始数据添加失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据添加出错，错误信息：" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStationCgDAL.Model.orgldata model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string TaskName, string POINT_NAME, DateTime SEARCH_TIME)
        {

            return dal.Delete(TaskName, POINT_NAME, SEARCH_TIME);
        }

        public bool OrgldataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.OrgldataTableLoad(startCyc, endCyc, startPageIndex, pageSize, xmname, colName, sord, out  dt))
                {
                    mssg = "原始数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "原始数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool OrgldataTableRowsCount(int startCyc, int endCyc, string colName, string xmname, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.OrgldataTableRowsCount(startCyc, endCyc, colName, xmname,out totalCont))
                {
                    mssg = "原始数据表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "原始数据表记录数加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        //根据项目名获取所以的周期
        public bool CycList(string xmname, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {

                if (dal.CycList(xmname, out dt))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }
        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

