﻿/**  版本信息模板在安装目录下，可自行修改。
* cycdirnet.cs
*
* 功 能： N/A
* 类 名： cycdirnet
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using System.Web.Script.Serialization;
using Tool;
using SqlHelpers;
namespace TotalStationCgDAL.BLL
{
    /// <summary>
    /// cycdirnet
    /// </summary>
    public partial class cycdirnet
    {

        private readonly TotalStationCgDAL.DAL.cycdirnet dal = new TotalStationCgDAL.DAL.cycdirnet();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public cycdirnet()
        { }
        #region  BasicMethod


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目{0}点名{1}第{2}周期测量时间{3}结果数据添加成功",model.TaskName,model.POINT_NAME,model.CYC,model.Time);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}点名{1}第{2}周期测量时间{3}结果数据添加失败", model.TaskName, model.POINT_NAME, model.CYC, model.Time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}点名{1}第{2}周期测量时间{3}结果数据添加出错，错误信息:" + ex.Message, model.TaskName, model.POINT_NAME, model.CYC, model.Time);
                return false;
            }
        }
        public bool MaxTime(string xmname, out DateTime maxTime,out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.MaxTime(xmname, out maxTime))
                {
                    mssg = string.Format("获取项目{0}表面位移成果数据的最大日期{1}成功", xmname, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}表面位移成果数据的最大日期失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移成果数据的最大日期出错，错误信息:" + ex.Message, xmname);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStationCgDAL.Model.cycdirnet model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string TaskName, int cyc, string POINT_NAME,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.Delete(TaskName, cyc, POINT_NAME))
                {
                    mssg = string.Format("删除{0}表面位移{1}{2}的数据成功", TaskName, POINT_NAME, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除{0}表面位移{1}{2}的数据失败", TaskName, POINT_NAME, cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除{0}表面位移{1}{2}的数据出错,错误信息:"+ex.Message, TaskName, POINT_NAME, cyc);
                return false;
            }

            
        }

        public bool CgPointNameResultDataLoad(int startPageIndex, int pageSize, string xmname, string pointName, out DataTable dt,out string mssg)
        {
            mssg = "";
            dt = null;
            try {
                if (dal.CgPointNameResultDataLoad(startPageIndex, pageSize, xmname, pointName, out dt))
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点的成果数据成功", xmname, pointName);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点的成果数据失败", xmname, pointName);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移{1}点的成果数据出错,错误信息:"+ex.Message, xmname, pointName);
                return false;
            }

            //return false;
        }

        public bool Singlepointcycload(string xmname, string pointname, out DataTable dt, out string mssg)
        {

            dt = null;
            try
            {
                if (dal.Singlepointcycload(xmname, pointname, out dt))
                {
                    mssg = string.Format("获取项目{0}表面位移{1}的周期数{2}成功", xmname, pointname, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}表面位移{1}的周期数{2}失败", xmname, pointname, dt.Rows.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移{1}的周期列表出错,错误信息:" + ex.Message, xmname, pointname);
                return false;
            }

        }

        public bool Singlecycdirnetdataload(string xmname, string pointname, int startcyc, int endcyc, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.Singlecycdirnetdataload(xmname, pointname, startcyc, endcyc, out dt))
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点{2}-{3}周期的数据{4}条成功", xmname, pointname, startcyc, endcyc, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点{2}-{3}周期的数据失败", xmname, pointname, startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移{1}点{2}-{3}周期的数据出错,错误信息:" + ex.Message, xmname, pointname, startcyc, endcyc);
                return false;
            }

        }

        public bool ResultDataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname,string sord ,out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultdataTableLoad(startCyc, endCyc, startPageIndex, pageSize, xmname, pointname, sord,out  dt))
                {
                    mssg = "结果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "结果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 条件搜索成果数据
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointName"></param>
        /// <param name="cyc"></param>
        /// <param name="dt"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool CgResultdataSearch(string xmname, string pointName, int cyc, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.CgResultdataSearch(xmname,pointName, cyc,out  dt))
                {
                    mssg = "结果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "结果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }



        public bool ResultTableRowsCount(int startCyc, int endCyc, string xmname, string pointname, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.ResultTableRowsCount(startCyc, endCyc, xmname, pointname, out totalCont))
                {
                    mssg = "结果数据表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "结果数据表记录数加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool ResultDataReportPrint(string sql, string xmname, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultDataReportPrint(sql, xmname, out dt))
                {
                    mssg = "结果数据报表数据表生成成功";
                    return true;
                }
                else
                {
                    mssg = "结果数据报表数据表生成失败";
                    return false;
                }
            }
            catch (Exception ex)
            {

                mssg = "结果数据报表数据表生成出错，错误信息" + ex.Message;
                return false;

            }
        }
        //根据项目名称获取端点周期
        public bool ExtremelyCycGet(string xmname, string DateFunction, out string mssg, out string ExtremelyCyc)
        {
            ExtremelyCyc = null;
            try
            {

                if (dal.ExtremelyCycGet(xmname, DateFunction, out ExtremelyCyc))
                {
                    mssg = "端点周期获取成功";
                    return true;
                }
                else
                {
                    mssg = "端点周期获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "端点周期获取出错，错误信息：" + ex.Message;
                return false;
            }

        }
        //根据项目名获取所以的周期
        public bool CycList(string xmname, out DataTable dt, out string mssg)
        {

            dt = null;
            try
            {

                if (dal.CycList(xmname, out dt))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }
        //根据项目名和点名获取所以的周期
        public bool CycList(string xmname, string pointname,out List<string> ls, out string mssg)
        {

            ls = null;
            try
            {

                if (dal.CycList(xmname, pointname,out ls))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 从起始和结束时间中获取周期
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="pointstr"></param>
        /// <param name="CycList"></param>
        /// <returns></returns>
        public bool GetCycFromDateTime(string xmname, string startTime, string endTime, string pointstr, out List<string> CycList, out string mssg)
        {
            CycList = null;
            mssg = "";
            try
            {
                if (dal.GetCycFromDateTime(xmname, startTime, endTime, pointstr, out CycList))
                {
                    mssg = "从日期中获取端点周期成功";
                    return true;
                }
                else
                {
                    mssg = "从日期中获取端点周期失败";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "从日期中获取端点周期出错，错误信息" + ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(string TaskName, DateTime Time, out object obj, out string mssg)
        {
            List<TotalStation.Model.fmos_obj.cycdirnet> lt = null;
            obj = null;
            try
            {
                if (dal.GetModelList(TaskName, Time, out lt))
                {
                    mssg = string.Format("获取测量时间在{0}后的结果数据成功!", Time);
                    obj = lt;
                    return true;
                }
                else
                {
                    mssg = string.Format("获取测量时间在{0}后的结果数据失败!", Time);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取测量时间在{0}后的结果数据出错!错误信息:" + ex.Message, Time);
                return false;
            }

        }

        public bool XmLastDate(string xmname, out string dateTime, out string mssg)
        {
            dateTime = "";
            try
            {
                if (dal.XmLastDate(xmname, out dateTime))
                {
                    mssg = string.Format("获取项目{0}最大时间成功", xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}最大时间失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}最大时间出错，错误信息:" + ex.Message, xmname);
                return false;
            }

        }

        public bool GetModel(string TaskName, string pointname, int cyc, out TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(TaskName, pointname, cyc, out model))
                {
                    mssg = string.Format("获取{0}{1}点第{2}周期的成果数据成功", TaskName, pointname, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}点第{2}周期的成果数据失败", TaskName, pointname, cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}{1}点第{2}周期的成果数据出错,错误信息:" + ex.Message, TaskName, pointname, cyc);
                return false;
            }
        }

        /// <summary>
        /// 前一个周期日期小于当前日期的记录是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <param name="cyc"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool IsPexCycdirnetExist(string xmname, string pointname, int cyc, string dt,out string mssg)
        {
            try
            {
                if (dal.IsPexCycdirnetExist(xmname, pointname, cyc, dt))
                {
                    mssg = string.Format("成果库中项目{0}点名{1}周期为{2}且数据采集日期小于{3}的记录存在", xmname, pointname, cyc, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("成果库中项目{0}点名{1}周期为{2}且数据采集日期小于{3}的记录不存在", xmname, pointname, cyc, dt);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("查询成果库中项目{0}点名{1}周期为{2}且数据采集日期小于{3}的记录出错,错误信息:"+ex.Message, xmname, pointname, cyc, dt);
                return false;
            }
            
        }

        public bool IsCycdirnetExist(string xmname, string pointname, int cyc,out string mssg)
        {
            try
            {
                if (dal.IsCycdirnetExist(xmname, pointname, cyc))
                {
                    mssg = string.Format("成果库中项目{0}点名{1}周期为{2}的记录存在", xmname, pointname, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("成果库中项目{0}点名{1}周期为{2}的记录不存在", xmname, pointname, cyc);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("查询成果库中项目{0}点名{1}周期为{2}的记录出错,错误信息："+ex.Message, xmname, pointname, cyc);
                return false;
            }
        }

        public bool TotalStationPointLoadDAL(int xmno, out List<string> ls,out string mssg)
        {
            ls = null;
            try
            {
                if (dal.TotalStationPointLoadDAL(xmno, out ls))
                {
                    mssg = string.Format("获取项目{0}的点名列表成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的点名列表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的点名列表出错，错误信息:" + ex.Message, xmno);
                return false;
            }

        }

        public bool GetModel(string TaskName, string pointname, DateTime dt, out TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(TaskName, pointname, dt, out model))
                {
                    mssg = string.Format("获取{0}{1}点{2}的成果数据成功", TaskName, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}点{2}的成果数据失败", TaskName, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}{1}点{2}的成果数据出错,错误信息:" + ex.Message, TaskName, pointname, dt);
                return false;
            }
        }
        public bool PointNameCycListGet(string xmname, string pointname, out List<string> ls, out string mssg)
        {
            ls = new List<string>();
            try
            {
                if (dal.PointNameCycListGet(xmname, pointname, out ls))
                {
                    mssg = string.Format("获取项目{0}点名{1}的周期列表数量{2}成功", xmname, pointname, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}点名{1}的周期列表失败", xmname, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}点名{1}的周期列表出错,错误信息:" + ex.Message, xmname, pointname);
                return false;
            }

        }

        public bool PointNewestDateTimeGet(string xmname, string pointname, out DateTime dt, out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(xmname, pointname, out dt))
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点的最新数据的测量时间{2}成功", xmname, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点的最新数据的测量时间失败", xmname, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移{1}点的最新数据的测量时间出错，错误信息:" + ex.Message, xmname, pointname);
                return false;
            }

        }

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

