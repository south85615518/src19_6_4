﻿/**  版本信息模板在安装目录下，可自行修改。
* orgldata.cs
*
* 功 能： N/A
* 类 名： orgldata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStationCgDAL.Model
{
	/// <summary>
	/// orgldata:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class orgldata
	{
		public orgldata()
		{}
		#region Model
		private int _id=0;
		private string _taskname;
		private int _cyc;
		private int _ch;
		private string _point_name;
		private string _point_har;
		private string _point_var;
		private double _har;
		private double _var;
		private double _sd;
		private double _haracc;
		private double _varacc;
		private double _sdacc;
		private double _crossincl;
		private double _lenincl;
		private DateTime _search_time;
		private string _stationmark;
		private string _stationname;
		private double _targethight=0;
		private double _dn;
		private double _de;
		private double _dz;
		private double _n;
		private double _e;
		private double _z;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TaskName
		{
			set{ _taskname=value;}
			get{return _taskname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int CYC
		{
			set{ _cyc=value;}
			get{return _cyc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int CH
		{
			set{ _ch=value;}
			get{return _ch;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POINT_NAME
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POINT_HAR
		{
			set{ _point_har=value;}
			get{return _point_har;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POINT_VAR
		{
			set{ _point_var=value;}
			get{return _point_var;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Har
		{
			set{ _har=value;}
			get{return _har;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Var
		{
			set{ _var=value;}
			get{return _var;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double SD
		{
			set{ _sd=value;}
			get{return _sd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double HarAcc
		{
			set{ _haracc=value;}
			get{return _haracc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double VarAcc
		{
			set{ _varacc=value;}
			get{return _varacc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double SdAcc
		{
			set{ _sdacc=value;}
			get{return _sdacc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double CrossIncl
		{
			set{ _crossincl=value;}
			get{return _crossincl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double LenIncl
		{
			set{ _lenincl=value;}
			get{return _lenincl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime SEARCH_TIME
		{
			set{ _search_time=value;}
			get{return _search_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string StationMark
		{
			set{ _stationmark=value;}
			get{return _stationmark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string StationName
		{
			set{ _stationname=value;}
			get{return _stationname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double TargetHight
		{
			set{ _targethight=value;}
			get{return _targethight;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dN
		{
			set{ _dn=value;}
			get{return _dn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dE
		{
			set{ _de=value;}
			get{return _de;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dZ
		{
			set{ _dz=value;}
			get{return _dz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double N
		{
			set{ _n=value;}
			get{return _n;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double E
		{
			set{ _e=value;}
			get{return _e;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Z
		{
			set{ _z=value;}
			get{return _z;}
		}
		#endregion Model

	}
}

