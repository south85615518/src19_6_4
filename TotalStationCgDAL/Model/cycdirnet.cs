﻿/**  版本信息模板在安装目录下，可自行修改。
* cycdirnet.cs
*
* 功 能： N/A
* 类 名： cycdirnet
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStationCgDAL.Model
{
	/// <summary>
	/// cycdirnet:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cycdirnet:TotalStation.Model.fmos_obj.cycdirnet
	{
		public cycdirnet()
		{}
		#region Model
		private double _this_dn;
		private double _this_de;
		private double _this_dz;
		private double _ac_dn;
		private double _ac_de;
		private double _ac_dz;
		private double _ac_dnspd;
		private double _ac_despd;
		private double _ac_dzspd;
		private double _this_dnspd;
		private double _this_despd;
		private double _this_dzspd;
		private double _this_davghar;
		private double _this_davgvar;
		private double _this_davgsd;
		private double _ac_davghar;
		private double _ac_davgvar;
		private double _ac_davgsd;
		private int? _cyc;
		private string _taskname;
		private string _avghar;
		private string _correctazimuth;
		private string _avgvar;
		private DateTime _time;
		private string _point_name;
		private double _davgsd;
		private double _n;
		private double _e;
		private double _z;
		private double _davghar;
		private double _davgvar;
		private double _davgzeroang;
        private bool _iscontrolpoint = false;
		private double _dcorrectazimuth;
		private string _calculationmethod;
		/// <summary>
		/// 
		/// </summary>
		public double This_dN
		{
			set{ _this_dn=value;}
			get{return _this_dn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_dE
		{
			set{ _this_de=value;}
			get{return _this_de;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_dZ
		{
			set{ _this_dz=value;}
			get{return _this_dz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dN
		{
			set{ _ac_dn=value;}
			get{return _ac_dn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dE
		{
			set{ _ac_de=value;}
			get{return _ac_de;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dZ
		{
			set{ _ac_dz=value;}
			get{return _ac_dz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dNspd
		{
			set{ _ac_dnspd=value;}
			get{return _ac_dnspd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dEspd
		{
			set{ _ac_despd=value;}
			get{return _ac_despd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dZspd
		{
			set{ _ac_dzspd=value;}
			get{return _ac_dzspd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_dNspd
		{
			set{ _this_dnspd=value;}
			get{return _this_dnspd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_dEspd
		{
			set{ _this_despd=value;}
			get{return _this_despd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_dZspd
		{
			set{ _this_dzspd=value;}
			get{return _this_dzspd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_dAvgHar
		{
			set{ _this_davghar=value;}
			get{return _this_davghar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_dAvgVar
		{
			set{ _this_davgvar=value;}
			get{return _this_davgvar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_dAvgSd
		{
			set{ _this_davgsd=value;}
			get{return _this_davgsd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dAvgHar
		{
			set{ _ac_davghar=value;}
			get{return _ac_davghar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dAvgVar
		{
			set{ _ac_davgvar=value;}
			get{return _ac_davgvar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_dAvgSd
		{
			set{ _ac_davgsd=value;}
			get{return _ac_davgsd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CYC
		{
			set{ _cyc=value;}
			get{return _cyc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TaskName
		{
			set{ _taskname=value;}
			get{return _taskname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AvgHar
		{
			set{ _avghar=value;}
			get{return _avghar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CorrectAzimuth
		{
			set{ _correctazimuth=value;}
			get{return _correctazimuth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AvgVar
		{
			set{ _avgvar=value;}
			get{return _avgvar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime Time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POINT_NAME
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dAvgSD
		{
			set{ _davgsd=value;}
			get{return _davgsd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double N
		{
			set{ _n=value;}
			get{return _n;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double E
		{
			set{ _e=value;}
			get{return _e;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Z
		{
			set{ _z=value;}
			get{return _z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dAvgHar
		{
			set{ _davghar=value;}
			get{return _davghar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dAvgVar
		{
			set{ _davgvar=value;}
			get{return _davgvar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dAvgZeroAng
		{
			set{ _davgzeroang=value;}
			get{return _davgzeroang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsControlPoint
		{
			set{ _iscontrolpoint=value;}
            get { return _iscontrolpoint; }
		}
		/// <summary>
		/// 
		/// </summary>
		public double dCorrectAzimuth
		{
			set{ _dcorrectazimuth=value;}
			get{return _dcorrectazimuth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string calculationMethod
		{
			set{ _calculationmethod=value;}
			get{return _calculationmethod;}
		}
		#endregion Model

	}
}

