﻿/**  版本信息模板在安装目录下，可自行修改。
* cycdirnet.cs
*
* 功 能： N/A
* 类 名： cycdirnet
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace TotalStationCgDAL.DAL
{
    /// <summary>
    /// 数据访问类:cycdirnet
    /// </summary>
    public partial class cycdirnet
    {
        public database db = new database();
        public cycdirnet()
        { }
        #region  BasicMethod



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.cycdirnet model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderCgConn(model.TaskName);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into fmos_cycdirnet(");
            strSql.Append("This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod)");
            strSql.Append(" values (");
            strSql.Append("@This_dN,@This_dE,@This_dZ,@Ac_dN,@Ac_dE,@Ac_dZ,@Ac_dNspd,@Ac_dEspd,@Ac_dZspd,@This_dNspd,@This_dEspd,@This_dZspd,@This_dAvgHar,@This_dAvgVar,@This_dAvgSd,@Ac_dAvgHar,@Ac_dAvgVar,@Ac_dAvgSd,@CYC,@TaskName,@AvgHar,@CorrectAzimuth,@AvgVar,@Time,@POINT_NAME,@dAvgSD,@N,@E,@Z,@dAvgHar,@dAvgVar,@dAvgZeroAng,@IsControlPoint,@dCorrectAzimuth,@calculationMethod)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("This_dN=@This_dN,");
            strSql.Append("This_dE=@This_dE,");
            strSql.Append("This_dZ=@This_dZ,");
            strSql.Append("Ac_dN=@Ac_dN,");
            strSql.Append("Ac_dE=@Ac_dE,");
            strSql.Append("Ac_dZ=@Ac_dZ,");
            strSql.Append("Ac_dNspd=@Ac_dNspd,");
            strSql.Append("Ac_dEspd=@Ac_dEspd,");
            strSql.Append("Ac_dZspd=@Ac_dZspd,");
            strSql.Append("This_dNspd=@This_dNspd,");
            strSql.Append("This_dEspd=@This_dEspd,");
            strSql.Append("This_dZspd=@This_dZspd,");
            strSql.Append("This_dAvgHar=@This_dAvgHar,");
            strSql.Append("This_dAvgVar=@This_dAvgVar,");
            strSql.Append("This_dAvgSd=@This_dAvgSd,");
            strSql.Append("Ac_dAvgHar=@Ac_dAvgHar,");
            strSql.Append("Ac_dAvgVar=@Ac_dAvgVar,");
            strSql.Append("Ac_dAvgSd=@Ac_dAvgSd,");
            strSql.Append("CYC=@CYC,");
            strSql.Append("AvgHar=@AvgHar,");
            strSql.Append("CorrectAzimuth=@CorrectAzimuth,");
            strSql.Append("AvgVar=@AvgVar,");
            strSql.Append("dAvgSD=@dAvgSD,");
            strSql.Append("N=@N,");
            strSql.Append("E=@E,");
            strSql.Append("Z=@Z,");
            strSql.Append("dAvgHar=@dAvgHar,");
            strSql.Append("dAvgVar=@dAvgVar,");
            strSql.Append("dAvgZeroAng=@dAvgZeroAng,");
            strSql.Append("IsControlPoint=@IsControlPoint,");
            strSql.Append("dCorrectAzimuth=@dCorrectAzimuth,");
            strSql.Append("calculationMethod=@calculationMethod,");
            strSql.Append("time=@time");
            OdbcParameter[] parameters = {
					new OdbcParameter("@This_dN", OdbcType.Double),
					new OdbcParameter("@This_dE", OdbcType.Double),
					new OdbcParameter("@This_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dN", OdbcType.Double),
					new OdbcParameter("@Ac_dE", OdbcType.Double),
					new OdbcParameter("@Ac_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dNspd", OdbcType.Double),
					new OdbcParameter("@This_dEspd", OdbcType.Double),
					new OdbcParameter("@This_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@CYC", OdbcType.Int,11),
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@Time", OdbcType.DateTime),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@dAvgSD", OdbcType.Double),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@dAvgHar", OdbcType.Double),
					new OdbcParameter("@dAvgVar", OdbcType.Double),
					new OdbcParameter("@dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@IsControlPoint", OdbcType.TinyInt,11),
					new OdbcParameter("@dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@calculationMethod", OdbcType.VarChar,120),

                    new OdbcParameter("@_This_dN", OdbcType.Double),
					new OdbcParameter("@_This_dE", OdbcType.Double),
					new OdbcParameter("@_This_dZ", OdbcType.Double),
					new OdbcParameter("@_Ac_dN", OdbcType.Double),
					new OdbcParameter("@_Ac_dE", OdbcType.Double),
					new OdbcParameter("@_Ac_dZ", OdbcType.Double),
					new OdbcParameter("@_Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@_Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@_Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@_This_dNspd", OdbcType.Double),
					new OdbcParameter("@_This_dEspd", OdbcType.Double),
					new OdbcParameter("@_This_dZspd", OdbcType.Double),
					new OdbcParameter("@_This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@_CYC", OdbcType.Int,11),
					new OdbcParameter("@_AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@_CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@_AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@_dAvgSD", OdbcType.Double),
					new OdbcParameter("@_N", OdbcType.Double),
					new OdbcParameter("@_E", OdbcType.Double),
					new OdbcParameter("@_Z", OdbcType.Double),
					new OdbcParameter("@_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@_IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("@_dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@_calculationMethod", OdbcType.VarChar,120),
                    new OdbcParameter("@_Time", OdbcType.DateTime)

                                         };
            parameters[0].Value = model.This_dN;
            parameters[1].Value = model.This_dE;
            parameters[2].Value = model.This_dZ;
            parameters[3].Value = model.Ac_dN;
            parameters[4].Value = model.Ac_dE;
            parameters[5].Value = model.Ac_dZ;
            parameters[6].Value = model.Ac_dNspd;
            parameters[7].Value = model.Ac_dEspd;
            parameters[8].Value = model.Ac_dZspd;
            parameters[9].Value = model.This_dNspd;
            parameters[10].Value = model.This_dEspd;
            parameters[11].Value = model.This_dZspd;
            parameters[12].Value = model.This_dAvgHar;
            parameters[13].Value = model.This_dAvgVar;
            parameters[14].Value = model.This_dAvgSd;
            parameters[15].Value = model.Ac_dAvgHar;
            parameters[16].Value = model.Ac_dAvgVar;
            parameters[17].Value = model.Ac_dAvgSd;
            parameters[18].Value = model.CYC;
            parameters[19].Value = model.TaskName;
            parameters[20].Value = model.AvgHar;
            parameters[21].Value = model.CorrectAzimuth;
            parameters[22].Value = model.AvgVar;
            parameters[23].Value = model.Time;
            parameters[24].Value = model.POINT_NAME;
            parameters[25].Value = model.dAvgSD;
            parameters[26].Value = model.N;
            parameters[27].Value = model.E;
            parameters[28].Value = model.Z;
            parameters[29].Value = model.dAvgHar;
            parameters[30].Value = model.dAvgVar;
            parameters[31].Value = model.dAvgZeroAng;
            parameters[32].Value = model.IsControlPoint;
            parameters[33].Value = model.dCorrectAzimuth;
            parameters[34].Value = model.calculationMethod;
            parameters[35].Value = model.This_dN;
            parameters[36].Value = model.This_dE;
            parameters[37].Value = model.This_dZ;
            parameters[38].Value = model.Ac_dN;
            parameters[39].Value = model.Ac_dE;
            parameters[40].Value = model.Ac_dZ;
            parameters[41].Value = model.Ac_dNspd;
            parameters[42].Value = model.Ac_dEspd;
            parameters[43].Value = model.Ac_dZspd;
            parameters[44].Value = model.This_dNspd;
            parameters[45].Value = model.This_dEspd;
            parameters[46].Value = model.This_dZspd;
            parameters[47].Value = model.This_dAvgHar;
            parameters[48].Value = model.This_dAvgVar;
            parameters[49].Value = model.This_dAvgSd;
            parameters[50].Value = model.Ac_dAvgHar;
            parameters[51].Value = model.Ac_dAvgVar;
            parameters[52].Value = model.Ac_dAvgSd;
            parameters[53].Value = model.CYC;
            parameters[54].Value = model.AvgHar;
            parameters[55].Value = model.CorrectAzimuth;
            parameters[56].Value = model.AvgVar;
            parameters[57].Value = model.dAvgSD;
            parameters[58].Value = model.N;
            parameters[59].Value = model.E;
            parameters[60].Value = model.Z;
            parameters[61].Value = model.dAvgHar;
            parameters[62].Value = model.dAvgVar;
            parameters[63].Value = model.dAvgZeroAng;
            parameters[64].Value = model.IsControlPoint;
            parameters[65].Value = model.dCorrectAzimuth;
            parameters[66].Value = model.calculationMethod;
            parameters[67].Value = model.Time;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.cycdirnet model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderCgConn(model.TaskName);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update fmos_cycdirnet set ");
            strSql.Append("This_dN=@This_dN,");
            strSql.Append("This_dE=@This_dE,");
            strSql.Append("This_dZ=@This_dZ,");
            strSql.Append("Ac_dN=@Ac_dN,");
            strSql.Append("Ac_dE=@Ac_dE,");
            strSql.Append("Ac_dZ=@Ac_dZ,");
            strSql.Append("Ac_dNspd=@Ac_dNspd,");
            strSql.Append("Ac_dEspd=@Ac_dEspd,");
            strSql.Append("Ac_dZspd=@Ac_dZspd,");
            strSql.Append("This_dNspd=@This_dNspd,");
            strSql.Append("This_dEspd=@This_dEspd,");
            strSql.Append("This_dZspd=@This_dZspd,");
            strSql.Append("This_dAvgHar=@This_dAvgHar,");
            strSql.Append("This_dAvgVar=@This_dAvgVar,");
            strSql.Append("This_dAvgSd=@This_dAvgSd,");
            strSql.Append("Ac_dAvgHar=@Ac_dAvgHar,");
            strSql.Append("Ac_dAvgVar=@Ac_dAvgVar,");
            strSql.Append("Ac_dAvgSd=@Ac_dAvgSd,");
            strSql.Append("CYC=@CYC,");
            strSql.Append("AvgHar=@AvgHar,");
            strSql.Append("CorrectAzimuth=@CorrectAzimuth,");
            strSql.Append("AvgVar=@AvgVar,");
            strSql.Append("dAvgSD=@dAvgSD,");
            strSql.Append("N=@N,");
            strSql.Append("E=@E,");
            strSql.Append("Z=@Z,");
            strSql.Append("dAvgHar=@dAvgHar,");
            strSql.Append("dAvgVar=@dAvgVar,");
            strSql.Append("dAvgZeroAng=@dAvgZeroAng,");
            strSql.Append("IsControlPoint=@IsControlPoint,");
            strSql.Append("dCorrectAzimuth=@dCorrectAzimuth,");
            strSql.Append("calculationMethod=@calculationMethod");
            strSql.Append(" where TaskName=@TaskName  and Time=@Time and POINT_NAME=@POINT_NAME ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@This_dN", OdbcType.Double),
					new OdbcParameter("@This_dE", OdbcType.Double),
					new OdbcParameter("@This_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dN", OdbcType.Double),
					new OdbcParameter("@Ac_dE", OdbcType.Double),
					new OdbcParameter("@Ac_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dNspd", OdbcType.Double),
					new OdbcParameter("@This_dEspd", OdbcType.Double),
					new OdbcParameter("@This_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@CYC", OdbcType.Int,11),
					new OdbcParameter("@AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@dAvgSD", OdbcType.Double),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@dAvgHar", OdbcType.Double),
					new OdbcParameter("@dAvgVar", OdbcType.Double),
					new OdbcParameter("@dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("@dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@calculationMethod", OdbcType.VarChar,120),
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@Time", OdbcType.DateTime),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120)};
            parameters[0].Value = model.This_dN;
            parameters[1].Value = model.This_dE;
            parameters[2].Value = model.This_dZ;
            parameters[3].Value = model.Ac_dN;
            parameters[4].Value = model.Ac_dE;
            parameters[5].Value = model.Ac_dZ;
            parameters[6].Value = model.Ac_dNspd;
            parameters[7].Value = model.Ac_dEspd;
            parameters[8].Value = model.Ac_dZspd;
            parameters[9].Value = model.This_dNspd;
            parameters[10].Value = model.This_dEspd;
            parameters[11].Value = model.This_dZspd;
            parameters[12].Value = model.This_dAvgHar;
            parameters[13].Value = model.This_dAvgVar;
            parameters[14].Value = model.This_dAvgSd;
            parameters[15].Value = model.Ac_dAvgHar;
            parameters[16].Value = model.Ac_dAvgVar;
            parameters[17].Value = model.Ac_dAvgSd;
            parameters[18].Value = model.CYC;
            parameters[19].Value = model.AvgHar;
            parameters[20].Value = model.CorrectAzimuth;
            parameters[21].Value = model.AvgVar;
            parameters[22].Value = model.dAvgSD;
            parameters[23].Value = model.N;
            parameters[24].Value = model.E;
            parameters[25].Value = model.Z;
            parameters[26].Value = model.dAvgHar;
            parameters[27].Value = model.dAvgVar;
            parameters[28].Value = model.dAvgZeroAng;
            parameters[29].Value = model.IsControlPoint;
            parameters[30].Value = model.dCorrectAzimuth;
            parameters[31].Value = model.calculationMethod;
            parameters[32].Value = model.TaskName;
            parameters[33].Value = model.Time;
            parameters[34].Value = model.POINT_NAME;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string TaskName, int cyc, string POINT_NAME)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from fmos_cycdirnet ");
            strSql.Append(" where    TaskName=@TaskName    and    cyc>=@cyc    and    POINT_NAME=@POINT_NAME    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@cyc", OdbcType.Int),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120)			
                                         };
            parameters[0].Value = TaskName;
            parameters[1].Value = cyc;
            parameters[2].Value = POINT_NAME;
            OdbcSQLHelper.Conn = db.GetStanderCgConn(TaskName);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ResultdataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname, string sord,out DataTable dt)
        {
            dt = new DataTable();
            string searchstr = pointname == "" || pointname == null||(pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select point_name,CYC,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time from fmos_cycdirnet where      taskName=@taskName      and    {0}    and     CYC    between    @startCyc      and    @endCyc  order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr,sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@startCyc", OdbcType.Int),
					new OdbcParameter("@endCyc", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = startCyc;
            parameters[2].Value = endCyc;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool CgResultdataSearch(string xmname, string pointName,int cyc, out DataTable dt)
        {
            
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            string sql = string.Format("select point_name,CYC,time,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz from fmos_cycdirnet where taskName='{0}'   and point_name='{1}' and cyc = {2} ", xmname, pointName, cyc);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool CgResultDataDelete(string xmname, string pointName, int cyc, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            string sql = string.Format("delete  from  fmos_cycdirnet where cyc <= {0}", xmname, pointName, cyc);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool CgPointNameResultDataLoad( int startPageIndex, int pageSize, string xmname,string pointName, out DataTable dt)
        {
            
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            string sql = string.Format("select point_name,CYC,time,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz  from fmos_cycdirnet where taskName='{2}' and  point_name='{3}'  order by cyc asc    limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, xmname, pointName);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }


        public bool ResultTableRowsCount(int startCyc, int endCyc, string xmname, string pointname, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from fmos_cycdirnet where taskName='{2}' and {3}  and CYC between {0} and {1}", startCyc, endCyc, xmname, searchstr);
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }

        public bool ResultDataReportPrint(string sql, string xmname, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            dt = querysql.querystanderdb(sql, conn);
            return true;
        }

        //根据项目名称获取端点周期
        public bool ExtremelyCycGet(string xmname, string DateFunction, out string ExtremelyCyc)
        {

            string sql = "select  " + DateFunction + "(cyc)  from fmos_cycdirnet where taskName='" + xmname + "'";
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            ExtremelyCyc = querysql.querystanderstr(sql, conn);
            return ExtremelyCyc != null ? true : false;

        }
        //根据项目名获取所以的周期
        public bool CycList(string xmname, out DataTable dt)
        {

            string sql = "select  distinct(cyc)  from fmos_cycdirnet where taskName='" + xmname + "'";
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            dt = querysql.querystanderdb(sql, conn);
            return dt != null ? true : false;
        }
        //根据项目名称获取某个点名的周期列表
        public bool CycList(string xmname, string pointname,out List<string> ls)
        {

            string sql = "select  distinct(cyc)  from fmos_cycdirnet where taskName='" + xmname + "' and point_name = '"+pointname+"'";
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            ls = querysql.querystanderlist(sql, conn);
            return ls != null ? true : false;
        }



        /// <summary>
        /// 从起始和结束时间中获取周期
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="pointstr"></param>
        /// <param name="CycList"></param>
        /// <returns></returns>
        public bool GetCycFromDateTime(string xmname, string startTime, string endTime, string pointstr, out List<string> CycList)
        {
            CycList = new List<string>();
            string sql = @"select min(cyc) from fmos_cycdirnet where fmos_cycdirnet.Time >= DATE_FORMAT('" + startTime + "','%y-%m-%d %H:%i:%s') and fmos_cycdirnet.Time <= DATE_FORMAT('" + endTime + "','%y-%m-%d %H:%i:%s') and point_name in (" + pointstr + ")";
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            string maxCyc = querysql.querystanderstr(sql, conn);
            CycList.Add(maxCyc);
            sql = @"select max(cyc) from fmos_cycdirnet where fmos_cycdirnet.Time >= DATE_FORMAT('" + startTime + "','%y-%m-%d %H:%i:%s') and fmos_cycdirnet.Time <= DATE_FORMAT('" + endTime + "','%y-%m-%d %H:%i:%s') and point_name in (" + pointstr + ")";
            string minCyc = querysql.querystanderstr(sql, conn);
            CycList.Add(minCyc);
            return true;
        }

        //public bool GetCycdirnetAlarmModelList(string xmname,string DataTime , out object obj)
        //{
        //    obj = null;
        //    string sql = "select *  ";
        //}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(string TaskName, DateTime Time, out List<TotalStation.Model.fmos_obj.cycdirnet> lt)
        {
            OdbcConnection conn = db.GetStanderCgConn(TaskName);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod from fmos_cycdirnet ");
            strSql.Append(" where    TaskName=@TaskName     and    Time>=@Time   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@Time", OdbcType.DateTime)
							};
            parameters[0].Value = TaskName;
            parameters[1].Value = Time;

            lt = new List<TotalStation.Model.fmos_obj.cycdirnet>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.cycdirnet DataRowToModel(DataRow row)
        {
            TotalStation.Model.fmos_obj.cycdirnet model = new TotalStationCgDAL.Model.cycdirnet();
            if (row != null)
            {
                if (row["This_dN"] != null && row["This_dN"].ToString() != "")
                {
                    model.This_dN = double.Parse(row["This_dN"].ToString());
                }
                if (row["This_dE"] != null && row["This_dE"].ToString() != "")
                {
                    model.This_dE = double.Parse(row["This_dE"].ToString());
                }
                if (row["This_dZ"] != null && row["This_dZ"].ToString() != "")
                {
                    model.This_dZ = double.Parse(row["This_dZ"].ToString());
                }
                if (row["N"] != null && row["N"].ToString() != "")
                {
                    model.N = double.Parse(row["N"].ToString());
                }
                if (row["E"] != null && row["E"].ToString() != "")
                {
                    model.E = double.Parse(row["E"].ToString());
                }
                if (row["Z"] != null && row["Z"].ToString() != "")
                {
                    model.Z = double.Parse(row["Z"].ToString());
                }
                if (row["Ac_dN"] != null && row["Ac_dN"].ToString() != "")
                {
                    model.Ac_dN = double.Parse(row["Ac_dN"].ToString());
                }
                if (row["Ac_dE"] != null && row["Ac_dE"].ToString() != "")
                {
                    model.Ac_dE = double.Parse(row["Ac_dE"].ToString());
                }
                if (row["Ac_dZ"] != null && row["Ac_dZ"].ToString() != "")
                {
                    model.Ac_dZ = double.Parse(row["Ac_dZ"].ToString());
                }
                if (row["CYC"] != null && row["CYC"].ToString() != "")
                {
                    model.CYC = int.Parse(row["CYC"].ToString());
                }
                if (row["TaskName"] != null)
                {
                    model.TaskName = row["TaskName"].ToString();
                }
                if (row["Time"] != null && row["Time"].ToString() != "")
                {
                    model.Time = Convert.ToDateTime(row["Time"].ToString());
                }
                if (row["POINT_NAME"] != null)
                {
                    model.POINT_NAME = row["POINT_NAME"].ToString();
                }

            }
            return model;
        }

        public bool XmLastDate(string xmname, out string dateTime)
        {
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            OdbcSQLHelper.Conn = conn;
            string sql = "select max(time) from fmos_cycdirnet where taskName='" + xmname + "'";
            dateTime = querysql.querystanderstr(sql, conn);
            return dateTime == null ? false : true;

        }

        public bool Singlepointcycload(string xmname, string pointname, out DataTable dt)
        {
            dt = null;
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select distinct(cyc),concat('第',cyc,'周期[',DATE_FORMAT(time,   '%y-%m-%d %H:%i:%S'),']') as timecyc  from fmos_cycdirnet where     taskName=@taskName    and      point_name=@point_name    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120)
                    
							};
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0];
            return true;
        }
        public bool Singlecycdirnetdataload(string xmname, string pointname, int startcyc, int endcyc, out DataTable dt)
        {
            dt = null;
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select point_name,CYC,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time from fmos_cycdirnet where     taskName=@taskName    and       point_name=@point_name     and    cyc>=@startcyc    and    cyc <= @endcyc   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int)
                    
							};
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0];
            return true;
        }

        public bool GetModel(string TaskName, string pointname, int cyc, out TotalStation.Model.fmos_obj.cycdirnet model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderCgConn(TaskName);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod from fmos_cycdirnet ");
            strSql.Append(" where    TaskName=@TaskName   and   point_name=@point_name    and      cyc = @cyc     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@cyc", OdbcType.Int,4)
							};
            parameters[0].Value = TaskName;
            parameters[1].Value = pointname;
            parameters[2].Value = cyc;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

        public bool IsPexCycdirnetExist(string xmname, string pointname, int cyc, string dt)
        {
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(*) from fmos_cycdirnet where taskName=@taskName and pointname=@pointname and  cyc=@cyc and time < @time ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@taskName", OdbcType.VarChar,120),
					new OdbcParameter("@pointname", OdbcType.VarChar,120),
                    new OdbcParameter("@cyc", OdbcType.Int,4),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            parameters[2].Value = cyc;
            parameters[3].Value = dt;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return (int)obj > 0 ? true : false;
        }

        public bool IsCycdirnetExist(string xmname,string pointname,int cyc)
        {
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(*) from fmos_cycdirnet where  taskName=@taskName  and   point_name=@point_name    and    cyc=@cyc     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@taskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@cyc", OdbcType.Int,4)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            parameters[2].Value = cyc;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return obj!=null&&int.Parse(obj.ToString()) > 0 ? true : false;
        }

        public bool TotalStationPointLoadDAL(int xmno, out List<string> ls)
        {

            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select DISTINCT(POINT_NAME) from fmos_pointalarmvalue where xmno=" + xmno ;
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }
        public bool MaxTime(string xmname, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from fmos_cycdirnet  where   taskName = @xmname");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmname",OdbcType.VarChar,100)
            };
            paramters[0].Value = xmname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }

        public bool GetModel(string TaskName, string pointname, DateTime dt, out TotalStation.Model.fmos_obj.cycdirnet model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderCgConn(TaskName);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod from fmos_cycdirnet ");
            strSql.Append(" where    TaskName=@TaskName   and   point_name=@point_name    and    time = @time   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = TaskName;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

        public bool PointNameCycListGet(string xmname, string pointname, out List<string> ls)
        {

            OdbcConnection conn = db.GetStanderCgConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(cyc) from fmos_cycdirnet  where      taskName=@taskName    and    {0}  ", pointname == "" || pointname == null|| (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            OdbcParameter[] parameters = {
					new OdbcParameter("@taskName", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmname;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                i++;
            }
            return true;

        }

        public bool PointNewestDateTimeGet(string xmname, string pointname, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from fmos_cycdirnet where    taskName=@taskName   and point_name = @point_name  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@taskName", OdbcType.VarChar,120),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) { dt = Convert.ToDateTime("0001-1-1"); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

