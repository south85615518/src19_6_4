﻿/**  版本信息模板在安装目录下，可自行修改。
* orgldata.cs
*
* 功 能： N/A
* 类 名： orgldata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
namespace TotalStationCgDAL.DAL
{
	/// <summary>
	/// 数据访问类:orgldata
	/// </summary>
	public partial class orgldata
	{
        public database db = new database();
		public orgldata()
		{}
		#region  BasicMethod


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TotalStationCgDAL.Model.orgldata model)
		{

			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into fmos_orgldata(");
			strSql.Append("ID,TaskName,CYC,CH,POINT_NAME,POINT_HAR,POINT_VAR,Har,Var,SD,HarAcc,VarAcc,SdAcc,CrossIncl,LenIncl,SEARCH_TIME,StationMark,StationName,TargetHight,dN,dE,dZ,N,E,Z)");
			strSql.Append(" values (");
			strSql.Append("@ID,@TaskName,@CYC,@CH,@POINT_NAME,@POINT_HAR,@POINT_VAR,@Har,@Var,@SD,@HarAcc,@VarAcc,@SdAcc,@CrossIncl,@LenIncl,@SEARCH_TIME,@StationMark,@StationName,@TargetHight,@dN,@dE,@dZ,@N,@E,@Z)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("ID=@ID,");
            strSql.Append("CYC=@CYC,");
            strSql.Append("CH=@CH,");
            strSql.Append("POINT_HAR=@POINT_HAR,");
            strSql.Append("POINT_VAR=@POINT_VAR,");
            strSql.Append("Har=@Har,");
            strSql.Append("Var=@Var,");
            strSql.Append("SD=@SD,");
            strSql.Append("HarAcc=@HarAcc,");
            strSql.Append("VarAcc=@VarAcc,");
            strSql.Append("SdAcc=@SdAcc,");
            strSql.Append("CrossIncl=@CrossIncl,");
            strSql.Append("LenIncl=@LenIncl,");
            strSql.Append("StationMark=@StationMark,");
            strSql.Append("StationName=@StationName,");
            strSql.Append("TargetHight=@TargetHight,");
            strSql.Append("dN=@dN,");
            strSql.Append("dE=@dE,");
            strSql.Append("dZ=@dZ,");
            strSql.Append("N=@N,");
            strSql.Append("E=@E,");
            strSql.Append("Z=@Z");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@CYC", OdbcType.Int,11),
					new OdbcParameter("@CH", OdbcType.Int,11),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_HAR", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_VAR", OdbcType.VarChar,120),
					new OdbcParameter("@Har", OdbcType.Double),
					new OdbcParameter("@Var", OdbcType.Double),
					new OdbcParameter("@SD", OdbcType.Double),
					new OdbcParameter("@HarAcc", OdbcType.Double),
					new OdbcParameter("@VarAcc", OdbcType.Double),
					new OdbcParameter("@SdAcc", OdbcType.Double),
					new OdbcParameter("@CrossIncl", OdbcType.Double),
					new OdbcParameter("@LenIncl", OdbcType.Double),
					new OdbcParameter("@SEARCH_TIME", OdbcType.DateTime),
					new OdbcParameter("@StationMark", OdbcType.VarChar,120),
					new OdbcParameter("@StationName", OdbcType.VarChar,120),
					new OdbcParameter("@TargetHight", OdbcType.Double),
					new OdbcParameter("@dN", OdbcType.Double),
					new OdbcParameter("@dE", OdbcType.Double),
					new OdbcParameter("@dZ", OdbcType.Double),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),

                    new OdbcParameter("@_ID", OdbcType.Int,11),

					new OdbcParameter("@_CYC", OdbcType.Int,11),
					new OdbcParameter("@_CH", OdbcType.Int,11),

					new OdbcParameter("@_POINT_HAR", OdbcType.VarChar,120),
					new OdbcParameter("@_POINT_VAR", OdbcType.VarChar,120),
					new OdbcParameter("@_Har", OdbcType.Double),
					new OdbcParameter("@_Var", OdbcType.Double),
					new OdbcParameter("@_SD", OdbcType.Double),
					new OdbcParameter("@_HarAcc", OdbcType.Double),
					new OdbcParameter("@_VarAcc", OdbcType.Double),
					new OdbcParameter("@_SdAcc", OdbcType.Double),
					new OdbcParameter("@_CrossIncl", OdbcType.Double),
					new OdbcParameter("@_LenIncl", OdbcType.Double),
					new OdbcParameter("@_StationMark", OdbcType.VarChar,120),
					new OdbcParameter("@_StationName", OdbcType.VarChar,120),
					new OdbcParameter("@_TargetHight", OdbcType.Double),
					new OdbcParameter("@_dN", OdbcType.Double),
					new OdbcParameter("@_dE", OdbcType.Double),
					new OdbcParameter("@_dZ", OdbcType.Double),
					new OdbcParameter("@_N", OdbcType.Double),
					new OdbcParameter("@_E", OdbcType.Double),
					new OdbcParameter("@_Z", OdbcType.Double)

                                         };
			parameters[0].Value = model.ID;
			parameters[1].Value = model.TaskName;
			parameters[2].Value = model.CYC;
			parameters[3].Value = model.CH;
			parameters[4].Value = model.POINT_NAME;
			parameters[5].Value = model.POINT_HAR;
			parameters[6].Value = model.POINT_VAR;
			parameters[7].Value = model.Har;
			parameters[8].Value = model.Var;
			parameters[9].Value = model.SD;
			parameters[10].Value = model.HarAcc;
			parameters[11].Value = model.VarAcc;
			parameters[12].Value = model.SdAcc;
			parameters[13].Value = model.CrossIncl;
			parameters[14].Value = model.LenIncl;
			parameters[15].Value = model.SEARCH_TIME;
			parameters[16].Value = model.StationMark;
			parameters[17].Value = model.StationName;
			parameters[18].Value = model.TargetHight;
			parameters[19].Value = model.dN;
			parameters[20].Value = model.dE;
			parameters[21].Value = model.dZ;
			parameters[22].Value = model.N;
			parameters[23].Value = model.E;
			parameters[24].Value = model.Z;

            parameters[25].Value = model.ID;
            parameters[26].Value = model.CYC;
            parameters[27].Value = model.CH;
            parameters[28].Value = model.POINT_HAR;
            parameters[29].Value = model.POINT_VAR;
            parameters[30].Value = model.Har;
            parameters[31].Value = model.Var;
            parameters[32].Value = model.SD;
            parameters[33].Value = model.HarAcc;
            parameters[34].Value = model.VarAcc;
            parameters[35].Value = model.SdAcc;
            parameters[36].Value = model.CrossIncl;
            parameters[37].Value = model.LenIncl;
            parameters[38].Value = model.StationMark;
            parameters[39].Value = model.StationName;
            parameters[40].Value = model.TargetHight;
            parameters[41].Value = model.dN;
            parameters[42].Value = model.dE;
            parameters[43].Value = model.dZ;
            parameters[44].Value = model.N;
            parameters[45].Value = model.E;
            parameters[46].Value = model.Z;


			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStationCgDAL.Model.orgldata model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update fmos_orgldata set ");
			strSql.Append("ID=@ID,");
			strSql.Append("CYC=@CYC,");
			strSql.Append("CH=@CH,");
			strSql.Append("POINT_HAR=@POINT_HAR,");
			strSql.Append("POINT_VAR=@POINT_VAR,");
			strSql.Append("Har=@Har,");
			strSql.Append("Var=@Var,");
			strSql.Append("SD=@SD,");
			strSql.Append("HarAcc=@HarAcc,");
			strSql.Append("VarAcc=@VarAcc,");
			strSql.Append("SdAcc=@SdAcc,");
			strSql.Append("CrossIncl=@CrossIncl,");
			strSql.Append("LenIncl=@LenIncl,");
			strSql.Append("StationMark=@StationMark,");
			strSql.Append("StationName=@StationName,");
			strSql.Append("TargetHight=@TargetHight,");
			strSql.Append("dN=@dN,");
			strSql.Append("dE=@dE,");
			strSql.Append("dZ=@dZ,");
			strSql.Append("N=@N,");
			strSql.Append("E=@E,");
			strSql.Append("Z=@Z");
			strSql.Append(" where TaskName=@TaskName and POINT_NAME=@POINT_NAME and SEARCH_TIME=@SEARCH_TIME ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@CYC", OdbcType.Int,11),
					new OdbcParameter("@CH", OdbcType.Int,11),
					new OdbcParameter("@POINT_HAR", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_VAR", OdbcType.VarChar,120),
					new OdbcParameter("@Har", OdbcType.Double),
					new OdbcParameter("@Var", OdbcType.Double),
					new OdbcParameter("@SD", OdbcType.Double),
					new OdbcParameter("@HarAcc", OdbcType.Double),
					new OdbcParameter("@VarAcc", OdbcType.Double),
					new OdbcParameter("@SdAcc", OdbcType.Double),
					new OdbcParameter("@CrossIncl", OdbcType.Double),
					new OdbcParameter("@LenIncl", OdbcType.Double),
					new OdbcParameter("@StationMark", OdbcType.VarChar,120),
					new OdbcParameter("@StationName", OdbcType.VarChar,120),
					new OdbcParameter("@TargetHight", OdbcType.Double),
					new OdbcParameter("@dN", OdbcType.Double),
					new OdbcParameter("@dE", OdbcType.Double),
					new OdbcParameter("@dZ", OdbcType.Double),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@SEARCH_TIME", OdbcType.DateTime)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.CYC;
			parameters[2].Value = model.CH;
			parameters[3].Value = model.POINT_HAR;
			parameters[4].Value = model.POINT_VAR;
			parameters[5].Value = model.Har;
			parameters[6].Value = model.Var;
			parameters[7].Value = model.SD;
			parameters[8].Value = model.HarAcc;
			parameters[9].Value = model.VarAcc;
			parameters[10].Value = model.SdAcc;
			parameters[11].Value = model.CrossIncl;
			parameters[12].Value = model.LenIncl;
			parameters[13].Value = model.StationMark;
			parameters[14].Value = model.StationName;
			parameters[15].Value = model.TargetHight;
			parameters[16].Value = model.dN;
			parameters[17].Value = model.dE;
			parameters[18].Value = model.dZ;
			parameters[19].Value = model.N;
			parameters[20].Value = model.E;
			parameters[21].Value = model.Z;
			parameters[22].Value = model.TaskName;
			parameters[23].Value = model.POINT_NAME;
			parameters[24].Value = model.SEARCH_TIME;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string TaskName,string POINT_NAME,DateTime SEARCH_TIME)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from fmos_orgldata ");
			strSql.Append(" where TaskName=@TaskName and POINT_NAME=@POINT_NAME and SEARCH_TIME=@SEARCH_TIME ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@SEARCH_TIME", OdbcType.DateTime)			};
			parameters[0].Value = TaskName;
			parameters[1].Value = POINT_NAME;
			parameters[2].Value = SEARCH_TIME;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        public bool OrgldataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string colName, string sord,out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            string sql = string.Format("select  point_name,CYC,point_har,point_var,sd,N,E,Z,dN,dE,dZ,search_Time from fmos_orgldata where taskName='{5}' and CYC between {0} and {1}  {4} limit {2},{3}", startCyc, endCyc, (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmname);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool OrgldataTableRowsCount(int startCyc, int endCyc, string colName,string xmname,out string totalCont)
        {
            string sql = string.Format("select count(*) from fmos_orgldata where taskName='{2}' and CYC between {0} and {1}", startCyc, endCyc, xmname);
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }
        //根据项目名获取所以的周期
        public bool CycList(string xmname, out DataTable dt)
        {

            string sql = "select  distinct(cyc)  from fmos_orgldata where taskName='" + xmname + "'";
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            dt = querysql.querystanderdb(sql, conn);
            return dt != null ? true : false;
        }
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

