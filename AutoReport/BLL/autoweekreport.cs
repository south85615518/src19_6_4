﻿/**  版本信息模板在安装目录下，可自行修改。
* autoweekreport.cs
*
* 功 能： N/A
* 类 名： autoweekreport
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/5 9:27:04   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace AutoReport.BLL
{
	/// <summary>
	/// autoweekreport
	/// </summary>
	public partial class autoweekreport
	{
		private readonly AutoReport.DAL.autoweekreport dal=new AutoReport.DAL.autoweekreport();
		public autoweekreport()
		{}
		#region  BasicMethod

		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AutoReport.Model.autoweekreport model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}每周{1}{2}时监测项{3}的报表生成任务成功", model.xmno, model.week, model.hour, model.jclx);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}每周{1}{2}时监测项{3}的报表生成任务失败", model.xmno, model.week, model.hour, model.jclx);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}每周{1}{2}时监测项{3}的报表生成任务出错，错误信息:" + ex.Message, model.xmno, model.week, model.hour, model.jclx);
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AutoReport.Model.autoweekreport model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno)
		{
			
			return dal.Delete(xmno);
		}
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public bool GetModel(int xmno, int week, int hour, out AutoReport.Model.autoweekreport model,out string mssg)
		{

            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, week, hour, out model))
                {
                    mssg = string.Format("项目编号{0}在每周{1}{2}时有监测项{3}周报生成任务", xmno, week, hour, model.jclx);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}在每周{1}{2}时没有周报生成任务", xmno, week, hour);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}在每周的{1}{2}时的周报生成任务出错,错误信息:" + ex.Message,xmno,week,hour);
                return false;
            }
		}
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno,  out AutoReport.Model.autoweekreport model, out string mssg)
        {

            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, out model))
                {
                    mssg = string.Format("项目编号{0}有监测项{3}在每周{1}{2}时周报生成任务", xmno, model.week, model.hour, model.jclx);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}没有周报生成任务", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的周报生成任务出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }
		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

