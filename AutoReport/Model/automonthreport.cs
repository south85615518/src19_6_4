﻿/**  版本信息模板在安装目录下，可自行修改。
* automonthreport.cs
*
* 功 能： N/A
* 类 名： automonthreport
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/5 9:26:50   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace AutoReport.Model
{
	/// <summary>
	/// automonthreport:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class automonthreport
	{
		public automonthreport()
		{}
		#region Model
		private int _xmno=0;
		private int? _day;
		private int? _hour;
		private string _jclx;
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? day
		{
			set{ _day=value;}
			get{return _day;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? hour
		{
			set{ _hour=value;}
			get{return _hour;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jclx
		{
			set{ _jclx=value;}
			get{return _jclx;}
		}
		#endregion Model

	}
}

