﻿/**  版本信息模板在安装目录下，可自行修改。
* autoweekreport.cs
*
* 功 能： N/A
* 类 名： autoweekreport
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/5 9:27:04   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
//Please add references
namespace AutoReport.DAL
{
    /// <summary>
    /// 数据访问类:autoweekreport
    /// </summary>
    public partial class autoweekreport
    {
        public static database db = new database();
        public autoweekreport()
        { }
        #region  BasicMethod


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(AutoReport.Model.autoweekreport model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("replace into autoweekreport(");
            strSql.Append("xmno,week,hour,jclx)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@week,@hour,@jclx)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@week", OdbcType.Int,11),
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@jclx", OdbcType.VarChar,200)};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.week;
            parameters[2].Value = model.hour;
            parameters[3].Value = model.jclx;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(AutoReport.Model.autoweekreport model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update autoweekreport set ");
            strSql.Append("week=@week,");
            strSql.Append("hour=@hour,");
            strSql.Append("jclx=@jclx");
            strSql.Append(" where xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@week", OdbcType.Int,11),
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@jclx", OdbcType.VarChar,200),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
            parameters[0].Value = model.week;
            parameters[1].Value = model.hour;
            parameters[2].Value = model.jclx;
            parameters[3].Value = model.xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from autoweekreport ");
            strSql.Append(" where xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, int week, int hour, out AutoReport.Model.autoweekreport model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,week,hour,jclx from autoweekreport ");
            strSql.Append(" where    xmno=@xmno     and    week = @week    and    hour=@hour   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@week", OdbcType.Int,11),
                    new OdbcParameter("@hour", OdbcType.Int,11)


                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = week;
            parameters[2].Value = hour;

            model = new AutoReport.Model.autoweekreport();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }

            return false;

        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, out AutoReport.Model.autoweekreport model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,week,hour,jclx from autoweekreport ");
            strSql.Append(" where    xmno=@xmno   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
                                         };
            parameters[0].Value = xmno;

            model = new AutoReport.Model.autoweekreport();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }

            return false;

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public AutoReport.Model.autoweekreport DataRowToModel(DataRow row)
        {
            AutoReport.Model.autoweekreport model = new AutoReport.Model.autoweekreport();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["week"] != null && row["week"].ToString() != "")
                {
                    model.week = int.Parse(row["week"].ToString());
                }
                if (row["hour"] != null && row["hour"].ToString() != "")
                {
                    model.hour = int.Parse(row["hour"].ToString());
                }
                if (row["jclx"] != null)
                {
                    model.jclx = row["jclx"].ToString();
                }
            }
            return model;
        }


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

