﻿/**  版本信息模板在安装目录下，可自行修改。
* automonthreport.cs
*
* 功 能： N/A
* 类 名： automonthreport
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/5 9:26:50   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;//Please add references
namespace AutoReport.DAL
{
	/// <summary>
	/// 数据访问类:automonthreport
	/// </summary>
	public partial class automonthreport
	{
        public static database db = new database();
		public automonthreport()
		{}
		#region  BasicMethod

	
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(int xmno)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from automonthreport");
        //    strSql.Append(" where xmno=@xmno ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@xmno", OdbcType.Int,11)			};
        //    parameters[0].Value = xmno;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(AutoReport.Model.automonthreport model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("replace into automonthreport(");
			strSql.Append("xmno,day,hour,jclx)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@day,@hour,@jclx)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@day", OdbcType.Int,11),
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@jclx", OdbcType.VarChar,200)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.day;
			parameters[2].Value = model.hour;
			parameters[3].Value = model.jclx;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters); 
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(AutoReport.Model.automonthreport model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update automonthreport set ");
			strSql.Append("day=@day,");
			strSql.Append("hour=@hour,");
			strSql.Append("jclx=@jclx");
			strSql.Append(" where xmno=@xmno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@day", OdbcType.Int,11),
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@jclx", OdbcType.VarChar,200),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.day;
			parameters[1].Value = model.hour;
			parameters[2].Value = model.jclx;
			parameters[3].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from automonthreport ");
			strSql.Append(" where xmno=@xmno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
			parameters[0].Value = xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool  GetModel(int xmno,int day,int hour,out AutoReport.Model.automonthreport model )
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select xmno,day,hour,jclx from automonthreport ");
			strSql.Append(" where     xmno=@xmno     and     day =@day    and    hour=@hour");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@day", OdbcType.Int,11),
                    new OdbcParameter("@hour", OdbcType.Int,11)
                                         };
			parameters[0].Value = xmno;
            parameters[1].Value = day;
            parameters[2].Value = hour;

			model=new AutoReport.Model.automonthreport();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model =  DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
            return false;
		}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, out AutoReport.Model.automonthreport model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,day,hour,jclx from automonthreport ");
            strSql.Append(" where     xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
                                         };
            parameters[0].Value = xmno;
            model = new AutoReport.Model.automonthreport();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }



		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public AutoReport.Model.automonthreport DataRowToModel(DataRow row)
		{
			AutoReport.Model.automonthreport model=new AutoReport.Model.automonthreport();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["day"]!=null && row["day"].ToString()!="")
				{
					model.day=int.Parse(row["day"].ToString());
				}
				if(row["hour"]!=null && row["hour"].ToString()!="")
				{
					model.hour=int.Parse(row["hour"].ToString());
				}
				if(row["jclx"]!=null)
				{
					model.jclx=row["jclx"].ToString();
				}
			}
			return model;
		}

		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

