﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chain.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chain
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/30 16:57:58   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace NGN.Model
{
	/// <summary>
	/// fixed_inclinometer_chain:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fixed_inclinometer_chain
	{
		public fixed_inclinometer_chain()
		{}
		#region Model
		private int _xmno=0;
		private string _chain_name;
		private int _port;
		private int _deviceno=0;
        private string _firstalarmname;
        private string _secondalarmname;
        private string _thirdalarmname;
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string chain_name
		{
			set{ _chain_name=value;}
			get{return _chain_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int port
		{
			set{ _port=value;}
			get{return _port;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int deviceno
		{
			set{ _deviceno=value;}
			get{return _deviceno;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string firstAlarmName
        {
            set { _firstalarmname = value; }
            get { return _firstalarmname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string secondAlarmName
        {
            set { _secondalarmname = value; }
            get { return _secondalarmname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string thirdAlarmName
        {
            set { _thirdalarmname = value; }
            get { return _thirdalarmname; }
        }
		#endregion Model

	}
}

