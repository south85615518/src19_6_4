﻿/**  版本信息模板在安装目录下，可自行修改。
* swdevice.cs
*
* 功 能： N/A
* 类 名： swdevice
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/28 15:23:55   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace NGN.Model
{
	/// <summary>
	/// swdevice:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class swdevice
	{
		public swdevice()
		{}
		#region Model
		private int _xmno;
		private int _deviceno;
		private int _initdeep;
		private string _point_name;
		private int _groundelevation;
		private string _senorno;
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int deviceno
		{
			set{ _deviceno=value;}
			get{return _deviceno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int initdeep
		{
			set{ _initdeep=value;}
			get{return _initdeep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int groundElevation
		{
			set{ _groundelevation=value;}
			get{return _groundelevation;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string senorno
		{
			set{ _senorno=value;}
			get{return _senorno;}
		}
		#endregion Model

	}
}

