﻿/**  版本信息模板在安装目录下，可自行修改。
* gtinclinometer.cs
*
* 功 能： N/A
* 类 名： gtinclinometer
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/10/22 19:06:56   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace NGN.Model
{
	/// <summary>
	/// gtinclinometer:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fixed_inclinometer_orgldata
	{
        public fixed_inclinometer_orgldata()
		{}
		#region Model
		private int _xmno=0;
		private string _holename;
		private double _deepth=0;
		private double _a_nagtive;
		private double _a_positive;
		private double _b_nagtive;
		private double _b_positive;
		private double _a_planediff;
		private double _a_avediff;
		private double _b_planediff;
		private double _b_avediff;
        private double _a_this_diff;
        private double _a_this_rap;

        public double a_this_rap
        {
            get { return _a_this_rap; }
            set { _a_this_rap = value; }
        }
        private double _b_this_rap;

        public double b_this_rap
        {
            get { return _b_this_rap; }
            set { _b_this_rap = value; }
        }
        public double a_this_diff
        {
            get { return _a_this_diff; }
            set { _a_this_diff = value; }
        }
		private double _a_ac_diff;
        private double _b_this_diff;

        public double b_this_diff
        {
            get { return _b_this_diff; }
            set { _b_this_diff = value; }
        }
		private double _a_cmp_diff;
        private double _b_ac_diff;
        private double _b_cmp_diff;
        private int _cyc;

        public int cyc
        {
            get { return _cyc; }
            set { _cyc = value; }
        }
		private DateTime _time;
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string holename
		{
			set{ _holename=value;}
			get{return _holename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double deepth
		{
			set{ _deepth=value;}
			get{return _deepth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double a_nagtive
		{
			set{ _a_nagtive=value;}
			get{return _a_nagtive;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double a_positive
		{
			set{ _a_positive=value;}
			get{return _a_positive;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double b_nagtive
		{
			set{ _b_nagtive=value;}
			get{return _b_nagtive;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double b_positive
		{
			set{ _b_positive=value;}
			get{return _b_positive;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double a_planediff
		{
			set{ _a_planediff=value;}
			get{return _a_planediff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double a_avediff
		{
			set{ _a_avediff=value;}
			get{return _a_avediff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double b_planediff
		{
			set{ _b_planediff=value;}
			get{return _b_planediff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double b_avediff
		{
			set{ _b_avediff=value;}
			get{return _b_avediff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double a_ac_diff
		{
            set { _a_ac_diff = value; }
            get { return _a_ac_diff; }
		}
		/// <summary>
		/// 
		/// </summary>
		public double a_cmp_diff
		{
			set{ _a_cmp_diff=value;}
			get{return _a_cmp_diff;}
		}
        /// <summary>
        /// 
        /// </summary>
        public double b_ac_diff
        {
            set { _b_ac_diff = value; }
            get { return _b_ac_diff; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double b_cmp_diff
        {
            set { _b_cmp_diff = value; }
            get { return _b_cmp_diff; }
        }
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		#endregion Model

	}
}

