﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_device.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_device
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/28 15:23:51   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace NGN.Model
{
	/// <summary>
	/// fixed_inclinometer_device:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fixed_inclinometer_device
	{
        public fixed_inclinometer_device()
        { }
        #region Model
        private int _xmno;
        private string _point_name;
        private int _deviceno = 0;
        private double _modulesA;
        private double _modulesB;
        private double _modulesC;
        private double _modulesD;
        private double _wheeldistance;
        private string _chain;
        private int _address = 0;
        private int _port = 0;
        private double _deep = 0;
        private string _sensorno;
        private string _firstalarmname;
        private string _secondalarmname;
        private string _thirdalarmname;
       


        /// <summary>
        /// 
        /// </summary>
        public int xmno
        {
            set { _xmno = value; }
            get { return _xmno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string point_name
        {
            set { _point_name = value; }
            get { return _point_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int deviceno
        {
            set { _deviceno = value; }
            get { return _deviceno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double modulesA
        {
            set { _modulesA = value; }
            get { return _modulesA; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double modulesB
        {
            set { _modulesB = value; }
            get { return _modulesB; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double modulesC
        {
            set { _modulesC = value; }
            get { return _modulesC; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double modulesD
        {
            set { _modulesD = value; }
            get { return _modulesD; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double Wheeldistance
        {
            set { _wheeldistance = value; }
            get { return _wheeldistance; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string chain
        {
            set { _chain = value; }
            get { return _chain; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int address
        {
            set { _address = value; }
            get { return _address; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int port
        {
            set { _port = value; }
            get { return _port; }
        }

        /// <summary>
        /// 
        /// </summary>
        public double deep
        {
            set { _deep = value; }
            get { return _deep; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string sensorno
        {
            set { _sensorno = value; }
            get { return _sensorno; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string firstAlarmName
        {
            set { _firstalarmname = value; }
            get { return _firstalarmname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string secondAlarmName
        {
            set { _secondalarmname = value; }
            get { return _secondalarmname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string thirdAlarmName
        {
            set { _thirdalarmname = value; }
            get { return _thirdalarmname; }
        }
        #endregion Model

	}
}

