﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.OleDb;
using Tool;
using System.Data;
using System.Data.Odbc;

namespace NGN.BLL
{
    public class HXYL
    {

        public database db = new database();
        public NGN.DAL.HXYL dal = new DAL.HXYL();
        public bool Addhxyl(NGN.Model.hxyl model,out string mssg)
        {

            try
            {
                if (dal.Addhxyl(model))
                {
                    mssg = string.Format("添加雨量计云数据设备编号{0}时间{1}数据成功", model.id, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加雨量计云数据设备编号{0}时间{1}数据失败", model.id, model.time);
                    return false;
                }
            }

            catch (Exception ex)
            {
                mssg = string.Format("添加雨量计云数据设备编号{0}时间{1}数据出错,错误信息:" + ex.Message, model.id, model.time);
                return false;
            }


        }

        public bool PreAcVal(NGN.Model.hxyl model, out NGN.Model.hxyl pretimemodel, out string mssg)
        {
            pretimemodel = new NGN.Model.hxyl();
            try
            {
                if (dal.PreAcVal(model, out pretimemodel))
                {
                    mssg = string.Format("获取设备号{0}{1}之前的累计雨量数据为{2}成功", model.id, model.time, pretimemodel.ac_val);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取设备号{0}{1}之前的累计雨量数据失败", model.id, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取设备号{0}{1}之前的累计雨量数据出错，错误信息:" + ex.Message, model.id, model.time);
                return false;
            }
        }

        public bool HXYLPointLoad(int xmno, out List<string> pointnamelist, out string mssg)
        {
            pointnamelist = null;
            try
            {
                if (dal.HXYLPointLoad(xmno, out pointnamelist))
                {
                    mssg = string.Format("成功获取项目编号{0}点名数量{1}", xmno, pointnamelist.Count);
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点名失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点名出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        public bool HXYLID(int xmno, string pointname, out string id, out string mssg)
        {
            id = "";
            try
            {
                if (dal.HXYLID(xmno, pointname, out id))
                {
                    mssg = string.Format("获取项目编号{0}点名为{1}的设备编号为{2}", xmno, pointname, id);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点名为{1}的设备编号失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点名为{1}的设备编号出错，错误信息：" + ex.Message, xmno, pointname);
                return false;
            }
        }
        public bool HXYLPointName(int xmno, string id, out string pointname, out string mssg)
        {
            pointname = "";
            try
            {
                if (dal.HXYLPointname(xmno, id, out pointname))
                {
                    mssg = string.Format("获取项目编号{0}设备号为{1}的点名为{2}", xmno, id, pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}设备号为{1}的点名失败", xmno, id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}设备号为{1}的点名出错，错误信息：" + ex.Message, xmno, id);
                return false;
            }
        }
        public bool HXYLMaxTime(string pointnamenostr,int xmno, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            mssg = "";
            try
            {
                if (dal.HXYLMaxTime(pointnamenostr, xmno ,out maxTime))
                {
                    mssg = string.Format("获取项目编号{0}设备号{1}雨量的最大日期为{2}",xmno ,pointnamenostr, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}设备号{1}雨量的最大日期失败", xmno ,pointnamenostr);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取设项目编号{0}设备号{1}雨量的最大日期出错，错误信息:" + ex.Message,xmno ,pointnamenostr);
                return false;
            }
        }
        public bool PointNewestDateTimeGet(int pointid, out DateTime dt, out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(pointid, out dt))
                {
                    mssg = string.Format("获取雨量点{0}数据的最新日期为{1}", pointid, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取雨量点{0}数据的最新日期失败", pointid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取雨量点{0}数据的最新日期出错，错误信息:" + ex.Message, pointid);
                return false;
            }

        }
        public bool GetModel(int pointid, DateTime dt, out   NGN.Model.hxyl model, out string mssg)
        {
            model = new Model.hxyl();
            try
            {
                if (dal.GetModel(pointid, dt, out model))
                {
                    mssg = string.Format("获取雨量点{0}测量时间{1}的记录成功", pointid, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取雨量点{0}测量时间{1}的记录失败", pointid, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取雨量点{0}测量时间{1}的记录出错,错误信息:" + ex.Message, pointid, dt);
                return false;
            }
        }
    }
}
