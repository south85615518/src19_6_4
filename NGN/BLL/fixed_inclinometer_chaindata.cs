﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chaindata.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chaindata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/30 16:58:19   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using NGN.Model;
namespace NGN.BLL
{
	/// <summary>
	/// fixed_inclinometer_chaindata
	/// </summary>
	public partial class fixed_inclinometer_chaindata
	{
		private readonly NGN.DAL.fixed_inclinometer_chaindata dal=new NGN.DAL.fixed_inclinometer_chaindata();
		public fixed_inclinometer_chaindata()
		{}
		#region  BasicMethod



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_chaindata model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}测斜链名为{1}测量时间{2}位移值{3}的数据记录录入成功", model.xmno, model.point_name, model.time, model.disp);
                    return true;
                }
                else {
                    mssg = string.Format("项目编号{0}测斜链名为{1}测量时间{2}位移值{3}的数据记录录入失败", model.xmno, model.point_name, model.time, model.disp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}测斜链名为{1}测量时间{2}位移值{3}的数据记录录入出错,错误信息:"+ex.Message, model.xmno, model.point_name, model.time, model.disp);
                return false;
            }
        }
        ///// <summary>
        ///// 更新一条数据
        ///// </summary>
        //public bool Update(NGN.Model.fixed_inclinometer_chaindata model)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("update fixed_inclinometer_chaindata set ");
        //    strSql.Append("disp=@disp");
        //    strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@disp", OdbcType.Double),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@point_name", OdbcType.VarChar,100),
        //            new OdbcParameter("@time", OdbcType.DateTime)};
        //    parameters[0].Value = model.disp;
        //    parameters[1].Value = model.xmno;
        //    parameters[2].Value = model.point_name;
        //    parameters[3].Value = model.time;

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// 删除一条数据
        /// </summary>
        //public bool Delete(int xmno, string point_name, DateTime time)
        //{

        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("delete from fixed_inclinometer_chaindata ");
        //    strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@point_name", OdbcType.VarChar,100),
        //            new OdbcParameter("@time", OdbcType.DateTime)			};
        //    parameters[0].Value = xmno;
        //    parameters[1].Value = point_name;
        //    parameters[2].Value = time;

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool  GetModel(int xmno, string point_name, DateTime time,out NGN.Model.fixed_inclinometer_chaindata model,out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, point_name, time, out model))
                {
                    mssg = string.Format("获取项目编号{0}测斜链{1}测量时间{2}的位移为{3}", model.xmno, model.point_name, model.time, model.disp);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目编号{0}测斜链{1}测量时间{2}的数据记录失败", model.xmno, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜链{1}测量时间{2}的数据记录出错,错误信息:"+ex.Message, model.xmno, model.point_name, model.time);
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public NGN.Model.fixed_inclinometer_chaindata DataRowToModel(DataRow row)
        {
            NGN.Model.fixed_inclinometer_chaindata model = new NGN.Model.fixed_inclinometer_chaindata();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                //model.disp=row["disp"].ToString();
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
            }
            return model;
        }

        public bool MaxTime(int xmno, string chainname, out DateTime maxTime,out string mssg)
        {
            maxTime = new DateTime();
            mssg = "";
            try
            {
                if (dal.MaxTime(xmno, chainname, out maxTime))
                {
                    mssg = string.Format("获取项目编号{0}测斜链名{1}的最新采集时间为{2}", xmno, chainname, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测斜链名{1}的最新采集时间失败", xmno, chainname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜链名{1}的最新采集时间出错,错误信息:"+ex.Message, xmno, chainname);
                return false;
            }
        }

        /// <summary>
        /// 删除临时表数据
        /// </summary>
        public bool Delete_tmp(int xmno,out int cont,out string mssg)
        {
            cont = 0;
            mssg = "";
            try
            {
                if (dal.Delete_tmp(xmno,out cont))
                {
                    mssg = string.Format("删除项目编号{0}固定测斜链临时表的数据{1}条成功",xmno,cont);
                    return true;
                }
                else {
                    mssg = string.Format("删除项目编号{0}固定测斜链临时表的数据失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}固定测斜链临时表的数据出错,错误信息："+ex.Message);
                return false;
            }
         
        }

        /// <summary>
        /// 得到一个测斜链数据对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_chaindata> lt, out string mssg)
        {
            lt = null;
            mssg = "";
            try
            {
                if (dal.GetModelList(xmno, out lt))
                {
                    mssg = string.Format("成功获取项目编号{0}测斜链采集数据对象{1}条", xmno, lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("成功获取项目编号{0}测斜链采集数据对象列表失败", xmno, lt.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("成功获取项目编号{0}测斜链采集数据对象列表出错,错误信息:" + ex.Message, xmno, lt.Count);
                return false;
            }
        }


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

