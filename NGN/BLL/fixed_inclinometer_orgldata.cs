﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_orgldata.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_orgldata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 8:51:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using NGN.Model;
using Tool;
namespace NGN.BLL
{
	/// <summary>
	/// fixed_inclinometer_orgldata
	/// </summary>
	public partial class fixed_inclinometer_orgldata
	{
		private readonly NGN.DAL.fixed_inclinometer_orgldata dal=new NGN.DAL.fixed_inclinometer_orgldata();
		public fixed_inclinometer_orgldata()
		{}
		#region  BasicMethod
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_orgldata model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}测斜孔名{1}深度{2}采集时间{3}数据记录录入成功", model.xmno, model.holename, model.deepth, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}测斜孔名{1}深度{2}采集时间{3}数据记录录入失败", model.xmno, model.holename, model.deepth, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}测斜孔名{1}深度{2}采集时间{3}数据记录录入出错,错误信息:" + ex.Message, model.xmno, model.holename, model.deepth, model.time);
                return false;
            }
        }



        ///// <summary>
        ///// 更新一条数据
        ///// </summary>
        //public bool Update(NGN.Model.fixed_inclinometer_orgldata model)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("update fixed_inclinometer_orgldata set ");
        //    strSql.Append("f=@f,");
        //    strSql.Append("disp=@disp");
        //    strSql.Append(" where xmno=@xmno and holename=@holename and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@f", OdbcType.Double),
        //            new OdbcParameter("@disp", OdbcType.Double),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@holename", OdbcType.VarChar,100),
        //            new OdbcParameter("@time", OdbcType.DateTime)};
        //    parameters[0].Value = model.f;
        //    parameters[1].Value = model.disp;
        //    parameters[2].Value = model.xmno;
        //    parameters[3].Value = model.holename;
        //    parameters[4].Value = model.time;

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(NGN.Model.fixed_inclinometer_orgldata model, out string mssg)
        {

            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("删除项目编号{0}点名{1}深度{2}测量时间{3}数据成功", model.xmno, model.holename, model.deepth, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}点名{1}深度{2}测量时间{3}数据失败", model.xmno, model.holename, model.deepth, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}点名{1}深度{2}测量时间{3}数据出错,错误信息:" + ex.Message, model.xmno, model.holename, model.deepth, model.time);
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string point_name, double deepth, DateTime time, out NGN.Model.fixed_inclinometer_orgldata model, out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(xmno, point_name, deepth, time, out model))
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象成功", xmno, point_name, model.deepth, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象失败", xmno, point_name, model.deepth, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象出错,错误信息:" + ex.Message, xmno, point_name, model.deepth, time);
                return false;
            }
        }

        /// <summary>
        /// 删除临时表数据
        /// </summary>
        public bool Delete_tmp(int xmno, out int cont,out string mssg)
        {
            cont = 0;
            mssg = "";
            try
            {
                if (dal.Delete_tmp(xmno, out cont))
                {
                    mssg = string.Format("删除项目编号{0}固定测斜段临时表数据{1}条成功", xmno, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}固定测斜段临时表数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}固定测斜段临时表数据出错,错误信息:"+ex.Message, xmno);
                return false;
            }
            
        }


        /// <summary>
        /// 得到一个测斜段数据对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt,out string mssg)
        {
            lt = null;
            mssg = "";
            try
            {
                if (dal.GetModelList(xmno, out lt))
                {
                    mssg = string.Format("成功获取项目编号{0}测斜采集数据对象{1}条", xmno, lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("成功获取项目编号{0}测斜采集数据对象列表失败", xmno, lt.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("成功获取项目编号{0}测斜采集数据对象列表出错,错误信息:" + ex.Message, xmno, lt.Count);
                return false;
            }
        }



        public bool PointMaxDateTimeGet(int xmno,  out DateTime dt,out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointMaxDateTimeGet(xmno, out dt))
                {
                    mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间为{1}", xmno, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间失败", xmno);
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间出错，错误信息:"+ex.Message, xmno);
                return false;
            }

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetLastTimeModel(int xmno, string point_name, double deepth, DateTime time, out NGN.Model.fixed_inclinometer_orgldata model, out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetLastTimeModel(xmno, point_name, deepth, time, out model))
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象成功", xmno, point_name, model.deepth, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象失败", xmno, point_name, model.deepth, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象出错,错误信息:" + ex.Message, xmno, point_name, model.deepth, time);
                return false;
            }
        }


        public bool ResultdataTableLoad(int startPageIndex, int pageSize, int xmno, string holename, string sord, int startcyc, int endcyc, out DataTable dt,out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.ResultdataTableLoad(startPageIndex, pageSize, xmno, holename, sord, startcyc, endcyc, out  dt))
                {
                    mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的数据成功", xmno, holename, startcyc, endcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的数据失败", xmno, holename, startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的数据出错,错误信息:" + ex.Message, xmno, holename, startcyc, endcyc);
                return false;
            }

        }
        public bool ResultTableRowsCount(string xmno, string holename, string sord, int startcyc, int endcyc, out string totalCont,out string mssg)
        {
            mssg = "";
            totalCont = null;
            try
            {
                if (dal.ResultTableRowsCount(xmno, holename, sord, startcyc, endcyc, out  totalCont))
                {
                    mssg = string.Format("获取到项目编号{0}孔{1}从{2}到{3}的数据记录数", xmno, holename, startcyc, endcyc, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的数据记录数失败", xmno, holename, startcyc, endcyc, totalCont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的出错,错误信息:" + ex.Message, xmno, holename, startcyc, endcyc);
                return false;
            }
        }

        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(int xmno, string holename, DateTime dt, out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.IsInsertData(xmno,holename ,dt))
                {
                    mssg = string.Format("经查库发现项目编号{0}点{1}{2}采集的{3}数据是接段数据", xmno,holename ,dt, "测斜");
                    return true;
                }
                else
                {
                    mssg = string.Format("经查库发现项目编号{0}点{1}{2}采集的{3}数据是最新数据", xmno,holename ,dt, "测斜");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点{1}{2}采集的{3}数据的性质出错，错误信息：" + ex.Message, xmno,holename,dt,"测斜");
                return false;
            }
        }
        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(int xmno, string holename,DateTime dt,  out int cyc, out string mssg)
        {
            mssg = "";
            cyc = -1;
            try
            {
                if (dal.InsertDataCycGet(xmno,holename ,dt,  out cyc))
                {
                    mssg = string.Format("获取项目编号{0}点{1}{2}的{3}段接周期为{4}", xmno,holename ,dt, "测斜", cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}的{2}段接周期失败", xmno,holename ,dt, "测斜");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}的{2}段接周期出错,错误信息:" + ex.Message, xmno,holename ,dt, "测斜");
                return false;
            }
        }
        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool InsertCycStep(int xmno,string holename ,int startcyc,  out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.InsertCycStep(xmno, holename ,startcyc))
                {
                    mssg = string.Format("项目{0}{1}点名{2}周期{3}后的所有周期后移成功", xmno,holename ,"测斜", startcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}{1}点名{2}周期{3}后的所有周期后移失败", xmno,holename ,"测斜", startcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}{1}点名{2}周期{3}后的所有周期后移出错,错误信息:" + ex.Message, xmno, holename, "测斜", startcyc);
                return false;
            }
        }

        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(int xmno,string holename ,int cyc,  out string mssg)
        {
            try
            {
                if (dal.IsInsertCycExist(xmno,holename ,cyc))
                {
                    mssg = string.Format("项目{0}{1}点名{2}已经存在周期{3}", xmno, "测斜",holename, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}{1}点名{2}不存在周期{3}", xmno, "测斜",holename, cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}点名{2}周期{3}的存在与否出错,错误信息:" + ex.Message, xmno,"测斜",holename,cyc);
                return false;
            }
        }

        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        //public bool InsertCycStep(int xmno,string holename ,int startcyc,  out string mssg)
        //{
        //    mssg = "";
        //    try
        //    {
        //        if (dal.InsertCycStep(xmno, holename, startcyc))
        //        {
        //            mssg = string.Format("项目{0}孔名{1}{2}周期{3}后的所有周期后移成功", xmno, holename, "测斜", startcyc);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("项目{0}孔名{1}周期{2}后的所有周期后移失败", xmno, holename, "测斜", startcyc);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("项目{0}孔名{1}{2}周期{3}后的所有周期后移出错,错误信息:" + ex.Message, xmno, holename, "测斜", startcyc);
        //        return false;
        //    }
        //}






        ///// <summary>
        ///// 得到一个测斜段对象实体列表
        ///// </summary>
        //public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt,out string mssg)
        //{
        //    mssg = "";
        //    lt = null;
        //    try {
        //        if(dal.GetModelList(xmno,out lt))
        //        {
        //            mssg = string.Format("获取临时表项目编号{0}的数据记录数{1}条",xmno,lt.Count);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("获取临时表项目编号{0}的数据记录数{1}",xmno,lt.Count);
        //            return false;
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //         mssg = string.Format("获取临时表项目编号{0}的数据记录数出错,错误信息:"+ex.Message,xmno);
        //            return false;
        //    }
        //}




        //public bool PointMaxDateTimeGet(int xmno, out DateTime dt)
        //{
        //    StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
        //    strSql.Append("select max(time) from gtinclinometer ");
        //    strSql.Append("   where     xmno=@xmno ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@xmno", OdbcType.Int,11)
        //                };
        //    parameters[0].Value = xmno;
        //    object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
        //    if (obj == null) { dt = new DateTime(); return false; }
        //    dt = Convert.ToDateTime(obj); return true;

        //}


        public bool MaxTime(int xmno, string holename, out DateTime maxtime,out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno, holename, out maxtime))
                {
                    mssg = string.Format("获取项目{0}测量数据点名{2}的最大日期{1}成功", xmno, maxtime, holename);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}测量数据点名{1}的最大日期失败", xmno, holename);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}测量数据点名{1}的最大日期出错，错误信息:" + ex.Message, xmno, holename);
                return false;
            }
        }
        public bool MinTime(int xmno, string holename, out DateTime minTime,out string mssg)
        {
            minTime = new DateTime();
            try
            {
                if (dal.MinTime(xmno, holename, out minTime))
                {
                    mssg = string.Format("获取项目{0}测量数据点名{2}的最小日期{1}成功", xmno, minTime, holename);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}测量数据点名{1}的最小日期失败", xmno, holename);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}测量数据点名{1}的最小日期出错，错误信息:" + ex.Message, xmno, holename);
                return false;
            }
        }

        public bool PointNameDateTimeListGet(int xmno, string pointname, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            ExceptionLog.ExceptionWrite("现在执行测量库周期加载");
            try
            {
                if (dal.PointNameDateTimeListGet(xmno, pointname, out ls))
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的测量数据时间列表记录数{3}成功", xmno, pointname, "", ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的测量数据时间列表失败", xmno, pointname, "");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}点{2}的测量数据时间列表出错,错误信息:" + ex.Message, xmno, pointname, "");
                return false;
            }


        }


        //根据项目名称获取端点周期
        public bool ExtremelyCycGet(int xmno, string holename, string DateFunction, out string mssg, out string ExtremelyCyc)
        {
            ExtremelyCyc = null;
            try
            {

                if (dal.ExtremelyCycGet(xmno,holename ,DateFunction, out ExtremelyCyc))
                {
                    mssg = "端点周期获取成功";
                    return true;
                }
                else
                {
                    mssg = "端点周期获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "端点周期获取出错，错误信息：" + ex.Message;
                return false;
            }

        }

        /// <summary>
        /// 判断记录是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool IsInclinometerDataExist(int xmno, string holename, DateTime dt,  out string mssg)
        {
            try
            {
                if (dal.IsInclinometerDataExist(xmno, holename, dt))
                {
                    mssg = string.Format("项目编号{0}{1}点名{2}采集时间{3}的记录已经存在", xmno, "测斜", holename, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}{1}不存在点名{2}采集时间{3}的记录", xmno, "测斜", holename, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}点名{2}采集时间{3}的记录出错,错误信息:" + ex.Message, xmno, "测斜", holename, dt);
                return false;
            }

        }

        public bool ReportDataView(List<string> cyclist, int startPageIndex, int pageSize, int xmno, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.ReportDataView(cyclist, startPageIndex, pageSize, xmno, sord, out  dt))
                {
                    mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表成功", xmno,"测斜",string.Join("、", cyclist));
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表失败", xmno, "测斜", string.Join("、", cyclist));
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表出错，错误信息:" + ex.Message, xmno, "测斜", string.Join("、", cyclist));
                return false;
            }

        }



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

