﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chain.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chain
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/30 16:57:58   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using NGN.Model;
namespace NGN.BLL
{
	/// <summary>
	/// fixed_inclinometer_chain
	/// </summary>
	public partial class fixed_inclinometer_chain
	{
		private readonly NGN.DAL.fixed_inclinometer_chain dal=new NGN.DAL.fixed_inclinometer_chain();
		public fixed_inclinometer_chain()
		{}
		#region  BasicMethod

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(NGN.Model.fixed_inclinometer_chain model,out string mssg)
		{
            try
            {

                if (dal.Add(model))
                {
                    mssg = string.Format("新增项目编号{0}端口号为{1}设备号为{2}链名为{3}的测斜链成功", model.xmno, model.port, model.deviceno, model.chain_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("新增项目编号{0}端口号为{1}设备号为{2}链名为{3}的测斜链失败", model.xmno, model.port, model.deviceno, model.chain_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("新增项目编号{0}端口号为{1}设备号为{2}链名为{3}的测斜链出错,错误信息:"+ex.Message, model.xmno, model.port, model.deviceno, model.chain_name);
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        //public bool Update(NGN.Model.fixed_inclinometer_chain model)
        //{
        //    return dal.Update(model);
        //}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(int xmno, string chain_name,out string mssg)
        {
            try
            {
                if (dal.Delete(xmno, chain_name))
                {
                    mssg = string.Format("删除项目编号{0}链名{1}的测斜链成功", xmno, chain_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}链名{1}的测斜链失败", xmno, chain_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}链名{1}的测斜链出错,错误信息:"+ex.Message, xmno, chain_name);
                return false;
            }
        }

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public NGN.Model.fixed_inclinometer_chain GetModel(int xmno, string chain_name, int port, int address)
        //{

        //    return dal.GetModel(xmno, chain_name, port, address);
        //}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, int deviceno, int port, out NGN.Model.fixed_inclinometer_chain model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, port, deviceno, out model))
                {
                    mssg = string.Format("获取项目编号{0}端口{1}设备号{2}的测斜链成功", xmno, port, deviceno);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目编号{0}端口{1}设备号{2}的测斜链失败", xmno, port, deviceno);
                    return false;
                }

                
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}端口{1}设备号{2}的测斜链出错,错误信息:"+ex.Message, xmno, port, deviceno);
                return false;
            }
            
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string point_name, out NGN.Model.fixed_inclinometer_chain model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, point_name, out model))
                {
                    mssg = string.Format("获取项目编号{0}链名为{1}的测斜链成功", xmno, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}链名为{1}的测斜链失败", xmno, point_name);
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}链名为{1}的测斜链出错,错误信息:" + ex.Message, xmno, point_name);
                return false;
            }

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public NGN.Model.fixed_inclinometer_chain DataRowToModel(DataRow row)
        {
            NGN.Model.fixed_inclinometer_chain model = new NGN.Model.fixed_inclinometer_chain();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["chain_name"] != null)
                {
                    model.chain_name = row["chain_name"].ToString();
                }
                if (row["port"] != null && row["port"].ToString() != "")
                {
                    model.port = int.Parse(row["port"].ToString());
                }
                if (row["deviceno"] != null && row["deviceno"].ToString() != "")
                {
                    model.deviceno = int.Parse(row["deviceno"].ToString());
                }
            }
            return model;
        }
        /// <summary>
        /// 加载测斜链表
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt,out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.PointTableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的测斜链记录共{1}条", xmno, dt.Rows.Count);
                    return true;
                }
                else {
                    mssg = string.Format("成功获取项目编号{0}的测斜链记录失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("成功获取项目编号{0}的测斜链记录出错,错误信息:"+ex.Message, xmno);
                return false;
            }

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateAlarm(NGN.Model.fixed_inclinometer_chain model,out string mssg)
        {
            try
            {
                if (dal.UpdateAlarm(model))
                {
                    mssg = string.Format("更新项目编号{0}测斜链{1}的预警名成功",model.xmno,model.chain_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}测斜链{1}的预警名失败", model.xmno, model.chain_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}测斜链{1}的预警名出错,错误信息:"+ex.Message, model.xmno, model.chain_name);
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdateAlarm(string pointNameStr, NGN.Model.fixed_inclinometer_chain model,out string mssg)
        {
            try
            {
                if (dal.MultiUpdateAlarm(pointNameStr,model))
                {
                    mssg = string.Format("批量更新项目编号{0}测斜链{1}的预警名成功", model.xmno, pointNameStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("批量更新项目编号{0}测斜链{1}的预警名失败", model.xmno, pointNameStr);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("批量更新项目编号{0}测斜链{1}的预警名出错,错误信息:" + ex.Message, model.xmno, pointNameStr);
                return false;
            }
        }

        public bool ChainNameLoad(int xmno, out List<string> chainnamelsit,out string mssg)
        {
            chainnamelsit = null;
            mssg = "";
            try
            {
                if (dal.ChainNameLoad(xmno, out chainnamelsit))
                {
                    mssg = string.Format("获取到项目编号{0}的测斜链名称{1}个", xmno, chainnamelsit.Count);
                    return true;
                }
                else {
                    mssg = string.Format("获取到项目编号{0}的测斜链名称失败", xmno, chainnamelsit.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}的测斜链名称出错,错误信息:"+ex.Message, xmno);
                return false;
            }
        }

        /// <summary>
        /// 清空关联的预警参数
        /// </summary>
        public bool DeleteAlarmValue(int xmno, string alarmname,out string mssg)
        {
            try
            {
                if (dal.DeleteAlarmValue(xmno, alarmname))
                {
                    mssg = string.Format("更新项目编号{0}测斜链中关联了预警名称{1}的测斜链的预警参数名称成功", xmno, alarmname);
                    return true;
                }
                else {
                    mssg = string.Format("更新项目编号{0}测斜链中关联了预警名称{1}的测斜链的预警参数名称失败", xmno, alarmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}测斜链中关联了预警名称{1}的测斜链的预警参数名称出错,错误信息:"+ex.Message, xmno, alarmname);
                return false;
            }
        }


        /// <summary>
        /// 加载测斜链数量
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableCountLoad(int xmno, out string cont,out string mssg)
        {
            mssg = "";
            cont = "";
            try
            {
                if (dal.PointTableCountLoad(xmno, out cont))
                {
                    mssg = string.Format("获取到项目编号{0}的孔位数{1}", xmno, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的孔位数失败", xmno);
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的孔位数出错,错误信息:"+ex.Message, xmno);
                return false;
            }
        }
		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

