﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NGN.BLL
{
    public partial class fixed_inclinometer_orgldata
    {
        public bool CgMaxTime(int xmno, string point_name, out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.CgMaxTime(xmno,  point_name, out maxtime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据点名{3}的最大日期{1}成功", xmno, maxtime, "", point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最大日期失败", xmno, "", point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最大日期出错，错误信息:" + ex.Message, xmno, "", point_name);
                return false;
            }
        }
        public bool CgMinTime(int xmno, string point_name, out DateTime Mintime, out string mssg)
        {
            Mintime = new DateTime();
            try
            {
                if (dal.CgMinTime(xmno,  point_name, out Mintime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据点名{3}的最小日期{1}成功", xmno, Mintime, "", point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最小日期失败", xmno, "", point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最小日期出错，错误信息:" + ex.Message, xmno, "", point_name);
                return false;
            }
        }

        public bool CgPointNameCYCDateTimeListGet(int xmno, string pointname,  out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.CgPointNameCYCDateTimeListGet(xmno, pointname, out ls))
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表记录数{3}成功", xmno, pointname, "", ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表失败", xmno, pointname, "");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表出错,错误信息:" + ex.Message, xmno, pointname, "");
                return false;
            }


        }

        public bool AddCgData(int xmno, string point_name, int importcyc, int cyc, out string mssg)
        {
            mssg = "";
            int rows = 0;
            try
            {
                if (dal.AddCgData(xmno, point_name, importcyc, cyc, out rows))
                {
                    mssg = string.Format("添加编辑库项目{0}{1}点名{2}第{3}为成果库第{4}周期的数据{5}条成功", xmno, "", point_name, cyc, importcyc, rows);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加编辑库项目{0}{1}点名{2}第{3}为成果库第{4}周期失败", xmno, "", point_name, cyc, importcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加编辑库项目{0}{1}点名{2}第{3}为成果库第{4}周期的数据出错,错误信息:" + ex.Message, xmno, "", point_name, cyc, importcyc);
                return false;
            }
        }
        /// <summary>
        /// 删除成果数据
        /// </summary>
        public bool DeleteCg(int xmno, string point_name,  int startcyc, int endcyc,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.DeleteCg(xmno, point_name, startcyc, endcyc))
                {
                    mssg = string.Format("删除项目{0}{1}点名{2}从{3}-{4}的数据成功", xmno, "", point_name, startcyc, endcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}{1}点名{2}从{3}-{4}的数据失败", xmno, "", point_name, startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}{1}点名{2}从{3}-{4}的数据出错,错误信息:" + ex.Message, xmno, "", point_name, startcyc, endcyc);
                return false;
            }
        }
        public bool DeleteCgTmp(int xmno, out string mssg)
        {
            try
            {
                if (dal.DeleteCgTmp(xmno))
                {
                    mssg = string.Format("删除项目{0}{3}成果临时表的数据成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}{3}成果临时表的数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}{3}成果临时表的数据出错,错误信息：" + ex.Message, xmno);
                return false;
            }
            
        }
        public bool CgResultdataTableLoad(int startPageIndex, int pageSize, int xmno, string holename, string sord, int startcyc, int endcyc, out DataTable dt, out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.CgResultdataTableLoad(startPageIndex, pageSize, xmno, holename, sord, startcyc, endcyc, out  dt))
                {
                    mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的数据成功", xmno, holename, startcyc, endcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的数据失败", xmno, holename, startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的数据出错,错误信息:" + ex.Message, xmno, holename, startcyc, endcyc);
                return false;
            }

        }
        public bool CgResultTableRowsCount(int xmno, string holename, string sord, int startcyc, int endcyc, out string totalCont, out string mssg)
        {
            mssg = "";
            totalCont = null;
            try
            {
                if (dal.CgResultTableRowsCount(xmno, holename, sord, startcyc, endcyc, out  totalCont))
                {
                    mssg = string.Format("获取到项目编号{0}孔{1}从{2}到{3}的数据记录数", xmno, holename, startcyc, endcyc, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的数据记录数失败", xmno, holename, startcyc, endcyc, totalCont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}孔{1}从{2}到{3}的出错,错误信息:" + ex.Message, xmno, holename, startcyc, endcyc);
                return false;
            }
        }

        /// <summary>
        /// 得到一个测斜段对象实体列表
        /// </summary>
        public bool GetCgModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt, out string mssg)
        {
            mssg = "";
            lt = null;
            try
            {
                if (dal.GetCgModelList(xmno, out lt))
                {
                    mssg = string.Format("获取临时表项目编号{0}的数据记录数{1}条", xmno, lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取临时表项目编号{0}的数据记录数{1}", xmno, lt.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取临时表项目编号{0}的数据记录数出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        

    }
}
