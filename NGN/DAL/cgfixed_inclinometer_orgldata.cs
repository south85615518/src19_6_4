﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_orgldata.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_orgldata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 8:51:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
using Tool;
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:fixed_inclinometer_orgldata
	/// </summary>
	public partial class fixed_inclinometer_orgldata
	{
       
		#region  BasicMethod


        public bool CgPointNameCYCDateTimeListGet(int xmno, string holename, out List<string> ls)
        {

            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmno);
            string holenamesearchstr = holename == "全部" ? string.Format(" holename  in  (select distinct(pointname ) from {0}.gtpointalarmvalue where xmno = "+xmno+")", surveyconn.Database) : " holename = '" + holename + "' ";
            string sql = "select  distinct(cyc),time  from gtinclinometer  where  xmno='" + xmno + "'   and  " + holenamesearchstr + "      order by cyc asc";


            //strSql.AppendFormat("select distinct(cyc),time  from  gtinclinometer    where         xmno='{0}'    and        {1}     order by time  asc  ", xmno,  holename == "" || holename == null || (holename.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  holename=  '{0}'", holename));
            ls = new List<string>();
            //OdbcParameter[] parameters = {
            //        new OdbcParameter("@xmno", OdbcType.VarChar,200),
            //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
            //                };
            //parameters[0].Value = xmno;
            //parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            ExceptionLog.ExceptionWrite(sql);
            string str = strSql.ToString();
            DataSet ds = OdbcSQLHelper.Query(sql);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("{0}[{1}]", ds.Tables[0].Rows[i].ItemArray[0], ds.Tables[0].Rows[i].ItemArray[1]));
                i++;
            }
            return true;

        }
        public bool CgMaxTime(int xmno, string holename, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtinclinometer  where   xmno = @xmno   and    holename=@holename  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int),
                new OdbcParameter("@holename",OdbcType.VarChar,200)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = holename;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool CgMinTime(int xmno, string holename, out DateTime minTime)
        {
            minTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select min(time) from gtinclinometer  where   xmno = @xmno   and    holename=@holename  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int),
                new OdbcParameter("@holename",OdbcType.VarChar,200)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = holename;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            minTime = Convert.ToDateTime(obj);
            return true;
        }



        public bool AddCgData(int xmno, string point_name, int importcyc, int cyc, out int rows)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection Conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            OdbcConnection CgConn = db.GetStanderCgConn(xmno);
            strSql.AppendFormat("REPLACE   into   {0}.gtinclinometer (xmno,holename,deepth,a_ac_diff,b_ac_diff,a_this_rap,b_this_rap,cyc,time)  select xmno,holename,deepth,a_ac_diff,b_ac_diff,a_this_rap,b_this_rap,"+importcyc+" as  cyc,time  from {1}.gtinclinometer_data   where   xmno=@xmno      and  cyc ="+cyc+"   ", CgConn.Database, Conn.Database);
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.VarChar,100)
                        
                                         };
            parameters[0].Value = xmno;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除成果数据
        /// </summary>
        public bool DeleteCg(int xmno, string holename, int startcyc, int endcyc)
        {
            string searchstr = holename == "" || holename == null || (holename.IndexOf("全部") != -1) ? "  1=1  " : string.Format("holename='{0}'", holename);
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from  gtinclinometer  where   xmno=@xmno      and    {0}    and     cyc    between    @startcyc      and    @endcyc  ", searchstr);
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.VarChar,100),
                                             new OdbcParameter("@startcyc", OdbcType.Int),
                                             new OdbcParameter("@endcyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = startcyc;
            parameters[2].Value = endcyc;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteCgTmp(int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            strSql.Append("delete  from  gtinclinometer_tmp ");
            strSql.Append(" where xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CgResultdataTableLoad(int startPageIndex, int pageSize, int xmno, string holename, string sord, int startcyc, int endcyc, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = holename == "" || holename == null || (holename.IndexOf("全部") != -1) ? "  1=1  " : string.Format("holename='{0}'", holename);
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select * from gtinclinometer  where   xmno='" + xmno + "'     and   cyc>='" + startcyc + "'      and       cyc <='" + endcyc + "'       and    {0}      order by {1}  asc  limit    " + (startPageIndex - 1) * pageSize + "," + pageSize * startPageIndex + "", searchstr, sord);

            ExceptionLog.ExceptionWrite(strSql.ToString());
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool CgResultTableRowsCount(int xmno, string holename, string sord, int startcyc, int endcyc, out string totalCont)
        {
            string searchstr = holename == null || holename == "" || (holename.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", holename);
            string sql = string.Format("select count(1) from  gtinclinometer where xmno='{0}'   and  time>='{2}' and  time <= '{3}'     and   {1}", xmno, searchstr, startcyc, endcyc);
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetCgModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt)
        {
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select *  from gtinclinometer_tmp  ");
            strSql.Append(" where    xmno=@xmno   order by  datatype ,point_name   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmno;
            lt = new List<NGN.Model.fixed_inclinometer_orgldata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

      


        ///// <summary>
        ///// 得到一个测斜段对象实体列表
        ///// </summary>
        //public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt)
        //{
        //    OdbcConnection conn = db.GetStanderConn(xmno);
        //    OdbcSQLHelper.Conn = conn;
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select xmno,holename,chain_name,disp,deep,time,f  from fixed_inclinometer_orgldata_tmp ");
        //    strSql.Append(" where    xmno=@xmno   ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@xmno", OdbcType.VarChar,120)
        //                    };
        //    parameters[0].Value = xmno;

        //    lt = new List<NGN.Model.fixed_inclinometer_orgldata>();
        //    DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
        //    int i = 0;
        //    while (i < ds.Tables[0].Rows.Count)
        //    {
        //        lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
        //        i++;
        //    }
        //    return true;
        //}



	

        






		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

