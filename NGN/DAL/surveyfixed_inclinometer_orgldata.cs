﻿/**  版本信息模板在安装目录下，可自行修改。
* gtinclinometer.cs
*
* 功 能： N/A
* 类 名： gtinclinometer
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 #SensorName# Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:gtinclinometer
	/// </summary>
    public partial class fixed_inclinometer_orgldata
	{
        public bool AddData(int xmno, string holename, int startcyc, int endcyc)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("REPLACE into gtinclinometer_data select * from gtinclinometer   where   xmno=@xmno      and  holename=@holename  and  cyc  between   @startcyc   and   @endcyc   ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.Int),
                                             new OdbcParameter("@holename", OdbcType.VarChar,100),
                                             new OdbcParameter("@startcyc", OdbcType.Int), 
                                             new OdbcParameter("@endcyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = holename;
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public bool SurveySurfaceFaceTimeDataAdd(int xmno, string pointname, DateTime time, DateTime importtime)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
        //    strSql.AppendFormat(@"insert into gtinclinometer_data (xmno,holename,first_oregion_scalarvalue,time) select xmno,holename,datatype,senorno,first_oregion_scalarvalue,time from  gtinclinometer_data where xmno = '" + xmno + "'  and  datatype='{1}'  and   holename   =   '" + pointname + "'   and     time='{0}'", importtime);

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        //public bool UpdataSurveyData(int xmno, string pointname, DateTime time,string vSet_name, string vLink_name, DateTime srcdatetime)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
        //    strSql.AppendFormat("update gtinclinometer_data set {1} = {1}+({0}-(select {0} from gtinclinometer where xmno=gtinclinometer_data.xmno and holename= gtinclinometer_data.holename and time =     '{5}')), {0}=(select   {0}   from gtinclinometer where xmno=gtinclinometer_data.xmno and holename= gtinclinometer_data.holename and time =    '{5}')  where       xmno= '{2}'     and  holename  =   '{3}'   and  time =   '{4}'   ", vSet_name, vLink_name, xmno, pointname, time, srcdatetime);
        //    string str = strSql.ToString();
        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public bool UpdataInclinometerData(NGN.Model.fixed_inclinometer_orgldata model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.AppendFormat(@"update gtinclinometer_data set a_this_rap =a_this_rap - a_ac_diff +{0},
                a_ac_diff = {0}  where    xmno= '{1}'  and holename = '{2}'     and  cyc =   {3} and deepth = {4}   ", model.a_ac_diff, model.xmno, model.holename, model.cyc,model.deepth);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdataInclinometerDataNextCYCThis(NGN.Model.fixed_inclinometer_orgldata model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.AppendFormat(@"update gtinclinometer_data set a_this_diff = a_ac_diff - {0}
                where    xmno= '{1}'  and  holename='{2}'    and  time >= DATE_ADD('" + model.time + "',INTERVAL -2 DAY)   and  time <= DATE_ADD('" + model.time + "',INTERVAL -1 DAY)  and  deepth = {4}  ", model.a_ac_diff, model.xmno, model.holename, model.cyc + 1, model.deepth);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public bool SurveyResultTableRowsCount(int xmno, string pointname,  int startcyc, int endcyc, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  holename = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from  gtinclinometer_data where xmno='{0}'   and  cyc>='{2}'  and  cyc <= '{3}'     and   {1}", xmno, searchstr,  startcyc, endcyc);
            OdbcConnection conn = db.GetStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteSurveyData(int xmno, string holename, int startcyc, int endcyc)
        {
            string searchstr = holename == "" || holename == null || (holename.IndexOf("全部") != -1) ? "  1=1  " : string.Format("holename='{0}'", holename);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from gtinclinometer_data ");
            strSql.AppendFormat(" where    xmno=@xmno    and    {0}        and cyc  between     @startcyc    and     @endcyc ", searchstr);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,100),
					new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = startcyc;
            parameters[2].Value = endcyc;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //public bool PointNameSurveyCYCDateTimeListGet(int xmno, string pointname, out List<string> ls)
        //{

        //    OdbcConnection conn = db.GetSurveyStanderConn(xmno);
        //    OdbcSQLHelper.Conn = conn;
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.AppendFormat("select distinct(cyc),time  from  gtinclinometer_data    where         xmno='{0}'    and     {1}     order by time  asc  ", xmno, pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  holename=  '{0}'", pointname));
        //    ls = new List<string>();
        //    //OdbcParameter[] parameters = {
        //    //        new OdbcParameter("@xmno", OdbcType.VarChar,200),
        //    //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
        //    //                };
        //    //parameters[0].Value = xmno;
        //    //parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
        //    string str = strSql.ToString();
        //    DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
        //    if (ds == null) return false;
        //    int i = 0;
        //    while (i < ds.Tables[0].Rows.Count)
        //    {
        //        ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
        //        i++;
        //    }
        //    return true;

        //}
        //根据项目名获取所以的周期
        public bool SurveyCYCDateTimeListGet(int xmno,string holename ,out List<string> cycTimeList)
        {
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmno);
            string holenamesearchstr = holename == "全部" ? string.Format(" holename  in  (select distinct(pointname ) from {0}.gtpointalarmvalue where xmno="+xmno+"  )", surveyconn.Database) : " holename = '" + holename + "' ";
            string sql = "select  distinct(cyc),time  from gtinclinometer_data  where  xmno='" + xmno + "'   and  " + holenamesearchstr + "      order by cyc asc";
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            DataTable dt = querysql.querystanderdb(sql, conn);
            cycTimeList = new List<string>();
            if (dt.Rows.Count == 0) return false;
            int i = 0;
            for (i = 0; i < dt.Rows.Count; i++)
            {
                cycTimeList.Add(string.Format("{0}[{1}]", dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1]));
            }
            return true;
        }


        public bool SurveyResultdataTableLoad(int startPageIndex, int pageSize, int xmno, string sord,string pointname,int startcyc, int endcyc, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("holename='{0}'", pointname);
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"select *  from gtinclinometer_data  where   xmno=@xmno    and    {0}   and   cyc  between      @startcyc    and    @endcyc      order by    {1}   asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);

            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                    

                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = startcyc;
            parameters[2].Value = endcyc;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
		
	}
}

