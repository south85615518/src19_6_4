﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_orgldata.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_orgldata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 8:51:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
using Tool;
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:fixed_inclinometer_orgldata
	/// </summary>
	public partial class fixed_inclinometer_orgldata
	{
        public static database db = new database();
		public fixed_inclinometer_orgldata()
		{}
		#region  BasicMethod

		

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(NGN.Model.fixed_inclinometer_orgldata model)
		{
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.Append("replace into gtinclinometer_calculate(");
            strSql.Append("xmno,holename,deepth,a_nagtive,a_positive,b_nagtive,b_positive,a_planediff,a_avediff,b_planediff,b_avediff,a_this_diff,a_ac_diff,b_this_diff,b_ac_diff,a_cmp_diff,b_cmp_diff,Time,cyc)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@holename,@deepth,@a_nagtive,@a_positive,@b_nagtive,@b_positive,@a_planediff,@a_avediff,@b_planediff,@b_avediff,@a_this_diff,@a_ac_diff,@b_this_diff,@b_ac_diff,@a_cmp_diff,@b_cmp_diff,@Time,@cyc)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
					new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@a_nagtive", OdbcType.Double),
					new OdbcParameter("@a_positive", OdbcType.Double),
					new OdbcParameter("@b_nagtive", OdbcType.Double),
					new OdbcParameter("@b_positive", OdbcType.Double),
					new OdbcParameter("@a_planediff", OdbcType.Double),
					new OdbcParameter("@a_avediff", OdbcType.Double),
					new OdbcParameter("@b_planediff", OdbcType.Double),
					new OdbcParameter("@b_avediff", OdbcType.Double),
                    new OdbcParameter("@a_this_diff", OdbcType.Double),
					new OdbcParameter("@a_ac_diff", OdbcType.Double),
					new OdbcParameter("@a_cmp_diff", OdbcType.Double),
                    new OdbcParameter("@b_this_diff", OdbcType.Double),
                    new OdbcParameter("@b_ac_diff", OdbcType.Double),
					new OdbcParameter("@b_cmp_diff", OdbcType.Double),
					new OdbcParameter("@Time", OdbcType.VarChar,100),
                    new OdbcParameter("@cyc", OdbcType.Int)
                                         };
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.holename;
            parameters[2].Value = model.deepth;
            parameters[3].Value = model.a_nagtive;
            parameters[4].Value = model.a_positive;
            parameters[5].Value = model.b_nagtive;
            parameters[6].Value = model.b_positive;
            parameters[7].Value = model.a_planediff;
            parameters[8].Value = model.a_avediff;
            parameters[9].Value = model.b_planediff;
            parameters[10].Value = model.b_avediff;
            parameters[11].Value = model.a_this_diff;
            parameters[12].Value = model.a_ac_diff;
            parameters[13].Value = model.a_cmp_diff;
            parameters[14].Value = model.b_this_diff;
            parameters[15].Value = model.b_ac_diff;
            parameters[16].Value = model.b_cmp_diff;
            parameters[17].Value = model.time;
            parameters[18].Value = model.cyc;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        //public bool Update(NGN.Model.fixed_inclinometer_orgldata model)
        //{
        //    StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
        //    strSql.Append("update fixed_inclinometer_orgldata set ");
        //    strSql.Append("f=@f,");
        //    strSql.Append("disp=@disp");
        //    strSql.Append(" where xmno=@xmno and holename=@holename and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@f", OdbcType.Double),
        //            new OdbcParameter("@disp", OdbcType.Double),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@holename", OdbcType.VarChar,100),
        //            new OdbcParameter("@time", OdbcType.DateTime)};
        //    parameters[0].Value = model.f;
        //    parameters[1].Value = model.disp;
        //    parameters[2].Value = model.xmno;
        //    parameters[3].Value = model.holename;
        //    parameters[4].Value = model.time;

        //    int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(NGN.Model.fixed_inclinometer_orgldata model)
		{

            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.Append("delete from gtinclinometer ");
            strSql.Append(" where xmno=@xmno and holename=@holename     and  time=@time ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
                    new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)			};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.holename;
            parameters[2].Value = model.deepth;
            parameters[3].Value = model.time;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        /// <summary>
        /// 删除临时表数据
        /// </summary>
        public bool Delete_tmp(int xmno,out int cont)
        {

            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("delete from gtinclinometer_tmp ");
            strSql.Append(" where xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
                                         };
            parameters[0].Value = xmno;
            cont = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (cont >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public bool GetModel(int xmno, string holename, double deepth, DateTime time, out NGN.Model.fixed_inclinometer_orgldata model)
		{

            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select    xmno,holename,deepth,a_nagtive,a_positive,b_nagtive,b_positive,a_planediff,a_avediff,b_planediff,b_avediff,a_this_diff,a_ac_diff,a_cmp_diff,b_this_diff,b_ac_diff,b_cmp_diff,Time,cyc,a_this_rap,b_this_rap  from gtinclinometer ");
            strSql.Append("   where     xmno=@xmno    and   holename=@holename  and  deepth=@deepth  and   time=@time  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
                    new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)			
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = holename;
            parameters[2].Value = deepth;
            parameters[3].Value = time;

            model = new NGN.Model.fixed_inclinometer_orgldata();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
		}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetLastTimeModel(int xmno, string holename, double deepth, DateTime time, out NGN.Model.fixed_inclinometer_orgldata model)
        {

            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select    xmno,holename,deepth,a_nagtive,a_positive,b_nagtive,b_positive,a_planediff,a_avediff,b_planediff,b_avediff,a_this_diff,a_ac_diff,a_cmp_diff,b_this_diff,b_ac_diff,b_cmp_diff,Time,cyc,a_this_rap,b_this_rap  from gtinclinometer_calculate ");
            strSql.Append("   where     xmno=@xmno    and   holename=@holename  and   deepth=@deepth   and   time<@time   order by time desc   limit 0,10");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
                    new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)			
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = holename;
            parameters[2].Value = deepth;
            parameters[3].Value = time;

            model = new NGN.Model.fixed_inclinometer_orgldata();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool GetSumAcDiff(int xmno, string holename, double deepth, DateTime time, out NGN.Model.fixed_inclinometer_orgldata model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select    sum(a_avediff) as a_ac_diff,sum(b_avediff) as b_ac_diff  from gtinclinometer ");
            strSql.Append("   where     xmno=@xmno    and   holename=@holename  and  deepth=@deepth  and   time<@time order by time desc   limit 0,10");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
                    new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)			
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = holename;
            parameters[2].Value = deepth;
            parameters[3].Value = time;
            model = new NGN.Model.fixed_inclinometer_orgldata();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ResultdataTableLoad(int startPageIndex, int pageSize, int xmno, string holename, string sord, int startcyc, int endcyc, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = holename == "" || holename == null || (holename.IndexOf("全部") != -1) ? "  1=1  " : string.Format("holename='{0}'", holename);
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select * from gtinclinometer  where   xmno='" + xmno + "'     and   cyc>='" + startcyc + "'      and       time <='" + endcyc + "'       and    {0}      order by {1}  asc  limit    " + (startPageIndex - 1) * pageSize + "," + pageSize * startPageIndex + "", searchstr, sord);

            ExceptionLog.ExceptionWrite(strSql.ToString());
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool ResultTableRowsCount(string xmno, string holename, string sord,  int startcyc, int endcyc, out string totalCont)
        {
            string searchstr = holename == null || holename == "" || (holename.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  holename = '{0}'  ", holename);
            string sql = string.Format("select count(1) from  gtinclinometer  where xmno='{0}'   and  cyc>='{2}' and  cyc <= '{3}'     and   {1}", xmno, searchstr, startcyc, endcyc);
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }

        /// <summary>
        /// 得到一个测斜段对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from gtinclinometer_tmp ");
            strSql.Append(" where    xmno=@xmno   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;

            lt = new List<NGN.Model.fixed_inclinometer_orgldata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


        public bool ReportDataView(List<string> cyclist, int startPageIndex, int pageSize, int xmno,  string sord, out DataTable dt)
        {
            dt = new DataTable();

            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select * from gtinclinometer  where      xmno=@xmno      and    holename  in (select distinct(holename) from " + surveyconn.Database + ".gtpointalarmvalue where xmno =" + xmno + " )    and   CYC    in ({0})  order by {1}  asc  limit    @startPageIndex,   @endPageIndex", string.Join(",", cyclist), sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = (startPageIndex - 1) * pageSize;
            parameters[2].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public NGN.Model.fixed_inclinometer_orgldata DataRowToModel(DataRow row)
        {
            NGN.Model.fixed_inclinometer_orgldata model = new NGN.Model.fixed_inclinometer_orgldata();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["holename"] != null)
                {
                    model.holename = row["holename"].ToString();
                }
                if (row["deepth"] != null)
                {
                    model.deepth = Convert.ToDouble(row["deepth"].ToString());
                }
                if (row["a_nagtive"] != null && row["a_nagtive"].ToString() != "")
                {
                    model.a_nagtive = Convert.ToDouble(row["a_nagtive"].ToString());
                }
                if (row["a_positive"] != null && row["a_positive"].ToString() != "")
                {
                    model.a_positive = Convert.ToDouble(row["a_positive"].ToString());
                }
                if (row["b_nagtive"] != null && row["b_nagtive"].ToString() != "")
                {
                    model.b_nagtive = Convert.ToDouble(row["b_nagtive"].ToString());
                }
                if (row["b_positive"] != null && row["b_positive"].ToString() != "")
                {
                    model.b_positive = Convert.ToDouble(row["b_positive"].ToString());
                }
                if (row["a_planediff"] != null && row["a_planediff"].ToString() != "")
                {
                    model.a_planediff = Convert.ToDouble(row["a_planediff"].ToString());
                }
                if (row["a_avediff"] != null && row["a_avediff"].ToString() != "")
                {
                    model.a_avediff = Convert.ToDouble(row["a_avediff"].ToString());
                }
                if (row["a_this_diff"] != null && row["a_this_diff"].ToString() != "")
                {
                    model.a_this_diff = Convert.ToDouble(row["a_this_diff"].ToString());
                }
                if (row["a_ac_diff"] != null && row["a_ac_diff"].ToString() != "")
                {
                    model.a_ac_diff = Convert.ToDouble(row["a_ac_diff"].ToString());
                }
                if (row["a_cmp_diff"] != null && row["a_cmp_diff"].ToString() != "")
                {
                    model.a_cmp_diff = Convert.ToDouble(row["a_cmp_diff"].ToString());
                }
                if (row["b_this_diff"] != null && row["b_this_diff"].ToString() != "")
                {
                    model.b_this_diff = Convert.ToDouble(row["b_this_diff"].ToString());
                }
                if (row["b_ac_diff"] != null && row["b_ac_diff"].ToString() != "")
                {
                    model.b_ac_diff = Convert.ToDouble(row["b_ac_diff"].ToString());
                }
                if (row["b_cmp_diff"] != null && row["b_cmp_diff"].ToString() != "")
                {
                    model.b_cmp_diff = Convert.ToDouble(row["b_cmp_diff"].ToString());
                }
                if (row["a_this_rap"] != null && row["a_this_rap"].ToString() != "")
                {
                    model.a_this_rap = Convert.ToDouble(row["a_this_rap"].ToString());
                }
                if (row["b_this_rap"] != null && row["b_this_rap"].ToString() != "")
                {
                    model.b_this_rap = Convert.ToDouble(row["b_this_rap"].ToString());
                }
                if (row["Time"] != null)
                {
                    model.time = Convert.ToDateTime(row["Time"].ToString());
                }
                if (row["cyc"] != null)
                {
                    model.time = Convert.ToDateTime(row["Time"].ToString());
                }
            }
            return model;
        }

        public bool PointMaxDateTimeGet(int xmno, out DateTime dt)
        {
            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select max(time) from gtinclinometer ");
            strSql.Append("   where     xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
						};
            parameters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }


        public bool MaxTime(int xmno,  string holename, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtinclinometer  where   xmno = @xmno   and    holename=@holename  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int),
                new OdbcParameter("@holename",OdbcType.VarChar,200)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = holename;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool MinTime(int xmno, string holename, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select min(time) from gtinclinometer  where   xmno = @xmno   and    holename=@holename  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int),
                new OdbcParameter("@holename",OdbcType.VarChar,200)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = holename;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool PointNameDateTimeListGet(int xmno, string holename, out List<string> cycTimeList)
        {

            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmno);
            string holenamesearchstr = holename == "全部" ? string.Format(" holename  in  (select distinct(pointname ) from {0}.gtpointalarmvalue where xmno = "+xmno+")", surveyconn.Database) : " holename = '" + holename + "' ";
            string sql = "select  distinct(cyc)  from gtinclinometer  where  xmno='" + xmno + "'   and  " + holenamesearchstr + "      order by cyc asc";
            ExceptionLog.ExceptionWrite(sql);
            OdbcConnection conn = db.GetStanderConn(xmno);
            DataTable dt = querysql.querystanderdb(sql, conn);
            cycTimeList = new List<string>();
            if (dt.Rows.Count == 0) return false;
            int i = 0;
            for (i = 0; i < dt.Rows.Count; i++)
            {
                cycTimeList.Add(string.Format("{0}", dt.Rows[i].ItemArray[0]));
            }
            return true;
        

        }

        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(int xmno,string holename ,DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select count(1) from   gtinclinometer   where   holename = @holename  and  time > @time    and      xmno=@xmno  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@holename",OdbcType.VarChar,100),
                 new OdbcParameter("@time",OdbcType.DateTime),
                 new OdbcParameter("@xmno",OdbcType.Int)
             };
            parameters[0].Value = holename;
            parameters[1].Value = dt;
            parameters[2].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);

            return Convert.ToInt32(obj) > 0 ? true : false;
        }
        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(int xmno,string holename ,DateTime dt,  out  int cyc)
        {
            cyc = -1;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select    min(cyc)    from    gtinclinometer    where  holename = @holename  and   time>@time    and      xmno=@xmno ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@holename",OdbcType.VarChar,100),
                 new OdbcParameter("@time",OdbcType.DateTime),
                 new OdbcParameter("@xmno",OdbcType.Int)
             };
            parameters[0].Value = holename;
            parameters[1].Value = dt;
            parameters[2].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            if (obj == null) return false;
            cyc = Convert.ToInt32(obj);
            return true;
        }


        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(int xmno,string holename ,int cyc)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select count(1) from gtinclinometer  where   holename = @holename  and   cyc =@cyc        and    xmno=@xmno  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@holename",OdbcType.VarChar,100),
                 new OdbcParameter("@cyc",OdbcType.Int),
                 new OdbcParameter("@xmno",OdbcType.Int)
             };
            parameters[0].Value = holename;
            parameters[1].Value = cyc;
            parameters[2].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            return Convert.ToInt32(obj) > 0 ? true : false;
        }




        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool InsertCycStep(int xmno,string holename ,int startcyc)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("update    gtinclinometer    set     cyc = cyc+1     where      cyc>@cyc   and  holename = @holename      and       xmno=@xmno ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@cyc",OdbcType.Int),
                 new OdbcParameter("@holename",OdbcType.VarChar,100),
                 new OdbcParameter("@xmno",OdbcType.Int)
             };
            parameters[0].Value = startcyc - 1;
            parameters[1].Value = holename;
            parameters[2].Value = xmno;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strsql.ToString(), parameters);
            return rows > 0 ? true : false;
        }


        //根据项目名称获取端点周期
        public bool ExtremelyCycGet(int xmno, string holename, string DateFunction, out string ExtremelyCyc)
        {

            string sql = "select  " + DateFunction + "(cyc)  from gtinclinometer where  holename='"+holename+"'    and     xmno='" + xmno + "'     ";
            OdbcConnection conn = db.GetStanderConn(xmno);
            ExtremelyCyc = querysql.querystanderstr(sql, conn);
            if (ExtremelyCyc == "") ExtremelyCyc = "0";
            return ExtremelyCyc != null ? true : false;

        }

        public bool IsInclinometerDataExist(int xmno, string holename, DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select    count(1)     from     gtinclinometer    where     xmno=@xmno     and     holename =@holename  and        time=@time  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@xmno",OdbcType.Int),
                 new OdbcParameter("@holename",OdbcType.VarChar,100),
                 new OdbcParameter("@time",OdbcType.DateTime)
             };
            parameters[0].Value = xmno;
            parameters[1].Value = holename;
            parameters[2].Value = dt;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            return Convert.ToInt32(obj) > 0 ? true : false;
        }

        ///// <summary>
        ///// 得到一个测斜段对象实体列表
        ///// </summary>
        //public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt)
        //{
        //    OdbcConnection conn = db.GetStanderConn(xmno);
        //    OdbcSQLHelper.Conn = conn;
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select xmno,holename,chain_name,disp,deep,time,f  from fixed_inclinometer_orgldata_tmp ");
        //    strSql.Append(" where    xmno=@xmno   ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@xmno", OdbcType.VarChar,120)
        //                    };
        //    parameters[0].Value = xmno;

        //    lt = new List<NGN.Model.fixed_inclinometer_orgldata>();
        //    DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
        //    int i = 0;
        //    while (i < ds.Tables[0].Rows.Count)
        //    {
        //        lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
        //        i++;
        //    }
        //    return true;
        //}



	

        






		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

