﻿/**  版本信息模板在安装目录下，可自行修改。
* swdevice.cs
*
* 功 能： N/A
* 类 名： swdevice
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/28 15:23:55   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//using Odbc.Data.OdbcClient;
//using Maticsoft.DBUtility;//Please add references
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:swdevice
	/// </summary>
	public partial class swdevice
	{
		public swdevice()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(NGN.Model.swdevice model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into swdevice(");
			strSql.Append("xmno,deviceno,initdeep,point_name,groundElevation,senorno)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@deviceno,@initdeep,@point_name,@groundElevation,@senorno)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@deviceno", OdbcType.Int,11),
					new OdbcParameter("@initdeep", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@groundElevation", OdbcType.Int,11),
					new OdbcParameter("@senorno", OdbcType.VarChar,100)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.deviceno;
			parameters[2].Value = model.initdeep;
			parameters[3].Value = model.point_name;
			parameters[4].Value = model.groundElevation;
			parameters[5].Value = model.senorno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(NGN.Model.swdevice model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update swdevice set ");
			strSql.Append("xmno=@xmno,");
			strSql.Append("deviceno=@deviceno,");
			strSql.Append("initdeep=@initdeep,");
			strSql.Append("point_name=@point_name,");
			strSql.Append("groundElevation=@groundElevation,");
			strSql.Append("senorno=@senorno");
			strSql.Append("  where  ");

			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@deviceno", OdbcType.Int,11),
					new OdbcParameter("@initdeep", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@groundElevation", OdbcType.Int,11),
					new OdbcParameter("@senorno", OdbcType.VarChar,100)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.deviceno;
			parameters[2].Value = model.initdeep;
			parameters[3].Value = model.point_name;
			parameters[4].Value = model.groundElevation;
			parameters[5].Value = model.senorno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from swdevice ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.swdevice GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select xmno,deviceno,initdeep,point_name,groundElevation,senorno from swdevice ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			NGN.Model.swdevice model=new NGN.Model.swdevice();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.swdevice DataRowToModel(DataRow row)
		{
			NGN.Model.swdevice model=new NGN.Model.swdevice();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["deviceno"]!=null && row["deviceno"].ToString()!="")
				{
					model.deviceno=int.Parse(row["deviceno"].ToString());
				}
				if(row["initdeep"]!=null && row["initdeep"].ToString()!="")
				{
					model.initdeep=int.Parse(row["initdeep"].ToString());
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["groundElevation"]!=null && row["groundElevation"].ToString()!="")
				{
					model.groundElevation=int.Parse(row["groundElevation"].ToString());
				}
				if(row["senorno"]!=null)
				{
					model.senorno=row["senorno"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select xmno,deviceno,initdeep,point_name,groundElevation,senorno ");
			strSql.Append(" FROM swdevice ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

