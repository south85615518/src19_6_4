﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;
using NFnet_Interface.DisplayDataProcess.GT;
using NFnet_BLL.DisplayDataProcess;

namespace NFnetUnitTest.datetimestamp
{
    public class TestPointDataDateTimeStamp
    {
        public ProcessPointDataDateTimeStampLoad processPointDataDateTimeStampLoad = new ProcessPointDataDateTimeStampLoad();
       // public ProcessSurveyDataLackDate processSurveyDataLackDate = new ProcessSurveyDataLackDate();
        public ProcessDataLackDate processDataLackDate = new ProcessDataLackDate();
        public PointDataDateTimeStamp timestamp = new PointDataDateTimeStamp();
        public ProcessGTPointLoad processGTPointLoad = new ProcessGTPointLoad();
        public string mssg = "";
        public void main()
        {
            //TestPointDataDateTimeStampLoad();
            //TestSurveyDataLackDate();
            TestDataLackDate();
        }
        public void TestPointDataDateTimeStampLoad()
        {

            List<string> points = processGTPointLoad.GTPointLoadBLL(106, data.Model.gtsensortype._axialforce,out mssg);
            List<PointDataDateTimeStamp> datetimestamplist = new List<PointDataDateTimeStamp>();
            foreach (var pointname in points)
            {
                datetimestamplist.Add(processPointDataDateTimeStampLoad.PointDataDateTimeStampLoad("新白广城际铁路下穿京广高铁施工监测", "",data.Model.gtsensortype._axialforce, timestamp,out mssg)); 
            }
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDataLackDate()
        {
            timestamp = new PointDataDateTimeStamp { point_name = "34180020-17", intervalhour = 6 };

            processDataLackDate.Datalackdate("新白广城际铁路下穿京广高铁施工监测", "34180020-24", data.Model.gtsensortype._axialforce, timestamp, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestCgDataLackDate()
        {
            //processCgDataLackDate.CgDatalackdate("新白广城际铁路下穿京广高铁施工监测", "34180020-17", data.Model.gtsensortype._stress, timestamp, out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
    }
}
