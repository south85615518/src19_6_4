﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Cells;
using Aspose.Cells.Charts;
using System.Drawing;
using Aspose.Cells.Drawing;

namespace NFnetUnitTest.interopexcel
{
    class AsposeCell
    {

        public void main()
        {
            TestAsposeCells();
        }
        private void CreateStaticData(Workbook workbook)
        {
            //Initialize Cells object
            Aspose.Cells.Cells cells = workbook.Worksheets[0].Cells;

            //Put string into a cells of Column A
            cells["A1"].PutValue("class");
            cells["A2"].PutValue("红萝卜");
            cells["A3"].PutValue("白萝卜");
            cells["A4"].PutValue("青萝卜");

            //Put a value into a Row 1
            cells["B1"].PutValue(2002);
            cells["C1"].PutValue(2003);
            cells["D1"].PutValue(2004);
            cells["E1"].PutValue(2005);
            cells["F1"].PutValue(2006);

            //Put a value into a Row 2
            cells["B2"].PutValue(40000);
            cells["C2"].PutValue(45000);
            cells["D2"].PutValue(50000);
            cells["E2"].PutValue(55000);
            cells["F2"].PutValue(70000);

            //Put a value into a Row 3
            cells["B3"].PutValue(10000);
            cells["C3"].PutValue(25000);
            cells["D3"].PutValue(40000);
            cells["E3"].PutValue(52000);
            cells["F3"].PutValue(60000);

            //Put a value into a Row 4
            cells["B4"].PutValue(5000);
            cells["C4"].PutValue(15000);
            cells["D4"].PutValue(35000);
            cells["E4"].PutValue(30000);
            cells["F4"].PutValue(20000);
        }
        private void CreateStaticReport(Workbook workbook)
        {
            //初始化 Worksheet
            Worksheet sheet = workbook.Worksheets[0];
            //设置 worksheet名称
            sheet.Name = "Line";
            //设置worksheet不显示
            sheet.IsGridlinesVisible = false;
            //根据数据源 创建 chart
            int chartIndex = 0;
            chartIndex = sheet.Charts.Add(ChartType.Line, 5, 1, 29, 15);
            //初始化chart
            Chart chart = sheet.Charts[chartIndex];
            //设置竖线不显示
            chart.CategoryAxis.MajorGridLines.IsVisible = false;
            //设置Title样式
            chart.Title.Text = "Sales By Class For Years";
            chart.Title.TextFont.Color = Color.Black;
            chart.Title.TextFont.IsBold = true;
            chart.Title.TextFont.Size = 12;
            //设置chart的数据源
            chart.NSeries.Add("B2:F4", false);
            chart.NSeries.CategoryData = "B1:F1";
            //Set Nseries color varience to True
            chart.NSeries.IsColorVaried = true;
            //初始化 Cells
            Cells cells = workbook.Worksheets[0].Cells;
            //循环 cells
            for (int i = 0; i < chart.NSeries.Count; i++)
            {
                //设置系列的名称
                chart.NSeries[i].Name = cells[i + 1, 0].Value.ToString();
                chart.NSeries[i].MarkerStyle = ChartMarkerType.Circle;
                //设置系列的名称 Background 与ForeGround
                chart.NSeries[i].MarkerBackgroundColor = Color.Yellow;
                chart.NSeries[i].MarkerForegroundColor = Color.Gold;
                //设置系列标记
                chart.NSeries[i].MarkerSize = 10;
                //设置Category的名称
                chart.CategoryAxis.Title.Text = "Year(2002-2006)";
                chart.CategoryAxis.Title.TextFont.Color = Color.Black;
                chart.CategoryAxis.Title.TextFont.IsBold = true;
                chart.CategoryAxis.Title.TextFont.Size = 10;
                //设置图例的位置
                chart.Legend.Position = LegendPositionType.Top;
            }

            //设置y轴的样式
            chart.ValueAxis.TickLabelPosition = TickLabelPositionType.NextToAxis;
            chart.ValueAxis.TickLabels.Font.Color = Color.Gray;
            chart.ValueAxis.AxisBetweenCategories = false;
            //chart.ValueAxis.TickLabels.Font.Size = 13;
            chart.ValueAxis.TickLabels.Font.IsBold = true;
            //Y坐标轴对数间隔展示
            // chart.ValueAxis.IsLogarithmic = true;
            chart.ValueAxis.MajorGridLines.Color = Color.Red;
            chart.ValueAxis.AxisLine.Color = Color.DarkGreen;
            //设置y坐标轴的厚度
            chart.ValueAxis.AxisLine.Weight = WeightType.WideLine;
            chart.ValueAxis.Title.Text = "y轴坐标";
            chart.ValueAxis.MajorUnit = 20000;//MajorUnit =2000;
            chart.ValueAxis.MaxValue = 200000;
            chart.ValueAxis.MinValue = 0;
            //设置右边坐标轴显示
            chart.SecondValueAxis.IsVisible = true;
            //设置y坐标轴间隔值字大小
            chart.SecondValueAxis.TickLabels.Font.Size = 12;
            chart.SecondValueAxis.Title.Text = "y轴坐标2";
            // chart.SecondValueAxis.MinorGridLines.IsVisible = true;
        }
        public void TestAsposeCells()
        {
            WorkbookDesigner designer = new WorkbookDesigner();
            string path = "e:/Templete/book1.xls";
            designer.Workbook.Open(path);
            Workbook workbook = designer.Workbook;
            CreateStaticData(workbook);
            CreateStaticReport(workbook);
            designer.Process();
            //将流文件写到客户端流的形式写到客户端，名称是_report.xls
            designer.Save("e:/_report.xls", SaveType.OpenInExcel, FileFormatType.Excel2003XML, System.Web.HttpContext.Current.Response);
            designer = null;
            // Response.End();
            //return View("getexcel");
        }

    }
}
