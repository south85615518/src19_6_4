﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;


namespace NFnetUnitTest.interopexcel
{
    public class excelchartTest
    {
        public Application ThisApplication = null;
        public Workbooks m_objBooks = null;
        public _Workbook ThisWorkbook = null;
        public Worksheet xlSheet = null;
        #region 柱状图
        public void main()
        {
            //CreateChart();
            button4_Click();
        }

        public void DeleteSheet()
        {
            foreach (Worksheet ws in ThisWorkbook.Worksheets)
                if (ws != ThisApplication.ActiveSheet)
                {
                    ws.Delete();
                }
            foreach (Chart cht in ThisWorkbook.Charts)

                cht.Delete();
        }
        private void AddDatasheet()
        {

            xlSheet = (Worksheet)ThisWorkbook.

                    Worksheets.Add(Type.Missing, ThisWorkbook.ActiveSheet,

                    Type.Missing, Type.Missing);



            xlSheet.Name = "数据";

        }
        private void LoadData()
        {

            Random ran = new Random();

            for (int i = 1; i <= 12; i++)
            {

                xlSheet.Cells[i, 1] = i.ToString() + "月";

                xlSheet.Cells[i, 2] = ran.Next(2000).ToString();

            }

        }

        private void LoadDatadb()
        {



            Random ran = new Random();

            

            for (int i = 1; i <= 12; i++)
            {

                xlSheet.Cells[i, 1] = i + "周期";

                xlSheet.Cells[i, 2] = ran.Next(2000).ToString();

                xlSheet.Cells[i, 3] = ran.Next(1500).ToString();

                xlSheet.Cells[i, 4] = ran.Next(1500).ToString();

                xlSheet.Cells[i+1, 8] = i + "周期";

                xlSheet.Cells[i+1, 9] = ran.Next(2000).ToString();

                xlSheet.Cells[i+1, 10] = ran.Next(1500).ToString();

                xlSheet.Cells[i+1, 11] = ran.Next(1500).ToString();

            }

        }

        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateChart()
        {
            
            Chart xlChart = (Chart)ThisWorkbook.Charts.
                Add(Type.Missing, xlSheet, 2, Type.Missing);
            xlChart = ThisWorkbook.Charts.get_Item(2);
            Range cellRange = (Range)xlSheet.get_Range("A2","D10");
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            xlChart.ChartWizard(cellRange,
                XlChartType.xlLineMarkers, Type.Missing,
                XlRowCol.xlColumns, 1, 0, true,
                "访问量比较(dahuzizyd.cnblogs.com)", "周期", "变化量",
                "");
            xlChart.ChartTitle.Position = XlChartElementPosition.xlChartElementPositionCustom;
            xlChart.ChartStyle = 26;
            xlChart.Name = xlSheet.Name+ "统计2";
            xlChart = ThisWorkbook.Charts.get_Item(1);
            xlChart.ChartWizard(cellRange,
               XlChartType.xlLineMarkers, Type.Missing,
               XlRowCol.xlColumns, 1, 0, true,
               "访问量比较(dahuzizyd.cnblogs.com)", "周期", "变化量",
               "");
            xlChart.Name = xlSheet.Name+ "统计1";
            
            ChartGroup grp = (ChartGroup)xlChart.ChartGroups(1);
            grp.GapWidth = 20;
            grp.VaryByCategories = true;
            //grp.SeriesLines.Delete();
            //foreach (Series series in grp.SeriesLines)
            //{
            //    series.Name = new Random().Next(99) + "s";
            //}

            Series s = (Series)grp.SeriesCollection(1);
            
            s.BarShape = XlBarShape.xlCylinder;
            s.HasDataLabels = true;

            xlChart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
            
            xlChart.ChartTitle.Font.Size = 24;
            xlChart.ChartTitle.Shadow = true;
            xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

            Axis valueAxis = (Axis)xlChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
            valueAxis.AxisTitle.Orientation = -90;

            Axis categoryAxis = (Axis)xlChart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
            categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
        }
        private void button4_Click()
        {
            try
            {
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add("e:\\Book1.xls"));

                ThisApplication.DisplayAlerts = false;


                //this.DeleteSheet();
                this.AddDatasheet();
                this.LoadDatadb();
                //this.WorkSheetCopy("D5","D5");
                List<string> sheetsName = new List<string>();
                foreach (Worksheet sheet in ThisWorkbook.Sheets)
                {
                    sheetsName.Add(sheet.Name);
                }
                int n = 1;
                foreach (string sheetName in sheetsName)
                {
                    ThisApplication.DisplayAlerts = false;
                    //xlSheet = ThisWorkbook.Sheets.get_Item(sheetName);
                    this.CreateChart();
                    
                }
                

                ThisWorkbook.SaveAs("E:\\Book2.xls", Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                string a = "";
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();
                //this.Close();
            }

        }
        #endregion
        #region 曲线图

        public void chartcreate( string path,int rowcnt)
        {
            ThisApplication = new Application();
            m_objBooks = (Workbooks)ThisApplication.Workbooks;
            ThisWorkbook = (_Workbook)(m_objBooks.Add(path));

        }
       



        #endregion

    



    }
}
