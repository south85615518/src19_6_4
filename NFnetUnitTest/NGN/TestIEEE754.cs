﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnetUnitTest.ngn
{
    public class TestIEEE754
    {
        public void main()
        {
            //BytesToIEEE754();
            //BinaryToNagativeFloatTest();
            INT0XToIEEE754(0x461FB688/*41BD9A00*/);
            //BinaryToFloat("0.1101");
            //byte[] data = { 0xFF, 0x1C, 0x00, 0x58, 0x00, 0x15, 0x0D, 0x01, 0x44, 0x55, 0x25, 0x55, 0x59, 0x14, 0x64, 0x00, 0x00, 0x98, 0x42 };
            //CRC16(data);

        }
        public void INT0XToIEEE754(int data0x)
        {
            string binarystring = oxToFloat(data0x);
            IEE754 iEE754 = IEE754GetFromBinaryString(binarystring);
            double value = IEE745real4Float(iEE754);
            ProcessPrintMssg.Print(string.Format("{0}转换成IEEE754浮点数是{1}", Convert.ToString(data0x, 16), value));
        }
        public string ConvertToOX(int data)
        {
            return Convert.ToString(data,16);
        }

        public double BinaryToNagativeFloat(int n)
        {
           
            return Math.Pow(2,-1*n);
            
        }
        public void BinaryToNagativeFloatTest()
        {
            double e = BinaryToNagativeFloat(8) + BinaryToNagativeFloat(11) + BinaryToNagativeFloat(12) + BinaryToNagativeFloat(17);
            ProcessPrintMssg.Print("e:"+e.ToString());
            double value = Math.Pow(-1, 0) * Math.Pow(2, -126) * e;
            ProcessPrintMssg.Print("value:"+value);
            //ProcessPrintMssg.Print( string.Format("2的-{0}次幂为{1}", 4, BinaryToNagativeFloat(4)));
        }
       
        public string oxToFloat(int data)
        {
            //int data = 0x02950000;
            string binarystring = Convert.ToString(data,2);
            StringBuilder strbuilder = new StringBuilder(256);
            int length = binarystring.Length;
            int i = 0;
            for (i = 32 - length; i > 0;i-- )
            {
                strbuilder.Append("0");
            }
            strbuilder.Append(binarystring);
            return strbuilder.ToString();
            //ProcessPrintMssg.Print(string.Format("{0}转换为二进制数{1}", Convert.ToString(data, 16), strbuilder.ToString()));
        }

        public class IEE754
        {
            public int s { get; set; }
            public int e { get; set; }
            public double m { get; set; }
        }
        public IEE754 IEE754GetFromBinaryString(string binarystring)
        {
            bool isequal = binarystring == "01000010100110000000000000000000";
            IEE754 iEE754 = new IEE754();
            iEE754.s = Convert.ToInt32(binarystring.Substring(0, 1));
            //01000010100110000000000000000000
            iEE754.e = Convert.ToInt32(binarystring.Substring(1, 8), 2);
            string mstr = binarystring.Substring(9);
            iEE754.m = BinaryToFloat("0."+mstr);
            if (iEE754.e > 0 && iEE754.e != Convert.ToInt32("11111111", 2))
            {
                iEE754.m += 1;
            }

            return iEE754;


        }

        public double IEE745real4Float(IEE754 iEE754 )
        {

            if(iEE754.e == 0) return Math.Pow(-1,iEE754.s)*Math.Pow(2,-126)*iEE754.m;
            return Math.Pow(-1, iEE754.s) * Math.Pow(2, iEE754.e - 127) * iEE754.m;
        }

        public double BinaryToFloat(string binarystr)
        {
            int dotpos = binarystr.IndexOf(".");
            int iInterger = dotpos, indxdot = dotpos;
            double integerValue = 0,dotvalue = 0;
            for (iInterger = dotpos-1; iInterger >= 0; iInterger --)
            {
                integerValue += Convert.ToDouble(binarystr[iInterger].ToString()) * Math.Pow(2, dotpos - iInterger-1);
            }
            for (indxdot = dotpos+1; indxdot < binarystr.Length; indxdot++)
            {
                dotvalue += Convert.ToDouble(binarystr[indxdot].ToString()) * Math.Pow(2, -1 * (indxdot - dotpos));
            }
            integerValue += dotvalue;
            ProcessPrintMssg.Print(string.Format("{0}转换成十进制是{1}", binarystr, integerValue));
            return integerValue ;
            
        }

        public static byte[] CRC16(byte[] data)
        {
            
            byte[] temdata = new byte[data.Length + 2];
            int xda, xdapoly;
            int i, j, xdabit;
            xda = 0xFFFF;
            xdapoly = 0xA001;
            for (i = 0; i < data.Length; i++)
            {
                xda ^= data[i];
                for (j = 0; j < 8; j++)
                {
                    xdabit = (int)(xda & 0x01);
                    xda >>= 1;
                    if (xdabit == 1)
                        xda ^= xdapoly;
                }
            }
            Array.Copy(data, 0, temdata, 0, data.Length);
            temdata[temdata.Length - 2] = (byte)(xda & 0xFF);
            temdata[temdata.Length - 1] = (byte)(xda >> 8);
            return temdata;
        }



        //public string GetSubString(string src,int startIndex, int length)
        //{
        //    return src.Substring(startIndex,length);
        //}


    }
    
     
}
