﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool.com;

namespace NFnetUnitTest.ngn
{
    public class TestFixInclinometer
    {
        public IEEE754Helper iEEE754Helper = new IEEE754Helper();
        public Tool.com.HexHelper hexHelper = new HexHelper();
        public void main()
        {
 
        }
        public FixInclinometer FixInclinometerDescode(byte[] data)
        {
            //FF 1C 00 06 00 2D 11 01 44 57 68 02 8E 03 64 4E 62 80 40 30 64 00 00 00 00 31 64 00 00 00 00 32 64 00 00 00 00 33 64 00 00 00 00 4B 8C
            //FixInclinometer FixInclinometer = new FixInclinometer();
            //FixInclinometer.id = Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 2, 2));
            //FixInclinometer.dt = Tool.DateHelper.Unixtimestamp(Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 9, 4), 16));
            //FixInclinometer.fOne = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 21, 4));
            //FixInclinometer.fTwo = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 25, 4));
            //FixInclinometer.fThree = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 29, 4));
            //FixInclinometer.fFour = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 28, 4));
            //return new FixInclinometer();
            return null;
        }

        public double FixInclinometerS(double a,double b,double c,double d,double L,double f)
        {
            /*
             * 
             */
            double s = L * Math.Sin(a + b * f + c * Math.Pow(f, 2) + d * Math.Pow(f, 3));
            return s;

        }
        public void FixInclinometerS(FixInclinometer fixInclinometer)
        {
            fixInclinometer.S += FixInclinometerS(fixInclinometer.a,fixInclinometer.b,fixInclinometer.c,fixInclinometer.d,fixInclinometer.L,fixInclinometer.fOne);
            fixInclinometer.S += FixInclinometerS(fixInclinometer.a, fixInclinometer.b, fixInclinometer.c, fixInclinometer.d, fixInclinometer.L, fixInclinometer.fTwo);
            fixInclinometer.S += FixInclinometerS(fixInclinometer.a, fixInclinometer.b, fixInclinometer.c, fixInclinometer.d, fixInclinometer.L, fixInclinometer.fThree);
            fixInclinometer.S += FixInclinometerS(fixInclinometer.a, fixInclinometer.b, fixInclinometer.c, fixInclinometer.d, fixInclinometer.L, fixInclinometer.fFour);
        }
        public class FixInclinometer
        {
            public int id { get; set; }
            public DateTime dt { get; set; }
            public double L { get; set; }
            public double a { get; set; }
            public double b { get; set; }
            public double c { get; set; }
            public double d { get; set; }
            public double fOne { get; set; }
            public double fTwo { get; set; }
            public double fThree { get; set; }
            public double fFour { get; set; }
            public double S{ get;set; }
        }
    }
}
