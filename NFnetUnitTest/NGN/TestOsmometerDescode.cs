﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool.com;

namespace NFnetUnitTest.ngn
{
    public class TestOsmometerDescode
    {
        public IEEE754Helper iEEE754Helper = new IEEE754Helper();
        public Tool.com.HexHelper hexHelper = new HexHelper();
        public void main()
        {
 
        }
        public Osmometer OsmometerDescode(byte[] data)
        {
            //FF 60 00 A9 00 27 10 01 44 57 67 F7 0A 02 44 56 86 41 3C 03 64 07 81 85 40 11 64 00 00 00 00 12 64 9A 99 88 C3 B5 24
            Osmometer osmometer = new Osmometer();
            osmometer.id = Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 2, 1));
            osmometer.dt = Tool.DateHelper.Unixtimestamp(Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 9, 4), 16));
            osmometer.F = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 28, 4));
            osmometer.Pm = (osmometer.F - osmometer.f0) * osmometer.k;
            return osmometer;
        }
        public class Osmometer
        {
            public int id { get; set; }
            public DateTime dt { get; set; }
            public double k { get; set; }
            public double b { get; set; }
            public double f0 { get; set; }
            public double F { get; set; }
            public double T0 { get; set; }
            public double T { get; set; }
            public double Q0 { get; set; }
            public double Q { get; set; }
            public double Pm { get; set; }
 
        }
        
        //Pm = k△F + b△T + △Q = k(F0 - F) + b(T - T0) + (Q0-Q)
        //public   OsmometerValue



    }
}
