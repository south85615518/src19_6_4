﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnetUnitTest.ngn
{
    public class TestUnixtimestamp
    {
        public void main()
        {
            //int a = Convert.ToInt32("0x60000",16);
            Unixtimestamp(1527246300000);
        }
        public DateTime Unixtimestamp(long datetimevalue)
        {

            //DateTime dt = new DateTime(datetimevalue);

            string datetimestr = LongDateTimeToDateTimeString(datetimevalue);

            ProcessPrintMssg.Print(string.Format("{0}转换成北京时间是{1}", datetimevalue, Convert.ToDateTime(datetimestr).AddHours(-8)));
            return Convert.ToDateTime(datetimestr);

        }

        /// <summary>
        /// 将时间戳转换为日期类型，并格式化
        /// </summary>
        /// <param name="longDateTime"></param>
        /// <returns></returns>
        private static string LongDateTimeToDateTimeString(long unixDate)
        {
            //用来格式化long类型时间的,声明的变量
            DateTime start;
            DateTime date;
            start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            date = start.AddMilliseconds(unixDate).ToLocalTime();

            return date.ToString("yyyy-MM-dd HH:mm:ss");

        }
    }
}
