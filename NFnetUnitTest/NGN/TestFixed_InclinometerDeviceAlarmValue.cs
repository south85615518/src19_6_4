﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnetUnitTest.ngn
{
    public class TestFixed_InclinometerdeviceAlarmValue
    {
        public ProcessFixed_Inclinometer_devicealarmvalueBLL processFixed_Inclinometer_devicealarmvalueBLL = new ProcessFixed_Inclinometer_devicealarmvalueBLL();
        public ProcessdeviceAlarmValueDelete processdeviceAlarmValueDelete = new ProcessdeviceAlarmValueDelete();
        public ProcessdeviceAlarmValueDelete processdeviceAlarmValueModelGet = new ProcessdeviceAlarmValueDelete();
        public NGN.Model.fixed_inclinometer_alarmvalue devicealarmvalue = new NGN.Model.fixed_inclinometer_alarmvalue { xmno = 29, alarmName = "一级预警", dispL = 0, dispU = 1 };
        public ProcessdeviceAlarmValueTableLoad processdeviceAlarmValueTableLoad = new ProcessdeviceAlarmValueTableLoad();
        public ProcessdeviceAlarmValueNameGet processdeviceAlarmValueNameGet = new ProcessdeviceAlarmValueNameGet();
        public string mssg = "";
        public void main()
        {
            //Fixed_InclinometerdeviceAlarmValueAdd();
            //Fixed_InclinometerdeviceAlarmValueUpdate();
            //Fixed_InclinometerdeviceAlarmValueDelete();
            //Fixed_InclinometerdeviceAlarmValueAdd();
            //Fixed_InclinometerdeviceAlarmValueModelGet();
            Fixed_InclinometerdeviceAlarmValueTableLoad();
            //Fixed_InclinometerdeviceAlarmValueNameGet();
        }
        public void Fixed_InclinometerdeviceAlarmValueAdd()
        {
            processFixed_Inclinometer_devicealarmvalueBLL.Add(devicealarmvalue, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerdeviceAlarmValueUpdate()
        {
            processFixed_Inclinometer_devicealarmvalueBLL.Update(devicealarmvalue, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerdeviceAlarmValueDelete()
        {
            processdeviceAlarmValueDelete.deviceAlarmValueDelete(devicealarmvalue.xmno, devicealarmvalue.alarmName, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerdeviceAlarmValueModelGet()
        {
            processdeviceAlarmValueModelGet.deviceAlarmValueDelete(devicealarmvalue.xmno, devicealarmvalue.alarmName, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerdeviceAlarmValueTableLoad()
        {
            processdeviceAlarmValueTableLoad.deviceAlarmValueTableLoad(1, 20, 29, "alarmname", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerdeviceAlarmValueNameGet()
        {
            processdeviceAlarmValueNameGet.deviceAlarmValueNameGet(devicealarmvalue.xmno, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
