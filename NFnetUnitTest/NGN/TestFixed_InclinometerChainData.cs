﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN;

namespace NFnetUnitTest.ngn
{
    public class TestFixed_InclinometerchainData
    {
        public ProcessFixed_Inclinometer_chainDataBLL chainDataBLL = new ProcessFixed_Inclinometer_chainDataBLL();
        public ProcessChainDataModelGet processchainDataModelGet = new ProcessChainDataModelGet();
        public NGN.Model.fixed_inclinometer_chaindata chaindata = new NGN.Model.fixed_inclinometer_chaindata
        {
            xmno = 29,
            disp = 1,
            point_name = "cx1",
            time = DateTime.Now
        };
        public string mssg = "";
        public void main()
        {
            //Fixed_InclinometerchainAdd();
            //Fixed_InclinometerchainModelGet();
            Fixed_InclinometerChainDataMaxTimeGet();
        }
        public void Fixed_InclinometerchainAdd()
        {
            chainDataBLL.Add(chaindata, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerchainModelGet()
        {
            processchainDataModelGet.chainDataModelGet(29, "cx1", chaindata.time, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public ProcessChainDataMaxTime processChainDataMaxTime = new ProcessChainDataMaxTime();
        public void Fixed_InclinometerChainDataMaxTimeGet()
        {
            processChainDataMaxTime.ChainDataMaxTime(29,"cx1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
