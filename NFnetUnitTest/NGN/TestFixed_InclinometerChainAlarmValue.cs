﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnetUnitTest.ngn
{
    public class TestFixed_InclinometerChainAlarmValue
    {
        public ProcessFixed_Inclinometer_chainalarmvalueBLL processFixed_Inclinometer_chainalarmvalueBLL = new ProcessFixed_Inclinometer_chainalarmvalueBLL();
        public ProcessChainAlarmValueDelete processChainAlarmValueDelete = new ProcessChainAlarmValueDelete();
        public ProcessChainAlarmValueModelGet processChainAlarmValueModelGet = new ProcessChainAlarmValueModelGet();
        public NGN.Model.fixed_inclinometer_chainalarmvalue chainalarmvalue = new NGN.Model.fixed_inclinometer_chainalarmvalue { xmno = 29, alarmName="一级预警", dispL = 0, dispU=1 };
        public ProcessChainAlarmValueTableLoad processChainAlarmValueTableLoad = new ProcessChainAlarmValueTableLoad();
        public ProcessChainAlarmValueNameGet processChainAlarmValueNameGet = new ProcessChainAlarmValueNameGet();
        public string mssg = "";
        public void main()
        {
            Fixed_InclinometerChainAlarmValueAdd();
            Fixed_InclinometerChainAlarmValueUpdate();
            Fixed_InclinometerChainAlarmValueDelete();
            Fixed_InclinometerChainAlarmValueAdd();
            Fixed_InclinometerChainAlarmValueModelGet();
            Fixed_InclinometerChainAlarmValueTableLoad();
            Fixed_InclinometerChainAlarmValueNameGet();
        }
        public void Fixed_InclinometerChainAlarmValueAdd()
        {
            processFixed_Inclinometer_chainalarmvalueBLL.Add(chainalarmvalue,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerChainAlarmValueUpdate()
        {
            processFixed_Inclinometer_chainalarmvalueBLL.Update(chainalarmvalue, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerChainAlarmValueDelete()
        {
            processChainAlarmValueDelete.ChainAlarmValueDelete(chainalarmvalue.xmno, chainalarmvalue.alarmName, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerChainAlarmValueModelGet()
        {
            processChainAlarmValueModelGet.ChainAlarmValueModelGet(chainalarmvalue.xmno, chainalarmvalue.alarmName,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerChainAlarmValueTableLoad()
        {
            processChainAlarmValueTableLoad.deviceAlarmValueTableLoad(1,20,29,"alarmname","asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Fixed_InclinometerChainAlarmValueNameGet()
        {
            processChainAlarmValueNameGet.ChainAlarmValueNameGet(chainalarmvalue.xmno,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
