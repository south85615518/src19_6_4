﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN;

namespace NFnetUnitTest.ngn
{
    public class TestFixed_InclinometerChain
    {
        public ProcessFixed_Inclinomater_chainBLL processFixed_Inclinomater_chainBLL = new ProcessFixed_Inclinomater_chainBLL();
        public ProcessChainDelete processChainDelete = new ProcessChainDelete();
        public ProcessChainModelGet processChainModelGet = new ProcessChainModelGet();
        public ProcessChainAlarmMultiUpdate processChainAlarmMultiUpdate = new ProcessChainAlarmMultiUpdate();
        public ProcessChainAlarmValueNameGet processChainAlarmValueNameGet = new ProcessChainAlarmValueNameGet();
        public ProcessChainTableLoad processChainTableLoad = new ProcessChainTableLoad();
        public ProcessFixed_Inclinomater_chainBLL pointAlarmBLL = new ProcessFixed_Inclinomater_chainBLL();
        public ProcessDTU_FixedInclinometerPointLoad fixedInclinometerPointLoad = new ProcessDTU_FixedInclinometerPointLoad();
        public NGN.Model.fixed_inclinometer_chain chainModel = new NGN.Model.fixed_inclinometer_chain {
             chain_name = "测斜1号孔位", deviceno = 2, port = 8680, xmno = 29, firstAlarmName="一级预警",
              secondAlarmName = "二级预警", thirdAlarmName = "三级预警"
        };
        public string mssg = "";
        public void main()
        {
            //ProcessFixed_InclinometerChainAdd();
            //ProcessFixed_InclinometerChainDelete();
            //ProcessFixed_InclinometerChainAdd();
            //ProcessFixed_InclinometerChainModelGet();
            //ProcessFixed_InclinometerChainUpdateAlarm();
            //ProcessFixed_InclinometerChainMultiUpdateAlarm();
            //ProcessFixed_InclinometerChainNameLoad();
            //ProcessFixed_InclinometerChainTableLoad();
            //ProcessFixed_InclinometerChainModelGetByChainName();
            ProcessDTU_Fixed_InclinometerPointLoad();
        }
        public void ProcessFixed_InclinometerChainAdd()
        {
            processFixed_Inclinomater_chainBLL.Add(chainModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerChainDelete()
        {
            processChainDelete.ChainDelete(29, chainModel.chain_name, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerChainModelGet()
        {
            processChainModelGet.ChainModelGet(29,chainModel.deviceno,chainModel.port,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerChainModelGetByChainName()
        {
            var processPointAlarmModelGetModel = new ProcessFixed_Inclinomater_chainBLL.fixed_inclinometer_chainModelGetByNameModel(29, "测斜1号孔位");
            NGN.Model.fixed_inclinometer_chainalarmvalue model = null;
            pointAlarmBLL.GetModel(processPointAlarmModelGetModel, out mssg);
            
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerChainUpdateAlarm()
        {
            processFixed_Inclinomater_chainBLL.UpdateAlarm(chainModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerChainMultiUpdateAlarm()
        {
            processChainAlarmMultiUpdate.ChainAlarmMultiUpdate(chainModel.chain_name,chainModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerChainNameLoad()
        {
            processChainAlarmValueNameGet.ChainAlarmValueNameGet(chainModel.xmno,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerChainTableLoad()
        {
            processChainTableLoad.ChainTableLoad(chainModel.xmno, 1, 20, "chain_name", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void ProcessDTU_Fixed_InclinometerPointLoad()
        {
            var model = fixedInclinometerPointLoad.DTU_FixedInclinometerPointLoad(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
