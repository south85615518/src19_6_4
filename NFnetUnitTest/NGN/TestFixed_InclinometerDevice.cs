﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN;

namespace NFnetUnitTest.ngn
{
    public class TestFixed_Inclinometerdevice
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public ProcessFixed_InclinometerDeviceExist processFixed_InclinometerDeviceExist = new ProcessFixed_InclinometerDeviceExist();
        public ProcessDeviceAlarmMultiUpdate processDeviceAlarmMultiUpdate = new ProcessDeviceAlarmMultiUpdate();
        public ProcessDeviceChainMultiUpdate processDeviceChainMultiUpdate = new ProcessDeviceChainMultiUpdate();
        public ProcessDeviceModelGet processDeviceModelGet = new ProcessDeviceModelGet();

        public ProcessDeviceChainDeviceNameLoad processDeviceChainDeviceNameLoad = new ProcessDeviceChainDeviceNameLoad();
        public ProcessDeviceTableLoad processDeviceTableLoad = new ProcessDeviceTableLoad();
        public ProcessDeviceChainDelete processDeviceChainDelete = new ProcessDeviceChainDelete();

        public ProcessPortModuleDeviceGet processPortModuleDeviceGet = new ProcessPortModuleDeviceGet();
        NGN.Model.fixed_inclinometer_device model = new NGN.Model.fixed_inclinometer_device
        {
            xmno = 29,
            point_name = "CX01",
            chain = "测斜1号孔位",
            deviceno = 2,
            address = 97,
            port = 8680,
            Wheeldistance = 1,
            modulesA = 1,
            modulesB = 2,
            modulesC = 3,
            modulesD = 4,
            firstAlarmName = "一级预警",
            secondAlarmName = "二级预警",
            thirdAlarmName = "三级预警",
            deep = 11
        };
        public NGN.Model.fixed_inclinometer_chain chainModel = new NGN.Model.fixed_inclinometer_chain
        {
            chain_name = "测斜1号孔位",
            deviceno = 2,
            port = 8680,
            xmno = 29,
            firstAlarmName = "一级预警",
            secondAlarmName = "二级预警",
            thirdAlarmName = "三级预警"
        };
        public string mssg = "";
        public void main()
        {
            //ProcessFixed_InclinometerdeviceAdd();
            //ProcessFixed_InclinometerdeviceDelete();
            ProcessFixed_InclinometerdeviceAdd();
            //ProcessFixed_InclinometerdeviceModelGet();
            //ProcessFixed_InclinometerdeviceUpdateAlarm();
            //ProcessFixed_InclinometerdeviceNameLoad();
            //ProcessFixed_InclinometerdeviceTableLoad();
            //TestPortModuleDeviceGet();
        }
        public void ProcessFixed_InclinometerdeviceExist()
        {
            processFixed_InclinometerDeviceExist.Fixed_InclinometerDeviceExist(model.point_name,model.xmno, out mssg);
            ProcessPrintMssg.Print(mssg);

        }
        public void ProcessFixed_InclinometerdeviceAdd()
        {
            processFixed_Inclinomater_deviceBLL.Add(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceUpdate()
        {
            processFixed_Inclinomater_deviceBLL.Update(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceDelete()
        {
            processFixed_Inclinomater_deviceBLL.Delete(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceChainUpdate()
        {
            processDeviceChainMultiUpdate.DeviceChainMultiUpdate(model.point_name,chainModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceModelGet()
        {
            processDeviceModelGet.DeviceModelGet(model.xmno,model.deviceno,model.address,model.port,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceUpdateAlarm()
        {
            processFixed_Inclinomater_deviceBLL.UpdateAlarm(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceMultiUpdateAlarm()
        {
            processDeviceAlarmMultiUpdate.deviceAlarmMultiUpdate(model.point_name,model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceNameLoad()
        {
            processDeviceChainDeviceNameLoad.ChainDeviceNameListLoad(model.chain,model.xmno,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceDeleteChain()
        {
            processDeviceChainDelete.DeviceChainDelete(model.xmno,model.chain,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessFixed_InclinometerdeviceTableLoad()
        {
            processDeviceTableLoad.deviceTableLoad(model.xmno,1,20,"point_name","asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestPortModuleDeviceGet()
        {
            processPortModuleDeviceGet.PortModuleDeviceGet( model.xmno,8680,"6",out mssg);
            ProcessPrintMssg.Print(mssg);
        }


    }
}
