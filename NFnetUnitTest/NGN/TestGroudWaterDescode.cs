﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool.com;

namespace NFnetUnitTest.ngn
{
    public class TestGroudWaterDescode
    {
        public IEEE754Helper iEEE754Helper = new IEEE754Helper();
        public Tool.com.HexHelper hexHelper = new HexHelper();
        public void main()
        {
 
        }
        public GroudWater GroudWaterDescode(byte[] data)
        {
            //FF 60 00 A9 00 27 10 01 44 57 67 F7 0A 02 44 56 86 41 3C 03 64 07 81 85 40 11 64 00 00 00 00 12 64 9A 99 88 C3 B5 24
            GroudWater groudWater = new GroudWater();
            groudWater.id = Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 2, 1));
            groudWater.dt = Tool.DateHelper.Unixtimestamp(Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 9, 4), 16));
            groudWater.deep = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 28, 4));
            return new GroudWater();
        }
        public class GroudWater
        {
            public int id { get; set; }
            public DateTime dt { get; set; }
            public double deep { get; set; }
            public double degree { get; set; }
 
        }
        



    }
}
