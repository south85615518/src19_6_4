﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;


namespace PylPublicResource.OtherModel.APPTool
{


    public class WatchDog
    {
        public event MsgBroadCast MsgBroadCastEvent;
        void risMsgBroadCastEvent(string msg)
        {
            if (MsgBroadCastEvent != null)
            {
                MsgBroadCastEvent(msg, this);
            }
        }
        //
        bool windowHide = false;
        //
        public const string SELF_ACTIVE_FILE_EXTENTION = "_self_ActiveFile.txt";
        //
        System.Timers.Timer dogDoor;
        //
        bool useFile = false;
        public bool UseFile
        {
            get { return useFile; }
            set { useFile = value; }
        }
        //
        string targetProgram = string.Empty;
        /// <summary>
        /// 目标程序，不带".exe"
        /// </summary>
        public string TargetProgram
        {
            get { return targetProgram; }
            set { targetProgram = value; }
        }
        //
        string targetProgramActiveFile = string.Empty;
        /// <summary>
        /// 目标程序的活动文件，全名称
        /// </summary>
        public string TargetProgramActiveFile
        {
            get { return targetProgramActiveFile; }
            set { targetProgramActiveFile = value; }
        }
        //
        string selfActiveFile = Application.StartupPath + "\\" + Application.ProductName + SELF_ACTIVE_FILE_EXTENTION;
        //
        void WriteActiveFile()
        {

            try
            {
                using (var fs = new FileStream(selfActiveFile, FileMode.Truncate, FileAccess.Write))
                {
                    fs.WriteByte(0);
                }
            }
            catch (Exception ex) { }
        }
        //  
        void StartProgram()
        {
            risMsgBroadCastEvent("启动程序" + DateTime.Now.ToString());
            try
            {
                var p = Process.Start(new ProcessStartInfo(Application.StartupPath + "\\" + TargetProgram + ".exe"));
                //risMsgBroadCastEvent(string.Format("启动程序时发生：{0}", ex.Message));
            }
            catch (Exception ex)
            {
                risMsgBroadCastEvent(string.Format("启动程序时发生：{0}", ex.Message));
            }
        }
        //
        bool IsProgramNoActivityForWhile()
        {
            var pro = Process.GetProcessesByName(TargetProgram);
            if (pro.Count() < 1)
            {
                return true;
            }
            //
            if (pro[0].Responding == false) { return true; }
            // 
            bool programNoActivityForWhile = false;
            //
            if (UseFile && File.Exists(TargetProgramActiveFile))
            {
                #region 使用激活文件来处理
                FileInfo f = new FileInfo(TargetProgramActiveFile);
                var lastWriteTime = f.LastWriteTime;
                var s = (DateTime.Now - lastWriteTime).TotalSeconds;
                //程序一段时间后没反应 
                programNoActivityForWhile = s > 30;
                //
                #endregion
            }
            return programNoActivityForWhile;
        }
        //
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();// 获取控制台句柄
        // 
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        //
        [DllImport("user32.dll", EntryPoint = "FindWindowEx")]   //找子窗体   
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        //
        [DllImport("User32.dll", EntryPoint = "SendMessage")]   //用于发送信息给窗体   
        private static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, string lParam);
        //
        [DllImport("User32.dll", EntryPoint = "ShowWindow")]   //
        private static extern bool ShowWindow(IntPtr hWnd, int type);
        //
        public void HideWindow()
        {
            windowHide = true;
            ShowWindow(GetConsoleWindow(), 0);
        }
        //
        public WatchDog()
        {
            this.TargetProgram = ConfigurationTool.GetAppSetting("TargetProgram");
            this.TargetProgramActiveFile = ConfigurationTool.GetAppSetting("TargetProgramActiveFile");
            var useFile = ConfigurationTool.GetAppSetting("useFile");

            bool usef;
            if (bool.TryParse(useFile, out usef))
            {
                this.UseFile = usef;
            }
        }
        //
        public void StopWatchDog()
        {
            if (dogDoor != null)
            {
                dogDoor.Stop();
            }
        }
        //
        public bool StartWatchDog()
        {
            if (string.IsNullOrEmpty(TargetProgram))
            {
                return false;
            }
            if (this.UseFile && string.IsNullOrEmpty(TargetProgramActiveFile))
            {
                return false;
            }
            if (this.UseFile && File.Exists(selfActiveFile) == false)
            {
                File.Create(selfActiveFile);
            }
            //先写一次
            WriteActiveFile();
            //
            if (dogDoor == null)
            {
                dogDoor = new System.Timers.Timer();
            }
            dogDoor.Interval = 10000;
            dogDoor.AutoReset = false;
            dogDoor.Elapsed += new System.Timers.ElapsedEventHandler(dogDoor_Elapsed);
            dogDoor.Start();
            //
            return true;
        }
        //
        void dogDoor_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //
            WriteActiveFile();
            //
            Process[] pro = Process.GetProcessesByName(TargetProgram);
            if (pro.Length == 0)
            {
                StartProgram();
            }
            else
            {
                //
                if (IsProgramNoActivityForWhile())
                {
                    AppTool.killApp(TargetProgram);
                }
                //杀死"程序已停止运行"的框
                AppTool.killApp("WerFault");
            }
            //
            dogDoor.Start();
        }
    }
}
