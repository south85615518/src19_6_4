﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tool;
using System.Runtime.InteropServices;
using NFnet_BLL.ProjectInfo;

namespace NFnetUnitTest.DTUServer
{
    public partial class Logwin : Form
    {
     
        public delegate void TextBoxFlush(string filename,TextBox tb);
        public event TextBoxFlush FlushEvent;
        public static DateTime timestart = DateTime.Now;
        public string mssg = "";
        public static string runmess = "";
        public static string tmpmssg = "";
        public NFnet_Interface.DisplayDataProcess.TMOS.TcpServer tcpserver { get; set; }
        public Logwin()
        {
            InitializeComponent();
            //TimerTick();
            this.timer1.Interval = 1000;
            this.timer1.Enabled = true;
            
        }
        
        private void Logwin_Load(object sender, EventArgs e)
        {
            this.timer2.Interval = 1000;
            this.timer2.Enabled = true;
            string ip = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\ipsetting.txt");
            this.textBox4.Text = ip;
            tcpserver = new NFnet_Interface.DisplayDataProcess.TMOS.TcpServer { rows = Convert.ToInt32(this.comboBox1.Text) ,IP = ip, listbox = this.listBox1 };
           tcpserver.TcpServerStart();
           
        }
        

        //WM_COPYDATA消息所要求的数据结构
        public struct CopyDataStruct
        {
            public IntPtr dwData;
            public int cbData;

            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        private const int WM_COPYDATA = 0x004A;
        //接收消息方法
        protected override void WndProc(ref System.Windows.Forms.Message e)
        {
            if (e.Msg == WM_COPYDATA)
            {
                CopyDataStruct cds = (CopyDataStruct)e.GetLParam(typeof(CopyDataStruct));
                //textBox1.Text = cds.lpData.ToString();  //将文本信息显示到文本框
                //MessageBox.Show(cds.lpData);
                string str = cds.lpData.ToString();
                List<string> ls = str.Split(',').ToList();
                this.listBox1.Invoke(new Action(() => { this.listBox1.Items.Insert(0,string.Format("接受到外部窗体发来的消息   "+str+"   "+DateTime.Now)); }));
                //Tool.FileHelper.FileInfoListWrite(ls, System.AppDomain.CurrentDomain.BaseDirectory + "\\Setting\\" + "dtusetting.txt");




            }
            base.WndProc(ref e);
        }
        public ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public void PortAdd(string str)
        {
            List<string> ls = str.Split(',').ToList();

            if (!processXmBLL.ProcessXmExist(ls[0], out mssg)) {
                ExceptionLog.ExceptionWrite(mssg);
                return; 
            }


            List<string> settingls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\Setting\\" + "dtusetting.txt");
            int i = 0;
            var lsport = (from m in settingls where m.IndexOf(".") == -1 select m).ToList();
            if (lsport.Contains(ls[1])) return;
            settingls.AddRange(ls);
            Tool.FileHelper.FileInfoListWrite(ls, System.AppDomain.CurrentDomain.BaseDirectory + "\\Setting\\" + "dtusetting.txt");

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            double sec = (DateTime.Now - timestart).TotalSeconds;
            int day = (int)sec / (24 * 3600);
            int hour = (int)(sec - day * 24 * 3600) / (3600);
            int minute = (int)(sec - day * 24 * 3600 - hour*3600) / (60);
            sec = (int)sec % 60;
            //runmess =string.Format("已运行:{0}天{1}时{2}分{3}秒", day, hour, minute, sec);
            this.label2.Invoke(new Action(() => { this.label2.Text = string.Format("已运行:{0}天{1}时{2}分{3}秒", day, hour, minute, sec); }));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (user.logined)
            {
                new XmPortSet().ShowDialog();
            }
            else
            {
                DialogResult dr = new Login().ShowDialog();
                if (dr == System.Windows.Forms.DialogResult.OK)
                {
                    new XmPortSet().ShowDialog();
                }
            }
        }

        //private void timer2_Tick(object sender, EventArgs e)
        //{

        //    this.textBox1.Invoke(new Action(() => { if (tmpmssg != NFnet_Interface.DisplayDataProcess.TMOS.TcpServer.mssg)this.textBox1.Text += "\r\n" + NFnet_Interface.DisplayDataProcess.TMOS.TcpServer.mssg; }));
        //    tmpmssg = NFnet_Interface.DisplayDataProcess.TMOS.TcpServer.mssg;
        //}

        private void button3_Click(object sender, EventArgs e)
        {
            Tool.FileHelper.FileStringWrite(this.textBox4.Text,System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\ipsetting.txt");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tcpserver.TcpServerStart();
        }

        private void Logwin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.DialogResult != DialogResult.OK)
            close();
            //this.DialogResult = DialogResult.Abort;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            close();
            this.DialogResult = DialogResult.OK;
        }
        public void close()
        {
            try
            {
                this.timer1.Enabled = false;
                tcpserver.TcpServerStopTotal();
                if (NFnet_Interface.DisplayDataProcess.TMOS.TcpServer.tcpListenerdts!=null)
                NFnet_Interface.DisplayDataProcess.TMOS.TcpServer.tcpListenerdts.Clear();
                tcpserver = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show("程序退出出错,错误信息:" + ex.Message );
            }
            finally
            {
                this.Dispose();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            tcpserver.rows = Convert.ToInt32(this.comboBox1.Text);
        }
    }
}
