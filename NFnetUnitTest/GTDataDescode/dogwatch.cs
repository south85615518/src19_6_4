﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Configuration;
using NFnet_BLL.DisplayDataProcess.pub;
namespace NFnetUnitTest.GTDataDescode
{
    public partial class dogwatch : Form
    {
        public static List<string> exelist = new List<string>();
        public bool closeop { get; set; }
        public dogwatch()
        {
            InitializeComponent();
            closeop = true;
        }

        private void dogwatch_Load(object sender, EventArgs e)
        {
            pathlistload();
            this.timer1.Interval = 1000;
            this.timer1.Enabled = true;
        }
       


        private void button2_Click(object sender, EventArgs e)
        {
            Thread importThread = new Thread(new ThreadStart(ImportDialog));
            importThread.SetApartmentState(ApartmentState.STA); //重点
            importThread.Start();
        }
        public void ImportDialog()
        {
            FileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string exePath = dialog.FileName;
                this.textBox1.Invoke(new Action(() => { this.textBox1.Text = exePath; }));

            }
        }

        public void pathlistload()
        {
            exelist = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory  + "Setting\\exepathlist.txt");
            this.listBox1.Items.Clear();
            foreach (var item in exelist)
            {
                this.listBox1.Items.Add(item);
            }
           
        }
       
       
        private void button1_Click(object sender, EventArgs e)
        {
            exelist.Add(this.textBox1.Text+"|"+this.textBox3.Text);
            Tool.FileHelper.FileInfoListWrite(exelist, System.AppDomain.CurrentDomain.BaseDirectory  + "Setting\\exepathlist.txt");
            pathlistload();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            exelist.Remove(this.listBox1.Text.ToString());
            Tool.FileHelper.FileInfoListWrite(exelist, System.AppDomain.CurrentDomain.BaseDirectory  + "Setting\\exepathlist.txt");
            pathlistload();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBox1.SelectedItems.Count == 0) this.button3.Enabled = false;
            else this.button3.Enabled = true;
        }

        public void execlose(string path)
        {
            

          System.Diagnostics.Process[] myProcesses = System.Diagnostics.Process.GetProcesses();
       
          foreach (System.Diagnostics.Process myProcess in myProcesses)
          {
              if (myProcess.ProcessName == Path.GetFileNameWithoutExtension(path))
              { myProcess.Kill(); }
                  //强制关闭该程序
          }

        }

        //public void WatchDogTimer()
        //{
        //    this.timerwatchdog.Interval = 60 * 1000 * Convert.ToInt32(this.textBox2.Text);
        //    timerwatchdog.Elapsed += new System.Timers.ElapsedEventHandler(ExeRestart);
        //    timerwatchdog.Enabled = true;
        //    timerwatchdog.AutoReset = true;
        //}

        

        private void button4_Click(object sender, EventArgs e)
        {
            this.timer1.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //timerwatchdog.Enabled = false;
            this.button2.Enabled = true;
            this.button5.Enabled = false;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.timer1.Stop();
            ProcessWinLaunchIfNotRunning.LaunchWin();
            this.timer1.Start();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            closeop = false;
            if (WindowState == FormWindowState.Minimized)
            {
                //还原窗体显示    
                WindowState = FormWindowState.Normal;
                //激活窗体并给予它焦点
                this.Activate();
                //任务栏区显示图标
                this.ShowInTaskbar = true;
                //托盘区图标隐藏
                notifyIcon1.Visible = false;
            }
        }

        private void dogwatch_SizeChanged(object sender, EventArgs e)
        {
            closeop = false;
            //判断是否选择的是最小化按钮
            if (WindowState == FormWindowState.Minimized)
            {
                //隐藏任务栏区图标
                this.ShowInTaskbar = false;
                //图标显示在托盘区
                notifyIcon1.Visible = true;
            }

        }

        private void dogwatch_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = true;
            if (closeop&&MessageBox.Show("是否确认退出程序？", "退出", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                // 关闭所有的线程
                this.Dispose();
                this.Close();
            }
            else
            {
                closeop = true;
                e.Cancel = true;
            } 
        }




    }
}
