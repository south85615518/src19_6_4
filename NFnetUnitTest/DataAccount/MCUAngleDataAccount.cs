﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.Other;
using System.Data;

namespace NFnetUnitTest.DataAccount
{
    public class MCUAngleDataAccount
    {
        public static ProcessMCUAngleDataAcc processMCUAngleDataAcc = new ProcessMCUAngleDataAcc();
        public System.Timers.Timer timer = new System.Timers.Timer(3600000);
        public static int sleepminute = 1;
        public static string mssg = "";
        public static bool mcuangleaccthrough = true;
        public static int mcuangleacctime = 0;
        public void main()
        {
            mcuangleaccthrough = true;
            Console.WriteLine("固定测斜数据重算将在"+sleepminute+"分钟后开始...");
            TimerTick();
        }
        public void datamcuangleacc(object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Enabled = false;
                timer.AutoReset = false;
                Console.WriteLine("-----固定测斜开始第" + ++mcuangleacctime + "次重算----");
                MCUAngleAcc();
            }
            catch (Exception ex)
            {
                Console.WriteLine("基康固定测斜数据重算出错，错误信息:"+ex.Message);
            }
            finally
            {
                if (mcuangleaccthrough)
                {
                    timer.Enabled = true;
                    timer.AutoReset = true;
                }
            }
        }

        public void TimerTick()
        {
            timer.Interval = sleepminute * 60 * 1000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(datamcuangleacc);
            timer.Enabled = true;
            timer.AutoReset = true;
        }
        

        public class mcuangleacc
        {
            public string pointname { get; set; }
            public double G { get; set; }
            public double R { get; set; }
            public double L { get; set; }
            public double D { get; set; }
            
        }

        public List<mcuangleacc> AccListInit()
        {
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "BKGFormulaDivisor/mcuangle.txt");
            List<mcuangleacc> lmcuangleacc = new List<mcuangleacc>();
            foreach(string str in ls )
            {
                string[] FormulaDivisor = str.Split(',');
                lmcuangleacc.Add(
                    new mcuangleacc
                    {
                        pointname = FormulaDivisor[0],
                        G = Convert.ToDouble(FormulaDivisor[1]),
                        L = Convert.ToDouble(FormulaDivisor[3]),
                        R = Convert.ToDouble(FormulaDivisor[2]),
                        D = Convert.ToDouble(FormulaDivisor[4])
                    }
                    );

            }
            return lmcuangleacc;
        }
        public void MCUAngleAcc()
        {
            List<mcuangleacc> lmcuangleacc = AccListInit();
            int i = 0;
            //string sqlcount = "select count(1) from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '韶关铀业' and st1='位移/角度' and mcuangleacc = 0  ";
            foreach (mcuangleacc acmodel in lmcuangleacc)
            {
                int j = 0;
                string sql = "select sname,pointid,r1,v2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '韶关铀业' and st1='位移/角度' and sensorset.sname='"+acmodel.pointname+"' and acc = 0  ";
                
                var queryBKGModel = new QueryBKGModel(sql);

                if (ProcessComBLL.Processquerystanderdb(queryBKGModel, out mssg))
                {

                    DataView dv = new DataView(queryBKGModel.dt);
                    
                    double r2 =0; 

                    foreach(DataRowView drv in dv)
                    {
                        try
                        {
                            if (!mcuangleaccthrough) { Console.WriteLine("已停止结算"); return; }
                            r2 = 0.5 * acmodel.G * acmodel.L * 1000 * (Convert.ToDouble(drv["v2"]) - acmodel.R) - acmodel.D;
                            if (processMCUAngleDataAcc.MCUAngleDataAcc(Convert.ToInt32(drv["pointid"]), Convert.ToDateTime(drv["dt"]), (double)Math.Round((decimal)r2, 3, MidpointRounding.AwayFromZero), out mssg))
                            {
                                Console.WriteLine("成功重算固定测斜点" + acmodel.pointname + " " + ++j + "/" + dv.Count + "条记录,第" + ++i + "条" + DateTime.Now);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(string.Format("固定测斜点{0} {1}数据重算出错，错误信息" + ex.Message, acmodel.pointname, drv["dt"]));
                            Console.WriteLine("现在审核数据...");
                            if ((drv["v2"] == null) )
                            {
                                Console.WriteLine(string.Format("固定测斜点{0}{1}测的数据结算结果不可用现在删除...",acmodel.pointname,drv["dt"]));
                                ProcessMCUAngleDataDelete processMCUAngleDataDelete = new ProcessMCUAngleDataDelete();
                                processMCUAngleDataDelete.MCUAngleDataDelete("韶关铀业", acmodel.pointname, Convert.ToDateTime(drv["dt"]), out mssg);
                                Console.WriteLine(mssg);
                            }
                        }
                    }
                    Console.WriteLine("-----------固定测斜点" + acmodel.pointname + "的所有记录重算完成共" + j + "条------------- " + DateTime.Now);
                } 
            }
            Console.WriteLine("固定测斜-----第" + mcuangleacctime + "次重算完成,共重算了" + i + "条记录---- " + DateTime.Now);
        }
    }


}
