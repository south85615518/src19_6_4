﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.Other;
using System.Data;

namespace NFnetUnitTest.DataAccount
{
    public class MCUDataAccount
    {
        public static ProcessMCUDataAcc processMCUDataAcc = new ProcessMCUDataAcc();
        public System.Timers.Timer timer = new System.Timers.Timer(3600000);
        public static int sleepminute = 1;
        public static string mssg = "";
        public static bool mcuaccthrough = true;
        public static int mcuacctime = 0;
        public void main()
        {
            mcuaccthrough = true;
            Console.WriteLine("浸润线数据重算将在" + sleepminute + "分钟后开始...");
            TimerTick();
            //MCUAcc();
        }
        public void datamcuacc(object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Enabled = false;
                timer.AutoReset = false;
                Console.WriteLine("-----浸润线开始第"+ ++ mcuacctime +"次重算----");
                MCUAcc();
            }
            catch (Exception ex)
            {
                Console.WriteLine("基康浸润线数据重算出错，错误信息:"+ex.Message);
            }
            finally
            {
                if (mcuaccthrough)
                {
                    timer.Enabled = true;
                    timer.AutoReset = true;
                }
            }
        }

        public void TimerTick()
        {
            timer.Interval = sleepminute * 60 * 1000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(datamcuacc);
            timer.Enabled = true;
            timer.AutoReset = true;
        }
        

        public class mcuacc
        {
            public string pointname { get; set; }
            public double G { get; set; }
            public double R { get; set; }
            public double L { get; set; }
            public double C { get; set; }
            public double T { get; set; }


        }
        public List<mcuacc> AccListInit()
        {
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "BKGFormulaDivisor/mcu.txt");
            List<mcuacc> lmcuacc = new List<mcuacc>();
            foreach(string str in ls )
            {
                string[] FormulaDivisor = str.Split(',');
                lmcuacc.Add(
                    new mcuacc { pointname = FormulaDivisor[0], G = Convert.ToDouble(FormulaDivisor[1]), L = Convert.ToDouble(FormulaDivisor[3]), R = Convert.ToDouble(FormulaDivisor[2]), C = Convert.ToDouble(FormulaDivisor[4]), T = Convert.ToDouble(FormulaDivisor[5]) }
                    );

            }
            return lmcuacc;
        }
        public void MCUAcc()
        {
            List<mcuacc> lmcuacc = AccListInit();
            int i = 0;
            //string sqlcount = "select count(1) from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '韶关铀业' and st1='位移/角度' and mcuacc = 0  ";
            foreach (mcuacc acmodel in lmcuacc)
            {
                int j = 0;
                string sql = "select sname,pointid,v1,mr2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '韶关铀业' and st1='应力' and sensorset.sname='"+acmodel.pointname+"' and acc = 0  ";
                
                var queryBKGModel = new QueryBKGModel(sql);

                if (ProcessComBLL.Processquerystanderdb(queryBKGModel, out mssg))
                {

                    DataView dv = new DataView(queryBKGModel.dt);
                    
                    double r2 =0; 

                    foreach(DataRowView drv in dv)
                    {
                        try
                        {
                            if (!mcuaccthrough) { Console.WriteLine("已停止结算"); return; }
                            
                            r2 = (double)Math.Round((decimal)(acmodel.L - (acmodel.G * (Convert.ToDouble(drv["v1"]) - acmodel.R) + acmodel.C * (Convert.ToDouble(drv["mr2"]) - acmodel.T)) * 0.102), 3, MidpointRounding.AwayFromZero);//0.5 * acmodel.G * acmodel.L*1000 * (Convert.ToDouble(drv["v2"]) - acmodel.R);
                            if (processMCUDataAcc.MCUDataAcc(Convert.ToInt32(drv["pointid"]), Convert.ToDateTime(drv["dt"]), (double)Math.Round((decimal)r2, 3, MidpointRounding.AwayFromZero), out mssg))
                            {
                                Console.WriteLine("成功重算浸润线点" + acmodel.pointname + " " + ++j + "/" + dv.Count + "条记录,第" + ++i + "条" + DateTime.Now);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(string.Format("浸润线点{0} {1}数据重算出错，错误信息" + ex.Message, acmodel.pointname, drv["dt"]));
                            Console.WriteLine("现在审核数据...");
                            if (drv["v1"] == null || drv["mr2"] == null || string.IsNullOrEmpty(drv["v1"].ToString()) || string.IsNullOrEmpty(drv["mr2"].ToString()))
                            {
                                Console.WriteLine(string.Format("浸润线点{0}{1}测的数据结算结果不可用现在删除...", acmodel.pointname, drv["dt"]));
                                ProcessMCUDataDelete processMCUDataDelete = new ProcessMCUDataDelete();
                                processMCUDataDelete.MCUDataDelete("韶关铀业", acmodel.pointname,Convert.ToDateTime(drv["dt"]),out mssg);
                                Console.WriteLine(mssg);
                            }
                        }
                    }
                    Console.WriteLine("-------------浸润线"+acmodel.pointname+"的所有记录重算完成共"+j+"条------------ "+DateTime.Now);
                } 
            }
            Console.WriteLine("浸润线-----第" + mcuacctime + "次重算完成,共重算了"+i+"条记录---- "+DateTime.Now);
        }
    }


}
