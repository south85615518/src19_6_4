﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
namespace NFnetUnitTest.UTCTest
{
    public class testUTC
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public void main()
        {
            UTCTimeSerialize();
        }
        public void UTCTimeSerialize()
        {
            utctime t = new utctime(Convert.ToDateTime("2018/2/23 1:11:52") );
            string tstr = jss.Serialize(t);
            
            utctime tdeserialize = (utctime)jss.Deserialize(tstr, typeof(utctime));
            ProcessPrintMssg.Print(string.Format("{0}反序列化后的时间是{1}",t.dt,tdeserialize.dt));
        }
        public class utctime
        {
            public DateTime dt { get; set; }
            public utctime(DateTime dt)
            {
                this.dt = dt;
            }
            public utctime()
            {
                
            }
        }
    }
}
