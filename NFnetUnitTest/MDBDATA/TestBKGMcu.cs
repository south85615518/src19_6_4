﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;
using NFnet_MODAL;

namespace NFnetUnitTest.MDB
{
    public class TestBKGMcu
    {
        public static ProcessBKGMCUPointLoad processBKGMCUPointLoad = new ProcessBKGMCUPointLoad();
        public static ProcessBKGMCUData processBKGMCUData = new ProcessBKGMCUData();
        public static ProcessMCUChart processMCUChart = new ProcessMCUChart();
        public static ProcessBKGMCUMaxTime processBKGMCUMaxTime = new ProcessBKGMCUMaxTime();
        public static ProcessMCUDataDelete processMCUDataDelete = new ProcessMCUDataDelete();
        public static ProcessMCUPointNewestDateTimeGet processMCUPointNewestDateTimeGet = new ProcessMCUPointNewestDateTimeGet();
        public static ProcessMCUDateTime processMCUDateTime = new ProcessMCUDateTime();
        public static ProcessMCUTimeIntervalTableLoad processMCUTimeIntervalTableLoad = new ProcessMCUTimeIntervalTableLoad();
        public static string mssg = "";
        public static string sql = "";
        public static string pointnamestr = "";
        //public static string mssg = "";
        public void main()
        {
            //BKGMcuAnglePointLoad();
            //BKGMcuDataTableLoad();
            //BKGMcuSerializestr();
            //TestBKGMcuMaxTime();
            //TestResultDataRqcxConditionCreate();
            //TestMCUDATADelete();
            TestMCUPointNewestDateTimeGet();
            TestMCUDateTime();
        }
        public void BKGMcuPointLoad()
        {
            processBKGMCUPointLoad.BKGMCUPointLoad("韶关铀业",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void BKGMcuDataTableLoad()
        {
            var resultDataRqcxConditionCreateCondition  = new ResultDataRqcxConditionCreateCondition("2018/2/5 12:12:34","2018/2/11 13:50:34",QueryType.RQCX,"","韶关铀业",new DateTime() );
            processBKGMCUData.ProcessResultDataRqcxConditionCreate(resultDataRqcxConditionCreateCondition);
            var fillInclinometerDbFillCondition = new FillBKGDbFillCondition("韶关745-J2-1,韶关745-J2-2", resultDataRqcxConditionCreateCondition.sql,"");
           
            processBKGMCUData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
            sql = fillInclinometerDbFillCondition.sql;
            pointnamestr = "韶关745-J2-1,韶关745-J2-2";
            ProcessPrintMssg.Print(string.Format("成功获取项目{0}的记录数{1}","韶关745",fillInclinometerDbFillCondition.dt.Rows.Count));
        }
        public void BKGMcuSerializestr()
        {
            //string sql = "";
            BKGMcuDataTableLoad();
            //var processChartCondition = new ProcessChartCondition(sql, "韶关745", pointnamestr,0);
            List<serie> ls = processMCUChart.SerializestrBKGMCU(sql, "韶关745", pointnamestr, out mssg);
            //ProcessPrintMssg.Print("");
        }
        public void TestBKGMcuMaxTime()
        {
            processBKGMCUMaxTime.BKGMCUMaxTime("韶关铀业",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public ProcessBKGMCUDataRqcxConditionCreate processBKGMCUDataRqcxConditionCreate = new ProcessBKGMCUDataRqcxConditionCreate();
        public void TestResultDataRqcxConditionCreate()
        {
            var model = processBKGMCUDataRqcxConditionCreate.ResultDataRqcxConditionCreate("","",QueryType.QT,"day","韶关铀业",DateTime.Now);
            ProcessPrintMssg.Print(model.sql);
        }
        public void TestMCUDATADelete()
        {
            processMCUDataDelete.MCUDataDelete("韶关铀业", "韶关745-J1-1", DateTime.Parse("2018/1/31 13:35:56"),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMCUPointNewestDateTimeGet()
        {
            processMCUPointNewestDateTimeGet.MCUPointNewestDateTimeGet("韶关铀业", "韶关745-J1-1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMCUDateTime()
        {
            DateTime dt = processMCUPointNewestDateTimeGet.MCUPointNewestDateTimeGet("韶关铀业", "韶关745-J1-1",out mssg);
            processMCUDateTime.MCUDateTime("韶关铀业", "韶关745-J1-1",dt ,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTimeIntervalTableLoad()
        {
            processMCUTimeIntervalTableLoad.MCUTimeIntervalTableLoad(29," 1=1 ",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
