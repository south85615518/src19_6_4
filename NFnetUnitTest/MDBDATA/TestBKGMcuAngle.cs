﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;
using NFnet_MODAL;

namespace NFnetUnitTest.MDB
{
    public class TestBKGMcuAngle
    {
        public static ProcessBKGMCUAnglePointLoad processBKGMCUAnglePointLoad = new ProcessBKGMCUAnglePointLoad();
        public static ProcessBKGMCUAngleData processBKGMCUAngleData = new ProcessBKGMCUAngleData();
        public static ProcessMCUAngleChart processMCUAngleChart = new ProcessMCUAngleChart();
        public static ProcessBKGMCUAngleMaxTime processBKGMCUAngleMaxTime = new ProcessBKGMCUAngleMaxTime();
        public static ProcessMCUAngleDataDelete processMCUAngleDataDelete = new ProcessMCUAngleDataDelete();
        public static ProcessMCUAngleDataAcc processMCUAngleDataAcc = new ProcessMCUAngleDataAcc();
        public static ProcessMCUAnglePointNewestDateTimeGet processMCUAnglePointNewestDateTimeGet = new ProcessMCUAnglePointNewestDateTimeGet();
        public static ProcessMCUAngleDateTime processMCUAngleDateTime = new ProcessMCUAngleDateTime();
        public static string mssg = "";
        public static string sql = "";
        public static string pointnamestr = "";
        public void main()
        {
              //BKGMcuAnglePointLoad();
            //BKGMcuDataTableLoad();
            //BKGMcuAngleSerializestr();
            //TestBKGMCUAngleMaxTime();
            //TestResultDataRqcxConditionCreate();
            //TestMCUAngleDATADelete();
            //TestMCUAngleAcc();
            TestMCUAnglePointNewestDateTimeGet();
            TestMCUAngleDateTime();

        }
        public void BKGMcuAnglePointLoad()
        {
            processBKGMCUAnglePointLoad.BKGMCUAnglePointLoad("韶关铀业",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void BKGMcuDataTableLoad()
        {
            var resultDataRqcxConditionCreateCondition = new ResultDataRqcxConditionCreateCondition("2018/1/21 12:12:34", "2018/1/21 13:50:34", QueryType.RQCX, "", "韶关铀业", new DateTime());
            processBKGMCUAngleData.ProcessResultDataRqcxConditionCreate(resultDataRqcxConditionCreateCondition);
            var fillInclinometerDbFillCondition = new FillBKGDbFillCondition("韶关745-N2-1,韶关745-N2-2", resultDataRqcxConditionCreateCondition.sql,"");
            
            processBKGMCUAngleData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
            sql = fillInclinometerDbFillCondition.sql;
            pointnamestr = "韶关745-N2-1,韶关745-N2-2,韶关745-N2-3,韶关745-N3-1,韶关745-N3-2,韶关745-N3-3";
            ProcessPrintMssg.Print(string.Format("成功获取项目{0}的记录数{1}","韶关745",fillInclinometerDbFillCondition.dt.Rows.Count));
        }
        public void BKGMcuAngleSerializestr()
        {
            //string sql = "";
            BKGMcuDataTableLoad();
            //var processChartCondition = new ProcessChartCondition(sql, "韶关745", pointnamestr,0);
           List<serie> ls = processMCUAngleChart.SerializestrBKGMCUAngle(sql,"韶关铀业",pointnamestr,out mssg);
           //ProcessPrintMssg.Print("");
        }
        public void TestBKGMCUAngleMaxTime()
        {
            processBKGMCUAngleMaxTime.BKGMCUAngleMaxTime("韶关铀业", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public ProcessBKGMCUAngleDataRqcxConditionCreate processBKGMCUAngleDataRqcxConditionCreate = new ProcessBKGMCUAngleDataRqcxConditionCreate();
        public void TestResultDataRqcxConditionCreate()
        {
            var model = processBKGMCUAngleDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018-1-21 19:59:46", "2018-1-23 19:59:46", QueryType.RQCX, "day", "韶关铀业", new DateTime());
            ProcessPrintMssg.Print(model.sql);
        }
        public void TestMCUAngleDATADelete()
        {
            processMCUAngleDataDelete.MCUAngleDataDelete("韶关铀业", "韶关745-N2-1", DateTime.Parse("2018/1/31 13:30:07"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMCUAngleAcc()
        {
            processMCUAngleDataAcc.MCUAngleDataAcc(1,DateTime.Parse("2018/1/21 12:23:49"),0.666666,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMCUAnglePointNewestDateTimeGet()
        {
            processMCUAnglePointNewestDateTimeGet.MCUAnglePointNewestDateTimeGet("韶关铀业", "韶关745-N2-1", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMCUAngleDateTime()
        {
            DateTime dt = processMCUAnglePointNewestDateTimeGet.MCUAnglePointNewestDateTimeGet("韶关铀业", "韶关745-N2-1", out mssg);
            processMCUAngleDateTime.MCUAngleDateTime("韶关铀业", "韶关745-N2-1", dt, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
