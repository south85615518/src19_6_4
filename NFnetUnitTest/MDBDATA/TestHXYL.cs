﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.HXYL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnetUnitTest.MDB
{
    public class TestHXYL
    {
        //public static string mssg = "";
        public static ProcessHXYLPointBLL processHXYLPointBLL = new ProcessHXYLPointBLL();
        public static ProcessHXYLData processHXYLData = new ProcessHXYLData();
        public static ProcessHXYLChart processHXYLChart = new ProcessHXYLChart();
        public static ProcessHXYLIDBLL processHXYLIDBLL = new ProcessHXYLIDBLL();
        public static ProcessHXYLMaxTime processHXYLMaxTime = new ProcessHXYLMaxTime();
        public static ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public static ProcessHXYLDataDelete processHXYLDataDelete = new ProcessHXYLDataDelete();
        public static ProcessHXYLPointNewestDateTimeGet processHXYLPointNewestDateTimeGet = new ProcessHXYLPointNewestDateTimeGet();
        public static ProcessHXYLDateTime processHXYLDateTime = new ProcessHXYLDateTime();
        public static string mssg = "";
        public static string sql = "";
        public static string pointnamestr = "";
        public void main()
        {
            //TestHXYLPoint();
            HXYLDataTableLoad();
            //HXYLSerializestr();
            //HXYLMaxTime();
            //TestHXYLAdd();
            //TestHXYLUpdate();
            //TestMaxTime();
            //TestHXYLDATALoad();
            //HXYLDATALoad();
            //TestHXYLDelivery();
            //TestHXYLMDBAdd();
            //TestHXYLDATADelete();
            //TestHXYLPointNewestDateTimeGet();
            //TestHXYLDateTime();
        }
        public void TestHXYLPoint()
        {
            processHXYLPointBLL.HXYLPoint(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void HXYLDataTableLoad()
        {
            var resultDataRqcxConditionCreateCondition = new ResultDataRqcxConditionCreateCondition("2018/2/7 16:12:34", "2018/2/10 16:50:34", QueryType.RQCX, "", "韶关铀业", new DateTime());
            string[] pointnames = "雨量计3号设备".Split(',');
            List<string> lis = new List<string>();
            foreach (var pointname in pointnames)
            {
                lis.Add(processHXYLIDBLL.HXYLPointID(29, "雨量计3号设备", out mssg));
            }

            processHXYLData.ProcessResultDataRqcxConditionCreateMon(resultDataRqcxConditionCreateCondition);
            var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition(string.Join(",",lis), resultDataRqcxConditionCreateCondition.sql, 29);

            processHXYLData.ProcessInclinometerDbFillMon(fillInclinometerDbFillCondition);
            sql = fillInclinometerDbFillCondition.sql;
            pointnamestr = "雨量计3号设备";
            ProcessPrintMssg.Print(string.Format("成功获取项目{0}的记录数{1}", "韶关745", fillInclinometerDbFillCondition.dt.Rows.Count));
        }
        public void HXYLSerializestr()
        {
            //string sql = "";
            HXYLDataTableLoad();
            //var processChartCondition = new ProcessChartCondition(sql, "韶关745", pointnamestr,0);
            List<serie> ls = processHXYLChart.SerializestrHXYL(sql, 29, pointnamestr, out mssg);
            //ProcessPrintMssg.Print("");
        }
        public void HXYLMaxTime()
        {
            string[] pointnames = "雨量计3号设备".Split(',');
            List<string> lis = new List<string>();
            foreach (var pointname in pointnames)
            {
                lis.Add(processHXYLIDBLL.HXYLPointID(29, "雨量计3号设备", out mssg));
            }
            processHXYLMaxTime.HXYLMaxTime(string.Join(",",lis),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        MDBDATA.Model.hxyl model = new MDBDATA.Model.hxyl { 
             id = "3", val = 0.1, time= Convert.ToDateTime("2018-1-25 16:21:00"), send = false
        };
        public void TestHXYLAdd()
        {
            processHXYLBLL.ProcessHXYLAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestHXYLUpdate()
        {
            processHXYLBLL.ProcessHXYLUpdate(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMaxTime()
        {
            DateTime maxTime = new DateTime();
            processHXYLBLL.ProcessMaxTime(out maxTime,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestHXYLDATALoad()
        {
            List<MDBDATA.Model.hxyl> ls = new List<MDBDATA.Model.hxyl>();
            processHXYLBLL.ProcessHXYLDATALoad(out ls,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public ProcessHXYLDATALoad processHXYLDATALoad = new ProcessHXYLDATALoad();
        public void HXYLDATALoad()
        {
            List<MDBDATA.Model.hxyl> ls = processHXYLDATALoad.HXYLDATALoad(Convert.ToDateTime("2017-3-4 12:00:00"),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestHXYLDelivery()
        {
            FMOS_SEND_INTERFACE.HXYLdeliver.ProcessHXYLdeliver();
        }
        public void TestHXYLMDBAdd()
        {
            processHXYLBLL.ProcessHXYLAddMDB(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestHXYLDATADelete()
        {
            processHXYLDataDelete.HXYLDataDelete("韶关铀业", "3", DateTime.Parse("2017/3/20 14:56:00"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestHXYLPointNewestDateTimeGet()
        {
            processHXYLPointNewestDateTimeGet.HXYLPointNewestDateTimeGet(1, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestHXYLDateTime()
        {
            DateTime dt = processHXYLPointNewestDateTimeGet.HXYLPointNewestDateTimeGet(1, out mssg);
            processHXYLDateTime.HXYLDateTime(1, dt, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
