﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GNSS;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnit.SGALARM
{
    public class GNSSDATAAlarm
    {
        public ProcessGNSSAlarmModelListGet processGNSSAlarmModelListGet = new ProcessGNSSAlarmModelListGet();
        public ProcessGNSSPointLoadBLL processGNSSPointLoadBLL = new ProcessGNSSPointLoadBLL();
        //public static ProcessBKGGNSSPointLoad processBKGGNSSPointLoad = new ProcessBKGGNSSPointLoad();
        public ProcessGNSSAlarmDataCount processGNSSAlarmDataCount = new ProcessGNSSAlarmDataCount();
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public static ProcessSmsSendBLL smsSendBLL = new ProcessSmsSendBLL();
        public string xmname { get; set; }
        public string mssg = "";
        public void main()
        {
            //GNSSAlarmModelListGet();
            //DATAAlarm();
            GNSSAlarmDataCount();
        }
        public void GNSSAlarmModelListGet()
        {
            List<string> pointnamelist = processGNSSPointLoadBLL.GNSSPointLoadBLL(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            processGNSSAlarmModelListGet.GNSSAlarmModelListGet(Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointnamelist, out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public int GNSSAlarmDataCount()
        {
            List<string> pointnamelist = processGNSSPointLoadBLL.GNSSPointLoadBLL(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);

            return processGNSSAlarmDataCount.GNSSAlarmDataCount(pointnamelist, out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public void DATAAlarm()
        {
            List<string> pointnamelist = processGNSSPointLoadBLL.GNSSPointLoadBLL(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);

            ProcessGNSSDataAlarmBLL processGNSSDataAlarmBLL = new ProcessGNSSDataAlarmBLL(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointnamelist);
            processGNSSDataAlarmBLL.main();
            emailSendBLL.ProcessEmailSend(processGNSSDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            smsSendBLL.ProcessSmsSend(processGNSSDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            Console.WriteLine("{0} 发送预警通知：{1}成功", DateTime.Now, string.Join("\r\n", processGNSSDataAlarmBLL.alarmInfoList));
            //ProcessPrintMssg.Print(string.Join(",", processGNSSDataAlarmBLL.alarmInfoList));
        }
    }
}

