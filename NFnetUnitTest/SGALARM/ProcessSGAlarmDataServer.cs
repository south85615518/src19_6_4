﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;
using NFnetUnitTest.TestDataDisplay;
using NFnetUnit.SGALARM;

namespace NFnetUnitTest.SGALARM
{
    public class ProcessSGAlarmDataServer
    {
        //表中记录数
        public static int mcucont = 0;
        public static int mcuAnglecont = 0;
        public static int hxylcont = 0;
        public static int gnsscont = 0;
        public static int gaugecont = 0;

        public static int mcutablecont = 0;
        public static int mcuAngletablecont = 0;
        public static int hxyltablecont = 0;
        public static int gnsstablecont = 0;
        public static int gaugetablecont = 0;

        public static System.Timers.Timer timermcu = new System.Timers.Timer(60000);
        public static System.Timers.Timer timermcuangle = new System.Timers.Timer(60000);
        public static System.Timers.Timer timerhxyl = new System.Timers.Timer(60000);
        public static System.Timers.Timer timergnss = new System.Timers.Timer(60000);
        public static System.Timers.Timer timergauge = new System.Timers.Timer(60000);

        public static MCUDATAAlarm_ mCUDATAAlarm = new MCUDATAAlarm_();
        public static MCUAngleDATAAlarm_ mCUAngleDATAAlarm = new MCUAngleDATAAlarm_();
        public static HXYLDATAAlarm hXYLDATAAlarm = new HXYLDATAAlarm();
        public static GNSSDATAAlarm gNSSDATAAlarm = new GNSSDATAAlarm();
        public static GaugeDATAAlarm gaugeDATAAlarm = new GaugeDATAAlarm();

        public static string xmname = "韶关铀业";
        public static void MCUalarm(object source, System.Timers.ElapsedEventArgs e)
        {
            timermcu.Elapsed -= new System.Timers.ElapsedEventHandler(MCUalarm);
            timermcu.Enabled = false;
            timermcu.AutoReset = false;
            Console.WriteLine(string.Format("{1}  开始执行浸润线第{0}次巡检", ++mcucont,DateTime.Now));
            mCUDATAAlarm.xmname = xmname;
            int cont = mCUDATAAlarm.MCUAlarmDataCount();
            Console.WriteLine("当前浸润线数据记录数为{0}之前数据记录数{1}", cont, mcutablecont);
            if (cont != mcutablecont)
            {
                mcutablecont = cont;
                mCUDATAAlarm.MCUDATAAlarm();
                
            }
            else
                Console.WriteLine("浸润线所选点号的数据没有更新，不触发预警    {0}",DateTime.Now);
            //testMCUDATAAlarm.MCUDATAAlarm();
            timermcu.Elapsed += new System.Timers.ElapsedEventHandler(MCUalarm);
            timermcu.Enabled = true;
            timermcu.AutoReset = true;

        }
        public static void MCUAnglealarm(object source, System.Timers.ElapsedEventArgs e)
        {
            timermcuangle.Elapsed -= new System.Timers.ElapsedEventHandler(MCUAnglealarm);
            timermcuangle.Enabled = false;
            timermcuangle.AutoReset = false;
            Console.WriteLine(string.Format("{1}  开始执行固定测斜第{0}次巡检", ++mcuAnglecont,DateTime.Now));
            mCUAngleDATAAlarm.xmname = xmname;
            int cont = mCUAngleDATAAlarm.MCUAlarmDataCount();
            Console.WriteLine("当前固定测斜数据记录数为{0}之前数据记录数{1}", cont, mcuAngletablecont);
            if (cont != mcuAngletablecont)
            {
                mcuAngletablecont = cont;
                mCUAngleDATAAlarm.MCUAngleDATAAlarm();
            }
            else
                Console.WriteLine("固定测斜所选点号的数据没有更新，不触发预警   {0}",DateTime.Now);
            //testMCUDATAAlarm.MCUDATAAlarm();
            timermcuangle.Elapsed += new System.Timers.ElapsedEventHandler(MCUAnglealarm);
            timermcuangle.Enabled = true;
            timermcuangle.AutoReset = true;
        }
        public static void HXYLalarm(object source, System.Timers.ElapsedEventArgs e)
        {
            timerhxyl.Elapsed -= new System.Timers.ElapsedEventHandler(HXYLalarm);
            timerhxyl.Enabled = false;
            timerhxyl.AutoReset = false;
            Console.WriteLine(string.Format("{1}  开始执行雨量第{0}次巡检", ++hxylcont,DateTime.Now));
            hXYLDATAAlarm.xmname = xmname;
            int cont = hXYLDATAAlarm.HXYLAlarmDataCount();
            Console.WriteLine("当前雨量数据记录数为{0}之前数据记录数{1}", cont, hxyltablecont);
            if (cont != hxyltablecont)
            {
                hxyltablecont = cont;
                hXYLDATAAlarm.DATAAlarm();
            }
            else
                Console.WriteLine("雨量所选点号的数据没有更新，不触发预警    {0}",DateTime.Now);
            //testMCUDATAAlarm.MCUDATAAlarm();
            timerhxyl.Elapsed += new System.Timers.ElapsedEventHandler(HXYLalarm);
            timerhxyl.Enabled = true;
            timerhxyl.AutoReset = true;
        }
        public static void GNSSalarm(object source, System.Timers.ElapsedEventArgs e)
        {
            timergnss.Elapsed -= new System.Timers.ElapsedEventHandler(GNSSalarm);
            timergnss.Enabled = false;
            timergnss.AutoReset = false;
            Console.WriteLine(string.Format("{1}   开始执行GNSS第{0}次巡检", ++gnsscont,DateTime.Now));
            gNSSDATAAlarm.xmname = xmname;
            int cont = gNSSDATAAlarm.GNSSAlarmDataCount();
            Console.WriteLine("当前GNSS数据记录数为{0}之前数据记录数{1}", cont, gnsstablecont);
            if (cont != gnsstablecont) 
            {
                gnsstablecont = cont;
                gNSSDATAAlarm.DATAAlarm(); 
            }
            else
                Console.WriteLine("GNSS所选点号的数据没有更新，不触发预警    {0}",DateTime.Now);
            //testMCUDATAAlarm.MCUDATAAlarm();
            timergnss.Elapsed += new System.Timers.ElapsedEventHandler(GNSSalarm);
            timergnss.Enabled = true;
            timergnss.AutoReset = true;
        }
        public static void Gaugealarm(object source, System.Timers.ElapsedEventArgs e)
        {
            timergauge.Elapsed -= new System.Timers.ElapsedEventHandler(Gaugealarm);
            timergauge.Enabled = false;
            timergauge.AutoReset = false;
            Console.WriteLine(string.Format("{1}    开始执行库水位第{0}次巡检", ++gaugecont,DateTime.Now));
            gaugeDATAAlarm.xmname = xmname;
            int cont = gaugeDATAAlarm.GaugeAlarmDataCount();
            Console.WriteLine("当前库水位数据记录数为{0}之前数据记录数{1}", cont, gaugetablecont);
            if (cont != gaugetablecont)
            {
                gaugetablecont = cont;
                gaugeDATAAlarm.DATAAlarm();
            }
            else
                Console.WriteLine("库水位所选点号的数据没有更新，不触发预警    {0}",DateTime.Now);
            //testMCUDATAAlarm.MCUDATAAlarm();
            timergauge.Elapsed += new System.Timers.ElapsedEventHandler(Gaugealarm);
            timergauge.Enabled = true;
            timergauge.AutoReset = true;
        }

        public static void init(string xmname)
        {
            ProcessSGAlarmDataServer.xmname = xmname;
            timermcu.Elapsed += new System.Timers.ElapsedEventHandler(MCUalarm);
            timermcu.Enabled = true;
            timermcu.AutoReset = true;

            timermcuangle.Elapsed += new System.Timers.ElapsedEventHandler(MCUAnglealarm);
            timermcuangle.Enabled = true;
            timermcuangle.AutoReset = true;

            timerhxyl.Elapsed += new System.Timers.ElapsedEventHandler(HXYLalarm);
            timerhxyl.Enabled = true;
            timerhxyl.AutoReset = true;

            timergnss.Elapsed += new System.Timers.ElapsedEventHandler(GNSSalarm);
            timergnss.Enabled = true;
            timergnss.AutoReset = true;

            timergauge.Elapsed += new System.Timers.ElapsedEventHandler(Gaugealarm);
            timergauge.Enabled = true;
            timergauge.AutoReset = true;

        }
        public static void TimerReset(System.Timers.Timer timer,int sec)
        {
            timer.Stop();
            timer.Interval = sec;
            timer.Start();
        }
    }
}
