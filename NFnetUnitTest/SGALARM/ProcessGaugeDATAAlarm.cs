﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GAUGE;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnit.SGALARM
{
    public class GaugeDATAAlarm
    {
        public ProcessGaugeAlarmModelListGet processGaugeAlarmModelListGet = new ProcessGaugeAlarmModelListGet();
        public ProcessGaugePointLoadBLL processGaugePointLoadBLL = new ProcessGaugePointLoadBLL();
        //public static ProcessBKGGaugePointLoad processBKGGaugePointLoad = new ProcessBKGGaugePointLoad();
        public ProcessGaugeAlarmDataCount processGaugeAlarmDataCount = new ProcessGaugeAlarmDataCount();
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public static ProcessSmsSendBLL smsSendBLL = new ProcessSmsSendBLL();
        public string xmname { get; set; }
        public string mssg = "";
        public void main()
        {
            //GaugeAlarmModelListGet();
            //DATAAlarm();
            GaugeAlarmDataCount();
        }
        public void GaugeAlarmModelListGet()
        {
            List<string> pointnamelist = processGaugePointLoadBLL.GaugePointLoadBLL(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            processGaugeAlarmModelListGet.GaugeAlarmModelListGet(pointnamelist, out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public int GaugeAlarmDataCount()
        {
            List<string> pointnamelist = processGaugePointLoadBLL.GaugePointLoadBLL(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            
            return processGaugeAlarmDataCount.GaugeAlarmDataCount( pointnamelist, out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public void DATAAlarm()
        {
            List<string> pointnamelist = processGaugePointLoadBLL.GaugePointLoadBLL(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);

            ProcessGaugeDataAlarmBLL processGaugeDataAlarmBLL = new ProcessGaugeDataAlarmBLL(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointnamelist);
            processGaugeDataAlarmBLL.main();
            emailSendBLL.ProcessEmailSend(processGaugeDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            smsSendBLL.ProcessSmsSend(processGaugeDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            Console.WriteLine("{0} 发送预警通知：{1}成功", DateTime.Now, string.Join("\r\n", processGaugeDataAlarmBLL.alarmInfoList));
            //ProcessPrintMssg.Print(string.Join(",", processGaugeDataAlarmBLL.alarmInfoList));
        }
       
    }
}

