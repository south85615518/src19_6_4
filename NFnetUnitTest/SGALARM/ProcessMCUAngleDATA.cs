﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnit.SGALARM
{
    public class MCUAngleDATAAlarm_
    {
        public ProcessMCUAngleAlarmModelListGet processMCUAngleAlarmModelListGet = new ProcessMCUAngleAlarmModelListGet();
        public ProcessMCUAngleAlarmDataCount processMCUAngleAlarmDataCount = new ProcessMCUAngleAlarmDataCount();
        public static ProcessBKGMCUAnglePointLoad processBKGMCUAnglePointLoad = new ProcessBKGMCUAnglePointLoad();
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public static ProcessSmsSendBLL smsSendBLL = new ProcessSmsSendBLL();
        public string xmname { get; set; }
        public string mssg = "";
        //public static 
        public void MCUAngleAlarmModelListGet()
        {
            List<string> pointnamelist = processBKGMCUAnglePointLoad.BKGMCUAnglePointLoad(xmname, out mssg);
            processMCUAngleAlarmModelListGet.MCUAngleAlarmModelListGet(xmname, pointnamelist, out mssg);
            ////ProcessPrintMssg.Print(mssg);

        }
        public int MCUAlarmDataCount()
        {
            List<string> pointnamelist = processBKGMCUAnglePointLoad.BKGMCUAnglePointLoad(xmname, out mssg);
            return processMCUAngleAlarmDataCount.MCUAngleAlarmDataCount(xmname, pointnamelist, out mssg);
            ////ProcessPrintMssg.Print(mssg);
        }
        public void MCUAngleDATAAlarm()
        {
            List<string> pointnamelist = processBKGMCUAnglePointLoad.BKGMCUAnglePointLoad(xmname, out mssg);
            ProcessMCUAngleDataAlarmBLL processMCUAngleDataAlarmBLL = new ProcessMCUAngleDataAlarmBLL(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointnamelist);
            processMCUAngleDataAlarmBLL.main();
            emailSendBLL.ProcessEmailSend(processMCUAngleDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            smsSendBLL.ProcessSmsSend(processMCUAngleDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
        
            Console.WriteLine("{0} 发送预警通知：{1}成功", DateTime.Now, string.Join("\r\n", processMCUAngleDataAlarmBLL.alarmInfoList));
            //ProcessPrintMssg.Print(string.Join(",", processMCUAngleDataAlarmBLL.alarmInfoList));
        }
    }
}
