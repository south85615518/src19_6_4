﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.HXYL;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnit.SGALARM
{
    public class HXYLDATAAlarm
    {
        public ProcessHXYLAlarmModelListGet processHXYLAlarmModelListGet = new ProcessHXYLAlarmModelListGet();
        public static ProcessHXYLPointBLL processHXYLPointBLL = new ProcessHXYLPointBLL();
        public static ProcessHXYLIDBLL processHXYLIDBLL = new ProcessHXYLIDBLL();
        public static ProcessHXYLAlarmDataCount processHXYLAlarmDataCount = new ProcessHXYLAlarmDataCount();
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public static ProcessSmsSendBLL smsSendBLL = new ProcessSmsSendBLL();
        public string xmname { get; set; }
        public string mssg = "";
        public void main()
        {
            //HXYLAlarmModelListGet();
            //DATAAlarm();
            HXYLAlarmDataCount();
        }
        public void HXYLAlarmModelListGet()
        {
            List<string> pointnamelist = processHXYLPointBLL.HXYLPoint(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            List<string> lis = new List<string>();
            foreach (var pointname in pointnamelist)
            {
                lis.Add(processHXYLIDBLL.HXYLPointID(Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointname, out mssg));
            }
            processHXYLAlarmModelListGet.HXYLAlarmModelListGet(lis, out mssg);
            ////ProcessPrintMssg.Print(mssg);
        }
        public int HXYLAlarmDataCount()
        {
            List<string> pointnamelist = processHXYLPointBLL.HXYLPoint(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            List<string> lis = new List<string>();
            foreach (var pointname in pointnamelist)
            {
                lis.Add(processHXYLIDBLL.HXYLPointID(Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointname, out mssg));
            }
            return processHXYLAlarmDataCount.HXYLAlarmDataCount(xmname, lis, out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public void DATAAlarm()
        {
            List<string> pointnamelist = processHXYLPointBLL.HXYLPoint(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            List<string> lis = new List<string>();
            foreach (var pointname in pointnamelist)
            {
                lis.Add(processHXYLIDBLL.HXYLPointID(Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointname, out mssg));
            }
            ProcessHXYLDataAlarmBLL processHXYLDataAlarmBLL = new ProcessHXYLDataAlarmBLL(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), lis);
            processHXYLDataAlarmBLL.main();
            emailSendBLL.ProcessEmailSend(processHXYLDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            smsSendBLL.ProcessSmsSend(processHXYLDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            Console.WriteLine("{0} 发送预警通知：{1}成功", DateTime.Now, string.Join("\r\n", processHXYLDataAlarmBLL.alarmInfoList));
            //ProcessPrintMssg.Print(string.Join(",", processHXYLDataAlarmBLL.alarmInfoList));
        }
    }
}
