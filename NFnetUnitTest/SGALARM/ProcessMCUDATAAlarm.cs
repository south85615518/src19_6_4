﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnit.SGALARM
{
    public class MCUDATAAlarm_
    {
        public ProcessMCUAlarmModelListGet processMCUAlarmModelListGet = new ProcessMCUAlarmModelListGet();
        public static ProcessBKGMCUPointLoad processBKGMCUPointLoad = new ProcessBKGMCUPointLoad();
        public ProcessMCUAlarmDataCount processMCUAlarmDataCount = new ProcessMCUAlarmDataCount();
        public string mssg = "";
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public static ProcessSmsSendBLL smsSendBLL = new ProcessSmsSendBLL();
        public string xmname { get; set; }
        public void MCUAlarmModelListGet()
        {
            List<string> pointnamelist = processBKGMCUPointLoad.BKGMCUPointLoad(xmname,out mssg);
            processMCUAlarmModelListGet.MCUAlarmModelListGet(xmname, pointnamelist,out mssg);
            ////ProcessPrintMssg.Print(mssg);
        }
        public int MCUAlarmDataCount()
        {
            List<string> pointnamelist = processBKGMCUPointLoad.BKGMCUPointLoad(xmname,out mssg);
            return processMCUAlarmDataCount.MCUAlarmDataCount(xmname, pointnamelist,out mssg);
            ////ProcessPrintMssg.Print(mssg);
             
        }
        public void MCUDATAAlarm()
        {
            List<string> pointnamelist = processBKGMCUPointLoad.BKGMCUPointLoad(xmname,out mssg);
            ProcessMCUDataAlarmBLL ProcessMCUDataAlarmBLL = new ProcessMCUDataAlarmBLL(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointnamelist);
            ProcessMCUDataAlarmBLL.main();
            emailSendBLL.ProcessEmailSend(ProcessMCUDataAlarmBLL.alarmInfoList,xmname,Aspect.IndirectValue.GetXmnoFromXmname(xmname),out mssg);
            smsSendBLL.ProcessSmsSend(ProcessMCUDataAlarmBLL.alarmInfoList, xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
        
            Console.WriteLine("{0} 发送预警通知：{1}成功", DateTime.Now, string.Join("\r\n", ProcessMCUDataAlarmBLL.alarmInfoList));
            //ProcessPrintMssg.Print(string.Join(",",ProcessMCUDataAlarmBLL.alarmInfoList));
        }
    }
}
