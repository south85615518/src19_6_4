﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tool;

namespace NFnetUnitTest.SGALARM
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ProcessSGAlarmDataServer.init("韶关铀业");
        }
        //浸润线
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessSGAlarmDataServer.TimerReset(ProcessSGAlarmDataServer.timermcu, 1000 * 60 * int.Parse(textBox1.Text));
                Console.WriteLine("浸润线巡检间隔重置成功");
            }
            catch (Exception ex)
            {
                Console.WriteLine("浸润线巡检间隔重置出错,错误信息:"+ex.Message);
            }
        }
        //固定测斜
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                ProcessSGAlarmDataServer.TimerReset(ProcessSGAlarmDataServer.timermcuangle, 1000 * 60 * int.Parse(textBox2.Text));
                Console.WriteLine("固定测斜巡检间隔重置成功");
            }
            catch (Exception ex)
            {
                Console.WriteLine("固定测斜巡检间隔重置出错,错误信息:" + ex.Message);
            }
        }
        //雨量
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessSGAlarmDataServer.TimerReset(ProcessSGAlarmDataServer.timerhxyl, 1000 * 60 * int.Parse(textBox3.Text));
                Console.WriteLine("雨量巡检间隔重置成功");
            }
            catch (Exception ex)
            {
                Console.WriteLine("雨量巡检间隔重置出错,错误信息:" + ex.Message);
            }
        }
        //gnss
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessSGAlarmDataServer.TimerReset(ProcessSGAlarmDataServer.timergnss, 1000 * 60 * int.Parse(textBox4.Text));
                Console.WriteLine("GNSS巡检间隔重置成功");
            }
            catch (Exception ex)
            {
                Console.WriteLine("GNSS巡检间隔重置出错,错误信息:" + ex.Message);
            }
        }
        //库水位
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessSGAlarmDataServer.TimerReset(ProcessSGAlarmDataServer.timergauge, 1000 * 60 * int.Parse(textBox5.Text));
                Console.WriteLine("库水位巡检间隔重置成功");
            }
            catch (Exception ex)
            {
                Console.WriteLine("库水位巡检间隔重置出错,错误信息:" + ex.Message);
            }
        }
    }
}
