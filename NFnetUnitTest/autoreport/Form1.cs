﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tool;
using NFnetUnitTest.VersionControl;
using System.Threading;

namespace NFnetUnitTest.autoreport
{
    public partial class Form1 : Form
    {

        public System.Timers.Timer timer = new System.Timers.Timer(3600000);
        
        public List<autoreport> lap = new List<autoreport>();
        public Form1()
        {

            
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                xmlistload();
                logload();
                init();
                Start();
            }
            catch (Exception ex)
            {
 
            }
        }
        public void init()
        {
            timer.Elapsed += new System.Timers.ElapsedEventHandler(logflush);
            timer.Enabled = false;
            timer.AutoReset = false;
        }
        public void logflush(object source, System.Timers.ElapsedEventArgs e)
        {
            List<string> commendlog = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\报表日志\\log.txt");
            //ProcessProgramBagPath.ListBoxBind(this.listBox2, commendlog);

            ListBoxFlushEvent += ListFlush;
            IAsyncResult ar = this.listBox1.BeginInvoke(ListBoxFlushEvent, this.listBox1, commendlog);
            this.listBox1.EndInvoke(ar);
            ListBoxFlushEvent -= ListFlush;
        }
        public void logload()
        {
            List<string> commendlog = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\报表日志\\log.txt");
            ProcessProgramBagPath.ListBoxBind(this.listBox1, commendlog);
        }
        public delegate void ListBoxFlush(ListBox listBox, List<string> ls);
        public event ListBoxFlush ListBoxFlushEvent;
        public void ListFlush(ListBox listBox, List<string> ls)
        {
            listBox.Items.Clear();
            foreach (var item in ls)
            {
                listBox.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Start();
        }

        public void Start()
        {
            if (this.button1.Text == "开启")
            {

                Console.WriteLine("报表生成已启动...");

                lap.Clear();
                foreach (var name in this.comboBox1.Items)
                {
                    autoreport report = new autoreport();
                    report.timer.Interval = 60 * 1000 * Convert.ToInt32(this.textBox1.Text);
                    report.listtimer = timer;
                    report.xmname = name.ToString();
                    Thread thread = new Thread(new ParameterizedThreadStart(reportthread));
                    thread.Start(report);
                    lap.Add(report);
                }
                this.button1.Text = "停止";
            }
            else
            {
                Console.WriteLine("报表生成已停止...");
                foreach (var report in lap)
                {
                    report.timer.Enabled = false;
                    report.listtimer.Enabled = false;
                }

                this.button1.Text = "开启";
            }
        }




        public void reportthread(object obj)
        {
            autoreport report = obj as autoreport;
            report.main();
        }



        public  void xmlistload()
        {
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\自动报表项目列表\\mulist.txt");
            this.comboBox1.DataSource = ls;
            
        }
     
    }
}
