﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Report;
using NFnet_Interface.Report;
using NFnetUnitTest.TestTotalStationReport;
using System.Windows.Forms;
using Tool;

namespace NFnetUnitTest.autoreport
{
    public class autoreport
    {
        public  string xmname = "";
        public ProcessMonthReport processMonthReport = new ProcessMonthReport();
        public ProcessWeekReport processWeekReport = new ProcessWeekReport();
        public System.Timers.Timer timer = new System.Timers.Timer(3600000);
        public ProcessAutoReport processAutoReport = new ProcessAutoReport();
        public System.Timers.Timer listtimer { get; set; }
        public string mssg = "";
        public void main()
        {
            init();
        }
        public  void init()
        {
            processAutoReport.xmname = xmname;
            processAutoReport.templatepath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "自动报表\\ReportExcel - 副本.xlsx";
            timer.Elapsed += new System.Timers.ElapsedEventHandler(report);
            timer.Enabled = true;
            timer.AutoReset = true;

        }
        public  void report(object source, System.Timers.ElapsedEventArgs e)
        {
            timer.Enabled = false;
            timer.AutoReset = false;
            listtimer.Enabled = true;
            listtimer.AutoReset = true;
            weekreport();
            monthreport();
            timer.Enabled = true;
            timer.AutoReset = true;
            listtimer.Enabled = false;
            listtimer.AutoReset = false;
        }
        public void weekreport()
        {
            AutoReport.Model.autoweekreport model = processWeekReport.WeekReport(Aspect.IndirectValue.GetXmnoFromXmname(xmname),Aspect.WeekToINT.ToINT(DateTime.Now.DayOfWeek),DateTime.Now.Hour,out mssg);
            ExceptionLog.ExceptionWrite(mssg);
            Console.WriteLine(mssg);
            if (model!=null)
            {
                
                processAutoReport.WeekReport(model.jclx);
            }
        }
        public void monthreport()
        {
            AutoReport.Model.automonthreport model = processMonthReport.MonthReport(Aspect.IndirectValue.GetXmnoFromXmname(xmname), DateTime.Now.Day, DateTime.Now.Hour, out mssg);
            ExceptionLog.ExceptionWrite(mssg);
            Console.WriteLine(mssg);
            if (model != null)
            {
                processAutoReport.MonthReport(model.jclx);
            }
        }
       

    }
}
