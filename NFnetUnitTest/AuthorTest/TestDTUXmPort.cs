﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;
using NFnet_Interface.UserProcess;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnetUnitTest.AuthorTest
{
    public class TestDTUXmPort
    {

        public ProcessDTUPortXm processDTUXmPortBLL = new ProcessDTUPortXm();
        public ProcessDTUPortXmname processDTUXmnamePortBLL = new ProcessDTUPortXmname();
        public ProcessDTUXmList processDTUXmList = new ProcessDTUXmList();
        public ProcessDTUXmPortTableLoad processDTUXmPortTableLoad = new ProcessDTUXmPortTableLoad();
        public static string mssg =  "";
        public void main()
        {
            //testDTUPortXm();
            //testDTUPortXmname();
            //testDTUXmList();
            testDTUXmPortTableLoad();
        }
        public void testDTUPortXm()
        {
            processDTUXmPortBLL.DTUPortXm(8092,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPortXmname()
        {
            processDTUXmnamePortBLL.DTUPortXm(8092, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUXmList()
        {
            processDTUXmList.DTUXmList(out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUXmPortTableLoad()
        {
            DataTable dt = processDTUXmPortTableLoad.DTUXmPortTableLoad(out mssg);
            ProcessPrintMssg.Print(mssg);
            string dtuSettingDir = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
            string dtuSettingPath = dtuSettingDir + "\\Setting\\dtusetting.txt";
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(dtuSettingPath);
            ls.Add("192.168.168.195");
            ls.Add("8089");
            Tool.FileHelper.FileInfoListWrite(ls, dtuSettingPath);

        }





    }
}
