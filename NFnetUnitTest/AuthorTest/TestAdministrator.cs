﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_DAL.MODEL;
using NFnet_BLL.UserProcess;
using NFnet_BLL.ProjectInfo;

namespace NFnetUnitTest.AuthorTest
{
   public class TestAdministrator
    {
       
        public static string userId = Console.ReadLine();
        public static string mssg = "";
        public ProcessMonitorBLL monitorBLL = new ProcessMonitorBLL();
        public ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public ProcessAdministratrorBLL administratorBLL = new ProcessAdministratrorBLL();
        public static Authority.Model.xmconnect model = new Authority.Model.xmconnect
        {
            //xmname = Console.ReadLine(),
            //jd = "1223.123",
            //wd = "1356.123",
            //createman = userId,
            //xmaddress = "四川成都水库大坝监测",
            //pro = "广东省",
            //city = "广州市",
            //country = "天河区",
            //jcdw = "南方测绘",
            //delegateDw = "南方测绘",
            //jcpgStartDate = DateTime.Now.ToString(),
            //jcpgEndDate = DateTime.Now.ToString(),
            //safeLevel = "一级",
            //ResponseMan = "A",
            //type = "水库大坝",
            //xmBz = "南方测绘",
            //buildInstrutment = "1,1,1"

        };
        public void main()
        {
            TestMonitorInfoLoad();
            //TestXmSave();
            //ProcessXmAdministratorInfo();
        }
        #region 项目
        public void TestXmSave()
        {
            processXmBLL.ProcessXmSave(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        #endregion 
        #region 监测员
        public void TestMonitorLoad()
        {
            var processMonitorLoadModelByUserIdModel = new ProcessMonitorBLL.ProcessMonitorLoadModelByUserId(userId);
            monitorBLL.ProcessMonitorLoad(processMonitorLoadModelByUserIdModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMonitorInfoLoad()
        {
            var processMonitorInfoLoadModel = new ProcessMonitorBLL.ProcessMonitorInfoLoadModel(Convert.ToInt32(userId));
            monitorBLL.ProcessMonitorInfoLoad(processMonitorInfoLoadModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMonitorInfoSave()
        {
 
        }
        #endregion
        #region 监督员
        #endregion
        #region 管理员
        public void ProcessXmAdministratorInfo()
        {
            var processXmAdministatorModel = new ProcessAdministratrorBLL.ProcessXmAdministatorModel(model.xmname);
            ProcessAdministratrorBLL.ProcessXmAdministator(processXmAdministatorModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        
        #endregion

    }
}
