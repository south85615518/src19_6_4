﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;
using System.Data;
using NFnet_DAL.MODEL;
using NFnet_DAL.BLL;

namespace NFnetUnitTest.AuthorTest
{
    /// <summary>
    /// 测试超级管理员业务逻辑处理
    /// </summary>
    class TestSuperAdministrator
    {
        public string mssg = "";
        private ProcessSuperAdministratorBLL superAdministratorBLL = new ProcessSuperAdministratorBLL();
        private ProcessAdministratrorBLL administratorBLL = new ProcessAdministratrorBLL();
        public static int rand = new Random(999).Next(999);
        public static UnitBLL unitBLL = new UnitBLL();
        public static NFnet_DAL.MODEL.Unit unit = new NFnet_DAL.MODEL.Unit
        {
            UnitName = "",//Console.ReadLine(),
            Addr = "公司地址1",
            Aptitude = "资质1",
            Pro = "广东省1",
            City = "广州市1",
            Country = "天河区1",
            DbName = "newdb1" + rand,
            Email = "86668689@qq.com1",
            Linkman = "联系人1",
            Natrue = "监测单位1",
            Police = "执照1",
            State = "未审核1",
            Taxproof = "资质证书1",
            Tel = "7868932462_1",
            CgdbName = "cgnewdb1" + rand


        };

        public void main()
        {
            //TestDbLoad();
            //TestUnitNameExit();
            //TestDbCreate();
            //TestUnitInfoSave();
            //TestUnitTableLoad();
            //TestUnitInfoUpdate();
            //TestProcessFreezeLogin();
            //TestProcessActiveLogin();
            //TestAdministratorLoad();
            //TestAdministratorAdd();
            //TestAdministratorUpdate();
            //TestUnitInfoLoad();
            TestTiggerCreate();
        }
        #region 单位管理测试
        public void TestDbLoad()
        {
            List<string> ls = new List<string>();
            if (ProcessSuperAdministratorBLL.ProcessDbListLoad(out ls, out mssg))
            {
                ProcessPrintMssg.Print(mssg);
                ProcessPrintMssg.Print(string.Join(",", ls));
            }
        }
        public void TestUnitNameExit()
        {
            ProcessSuperAdministratorBLL.ProcessUnitNameExist(unit.UnitName, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDbCreate()
        {

            var processUnitCreateModel = new ProcessSuperAdministratorBLL.ProcessUnitCreateModel(unit, "e:/tot.sql", "e:/tot.sql");
            ProcessSuperAdministratorBLL.ProcessUnitCreate(processUnitCreateModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestUnitInfoSave()
        {
            ProcessSuperAdministratorBLL.ProcessUnitSave(unit, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestUnitInfoLoad()
        {
            Unit model = null;
            superAdministratorBLL.ProcessUnitInfoLoad(unit.UnitName, out model, out mssg);
            ProcessPrintMssg.Print(mssg);
            
        }
        public void TestUnitTableLoad()
        {
            DataTable dt = null;
            superAdministratorBLL.ProcessUnitLoad(out dt, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestUnitInfoUpdate()
        {
            superAdministratorBLL.ProcessUnitUpdate(unit, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        
        public void TestProcessFreezeLogin()
        {
            superAdministratorBLL.ProcessFreezeLogin(unit.UnitName, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessActiveLogin()
        {
            superAdministratorBLL.ProcessActiveLogin(unit.UnitName, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        #endregion 单位管理测试
        #region 管理员信息测试
        private member member = new member
        {
            UserId = "admin",
            Password = "12344324",
            Role = "管理员",
            UserGroup = "单位管理组",
            UserName = "adminstrator",
            Position = "管理员",
            Tel = "13545333321",
            Email = "34242143@qq.com",
            Sgzsmc = "上岗证书名称",
            Sgzsbh = "23412432432",
            Sgzs = "上岗证书",
            Zczsmc = "职称证书名称234",
            Zczsbh = "职称证书编号234",
            Zczs = "职称证书324",
            UnitName = unit.UnitName

        }; 
        public void TestAdministratorLoad()
        {
            var processAdministratorLoadModel = new ProcessAdministratrorBLL.ProcessAdministratorLoadModel(unit.UnitName);

            administratorBLL.ProcessAdministratorLoad(processAdministratorLoadModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestAdministratorAdd()
        {
            superAdministratorBLL.ProcessAdministratorAdd(member, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestAdministratorUpdate()
        {
            superAdministratorBLL.ProcessAdministratorUpdate(member, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        #endregion 管理员信息测试
        public void TestTiggerCreate()
        {
            var processTriggersCreateModel = new UnitBLL.ProcessTriggersCreateModel("E:\\后台项目\\前台DAY02\\DAY02\\SQLScript\\triggerlist.txt", "south_4");
            unitBLL.ProcessTriggersCreate(processTriggersCreateModel);
        }
    }
}
