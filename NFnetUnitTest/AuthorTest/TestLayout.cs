﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.XmInfo;

namespace NFnetUnitTest.AuthorTest
{
    public class TestLayout
    {
        public string xmname = Console.ReadLine();
        private ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        private string mssg = "";
        public void main()
        {
            //TestLayoutListLoad();
            TestBaseMapLoad();
        }
        public void TestLayoutListLoad()
        {
            var processLayoutBLLModel = new ProcessLayoutBLL.ProcessLayoutListLoadModel(Aspect.IndirectValue.GetXmnoFromXmname(xmname),xmname);
            layoutBLL.ProcessLayoutListLoad(processLayoutBLLModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestBaseMapLoad()
        {
            var processBaseMapLoadModel = new ProcessLayoutBLL.ProcessBaseMapLoadModel("A",2,"../..a");
            layoutBLL.ProcessBaseMapLoad(processBaseMapLoadModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
