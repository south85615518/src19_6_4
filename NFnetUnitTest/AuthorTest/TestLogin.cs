﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL;
using NFnet_BLL.LoginProcess;
using NFnetUIHelper;
using Aspect;
namespace NFnetUnitTest.AuthorTest
{
    /// <summary>
    /// 登陆部分测试类
    /// </summary>
    class TestLogin
    {
        public ProcessLoginBLL loginBLL = new ProcessLoginBLL();
        public NFnet_BLL.LoginProcess.ProcessLoginBLL.ProcessLoginNGNModel loginmodel = new NFnet_BLL.LoginProcess.ProcessLoginBLL.ProcessLoginNGNModel(Console.ReadLine(), Console.ReadLine());
        
       
        string mssg = "";
        public void test()
        {

            
            
            ProcessTestLoginBLL();
            //ProcessNearestXmGet();
            //ProcessTestAuthTicket();
            //ProcessHeadSet();
        }
        public void ProcessTestLoginBLL()
        {
            ProcessLoginBLL.ProcessLogin(loginmodel,out mssg);
            ProcessPrintMssg.Print(mssg);
            
        }
        public void ProcessNearestXmGet()
        {
            string xmname = "";
            var xmmodel = new ProcessLoginBLL.ProcessRoleNearestXmModel("sa", loginmodel.Role);
           if (ProcessLoginBLL.ProcessRoleNearestXm(xmmodel,out mssg))
           {
               xmname = xmmodel.xmname;
           }
           ProcessPrintMssg.Print("获取项目名"+xmname+mssg);
       }
        public void ProcessTestAuthTicket()
        {
            var processAuthTicketModel = new ProcessAuthTicketBLL.ProcessAuthTicketModel(loginmodel.name, "127.0.0.1");
            ProcessAuthTicketBLL.scurity(processAuthTicketModel);
            ProcessPrintMssg.Print("票据查验成功");
        }
        public  void ProcessHeadSet()
        {
            Aspect.PageHeader.ProcessTitleSetModel model = new Aspect.PageHeader.ProcessTitleSetModel("", "sa", loginmodel.Role, loginmodel.currentUseInfo);
            if (Aspect.PageHeader.IdentyTxtFill(model, out mssg))
            {
                ProcessPrintMssg.Print("身份确认"+model.title);
            }
        }
    }
}
