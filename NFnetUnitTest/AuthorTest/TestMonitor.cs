﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;

namespace NFnetUnitTest.AuthorTest
{
    class TestMonitor
    {
        public string xmname = "华南水电四川成都大坝监测A";
        public ProcessMonitorBLL processMonitorBLL = new ProcessMonitorBLL();
        public string mssg = ""; 
        public void main() {
            //TestProcessXmUnitMember();
            //TestProcessMonitorLoad();
            TestMonitorSameName();
        }
        public void TestProcessXmUnitMember()
        {
            var processXmMonitorListModel = new ProcessMonitorBLL.ProcessXmMonitorListModel(Aspect.IndirectValue.GetXmnoFromXmname(xmname));
            processMonitorBLL.ProcessXmMonitorList(processXmMonitorListModel,out mssg );
            ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessMonitorLoad()
        {
            var processMonitorBLLModel = new ProcessMonitorBLL.ProcessMonitorLoadModelByUserId("hhw");
            processMonitorBLL.ProcessMonitorLoad(processMonitorBLLModel, out mssg);
            ProcessPrintMssg.Print(mssg);
           
    
        }
        public void TestMonitorSameName()
        {
            var processSameNameCondition = new NFnetUIHelper.AuthorySameName.ProcessSameNameCondition("李念辉",null);
            NFnetUIHelper.AuthorySameName.MonitorName(processSameNameCondition,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
