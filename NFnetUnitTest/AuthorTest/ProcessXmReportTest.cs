﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.pub;
using System.Data;
using SqlHelpers;

namespace NFnetUnitTest.AuthorTest
{
    public class ProcessXmReportTest
    {
        public ProcessXmReportPrintBLL processXmReportPrintBLL = new ProcessXmReportPrintBLL();
        public static string mssg = "";
        public void main()
        {

            List<string> ls = new List<string>();
            ls.Add("aaaabbs");
            ls.Add("aaaabbstertertf");
            var model = ls.Find(m => m.IndexOf("f") != -1) == null ? null : ls.Find(m => m.IndexOf("f") != -1).ToList();
            XmReportTest();
        }
        public void XmReportTest()
        {
            DataTable dt = new DataTable();
            string sql = "select  xmname,[_call] as responseman,xmaddress,delegateDw,jcpgStartDate,DateDiff(\"d\",jcpgStartDate,Date()) as rundays ,jcpgEndDate,DateDiff(\"d\",Date(),jcpgEndDate) as DaysAprochalFinished, DateDiff(\"d\",jcpgStartDate,jcpgEndDate) as totaldays, jcpgEndDate & '-' &  jcpgEndDate as monitordate,iif(format(jcpgEndDate,'yyyy/mm/dd HH:mm:ss') > format(date(),'yyyy/mm/dd HH:mm:ss'),'监测中','已完工') as state  from xmconnect,unitmember where xmconnect.responseman = unitmember.username and company='广州铁路科技开发有限公司'";

            querysql query = new querysql();
            dt = query.querytaccesstdb(sql);



            var processXmReportModel = new ProcessXmReportPrintBLL.ProcessXmReportModel("广州铁路科技开发有限公司", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processXmReportPrintBLL.ProcessXmReport(processXmReportModel,out mssg);
        }
    }
}
