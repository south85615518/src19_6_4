﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;

namespace NFnetUnitTest
{
    public class TestGetXmnoBySno
    {
        //根据设备编号获取到项目编号
        public void TestGetXmnoBySno(string sno, out string mssg, out string xmno)
        {
            ProcessPeopleXmnameListCondition.ProcessGetXmnoBySno(sno, out mssg, out xmno);
            Console.WriteLine(mssg);
        }
    }
}
