﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NFnetUnitTest.B_L
{
    public partial class 子午线收敛角计算 : Form
    {
        public 子午线收敛角计算()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double B = Convert.ToDouble(this.textBox2.Text) * 3.14159265354 / 180;
            double l = Convert.ToDouble(this.textBox1.Text);
            double cosB_2 = Math.Pow(Math.Cos(B),2);
            //克拉索夫斯基椭球参数带入计算结果
            double r = (1 + ((0.33333 + 0.00674 * cosB_2) - (0.0667 - 0.2 * cosB_2) * Math.Pow(l, 2)) * Math.Pow(l, 2)*cosB_2)*l*Math.Sin(B);
            
            //double dr = r /3600;
            this.label4.Text = r.ToString(); //ConvertDigitalToDegrees(r);
            this.label5.Text = string.Format("纬度:34.1385051863 --- 0.2111647103 \n 纬度:34.1800234625 --- 0.1921952361 ");
            /*
             * １ ＋［（０．３３３ ３３ ＋０．００６ ７４ｃｏｓ ２ B）
－（０．０６６ ７ －０ ．２ｃｏｓ ２ B）l２ ］· l２ ｃｏｓ２ B
· lｓｉｎB
             */

        }

        /// <summary>
        /// 数字经纬度和度分秒经纬度转换 (Digital degree of latitude and longitude and vehicle to latitude and longitude conversion)
        /// </summary>
        /// <param name="digitalDegree">数字经纬度</param>
        /// <return>度分秒经纬度</return>
        static public string ConvertDigitalToDegrees(double digitalDegree)
        {
            const double num = 60;
            int degree = (int)digitalDegree;
            double tmp = (digitalDegree - degree) * num;
            int minute = (int)tmp;
            double second = (tmp - minute) * num;
            string degrees = "" + degree + "°" + minute + "′" + second + "″";
            return degrees;
        }
        /// <summary>
        /// 度分秒经纬度(必须含有'°')和数字经纬度转换
        /// </summary>
        /// <param name="digitalDegree">度分秒经纬度</param>
        /// <return>数字经纬度</return>
        static public double ConvertDegreesToDigital(string degrees)
        {
            const double num = 60;
            double digitalDegree = 0.0;
            int d = degrees.IndexOf('°');           //度的符号对应的 Unicode 代码为：00B0[1]（六十进制），显示为°。
            if (d < 0)
            {
                return digitalDegree;
            }
            string degree = degrees.Substring(0, d);
            digitalDegree += Convert.ToDouble(degree);

            int m = degrees.IndexOf('′');           //分的符号对应的 Unicode 代码为：2032[1]（六十进制），显示为′。
            if (m < 0)
            {
                return digitalDegree;
            }
            string minute = degrees.Substring(d + 1, m - d - 1);
            digitalDegree += ((Convert.ToDouble(minute)) / num);

            int s = degrees.IndexOf('″');           //秒的符号对应的 Unicode 代码为：2033[1]（六十进制），显示为″。
            if (s < 0)
            {
                return digitalDegree;
            }
            string second = degrees.Substring(m + 1, s - m - 1);
            digitalDegree += (Convert.ToDouble(second) / (num * num));

            return digitalDegree;
        }

        private void button2_Click(object sender, EventArgs arg)
        {
            double Bf = Convert.ToDouble(this.textBox3.Text);

            double y = Convert.ToDouble(this.textBox5.Text);


            double a = 6378245, e = 1 / 298.3, _e = Math.Sqrt(0.00673950181947);

            double Nf = a / (Math.Sqrt(1 - e * e * Math.Pow(Math.Sin(Bf), 2)));

            double t = Math.Tan(Bf); 
            double z = y/(Nf*Math.Cos(Bf));

            //double r = 1 - ((0.33333 - 0.00225 * Math.Pow(Math.Cos(Bf), 4)) - (0.2 - 0.0667 * Math.Pow(Math.Cos(Bf), 2)) * Math.Pow(z, 2)) * Math.Sin(Bf);
            //this.label10.Text = r.ToString();

            double n = _e * Math.Pow( Math.Cos(Bf),2 );

            double r = (1 / Nf) * y * t - (1 / 3 * Math.Pow(Nf, 3)) * Math.Pow(y, 2) * t * (1 + t * t - n * n) + Math.Pow(y, 5) / (15 * Math.Pow(Nf, 5)) * (2 + 5 * Math.Pow(t, 2) + 3 * Math.Pow(t, 4));
            this.label10.Text = (r*180/3.14159265354).ToString();
        }

    

     

    }
}
