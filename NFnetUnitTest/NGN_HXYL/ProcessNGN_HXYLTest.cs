﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;
using NFnet_Interface.DisplayDataProcess.NGN_HXYL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_MODAL;

namespace NFnetUnitTest.NGN_HXYL
{
    public class ProcessNGN_HXYLTest
    {
        public string mssg = "";
        public ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public ProcessHXYLMaxTime processHXYLMaxTime = new ProcessHXYLMaxTime();
        public ProcessHXYLPreAcVal processHXYLPreAcVal = new ProcessHXYLPreAcVal();
        public ProcessHXYLPointBLL processHXYLPointBLL = new ProcessHXYLPointBLL();
        public ProcessHXYLData processHXYLData = new ProcessHXYLData();
        public static ProcessHXYLDataRqcxConditionCreate processHXYLDataRqcxConditionCreate = new ProcessHXYLDataRqcxConditionCreate();

        public ProcessSerializestrSW senorChartBLL = new ProcessSerializestrSW();
        public static NFnet_Interface.DisplayDataProcess.BKG.ProcessHXYLChart processHXYLChart = new NFnet_Interface.DisplayDataProcess.BKG.ProcessHXYLChart();



        public NGN.Model.hxyl hxyl = new NGN.Model.hxyl { id = "1", time = DateTime.Now, val = new Random().Next(999), xmno = 29     };

        public void main()
        {
            //NGN_HXYLAdd();
            //NGN_HXYLMaxTime();
            //NGN_HXYLPreAcValGet();
            //NGN_HXYLData();
            NGN_HXYLChart();
        }
        public void NGN_HXYLAdd()
        {
            processHXYLBLL.ProcessHXYLAdd(hxyl,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void NGN_HXYLMaxTime()
        {
            DateTime dt = processHXYLMaxTime.HXYLMaxTime("1",29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void NGN_HXYLPreAcValGet()
        {
            var model = processHXYLPreAcVal.HXYLPreAcVal(hxyl,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
      
        public void NGN_HXYLData()
        {

            var resultDataRqcxConditionCreateCondition = processHXYLDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018/6/28","2018/6/29", QueryType.RQCX, "", "UTS1", new DateTime());

            List<string> lis = new List<string>();
            lis.Add("1");
            var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition(string.Join(",", lis), resultDataRqcxConditionCreateCondition.sql, Aspect.IndirectValue.GetXmnoFromXmname("UTS1"));
            processHXYLData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
        }
        public void NGN_HXYLChart()
        {
            var resultDataRqcxConditionCreateCondition = processHXYLDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018/6/28", "2018/6/29", QueryType.RQCX, "", "UTS1", new DateTime());

            List<string> lis = new List<string>();
            lis.Add("1");
            var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition(string.Join(",", lis), resultDataRqcxConditionCreateCondition.sql, Aspect.IndirectValue.GetXmnoFromXmname("UTS1"));
            processHXYLData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);


            List<serie> lsHXYL = new List<serie>();
            lsHXYL = processHXYLChart.SerializestrHXYL(fillInclinometerDbFillCondition.sql, 29,"雨量计1号设备", out mssg);

        }
    }
}
