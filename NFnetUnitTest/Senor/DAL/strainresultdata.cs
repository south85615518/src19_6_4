﻿/**  版本信息模板在安装目录下，可自行修改。
* strainresultdata.cs
*
* 功 能： N/A
* 类 名： strainresultdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/7/23 16:50:30   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
namespace Strain.DAL
{
	/// <summary>
	/// 数据访问类:strainresultdata
	/// </summary>
	public partial class strainresultdata
	{
        public database db = new database();
		public strainresultdata()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Strain.Model.strainresultdata model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("replace into strainresultdata(");
			strSql.Append("pointname,initstrengthVal,strengthVal,this_val,ac_val,monitorTime,xmno)");
			strSql.Append(" values (");
			strSql.Append("@pointname,@initstrengthVal,@strengthVal,@this_val,@ac_val,@monitorTime,@xmno)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@pointname", OdbcType.VarChar,20),
					new OdbcParameter("@initstrengthVal", OdbcType.Double),
					new OdbcParameter("@strengthVal", OdbcType.Double),
					new OdbcParameter("@this_val", OdbcType.Double),
					new OdbcParameter("@ac_val", OdbcType.Double),
					new OdbcParameter("@monitorTime", OdbcType.DateTime),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };
			parameters[0].Value = model.pointname;
			parameters[1].Value = model.initstrengthVal;
			parameters[2].Value = model.strengthVal;
			parameters[3].Value = model.this_val;
			parameters[4].Value = model.ac_val;
			parameters[5].Value = model.monitorTime;
            parameters[6].Value = model.xmno;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Strain.Model.strainresultdata model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update strainresultdata set ");
			strSql.Append("pointname=@pointname,");
			strSql.Append("initstrengthVal=@initstrengthVal,");
			strSql.Append("strengthVal=@strengthVal,");
			strSql.Append("this_val=@this_val,");
			strSql.Append("ac_val=@ac_val,");
			strSql.Append("monitorTime=@monitorTime");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@pointname", OdbcType.VarChar,20),
					new OdbcParameter("@initstrengthVal", OdbcType.Double),
					new OdbcParameter("@strengthVal", OdbcType.Double),
					new OdbcParameter("@this_val", OdbcType.Double),
					new OdbcParameter("@ac_val", OdbcType.Double),
					new OdbcParameter("@monitorTime", OdbcType.DateTime)};
			parameters[0].Value = model.pointname;
			parameters[1].Value = model.initstrengthVal;
			parameters[2].Value = model.strengthVal;
			parameters[3].Value = model.this_val;
			parameters[4].Value = model.ac_val;
			parameters[5].Value = model.monitorTime;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from strainresultdata ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Strain.Model.strainresultdata GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pointname,initstrengthVal,strengthVal,this_val,ac_val,monitorTime from strainresultdata ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			Strain.Model.strainresultdata model=new Strain.Model.strainresultdata();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Strain.Model.strainresultdata DataRowToModel(DataRow row)
		{
			Strain.Model.strainresultdata model=new Strain.Model.strainresultdata();
			if (row != null)
			{
				if(row["pointname"]!=null)
				{
					model.pointname=row["pointname"].ToString();
				}
				if(row["initstrengthVal"]!=null && row["initstrengthVal"].ToString()!="")
				{
					model.initstrengthVal=double.Parse(row["initstrengthVal"].ToString());
				}
				if(row["strengthVal"]!=null && row["strengthVal"].ToString()!="")
				{
					model.strengthVal=double.Parse(row["strengthVal"].ToString());
				}
				if(row["this_val"]!=null && row["this_val"].ToString()!="")
				{
					model.this_val=double.Parse(row["this_val"].ToString());
				}
				if(row["ac_val"]!=null && row["ac_val"].ToString()!="")
				{
					model.ac_val=double.Parse(row["ac_val"].ToString());
				}
				if(row["monitorTime"]!=null && row["monitorTime"].ToString()!="")
				{
					model.monitorTime=DateTime.Parse(row["monitorTime"].ToString());
				}
			}
			return model;
		}

		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

