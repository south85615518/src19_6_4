﻿/**  版本信息模板在安装目录下，可自行修改。
* strainresultdata.cs
*
* 功 能： N/A
* 类 名： strainresultdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/7/23 16:50:30   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Strain.Model
{
	/// <summary>
	/// strainresultdata:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class strainresultdata
	{
		public strainresultdata()
		{}
		#region Model
		private string _pointname;
		private double _initstrengthval;
		private double _strengthval;
		private double _this_val;
		private double _ac_val;
		private DateTime _monitortime;
        private int _xmno;
        private int _cyc;

        public int cyc
        {
            get { return _cyc; }
            set { _cyc = value; }
        }
        public int xmno
        {
            get { return _xmno; }
            set { _xmno = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string pointname
		{
			set{ _pointname=value;}
			get{return _pointname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double initstrengthVal
		{
			set{ _initstrengthval=value;}
			get{return _initstrengthval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double strengthVal
		{
			set{ _strengthval=value;}
			get{return _strengthval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_val
		{
			set{ _this_val=value;}
			get{return _this_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_val
		{
			set{ _ac_val=value;}
			get{return _ac_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime monitorTime
		{
			set{ _monitortime=value;}
			get{return _monitortime;}
		}
		#endregion Model

	}
}

