﻿/**  版本信息模板在安装目录下，可自行修改。
* strainresultdata.cs
*
* 功 能： N/A
* 类 名： strainresultdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 Strain Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using Tool;
using SqlHelpers;
namespace Strain.BLL
{
	/// <summary>
	/// strainresultdata
	/// </summary>
	public partial class strainresultdata
	{
        
		private readonly Strain.DAL.strainresultdata dal=new Strain.DAL.strainresultdata();
		public strainresultdata()
		{}
		#region  BasicMethod
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Strain.Model.strainresultdata model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = "应力结果数据添加成功";
                    return true;
                }
                else {
                    mssg = "应力结果数据添加失败";
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = "应力结果数据添加出错，错误信息："+ex.Message;
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Strain.Model.strainresultdata model,out string mssg)
		{

            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("项目编号{0}点{1}测量时间{2}应力结果数据更新成功",model.xmno,model.pointname,model.monitorTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}点{1}测量时间{2}应力结果数据更新失败", model.xmno, model.pointname, model.monitorTime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}点{1}测量时间{2}应力结果数据更新出错,错误信息:"+ex.Message, model.xmno, model.pointname, model.monitorTime);
                return false;
            }



			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,DateTime monitorTime,string pointname)
		{
			
			return dal.Delete(xmno,monitorTime,pointname);
		}
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteTmp(int xmno,out string mssg)
        {
            try
            {
                if (dal.DeleteTmp(xmno))
                {
                    mssg = string.Format("删除项目编号{0}应力临时表的数据成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}应力临时表的数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}应力临时表的数据出错,错误信息："+ex.Message, xmno);
                return false;
            }

        }
        public bool ResultDataTableLoad( int startPageIndex, int pageSize, int xmno, string pointname, string sord,out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultdataTableLoad(startPageIndex, pageSize, xmno, pointname, sord,out  dt))
                {
                    mssg = "应力结果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "应力结果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "应力结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool ResultTableRowsCount( int xmno, string pointname,out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.ResultTableRowsCount(xmno,  pointname,out totalCont))
                {
                    mssg = "应力结果数据表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "应力结果数据表记录数加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool ResultDataReportPrint(string sql, int xmno, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultDataReportPrint(sql, xmno, out dt))
                {
                    mssg = "应力结果数据报表数据表生成成功";
                    return true;
                }
                else
                {
                    mssg = "应力结果数据报表数据表生成失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                
                    mssg = "应力结果数据报表数据表生成出错，错误信息"+ex.Message;
                    return false;
                
            }
        }
       
        

        public bool MaxTime(int xmno, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno, out maxTime))
                {
                    mssg = string.Format("获取项目编号{0}应力测量数据的最大日期{1}成功", xmno, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}应力测量数据的最大日期失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}应力测量数据的最大日期出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno,out List<Strain.Model.strainresultdata> model ,out string mssg)
        {
            List<Strain.Model.strainresultdata> lt = null;
            model = null;
            try
            {
                if (dal.GetModelList(xmno,out lt))
                {
                    mssg = string.Format("获取项目编号{0}应力临时表中的应力结果数据成功!", xmno);
                    model = lt;
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}应力临时表中的应力结果数据失败!", xmno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}应力临时表中的应力结果数据出错!错误信息:" + ex.Message, xmno);
                return false;
            }
           
        }


        public bool GetModel(int xmno, string pointname, DateTime dt, out Strain.Model.strainresultdata model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, dt, out model))
                {
                    mssg = string.Format("获取{0}{1}点{2}的应力测量数据成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}点{2}的应力测量数据失败", xmno, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}{1}点{2}的应力测量数据出错,错误信息:" + ex.Message, xmno, pointname, dt);
                return false;
            }
        }

        

      
        public bool StrainPointLoadBLL(int xmno, out List<string> ls, out string mssg)
        {
            ls = null;
            try
            {
                if (dal.StrainPointLoadDAL(xmno, out ls))
                {
                    mssg = "应力点号加载成功";
                    return true;

                }
                else
                {
                    mssg = "应力点号加载失败";
                    return true;
                }

            }
            catch (Exception ex)
            {
                mssg = "应力点号加载出错，错误信息" + ex.Message;
                return true;
            }

        }

       

        public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt,out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(xmno, pointname, out dt))
                {
                    mssg = string.Format("获取项目编号{0}应力{1}点的最新数据的测量时间{2}成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}应力{1}点的最新数据的测量时间失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}应力{1}点的最新数据的测量时间出错，错误信息:"+ex.Message, xmno, pointname);
                return false;
            }

        }

        public bool XmStateTable(int startPageIndex, int pageSize, int xmno, string xmname, string unitname, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.XmStateTable(startPageIndex, pageSize, xmno, xmname, unitname, colName, sord, out  dt))
                {
                    mssg = string.Format("获取编号为{0}项目编号名{1}的应力项目编号状态表记录数{2}条成功", xmno, xmname, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取编号为{0}项目编号名{1}的应力项目编号状态表失败", xmno, xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取编号为{0}项目编号名{1}的应力项目编号状态表出错,错误信息:"+ex.Message, xmno, xmname);
                return false;
            }
        }


        
       

        



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

