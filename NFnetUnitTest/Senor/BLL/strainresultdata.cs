﻿/**  版本信息模板在安装目录下，可自行修改。
* strainresultdata.cs
*
* 功 能： N/A
* 类 名： strainresultdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/7/23 16:50:30   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Strain.BLL
{
	/// <summary>
	/// strainresultdata
	/// </summary>
	public partial class strainresultdata
	{
		private readonly Strain.DAL.strainresultdata dal=new Strain.DAL.strainresultdata();
		public strainresultdata()
		{}
		#region  BasicMethod

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Strain.Model.strainresultdata model,out string mssg)
		{
            mssg = "";
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}点名{1}采集时间{2}的应力数据成功", model.xmno, model.pointname, model.monitorTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}点名{1}采集时间{2}的应力数据失败", model.xmno, model.pointname, model.monitorTime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}点名{1}采集时间{2}的应力数据出错,错误信息:"+ex.Message, model.xmno, model.pointname, model.monitorTime);
                return false;
            }

		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Strain.Model.strainresultdata model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete();
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Strain.Model.strainresultdata GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.GetModel();
		}

		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

