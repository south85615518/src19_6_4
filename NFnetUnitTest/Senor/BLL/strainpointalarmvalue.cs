﻿/**  版本信息模板在安装目录下，可自行修改。
* strainpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： strainpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:28   N/A    初版
*
* Copyright (c) 2012 Strain Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace Strain.BLL
{
    /// <summary>
    /// 数据访问类:strainpointalarmvalue
    /// </summary>
    public partial class strainpointalarmvalue
    {
        public Strain.DAL.strainpointalarmvalue dal = new Strain.DAL.strainpointalarmvalue();
        public strainpointalarmvalue()
        { }
        #region  BasicMethod
        //判断点名对象是否存在
        public bool Exist(Strain.Model.strainpointalarmvalue model,out string mssg)
        {

            try
            {
                if (dal.Exist(model))
                {
                    mssg = string.Format("项目编号{0}应力{1}点已经存在", model.xmno, model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}应力{1}点不存在", model.xmno, model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目编号{0}应力{1}点查询出错,错误信息:" + ex.Message;
                return true;
            }

        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Strain.Model.strainpointalarmvalue model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = "应力预警点新增成功";
                    return true;
                }
                else
                {
                    mssg = "应力预警点新增失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "应力预警点新增出错，错误信息" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Strain.Model.strainpointalarmvalue model,out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("应力预警点{0}更新成功", model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("应力预警点{0}更新失败", model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "应力预警点更新出错，错误信息" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, Strain.Model.strainpointalarmvalue model,out string mssg)
        {
           try
            {
                if (dal.MultiUpdate(pointNameStr, model))
                {
                    mssg = "应力预警点批量更新成功";
                    return true;
                }
                else
                {
                    mssg = "应力预警点批量更新失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "应力预警点批量更新出错，错误信息" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(Strain.Model.strainpointalarmvalue model,out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            try
            {
                if (dal.Delete(model))
                {
                    mssg = "应力预警点删除成功";
                    return true;
                }
                else
                {
                    mssg = "应力预警点删除失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "应力预警点删除出错，错误信息" + ex.Message;
                return false;
            }
        }
       

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, out Strain.Model.strainpointalarmvalue model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, out model))
                {
                    mssg = string.Format("获取{0}的应力的预警参数成功!", pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}的应力的预警参数失败!", pointname);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的应力的预警参数出错!错误信息:" + ex.Message, pointname);
                return false;
            }
        }

        public bool PointAlarmValueTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt,out string mssg)
        {
           dt = null;
            try
            {
                if (dal.PointAlarmValueTableLoad(searchstring, startPageIndex, pageSize, xmno, xmname, colName, sord, out  dt))
                {
                    mssg = "应力点号预警参数加载成功！";
                    return true;
                }
                else
                {
                    mssg = "应力点号预警参数加载失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "应力点号预警参数加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool PointTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.PointTableLoad(searchstring, startPageIndex, pageSize, xmno, xmname, colName, sord, out  dt))
                {
                    mssg = string.Format("项目{0}应力点号表{1}条记录加载成功！", xmname, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}应力点号表加载失败！", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}应力点号表加载出错,错误信息:" + ex.Message, xmname);
                return false;
            }
        }

        public bool PointAlarmValueTableRowsCount(string xmname, string searchstring, int xmno, out string totalCont,out string mssg)
        {

           totalCont = "";
            try
            {
                if (dal.PointAlarmValueTableRowsCount(xmname, searchstring, xmno, out  totalCont))
                {
                    mssg = string.Format("应力点号预警参数记录数{0}加载成功！", totalCont);
                    return true;
                }
                else
                {
                    mssg = "应力点号预警参数记录数加载失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "应力点号预警参数记录数加载出错，出错信息" + ex.Message;
                return false;
            }
        }
        #endregion  BasicMethod
    }
}

