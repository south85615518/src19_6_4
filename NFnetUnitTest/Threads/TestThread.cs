﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Tool;

namespace NFnetUnitTest.Threads
{
   public class TestThread
    {
       public  string astatic = "";

       public void main()
       {
           ThreadMain();
       }
       public void ThreadMain()
       {
           Thread threadA = new Thread(wait);
           threadA.Name = "A线程";
           threadA.Start();
           Thread threadB = new Thread(wait);
           threadB.Name = "B线程";
           threadB.Start();

           Console.WriteLine("程序结束!");
       }

       public void wait()
       {
           ExceptionLog.ExceptionWrite("我是前台线程"+Thread.CurrentThread.Name+"，我现在等待9秒后开始我的工作...");
           Thread.Sleep(1000);
           ExceptionLog.ExceptionWrite("我是前台线程" + Thread.CurrentThread.Name + "，我现在等待8秒后开始我的工作...");
           Thread.Sleep(1000);
           ExceptionLog.ExceptionWrite("我是前台线程" + Thread.CurrentThread.Name + "，我现在等待7秒后开始我的工作...");
           Thread.Sleep(1000);
           ExceptionLog.ExceptionWrite("我是前台线程" + Thread.CurrentThread.Name + "，我现在等待6秒后开始我的工作...");
           Thread.Sleep(1000);
           ExceptionLog.ExceptionWrite("我是前台线程" + Thread.CurrentThread.Name + "，我现在等待5秒后开始我的工作...");
           Thread.Sleep(1000);
           ExceptionLog.ExceptionWrite("我是前台线程" + Thread.CurrentThread.Name + "，我现在等待4秒后开始我的工作...");
           Thread.Sleep(1000);
           ExceptionLog.ExceptionWrite("我是前台线程" + Thread.CurrentThread.Name + "，我现在等待3秒后开始我的工作...");
           Thread.Sleep(1000);
           ExceptionLog.ExceptionWrite("我是前台线程" + Thread.CurrentThread.Name + "，我现在等待2秒后开始我的工作...");
           Thread.Sleep(1000);
           ExceptionLog.ExceptionWrite("我是前台线程" + Thread.CurrentThread.Name + "，我现在等待1秒后开始我的工作...");
           //Console.WriteLine("线程结束");
           ExceptionLog.ExceptionWrite("主线程虽然退出了，他妈的我还在打印你能怎么地...我在睡10秒...");
           Thread.Sleep(10000);
       }



       public void ThreadFunc()
       {
           databag dbg1 = new databag("A",20,"男");
           databag dbg2 = new databag("B", 21, "男");
           databag dbg3 = new databag("C", 22, "男");
           databag dbg4 = new databag("D", 23, "男");
           databag dbg5 = new databag("E", 24, "男");
           databag dbg6= new databag("F", 25, "男");
           databag dbg7= new databag("G", 26, "男");
           databag dbg8 = new databag("H", 27, "男");
           Thread t1 = new Thread(new ParameterizedThreadStart(threadmain));
           t1.Start(dbg1);
           Thread t2 = new Thread(new ParameterizedThreadStart(threadmain));
           t2.Start(dbg2);
           Thread t3 = new Thread(new ParameterizedThreadStart(threadmain));
           t3.Start(dbg3);
           Thread t4 = new Thread(new ParameterizedThreadStart(threadmain));
           t4.Start(dbg4);
           Thread t5 = new Thread(new ParameterizedThreadStart(threadmain));
           t5.Start(dbg5);
           Thread t6 = new Thread(new ParameterizedThreadStart(threadmain));
           t6.Start(dbg6);
           Thread t7 = new Thread(new ParameterizedThreadStart(threadmain));
           t7.Start(dbg7);
           Thread t8 = new Thread(new ParameterizedThreadStart(threadmain));
           t8.Start(dbg8);
       }
       public class databag
       {
           public string name { get; set; }
           public int age { get; set; }
           public string sex { get; set; }
           public databag(string name,int age,string sex)
           {
               this.name = name;
               this.age = age;
               this.sex = sex;
           }
           
       }
       public void threadmain(object obj)
       {
           databag bag = obj as databag;
           int age = 0;string sex = "";
           threadprocess(bag,out age,out sex);
           string astatic = bag.name;
           
           publicfun(bag);
           Thread.Sleep(age * 100);
           valuethread(bag.name,age,sex);
           ///ExceptionLog.DTURecordWrite(string.Format("姓名{0}年龄{1}性别{2} 公共资源值为{3}", bag.name, age, sex,astatic));
           Thread.Sleep(1000);
           Console.WriteLine(string.Format("姓名{0}年龄{1}性别{2} 公共资源值为{3}", bag.name, age, sex, astatic));
           Console.ReadLine();
       }
       public void threadprocess( databag bag,out int age,out string sex)
       {
           age = bag.age + 1;
           sex = bag.sex +"_"+bag.name;
       }
       public static void publicfun(databag bag)
       {
           string name = publicclass.seedwork(bag.name,bag.age);
           bag.name += "<-->" + name;
       }
       public static void valuethread(string name,int age,string sex)
       {
           Console.WriteLine(string.Format("传值静态函数里的 名字:{0}的性别是:{1}年龄：{2}",name,sex,age));
           
       }
       public class publicclass
       {
           public static int seed ;
           public static string seedwork(string name,int seedbase)
           {
               seed = seedbase;
               return name+"  "+(seed+1);
           }
       }
    }
}
