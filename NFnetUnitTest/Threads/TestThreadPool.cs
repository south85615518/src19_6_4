﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NFnetUnitTest.Threads
{
    public class TestThreadPool
    {

        public void ThreadPoolCreate()
        {
            //ThreadPool.SetMaxThreads(2,2);
            //ThreadPool.SetMaxThreads(4,4);
            int i = 0;
            for (i=0; i < 100; i++)
            {
                ThreadPool.QueueUserWorkItem(state =>
                {
                    Console.WriteLine("我是第" + i + "个线程我现在睡5秒");
                    Thread.Sleep(5000);
                });
            }
            Console.WriteLine("等待线程结束...");
            Console.ReadLine();
        }
    }
}
