﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GAUGE;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGaugeDATAAlarm
    {
        public ProcessGaugeAlarmModelListGet processGaugeAlarmModelListGet = new ProcessGaugeAlarmModelListGet();
        public ProcessGaugePointLoadBLL processGaugePointLoadBLL = new ProcessGaugePointLoadBLL();
        //public static ProcessBKGGaugePointLoad processBKGGaugePointLoad = new ProcessBKGGaugePointLoad();
        public ProcessGaugeAlarmDataCount processGaugeAlarmDataCount = new ProcessGaugeAlarmDataCount();
        public string mssg = "";
        public void main()
        {
            //TestGaugeAlarmModelListGet();
            //TestDATAAlarm();
            TestGaugeAlarmDataCount();
        }
        public void TestGaugeAlarmModelListGet()
        {
            List<string> pointnamelist = processGaugePointLoadBLL.GaugePointLoadBLL(29, out mssg);
            processGaugeAlarmModelListGet.GaugeAlarmModelListGet(pointnamelist, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGaugeAlarmDataCount()
        {
            List<string> pointnamelist = processGaugePointLoadBLL.GaugePointLoadBLL(29, out mssg);
            
            processGaugeAlarmDataCount.GaugeAlarmDataCount( pointnamelist, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDATAAlarm()
        {
            List<string> pointnamelist = processGaugePointLoadBLL.GaugePointLoadBLL(29, out mssg);

            ProcessGaugeDataAlarmBLL processGaugeDataAlarmBLL = new ProcessGaugeDataAlarmBLL("韶关铀业", 29, pointnamelist);
            processGaugeDataAlarmBLL.main();
            ProcessPrintMssg.Print(string.Join(",", processGaugeDataAlarmBLL.alarmInfoList));
        }
       
    }
}

