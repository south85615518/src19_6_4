﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using NFnet_Interface.DisplayDataProcess.GT;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestgtSurfaceData
    {
        public ProcessSurfaceDataBLL processSurfaceDataBLL = new ProcessSurfaceDataBLL();


       
        public data.Model.gtsurfacedata model = new data.Model.gtsurfacedata { xmno = 106, surfacename="A", calculatepointsCont = 3, ac_val = 100, this_val = 10, time = DateTime.Now };
        public static string mssg = "";
        public void main()
        {
            TestSurfaceDataAdd();
            TestSurfaceDataSameTimeModel();
        }
        public void TestSurfaceDataAdd()
        {
            processSurfaceDataBLL.ProcessResultDataAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSurfaceDataDelete()
        {

        }
        public void TestSurfaceDataSameTimeModel()
        {
           // var surfaceDataSameTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(106, "A", model.time);
           // processSurfaceDataBLL.SurfaceDataSameTimeModel(surfaceDataSameTimeModelGetModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        #region 测量库
        public static ProcessSurfaceSurveyDataAdd processSurfaceSurveyDataAdd = new ProcessSurfaceSurveyDataAdd();
        public static ProcessSurfaceSurveyDataUpdate processSurfaceSurveyDataUpdate = new ProcessSurfaceSurveyDataUpdate();
        public static ProcessSurfaceSurveyPointTimeDataAdd processSurfaceSurveyPointTimeDataAdd = new ProcessSurfaceSurveyPointTimeDataAdd();
        public static ProcessSurfaceSurveyResultDataTableLoad processSurfaceSurveyResultDataTableLoad = new ProcessSurfaceSurveyResultDataTableLoad();
        //public static ProcessSurfaceSurveyTimeDataDelete processSurfaceSurveyTimeDataDelete = new ProcessSurfaceSurveyTimeDataDelete();
        public static ProcessSurfaceSurveyTimeList processSurfaceSurveyTimeList = new ProcessSurfaceSurveyTimeList();
        public void surveymain()
        {
            //TestSurfaceSurveyDataAdd();
            //TestSurfaceSurveyDataUpdate();
            //TestSurfaceSurveyResultDataTableLoad();
            //TestSurfaceSurveyTimeDataDelete();
            TestSurfaceSurveyTimeList();
            ProcessPrintMssg.Print(mssg);
        }

        public static void  TestSurfaceSurveyDataAdd()
        {
           // processSurfaceSurveyDataAdd.SurveyDataAdd(106, "A", Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"),out mssg);
        }
        public static void  TestSurfaceSurveyDataUpdate()
        {
            //processSurfaceSurveyDataUpdate.SurveyDataUpdate(106, "A", Convert.ToDateTime("2018-10-08 15:10:06"),"this_val","ac_val", Convert.ToDateTime("2018-10-10 18:17:02"), out mssg);
        }

        public static void  TestSurfaceSurveyResultDataTableLoad()
        {
            //processSurfaceSurveyResultDataTableLoad.SurveyResultDataTableLoad(106, "surfacename,time", 1, 50, "A", Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"), out mssg);
        }
        public static void  TestSurfaceSurveyTimeDataDelete()
        {
            //processSurfaceSurveyTimeDataDelete.SurveyTimeDataDelete(106, "A" ,Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"),out mssg);
        }
        public static void TestSurfaceSurveyTimeList()
        {
            //processSurfaceSurveyTimeList.SurveyTimeList(106, "A",out mssg);
        }
        public static void TestSurfaceSurveyPointTimeDataAdd()
        {

        }
        #endregion

        #region 成果库
        public void cgmain()
        {
            ProcessSurfaceCgResultDataImport();
            //ProcessSurfaceCgResultDataTableLoad();
            //ProcessSurfaceCgTimeDataDelete();
            ProcessPrintMssg.Print(mssg);
        }
        public static ProcessSurfaceCgResultDataImport processSurfaceCgResultDataImport = new ProcessSurfaceCgResultDataImport();
        public static ProcessSurfaceCgResultDataTableLoad processSurfaceCgResultDataTableLoad = new ProcessSurfaceCgResultDataTableLoad();
       // public static ProcessSurfaceCgTimeDataDelete processSurfaceCgTimeDataDelete = new ProcessSurfaceCgTimeDataDelete();
        public static ProcessSurfaceCgTimeList processSurfaceCgTimeList = new ProcessSurfaceCgTimeList();

        public static void  ProcessSurfaceCgResultDataImport()
        {
            //processSurfaceCgResultDataImport.CgResultDataImport(106,"A",Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public static void  ProcessSurfaceCgResultDataTableLoad()
        {
            //processSurfaceCgResultDataTableLoad.CgScalarResultDataTableLoad(106,"A",1,500," surfacename ",Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"),out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public static void  ProcessSurfaceCgTimeDataDelete()
        {
            //processSurfaceCgTimeDataDelete.CgTimeDataDelete(106, "A", Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public static void ProcessSurfaceCgTimeList()
        {
 
        }

        #endregion

    }
}
