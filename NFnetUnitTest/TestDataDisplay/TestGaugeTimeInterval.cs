﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;
//using NFnet_BLL.DisplayDataProcess.Gauge;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGaugeTimeInterval
    {
        public static ProcessGaugeTimeIntervalTableLoad processGaugeTimeIntervalTableLoad = new ProcessGaugeTimeIntervalTableLoad();
        public static ProcessGaugeBLL processGaugeBLL = new ProcessGaugeBLL();
        public static string mssg = "";
        public Gauge.Model.gaugetimeinterval model = new Gauge.Model.gaugetimeinterval
        {
            xmno = 29,
            pointname = "韶关库水位",
             hour = 2,
              minute = 0
        };
        public void main()
        {
            //TestTimeIntervalAdd();
            TestTimeIntervalTableLoad();
        }
        public void TestTimeIntervalTableLoad()
        {
            processGaugeTimeIntervalTableLoad.GaugeTimeIntervalTableLoad(29, " 1=1 ", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTimeIntervalAdd()
        {
            processGaugeBLL.ProcessGaugeTimeIntevalAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
