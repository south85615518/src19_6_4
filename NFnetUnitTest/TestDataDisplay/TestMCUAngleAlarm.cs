﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestMCUAngleAlarm
    {
        public ProcessMCUAngleAlarmAdd processMCUAngleAlarmAdd = new ProcessMCUAngleAlarmAdd();
        public ProcessMCUAngleAlarmEdit processMCUAngleAlarmEdit = new ProcessMCUAngleAlarmEdit();
        public ProcessMCUAngleAlarmDel processMCUAngleAlarmDel = new ProcessMCUAngleAlarmDel();
        public ProcessMCUAngleAlarmLoad processMCUAngleAlarmLoad = new ProcessMCUAngleAlarmLoad();
        public ProcessMCUAngleAlarmRecordsCount processMCUAngleAlarmRecordsCount = new ProcessMCUAngleAlarmRecordsCount();

        public ProcessMCUAngleAlarmValues processMCUAngleAlarmValues = new ProcessMCUAngleAlarmValues();
        public ProcessMCUAngleAlarmValueModel processMCUAngleAlarmValueModel = new ProcessMCUAngleAlarmValueModel();
        public static string mssg = "";
        public MDBDATA.Model.mcuanglealarmvalue model = new MDBDATA.Model.mcuanglealarmvalue
        {
            name = "一级预警",
            disp = 130,
            dispL = 106,
            scalevalue = 40,
            scalevalueL = 16,
            xmno = 29
        };
        public void main()
        {
            //Add();
            Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //AlarmNameStr();
            //TestAlarmValueModelGet();
        }
        public void Add()
        {
            processMCUAngleAlarmAdd.AlarmAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processMCUAngleAlarmEdit.AlarmEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processMCUAngleAlarmDel.AlarmDel(29, "一级预警", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processMCUAngleAlarmLoad.AlarmLoad(29, "name", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processMCUAngleAlarmRecordsCount.AlarmRecordsCount(29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void AlarmNameStr()
        {
            processMCUAngleAlarmValues.AlarmValues(29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestAlarmValueModelGet()
        {
            processMCUAngleAlarmValueModel.MCUAngleAlarmValueModel("一级预警", 29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
