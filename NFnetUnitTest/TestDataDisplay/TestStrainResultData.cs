﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Strain;
using NFnet_BLL.DataProcess.Strain.Sensor;
using NFnet_Interface.DisplayDataProcess.Strain;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestStrainResultData
    {
        public ProcessStrainResultDataMaxTime processStrainResultDataMaxTime = new ProcessStrainResultDataMaxTime();
        public ProcessStrainResultDataRqcxConditionCreate processStrainResultDataRqcxConditionCreate = new ProcessStrainResultDataRqcxConditionCreate();
        public ProcessStrainPointLoad processStrainPointLoad = new ProcessStrainPointLoad();
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public ProcessStrainChart processStrainChart = new ProcessStrainChart();
        public string mssg = "";
        public void main()
        {
            TestProcessfillStrainDbFill();
        }
        public void TestStrainResultDataMaxTime()
        {
            //processStrainResultDataMaxTime.StrainResultDataMaxTime(29,out mssg );
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainResultDataRqcxConditionCreate()
        {
            processStrainResultDataRqcxConditionCreate.StrainResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", 29, new DateTime());
        }

        public void TestProcessfillStrainDbFill()
        {
            var sqlmodel = processStrainResultDataRqcxConditionCreate.StrainResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", 29, new DateTime());
            var model = new NFnet_BLL.DisplayDataProcess.FillSensorDbFillCondition("S01",sqlmodel.sql,29);
            processResultDataBLL.ProcessfillStrainDbFill(model);
            var series = processStrainChart.SerializestrStrain(model.sql,29,"'S01'",out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestStrainPointLoad()
        {
            processStrainPointLoad.StrainPointLoad(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        
    }
}
