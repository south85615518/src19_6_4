﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile.GTSensorServer;
using NFnet_BLL.DisplayDataProcess.GTInclinometer;
using NFnet_Interface.DisplayDataProcess.GT;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestgtInclinometer
    {
        //public ProcessGTInclinometer_orglDataBLL processGTInclinometer_orglDataBLL = new ProcessGTInclinometer_orglDataBLL();
        //public data.Model.gtinclinometer model = new data.Model.gtinclinometer { xmno = 106, holename = "yy-2", time = DateTime.Now, deepth = 5 };
        //public string mssg = "";
        //public void main()
        //{
        //    //TestGtInclinometerAdd();
        //    //TestSumAcDiff();
        //}
        //public void TestGtInclinometerAdd()
        //{
        //    processGTInclinometer_orglDataBLL.Add(model,out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        //public void TestSumAcDiff()
        //{
        //    var fixed_inclinometer_orgldataModelGetModel = new ProcessGTInclinometer_orglDataBLL.fixed_inclinometer_orgldataModelGetModel(106,"yy-2",DateTime.Now);
        //    processGTInclinometer_orglDataBLL.GetSumAcDiff(fixed_inclinometer_orgldataModelGetModel,out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}

        


        #region 编辑库

        public ProcessGTInclinometerSurveyResultDataTableLoad processSurveyGTInclinometerResultDataTableLoad = new ProcessGTInclinometerSurveyResultDataTableLoad();
        public ProcessGTInclinometerSurveyDataTableCountLoad processSurveyGTInclinometerDataTableCountLoad = new ProcessGTInclinometerSurveyDataTableCountLoad();
        public ProcessGTInclinometerSurveyTimeList processgtPointNameDateTimeListLoad = new ProcessGTInclinometerSurveyTimeList();
        public static string mssg = "";
        public void main()
        {
            //TestGTInclinometerResultDataMaxTime();
            //TestGTInclinometerResultDataRqcxConditionCreate();
            //TestProcessfillGTInclinometerDbFill();
            //TestGTInclinometerResultDataPlotLineAlarm();
            //TestGTInclinometerResultDataTableLoad();
            //TestGTInclinometerResultDataTableCountLoad();
            TestResultDataDateTimeListLoad();
        }
      

        public void TestGTInclinometerResultDataTableLoad()
        {
            //processSurveyGTInclinometerResultDataTableLoad.SurveyResultDataTableLoad(106, "yyl-2", 1, 20, "holename", Convert.ToDateTime("2018-09-17"), Convert.ToDateTime("2018-09-18"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        //public void TestGTInclinometerResultDataTableLoad()
        //{
        //    processGTInclinometerResultDataTableLoad.GTInclinometerResultDataTableLoad(106, "B1(Y5-Y6/P6-P7)-1北", 1, 20, "point_name", Convert.ToDateTime("2018-09-17"), Convert.ToDateTime("2018-09-18"), out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        public void TestGTInclinometerResultDataTableCountLoad()
        {
            //processSurveyGTInclinometerDataTableCountLoad.SurveyDataTableCountLoad(106, "B1(Y5-Y6/P6-P7)-1北", Convert.ToDateTime("2018-09-17"), Convert.ToDateTime("2018-09-18"), out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public void TestResultDataDateTimeListLoad()
        {
            //;
            ProcessPrintMssg.Print(mssg);
        }
     
        public static ProcessGTInclinometerSurveyDataUpdate processGTInclinometerSurveyDataUpdate = new ProcessGTInclinometerSurveyDataUpdate();
        public static ProcessGTInclinometerSurveyPointCYCDataAdd processGTInclinometerSurveyPointTimeDataAdd = new ProcessGTInclinometerSurveyPointCYCDataAdd();
        public static ProcessGTInclinometerSurveyTimeDataDelete processGTInclinometerSurveyTimeDataDelete = new ProcessGTInclinometerSurveyTimeDataDelete();
        public static ProcessGTInclinometerSurveyTimeList processGTInclinometerSurveyTimeList = new ProcessGTInclinometerSurveyTimeList();
        public static void TestSurfaceSurveyDataAdd()
        {
            //processGTInclinometerSurveyPointTimeDataAdd.SurveyPointTimeDataAdd(106, "A", Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"), out mssg);
        }
        public static void TestSurfaceSurveyDataUpdate()
        {
            processGTInclinometerSurveyDataUpdate.SurveyDataUpdate(106, "A", Convert.ToDateTime("2018-10-08 15:10:06"), "this_val", "ac_val", Convert.ToDateTime("2018-10-10 18:17:02"), out mssg);
        }

        //public static void TestSurfaceSurveyResultDataTableLoad()
        //{
        //    processGTInclinometerSurveyResultDataTableLoad.SurveyGTInclinometerResultDataTableLoad(106, "surfacename,time", 1, 50, "A", Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"), out mssg);
        //}
        public static void TestSurfaceSurveyTimeDataDelete()
        {
            //processGTInclinometerSurveyTimeDataDelete.SurveyTimeDataDelete(106, "A", Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 18:17:02"), out mssg);
        }
        public static void TestSurfaceSurveyTimeList()
        {
            //processGTInclinometerSurveyTimeList.SurveyTimeList(106, "A", out mssg);
        }
        public static void TestSurfaceSurveyPointTimeDataAdd()
        {

        }
        #endregion
        #region 成果库

        public void cgmain()
        {
            //ProcessGTInclinometerCgResultDataImport();
            //ProcessGTInclinometerCgResultDataTableLoad();
            //ProcessGTInclinometerCgTimeDataDelete();
            var model = Aspect.IndirectValue.MonitorInfoGetFromUserID("xiexuming",out mssg);
            //ProcessGTInclinometerCgTimeList();
            ProcessPrintMssg.Print(mssg);
        }
        public static ProcessGTInclinometerCgResultDataImport processGTInclinometerCgResultDataImport = new ProcessGTInclinometerCgResultDataImport();
        public static ProcessGTInclinometerCgResultDataTableLoad processGTInclinometerCgResultDataTableLoad = new ProcessGTInclinometerCgResultDataTableLoad();
        public static ProcessGTInclinometerCgTimeDataDelete processGTInclinometerCgTimeDataDelete = new ProcessGTInclinometerCgTimeDataDelete();
        public static ProcessGTInclinometerCgTimeList processGTInclinometerCgTimeList = new ProcessGTInclinometerCgTimeList();

        public static void ProcessGTInclinometerCgResultDataImport()
        {
            //processGTInclinometerCgResultDataImport.CgResultDataImport(106, "yyl-2", Convert.ToDateTime("2013-6-08 15:10:06"), Convert.ToDateTime("2013-6-10 18:17:02"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public static void ProcessGTInclinometerCgResultDataTableLoad()
        {
            //processGTInclinometerCgResultDataTableLoad.CgScalarResultDataTableLoad(106, "yyl-2", 1, 500, " holename ", Convert.ToDateTime("2013-6-08 15:10:06"), Convert.ToDateTime("2013-6-10 15:10:06"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public static void ProcessGTInclinometerCgTimeDataDelete()
        {
            //processGTInclinometerCgTimeDataDelete.CgTimeDataDelete(106, "yyl-2", Convert.ToDateTime("2013-6-08 15:10:06"), Convert.ToDateTime("2013-6-15 15:10:06"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public static void ProcessGTInclinometerCgTimeList()
        {
            //processGTInclinometerCgTimeList.CgTimeList(106, "yyl-2",out mssg);
            ProcessPrintMssg.Print(mssg);
        }


        #endregion
    }
}
