﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.HXYL;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestHXYLAlarm
    {
        public ProcessHXYLAlarmAdd processHXYLAlarmAdd = new ProcessHXYLAlarmAdd();
        public ProcessHXYLAlarmEdit processHXYLAlarmEdit = new ProcessHXYLAlarmEdit();
        public ProcessHXYLAlarmDel processHXYLAlarmDel = new ProcessHXYLAlarmDel();
        public ProcessHXYLAlarmLoad processHXYLAlarmLoad = new ProcessHXYLAlarmLoad();
        public ProcessHXYLAlarmRecordsCount processHXYLAlarmRecordsCount = new ProcessHXYLAlarmRecordsCount();

        public ProcessHXYLAlarmValues processHXYLAlarmValues = new ProcessHXYLAlarmValues();
        public ProcessHXYLAlarmValueModel processHXYLAlarmValueModel = new ProcessHXYLAlarmValueModel();
        public static string mssg = "";
        public MDBDATA.Model.hxylalarmvalue model = new MDBDATA.Model.hxylalarmvalue
        {
            name = "一级预警",
            hyetal = 120,
            ac_hyetal = 6000,
            xmno = 29
        };
        public void main()
        {
            //Add();
            //Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //AlarmNameStr();
            AlarmValueModel();
        }
        public void Add()
        {
            processHXYLAlarmAdd.AlarmAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processHXYLAlarmEdit.AlarmEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processHXYLAlarmDel.AlarmDel(29, "一级预警", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processHXYLAlarmLoad.AlarmLoad(29,"name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processHXYLAlarmRecordsCount.AlarmRecordsCount(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void AlarmNameStr()
        {
            processHXYLAlarmValues.AlarmValues(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void AlarmValueModel()
        {
            processHXYLAlarmValueModel.HXYLAlarmValueModel(29, "一级预警", out mssg);
            ProcessPrintMssg.Print(mssg);

        }

    }
}
