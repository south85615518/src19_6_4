﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.pub;
using System.Data;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestAspectSeries
    {
        public ProcessAspectChart processAspectChart = new ProcessAspectChart();
        public string mssg = "";
        public void main()
        {
            //var model = processAspectChart.AspectChart(96,out mssg);
            TestSerializestrGTSensordata();
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSerializestrGTSensordata() {
            DataTable dt = new DataTable();
            dt.Columns.Add("point_name");
            dt.Columns.Add("type");
            DataRow dr = dt.NewRow();
            dr["point_name"] = "Gjj206-2-8";
            dr["type"] = "围护墙钢筋应力";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["point_name"] = "Gjj206-2-6";
            dr["type"] = "围护墙钢筋应力";
            dt.Rows.Add(dr);
            var model = processAspectChart.ProcessSerializestrGTSensordata(96,dt,data.Model.gtsensortype._enclosureWallReinforcementStress);
        }
    }
}
