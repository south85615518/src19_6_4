﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestMCUDATAAlarm
    {
        public ProcessMCUAlarmModelListGet processMCUAlarmModelListGet = new ProcessMCUAlarmModelListGet();
        public static ProcessBKGMCUPointLoad processBKGMCUPointLoad = new ProcessBKGMCUPointLoad();
        public ProcessMCUAlarmDataCount processMCUAlarmDataCount = new ProcessMCUAlarmDataCount();
        public ProcessSmsSendBLL processSmsSendBLL = new ProcessSmsSendBLL();
        public string mssg = "";
        public void main()
        {
            //TestMCUAlarmModelListGet();
            MCUDATAAlarm();
            //TestMCUAlarmDataCount();
        }
        public void TestMCUAlarmModelListGet()
        {
            List<string> pointnamelist = processBKGMCUPointLoad.BKGMCUPointLoad("韶关铀业",out mssg);
            processMCUAlarmModelListGet.MCUAlarmModelListGet("韶关铀业", pointnamelist,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMCUAlarmDataCount()
        {
            List<string> pointnamelist = processBKGMCUPointLoad.BKGMCUPointLoad("韶关铀业",out mssg);
            processMCUAlarmDataCount.MCUAlarmDataCount("韶关铀业", pointnamelist,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void MCUDATAAlarm()
        {
            List<string> pointnamelist = processBKGMCUPointLoad.BKGMCUPointLoad("韶关铀业",out mssg);
            ProcessMCUDataAlarmBLL ProcessMCUDataAlarmBLL = new ProcessMCUDataAlarmBLL("韶关铀业", 29, pointnamelist);
            ProcessMCUDataAlarmBLL.main();
            processSmsSendBLL.ProcessSmsSend(ProcessMCUDataAlarmBLL.alarmInfoList, "韶关铀业", Aspect.IndirectValue.GetXmnoFromXmname("韶关铀业"), out mssg);
            //ProcessPrintMssg.Print(string.Join(",",ProcessMCUDataAlarmBLL.alarmInfoList));
        }
        
    }
}
