﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GTSettlement;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGTSettlementPointAlarmValue
    {
        public ProcessSettlementPointAlarmModel processSettlementPointAlarmModel = new ProcessSettlementPointAlarmModel();
        public ProcessGTSettlementPointAlarmRecordsCount processSettlementPointAlarmRecordsCount = new ProcessGTSettlementPointAlarmRecordsCount();
        public ProcessSettlementPointAlarmValueAdd processSettlementPointAlarmValueAdd = new ProcessSettlementPointAlarmValueAdd();
        public ProcessSettlementPointAlarmValueDel processSettlementPointAlarmValueDel = new ProcessSettlementPointAlarmValueDel();
        public ProcessSettlementPointAlarmValueEdit processSettlementPointAlarmValueEdit = new ProcessSettlementPointAlarmValueEdit();
        public ProcessSettlementPointAlarmValueLoad processSettlementPointAlarmValueLoad = new ProcessSettlementPointAlarmValueLoad();
        public ProcessSettlementPointAlarmValueMutilEdit processSettlementPointAlarmValueMutilEdit = new ProcessSettlementPointAlarmValueMutilEdit();
        public ProcessGTSettlementBasePointLoad processGTSettlementBasePointLoad = new ProcessGTSettlementBasePointLoad();
        public data.Model.gtsettlementpointalarmvalue model = new data.Model.gtsettlementpointalarmvalue { point_name = "s1", firstalarmname = "一级预警", secalarmname = "二级预警", thirdalarmname = "三级预警", remark = "应力1号点", xmno = 106, deviceno = 90, addressno = 100, dtuno = "31919", initsettlementval = 0 };
        public string mssg = "";
        public void main()
        {
            //TestSettlementPointAlarmAdd();
            //TestSettlementPointAlarmEdit();
            //TestSettlementPointAlarmDel();
            //TestSettlementPointAlarmLoad();
            //TestSettlementPointAlarmModel();
            TestSettlementPointAlarmValueMutilEdit();
            //TestSettlementBasePointLoad();
        }
        public void TestSettlementPointAlarmAdd()
        {
            processSettlementPointAlarmValueAdd.PointAlarmValueAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmEdit()
        {
            model.thirdalarmname = "一级预警";
            processSettlementPointAlarmValueEdit.PointAlarmValueEdit(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmDel()
        {
            processSettlementPointAlarmValueDel.SettlementPointAlarmValueDel(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmLoad()
        {
            processSettlementPointAlarmValueLoad.PointAlarmValueLoad(106," 1=1 ","point_name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmModel()
        {
            processSettlementPointAlarmModel.SettlementPointAlarmModel(106,"s1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmValueMutilEdit()
        {
            processSettlementPointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model,"s1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementBasePointLoad()
        {
            processGTSettlementBasePointLoad.SettlementBasePointLoadBLL(106,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
