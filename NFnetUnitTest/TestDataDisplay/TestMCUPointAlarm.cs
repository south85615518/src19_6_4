﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestMCUPointAlarmValue
    {
        public ProcessMCUPointAlarmValueAdd processMCUPointAlarmValueAdd = new ProcessMCUPointAlarmValueAdd();
        public ProcessMCUPointAlarmValueEdit processMCUPointAlarmValueEdit = new ProcessMCUPointAlarmValueEdit();

        public ProcessMCUPointAlarmValueDel processMCUPointAlarmValueDel = new ProcessMCUPointAlarmValueDel();
        public ProcessMCUPointAlarmValueLoad processMCUPointAlarmValueLoad = new ProcessMCUPointAlarmValueLoad();
        public ProcessMCUPointAlarmRecordsCount processMCUPointAlarmRecordsCount = new ProcessMCUPointAlarmRecordsCount();
        public ProcessMCUPointAlarmValueMutilEdit processMCUPointAlarmValueMutilEdit = new ProcessMCUPointAlarmValueMutilEdit();
        public ProcessMCUPointAlarmModel processMCUPointAlarmModel = new ProcessMCUPointAlarmModel();
        public static string mssg = "";
        public MDBDATA.Model.mcupointalarmvalue model = new MDBDATA.Model.mcupointalarmvalue
        {
            point_name = "韶关745-J1-4",
            firstalarm = "1级预警",
            secalarm = "1级预警",
            thirdalarm = "1级预警",
            pointtype = "浸润线",
            remark = "",
            xmno = 29,
            holedepth = 20,
            id=6

        };
        public void main()
        {
            //Add();
            Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //MultiEdit();
            //TestMCUPointAlarmModelGet();
        }
        public void Add()
        {
            processMCUPointAlarmValueAdd.PointAlarmValueAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processMCUPointAlarmValueEdit.PointAlarmValueEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void MultiEdit()
        {
            processMCUPointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model, "韶关745-J1-1','韶关745-J1-2", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processMCUPointAlarmValueDel.PointAlarmValueDel(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processMCUPointAlarmValueLoad.PointAlarmValueLoad("数据测试",29," 1=1 ", "point_name", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processMCUPointAlarmRecordsCount.PointAlarmRecordsCount("",29," 1=1 ",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMCUPointAlarmModelGet()
        {
            processMCUPointAlarmModel.MCUPointAlarmModel(29, "韶关745-J1-1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
