﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestMCUAngleDATAAlarm
    {
        public ProcessMCUAngleAlarmModelListGet processMCUAngleAlarmModelListGet = new ProcessMCUAngleAlarmModelListGet();
        public ProcessMCUAngleAlarmDataCount processMCUAngleAlarmDataCount = new ProcessMCUAngleAlarmDataCount();
        public static ProcessBKGMCUAnglePointLoad processBKGMCUAnglePointLoad = new ProcessBKGMCUAnglePointLoad();
        public string mssg = "";
        public void main()
        {
            //TestMCUAngleAlarmModelListGet();
            //MCUAngleDATAAlarm();
            TestMCUAlarmDataCount();
        }
        public void TestMCUAngleAlarmModelListGet()
        {
            List<string> pointnamelist = processBKGMCUAnglePointLoad.BKGMCUAnglePointLoad("韶关铀业", out mssg);
            processMCUAngleAlarmModelListGet.MCUAngleAlarmModelListGet("韶关铀业", pointnamelist, out mssg);
            ProcessPrintMssg.Print(mssg);

        }
        public void TestMCUAlarmDataCount()
        {
            List<string> pointnamelist = processBKGMCUAnglePointLoad.BKGMCUAnglePointLoad("韶关铀业", out mssg);
            processMCUAngleAlarmDataCount.MCUAngleAlarmDataCount("韶关铀业", pointnamelist, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void MCUAngleDATAAlarm()
        {
            List<string> pointnamelist = processBKGMCUAnglePointLoad.BKGMCUAnglePointLoad("韶关铀业", out mssg);
            ProcessMCUAngleDataAlarmBLL processMCUAngleDataAlarmBLL = new ProcessMCUAngleDataAlarmBLL("韶关铀业", 29, pointnamelist);
            processMCUAngleDataAlarmBLL.main();
            ProcessPrintMssg.Print(string.Join(",", processMCUAngleDataAlarmBLL.alarmInfoList));
        }
    }
}
