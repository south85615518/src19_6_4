﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.HXYL;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestHXYLPointAlarmValue
    {
        public ProcessHXYLPointAlarmValueAdd processHXYLPointAlarmValueAdd = new ProcessHXYLPointAlarmValueAdd();
        public ProcessHXYLPointAlarmValueEdit processHXYLPointAlarmValueEdit = new ProcessHXYLPointAlarmValueEdit();

        public ProcessHXYLPointAlarmValueDel processHXYLPointAlarmValueDel = new ProcessHXYLPointAlarmValueDel();
        public ProcessHXYLPointAlarmValueLoad processHXYLPointAlarmValueLoad = new ProcessHXYLPointAlarmValueLoad();
        public ProcessHXYLPointAlarmRecordsCount processHXYLPointAlarmRecordsCount = new ProcessHXYLPointAlarmRecordsCount();
        public ProcessHXYLPointAlarmValueMutilEdit processHXYLPointAlarmValueMutilEdit = new ProcessHXYLPointAlarmValueMutilEdit();
        public ProcessHXYLPointAlarmModel processHXYLPointAlarmModel = new ProcessHXYLPointAlarmModel();
        public static string mssg = "";
        public MDBDATA.Model.hxylpointalarmvalue model = new MDBDATA.Model.hxylpointalarmvalue
        {
            point_name = "韶关745-J1-2",
            firstalarm = "一级预警",
            secalarm = "一级预警",
            thirdalarm = "一级预警",
            pointtype = "雨量",
            remark = "",
             xmno = 29,
             id=1

        };
        public void main()
        {
            //Add();
            //Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //MultiEdit();
            PointAlarmModel();
        }
        public void Add()
        {
            processHXYLPointAlarmValueAdd.PointAlarmValueAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processHXYLPointAlarmValueEdit.PointAlarmValueEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processHXYLPointAlarmValueDel.PointAlarmValueDel(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processHXYLPointAlarmValueLoad.PointAlarmValueLoad("数据测试",29," 1=1 ", "point_name", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processHXYLPointAlarmRecordsCount.PointAlarmRecordsCount("",29," 1=1 ",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void MultiEdit()
        {
            processHXYLPointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model, "韶关745-J1-1','韶关745-J1-2", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void PointAlarmModel()
        {
            processHXYLPointAlarmModel.HXYLPointAlarmModel(29,"3号设备",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
