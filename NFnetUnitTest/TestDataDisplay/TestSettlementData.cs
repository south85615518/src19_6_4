﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestSettlementData
    {
        public NFnet_BLL.DisplayDataProcess.Settlement.Senor.ProcessSettlementDataBLL processSettlementDataBLL = new NFnet_BLL.DisplayDataProcess.Settlement.Senor.ProcessSettlementDataBLL();

        public global::Settlement.Model.Settlementresultdata model = new Settlement.Model.Settlementresultdata {
            pointname = "ST01",
            xmno = 29,
            cyc = 1,
            monitorTime = DateTime.Now,
            elevation = 10.895,
            initelevation = 0, ac_val = 10.895, this_val =2.3432
        };
        public string mssg = "";
        public void main()
        {
            TestSettlementDataAdd();
        }
        public void TestSettlementDataAdd()
        {
            processSettlementDataBLL.Add(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
