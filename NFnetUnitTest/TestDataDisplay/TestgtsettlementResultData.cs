﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GTSettlement;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGTSettlementResultData
    {
        #region 测量库
        public ProcessSettlementResultDataMaxTime processSettlementResultDataMaxTime = new ProcessSettlementResultDataMaxTime();
        public ProcessSettlementResultDataRqcxConditionCreate processSettlementResultDataRqcxConditionCreate = new ProcessSettlementResultDataRqcxConditionCreate();
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public ProcessSettlementChart processSettlementChart = new ProcessSettlementChart();
        public ProcessSettlementResultDataPlotLineAlarm processSettlementResultDataPlotLineAlarm = new ProcessSettlementResultDataPlotLineAlarm();
        public ProcessSettlementResultDataTableLoad processSettlementResultDataTableLoad = new ProcessSettlementResultDataTableLoad();
        public ProcessSettlementDataTableCountLoad processSettlementDataTableCountLoad = new ProcessSettlementDataTableCountLoad();
        public ProcessSettlementPointNameDateTimeListLoad processgtPointNameDateTimeListLoad = new ProcessSettlementPointNameDateTimeListLoad();
        public static string mssg = "";
        public void main()
        {
            //TestSettlementResultDataMaxTime();
            //TestSettlementResultDataRqcxConditionCreate();
            //TestProcessfillSettlementDbFill();
            TestSettlementResultDataPlotLineAlarm();
            TestSettlementResultDataTableLoad();
            TestSettlementResultDataTableCountLoad();
            TestResultDataDateTimeListLoad();
        }
        public void TestSettlementResultDataMaxTime()
        {
            processSettlementResultDataMaxTime.SettlementResultDataMaxTime(106,out mssg );
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementResultDataRqcxConditionCreate()
        {
            processSettlementResultDataRqcxConditionCreate.SettlementResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", 106, new DateTime());
        }

        public void TestProcessfillSettlementDbFill()
        {
            var sqlmodel = processSettlementResultDataRqcxConditionCreate.SettlementResultDataRqcxConditionCreate("2018-9-18", "2018-9-20", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", 106, new DateTime());
            var model = new NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition("s1",sqlmodel.sql,106);
            processResultDataBLL.ProcessfillTotalStationDbFill(model);
            var series = processSettlementChart.SerializestrSettlement(model.sql,106,"'s1'",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementResultDataPlotLineAlarm()
        {
            processSettlementResultDataPlotLineAlarm.SettlementResultDataPlotLineAlarm("s1",106);
        }

        public void TestSettlementResultDataTableLoad()
        {
            processSettlementResultDataTableLoad.SettlementResultDataTableLoad(106, "B1(Y5-Y6/P6-P7)-1北", 1, 20, "point_name", Convert.ToDateTime("2018-09-17"), Convert.ToDateTime("2018-09-18"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        //public void TestSettlementResultDataTableLoad()
        //{
        //    processSettlementResultDataTableLoad.SettlementResultDataTableLoad(106, "B1(Y5-Y6/P6-P7)-1北", 1, 20, "point_name", Convert.ToDateTime("2018-09-17"), Convert.ToDateTime("2018-09-18"), out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        public void TestSettlementResultDataTableCountLoad()
        {
            processSettlementDataTableCountLoad.SettlementDataTableCountLoad(106, "B1(Y5-Y6/P6-P7)-1北", Convert.ToDateTime("2018-09-17"), Convert.ToDateTime("2018-09-18"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestResultDataDateTimeListLoad()
        {
            processgtPointNameDateTimeListLoad.GtPointNameDateTimeListLoad(106, "B1(Y5-Y6/P6-P7)-1北", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        #endregion
        #region 编辑库
        public static ProcessSettlementSurveyDataAdd processSettlementSurveyDataAdd = new ProcessSettlementSurveyDataAdd();
        public static ProcessSettlementSurveyDataUpdate processSettlementSurveyDataUpdate = new ProcessSettlementSurveyDataUpdate();
        public static ProcessSettlementSurveyPointTimeDataAdd processSettlementSurveyPointTimeDataAdd = new ProcessSettlementSurveyPointTimeDataAdd();
        public static ProcessSurveySettlementResultDataTableLoad processSettlementSurveyResultDataTableLoad = new ProcessSurveySettlementResultDataTableLoad();
        public static ProcessSettlementSurveyTimeDataDelete processSettlementSurveyTimeDataDelete = new ProcessSettlementSurveyTimeDataDelete();
        public static ProcessSettlementSurveyTimeList processSettlementSurveyTimeList = new ProcessSettlementSurveyTimeList();
        public void SurveyMain()
        {
            //TestSettlementSurveyDataAdd();
            //TestSettlementSurveyDataUpdate();
            //TestSettlementSurveyResultDataTableLoad();
            //TestSettlementSurveyTimeDataDelete();
            TestSettlementSurveyTimeList();
            ProcessPrintMssg.Print(mssg);
        }
        public static void TestSettlementSurveyDataAdd()
        {
            processSettlementSurveyDataAdd.SurveyDataAdd(106, "s1", Convert.ToDateTime("2018-9-12 15:10:06"), Convert.ToDateTime("2018-9-15 18:17:02"), out mssg);
        }
        public static void TestSettlementSurveyDataUpdate()
        {
            processSettlementSurveyDataUpdate.SurveyDataUpdate(106, "s1", Convert.ToDateTime("2018-9-12 15:10:06"), "this_val", "ac_val", Convert.ToDateTime("2018-9-15 18:17:02"), out mssg);
        }

        public static void TestSettlementSurveyResultDataTableLoad()
        {
            processSettlementSurveyResultDataTableLoad.SurveySettlementResultDataTableLoad(106, "point_name,time", 1, 50, "s1", Convert.ToDateTime("2018-9-12 15:10:06"), Convert.ToDateTime("2018-9-15 18:17:02"), out mssg);
        }
        public static void TestSettlementSurveyTimeDataDelete()
        {
            processSettlementSurveyTimeDataDelete.SurveyTimeDataDelete(106, "s1", Convert.ToDateTime("2018-9-12 15:10:06"), Convert.ToDateTime("2018-9-15 18:17:02"), out mssg);
        }
        public static void TestSettlementSurveyTimeList()
        {
            processSettlementSurveyTimeList.SurveyTimeList(106, "s1", out mssg);
        }
        public static void TestSettlementSurveyPointTimeDataAdd()
        {

        }
        #endregion
        #region 成果库

        public void cgmain()
        {
            //ProcessSettlementCgResultDataImport();
            //ProcessSettlementCgResultDataTableLoad();
            //ProcessSettlementCgTimeDataDelete();
            ProcessPrintMssg.Print(mssg);
        }
        public static ProcessSettlementCgResultDataImport processSettlementCgResultDataImport = new ProcessSettlementCgResultDataImport();
        public static ProcessSettlementCgResultDataTableLoad processSettlementCgResultDataTableLoad = new ProcessSettlementCgResultDataTableLoad();
        public static ProcessSettlementCgTimeDataDelete processSettlementCgTimeDataDelete = new ProcessSettlementCgTimeDataDelete();
        public static ProcessSettlementCgTimeList processSettlementCgTimeList = new ProcessSettlementCgTimeList();

        public static void ProcessSettlementCgResultDataImport()
        {
            processSettlementCgResultDataImport.CgResultDataImport(106, "s1", Convert.ToDateTime("2018-9-12 15:10:06"), Convert.ToDateTime("2018-9-15 18:17:02"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public static void ProcessSettlementCgResultDataTableLoad()
        {
            processSettlementCgResultDataTableLoad.CgScalarResultDataTableLoad(106, "s1", 1, 500, " surfacename ", Convert.ToDateTime("2018-9-12 15:10:06"), Convert.ToDateTime("2018-9-15 18:17:02"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public static void ProcessSettlementCgTimeDataDelete()
        {
            processSettlementCgTimeDataDelete.CgTimeDataDelete(106, "s1", Convert.ToDateTime("2018-9-12 15:10:06"), Convert.ToDateTime("2018-9-15 18:17:02"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public static void ProcessSettlementCgTimeList()
        {

        }


        #endregion
    }
}
