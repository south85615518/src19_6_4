﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestMCUTimeInterval
    {
        public static ProcessMCUTimeIntervalTableLoad processMCUTimeIntervalTableLoad = new ProcessMCUTimeIntervalTableLoad();
        public static ProcessMCUBLL processMCUBLL = new ProcessMCUBLL();
        public static string mssg = "";
        public MDBDATA.Model.bkgtimeinterval model = new MDBDATA.Model.bkgtimeinterval
        {
            xmno = 29,
            pointname = "韶关745-N1-1",
             hour = 5,
              minute = 10
        };
        public void main()
        {
            TestTimeIntervalTableLoad();
            //TestTimeIntervalAdd();
        }
        public void TestTimeIntervalTableLoad()
        {
            processMCUTimeIntervalTableLoad.MCUTimeIntervalTableLoad(29, " 1=1 ", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTimeIntervalAdd()
        {
            processMCUBLL.ProcessMCUTimeIntevalAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
