﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GNSS;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGNSSDATAAlarm
    {
        public ProcessGNSSAlarmModelListGet processGNSSAlarmModelListGet = new ProcessGNSSAlarmModelListGet();
        public ProcessGNSSPointLoadBLL processGNSSPointLoadBLL = new ProcessGNSSPointLoadBLL();
        //public static ProcessBKGGNSSPointLoad processBKGGNSSPointLoad = new ProcessBKGGNSSPointLoad();
        public ProcessGNSSAlarmDataCount processGNSSAlarmDataCount = new ProcessGNSSAlarmDataCount();
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public static ProcessSmsSendBLL smsSendBLL = new ProcessSmsSendBLL();
        public string mssg = "";
        public void main()
        {
            //TestGNSSAlarmModelListGet();
            //TestDATAAlarm();
            //TestGNSSAlarmDataCount();
            TestDATAAlarm();
        }
        public void TestGNSSAlarmModelListGet()
        {
            List<string> pointnamelist = processGNSSPointLoadBLL.GNSSPointLoadBLL(29, out mssg);
            processGNSSAlarmModelListGet.GNSSAlarmModelListGet(29, pointnamelist, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGNSSAlarmDataCount()
        {
            List<string> pointnamelist = processGNSSPointLoadBLL.GNSSPointLoadBLL(29, out mssg);

            processGNSSAlarmDataCount.GNSSAlarmDataCount(pointnamelist, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDATAAlarm()
        {
            List<string> pointnamelist = processGNSSPointLoadBLL.GNSSPointLoadBLL(29, out mssg);

            ProcessGNSSDataAlarmBLL processGNSSDataAlarmBLL = new ProcessGNSSDataAlarmBLL("韶关铀业", 29, pointnamelist);
            processGNSSDataAlarmBLL.main();
           // emailSendBLL.ProcessEmailSend(processGNSSDataAlarmBLL.alarmInfoList, "韶关铀业", Aspect.IndirectValue.GetXmnoFromXmname("韶关铀业"), out mssg);
            smsSendBLL.ProcessSmsSend(processGNSSDataAlarmBLL.alarmInfoList, "韶关铀业", Aspect.IndirectValue.GetXmnoFromXmname("韶关铀业"), out mssg);
        
            ProcessPrintMssg.Print(string.Join(",", processGNSSDataAlarmBLL.alarmInfoList));
        }
    }
}

