﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GNSS;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGNSSPointAlarmValue
    {
        public ProcessGNSSPointAlarmValueAdd processGNSSPointAlarmValueAdd = new ProcessGNSSPointAlarmValueAdd();
        public ProcessGNSSPointAlarmValueEdit processGNSSPointAlarmValueEdit = new ProcessGNSSPointAlarmValueEdit();

        public ProcessGNSSPointAlarmValueDel processGNSSPointAlarmValueDel = new ProcessGNSSPointAlarmValueDel();
        public ProcessGNSSPointAlarmValueLoad processGNSSPointAlarmValueLoad = new ProcessGNSSPointAlarmValueLoad();
        public ProcessGNSSPointAlarmRecordsCount processGNSSPointAlarmRecordsCount = new ProcessGNSSPointAlarmRecordsCount();
        public ProcessGNSSPointAlarmValueMutilEdit processGNSSPointAlarmValueMutilEdit = new ProcessGNSSPointAlarmValueMutilEdit();
        public ProcessGNSSPointAlarmModel processGNSSPointAlarmModel = new ProcessGNSSPointAlarmModel();
        public static string mssg = "";
        public GPS.Model.gnsspointalarmvalue model = new GPS.Model.gnsspointalarmvalue
        {
            point_name = "韶关745-J1-2",
            firstalarm = "一级预警",
            secalarm = "一级预警",
            thirdalarm = "一级预警",
            pointtype = "GPS",
            remark = "",
             xmno = 29,
             id=1

        };
        public void main()
        {
            //Add();
            //Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //MultiEdit();
            PointAlarmValue();
        }
        public void Add()
        {
            processGNSSPointAlarmValueAdd.PointAlarmValueAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processGNSSPointAlarmValueEdit.PointAlarmValueEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processGNSSPointAlarmValueDel.PointAlarmValueDel(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processGNSSPointAlarmValueLoad.PointAlarmValueLoad("数据测试",29," 1=1 ", "point_name", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processGNSSPointAlarmRecordsCount.PointAlarmRecordsCount("",29," 1=1 ",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void MultiEdit()
        {
            processGNSSPointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model, "韶关745-J1-1','韶关745-J1-2",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void PointAlarmValue()
        {
            processGNSSPointAlarmModel.GNSSPointAlarmModel(29, "sg01", out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
