﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestDTU
    {
        public static string mssg = "";
        public static ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();
        public static ProcessDTUTimeIntervalLoad processDTUTimeIntervalLoad = new ProcessDTUTimeIntervalLoad();
        public static ProcessDTUQueueLoad processDTUQueueLoad = new ProcessDTUQueueLoad();
        public static ProcessDTUTaskQueueFinishedSet processDTUTaskQueueFinishedSet = new ProcessDTUTaskQueueFinishedSet();
        public static ProcessDTUAlarmModelListGet processDTUAlarmModelListGet = new ProcessDTUAlarmModelListGet();

        public static ProcessFixedInclinometerTimetaskIntervalTable processFixedInclinometerTimetaskIntervalTable = new ProcessFixedInclinometerTimetaskIntervalTable();
        public static ProcessFixedInclinometerXmPortTimetaskIntervalTable processFixedInclinometerXmPortTimetaskIntervalTable = new ProcessFixedInclinometerXmPortTimetaskIntervalTable();

        public static ProcessFixedInclinometerModuleLPLoad processFixedInclinometerModuleLPLoad = new ProcessFixedInclinometerModuleLPLoad();
        public static ProcessFixedInclinometerXmPortModuleLPLoad processFixedInclinometerXmPortModuleLPLoad = new ProcessFixedInclinometerXmPortModuleLPLoad();



        public static DTU.Model.dtudata model = new DTU.Model.dtudata
        {
            point_name = "DTU_1",
            mkh = 1,
            deep = 1000,
            groundElevation = 1300,
            time = DateTime.Now,
            xmno = 29

        };
        //public static DTU.Model.dtutimetask model = new DTU.Model.dtutimetask
        //{

        //    hour = 0,
        //    minute = 10,
        //    module = "4850001",
        //    sec = 0,
        //    time = DateTime.Now,
        //    times = 1,
        //    xmno = 29


        //};
        public void main()
        {
            //TestDTUDataAdd();
            //TestDTUtaskLoad();
            //TestDTUTaskQueue();
            //TestDTUTaskQueueFinishedSet();
            //TestDTUDATAModeListlLoad();
            //TestDTUDataTmpDel();

            TestFixedInclinometertaskLoad();
            TestFixedInclinometerLPLoad();
            TestFixedInclinometerXmPortTimetaskIntervalTable();
            TestFixedInclinometerXmPortModuleLPLoad();
        }
        public void TestDTUDataAdd()
        {
            processDTUDATABLL.ProcessDTUDATAInsertBLL(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDTUDataTmpDel()
        {
            processDTUDATABLL.ProcessDATATmpDel(29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDTUDATAModeListlLoad()
        {
            processDTUAlarmModelListGet.DTUAlarmModelListGet(29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDTUtaskLoad()
        {
           var model =  processDTUTimeIntervalLoad.DTUTimeIntervalLoad(29, "4850001", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDTUTaskQueue()
        {
            processDTUQueueLoad.DTUQueueLoad(29, "4850003", out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestDTUTaskQueueFinishedSet()
        {
            processDTUTaskQueueFinishedSet.DTUTaskQueueFinishedSet(29, "DTU_1", 0, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestFixedInclinometertaskLoad()
        {
            processFixedInclinometerTimetaskIntervalTable.FixedInclinometerTimetaskIntervalTableLoad(1,20,29,"deviceno","asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestFixedInclinometerLPLoad()
        {
            processFixedInclinometerModuleLPLoad.FixedInclinometerModuleLPLoad(1, 20, 29, "deviceno", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestFixedInclinometerXmPortTimetaskIntervalTable()
        {
            processFixedInclinometerXmPortTimetaskIntervalTable.FixedInclinometerXmPortTimetaskIntervalTable(1, 20, 29,"UTS1", "point_name", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestFixedInclinometerXmPortModuleLPLoad()
        {
            processFixedInclinometerXmPortModuleLPLoad.FixedInclinometerXmPortModuleLPLoad(1, 20, 29,"UTS1", "deviceno", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        //public void TestDTUTimeTaskAdd()
        //{

        //}

    }
}
