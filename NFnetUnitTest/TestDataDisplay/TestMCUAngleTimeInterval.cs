﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestAngleMCUTimeInterval
    {
        public static ProcessMCUTimeIntervalTableLoad processMCUTimeIntervalTableLoad = new ProcessMCUTimeIntervalTableLoad();
        public static ProcessMCUBLL processMCUBLL = new ProcessMCUBLL();
        public static string mssg = "";
        public MDBDATA.Model.bkgtimeinterval model = new MDBDATA.Model.bkgtimeinterval
        {
            xmno = 29,
            pointname = "韶关745-J1-1",
             hour = 6,
              minute = 0
        };
        public void main()
        {
            //TestTimeIntervalAdd();
            TestTimeIntervalTableLoad();
        }
        public void TestTimeIntervalTableLoad()
        {
            processMCUTimeIntervalTableLoad.MCUTimeIntervalTableLoad(29, " 1=1 ", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTimeIntervalAdd()
        {
            processMCUBLL.ProcessMCUTimeIntevalAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
