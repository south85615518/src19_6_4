﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Settlement;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestSettlementPointAlarmValue
    {
        public ProcessSettlementPointAlarmModel processSettlementPointAlarmModel = new ProcessSettlementPointAlarmModel();
        public ProcessSettlementPointAlarmRecordsCount processSettlementPointAlarmRecordsCount = new ProcessSettlementPointAlarmRecordsCount();
        public ProcessSettlementPointAlarmValueAdd processSettlementPointAlarmValueAdd = new ProcessSettlementPointAlarmValueAdd();
        public ProcessSettlementPointAlarmValueDel processSettlementPointAlarmValueDel = new ProcessSettlementPointAlarmValueDel();
        public ProcessSettlementPointAlarmValueEdit processSettlementPointAlarmValueEdit = new ProcessSettlementPointAlarmValueEdit();
        public ProcessSettlementPointAlarmValueLoad processSettlementPointAlarmValueLoad = new ProcessSettlementPointAlarmValueLoad();
        public ProcessSettlementPointAlarmValueMutilEdit processSettlementPointAlarmValueMutilEdit = new ProcessSettlementPointAlarmValueMutilEdit();
       
        public Settlement.Model.settlementpointalarmvalue model = new Settlement.Model.settlementpointalarmvalue { point_name="s1", firstAlarmName = "一级预警",  secondAlarmName="二级预警",  thirdAlarmName="三级预警", remark="应力1号点", xmno = 29 };
        public string mssg = "";
        public void main()
        {
            TestSettlementPointAlarmAdd();
            //TestSettlementPointAlarmEdit();
            //TestSettlementPointAlarmDel();
            TestSettlementPointAlarmLoad();
            TestSettlementPointAlarmModel();
            TestSettlementPointAlarmValueMutilEdit();
        }
        public void TestSettlementPointAlarmAdd()
        {
            processSettlementPointAlarmValueAdd.PointAlarmValueAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmEdit()
        {
            model.thirdAlarmName = "一级预警";
            processSettlementPointAlarmValueEdit.PointAlarmValueEdit(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmDel()
        {
            processSettlementPointAlarmValueDel.PointAlarmValueDel(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmLoad()
        {
            processSettlementPointAlarmValueLoad.PointAlarmValueLoad(29," 1=1 ","point_name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
      
        public void TestSettlementPointAlarmModel()
        {
            processSettlementPointAlarmModel.SettlementPointAlarmModel(29,"s1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementPointAlarmValueMutilEdit()
        {
            processSettlementPointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model,"s1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        


    }
}
