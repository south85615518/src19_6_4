﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using System.Data;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using Tool;

namespace NFnetUnitTest.TestDataDisplay
{
   public class TestInclinometer
    {
       public ProcessInclinometerPointLoad inclinometerBLL = new ProcessInclinometerPointLoad();
       public ProcessInclinometerDbFill inclinometerDbFillBLL = new ProcessInclinometerDbFill();
       public ProcessSerializestrSBWY serializestrSBWY = new ProcessSerializestrSBWY();
       public ProcessInclinometerBLL inclinometerDataBLL = new ProcessInclinometerBLL();
       public ProcessSenorDataLoad senorDataLoad = new ProcessSenorDataLoad();
       public ProcessSenorPointNameCycListLoad senorPointNameCycListLoad = new ProcessSenorPointNameCycListLoad();

       public ProcessInclinometerDataRqcxConditionCreate inclinometerDataRqcxConditionCreate = new ProcessInclinometerDataRqcxConditionCreate();
       public ProcessSenorDataReordCount senorDataRecordCount = new ProcessSenorDataReordCount();
       public static string mssg = "";
       public void main()
       {
           //TestInclinometerPointLoad();
           //TestInclinometerTableLoad();
           //TestProcessInclinometerDataAlarmModelList();
           //TestSenorDateTime();
           //TestSenorDataExport();
           TestSenorDataLoad();
           //TestSenorPointCycListLoad();
           //TestSenorDataCount();
       }
       public void TestInclinometerPointLoad()
       {
           List<string> ls = inclinometerBLL.InclinometerPointLoad( Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), out mssg);
           ProcessPrintMssg.Print(mssg);
       }
       public void TestInclinometerTableLoad()
       {
           var resultDataRqcxConditionCreateCondition = inclinometerDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2017-11-23 14:13:08", "2017-11-25 14:13:08", QueryType.RQCX, "", "数据测试", Convert.ToDateTime("2017-11-25 14:13:08"));
           FillInclinometerDbFillCondition dbFillCondition = inclinometerDbFillBLL.InclinometerDbFill("CX1-51", resultDataRqcxConditionCreateCondition.sql, Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), Role.administratrorModel, false);
           ProcessPrintMssg.Print(string.Format("获取记录数{0}", dbFillCondition.dt.Rows.Count));
           serializestrSBWY.SerializestrSBWY(dbFillCondition.sql, "数据测试", "CX1-51", getclinometerzu(), Role.administratrorModel, false, out mssg);
           ProcessPrintMssg.Print(mssg);
       }

       public void TestProcessInclinometerDataAlarmModelList()
       {
           var processInclinometerDataAlarmModelListModel = new ProcessInclinometerBLL.ProcessInclinometerDataAlarmModelListModel(Aspect.IndirectValue.GetXmnoFromXmname("数据测试"));
           inclinometerDataBLL.ProcessInclinometerDataAlarmModelList(processInclinometerDataAlarmModelListModel,out mssg);
           ProcessPrintMssg.Print(mssg);
       }
       public static ProcessSenorDateTime senorDateTime = new ProcessSenorDateTime();
       public void TestSenorDateTime()
       {
           InclimeterDAL.Model.senor_data model = senorDateTime.SenorDateTime(Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), "CX1-52", Convert.ToDateTime("2017-11-17 11:23:04"), Role.administratrorModel, false, out mssg);
           string a = "";
       }

       public void TestSenorDataExport()
       {
           var processInclinometerDataAlarmModelListModel = new ProcessInclinometerBLL.ProcessInclinometerDataAlarmModelListModel(29);
           inclinometerDataBLL.ProcessInclinometerDataAlarmModelList(processInclinometerDataAlarmModelListModel,out mssg);
           ObjectHelper.ClassSerialization(processInclinometerDataAlarmModelListModel.model,"D:\\"+DateTime.Now.ToFileTime()+".txt");
       }

       public void TestSenorDataLoad()
       {
           DataTable dt = senorDataLoad.SenorDataLoad(Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), "C005", Convert.ToDateTime("2015-2-9"), Convert.ToDateTime("2015-2-15"),1,20," point_name,deep desc ,time ",Role.administratrorModel,false,out mssg);
       }

       public void TestSenorPointCycListLoad()
       {
           List<string> ls = senorPointNameCycListLoad.SenorPointNameCycListLoad(Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), "C005",Role.administratrorModel,false,out mssg);
       }

       public void TestSenorDataCount()
       {
           int cont = senorDataRecordCount.SenorDataReordCount(Aspect.IndirectValue.GetXmnoFromXmname("数据测试"),"C005",Convert.ToDateTime("2015-2-9"), Convert.ToDateTime("2015-2-15"),Role.administratrorModel,false,out mssg);
       }
       /// <summary>
       /// 获取fmos组号
       /// </summary>
       /// <returns></returns>
       public zuxyz[] getclinometerzu()
       {

           string[] XzBlAry = { "this_disp", "ac_disp", "this_rap" };
           List<zuxyz> zusls = new List<zuxyz>();
           List<string> bc = new List<string>();
           List<string> lj = new List<string>();
           List<string> ap = new List<string>();

           for (int i = 0; i < XzBlAry.Length; i++)
           {
               if ((XzBlAry[i] == "this_disp"))
               {
                   bc.Add(XzBlAry[i]);
               }

               if ((XzBlAry[i] == "ac_disp"))
               {
                   lj.Add(XzBlAry[i]);
               }
               if ((XzBlAry[i] == "this_rap"))
               {
                   ap.Add(XzBlAry[i]);
               }
           }
           if (bc.Count != 0)
           {
               zuxyz zu = new zuxyz { Bls = bc.ToArray<string>(), Name = "本次变化量" };
               zusls.Add(zu);
           }
           if (lj.Count != 0)
           {
               zuxyz zu = new zuxyz { Bls = lj.ToArray<string>(), Name = "累计变化" };
               zusls.Add(zu);
           }
           if (ap.Count != 0)
           {
               zuxyz zu = new zuxyz { Bls = ap.ToArray<string>(), Name = "本次速率" };
               zusls.Add(zu);
           }
           return zusls.ToArray<zuxyz>();


       }
       
    }
}
