﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GT;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestgtResultData
    {
        
        
        #region 测量库
        public ProcessResultDataMaxTime processResultDataMaxTime = new ProcessResultDataMaxTime();
        public ProcessResultDataRqcxConditionCreate processResultDataRqcxConditionCreate = new ProcessResultDataRqcxConditionCreate();
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public ProcessgtsensordataChart processChart = new ProcessgtsensordataChart();
        public Processgtsensordata_pointChart processpointChart = new Processgtsensordata_pointChart();
        public Processtwoscalar_Chart processtwoscalarChart = new Processtwoscalar_Chart();
        public Process_scalar_ResultDataPlotLineAlarm process_scalar_ResultDataPlotLineAlarm = new Process_scalar_ResultDataPlotLineAlarm();
        public ProcessSingleScalarResultDataTableLoad processSingleScalarResultDataTableLoad = new ProcessSingleScalarResultDataTableLoad();
        public ProcessTwoScalarResultDataTableLoad processTwoScalarResultDataTableLoad = new ProcessTwoScalarResultDataTableLoad();
        public ProcessScalarDataTableCountLoad processScalarDataTableCountLoad = new ProcessScalarDataTableCountLoad();
        public ProcessgtPointNameDateTimeListLoad processgtPointNameDateTimeListLoad = new ProcessgtPointNameDateTimeListLoad();
        public ProcessTimeDataDelete processTimeDataDelete = new ProcessTimeDataDelete();
        public string mssg = "";
        public void main()
        {
            //TestResultDataMaxTime();
            //TestResultDataRqcxConditionCreate();
            //TestSingleScalarProcessfillDbFill();
            //Testsinglescalar_ResultDataPlotLineAlarm();
            //Testsinglescalar_ResultDataTableLoad();
            //TestTwoscalar_ResultDataTableLoad();
            //Testscalar_ResultDataTableCountLoad();
            //TestResultDataDateTimeListLoad();
            //TestTimeDataDelete();
            //TestSingleScalarProcessfillDbFill();
            TestTotalStationChartBLL();
        }
        public void TestResultDataMaxTime()
        {
            processResultDataMaxTime.ResultDataMaxTime("新白广城际铁路下穿京广高铁施工监测",data.Model.gtsensortype._displacement ,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestResultDataRqcxConditionCreate()
        {
            processResultDataRqcxConditionCreate.ScalarResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", "", new DateTime());
        }

        public void TestSingleScalarProcessfillDbFill()
        {
            var sqlmodel = processResultDataRqcxConditionCreate.ScalarResultDataRqcxConditionCreate("2018-12-7", "2018-12-14", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", "", new DateTime());
            var model = new NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition(@"Gjj205-1-1
,Gjj205-1-5
,Gjj205-1-7
,Gjj205-1-8
,Gjj205-1-9
,Gjj205-2-1
,Gjj205-2-3
,Gjj205-2-4
,Gjj205-2-5
,Gjj205-2-6
,Gjj205-2-7
,Gjj205-2-8
,GJJ206-1-1
,GJJ206-1-10
,GJJ206-1-2
,GJJ206-1-3
,GJJ206-1-4
,GJJ206-1-5
,GJJ206-1-6
,GJJ206-1-8
,Gjj206-2-10
,GJJ206-2-2
,Gjj206-2-4
,Gjj206-2-8
,Gjj206-3-1
,Gjj206-3-10
,Gjj206-3-2
,Gjj206-3-3
,Gjj206-3-4", sqlmodel.sql, "新白广城际铁路下穿京广高铁施工监测");
            processResultDataBLL.ProcessSingleScalarfillTotalStationDbFill(model);
            //var series = processChart.Serializestrgtsensordata(model.sql, "新白广城际铁路下穿京广高铁施工监测", "'WG-YB-K18-1'", out mssg);
            var point_series = processpointChart.Serializestrgtsensordata_point("select * from gtsensordata where time between '2018-12-7'   and  '2018-12-14' and  datatype like '%围护墙%' order by point_name,time asc ", "96", model.pointname, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTwoScalarProcessfillDbFill()
        {
            var sqlmodel = processResultDataRqcxConditionCreate.ScalarResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", "", new DateTime());
            var model = new NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition("S01", sqlmodel.sql, 29);
            processResultDataBLL.ProcessTwoScalarfillTotalStationDbFill(model);
            var series = processtwoscalarChart.Serializestrtwoscalar_Chart(model.sql, 29, "'S01'", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Testsinglescalar_ResultDataPlotLineAlarm()
        {
            process_scalar_ResultDataPlotLineAlarm.scalar_ResultDataPlotLineAlarm("GJ-1", "新白广城际铁路下穿京广高铁施工监测", 65, data.Model.gtsensortype._deflection);
        }

        public void Testsinglescalar_ResultDataTableLoad()
        {
            //processSingleScalarResultDataTableLoad.SingleScalarResultDataTableLoad("新白广城际铁路下穿京广高铁施工监测", "全部", 1, 20, "point_name", data.Model.gtsensortype._supportAxialForce, Convert.ToDateTime("2018-10-08 15:10:06"), Convert.ToDateTime("2018-10-10 17:57:02"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestTwoscalar_ResultDataTableLoad()
        {
            processTwoScalarResultDataTableLoad.TwoScalarResultDataTableLoad("新白广城际铁路下穿京广高铁施工监测", "B1(Y5-Y6/P6-P7)-1北", 1, 20, "point_name", data.Model.gtsensortype._deflection, Convert.ToDateTime("2018-06-17"), Convert.ToDateTime("2018-06-18"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Testscalar_ResultDataTableCountLoad()
        {
            //processScalarDataTableCountLoad.ScalarDataTableCountLoad("新白广城际铁路下穿京广高铁施工监测", "B1(Y5-Y6/P6-P7)-1北",data.Model.gtsensortype._deflection,Convert.ToDateTime("2018-06-17"), Convert.ToDateTime("2018-06-18"),out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public void TestResultDataDateTimeListLoad()
        {
            processgtPointNameDateTimeListLoad.GtPointNameDateTimeListLoad("新白广城际铁路下穿京广高铁施工监测","B1(Y5-Y6/P6-P7)-1北",data.Model.gtsensortype._strain,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTimeDataDelete()
        {
            //processTimeDataDelete.TimeDataDelete("新白广城际铁路下穿京广高铁施工监测", "34180020-17", Convert.ToDateTime("2018-09-21 17:33:36"), Convert.ToDateTime("2018-09-21 17:53:36"), data.Model.gtsensortype._stress, out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        #endregion
        #region 编辑库
        public ProcessSurveyPointTimeDataAdd processSurveyPointTimeDataAdd = new ProcessSurveyPointTimeDataAdd();
        public ProcessSurveyDataUpdate processSurveyDataUpdate = new ProcessSurveyDataUpdate();
        //public ProcessSurveyTimeDataDelete processSurveyTimeDataDelete = new ProcessSurveyTimeDataDelete();
        public ProcessSurveyTimeList processSurveyTimeList = new ProcessSurveyTimeList();
        public ProcessSurveyDataAdd processSurveyDataAdd = new ProcessSurveyDataAdd();
        public void SurveyMain()
        {
            //TestSurveyDataAdd();
            //TestSurveyPointTimeDataAdd();
            //TestSurveyDataUpdate();
            //TestSurveyTimeDataDelete();
            //TestSurveyTimeList();
            TestTotalStationChartBLL();
        }
        public void TestSurveyDataAdd()
        {
            processSurveyDataAdd.SurveyDataAdd("103", 106,data.Model.gtsensortype._fractureMeter,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        //public void TestSurveyPointTimeDataAdd()
        //{
        //    processSurveyPointTimeDataAdd.SurveyPointTimeDataAdd("新白广城际铁路下穿京广高铁施工监测", "34180020-17", data.Model.gtsensortype._stress, Convert.ToDateTime("2018-09-21 17:30:36"), Convert.ToDateTime("2018-09-22 10:35:32"), out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        //public void TestSurveyDataUpdate()
        //{
        //    processSurveyDataUpdate.SurveyDataUpdate("新白广城际铁路下穿京广高铁施工监测", "34180020-17", Convert.ToDateTime("2018-09-22 10:23:32"), data.Model.gtsensortype._stress, "first_this_scalarvalue", "sec_this_scalarvalue", Convert.ToDateTime("2018-09-21 17:33:36"),out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        //public void TestSurveyTimeDataDelete()
        //{
        //    processSurveyTimeDataDelete.SurveyTimeDataDelete("新白广城际铁路下穿京广高铁施工监测", "34180020-17", Convert.ToDateTime("2018-09-21 17:33:36"), Convert.ToDateTime("2018-09-22 16:33:36"), data.Model.gtsensortype._stress,out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        //public void TestSurveyTimeList()
        //{
        //    processSurveyTimeList.SurveyTimeList("新白广城际铁路下穿京广高铁施工监测", "34180020-17", data.Model.gtsensortype._stress,out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        public void TestTotalStationChartBLL()
        {
            string sql = "select  point_name,cyc,n,e,z,round(this_dn,2) as this_dn,round(this_de,2) this_de,round(this_dz,2) as this_dz, round(ac_dn,2) as ac_dn,round(ac_de,2) as ac_de,round(ac_dz,2) as ac_dz,time,siblingpoint_name  from  fmos_cycdirnet where  taskName = '127' and   POINT_NAME in('1-1','1-2','1-3','1-4','1-5','1-6','1-7','2-1','2-2','2-3','2-4','2-5','2-6','2-7','3-1','3-2','3-3','3-4','3-5','3-6','3-7','4-1','4-2','4-3','4-4','4-5','4-6','4-7','DG-1','DG-2','DG-3','DG-4','JC-1','JC-2')   and cyc >= 1   and  cyc <= 11 order by POINT_NAME,cyc,Time asc ";
            var SerializestrBMWYC_CYCondition = new NFnet_BLL.DisplayDataProcess.SerializestrBMWY_POINTCondition(sql, 127, "\'1-1\',\'1-2\',\'1-3\',\'1-4\',\'1-5\',\'1-6\',\'1-7\',\'2-1\',\'2-2\',\'2-3\',\'2-4\',\'2-5\',\'2-6\',\'2-7\',\'3-1\',\'3-2\',\'3-3\',\'3-4\',\'3-5\',\'3-6\',\'3-7\',\'4-1\',\'4-2\',\'4-3\',\'4-4\',\'4-5\',\'4-6\',\'4-7\',\'DG-1\',\'DG-2\',\'DG-3\',\'DG-4\',\'JC-1\',\'JC-2\'", new global::NFnet_BLL.DataProcess.ProcessResultDataBLL().getfmoszu());
            NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessTotalStationChartBLL.ProcessSerializestrBMWY_point(SerializestrBMWYC_CYCondition,out mssg);
            ProcessPrintMssg.Print(mssg);
        }


        #endregion
        #region 成果库
        public ProcessCgResultDataImport processCgResultDataImport = new ProcessCgResultDataImport();
        //public ProcessCgTimeDataDelete processCgTimeDataDelete = new ProcessCgTimeDataDelete();
        public ProcessCgCYCList processCgTimeList = new ProcessCgCYCList();

        public void CgMain()
        {
            //TestSurveyDataAdd();
            TestCgResultDataImport();
            //TestCgTimeDataDelete();
            //CgTimeList();
        }
        public void TestCgResultDataImport()
        {
            processCgResultDataImport.CgResultDataImport("103", data.Model.gtsensortype._fractureMeter,1,106,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        //public void TestCgTimeDataDelete()
        //{
        //    processCgTimeDataDelete.CgTimeDataDelete("新白广城际铁路下穿京广高铁施工监测", "34180020-17", Convert.ToDateTime("2018-09-21 17:33:36"), Convert.ToDateTime("2018-09-22 16:33:36"), data.Model.gtsensortype._stress,out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        //public void CgTimeList()
        //{
        //    processCgTimeList.CgTimeList("新白广城际铁路下穿京广高铁施工监测", "34180020-17", data.Model.gtsensortype._stress,out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        #endregion

    }
}
