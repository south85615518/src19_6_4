﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGNSSTimeInterval
    {
        public static ProcessGNSSTimeIntervalTableLoad processGNSSTimeIntervalTableLoad = new ProcessGNSSTimeIntervalTableLoad();
        public static ProcessGNSSBLL processGNSSBLL = new ProcessGNSSBLL();
        public static string mssg = "";
        public GPS.Model.gnsstimeinterval model = new GPS.Model.gnsstimeinterval
        {
            xmno = 29,
            pointname = "sg01",
             hour = 4,
              minute = 0
        };
        public void main()
        {
            //TestTimeIntervalAdd();
            TestTimeIntervalTableLoad();
        }
        public void TestTimeIntervalTableLoad()
        {
            processGNSSTimeIntervalTableLoad.GNSSTimeIntervalTableLoad(29, " 1=1 ", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTimeIntervalAdd()
        {
            processGNSSBLL.ProcessGNSSTimeIntevalAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
