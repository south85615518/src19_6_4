﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Strain;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestStrainPointAlarmValue
    {
        public ProcessStrainPointAlarmModel processStrainPointAlarmModel = new ProcessStrainPointAlarmModel();
        public ProcessStrainPointAlarmRecordsCount processStrainPointAlarmRecordsCount = new ProcessStrainPointAlarmRecordsCount();
        public ProcessStrainPointAlarmValueAdd processStrainPointAlarmValueAdd = new ProcessStrainPointAlarmValueAdd();
        public ProcessStrainPointAlarmValueDel processStrainPointAlarmValueDel = new ProcessStrainPointAlarmValueDel();
        public ProcessStrainPointAlarmValueEdit processStrainPointAlarmValueEdit = new ProcessStrainPointAlarmValueEdit();
        public ProcessStrainPointAlarmValueLoad processStrainPointAlarmValueLoad = new ProcessStrainPointAlarmValueLoad();
        public ProcessStrainPointAlarmValueMutilEdit processStrainPointAlarmValueMutilEdit = new ProcessStrainPointAlarmValueMutilEdit();
        public Strain.Model.strainpointalarmvalue model = new Strain.Model.strainpointalarmvalue { point_name="s1", firstAlarmName = "一级预警", secondAlarmName="二级预警", thirdAlarmName="三级预警", remark="应力1号点", xmno = 29 };
        public string mssg = "";
        public void main()
        {
            TestStrainPointAlarmAdd();
            //TestStrainPointAlarmEdit();
            //TestStrainPointAlarmDel();
            TestStrainPointAlarmLoad();
            TestStrainPointAlarmModel();
            TestStrainPointAlarmValueMutilEdit();
        }
        public void TestStrainPointAlarmAdd()
        {
            processStrainPointAlarmValueAdd.PointAlarmValueAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainPointAlarmEdit()
        {
            model.thirdAlarmName = "一级预警";
            processStrainPointAlarmValueEdit.PointAlarmValueEdit(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainPointAlarmDel()
        {
            processStrainPointAlarmValueDel.PointAlarmValueDel(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainPointAlarmLoad()
        {
            processStrainPointAlarmValueLoad.PointAlarmValueLoad(29," 1=1 ","point_name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainPointAlarmModel()
        {
            processStrainPointAlarmModel.StrainPointAlarmModel(29,"s1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainPointAlarmValueMutilEdit()
        {
            processStrainPointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model,"s1",out mssg);
        }
    }
}
