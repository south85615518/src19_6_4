﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GAUGE;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGaugeAlarm
    {
        public ProcessGaugeAlarmAdd processGaugeAlarmAdd = new ProcessGaugeAlarmAdd();
        public ProcessGaugeAlarmEdit processGaugeAlarmEdit = new ProcessGaugeAlarmEdit();
        public ProcessGaugeAlarmDel processGaugeAlarmDel = new ProcessGaugeAlarmDel();
        public ProcessGaugeAlarmLoad processGaugeAlarmLoad = new ProcessGaugeAlarmLoad();
        public ProcessGaugeAlarmRecordsCount processGaugeAlarmRecordsCount = new ProcessGaugeAlarmRecordsCount();

        public ProcessGaugeAlarmValues processGaugeAlarmValues = new ProcessGaugeAlarmValues();
        //public ProcessGNSSAlarmValueModel processGaugeAlarm = new ProcessGNSSAlarmValueModel();
        public static string mssg = "";
        public Gauge.Model.reservoirwaterlevel model = new Gauge.Model.reservoirwaterlevel
        {
            name = "一级预警",
            deep = 125,
            deepL = 120,
            xmno = 29
        };
        public void main()
        {
            //Add();
            //Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //AlarmNameStr();
            //AlarmValueModel();
        }
        public void Add()
        {
            processGaugeAlarmAdd.AlarmAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processGaugeAlarmEdit.AlarmEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processGaugeAlarmDel.AlarmDel(29, "一级预警", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processGaugeAlarmLoad.AlarmLoad(29, "name", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processGaugeAlarmRecordsCount.AlarmRecordsCount(29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void AlarmNameStr()
        {
            processGaugeAlarmValues.AlarmValues(29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        //public void AlarmValueModel()
        //{
        //    processGaugeAlarm.GNSSAlarmValueModel("一级预警",29,out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
    }
}
