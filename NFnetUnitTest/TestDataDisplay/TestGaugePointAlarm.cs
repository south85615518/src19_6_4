﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GAUGE;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGaugePointAlarmValue
    {
        public ProcessGaugePointAlarmValueAdd processGaugePointAlarmValueAdd = new ProcessGaugePointAlarmValueAdd();
        public ProcessGaugePointAlarmValueEdit processGaugePointAlarmValueEdit = new ProcessGaugePointAlarmValueEdit();

        public ProcessGaugePointAlarmValueDel processGaugePointAlarmValueDel = new ProcessGaugePointAlarmValueDel();
        public ProcessGaugePointAlarmValueLoad processGaugePointAlarmValueLoad = new ProcessGaugePointAlarmValueLoad();
        public ProcessGaugePointAlarmRecordsCount processGaugePointAlarmRecordsCount = new ProcessGaugePointAlarmRecordsCount();
        public ProcessGaugePointAlarmValueMutilEdit processGaugePointAlarmValueMutilEdit = new ProcessGaugePointAlarmValueMutilEdit();
        public ProcessGaugePointAlarmModel processGaugePointAlarmModel = new ProcessGaugePointAlarmModel();
        public static string mssg = "";
        public Gauge.Model.reservoirwaterlevelalarmvalue model = new Gauge.Model.reservoirwaterlevelalarmvalue
        {
            point_name = "韶关745-J1-2",
            firstalarm = "一级预警",
            secalarm = "1级预警",
            thirdalarm = "一级预警",
            pointtype = "库水位",
            remark = "",
             xmno = 29

        };
        public void main()
        {
            //Add();
            //Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //MultiEdit();
            GaugePointAlarmModel();
        }
        public void Add()
        {
            processGaugePointAlarmValueAdd.PointAlarmValueAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processGaugePointAlarmValueEdit.PointAlarmValueEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processGaugePointAlarmValueDel.PointAlarmValueDel(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processGaugePointAlarmValueLoad.PointAlarmValueLoad("数据测试",29," 1=1 ", "point_name", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processGaugePointAlarmRecordsCount.PointAlarmRecordsCount("",29," 1=1 ",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void MultiEdit()
        {
            processGaugePointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model, "韶关745-J1-1','韶关745-J1-2", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void GaugePointAlarmModel()
        {
            processGaugePointAlarmModel.GaugePointAlarmModel(29,"韶关库水位",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
