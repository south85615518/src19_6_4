﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestMCUAnglePointAlarmValue
    {
        public ProcessMCUAnglePointAlarmValueAdd processMCUAnglePointAlarmValueAdd = new ProcessMCUAnglePointAlarmValueAdd();
        public ProcessMCUAnglePointAlarmValueEdit processMCUAnglePointAlarmValueEdit = new ProcessMCUAnglePointAlarmValueEdit();

        public ProcessMCUAnglePointAlarmValueDel processMCUAnglePointAlarmValueDel = new ProcessMCUAnglePointAlarmValueDel();
        public ProcessMCUAnglePointAlarmValueLoad processMCUAnglePointAlarmValueLoad = new ProcessMCUAnglePointAlarmValueLoad();
        public ProcessMCUAnglePointAlarmRecordsCount processMCUAnglePointAlarmRecordsCount = new ProcessMCUAnglePointAlarmRecordsCount();
        public ProcessMCUAnglePointAlarmValueMutilEdit processMCUAnglePointAlarmValueMutilEdit = new ProcessMCUAnglePointAlarmValueMutilEdit();
        public ProcessMCUAnglePointAlarmModel processMCUAnglePointAlarmModel = new ProcessMCUAnglePointAlarmModel();
        public static string mssg = "";
        public MDBDATA.Model.mcuanglepointalarm model = new MDBDATA.Model.mcuanglepointalarm
        {
            point_name = "韶关745-J1-1",
            firstalarm = "一级预警",
            secalarm = "一级预警",
            thirdalarm = "二级预警",
            pointtype = "固定测斜",
            remark = "",
             xmno = 29,
             id=1

        };
        public void main()
        {
            //Add();
            //Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //MultiEdit();
            TestMCUAnglePointAlarmModelGet();
        }
        public void Add()
        {
            processMCUAnglePointAlarmValueAdd.PointAlarmValueAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processMCUAnglePointAlarmValueEdit.PointAlarmValueEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processMCUAnglePointAlarmValueDel.PointAlarmValueDel(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processMCUAnglePointAlarmValueLoad.PointAlarmValueLoad("数据测试",29," 1=1 ", "point_name", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processMCUAnglePointAlarmRecordsCount.PointAlarmRecordsCount("",29," 1=1 ",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void MultiEdit()
        {
            processMCUAnglePointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model, "韶关745-J1-1','韶关745-J1-2", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMCUAnglePointAlarmModelGet()
        {
            processMCUAnglePointAlarmModel.MCUAnglePointAlarmModel(29, "韶关745-N2-1", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
