﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Settlement;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestSettlementAlarmValue
    {
        public ProcessSettlementAlarmAdd processSettlementAlarmAdd = new ProcessSettlementAlarmAdd();
        public ProcessSettlementAlarmDel processSettlementAlarmDel = new ProcessSettlementAlarmDel();
        public ProcessSettlementAlarmEdit processSettlementAlarmEdit = new ProcessSettlementAlarmEdit();
        public ProcessSettlementAlarmLoad processSettlementAlarmLoad = new ProcessSettlementAlarmLoad();
        public ProcessSettlementAlarmValueModel ProcessSettlementAlarmValueModel = new ProcessSettlementAlarmValueModel();
        public ProcessSettlementAlarmValues processSettlementAlarmValues = new ProcessSettlementAlarmValues();
        public Settlement.Model.settlementalarmvalue model = new Settlement.Model.settlementalarmvalue { name="一级预警", elevation=10, xmno =29 };
        public string mssg = "";
        public void main()
        {
            TestSettlementAlarmValueAdd();
            TestSettlementAlarmValueUpdate();
            TestSettlementAlarmValueDel();
            TestSettlementAlarmValueTableLoad();
            TestSettlementAlarmValueModelLoad();
            TestSettlementAlarmValueNames();
        }
        public void TestSettlementAlarmValueAdd()
        {
            processSettlementAlarmAdd.AlarmAdd(model,out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueUpdate()
        {
            model.elevation = 15;
            processSettlementAlarmEdit.AlarmEdit(model, out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueDel()
        {
            processSettlementAlarmDel.AlarmDel(29,"一级预警", out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueTableLoad()
        {
            processSettlementAlarmLoad.AlarmLoad(29,"name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueModelLoad()
        {
            ProcessSettlementAlarmValueModel.SettlementAlarmValueModel("一级预警",29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueNames()
        {
            processSettlementAlarmValues.AlarmValues(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
