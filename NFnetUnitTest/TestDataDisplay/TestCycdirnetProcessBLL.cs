﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess.Strain.SurfaceDisplacement.totalstation;

namespace NFnetUnitTest.TestDataDisplay
{
   public class TestCycdirnetProcessBLL
    {
       ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
       public ProcessXmStateTableLoad processXmStateTableLoad = new ProcessXmStateTableLoad();
       public ProcessTotalStationDataUploadIntervalTableLoad processTotalStationDataUploadIntervalTableLoad = new ProcessTotalStationDataUploadIntervalTableLoad();
       public  ProcessTotalStationDataUploadInterval processTotalStationDataUploadInterval = new ProcessTotalStationDataUploadInterval();
       public ProcessSurveyDataLackPointsModellistFromCgDataModelGet processSurveyDataLackPointsModellistFromCgDataModelGet = new ProcessSurveyDataLackPointsModellistFromCgDataModelGet();
       public ProcessSurveyDataGetTimeFromCyc processSurveyDataGetTimeFromCyc = new ProcessSurveyDataGetTimeFromCyc();
       string mssg = "";
       string xmname = "华南水电四川成都大坝监测A";
        public void main() {
            //ProcessResultMaxDate();
            //TestXmStateTableLoad();
            //TestTotalStationDataUploadIntervalAdd();
            //TestTotalStationDataUploadIntervalTableLoad();
            //SurveyDataLackPointsModellistFromCgDataModelGet();
            SurveyDataGetTimeFromCyc();
        }
        public void ProcessResultMaxDate()
        {
            var processXmLastDateModel = new ProcessResultDataBLL.ProcessXmLastDateModel(xmname);
            string dateTime = "";
            if (resultDataBLL.ProcessXmLastDate(processXmLastDateModel, out mssg))
            {
                dateTime = processXmLastDateModel.dateTime;
                ProcessPrintMssg.Print(mssg);
            }
        }
        public void TestXmStateTableLoad()
        {
            processXmStateTableLoad.XmStateTableLoad(1,20,29,"韶关铀业","南方测绘","xmno","asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTotalStationDataUploadIntervalTableLoad()
        {
            processTotalStationDataUploadIntervalTableLoad.TotalStationDataUploadIntervalTableLoad("韶关铀业",29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTotalStationDataUploadIntervalAdd()
        {
            TotalStation.Model.fmos_obj.totalstationdatauploadinterval model = new TotalStation.Model.fmos_obj.totalstationdatauploadinterval { xmname="韶关铀业", xmno=29, hour=1, minute = 40 };
            processTotalStationDataUploadInterval.Add(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void SurveyDataLackPointsModellistFromCgDataModelGet()
        {
            var modellist = processSurveyDataLackPointsModellistFromCgDataModelGet.SurveyDataLackPointsModellistFromCgDataModelGet(96, "QD204-1,QD204-2,QD204-3,QD204-4".Split(',').ToList(),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void SurveyDataGetTimeFromCyc()
        {
            var modellist = processSurveyDataGetTimeFromCyc.SurveyDataGetTimeFromCyc(96, 774, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
