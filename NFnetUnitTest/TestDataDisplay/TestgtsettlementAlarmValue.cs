﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GTSettlement;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGTSettlementAlarmValue
    {
        public ProcessSettlementAlarmAdd processSettlementAlarmAdd = new ProcessSettlementAlarmAdd();
        public ProcessSettlementAlarmDel processSettlementAlarmDel = new ProcessSettlementAlarmDel();
        public ProcessSettlementAlarmEdit processSettlementAlarmEdit = new ProcessSettlementAlarmEdit();
        public ProcessSettlementAlarmLoad processSettlementAlarmLoad = new ProcessSettlementAlarmLoad();
        public ProcessSettlementAlarmValueModel ProcessSettlementAlarmValueModel = new ProcessSettlementAlarmValueModel();
        public ProcessSettlementAlarmValues processSettlementAlarmValues = new ProcessSettlementAlarmValues();
        public data.Model.gtsettlementalarmvalue model = new data.Model.gtsettlementalarmvalue {  alarmname="一级预警", this_val=1, ac_val =1,  rap =1, settlementdiff = 0 , xmno =106 };
        public string mssg = "";
        public void main()
        {
            TestSettlementAlarmValueAdd();
            TestSettlementAlarmValueUpdate();
            //TestSettlementAlarmValueDel();
            TestSettlementAlarmValueTableLoad();
            TestSettlementAlarmValueModelLoad();
            TestSettlementAlarmValueNames();
        }
        public void TestSettlementAlarmValueAdd()
        {
            processSettlementAlarmAdd.AlarmAdd(model,out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueUpdate()
        {
            model.settlementdiff = 15;
            processSettlementAlarmEdit.AlarmEdit(model, out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueDel()
        {
            processSettlementAlarmDel.AlarmDel(106,"一级预警", out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueTableLoad()
        {
            processSettlementAlarmLoad.AlarmLoad(106,"alarmname",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueModelLoad()
        {
            ProcessSettlementAlarmValueModel.SettlementAlarmValueModel("一级预警",106,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementAlarmValueNames()
        {
            processSettlementAlarmValues.AlarmValues(106,out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
