﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GT;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGTAlarmValue
    {
        public ProcessGTAlarmAdd processGTAlarmAdd = new ProcessGTAlarmAdd();
        public ProcessGTAlarmDel processGTAlarmDel = new ProcessGTAlarmDel();
        public ProcessGTAlarmEdit processGTAlarmEdit = new ProcessGTAlarmEdit();
        public Process_singlescalar_AlarmLoad process_singlescalar_AlarmLoad = new Process_singlescalar_AlarmLoad();
        public Process_twoscalar_AlarmLoad process_twoscalar_AlarmLoad = new Process_twoscalar_AlarmLoad();
        public Process_singlescalar_AlarmValueModel processGTAlarmValueModel = new Process_singlescalar_AlarmValueModel();
        public Process_twoscalar_AlarmValueModel processtwoscalarGTAlarmValueModel = new Process_twoscalar_AlarmValueModel();
        public ProcessGTAlarmValues processGTAlarmValues = new ProcessGTAlarmValues();
        public data.Model.gtalarmvalue model_ = new data.Model.gtalarmvalue {  alarmname="一级预警",  datatype=data.Model.gtsensortype._displacement, xmno =106, valuetype=1, single_this_scalarvalue_u =10, single_this_scalarvalue_l = 0, single_ac_scalarvalue_u = 20, single_ac_scalarvalue_l=10 };
        public data.Model.gtalarmvalue model = new data.Model.gtalarmvalue { alarmname = "一级预警", datatype = data.Model.gtsensortype._displacement, xmno = 106, valuetype = 1, first_this_scalarvalue_u = 10, first_this_scalarvalue_l = 0, first_ac_scalarvalue_u = 20, first_ac_scalarvalue_l = 10, sec_this_scalarvalue_u = 10, sec_this_scalarvalue_l = 0, sec_ac_scalarvalue_u = 20, sec_ac_scalarvalue_l = 10 };
        public string mssg = "";
        public void main()
        {
            TestGTAlarmValueAdd();
            TestGTAlarmValueUpdate();
            //TestGTAlarmValueDel();
            //TestGTAlarmValueTableLoad();
            //TestSingleScalarAlarmValueTableLoad();
            //TestTwoScalarAlarmValueTableLoad();
            //TestGTAlarmValueModelLoad();
            //TestTwoScalarGTAlarmValueModelLoad();
            TestGTAlarmValueNames();
        }
        public void TestGTAlarmValueAdd()
        {
            processGTAlarmAdd.AlarmAdd(model,out  mssg);
            model.alarmname = "二级预警";
            processGTAlarmAdd.AlarmAdd(model, out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGTAlarmValueUpdate()
        {
            model.first_ac_scalarvalue_l = 15;
            processGTAlarmEdit.AlarmEdit(model, out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGTAlarmValueDel()
        {
            processGTAlarmDel.AlarmDel(106,"一级预警", out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSingleScalarAlarmValueTableLoad()
        {
            process_singlescalar_AlarmLoad.singlescalar_AlarmLoad(106, "alarmname", 1, 20, "asc", data.Model.gtsensortype._displacement, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTwoScalarAlarmValueTableLoad()
        {
            process_twoscalar_AlarmLoad.twoscalar_AlarmLoad(106, "alarmname", 1, 20, "asc", data.Model.gtsensortype._displacement, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGTAlarmValueModelLoad()
        {
            processGTAlarmValueModel.GTAlarmValueModel("二级预警", 106, data.Model.gtsensortype._displacement, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestTwoScalarGTAlarmValueModelLoad()
        {
            processtwoscalarGTAlarmValueModel.GTAlarmValueModel("二级预警", 106, data.Model.gtsensortype._displacement, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGTAlarmValueNames()
        {
            processGTAlarmValues.AlarmValues(106, data.Model.gtsensortype._displacement, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
