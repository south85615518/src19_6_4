﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Settlement;
using NFnet_BLL.DataProcess;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.Settlement;
using NFnet_Interface.DisplayDataProcess.GTSettlement;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestSettlementResultData
    {
       /* #region 测量库
        public NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.Settlement.ProcessSettlementResultDataMaxTime processSettlementResultDataMaxTime = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.Settlement.ProcessSettlementResultDataMaxTime();
        public NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.Settlement.ProcessSettlementResultDataRqcxConditionCreate processSettlementResultDataRqcxConditionCreate = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.Settlement.ProcessSettlementResultDataRqcxConditionCreate();
        public ProcessSettlementResultDataBLL processResultDataBLL = new ProcessSettlementResultDataBLL();
        public NFnet_Interface.DisplayDataProcess.Settlement.ProcessSettlementChart processSettlementChart = new NFnet_Interface.DisplayDataProcess.Settlement.ProcessSettlementChart();
        public ProcessSettlementPointLoad processSettlementPointLoad = new ProcessSettlementPointLoad();
        public string mssg = "";
        public void main()
        {
            //TestSettlementResultDataMaxTime();
            TestSettlementResultDataRqcxConditionCreate();
            TestProcessfillSettlementDbFill();
        }
        public void TestSettlementResultDataMaxTime()
        {
            processSettlementResultDataMaxTime.SettlementResultDataMaxTime(29,out mssg );
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementResultDataRqcxConditionCreate()
        {
            var model = processSettlementResultDataRqcxConditionCreate.SettlementResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", 29, new DateTime());
        }

        public void TestProcessfillSettlementDbFill()
        {
            var sqlmodel = processSettlementResultDataRqcxConditionCreate.SettlementResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", 29, new DateTime());
            var model = new NFnet_BLL.DisplayDataProcess.FillSensorDbFillCondition("S01",sqlmodel.sql,29);
            processResultDataBLL.ProcessfillTotalStationDbFill(model);
            var series = processSettlementChart.SerializestrSettlement(model.sql,29,"'S01'",out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestSettlementPointLoad()
        {
            processSettlementPointLoad.SettlementPointLoad(29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        #endregion
        #region 编辑库
        public ProcessSettlementSurveyDataAdd processSettlementSurveyDataAdd = new ProcessSettlementSurveyDataAdd();
        public ProcessSettlementSurveyDataUpdate processSettlementSurveyDataUpdate = new ProcessSettlementSurveyDataUpdate();
        public ProcessSettlementSurveyPointTimeDataAdd processSettlementSurveyPointTimeDataAdd = new ProcessSettlementSurveyPointTimeDataAdd();
        public ProcessSettlementSurveyTimeDataDelete processSettlementSurveyTimeDataDelete = new ProcessSettlementSurveyTimeDataDelete();
        public ProcessSettlementSurveyTimeList processSettlementSurveyTimeList = new ProcessSettlementSurveyTimeList();
        public void surveymain()
        {
            
        }
        public int xmno = Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测");
        public void SettlementSurveyDataAdd()
        {
            processSettlementSurveyDataAdd.SurveyDataAdd(xmno, "s1", Convert.ToDateTime("2018-9-14"), Convert.ToDateTime("2018-9-18"),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void SettlementSurveyDataUpdate()
        {
            processSettlementSurveyDataUpdate.SurveyDataUpdate(xmno, "s1", Convert.ToDateTime("2018-9-14"), "this_val", "ac_val", Convert.ToDateTime("2018-9-16"),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void SettlementSurveyPointTimeDataAdd()
        {
            processSettlementSurveyPointTimeDataAdd.SurveyPointTimeDataAdd(xmno,"s1",Convert.ToDateTime("2018-9-14"), Convert.ToDateTime("2018-9-18"),out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void SettlementSurveyTimeDataDelete()
        {
            processSettlementSurveyTimeDataDelete.SurveyTimeDataDelete(xmno, "s1", Convert.ToDateTime("2018-9-14"), Convert.ToDateTime("2018-9-18"), out mssg);
        }
        public void SettlementSurveyTimeList()
        {
            processSettlementSurveyTimeList.SurveyTimeList(xmno,"s1",out mssg);
        }
        #endregion
        #region 成果库

        #endregion*/
    }
}
