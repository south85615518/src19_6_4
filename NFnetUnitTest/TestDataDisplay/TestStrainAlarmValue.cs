﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Strain;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestStrainAlarmValue
    {
        public ProcessStrainAlarmAdd processStrainAlarmAdd = new ProcessStrainAlarmAdd();
        public ProcessStrainAlarmDel processStrainAlarmDel = new ProcessStrainAlarmDel();
        public ProcessStrainAlarmEdit processStrainAlarmEdit = new ProcessStrainAlarmEdit();
        public ProcessStrainAlarmLoad processStrainAlarmLoad = new ProcessStrainAlarmLoad();
        public ProcessStrainAlarmValueModel ProcessStrainAlarmValueModel = new ProcessStrainAlarmValueModel();
        public ProcessStrainAlarmValues processStrainAlarmValues = new ProcessStrainAlarmValues();
        public Strain.Model.strainalarmvalue model = new Strain.Model.strainalarmvalue { name="一级预警", strain=10, xmno =29 };
        public string mssg = "";
        public void main()
        {
            TestStrainAlarmValueAdd();
            //TestStrainAlarmValueUpdate();
            //TestStrainAlarmValueDel();
            TestStrainAlarmValueTableLoad();
            TestStrainAlarmValueModelLoad();
            TestStrainAlarmValueNames();
        }
        public void TestStrainAlarmValueAdd()
        {
            processStrainAlarmAdd.AlarmAdd(model,out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainAlarmValueUpdate()
        {
            model.strain = 15;
            processStrainAlarmEdit.AlarmEdit(model, out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainAlarmValueDel()
        {
            processStrainAlarmDel.AlarmDel(29,"一级预警", out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainAlarmValueTableLoad()
        {
            processStrainAlarmLoad.AlarmLoad(29,"name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainAlarmValueModelLoad()
        {
            ProcessStrainAlarmValueModel.StrainAlarmValueModel("一级预警",29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestStrainAlarmValueNames()
        {
            processStrainAlarmValues.AlarmValues(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
