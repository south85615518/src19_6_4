﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using System.Data;
using SqlHelpers;
using System.Data.Odbc;
using NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestCycdirnetData
    {
        public ProcessResultSurveyDataImport processResultSurveyDataImport = new ProcessResultSurveyDataImport();
        //public ProcessSurveyDataUpdate processSurveyDataUpdate = new ProcessSurveyDataUpdate();
        public ProcessSurveyPointCycDataAdd processSurveyPointCycDataAdd = new ProcessSurveyPointCycDataAdd();
        public ProcessLackPoints processLackPoints = new ProcessLackPoints();
        public ProcessCgCycList processCgCycList = new ProcessCgCycList();
        public ProcessSurveyCYCDataDelete processSurveyCYCDataDelete = new ProcessSurveyCYCDataDelete();
        public ProcessCgResultDataImport processCgResultDataImport = new ProcessCgResultDataImport();
        public ProcessDeleteCgDataCycOnChain processDeleteCgDataCycOnChain = new ProcessDeleteCgDataCycOnChain();
        public ProcessTotalStationBLL processTotalStationBLL = new ProcessTotalStationBLL();
        public string mssg = "";
        public void main()
        {
            //TestResultSurveyDataImport("新白广城际铁路下穿京广高铁施工监测",1,out mssg );
            //TestSurveyDataUpdate("新白广城际铁路下穿京广高铁施工监测","QD204-1","This_dE","Ac_dE",1,7,out mssg);
            //TestSurveyPointCycDataAdd("新白广城际铁路下穿京广高铁施工监测","QD204-1",7,6,out mssg);
            //TestLackPoints("新白广城际铁路下穿京广高铁施工监测", Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"),3,out mssg);
            //TestCgResultDataImport("新白广城际铁路下穿京广高铁施工监测",6,2);
            //TestCgCycList("新白广城际铁路下穿京广高铁施工监测");
            //TestSurveyCYCDataDelete(5, 6, "新白广城际铁路下穿京广高铁施工监测","");
            //TestDeleteCgDataCycOnChain("新白广城际铁路下穿京广高铁施工监测",1,out mssg);
            //ProcessPrintMssg.Print(mssg);
            CYCSort();
        }
        public void TestResultSurveyDataImport(string xmname,int cyc,out string mssg)
        {
            processResultSurveyDataImport.ResultSurveyDataImport(xmname,cyc,out mssg);
        }
        public void TestSurveyDataUpdate(string xmname, string pointname, string vSet_name, string vLink_name, int srcdatacyc, out string mssg)
        {
            mssg = "";
           // processSurveyDataUpdate.SurveyDataUpdate( xmname,  pointname,  vSet_name,  vLink_name,   srcdatacyc,out  mssg);
        }
        public void TestSurveyPointCycDataAdd(string xmname, string pointname, int cyc, int importcyc,out string mssg)
        {
            processSurveyPointCycDataAdd.SurveyPointCycDataAdd( xmname,  pointname,  cyc,  importcyc,out  mssg);
        }
        public void TestLackPoints(string xmname, int xmno, int cyc, out string mssg)
        {
            processLackPoints.LackPoints( xmname,  xmno,  cyc,out  mssg);
        }
        public void TestCgCycList(string xmname)
        {
            processCgCycList.CgCycList(xmname,out mssg);
        }
        public void TestSurveyCYCDataDelete(int startCyc,int endCyc,string xmname,string pointname)
        {
            processSurveyCYCDataDelete.SurveyCYCDataDelete(startCyc,endCyc,xmname,"",out mssg);
        }
        public void TestCgResultDataImport(string xmname,int cyc,int importcyc)
        {
            processCgResultDataImport.CgResultDataImport(xmname,cyc,importcyc,out mssg);
        }
        public void TestDeleteCgDataCycOnChain(string xmname,int cyc,out string mssg)
        {
            processDeleteCgDataCycOnChain.DeleteCgDataCycOnChain(xmname,cyc,out mssg);
        }
        public void CYCSort()
        {
            //database db = new database();
            //string sql = "SELECT * FROM `fmos_cycdirnet` where taskname =123 group by DATE_FORMAT(time,'%y%m%d %h') order by time;";    
            //OdbcConnection conn = db.GetStanderConn(127);
            //DataTable dt = querysql.querystanderdb(sql,conn);
            //DataView dv = new DataView(dt);
            //int i = 1;
            //foreach(DataRowView drv in dv)
            //{
            //    sql = string.Format("update fmos_cycdirnet set cyc={0} where taskname=123 and  DATE_FORMAT(time,'%y%m%d %h') = DATE_FORMAT('{1}','%y%m%d %h') or  DATE_FORMAT(time,'%y%m%d %h') = DATE_FORMAT('{2}','%y%m%d %h')", i, drv["time"], Convert.ToDateTime(drv["time"]).AddHours(1));
            //    i++;
            //    updatedb udb = new updatedb();
            //    udb.UpdateStanderDB(sql, conn);
            //}
            //processTotalStationBLL.CYCSort("123");


        }

        
    }
}
