﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GNSS;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestGNSSAlarm
    {
        public ProcessGNSSAlarmAdd processGNSSAlarmAdd = new ProcessGNSSAlarmAdd();
        public ProcessGNSSAlarmEdit processGNSSAlarmEdit = new ProcessGNSSAlarmEdit();
        public ProcessGNSSAlarmDel processGNSSAlarmDel = new ProcessGNSSAlarmDel();
        public ProcessGNSSAlarmLoad processGNSSAlarmLoad = new ProcessGNSSAlarmLoad();
        public ProcessGNSSAlarmRecordsCount processGNSSAlarmRecordsCount = new ProcessGNSSAlarmRecordsCount();

        public ProcessGNSSAlarmValues processGNSSAlarmValues = new ProcessGNSSAlarmValues();
        public ProcessGNSSAlarmValueModel processGNSSAlarmValueModel = new ProcessGNSSAlarmValueModel();
        public static string mssg = "";
        public GPS.Model.gnssalarmvalue model = new GPS.Model.gnssalarmvalue
        {
            name = "二级预警",
            this_x = 120,
            this_y = 120,
            this_z = 120,
            ac_x = 100,
            ac_y = 100,
            ac_z = 100,
            xmno = 29
        };
        public void main()
        {
            //Add();
            //Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //AlarmNameStr();
            AlarmValueModel();
        }
        public void Add()
        {
            processGNSSAlarmAdd.AlarmAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processGNSSAlarmEdit.AlarmEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Delete()
        {
            processGNSSAlarmDel.AlarmDel(29, "一级预警", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processGNSSAlarmLoad.AlarmLoad(29,"name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processGNSSAlarmRecordsCount.AlarmRecordsCount(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void AlarmNameStr()
        {
            processGNSSAlarmValues.AlarmValues(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void AlarmValueModel()
        {
            processGNSSAlarmValueModel.GNSSAlarmValueModel("一级预警",29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
