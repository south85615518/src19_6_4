﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.BKG;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestMCUAlarm
    {
        public ProcessMCUAlarmAdd processMCUAlarmAdd = new ProcessMCUAlarmAdd();
        public ProcessMCUAlarmEdit processMCUAlarmEdit = new ProcessMCUAlarmEdit();
        public ProcessMCUAlarmDel processMCUAlarmDel = new ProcessMCUAlarmDel();
        public ProcessMCUAlarmLoad processMCUAlarmLoad = new ProcessMCUAlarmLoad();
        public ProcessMCUAlarmRecordsCount processMCUAlarmRecordsCount = new ProcessMCUAlarmRecordsCount();
        public ProcessMCUPointAlarmValueMutilEdit processMCUPointAlarmValueMutilEdit = new ProcessMCUPointAlarmValueMutilEdit();
        public ProcessMCUAlarmValues processMCUAlarmValues = new ProcessMCUAlarmValues();
        public ProcessMCUAlarmValueModel processMCUAlarmValueModel = new ProcessMCUAlarmValueModel();
        public static string mssg = "";
        public MDBDATA.Model.mcualarmvalue model = new MDBDATA.Model.mcualarmvalue
        {
            name = "一级预警",
            deep = 120,
            deepL = 108,
            temperlate = 40,
            temperlateL = 36,
            xmno = 29
        };
        public void main()
        {
            //Add();
            //Update();
            //Delete();
            //TableLoad();
            //TableContLoad();
            //AlarmNameStr();
            //TestAlarmValueModelGet();
        }
        public void Add()
        {
            processMCUAlarmAdd.AlarmAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Update()
        {
            processMCUAlarmEdit.AlarmEdit(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        
        public void Delete()
        {
            processMCUAlarmDel.AlarmDel(29, "一级预警", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableLoad()
        {
            processMCUAlarmLoad.AlarmLoad(29,"name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TableContLoad()
        {
            processMCUAlarmRecordsCount.AlarmRecordsCount(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void AlarmNameStr()
        {
            processMCUAlarmValues.AlarmValues(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestAlarmValueModelGet()
        {
            processMCUAlarmValueModel.MCUAlarmValueModel("一级预警",29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
