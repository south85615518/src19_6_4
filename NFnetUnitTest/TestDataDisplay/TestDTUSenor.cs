﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using System.Data;
using Tool;
using SqlHelpers;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestDTUSenor
    {
        public string mssg = "";
        public ProcessDTUSenorLoad processDTUSenorLoad = new ProcessDTUSenorLoad();
        public ProcessDTUSenorTableCount processDTUSenorTableCount = new ProcessDTUSenorTableCount();
        public void main()
        {
            //TestDTUSenorLoad();
            //TestDTUSenorTableCount();
            //TestDTUSensorDataImport();
            TestDTUModuleDataImport();
        }
        public void TestDTUSenorLoad()
        {
            DataTable dt = processDTUSenorLoad.DTUSenorLoad(1, 20, 29, "senorno", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDTUSenorTableCount()
        {
            int cont = processDTUSenorTableCount.DTUSenorTableCount(29, " 1=1 ", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDTUSensorDataImport()
        {
            GTXLSHelper.PublicCompatibleInitializeWorkbook("d:\\desktop\\20190318表面应变计发货清单190315-C.xlsx");
            DataTable dt = new DataTable();
            GTXLSHelper.ProcessSensorDataDataTableImport("d:\\desktop\\20190318表面应变计发货清单190315-C.xlsx", out dt, out mssg);

            DataView dv = new DataView(dt);
            foreach (DataRowView drv in dv)
            {
                string sql = string.Format(@"insert ignore into dtusenor (senorno,sensortype,remark,xmno,senorname,time,aboundtime)
values
('{0}','应变计',{1},{2},'工讯应变计','{3}',1)", drv["传感器编号"], Convert.ToDouble(drv["标定系数K"]) * 0.001, 106, DateTime.Now);
                updatedb udb = new updatedb();
                OdbcSQLHelper.Conn = new database().GetStanderConn(106);
                udb.UpdateStanderDB(sql, OdbcSQLHelper.Conn);

            }

        }

        //        public void TestDTUModuleDataImport()
        //        {
        //            GTXLSHelper.PublicCompatibleInitializeWorkbook("C:\\Users\\pc\\Documents\\WeChat Files\\wxid_d4heivirl1er22\\FileStorage\\File\\2019-04\\南沙档案馆应力表(2).xlsx");
        //            DataTable dt = new DataTable();
        //            GTXLSHelper.ProcessModuleDataTableImport("C:\\Users\\pc\\Documents\\WeChat Files\\wxid_d4heivirl1er22\\FileStorage\\File\\2019-04\\应变计参数录入模板.xlsx", out dt, out mssg);

        //            DataView dv = new DataView(dt);
        //            foreach (DataRowView drv in dv)
        //            {
        //                if (drv["点名"] == "") continue;
        //                string sql = string.Format(@"insert ignore into dtumodule (name,id,addressno,od,port,xmno,senorno,sensortype)
        //values
        //('icontrol-s','{0}','{1}','{2}','{3}','{4}','{5}',0)", drv["模块号"].ToString() + Convert.ToInt32(drv["通道号"].ToString().Replace("td", "").Replace("od", "")) + drv["传感器编号"].ToString(), drv["模块号"].ToString(), Convert.ToInt32(drv["通道号"].ToString().Replace("td", "").Replace("od", "")), 8366, 133, drv["传感器编号"].ToString());
        //                updatedb udb = new updatedb();
        //                OdbcSQLHelper.Conn = new database().GetStanderConn(133);
        //                udb.UpdateStanderDB(sql, OdbcSQLHelper.Conn);

        //                sql = string.Format(@"insert ignore into dtu (point_name,module,sensortype,xmno)
        //values
        //('{0}','{1}',0,133)", drv["点名"], drv["模块号"].ToString() + Convert.ToInt32(drv["通道号"].ToString().Replace("td", "").Replace("od", "")) + drv["传感器编号"].ToString(), 0, 133);

        //                OdbcSQLHelper.Conn = new database().GetStanderConn(133);
        //                udb.UpdateStanderDB(sql, OdbcSQLHelper.Conn);


        //            }

        //        }

        public void TestDTUModuleDataImport()
        {
           GTXLSHelper.PublicCompatibleInitializeWorkbook("C:\\Users\\pc\\Documents\\WeChat Files\\wxid_d4heivirl1er22\\FileStorage\\File\\2019-04\\南沙档案馆应力表(2).xlsx");
           DataTable dt = new DataTable();
           GTXLSHelper.ProcessModuleDataTableImport("C:\\Users\\pc\\Documents\\WeChat Files\\wxid_d4heivirl1er22\\FileStorage\\File\\2019-04\\南沙档案馆应力表(2).xlsx", out dt, out mssg);

           DataView dv = new DataView(dt);
           foreach (DataRowView drv in dv)
           {
               if (drv["点名"] == "") continue;
               try
               {
                   string sql = @"update dtu set holedepth = " + Convert.ToDouble(drv["应力值"]) * 100 + @" where point_name = '" + drv["点名"] + "A' or point_name = '" + drv["点名"] + "B'";

                   updatedb udb = new updatedb();
                   OdbcSQLHelper.Conn = new database().GetStanderConn(133);
                   udb.UpdateStanderDB(sql, OdbcSQLHelper.Conn);
               }
               catch (Exception ex)
               { 

               }
           }

        }
        /*
        * GTXLSHelper.PublicCompatibleInitializeWorkbook("C:\\Users\\pc\\Documents\\WeChat Files\\wxid_d4heivirl1er22\\FileStorage\\File\\2019-04\\南沙档案馆应力表(2).xlsx");
           DataTable dt = new DataTable();
           GTXLSHelper.ProcessModuleDataTableImport("C:\\Users\\pc\\Documents\\WeChat Files\\wxid_d4heivirl1er22\\FileStorage\\File\\2019-04\\南沙档案馆应力表(2).xlsx", out dt, out mssg);

           DataView dv = new DataView(dt);
           foreach (DataRowView drv in dv)
           {
               if (drv["点名"] == "") continue;
               string sql = string.Format(@"update dtu set holedeepth = "+Convert.ToDouble(drv["应力值"])*100

               updatedb udb = new updatedb();
               OdbcSQLHelper.Conn = new database().GetStanderConn(133);
               udb.UpdateStanderDB(sql, OdbcSQLHelper.Conn);
        */
    }

    
}
