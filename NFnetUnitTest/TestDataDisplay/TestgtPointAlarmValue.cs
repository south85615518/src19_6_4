﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GT;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestPointAlarmValue
    {
        public ProcessPointAlarmModel processPointAlarmModel = new ProcessPointAlarmModel();
        public ProcessGTPointAlarmRecordsCount processPointAlarmRecordsCount = new ProcessGTPointAlarmRecordsCount();
        public ProcessPointAlarmValueAdd processPointAlarmValueAdd = new ProcessPointAlarmValueAdd();
        public ProcessPointAlarmValueDel processPointAlarmValueDel = new ProcessPointAlarmValueDel();
        public ProcessPointAlarmValueEdit processPointAlarmValueEdit = new ProcessPointAlarmValueEdit();

        public ProcessPointAlarmValueLoad processPointAlarmValueLoad = new ProcessPointAlarmValueLoad();
        public ProcessGTWaterLinePointAlarmValueLoad processGTWaterLinePointAlarmValueLoad = new ProcessGTWaterLinePointAlarmValueLoad();
        //public ProcessGTSurfacePointAlarmValueMutilEdit processGTSurfacePointAlarmValueMutilEdit = new ProcessGTSurfacePointAlarmValueMutilEdit();
        public ProcessSurfacePointAlarmModel processSurfacePointAlarmModel = new ProcessSurfacePointAlarmModel();
        public ProcessGTSurfacePointAlarmRecordsCount processGTSurfacePointAlarmRecordsCount = new ProcessGTSurfacePointAlarmRecordsCount();
        public ProcessSurfacePointAlarmValueAdd processSurfacePointAlarmValueAdd = new ProcessSurfacePointAlarmValueAdd();
        public ProcessSurfacePointAlarmValueDel processSurfacePointAlarmValueDel = new ProcessSurfacePointAlarmValueDel();
        public ProcessSurfacePointAlarmValueEdit processSurfacePointAlarmValueEdit = new ProcessSurfacePointAlarmValueEdit();
        public ProcessSurfacePointAlarmValueLoad processSurfacePointAlarmValueLoad = new ProcessSurfacePointAlarmValueLoad();


        public ProcessGTSupportingAxialForcePointAlarmValueLoad processGTSupportingAxialForcePointAlarmValueLoad = new ProcessGTSupportingAxialForcePointAlarmValueLoad();

        public ProcessGTPointAlarmValueMutilEdit processPointAlarmValueMutilEdit = new ProcessGTPointAlarmValueMutilEdit();
        public data.Model.gtpointalarmvalue model = new data.Model.gtpointalarmvalue {  pointname="s1",  firstalarmname = "一级预警",  secondalarmname="二级预警",  thirdalarmname="三级预警", datatype = data.Model.gtsensortype._displacement ,remark="应力1号点", xmno = 106, isInCalculate = false, line =15, surfaceName = "B", orificeHeight = 100 };
        public data.Model.gtsurfacestructure surfacemodel = new data.Model.gtsurfacestructure {  surfacename = "s1", firstalarmname = "一级预警", secondalarmname = "二级预警", thirdalarmname = "三级预警", datatype = data.Model.gtsensortype._displacement, remark = "应力1号点", xmno = 106, concreteConversionArea = 10, steelConversionArea = 10, concreteElasticityModulus = 100, steelElasticityModulus = 100   };
        public string mssg = "";
        public void main()
        {
            //TestPointAlarmAdd();
            //TestPointAlarmEdit();
            //TestPointAlarmDel();
            //TestPointAlarmLoad();
            //TestPointAlarmModel();
            //TestPointAlarmValueMutilEdit();
            //TestGTWaterLinePointPointAlarmLoad();
            //TestGTSupportingAxialForcePointAlarmLoad();


            //TestSurfacePointAlarmModel();
            TestSurfacePointAlarmValueMutilEdit();
            //TestSurfacePointAlarmAdd();
            //TestSurfacePointAlarmEdit();
            //TestSurfacePointAlarmDel();
            //TestSurfacePointAlarmLoad();
        }
        #region 预警点
        public void TestPointAlarmAdd()
        {
            processPointAlarmValueAdd.PointAlarmValueAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestPointAlarmEdit()
        {
            model.thirdalarmname = "一级预警";
            processPointAlarmValueEdit.PointAlarmValueEdit(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestPointAlarmDel()
        {
            processPointAlarmValueDel.PointAlarmValueDel(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestPointAlarmLoad()
        {
            processPointAlarmValueLoad.PointAlarmValueLoad(106, " 1=1 ", "pointname", 1, 20, "asc",data.Model.gtsensortype._displacement ,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGTWaterLinePointPointAlarmLoad()
        {
            processGTWaterLinePointAlarmValueLoad.GTWaterLinePointAlarmValueLoad(106, " 1=1 ", "pointname", 1, 20, "asc", data.Model.gtsensortype._displacement, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGTSupportingAxialForcePointAlarmLoad()
        {
            processGTSupportingAxialForcePointAlarmValueLoad.GTSupportingAxialForcePointAlarmValueLoad(106, " 1=1 ", "pointname", 1, 20, "asc", data.Model.gtsensortype._displacement, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestPointAlarmModel()
        {
            //processPointAlarmModel.PointAlarmModel(106,"s1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestPointAlarmValueMutilEdit()
        {
            processPointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model,"s1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        #endregion
        #region 钢筋支撑轴面



        public void TestSurfacePointAlarmModel()
        {
            //processSurfacePointAlarmModel.SurfacePointAlarmModel(106, "s1", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSurfacePointAlarmValueMutilEdit()
        {
            new ProcessGTSurfacePointAlarmValueMutilEdit().SurfacePointAlarmValueMutilEdit(surfacemodel, "s1", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSurfacePointAlarmAdd()
        {
            processSurfacePointAlarmValueAdd.SurfacePointAlarmValueAdd(surfacemodel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSurfacePointAlarmEdit()
        {
            model.thirdalarmname = "一级预警";
            processSurfacePointAlarmValueEdit.SurfacePointAlarmValueEdit(surfacemodel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSurfacePointAlarmDel()
        {
            processSurfacePointAlarmValueDel.SurfacePointAlarmValueDel(surfacemodel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSurfacePointAlarmLoad()
        {
            //processSurfacePointAlarmValueLoad.PointAlarmValueLoad(106, " 1=1 ", "surfacename", 1, 20, "asc",  out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        #endregion




    }
}
