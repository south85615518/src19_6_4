﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.HXYL;

namespace NFnetUnitTest.TestDataDisplay
{
    public class TestHXYLDATAAlarm
    {
        public ProcessHXYLAlarmModelListGet processHXYLAlarmModelListGet = new ProcessHXYLAlarmModelListGet();
        public static ProcessHXYLPointBLL processHXYLPointBLL = new ProcessHXYLPointBLL();
        public static ProcessHXYLIDBLL processHXYLIDBLL = new ProcessHXYLIDBLL();
        public static ProcessHXYLAlarmDataCount processHXYLAlarmDataCount = new ProcessHXYLAlarmDataCount();
        public string mssg = "";
        public void main()
        {
            //TestHXYLAlarmModelListGet();
            //TestDATAAlarm();
            TestHXYLAlarmDataCount();
        }
        public void TestHXYLAlarmModelListGet()
        {
            List<string> pointnamelist = processHXYLPointBLL.HXYLPoint(29, out mssg);
            List<string> lis = new List<string>();
            foreach (var pointname in pointnamelist)
            {
                lis.Add(processHXYLIDBLL.HXYLPointID(29, pointname, out mssg));
            }
            processHXYLAlarmModelListGet.HXYLAlarmModelListGet(lis, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestHXYLAlarmDataCount()
        {
            List<string> pointnamelist = processHXYLPointBLL.HXYLPoint(29, out mssg);
            List<string> lis = new List<string>();
            foreach (var pointname in pointnamelist)
            {
                lis.Add(processHXYLIDBLL.HXYLPointID(29, pointname, out mssg));
            }
            processHXYLAlarmDataCount.HXYLAlarmDataCount("韶关铀业", lis, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDATAAlarm()
        {
            List<string> pointnamelist = processHXYLPointBLL.HXYLPoint(29, out mssg);
            List<string> lis = new List<string>();
            foreach (var pointname in pointnamelist)
            {
                lis.Add(processHXYLIDBLL.HXYLPointID(29, pointname, out mssg));
            }
            ProcessHXYLDataAlarmBLL processHXYLDataAlarmBLL = new ProcessHXYLDataAlarmBLL("韶关铀业", 29, lis);
            processHXYLDataAlarmBLL.main();
            ProcessPrintMssg.Print(string.Join(",", processHXYLDataAlarmBLL.alarmInfoList));
        }
    }
}
