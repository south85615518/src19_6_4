﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace NFnetUnitTest.sms
{
    public class TestCom
    {
        public SerialPort sender;
        public string command = "AT#AT+CMGF=1#AT+CSCS=\"GSM\"#AT+CMGS=\"13422208426\"#hello south";
        public string mssg;
        public TestCom(string comName)
        {
            sender = new SerialPort(comName);
            sender.BaudRate = 115200;
            sender.Parity = Parity.None;
            sender.DataBits = 8;
            sender.StopBits = StopBits.One;
        }
        public TestCom()
        {
            
        }
       
        public void main()
        {
            //if (sender.IsOpen) sender.Close();
            List<string> cls = command.Split('#').ToList();
            
            foreach(string cmd in cls)
            {
                Console.Write("现在发送:{0}\n",cmd);
                if (cls.IndexOf(cmd) == cls.Count - 1)
                {
                    TestComWrite(string.Format("{0}", cmd));
                    TestComWrite("0X1A\r\n");
                    Console.Write("{0}返回消息{1}\n", sender.PortName, TestComRead());
                }
                else
                {
                    TestComWrite(string.Format("{0}\r\n", cmd));
                    Thread.Sleep(3000);
                    Console.Write("{0}返回消息{1}\n", sender.PortName, TestComRead());
                }
            }
            
           
            sender.Close();
            ProcessPrintMssg.Print("========短信发送成功========");

        }
        public bool TestComWrite(string sendsms)
        {
            byte[] byteArr = null;
            if (sendsms.IndexOf("0X1A")!=-1)
            {
                byteArr = new byte[] {0X1A};
            }
            else
            {
                byteArr = Encoding.Default.GetBytes(sendsms);
            }
            
            try
            {
                if (!sender.IsOpen) sender.Open();
                sender.Write(byteArr, 0, byteArr.Length);
                return true;
            }
            catch (Exception ex)
            {
                mssg = string.Format("从com口写入出错，出错原因："+ex.Message);
                ProcessPrintMssg.Print(mssg);
                return false;
            }

        }
        public string TestComRead()
        {
            string recsms = "";
            byte[] byteArr = new byte[100];
            try
            {
                if (!sender.IsOpen) sender.Open();
                sender.Read(byteArr, 0, byteArr.Length);
                recsms = Encoding.Default.GetString(byteArr);
                return recsms;
            }
            catch (Exception ex)
            {
                mssg = string.Format("从com口读取出错，出错原因：" + ex.Message);
                ProcessPrintMssg.Print(mssg);
                return "";
            }
        }
    }
}
