﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Other;
using Tool.sms;
using NFnet_BLL.UserProcess;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnitTest.sms
{
    class TestSms
    {
        public static ProcessAdministratrorBLL administratorBLL = new ProcessAdministratrorBLL();
        public static ProcessMonitorBLL monitorBLL = new ProcessMonitorBLL();
        public static ProcessSuperviseBLL superviseBLL = new ProcessSuperviseBLL();
        public static string mssg = "";
        public void main()
        {
            ProcessSettingFileRead();
            //while (true)
            //{
            //    ProcessAdminSms();
            //    ProcessMonitorSms();
            //    ProcesssuperviseSms();
            //    if (Console.ReadKey().KeyChar == 't') break;
            //}
            //TestSmsListSend();
        }
        public void ProcessSettingFileRead()
        {
            Isendsms send = ProcessReadSmsConfigBLL.GetSmsSender(System.AppDomain.CurrentDomain.BaseDirectory+"/smssetting/sms.txt");
            send.smssend("13422208426", "1111111111广佛肇高速下穿武广高铁流溪河大桥监测 QDLXH201-2第288期  2019/1/10 10:37:13  本次东方向1.12  QDLXH201-2一级预警11111111111111111", out mssg);
            //send.pdusmssend("13422208426", "0891683124228024F611000D91688118933727F40008AA044F60597D", out mssg);
        }
        

        public bool ProcessAdminSms()
        {
            Console.Write("输入管理员编号");
            return administratorBLL.ProcessAdministratorsmsBLL(Console.ReadLine(), out mssg);
        }
        public bool ProcessMonitorSms()
        {
            Console.Write("输入监测员编号");
            return monitorBLL.ProcessMonitorsmsBLL(int.Parse(Console.ReadLine()), out mssg);
        }
        public bool ProcesssuperviseSms()
        {
            Console.Write("输入监督员编号");
            return superviseBLL.ProcessSupervisesmsBLL(int.Parse(Console.ReadLine()), out mssg);
        }

        public void TestSmsListSend()
        {
            ProcessSmsSendBLL processSmsSendBLL = new ProcessSmsSendBLL();
            var sendlist = new List<Authority.Model.smssendmember>();
            sendlist.Add(new Authority.Model.smssendmember { name = "谢续明", tel = "13422208426" });
            sendlist.Add(new Authority.Model.smssendmember { name = "唐瑞强", tel = "18813973724" });
            List<string> messlist = new List<string>();
            messlist.Add("短信内容A");
            messlist.Add("短信内容B");
            List<string> mssglist = new List<string>();
            //processSmsSendBLL.ProcessSmsSend(sendlist,messlist,out mssglist );
            ProcessPrintMssg.Print(string.Join("\r\n", mssglist));
        }

 
    }

}
