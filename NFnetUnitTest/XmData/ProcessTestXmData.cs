﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.XmData;

namespace NFnetUnitTest.XmData
{
    public class ProcessTestXmData
    {
        public ProcessXmDataUpdateTableLoad processXmDataUpdateTableLoad = new ProcessXmDataUpdateTableLoad();
        public static string mssg = "";
        public void main()
        {
            //TestXmData();
            //lambdasort();
            Console.WriteLine("-------");
            //singlesort();
            LinqLeftJoin();
        }
        public void TestXmData()
        {
            processXmDataUpdateTableLoad.XmDataUpdateTableLoad("广州铁路科技开发有限公司",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public class sortmodel
        {
            public string xmname{get;set;}
            public int alarmcont { get; set; }
            public int alarmlevel { get; set; }
            public DateTime updatetime{get;set;}
            public DateTime finishedtime { get; set; }

        }

        public void lambdasort()
        {
            List<sortmodel> sortmodelist = new List<sortmodel>();
            sortmodelist.Add(new sortmodel { xmname = "A", alarmcont = 1, alarmlevel = 3, finishedtime = Convert.ToDateTime("2019-1-23"), updatetime  = Convert.ToDateTime("2019-1-20") });
            sortmodelist.Add(new sortmodel { xmname = "B", alarmcont = 5, alarmlevel = 1, finishedtime = Convert.ToDateTime("2019-1-25"), updatetime = Convert.ToDateTime("2019-1-21") });
            sortmodelist.Add(new sortmodel { xmname = "C", alarmcont = 4, alarmlevel = 3, finishedtime = Convert.ToDateTime("2019-1-23"), updatetime = Convert.ToDateTime("2019-1-25") });
            sortmodelist.Add(new sortmodel { xmname = "D", alarmcont = 2, alarmlevel = 1, finishedtime = Convert.ToDateTime("2019-1-28"), updatetime = Convert.ToDateTime("2019-1-28") });
            sortmodelist.Add(new sortmodel { xmname = "E", alarmcont = 4, alarmlevel = 3, finishedtime = Convert.ToDateTime("2019-1-23"), updatetime = Convert.ToDateTime("2019-1-20") });
            var modelist = (sortmodelist.OrderByDescending(m => m.alarmcont ).OrderByDescending(m => m.alarmlevel).OrderByDescending(m => m.updatetime).OrderByDescending(m => m.finishedtime)).ToList<sortmodel>();
            modelist.ForEach(m=>Console.WriteLine(m.xmname));
            Console.ReadKey();
            
        }
        public void singlesort()
        {
            List<sortmodel> sortmodelist = new List<sortmodel>();
            sortmodelist.Add(new sortmodel { xmname = "A", alarmcont = 1, alarmlevel = 3, finishedtime = Convert.ToDateTime("2019-1-23"), updatetime = Convert.ToDateTime("2019-1-20") });
            sortmodelist.Add(new sortmodel { xmname = "B", alarmcont = 5, alarmlevel = 1, finishedtime = Convert.ToDateTime("2019-1-25"), updatetime = Convert.ToDateTime("2019-1-21") });
            sortmodelist.Add(new sortmodel { xmname = "C", alarmcont = 4, alarmlevel = 3, finishedtime = Convert.ToDateTime("2019-1-23"), updatetime = Convert.ToDateTime("2019-1-25") });
            sortmodelist.Add(new sortmodel { xmname = "D", alarmcont = 2, alarmlevel = 1, finishedtime = Convert.ToDateTime("2019-1-28"), updatetime = Convert.ToDateTime("2019-1-28") });
            sortmodelist.Add(new sortmodel { xmname = "E", alarmcont = 4, alarmlevel = 3, finishedtime = Convert.ToDateTime("2019-1-23"), updatetime = Convert.ToDateTime("2019-1-20") });
            var modelist = (sortmodelist.OrderByDescending(m => m.finishedtime)).ToList<sortmodel>();
            modelist = (modelist.OrderByDescending(m => m.updatetime)).ToList<sortmodel>();
            modelist = (modelist.OrderByDescending(m => m.alarmlevel)).ToList<sortmodel>();
            modelist = (modelist.OrderByDescending(m => m.alarmcont)).ToList<sortmodel>();
            modelist.ForEach(m => Console.WriteLine(m.xmname));
            Console.ReadKey();
        }
        public void LinqLeftJoin()
        {
            List<A> listA = new List<A>();
            listA.Add(new A{ name="Tom", type="stu" } );
            listA.Add(new A { name = "Jim", type = "stu" });
            listA.Add(new A { name = "Lucy", type = "stu" });
            List<B> listB = new List<B>();
            listB.Add(new B { name = "Tom",  color = "blue" });
            listB.Add(new B { name = "Jim",  color = "white" });

            var modellist = (from m in listA
                            join t in listB on m.name equals t.name into pvunion
                            from pv in pvunion.DefaultIfEmpty()
                            
                            select new { name = m.name, color =pv==null? "":pv.color }).ToList();


        }
        public class A {
            public string name { get; set; }
            public string type { get; set; }
        }
        public class B {
            public string name { get; set; }
            public string color { get; set; }
        }
    }
}
