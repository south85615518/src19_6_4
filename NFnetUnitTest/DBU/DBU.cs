﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;

namespace NFnetUnitTest.DBU
{
    public class dbu
    {
        public  string sql { get; set; }
        public  bool sqlsafecheck = false;
        public  string mssg = "";
        public  List<string> mssglist = new List<string>();
        public void main()
        {
            if (sqlcheck())
            {
                DBUExcute();
            }
        }
        public  bool sqlcheck()
        {
            if (sqlsafecheck &&( sql.ToLower().IndexOf("drop") != -1 || sql.ToLower().IndexOf("delete") != -1 || sql.ToLower().IndexOf("alter") != -1))
            {
                ExceptionLog.DBUWrite("输入SQL语句对数据库可能存在危险！不予执行！");
                
                return false;
            }
            return true;
        }

        public  void DBUExcute()
        {
            List<string> xmnameList = Aspect.IndirectValue.XmnameListLoad(out mssg);
            int i = 0;
            foreach(var xmname in xmnameList )
            {
                SqlHelpers.SQLScriptHelper.SQLScriptExcute(xmname,sql,out mssg);
                ExceptionLog.DBUWrite((i / xmnameList.Count).ToString());
                i++;
            }
        }


    }
}
