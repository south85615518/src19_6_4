﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Tool;
using NFnetUnitTest.VersionControl;

namespace NFnetUnitTest.DBU
{
    public partial class Form1 : Form
    {

        public static string mssg = "";
        public static System.Timers.Timer timer = new System.Timers.Timer(500);
        public delegate void ListBoxFlush(ListBox listBox, List<string> ls);
        public event ListBoxFlush ListBoxFlushEvent;
        public static bool finished = true;
        public static bool sqlsafecheck = false;
        public Form1()
        {
            InitializeComponent();
        }
        public void init()
        {
            //ProcessSGAlarmDataServer.xmname = xmname;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(processloadline);
            timer.Enabled = true;
            timer.AutoReset = true;
        }

        public void processloadline(object source, System.Timers.ElapsedEventArgs e)
        {
            timer.AutoReset = false;
            ListBoxFlushEvent += ListFlush;
            List<string> commendlog = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory+"数据库更新日志\\log.txt");
            IAsyncResult ar = this.listBox1.BeginInvoke(ListBoxFlushEvent, this.listBox1, commendlog);
            this.listBox1.EndInvoke(ar);
            ListBoxFlushEvent -= ListFlush;
            if(!finished)
            timer.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (finished == false) { MessageBox.Show("数据库脚本正在执行..."); return; }
            Thread thread = new Thread(SqlExcute);
            thread.Start();
            init();

           
        }
        public void SqlExcute()
        {
            finished = false;
            dbu dbu = new dbu { sql = this.textBox1.Text, sqlsafecheck = sqlsafecheck };
            dbu.main();
            ExceptionLog.DBUWrite("数据库脚本执行完成!");
            finished = true;
        }
        public void ListFlush(ListBox listBox, List<string> ls)
        {
            listBox.Items.Clear();
            foreach (var item in ls)
            {
                listBox.Items.Add(item);
            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            logflush();
        }
        private void logflush()
        {
            //List<string> commendlog = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "数据库更新日志\\log.txt");
            //ProcessProgramBagPath.ListBoxBind(this.listBox1, commendlog);
            ListBoxFlushEvent += ListFlush;
            List<string> commendlog = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "数据库更新日志\\log.txt");
            IAsyncResult ar = this.listBox1.BeginInvoke(ListBoxFlushEvent, this.listBox1, commendlog);
            this.listBox1.EndInvoke(ar);
            ListBoxFlushEvent -= ListFlush;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (this.button2.Text == "解除关键字安全检查")
            {
                sqlsafecheck = false;
                this.button2.Text = "开启关键字安全检查";
                return;
            }
            else
            
                this.button2.Text = "解除关键字安全检查";
                sqlsafecheck = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Tool.FileHelper.FileClear(System.AppDomain.CurrentDomain.BaseDirectory + "数据库更新日志\\log.txt");
            logflush();
            //logflush();
        }
    }
}
