﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Tool;
using System.IO;
using NFnet_BLL.DataImport.ProcessFile;
using data.Model;



namespace NFnetUnitTest.HTTP
{
    public class TestHttpServer
    {

        static HttpListener httpobj;
        static HttpWebRequest httpwebrequest;
        public static ProcessFileBLL processFileBLL = new ProcessFileBLL();
       // public static NFnet_BLL.Vibration.ProcessVibrationBLL processVibrationBLL = new NFnet_BLL.Vibration.ProcessVibrationBLL();
        public static string PATH = System.AppDomain.CurrentDomain.BaseDirectory + "文件管理/测振仪数据/";
        public static string mssg = ""; 
        static void Main(string[] args)
        {
            try
            {
                var model = Tool.JsonHelper.ObjectDeserialize<dataAlert>("{'sn':1812019035,'file':{'RecTime':'2019-05-22T19:53:36','FileLen':0,'Data':null,'MaxVal':0.0,'FileNo':null},'IsAlert':true}");
                //Tool.com.UrlHelper.GetRequestQueryStringFromUrl("http://y6777?ers=1&tyty=234234&code=rwrewrwerwerwe");
                //提供一个简单的、可通过编程方式控制的 HTTP 协议侦听器。此类不能被继承。
                httpobj = new HttpListener();
                //定义url及端口号，通常设置为配置文件
                string url = FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\Setting\\HttpServerUrl.txt");
                Console.WriteLine(@"监听URL：" + url);
                httpobj.Prefixes.Add(url);
                //启动监听器
                httpobj.Start();
                //异步监听客户端请求，当客户端的网络请求到来时会自动执行Result委托
                //该委托没有返回值，有一个IAsyncResult接口的参数，可通过该参数获取context对象
                httpobj.BeginGetContext(Result, null);
                Console.WriteLine(@"服务端初始化完毕，正在等待客户端请求,时间：" + DateTime.Now.ToString() + "\r\n");

                string a = @"";
            }
            catch (Exception ex)
            {
                Console.WriteLine(@"服务端初始化出错，错误信息：" + ex.Message + DateTime.Now.ToString() + "\r\n");

            }
            finally
            {
                Console.ReadKey();
            }
        }


        private static void Result(IAsyncResult ar)
        {
            //当接收到请求后程序流会走到这里

            //继续异步监听
            httpobj.BeginGetContext(Result, null);
            var guid = Guid.NewGuid().ToString();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(@"接到新的请求:" + guid + ",时间：" + DateTime.Now.ToString() + "");

            //获得context对象
            var context = httpobj.EndGetContext(ar);

            var request = context.Request;
            var response = context.Response;
            var url = context.Request.Url;
            Console.WriteLine("请求地址:" + url);

            //context.Request.ContentType = "application/json";
            //httpwebrequest.GetRequestStream

            //HttpListenerPostParaHelper httppost=new HttpListenerPostParaHelper (request);
            //        //获取Post过来的参数和数据
            //        List<HttpListenerPostValue> lst=httppost.GetHttpListenerPostValue();
            DateTime dt = DateTime.Now;
            string code = FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\Setting\\ZKCKUserCode.txt");
            string filenametemplate = string.Format("{0}_{1}月_{2}日_{3}时_{4}分_{5}秒{6}", "{0}", dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Millisecond, ".txt");
            string fileName = string.Format(filenametemplate, "测振仪推送数据");
            Dictionary<string, string> parameters = Tool.com.UrlHelper.GetRequestQueryStringFromUrl(url.ToString());
            List<string> querystringparameters = new List<string>();
            foreach (var model in parameters.Keys)
            {
                Console.WriteLine(string.Format("key:{0}-value:{1}", model, context.Request.QueryString[model.ToString()]));
                querystringparameters.Add(string.Format("{0}:{1}", model, context.Request.QueryString[model.ToString()]));
            }
            data.Model.dataAlert requestModel = new data.Model.dataAlert();
            string poststr = "";
            Stream sr = context.Request.InputStream;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(sr, Encoding.UTF8))
            {
                string str = reader.ReadToEnd();
                Console.WriteLine("请求参数:" + str);
                poststr = str;

                string path = PATH + fileName;
                processFileBLL.JsonUploadProcess(string.Join("\r\n", querystringparameters) + "\r\n" + str, path);

                requestModel = JsonHelper.ObjectDeserialize<data.Model.dataAlert>(str);


                //if(parameters.Keys.Contains("code"))

                //ExceptionLog.ExceptionWrite
            }

            context.Response.ContentType = "text/plain;charset=UTF-8";//告诉客户端返回的ContentType类型为纯文本格式，编码为UTF-8
            context.Response.AddHeader("Content-type", "text/plain");//添加响应头信息
            context.Response.ContentEncoding = Encoding.UTF8;
            string returnObj = null;//定义返回客户端的信息
            if (request.HttpMethod == "POST" && request.InputStream != null)
            {
                //处理客户端发送的请求并返回处理信息
                returnObj = HandleRequest(request, response);
            }
            else
            {
                returnObj = @"不是post请求或者传过来的数据为空";
            }
            var returnByteArr = Encoding.UTF8.GetBytes(returnObj);//设置客户端返回信息的编码
            try
            {
                using (var stream = response.OutputStream)
                {
                    //把处理信息返回到客户端
                    stream.Write(returnByteArr, 0, returnByteArr.Length);
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(@"网络蹦了：{ex.ToString()}");
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(@"请求处理完成：" + guid + ",时间：" + DateTime.Now.ToString() + "\r\n");

            ProcessSnFileDataGet(requestModel.sn, code, filenametemplate);
            //if (parameters.Keys.Contains("code"))
            string fileinfo = Tool.FileHelper.ProcessStringRead(PATH + string.Format(filenametemplate, "获取测振仪文件列表返回信息文件"));
            List<datafileinfo> ldf = (List<datafileinfo>)Tool.JsonHelper.ObjectDeserialize<List<datafileinfo>>(fileinfo);



            foreach(var model in ldf)
            {
                data.Model.dataRequestInfo postdata = new data.Model.dataRequestInfo
                {
                    sn = requestModel.sn,
                    file = new datafileinfo
                    {
                        FileNo = model.FileNo,
                        RecTime = model.RecTime.Replace("T"," "),
                        FileLen = model.FileLen,
                        MaxVal = model.MaxVal

                    },
                    Key = code,
                    FileNo = model.FileNo,
                    Type = "4850N"

                };



                ProcessFileDataGet(postdata,code, filenametemplate);
                break;

            };








            //Console.WriteLine(sr.Length.ToString());
            //var requestparameters = context.Request.Url;
            //foreach (var model in requestparameters.Keys)
            //{
            //    ExceptionLog.ExceptionWrite(string.Format("key:{0}-value:{1}", model, context.Request.Form[model.ToString()]));
            //}




            ////如果是js的ajax请求，还可以设置跨域的ip地址与参数
            //context.Response.AppendHeader("Access-Control-Allow-Origin", "*");//后台跨域请求，通常设置为配置文件
            //context.Response.AppendHeader("Access-Control-Allow-Headers", "ID,PW");//后台跨域参数设置，通常设置为配置文件
            //context.Response.AppendHeader("Access-Control-Allow-Method", "post");//后台跨域请求设置，通常设置为配置文件

        }

        private static string HandleRequest(HttpListenerRequest request, HttpListenerResponse response)
        {
            string data = null;
            try
            {
                var byteList = new List<byte>();
                var byteArr = new byte[2048];
                int readLen = 0;
                int len = 0;
                //接收客户端传过来的数据并转成字符串类型
                do
                {
                    readLen = request.InputStream.Read(byteArr, 0, byteArr.Length);
                    len += readLen;
                    byteList.AddRange(byteArr);
                } while (readLen != 0);
                data = Encoding.UTF8.GetString(byteList.ToArray(), 0, len);

                //获取得到数据data可以进行其他操作
            }
            catch (Exception ex)
            {
                response.StatusDescription = "404";
                response.StatusCode = 404;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(@"在接收数据时发生错误:" + ex.ToString());
                return @"在接收数据时发生错误:" + ex.ToString();//把服务端错误信息直接返回可能会导致信息不安全，此处仅供参考
            }
            response.StatusDescription = "200";//获取或设置返回给客户端的 HTTP 状态代码的文本说明。
            response.StatusCode = 200;// 获取或设置返回给客户端的 HTTP 状态代码。
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(@"接收数据完成:" + data.Trim() + ",时间：" + DateTime.Now.ToString());
            return @"接收数据完成";
        }
        public static void ProcessSnFileDataGet(string sn, string code, string filetemplate)
        {
            try
            {
                string serverstr = FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\Setting\\ZKCKServer.txt");
                string url = string.Format("http://{0}/api/FileController/GetFile?sn={1}&code={2}", serverstr, sn, code);
                Tool.http.HttpHelper httpHelper = new Tool.http.HttpHelper();
                Console.WriteLine("现在请求{0} {1}", url, DateTime.Now);
                string FileInfoGetStr = httpHelper.PostMessageToWeb("GET", url, "");
                Console.WriteLine("请求{0}返回结果{1}", url, FileInfoGetStr);
                string path = PATH + string.Format(filetemplate, "获取测振仪文件列表返回信息文件");
                processFileBLL.JsonUploadProcess(FileInfoGetStr, path);
            }
            catch (Exception ex)
            {
                Console.WriteLine("获取设备{0}的数据文件信息出错,错误信息：" + ex.Message);
            }

        }
     
        public static void ProcessFileDataGet(data.Model.dataRequestInfo postdata, string code, string filetemplate)
        {
            string postdatastr = "";
            try
            {
                postdatastr = JsonHelper.ObjectSerialize<data.Model.dataRequestInfo>(postdata);
                string serverstr = FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\Setting\\ZKCKServer.txt");
                string url = string.Format("http://{0}/api/FileController/GetData?code={1}", serverstr, code);
                Tool.http.HttpHelper httpHelper = new Tool.http.HttpHelper();
                Console.WriteLine("现在请求{0}获取数据 请求参数:{1} {2} ", url, postdatastr, DateTime.Now);
                string SnDataGetStr = httpHelper.PostMessageToWeb(url, postdatastr);
                Console.WriteLine("请求{0}返回结果{1}", url, SnDataGetStr);
                Dictionary<string, object> model = (Dictionary<string, object>)JsonHelper.ObjectSerialize(SnDataGetStr);
                dynamic dynamicmodel = JsonHelper.ObjectSerialize(model["Data"].ToString());
                chartDataModel datamodel = new chartDataModel();
                datamodel.chanNum = Tool.JsonHelper.JsonDataDescrialize(SnDataGetStr, "chanNum", 0);
                string itemstr = Tool.JsonHelper.JsonDataDescrialize(SnDataGetStr, "item", 0);
                datamodel = JsonHelper.JsonDataToModel<chartDataModel>(itemstr);

                string path = PATH + string.Format(filetemplate, "获取测振仪仪器数据文件");
                var chartdatamodel = JsonHelper.ObjectDeserialize<chartDataModel>(itemstr);

                double[] maxDataVal = ObjectHelper.ObjectArraryToTArrary<double>( (object[])(Tool.JsonHelper.DeserializeObject(datamodel.maxDataVal)));
                processFileBLL.JsonUploadProcess(SnDataGetStr, path);

                vibration2.Model.vibration vibrationmodel = new vibration2.Model.vibration { 
                    point_name = postdata.sn,
                    millionsec = maxDataVal[0],
                    XMax = maxDataVal[1],
                    YMax = maxDataVal[2],
                    ZMax = maxDataVal[3],
                    time = Convert.ToDateTime(string.Format("{0}/{1}/{2} {3}:{4}:{5}", datamodel.year, datamodel.month, datamodel.day, datamodel.hour, datamodel.minute, datamodel.second))
                };
             //   processVibrationBLL.ProcessAddVibration(vibrationmodel,out mssg);
                 /*day":27,"hour":23,"maxDataVal":[0.1951749,0.062747054,0.091167204,0.0066995686,0.0,0.0,0.0,0.0],"minute":44,"month":5,"portDatas":[{"b":0,"full":7169,"k":26.9,"max":0.1951749,"rang":"10V","rangIndex":1,"res":0,"trigLevel":0.1,"trigMode":1,"unit":"m/s","unitIndex":5,"zero":32768},{"b":0,"full":7170,"k":26.9,"max":0.062747054,"rang":"10V","rangIndex":1,"res":0,"trigLevel":0.1,"trigMode":1,"unit":"m/s","unitIndex":5,"zero":32764},{"b":0,"full":7170,"k":26.4,"max":0.091167204,"rang":"10V","rangIndex":1,"res":0,"trigLevel":0.1,"trigMode":1,"unit":"m/s","unitIndex":5,"zero":32764},{"b":0,"full":7173,"k":28,"max":0.0066995686,"rang":"10V","rangIndex":1,"res":0,"trigLevel":0.1,"trigMode":1,"unit":"m/s","unitIndex":5,"zero":32761}],"recLen":2,"sampleRate":5000,"second":42,"trigDelay":-100,"type":"4850N","year":2019},"ok":true,"msg":"查询成功"}","IsSuccess":true,"Message":"调用成功，已请求接口","Model":null,"Code":1}*/
                 





            }
            catch (Exception ex)
            {
                Console.WriteLine("获取设备信息{0}的数据文件数据出错,错误信息：" + ex.Message, postdatastr);
            }
        }
        

    }
    public class chartDataModel
    {

        public string chanNum { get; set; }
        public string chartData { get; set; }
        public string data { get; set; }
        public string day { get; set; }
        public string hour { get; set; }
        public string maxDataVal { get; set; }
        public string minute { get; set; }
        public string month { get; set; }
        public string portDatas { get; set; }
        public string recLen { get; set; }
        public string sampleRate { get; set; }
        public string second { get; set; }
        public string trigDelay { get; set; }
        public string type { get; set; }
        public string year { get; set; }
    }
    

    
}
