﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Controlsurvey.TotalStationBLL;
using FmosWebServer;
using System.Web.Script.Serialization;
namespace NFnetUnitTest.Controlsurvey
{
    class TestSettingBLL
    {
        public static WebServer server = new WebServer("192.168.168.15", 8092);
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessSettingBLL settingBLL = new ProcessSettingBLL();
        public static string mssg = "";
        public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
        public void main() {
            ProcessSetting();


        }
        public bool ProcessSetting()
        {
            var processSettingFormatModel = new ProcessSettingBLL.ProcessSettingFormatModel(xmname);
            if (settingBLL.ProcessSettingFormat(processSettingFormatModel, out mssg))
            {
                server.Start();
                string settingJson =jss.Serialize(processSettingFormatModel.st);
                while (true)
                {
                    char cmd = Console.ReadKey().KeyChar;
                    
                    if (cmd == 't') break;
                    server.SetSetting(xmname, settingJson, out mssg);
                    ProcessPrintMssg.Print(mssg);
                }
                server.Stop();
            }
            else
            {
                ProcessPrintMssg.Print(mssg);
            }
            return false;
        }
    }
}
