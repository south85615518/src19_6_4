﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FmosWebServer;
using System.Web.Script.Serialization;
using NFnet_BLL.Controlsurvey.TotalStationBLL;
using TotalStation.Model.fmos_obj;
using System.Threading;
namespace NFnetUnitTest.Controlsurvey
{

    public class TestTimeSetBLL
    {
        public WebServer server = null;// new WebServer("192.168.168.15", 8092);
        public  JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessTimeTaskBLL timetaskBLL = new ProcessTimeTaskBLL();
        public static string mssg = "";
        public string xmname = "华南水电四川成都大坝监测AB";
        public static string timeJson = "";
        public void main()
        {
            //ProcessTimeSet();
            //ProcessTimeTaskAdd();
            //ProcessTimeTaskDel();
            //ProcessTimeSetBLL();
            ProcessTimeTaskSort();
        }

        public bool ProcessTimeSetBLL()
        {
            if (timetaskBLL.ProcessTimeSetControlSurvey(xmname, out mssg))
            {
                ProcessPrintMssg.Print(mssg);
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public bool ProcessTimeSet()
        {
            var ProcessTimeTaskFormatModel = new ProcessTimeTaskBLL.ProcessTimeTaskFormatModel(xmname);

            if (timetaskBLL.ProcessTimeTaskFormat(ProcessTimeTaskFormatModel, out mssg))
            {
                server.Start();
                while (true)
                {
                    char cmd = Console.ReadKey().KeyChar;
                    if (cmd == 't') break;
                    server.SetTime(xmname, ConvertToString(ProcessTimeTaskFormatModel.timetask), out mssg);
                    ProcessPrintMssg.Print(mssg);
                }
                server.Stop();
            }
            else
            {
                ProcessPrintMssg.Print(mssg);
            }



            return false;
        }

        public List<string> ConvertToString(List<timetask> timetasks)
        {
            List<string> ls = new List<string>();
            foreach (timetask e in timetasks)
            {
                ls.Add(jss.Serialize(e));
            }
            return ls;
        }

        public void ProcessTimeTaskAdd()
        {
            timetask model = new timetask
            {
                CH = 2,
                DateTaskName = new Random(99).NextDouble().ToString(),
                StartTimeType = 1,
                TimingHour = 22,
                TimingMinute = 0,
                mInterval = 2,
                Indx = new Random(999).Next(),
                taskName = xmname



            };
            timetaskBLL.ProcessTimeTaskAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
            ProcessTimeTaskDel(model.DateTaskName);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessTimeTaskDel(string dateTaskName)
        {
            timetask model = new timetask
            {
                CH = 2,
                DateTaskName = dateTaskName,
                StartTimeType = 1,
                TimingHour = 22,
                TimingMinute = 0,
                mInterval = 2,
                Indx = new Random(999).Next(),
                taskName = xmname



            };
            timetaskBLL.ProcessTimeTaskDel(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void ProcessTimeTaskSort()
        {

            var ProcessTimeTaskFormatModel = new ProcessTimeTaskBLL.ProcessTimeTaskFormatModel(xmname);
            timetaskBLL.ProcessTimeTaskAllSort(ProcessTimeTaskFormatModel,out mssg);
            ProcessPrintMssg.Print(mssg);

        }




    }
}
