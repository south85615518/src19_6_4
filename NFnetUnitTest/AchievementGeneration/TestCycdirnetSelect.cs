﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AchievementGeneration.TotalStationDAL;
using TotalStation.Model.fmos_obj;


namespace NFnetUnitTest.AchievementGeneration
{
    class TestCycdirnetSelect
    {
        public ProcessCycdirnetBLL cycdirnetBLL = new ProcessCycdirnetBLL();
        public ProcessCycdirnetCgBLL cycdirnetCgBLL = new ProcessCycdirnetCgBLL();
        public string xmname = "数据测试";

        public string cyc = "";

        cycdirnet cycdirnetModel = new cycdirnet();

        //目标周期
        //public string cycTarget = Console.ReadLine();
        public string mssg = "";
        public string pointname = "";
        public void main()
        {
            //ProcessCycdirnetSelect();
            //ProcessCgPointnameCycdirnetExist();
            //ProcessCycdirnetSelect();
            //ProcessCgCycdirnetAdd(cycdirnetModel);
            char ch;
            while (true)
            {
                try
                {
                    Console.WriteLine("输入周期：");
                    cyc = Console.ReadLine();
                    Console.WriteLine("输入点名：");
                    pointname = Console.ReadLine();
                    ProcessCgCycdirnet();
                    Console.WriteLine("按任意非T键执行");
                    char cmd = Console.ReadKey().KeyChar;

                    if (cmd == 't') break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("程序出错");
                    Console.WriteLine("退出请输入T");
                    char cmd = Console.ReadKey().KeyChar;

                    if (cmd == 't') break;
                }
            }

        }
        /// <summary>
        /// 选取结果数据中特定周期的数据
        /// </summary>
        public bool ProcessCycdirnetSelect()
        {
            var processCycdirnetModelGetModel = new ProcessCycdirnetBLL.ProcessCycdirnetModelGetModel(xmname, pointname, int.Parse(cyc));
            if (cycdirnetBLL.ProcessCycdirnetModel(processCycdirnetModelGetModel, out mssg))
            {
                cycdirnetModel = processCycdirnetModelGetModel.model;
                ProcessPrintMssg.Print(mssg);
                return true;
            }
            else
            {
                ProcessPrintMssg.Print(mssg);
                return false;
            }


        }

        public bool ProcessCgPointnameCycdirnetExist()
        {
            //检查成果库中是否存在
            var processCgPointnameCycdirnetExistModel = new ProcessCycdirnetCgBLL.ProcessCgPointnameCycdirnetExistModel(xmname, pointname, (int)cycdirnetModel.CYC);
            if (cycdirnetCgBLL.ProcessCgPointnameCycdirnetExist(processCgPointnameCycdirnetExistModel, out mssg))
            {

                ProcessPrintMssg.Print(mssg);
                return true;
            }
            return false;

        }


        /// <summary>
        /// 测量库到成果库中数据处理
        /// </summary>
        public void ProcessCgCycdirnetAdd(TotalStation.Model.fmos_obj.cycdirnet cycdirnet, out string mssg)
        {

            cycdirnetCgBLL.ProcessCgCycdirnetAdd(cycdirnet, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void ProcessCgCycdirnet()
        {
            //1.从测量库中获取一条记录
            if (!ProcessCycdirnetSelect()) return;

            Console.WriteLine("存为第_周期数据：");
            cycdirnetModel.CYC = int.Parse(Console.ReadLine());

            //2.查看成果库中是否已经存在相同的记录
            //3.如果不存在，则需获取成果库中上一周期的记录
            if (!ProcessCgPointnameCycdirnetExist())
            {
                var processCycdirnetModelGetModel = new ProcessCycdirnetCgBLL.ProcessCycdirnetModelGetModel(xmname, pointname, (int)cycdirnetModel.CYC - 1);
                cycdirnetCgBLL.ProcessCycdirnetModel(processCycdirnetModelGetModel, out mssg);
                ProcessPrintMssg.Print(mssg);
                if (processCycdirnetModelGetModel.model == null && cycdirnetModel.CYC == 1)
                {
                    ProcessCgCycdirnetAdd(cycdirnetModel, out mssg);
                    return;
                }
                else if (processCycdirnetModelGetModel.model != null)
                {
                    if (processCycdirnetModelGetModel.model.Time >= cycdirnetModel.Time)
                    {
                        //异常1.周期和测量时间的大小排序不一致
                        ProcessPrintMssg.Print("周期和测量时间的大小排序不一致");
                        return;
                    }
                    else
                    {
                        //4.如果上一周的时间小于当前时间则执行插入
                        //重新计算本次变化量
                        cycdirnetCgBLL.cycdirnetThisCalculation(processCycdirnetModelGetModel.model, cycdirnetModel);
                        ProcessCgCycdirnetAdd(cycdirnetModel, out mssg);
                        
                        return;
                    }
                }
                else if (processCycdirnetModelGetModel.model == null && cycdirnetModel.CYC != 1)
                {
                    //异常2.请先插入前一个周期的数据
                    ProcessPrintMssg.Print(string.Format("请先插入第{0}周期的数据", cycdirnetModel.CYC - 1));
                    return;
                }

            }
            else
            {
                //5.如果存在则应获取上一周的数据记录，获取下一周期的数据，如果同
                //时满足需要更新的记录的测量时间比上一个周期的测量时间大比下一个周期的测量时间小的情况下
                //边值处理：如果获取不到上一周获取下一周则认为当前周期没有上一周或者下一周期
                var processCycdirnetModelPex = new ProcessCycdirnetCgBLL.ProcessCycdirnetModelGetModel(xmname, pointname, (int)cycdirnetModel.CYC - 1);
                cycdirnetCgBLL.ProcessCycdirnetModel(processCycdirnetModelPex, out mssg);
                ProcessPrintMssg.Print(mssg);
                var processCycdirnetModelNext = new ProcessCycdirnetCgBLL.ProcessCycdirnetModelGetModel(xmname, pointname, (int)cycdirnetModel.CYC + 1);
                cycdirnetCgBLL.ProcessCycdirnetModel(processCycdirnetModelNext, out mssg);
                ProcessPrintMssg.Print(mssg);
                if (processCycdirnetModelPex.model == null && processCycdirnetModelNext.model == null)
                {
                    ProcessCgCycdirnetAdd(cycdirnetModel, out mssg);
                    return;
                }

                //如果下一个周期数据为空并且满足上一周的测量时间小于当前时间则执行更新
                else if (processCycdirnetModelNext.model == null && (cycdirnetModel.Time > processCycdirnetModelPex.model.Time))
                {
                    //重新计算本周期本次变化
                    cycdirnetCgBLL.cycdirnetThisCalculation(processCycdirnetModelPex.model, cycdirnetModel);
                    ProcessCgCycdirnetAdd(cycdirnetModel, out mssg);
                    
                   
                    return;
                }
                else if (cycdirnetModel.Time > processCycdirnetModelPex.model.Time && cycdirnetModel.Time < processCycdirnetModelNext.model.Time)
                {

                    cycdirnetCgBLL.cycdirnetThisCalculation(processCycdirnetModelPex.model, cycdirnetModel);
                    ProcessCgCycdirnetAdd(cycdirnetModel, out mssg);
                    //重新计算下一周的本次变化
                    cycdirnetCgBLL.cycdirnetThisCalculation(cycdirnetModel, processCycdirnetModelNext.model);
                    ProcessCgCycdirnetAdd(processCycdirnetModelNext.model, out mssg);
                    
                    return;
                }
                else
                {
                    ProcessPrintMssg.Print("该记录不符合更新条件，不执行更新");
                }
               


            }








        }




    }
}
