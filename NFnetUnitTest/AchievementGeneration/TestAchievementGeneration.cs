﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using Tool;
using NFnet_BLL.DataProcess;
using System.Web.Script.Serialization;
using NFnet_BLL.AchievementGeneration.TotalStationDAL;
using TotalStation.Model.fmos_obj;

namespace NFnetUnitTest.AchievementGeneration
{
    class TestAchievementGeneration
    {
//        public static ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
//        public static JavaScriptSerializer jss = new JavaScriptSerializer();
//        public static ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
//        public static ProcessCgResultDataBLL cgResultDataBLL = new ProcessCgResultDataBLL();
//        public static ProcessCycdirnetBLL cycdirnetBLL = new ProcessCycdirnetBLL();
//        public static ProcessCycdirnetCgBLL cycdirnetCgBLL = new ProcessCycdirnetCgBLL();
//        public string jsondata;
//        public static string mssg = "";
//        public void main()
//        {
//            while (true)
//            {
//                try
//                {
//                    Console.Write(@"请选择要测试的模块编号\n
//                    【0】点号列表加载\n
//                    【1】成果库周期列表加载\n
//                    【2】测量库点号数据加载\n
//                    【3】成果库点号数据加载\n
//                    【4】计算本次变化\n
//                    【5】测量库数据加载\n
//                    【t】退出\n");
//                    Console.Write("输入编号_\n");
//                    char type = Console.ReadKey().KeyChar;
//                    if (type == 't') break;
//                    switch (type)
//                    {
//                        case '0':
//                            TestPointTableLoad();
//                            break;
//                        case '1':
//                            TestCgPointCycListLoad();
//                            break;
//                        case '2':
//                            ProcessCycdirnetDataLoad();
//                            break;
//                        case '3':
//                            ProcessCgCycdirnet();
//                            break;
//                        case '4': ProcessCycdirnetThisCalculation();
//                            break;
//                        case '5':
//                            Console.Write("选择想要执行的操作：[0]删除成果数据[\\]插入或者更新成果数据\n");
//                            if (Console.ReadKey().KeyChar == '0')
//                                TestProcessCycdirnetDataDelete();
//                            else
//                            {
//                                Console.WriteLine("输入[0]更新[1]添加");
//                                if (Console.ReadKey().KeyChar == '0')
//                                    TestCgCycdirnetUpdate();
//                                else
//                                    //TestCgCycdirnetAdd();
//                                    //TestProcessProcessCgCycdirnetAdd();
//                                    TestProcessCycdirnetDataTableLoad();
//                                    //TestProcessCgPointNameResultDataLoad();

                               
//                            }
//                            break;
//                    }
//                }
//                catch (Exception ex)
//                {
//                    Console.WriteLine("程序出错，按任意键执行......");

//                }
//            }
//        }

//        public void TestPointTableLoad()
//        {
//            //加载左侧点号列表
//            int cont = 0;
//            DataTable dt = null;
//            string mssg = "";
//            string sFiled = "undefined", sString = "";
//            Console.Write("项目名:");
//            string xmname = "数据测试";//Console.ReadLine();
//            Console.Write("当前页数:");
//            int pageIndex = 1;//int.Parse(Console.ReadLine());
//            Console.Write("每页的行数:");
//            int rows = 10;// int.Parse(Console.ReadLine());
//            Console.Write("排序列:");
//            string sord = " asc ";//Console.ReadLine();
//            string colName = "point_name";
//            //拼凑查询条件
//            string searchString = sFiled != "undefined" && sFiled != null ? string.Format("{0} like '%{1}%' ", sFiled, sString) : "1 = 1";

//            var processPointAlarmLoadModel = new ProcessPointAlarmBLL.ProcessAlarmLoadModel(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), searchString, colName, pageIndex, rows, sord);
//            var processPointAlarmRecordsCountModel = new ProcessPointAlarmBLL.ProcessAlarmRecordsCountModel(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), searchString, pageIndex, rows, sord);
//            if (pointAlarmBLL.ProcessPointLoad(processPointAlarmLoadModel, out mssg))
//            {
//                dt = processPointAlarmLoadModel.dt;
//                if (pointAlarmBLL.ProcessPointAlarmRecordsCount(processPointAlarmRecordsCountModel, out mssg))
//                {
//                    cont = Convert.ToInt32(processPointAlarmRecordsCountModel.totalCont);

//                }
//                else
//                {
//                    //计算结果数据数量出错信息反馈
//                }
//            }
//            else
//            {
//                //加载结果数据出错错误信息反馈

//            }
//            if (JsonHelper.ProcessDataIntoJson(dt, cont, pageIndex, rows, out jsondata))
//            {
//                ProcessPrintMssg.Print(jsondata);
//            }
//        }

//        public void TestCgPointCycListLoad()
//        {
//            Console.Write("项目名:");
//            string xmname = "数据测试";//Console.ReadLine();
//            string mssg = "";
//            Console.Write("点名:");
//            string pointName = Console.ReadLine();
//            //加载成果库中某个点名的周期列表
//            var processCgResultDataCycLoadModel = new ProcessCgResultDataBLL.ProcessCgResultDataCycLoadModel(xmname, pointName);
//            if (!ProcessCgResultDataBLL.ProcessCgResultDataCycLoad(processCgResultDataCycLoadModel, out mssg))
//            {
//                ProcessPrintMssg.Print(mssg);
//                return;
//            }
//            jsondata = jss.Serialize(processCgResultDataCycLoadModel.ls);
//            ProcessPrintMssg.Print(jsondata);
//            return;
//        }

//        public void ProcessCycdirnetDataLoad()
//        {
//            int cont = 0;
//            DataTable dt = null;
//            string mssg = "";
//            string sFiled = "undefined", sString = "";
//            Console.Write("项目名:");
//            string xmname = "数据测试";//Console.ReadLine();
//            Console.Write("当前页数:");
//            int pageIndex = 1;//int.Parse(Console.ReadLine());
//            Console.Write("每页的行数:");
//            int rows = 10;// int.Parse(Console.ReadLine());
//            Console.Write("排序列:");
//            string sord = " asc ";//Console.ReadLine();
//            string colName = "point_name";
//            string pointName = Console.ReadLine();
//            int cyc = -1;
//            var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointName,DateTime.Now, DateTime.Now);
//            if (resultDataBLL.ProcessResultDataSearch(processResultData, out mssg))
//            {
//                dt = processResultData.dt;
//                cont = dt.Rows.Count;
//            }
//            else
//            {
//                //加载结果数据出错错误信息反馈

//            }
//            if (JsonHelper.ProcessDataIntoJson(dt, cont, pageIndex, rows, out jsondata))
//            {
//                ProcessPrintMssg.Print(jsondata);
//            }


//        }

//        public void ProcessCgCycdirnet()
//        {
//            int cont = 0;
//            DataTable dt = null;
//            string mssg = "";
//            string sFiled = "undefined", sString = "";
//            Console.Write("项目名:");
//            string xmname = "数据测试";//Console.ReadLine();
//            Console.Write("当前页数:");
//            int pageIndex = 1;//int.Parse(Console.ReadLine());
//            Console.Write("每页的行数:");
//            int rows = 10;// int.Parse(Console.ReadLine());
//            Console.Write("排序列:");
//            string sord = " asc ";//Console.ReadLine();
//            string colName = "point_name";
//            string pointName = Console.ReadLine();
//            Console.Write("周期:");
//            int cyc = int.Parse(Console.ReadLine());
//            var processResultDataLoadModel = new ProcessCgResultDataBLL.ProcessCgResultDataSearchModel(xmname, pointName, cyc);

//            cont = 0;
//            dt = null;
//            if (cgResultDataBLL.ProcessCgResultDataSearch(processResultDataLoadModel, out mssg))
//            {
//                dt = processResultDataLoadModel.dt;
//                cont = dt.Rows.Count;
//            }
//            else
//            {
//                //加载结果数据出错错误信息反馈

//            }
//            if (JsonHelper.ProcessDataIntoJson(dt, cont, pageIndex, rows, out jsondata))
//            {
//                ProcessPrintMssg.Print(jsondata);
//            }
//        }

//        public void ProcessCycdirnetThisCalculation()
//        {
//            Console.Write("项目名:");
//            string xmname = "数据测试";//Console.ReadLine();
//            Console.Write("当前页数:");
//            int pageIndex = 1;//int.Parse(Console.ReadLine());
//            Console.Write("每页的行数:");
//            int rows = 10;// int.Parse(Console.ReadLine());
//            Console.Write("排序列:");
//            string sord = " asc ";//Console.ReadLine();
//            string colName = "point_name";
//            string pointname = Console.ReadLine();
//            Console.Write("测量数据周期:");
//            //向成果库添加或者更新数据
//            int c = int.Parse(Console.ReadLine());//测量库中选择的周期
//            Console.Write("成果数据周期:");
//            int cg = int.Parse(Console.ReadLine());//存为成果库的周期
//            //参数说明 cyc 测量库结果数据的周期
//            //获取两个周期的数据对象
//            var processCycdirnetModelCg = new ProcessCycdirnetCgBLL.ProcessCycdirnetModelGetModel(xmname, pointname, cg);
//            cycdirnetCgBLL.ProcessCycdirnetModel(processCycdirnetModelCg, out mssg);

//            var processCycdirnetModel = new ProcessCycdirnetBLL.ProcessCycdirnetModelGetModel(xmname, pointname, c);
//            cycdirnetBLL.ProcessCycdirnetModel(processCycdirnetModel, out mssg);

//            cycdirnetCgBLL.cycdirnetThisCalculation(processCycdirnetModelCg.model, processCycdirnetModel.model);
//            TotalStation.Model.fmos_obj.cycdirnet cycdirnet = null;
//            cycdirnet = processCycdirnetModel.model;
//            ProcessPrintMssg.Print(string.Format("\nthis_dn:{0}\nthis_de:{1}\nthis_dz:{1}\n", cycdirnet.This_dN, cycdirnet.This_dE, cycdirnet.This_dZ));
//        }
//        //public static ProcessCgPointAlarmBLL cgPointAlarmBLL = new ProcessCgPointAlarmBLL();
//        /// <summary>
//        /// 删除选定周期以及后面的所有数据
//        /// </summary>
//        public void TestProcessCycdirnetDataDelete()
//        {
//            while (true)
//            {
//                string xmname = "数据测试";
//                string pointname = "Z7-4";
//                Console.WriteLine("成果库中有的周期有：");
//                var processCgResultDataCycLoadModel = new ProcessCgResultDataBLL.ProcessCgResultDataCycLoadModel(xmname, pointname);
//                if (!ProcessCgResultDataBLL.ProcessCgResultDataCycLoad(processCgResultDataCycLoadModel, out mssg))
//                {

//                    return;
//                }
//                Console.WriteLine("成果库中的数据周期有");
//                if (processCgResultDataCycLoadModel.ls.Count == 0)
//                    Console.WriteLine("目前成果库中{0}没有数据", pointname);
//                Console.WriteLine("目前成果库中{0}存在的周期有{1}", pointname, string.Join(",", processCgResultDataCycLoadModel.ls));
//                // Console.WriteLine("请输入成果库周期");
//                //int cyc = Convert.ToInt32( Console.ReadLine());
//                int cyc = 0;
//                while (true && processCgResultDataCycLoadModel.ls.Count > 0)
//                {
//                    Console.WriteLine("请输入成果库周期");
//                    cyc = Convert.ToInt32(Console.ReadLine());
//                    if (!processCgResultDataCycLoadModel.ls.Contains(cyc.ToString()))
//                    {
//                        Console.WriteLine("只能选择成果库中已有的周期，请重新输入：");
//                        continue;
//                    }
//                    break;
//                }
//                var processCgCycdirnetDelModel =new ProcessCycdirnetCgBLL.ProcessCgCycdirnetDelModel(xmname,cyc,pointname);
//                cycdirnetCgBLL.ProcessCgCycdirnetDel(processCgCycdirnetDelModel,out mssg);
//                ProcessPrintMssg.Print(mssg);
//                cgPointAlarmBLL.ProcessHotPotColorUpdate(Aspect.IndirectValue.GetXmnoFromXmname(xmname), "表面位移", "全站仪", pointname, 0, out mssg);
//                Console.Write("输入任意非t键执行否则返回上一层\n");
//                if (Console.ReadKey().KeyChar == 't') break;
//            }


//        }

//        //测量库数据根据成果库周期的选择而加载
//        public void TestProcessCycdirnetDataLoad()
//        {
//            //成果周期列表
//            List<string> cgCycList = new List<string>(); 
//            //测量周期列表
//            List<string> cycList = new List<string>();
//            //成果库选定周期前周期的数据对象
//            cycdirnet cycdirnetModelEx = new cycdirnet();
//            //成果库选定周期的数据对象
//            cycdirnet cycdirnetModel = new cycdirnet();
//            //成果库选定周期的后一个周期对象
//            cycdirnet cycdirnetModelNext = new cycdirnet();
//            //成果库选定的周期
//            int cyc = 0;
//            //测量库数据表
//            DataTable dt = null;
//            //测量库中的数据周期列表
//            List<string> ls = new List<string>();
//            //测量库中选中要加入成果库的数据周期
//            int cycdirnetCyc = 0;
//            //测量库中选中的数据对象
//            cycdirnet cycdirnetTarget = new cycdirnet();
//            //成果周期选定的序号
//            int index = 0;
//            while (true)
//            {
//                string xmname = "数据测试";
//                string pointname = "Z7-4";
//                Console.WriteLine("成果库中有的周期有：");
//                cgCycList = CgCycdirnetDataList(xmname, pointname);
//                Console.WriteLine("成果库中的数据周期有");
//                if (cgCycList.Count == 0)
//                    Console.WriteLine("目前成果库中{0}没有数据", pointname);
//                Console.WriteLine("目前成果库中{0}存在的周期有{1}", pointname, string.Join(",", cgCycList));
                
//                while (true && cgCycList.Count > 0)
//                {
//                    Console.WriteLine("请输入成果库周期");
//                    cyc = Convert.ToInt32(Console.ReadLine());
//                    if (!cgCycList.Contains(cyc.ToString()))
//                    {
//                        Console.WriteLine("只能选择成果库中已有的周期，请重新输入：");
//                        continue;
//                    }
//                    index = cgCycList.IndexOf(cyc.ToString());
//                    break;
//                }
                
//                //获取成果库中该周期的数据
//                cycdirnetModel = CgCycdirnet(xmname,pointname,cyc);
                
                 
//            var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("0001-1-1"));
//                int cont = 0;
                
//                //如果选定周期等于0则列出所有数据并选择选择一条数据加入成果库
//                if (cyc == 0)
//                {
//                    CycdirnetDataTable(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("9999-1-1"), out ls, out cycdirnetCyc);



//                    //插入第1周期的数据
//                    cycdirnetTarget = Cycdirnet(xmname, pointname, cycdirnetCyc);

//                    CgCycdirnetAdd(null, cycdirnetTarget);
                 
                 



//                }
                
//                //如果选择定的周期不是最后一个周期则按照它的周期的时间和它下一个周期的时间筛选可用的周期
//                if (index < cgCycList.Count && index > 1)
//                {
//                    cycdirnetModelEx = CgCycdirnet(xmname,pointname,cyc-1);
//                    cycdirnetModelNext = CgCycdirnet(xmname, pointname,cyc+1);

//                    if (index < cgCycList.Count - 1)
//                    {
                        

//                        CycdirnetDataTable(xmname, pointname, cycdirnetModelEx.Time, cycdirnetModelNext.Time, out ls, out cycdirnetCyc);


                       
//                    }
//                    else if (index == 1)
//                    {
//                        CycdirnetDataTable(xmname, pointname,Convert.ToDateTime("0001-1-1"), cycdirnetModelNext.Time, out ls, out cycdirnetCyc);
//                    }
//                    else
//                    {
//                        CycdirnetDataTable(xmname, pointname, cycdirnetModelEx.Time, Convert.ToDateTime("9999-1-1"), out ls, out cycdirnetCyc);

//                    }
                  
                  
                    
                    
//                    //重新计算本周期的变化量覆盖成果库中原有的周期,重新计算下一个周期的变化量更新
                    

//                    //while (true)
//                    //{
//                    //    if (index < cgCycList.Count - 1)
//                    //    Console.WriteLine(string.Format("选择测量库第_周期数据替换成果库第{0}周期的数据中",cyc));
//                    //    else
//                    //    Console.WriteLine(string.Format("选择测量库第_周期数据添加到成果库第{0}周期的数据中", cyc+1));
//                    //    cycdirnetCyc = Convert.ToInt32(Console.ReadLine());
//                    //    if (!ls.Contains(cycdirnetCyc.ToString()))
//                    //    {
//                    //        Console.WriteLine("只能选择以上列表列出的周期，请重新输入：");
//                    //        continue;
//                    //    }
//                    //    break;
//                    //}

//                    cycdirnetTarget = Cycdirnet(xmname, pointname, cycdirnetCyc);
//                    //获取上一周期的对象
                    
//                    if (index < cgCycList.Count - 1)
//                    {
//                        cycdirnetTarget.CYC = cyc;
//                        CgCycdirnetAdd(cycdirnetModelEx, cycdirnetTarget);
//                        CgCycdirnetAdd(cycdirnetTarget,cycdirnetModelNext);
                        
//                    }
//                    else
//                    {
//                        cycdirnetTarget.CYC = cyc;
//                        CgCycdirnetAdd(cycdirnetModelEx, cycdirnetTarget);
//                    }

//                }
//                Console.WriteLine("输入非t键执行否则返回上一层\n");
//                if (Console.ReadKey().KeyChar == 't') break;

//            }


//        }
//        //获取测量库指定周期的数据对象
//        public cycdirnet Cycdirnet(string xmname, string pointname, int cyc)
//        {
//            string mssg = "";
//            var processCycdirnetModelGetModel = new ProcessCycdirnetBLL.ProcessCycdirnetModelGetModel(xmname, pointname, cyc);
//            if (!cycdirnetBLL.ProcessCycdirnetModel(processCycdirnetModelGetModel, out mssg))
//            {


//                //return;
//            }
//            return processCycdirnetModelGetModel.model;
//        }
//        //获取成果库指定周期的数据对象
//        public cycdirnet CgCycdirnet(string xmname,string pointname,int cyc)
//        {
//            string mssg = "";
//            var processCycdirnetModelGetModel = new ProcessCycdirnetCgBLL.ProcessCycdirnetModelGetModel(xmname, pointname, cyc);
//            if (!cycdirnetCgBLL.ProcessCycdirnetModel(processCycdirnetModelGetModel, out mssg))
//            {


//                //return;
//            }
//            return processCycdirnetModelGetModel.model;
//        }

//        public List<string> CgCycdirnetDataList(string xmname, string pointname)
//        {
//            string mssg = "";
//            var processCgResultDataCycLoadModel = new ProcessCgResultDataBLL.ProcessCgResultDataCycLoadModel(xmname, pointname);
//            if (!ProcessCgResultDataBLL.ProcessCgResultDataCycLoad(processCgResultDataCycLoadModel, out mssg))
//            {

//                //return;
//            }
//            return processCgResultDataCycLoadModel.ls;
//        }


//        //获取时间范围内的测量库中的数据表
//        public DataTable CycdirnetDataTable(string xmname, string pointname, DateTime startTime, DateTime endTime, out List<string> ls, out int cycdirnetCyc)
//        {
//            string mssg = "";
//            ls = new List<string>();
//            var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointname, Convert.ToDateTime(startTime), Convert.ToDateTime(endTime));
//            if (!resultDataBLL.ProcessResultDataSearch(processResultData, out mssg))
//            {
//                Console.WriteLine("获取{0}测量库中的数据失败", pointname);
                
//            }
//            ProcessPrintMssg.Print("获取符合要求的周期和时间有");
//            DataView dv = new DataView(processResultData.dt);
//            foreach (DataRowView drv in dv)
//            {
//                Console.Write(string.Format("周期:{0}\t时间:{1}\n", drv["cyc"].ToString(), drv["time"].ToString()));
//                ls.Add(drv["cyc"].ToString());
//            }
//            while (true)
//            {
//                Console.WriteLine("选择第_周期加入成果库中");
//                cycdirnetCyc = Convert.ToInt32(Console.ReadLine());
//                if (!ls.Contains(cycdirnetCyc.ToString()))
//                {
//                    Console.WriteLine("只能选择以上列表列出的周期，请重新输入：");
//                    continue;
//                }
//                break;
//            }
//            return processResultData.dt;
//        }
//        //
//        public void CgCycdirnetAdd(cycdirnet basecyc, cycdirnet targetcyc)
//        {
//            string mssg = "";
//            cycdirnetCgBLL.cycdirnetThisCalculation(basecyc, targetcyc);
//            cycdirnetCgBLL.ProcessCgCycdirnetAdd(targetcyc, out mssg);
//            ProcessPrintMssg.Print(mssg);
//        }

        


//        //成果数据添加
//        public void TestCgCycdirnetAdd()
//        {
//            //成果周期列表
//            List<string> cgCycList = new List<string>();
//            //测量周期列表
//            List<string> cycList = new List<string>();
//            //成果库选定周期前周期的数据对象
//            cycdirnet cycdirnetModelEx = new cycdirnet();
//            //成果库选定周期的数据对象
//            cycdirnet cycdirnetModel = new cycdirnet();
//            //成果库选定周期的后一个周期对象
//            cycdirnet cycdirnetModelNext = new cycdirnet();
//            //成果库选定的周期
//            int cyc = 0;
//            //测量库数据表
//            DataTable dt = null;
//            //测量库中的数据周期列表
//            List<string> ls = new List<string>();
//            //测量库中选中要加入成果库的数据周期
//            int cycdirnetCyc = 0;
//            //测量库中选中的数据对象
//            cycdirnet cycdirnetTarget = new cycdirnet();
//            //成果周期选定的序号
//            int index = 0;
//            while (true)
//            {
//                string xmname = "数据测试";
//                string pointname = "Z7-4";
//                Console.WriteLine("成果库中有的周期有：");
//                cgCycList = CgCycdirnetDataList(xmname, pointname);
//                Console.WriteLine("成果库中的数据周期有");
//                if (cgCycList.Count == 0)
//                    Console.WriteLine("目前成果库中{0}没有数据", pointname);
//                Console.WriteLine("目前成果库中{0}存在的周期有{1}", pointname, string.Join(",", cgCycList));

//                if (cgCycList.Count == 0) cyc = 1; else cyc =Convert.ToInt32(cgCycList[cgCycList.Count-1])+1;

//                //获取成果库中该周期的数据
//                cycdirnetModel = CgCycdirnet(xmname, pointname, cyc);


//                var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("0001-1-1"));
//                int cont = 0;

//                //如果选定周期等于0则列出所有数据并选择选择一条数据加入成果库
//                if (cyc == 1)
//                {
//                    CycdirnetDataTable(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("9999-1-1"), out ls, out cycdirnetCyc);



//                    //插入第1周期的数据
//                    cycdirnetTarget = Cycdirnet(xmname, pointname, cycdirnetCyc);

//                    CgCycdirnetAdd(null, cycdirnetTarget);





//                }

//                //如果选择定的周期不是最后一个周期则按照它的周期的时间和它下一个周期的时间筛选可用的周期
//                else
//                {

//                    cycdirnetModelEx = CgCycdirnet(xmname, pointname, cyc - 1);
//                    CycdirnetDataTable(xmname, pointname, cycdirnetModelEx.Time, Convert.ToDateTime("9999-1-1"), out ls, out cycdirnetCyc);


//                    //重新计算本周期的变化量覆盖成果库中原有的周期,重新计算下一个周期的变化量更新
//                    cycdirnetTarget = Cycdirnet(xmname, pointname, cycdirnetCyc);
//                    //获取上一周期的对象

//                    cycdirnetTarget.CYC = cyc;
//                    CgCycdirnetAdd(cycdirnetModelEx, cycdirnetTarget);
                   
//                }
//                Console.WriteLine("输入非t键执行否则返回上一层\n");
//                if (Console.ReadKey().KeyChar == 't') break;

//            }


//        }

//        //成果数据更新
//        public void TestCgCycdirnetUpdate()
//        {

//            //成果周期列表
//            List<string> cgCycList = new List<string>();
//            //测量周期列表
//            List<string> cycList = new List<string>();
//            //成果库选定周期前周期的数据对象
//            cycdirnet cycdirnetModelEx = new cycdirnet();
//            //成果库选定周期的数据对象
//            cycdirnet cycdirnetModel = new cycdirnet();
//            //成果库选定周期的后一个周期对象
//            cycdirnet cycdirnetModelNext = new cycdirnet();
//            //成果库选定的周期
//            int cyc = 0;
//            //测量库数据表
//            DataTable dt = null;
//            //测量库中的数据周期列表
//            List<string> ls = new List<string>();
//            //测量库中选中要加入成果库的数据周期
//            int cycdirnetCyc = 0;
//            //测量库中选中的数据对象
//            cycdirnet cycdirnetTarget = new cycdirnet();
//            //成果周期选定的序号
//            int index = 0;
//            while (true)
//            {
//                string xmname = "数据测试";
//                string pointname = "Z7-4";
//                Console.WriteLine("成果库中有的周期有：");
//                cgCycList = CgCycdirnetDataList(xmname, pointname);
//                Console.WriteLine("成果库中的数据周期有");
//                if (cgCycList.Count == 0)
//                    Console.WriteLine("目前成果库中{0}没有数据", pointname);
//                Console.WriteLine("目前成果库中{0}存在的周期有{1}", pointname, string.Join(",", cgCycList));

//                while (true && cgCycList.Count > 0)
//                {
//                    Console.WriteLine("请输入成果库周期");
//                    cyc = Convert.ToInt32(Console.ReadLine());
//                    if (!cgCycList.Contains(cyc.ToString()))
//                    {
//                        Console.WriteLine("只能选择成果库中已有的周期，请重新输入：");
//                        continue;
//                    }
//                    index = cgCycList.IndexOf(cyc.ToString());
//                    break;
//                }

//                //获取成果库中该周期的数据
//                cycdirnetModel = CgCycdirnet(xmname, pointname, cyc);


//                var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("0001-1-1"));
//                int cont = 0;

               

               
//                cycdirnetModelEx = CgCycdirnet(xmname, pointname, cyc - 1);
//                cycdirnetModelNext = CgCycdirnet(xmname, pointname, cyc + 1);

//                    if (index < cgCycList.Count - 1&&index>0)
//                    {


//                        CycdirnetDataTable(xmname, pointname, cycdirnetModelEx.Time, cycdirnetModelNext.Time, out ls, out cycdirnetCyc);



//                    }
//                    else if (index == 0)
//                    {
//                        CycdirnetDataTable(xmname, pointname, Convert.ToDateTime("0001-1-1"), cycdirnetModelNext.Time, out ls, out cycdirnetCyc);
//                    }
//                    else
//                    {
//                        CycdirnetDataTable(xmname, pointname, cycdirnetModelEx.Time, Convert.ToDateTime("9999-1-1"), out ls, out cycdirnetCyc);

//                    }

//                    //重新计算本周期的变化量覆盖成果库中原有的周期,重新计算下一个周期的变化量更新

//                    cycdirnetTarget = Cycdirnet(xmname, pointname, cycdirnetCyc);
//                    //获取上一周期的对象

//                    if (index < cgCycList.Count - 1)
//                    {
//                        cycdirnetTarget.CYC = cyc;
//                        CgCycdirnetAdd(cycdirnetModelEx, cycdirnetTarget);
//                        CgCycdirnetAdd(cycdirnetTarget, cycdirnetModelNext);

//                    }
//                    else
//                    {
//                        cycdirnetTarget.CYC = cyc;
//                        CgCycdirnetAdd(cycdirnetModelEx, cycdirnetTarget);
//                    }
//                    Console.WriteLine("输入非t键执行否则返回上一层\n");
//                    if (Console.ReadKey().KeyChar == 't') break;

//                }
                

            
    
//        }
//        //public ProcessCycdirnetBLL cycdirnetBLL = new ProcessCycdirnetBLL();
//        public void TestProcessProcessCgCycdirnetAdd()
//        {
//            string xmname = "数据测试";
//            string pointname = "Z7-4";
//            int cyc = 3;
//            cycdirnetBLL.ProcessCgCycdirnetAdd(xmname,Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointname, cyc);
//        }
//        public void TestProcessProcessCgCycdirnetUpdate()
//        {
//            string xmname = "数据测试";
//            string pointname = "Z7-4";
//            Console.WriteLine("输入成果库周期");
//            int cyc = Convert.ToInt32(Console.ReadLine());
//            Console.WriteLine("输入测量库周期");
//            int cycdirnet = Convert.ToInt32(Console.ReadLine());
//            cycdirnetBLL.ProcessCgCycdirnetUpdate(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointname, cyc, cycdirnet);
//        }
//        public void TestProcessCycdirnetDataTableLoad()
//        {
//            string xmname = "数据测试";
//            string pointname = "Z7-4";
//            cycdirnetBLL.ProcessCycdirnetDataTableLoad(xmname,pointname,0);
//        }
        
//        public void TestProcessCgPointNameResultDataLoad()
//        {
//            string xmname = "数据测试";
//            string pointname = "Z7-4";
//            var cgPointNameResultDataLoadModel = new ProcessCycdirnetCgBLL.CgPointNameResultDataLoadModel(xmname,pointname);
//            cycdirnetCgBLL.ProcessCgPointNameResultDataLoad(cgPointNameResultDataLoadModel,out mssg);
//            ProcessPrintMssg.Print(mssg);
//        }



    }
}
