﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnetUnitTest.LINQTest
{
    public class TestLinq
    {
        public void main()
        {
            TestConsoleFormat();
        }
        public void TestConsoleFormat()
        {
            int[] intary = {1,2,3,4,5,6,7 };

            foreach (int num in intary)
            {
                Console.Write("{0,1} ", num);
            }
            Console.ReadLine();
            //Console.Write("{0,1}");
        }
    }
}
