﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.#传感器名称#;
using NFnet_BLL.DataProcess.#SensorName#.Sensor;

namespace NFnetUnitTest.TestDataDisplay
{
    public class Test#SensorName#ResultData
    {
        public Process#SensorName#ResultDataMaxTime process#SensorName#ResultDataMaxTime = new Process#SensorName#ResultDataMaxTime();
        public Process#SensorName#ResultDataRqcxConditionCreate process#SensorName#ResultDataRqcxConditionCreate = new Process#SensorName#ResultDataRqcxConditionCreate();
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public Process#SensorName#Chart process#SensorName#Chart = new Process#SensorName#Chart();
        public string mssg = "";
        public void main()
        {
            //Test#SensorName#ResultDataMaxTime();
            //Test#SensorName#ResultDataRqcxConditionCreate();
            TestProcessfill#SensorName#DbFill();
        }
        public void Test#SensorName#ResultDataMaxTime()
        {
            process#SensorName#ResultDataMaxTime.#SensorName#ResultDataMaxTime(29,out mssg );
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#ResultDataRqcxConditionCreate()
        {
            process#SensorName#ResultDataRqcxConditionCreate.#SensorName#ResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", 29, new DateTime());
        }

        public void TestProcessfill#SensorName#DbFill()
        {
            var sqlmodel = process#SensorName#ResultDataRqcxConditionCreate.#SensorName#ResultDataRqcxConditionCreate("2018-7-24", "2018-7-25", NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", 29, new DateTime());
            var model = new NFnet_BLL.DisplayDataProcess.FillSensorDbFillCondition("S01",sqlmodel.sql,29);
            processResultDataBLL.Processfill#SensorName#DbFill(model);
            var series = process#SensorName#Chart.Serializestr#SensorName#(model.sql,29,"'S01'",out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        
    }
}
