﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.#传感器文件名#;

namespace NFnetUnitTest.TestDataDisplay
{
    public class Test#SensorName#PointAlarmValue
    {
        public Process#SensorName#PointAlarmModel process#SensorName#PointAlarmModel = new Process#SensorName#PointAlarmModel();
        public Process#SensorName#PointAlarmRecordsCount process#SensorName#PointAlarmRecordsCount = new Process#SensorName#PointAlarmRecordsCount();
        public Process#SensorName#PointAlarmValueAdd process#SensorName#PointAlarmValueAdd = new Process#SensorName#PointAlarmValueAdd();
        public Process#SensorName#PointAlarmValueDel process#SensorName#PointAlarmValueDel = new Process#SensorName#PointAlarmValueDel();
        public Process#SensorName#PointAlarmValueEdit process#SensorName#PointAlarmValueEdit = new Process#SensorName#PointAlarmValueEdit();
        public Process#SensorName#PointAlarmValueLoad process#SensorName#PointAlarmValueLoad = new Process#SensorName#PointAlarmValueLoad();
        public Process#SensorName#PointAlarmValueMutilEdit process#SensorName#PointAlarmValueMutilEdit = new Process#SensorName#PointAlarmValueMutilEdit();
        public #SensorName#.Model.strainpointalarmvalue model = new #SensorName#.Model.strainpointalarmvalue { point_name="s1", #firstAlarmName# = "一级预警", #secondAlarmName#="二级预警", #thirdAlarmName#="三级预警", remark="应力1号点", xmno = 29 };
        public string mssg = "";
        public void main()
        {
            Test#SensorName#PointAlarmAdd();
            //Test#SensorName#PointAlarmEdit();
            //Test#SensorName#PointAlarmDel();
            Test#SensorName#PointAlarmLoad();
            Test#SensorName#PointAlarmModel();
            Test#SensorName#PointAlarmValueMutilEdit();
        }
        public void Test#SensorName#PointAlarmAdd()
        {
            process#SensorName#PointAlarmValueAdd.PointAlarmValueAdd(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#PointAlarmEdit()
        {
            model.thirdAlarmName = "一级预警";
            process#SensorName#PointAlarmValueEdit.PointAlarmValueEdit(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#PointAlarmDel()
        {
            process#SensorName#PointAlarmValueDel.PointAlarmValueDel(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#PointAlarmLoad()
        {
            process#SensorName#PointAlarmValueLoad.PointAlarmValueLoad(29," 1=1 ","point_name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#PointAlarmModel()
        {
            process#SensorName#PointAlarmModel.#SensorName#PointAlarmModel(29,"s1",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#PointAlarmValueMutilEdit()
        {
            process#SensorName#PointAlarmValueMutilEdit.PointAlarmValueMutilEdit(model,"s1",out mssg);
        }
    }
}
