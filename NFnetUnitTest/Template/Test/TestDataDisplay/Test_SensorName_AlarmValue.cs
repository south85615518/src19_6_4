﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.#传感器文件名#;

namespace NFnetUnitTest.TestDataDisplay
{
    public class Test#SensorName#AlarmValue
    {
        public Process#SensorName#AlarmAdd process#SensorName#AlarmAdd = new Process#SensorName#AlarmAdd();
        public Process#SensorName#AlarmDel process#SensorName#AlarmDel = new Process#SensorName#AlarmDel();
        public Process#SensorName#AlarmEdit process#SensorName#AlarmEdit = new Process#SensorName#AlarmEdit();
        public Process#SensorName#AlarmLoad process#SensorName#AlarmLoad = new Process#SensorName#AlarmLoad();
        public Process#SensorName#AlarmValueModel Process#SensorName#AlarmValueModel = new Process#SensorName#AlarmValueModel();
        public Process#SensorName#AlarmValues process#SensorName#AlarmValues = new Process#SensorName#AlarmValues();
        public #SensorName#.Model.strainalarmvalue model = new #SensorName#.Model.strainalarmvalue { name="一级预警", #dP#=10, xmno =29 };
        public string mssg = "";
        public void main()
        {
            Test#SensorName#AlarmValueAdd();
            //Test#SensorName#AlarmValueUpdate();
            //Test#SensorName#AlarmValueDel();
            Test#SensorName#AlarmValueTableLoad();
            Test#SensorName#AlarmValueModelLoad();
            Test#SensorName#AlarmValueNames();
        }
        public void Test#SensorName#AlarmValueAdd()
        {
            process#SensorName#AlarmAdd.AlarmAdd(model,out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#AlarmValueUpdate()
        {
            model.strain = 15;
            process#SensorName#AlarmEdit.AlarmEdit(model, out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#AlarmValueDel()
        {
            process#SensorName#AlarmDel.AlarmDel(29,"一级预警", out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#AlarmValueTableLoad()
        {
            process#SensorName#AlarmLoad.AlarmLoad(29,"name",1,20,"asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#AlarmValueModelLoad()
        {
            Process#SensorName#AlarmValueModel.#SensorName#AlarmValueModel("一级预警",29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void Test#SensorName#AlarmValueNames()
        {
            process#SensorName#AlarmValues.AlarmValues(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
