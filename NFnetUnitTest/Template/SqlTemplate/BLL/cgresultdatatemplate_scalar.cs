﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace #SensorName#.BLL
{
    public partial class #SensorName#.Model.#ResultDataTableName#
    {
        public bool CgMaxTime(string project_name, Model.gtsensortype datatype, string point_name, out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.CgMaxTime(project_name, datatype, point_name, out maxtime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据点名{3}的最大日期{1}成功", project_name, maxtime, datatype, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最大日期失败", project_name, datatype, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最大日期出错，错误信息:" + ex.Message, project_name, datatype, point_name);
                return false;
            }
        }
        public bool CgMinTime(string project_name, Model.gtsensortype datatype, string point_name, out DateTime Mintime, out string mssg)
        {
            Mintime = new DateTime();
            try
            {
                if (dal.CgMinTime(project_name, datatype, point_name, out Mintime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据点名{3}的最小日期{1}成功", project_name, Mintime, datatype, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最小日期失败", project_name, datatype, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最小日期出错，错误信息:" + ex.Message, project_name, datatype, point_name);
                return false;
            }
        }

        public bool CgPointNameDateTimeListGet(string xmname, string pointname, #SensorName#.Model.gtsensortype datatype, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.CgPointNameDateTimeListGet(xmname, pointname, datatype, out ls))
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表记录数{3}成功", xmname, pointname, datatype, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表失败", xmname, pointname, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表出错,错误信息:" + ex.Message, xmname, pointname, datatype);
                return false;
            }


        }

        public bool AddCgData(string xmname, string point_name, #SensorName#.Model.gtsensortype datatype, DateTime starttime, DateTime endtime,out string mssg)
        {
            mssg = "";
            int rows = 0;
            try
            {
                if (dal.AddCgData(xmname, point_name, datatype, starttime, endtime, out rows))
                {
                    mssg = string.Format("添加项目{0}{1}点名{2}从{3}-{4}的数据{5}条成功", xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime, rows);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目{0}{1}点名{2}从{3}-{4}的数据失败", xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目{0}{1}点名{2}从{3}-{4}的数据出错,错误信息:" + ex.Message, xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                return false;
            }
        }
        /// <summary>
        /// 删除成果数据
        /// </summary>
        public bool DeleteCg(string xmname, string point_name, #SensorName#.Model.gtsensortype datatype, DateTime starttime, DateTime endtime,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.DeleteCg(xmname, point_name, datatype, starttime, endtime))
                {
                    mssg = string.Format("删除项目{0}{1}点名{2}从{3}-{4}的数据成功", xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype),point_name, starttime, endtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}{1}点名{2}从{3}-{4}的数据失败", xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype),point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}{1}点名{2}从{3}-{4}的数据出错,错误信息:" + ex.Message, xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                return false;
            }
        }
        public bool DeleteCgTmp(string xmname, #SensorName#.Model.gtsensortype datatype,out string mssg)
        {
            try
            {
                if (dal.DeleteCgTmp(xmname, datatype))
                {
                    mssg = string.Format("删除项目{0}{3}成果临时表的数据成功", xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}{3}成果临时表的数据失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}{3}成果临时表的数据出错,错误信息：" + ex.Message, xmname);
                return false;
            }
            
        }
        public bool CgSingleScalarResultdataTableLoad(int startPageIndex, int pageSize, string project_name, string pointname, string sord, Model.gtsensortype datatype, DateTime startTime, DateTime endTime, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.CgSingleScalarResultdataTableLoad(startPageIndex, pageSize, project_name, pointname, sord, datatype, startTime, endTime, out  dt))
                {
                    mssg = string.Format("{0}成果数据表加载成功!", project_name);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}成果数据表加载失败!", project_name);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}成果数据表加载出错，出错信息" + ex.Message, project_name);
                return false;
            }
        }

        public bool CgScalarResultTableRowsCount(string project_name, string pointname, string sord, Model.gtsensortype datatype, DateTime startTime, DateTime endTime, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.CgScalarResultTableRowsCount(project_name, pointname, sord, datatype, startTime, endTime, out totalCont))
                {
                    mssg = string.Format("{0}成果数据表记录数加载成功!", project_name);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}成果数据表记录数加载成功!", project_name);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}成果数据表加载出错，出错信息" + ex.Message, project_name);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgScalarGetModelList(string project_name, out List<#SensorName#.Model.#SensorName#.Model.#ResultDataTableName#> lt,out string mssg)
        {
            lt = new List<Model.#SensorName#.Model.#ResultDataTableName#>();
            try
            {
                if (dal.CgScalarGetModelList(project_name, out lt))
                {
                    mssg = string.Format("获取项目{0}成果临时表中的结果数据成功!", project_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}成果临时表中的结果数据失败!", project_name);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}成果临时表中的结果数据出错!错误信息:" + ex.Message, project_name);
                return false;
            }
        }

    }
}
