﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace #SensorName#.BLL
{
    public partial class #ResultDataTableName#
    {
        public bool AddSurveyData(string xmname, string point_name, DateTime starttime, DateTime endtime, #SensorName#.Model.gtsensortype datatype, out string mssg)
        {
            try
            {
                if (dal.AddData(xmname, point_name, starttime, endtime, datatype))
                {
                    mssg = string.Format("添加项目{0}{1}点名{2}从{3}到{4}之间的数据到编辑库成功", xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目{0}{1}点名{2}从{3}到{4}之间的数据到编辑库失败", xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目{0}{1}点名{2}从{3}到{4}之间的数据到编辑库出错,错误信息:" + ex.Message, xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                return false;
            }
        }

        public bool SurveyPointTimeDataAdd(string xmname, #SensorName#.Model.gtsensortype datatype, string pointname, DateTime time, DateTime importtime, out string mssg)
        {
            try
            {
                if (dal.SurveyPointTimeDataAdd(xmname, pointname, datatype, time, importtime))
                {
                    mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据成功", xmname, datatype, pointname, time, importtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据失败", xmname, datatype, pointname, time, importtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据出错,错误信息:" + ex.Message, xmname, datatype, pointname, time, importtime);
                return false;
            }
        }

        public bool UpdataSurveyData(string taskname, string pointname, DateTime time, #SensorName#.Model.gtsensortype datatype, string vSet_name, string vLink_name, DateTime srcdatetime, out string mssg)
        {
            try
            {
                if (dal.UpdataSurveyData(taskname, pointname, time, datatype, vSet_name, vLink_name, srcdatetime))
                {
                    mssg = string.Format("设置项目{0}点{1}日期{2}的{3}{4}日期{2}的值成功", taskname, pointname, time, vSet_name, vLink_name, srcdatetime);
                    return true;
                }
                else
                {
                    mssg = string.Format("设置项目{0}点{1}日期{2}{3}{4}日期{2}的值失败", taskname, pointname, time, vSet_name, vLink_name, srcdatetime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("设置项目{0}点{1}日期{2}{3}为{4}日期{2}的值出错,错误信息:" + ex.Message, taskname, pointname, time, vSet_name, vLink_name, srcdatetime);
                return false;
            }
        }
        public bool DeleteSurveyData(string project_name, string point_name, #SensorName#.Model.gtsensortype datatype, DateTime starttime, DateTime endtime, out string mssg)
        {
            try
            {
                if (dal.DeleteSurveyData(project_name, point_name, datatype, starttime, endtime))
                {
                    mssg = string.Format("删除项目编辑库{0}{1}点名{2}从{3}到{4}的数据成功", project_name, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编辑库{0}{1}点名{2}从{3}到{4}的数据失败", project_name, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编辑库{0}{1}点名{2}从{3}到{4}的数据出错,错误信息：" + ex.Message, project_name, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
                return false;
            }
        }
        public bool PointNameSurveyDateTimeListGet(string xmname, string pointname, #SensorName#.Model.gtsensortype datatype, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.PointNameSurveyDateTimeListGet(xmname, pointname, datatype, out ls))
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}编辑库的时间列表记录数{3}成功", xmname, pointname, datatype, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}编辑库的时间列表失败", xmname, pointname, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}点{2}编辑库的时间列表出错,错误信息:" + ex.Message, xmname, pointname, datatype);
                return false;
            }


        }
    }
}
