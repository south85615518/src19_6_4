﻿/**  版本信息模板在安装目录下，可自行修改。
* #ResultDataTableName#.cs
*
* 功 能： N/A
* 类 名： #ResultDataTableName#
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 #SensorName# Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace #SensorName#.DAL
{
	/// <summary>
	/// 数据访问类:#ResultDataTableName#
	/// </summary>
	public partial class #ResultDataTableName#
	{
        public database db = new database();
		public #ResultDataTableName#()
		{}
		#region  BasicMethod


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(#SensorName#.Model.#ResultDataTableName# model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("replace into #ResultDataTableName#(");
			strSql.Append("#pointName#,#initVal#,#scalarVal#,#this_val#,#ac_val#,#monitorTime#,xmno)");
			strSql.Append(" values (");
			strSql.Append("@#pointName#,@#initVal#,@#scalarVal#,@#this_val#,@#ac_val#,@#monitorTime#,@xmno)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@#pointName#", OdbcType.VarChar,20),
					new OdbcParameter("@#initVal#", OdbcType.Double),
					new OdbcParameter("@#scalarVal#", OdbcType.Double),
					new OdbcParameter("@#this_val#", OdbcType.Double),
					new OdbcParameter("@#ac_val#", OdbcType.Double),
					new OdbcParameter("@#monitorTime#", OdbcType.DateTime),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };
			parameters[0].Value = model.#pointName#;
			parameters[1].Value = model.#initVal#;
			parameters[2].Value = model.#scalarVal#;
			parameters[3].Value = model.#this_val#;
			parameters[4].Value = model.#ac_val#;
			parameters[5].Value = model.#monitorTime#;
            parameters[6].Value = model.xmno;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(#SensorName#.Model.#ResultDataTableName# model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update #ResultDataTableName# set ");
			strSql.Append("#pointName#=@#pointName#,");
			strSql.Append("#initVal#=@#initVal#,");
			strSql.Append("#scalarVal#=@#scalarVal#,");
			strSql.Append("#this_val#=@#this_val#,");
			strSql.Append("#ac_val#=@#ac_val#,");
			strSql.Append("#monitorTime#=@#monitorTime#");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@#pointName#", OdbcType.VarChar,20),
					new OdbcParameter("@#initVal#", OdbcType.Double),
					new OdbcParameter("@#scalarVal#", OdbcType.Double),
					new OdbcParameter("@#this_val#", OdbcType.Double),
					new OdbcParameter("@#ac_val#", OdbcType.Double),
					new OdbcParameter("@#monitorTime#", OdbcType.DateTime)};
			parameters[0].Value = model.#pointName#;
			parameters[1].Value = model.#initVal#;
			parameters[2].Value = model.#scalarVal#;
			parameters[3].Value = model.#this_val#;
			parameters[4].Value = model.#ac_val#;
			parameters[5].Value = model.#monitorTime#;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
      
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,DateTime Time,string POINT_NAME)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from #ResultDataTableName# ");
			strSql.Append(" where xmno=@xmno and #monitorTime#=@#monitorTime# and #pointName#=@#pointName# ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@#monitorTime#", OdbcType.DateTime),
					new OdbcParameter("@#pointName#", OdbcType.VarChar,120)			};
			parameters[0].Value = xmno;
			parameters[1].Value = Time;
			parameters[2].Value = POINT_NAME;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
       
        public bool DeleteTmp(int xmno)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from #ResultDataTableName#_tmp ");
            strSql.Append(" where xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int)
							};
            parameters[0].Value = xmno;
            

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ResultdataTableLoad(int startPageIndex, int pageSize, int xmno,string pointname ,string sord ,out DataTable dt)
        {
            dt = new DataTable();
            
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select #pointName#,#initVal#,#scalarVal#,#this_val#,#ac_val#,#monitorTime#  from #ResultDataTableName#  where   xmno=@xmno    and    {0}      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr,sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = (startPageIndex - 1) * pageSize;
            parameters[2].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(),parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;
           
        }

        public bool ResultTableRowsCount(int xmno,string pointname ,out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from #ResultDataTableName# where xmno='{0}' and {1}",  xmno,searchstr);
            OdbcConnection conn = db.GetStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }

        public bool ResultDataReportPrint(string sql,int xmno,out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            dt = querysql.querystanderdb(sql,conn);
            return true;
        }

        

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno,out List<#SensorName#.Model.#ResultDataTableName#> lt )
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select #pointName#,#initVal#,#scalarVal#,#this_val#,#ac_val#,#monitorTime#  from #ResultDataTableName#_tmp ");
            strSql.Append(" where    xmno=@xmno   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int)
							};
            parameters[0].Value = xmno;

            lt = new List<Model.#ResultDataTableName#>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i<ds.Tables[0].Rows.Count)
            {
                lt.Add( DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

    
        
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public #SensorName#.Model.#ResultDataTableName# DataRowToModel(DataRow row)
        {
            #SensorName#.Model.#ResultDataTableName# model = new #SensorName#.Model.#ResultDataTableName#();
            if (row != null)
            {
                if (row["#pointName#"] != null && row["#pointName#"].ToString() != "")
                {
                    model.#pointName# = row["#pointName#"].ToString();
                }
                if (row["#initVal#"] != null && row["#initVal#"].ToString() != "")
                {
                    model.#initVal# = double.Parse(row["#initVal#"].ToString());
                }
                if (row["#scalarVal#"] != null && row["#scalarVal#"].ToString() != "")
                {
                    model.#scalarVal# = double.Parse(row["#scalarVal#"].ToString());
                }
                if (row["#this_val#"] != null && row["#this_val#"].ToString() != "")
                {
                    model.this_val = double.Parse(row["#this_val#"].ToString());
                }
               if (row["#ac_val#"] != null && row["#ac_val#"].ToString() != "")
                {
                    model.ac_val = double.Parse(row["#ac_val#"].ToString());
                }
                if (row["xmno"] != null && row["xmno"] != "")
                {
                    model.xmno = int.Parse(row["xmno"].toString());
                }
                if (row["#monitorTime#"] != null && row["#monitorTime#"].ToString() != "")
                {
                    model.#monitorTime# = Convert.ToDateTime(row["#monitorTime#"].ToString());
                }
               
               
            }
            return model;
        }
    
        public bool GetModel(int xmno, string pointname, DateTime dt, out #SensorName#.Model.#ResultDataTableName# model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select #pointName#,#initVal#,#scalarVal#,#this_val#,#ac_val#,#monitorTime#  from #ResultDataTableName# ");
            strSql.Append(" where    xmno=@xmno   and   #pointName#=@#pointName#    and    #monitorTime# = @#monitorTime#   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@#pointName#", OdbcType.VarChar,120),
                    new OdbcParameter("@#monitorTime#", OdbcType.DateTime)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

        public bool MaxTime(int xmno,out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(#monitorTime#) from #ResultDataTableName#  where   xmno = @xmno");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.VarChar,100)
            };
            paramters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool #SensorName#PointLoadDAL(int xmno, out List<string> ls)
        {
           
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select DISTINCT(POINT_NAME) from fmos_pointalarmvalue where xmno="+xmno;
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }

        public bool PointNewestDateTimeGet(int xmno,string pointname,out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(#monitorTime#)  from #ResultDataTableName#    where    xmno=@xmno   and #pointName# = @#pointName#  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
                    new OdbcParameter("@#pointName#", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
            if (obj == null) { dt = Convert.ToDateTime("0001-1-1"); return false; }
            dt = Convert.ToDateTime(obj); return true;
            
        }

         public bool XmStateTable(int startPageIndex, int pageSize, int xmno,string xmname,string unitname ,string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder(256);
            //strSql.AppendFormat(@"SELECT '"+xmno+"' as xmno,'" + unitname + "' as unitname ,max(time) as lastrectime,iF((SELECT IFNULL(MAX(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from #ResultDataTableName# where xmno ='" + xmno + @"' ) > DATE_ADD(SYSDATE(),INTERVAL -IFNULL((select  hour*60+minute from #SensorName#datauploadinterval  where  xmno='"+xmno+@"'),24*60) minute),'true','false' ) as state ,(select count(1) from pointcheck  where xmno=" + xmno + " and type='表面位移') as alarmcount from #ResultDataTableName# where taskname='" + xmno + @"'");
            dt = querysql.querystanderdb(strSql.ToString(), conn);
            if (dt != null) return true;
            return false;
        }

         public bool Is#ResultDataTableName#DataExist(int xmno,string pointname,DateTime dt)
         {
             OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
             StringBuilder strsql = new StringBuilder();
             strsql.Append("select    count(1)     from     #ResultDataTableName#    where     xmno=@xmno     and     point_name =@point_name    and        time=@time  ");
             OdbcParameter[] parameters ={ 
                 new OdbcParameter("@xmno",OdbcType.Int),
                 new OdbcParameter("@point_name",OdbcType.VarChar,100),
                 new OdbcParameter("@time",OdbcType.DateTime)
             };
             parameters[0].Value = xmno;
             parameters[1].Value = pointname;
             parameters[2].Value = dt;
             object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
             return Convert.ToInt32(obj) > 0 ? true : false;
         }

		#endregion  BasicMethod
	}
}

