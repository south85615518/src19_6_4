﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Data;

namespace #SensorName#.DAL
{
    public partial class #ResultDataTableName#
    {
        public bool CgPointNameDateTimeListGet(string xmname, string pointname,  out List<string> ls)
        {

            OdbcConnection conn = db.GetCgStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  #ResultDataTableName#    where         project_name='{0}'    and   datatype='{1}'   and     {2}     order by time  asc  ", xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            //OdbcParameter[] parameters = {
            //        new OdbcParameter("@xmname", OdbcType.VarChar,200),
            //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
            //                };
            //parameters[0].Value = xmname;
            //parameters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            string str = strSql.ToString();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }
        public bool CgMaxTime(string project_name, string point_name, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(project_name);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from #ResultDataTableName#  where   project_name = @project_name   and   datatype=@datatype  and point_name=@point_name  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@project_name",OdbcType.VarChar,100),
                new OdbcParameter("@datatype",OdbcType.VarChar,100),
                new OdbcParameter("@point_name",OdbcType.VarChar,100)
            };
            paramters[0].Value = project_name;
            paramters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            paramters[2].Value = point_name;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool CgMinTime(string project_name, string point_name, out DateTime MinTime)
        {
            MinTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(project_name);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Min(time) from #ResultDataTableName#  where   project_name = @project_name   and   datatype=@datatype  and point_name=@point_name  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@project_name",OdbcType.VarChar,100),
                new OdbcParameter("@datatype",OdbcType.VarChar,100),
                new OdbcParameter("@point_name",OdbcType.VarChar,100)
            };
            paramters[0].Value = project_name;
            paramters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            paramters[2].Value = point_name;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            MinTime = Convert.ToDateTime(obj);
            return true;
        }



        public bool AddCgData(string xmname,string point_name,DateTime starttime, DateTime endtime ,out int rows)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection Conn = db.GetSurveyStanderConn(xmname);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            OdbcConnection CgConn = db.GetStanderCgConn(xmname);
            strSql.AppendFormat("REPLACE into {0}.#ResultDataTableName#  select * from {1}.#ResultDataTableName#_data   where   project_name=@project_name     and  datatype=@datatype   and   time  between @starttime  and  @endtime  ", CgConn.Database, Conn.Database);
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@project_name", OdbcType.VarChar,100),
                                             new OdbcParameter("@datatype", OdbcType.VarChar,100),
                                             new OdbcParameter("@starttime", OdbcType.DateTime),
                                             new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = starttime;
            parameters[3].Value = endtime;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除成果数据
        /// </summary>
        public bool DeleteCg(string xmname, string pointname,  DateTime starttime, DateTime endtime)
        {
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from  #ResultDataTableName#  where   project_name=@project_name  and  datatype=@datatype    and    {0}    and     time    between    @starttime      and    @endtime  ", searchstr);
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@project_name", OdbcType.VarChar,100),
                                             new OdbcParameter("@datatype", OdbcType.VarChar,100),
                                             new OdbcParameter("@starttime", OdbcType.DateTime),
                                             new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = starttime;
            parameters[3].Value = endtime;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteCgTmp(string xmname)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmname);
            strSql.Append("delete  from  #ResultDataTableName#_tmp ");
            strSql.Append(" where project_name=@project_name  and  datatype = @datatype ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,120),
                    new OdbcParameter("@datatype", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CgSingleScalarResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord,  DateTime startTime, DateTime endTime, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetCgStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue as single_oregion_scalarvalue,first_this_scalarvalue as single_this_scalarvalue,first_ac_scalarvalue as single_ac_scalarvalue,cyc,time  from #ResultDataTableName#  where   project_name=@xmname     and    datatype=@datatype    and      time>@starttime      and       time < @endtime       and    {0}      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),


                    new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime),

                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startTime;
            parameters[3].Value = endTime;
            parameters[4].Value = (startPageIndex - 1) * pageSize;
            parameters[5].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool CgScalarResultTableRowsCount(string project_name, string pointname, string sord, DateTime startTime, DateTime endTime, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from  #ResultDataTableName# where project_name='{0}' and datatype= '{2}'  and  time>'{3}' and  time < '{4}'     and   {1}", project_name, searchstr, datatype, startTime, endTime);
            OdbcConnection conn = db.GetCgStanderConn(project_name);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgScalarGetModelList(string project_name, out List<#SensorName#.Model.#ResultDataTableName#> lt)
        {
            OdbcConnection conn = db.GetCgStanderConn(project_name);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select project_name,point_name,senorno,datatype,valuetype,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,first_oregion_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_oregion_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc,time  from #ResultDataTableName#_tmp  ");
            strSql.Append(" where    project_name=@project_name   order by  datatype ,point_name   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100)
							};
            parameters[0].Value = project_name;
            lt = new List<Model.#ResultDataTableName#>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(ScalarDataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


    }
}
