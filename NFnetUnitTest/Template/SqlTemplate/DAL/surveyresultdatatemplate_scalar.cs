﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;

namespace #SensorName#.DAL
{
    public partial class #ResultDataTableName#
    {
        public bool AddData(string xmname, string point_name, DateTime starttime, DateTime endtime, #SensorName#.Model.gtsensortype datatype)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            strSql.Append("REPLACE into #ResultDataTableName#_data select * from #ResultDataTableName#   where   project_name=@project_name  and  datatype=@datatype    and  point_name=@point_name  and  time  between   @starttime   and   @endtime   ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@project_name", OdbcType.VarChar,100),
                                             new OdbcParameter("@datatype", OdbcType.VarChar,100),
                                             new OdbcParameter("@point_name", OdbcType.VarChar,100),
                                             new OdbcParameter("@starttime", OdbcType.DateTime), 
                                             new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = point_name;
            parameters[3].Value = starttime;
            parameters[4].Value = endtime;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SurveyPointTimeDataAdd(string xmname, string pointname, #SensorName#.Model.gtsensortype datatype, DateTime time, DateTime importtime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            strSql.AppendFormat(@"insert into #ResultDataTableName#_data (project_name,point_name,datatype,senorno,first_oregion_scalarvalue,time) select project_name,point_name,datatype,senorno,first_oregion_scalarvalue,time from  #ResultDataTableName#_data where project_name = '" + xmname + "'  and  datatype='{1}'  and   point_name   =   '" + pointname + "'   and     time='{0}'", importtime, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype));

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdataSurveyData(string taskname, string pointname, DateTime time, #SensorName#.Model.gtsensortype datatype, string vSet_name, string vLink_name, DateTime srcdatetime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(taskname);
            strSql.AppendFormat("update #ResultDataTableName#_data set {1} = {1}+({0}-(select {0} from #ResultDataTableName# where project_name=#ResultDataTableName#_#SensorName#.project_name and point_name= #ResultDataTableName#_#SensorName#.point_name and time =     '{5}')), {0}=(select   {0}   from #ResultDataTableName# where project_name=#ResultDataTableName#_#SensorName#.project_name and point_name= #ResultDataTableName#_#SensorName#.point_name and time =    '{5}')  where       project_name= '{2}'    and  point_name  =   '{3}'   and  time =   '{4}'   ", vSet_name, vLink_name, taskname, pointname, time, srcdatetime);
            string str = strSql.ToString();
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteSurveyData(string project_name, string point_name, #SensorName#.Model.gtsensortype datatype, DateTime starttime, DateTime endtime)
        {
            string searchstr = point_name == "" || point_name == null || (point_name.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", point_name);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(project_name);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from #ResultDataTableName#_data ");
            strSql.AppendFormat(" where    project_name=@project_name    and    {0}     and    datatype=@datatype    and time  between     @starttime    and     @endtime ", searchstr);
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100),
					new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = project_name;
            parameters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = starttime;
            parameters[3].Value = endtime;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool PointNameSurveyDateTimeListGet(string xmname, string pointname, #SensorName#.Model.gtsensortype datatype, out List<string> ls)
        {

            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  #ResultDataTableName#_data    where         project_name='{0}'    and   datatype='{1}'   and     {2}     order by time  asc  ", xmname, #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype), pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            //OdbcParameter[] parameters = {
            //        new OdbcParameter("@xmname", OdbcType.VarChar,200),
            //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
            //                };
            //parameters[0].Value = xmname;
            //parameters[1].Value = #SensorName#.DAL.gtsensortype.GTSensorTypeToString(datatype);
            string str = strSql.ToString();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }

    }
}
