﻿/**  版本信息模板在安装目录下，可自行修改。
* #AlarmValueTableName#.cs
*
* 功 能： N/A
* 类 名： #AlarmValueTableName#
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:27   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using System.Collections.Generic;
using SqlHelpers;
namespace #SensorName#.DAL
{
	/// <summary>
	/// 
	/// </summary>
	public partial class #AlarmValueTableName#
	{
        public static database db = new database();
		public #AlarmValueTableName#()
		{}
        #region  BasicMethod
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(#SensorName#.Model.#AlarmValueTableName# model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("insert into #AlarmValueTableName#(");
            strSql.Append("name,#dP#,#dPL#,xmno)");
            strSql.Append(" values (");
            strSql.Append("@name,@#dP#,@#dPL#,@xmno)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@#dP#", OdbcType.Double),
                    new OdbcParameter("@#dPL#", OdbcType.Double),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };
            parameters[0].Value = model.name;
            parameters[1].Value = model.#dP#;
            parameters[2].Value = model.#dPL#;
            parameters[3].Value = model.xmno;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(#SensorName#.Model.#AlarmValueTableName# model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("update #AlarmValueTableName# set ");
            strSql.Append("#dP#=@#dP#,#dPL#=@#dPL#");
            strSql.Append("     where     name=@name     and      xmno=@xmno   ");
            OdbcParameter[] parameters = {
                    
					new OdbcParameter("@#dP#", OdbcType.Double),
                    new OdbcParameter("@#dPL#", OdbcType.Double),
                    new OdbcParameter("@name", OdbcType.VarChar,100),
                    new OdbcParameter("@xmno", OdbcType.Int)
					};
            parameters[0].Value = model.#dP#;
            parameters[1].Value = model.#dPL#;
            parameters[2].Value = model.name;
            parameters[3].Value = model.xmno;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int xmno, string alarmname)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from #AlarmValueTableName# ");
            strSql.Append("     where     name=@name    and    xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = alarmname;
            parameters[1].Value = xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                PointAlarmValueDelCasc(alarmname, xmno);
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(string alarmname, int xmno)
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update #AlarmValueTableName#pointalarmvalue set #firstAlarmName#='' where xmno=@xmno and #firstAlarmName#=@#firstAlarmName#");
            //二级预警
            ls.Add("update #AlarmValueTableName#pointalarmvalue set #secondAlarmName#='' where xmno=@xmno and #secondAlarmName#=@#secondAlarmName#");
            //三级预警
            ls.Add("update #AlarmValueTableName#pointalarmvalue set #thirdAlarmName#='' where xmno=@xmno and #thirdAlarmName#=@#thirdAlarmName#");
            List<string> cascSql = ls;
            OdbcConnection conn = db.GetStanderConn(xmno);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@xmno", "'" + xmno + "'");
                temp = temp.Replace("@#firstAlarmName#", "'" + alarmname + "'");
                temp = temp.Replace("@#secondAlarmName#", "'" + alarmname + "'");
                temp = temp.Replace("@#thirdAlarmName#", "'" + alarmname + "'");

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(temp, conn);


            }
            return true;
        }




        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select name,#dP#,#dPL#  from  #AlarmValueTableName#    where      xmno =  '" + xmno + "'    {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(1) from #AlarmValueTableName# where xmno = '" + xmno + "'";
            if (searchstring != null&&searchstring.Trim() != "1=1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
            
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public #SensorName#.Model.#AlarmValueTableName# GetModel(string name, int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select name,#dP#,#dPL#,xmno from #AlarmValueTableName# ");
            strSql.Append(" where        name=@name       and         xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = name;
            parameters[1].Value = xmno;

            #SensorName#.Model.#AlarmValueTableName# model = new #SensorName#.Model.#AlarmValueTableName#();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno, out string alarmValueNameStr)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select distinct(name) from #AlarmValueTableName# where xmno = " + xmno + "";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            alarmValueNameStr = string.Join(";", lsFormat);
            return true;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public #SensorName#.Model.#AlarmValueTableName# DataRowToModel(DataRow row)
        {
            #SensorName#.Model.#AlarmValueTableName# model = new #SensorName#.Model.#AlarmValueTableName#();
            if (row != null)
            {
                if (row["name"] != null)
                {
                    model.name = row["name"].ToString();
                }
                //model.#dP#=row["#dP#"].ToString();
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["#dP#"] != null && row["#dP#"].ToString() != "")
                {
                    model.#dP# = Convert.ToDouble(Convert.ToDouble(row["#dP#"]).ToString("0.000"));
                }
                if (row["#dPL#"] != null && row["#dPL#"].ToString() != "")
                {
                    model.#dPL# = Convert.ToDouble(Convert.ToDouble(row["#dPL#"]).ToString("0.000"));
                }

            }
            return model;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string name, int xmno, out #SensorName#.Model.#AlarmValueTableName# model)
        {
            model = new Model.#AlarmValueTableName#();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select name,#dP#,#dPL#,xmno from #AlarmValueTableName# ");
            strSql.Append(" where        name=@name       and         xmno=@xmno  ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@name", OdbcType.VarChar,100),
			        new OdbcParameter("@xmno", OdbcType.Int)
                                         };
            parameters[0].Value = name;
            parameters[1].Value = xmno;
            //InclimeterDAL.Model.inclinometer_alarmvalue model=new InclimeterDAL.Model.inclinometer_alarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select alarmname,#dP#,xmno,id ");
            strSql.Append(" FROM #AlarmValueTableName# ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return OdbcSQLHelper.Query(strSql.ToString());
        }
        #endregion  BasicMethod
	}
}

