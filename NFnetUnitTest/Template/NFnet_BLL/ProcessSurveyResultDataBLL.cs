﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    public partial class ProcessResultDataBLL
    {

        public bool AddSurveyData(AddSurveyDataModel model, out string mssg)
        {
            return bll.AddSurveyData(model.xmname, model.point_name, model.starttime, model.endtime, model.datatype, out mssg);
        }
        public class AddSurveyDataModel
        {
            public string xmname { get; set; }
            public string point_name { get; set; }
            public DateTime starttime { get; set; }
            public DateTime endtime { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public AddSurveyDataModel(string xmname, string point_name, DateTime starttime, DateTime endtime, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.point_name = point_name;
                this.starttime = starttime;
                this.endtime = endtime;
                this.datatype = datatype;
            }
        }

        public bool UpdataSurveyData(UpdataSurveyDataModel model, out string mssg)
        {
            return bll.UpdataSurveyData(model.xmname, model.point_name, model.dt, model.datatype, model.vSet_name, model.vLink_name, model.srcdatetime, out mssg);
        }
        public class UpdataSurveyDataModel
        {
            public string xmname { get; set; }
            public string point_name { get; set; }
            public DateTime dt { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string vSet_name { get; set; }
            public string vLink_name { get; set; }
            public DateTime srcdatetime { get; set; }
            public UpdataSurveyDataModel(string xmname, string point_name, DateTime dt, data.Model.gtsensortype datatype, string vSet_name, string vLink_name, DateTime srcdatetime)
            {
                this.xmname = xmname;
                this.point_name = point_name;
                this.dt = dt;
                this.datatype = datatype;
                this.vSet_name = vSet_name;
                this.vLink_name = vLink_name;
                this.srcdatetime = srcdatetime;
            }
        }
        public bool ProcessSurveyPointTimeDataAdd(ProcessSurveyPointTimeDataAddModel model, out string mssg)
        {
            return bll.SurveyPointTimeDataAdd(model.xmname, model.datatype, model.pointname, model.time, model.importtime, out mssg);
        }
        public class ProcessSurveyPointTimeDataAddModel
        {
            public string xmname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string pointname { get; set; }
            public DateTime time { get; set; }
            public DateTime importtime { get; set; }
            public ProcessSurveyPointTimeDataAddModel(string xmname, data.Model.gtsensortype datatype, string pointname, DateTime time, DateTime importtime)
            {
                this.xmname = xmname;
                this.datatype = datatype;
                this.pointname = pointname;
                this.time = time;
                this.importtime = importtime;
            }

        }
        public bool DeleteSurveyData(DeleteModel model, out string mssg)
        {
            return bll.DeleteSurveyData(model.xmname, model.point_name, model.datatype, model.starttime, model.endtime, out mssg);

        }
       
        public bool ProcessSurveyResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.PointNameSurveyDateTimeListGet(model.xmname, model.pointname, model.datatype, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
    }
}