﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using System.Threading;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.#传感器文件名#
{
    public partial class ProcessResultDataAlarmBLL
    {
        
        public bool cgalarmmain()
        {
            alarmcont =0;
            
            return Cg#ResultDataTableName#ModelList();

        }
        public bool Cg#ResultDataTableName#ModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessResultDataBLL.ProcessResultDataAlarmModelListModel(xmname);
            List<global::#SensorName#.Model.#ResultDataTableName#> modellist;
            if (resultBLL.ProcessCgResultDataAlarmModelList(processResultDataAlarmModelListModel, out modellist, out mssg))
            {
                Cg#ResultDataTableName#PointAlarm(modellist, "表面位移--#传感器名称#--超限预警");
                return true;
            }
            return false;
        }
       
        public List<string> CgResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            ExceptionLog.TotalSationPointCheckVedioWrite("************开始预警**********");
            cgalarmmain();
            return this.alarmInfoList;
        }

        public bool Cg#ResultDataTableName#PointAlarm(List<#SensorName#.Model.#ResultDataTableName#> lc, string title)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format("==========={0}===========", DateTime.Now));
            ls.Add(string.Format("==========={0}===========", xmname));
            ls.Add(string.Format("==========={0}===========", title));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            //ExceptionLog.ExceptionWriteCheck(ls);
            ExceptionLog.TotalSationPointAlarmVedioWrite("获取到项目" + xmname + "预警临时表中记录数" + lc.Count + "条");
            foreach (#SensorName#.Model.#ResultDataTableName# cl in lc)
            {
                #SensorName#.Model.#PointNameTableName# pointvalue = TestPointAlarmValue(cl.POINT_NAME);
                List<#SensorName#.Model.#AlarmValueTableName#> alarmList = TestAlarmValueList(pointvalue);
                TestCgPointAlarmfilterInformation(alarmList, cl);

            }
            ExceptionLog.TotalSationPointAlarmVedioWrite("项目" + xmname + "本次预警共产生新预警记录" + alarmcont + "条");
            ExceptionLog.TotalSationPointAlarmVedioWrite("************预警结束**********");
            return true;
        }
        public void TestCgPointAlarmfilterInformation(List<#SensorName#.Model.#AlarmValueTableName#> levelalarmvalue, #SensorName#.Model.#ResultDataTableName# resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (pointAlarmBLL.ProcessCgPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                alarmcont++;
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }


    }
}