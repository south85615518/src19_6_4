﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.#传感器文件名#
{
    public class ProcessResultDataAlarmBLL
    {

        #region  变量声明
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public static string mssg = "";
        public static string timeJson = "";
        public static ProcessResultDataBLL resultBLL = new ProcessResultDataBLL();
        #endregion
        #region 成员变量
        public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        #endregion
        
        public ProcessResultDataAlarmBLL()
        {
        }
        public ProcessResultDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = xmname;
            this.xmno = xmno;
        }
        
        public bool main()
        {

            return Get#SensorName#ResultDataModelList();

        }
        public bool Get#SensorName#ResultDataModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessResultDataBLL.ProcessResultDataAlarmModelListModel(xmname);
            List<global::#SensorName#.Model.#ResultDataTableName#> modellist = new List<global::#SensorName#.Model.#ResultDataTableName#>();
            if (resultBLL.ProcessResultDataAlarmModelList(processResultDataAlarmModelListModel, out modellist, out mssg))
            {
                #SensorName#ResultDataPointAlarm(modellist);
                return true;
            }
            return false;

        }
        public #SensorName#.Model.#PointAlarmTableName#     GetPointAlarmValue(string pointName)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointName);
            #SensorName#.Model.#PointAlarmTableName# model = null;
            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                return processPointAlarmModelGetModel.model;

            }
            return null;
        }
        public List<global::#SensorName#.Model.#AlarmValueTableName#> GetAlarmValueList(#SensorName#.Model.#PointAlarmTableName# pointalarm)
        {
            List<global::#SensorName#.Model.#AlarmValueTableName#> alarmvalueList = new List<global::#SensorName#.Model.#AlarmValueTableName#>();
            //一级
            var processAlarmModelGetByNameModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.firstAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("一级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.secondAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("二级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.thirdAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("三级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            return alarmvalueList;
        }
        public void GetPointAlarmfilterInformation(List<global::#SensorName#.Model.#AlarmValueTableName#> levelalarmvalue, global::#SensorName#.Model.#ResultDataTableName# resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (pointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));

                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public bool #SensorName#ResultDataPointAlarm(List<cycdirnet> lc)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format("===================================={0}=================================", DateTime.Now));
            ls.Add(string.Format("===================================={0}=================================", xmname));
            ls.Add(string.Format("===================================={0}=================================", "表面位移--全站仪--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (cycdirnet cl in lc)
            {
                if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}#SensorName#", xmname))) return false;
                #SensorName#.Model.#PointAlarmTableName# pointvalue = GetPointAlarmValue(cl.POINT_NAME);
                List<global::#SensorName#.Model.#AlarmValueTableName#> alarmList = GetAlarmValueList(pointvalue);
                GetPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> ResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        public void #SensorName#ResultDataPointAlarm(global::#SensorName#.Model.#ResultDataTableName# resultmodel)
        {

            #SensorName#.Model.#PointAlarmTableName# pointvalue = GetPointAlarmValue(resultmodel.POINT_NAME);
            if (pointvalue == null) return;
            List<global::#SensorName#.Model.#AlarmValueTableName#> alarmList = GetAlarmValueList(pointvalue);
            GetPointAlarmfilterInformation(alarmList, resultmodel);
        }




    }
}