﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.#SensorName#
{
    public partial class ProcessResultDataBLL
    {
        
        public bool ProcesspointcgdataResultDataMaxTime(#SensorName#SensorMaxTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.CgMaxTime(model.xmname, model.datatype,model.pointname ,out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }

        public bool ProcesspointcgdataResultDataMinTime(#SensorName#SensorMinTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.CgMinTime(model.xmname, model.datatype, model.pointname,out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            return false;


        }
        public bool ProcessCgResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.CgPointNameDateTimeListGet(model.xmname, model.pointname, model.datatype, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessCgDataImport(AddSurveyDataModel model, out string mssg)
        {
            return bll.AddCgData(model.xmname, model.point_name, model.datatype, model.starttime, model.endtime,out mssg); 
        }
        
         //<summary>
         //删除成果数据
         //</summary>
        public bool DeleteCg(DeleteModel model, out string mssg)
        {
            return bll.DeleteCg(model.xmname, model.point_name, model.datatype, model.starttime, model.endtime, out mssg);

        }
        public bool DeleteCgTmp(DeleteTmpModel model, out string mssg)
        {
            return bll.DeleteCgTmp(model.xmname, model.datatype, out mssg);

        }
        public bool CgSingleScalarResultdataTableLoad(#SensorName#ResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.CgSingleScalarResultdataTableLoad(model.pageIndex, model.rows, model.xmname, model.pointname, model.sord, model.datatype, model.starttime, model.endtime, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
        }

        public bool CgScalarResultdataTableRecordsCount(#SensorName#ResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.CgScalarResultTableRowsCount(model.xmname, model.pointname, model.sord, model.datatype, model.startTime, model.endTime, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }

        public bool ProcessCgScalarResultDataAlarmModelList(ProcessScalarResultDataAlarmModelListModel model, out List<global::#SensorName#.Model.#ResultDataTableName#> modellist, out string mssg)
        {
            modellist = new List<global::#SensorName#.Model.#ResultDataTableName#>();
            if (bll.CgScalarGetModelList(model.xmname, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        


    }
}