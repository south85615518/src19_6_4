﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;


namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#Alarm
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool Alarm(global::#SensorName#.Model.#PointAlarmTableName# model, out string mssg)
        {
            return pointAlarmBLL.ProcessAlarm(model,out mssg); 
        }
    }
}
