﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器文件名#;

namespace NFnet_Interface.DisplayDataProcess.#SensorName#
{
    public class Process#SensorName#PointLoad
    {
        public Process#SensorName#BLL process#SensorName#BLL = new Process#SensorName#BLL();
        public List<string> #SensorName#PointLoadBLL(int xmno,out string mssg)
        {
            var process#SensorName#PointLoadModel = new NFnet_BLL.DisplayDataProcess.#SensorName#.Process#SensorName#BLL.Process#SensorName#PointLoadModel(xmno);
            if (process#SensorName#BLL.Process#SensorName#PointLoad(process#SensorName#PointLoadModel, out mssg))
            {
                return process#SensorName#PointLoadModel.pointlist;
            }
            return new List<string>();
        }
    }
}
