﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess#传感器文件名#;


namespace NFnet_Interface.DisplayDataProcess#传感器文件名#
{
    public class ProcessSurveyDataUpdate
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyDataUpdate(string xmname, string pointname, DateTime dt,data.Model.gtsensortype datatype,string vSet_name, string vLink_name, DateTime srcdatatime,out string mssg)
        {
            var model = new ProcessResultDataBLL.UpdataSurveyDataModel(xmname, pointname, dt, datatype, vSet_name, vLink_name, srcdatatime);
            return processResultDataBLL.UpdataSurveyData(model,out mssg);
        }
    }
}
