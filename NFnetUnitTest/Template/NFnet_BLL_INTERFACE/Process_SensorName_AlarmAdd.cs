﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;
//using NFnet_BLL.DisplayDataProcess.#传感器名称#;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#AlarmAdd
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public bool AlarmAdd(global::#SensorName#.Model.#AlarmValueTableName# model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessAlarmValueBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.Process#AlarmValueTableName#Add(model, out mssg);
        }
    }
}
