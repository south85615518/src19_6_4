﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess#传感器文件名#;

namespace NFnet_Interface.DisplayDataProcess#传感器文件名#
{
    public class ProcessSurveyTimeDataDelete
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyTimeDataDelete( string xmname, string pointname, DateTime starttime, DateTime endtime,data.Model.gtsensortype datatype,out string mssg)
        {
            var model = new ProcessResultDataBLL.DeleteModel(xmname,pointname,starttime,endtime,datatype);
            return processResultDataBLL.DeleteSurveyData(model, out mssg);
        }
    }
}
