﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;
//using NFnet_BLL.DisplayDataProcess.#传感器名称#;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#AlarmDel
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public bool AlarmDel(int xmno, string name,out string mssg)
        {
            var processAlarmDelModel = new ProcessAlarmValueBLL.Process#AlarmValueTableName#DeleteModel( xmno,name);
            return alarmBLL.ProcessAlarmDel(processAlarmDelModel, out mssg);
        }
    }
}
