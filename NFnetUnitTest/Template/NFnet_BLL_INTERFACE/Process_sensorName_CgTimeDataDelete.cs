﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess#传感器文件名#;

namespace NFnet_Interface.DisplayDataProcess#传感器文件名#
{
    public class ProcessCgTimeDataDelete
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool CgTimeDataDelete( string xmname, string pointname,DateTime starttime, DateTime endtime, data.Model.gtsensortype datatype ,out string mssg)
        {
            var model = new ProcessResultDataBLL.DeleteModel( xmname, pointname,starttime, endtime,datatype);
            return processResultDataBLL.DeleteCg(model, out mssg);
        }
    }
}
