﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;
//using NFnet_BLL.DisplayDataProcess.#传感器名称#;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#AlarmValues
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public string AlarmValues(int xmno, out string mssg)
        {
            var alarmvaluename = new ProcessAlarmValueBLL.ProcessAlarmValueNameModel(xmno, "");
            if (alarmBLL.ProcessAlarmValueName(alarmvaluename, out mssg))
            {
                return alarmvaluename.alarmValueNameStr;
            }
             return mssg;          
        }
    }
}
