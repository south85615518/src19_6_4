﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.#传感器文件名#
{
    public class Process#SensorName#ResultDataRqcxConditionCreate
    {
        public Process#SensorName#ResultDataBLL resultDataBLL = new Process#SensorName#ResultDataBLL();
        public ResultDataRqcxConditionCreateCondition #SensorName#ResultDataRqcxConditionCreate( QueryType QT,string range ,int xmno, DateTime maxTime)
        {
            List<string> ls = new List<string>();

            var process#SensorName#ResultDataRqcxConditionCreateModel = new ResultDataRqcxConditionCreateCondition(QT, range, xmno, maxTime);
            resultDataBLL.Process#SensorName#ResultDataRqcxConditionCreate(process#SensorName#ResultDataRqcxConditionCreateModel);
            return process#SensorName#ResultDataRqcxConditionCreateModel;
        }
        
        
    }
}
