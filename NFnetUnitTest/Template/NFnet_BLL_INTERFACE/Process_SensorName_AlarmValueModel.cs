﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;
//using NFnet_BLL.DisplayDataProcess.#传感器名称#BKG;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#AlarmValueModel
    {
        public ProcessAlarmValueBLL process#SensorName#AlarmValueBLL = new ProcessAlarmValueBLL();
        public global::#SensorName#.Model.#AlarmValueTableName# #SensorName#AlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var process#SensorName#AlarmValueModelGetModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (process#SensorName#AlarmValueBLL.ProcessAlarmModelGetByName(process#SensorName#AlarmValueModelGetModel, out mssg))
            {
                return process#SensorName#AlarmValueModelGetModel.model;
            }
            return new global::#SensorName#.Model.#AlarmValueTableName#();
        }
    }
}
