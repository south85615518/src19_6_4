﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.#传感器文件名#;

namespace NFnet_Interface.DisplayDataProcess.#传感器文件名#
{
    public class Process#SensorName#Chart
    {
        public Process#SensorName#ChartBLL process#SensorName#ChartBLL = new Process#SensorName#ChartBLL();
        public List<serie> Serializestr#SensorName#(object sql, object xmno, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmno,pointname,0);

            if (Process#SensorName#ChartBLL.ProcessSerializestr#SensorName#(processChartCondition, out mssg))
                return processChartCondition.series;
            return new List<serie>();


        }
    }
}
