﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;
//using NFnet_BLL.DisplayDataProcess.#传感器名称#;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#AlarmRecordsCount
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public int AlarmRecordsCount(int xmno, out string mssg)
        {
            var processAlarmRecordsCountModel = new ProcessAlarmValueBLL.ProcessAlarmRecordsCountModel(xmno);



            if (alarmBLL.ProcessAlarmRecordsCount(processAlarmRecordsCountModel, out mssg))
            {
                return Convert.ToInt32(processAlarmRecordsCountModel.totalCont);

            }
            else
            {
                //计算结果数据数量出错信息反馈
                return 0;
            }
        }
    }
}
