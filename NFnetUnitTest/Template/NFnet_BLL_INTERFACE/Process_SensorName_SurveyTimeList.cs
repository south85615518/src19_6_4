﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess#传感器文件名#;


namespace NFnet_Interface.DisplayDataProcess#传感器文件名#
{
    public class ProcessSurveyTimeList
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<string> SurveyTimeList(string xmname,string point_name,data.Model.gtsensortype datatype,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessResultDataTimeListLoadModel(xmname,point_name,datatype);
            if (processResultDataBLL.ProcessSurveyResultDataTimeListLoad(model, out mssg))
                return model.ls;
            return null;
        }
    }
}
