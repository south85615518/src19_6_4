﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;
//using NFnet_BLL.DisplayDataProcess.#传感器名称#;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#AlarmEdit
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public bool AlarmEdit(global::#SensorName#.Model.#AlarmValueTableName# model, out string mssg)
        {
            //var processAlarmEditModel = new ProcessAlarmValueBLL.(model, xmname);
            //return alarmBLL.ProcessAlarmEdit(processAlarmEditModel, out mssg);
            return alarmBLL.Process#AlarmValueTableName#Edit(model,out mssg);
        }
    }
}
