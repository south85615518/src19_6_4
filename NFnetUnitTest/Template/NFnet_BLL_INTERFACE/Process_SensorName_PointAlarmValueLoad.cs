﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;
//using NFnet_BLL.DisplayDataProcess.#传感器名称#;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#PointAlarmValueLoad
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public DataTable PointAlarmValueLoad(string xmname,int xmno,string searchstring,string colName,int pageIndex,int rows,string sord,out string mssg)
        {
            var processPointAlarmLoadModel = new ProcessPointAlarmBLL.ProcessAlarmLoadModel(xmname, xmno, searchstring, colName, pageIndex, rows, sord);
            if (pointAlarmBLL.ProcessPointLoad(processPointAlarmLoadModel, out mssg))
            {
                return processPointAlarmLoadModel.dt;
            }
            else
            {
                //加载结果数据出错错误信息反馈
                return new DataTable();
            }
        }
    }
}
