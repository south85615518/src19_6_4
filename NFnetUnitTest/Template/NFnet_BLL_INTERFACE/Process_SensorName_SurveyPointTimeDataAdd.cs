﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess#传感器文件名#;


namespace NFnet_Interface.DisplayDataProcess#传感器文件名#
{
    public class ProcessSurveyPointTimeDataAdd
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyPointTimeDataAdd(string xmname, string pointname,data.Model .gtsensortype datatype ,DateTime dt, DateTime importdt,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessSurveyPointTimeDataAddModel(xmname, datatype, pointname, dt, importdt);
            return processResultDataBLL.ProcessSurveyPointTimeDataAdd(model,out mssg);
        }
    }
}
