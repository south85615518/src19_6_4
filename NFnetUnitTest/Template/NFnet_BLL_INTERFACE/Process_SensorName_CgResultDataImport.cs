﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess#传感器文件名#;


namespace NFnet_Interface.DisplayDataProcess#传感器文件名#
{
    public class ProcessCgResultDataImport
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool CgResultDataImport(string xmname, string point_name, data.Model.gtsensortype datatype, DateTime starttime, DateTime endtime, out string mssg)
        {
            var model = new ProcessResultDataBLL.AddSurveyDataModel(xmname, point_name, starttime, endtime, datatype);
            return processResultDataBLL.ProcessCgDataImport(model,out mssg);
        }
    }
}
