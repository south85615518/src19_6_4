﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#PointAlarmModel
    {
        public ProcessPointAlarmBLL process#SensorName#PointAlarmBLL = new ProcessPointAlarmBLL();
        public global::#SensorName#.Model.#PointAlarmTableName# #SensorName#PointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var process#SensorName#PointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (process#SensorName#PointAlarmBLL.ProcessPointAlarmModelGet(process#SensorName#PointAlarmModel, out mssg))
            {
                return process#SensorName#PointAlarmModel.model;
            }
            return new global::#SensorName#.Model.#PointAlarmTableName#();
        }
    }
}
