﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.#传感器文件名#;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.#传感器文件名#
{
    public class Process#SensorName#ResultDataPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string #SensorName#ResultDataPlotLineAlarm(string pointnamestr, string xmname,int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<#SensorName#PlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                Process#SensorName#ResultDataAlarmBLL process#SensorName#ResultDataAlarmBLL = new Process#SensorName#ResultDataAlarmBLL(xmname, xmno);
                List<#SensorName#PlotlineAlarmModel> lmm = new List<#SensorName#PlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    var pointvalue = process#SensorName#ResultDataAlarmBLL.TestPointAlarmValue(pointname);
                    var alarmList = process#SensorName#ResultDataAlarmBLL.TestAlarmValueList(pointvalue);
                    #SensorName#PlotlineAlarmModel model = new #SensorName#PlotlineAlarmModel
                    {
                        pointname = pointname,
                        #firstAlarmName# = alarmList[0],
                        #secondAlarmName# = alarmList[1],
                        #thirdAlarmName# = alarmList[2]
                    };
                    lmm.Add(model);
                    break;
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移预警参数加载出错,错误信息:"+ex.Message);
                return null;
            }
        }
    }
}
