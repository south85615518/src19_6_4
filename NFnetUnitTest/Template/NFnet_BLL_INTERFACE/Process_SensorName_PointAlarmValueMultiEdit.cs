﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.#传感器名称#;

namespace NFnet_Interface.DisplayDataProcess.#传感器名称#
{
    public class Process#SensorName#PointAlarmValueMutilEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueMutilEdit(global::#SensorName#.Model.#PointAlarmTableName# model,string pointstr ,out string mssg)
        {
            var processAlarmMultiUpdateModel = new ProcessPointAlarmBLL.ProcessAlarmMultiUpdateModel(pointstr, model);
            pointAlarmBLL.ProcessAlarmMultiUpdate(processAlarmMultiUpdateModel, out mssg);
        }
    }
}
