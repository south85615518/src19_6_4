﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess#传感器文件名#;


namespace NFnet_Interface.DisplayDataProcess#传感器文件名#
{
    public class ProcessSurveyDataAdd
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyDataAdd(string xmname, string point_name, DateTime starttime, DateTime endtime, data.Model.gtsensortype datatype, out string mssg)
        {
            var model = new ProcessResultDataBLL.AddSurveyDataModel(xmname, point_name, starttime, endtime, datatype);
            return processResultDataBLL.AddSurveyData(model,out mssg);
        }
    }
}
