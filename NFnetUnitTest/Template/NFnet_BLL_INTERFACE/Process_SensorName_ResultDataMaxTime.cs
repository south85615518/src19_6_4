﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.#传感器文件名#;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.#传感器文件名#
{
   public class Process#SensorName#ResultDataMaxTime
    {

        public Process#SensorName#ResultDataBLL resultDataBLL = new Process#SensorName#ResultDataBLL();
        public  DateTime #SensorName#ResultDataMaxTime(int xmno,out string mssg)
        {
            var process#SensorName#ResultDataMaxTime = new SenorMaxTimeCondition(xmno);
            
            if (!resultDataBLL.Process#SensorName#ResultDataMaxTime(process#SensorName#ResultDataMaxTime, out mssg)) return new DateTime();
            return process#SensorName#ResultDataMaxTime.maxTime;
            

        }
    }
}
