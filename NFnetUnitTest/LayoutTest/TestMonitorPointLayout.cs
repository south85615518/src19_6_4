﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.XmInfo.pub;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.AuthorityAlarmProcess;
using System.Data;
using SqlHelpers;
using System.Data.Odbc;
using NFnet_BLL.DataImport.ProcessFile;
using NFnet_BLL.XmInfo;
namespace NFnetUnitTest.LayoutTest
{

    class TestMonitorPointLayout
    {
        public static ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public static ProcessCgLayoutBLL cgLayoutBLL = new ProcessCgLayoutBLL();
        public static ProcessRoleLayoutBLL roleLayoutBLL = new ProcessRoleLayoutBLL();
        public static ProcessFileBLL fileBLL = new ProcessFileBLL();
        public string mssg = "";
        public static Layout.Model.monitoringpointlayout model = new Layout.Model.monitoringpointlayout
        {
            xmno = 29,//Convert.ToInt32(Console.ReadLine()),
            jclx = "表面位移",
            jcoption = "",
            pointName = ""//Console.ReadLine()
        };
        public int basemapid = 0;
        public void main()
        {
            //TestProcessMonitorPointLayoutColor();
            //TestProcessMonitorPointLayoutAlarmPointCont();
            //TestProcessLayoutHotPots();
            //TestCgProcessMonitorPointLayoutColor();
            //TestProcessMonitorPointExsit();
            //TestProcessalarmsummary();
            //TestBaseMapIdGet();
            //TestProcessLayoutPointAlarmParamInfo();
            //TestProcessLayoutBaseMapDelete();
            //TestProcessFolderUrlGet();
            TestProcessBaseMapFileDel();
        }
        public void TestProcessMonitorPointLayoutColor()
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(model.xmno, model.jclx, model.jcoption, model.pointName, Convert.ToInt32(Console.ReadLine()));
            layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestCgProcessMonitorPointLayoutColor()
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessCgLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(model.xmno, model.jclx, model.jcoption, model.pointName, Convert.ToInt32(Console.ReadLine()));
            cgLayoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }


        public void TestProcessMonitorPointLayoutAlarmPointCont()
        {
            basemapid = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("输入0表示监测员或者管理员,1表示监督员");
            Role role = Console.ReadKey().KeyChar == '1' ? Role.superviseModel : Role.administratrorModel;
            var monitorPointLayoutAlarm = new MonitorPointLayoutAlarmPointCont(model.xmno, basemapid);
            var processMonitorPointLayoutAlarmPointContModel = new ProcessLayoutBLL.RoleMonitorPointLayoutAlarmPointCont(monitorPointLayoutAlarm, role);
            layoutBLL.ProcessMonitorPointLayoutAlarmPointCont(processMonitorPointLayoutAlarmPointContModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestProcessLayoutHotPots()
        {
            Console.WriteLine("输入监测平面图编号：");
            basemapid = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("输入0表示监测员或者管理员,1表示监督员");
            Role role = Console.ReadKey().KeyChar == '1' ? Role.superviseModel : Role.administratrorModel;
            var processLayoutMonitorPointLoadByBaseMapIdModel = new LayoutMonitorPointLoadByBaseMapIdCondition(Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), "表面位移", Convert.ToInt32(basemapid));
            var roleLayoutBLLModel = new ProcessRoleLayoutBLL.ProcessLayoutHotPotsModel(processLayoutMonitorPointLoadByBaseMapIdModel, role);

            roleLayoutBLL.ProcessLayoutHotPots(roleLayoutBLLModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestProcessMonitorPointExsit()
        {
            var processLayoutModel = new ProcessLayoutBLL.ProcessLayoutModel(model.pointName, model.jclx, model.xmno);
            layoutBLL.ProcessLayoutExist(processLayoutModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestProcessalarmsummary()
        {
            //var processalarmsummaryModel = new ProcessLayoutBLL.ProcessalarmsummaryModel(model.xmno);
            //layoutBLL.Processalarmsummary(processalarmsummaryModel,out mssg);
            //ProcessPrintMssg.Print(mssg);
        }

        public void TestBaseMapIdGet()
        {
            var processBaseMapIdGetModel = new ProcessLayoutBLL.ProcessBaseMapIdGetModel(model.xmno, model.jclx, model.pointName);
            layoutBLL.ProcessBaseMapGet(processBaseMapIdGetModel, out mssg);
            ProcessPrintMssg.Print(mssg);


        }
        public void TestProcessLayoutPointAlarmParamInfo()
        {
            string xmname = "数据测试";
            string pointname = "Z7-4";
            var layoutPointAlarmParamInfoModel = new ProcessLayoutBLL.LayoutPointAlarmParamInfoModel(Aspect.IndirectValue.GetXmnoFromXmname(xmname), xmname, pointname);
            layoutBLL.ProcessLayoutPointAlarmParamInfo(layoutPointAlarmParamInfoModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestProcessLayoutBaseMapDelete()
        {

            string xmname = "数据测试";
            string path = "D:/day02/_mvmp/DAY02/项目设置/方案编辑/监测平面图/数据测试/图片.jpg";

            string sql = "select folderUrl from basemap where folderUrl = 'D:/day02/_mvmp/DAY02/项目设置/方案编辑/监测平面图/数据测试/图片.jpg' ";
            database db = new database();
            OdbcConnection conn = db.GetStanderConn(xmname);
            string result = SqlHelpers.querysql.querystanderstr(sql, conn);
            var processLayoutBaseMapDeleteModel = new ProcessLayoutBLL.ProcessLayoutBaseMapDeleteModel(xmname, path);
            layoutBLL.ProcessLayoutBaseMapDelete(processLayoutBaseMapDeleteModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestProcessFolderUrlGet()
        {
            string xmname = "数据测试";
            string path = "D:/day02/_mvmp/DAY02/项目设置/方案编辑/监测平面图/数据测试/图片.jpg";
            var processFolderUrlGetModel = new ProcessLayoutBLL.ProcessFolderUrlGetModel(xmname, path);
            layoutBLL.ProcessFolderUrlGet(processFolderUrlGetModel, out mssg);
            ProcessPrintMssg.Print(mssg + "\n" + processFolderUrlGetModel.folderUrl);
        }
        public void TestProcessBaseMapFileDel()
        {
            string xmname = "数据测试";
            string path = "D:/day02/_mvmp/DAY02/项目设置/方案编辑/监测平面图/数据测试/图片.jpg";
            fileBLL.ProcessBaseMapDelete(xmname,path,out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
