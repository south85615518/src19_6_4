﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;
using NFnet_Interface.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    
    class TestMonitorAlarm
    {
        string mssg = "";
        public ProcessMonitorAlarmBLL processMonitorAlarmBLL = new ProcessMonitorAlarmBLL();
        public ProcessXmMailLastSendTime processXmMailLastSendTime = new ProcessXmMailLastSendTime();
        //监测员预警通知存库
        public AuthorityAlarm.Model.monitoralarm monitorAlarm = new AuthorityAlarm.Model.monitoralarm
        {
            xmno = 23,
            context = "hello",
            time = DateTime.Now,
            confirm = false,
            mail = false,
            unitmember = 4,
            dataType = 1,
            ForwardTime = 0,
            mess = false,
            mid = string.Format("M{0}{1}", 4, DateHelper.DateTimeToString(DateTime.Now))
        };
       
        public void main() {
            //TestMonitorAlarmAddProcess();
            //TestMonitorAlarmUpdateProcess();
            TestMonitorAlarmLastSendTime();
        }
        public void TestMonitorAlarmAddProcess()
        {
            processMonitorAlarmBLL.ProcessMonitorAlarmAdd(monitorAlarm,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMonitorAlarmUpdateProcess()
        {
            monitorAlarm.mid = Console.ReadLine();
            monitorAlarm.mail = true;
            processMonitorAlarmBLL.ProcessMonitorAlarmUpdate(monitorAlarm, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestMonitorAlarmLastSendTime()
        {
            processXmMailLastSendTime.XmMailLastSendTime(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
