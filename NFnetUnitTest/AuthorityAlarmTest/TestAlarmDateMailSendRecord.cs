﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;
using Tool;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    class TestAlarmDateMailSendRecord
    {
        public ProcessAlarmDateMailSendRecordBLL mailSendRecordBLL = new ProcessAlarmDateMailSendRecordBLL();

     
        public void main() {
            ProcessAlarmDateMailSendRecord();
        }
        public void ProcessAlarmDateMailSendRecord()
        {
            int sec = 0;
            string mssg = "";
            DateTime dt = Convert.ToDateTime("2017/08/25");
            AuthorityAlarm.Model.alarmdatemailsendrecord mailsendrecord = new AuthorityAlarm.Model.alarmdatemailsendrecord
            {
                did = string.Format("D{0}", DateHelper.DateTimeToString(dt.AddSeconds(++sec))),
                sdate = dt,
                xmno = 29
            };
            mailSendRecordBLL.ProcessAlarmDateMailSendRecordAdd(mailsendrecord,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
