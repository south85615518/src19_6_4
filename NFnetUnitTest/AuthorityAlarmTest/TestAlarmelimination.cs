﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;
using AuthorityAlarm.Model;
using NFnet_Interface.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    public class TestAlarmelimination
    {
        public ProcessAlarmLiminationBLL processAlarmLiminationBLL = new ProcessAlarmLiminationBLL();
        public alarmelimination alarmeliminationmodel = new alarmelimination { xmno = 106, jclx = "水位", point_name = "p", EliminationTime = DateTime.Now, alarmtime = DateTime.Now.AddDays(-1), EliminationContext = "内容", EliminationMonitorID = "userID"  };
        public ProcessAlarmLiminationTableLoad processAlarmLiminationTableLoad = new ProcessAlarmLiminationTableLoad();
        public ProcessAlarmLiminationTableCount processAlarmLiminationTableCount = new ProcessAlarmLiminationTableCount();
        public static string mssg = "";

        public void main()
        {
            //TestAlarmeliminationAdd();
            //TestAlarmeliminationTableLoad();
            TestAlarmeliminationTableCountLoad();
        }
        public void TestAlarmeliminationAdd()
        {
            processAlarmLiminationBLL.ProcessAlarmLiminationAdd(alarmeliminationmodel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestAlarmeliminationUpdate()
        {
            //processAlarmLiminationBLL.Update();
        }
        public void TestAlarmeliminationTableLoad()
        {
            processAlarmLiminationTableLoad.AlarmLiminationTableLoad("南方测绘广州分公司", "xiexuming", 106, "A", "P", DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1), "A", 1, 20, "xmno", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestAlarmeliminationTableCountLoad()
        {
            processAlarmLiminationTableCount.AlarmLiminationTableCountLoad("南方测绘广州分公司", "xiexuming", 106, "A", "P", DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1), "A", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
