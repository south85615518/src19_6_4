﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    class TestCgAlarmSplitOnDate
    {
        public static ProcessalarmsplitondateBLL cgalarmBLL = new ProcessalarmsplitondateBLL();
        public static string mssg = "";
        AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
        {
            dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
            jclx = "表面位移",
            pointName = "Z7-5",
            xmno = 96,
            time = Convert.ToDateTime("2017/8/2 15:30"),
            adate = DateTime.Now

        };

        public void main()
        {
            TestCgAlarmSplitOnDateAdd();
            //TestCgAlarmSplitOnDateDel();
            //TestCgAlarmOnDateSplit();
            //TestCgDateAlarmOnDateSplit();
            //TestCgAlarmOnDateSplit();
            //TestProcessCgAlarmSplitCont();
            //TestProcessCgAlarmSplitReaded();
            //TestCgAlarmOnDateSplitUnionSendModel();
        }
        public void TestCgAlarmSplitOnDateAdd()
        {
            cgalarmBLL.ProcesscgalarmsplitondateAdd(cgalarm, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        //public void TestCgAlarmSplitOnDateDel()
        //{
        //    var processCgAlarmSplitOnDateDeleteModel = new ProcessCgAlarmSplitOnDateBLL.ProcessCgAlarmSplitOnDateDeleteModel(cgalarm.xmno, "表面位移", cgalarm.pointName, cgalarm.time);
        //    cgalarmBLL.ProcessCgAlarmSplitOnDateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        public void TestCgDateAlarmOnDateSplit()
        {
            var processCgAlarmSplitOnDateModelListModel = new ProcessalarmsplitondateBLL.ProcessDateAlarmSplitOnDateModelListModel(106, Convert.ToDateTime("2018-11-08"),"");
            cgalarmBLL.ProcessalarmsplitondateModelList(processCgAlarmSplitOnDateModelListModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestCgAlarmOnDateSplitUnionSendModel()
        {
            List<Authority.Model.MonitorMember> monitorlist = new List<Authority.Model.MonitorMember>();
            monitorlist.Add(new Authority.Model.MonitorMember{ tel ="13422208426", userNO=23 });
            List<string> datestrlist = new List<string>();
            datestrlist.Add("2018-11-8");
            var model = new ProcessalarmsplitondateBLL.ProcessCgAlarmOnDateSplitUnionSendModel(106, monitorlist, "新白广城际铁路下穿京广高铁施工监测", datestrlist);
            cgalarmBLL.ProcessCgAlarmOnDateSplitUnionSend(model,out mssg);
        }


        //public void TestProcessCgAlarmSplitCont()
        //{
        //    var processcgalarmsplitondatecontModel = new ProcessCgAlarmSplitOnDateBLL.ProcesscgalarmsplitondatecontModel(cgalarm.xmno, "表面位移");
        //    cgalarmBLL.Processcgalarmsplitondatecont(processcgalarmsplitondatecontModel, out  mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}

        //public void TestProcessCgAlarmSplitReaded()
        //{
        //    var processAlarmCgSplitOnDateReadedModel = new ProcessCgAlarmSplitOnDateBLL.ProcessAlarmCgSplitOnDateReadedModel(cgalarm.xmno, Convert.ToDateTime("2017-09-05"));
        //    cgalarmBLL.Processalarmcgsplitondatereaded(processAlarmCgSplitOnDateReadedModel, out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}


        //public void TestCgAlarmOnDateSplit()
        //{
        //    var processCgAlarmSplitOnDateModelListModel = new ProcessCgAlarmSplitOnDateBLL.ProcessCgAlarmSplitOnDateModelListModel(cgalarm.xmno);
        //    cgalarmBLL.ProcessCgAlarmSplitOnDateModelList(processCgAlarmSplitOnDateModelListModel, out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //    var processCgAlarmOnDateSplitModel = new ProcessCgAlarmSplitOnDateBLL.ProcessCgAlarmOnDateSplitModel(processCgAlarmSplitOnDateModelListModel.ls);
        //    cgalarmBLL.ProcessCgAlarmOnDateSplit(processCgAlarmOnDateSplitModel, out mssg);
        //    Console.Write("================日期分包预警统计如下====================\n");
        //    Console.Write("日期\t\t未读\t\t数量\t\t发送次数\n");
        //    foreach (var model in processCgAlarmOnDateSplitModel.modells)
        //    {
        //        Console.Write("{0}\t{1}\t\t{2}\t\t{3}\n", string.Format("{0}-{1}-{2}", Convert.ToDateTime(model.dt).Year, Convert.ToDateTime(model.dt).Month, Convert.ToDateTime(model.dt).Day), model.read + "条", model.acont + "条", model.scont);
        //        Console.Write("{0}\n", model.alarmContext.Replace("<p>", "\n"));
        //    }
        //    Console.ReadLine();

        //    List<string> ldt = new List<string>();
        //    Console.Write("选择日期包输入T结束\n");
        //    char dtch;
        //    while ((dtch = Console.ReadKey().KeyChar) != 't')
        //    {
        //        ldt.Add(processCgAlarmOnDateSplitModel.modells[Convert.ToInt32(dtch.ToString())].dt.ToString());

        //    }
        //    List<string> lsmail = new List<string>();
        //    Console.Write("输入要发送的电子邮箱输入T结束\n");
        //    while ((dtch = Console.ReadKey().KeyChar) != 't')
        //    {
        //        Console.Write("邮箱号:_\n");
        //        lsmail.Add(Console.ReadLine());
        //        Console.Write("输入t结束\n");
        //    }
        //    var processCgAlarmOnDateSplitUnionSendModel = new ProcessCgAlarmSplitOnDateBLL.ProcessCgAlarmOnDateSplitUnionSendModel(29, lsmail, "数据测试", ldt, processCgAlarmOnDateSplitModel.ls);
        //    cgalarmBLL.ProcessCgAlarmOnDateSplitUnionSend(processCgAlarmOnDateSplitUnionSendModel, out mssg);
        //    ProcessPrintMssg.Print(mssg);


        //}
        

    }
}
