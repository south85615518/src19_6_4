﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    class TestPointAlarm
    {
        public string mssg = "";
        public static ProcessCgAlarmBLL cgalarmBLL = new ProcessCgAlarmBLL();
        public void main()
        {
            while (true)
            {

                //TestProcessPointCheckAdd();
                //TestProcessXmHightestAlarm();
                //ProcessXmAlarmLsit();
                //DataDoublePrase();
                //ProcessTestPointAlarmModel();
                ProcessTestPointAlarmModelList();
                Console.Write("输入任意非T键执行\n");
                if (Console.ReadKey().KeyChar == 't') break;
            }

        }
        public void ProcessTestPointAlarmModel()
        {
            string xmname = "数据测试";
            string jclx = "表面位移";
            string pointName = "Z7-5";
            var processPointCheckModelGetModel = new RolePointAlarmCondition(Aspect.IndirectValue.GetXmnoFromXmname(xmname), jclx, pointName);
            cgalarmBLL.ProcessPointAlarmModelGet(processPointCheckModelGetModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessTestPointAlarmModelList()
        {
            var processPointCheckModelListModel = new ProcessCgAlarmBLL.ProcessPointCheckModelListModel(29);
            cgalarmBLL.ProcessPointCheckModelList(processPointCheckModelListModel, out mssg);
            foreach (var check in processPointCheckModelListModel.modelList)
            {
                Console.Write(string.Format("\n{0} {1} {2}级预警 {3}\n", check.type, check.point_name, check.alarm, check.time));

            }
        }
    }
}
