﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;
using AuthorityAlarm.Model;
using NFnet_Interface.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    public class Testsmssendrecord
    {
        public ProcessSmsSendRecordBLL processsmssendrecordBLL = new ProcessSmsSendRecordBLL();
        public smssendrecord smssendrecordmodel = new smssendrecord { xmno = 96,   sendtime = DateTime.Now,  sendcontext = "内容", messtype = "短信发送测试", result = "发送成功", sendto = "谢续明[13422208426]"  };
        public ProcessSmsSendRecordTableLoad processSmsSendRecordTableLoad = new ProcessSmsSendRecordTableLoad();
        public ProcessSmsSendRecordTableCount processSmsSendRecordTableCount = new ProcessSmsSendRecordTableCount();
        public static string mssg = "";

        public void main()
        {
            //TestsmssendrecordAdd();
            //TestsmssendrecordTableLoad();
            TestsmssendrecordTableCountLoad();
        }
        public void TestsmssendrecordAdd()
        {
            processsmssendrecordBLL.ProcessSmsSendRecordAdd(smssendrecordmodel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestsmssendrecordUpdate()
        {
            //processSmsSendRecordBLL.Update();
        }
        public void TestsmssendrecordTableLoad()
        {
            processSmsSendRecordTableLoad.SmsSendRecordTableLoad("南方测绘广州分公司",96,  DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1), "A", 1, 20, "xmno", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestsmssendrecordTableCountLoad()
        {
            processSmsSendRecordTableCount.SmsSendRecordTableCountLoad("南方测绘广州分公司",96, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1), "A", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
