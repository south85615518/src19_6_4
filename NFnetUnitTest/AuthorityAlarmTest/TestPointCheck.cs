﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;
using Tool;
using NFnet_BLL.LoginProcess;
using NFnet_Interface.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    public class TestPointCheck
    {
        public ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public ProcessRoleAlarmBLL roleAlarmBLL = new ProcessRoleAlarmBLL();
        public static int sec = -1;
        public static string mssg = "";
        public static AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
        {
            xmno = 30,
            point_name = "sp2",
            time = Convert.ToDateTime("2017-08-07 08:12:34"),
            type = "表面位移",
            atime = DateTime.Now,
            pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddSeconds(++sec))),
            readed = false
        };
        public void main()
        {
            //while (true)
            //{
            //    TestProcessPointCheckDelete();
            //    //TestProcessPointCheckAdd();
            //    //TestProcessXmHightestAlarm();
            //    //ProcessXmAlarmLsit();
            //    //DataDoublePrase();
            //    //ProcessTestPointCheckModel();
            //    //TestProcessPointCheckUpdate();
            //    Console.Write("输入任意非T键执行\n");
            //    if (Console.ReadKey().KeyChar == 't') break;
            //}
            TestXmAlarmCont();
            TestMaxAlarm();



        }
        public void TestProcessPointCheckAdd()
        {
            Console.Write("预警级别[1,2,3]\n");
            pointCheck.alarm = int.Parse(Console.ReadLine());
            pointCheck.pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddSeconds(++sec)));
            pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestProcessPointCheckUpdate()
        {
            //Console.Write("预警级别[1,2,3]\n");
            pointCheck.alarm = 0;
            pointCheck.pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddSeconds(++sec)));
            pointCheckBLL.ProcessPointCheckUpdate(pointCheck, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestProcessPointCheckDelete()
        {
            //Console.Write("预警级别[1,2,3]\n");
            pointCheck.alarm = 0;
            pointCheck.pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddSeconds(++sec)));
            pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
            ProcessPrintMssg.Print(mssg);
        }


        public void TestProcessXmHightestAlarm()
        {
            var xmHightestAlarmCondition = new XmHightestAlarmCondition(pointCheck.xmno);
            Console.WriteLine("输入0表示监测员或者管理员,1表示监督员");
            Role role = Console.ReadKey().KeyChar == '1' ? Role.superviseModel : Role.administratrorModel;
            var processXmHightestAlarmModel = new ProcessRoleAlarmBLL.ProcessXmHightestAlarmModel(xmHightestAlarmCondition, role);
            roleAlarmBLL.ProcessXmHightestAlarm(processXmHightestAlarmModel, out mssg);
            //pointCheckBLL.ProcessXmHightestAlarm(processXmHightestAlarmModel, out mssg);
            ProcessPrintMssg.Print(string.Format("{0}地图显示颜色为{1}色\n", mssg, ColorDispaly(processXmHightestAlarmModel.model.alarm)));
            Console.Write("键入'v'查看项目自检详情\n");

            if (Console.ReadKey().KeyChar == 'v')
            {

                ProcessXmAlarmLsit();
            }
        }

        public void ProcessXmAlarmLsit()
        {
            var processPointCheckModelListModel = new ProcessPointCheckBLL.ProcessPointCheckModelListModel(pointCheck.xmno);
            pointCheckBLL.ProcessPointCheckModelList(processPointCheckModelListModel, out mssg);
            foreach (var check in processPointCheckModelListModel.modelList)
            {
                Console.Write(string.Format("\n{0} {1} {2}级预警 {3}\n", check.type, check.point_name, check.alarm, check.time));

            }
        }
        public string ColorDispaly(int alarm)
        {
            switch (alarm)
            {

                case 1: return "黄";
                case 2: return "橙";
                case 3: return "红";
                default: return "绿";
            }

        }

        public void ProcessTestPointCheckModel()
        {
            string xmname = "数据测试";
            string jclx = "表面位移";
            string pointName = "Z7-5";
            var processPointCheckModelGetModel = new RolePointAlarmCondition(Aspect.IndirectValue.GetXmnoFromXmname(xmname), jclx, pointName);
            pointCheckBLL.ProcessPointCheckModelGet(processPointCheckModelGetModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public ProcessXmAlarmCount processXmAlarmCont = new ProcessXmAlarmCount();
        public void TestXmAlarmCont()
        {
            processXmAlarmCont.XmAlarmCount(29, "表面位移", out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public ProcessMaxAlarm processMaxAlarm = new ProcessMaxAlarm();
        public void TestMaxAlarm()
        {
            processMaxAlarm.MaxAlarm(29, "表面位移", out mssg);
            ProcessPrintMssg.Print(mssg);
        }


    }
}
