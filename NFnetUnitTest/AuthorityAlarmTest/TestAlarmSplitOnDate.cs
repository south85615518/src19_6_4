﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;
using NFnet_Interface.AuthorityAlarmProcess;
using NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    class TestAlarmSplitOnDate
    {
        public static ProcessalarmsplitondateBLL alarmBLL = new ProcessalarmsplitondateBLL();
        public static ProcessAlarmSplitOnDateConfirm processAlarmSplitOnDateConfirm = new ProcessAlarmSplitOnDateConfirm();
        public static ProcessAlarmSplitOnDateDeleteByAlarmNo processAlarmSplitOnDateDeleteByAlarmNo = new ProcessAlarmSplitOnDateDeleteByAlarmNo();
        public static ProcessAlarmSplitOnDateDeleteByDate processAlarmSplitOnDateDeleteByDate = new ProcessAlarmSplitOnDateDeleteByDate();
        public static ProcessReportAlarmSplitModelList processReportAlarmSplitModelList = new ProcessReportAlarmSplitModelList();
        public static string mssg = "";
        AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
        {
            dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
            jclx = "表面位移",
            pointName = "Z7-5",
            xmno = 29,
            time = Convert.ToDateTime("2017/8/2 15:30"),
            adate = DateTime.Now

        };

        public void main()
        {
            //TestCgAlarmSplitOnDateAdd();
            //TestCgAlarmSplitOnDateDel();
            //TestCgAlarmOnDateSplit();
            //TestDateAlarmOnDateSplit();
            //TestAlarmOnDateSplit();
            //TestProcessCgAlarmSplitCont();
            //TestProcessCgAlarmSplitReaded();
            //TestAlarmSplitOnDateConfirm();
            //ProcessPrintMssg.Print(mssg);
            //TestAlarmSplitOnDateDeleteByAlarmNo();
            //ProcessPrintMssg.Print(mssg);
            //TestAlarmSplitOnDateDeleteByDate();
            //ProcessPrintMssg.Print(mssg);
            //TesttAlarmSplitOnDateListLoad();
            TestReportAlarmSplit();
        }

        public void TestAlarmSplitOnDateConfirm()
        {
            processAlarmSplitOnDateConfirm.AlarmSplitOnDateConfirm(106, "D189512145716570",out mssg);
        }
        public void TestAlarmSplitOnDateDeleteByAlarmNo()
        {
            processAlarmSplitOnDateDeleteByAlarmNo.AlarmSplitOnDateDeleteByAlarmNo(106, "D189512145716570", out mssg);
        }
        public void TestAlarmSplitOnDateDeleteByDate()
        {
            processAlarmSplitOnDateDeleteByDate.AlarmSplitOnDateDeleteByDate(106, Convert.ToDateTime("2018-09-05"), out mssg);
        }
        public void TesttAlarmSplitOnDateListLoad()
        {
            //List<int> cyclist = new List<int>();
            //cyclist.Add(1);
            //cyclist.Add(2);
            //processReportAlarmSplitModelList.ReportAlarmSplitModelList(96,cyclist,data.Model.gtsensortype._HorizontalDisplacement,out mssg);
            //ProcessPrintMssg.Print(mssg);
        }

        //public void TestAlarmSplitOnDateAdd()
        //{
        //    alarmBLL.ProcessAlarmSplitOnDateAdd(cgalarm, out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        //public void TestCgAlarmSplitOnDateDel()
        //{
        //    var processCgAlarmSplitOnDateDeleteModel = new ProcessCgAlarmSplitOnDateBLL.ProcessCgAlarmSplitOnDateDeleteModel(cgalarm.xmno, "表面位移", cgalarm.pointName, cgalarm.time);
        //    cgalarmBLL.ProcessCgAlarmSplitOnDateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}
        //public void TestDateAlarmOnDateSplit()
        //{
        //    var processAlarmSplitOnDateModelListModel = new ProcessalarmsplitondateBLL.ProcessDateAlarmSplitOnDateModelListModel(29, Convert.ToDateTime("2017-11-14"));
        //    alarmBLL.ProcessalarmsplitondateModelList(processAlarmSplitOnDateModelListModel, out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}

        //public void TestProcessCgAlarmSplitCont()
        //{
        //    var processalarmsplitondatecontModel = new ProcessCgAlarmSplitOnDateBLL.ProcessalarmsplitondatecontModel(cgalarm.xmno,"表面位移");
        //    cgalarmBLL.Processalarmsplitondatecont(processalarmsplitondatecontModel,out  mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}

        //public void TestProcessCgAlarmSplitReaded()
        //{
        //    var processAlarmCgSplitOnDateReadedModel = new ProcessCgAlarmSplitOnDateBLL.ProcessAlarmCgSplitOnDateReadedModel(cgalarm.xmno, Convert.ToDateTime("2017-09-05"));
        //    cgalarmBLL.Processalarmcgsplitondatereaded(processAlarmCgSplitOnDateReadedModel,out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //}


        public void TestAlarmOnDateSplit()
        {
            var processCgAlarmSplitOnDateModelListModel = new ProcessalarmsplitondateBLL.ProcessAlarmSplitOnDateModelListModel(cgalarm.xmno,new DateTime(),new DateTime(),"z8");
            alarmBLL.ProcessAlarmSplitOnDateModelList(processCgAlarmSplitOnDateModelListModel,out mssg);
            //ProcessPrintMssg.Print(mssg);
            var processAlarmOnDateSplitModel = new ProcessalarmsplitondateBLL.ProcessAlarmOnDateSplitModel(processCgAlarmSplitOnDateModelListModel.ls);
            alarmBLL.ProcessAlarmOnDateSplit(processAlarmOnDateSplitModel,out mssg);
            Console.Write("================日期分包预警统计如下====================\n");
            Console.Write("日期\t\t未读\t\t数量\t\t发送次数\n");
            foreach (var model in processAlarmOnDateSplitModel.modells)
            {
                Console.Write("{0}\t{1}\t\t{2}\t\t{3}\n", string.Format("{0}-{1}-{2}", Convert.ToDateTime(model.dt).Year, Convert.ToDateTime(model.dt).Month, Convert.ToDateTime(model.dt).Day),model.read+"条" ,model.acont + "条", model.scont);
                Console.Write("{0}\n",  model.alarmContext.Replace("<p>","\n"));
            }
            Console.ReadLine();

            List<string> ldt = new List<string>();
            Console.Write("选择日期包输入T结束\n");
            char dtch; 
            //while((dtch =Console.ReadKey().KeyChar )!= 't')
            //{
            //    ldt.Add(processCgAlarmOnDateSplitModel.modells[Convert.ToInt32(dtch.ToString())].dt.ToString());

            //}
            //List<string> lsmail = new List<string>();
            //Console.Write("输入要发送的电子邮箱输入T结束\n");
            //while ((dtch = Console.ReadKey().KeyChar) != 't')
            //{
            //    Console.Write("邮箱号:_\n");
            //    lsmail.Add(Console.ReadLine());
            //    Console.Write("输入t结束\n");
            //}
            //var processCgAlarmOnDateSplitUnionSendModel = new ProcessCgAlarmSplitOnDateBLL.ProcessCgAlarmOnDateSplitUnionSendModel(29, lsmail, "数据测试", ldt, processCgAlarmOnDateSplitModel.ls);
            //cgalarmBLL.ProcessCgAlarmOnDateSplitUnionSend(processCgAlarmOnDateSplitUnionSendModel,out mssg);
            //ProcessPrintMssg.Print(mssg);


        }
        public void TestReportAlarmSplit()
        {
           var model = NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess.ProcessReportAlarmSplitModelList.ReportAlarmSplitMssgLoad(NFnet_BLL.DisplayDataProcess.pub.ProcessAspectIndirectValue.GetXmnoFromXmname("羊台山隧道监测"), "797,798,799".Split(',').ToList(), data.Model.gtsensortype._fractureMeter, out mssg);
        }

    }
}
