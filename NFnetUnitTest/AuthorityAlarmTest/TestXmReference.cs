﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;
using NFnet_Interface.UserProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    public class TestXmReference
    {
        public ProcessXmReference processXmReference = new ProcessXmReference();
        public ProcessXmReferenceListLoad processXmReferenceListLoad = new ProcessXmReferenceListLoad();
        public ProcessClientmnameXmnoGet processClientmnameXmnoGet = new ProcessClientmnameXmnoGet();
        public Authority.Model.xmreference model = new Authority.Model.xmreference { xmno = 106, client_xmname="客户端项目1" };
        public string mssg = "";
        public void main()
        {
            TestXmReferenceAdd();
            //TestXmReferenceExist();
            //TestXmReferenceDelete();
            //TestXmReferenceExist();
            //TestXmReferenceListLoad();
            TestClientxmnameXmnoGet();
            ProcessPrintMssg.Print(mssg);
        }
        public void TestXmReferenceAdd()
        {
            processXmReference.ProcessXmReferenceAdd(model,out mssg);
        }
        public void TestXmReferenceDelete()
        {
            //processXmReference.ProcessXmReferenceDelete(, out mssg);
        }
        public void TestXmReferenceExist()
        {
            processXmReference.ProcessXmReferenceExist(model, out mssg);
        }
        public void TestXmReferenceListLoad()
        {
            processXmReferenceListLoad.XmReferenceListLoad(model.xmno,out mssg);
        }
        public void TestClientxmnameXmnoGet()
        {
            processClientmnameXmnoGet.ClientmnameXmnoGet("客户端项目1",out mssg);
        }
    }
}
