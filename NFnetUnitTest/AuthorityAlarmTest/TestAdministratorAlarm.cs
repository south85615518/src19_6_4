﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    
    class TestAdministratorAlarm
    {
        string mssg = "";
        public ProcessAdministratorAlarmBLL processAdministratorAlarmBLL = new ProcessAdministratorAlarmBLL();
        //监测员预警通知存库
        public AuthorityAlarm.Model.adminalarm monitorAlarm = new AuthorityAlarm.Model.adminalarm
        {
            xmno = 23,
            context = "_hello",
             sendTime = DateTime.Now,
            confirm = false,
            mail = false,
            adminno = "sa",
            dataType = 1,
            ForwardTime = 0,
            mess = false,
            aid = string.Format("A{0}{1}", 4, DateHelper.DateTimeToString(DateTime.Now))
        };
       
        public void main() {
            //TestAdministratorAlarmAddProcess();
            TestAdministratorAlarmUpdateProcess();
        }
        public void TestAdministratorAlarmAddProcess()
        {
            processAdministratorAlarmBLL.ProcessAdministrtorAlarmAdd(monitorAlarm,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestAdministratorAlarmUpdateProcess()
        {
            monitorAlarm.aid = Console.ReadLine();
            monitorAlarm.mail = true;
            processAdministratorAlarmBLL.ProcessAdministrtorAlarmUpdate(monitorAlarm, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
