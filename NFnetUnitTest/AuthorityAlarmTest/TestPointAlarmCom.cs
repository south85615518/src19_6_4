﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnetUnitTest.AuthorityAlarmTest
{
    class TestPointAlarmCom
    {
        public RolePointAlarmCom pointAlarmCom = new RolePointAlarmCom();
        public string mssg = "";
        public void main()
        {
            ProcessRolePointAlarm();
        }
        public bool ProcessRolePointAlarm()
        {

            string xmname = "数据测试";
            string jclx = "表面位移";
            string pointName = "Sp2";
            Role role = Role.superviseModel;

            var rolePointAlarmCondition = new RolePointAlarmCondition(Aspect.IndirectValue.GetXmnoFromXmname(xmname), jclx, pointName);
            var processPointAlarmModel = new NFnet_BLL.AuthorityAlarmProcess.RolePointAlarmCom.ProcessPointAlarmModel(rolePointAlarmCondition, role);

            if (!pointAlarmCom.ProcessPointAlarm(processPointAlarmModel, out mssg))
            {

                return false;
            }

            return true;

        }
    }
}
