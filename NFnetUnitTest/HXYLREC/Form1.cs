﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NFnet_BLL.DisplayDataProcess;

namespace NFnetUnitTest.HXYLREC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public ProcessHXYLRECServer tcpServer = new ProcessHXYLRECServer();
        private void Form1_Load(object sender, EventArgs e)
        {
            tcpServer.HXYLTcpServerStart(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\hxylsetting.txt",Convert.ToInt32(this.textBox1.Text));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProcessHXYLRECServer.sleepminute = Convert.ToInt32(this.textBox1.Text);
            Console.WriteLine("获取雨量数据间隔设置成功");
        }
    }
}
