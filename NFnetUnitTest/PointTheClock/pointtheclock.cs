﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NFnetUnitTest.PointTheClock
{
    public partial class pointtheclock : Form
    {
        public static DateTime dtpoint = new DateTime();
        public static int i = 0;
        public pointtheclock()
        {
            dtpoint = DateTime.Now;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine(string.Format("程序段{1}*******运行时长{0}ms*******",(DateTime.Now - dtpoint).TotalMilliseconds,i++));
            dtpoint = DateTime.Now;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Console.WriteLine(string.Format("————————————重新测试———————————"));
            dtpoint = DateTime.Now;
            i = 0;

        }
    }
}
