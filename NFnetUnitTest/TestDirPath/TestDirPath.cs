﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnetUnitTest.TestDirPath
{
    public class TestProcessDirPath
    {
        public void main()
        {
            DirPath();
        }
        public void DirPath()
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory;
            string currentPath = System.IO.Directory.GetCurrentDirectory();
            string midPath = path.Substring(path.IndexOf("NFnetUnitTest\\"));
            int cont = 0;
            StringBuilder str = new StringBuilder(256);
            while(true)
            {
                if (midPath.IndexOf("\\") != -1 || midPath.IndexOf("/") != -1)
                {
                    cont++;
                    midPath = midPath.Substring(midPath.IndexOf("\\")+1);
                    str.Append("..\\");
                }
                else break;
            }

            ProcessPrintMssg.Print(str.ToString());
        }
    }
}
