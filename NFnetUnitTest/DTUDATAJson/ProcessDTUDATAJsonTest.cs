﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.WaterLevel;

namespace NFnetUnitTest.DTUDATAJson
{
    
    public class ProcessDTUDATAJsonTest
    {
        public DTU.Model.dtudata dtudata =null;
        public List<DTU.Model.dtudata> dtudatalist = new List<DTU.Model.dtudata>();
        public ProcessDTUDATAJson processDTUDATAJson = new ProcessDTUDATAJson();
        public static string mssg = "";
        public void main()
        {
            //Int32ConvertByte();
            //DTUDATAJsonTest();
            //DTUDATAJsonHttpTest();
            //HttpUrlPostTest();
            HttpServerUrlPostTest();
        }
        public void DTUDATAJsonTest()
        {
            dtudata = new DTU.Model.dtudata { deep=10, time = DateTime.Now, point_name="PT1", xmno=29, groundElevation=8 };
            dtudatalist.Add(dtudata);
            dtudata = new DTU.Model.dtudata { deep = 12, time = DateTime.Now, point_name = "PT2", xmno = 29, groundElevation = 8 };
            dtudatalist.Add(dtudata);
            processDTUDATAJson.DTUDATAJsonSend(29,dtudatalist,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void DTUDATAJsonHttpTest()
        {
            string httpurl = "http://192.168.168.166/数据文件上传接口/DTUDATAHttpAPI.ashx?name=yckdstza&pass=6543211&xmname=廊坊中铁物探通新岭、科学馆站监测项目&pointname=txl02&starttime=2018/7/16&endtime=2018/7/18";
            Tool.http.HttpHelper httphelper = new Tool.http.HttpHelper();
            mssg = httphelper.SendMessageToWeb(httpurl);
        }
        public void HttpUrlPostTest()
        {
            string posturl = "http://211.149.176.182:8088/api/FileController/GetData?code=52094231426ACAD43BD817314BF064A5";
            Tool.http.HttpHelper httphelper = new Tool.http.HttpHelper();
            mssg = httphelper.SendMessageToWeb(posturl);
        }
        public void HttpServerUrlPostTest()
        {
            string posturl = "http://47.107.244.166:8087/push?code=52094231426ACAD43BD817314BF064A5";
            Tool.http.HttpHelper httphelper = new Tool.http.HttpHelper();
            mssg = httphelper.SendMessageToWeb(posturl);
        }
        public void Int32ConvertByte()
        {
            //byte b = (byte)Convert.ToInt32("25535");
            //ProcessPrintMssg.Print(string.Format("{0}转为字节是{1}",25535,b));
            //int d = Convert.ToInt32(b);
            //ProcessPrintMssg.Print(string.Format("{0}转为数字是{1}", b,d));
            //string a = Encoding.UTF8.GetString( new byte[2]{5,8} );
            //byte[] bytes = Tool.com.CRC16Helper.CRC16(new byte[2] { 5, -2 });
            //byte b = (byte)Convert.ToInt32(-258); 
            
        }
    }
}
 