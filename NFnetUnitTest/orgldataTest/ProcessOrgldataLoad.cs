﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using System.Data;
using Tool;

namespace NFnetUnitTest.orgldataTest
{
    public class ProcessOrgldataLoad
    {
        public static ProcessOrglDataBLL OrglDataBLL = new ProcessOrglDataBLL();
        public void main() {
            ProcessOrgldataTableLoad();
        }
        public void ProcessOrgldataTableLoad()
        {
            string jsondata = "";
            var processOrglDataLoadModel = new ProcessOrglDataBLL.ProcessOrglDataLoadModel("数据测试", 1, 50, "Z7-4", 1, 10);
            var processOrglDataRecordsCountModel = new ProcessOrglDataBLL.ProcessOrglDataRecordsCountModel("数据测试", 1, 50, "Z7-4", 1, 10);
            int cont = 0;
            DataTable dt = null;
            string mssg = "";
            if (OrglDataBLL.ProcessOrglDataLoad(processOrglDataLoadModel, out mssg))
            {
                dt = processOrglDataLoadModel.dt;
                if (OrglDataBLL.ProcessOrglDataRecordsCount(processOrglDataRecordsCountModel, out mssg))
                {
                    cont = Convert.ToInt32(processOrglDataRecordsCountModel.totalCont);

                }
                else
                {
                    //计算结果数据数量出错信息反馈
                }
            }
            else
            {
                //加载结果数据出错错误信息反馈

            }
            if (JsonHelper.ProcessDataIntoJson(dt, cont, 1, 50, out jsondata))
            {

            }
        }
    }
}
