﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnetUnitTest.Device
{
    public class TestDevice
    {
        //public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        //public ProcessDeviceDelete processDeviceDelete = new ProcessDeviceDelete();
        //public ProcessDeviceModelGet processDeviceModelGet = new ProcessDeviceModelGet();
        //public ProcessDeviceStateSet processDeviceStateSet = new ProcessDeviceStateSet();
        //public ProcessDeviceUseStateSet processDeviceUseStateSet = new ProcessDeviceUseStateSet();
        public ProcessXmDeviceAlarm processXmDeviceAlarm = new ProcessXmDeviceAlarm();
        //public ProcessXmDeviceTableCountLoad processXmDeviceTableCountLoad = new ProcessXmDeviceTableCountLoad();
        //public ProcessXmDeviceTableLoad processXmDeviceTableLoad = new ProcessXmDeviceTableLoad();

        //public global::device.Model.xmdevice model = new device.Model.xmdevice {
        //    xmno = Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"),
        //     senorname = "索佳XDK", senortype = "全站仪", senorno="TOTALSTATION01",
        //      producetiontime = Convert.ToDateTime("2018-1-6"), buyfrom = "南方测绘广州分公司",
        //       effectionyears = 1, lastinspectiontime = DateTime.Now, useing = true,  inspectionalarmvalue = 10, inspectiontimes=0, alarm = false, state = "在用"
        //};
        //public string mssg = "";

        //public List<string> searchListInit()
        //{
        //    List<string> ls = new List<string>();
        //    //仪器名称关键字查询
        //    string sensornamekeyword = "索佳";
        //    ls.Add(string.Format("  senorname  like  '%{0}%'    ",sensornamekeyword));
        //    //仪器类型
        //    string sensortype = "";
        //    ls.Add( sensortype == ""?"  1=1  " : string.Format("   sensortype = '{0}'  ",sensortype));
        //    //仪器编号 
        //    string sensorno = "";
        //    ls.Add(sensorno == ""?" 1=1 ":string.Format(" sensorno ='{0}' ",sensorno));
        //    //采购方
        //    string buyfrom = "";
        //    ls.Add(buyfrom == ""?"  1=1  ":string.Format(" buyfrom = '{0}'  ",buyfrom));
        //    //在用/停用
        //    bool useing = true;
        //    ls.Add(useing == null ? "  1=1  " : string.Format(" useing = '{0}'  ", useing?1:0));
        //    //可用与否
        //    string state = "";
        //    ls.Add(state == ""?" 1=1 ":string.Format("  state = '{0}' ",state));
        //    //质检次数
        //    int inspectiontimes = 0;
        //    ls.Add(string.Format("  inspectiontimes >= '{0}'  ", inspectiontimes));
        //    //警戒值
        //    int inspectionalarmvalue = 0;
        //    ls.Add(string.Format("  inspectionalarmvalue >= '{0}'  ", inspectionalarmvalue));
        //    return ls;

        //}



        public void main()
        {
            //TestDeviceAdd();
            //TestDeviceUpdate();
            //TestDeviceTableLoad(1, 20, Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"), searchListInit(), "  senortype,senorname,buyfrom,alarm,inspectiontimes,senorno  asc ", out mssg);
            //TestDeviceDelete(Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"),1,out mssg);
            //TestDeviceStateSet(Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"), "TOTALSTATION01","停用",out mssg);
            //TestDeviceUseState(Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"), "TOTALSTATION01",false, out mssg);

            TestUnitDeviceAlarm();
        }
        //public void TestDeviceAdd()
        //{
        //    processDeviceBLL.ProcessDeviceAdd(model, out mssg);
            
        //}
        //public void TestDeviceUpdate()
        //{
        //    model.state = "停用";
        //    model.id = 1;
        //    processDeviceBLL.ProcessDeviceUpdate(model, out mssg);
            
        //}
        //public void TestDeviceDelete(int xmno,int id,out string mssg)
        //{
        //    //processDeviceDelete.DeviceDelete(xmno,id,out mssg);
        //}
        //public void TestDeviceModelGet(int xmno,string deviceno,out string mssg)
        //{
        //    //processDeviceModelGet.DeviceModelGet(xmno,deviceno,out mssg);
        //}
        //public void TestDeviceStateSet(int xmno,string sensorno,string state,out string mssg)
        //{
        //    //processDeviceStateSet.DeviceStateSet(xmno,sensorno,state,out mssg);
        //}
        //public void TestDeviceUseState(int xmno, string sensorno, bool state, out string mssg)
        //{
        //    processDeviceUseStateSet.DeviceUseStateSet(xmno,sensorno,state,out mssg);
        //}
        //public void TestDeviceTableLoad(int pageIndex,int rows,int xmno,List<string >searchlist,string sord,out string mssg)
        //{
        //    processXmDeviceTableLoad.XmDeviceTableLoad( pageIndex, rows, xmno,searchlist, sord,out mssg);
        //}
        //public void TestDeviceTableCount(int xmno,List<string> searchList,out string mssg )
        //{
        //    processXmDeviceTableCountLoad.XmDeviceTableCountLoad(xmno,searchList,out mssg);
        //}
        public void TestUnitDeviceAlarm()
        {
            //processXmDeviceAlarm.XmDeviceAlarm(xmno, out mssg);
            processXmDeviceAlarm = new ProcessXmDeviceAlarm { unitname = "广州铁路科技开发有限公司", alarmlist = new List<string>() };
            processXmDeviceAlarm.main();
        }
    }
}
