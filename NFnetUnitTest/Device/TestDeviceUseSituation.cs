﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.deviceusesituation;
using NFnet_Interface.Device;

namespace NFnetUnitTest.deviceusesituation
{
    public class TestdeviceusesituationUseSituation
    {
        public ProcessdeviceusesituationUseSituation processdeviceusesituationBLL = new ProcessdeviceusesituationUseSituation();
        public ProcessDeviceUseSituationDelete processdeviceusesituationDelete = new ProcessDeviceUseSituationDelete();
        public ProcessDeviceUseSituationModelGet processDeviceUseSituationModelGet = new ProcessDeviceUseSituationModelGet();
        public ProcessDeviceUseSituationTableCountLoad processXmDeviceUseSituationTableCountLoad = new ProcessDeviceUseSituationTableCountLoad();
        public ProcessDeviceUseSituationTableLoad processXmDeviceUseSituationTableLoad = new ProcessDeviceUseSituationTableLoad();

        public global::device.Model.deviceusesituation model = new device.Model.deviceusesituation
        {
            unitname = "广州铁路科技开发有限公司",
            xmno = Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"),
            deviceno = "索佳XDK",
            monitorID  = "gtjcy",
            monitorName = "Joe",
            recivetime = DateTime.Now,
            passmanname = "Jeff",
            xmname = "新白广城际铁路下穿京广高铁施工监测",
            hasinspection = 0,
            inspectiontime  = DateTime.Now.AddDays(-1),
            address = "广州市花都区S110(广清高速)",
             inspectresult = "正常",
            inspectiontimes = 1,
            inspectionproofPath  = "",
            nextinspectiontime = DateTime.Now.AddDays(7)
        };
        public string mssg = "";

        public List<string> searchListInit()
        {
            List<string> ls = new List<string>();
            //仪器名称关键字查询
            string sensornamekeyword = "索佳";
            ls.Add(string.Format("  senorname  like  '%{0}%'    ", sensornamekeyword));
            //仪器类型
            string sensortype = "";
            ls.Add(sensortype == "" ? "  1=1  " : string.Format("   sensortype = '{0}'  ", sensortype));
            //仪器编号 
            string sensorno = "";
            ls.Add(sensorno == "" ? " 1=1 " : string.Format(" sensorno ='{0}' ", sensorno));
            //采购方
            string buyfrom = "";
            ls.Add(buyfrom == "" ? "  1=1  " : string.Format(" buyfrom = '{0}'  ", buyfrom));
            //在用/停用
            bool useing = true;
            ls.Add(useing == null ? "  1=1  " : string.Format(" useing = '{0}'  ", useing ? 1 : 0));
            //可用与否
            string state = "";
            ls.Add(state == "" ? " 1=1 " : string.Format("  state = '{0}' ", state));
            //质检次数
            int inspectiontimes = 0;
            ls.Add(string.Format("  inspectiontimes >= '{0}'  ", inspectiontimes));
            //警戒值
            int inspectionalarmvalue = 0;
            ls.Add(string.Format("  inspectionalarmvalue >= '{0}'  ", inspectionalarmvalue));
            return ls;

        }



        public void main()
        {
            List<string> searchlist = new List<string>();

            searchlist.Add("  1=1  ");
            TestdeviceusesituationAdd();
            var model = TestdeviceusesituationModelGet("广州铁路科技开发有限公司", 1, out mssg);
            TestdeviceusesituationUpdate();

            //TestdeviceusesituationAdd();
            //TestdeviceusesituationUpdate();
            TestdeviceusesituationTableLoad("广州铁路科技开发有限公司", 1, 100, searchlist, " asc ", out mssg);
            TestdeviceusesituationTableCount("广州铁路科技开发有限公司", searchlist, out mssg);
            //TestdeviceusesituationDelete(Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"), 1, out mssg);
            //TestdeviceusesituationStateSet(Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"), "TOTALSTATION01","停用",out mssg);
            //TestdeviceusesituationUseState(Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"), "TOTALSTATION01",false, out mssg);
            //TestdeviceusesituationModelGet(Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"), "TOTALSTATION01", out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public void TestdeviceusesituationAdd()
        {
            processdeviceusesituationBLL.ProcessdeviceusesituationAdd(model, out mssg);

        }
        public void TestdeviceusesituationUpdate()
        {
            
            processdeviceusesituationBLL.ProcessdeviceusesituationUpdate(model, out mssg);

        }
        public bool TestdeviceusesituationDelete(string unitname, int id, out string mssg)
        {
            return processdeviceusesituationDelete.DeviceDelete(unitname,id,out mssg);
            
        }
        public device.Model.deviceusesituation TestdeviceusesituationModelGet(string unitname, int id, out string mssg)
        {
           return processDeviceUseSituationModelGet.DeviceModelGet(unitname, id, out mssg);
        }
        //public void TestdeviceusesituationStateSet(int xmno, string sensorno, string state, out string mssg)
        //{
        //    //processdeviceusesituationStateSet.deviceusesituationStateSet(xmno,sensorno,state,out mssg);
        //}
        //public void TestdeviceusesituationUseState(int xmno, string sensorno, bool state, out string mssg)
        //{
        //    processdeviceusesituationUseStateSet.deviceusesituationUseStateSet(xmno, sensorno, state, out mssg);
        //}
        public void TestdeviceusesituationTableLoad(string unitname,int pageIndex, int rows, List<string> searchlist, string sord, out string mssg)
        {
            processXmDeviceUseSituationTableLoad.XmDeviceTableLoad(unitname,pageIndex, rows,  searchlist, sord, out mssg);
        }
        public void TestdeviceusesituationTableCount(string unitname, List<string> searchList, out string mssg)
        {
            processXmDeviceUseSituationTableCountLoad.XmDeviceTableCountLoad(unitname, searchList, out mssg);
        }
      
    }
}
