﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnetUnitTest.DataImport
{
    public class TestGTSensorDataImport
    {
        public ProcessGTSensorDataBLL processGTSensorDataBLL = new ProcessGTSensorDataBLL();
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public string mssg = "";
        public data.Model.gtsensordata gtsensordata = new data.Model.gtsensordata {  project_name = "96", point_name="gjj-205-2-5" ,cyc = 2,  time = DateTime.Now.AddHours(-5), datatype = "围护墙钢筋应力", senorno = "095526", valuetype = 2 };
        public void main()
        {

         //ProcessIsCycdirnetDataExist();
     
         //ProcessSiblingDataCycGet();

           //ProcessIsInsertData();

         //ProcessInsertDataCycGet();
       
         //ProcessIsInsertCycExist();
      
         //ProcessInsertCycStep();
        
         //ProcessResultDataExtremely();
       


            GTSensorDataImport();
            //DirImport("d:\\Desktop\\混凝土支撑内力");
        }
        public void GTSensorDataImport()
        {
            var model = new NFnet_BLL.DataImport.ProcessFile.ProcessGTSensorDataBLL.ProcessFileDecodeTxtModel("新白广城际铁路下穿京广高铁施工监测", 96, "d:\\Desktop\\金马传感器数据_金马设备数据_2月_21日_12时_8分_580_结构应力.txt", "金马设备数据");
            processGTSensorDataBLL.ProcessFileDecodeTxtDescode(model,out mssg);
        }
        public void DirImport(string dirpath)
        {
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            foreach(string filename in  filenamelist)
            {
                var model = new NFnet_BLL.DataImport.ProcessFile.ProcessGTSensorDataBLL.ProcessFileDecodeTxtModel("新白广城际铁路下穿京广高铁施工监测", 96, filename, "金马设备数据");
                processGTSensorDataBLL.ProcessFileDecodeTxtDescode(model, out mssg);
            }
        }
        
        public void ProcessIsCycdirnetDataExist()
        {
            var model = new ProcessResultDataBLL.IsCycdirnetDataExistModel(gtsensordata.project_name,gtsensordata.point_name,gtsensordata.time,data.DAL.gtsensortype.GTStringToSensorType(gtsensordata.datatype));
            processResultDataBLL.IsCycdirnetDataExist(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
     
        public void ProcessSiblingDataCycGet()
        {
            //processResultDataBLL.ProcessResultDataAdd(gtsensordata,out mssg);
            //gtsensordata = new data.Model.gtsensordata { project_name = "96", point_name = "gjj-205-2-6", cyc = 2, time = DateTime.Now, datatype = "围护墙钢筋应力", senorno = "095520", valuetype = 2 };
            //processResultDataBLL.ProcessResultDataAdd(gtsensordata, out mssg);
            //ProcessPrintMssg.Print(mssg);
            var model = new ProcessResultDataBLL.DataCycGetModel(gtsensordata.project_name, gtsensordata.time, data.DAL.gtsensortype.GTStringToSensorType(gtsensordata.datatype), "gjj-205-2-7".Split(',').ToList(),1);
            processResultDataBLL.SiblingDataCycGet(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessIsInsertData()
        {
            var model = new ProcessResultDataBLL.IsTypeDataModel(gtsensordata.project_name, gtsensordata.time, data.DAL.gtsensortype.GTStringToSensorType(gtsensordata.datatype));
            processResultDataBLL.IsInsertData(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessInsertDataCycGet()
        {
            var model = new ProcessResultDataBLL.InsertDataCycGetModel(gtsensordata.project_name, gtsensordata.time, data.DAL.gtsensortype.GTStringToSensorType(gtsensordata.datatype));
            processResultDataBLL.InsertDataCycGet(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessIsInsertCycExist()
        {
            var model = new ProcessResultDataBLL.IsInsertCycExistModel(gtsensordata.project_name, gtsensordata.cyc, data.DAL.gtsensortype.GTStringToSensorType(gtsensordata.datatype));
            processResultDataBLL.IsInsertCycExist(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessInsertCycStep()
        {
            var model = new ProcessResultDataBLL.InsertCycStepModel(gtsensordata.project_name, gtsensordata.cyc, data.DAL.gtsensortype.GTStringToSensorType(gtsensordata.datatype));
            processResultDataBLL.InsertCycStep(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void ProcessResultDataExtremely()
        {
            var model = new ProcessResultDataBLL.ResultDataExtremelyCondition(gtsensordata.project_name,"max", data.DAL.gtsensortype.GTStringToSensorType(gtsensordata.datatype));
            processResultDataBLL.ProcessResultDataExtremely(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        //public void TestResultSurveyDataImport(string xmname, int cyc, out string mssg)
        //{
        //    processResultSurveyDataImport.ResultSurveyDataImport(xmname, cyc, out mssg);
        //}
        //public void TestSurveyDataUpdate(string xmname, string pointname, string vSet_name, string vLink_name, int cyc, int srcdatacyc, out string mssg)
        //{
        //    processSurveyDataUpdate.SurveyDataUpdate(xmname, pointname, vSet_name, vLink_name, cyc, srcdatacyc, out  mssg);
        //}
        //public void TestSurveyPointCycDataAdd(string xmname, string pointname, int cyc, int importcyc, out string mssg)
        //{
        //    processSurveyPointCycDataAdd.SurveyPointCycDataAdd(xmname, pointname, cyc, importcyc, out  mssg);
        //}
        //public void TestLackPoints(string xmname, int xmno, int cyc, out string mssg)
        //{
        //    processLackPoints.LackPoints(xmname, xmno, cyc, out  mssg);
        //}
        //public void TestCgCycList(string xmname)
        //{
        //    processCgCycList.CgCycList(xmname, out mssg);
        //}
        //public void TestSurveyCYCDataDelete(int startCyc, int endCyc, string xmname, string pointname)
        //{
        //    processSurveyCYCDataDelete.SurveyCYCDataDelete(startCyc, endCyc, xmname, "", out mssg);
        //}
        //public void TestCgResultDataImport(string xmname, int cyc, int importcyc)
        //{
        //    processCgResultDataImport.CgResultDataImport(xmname, cyc, importcyc, out mssg);
        //}
        //public void TestDeleteCgDataCycOnChain(string xmname, int cyc, out string mssg)
        //{
        //    processDeleteCgDataCycOnChain.DeleteCgDataCycOnChain(xmname, cyc, out mssg);
        //}



    }
}
