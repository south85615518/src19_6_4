﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnetUnitTest.DataImport
{
   public class TestDTUAlarmValue
    {
       public static ProcessDTUAlarmValueBLL processDTUAlarmValueBLL = new ProcessDTUAlarmValueBLL();
       public ProcessDTUAlarmValueTableLoad processDTUAlarmValueTableLoad = new ProcessDTUAlarmValueTableLoad();
       public ProcessDTUAlarmValueTableCount processDTUAlarmValueTableCount = new ProcessDTUAlarmValueTableCount();
       public ProcessDTUAlarmValueDelete processDTUAlarmValueDelete = new ProcessDTUAlarmValueDelete();
       public ProcessDTUAlarmNameStr processDTUAlarmNameStr = new ProcessDTUAlarmNameStr();
       public ProcessDTUAlarmValueModel processDTUAlarmValueModel = new ProcessDTUAlarmValueModel();
       public static string mssg = "";
       public DTU.Model.dtualarmvalue model = new DTU.Model.dtualarmvalue { 
            alarmname = "1级预警",
             xmno = 29,
              deep = 0.46,
               thisdeep = 1.530,
                acdeep = 0.5,
                 rap = 0.8,
               id = 0
       };

       public void main()
       {
           //testDTUAlarmValueAdd();
           //testDTUAlarmValueUpdate();
           testDTUAlarmLoad();
           //testDTUAlarmTableCount();
           //TestProcessDTUAlarmValueStr();
           //testDTUAlarmValueModel();
       }
       public void testDTUAlarmValueAdd()
       {
           processDTUAlarmValueBLL.ProcessDTUAlarmValueAdd(model,out mssg);
           ProcessPrintMssg.Print(mssg);
       }
       public void testDTUAlarmValueUpdate()
       {
           processDTUAlarmValueBLL.ProcessDTUAlarmValueUpdate(model, out mssg);
           ProcessPrintMssg.Print(mssg);
       }
       public void testDTUAlarmLoad()
       {
           DataTable dt = processDTUAlarmValueTableLoad.DTUAlarmValue(29,"alarmname",1,20,"asc",out mssg);
           ProcessPrintMssg.Print(mssg);
       }
       public void testDTUAlarmTableCount()
       {
           int cont = processDTUAlarmValueTableCount.DTUAlarmValueTableCount(29, " 1=1 ", out mssg);
           ProcessPrintMssg.Print(mssg);
       }
       public void TestProcessDTUAlarmValueStr()
       {
           processDTUAlarmNameStr.DTUAlarmNameStr(29, out mssg);
           ProcessPrintMssg.Print(mssg);
       }
       public void TestProcessDTUAlarmValueDel()
       {
           processDTUAlarmValueDelete.DTUAlarmValueDelete(29,"一级预警",out mssg);
           ProcessPrintMssg.Print(mssg);
       }
       public void testDTUAlarmValueModel()
       {
           DTU.Model.dtualarmvalue dtumodel = processDTUAlarmValueModel.DTUAlarmValueModel("1级预警",29,out mssg);
           ProcessPrintMssg.Print(mssg);
       }


    }
}
