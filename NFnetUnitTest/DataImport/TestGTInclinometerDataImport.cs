﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile.GTSensorServer;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnetUnitTest.DataImport
{
    public class TestGTInclinometerDataImport
    {
        public ProcessFixed_Inclinometer_orglDataBLL processGTInclinometer_orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public NGN.Model.fixed_inclinometer_orgldata fixed_inclinometer_orgldatamodel = new NGN.Model.fixed_inclinometer_orgldata { xmno = 96, holename = "1", cyc = 2, time = Convert.ToDateTime("2018-12-07 11:31:09") };
        public ProcessGTInclinometer processGTInclinometer = new ProcessGTInclinometer();
        public string mssg = "";
        public void main()
        {
            //ProcessIsInsertData();

            //ProcessInsertDataCycGet();

            //ProcessIsInsertCycExist();

            //ProcessInsertCycStep();
            GTInclinometerDataImport();
            //DirImport("d:\\Desktop\\db\\客户端数据\\混凝土支撑内力");
        }
        public void GTInclinometerDataImport()
        {
            var model = new NFnet_BLL.DataImport.ProcessFile.GTSensorServer.ProcessGTInclinometer.ProcessFileDecodeTxtModel(96, "d:\\广铁测斜\\1.zip", "测斜原始数据");
            processGTInclinometer.ProcessFileDecodeTxt(model, out mssg);
        }
        public void GTInclinometerXlsDataImport()
        {
            var model = new NFnet_BLL.DataImport.ProcessFile.GTSensorServer.ProcessGTInclinometer.ProcessFileDecodeTxtModel(106, "d:\\Desktop\\1 (3)\\广铁测斜数据\\yyl-2(2).csv", "测斜原始数据");
            processGTInclinometer.ProcessFileDecodeTxt(model, out mssg);
        }
        //public void DirImport(string dirpath)
        //{
        //    List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
        //    foreach (string filename in filenamelist)
        //    {
        //        var model = new NFnet_BLL.DataImport.ProcessFile.ProcessGTInclinometerDataBLL.ProcessFileDecodeTxtModel("新白广城际铁路下穿京广高铁施工监测", 106, filename, "金马设备数据");
        //        processGTInclinometerDataBLL.ProcessFileDecodeTxt(model, out mssg);
        //    }
        //}

        public void ProcessInclinometerXlsDataCheck()
        {
            Tool.SenorXLSHelper.ProcessXlsDataImportCheck("C:\\Users\\pc\\Desktop\\测斜", Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void ProcessIsInsertData()
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.IsTypeDataModel(fixed_inclinometer_orgldatamodel.xmno, fixed_inclinometer_orgldatamodel.time, fixed_inclinometer_orgldatamodel.holename);
            processGTInclinometer_orglDataBLL.IsInsertData(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessInsertDataCycGet()
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.InsertDataCycGetModel(fixed_inclinometer_orgldatamodel.xmno, fixed_inclinometer_orgldatamodel.time, fixed_inclinometer_orgldatamodel.holename);
            processGTInclinometer_orglDataBLL.InsertDataCycGet(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessIsInsertCycExist()
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.IsInsertCycExistModel(fixed_inclinometer_orgldatamodel.xmno, fixed_inclinometer_orgldatamodel.cyc, fixed_inclinometer_orgldatamodel.holename);
            processGTInclinometer_orglDataBLL.IsInsertCycExist(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessInsertCycStep()
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.InsertCycStepModel(fixed_inclinometer_orgldatamodel.xmno, fixed_inclinometer_orgldatamodel.cyc, fixed_inclinometer_orgldatamodel.holename);
            processGTInclinometer_orglDataBLL.ProcessInsertCycStep(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
