﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;
using System.IO;
using NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement;

namespace NFnetUnitTest.DataImport
{
    public class TestExcelDataImport
    {
        public ExcelHelperHandle ExcelHelper = new ExcelHelperHandle();
        public TotalStation.BLL.fmos_obj.cycdirnet cycdirnetBLL = new TotalStation.BLL.fmos_obj.cycdirnet();
        public ProcessTotalStationBLL_GT processTotalStationBLL_GT = new ProcessTotalStationBLL_GT();
        public string mssg = "";
        public TotalStation.Model.fmos_obj.cycdirnet model = new TotalStation.Model.fmos_obj.cycdirnet {
            POINT_NAME = "QD-1",
            TaskName = "新白广城际铁路下穿京广高铁施工监测",
            Time = DateTime.Now,
            This_dN = 0.1,
            This_dE = 0.1,
            This_dZ =0.1,
            Ac_dN = 0.2,
            Ac_dE = 0.2,
            Ac_dZ = 0.2,
            CYC = 1,
            siblingpointname = "QD-2"
        };
        public void main()
        {
            //ProcessGTXlsDataCheck();
            ProcessGTXlsDataImport();
            //ExcelHelper.workbookpath = "d:\\Desktop\\新白广城际铁路下穿京广高铁施工监测日报20180706.xls";
            //ExcelHelper.ExcelInit();
            //DataImport();
            //TotalStationDataImport();
            TestResultDataAdd();
        }
        public void TestResultDataAdd()
        {
            cycdirnetBLL.Add(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }


        public void ProcessGTXlsDataImport()
        {
            var model = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessFileDecodeTxtModel("新白广城际铁路下穿京广高铁施工监测", 68, "d:\\Desktop\\固定测斜仪.xlsx", "成果数据");
            processTotalStationBLL_GT.ProcessFileDecodeTxt(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessGTXlsDataCheck()
        {
            bool ipass = Tool.GTXLSHelper.ProcessXlsDataImportCheck("d:\\Desktop\\日报", out mssg);
            ProcessPrintMssg.Print(mssg);
        }


        public void DataImport()
        {
            int i = 0;
            for (i = 1; i < ExcelHelper.xBook.Sheets.Count+1; i++)
            {
                ExcelHelper.xSheet = (Microsoft.Office.Interop.Excel.Worksheet)ExcelHelper.xBook.Sheets[i];
                ExcelHelper.xSheet.Activate();
                TotalStationDataImport();

            }
            ExcelHelper.xApp.Quit();
        }
        public void TotalStationDataImport()
        {
            //ExcelHelper.xSheet.Cells.Value
            string xmname = ExcelHelper.xSheet.Cells[4, 2].Value;
            int cyc = Convert.ToInt32(ExcelHelper.xSheet.Cells[3,1].Value);
            DateTime dt= ExcelHelper.xSheet.Cells[5,6].Value;
            int i = 8;
            TotalStation.Model.fmos_obj.cycdirnet model = null;
            while (ExcelHelper.xSheet.Cells[i, 1].Value.IndexOf("监测小结") == -1)
            {
                model = new TotalStation.Model.fmos_obj.cycdirnet { 
                     POINT_NAME = ExcelHelper.xSheet.Cells[i,1].Value,
                      TaskName = xmname,
                     Time = dt,
                     This_dN = ExcelHelper.xSheet.Cells[i, 2].Value,
                     This_dE = ExcelHelper.xSheet.Cells[i, 3].Value,
                     This_dZ = ExcelHelper.xSheet.Cells[i, 4].Value,
                     Ac_dN = ExcelHelper.xSheet.Cells[i, 5].Value,
                     Ac_dE = ExcelHelper.xSheet.Cells[i, 6].Value,
                     Ac_dZ = ExcelHelper.xSheet.Cells[i, 7].Value,
                      CYC = cyc,
                     siblingpointname = SiblingPointNameGet(ExcelHelper.xSheet.Cells[i,1].Value)
                };
                cycdirnetBLL.Add(model,out mssg);
                i++;
                Console.WriteLine(mssg);
            }
           
        }

        




        public string SiblingPointNameGet(string pointname)
        {
            int i = 8;
            while (ExcelHelper.xSheet.Cells[i, 8].Value != "" && ExcelHelper.xSheet.Cells[i, 8].Value != null)
            {
                string siblingpointstr = ExcelHelper.xSheet.Cells[i, 8].Value;
                if (siblingpointstr.IndexOf(string.Format("{0}：",pointname)) != -1)
                {
                    return ExcelHelper.xSheet.Cells[i, 8].Value.Split('：')[1];
                }
                i++;
            }
            return "";
        }



        //public void ExcelClose()
        //{

        //     //保存
        //public  void save()
        //{
        //    ExcelHelper.xBook.Save();
        //    ExcelHelper.xSheet = null;
        //    ExcelHelper.xBook = null;
        //    ExcelHelper.xApp.Quit(); //这一句是非常重要的，否则Excel对象不能从内存中退出 
        //    ExcelHelper.xApp = null;
        //}
        //}

    }
}
