﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;

namespace NFnetUnitTest.DataImport
{
    public class TestDTUSenor
    {
        public static ProcessDTUSenorBLL processDTUSenorBLL = new ProcessDTUSenorBLL();
        public static ProcessDTUSenorDelete processDTUSenorDelete = new ProcessDTUSenorDelete();
        public static ProcessDTUSenorNoGet processDTUSenorNoGet = new ProcessDTUSenorNoGet();
        public static string mssg = "";
        public static DTU.Model.dtusenor model = new DTU.Model.dtusenor
        {
            senorno = "s" + DateTime.Now.ToFileTime(),
            senorname = "1",
            senortype = "1",
            remark = "1",
            time = DateTime.Now,
            xmno = 29,
            aboundtime = 4
        };
        public void main()
        {
            //TestProcessDTUSenorAddBLL();
            //TestProcessDTUSenorDeleteBLL();
            //TestProcessDTUSenorUpdateBLL();
            //TestProcessDTUSenorNoStr();
        }
        public void TestProcessDTUSenorAddBLL()
        {
            processDTUSenorBLL.ProcessDTUSenorAddBLL(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessDTUSenorDeleteBLL()
        {
            processDTUSenorDelete.DTUSenorDelete(29, "s131576217106063779",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessDTUSenorUpdateBLL()
        {
            model.senorno = "s131580514522176240";
            processDTUSenorBLL.ProcessDTUSenorUpdateBLL(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessDTUSenorNoStr()
        {
            processDTUSenorNoGet.DTUSenorNoGet(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
