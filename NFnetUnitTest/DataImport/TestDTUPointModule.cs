﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnetUnitTest.DataImport
{
    public class TestDTUPointModule
    {
        public ProcessDTUPointModule processDTUPointModule = new ProcessDTUPointModule();
        public ProcessDTUModuleBLL processDTUModuleBLL = new ProcessDTUModuleBLL();
        public ProcessDTUModuleLoad processDTUModuleLoad = new ProcessDTUModuleLoad();
        public ProcessDTUModuleTableCount processDTUModuleTableCount = new ProcessDTUModuleTableCount();
        public ProcessDTUModuleDelete processDTUModuleDelete = new ProcessDTUModuleDelete();
        public ProcessDTUModuleNoGet processDTUModuleNoGet = new ProcessDTUModuleNoGet();
        //public ProcessDTUModuleBLL processDTUModuleBLL = new ProcessDTUModuleBLL();
        public static string mssg = "";
        public static DTU.Model.dtumodule model = new DTU.Model.dtumodule
        {
            id = "M" + DateTime.Now.ToFileTime(),
            addressno = "4850001",
            od = "od3",
            name = "12",
            port = 8092,
            senorno = "1",
            xmno = 29
        };
        public void main()
        {
            //testDTUPointModule();
            //testDTUPointModuleAdd();
            //testDTUPointModuleLoad();
            //testDTUPointModuleTableCount();
            //testDTUPointModuleDelete();
            //testDTUPointModuleUpdate();
            TestProcessDTUModuleNoStr();
        }
        public void testDTUPointModule()
        {
            processDTUPointModule.DTUPointModule(29, 8092, "5", "DTU4851", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointModuleAdd()
        {
            //model.addressno = 
            processDTUModuleBLL.ProcessDTUModuleAddBLL(model,out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointModuleUpdate()
        {
            model.id = "M131580524469324502";
            model.senorno = "s131580514522176240";
            processDTUModuleBLL.ProcessDTUModuleUpdateBLL(model, out  mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointModuleLoad()
        {
           DataTable dt =  processDTUModuleLoad.DTUModuleLoad(1,20,29,"id","asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointModuleTableCount()
        {
            processDTUModuleTableCount.DTUModuleTableCount(29, " 1=1 ",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointModuleDelete()
        {
            processDTUModuleDelete.DTUModuleDelete(29, "4850001", "od5", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessDTUModuleNoStr()
        {
            processDTUModuleNoGet.DTUModuleNoGet(29, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        
    }
}
