﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnetUnitTest.DataImport
{
    public class TestDTUModuleLP
    {
        public ProcessDTUModuleLPBLL processDTUModuleLPBLL = new ProcessDTUModuleLPBLL();
        public ProcessDTUModuleLPLoad processDTUModuleLPLoad = new ProcessDTUModuleLPLoad();
        public ProcessDTUXmPortModuleLPLoad processDTUXmPortModuleLPLoad = new ProcessDTUXmPortModuleLPLoad();
        public ProcessDTUModuleLPTableCount processDTUModuleLPTableCount = new ProcessDTUModuleLPTableCount();
        public ProcessDTUModuleLPModelBLL processDTUModuleLPModelBLL = new ProcessDTUModuleLPModelBLL();
        public DTU.Model.dtulp model = new DTU.Model.dtulp
        {
            xmno = 29,
            module = "4850003",
            LP = 0
        };
        public static string mssg = "";
        public void main()
        {
            //testDTUModuleLPAdd();
            testDTUModuleLPUpdate();
            //testDTUModuleLPUpdateTableLoad();
            //testDTUModuleLPUpdateTableCount();
            //testDTUModuleLPModel();
            //testDTUXmPortModuleLPUpdateTableLoad();
        }
        public void testDTUModuleLPAdd()
        {
            processDTUModuleLPBLL.ProcessDTUModuleLPAdd(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUModuleLPUpdate()
        {
            model.LP = 9;
            processDTUModuleLPBLL.ProcessDTUModuleLPUpdate(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUModuleLPUpdateTableLoad()
        {
            processDTUModuleLPLoad.DTUModuleLPLoad(1, 100, 29, "module", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUXmPortModuleLPUpdateTableLoad()
        {
           DataTable dt = processDTUXmPortModuleLPLoad.DTUXmPortModuleLPLoad(1, 100, 29,"数据测试" ,"module", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUModuleLPUpdateTableCount()
        {
            processDTUModuleLPTableCount.DTUModuleTableCount(29, "1=1", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUModuleLPModel()
        {
            processDTUModuleLPModelBLL.DTUModuleLPModelBLL(29, "1315805",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        
    }
}
