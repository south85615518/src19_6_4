﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnetUnitTest.DataImport
{
    public class TestDTUTask
    {

        public ProcessDTUTimeIntervalBLL processDTUTimeIntervalBLL = new ProcessDTUTimeIntervalBLL();
        public ProcessDTUTimetaskIntervalTable processDTUTimetaskIntervalTable = new ProcessDTUTimetaskIntervalTable();
        public ProcessDTUXmPortTimetaskIntervalTable processDTUXmPortTimetaskIntervalTable = new ProcessDTUXmPortTimetaskIntervalTable();
        public ProcessDTUTimetaskIntervelTableCount processDTUTimetaskIntervelTableCount = new ProcessDTUTimetaskIntervelTableCount();
        public static string mssg = "";
        public DTU.Model.dtutimetask task = new DTU.Model.dtutimetask
        {
            xmno = 29,
            //hour = 0,
            //minute = 9,
            module = "4850001",
            //times = 1,
            //sec = 0,
            applicated = false,
            time = DateTime.Now
        };
        public void main()
        {
            //testDTUTaskAdd();
            //testDTUTaskUpdate();
            //testDTUTaskTableLoad();
            //testDTUTaskTableCountLoad();
            //testDTUXmPortTaskTableLoad();
            testDTUTaskUpdateFlagSet();
        }
        public void testDTUTaskAdd()
        {
            processDTUTimeIntervalBLL.ProcessDTUTimeIntervalAdd(task,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUTaskUpdate()
        {
            processDTUTimeIntervalBLL.ProcessDTUTimeIntervalUpdate(task, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUTaskUpdateFlagSet()
        {
            processDTUTimeIntervalBLL.ProcessDTUTimeIntervalUpdateFlagSet(task, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUTaskTableLoad()
        {
            processDTUTimetaskIntervalTable.DTUTimeIntervalLoad(1,20,29,"module","asc",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUXmPortTaskTableLoad()
        {
            DataTable dt =  processDTUXmPortTimetaskIntervalTable.DTUXmPortTimetaskIntervalTable(1, 20, 29, "数据测试", "module", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUTaskTableCountLoad()
        {
            processDTUTimetaskIntervelTableCount.DTUTimeIntervalTableCount(29, " 1=1 ",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
