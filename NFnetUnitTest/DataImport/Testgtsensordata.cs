﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile;

namespace NFnetUnitTest.DataImport
{
    public class Testgtsensordata
    {

        public data.Model.gtsensordata gtsensordata = new data.Model.gtsensordata {
             project_name = "新白广城际铁路下穿京广高铁施工监测", point_name = "SW01" ,datatype ="水位", time = DateTime.Now
        };
        public ProcessGTSensorDataBLL processGTSensorDataBLL = new ProcessGTSensorDataBLL();
        public string mssg = "";
        public void main()
        {
            TestgtsensordataAdd();
            ProcessPrintMssg.Print(mssg);
        }
        public void TestgtsensordataAdd()
        {
            processGTSensorDataBLL.ProcesGtSensorDataAdd(gtsensordata,out mssg);
        }
    }
}
