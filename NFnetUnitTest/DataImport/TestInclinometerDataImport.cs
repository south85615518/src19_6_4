﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile.Inclinometer;
using System.IO;
using NFnet_BLL.DisplayDataProcess.GTInclinometer;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnetUnitTest.DataImport
{
    public class TestInclinometerDataImport
    {
        public ProcessInclinometerBLL processInclinometerBLL = new ProcessInclinometerBLL();
        public ProcessFixed_Inclinometer_orglDataBLL processGTInclinometer_orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public static string mssg = "";
        public NGN.Model.fixed_inclinometer_orgldata fixed_inclinometer_orgldatamodel = new NGN.Model.fixed_inclinometer_orgldata { xmno = 96, holename = "1", cyc = 1, time = Convert.ToDateTime("2018-12-07 11:31:09") };
        public void main()
        {
            //ProcessInclinometerDataImport();
            //ProcessInclinometerXlsDataImport();
            //ProcessInclinometerXlsDataCheck();

           

            //ProcessIsInsertData();

            //ProcessInsertDataCycGet();

            //ProcessIsInsertCycExist();

            //ProcessInsertCycStep();




            //GTSensorDataImport();

        }
        public void ProcessInclinometerDataImport()
        {
            var processFileDecodeTxtModel = new ProcessInclinometerBLL.ProcessFileDecodeTxtModel("数据测试", Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), "e:\\GPS.txt", "结果数据");
            processInclinometerBLL.ProcessFileDecodeTxt(processFileDecodeTxtModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessInclinometerXlsDataImport()
        {
            string zippath = "C:\\Users\\pc\\Desktop\\传感器参数(lhq)\\1.zip";
            string dirpath = Path.GetDirectoryName(zippath) + "\\" + DateTime.Now.ToFileTime();
            Tool.FileHelper.unZipFile(zippath, dirpath);
            //return;
            //if (!processInclinometerBLL.ProcessXlsDataDirImport(dirpath, 29, out mssg))
            //{
            //    ProcessPrintMssg.Print(string.Format("数据文件导入完成,{0}",mssg));
            //}
        }
       



        
    }
}
