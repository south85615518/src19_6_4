﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NFnetUnitTest.TestInclinometerReport;
using System.Threading;
using NFnetUnitTest.TestDTUReport;
using NFnet_Interface.UserProcess;
using NFnet_BLL.LoginProcess;
using Tool;

namespace NFnetUnitTest.DataImport
{
    public partial class FormDataImport : Form
    {

        public System.Timers.Timer timerresInspection = new System.Timers.Timer(10000);
        
        //public void Form1_Load(object sender, EventArgs e)
        //{
        //    InitializeComponent();
        //    xmnamelistload();
        //    //xmname = "韶关铀业";
           
        //}
        public FormDataImport()
        {
            InitializeComponent();
        }
        
        //}

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TestImportCycdirnet testImportCycdirnet = new TestImportCycdirnet { listbox = this.listBox1, starttime = Convert.ToDateTime(this.dateTimePicker1.Text), endtime = Convert.ToDateTime(this.dateTimePicker2.Text), rows = Convert.ToInt32(this.comboBox1.Text), folderPath = this.textBox1.Text };
            ThreadPool.QueueUserWorkItem((state) => { testImportCycdirnet.main(); });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Thread importThread = new Thread(new ThreadStart(ImportDialog));
            importThread.SetApartmentState(ApartmentState.STA); //重点
            importThread.Start();

        }
        public void ImportDialog()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string foldPath = dialog.SelectedPath;

                this.textBox1.Invoke(new Action(() => { this.textBox1.Text = foldPath; }));

            }

            //this.textBox1.BeginInvoke(FlushEvent, fm.FileName);
            //this.textBox1.EndInvoke();
        }
       
      

    }
}
