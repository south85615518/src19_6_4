﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement;
using System.Threading;
using NFnet_BLL.DataProcess;
using Tool;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.AchievementGeneration.TotalStationDAL;
using System.IO;
using System.Windows.Forms;

namespace NFnetUnitTest.DataImport
{
   public  class TestImportCycdirnet
    {
       public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
       public ListBox listbox { get; set; }
       public int rows { get; set; }
       public string folderPath { get; set; }
       public string mssg = "";
       public string xmname = "";
        public void main(){
            //Thread t = new Thread(TestProcessImportCycdirnet);
            //t.Start();
            //ThreadStop();
            //TestTimeSort();
            //TestIsInsertData();
            //InsertDataCycGet();
            //InserCycStep();
            //IsInsertCycExist();
            //IsCycdirnetDataExist();
            //TestCycModelListGet();
            //TestCycModelUpdate();
            //TestResultDataCycInterfaceThisValModify();
            //TestProcessDirImport("广州北站基坑工程京广高铁设备监测", "d:\\北站数据", 1, 1);
            DataFileDescode();
        }
        public DateTime starttime { get; set; }
        public DateTime endtime { get; set; }
        public void DataFileDescode()
        {
            string[] dif = Directory.GetDirectories(folderPath);
            string xmname = "";
            int i = 1;
            foreach(string dirname in dif )
            {
                try
                {
                    xmname = Path.GetFileName(dirname);
                    TestProcessDirImport(xmname, dirname, i++, dif.Length);
                }
                catch (Exception ex)
                {
                    this.listbox.Invoke(new Action(() => { string.Format("项目{0}数据文件导入出错,错误信息:" + ex.Message,xmname); }));
                }
            }
        }




        public void TestProcessImportCycdirnet()
        {
            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据025058_结果数据_6月_4日_14时_50分_1秒.txt", "结果数据");
            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据035127_结果数据_6月_4日_15时_51分_1秒.txt", "结果数据");
            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("DTS2", Aspect.IndirectValue.GetXmnoFromXmname("DTS2"), "d:\\desktop\\tempDTS2结果数据101439_结果数据_6月_1日_10时_14分_1秒.txt", "结果数据");
            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempDTS2结果数据081525_结果数据_6月_1日_8时_15分_1秒.txt", "结果数据");
            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据125256_结果数据_6月_4日_12时_53分_1秒.txt", "结果数据");

            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("DTS2", Aspect.IndirectValue.GetXmnoFromXmname("DTS2"), "d:\\desktop\\tempUTS1结果数据055036_结果数据_4月_18日_17时_50分_1秒.txt", "结果数据");
            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据125112_结果数据_5月_16日_12时_50分_1秒.txt", "结果数据");
            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据055046_结果数据_4月_20日_17时_50分_1秒.txt", "结果数据");

            var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据084959_结果数据_4月_9日_8时_49分_1秒.txt", "结果数据");
            //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据055036_结果数据_4月_18日_17时_50分_1秒.txt", "结果数据");
            


            ProcessTotalStationBLL totalStationBLL = new ProcessTotalStationBLL();
            string mssg = "";
            totalStationBLL.ProcessFileDecodeTxt(processFileDecodeTxtModel,out mssg);
        }

        public void TestProcessDirImport(string xmname, string dirpath, int idex, int cont)
        {
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            filenamelist = filenamelist.Where(m => File.GetLastWriteTime(m) >= starttime && File.GetLastWriteTime(m) <= endtime).ToList();
            int i = 0;
            
            foreach (string filename in filenamelist)
            {
                if (filename.IndexOf("原始数据") != -1)
                {
                    i++; continue;
                    Console.WriteLine("****************原始数据(" + ++i + "/" + filenamelist.Count + "/" + idex + "/" + cont + ")****************");
                    Console.WriteLine("现在导入" + filename);
                    var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), filename, "原始数据");
                    //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据055036_结果数据_4月_18日_17时_50分_1秒.txt", "结果数据");
                    ProcessTotalStationBLL totalStationBLL = new ProcessTotalStationBLL { };
                    string mssg = "";
                    totalStationBLL.ProcessFileDecodeTxt(processFileDecodeTxtModel, out mssg);

                }
                else
                {

                    Console.WriteLine("****************结果数据(" + ++i + "/" + filenamelist.Count + "/" + idex + "/" + cont + ")****************");
                    Console.WriteLine("现在导入"+filename);
                    var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), filename, "结果数据");
                    //var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("UTS1", Aspect.IndirectValue.GetXmnoFromXmname("UTS1"), "d:\\desktop\\tempUTS1结果数据055036_结果数据_4月_18日_17时_50分_1秒.txt", "结果数据");
                    ProcessTotalStationBLL totalStationBLL = new ProcessTotalStationBLL {  listbox = listbox, rows = rows };
                    string mssg = "";
                    totalStationBLL.ProcessFileDecodeTxt(processFileDecodeTxtModel, out mssg);

                }

            }
        }


        public void TestCycInsertData()
        {
 
            //测试空数据时数据导入
            //测试重复导入 将主键字改为 项目名 点名 时间
            //测试数据正常导入（周期不连续 时间最新）
            //测试数据插入（周期大于最新周期 但时间不是最新）




        }
        public void TestTimeSort()
        {
            List<DateTime> ldt = new List<DateTime>();
            ldt.Add(DateTime.Now);
            Thread.Sleep(1000);
            ldt.Insert(0,DateTime.Now);
            Thread.Sleep(1000);
            ldt.Add(DateTime.Now);
            Thread.Sleep(1000);
            ldt.Insert(1,DateTime.Now);
            Thread.Sleep(1000);
            ldt.Insert(2,DateTime.Now);
            Thread.Sleep(1000);
            ldt.Add(DateTime.Now);
            ldt.Sort();
            int i = 0;
            foreach (var dt in ldt)
            {
                Console.WriteLine(i+":"+dt);
                i++;
            }
            ProcessPrintMssg.Print("");
        }


        public void TestIsInsertData()
        {
            //var IsInsertDataModel = new ProcessResultDataBLL.IsInsertDataModel("UTS1", DateTime.Now.AddMonths(0));
            //if (!processResultDataBLL.IsInsertData(IsInsertDataModel, out mssg)) ExceptionLog.ExceptionWrite(mssg);
            //ProcessPrintMssg.Print(mssg);
            
        }
        public void InsertDataCycGet()
        {
            var InsertDataCycGetModel = new ProcessResultDataBLL.InsertDataCycGetModel("UTS1", DateTime.Now);
            if (!processResultDataBLL.InsertDataCycGet(InsertDataCycGetModel, out mssg)) ExceptionLog.ExceptionWrite(mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void InserCycStep()
        {
            var InsertCycStepModel = new ProcessResultDataBLL.InsertCycStepModel("UTS1", 1454);
            if (!processResultDataBLL.InsertCycStep(InsertCycStepModel, out mssg))
                ExceptionLog.ExceptionWrite(mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void IsInsertCycExist()
        {
            var IsInsertCycExistModel = new ProcessResultDataBLL.IsInsertCycExistModel("UTS1", 1);
            processResultDataBLL.IsInsertCycExist(IsInsertCycExistModel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void IsCycdirnetDataExist()
        {
            var IsCycdirnetDataExistModel = new ProcessResultDataBLL.IsCycdirnetDataExistModel("UTS1", "U002C",DateTime.Now);
            processResultDataBLL.IsCycdirnetDataExist(IsCycdirnetDataExistModel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public ProcessCycModelListGet processCycModelListGet = new ProcessCycModelListGet();
        public void TestCycModelListGet()
        {
            processCycModelListGet.CycModelListGet("UTS1",2,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        ProcessCycdirnetBLL processCycdirnetBLL = new ProcessCycdirnetBLL();
        public void TestCycModelUpdate()
        {
            var processCycdirnetModel = new ProcessCycdirnetBLL.ProcessCycdirnetModelGetModel("UTS1", "U002C", 1);
            processCycdirnetBLL.ProcessCycdirnetModel(processCycdirnetModel, out mssg);
            ProcessPrintMssg.Print(mssg);
            processCycdirnetModel.model.IsControlPoint = processCycdirnetModel.model.IsControlPoint?false:true;
            processResultDataBLL.ProcessResultDataUpdate(processCycdirnetModel.model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }


        public void TestResultDataCycInterfaceThisValModify()
        {
            int cyc_right = -1,cyc_left = -1;
            Console.WriteLine("输入项目名称:");
            xmname = Console.ReadLine();

            do{
            Console.WriteLine("输入右接周期数:");
            cyc_right = Convert.ToInt32(Console.ReadLine());
            List<global::TotalStation.Model.fmos_obj.cycdirnet> interfacecyc_right = processCycModelListGet.CycModelListGet(xmname, cyc_right, out mssg);
            Console.WriteLine("输入左接周期数:");
            cyc_left = Convert.ToInt32(Console.ReadLine());
            List<global::TotalStation.Model.fmos_obj.cycdirnet> interfacecyc_left = processCycModelListGet.CycModelListGet(xmname, cyc_left, out mssg);

            List<string> points = (from m in interfacecyc_right select m.POINT_NAME).ToList();
            global::TotalStation.Model.fmos_obj.cycdirnet model_r = null;
            global::TotalStation.Model.fmos_obj.cycdirnet model_l = null;
            foreach(var pointname in points)
            {

                var model_rList = (from m in interfacecyc_right where m.POINT_NAME == pointname select m).ToList();
                var model_lList = (from m in interfacecyc_left where m.POINT_NAME == pointname select m).ToList();
                if (model_lList.Count == 0) continue;
                model_r = model_rList[0] as global::TotalStation.Model.fmos_obj.cycdirnet;
                model_l = model_lList[0] as global::TotalStation.Model.fmos_obj.cycdirnet;
                if (model_l == null) { Console.WriteLine(string.Format("左接周期数据中列表没有点名{0}的记录",pointname));continue; };
                model_r.This_dN = model_r.N = model_l.N;
                model_r.This_dE = model_r.E = model_l.E;
                model_r.This_dZ = model_r.Z = model_l.Z;
                processResultDataBLL.ProcessResultDataUpdate(model_r, out mssg);
                Console.WriteLine(mssg);
            }
            Console.WriteLine("输入任何非'T'键再次执行:");
            if (Console.ReadKey().KeyChar == 't') { mssg = "程序执行完!"; break; }

            }while(true);
            ProcessPrintMssg.Print(mssg);
        }


        public void ThreadStop()
        {
            Console.WriteLine("输入T结束线程");
            if (Console.ReadKey().KeyChar == 't')
            {
                Tool.ThreadProcess.Threads.Remove(string.Format("UTS1cycdirnet"));
            }
            ProcessPrintMssg.Print("程序结束!");
        }
    }
    

}
