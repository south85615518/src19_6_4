﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnetUnitTest.DataImport
{
    public class TestDTUPointAdd
    {
        public static string mssg = "";
        public static ProcessDTUPointBLL processDTUPointBLL = new ProcessDTUPointBLL();
        public static DTU.Model.dtu model = new DTU.Model.dtu
        {
            point_name = "DTU_2",
            id = 0,
            xmno = 29,
            firstAlarm = "一级预警",
            secalarm = "二级预警",
            thirdalarm = "三级预警",
            module = "M131576219793073357",
            pointtype = "水位",
            remark = "江门水位监测一号点"
        };
        public void main()
        {
            TestDTUPointNameAdd();
        }
        public void TestDTUPointNameAdd()
        {
            processDTUPointBLL.ProcessDTUPointAddBLL(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestDTUPointNameUpdate()
        {
            processDTUPointBLL.ProcessDTUPointUpdateBLL(model, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
