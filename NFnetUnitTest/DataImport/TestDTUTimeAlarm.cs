﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;

namespace NFnetUnitTest.DataImport
{
    public class TestDTUTimeAlarm
    {
        public ProcessDTUTimeAlarmBLL processDTUTimeAlarmBLL = new ProcessDTUTimeAlarmBLL();
        public ProcessDTUTimeAlarmTable processDTUTimeAlarmLoad = new ProcessDTUTimeAlarmTable();
        public ProcessDTUTimeAlarmTableCount processDTUTimeAlarmTableCount = new ProcessDTUTimeAlarmTableCount();
        public ProcessDTUTimeAlarmLoad processDTUTimeAlarmModelLoad = new ProcessDTUTimeAlarmLoad();
        public static string mssg = "";
        public DTU.Model.dtutimealarm task = new DTU.Model.dtutimealarm
        {
            xmno = 29,
            hour = 0,
            minute = 9,
            module = "4850002THSS",
            times = 1,
            sec = 0,
            time = DateTime.Now
        };
        public void main()
        {
            //testDTUTimeAlarmAdd();
            //testDTUTimeAlarmUpdate();
            //testDTUTimeAlarmTableLoad();
            //testDTUTimeAlarmTableCountLoad();
            testDTUTimeAlarmModelLoad();
        }
        public void testDTUTimeAlarmAdd()
        {
            processDTUTimeAlarmBLL.ProcessDTUTimeAlarmAdd(task, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUTimeAlarmUpdate()
        {
            task.minute = 10;
            processDTUTimeAlarmBLL.ProcessDTUTimeAlarmUpdate(task, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUTimeAlarmTableLoad()
        {
            processDTUTimeAlarmLoad.DTUTimeAlarmLoad(1, 20, 29, "module", "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUTimeAlarmTableCountLoad()
        {
            processDTUTimeAlarmTableCount.DTUTimeAlarmTableCount(29, " 1=1 ", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUTimeAlarmModelLoad()
        {
            processDTUTimeAlarmModelLoad.DTUTimeAlarmLoad(29, "4850001",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
