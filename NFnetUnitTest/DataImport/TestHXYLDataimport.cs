﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SqlHelpers;
using Tool;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnetUnitTest.DataImport
{
    public class TestHXYLDataimport
    {
        public static ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public static string mssg = "";
        public void main()
        {
            HXYLDataImport();
        }
        public void HXYLDataImport()
        {
            string sql = @"SELECT  [ID]
      ,[RID]
      ,[Rainfall]
      ,[Addr]
      ,[MonitorTime]
      ,[initRainFall]
  FROM [MeteorologyRaininfo]";
            DataTable[] dtary = new DataTable[4];

            querysql.querysqlserverstanderdb(sql, "SMOSDB2014", out dtary[0]);

            querysql.querysqlserverstanderdb(sql, "SMOSDB2015", out dtary[1]);

            querysql.querysqlserverstanderdb(sql, "SMOSDB2016", out dtary[2]);

            querysql.querysqlserverstanderdb(sql, "SMOSDB2017", out dtary[3]);

            DataTable dt = DataTableHelper.ProcessDataTableMerge(dtary.ToList());
            DataView dv = new DataView(dt);
            dv.Sort = "rid,monitortime asc";
            MDBDATA.Model.hxyl model = new MDBDATA.Model.hxyl();
            
            foreach(DataRowView drv in dv)
            {
                model = new MDBDATA.Model.hxyl { id = "1", val = Convert.ToDouble(drv["Rainfall"]), time = Convert.ToDateTime(drv["monitortime"]), send = false };
                processHXYLBLL.ProcessHXYLAddMDB(model,out mssg);
            }
            ProcessPrintMssg.Print(mssg);
        }
    }
}
