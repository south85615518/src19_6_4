﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement;

namespace NFnetUnitTest.DataImport
{
    class TestImportOrgldata
    {
        public void main()
        {
            //Thread t = new Thread(TestProcessImportOrgldata);
            //t.Start();
            //ThreadStop();
            TestProcessDirImportOrgldata();
        }
        public void TestProcessImportOrgldata()
        {
            var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("数据测试", Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), "e:\\net05第2周期数据1-2.txt", "原始数据");
            ProcessTotalStationBLL totalStationBLL = new ProcessTotalStationBLL();
            string mssg = "";
            totalStationBLL.ProcessFileDecodeTxt(processFileDecodeTxtModel, out mssg);
        }
        
        public void TestProcessDirImportOrgldata()
        {

            List<string> filenamelist = Tool.FileHelper.DirTravel("d:/广铁原始数据/广佛肇高速下穿武广高铁流溪河大桥监测/全站仪");
            var orgldatafilenames = (from m in filenamelist where m.IndexOf("_原始数据_") != -1 select m).ToList();
            foreach (string filename in orgldatafilenames)
            {
                var processFileDecodeTxtModel = new ProcessTotalStationBLL.ProcessFileDecodeTxtModel("广佛肇高速下穿武广高铁流溪河大桥监测", Aspect.IndirectValue.GetXmnoFromXmname("广佛肇高速下穿武广高铁流溪河大桥监测"), filename, "原始数据");
            ProcessTotalStationBLL totalStationBLL = new ProcessTotalStationBLL();
            string mssg = "";
            totalStationBLL.ProcessFileDecodeTxt(processFileDecodeTxtModel, out mssg);
            }
        }

        public void ThreadStop()
        {
            Console.WriteLine("输入T结束线程");
            if (Console.ReadKey().KeyChar == 't')
            {
                Tool.ThreadProcess.Threads.Remove(string.Format("数据测试cycdirnet"));
            }
            ProcessPrintMssg.Print("程序结束!");
        }
    }
}
