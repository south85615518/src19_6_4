﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile;

namespace NFnetUnitTest.DataImport
{
    public class TestGTXlsSensorDataImport
    {
        public ProcessGTSensorDataBLL processGTSensorDataBLL = new ProcessGTSensorDataBLL();
        public string mssg = "";
        public void main()
        {
            //GTSensorDataImport();
            //XlsDirImport("d:\\Desktop\\金马数据");
            GTXlsSensorDataImport();
        }
        public void GTXlsSensorDataImport()
        {
            var model = new NFnet_BLL.DataImport.ProcessFile.ProcessGTSensorDataBLL.ProcessFileDecodeTxtModel("新白广城际铁路下穿京广高铁施工监测", 96, "C:\\Users\\pc\\Documents\\WeChat Files\\wxid_d4heivirl1er22\\Files\\金马数据导出格式.xlsx", "金马设备数据");
            processGTSensorDataBLL.ProcessFileDecodeTxt(model,out mssg);
        }
        public void XlsDirImport(string dirpath)
        {
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            foreach(string filename in  filenamelist)
            {
                var model = new NFnet_BLL.DataImport.ProcessFile.ProcessGTSensorDataBLL.ProcessFileDecodeTxtModel("新白广城际铁路下穿京广高铁施工监测", 96,filename, "金马设备数据");
                processGTSensorDataBLL.ProcessFileDecodeTxt(model, out mssg);
            }
        }

        //public void TestResultSurveyDataImport(string xmname, int cyc, out string mssg)
        //{
        //    processResultSurveyDataImport.ResultSurveyDataImport(xmname, cyc, out mssg);
        //}
        //public void TestSurveyDataUpdate(string xmname, string pointname, string vSet_name, string vLink_name, int cyc, int srcdatacyc, out string mssg)
        //{
        //    processSurveyDataUpdate.SurveyDataUpdate(xmname, pointname, vSet_name, vLink_name, cyc, srcdatacyc, out  mssg);
        //}
        //public void TestSurveyPointCycDataAdd(string xmname, string pointname, int cyc, int importcyc, out string mssg)
        //{
        //    processSurveyPointCycDataAdd.SurveyPointCycDataAdd(xmname, pointname, cyc, importcyc, out  mssg);
        //}
        //public void TestLackPoints(string xmname, int xmno, int cyc, out string mssg)
        //{
        //    processLackPoints.LackPoints(xmname, xmno, cyc, out  mssg);
        //}
        //public void TestCgCycList(string xmname)
        //{
        //    processCgCycList.CgCycList(xmname, out mssg);
        //}
        //public void TestSurveyCYCDataDelete(int startCyc, int endCyc, string xmname, string pointname)
        //{
        //    processSurveyCYCDataDelete.SurveyCYCDataDelete(startCyc, endCyc, xmname, "", out mssg);
        //}
        //public void TestCgResultDataImport(string xmname, int cyc, int importcyc)
        //{
        //    processCgResultDataImport.CgResultDataImport(xmname, cyc, importcyc, out mssg);
        //}
        //public void TestDeleteCgDataCycOnChain(string xmname, int cyc, out string mssg)
        //{
        //    processDeleteCgDataCycOnChain.DeleteCgDataCycOnChain(xmname, cyc, out mssg);
        //}



    }
}
