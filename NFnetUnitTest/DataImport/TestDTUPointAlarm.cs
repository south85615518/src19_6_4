﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnetUnitTest.DataImport
{
    public class TestDTUPointAlarm
    {
        public ProcessDTUPointAlarmTableLoad processDTUPointAlarmTableLoad = new ProcessDTUPointAlarmTableLoad();
        public ProcessDTUPointAlarmMutilEdit processDTUPointAlarmMutilEdit = new ProcessDTUPointAlarmMutilEdit();
        public ProcessDTUPointAlarmModel processDTUPointAlarmModel = new ProcessDTUPointAlarmModel();
        public ProcessDTUList processDTUList = new ProcessDTUList();
        //public ProcessDTUPointAlarmTableLoad processDTUPointAlarmTableLoad = new ProcessDTUPointAlarmTableLoad();
        public static string mssg = "";
        public DTU.Model.dtu dtu = new DTU.Model.dtu
        {
            xmno = 29,
            firstAlarm = "一级预警",
            holedepth = 0,
            line = 1.77,
            module = "M131580524469324502",
            point_name = "DTU_1",
            pointtype = "水位计",
            remark = "江门水位一号点",
            secalarm = "二级预警",
            thirdalarm = "四级预警",
            id = 4

        };
        public ProcessDTUPointAlarmBLL processDTUPointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public void main()
        {
            //testDTUPointAlarmAdd();
            //testDTUPointAlarmUpdate();
            //testDTUPointAlarmMultiEdit();
            //testDTUPointALarmLoad();
            //testDTUPointAlarmModel();
            testDTUList();
        }
        public void testDTUPointAlarmAdd()
        {
            processDTUPointAlarmBLL.ProcessDTUPointAlarmAdd(dtu, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointAlarmUpdate()
        {
            processDTUPointAlarmBLL.ProcessDTUPointAlarmUpdate(dtu, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointAlarmMultiEdit()
        {
            processDTUPointAlarmMutilEdit.PointAlarmValueMutilEdit(dtu, "DTU_1','DTU_2", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointALarmLoad()
        {
            processDTUPointAlarmTableLoad.DTUPointAlarm(29, "point_name", 1, 20, "asc", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUPointAlarmModel()
        {
            DTU.Model.dtu dtumodel = processDTUPointAlarmModel.DTUPointAlarmModel(29, "DTU_1", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void testDTUList()
        {
            DataTable dt = processDTUList.DTUList(29, "数据测试", "  dtu.xmno,dtumodule.addressno,dtumodule.od,dtu.point_name  ", 1, 100, " asc ", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
    }
}
