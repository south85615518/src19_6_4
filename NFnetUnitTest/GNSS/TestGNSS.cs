﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.GNSS;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_MODAL;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnetUnitTest.GNSS
{
    public class TestGNSS
    {
        public ProcessGNSSPointLoadBLL processGNSSPointLoadBLL = new ProcessGNSSPointLoadBLL();
        public ProcessGNSSDATABLL processGNSSDATABLL = new ProcessGNSSDATABLL();
        public ProcessGNSSMaxTime processGNSSMaxTime = new ProcessGNSSMaxTime();
        public ProcessGNSSDATADbFill processGNSSDATADbFill = new ProcessGNSSDATADbFill();
        public ProcessGNSSCom processGNSSCom = new ProcessGNSSCom();
        public ProcessGNSSDataRqcxConditionCreate processGNSSDataRqcxConditionCreate = new ProcessGNSSDataRqcxConditionCreate();
        public ProcessGNSSChart processGNSSChart = new ProcessGNSSChart();
        public static ProcessGNSSDATADelete processGNSSDATADelete = new ProcessGNSSDATADelete();

        //public static ProcessGNSSDataDelete processGNSSDataDelete = new ProcessGNSSDataDelete();
        public static ProcessGNSSPointNewestDateTimeGet processGNSSPointNewestDateTimeGet = new ProcessGNSSPointNewestDateTimeGet();
        public static ProcessGNSSDateTime processGNSSDateTime = new ProcessGNSSDateTime();
        public static string mssg = "";
        public string sql = "";
        public zuxyz[] zus;
        public void main()
        {
            //TestGNSSPointLoad();
            GNSSDataLoad();
            //GNSSChart();
            //TestGNSSMaxTime();
            //TestGNSSDATADelete();
            //TestGNSSPointNewestDateTimeGet();
            //TestGNSSDateTime();
        }
        public void TestGNSSPointLoad()
        {
            List<string> pointlist = processGNSSPointLoadBLL.GNSSPointLoadBLL(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void GNSSDataLoad()
        {
            var gnssDataRqcxConditionCreateModel = processGNSSDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018-1-20 00:00:00", "2018-1-24 00:00:00", QueryType.RQCX, "", "从化边坡监测", new DateTime());

            //var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition("GPS1,GPS2", gnssDataRqcxConditionCreateModel.sql, 29);
            sql = gnssDataRqcxConditionCreateModel.sql;
            var model = processGNSSDATADbFill.GNSSDbFill("sg01,sg02", gnssDataRqcxConditionCreateModel.sql, 29);
            ProcessPrintMssg.Print(string.Format("获取到项目编号{0}的GNSS数据表记录{1}条", 29, model.dt.Rows.Count));
        }
        public void GNSSChart()
        {
            GNSSDataLoad();
            List<serie> ls = processGNSSChart.SerializestrGNSS(sql, "数据测试", "GPS1,GPS2", processGNSSCom.getGNSSzu(),out mssg);
            
            ProcessPrintMssg.Print(string.Format("生成项目编号{0}的数据曲线{1}条",29,ls.Count));
        }
        public void TestGNSSMaxTime()
        {
            processGNSSMaxTime.GNSSMaxTime("sg01,sg02,sg03",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGNSSDATADelete()
        {
            processGNSSDATADelete.GNSSDATABLLDelete("韶关铀业", "GPS2", DateTime.Parse("2018/1/31 13:35:56"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGNSSPointNewestDateTimeGet()
        {
            processGNSSPointNewestDateTimeGet.GNSSPointNewestDateTimeGet("sg01", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGNSSDateTime()
        {
            DateTime dt = processGNSSPointNewestDateTimeGet.GNSSPointNewestDateTimeGet("sg01", out mssg);
            processGNSSDateTime.GNSSDateTime("sg01", dt, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

    }
}
