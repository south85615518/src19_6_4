﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using NFnet_Interface.DisplayDataProcess.Gauge;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_MODAL;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_Interface.DisplayDataProcess.GAUGE;
using NFnet_BLL.DisplayDataProcess.GAUGE;
//using NFnet_BLL.DisplayDataProcess.Gauge;

namespace NFnetUnitTest.GAUGE
{
    public class TestGauge
    {
        public ProcessGaugePointLoadBLL processGaugePointLoadBLL = new ProcessGaugePointLoadBLL();
        public ProcessGaugeDATABLL processGaugeDATABLL = new ProcessGaugeDATABLL();
        public ProcessGaugeMaxTime processGaugeMaxTime = new ProcessGaugeMaxTime();
        public ProcessGaugeDATADbFill processGaugeDATADbFill = new ProcessGaugeDATADbFill();
        public ProcessDTUSenorCom processGaugeCom = new ProcessDTUSenorCom();
        public ProcessGaugeDataRqcxConditionCreate processGaugeDataRqcxConditionCreate = new ProcessGaugeDataRqcxConditionCreate();
        public ProcessGaugeChart processGaugeChart = new ProcessGaugeChart();
        public static ProcessGaugeDATADelete processGaugeDATADelete = new ProcessGaugeDATADelete();

        public static ProcessGAUGEPointNewestDateTimeGet processGAUGEPointNewestDateTimeGet = new ProcessGAUGEPointNewestDateTimeGet();
        public static ProcessGaugeDateTime processGAUGEDateTime = new ProcessGaugeDateTime();

        public static string mssg = "";
        public string sql = "";
        public zuxyz[] zus;
        public void main()
        {
            //TestGaugePointLoad();
            GaugeDataLoad();
            //GaugeChart();
            //TestGaugeMaxTime();
            //TestGaugeDATADelete();
            //TestGAUGEPointNewestDateTimeGet();
            //TestGAUGEDateTime();
        }
        public void TestGaugePointLoad()
        {
            List<string> pointlist = processGaugePointLoadBLL.GaugePointLoadBLL(29,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void GaugeDataLoad()
        {
            var gnssDataRqcxConditionCreateModel = processGaugeDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018-1-25 00:00:00", "2018-1-26 00:00:00", QueryType.RQCX, "", "数据测试", new DateTime());

            //var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition("GPS1,GPS2", gnssDataRqcxConditionCreateModel.sql, 29);
            sql = gnssDataRqcxConditionCreateModel.sql;
            var model = processGaugeDATADbFill.GaugeDbFill("韶关库水位", gnssDataRqcxConditionCreateModel.sql, 29);
            ProcessPrintMssg.Print(string.Format("获取到项目编号{0}的Gauge数据表记录{1}条", 29, model.dt.Rows.Count));
        }
        public void GaugeChart()
        {
            GaugeDataLoad();
            List<serie> ls = processGaugeChart.SerializestrGauge(sql, "数据测试", "GPS1,GPS2", out mssg);
            
            ProcessPrintMssg.Print(string.Format("生成项目编号{0}的数据曲线{1}条",29,ls.Count));
        }
        public void TestGaugeMaxTime()
        {
            processGaugeMaxTime.GaugeMaxTime("韶关库水位",out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGaugeDATADelete()
        {
            processGaugeDATADelete.GaugeDATABLLDelete("韶关铀业", "韶关库水位", DateTime.Parse("2018-01-16 10:32:57"), out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGAUGEPointNewestDateTimeGet()
        {
            processGAUGEPointNewestDateTimeGet.GAUGEPointNewestDateTimeGet("韶关库水位", out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestGAUGEDateTime()
        {
            Console.WriteLine("开始执行1/2  " + DateTime.Now);
            DateTime dt = processGAUGEPointNewestDateTimeGet.GAUGEPointNewestDateTimeGet("韶关库水位", out mssg);
            Console.WriteLine("开始执行2/2  " + DateTime.Now);
            processGAUGEDateTime.GaugeDateTime("韶关库水位", dt, out mssg);
            Console.WriteLine("执行完成" + DateTime.Now );
            ProcessPrintMssg.Print(mssg);
        }
    }
}
