﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers.dbcreate;
using MySql.Data.MySqlClient;
using SqlHelpers;
using System.IO;
using System.Data.Odbc;

namespace NFnetUnitTest.MysqlClientTest
{
    public class TestMysqlClient
    {
        public static database db = new database();
        public string dbname = ""; 
        public void main()
        {
            //TestProcessMysqlClient();
            Console.Write("输入数据库名称");
           dbname = Console.ReadLine();
           TestOdbcConn();
        }
        public void TestProcessMysqlClient()
        {
            //dbcreate dbc = new dbcreate();
            //dbc.createdb("s7");
            ////string sql = " SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'fmosdata'";


            //MySqlConnection conn = TargetBaseLink.GetMysqlConnect("s7");
            //FileInfo file = new FileInfo("D:\\day02\\_mvmp\\DAY02\\SQLScript\\cgtotalstation.sql");  //filename是sql脚本文件路径。  
            //string sql = file.OpenText().ReadToEnd();
            //MySqlScript script = new MySqlScript(conn);
            //script.Query = sql;
            //int count = script.Execute();
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect("s7");
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(@"drop TRIGGER if  EXISTS  hotmapnewinsert", conn);
            udb.UpdateStanderDB(@"create trigger hotmapnewinsert before insert
on monitoringpointlayout for each row
begin
declare c,cg int;
set c = (select IFNULL(max(alarm),0)  from pointcheck  where xmno = new.xmno and point_name = new.pointName);
set cg = (select IFNULL(max(alarm),0)  from cgs7.pointalarm  where xmno = new.xmno and point_name = new.pointName);
set new.color =c;
set new.cgcolor =c;
end", conn);
            //string a = string.Format("", "a");

        }
        public void TestOdbcConn()
        {
            try
            {
                OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            }
            catch (Exception ex)
            {
                ProcessPrintMssg.Print(ex.Message);
            }
            
        }
    }
}
