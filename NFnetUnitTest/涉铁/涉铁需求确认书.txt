﻿
涉铁工程安全监测预警系统需求规格说明书
单位信息管理
通过云平台可以创建新的单位，管理单位信息，管理单位人员信息。
1.超级管理员创建新的单位，录入单位信息，同时创建单位的项目数据库
2.角色划分：系统管理员，管理员，监测员，监督员
系统管理员：创建单位信息，添加项目管理员，创建单位数据库。
管理员：创建项目，添加监测员信息，添加监督员信息，将项目的检测和查看权限分配给监测员和监督员
监测员：
项目信息管理：查看项目基本信息,上传项目监测方案,设备和传感器参数设置，测点设置，测点预警参数设置，监测平面布置图点位设置。
项目数据浏览：查看某一时间段某些测点的数据表和变化曲线。
项目数据录入：采集数据将数据存储到平台的数据库，通过TCP/IP协议以数据文件的形式上传到平台。也可以从平台以手动的录入和手动上传的形式上传。
成果整理：将生产原始数据通过删减、整理、合并的方式生成成果数据存入成果库中。
预警处理：分为自检处理和预警处理，自检处理是在采集数据录入平台之后依据输入的预警参数进行判断如有超限将触发预警，并记录在自检库中，并主动给项目相关的监测员发送预警通知。预警处理是在监测员对数据进行成果处理后出发的预警，预警参数和自检参数是相同的，触发机制也一样，但都是由数据入库，对新入库的数据进行超限检查产生。
自检处理的预警通知是自动发送给监测员，而预警处理是通过监测员确认之后，勾选几条或者全部的预警通知合并之后以邮件或短信[可选]方式发送给项目管理员和监督员。
数据下载：监测员可以下载上传的历史数据文件。
监督员：
项目分布图：登录进平台后首先进入项目分布地图，能在电子地图上查看到所有项目的地理位置分布，并能通过项目图标的颜色识别该项目当前的安全状态。在地图的右侧
项目信息浏览：浏览相关联的所有项目的基本信息，监测方案，测点信息。打开监测平面布局总图可以看到该项目的所有布点信息，并用红、橙、黄、绿分别表示三级，二级，一级，正常的4种预警状态，在平面的旁边可以看到该项目的所有预警信息。点击信息条中的预警点号可以再平面图上打开该点的最新数据情况。
项目数据浏览：浏览所有的成果数据，这里的成果数据全部由成果生成存入到成果库里的数据，查看数据曲线和数据表。
查看历史预警信息。
报表管理：设置日报,周报月报的生成条件，比如日报是每天的24点，周报是每周日的24点，月报是每月的28号，勾选需要自动生成报表的量，则系统将在每天的24点，每周日24点生成这一周的报表和每月的28号生成这一个月的报表以邮件的形式发送给监测员。
数据查询之后生成的成果可以通过报表导出功能导出下载到本地。
其中管理员权限包括监测员的权限;监测员的权限包括监督员的权限。
监测仪器包括：全站仪，静力水准仪，应力计，水位计，支撑轴力，倾斜仪，测斜仪，分层沉降。
平台将采集监测仪器采集的原始数据转换成特定监测量的某个测点的生产数据。
监测量分为：表面位移，深部位移，地下水位，支撑轴力，沉降。


