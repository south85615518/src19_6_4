﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vibration2.Model;
using NFnet_BLL.InterfaceServiceBLL;

//测振仪数据
namespace NFnetUnitTest.Vibration
{
    public class TestVibration
    {
        string mssg = "";
        private readonly NFnet_BLL.Vibration.ProcessVibrationBLL bll = new NFnet_BLL.Vibration.ProcessVibrationBLL();

        //添加
        public void TestAddVibration()
        {
            vibration model = new vibration()
            {
                xmno = 96,
                point_name = "点名",
                time = DateTime.Now.ToLocalTime(),
                XMax = 10,
                YMax = 10,
                ZMax = 10,
                DataFilePath = "D://",
                millionsec = 50
            };
            
             bll.ProcessAddVibration(model, out mssg);
             Console.WriteLine(mssg);
        }

        //更新
        public void TestUpdateVibration()
        {
            vibration model = new vibration()
            {
                xmno = 96,
                point_name = "123",
                time = DateTime.Now.ToLocalTime(),
                XMax = 20,
                YMax = 10,
                ZMax = 10,
                DataFilePath = "D://",
                millionsec = 50
            };

            bll.ProcessUpdateVibration(model, out mssg);
            Console.WriteLine(mssg);
        }

        //根据项目编号 点名 时间 分页获取测振仪数据List
        public void TestGetVibrationList()
        {
            NFnet_Interface.Vibration.ProcessGetVibrationList test = new NFnet_Interface.Vibration.ProcessGetVibrationList();
            test.GetVibrationList(96, "123", DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1), 1, 10, "point_name", "desc", out mssg);
            Console.WriteLine(mssg);
        }

        //根据项目编号 点名 时间 分页获取测振仪数据List总行数
        public void TestGetVibrationListCount()
        {
            NFnet_Interface.Vibration.ProcessGetVibrationListCount test = new NFnet_Interface.Vibration.ProcessGetVibrationListCount();
            test.GetVibrationListCount(96, "123", DateTime.Now.AddDays(-1), DateTime.Now.AddDays(1), out mssg);
            Console.WriteLine(mssg);
        }
    }
}
