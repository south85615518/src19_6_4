﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using data.Model;
namespace NFnetUnitTest.ToolTest
{
    public class TestJsonSerialize
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public class user {
            public string name { get;set; }
            public string pass { get; set; } 
        }

        public static void _Main(string[] args)
        {
            List<DateTime> datetimelsit = new List<DateTime>();

            datetimelsit.Add(DateTime.Now);
            datetimelsit.Add(DateTime.Now.AddMinutes(-20));
            datetimelsit.Add(DateTime.Now.AddHours(-2));
            datetimelsit.Add(DateTime.Now.AddHours(-1));
            datetimelsit.Add(DateTime.Now);
            datetimelsit.Add(DateTime.Now);
            DateTime dtmin= Tool.DateHelper.GetMinDateTimeFromTimeList(datetimelsit, 2);



            //TestSerialize();
            TestModelDeserialize();
        }
        public void TestSerialize()
        {
            List<user> userlist = new List<user>();
            userlist.Add(new user { name="Tom", pass="123" });
            userlist.Add(new user { name = "Joe", pass = "123" });
            string jsonstr = jss.Serialize(userlist);
            ProcessPrintMssg.Print("----序列化----");
            ProcessPrintMssg.Print(jsonstr);
            ProcessPrintMssg.Print("----反序列化----");
            userlist = jss.Deserialize<List<user>>(jsonstr);
            userlist.ForEach(m => { ProcessPrintMssg.Print(string.Format("name:{0}  pass:{1}",m.name,m.pass)); });
        }
        public static void TestModelDeserialize()
        {
       
            string str = @"{
                    'sn': 1,
                    'file': {
                        'RecTime': '2019-05-20T08:41:10.5743981+08:00',
                        'FileLen': 1,
                        'Data': [
      [
        1.1,
        2.1
      ],
      [
        1.1,
        2.1
      ]
    ],
                        'MaxVal': 1.1,
                        'FileNo': 1
                    },
                    'Key': 'sample string 2',
                    'MaxValue': [
    1.1,
    2.1
  ],
                    'FileNo': 3,
                    'Type': 'sample string 4'
                }";
            var model = jss.Deserialize<dataRequestInfo>(str);

            
        
        }
    }
}
