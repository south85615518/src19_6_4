﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace NFnetUnitTest.ToolTest
{
    class TestTool
    {
        public void main() {
            //TestFileUnZip();
            //FileHelperTest();
            //FileInfoListTest();
            //FileUnion();
            //TestByteArray();
            //FileInfoListTest();
            //TestUnicodeConvert();
            TestGetNumFromString();
        }
        public void TestFileUnZip()
        {
            string dirpath = Path.GetDirectoryName("d:\\Desktop\\日报.zip");
            Tool.FileHelper.unZipFile("d:\\Desktop\\日报.zip", dirpath);
        }
        public void FileHelperTest()
        {
            Tool.FileHelper.ProcessFileInfoList("D:/测点文件_测点_2月_27日_16时_12分_1秒.txt");
        }
        public void FileInfoListTest()
        {
            List<string> ls = Tool.FileHelper.ProcessFileInfoList("d:\\Desktop\\表面位移修正值\\UTS1\\offset.txt");
            List<Tool.com.Offset> ltf = Tool.com.OffsetHelper.OffsetModelListLoad(ls);
            DataTable dt = new DataTable();
            dt.Columns.Add("A");
            dt.Columns.Add("B");
            DataRow dr = dt.NewRow();
            dr["A"] = 0;
            dr["B"] = 1;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["A"] = 0;
            dr["B"] = 2;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["A"] = 1;
            dr["B"] = 3;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["A"] = 1;
            dr["B"] = 4;
            dt.Rows.Add(dr);
           DataTable offsetTab = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(dt,null);
        }
        public void FileUnion()
        {
            Tool.FileHelper.FileCopy("C:\\Users\\pc\\Desktop\\triggers_", "C:\\Users\\pc\\Desktop\\triggers_\\unionpath.txt");
        }
        public void TestByteArray()
        {
            string hexstring = @"
FF 1C 00 07 00 21 11 01 44 5B 20 EF 58 03 64 5E BA 81 40 15 64 00 00 00 00 21 64 00 00 00 00 60 8E
FF 1C 00 07 00 21 11 01 44 5B 20 FE D0 03 64 FC A9 81 40 15 64 00 00 00 00 21 64 00 00 00 00 FE 67
FF 1C 00 07 00 21 11 01 44 5B 21 0E 48 03 64 35 5E 82 40 15 64 00 00 00 00 21 64 00 00 00 00 97 6C 
FF 1C 00 07 00 21 11 01 44 5B 21 1D C0 03 64 D3 4D 82 40 15 64 00 00 00 00 21 64 00 00 00 00 43 54 
FF 1C 00 07 00 21 11 01 44 5B 21 2D 38 03 64 C1 CA 81 40 15 64 00 00 00 00 21 64 00 00 00 00 DF 18 
FF 1C 00 07 00 21 11 01 44 5B 21 3C B0 03 64 5C 8F 82 40 15 64 00 00 00 00 21 64 00 00 00 00 C5 2C 
FF 1C 00 07 00 21 11 01 44 5B 21 4C 28 03 64 37 89 81 40 15 64 00 00 00 00 21 64 00 00 00 00 6B 75 
FF 1C 00 07 00 21 11 01 44 5B 21 5B A0 03 64 0E 2D 82 40 15 64 00 00 00 00 21 64 00 00 00 00 A2 D4 
FF 1C 00 07 00 21 11 01 44 5B 21 6B 18 03 64 AC 1C 82 40 15 64 00 00 00 00 21 64 00 00 00 00 6B 96 
FF 1C 00 07 00 21 11 01 44 5B 21 7A 90 03 64 D3 4D 82 40 15 64 00 00 00 00 21 64 00 00 00 00 AE 47 
FF 1C 00 07 00 21 11 01 44 5B 21 8A 08 03 64 71 3D 82 40 15 64 00 00 00 00 21 64 00 00 00 00 7C E3 
FF 1C 00 07 00 21 11 01 44 5B 21 99 80 03 64 0E 2D 82 40 15 64 00 00 00 00 21 64 00 00 00 00 41 E0 
FF 1C 00 07 00 21 11 01 44 5B 21 A8 F8 03 64 AC 1C 82 40 15 64 00 00 00 00 21 64 00 00 00 00 89 62 
FF 1C 00 07 00 21 11 01 44 5B 21 B8 70 03 64 4A 0C 82 40 15 64 00 00 00 00 21 64 00 00 00 00 5A 68 
FF 1C 00 07 00 21 11 01 44 5B 21 C7 E8 03 64 85 EB 81 40 15 64 00 00 00 00 21 64 00 00 00 00 FB 3C 
FF 1C 00 07 00 21 11 01 44 5B 21 D7 60 03 64 23 DB 81 40 15 64 00 00 00 00 21 64 00 00 00 00 39 DE 
FF 1C 00 07 00 21 11 01 44 5B 21 E6 D8 03 64 9A 99 81 40 15 64 00 00 00 00 21 64 00 00 00 00 B4 93 
FF 1C 00 07 00 21 11 01 44 5B 21 F6 50 03 64 37 89 81 40 15 64 00 00 00 00 21 64 00 00 00 00 11 5E 
FF 1C 00 07 00 21 11 01 44 5B 22 05 C8 03 64 5E BA 81 40 15 64 00 00 00 00 21 64 00 00 00 00 A0 AC 
FF 1C 00 07 00 21 11 01 44 5B 22 15 40 03 64 C1 CA 81 40 15 64 00 00 00 00 21 64 00 00 00 00 F0 3C 
FF 1C 00 07 00 21 11 01 44 5B 22 24 B8 03 64 D3 4D 82 40 15 64 00 00 00 00 21 64 00 00 00 00 51 A1";
            string[] hexstringarray = hexstring.Split(' ','\n','\r');
            List<byte> bytelist = new List<byte>();
            int i = 0;
            for(i = 0; i < hexstringarray.Length;i++)
            {
                if (hexstringarray[i] == "") continue;
                bytelist.Add(Convert.ToByte("0x"+hexstringarray[i],16));
            }
            //string str = Tool.com.HexHelper.ByteToHexString(bytelist.ToArray());
            List<byte[]> lb = Tool.com.ByteArrayHelper.ByteArrayToArrayList(bytelist.ToArray(),new byte[2]{0xff,0x1c});
            List<string> strlist = new List<string>();
            foreach(byte[] bytes in lb)
            {
                strlist.Add(Tool.com.HexHelper.ByteToHexString(bytes));
            }
            string hecsrc = string.Join("\r\n",strlist);
        }


        public void TestUnicodeConvert()
        {
            //string mssg = Tool.com.UnicodeHelper.MyUrlDeCode("%E9%92%A2%E7%AD%8B%E5%BA%94%E5%8A%9B", Encoding.GetEncoding("gbk"));
            //ProcessPrintMssg.Print(mssg);

        }
        public void TestGetNumFromString()
        {
            string numstr = Tool.com.StringHelper.GetNumFromString("数据报表贺州高铁监测_表面位移_第999周期-第1021周期多点单周期数据29.xlsx");
            ProcessPrintMssg.Print(numstr);
        }

    }
}
