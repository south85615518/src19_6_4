﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;

namespace NFnetUnitTest.resultDataTest
{
    public class TotalStationResultDataTest
    {
        public static ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public static ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public string mssg="";
        public void main() {
            //TestProcessResultdataTableLoad();
            //TestProcessCgResultdataTableLoad();
            //TestProcessResultDataMaxTime();
            TestProcessResultDataPointDateTimeCycGet();
        }
        public void TestProcessResultdataTableLoad()
        {
            string xmname = "数据测试";
            string pointname = "Z7-4";
            //var processResultDataLoadModel = new ProcessResultDataBLL.ProcessResultDataLoadModel(xmname,pointname,1,1000,1,10);
            //resultDataBLL.ProcessResultDataLoad(processResultDataLoadModel,out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessCgResultdataTableLoad()
        {
            string xmname = "数据测试";
            string pointname = "Z7-4";
            //var processResultDataLoadModel = new ProcessResultDataBLL.ProcessResultDataLoadModel(xmname, pointname, 1, 1000, 1, 10);
            //resultCgDataBLL.ProcessResultDataLoad(processResultDataLoadModel, out mssg);
            //ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessResultDataMaxTime()
        {
            string xmname = "数据测试";
            var processResultDataMaxTime = new TotalStationResultDataMaxTimeCondition(xmname);
            var processResultDataMaxTimeCom = new ProcessResultDataCom.ProcessResultDataMaxTimeModel(processResultDataMaxTime, Role.administratrorModel, false);
            resultDataCom.ProcessResultDataMaxTime(processResultDataMaxTimeCom, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void TestProcessResultDataPointDateTimeCycGet()
        {
            //var processPointDateTimeCycGetModel = new ProcessResultDataBLL.ProcessPointDateTimeCycGetModel("106", "JZ01", Convert.ToDateTime("2018-09-19 10:48:31"));
            //resultDataBLL.ProcessPointDateTimeCycGet(processPointDateTimeCycGetModel,out mssg);
            //ProcessPrintMssg.Print(mssg);
        }



    }
}
