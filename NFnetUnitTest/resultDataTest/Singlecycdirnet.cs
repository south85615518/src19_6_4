﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess;

namespace NFnetUnitTest.resultDataTest
{
   public class Singlecycdirnet
    {
        public static ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public static string mssg = "";
        public void main()
        {
            //TestSinglecycdirnet();
            TestSinglecycdirnetdata();
        }
        public void TestSinglecycdirnet()
        {
            var processSinglepointcycloadmodel = new SinglepointcycloadCondition("数据测试","Z7-4");
            resultDataBLL.ProcessSinglepointcycload(processSinglepointcycloadmodel,out mssg);
            DataView dv = new DataView(processSinglepointcycloadmodel.dt);
            foreach(DataRowView drv in dv)
            {
                Console.Write(string.Format("周期{0}日期{1}\n",drv["cyc"].ToString(),drv["time"].ToString()));
            }
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSinglecycdirnetdata()
        {
            var processsSinglecycdirnetdataloadmodel = new SinglecycdirnetdataloadCondition("数据测试","Z7-5",0,10);
            resultDataBLL.ProcesssSinglecycdirnetdataload(processsSinglecycdirnetdataloadmodel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
       

    }
}
