﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.UserProcess;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnetUnitTest.AlarmTest
{
    public class TestCgCycdirnetBLL
    {
        public DateTime  dt= new DateTime();
        public ProcessCgResultDataAlarmBLL dataAlarmBLL = new ProcessCgResultDataAlarmBLL();
        public ProcessAdministratorAlarmBLL administratorAlarmBLL = new ProcessAdministratorAlarmBLL();
        public MailHelper mail = new MailHelper();
        public string mssg = "";
        public void main()
        {

            while(true)
            {
                try
                {
                    
                    Console.WriteLine("从____时候开始预警");
                    dt = Convert.ToDateTime(Console.ReadLine());
                    dataAlarmBLL = new ProcessCgResultDataAlarmBLL("数据测试",29,dt.ToString());
                    dataAlarmBLL.main();
                    Console.WriteLine("是否将预警信息发送给管理员Y/N");
                    if (Console.ReadKey().KeyChar == 'y')
                    {
                        //将预警信息打包发送给管理员
                        int sec = 0;
                        //邮件群发
                        if (dataAlarmBLL.alarmInfoList.Count > 5)
                        {
                            var processXmAdministatorModel = new ProcessAdministratrorBLL.ProcessXmAdministatorModel(dataAlarmBLL.xmname);
                            ProcessAdministratrorBLL.ProcessXmAdministator(processXmAdministatorModel, out mssg);
                            //管理员预警通知存库
                            
                            //监测员预警通知存库
                            AuthorityAlarm.Model.adminalarm adminAlarm = new AuthorityAlarm.Model.adminalarm
                            {
                                xmno = Aspect.IndirectValue.GetXmnoFromXmname(dataAlarmBLL.xmname),
                                context = string.Join(",", dataAlarmBLL.alarmInfoList.Where(s => (s != "")).ToList()),
                                sendTime = DateTime.Now,
                                confirm = false,
                                mail = false,
                                adminno =  processXmAdministatorModel.model.userId,
                                dataType = 1,
                                ForwardTime = 0,
                                mess = false,
                                aid = string.Format("A{0}{1}", processXmAdministatorModel.model.userId, DateHelper.DateTimeToString(dt.AddSeconds(++sec)))
                            };
                            if (!administratorAlarmBLL.ProcessAdministrtorAlarmAdd(adminAlarm, out mssg))
                            {
                                ProcessPrintMssg.Print(mssg);
                                return ;
                            }

                                //监测员预警通知发送邮件
                                int t = 0;
                                while (t < 3)
                                {
                                    //重发三次
                                    if (!mail.Small(string.Join("<p>", dataAlarmBLL.alarmInfoList.Where(s => (s != "")).ToList()), processXmAdministatorModel.model.email, string.Format("{0}预警信息通知", dataAlarmBLL.xmname), out mssg))
                                    {
                                        t++;
                                    }
                                    else
                                    {
                                        //更新管理员短信标志
                                        adminAlarm.mess = true;
                                        if (!administratorAlarmBLL.ProcessAdministrtorAlarmUpdate(adminAlarm, out mssg))
                                        {
                                            ProcessPrintMssg.Print(mssg);
                                            return ;
                                        }
                                        break;
                                    }
                                }

                            }


                        }

                    Console.WriteLine("按任意非T键执行");
                    if (Console.ReadKey().KeyChar == 't')
                        break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("程序出错，按任意非T键继续");
                    if (Console.ReadKey().KeyChar == 't')
                        break;
                }
            }
        }




    }
}
