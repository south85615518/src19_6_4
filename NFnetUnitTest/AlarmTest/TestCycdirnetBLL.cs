﻿using System.Text;
using NFnet_BLL.Controlsurvey.TotalStationBLL;
using FmosWebServer;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using System;
using System.Collections.Generic;
using TotalStation.Model.fmos_obj;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using Tool;
namespace NFnetUnitTest.AlarmTest
{

    class TestCycdirnetBLL
    {
        public static WebServer server = new WebServer("192.168.168.15", 8092);
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public static string mssg = "";
        public string xmname = "数据测试";
        public static string timeJson = "";
        public static ProcessResultDataBLL resultBLL = new ProcessResultDataBLL();
        public void main()
        {
            TestCycdirnetModelList();
            //TestPointAlarmValue("sp1");
        }
        public void TestCycdirnetModelList()
        {
            var processResultDataAlarmModelListModel = new ProcessResultDataBLL.ProcessResultDataAlarmModelListModel(xmname);
            object obj;
            if (/*resultBLL.ProcessResultDataAlarmModelList(processResultDataAlarmModelListModel, out obj, out mssg)*/true)
            {
                List<cycdirnet> lc = (List<cycdirnet>)null;
                CycdirnetPointAlarm(lc);
            }
            
                //ProcessPrintMssg.Print(mssg);
           
        }
        public PointAttribute.Model.fmos_pointalarmvalue TestPointAlarmValue(string pointName)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointName);
            PointAttribute.Model.fmos_pointalarmvalue model = null;
            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print(mssg);
                //TestAlarmValueList(processPointAlarmModelGetModel.model);
                return processPointAlarmModelGetModel.model;

            }
            return null;
        }
        public List<NFnet_DAL.MODEL.alarmvalue> TestAlarmValueList(PointAttribute.Model.fmos_pointalarmvalue pointalarm)
        {
            List<NFnet_DAL.MODEL.alarmvalue> alarmvalueList = new List<NFnet_DAL.MODEL.alarmvalue>();
            //一级
            var processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(pointalarm.xmno, pointalarm.FirstAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("一级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(pointalarm.xmno, pointalarm.SecondAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("二级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(pointalarm.xmno, pointalarm.ThirdAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("三级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<NFnet_DAL.MODEL.alarmvalue> levelalarmvalue, cycdirnet resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel,Aspect.IndirectValue.GetXmnoFromXmname(xmname));
            if (pointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
              //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));

                ExceptionLog.ExceptionWrite(processPointAlarmfilterInformationModel.ls);
            }
        }
        public void CycdirnetPointAlarm(List<cycdirnet> lc)
        {
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format("===================================={0}=================================", DateTime.Now));
            ls.Add("\n");
            ExceptionLog.ExceptionWrite(ls);
            foreach (cycdirnet cl in lc)
            {
                PointAttribute.Model.fmos_pointalarmvalue pointvalue = TestPointAlarmValue(cl.POINT_NAME);
                List<NFnet_DAL.MODEL.alarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);
                
            }
            //Console.ReadLine();
        }
    }
}
