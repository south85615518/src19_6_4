﻿using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using System;
using System.Collections.Generic;
using TotalStation.Model.fmos_obj;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using Tool;
using NFnet_BLL.AchievementGeneration.TotalStationDAL;
using NFnet_BLL.LoginProcess;

namespace NFnetUnitTest.AlarmTest
{
  public  class TestTimeCycdirnetBLL
    {
        
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public static string mssg = "";
        public string xmname = "数据测试";
        public static string timeJson = "";
        public static ProcessResultDataBLL resultBLL = new ProcessResultDataBLL();
        public ProcessCycdirnetBLL cycdirnetBLL = new ProcessCycdirnetBLL();
        public ProcessResultDataAlarmBLL resultDataAlarmBLL = new ProcessResultDataAlarmBLL();
        public ProcessCycdirnetCom cycdirnetCom = new ProcessCycdirnetCom();
        public DateTime dt ;
        public string pointName;
        public void main(){

            char ch;
            while (true)
            {
                try
                {
                    Console.WriteLine("输入时间：");
                    dt = Convert.ToDateTime(Console.ReadLine());
                    Console.WriteLine("输入点名：");
                    pointName = Console.ReadLine();
                    TestProcessCycdirnetTimeBLL();
                    Console.WriteLine("按任意非T键执行");
                    char cmd = Console.ReadKey().KeyChar;

                    if (cmd == 't') break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("程序出错");
                    Console.WriteLine("退出请输入T");
                    char cmd = Console.ReadKey().KeyChar;

                    if (cmd == 't') break;
                }
            }

        }
        public void TestProcessCycdirnetTimeBLL()
        {

            Console.Write("选择身份，输入0表示监测员或者管理员1表示监测员");
            Role role = Console.ReadKey().KeyChar == '0'?Role.administratrorModel:Role.superviseModel;
            var processCycdirnetTimeModelGetModel = new CycdirnetTimeModelGetCondition(xmname, pointName, dt);
            var processCycdirnetModelofModel = new ProcessCycdirnetCom.ProcessCycdirnetModelofModel(processCycdirnetTimeModelGetModel,role);
            cycdirnetCom.ProcessCycdirnetModel(processCycdirnetModelofModel, out mssg);
            ProcessPrintMssg.Print(mssg);
            resultDataAlarmBLL = new ProcessResultDataAlarmBLL(xmname,Aspect.IndirectValue.GetXmnoFromXmname(xmname));
            resultDataAlarmBLL.alarmInfoList = new List<string>();
            resultDataAlarmBLL.CycdirnetPointAlarm(processCycdirnetTimeModelGetModel.model);
            ProcessPrintMssg.Print(string.Join("\n",resultDataAlarmBLL.alarmInfoList));
        }
       

    }
}
