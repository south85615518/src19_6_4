﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace NFnetUnitTest.VersionControl
{
    public partial class programbagchoice : Form
    {
        public programbagchoice()
        {
            InitializeComponent();
        }
        public delegate void ListBoxFlush(ListBox listBox,List<string>ls);
        public event ListBoxFlush ListBoxFlushEvent;
        public string processname = "";
        public static string rootpath = System.AppDomain.CurrentDomain.BaseDirectory;
        public event NFnetUnitTest.DTUServer.Form1.TextBoxFlush FlushEvent;

      

        private void programbagchoice_Load(object sender, EventArgs e)
        {
            ProcessProgramBagPath.processname = processname;
            programpathload();
        }
        private void btnPath_Click(object sender, EventArgs e)
        {

            Thread importThread = new Thread(new ThreadStart(ImportDialog));
            importThread.SetApartmentState(ApartmentState.STA); //重点
            importThread.Start();
            //txtImportPath.Text = importPath;


            
            //System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            //dialog.ShowDialog();
            //dialog.Description = "请选择Txt所在文件夹";
            //if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    if (string.IsNullOrEmpty(dialog.SelectedPath))
            //    {
            //        //System.Windows.MessageBox.Show(this, "文件夹路径不能为空", "提示");
            //        return;
            //    }
            //    //this.LoadingText = "处理中...";
            //    //this.LoadingDisplay = true;
            //    //Action<string> a = DaoRuData;
            //    //a.BeginInvoke(dialog.SelectedPath, asyncCallback, a);
            //}

        }
        public void ImportDialog()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string foldPath = dialog.SelectedPath;
                
                //MessageBox.Show("已选择文件夹:" + foldPath, "选择文件夹提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                selectpathlistload(foldPath);
                FlushEvent += TextFlush;
                IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, foldPath);
                this.listBox1.EndInvoke(ar);
                FlushEvent -= TextFlush;

            }
            
            //this.textBox1.BeginInvoke(FlushEvent, fm.FileName);
            //this.textBox1.EndInvoke();
        }
        public void TextFlush(string filename)
        {
            this.textBox1.Text = filename;
        }
        private void btnOpen_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Explorer.exe", "c:\\windows");
        }
        public void programpathload()
        {
             ProcessProgramBagPath.ListBoxBind(this.listBox2, ProcessProgramBagPath.ProgramBagPathLoad());
            
        }
        public void selectpathlistload(string path)
        {
            //this.listBox1.Items.AddRange(ProcessProgramBagPath.DirPathList(path));

            //ProcessProgramBagPath.ListBoxBind(this.listBox1,ProcessProgramBagPath.DirPathList(path));
            ListBoxFlushEvent += ListFlush;
            IAsyncResult ar = this.listBox1.BeginInvoke(ListBoxFlushEvent, this.listBox1, ProcessProgramBagPath.DirPathList(path));
            this.listBox1.EndInvoke(ar);
            ListBoxFlushEvent -= ListFlush;
        }
        //删除
        private void button3_Click(object sender, EventArgs e)
        {
            List<string> ls = new List<string>();
            foreach (string name in this.listBox2.SelectedItems)
            {
                ls.Add(name);
            }
            ProcessProgramBagPath.programmenulistremove(ls,listBox2,listBox1);
           
        }
        //添加
        private void button4_Click(object sender, EventArgs e)
        {
            //if (this.listBox1.SelectedItems.Count == 0) MessageBox.Show("请选择");
            List<string> ls = new List<string>();
            var tmpItems = this.listBox1.SelectedItems;
            List<string> lssource = new List<string>();
            foreach (string name in tmpItems)
            {
                ls.Add(name);
                //this.listBox1.Items.Remove(name);
            }
            foreach (string item in this.listBox1.Items)
            {
                if (!this.listBox1.SelectedItems.Contains(item))
                    lssource.Add(item);
            }




            //string selectpathlist = this.listBox1.SelectedValue.ToString();
            //var ls = from m in (List<string>)this.listBox1.SelectedItems select
            ProcessProgramBagPath.programmenulistflesh(ls);
            ProcessProgramBagPath.ListBoxBind(this.listBox2, ProcessProgramBagPath.menulist);
            ProcessProgramBagPath.ListBoxBind(this.listBox1, lssource);
            //ProcessProgramBagPath.ListBoxBind(this.listBox1, tmpItems);

            //ListBoxFlushEvent += ListFlush;
            //IAsyncResult ar = this.listBox1.BeginInvoke(ListBoxFlushEvent, this.listBox1, ProcessProgramBagPath.DirPathList(path));
            //this.listBox1.EndInvoke(ar);
            //ListBoxFlushEvent -= ListFlush;
            //ListBoxFlushEvent += ListFlush;
            //ar = this.listBox2.BeginInvoke(ListBoxFlushEvent, this.listBox1, ProcessProgramBagPath.DirPathList(path));
            //this.listBox1.EndInvoke(ar);
            //ListBoxFlushEvent -= ListFlush;

        }
        //保存
        private void button2_Click(object sender, EventArgs e)
        {
            ProcessProgramBagPath.programmenulistsave();
            MessageBox.Show("保存成功!");
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBox2.SelectedItems.Count > 0) this.button3.Enabled = true;
            else this.button3.Enabled = false;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBox1.SelectedItems.Count > 0) this.button4.Enabled = true;
            else this.button4.Enabled = false;
        }
        public void ListFlush(ListBox listBox, List<string> ls)
        {
            listBox.Items.Clear();
            foreach (var item in ls)
            {
                listBox.Items.Add(item);
            }
        }
    }
}
