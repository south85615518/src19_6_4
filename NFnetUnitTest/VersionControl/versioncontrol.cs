﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tool;
using System.IO;
using System.Threading;

namespace NFnetUnitTest.VersionControl
{
    public partial class versioncontrol : Form
    {
        public versioncontrol()
        {
            InitializeComponent();
        }
        public string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\版本库";
        public static System.Timers.Timer timer = new System.Timers.Timer(500);
        public event NFnetUnitTest.DTUServer.Form1.TextBoxFlush FlushEvent;
        public delegate void ControlEnableSetFlush(Button btn, bool enable);
        public event ControlEnableSetFlush controlEnableSetFlush;
        public  static string currentprocessname;
        public static string processtype = "";
        public static bool finished = true;
        public void init()
        {
            //ProcessSGAlarmDataServer.xmname = xmname;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(processloadline);
            timer.Enabled = true;
            timer.AutoReset = true;
        }
        public void processloadline(object source, System.Timers.ElapsedEventArgs e)
        {

            timer.Elapsed -= new System.Timers.ElapsedEventHandler(processloadline);
            timer.Enabled = false;
            timer.AutoReset = false;
            IAsyncResult ar = null;
            if (LoadLineHelper.currentfilename.Keys.Contains("fileProcess"))
            {
                FlushEvent += TextFlush;
                ar = this.label1.BeginInvoke(FlushEvent, LoadLineHelper.currentfilename["fileProcess"]);
                this.listBox1.EndInvoke(ar);
                FlushEvent -= TextFlush;
            }
            if (!finished)
            {
                timer.Elapsed += new System.Timers.ElapsedEventHandler(processloadline);
                timer.Enabled = true;
                timer.AutoReset = true;

            }
            else
            {
                FlushEvent += TextFlush;
                ar = this.label1.BeginInvoke(FlushEvent, "版本生成完成!");
                this.listBox1.EndInvoke(ar);
                FlushEvent -= TextFlush;
                //controlEnableSetFlush += ControlEnableSet;
                //ar = this.button1.BeginInvoke(controlEnableSetFlush, true);
                //this.button1.EndInvoke(ar);
                //controlEnableSetFlush -= ControlEnableSet;
            }


        }
        private void versioncontrol_Load(object sender, EventArgs e)
        {
            //string commendlog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "版本日志\\log.txt");
            //this.textBox1.Text = commendlog;
            //logload();
            //programpathload();
            //ProcessProgramBagPath.ListBoxBind(this.listBox1, ProcessProgramBagPath.ProgramBagPathLoad());
            processnamelogload();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (finished == false) { MessageBox.Show("版本正在生成..."); return; }
            //this.button1.Enabled = false;
            processtype = "iis";
            Thread thread = new Thread(versioncreateprocess);

            thread.Start();
            init();
        }
        public delegate void ListBoxFlush(ListBox listBox, List<string> ls);
        public event ListBoxFlush ListBoxFlushEvent;
        public void versioncreateprocess()
        {
            finished = false;

            versioncreate versioncreate = new versioncreate { processtype = processtype, processname = currentprocessname };
            versioncreate.main();
            List<string> commendlog = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\"+currentprocessname + "\\版本日志\\log.txt");
            //ProcessProgramBagPath.ListBoxBind(this.listBox2, commendlog);

            ListBoxFlushEvent += ListFlush;
            IAsyncResult ar = this.listBox2.BeginInvoke(ListBoxFlushEvent, this.listBox2, commendlog);
            this.listBox1.EndInvoke(ar);
            ListBoxFlushEvent -= ListFlush;



            //controlEnableSetFlush += ControlEnableSet;
            //ar = this.button1.BeginInvoke(controlEnableSetFlush,true);
            //this.listBox1.EndInvoke(ar);
            //controlEnableSetFlush -= ControlEnableSet;
            finished = true;
        }


        public void logload(string processname)
        {
            List<string> commendlog = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" +processname+ "\\版本日志\\log.txt");
            ProcessProgramBagPath.ListBoxBind(this.listBox2, commendlog);
        }
        public void processnamelogload()
        {
            List<string> commendlog = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "程序库\\namelist.txt");
            ProcessProgramBagPath.ComboBoxBind(this.comboBox1, commendlog);
        }
        //public void programpathload()
        //{
        //    List<string> lspath = ProcessProgramBagPath.ProgramBagPathLoad();
        //    this.listBox1.DataSource = lspath;
        //}

        private void button2_Click(object sender, EventArgs e)
        {
            programbagchoice pbc = new programbagchoice { processname = currentprocessname };
            pbc.ShowDialog();
            ProcessProgramBagPath.ListBoxBind(this.listBox1, ProcessProgramBagPath.ProgramBagPathLoad());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string folderpath = System.AppDomain.CurrentDomain.BaseDirectory + "版本库" + "\\" +currentprocessname +"\\"+ this.listBox2.SelectedItem.ToString().Split('|')[0];
            //if (File.Exists(folderpath))
            System.Diagnostics.Process.Start("Explorer.exe", @folderpath);
            //else
            //{
            //    System.Diagnostics.Process.Start("Explorer.exe",  @path);
            //    MessageBox.Show("找不到" + folderpath + "!");
            //}
            //this.label1.Text = "注意：此方式打开的压缩文件会存在乱码情况，请用压缩软件进行压缩打开！";

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBox2.SelectedItems.Count > 0) { this.button3.Enabled = true; this.button5.Enabled = true; }
            else { this.button3.Enabled = false; this.button5.Enabled = false; }
        }

        public void TextFlush(string filename)
        {


            if (filename.IndexOf("完成") == -1)
                this.label1.Text = filename + "!";
            else
                this.label1.Text = "正在" + filename + "......";
        }
        public void ControlEnableSet(Button btn, bool enable)
        {

            btn.Enabled = true;


        }

        public void ListFlush(ListBox listBox, List<string> ls)
        {
            listBox.Items.Clear();
            foreach (var item in ls)
            {
                listBox.Items.Add(item);
            }
        }
        public void ComboBoxFlush(ComboBox listBox, List<string> ls)
        {
            listBox.Items.Clear();
            foreach (var item in ls)
            {
                listBox.Items.Add(item);
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            FileHelper.FileInfoListWrite(new List<string>(), System.AppDomain.CurrentDomain.BaseDirectory + "\\"+currentprocessname+ "\\版本日志\\log.txt");
            this.listBox2.Items.Clear();
        }
        public static string filename = "";
        private void button5_Click(object sender, EventArgs e)
        {



            filename = this.listBox2.SelectedItem.ToString().Split('|')[0];
            this.label1.Text = "版本正在更新...";
            Thread thread = new Thread(ProcessFileSend);
            thread.Start();


        }

        public void ProcessFileSend()
        {
            List<string> ipport = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\端口配置文件\\setting.txt");
            string connectmssg = "";
            IAsyncResult ar = null;
            if (TcpServer.client == null || TcpServer.client.Connected == false)
            {
                TcpServer.client = new System.Net.Sockets.TcpClient();

                connectmssg = TcpServer.ServerConncet(ipport[0], ipport[1]);
                if (connectmssg.IndexOf("失败") != -1)
                {
                    FlushEvent += TextFlush;
                    ar = this.label1.BeginInvoke(FlushEvent, "服务器连接失败!");
                    this.listBox1.EndInvoke(ar);
                    FlushEvent -= TextFlush;
                    return;
                }
            }
            TcpServer server = new TcpServer { StrFile = System.AppDomain.CurrentDomain.BaseDirectory + "版本库\\"+currentprocessname + "\\" + filename + ".zip" };
            //MessageBox.Show("开始发送文件");

            string result = server.SendFile(ipport[0], ipport[1]);
            FlushEvent += TextFlush;
            ar = this.label1.BeginInvoke(FlushEvent, "版本更新完成!");
            this.listBox1.EndInvoke(ar);
            FlushEvent -= TextFlush;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (finished == false) { MessageBox.Show("版本正在生成..."); return; }
            //this.button1.Enabled = false;
            processtype = "exe";
            Thread thread = new Thread(versioncreateprocess);

            thread.Start();
            init();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            List<string> ls = new List<string>();
            if (!this.comboBox1.Items.Contains(this.textBox1.Text))
            {
                this.comboBox1.Items.Add(this.textBox1.Text);
            }
            foreach (var item in this.comboBox1.Items)
            {
                ls.Add((string)item);
            }
            ProcessProgramBagPath.programtxtsave(ls, System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "程序库\\namelist.txt");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            List<string> ls = new List<string>();

            this.comboBox1.Items.RemoveAt(this.comboBox1.SelectedIndex);
            
            foreach (var item in this.comboBox1.Items)
            {
                ls.Add((string)item);
            }
            ProcessProgramBagPath.programtxtsave(ls, System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "程序库\\namelist.txt");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentprocessname = this.comboBox1.SelectedText;
            logload(currentprocessname);
        }
        


    }
}
