﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;
using System.IO;

namespace NFnetUnitTest.VersionControl
{
    public class versioncreate
    {
        public  string processtype = "";
        public  string processname = "";
        public static string path = System.AppDomain.CurrentDomain.BaseDirectory + "版本日志";
        public static List<versionmodel> versionlist =new List<versionmodel>();
        public void main()
        {
            var versionmodel = versionlistload();
            versioncreator(versionmodel, 3);

        }



        public  versionmodel versionlistload()
        {
            try
            {
                if (!(File.Exists(path + "\\" + processname + "\\log.txt")))
                {
                    //File.Create(path + "\\log.txt");
                    FileStream fs = File.Open(path + "\\" + processname + "\\log.txt", FileMode.CreateNew, FileAccess.ReadWrite);
                    fs.Close();

                }
                versionlist = new List<versionmodel>();
                List<string> lsversion = FileHelper.ProcessFileInfoList(path + "\\" + processname + "\\log.txt");
                foreach (string versionstr in lsversion)
                {
                    
                    string[] vars = versionstr.Split('|');
                    versionlist.Add(new versionmodel
                    {
                        version = vars[0],
                        versiondate = Convert.ToDateTime(vars[1])
                    });
                    
                }
                return versionlist[0];
                //return new versionmodel();
            }
            catch (Exception ex)
            {
                Console.WriteLine("版本生成出错,错误信息:" + ex.Message);
                return new versionmodel();
            }
        }
        //public static 
        public class versionmodel
        {
            public string version { get; set; }
            public DateTime versiondate { get; set; }


        }
        public void versioncreator(versionmodel lastversion, int versioninterval)
        {
            string record = "";
            string version = "";
            string lastversionstr = lastversion.version;
            if (string.IsNullOrEmpty(lastversionstr))
            {
                record = "v1.0.0|" + DateTime.Now;
                version = "v1.0.0";
                ExceptionLog.VersionRecordWrite(record);
                return;
            };
            double lastversionmonth = Convert.ToDouble(lastversionstr.Substring(1, lastversionstr.IndexOf(".")));


            DateTime dtnow = DateTime.Now;
            double vm = (double)(dtnow - lastversion.versiondate).TotalDays / (30 * versioninterval) > 1 ? lastversionmonth + 1 : lastversionmonth;
            //double d = (double)() /  3000;
            double vd = MathRound((double)dtnow.Day / (30 * versioninterval), 3);
            double vtime = MathRound(((double)dtnow.TimeOfDay.TotalMinutes) / (60 * 24 * versioninterval), 5);
            version = string.Format("v{0}.{1}.{2}", vm, vd * 1000, vtime * 100000);
            record = string.Format("{0}|{1}", version, DateTime.Now);
            
            //return string.Format("v{0}-{1}", vtime, DateTime.Now);
            Console.WriteLine(string.Format("{0}={1}?", versionlist[versionlist.Count - 1].version,version));
            if (versionlist[versionlist.Count - 1].version == version) { Console.WriteLine("请不要生成版本太快,版本生成失败!"); return; }
            DirZip(version);
            ExceptionLog.VersionRecordWrite(record);

        }
        public double MathRound(double num, int roundnum)
        {
            double numw = (double)Math.Round((decimal)num, roundnum);
            return numw;
        }
        public void DirZip(string version)
        {
            List<string> dirpath = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory  + "项目目录列表\\menulist.txt");
            List<string> exceptionlist = new List<string>();
            exceptionlist.Add(System.AppDomain.CurrentDomain.BaseDirectory + "版本库");
            foreach (string path in dirpath)
            {
                Tool.FileHelper.Copy(path, System.AppDomain.CurrentDomain.BaseDirectory  + "版本库\\"+processname+"\\" + version, true, "fileProcess", System.AppDomain.CurrentDomain.BaseDirectory  + "版本库");
                LoadLineHelper.currentfilename["filecopy"] = "文件复制完成!";
            }
            List<string> ls = new List<string>();
            ls.Add(processname+"|"+version);
            FileHelper.FileInfoListWrite(ls, System.AppDomain.CurrentDomain.BaseDirectory + "版本库\\" + processname + "\\" + version + "\\版本.txt");
            //FileHelper.DirRename(System.AppDomain.CurrentDomain.BaseDirectory + "版本库\\" + version+"\\");
            Tool.FileHelper.DirZip(System.AppDomain.CurrentDomain.BaseDirectory + "版本库\\" + processname + "\\" + version, System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "版本库\\" + processname + "\\" + version, "fileProcess");
        }
        


    }
}
