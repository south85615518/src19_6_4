﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.UserProcess;
using Tool;

namespace NFnetUnitTest.MessgSend
{
    public partial class MailSend : Form
    {
        public MailSend()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mssg = "";

            Tool.MailHelper mailHelper = new Tool.MailHelper();
            mailHelper.Small(this.textBox2.Text,this.textBox1.Text.Split(',').ToList(),this.textBox3.Text,out mssg);
            this.label4.Text = mssg;
        }

        private void MailSend_Load(object sender, EventArgs e)
        {
            xmlistload();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProcessMessgSend processMessgSend = new ProcessMessgSend();
            processMessgSend.main();
        }
        public MailHelper mail = new MailHelper();
        public ProcessEmailSendBLL processEmailSendBLL = new ProcessEmailSendBLL();
        public string mssg = "";
        private void button3_Click(object sender, EventArgs e)
        {
            List<System.Net.Mail.Attachment> attachmentlist = new List<System.Net.Mail.Attachment>();
            List<string> recmails = new List<string>();
            List<FileStream> fslist = new List<FileStream>();
            
                FileStream fs = File.Open(this.textBox4.Text, FileMode.Open, FileAccess.Read);

                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(
                                 fs, Path.GetFileName(this.textBox4.Text), System.Net.Mime.MediaTypeNames.Application.Zip);
                //fs.Close();
                attachmentlist.Add(attachment);
                //fslist.Add(fs);
                foreach(string email in this.listBox1.SelectedItems)
                {
                    recmails.Add(email);
                }

            //重发三次
                mail.Small(Path.GetFileNameWithoutExtension(this.textBox4.Text), recmails, Path.GetFileNameWithoutExtension(this.textBox4.Text), attachmentlist, out mssg);
                this.label8.Text = mssg;
        }



        public event NFnetUnitTest.DTUServer.Form1.TextBoxFlush FlushEvent;
       

        private void button4_Click(object sender, EventArgs e)
        {

            Thread importThread = new Thread(new ThreadStart(ImportDialog));
            importThread.SetApartmentState(ApartmentState.STA); //重点
            importThread.Start();
            //txtImportPath.Text = importPath;



            //System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            //dialog.ShowDialog();
            //dialog.Description = "请选择Txt所在文件夹";
            //if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    if (string.IsNullOrEmpty(dialog.SelectedPath))
            //    {
            //        //System.Windows.MessageBox.Show(this, "文件夹路径不能为空", "提示");
            //        return;
            //    }
            //    //this.LoadingText = "处理中...";
            //    //this.LoadingDisplay = true;
            //    //Action<string> a = DaoRuData;
            //    //a.BeginInvoke(dialog.SelectedPath, asyncCallback, a);
            //}

        }
        public void ImportDialog()
        {
            FileDialog dialog = new OpenFileDialog();
            dialog.Title = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string foldPath = dialog.FileName;

                //MessageBox.Show("已选择文件夹:" + foldPath, "选择文件夹提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //selectpathlistload(foldPath);
                FlushEvent += TextFlush;
                IAsyncResult ar = this.textBox4.BeginInvoke(FlushEvent, foldPath);
                this.textBox4.EndInvoke(ar);
                FlushEvent -= TextFlush;

            }

            //this.textBox1.BeginInvoke(FlushEvent, fm.FileName);
            //this.textBox1.EndInvoke();
        }

        public void TextFlush(string filename)
        {
            this.textBox4.Text = filename;
        }
        public ProcessMonitorBLL processMonitorBLL = new ProcessMonitorBLL();
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            var processXmMonitorListModel = new ProcessMonitorBLL.ProcessXmMonitorListModel(Aspect.IndirectValue.GetXmnoFromXmname(this.comboBox1.Text));
            processMonitorBLL.ProcessXmMonitorList(processXmMonitorListModel, out mssg);


            DateTime dt = DateTime.Now;
            int sec = 0;
            List<string> recmails = new List<string>();
            List<AuthorityAlarm.Model.monitoralarm> lamm = new List<AuthorityAlarm.Model.monitoralarm>();
            ProcessMailDelegateSend pms = new ProcessMailDelegateSend();
            this.listBox1.Items.Clear();

            foreach (Authority.Model.UnitMember unitMemberModel in processXmMonitorListModel.lum)
            {
                //recmails.Add(unitMemberModel.email);
                this.listBox1.Items.Add(unitMemberModel.email);
            }
        }
        public void xmlistload()
        {
            List<string> ls = Aspect.IndirectValue.XmnameListLoad(out mssg);
            this.comboBox1.DataSource = ls;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            for(int i=0;i<this.listBox1.Items.Count;i++)
            {
                this.listBox1.SetSelected(i,true);
                
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.listBox1.ClearSelected();
        }
    }
}
