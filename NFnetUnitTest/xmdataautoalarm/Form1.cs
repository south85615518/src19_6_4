﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tool;
using NFnetUnitTest.VersionControl;
using System.Threading;
using SqlHelpers;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.GTSensorServer;

namespace NFnetUnitTest.xmdataautoalarm
{
    public partial class Form1 : Form
    {

        public System.Timers.Timer timer = new System.Timers.Timer(60000);
        public System.Timers.Timer timerlog = new System.Timers.Timer(2000);
        //public System.Timers.Timer timerlogclear = new System.Timers.Timer(3600000);
        public ProcessGTSettlementBLL processGTSettlementBLL = new ProcessGTSettlementBLL();


        public string mssg = "";
        public Form1()
        {
            InitializeComponent();
           

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }
        public void init()
        {
            timer.Elapsed += new System.Timers.ElapsedEventHandler(Inspectthread);
            timer.Enabled = true;
            timer.Start();
            timerlog.Elapsed += new System.Timers.ElapsedEventHandler(logflush);
            timerlog.Enabled = false;
            
            //timer.AutoReset = false;
        }


        public delegate void TextBoxFlush(TextBox textBox, string str);
        public event TextBoxFlush TextBoxFlushEvent;
        public void TextFlush(TextBox textBox, string str)
        {
            textBox.Clear();
            
            textBox.Text = str ;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Start();
        }

        public void Start()
        {
            if (this.button1.Text == "开启")
            {

                //Console.WriteLine("报表生成已启动...");

                init();
                this.button1.Text = "停止";
            }
            else
            {
                // Console.WriteLine("报表生成已停止...");
                //foreach (var report in lap)
                //{
                timer.Enabled = false;
                timer.Stop();
                //    report.listtimer.Enabled = false;
                //}

                this.button1.Text = "开启";
            }
        }
        public void Inspectthread(object source, System.Timers.ElapsedEventArgs e)
        {
            //autoreport report = obj as autoreport;
            //report.main();
            timer.Stop();// = false;
            timer.Enabled = false;
            timerlog.Enabled = true;
            timerlog.Start();
            try
            {
                string setting = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\Setting\\gtdtudataSetting.txt");
                processGTSettlementBLL.ProcessDTUDataFileTravel(setting);
            }
            catch (Exception ex)
            {
 
            }
            Tool.FileHelper.FileClear(System.AppDomain.CurrentDomain.BaseDirectory + "\\广铁沉降数据接收日志\\log.txt");
            timerlog.Stop();
            timerlog.Enabled = false;
            timer.Enabled = true;
            timer.Start();
        }

        public void logflush(object source, System.Timers.ElapsedEventArgs e)
        {
            if (FileHelper.FileLength(System.AppDomain.CurrentDomain.BaseDirectory + "\\广铁沉降数据接收日志\\log.txt") > 20)
                Tool.FileHelper.FileClear(System.AppDomain.CurrentDomain.BaseDirectory + "\\广铁沉降数据接收日志\\log.txt");
            string commendlog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\广铁沉降数据接收日志\\log.txt");
            //ProcessProgramBagPath.TextBoxBind(this.textBox2, commendlog);

            TextBoxFlushEvent += TextFlush;
            IAsyncResult ar = this.textBox2.BeginInvoke(TextBoxFlushEvent, this.textBox2, commendlog);
            this.textBox2.EndInvoke(ar);
            TextBoxFlushEvent -= TextFlush;
        }
        public void logflush()
        {
            if(FileHelper.FileLength(System.AppDomain.CurrentDomain.BaseDirectory + "\\广铁沉降数据接收日志\\log.txt") > 20)
                Tool.FileHelper.FileClear(System.AppDomain.CurrentDomain.BaseDirectory + "\\广铁沉降数据接收日志\\log.txt");
            string commendlog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\广铁沉降数据接收日志\\log.txt");
            //ProcessProgramBagPath.TextBoxBind(this.textBox2, commendlog);

            TextBoxFlushEvent += TextFlush;
            IAsyncResult ar = this.textBox2.BeginInvoke(TextBoxFlushEvent, this.textBox2, commendlog);
            this.textBox2.EndInvoke(ar);
            TextBoxFlushEvent -= TextFlush;
        }
        public static List<string> xmnameList = new List<string>();
        public ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
      



        private void button1_Click_1(object sender, EventArgs e)
        {
            timer.Stop();
            timer.Interval = Convert.ToInt32(this.textBox1.Text) * 1000 * 60;
            Start();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            Tool.FileHelper.FileClear(System.AppDomain.CurrentDomain.BaseDirectory + "\\广铁沉降数据接收日志\\log.txt");
            logflush();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }




    }
}
