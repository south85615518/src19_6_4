﻿using System;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using System.Data.Odbc;
using NFnet_BLL.Other;

namespace NFnetUnitTest.TestFractureMeterReport
{
    public class ProcessTestFractureMeterReport
    {
        public void main()
        {
            //estFractureMeterReportSPMC();
            //TestFractureMeterReportMPSC();
            //TestFractureMeterReportMPMC();
            //TestFractureMeterReportSPMC();
            //TestSenorDataMPMTTabCreate();
            //TestFractureMeterReportPrintTime(336);
        }
        //public ProcessFractureMeterBLL resultDataBLL = new ProcessFractureMeterBLL();
        //public ProcessFractureMeterCom inclinometerCom = new ProcessFractureMeterCom();
        public ProcessReportPrintBLL processReportPrintBLL = new ProcessReportPrintBLL();
        //public ProcessFractureMeterReportPrintBLL reportPrintBLL = new ProcessFractureMeterReportPrintBLL();
        //public ProcessFractureMeterSenorCom resultDataCom = new ProcessFractureMeterSenorCom();
        public static string mssg = "";
        public string xmname = "羊台山隧道监测-广南高铁段委托";
        public void TestFractureMeterReportSPMC()
        {

            string sql = "select point_name,time,single_this_scalarvalue,single_ac_scalarvalue  from  gtsensordata  where datatype='静力水准'   order by point_name,time asc";

            var querystanderdbIntModel = new QuerystanderdbIntModel(sql,96);
            ProcessComBLL.Processquerystanderdb(querystanderdbIntModel,out mssg);

            var processTotalStationSPMCReportModel = new ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("4", "6", xmname, "D:\\ReportExcel - 副本.xlsx", querystanderdbIntModel.dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processReportPrintBLL.ProcessFractureMeterSPMTReport(processTotalStationSPMCReportModel, out mssg);


        }
        //public ProcessFractureMeterReportPrintTime processFractureMeterReportPrintTime = new ProcessFractureMeterReportPrintTime();
        //public void TestFractureMeterReportPrintTime(int rows)
        //{
        //    string reportprintstr = processFractureMeterReportPrintTime.FractureMeterReportPrintTime(rows);
        //    ProcessPrintMssg.Print(reportprintstr);
        //}
        //public void TestSenorDataMPMTTabCreate()
        //{
        //    string sql = "select point_name,time,this_disp, ac_disp,this_rap  from  senor_data where point_name in ('CX1-51','CX1-52') and time between '2017-11-16' and '2017-11-18' order by point_name , time ";
        //    var resultDataReportTableCreateCondition = new SenorDataReportTableCreateCondition(sql, xmname, Aspect.IndirectValue.GetXmnoFromXmname("数据测试"));
        //    var processSenorDataReportTableCreateModel = new ProcessFractureMeterCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
        //    DataTable dt = null;
        //    if (inclinometerCom.ProcessSenorDataReportTableCreate(processSenorDataReportTableCreateModel, out mssg))
        //    {
        //        dt = processSenorDataReportTableCreateModel.model.dt;
        //    }
        //    //var processFractureMeterSPMTReportModel = new ProcessSenorDataReportPrintBLL.ProcessSenorDataSPMCReportModel("2017-11-16", "2017-11-18", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
        //    //senorDataReportPrintBLL.ProcessSenorDataMPMTReport(processFractureMeterSPMTReportModel, out mssg);
        //}
    }
}
