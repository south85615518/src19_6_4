﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using Tool;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.Gnss;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.Other;
using NFnet_BLL.DisplayDataProcess.HXYL;
using NFnet_Interface.DisplayDataProcess.HXYL;

namespace NFnetUnitTest.TestTotalStationReport
{
   public class ProcessHXYLReport
    {
        public void main() {
            TestTotalStationReportSPMC();
            //TestTotalStationReportMPSC();
            //TestTotalStationReportMPMC();
        }
        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public ProcessTotalStationReportPrintBLL reportPrintBLL = new ProcessTotalStationReportPrintBLL();
        public ProcessHXYLReportPrintBLL processHXYLReportPrintBLL = new ProcessHXYLReportPrintBLL();
        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public static ProcessBKGMCUData processBKGMCUData = new ProcessBKGMCUData();
        public ProcessHXYLData processHXYLData = new ProcessHXYLData();
        public static ProcessHXYLDataRqcxConditionCreateDay processHXYLDataRqcxConditionCreateDay = new ProcessHXYLDataRqcxConditionCreateDay();
        //public ProcessMCUDataRqcxConditionCreate processMCUDataRqcxConditionCreate = new ProcessMCUDataRqcxConditionCreate();
        //public ProcessMCUDATADbFill processMCUDATADbFill = new ProcessMCUDATADbFill();
        public static string mssg = "";
        public string xmname = "韶关铀业";
        public static ProcessHXYLIDBLL processHXYLIDBLL = new ProcessHXYLIDBLL();
        public void TestTotalStationReportSPMC()
        {

            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2','Z4-5','Z6-5','Z8-5','Z2-5','Z3-5') and cyc between 10 and 10 order by point_name , cyc ";
            sql = @"select 设备编号 ,分钟雨量,format( cstr(年份)+'/'+cstr(月份)+'/'+cstr(日)+' '+cstr(时)+':'+cstr(分),'yyyy/mm/dd HH:mm:ss') as dt from 雨量历史数据 where format( cstr(年份)+'/'+cstr(月份)+'/'+cstr(日)+' '+cstr(时)+':'+cstr(分),'yyyy/mm/dd HH:mm:ss')>=format('2018/2/9 00:00:00','yyyy/mm/dd HH:mm:ss') and format( cstr(年份)+'/'+cstr(月份)+'/'+cstr(日)+' '+cstr(时)+':'+cstr(分),'yyyy/mm/dd HH:mm:ss')<=format('2018/2/10 00:00:00','yyyy/mm/dd HH:mm:ss')   ";


            var hxylmodel = new QueryHXYLModel(sql);
            if (!ProcessComBLL.Processquerystanderdb(hxylmodel, out mssg))
            {
                //错误信息反馈
            }
            DataTable dt = processHXYLData.HXYLTableFormat(Aspect.IndirectValue.GetXmnoFromXmname(xmname), hxylmodel.dt);

            DateTime date = Convert.ToDateTime("2014/12/1");
            do
            {
                //ExceptionLog.ExceptionWrite("日期查询");
                var resultDataRqcxConditionCreateCondition = processHXYLDataRqcxConditionCreateDay.ResultDataRqcxConditionCreateDay(date.ToString(), date.AddMonths(1).ToString(), QueryType.RQCX, "", "从化边坡监测", new DateTime());

                string[] pointnames = "雨量计1号设备".Split(',');
                List<string> lis = new List<string>();
                
                foreach (var pointname in pointnames)
                {
                    ExceptionLog.ExceptionWrite("水位点名转换");
                    lis.Add(processHXYLIDBLL.HXYLPointID(Aspect.IndirectValue.GetXmnoFromXmname("从化边坡监测"), pointname, out mssg));
                    ExceptionLog.ExceptionWrite("水位点名转换完成");
                }
                var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition(string.Join(",", lis), resultDataRqcxConditionCreateCondition.sql, Aspect.IndirectValue.GetXmnoFromXmname("从化边坡监测"));
                //ExceptionLog.ExceptionWrite("水位数据表生成");
                processHXYLData.ProcessInclinometerDbFillDay(fillInclinometerDbFillCondition);
                
                var processTotalStationSPMCReportModel = new ProcessHXYLReportPrintBLL.ProcessTotalStationSPMCReportModel("2018-2-17 00:00:00", "2018-2-20 00:00:00", xmname, "D:\\ReportExcel - 副本.xlsx", fillInclinometerDbFillCondition.dt, "d:\\雨量报表\\" +string.Format("{0}年{1}月份数据报表",date.Year,date.Month)  + ".xlsx");
                //reportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
                processHXYLReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
                Console.WriteLine(string.Format("{0}年{1}月份数据报表打印完成", date.Year, date.Month));
                date = date.AddMonths(1);
            }
            while (dt != null);






            //var MCUDataRqcxConditionCreateModel = processMCUDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018-1-20 00:00:00", "2018-1-24 00:00:00", QueryType.RQCX, "", "韶关铀业", new DateTime());
            //var fillInclinometerDbFillCondition = new FillBKGDbFillCondition("韶关745-J1-1,韶关745-J2-1", sql, "韶关铀业");

            //processBKGMCUData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
            //var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition("GPS1,GPS2", MCUDataRqcxConditionCreateModel.sql, 29);
            //sql = fillInclinometerDbFillCondition.sql;
            //var model = processBKGMCUData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);



            //DataTable dt = null;
            //dt = fillInclinometerDbFillCondition.dt;
            //var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            //var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition,Role.monitorModel,false);
            //DataTable dt = null;
            //if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            //{
            //    dt = processResultDataReportTableCreateModel.model.dt;
            //}
           

        }

        public void TestTotalStationReportMPSC()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2') and cyc between 1 and 10 order by point_name , cyc ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            //int cont = DataTableHelper.ProcessDataTableRowsCount(dt, "point_name");
            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPSCReport(processTotalStationMPSCReportModel, out mssg);
            //ChartHelper chartHelper = new ChartHelper();
            //chartHelper.ChartReportHelper(processTotalStationMPSCReportModel.exportpath, DataTableHelper.ProcessDataTableRowsCount(processTotalStationMPSCReportModel.dt, "point_name"), "点名");

        }

        public void TestTotalStationReportMPMC()
        {
            string sql = "select  cyc,point_name, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2') and cyc between 1 and 10 order by  cyc,point_name  ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }

            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPMCReport(processTotalStationMPSCReportModel, out mssg);


        }


    }
}
