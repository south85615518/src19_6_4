﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using Tool;
using System.Web.Script.Serialization;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnetUnitTest.TestTotalStationReport
{
    class ProcessTestTotalStationReport
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public void main() {
            //TestTotalStationReportSPMC();
            //TestTotalStationReportMPSC();
            //TestTotalStationReportMPMC();
            TestTotalStationReportMPSC_WithoutSettlement();
            //TestTotalStationReportMPSC();
            //TestTotalStation_dimension_ReportMPSC_WithoutSettlement();
        }
        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public ProcessTotalStationReportPrintBLL reportPrintBLL = new ProcessTotalStationReportPrintBLL();
        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public ProcessDataTableIntoModelList processDataTableIntoModelList = new ProcessDataTableIntoModelList();
        public ProcessModelListIntoDataTable processModelListIntoDataTable = new ProcessModelListIntoDataTable();
        public ProcessReportDelegateSend processReportDelegateSend = new ProcessReportDelegateSend();
        public static string mssg = "";
        public string xmname = "广州北站基坑工程京广高铁设备监测"; 
        public void TestTotalStationReportSPMC()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time,'' as siblingpoint_name from fmos_cycdirnet    order by point_name , cyc ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition,Role.monitorModel,false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            var processTotalStationSPMCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("4", "6", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() +new Random().Next(999)+ ".xlsx");
            reportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);


        }

        public void TestTotalStationReportMPSC_WithoutSettlement()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time,siblingpoint_name from fmos_cycdirnet where taskname='124'  and  point_name in (select distinct(point_name) from fmos_pointalarmvalue where xmno = 124 )  and  cyc in (313)  order by  cyc,point_name  ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, "96");
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            int cont = DataTableHelper.ProcessDataTableRowsCount(dt, "point_name");
            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPSCReport_GT_WhithoutSettlement(processTotalStationMPSCReportModel, out mssg);
         

        }
        public void TestTotalStation_dimension_ReportMPSC_WithoutSettlement()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time,siblingpoint_name from cggt.fmos_cycdirnet where taskname='129'  and  point_name in (select distinct(point_name) from fmos_pointalarmvalue where xmno = 129 )  and  cyc in (2)  order by  cyc,point_name  ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, "96");
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            int cont = DataTableHelper.ProcessDataTableRowsCount(dt, "point_name");
            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel_.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPSCReport_GT_dimension_Report_WithoutSettlement(processTotalStationMPSCReportModel, out mssg);


        }

        public void TestTotalStation_ReportMPSC()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time,siblingpoint_name from cggt.fmos_cycdirnet where taskname='129'  and  point_name in (select distinct(point_name) from fmos_pointalarmvalue where xmno = 124 )  and  cyc in (2)  order by  cyc,point_name  ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, "96");
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            int cont = DataTableHelper.ProcessDataTableRowsCount(dt, "point_name");
            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel_.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPSCReport_GT_dimension_Report_WithoutSettlement(processTotalStationMPSCReportModel, out mssg);


        }




        public void TestTotalStationReportMPSC()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where taskname='103'  and point_name in (select distinct(point_name) from fmos_pointalarmvalue  where xmno =103 )  and cyc between 146  and  153    order by cyc,point_name   ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, "96");
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            int cont = DataTableHelper.ProcessDataTableRowsCount(dt, "point_name");
            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx","周");
            reportPrintBLL.ProcessTotalStationMPSCReport_GT_Report_WithoutSettlement(processTotalStationMPSCReportModel, out mssg);
            //List<TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist = processDataTableIntoModelList.DataTableIntoModelList(dt,out mssg);

            //dt = processModelListIntoDataTable.ModelListIntoDataTable("从化边坡监测",cycdirnetlist,out mssg);
            //jss.MaxJsonLength = 640000000;
            //                  //2097152
            //string cycdirnetliststr = jss.Serialize(cycdirnetlist);

            //Tool.DATAREPORT.Pub.reportprotocol reportprotocol = new Tool.DATAREPORT.Pub.reportprotocol {
            //    lobj = cycdirnetliststr,
            //    expath = DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx",
            //    printtype = Tool.DATAREPORT.Pub.printtype.TOTALSTATION_MPSC,
            //    xmname = "从化边坡监测"
            //};
            //string result = jss.Serialize(dt);

            //reportprotocol = (Tool.DATAREPORT.Pub.reportprotocol)jss.Deserialize<Tool.DATAREPORT.Pub.reportprotocol>(result);

            //processReportDelegateSend.testreportdelegate(reportprotocol);
            //ChartHelper chartHelper = new ChartHelper();
            //chartHelper.ChartReportHelper(processTotalStationMPSCReportModel.exportpath, DataTableHelper.ProcessDataTableRowsCount(processTotalStationMPSCReportModel.dt, "point_name"), "点名");

        }

        public void TestTotalStationReportMPMC()
        {
            string sql = "select  cyc,point_name, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet  order by  cyc,point_name  ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }

            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPMCReport(processTotalStationMPSCReportModel, out mssg);


        }


    }
}
