﻿using System;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;

namespace NFnetUnitTest.TestDTUReport
{
    public class ProcessTestDTUReport
    {
        public void main()
        {
            TestDTUReportSPMC();
            //TestDTUReportMPSC();
            //TestDTUReportMPMC();
            //TestDTUReportSPMC();
            //TestSenorDataMPMTTabCreate();
            //TestDTUReportPrintTime(336);
        }
        //public ProcessDTUBLL resultDataBLL = new ProcessDTUBLL();
        //public ProcessDTUCom inclinometerCom = new ProcessDTUCom();
        public ProcessDTUDataReportPrintBLL senorDataReportPrintBLL = new ProcessDTUDataReportPrintBLL();
        //public ProcessDTUReportPrintBLL reportPrintBLL = new ProcessDTUReportPrintBLL();
        public ProcessDTUSenorCom resultDataCom = new ProcessDTUSenorCom();
        public static string mssg = "";
        public string xmname = "数据测试";
        public void TestDTUReportSPMC()
        {
            
            string sql = "select point_name,deep,predeep,thisdeep,acdeep,rap,time,holedepth,times  from  dtudatareport where time < '2018-1-7 17:00:00' and time >'2018-1-7 8:00:00'  order by point_name , time asc";

            Authority.Model.xmconnect xmmodel = Aspect.IndirectValue.GetXmconnectFromXmname("UTS1");

            Tool.SenorReport senorReport = new Tool.SenorReport
            {
                xmname = xmmodel.xmname,
                addr = xmmodel.xmaddress,
                jclx = "地下水位",
                instrument = "投入式水位计",
                jcdw = xmmodel.jcdw,
                wtdw = xmmodel.delegateDw,
                tabHead = "点名,上次水深,本次水深,本次变化量,累计变化量,变化速率,时间"

            };

            var resultDataReportTableCreateCondition = new SenorDataReportTableCreateCondition(sql, xmname, Aspect.IndirectValue.GetXmnoFromXmname("UTS1"));
            var processSenorDataReportTableCreateModel = new ProcessDTUSenorCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessSenorDataReportTableCreate(processSenorDataReportTableCreateModel, out mssg))
            {
                dt = processSenorDataReportTableCreateModel.model.dt;
            }
            int rows = dt.Rows.Count;
            var processDTUSPMTReportModel = new ProcessDTUDataReportPrintBLL.ProcessSenorDataSPMCReportModel(senorReport, Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), "数据测试", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            senorDataReportPrintBLL.ProcessSenorDataSPMTReport(processDTUSPMTReportModel, out mssg);


        }
        public ProcessDTUReportPrintTime processDTUReportPrintTime = new ProcessDTUReportPrintTime();
        public void TestDTUReportPrintTime(int rows)
        {
            string reportprintstr = processDTUReportPrintTime.DTUReportPrintTime(rows);
            ProcessPrintMssg.Print(reportprintstr);
        }
        //public void TestSenorDataMPMTTabCreate()
        //{
        //    string sql = "select point_name,time,this_disp, ac_disp,this_rap  from  senor_data where point_name in ('CX1-51','CX1-52') and time between '2017-11-16' and '2017-11-18' order by point_name , time ";
        //    var resultDataReportTableCreateCondition = new SenorDataReportTableCreateCondition(sql, xmname, Aspect.IndirectValue.GetXmnoFromXmname("数据测试"));
        //    var processSenorDataReportTableCreateModel = new ProcessDTUCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
        //    DataTable dt = null;
        //    if (inclinometerCom.ProcessSenorDataReportTableCreate(processSenorDataReportTableCreateModel, out mssg))
        //    {
        //        dt = processSenorDataReportTableCreateModel.model.dt;
        //    }
        //    //var processDTUSPMTReportModel = new ProcessSenorDataReportPrintBLL.ProcessSenorDataSPMCReportModel("2017-11-16", "2017-11-18", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
        //    //senorDataReportPrintBLL.ProcessSenorDataMPMTReport(processDTUSPMTReportModel, out mssg);
        //}
    }
}
