﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata;
using System.Data.Odbc;
using SqlHelpers;

namespace NFnetUnitTest.TestTotalStationReport
{
    public class TestSensordataReport
    {
        public string mssg = "";
        public DataTable dt = new DataTable();
        public ProcessResultDataBLL processGTResultDataBLL = new ProcessResultDataBLL();
        public NFnet_BLL.DataProcess.ProcessSurfaceDataBLL processSurfaceDataBLL = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL();
        public NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL processReportPrintBLL = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL();
        public void main()
        {
            //TestGTSensordataReport();
            //TestGTSensordata_timespanReport();
            //TestEnclosureWallReinforcementStressSensordataReport();
            //TestEnclosureWallReinforcementStress_timespanReport();
            //TestSupportAxialForcedataReport();
            //TestSupportAxialForcedata_timespanReport();
            //TestGTInclinometerdata_timespanReport();
            TestTotalStationOrgldataReport();
        }

        public void TestTotalStationOrgldataReport()
        {
            database db = new database();
            ProcessOrglDataReportPrintBLL processOrglDataReportPrintBLL = new ProcessOrglDataReportPrintBLL();
            string sql = "select point_name,cyc,single_oregion_scalarvalue,first_oregion_scalarvalue,sec_oregion_scalarvalue,time from  gtsensordata  where project_name=103  and datatype='位移' and cyc between 1 and  20  order by cyc,point_name,time asc  ";
            //var model = new QuerystanderdbIntModel(sql,106);
            OdbcConnection conn = db.GetStanderConn(96);
            DataTable dt = querysql.querystanderdb(sql, conn);
            var processTotalStationSPMCReportModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessOrglDataReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._fractureMeter),"A,B,C", "新白广城际铁路下穿京广高铁施工监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            processOrglDataReportPrintBLL.ProcessgtsensordataMPMCReport(processTotalStationSPMCReportModel, out mssg);
            ProcessPrintMssg.Print(mssg);

        }



        #region FractureMeter
        public void TestGTSensordataReport()
        {
            string sql = "select point_name,cyc,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,time from gtsensordata where project_name='103'  and  cyc>150  order by  cyc,point_name  ";
            if (!processGTResultDataBLL.GetResultDataTable(sql, "103", out dt, out mssg)) return ;
            var processTotalStationMPSCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "羊台山隧道监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processReportPrintBLL.ProcessGtSensordataTotalStationMPSCReport_GT(processTotalStationMPSCReportModel, out mssg);
        }
        public void TestGTSensordata_timespanReport()
        {
            string sql = "select point_name,cyc,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,time from gtsensordata where project_name='103'  and  cyc>145  order by  cyc,point_name  ";
            if (!processGTResultDataBLL.GetResultDataTable(sql, "103", out dt, out mssg)) return;
            var processTotalStationMPSCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "羊台山隧道监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processReportPrintBLL.ProcessGtSensordata_timespanTotalStationMPSCReport_GT(processTotalStationMPSCReportModel, out mssg);
        }
        #endregion

        #region EnclosureWallReinforcementStress


        public void TestEnclosureWallReinforcementStressSensordataReport()
        {
            string sql = "select point_name,cyc,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,time from gtsensordata where project_name='96' and  datatype='围护墙钢筋应力'  and  cyc>457   order by  cyc,point_name  ";
            if (!processGTResultDataBLL.GetResultDataTable(sql, "96", out dt, out mssg)) return;
            var processTotalStationMPSCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "新白广城际铁路下穿京广高铁施工监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processReportPrintBLL.ProcessEnclosureWallReinforcementMPSCReport_GT(processTotalStationMPSCReportModel, out mssg);
        }
        public void TestEnclosureWallReinforcementStress_timespanReport()
        {
             string sql = "select point_name,cyc,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,time from gtsensordata where project_name='96' and  datatype='围护墙钢筋应力'  and  cyc>459  and  cyc<463  order by  cyc,point_name  ";
            if (!processGTResultDataBLL.GetResultDataTable(sql, "96", out dt, out mssg)) return;
            var processTotalStationMPSCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "新白广城际铁路下穿京广高铁施工监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processReportPrintBLL.ProcessEnclosureWallReinforcement_timespanMPSCReport_GT(processTotalStationMPSCReportModel, out mssg);
        }
        #endregion

        #region SupportAxialForce


        public void TestSupportAxialForcedataReport()
        {
            string sql = "select surfacename ,cyc,this_val,ac_val,time from gtsurfacedata  where xmno=96   and  cyc>539   order by  cyc,surfacename  ";
            if (!processSurfaceDataBLL.GetResultDataTable(sql, 96, out dt, out mssg)) return;
            var processTotalStationMPSCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "新白广城际铁路下穿京广高铁施工监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processReportPrintBLL.ProcessSupportAxialForceMPSCReport_GT(processTotalStationMPSCReportModel, out mssg);
        }
        public void TestSupportAxialForcedata_timespanReport()
        {
            string sql = "select surfacename,cyc,this_val,ac_val,time from gtsurfacedata  where xmno=96   and  cyc>539   order by  cyc,surfacename  ";
            if (!processSurfaceDataBLL.GetResultDataTable(sql, 96, out dt, out mssg)) return;
            var processTotalStationMPSCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "新白广城际铁路下穿京广高铁施工监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processReportPrintBLL.ProcessSupportAxialForce_timespanMPSCReport_GT(processTotalStationMPSCReportModel, out mssg);
        }
        #endregion

        #region GTInclinometer


        //public void TestGTInclinometerdataReport()
        //{
        //    string sql = "select surfacename ,cyc,this_val,ac_val,time from gtsurfacedata  where xmno=96   and  cyc>539   order by  cyc,surfacename  ";
        //    if (!processSurfaceDataBLL.GetResultDataTable(sql, 96, out dt, out mssg)) return;
        //    var processTotalStationMPSCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "新白广城际铁路下穿京广高铁施工监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
        //    processReportPrintBLL.(processTotalStationMPSCReportModel, out mssg);
        //}
        public void TestGTInclinometerdata_timespanReport()
        {
            string sql = "select holename,-deepth as deepth,cyc,a_ac_diff,a_this_diff,a_this_rap,time from  gtinclinometer  where  xmno=96   and  holename in (1,2) and  cyc between 1  and  3   order by  holename,cyc,deepth ";
            if (!processSurfaceDataBLL.GetResultDataTable(sql, 96, out dt, out mssg)) return;
            var processTotalStationMPSCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata.ProcessReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "新白广城际铁路下穿京广高铁施工监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + new Random().Next(999) + ".xlsx");
            processReportPrintBLL.ProcessGTInclinometer_timespanMPSCReport_GT(processTotalStationMPSCReportModel, out mssg);
        }
        #endregion

    }
}
