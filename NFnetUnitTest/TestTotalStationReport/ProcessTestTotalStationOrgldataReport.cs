﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using Tool;
using System.Web.Script.Serialization;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.Other;
using SqlHelpers;
using System.Data.Odbc;
using NFnet_Interface.DisplayDataProcess.pub;

namespace NFnetUnitTest.TestTotalStationReport
{
   public class ProcessTestTotalStationOrgldataReport
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static database db = new database();
        public static ProcessOrglDataReportPrintBLL processOrglDataReportPrintBLL = new ProcessOrglDataReportPrintBLL();
        public string mssg = "";
        public void main() {
            //TestTotalStationOrgldataReport();
            TestClassType();
        }
        public void TestTotalStationOrgldataReport()
        {
            
            string sql = "select point_name,cyc,Har,Var,SD,N,E,Z,search_time from  fmos_orgldata  where taskname=96  order by cyc,point_name,search_time asc  ";
            //var model = new QuerystanderdbIntModel(sql,106);
            OdbcConnection conn = db.GetStanderConn(96);
            DataTable dt = querysql.querystanderdb(sql,conn);
            var processTotalStationSPMCReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessOrglDataReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", "新白广城际铁路下穿京广高铁施工监测", "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            processOrglDataReportPrintBLL.ProcessTotalStationMPMCReport(processTotalStationSPMCReportModel,out mssg);
            ProcessPrintMssg.Print(mssg);

        }
        public void TestClassType()
        {
            ProcessAspectChart.AspectSeries aspectSeries = new ProcessAspectChart.AspectSeries();
            NFnet_MODAL.serie serie = new NFnet_MODAL.serie();
            //aspectSeries.Add<NFnet_MODAL.serie>(serie);
        }

    }
}
