﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using Tool;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.Gnss;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;
using SqlHelpers;
using NFnet_Interface.Other;

namespace NFnetUnitTest.TestTotalStationReport
{
    class ProcessMCUReport
    {
        public void main() {
            TestTotalStationReportSPMC();
            //TestTotalStationReportMPSC();
            //TestTotalStationReportMPMC();
        }
        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public ProcessTotalStationReportPrintBLL reportPrintBLL = new ProcessTotalStationReportPrintBLL();
        public ProcessMCUReportPrintBLL processMCUReportPrintBLL = new ProcessMCUReportPrintBLL();
        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public static ProcessBKGMCUData processBKGMCUData = new ProcessBKGMCUData();
        //public ProcessMCUDataRqcxConditionCreate processMCUDataRqcxConditionCreate = new ProcessMCUDataRqcxConditionCreate();
        //public ProcessMCUDATADbFill processMCUDATADbFill = new ProcessMCUDATADbFill();
        public ProcessStanderDBLoad processStanderDBLoad = new ProcessStanderDBLoad();
        public static string mssg = "";
        public string xmname = "韶关铀业"; 
        public void TestTotalStationReportSPMC()
        {

            //string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2','Z4-5','Z6-5','Z8-5','Z2-5','Z3-5') and cyc between 10 and 10 order by point_name , cyc ";
            //sql = @"select sname,r1*0.09875 as r_1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='应力' and dt>=format('2018-1-20 00:00:00','yyyy/mm/dd HH:mm:ss') and dt <= format('2018-1-23 10:30:00','yyyy/mm/dd HH:mm:ss') ";
            ////var MCUDataRqcxConditionCreateModel = processMCUDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018-1-20 00:00:00", "2018-1-24 00:00:00", QueryType.RQCX, "", "韶关铀业", new DateTime());
            //var fillInclinometerDbFillCondition = new FillBKGDbFillCondition("韶关745-J1-1,韶关745-J2-1", sql, "韶关铀业");

            //processBKGMCUData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
            //var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition("GPS1,GPS2", MCUDataRqcxConditionCreateModel.sql, 29);
            //sql = fillInclinometerDbFillCondition.sql;
            //var model = processBKGMCUData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
            List<ngnreportcondition> lngn = new List<ngnreportcondition>();
            lngn.Add(new ngnreportcondition { points = @"DZJCBY05,DZJCBYQ01,DZJCBYQ02,DZJCBY03,DZJCBYQ03,DZJCBYQ04,GZDTSDZHJ11", xmname = "地调院地下水位监测" });
            //lngn.Add(new ngnreportcondition { points = @"DZJCBY01,DZJCBY02", xmname = "市地调院" });
            //lngn.Add(new ngnreportcondition { points = @"DZJCBY09,DZJCBY011", xmname = "白云嘉禾望岗保障房" });
            //lngn.Add(new ngnreportcondition { points = @"DZJCBY03,DZJCBY04,DZJCBY05,DZJCBY06,DZJCBY07", xmname = "白云石丰路" });
            //lngn.Add(new ngnreportcondition { points = @"DZJCBY010", xmname = "白云鸦岗" });
            //lngn.Add(new ngnreportcondition { points = @"TXJC001,TXJCQ001,TXJC002,TXJCQ002,TXJC003", xmname = "白云同德围" });
            //lngn.Add(new ngnreportcondition { points = @"GZDTSDZHJ12,GZDTSDZHJ13,GZDTSDZHJ15,GZDTSDZHJ16,GZDTSDZHJ18,GZDTSDZHJ05", xmname = "荔湾大坦沙" });
            //lngn.Add(new ngnreportcondition { points = @"GZJSZDZHJ07,GZJSZDZHJ23", xmname = "白云金沙洲" });
            string sql = "select point_name ,r1,r2,sj from ngndata ";

            foreach (ngnreportcondition conditionmodel in lngn)
            {
                string tmp = string.Format(sql, conditionmodel.points.Replace(",","','"));
                DataTable dt = processStanderDBLoad.StanderDBLoad(tmp, "韶关铀业", Role.administratrorModel, false, out mssg);
                //dt = fillInclinometerDbFillCondition.dt;
                //var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
                //var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition,Role.monitorModel,false);
                //DataTable dt = null;
                //if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
                //{
                //    dt = processResultDataReportTableCreateModel.model.dt;
                //}
                //if (dt.Rows.Count == 0) { 
                //    sql = "select point_name ,r1,r2,sj from ngndata where point_name in ('{0}')  and hour(sj) ";
                //    string.Format(sql, conditionmodel.points.Replace(",", "','"));
                //     dt = processStanderDBLoad.StanderDBLoad(tmp, "韶关铀业", Role.administratrorModel, false, out mssg);
                //}
                Console.WriteLine(string.Format("{0}地下水位数据共{1}条", conditionmodel.xmname, dt.Rows.Count));
                var processTotalStationSPMCReportModel = new ProcessMCUReportPrintBLL.ProcessTotalStationSPMCReportModel("2018-1-17 00:00:00", "2018-4-20 00:00:00", conditionmodel.xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + conditionmodel.xmname + DateTime.Now.ToFileTime() + ".xlsx");
                //reportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
                processMCUReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);

            }
            ProcessPrintMssg.Print(mssg);
            
        }
        public class ngnreportcondition
        {
            public string points { get; set; }
            public string xmname { get; set; }
        }
        public void TestTotalStationReportMPSC()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2') and cyc between 1 and 10 order by point_name , cyc ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            //int cont = DataTableHelper.ProcessDataTableRowsCount(dt, "point_name");
            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPSCReport(processTotalStationMPSCReportModel, out mssg);
            //ChartHelper chartHelper = new ChartHelper();
            //chartHelper.ChartReportHelper(processTotalStationMPSCReportModel.exportpath, DataTableHelper.ProcessDataTableRowsCount(processTotalStationMPSCReportModel.dt, "point_name"), "点名");

        }

        public void TestTotalStationReportMPMC()
        {
            string sql = "select  cyc,point_name, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2') and cyc between 1 and 10 order by  cyc,point_name  ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }

            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPMCReport(processTotalStationMPSCReportModel, out mssg);


        }


    }
}
