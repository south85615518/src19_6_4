﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;
using System.Threading;

namespace NFnetUnitTest.TestTotalStationReport
{
   public class ProcessThreadPrintReport
    {
       public void main()
       {
           try
           {
               TheadProcess();
           }
           catch (Exception ex)
           {
               ProcessPrintMssg.Print(ex.Message);
           }
       }
       public void TheadProcess()
       {
           //Thread threaddtu = new Thread(DTUPrint);
           //threaddtu.Start();
           Thread threadgauge = new Thread(GaugePrint);
           threadgauge.Start();
           Thread threadgnss = new Thread(GnssPrint);
           threadgnss.Start();
       }
       public void DTUPrint()
       {
          (new  DTUReportHelper()).XInitializeWorkbook("d:/ReportExcel - 副本.xlsx");
       }
       public void GaugePrint()
       {
           ProcessTestGaugeReport processTestGaugeReport = new ProcessTestGaugeReport();
           processTestGaugeReport.main();
       }
       public void GnssPrint()
       {
           ProcessGnssStationReport processGnssStationReport = new ProcessGnssStationReport();
           processGnssStationReport.main();
       }

    }
}
