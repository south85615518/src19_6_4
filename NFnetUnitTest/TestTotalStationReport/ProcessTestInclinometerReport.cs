﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using System.Data;

namespace NFnetUnitTest.TestInclinometerReport
{
    public class ProcessTestInclinometerReport
    {
        public void main()
        {
            //TestInclinometerReportSPMC();
            //TestInclinometerReportMPSC();
            //TestInclinometerReportMPMC();
            TestInclinometerReportSPMC();
            //TestSenorDataMPMTTabCreate();
        }
        public ProcessInclinometerBLL resultDataBLL = new ProcessInclinometerBLL();
        public ProcessInclinometerCom inclinometerCom = new ProcessInclinometerCom();
        public ProcessSenorDataReportPrintBLL senorDataReportPrintBLL = new ProcessSenorDataReportPrintBLL();
        //public ProcessInclinometerReportPrintBLL reportPrintBLL = new ProcessInclinometerReportPrintBLL();
        //public ProcessSenorDataCom resultDataCom = new ProcessSenorDataCom();
        public static string mssg = "";
        public string xmname = "新白广城际铁路下穿京广高铁施工监测";
        public void TestInclinometerReportSPMC()
        {
            string sql = "select region,point_name,deep,previous_disp,ac_disp,this_disp,this_rap,mtimes,previous_time,time  from  senor_data where point_name in ('CX01') and time between '2018-09-12 15:00' and '2018-09-12 17:00' order by point_name , time ";

            Authority.Model.xmconnect xmmodel = Aspect.IndirectValue.GetXmconnectFromXmname("新白广城际铁路下穿京广高铁施工监测");

            Tool.SenorReport senorReport = new Tool.SenorReport
            {
                xmname = xmmodel.xmname,
                addr = xmmodel.xmaddress,
                jclx = "深部位移",
                instrument = "加拿大数字测斜仪RST",
                jcdw = xmmodel.jcdw,
                wtdw = xmmodel.delegateDw,
                tabHead = "深度(m),上次累计位移形变(mm),本次累计位移形变(mm),本次位移形变(mm),位移形变速率(mm/d)"

            };

            var resultDataReportTableCreateCondition = new SenorDataReportTableCreateCondition(sql, xmname, Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"));
            var processSenorDataReportTableCreateModel = new ProcessInclinometerCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (inclinometerCom.ProcessSenorDataReportTableCreate(processSenorDataReportTableCreateModel, out mssg))
            {
                dt = processSenorDataReportTableCreateModel.model.dt;
            }
            //var processInclinometerSPMTReportModel = new ProcessSenorDataReportPrintBLL.ProcessSenorDataSPMCReportModel(senorReport, Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"), "新白广城际铁路下穿京广高铁施工监测","D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            //senorDataReportPrintBLL.ProcessSenorDataSPMTReport(processInclinometerSPMTReportModel, out mssg);


        }

        public void TestSenorDataMPMTTabCreate()
        {
            string sql = "select point_name,time,this_disp, ac_disp,this_rap  from  senor_data where point_name in ('CX1-51','CX1-52') and time between '2017-11-16' and '2017-11-18' order by point_name , time ";
            var resultDataReportTableCreateCondition = new SenorDataReportTableCreateCondition(sql, xmname, Aspect.IndirectValue.GetXmnoFromXmname("新白广城际铁路下穿京广高铁施工监测"));
            var processSenorDataReportTableCreateModel = new ProcessInclinometerCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (inclinometerCom.ProcessSenorDataReportTableCreate(processSenorDataReportTableCreateModel, out mssg))
            {
                dt = processSenorDataReportTableCreateModel.model.dt;
            }
            //var processInclinometerSPMTReportModel = new ProcessSenorDataReportPrintBLL.ProcessSenorDataSPMCReportModel("2017-11-16", "2017-11-18", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            //senorDataReportPrintBLL.ProcessSenorDataMPMTReport(processInclinometerSPMTReportModel, out mssg);
        }
    }
}
