﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.BKG;
using NFnet_BLL.Other;
using System.Data;
using System.IO;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_Interface.DisplayDataProcess.GNSS;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.Gnss;
using Tool;
using NFnet_Interface.DisplayDataProcess.NGN_HXYL;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using System.Web.Script.Serialization;

namespace NFnetUnitTest.TestTotalStationReport
{
    public class ProcessAutoReport
    {



        public string templatepath { get; set; }
        public string xmname { get; set; }
        public JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessGaugeReportPrintBLL processGaugeReportPrintBLL = new ProcessGaugeReportPrintBLL();

        public ProcessBKGMCUPointLoad processBKGMCUPointLoad = new ProcessBKGMCUPointLoad();
        public ProcessBKGMCUData processBKGMCUData = new ProcessBKGMCUData();
        public ProcessMCUReportPrintBLL processMCUReportPrintBLL = new ProcessMCUReportPrintBLL();

        public ProcessBKGMCUAnglePointLoad processBKGMCUAnglePointLoad = new ProcessBKGMCUAnglePointLoad();
        public ProcessBKGMCUAngleData processBKGMCUAngleData = new ProcessBKGMCUAngleData();
        public ProcessMCUAngleReportPrintBLL processMCUAngleReportPrintBLL = new ProcessMCUAngleReportPrintBLL();


        public ProcessGNSSPointLoadBLL processGNSSPointLoadBLL = new ProcessGNSSPointLoadBLL();
        public ProcessGNSSDataRqcxConditionCreate processGNSSDataRqcxConditionCreate = new ProcessGNSSDataRqcxConditionCreate();
        public ProcessGNSSDATADbFill processGNSSDATADbFill = new ProcessGNSSDATADbFill();
        public ProcessGnssReportPrintBLL processGnssReportPrintBLL = new ProcessGnssReportPrintBLL();


        public ProcessTotalStationPointLoad processTotalStationPointLoad = new ProcessTotalStationPointLoad();
        public ProcessResultDataRqcxConditionCreate processResultDataRqcxConditionCreate = new ProcessResultDataRqcxConditionCreate();
        public ProcessFillTotalStationDbFill fillTotalStationDbFill = new ProcessFillTotalStationDbFill();
        public ProcessTotalStationReportPrintBLL processTotalStationReportPrintBLL = new ProcessTotalStationReportPrintBLL();
        public ProcessModelListIntoDataTable processModelListIntoDataTable = new ProcessModelListIntoDataTable();
        public ProcessDataTableIntoModelList processDataTableIntoModelList = new ProcessDataTableIntoModelList();
        public ProcessReportDelegateSend processReportDelegateSend = new ProcessReportDelegateSend();



        public ProcessHXYLDataRqcxConditionCreateDay processHXYLDataRqcxConditionCreateDay = new ProcessHXYLDataRqcxConditionCreateDay();
        NFnet_Interface.DisplayDataProcess.HXYL.ProcessHXYLIDBLL processHXYLIDBLL = new NFnet_Interface.DisplayDataProcess.HXYL.ProcessHXYLIDBLL();
        public ProcessHXYLReportPrintBLL processHXYLReportPrintBLL = new ProcessHXYLReportPrintBLL();
        public ProcessHXYLData processHXYLData = new ProcessHXYLData();
        public ProcessHXYLPointBLL processHXYLPointBLL = new ProcessHXYLPointBLL();


        public ProcessEmailSendBLL processEmailSendBLL = new ProcessEmailSendBLL();

        public Mutex mutex = new Mutex();
        public int cnt = 0;
        public string reportpath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "自动报表\\";
        public string reportrangename = "";
        public string mssg = "";

        ManualResetEvent manualEvent = new ManualResetEvent(false);

        public void main()
        {

            
            //try
            //{
            //    this.xmname = "从化边坡监测";
            //    this.templatepath = "d:/ReportExcel - 副本.xlsx";
            //    WeekReport();
            //}
            //catch (Exception ex)
            //{
            //    ProcessPrintMssg.Print(string.Format("{0}周报生成出错,错误信息："+ex.Message));
            //}
        }
        public void WeekReport(string jclxstr)
        {
            cnt = 0;
            List<string> jclxs = jclxstr.Split(',').ToList();
            DateTime dt = DateTime.Now;
            int weekdays = Convert.ToInt32(dt.DayOfWeek);
            DateTime starttime = Convert.ToDateTime(string.Format("{0}/{1}/{2} 00:00", dt.Year, dt.Month, dt.Day)).AddDays(-7);
            DateTime endtime = DateTime.Now;
            int weekidx = DateHelper.GetWeekOfYear(endtime);
            reportpath += string.Format("\\{0}\\第{1}周{2}_{3}_{4}_{5}数据报表\\", xmname, weekidx,starttime.Month,starttime.Day,endtime.Month,endtime.Day);
            if (!Directory.Exists(reportpath)) Directory.CreateDirectory(reportpath);
            reportrange range = new reportrange { starttime = starttime, endtime = endtime, rangename = string.Format("{0}年第{1}周数据报表", endtime.Year, weekidx) };
            if (jclxs.Contains("库水位"))
            {
                Thread tgauge = new Thread(new ParameterizedThreadStart(Gauge));
                tgauge.Start(range);
                cnt++;
            }
            if (jclxs.Contains("浸润线"))
            {
                Thread tmcu = new Thread(new ParameterizedThreadStart(Mcu));
                tmcu.Start(range);
                cnt++;
            }
            if (jclxs.Contains("固定测斜"))
            {
                Thread tmcuangle = new Thread(new ParameterizedThreadStart(McuAngle));
                tmcuangle.Start(range);
                cnt++;
            }
            if (jclxs.Contains("GNSS"))
            {
                Thread tgnss = new Thread(new ParameterizedThreadStart(Gnss));
                tgnss.Start(range);
                cnt++;
            }
            if (jclxs.Contains("雨量"))
            {
                Thread tHXYL = new Thread(new ParameterizedThreadStart(HXYL));
                tHXYL.Start(range);
                cnt++;
            }
            if (jclxs.Contains("TotalStationMPSC"))
            {
                Thread tTotalStationMPSC = new Thread(new ParameterizedThreadStart(TotalStationMPSC));
                tTotalStationMPSC.Start(range);
                cnt++;
            }
            Console.WriteLine(DateTime.Now+" 周报开始打印");
            manualEvent.WaitOne();
            ExceptionLog.AutoReportWrite(string.Format("{2} {0}年第{1}周数据报表生成完成", endtime.Year, weekidx,DateTime.Now));
            Console.WriteLine(DateTime.Now+" 周报打印完成,开始打包...");
            FileHelper.DirZipPack(Path.GetDirectoryName(reportpath));

            processEmailSendBLL.ProcessEmailSend(new string[1] { reportpath.Substring(0, reportpath.LastIndexOf("\\")) + ".zip" }.ToList(), string.Format("{5}第{0}周[{1}月{2}日-{3}月{4}日]数据报表|" + jclxstr, weekidx, starttime.Month, starttime.Day, endtime.Month, endtime.Day, xmname), xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            Console.WriteLine(DateTime.Now+" "+mssg);
            ExceptionLog.AutoReportWrite(DateTime.Now+" "+mssg);
            
        }


        public void MonthReport(string jclxstr)
        {
            cnt = 0;
            List<string> jclxs = jclxstr.Split(',').ToList();
            DateTime dt = DateTime.Now;
            int weekdays = Convert.ToInt32(dt.DayOfWeek);
            DateTime starttime = dt.AddDays(-38);
            DateTime endtime = dt;
            int weekidx = DateHelper.GetWeekOfYear(endtime);
            reportpath += string.Format("\\{0}\\{2}年{1}月数据报表\\", xmname, DateTime.Now.Month,dt.Year);
            if (!Directory.Exists(reportpath)) Directory.CreateDirectory(reportpath);
            reportrange range = new reportrange { starttime = starttime, endtime = endtime, rangename = string.Format("{0}年{1}月数据报表", endtime.Year, endtime.Month) };
            if (jclxs.Contains("库水位"))
            {
                Thread tgauge = new Thread(new ParameterizedThreadStart(Gauge));
                tgauge.Start(range);
                cnt++;
            }
            if (jclxs.Contains("浸润线"))
            {
                Thread tmcu = new Thread(new ParameterizedThreadStart(Mcu));
                tmcu.Start(range);
                cnt++;
            }
            if (jclxs.Contains("固定测斜"))
            {
                Thread tmcuangle = new Thread(new ParameterizedThreadStart(McuAngle));
                tmcuangle.Start(range);
                cnt++;
            }
            if (jclxs.Contains("GNSS"))
            {
                Thread tgnss = new Thread(new ParameterizedThreadStart(Gnss));
                tgnss.Start(range);
                cnt++;
            }
            if (jclxs.Contains("雨量"))
            {
                Thread tHXYL = new Thread(new ParameterizedThreadStart(HXYL));
                tHXYL.Start(range);
                cnt++;
            }
            if (jclxs.Contains("TotalStationMPSC"))
            {
                Thread tTotalStationMPSC = new Thread(new ParameterizedThreadStart(TotalStationMPSC));
                tTotalStationMPSC.Start(range);
                cnt++;
            }
            Console.WriteLine(DateTime.Now+" 月报报开始打印");
            manualEvent.WaitOne();
            ExceptionLog.AutoReportWrite(string.Format("{2} {0}年{1}月数据报表生成完成", endtime.Year, endtime.Month,DateTime.Now));
            Console.WriteLine(DateTime.Now+" 月报报打印完成,开始打包...");
            FileHelper.DirZip(reportpath, Path.GetDirectoryName(reportpath), "月报打包");
            //processEmailSendBLL.ProcessEmailSend(new string[1] { reportpath.Substring(0, reportpath.LastIndexOf("\\")) + ".zip" }.ToList(), string.Format("{1}{0}月数据报表|" + jclxstr, DateTime.Now.Month,dt.Year), xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            Console.WriteLine(DateTime.Now + " " + mssg);
            ExceptionLog.AutoReportWrite(DateTime.Now + " " + mssg);
        }



        public void Threadwait()
        {
            Thread.Sleep(5000);
            manualEvent.Set();
        }

        public void Gauge(object obj)
        {

            reportrange range = obj as reportrange;
            Console.WriteLine("库水位" + range.rangename + "开始打印");
            string sql = string.Format("select point_name,deep,predeep,thisdeep,acdeep,rap,time,holedepth,times  from  gaugedatareport where time < '{0}' and time >'{1}' and  hour(time) in (0,6,12,18)  and   minute(time) < 10   order by point_name , time asc", range.endtime, range.starttime);
            var queryremoteModel = new QueryremoteModel(sql, "smos_client");
            if (!ProcessComBLL.Processqueryremotedb(queryremoteModel, out mssg))
            {
                return;
            }
            DataTable dt = queryremoteModel.dt;
            var processTotalStationSPMCReportModel = new ProcessGaugeReportPrintBLL.ProcessTotalStationSPMCReportModel("", "", xmname, templatepath, dt, Path.GetDirectoryName(reportpath) + "\\库水位" + range.rangename + ".xlsx");
            processGaugeReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            Console.WriteLine("库水位" + range.rangename + "打印完成");
            if (--cnt == 0) manualEvent.Set();
            //range.manualResetEvent.Set();
        }
        public void Mcu(object obj)
        {
            reportrange range = obj as reportrange;
            Console.WriteLine("浸润线" + range.rangename + "开始打印");
            string sql = string.Format(@"select sname,r1*0.09875 as r_1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and  sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='应力' and datepart('s',dt) in (0,5,10,15,20,25,30,35,40,45,50,55,60)  and dt>=format('{0}','yyyy/mm/dd HH:mm:ss') and dt <= format('{1}','yyyy/mm/dd HH:mm:ss')   ", range.starttime, range.endtime);
            List<string> ls = processBKGMCUPointLoad.BKGMCUPointLoad(xmname, out mssg);
            var fillInclinometerDbFillCondition = new FillBKGDbFillCondition(string.Join(",", ls), sql, xmname);

            processBKGMCUData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
            DataTable dt = null;
            dt = fillInclinometerDbFillCondition.dt;
            var processTotalStationSPMCReportModel = new ProcessMCUReportPrintBLL.ProcessTotalStationSPMCReportModel("", "", xmname, templatepath, dt, Path.GetDirectoryName(reportpath) + "\\浸润线" + range.rangename + ".xlsx");
            processMCUReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            Console.WriteLine("浸润线" + range.rangename + "打印完成");
            if (--cnt == 0) manualEvent.Set();
            //range.manualResetEvent.Set();
        }
        public void McuAngle(object obj)
        {
            reportrange range = obj as reportrange;
            Console.WriteLine("固定测斜" + range.rangename + "开始打印");
            string sql = string.Format(@"select sname,r1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='位移/角度'  and   datepart('s',dt) in (0,5,10,15,20,25,30,35,40,45,50,55,60)    and    dt>=format('{0}','yyyy/mm/dd HH:mm:ss') and dt <= format('{1}','yyyy/mm/dd HH:mm:ss')  ", range.starttime, range.endtime);
            List<string> ls = processBKGMCUAnglePointLoad.BKGMCUAnglePointLoad(xmname, out mssg);
            var fillInclinometerDbFillCondition = new FillBKGDbFillCondition(string.Join(",", ls), sql, xmname);

            processBKGMCUAngleData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
            DataTable dt = null;
            dt = fillInclinometerDbFillCondition.dt;
            var processTotalStationSPMCReportModel = new ProcessMCUAngleReportPrintBLL.ProcessTotalStationSPMCReportModel(range.starttime.ToString(), range.endtime.ToString(), xmname, templatepath, dt, Path.GetDirectoryName(reportpath) + "\\固定测斜" + range.rangename + ".xlsx");
            processMCUAngleReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            Console.WriteLine("固定测斜" + range.rangename + "打印完成");
            if (--cnt == 0) manualEvent.Set();
            //range.manualResetEvent.Set();
        }
        public void Gnss(object obj)
        {
            reportrange range = obj as reportrange;
            Console.WriteLine("GNSS" + range.rangename + "开始打印");
            List<string> ls = processGNSSPointLoadBLL.GNSSPointLoadBLL(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            var gnssDataRqcxConditionCreateModel = processGNSSDataRqcxConditionCreate.ResultDataRqcxConditionCreate(range.starttime.ToString(), range.endtime.ToString(), QueryType.RQCX, "", xmname, new DateTime());
            string sql = gnssDataRqcxConditionCreateModel.sql;
            var model = processGNSSDATADbFill.GNSSDbFill(string.Join(",", ls), sql, Aspect.IndirectValue.GetXmnoFromXmname(xmname));
            DataTable dt = null;
            dt = model.dt;
            var processTotalStationSPMCReportModel = new ProcessGnssReportPrintBLL.ProcessTotalStationSPMCReportModel("", "", xmname, templatepath, dt, Path.GetDirectoryName(reportpath) + "\\GNSS" + range.rangename + ".xlsx");
            processGnssReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            Console.WriteLine("GNSS" + range.rangename + "打印完成");
            if (--cnt == 0) manualEvent.Set();
            //range.manualResetEvent.Set();
        }
        public void TotalStationMPSC(object obj)
        {
            reportrange range = obj as reportrange;
            Console.WriteLine("TotalStation" + range.rangename + "开始打印");
            List<string> ls = processTotalStationPointLoad.TotalStationPointLoad(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);

            var resultDataRqcxConditionCreateModel = processResultDataRqcxConditionCreate.ResultDataRqcxConditionCreate(range.starttime.ToString(), range.endtime.ToString(), QueryType.RQCX, "", xmname, new DateTime());
            string sql = resultDataRqcxConditionCreateModel.sql;
            var model = fillTotalStationDbFill.FillTotalStationDbFill(string.Join(",", ls), sql, xmname,NFnet_BLL.LoginProcess.Role.administratrorModel,false);
            DataTable dt = null;
            dt = model.model.dt;

            if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + xmname + "\\offset.txt"))
            {
                ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...",xmname));
                dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + xmname + "\\offset.txt");
            }
            if (dt.Rows.Count < 10)
            {
                var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("", "", xmname, templatepath, dt, Path.GetDirectoryName(reportpath) + "\\表面位移_多点单周期" + range.rangename + ".xlsx");
                processTotalStationReportPrintBLL.ProcessTotalStationMPSCReport(processTotalStationMPSCReportModel, out mssg);
            }
            else
            {
                List<TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist = processDataTableIntoModelList.DataTableIntoModelList(dt, out mssg);

                dt = processModelListIntoDataTable.ModelListIntoDataTable(xmname, cycdirnetlist, out mssg);
                jss.MaxJsonLength = 640000000;
                //2097152
                string cycdirnetliststr = jss.Serialize(cycdirnetlist);

                Tool.DATAREPORT.Pub.reportprotocol reportprotocol = new Tool.DATAREPORT.Pub.reportprotocol
                {
                    lobj = cycdirnetliststr,
                    expath = "表面位移_多点单周期" + range.rangename + DateTime.Now.ToFileTime() + ".xlsx",
                    printtype = Tool.DATAREPORT.Pub.printtype.TOTALSTATION_MPSC,
                    xmname = xmname
                };
                processReportDelegateSend.testreportdelegate(reportprotocol);
                int timewait = 2*60*60;
                string recpath = "";
                while(timewait>0)
                {
                    Thread.Sleep(10 * 1000);
                    if ((recpath = Tool.ExceptionLog.IsReportRecFinished(reportprotocol.expath) )!= null)
                    {
                        File.Copy(recpath, Path.GetDirectoryName(reportpath) + "\\表面位移_多点单周期" + range.rangename + ".xlsx",true);
                        ExceptionLog.ExceptionWrite(reportprotocol.expath+"报表托管打印完成");
                        break;
                    }
                    timewait -= 10;

                }
            }
            Console.WriteLine("TotalStation" + range.rangename + "打印完成");
            if (--cnt == 0) manualEvent.Set();
            //range.manualResetEvent.Set();
        }
        public void HXYL(object obj)
        {

            //ExceptionLog.ExceptionWrite("日期查询");
            reportrange range = obj as reportrange;
            Console.WriteLine("雨量" + range.rangename + "开始打印");
            var resultDataRqcxConditionCreateCondition = processHXYLDataRqcxConditionCreateDay.ResultDataRqcxConditionCreateDay(range.starttime.ToString(), range.endtime.ToString(), QueryType.RQCX, "", xmname, new DateTime());
            List<string> ls = processHXYLPointBLL.HXYLPoint(Aspect.IndirectValue.GetXmnoFromXmname(xmname), out mssg);
            List<string> lis = new List<string>();

            foreach (var pointname in ls)
            {
                ExceptionLog.ExceptionWrite("雨量点名转换");
                lis.Add(processHXYLIDBLL.HXYLPointID(Aspect.IndirectValue.GetXmnoFromXmname(xmname), pointname, out mssg));
                ExceptionLog.ExceptionWrite("雨量点名转换完成");
            }
            var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition(string.Join(",", lis), resultDataRqcxConditionCreateCondition.sql, Aspect.IndirectValue.GetXmnoFromXmname(xmname));
            //ExceptionLog.ExceptionWrite("水位数据表生成");
            processHXYLData.ProcessInclinometerDbFillDay(fillInclinometerDbFillCondition);

            var processTotalStationSPMCReportModel = new ProcessHXYLReportPrintBLL.ProcessTotalStationSPMCReportModel(range.starttime.ToString(), range.endtime.ToString(), xmname, templatepath, fillInclinometerDbFillCondition.dt, Path.GetDirectoryName(reportpath) + "\\雨量" + range.rangename + ".xlsx");
            //reportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            processHXYLReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            Console.WriteLine("雨量" + range.rangename+"打印完成");
            if (--cnt == 0) manualEvent.Set();
            //range.manualResetEvent.Set();


        }
        public class reportrange
        {
            public string rangename { get; set; }
            public DateTime starttime { get; set; }
            public DateTime endtime { get; set; }
            public ManualResetEvent manualResetEvent { get; set; }
        }


    }
}
