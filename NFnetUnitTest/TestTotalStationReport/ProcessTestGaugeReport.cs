﻿using System;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.Other;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.BKG;

namespace NFnetUnitTest.TestTotalStationReport
{
    public class ProcessTestGaugeReport
    {
        public void main()
        {
            TestDTUReportSPMC();
            //TestDTUReportMPSC();
            //TestDTUReportMPMC();
            //TestDTUReportSPMC();
            //TestSenorDataMPMTTabCreate();
            //TestDTUReportPrintTime(336);
        }
        //public ProcessDTUBLL resultDataBLL = new ProcessDTUBLL();
        //public ProcessDTUCom inclinometerCom = new ProcessDTUCom();
        public ProcessGaugeReportPrintBLL processGaugeReportPrintBLL = new ProcessGaugeReportPrintBLL();
        //public ProcessDTUReportPrintBLL reportPrintBLL = new ProcessDTUReportPrintBLL();
        public ProcessDTUSenorCom resultDataCom = new ProcessDTUSenorCom();
        public static string mssg = "";
        public string xmname = "韶关铀业";
        public void TestDTUReportSPMC()
        {
            double a = (double)Math.Round((decimal)0.049999999999996,1,MidpointRounding.AwayFromZero);
            string sql = "select point_name,deep,predeep,thisdeep,acdeep,rap,time,holedepth,times  from  gaugedatareport where time < '2018-7-31 10:00:00' and time >'2018-7-1 22:00:00'  and  minute(time) < 5  order by point_name , time asc";

            Authority.Model.xmconnect xmmodel = Aspect.IndirectValue.GetXmconnectFromXmname("韶关铀业");

            Tool.SenorReport senorReport = new Tool.SenorReport
            {
                xmname = xmmodel.xmname,
                addr = xmmodel.xmaddress,
                jclx = "地下水位",
                instrument = "投入式水位计",
                jcdw = xmmodel.jcdw,
                wtdw = xmmodel.delegateDw,
                tabHead = "点名,上次水深,本次水深,本次变化量,累计变化量,变化速率,时间"

            };

            var queryremoteModel = new QueryremoteModel(sql, "smos_client");
            if (!ProcessComBLL.Processqueryremotedb(queryremoteModel, out mssg))
            {
                return ;
            }
                DataTable dt = queryremoteModel.dt;


            var processTotalStationSPMCReportModel = new ProcessGaugeReportPrintBLL.ProcessTotalStationSPMCReportModel("2018-2-10 00:00:00", "2018-2-20 00:00:00", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            //reportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            processGaugeReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);


        }
        public ProcessDTUReportPrintTime processDTUReportPrintTime = new ProcessDTUReportPrintTime();
        public void TestDTUReportPrintTime(int rows)
        {
            string reportprintstr = processDTUReportPrintTime.DTUReportPrintTime(rows);
            ProcessPrintMssg.Print(reportprintstr);
        }
        //public void TestSenorDataMPMTTabCreate()
        //{
        //    string sql = "select point_name,time,this_disp, ac_disp,this_rap  from  senor_data where point_name in ('CX1-51','CX1-52') and time between '2017-11-16' and '2017-11-18' order by point_name , time ";
        //    var resultDataReportTableCreateCondition = new SenorDataReportTableCreateCondition(sql, xmname, Aspect.IndirectValue.GetXmnoFromXmname("韶关铀业"));
        //    var processSenorDataReportTableCreateModel = new ProcessDTUCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
        //    DataTable dt = null;
        //    if (inclinometerCom.ProcessSenorDataReportTableCreate(processSenorDataReportTableCreateModel, out mssg))
        //    {
        //        dt = processSenorDataReportTableCreateModel.model.dt;
        //    }
        //    //var processDTUSPMTReportModel = new ProcessSenorDataReportPrintBLL.ProcessSenorDataSPMCReportModel("2017-11-16", "2017-11-18", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
        //    //senorDataReportPrintBLL.ProcessSenorDataMPMTReport(processDTUSPMTReportModel, out mssg);
        //}
    }
}
