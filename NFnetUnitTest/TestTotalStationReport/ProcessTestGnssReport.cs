﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using Tool;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.Gnss;
using NPOI.SS.UserModel;
using System.IO;

namespace NFnetUnitTest.TestTotalStationReport
{
    class ProcessGnssStationReport
    {
        public void main() {
            TestTotalStationReportSPMC();
            //TestTotalStationReportMPSC();
            //TestTotalStationReportMPMC();
            //TestGnssChart("d:/131683659645695150.xlsx");
           //write();
        }
        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public ProcessTotalStationReportPrintBLL reportPrintBLL = new ProcessTotalStationReportPrintBLL();
        public ProcessGnssReportPrintBLL processGnssReportPrintBLL = new ProcessGnssReportPrintBLL();
        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public ProcessGNSSDataRqcxConditionCreate processGNSSDataRqcxConditionCreate = new ProcessGNSSDataRqcxConditionCreate();
        public ProcessGNSSDATADbFill processGNSSDATADbFill = new ProcessGNSSDATADbFill();
        public static string mssg = "";
        public string xmname = "数据测试"; 
        public void TestTotalStationReportSPMC()
        {

            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2','Z4-5','Z6-5','Z8-5','Z2-5','Z3-5') and cyc between 10 and 10 order by point_name , cyc ";
            sql = @"select a.point_name,a.x,a.y,a.z,a.this_x,a.this_y,a.this_z,a.l_x,a.l_y,a.l_z,time 
from gnssdatareport a  where   POINT_NAME in('sg01','sg02','sg03')   and Time>=date_format('2018-07-01 08:56:52','%y-%m-%d %H:%i:%s') and Time<=date_format('2017-8-9 00:00:00','%y-%m-%d %H:%i:%s') order by POINT_NAME,Time asc ";
            var gnssDataRqcxConditionCreateModel = processGNSSDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018-7-1 11:00:52", "2018-8-9 00:00:00", QueryType.RQCX, "", "韶关铀业", new DateTime());

            //var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition("GPS1,GPS2", gnssDataRqcxConditionCreateModel.sql, 29);
            sql = gnssDataRqcxConditionCreateModel.sql;
            var model = processGNSSDATADbFill.GNSSDbFill("sg01,sg02,sg03", /*gnssDataRqcxConditionCreateMo00del.sql*/sql, 29);



            DataTable dt = null;
            dt = model.dt;
            //var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            //var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition,Role.monitorModel,false);
            //DataTable dt = null;
            //if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            //{
            //    dt = processResultDataReportTableCreateModel.model.dt;
            //}
            var processTotalStationSPMCReportModel = new ProcessGnssReportPrintBLL.ProcessTotalStationSPMCReportModel("4", "6", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            //reportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            processGnssReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);

        }

        public void TestTotalStationReportMPSC()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2') and cyc between 1 and 10 order by point_name , cyc ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            //int cont = DataTableHelper.ProcessDataTableRowsCount(dt, "point_name");
            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPSCReport(processTotalStationMPSCReportModel, out mssg);
            //ChartHelper chartHelper = new ChartHelper();
            //chartHelper.ChartReportHelper(processTotalStationMPSCReportModel.exportpath, DataTableHelper.ProcessDataTableRowsCount(processTotalStationMPSCReportModel.dt, "point_name"), "点名");

        }

        public void TestTotalStationReportMPMC()
        {
            string sql = "select  cyc,point_name, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2') and cyc between 1 and 10 order by  cyc,point_name  ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }

            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPMCReport(processTotalStationMPSCReportModel, out mssg);


        }
        public void TestGnssChart(string path)
        {
            //GnssChartHelper chartHelper = new GnssChartHelper();
            //chartHelper.ChartReportHelper(model.exportpath, DataTableHelper.ProcessDataTableRowsCount(model.dt, "cyc"), "周期");
            //chartHelper.ChartListReportHelper(path, new List<ChartCreateEnviroment>());
        }
        public void write()
        {
        //    GnssReportHelper.excelwrite("D:/131683377693878428 - 副本 (5).xlsx");
        //    TestGnssChart(Path.GetDirectoryName("D:/131683377693878428 - 副本 (5).xlsx") + "/" + Path.GetFileNameWithoutExtension("D:/131683377693878428 - 副本 (5).xlsx") + "模板数据.xlsx");
        }
    }
}
