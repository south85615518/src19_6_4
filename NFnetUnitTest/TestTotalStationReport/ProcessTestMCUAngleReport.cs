﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using Tool;
using NFnet_Interface.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.Gnss;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.BKG;
using NFnet_BLL.DisplayDataProcess.BKG;
using SqlHelpers;

namespace NFnetUnitTest.TestTotalStationReport
{
    public class ProcessMCUAngleReport
    {
        public void main() {
            TestTotalStationReportSPMC();
            //TestTotalStationReportMPSC();
            //TestTotalStationReportMPMC();
        }
        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public ProcessTotalStationReportPrintBLL reportPrintBLL = new ProcessTotalStationReportPrintBLL();
        public ProcessMCUAngleReportPrintBLL processMCUAngleReportPrintBLL = new ProcessMCUAngleReportPrintBLL();
        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public static ProcessBKGMCUAngleData processBKGMCUAngleData = new ProcessBKGMCUAngleData();
        //public ProcessMCUDataRqcxConditionCreate processMCUDataRqcxConditionCreate = new ProcessMCUDataRqcxConditionCreate();
        //public ProcessMCUDATADbFill processMCUDATADbFill = new ProcessMCUDATADbFill();
        public static string mssg = "";
        public string xmname = "从化边坡监测"; 
        public void TestTotalStationReportSPMC()
        {

            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2','Z4-5','Z6-5','Z8-5','Z2-5','Z3-5') and cyc between 10 and 10 order by point_name , cyc ";
            sql = @"select sname,r1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='位移/角度' and dt>=format('2018-2-7 00:00:00','yyyy/mm/dd HH:mm:ss') and dt <= format('2018-2-7 09:30:00','yyyy/mm/dd HH:mm:ss')  ";
            //sql = @"select sname,r1*0.09875 as r_1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='应力' and dt>=format('2018-2-7 00:00:00','yyyy/mm/dd HH:mm:ss') and dt <= format('2018-2-7 09:30:00','yyyy/mm/dd HH:mm:ss') ";
            //var MCUDataRqcxConditionCreateModel = processMCUDataRqcxConditionCreate.ResultDataRqcxConditionCreate("2018-1-20 00:00:00", "2018-1-24 00:00:00", QueryType.RQCX, "", "韶关铀业", new DateTime());
            var fillInclinometerDbFillCondition = new FillBKGDbFillCondition("韶关745-N2-1,韶关745-N2-2", sql, "韶关铀业");

            processBKGMCUAngleData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);
            //var fillInclinometerDbFillCondition = new FillInclinometerDbFillCondition("GPS1,GPS2", MCUDataRqcxConditionCreateModel.sql, 29);
            //sql = fillInclinometerDbFillCondition.sql;
            //var model = processBKGMCUData.ProcessInclinometerDbFill(fillInclinometerDbFillCondition);



            DataTable dt = null;
            dt = fillInclinometerDbFillCondition.dt;

            sql = @"SELECT   'CX'+convert(varchar,[GID]) as sname
      ,[S] as r1,20 as r2,
		[MonitorTime] as dt
  FROM McudInclinometerinfo where  DATEPART(hh,MonitorTime) in(0) order by GID,monitortime asc  ";

            DataTable[] dtary = new DataTable[4];

            querysql.querysqlserverstanderdb(sql, "SMOSDB2014", out dtary[0]);
            
            querysql.querysqlserverstanderdb(sql, "SMOSDB2015", out dtary[1]);
            
            querysql.querysqlserverstanderdb(sql, "SMOSDB2016", out dtary[2]);
            
            querysql.querysqlserverstanderdb(sql, "SMOSDB2017", out dtary[3]);

            dt = DataTableHelper.ProcessDataTableMerge(dtary.ToList());
            DataView dv = new DataView(dt);
            dv.Sort = "sname,dt asc";
            DataTable dvtable = dv.ToTable();
            //var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            //var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition,Role.monitorModel,false);
            //DataTable dt = null;
            //if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            //{
            //    dt = processResultDataReportTableCreateModel.model.dt;
            //}
            var processTotalStationSPMCReportModel = new ProcessMCUAngleReportPrintBLL.ProcessTotalStationSPMCReportModel("2018-2-7 00:00:00", "2018-2-7 09:30:00", xmname, "D:\\ReportExcel - 副本.xlsx", dvtable, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            //reportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);
            processMCUAngleReportPrintBLL.ProcessTotalStationSPMCReport(processTotalStationSPMCReportModel, out mssg);

        }

        public void TestTotalStationReportMPSC()
        {
            string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2') and cyc between 1 and 10 order by point_name , cyc ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }
            //int cont = DataTableHelper.ProcessDataTableRowsCount(dt, "point_name");
            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPSCReport(processTotalStationMPSCReportModel, out mssg);
            //ChartHelper chartHelper = new ChartHelper();
            //chartHelper.ChartReportHelper(processTotalStationMPSCReportModel.exportpath, DataTableHelper.ProcessDataTableRowsCount(processTotalStationMPSCReportModel.dt, "point_name"), "点名");

        }

        public void TestTotalStationReportMPMC()
        {
            string sql = "select  cyc,point_name, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet where point_name in ('Z7-4','Z7-5','Z5-2') and cyc between 1 and 10 order by  cyc,point_name  ";
            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql, xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, Role.monitorModel, false);
            DataTable dt = null;
            if (resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                dt = processResultDataReportTableCreateModel.model.dt;
            }

            var processTotalStationMPSCReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationSPMCReportModel("1", "10", xmname, "D:\\ReportExcel - 副本.xlsx", dt, "d:\\" + DateTime.Now.ToFileTime() + ".xlsx");
            reportPrintBLL.ProcessTotalStationMPMCReport(processTotalStationMPSCReportModel, out mssg);


        }


    }
}
