﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
namespace NFnetUnitTest.DataTest
{
    public class TestJson
    {
        public JavaScriptSerializer jss = new JavaScriptSerializer();
        public void main()
        {
            //string datajson = jss.Serialize(new httpuploaddata());
            TestJsonDescode();
        }
        public void TestJsonDescode()
        {
            string json = @"{
  'sn': 1,
  'file': {
    'RecTime': '2019-05-16T16:25:46.7799062+08:00',
    'FileLen': 1,
    'Data': [
      [
        1.1,
        2.1
      ],
      [
        1.1,
        2.1
      ]
    ],
    'MaxVal': 1.1,
    'FileNo': 1
  },
  'Key': 'sample string 2',
  'MaxValue': [
    1.1,
    2.1
  ],
  'FileNo': 3,
  'Type': 'sample string 4'
}";
            var model = jss.DeserializeObject(json);
            

        }
        
    }
   



    public class httpuploaddata
    {
        public string project_name { get; set; }
        public string point_name { get; set; }
        public string datatype { get; set; }
        public string valuetype { get; set; }
        public double oregion_N { get; set; }
        public double oregion_E { get; set; }
        public double oregion_Z { get; set; }
        public double single_oregion_scalarvalue { get; set; }
        public double first_oregion_scalarvalue { get; set; }
        public double sec_oregion_scalarvalue { get; set; }
        public double this_dN { get; set; }
        public double this_dE { get; set; }
        public double this_dZ { get; set; }
        public double ac_dN { get; set; }
        public double ac_dE { get; set; }
        public double ac_dZ { get; set; }
        public double single_this_scalarvalue { get; set; }
        public double single_ac_scalarvalue { get; set; }
        public double first_this_scalarvalue { get; set; }
        public double first_ac_scalarvalue { get; set; }
        public double sec_this_scalarvalue { get; set; }
        public double sec_ac_scalarvalue { get; set; }
        public int cyc { get; set; }
        public DateTime time { get; set; }
    }
    

}
