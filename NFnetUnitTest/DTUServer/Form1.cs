﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using Tool;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.LoginProcess;
using System.Threading;

namespace NFnetUnitTest.DTUServer
{
    public partial class Form1 : Form
    {
        public static TcpListener tcpListener = null;
        public delegate void ClientConnectEvent(IAsyncResult ar);
        private event ClientConnectEvent conEvent;
        public delegate void TextBoxFlush(string filename);
        public event TextBoxFlush FlushEvent;
        public System.Timers.Timer timer = new System.Timers.Timer(10000);
        public static string ip;
        public static string port;
        public static Dictionary<TcpListener, Socket> tcpListenerdts = new Dictionary<TcpListener, Socket>();
        public static Dictionary<TcpListener, int> tcpListenerport = new Dictionary<TcpListener, int>();
        //public List<tcpclientIPInfo> ltlif = new List<tcpclientIPInfo>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //this.comboBox2.SelectedIndex = 1;

                //EncodingSet();
                List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad("D:\\华南水电\\DTUServer\\TcpSever_MYSQL+\\TcpSever\\bin\\debug\\端口配置文件\\setting.txt");
                TcpserverMutilStart(ltlif);
                TimerTick();



            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("服务器监听失败!");
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            TcpServerStopTotal();
        }
        #region 启动服务器监听
        public void TcpserverMutilStart(List<tcpclientIPInfo> ltlif)
        {
            try
            {

                //FlushEvent += ClientConnectFlush;
                foreach (tcpclientIPInfo tif in ltlif)
                {

                    TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(tif.ip), Convert.ToInt32(tif.port)));
                    tcpListenstart(tcpListener, tif.port);
                }

            }
            catch (Exception ex)
            {

            }


        }
        public string tcpListenstart(TcpListener tcpListener, int port)
        {
            string mss = "";
            try
            {
                conEvent += clientConnect;
                tcpListener.Start();
                this.listBox2.Items.Add(string.Format("端口{0}监听成功!{1}", port, DateTime.Now));
                tcpListenerport[tcpListener] = port;
                IAsyncResult ar;
                tcpListener.BeginAcceptSocket(new AsyncCallback(conEvent), tcpListener);
                conEvent -= clientConnect;
                //tcpListenstart(tcpListener, port);

            }
            catch (Exception ex)
            {
                mss = ex.Message;
            }
            return mss;
        }
        public delegate void AcceptEvent(AsyncCallback ar, TcpListener tcpListener);
        public event AcceptEvent evt;
        public static int contconnect =0;
        private void clientConnect(IAsyncResult ar)
        {

            try
            {
                
                TcpListener listener = (TcpListener)ar.AsyncState;

                //接受客户的连接,得到连接的Socket
                //if (contconnect == 0)
                //{
                //    contconnect++; conEvent += clientConnect;
                //    listener.BeginAcceptSocket(new AsyncCallback(conEvent), listener);
                //    conEvent -= clientConnect; return;
                //}
                Socket s = listener.EndAcceptSocket(ar);
                //receiveData(client);
                //if(tcp)
                tcpListenerdts[listener] = s;

                FlushEvent += ClientConnectFlush;
                string mssg = string.Format("远程客户端{0}上线 {1}  {2}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, DateTime.Now, tcpListenerport[listener]);
                ExceptionLog.ExceptionWrite(mssg);
                ar = this.listBox2.BeginInvoke(FlushEvent, mssg);
                this.listBox2.EndInvoke(ar);
                FlushEvent -= ClientConnectFlush;

                //FlushEvent -= ClientConnectFlush;
                conEvent += clientConnect;
                listener.BeginAcceptSocket(new AsyncCallback(conEvent), listener);
                conEvent -= clientConnect;
                DataRevThread dataRevThreadModel = new DataRevThread(s, tcpListenerport[listener], listener);
                Thread t = new Thread(new ParameterizedThreadStart(datarev));
                //t.Name = string.Format("{0}resultdataimport", model.xmname);
                t.Start(dataRevThreadModel);


            }

            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
            }

        }
        public class DataRevThread
        {
            public Socket s { get; set; }
            public int port { get; set; }
            public TcpListener listener { get; set; }
            public DataRevThread(Socket s, int port, TcpListener listener)
            {
                this.s = s;
                this.port = port;
                this.listener = listener;
            }
        }
        public void datarev(object obj)
        {
            DataRevThread datarev = obj as DataRevThread;
            double cont = DTUDataGet(datarev.port, datarev.s,0);
            ReceiveMess(datarev.s, datarev.port, cont, 0,0);
            //ReStartListen(datarev.listener);
        }

        #endregion
        #region 心跳包
        public void TimerTick()
        {
            timer.Elapsed += new System.Timers.ElapsedEventHandler(HearBeat);

            timer.Enabled = true;

            timer.AutoReset = true;
        }
        private static object UsingPrinterLocker = new object();
        private static object UsingHistoryContLocker = new object();
        List<TcpListener> dtsDelete = new List<TcpListener>();
        public void HearBeat(object source, System.Timers.ElapsedEventArgs e)
        {
            //int i = 0;



            if (tcpListenerdts.Keys.Count == 0) return;
            System.Threading.Monitor.Enter(UsingPrinterLocker);

            try
            {
                foreach (TcpListener listener in tcpListenerdts.Keys)
                {
                    //测试连接在该端口上的客户端的连接状态
                    if (tcpListenerdts[listener] == null) continue;
                    //TcpClient tc = null;
                    Socket s = tcpListenerdts[listener] as Socket;


                    if (s == null) return;
                    if (!IsSocketAlive(s))
                    {
                        //if (s == null) return;

                        FlushEvent += ClientConnectFlush;
                        string mssg = string.Format("远程客户端{0}已经断开连接 {1}  {2}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, DateTime.Now, tcpListenerport[listener]);
                        ExceptionLog.ExceptionWrite(mssg);

                        IAsyncResult ar = this.listBox2.BeginInvoke(FlushEvent, mssg);
                        this.listBox2.EndInvoke(ar);
                        FlushEvent -= ClientConnectFlush;
                        //conEvent += clientConnect;

                        conEvent += clientConnect;
                        listener.BeginAcceptSocket(new AsyncCallback(conEvent), listener);
                        conEvent -= clientConnect;
                        //tcpListenerdts[listener] = null;
                        dtsDelete.Add(listener);
                        //tcpListenerdts.Remove(listener);
                    }



                }
                if (dtsDelete.Count > 0)
                {
                    for (int i = 0; i < dtsDelete.Count; i++)
                    {
                        Socket stmp = tcpListenerdts[dtsDelete[i]];
                        stmp.Close();
                        stmp.Dispose();
                        tcpListenerdts[dtsDelete[i]] = null;


                    }
                    dtsDelete = new List<TcpListener>();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                System.Threading.Monitor.Exit(UsingPrinterLocker);
            }


        }

        public bool IsSocketAlive(Socket s)
        {
            //byte[] buff = new byte[100];

            if (s == null) return false;
            try
            {

                s.Send(Encoding.UTF8.GetBytes("1"));
                return true;

            }
            catch (Exception ex)
            {

                return false;
            }

        }

        #endregion
        #region 窗体控件信息显示
        public void ClientConnectFlush(string mssg)
        {
            DateTime dt = DateTime.Now;

            this.listBox2.Items.Add(mssg);


        }
        public void ClientDataFlush(string mssg)
        {

            DateTime dt = DateTime.Now;

            this.listBox1.Items.Add(mssg);


        }


        #endregion
        #region 数据通信
        //private static AutoResetEvent UsingSockectRevData = new AutoResetEvent(false);
        //private static AutoResetEvent UsingSockectDTUID = new AutoResetEvent(false);
        //客户端上线从服务器等待客户端的数据回发
        //private static object SetingContLocker = new object();
        public void ReceiveMess(Socket s, int port, double cont, int revcont,int times)
        {

            ExceptionLog.ExceptionWrite("开始接收数据");
            //对方发送文件
            DateTime dt = DateTime.Now;

            try
            {

                StringBuilder datastr = new StringBuilder(256);
                int count;
                byte[] b = new byte[4096];
                //设置20秒等待时间
                //bool state = s.Blocking;
                s.ReceiveTimeout = 1200000;

                try
                {
                    if (cont > 0)
                        s.Send(Encoding.UTF8.GetBytes(string.Format("get data:{0}",Convert.ToInt32(cont))));
                    while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                    {
                        var str = Encoding.UTF8.GetString(b, 0, count);
                        datastr.Append(str);

                        if (cont == 0)
                        {
                            cont = DTUDataGet(port,s,times+1);
                            if (cont > 0)
                            {
                                //ReceiveMess(s, port, cont, revcont, times + 1);
                                s.Send(Encoding.UTF8.GetBytes(string.Format("get data:{0}", Convert.ToInt32(cont))));
                                times++;
                            }
                        }


                        if (IsThisSendFinish(s, datastr.ToString(), port, cont))
                        {
                            datastr = new StringBuilder(256);
                            cont = 0;
                            //UsingSockectRevData.Set();
                        }
                        if (datastr.ToString().IndexOf("the end") != -1)
                        {
                            datastr = new StringBuilder();
                            s.Send(Encoding.UTF8.GetBytes(string.Format("get data:{0}", Convert.ToInt32(cont)+1)));
                        }
                        b = new byte[4096];
                        s.ReceiveTimeout = 1200000;
                    }
                    //DataRec(s,port);
                    //是否需要给设备发送命令，如果没有关闭连接
                    //s.Shutdown(SocketShutdown.Both);
                }
                catch (Exception ex)
                {
                    if (revcont == 3)
                    {
                        //关闭连接
                        FlushEvent += ClientDataFlush;
                        IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("IP:{0} {1}  接收出错接收次数已经超过三次,准备退出连接...", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port));
                        this.listBox1.EndInvoke(ar);
                        FlushEvent -= ClientDataFlush;
                        s.Shutdown(SocketShutdown.Both);
                        SocketClose(s);
                        return;
                    }
                    else
                    {
                        FlushEvent += ClientDataFlush;
                        IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("IP:{0} {1}  接收出错重新接收 次数{2}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port, revcont));
                        //DTUDATAImport(datastr);

                        this.listBox1.EndInvoke(ar);
                        FlushEvent -= ClientDataFlush;
                    }
                    revcont++;
                    ReceiveMess(s, port, cont, revcont,0);
                    ////向客户端发送数据获取请求
                    //DTUDATARequestSend(s, port);
                    //conEvent += clientConnect;
                    //listener.BeginAcceptSocket(new AsyncCallback(conEvent), listener);
                    //conEvent -= clientConnect;
                    return;
                }
                ExceptionLog.ExceptionWrite("数据接收完成");
                if (this.listBox1.InvokeRequired)
                {

                    if (messvalidate())
                    {
                        s.Send(Encoding.UTF8.GetBytes("success"));
                        FlushEvent += ClientDataFlush;
                        IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("接收到{0}  从{2}发来的数据{1}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, datastr, port));

                        DTUDATAImport(datastr.ToString(), port, s);
                        this.listBox1.EndInvoke(ar);
                        FlushEvent -= ClientDataFlush;

                    }
                    else
                    {
                        FlushEvent += ClientDataFlush;
                        IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("接收到{0}  从{1}回传数据出错!", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port));
                        this.listBox1.EndInvoke(ar);
                        FlushEvent -= ClientDataFlush;
                    }

                }
                else
                {
                    this.listBox1.Items.Add("\n" + dt + "数据校验失败");
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
            }


        }

        public bool IsThisSendFinish(Socket s, string str, int port, double cont)
        {
            if (str.IndexOf("the end") != -1)
            {
                if (cont > 0 && str.Split('\r').Length < cont + 2) return false;

                DTUDATAImport(str, port, s);
                FlushEvent += ClientDataFlush;
                IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("{2} 接收{0}  从{1}发来的数据【{3}】  ", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port, DateTime.Now, str));
                //DTUDATAImport(datastr);
                this.listBox1.EndInvoke(ar);
                FlushEvent -= ClientDataFlush;
                return true;
            }
            return false;
        }

        public bool GetDTUIDFromFirstData(Socket s, string str, int port, out string point, out int xmno)
        {

            point = ""; xmno = 0;

            string addressno = str.Split(':')[0];
            string[] attrs = str.Split('\r');
            int i = 0;
            //string pointname = "";
            double line = 0;
            double holedepth = 0;
            xmno = Aspect.IndirectValue.GetDTUXmnoFromPort(port);
            //for (i = 1; i < attrs.Length - 1; i++)
            //{

                str = attrs[0];
                if (str.IndexOf("2017/") == -1) return false;
                str = str.Substring(str.IndexOf("\r\n") + 2);
                string datastr = str.Substring(str.IndexOf("2017/"));
                string[] dataary = datastr.Split(' ');
                Aspect.IndirectValue.DTUModulePointName(xmno, port, addressno, out point);
                DateTime dtutime = Convert.ToDateTime(dataary[0] + " " + dataary[1]);
                if ((DateTime.Now - dtutime).TotalDays > 1)
                {

                    FlushEvent += ClientDataFlush;
                    IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("DTU当前时间是{1}系统时间是{2}现在向{0}发送时间校准指令", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, dtutime, DateTime.Now));
                    //DTUDATAImport(datastr);
                    this.listBox1.EndInvoke(ar);
                    FlushEvent -= ClientDataFlush;
                    s.Send(Encoding.UTF8.GetBytes(string.Format("set rtc:{0},{1},{2},{3},{4},{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second)));
                    byte[] buf = new byte[4096];
                    if (s.Receive(buf, SocketFlags.None) > 0)
                    {
                        string strrev = Encoding.UTF8.GetString(buf);
                        if (strrev.IndexOf("success") != -1)
                        {
                            FlushEvent += ClientDataFlush;
                            ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("校准{0}时间成功", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address));
                            //DTUDATAImport(datastr);
                            this.listBox1.EndInvoke(ar);
                            FlushEvent -= ClientDataFlush;
                        }
                        else
                        {
                            FlushEvent += ClientDataFlush;
                            ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("校准{0}时间失败", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address));
                            //DTUDATAImport(datastr);
                            this.listBox1.EndInvoke(ar);
                            FlushEvent -= ClientDataFlush;
                        }
                    }
                }
                return true;
            
            
        }
        public bool DataRec(Socket s, int port)
        {
            //int count = 0;
            //byte[] b = new byte[1024];

            //StringBuilder datastr = new StringBuilder(256);

            //while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
            //{
            //    var str = Encoding.UTF8.GetString(b, 0, count);
            //    datastr.Append(str);
            //    if (IsThisSendFinish(s, datastr.ToString(), port))
            //    {
            //        datastr = new StringBuilder(256);
            //    }
            //    b = new byte[4096];

            //}
            return false;

        }
        //服务器等待客户端的数据超时进行主动获取
        public void DTUDATARequestSend(Socket s, int port)
        {
            int i = 0;
            s.SendTimeout = 120000;
            IAsyncResult ar = null;
            //连续请求三次
            for (i = 0; i < 3; i++)
            {
                try
                {
                    //设置10秒等待时间

                    s.Send(Encoding.UTF8.GetBytes("set jiange:00,01,01"));

                    FlushEvent += ClientDataFlush;
                    ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("向客户端{0}  由{1}发送数据获取请求成功,现在等待客户端数据回传...", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port));

                    this.listBox1.EndInvoke(ar);
                    FlushEvent -= ClientDataFlush;


                    int count;
                    StringBuilder datastr = new StringBuilder(256);
                    byte[] b = new byte[4096];
                    //设置20秒等待时间
                    //bool state = s.Blocking;
                    s.ReceiveTimeout = 1200000;


                    //DataRec(s,port);
                    //int count = 0;

                    while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                    {
                        var str = Encoding.UTF8.GetString(b, 0, count);
                        datastr.Append(str);
                        if (IsThisSendFinish(s, datastr.ToString(), port, 0))
                        {
                            datastr = new StringBuilder(256);
                        }
                        b = new byte[4096];

                    }

                    //ReceiveMess(s,port);

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(string.Format("向客户端{0}发送数据请求命令失败,失败原因{1} 请求次数{2}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, ex.Message, i));
                    continue;
                }

                SocketClose(s);

                FlushEvent += ClientDataFlush;
                ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("向{0}  从{2}请求数据获取超时!设备采集时间间隔超过了规定时间或者设备已经损坏", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address));
                this.listBox1.EndInvoke(ar);
                FlushEvent -= ClientDataFlush;
                //s.ReceiveTimeout = 10000;
                //byte[] buff = new byte[100];
                //int count;
                //byte[] b = new byte[4098];
                ////设置20秒等待时间
                //s.ReceiveTimeout = 10000;
                //string datastr = "";


                //try
                //{
                //    while ((count = s.Receive(b, 4098, SocketFlags.None)) != 0)
                //    {
                //        //binaryWrite.Write(b, 0, count);
                //        var str = Encoding.UTF8.GetString(b, 0, count);
                //        //sw.Write(str);
                //        //sw.Flush();
                //        datastr += str;
                //        b = new byte[4098];
                //    }
                //    ExceptionLog.ExceptionWrite("数据接收完成");
                //    //s.Shutdown(SocketShutdown.Both);
                //    if (this.listBox1.InvokeRequired)
                //    {

                //        if (messvalidate())
                //        {
                //            s.Send(Encoding.UTF8.GetBytes("success"));
                //            FlushEvent += ClientDataFlush;
                //            IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("接收到{0}  从{2}发来的数据{1}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, datastr, port));
                //            DTUDATAImport(datastr, port, s);
                //            this.listBox1.EndInvoke(ar);
                //            FlushEvent -= ClientDataFlush;
                //        }
                //        else
                //        {
                //            //Encoding.Default.

                //            s.Send(Encoding.UTF8.GetBytes("failed"));
                //            FlushEvent += ClientDataFlush;
                //            IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("接收到{0}  从{1}回传数据出错!", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port));
                //            this.listBox1.EndInvoke(ar);
                //            FlushEvent -= ClientDataFlush;
                //        }

                //    }
                //    else
                //    {
                //        this.listBox1.Items.Add("\n" + "数据校验失败");
                //    }
                //}

                //catch (Exception ex)
                //{
                //    //s.Shutdown(SocketShutdown.Receive);
                //    //s.Shutdown(SocketShutdown.Both);
                //    SocketClose(s);

                //    FlushEvent += ClientDataFlush;
                //    IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("向{0}  从{2}请求数据获取超时!设备采集时间间隔超过了规定时间或者设备已经损坏", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, datastr, port));
                //    this.listBox1.EndInvoke(ar);
                //    FlushEvent -= ClientDataFlush;
                //    //设置时间间隔
                //    //CommendListTaskDelivery(xmno, pointname, s);

                //}
            }
        }

        #endregion
        #region 数据处理
        /// <summary>
        /// 数据导入
        /// </summary>
        /// <param name="str"></param>
        public void DTUDATAImport(string datastr, int port, Socket s)
        {

            DTUDATADescode(port, datastr, s);

        }
        /// <summary>
        /// 数据校验
        /// </summary>
        /// <returns></returns>
        public bool messvalidate()
        {
            return true;
        }
        public string point_name = "";
        public int xmno = 0;
        public static ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();
        public static ProcessDTUTaskQueueFinishedSet processDTUTaskQueueFinishedSetBLL = new ProcessDTUTaskQueueFinishedSet();
        public static ProcessDTUDataAlarmBLL processDTUDataAlarmBLL = new ProcessDTUDataAlarmBLL();
        public static string mssg = "";
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        //DTU数据解析入库
        public void DTUDATADescode(int port, string str, Socket s)
        {
            string addressno = str.Split(':')[0];
            string[] attrs = str.Split('\r');
            int i = 0;
            string pointname = "";
            double line = 0;
            double holedepth = 0;
            int xmno = Aspect.IndirectValue.GetDTUXmnoFromPort(port);
            processDTUDATABLL.ProcessDATATmpDel(xmno, out mssg);
            for (i = 1; i < attrs.Length - 1; i++)
            {

                str = attrs[i];
                if (str.IndexOf("2017/") == -1) continue;
                str = str.Substring(str.IndexOf("\r\n") + 2);
                string datastr = str.Substring(str.IndexOf("2017/"));
                string[] dataary = datastr.Split(' ');



                Aspect.IndirectValue.DTUModulePointName(xmno, port, dataary[2], addressno, out pointname, out line, out holedepth);
                DTU.Model.dtudata model = new DTU.Model.dtudata
                {
                    point_name = pointname,
                    mkh = 0,
                    deep = Convert.ToDouble((line - Convert.ToDouble(dataary[3]) / 1000).ToString("0.000")),
                    time = Convert.ToDateTime(dataary[0] + " " + dataary[1]),
                    xmno = xmno,
                    dtutime = Convert.ToDateTime(dataary[0] + " " + dataary[1]),
                    groundElevation = Convert.ToDouble((Convert.ToDouble(dataary[3]) / 1000).ToString("0.000"))
                };

                processDTUDATABLL.ProcessDTUDATAInsertBLL(model, out mssg);

                CommendListTaskDelivery(xmno, pointname, s);
            }

            List<string> alarmInfoList = processDTUDataAlarmBLL.DTUDataAlarm("数据测试", 29);
            emailSendBLL.ProcessEmailSend(alarmInfoList, "数据测试", 29, out mssg);
        }
        #endregion
        #region 关闭/重启
        //重新开启监听
        public void ReStartListen(TcpListener tcpListener)
        {
            //conEvent += clientConnect;
            //IAsyncResult ar;
            
            //tcpListener.BeginAcceptSocket(new AsyncCallback(conEvent), tcpListener);
            //conEvent -= clientConnect;
            string a = "";
        }
        //停止服务
        public void TcpServerStopTotal()
        {
            try
            {
                foreach (TcpListener tcpListener in tcpListenerdts.Keys)
                {
                    if (tcpListenerdts[tcpListener] != null)
                    {
                        //关闭连接
                        Socket s = tcpListenerdts[tcpListener];
                        s.Shutdown(SocketShutdown.Both);
                        s.Dispose();
                        s = null;
                    }

                    tcpListener.Server.Close();
                    tcpListener.Stop();



                }
                tcpListenerdts.Clear();
            }
            catch (Exception ex)
            {


            }
        }
        public void TcpServerStop(TcpListener tcpListener)
        {
            try
            {

                //关闭连接
                Socket s = tcpListenerdts[tcpListener];
                s.Shutdown(SocketShutdown.Both);
                s.Dispose();
                s = null;
                tcpListener.Server.Close();
                tcpListener.Stop();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("关闭服务器端口{0}监听出错,错误信息：" + ex.Message);

            }
        }
        private object useSocketLocker = new object();
        public void SocketClose(Socket s)
        {
            System.Threading.Monitor.Enter(useSocketLocker);
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("关闭远程客户端出错!出错原因:" + ex.Message);
            }
            finally
            {
                System.Threading.Monitor.Exit(useSocketLocker);
            }
        }
        /// <summary>
        /// 重启单个服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        public void TcpListenerRestart(TcpListener listenser)
        {
            List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad("端口配置文件\\setting.txt");
            int port = tcpListenerport[listenser];
            var ip = (from m in ltlif where m.port == port select m.ip).ToList()[0];
            TcpServerStop(listenser);
            try
            {
                TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(port)));
                tcpListenstart(tcpListener, port);
                ExceptionLog.ExceptionWrite(string.Format("重启服务器端口{0}成功!", port));
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("重启服务器端口{0}出错，错误信息:" + ex.Message, port));
            }

        }
        /// <summary>
        /// 重启所有服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        public void TcpListenerRestartTotal(TcpListener listenser)
        {

        }
        #endregion
        #region 设置命令
        public DTUCommend CommendCreate(int commendIndex, int xmno, string point_name)
        {
            DTUCommend dtucommend = null;
            switch (commendIndex)
            {
                //设置时间间隔
                case 0:

                    string commendstr = GetTimeInterval(xmno, point_name);
                    dtucommend = new DTUCommend("设置时间间隔", "success", "failed", 60000, 60000, commendstr, 0);
                    return dtucommend;
                //break;
            }
            return new DTUCommend();
        }
        public enum CommendList
        {
            TIME_INTERVAL, TIME_TASK
        };
        public string GetTaskTime(int xmno, string point_name)
        {
            return "";
        }

        public string GetTimeInterval(int xmno, string point_name)
        {
            ProcessDTUTimeIntervalLoad dtuTimeIntervalLoad = new ProcessDTUTimeIntervalLoad();
            var model = dtuTimeIntervalLoad.DTUTimeIntervalLoad(xmno, point_name, out mssg);
            if (model == null)
                return "";
            return string.Format("set jiange:{0},{1},{2}", model.hour.ToString("00"), model.minute.ToString("00"), model.times.ToString("00"));
        }

        public class DTUCommend
        {
            public string commendName { get; set; }
            public string delivery { get; set; }
            public string successWords { get; set; }
            public string failedWords { get; set; }
            public int sendTimeOut { get; set; }
            public int recTimeOut { get; set; }
            public int commendIndex { get; set; }
            //public int 
            public DTUCommend(string commendName, string successWords, string failedWords, int sendTimeOut, int recTimeOut, string delivery, int commendIndex)
            {
                this.commendName = commendName;
                this.successWords = successWords;
                this.failedWords = failedWords;
                this.sendTimeOut = sendTimeOut;
                this.recTimeOut = recTimeOut;
                this.delivery = delivery;
                this.commendIndex = commendIndex;
            }
            public DTUCommend()
            {

            }
        }


        public void CommendListTaskDelivery(int xmno, string pointname, Socket s)
        {
            List<DTUCommend> lscommend = new List<DTUCommend>();
            List<int> ls = GetCommendFromTaskQueue(xmno, pointname);
            if (ls.Count == 0) return;
            foreach (int commendIndx in ls)
            {
                lscommend.Add(CommendCreate(commendIndx, xmno, pointname));
            }
            List<string> resultList = new List<string>();
            foreach (DTUCommend commend in lscommend)
            {
                if (commendDelivery(xmno, pointname, s, commend))
                {
                    resultList.Add(string.Format("向项目{0}点名{1}发送{2}命令成功  ", xmno, pointname, commend.commendName, DateTime.Now));
                }
                else
                {
                    resultList.Add(string.Format("向项目{0}点名{1}发送{2}命令失败  ", xmno, pointname, commend.commendName, DateTime.Now));
                }

            }
            FlushEvent += ClientDataFlush;
            IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Join("\n", resultList));
            //DTUDATAImport(datastr);
            this.listBox1.EndInvoke(ar);
            FlushEvent -= ClientDataFlush;

        }
        public bool commendDelivery(int xmno, string point_name, Socket s, DTUCommend commend)
        {
            int i = 0;
            for (i = 0; i < 3; i++)
            {
                try
                {
                    byte[] buff = new byte[1024];
                    s.SendTimeout = commend.sendTimeOut;
                    s.Send(Encoding.UTF8.GetBytes(commend.delivery));
                    s.ReceiveTimeout = 2 * commend.recTimeOut;
                    int cont = s.Receive(buff, SocketFlags.None);
                    if (cont < 1024)
                    {
                        string recresult = Encoding.UTF8.GetString(buff);
                        if (commend.successWords.IndexOf("success") != -1)
                        {
                            processDTUTaskQueueFinishedSetBLL.DTUTaskQueueFinishedSet(xmno, point_name, commend.commendIndex, out mssg);
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        //发送返回的字节数太多
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(string.Format("向项目编号{0}点{1}发送命令{2}失败 第{3}次", xmno, point_name, commend.commendName, i));
                    return false;
                }
            }
            return false;
        }


        public ProcessDTUQueueLoad processDTUQueueLoad = new ProcessDTUQueueLoad();
        public List<int> GetCommendFromTaskQueue(int xmno, string pointname)
        {
            return processDTUQueueLoad.DTUQueueLoad(xmno, pointname, out mssg);
        }
        public ProcessSenorPointMaxTime processSenorPointMaxTime = new ProcessSenorPointMaxTime();
        public ProcessDTUTimeIntervalLoad processDTUTimeIntervalLoad = new ProcessDTUTimeIntervalLoad();
        public double DTUDataGet(int port, Socket s,int times )
        {
            //要一条数据
            if (times > 2) return 0;
            
            StringBuilder datastr = new StringBuilder(256);
            int count;
            byte[] b = new byte[4096];
            int xmno = 0;
            string point_name = "";
            //设置20秒等待时间
            //bool state = s.Blocking;
            s.ReceiveTimeout = 1200000;

            try
            {
                s.Send(Encoding.UTF8.GetBytes("get rtc"));
                while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                {
                    var str = Encoding.UTF8.GetString(b, 0, count);
                    datastr.Append(str);
                    int len = datastr.ToString().Split('\r').Length;
                    if (len == 2   )
                    {

                        if (!GetDTUIDFromFirstData(s, datastr.ToString(), port, out point_name, out xmno))
                        {
                            datastr = new StringBuilder(256);
                            s.Send(Encoding.UTF8.GetBytes("get rtc"));
                            b = new byte[4096];
                            s.ReceiveTimeout = 1200000;
                            continue;
                        }
                    }
                    else 
                    {
                        string datastring = datastr.ToString();
                        if (datastring.IndexOf("the end") != -1)
                        {
                            datastr = new StringBuilder(256);
                            s.Send(Encoding.UTF8.GetBytes("get rtc"));
                        }
                        b = new byte[4096];
                        s.ReceiveTimeout = 1200000;
                        continue;
                    }
                    
                    //UsingSockectRevData.WaitOne();
                    //UsingSockectDTUID

                    System.Threading.Monitor.Enter(UsingHistoryContLocker);
                    DateTime maxTime = processSenorPointMaxTime.SenorMaxTime(xmno, point_name, Role.administratrorModel, false, out mssg);
                    DTU.Model.dtutimetask task = processDTUTimeIntervalLoad.DTUTimeIntervalLoad(xmno, point_name, out mssg);
                    double toBeDivMinute = (task.hour * 60 + task.minute) * 60;
                    double totalsec = (DateTime.Now - maxTime).TotalSeconds;
                    double totalcount =Math.Ceiling(totalsec / toBeDivMinute);
                    System.Threading.Monitor.Exit(UsingHistoryContLocker);
                    FlushEvent += ClientDataFlush;
                    totalcount = totalcount > 1000 ? 1000 : totalcount;
                    IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("{0}有{1}条数据未上传，现在下载...", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, totalcount));
                    this.listBox1.EndInvoke(ar);
                    FlushEvent -= ClientDataFlush;
                    
                    return totalcount;
                   
                    
                }
                //DataRec(s,port);
                //是否需要给设备发送命令，如果没有关闭连接
                //s.Shutdown(SocketShutdown.Both);
            }
            catch (Exception ex)
            {

                //FlushEvent += ClientDataFlush;
                //IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("接收{0}  从{1}发来的数据超时，现在向客户端发送数据获取请求", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port));
                ////DTUDATAImport(datastr);
                //this.listBox1.EndInvoke(ar);
                //FlushEvent -= ClientDataFlush;
                ////向客户端发送数据获取请求
                //DTUDATARequestSend(s, port);
                return 0;
            }
            return 0;



        }

        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            TcpServerStopTotal();
        }

    }
}
