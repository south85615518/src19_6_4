﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tool;

namespace NFnetUnitTest.DTUServer
{
    public partial class Logwin : Form
    {
        public System.Timers.Timer timerresInspection = new System.Timers.Timer(10000);
        public delegate void TextBoxFlush(string filename,TextBox tb);
        public event TextBoxFlush FlushEvent;
        public Logwin()
        {
            InitializeComponent();
            TimerTick();

        }
        public void TimerTick()
        {


            timerresInspection.Elapsed += new System.Timers.ElapsedEventHandler(logflush);
            timerresInspection.Enabled = true;
            timerresInspection.AutoReset = true;
        }
        private void Logwin_Load(object sender, EventArgs e)
        {
            string commendlog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU命令日志\\log.txt");
            this.textBox1.Text = commendlog;
            string portlistenlog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU端口巡检日志\\log.txt");
            this.textBox2.Text = portlistenlog;
            string datareclog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU日志\\log.txt");
            this.textBox3.Text = datareclog;
        }
        public void logflush(object source, System.Timers.ElapsedEventArgs e)
        {
            FlushEvent += DataFlush;

            string commendlog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU命令日志\\log.txt");
            IAsyncResult ar = this.textBox1.BeginInvoke(FlushEvent,commendlog,this.textBox1);
            string portlistenlog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU端口巡检日志\\log.txt");
            ar = this.textBox2.BeginInvoke(FlushEvent, portlistenlog, this.textBox2);

            string datareclog = FileHelper.ProcessTxtLog(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU日志\\log.txt");
            ar = this.textBox3.BeginInvoke(FlushEvent, datareclog, this.textBox3);
            FlushEvent -= DataFlush;
        }
        public void DataFlush(string mssg,TextBox tb)
        {

            //DateTime dt = DateTime.Now;
            string id = tb.Name;
            tb.Text = mssg;


        }
    }
}
