﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NFnet_BLL.DisplayDataProcess;

namespace NFnetUnitTest.DTUServer
{
    public partial class Form2 : Form
    {
        public ProcessDTUTcpServer tcpServer = new ProcessDTUTcpServer();
        public Form2()
        {
            InitializeComponent();
            tcpServer.DTUTcpServerStart(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\dtusetting.txt");
            //this.textBox1.Text = "11221\r\n21212\r\n2132121\r\n";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tcpServer.TcpServerStopTotal();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Logwin logwin = new Logwin();
            logwin.ShowDialog();
        }
    }
}
