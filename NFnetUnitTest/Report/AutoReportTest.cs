﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Report;
using NFnet_Interface.Report;

namespace NFnetUnitTest.Report
{
    public class AutoReportTest
    {
        public ProcessMonthReportBLL processMonthReportBLL = new ProcessMonthReportBLL();
        public ProcessMonthReport processMonthReport = new ProcessMonthReport();
        public ProcessXmMonthReport processXmMonthReport = new ProcessXmMonthReport();
        public ProcessXmWeekReport processXmWeekReport = new ProcessXmWeekReport();
        public ProcessWeekReportBLL processWeekReportBLL = new ProcessWeekReportBLL();
        public ProcessWeekReport processWeekReport = new ProcessWeekReport();

        public string mssg = "";
        public AutoReport.Model.automonthreport monthreportmodel = new AutoReport.Model.automonthreport { 
             xmno = Aspect.IndirectValue.GetXmnoFromXmname("从化边坡监测"),
             day =  6,
             hour = 0,
             jclx = "库水位,GNSS"
        };
        public AutoReport.Model.autoweekreport weekreportmodel = new AutoReport.Model.autoweekreport
        {
            xmno = Aspect.IndirectValue.GetXmnoFromXmname("从化边坡监测"),
            week = 6,
            hour = 0,
            jclx = "库水位,GNSS"
        };
        public void main()
        {
            //ProcessMonthReportAdd();
            //ProcessMonthReportModelGet();
            //ProcessWeekReportAdd();
            //ProcessWeekReportModelGet();
            xmweekreport();
            xmmonthreport();
        }
        public void ProcessMonthReportAdd()
        {
            processMonthReportBLL.Add(monthreportmodel,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessMonthReportModelGet()
        {
            processMonthReport.MonthReport(Aspect.IndirectValue.GetXmnoFromXmname("从化边坡监测"),6,0, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void ProcessWeekReportAdd()
        {
            processWeekReportBLL.Add(weekreportmodel, out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void ProcessWeekReportModelGet()
        {
            processWeekReport.WeekReport(Aspect.IndirectValue.GetXmnoFromXmname("从化边坡监测"),6, 0, out mssg);
            ProcessPrintMssg.Print(mssg);
        }

        public void xmweekreport()
        {
            AutoReport.Model.autoweekreport model = processXmWeekReport.XmWeekReport(Aspect.IndirectValue.GetXmnoFromXmname("从化边坡监测"), out mssg);
            ProcessPrintMssg.Print(mssg);
            //if (model != null)
            //{

            //    processAutoReport.WeekReport(model.jclx);
            //}
        }
        public void xmmonthreport()
        {
            AutoReport.Model.automonthreport model = processXmMonthReport.XmMonthReport(Aspect.IndirectValue.GetXmnoFromXmname("从化边坡监测"), out mssg);
            ProcessPrintMssg.Print(mssg);
            //if (model != null)
            //{
            //    processAutoReport.MonthReport(model.jclx);
            //}
        }

    }
}
