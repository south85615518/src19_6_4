﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Inclinometer;

namespace NFnetUnitTest.PointValueAttributeTest
{
    public class TestInclinometerPointValueAlarm
    {
        public static ProcessPointAlarmValueLoad pointAlarmBLL = new ProcessPointAlarmValueLoad();
        public static ProcessPointAlarmRecordsCount pointAlarmRecordsCountBLL = new ProcessPointAlarmRecordsCount();
        public static ProcessAlarmRecordsCount alarmRecordsCountBLL = new ProcessAlarmRecordsCount();
        public static ProcessPointAlarmValueEdit pointAlarmValueEdit = new ProcessPointAlarmValueEdit();
        public static ProcessPointAlarmValueMutilEdit pointAlarmValueMultilEdit = new ProcessPointAlarmValueMutilEdit();
        public static ProcessPointAlarmValueAdd pointAlarmValueAdd = new ProcessPointAlarmValueAdd();
        public static ProcessPointAlarmValueDel pointAlarmValueDel = new ProcessPointAlarmValueDel();
        public static ProcessPointAlarmExist pointAlarmExist = new ProcessPointAlarmExist();
        public static string mssg = "";
        public static PointAttribute.Model.fmos_pointalarmvalue model = new PointAttribute.Model.fmos_pointalarmvalue
            {
                POINT_NAME = "CX1-1",
                pointtype = "测斜仪",
                xmno = 29,
                FirstAlarmName = "1",
                SecondAlarmName = "4",
                ThirdAlarmName = "3",
                remark = "深部位移测斜点",
                ID = 103


            };

        public static NFnet_DAL.MODEL.pointalarmvalue pointModel = new NFnet_DAL.MODEL.pointalarmvalue
        {
             PointName = "CX1-2",
             Pointtype = "测斜仪",
             Xmno = 29,
             FirstAlarmName = "1",
             SecondAlarmName = "2",
             ThirdAlarmName = "3",
             Remark = "深部位移测斜点"
        };

        public void main()
        {
            //TestPointValueAlarmExist();
            //PointValueAlarmAdd();
            //PointValueAlarmEdit();
            //PointValueAlarmLoad();
            //PointValueAlarmRecordsCount();
            PointLoad();
            ProcessPrintMssg.Print(mssg);
        }
        public void TestPointValueAlarmExist()
        {
            pointAlarmExist.PointAlarmExist(model,out mssg);
        }
        public void PointValueAlarmAdd()
        {

            pointAlarmValueAdd.PointAlarmValueAdd(model,out mssg);
            
        }
        public void PointValueAlarmEdit()
        {
            pointAlarmValueEdit.PointAlarmValueEdit(model, out mssg);
        }

        public void PointValueAlarmLoad()
        {
            pointAlarmBLL.PointAlarmValueLoad("数据测试",Aspect.IndirectValue.GetXmnoFromXmname("数据测试"),"1=1","point_name",1,20,"asc",out mssg);
        }
        public void PointValueAlarmRecordsCount()
        {
            pointAlarmRecordsCountBLL.PointAlarmRecordsCount("数据测试",29,"1=1" ,out mssg);
        }
        public ProcessInclinometerPointLoad inclinometerBLL = new ProcessInclinometerPointLoad();
        public void PointLoad()
        {
            List<string> ls = inclinometerBLL.InclinometerPointLoad(Aspect.IndirectValue.GetXmnoFromXmname("数据测试"), out mssg);
        }
    }
}
