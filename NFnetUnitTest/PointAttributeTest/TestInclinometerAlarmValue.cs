﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.Inclinometer;

namespace NFnetUnitTest.PointValueAttributeTest
{
    public class TestInclinometerValueAlarm
    {
        public static ProcessAlarmLoad pointAlarmBLL = new ProcessAlarmLoad();

        public static ProcessAlarmValues alarmValues = new ProcessAlarmValues();
        public static ProcessAlarmRecordsCount alarmRecordsCountBLL = new ProcessAlarmRecordsCount();
        public static ProcessAlarmEdit AlarmEdit = new ProcessAlarmEdit();
        public static ProcessAlarmAdd AlarmAdd = new ProcessAlarmAdd();
        public static ProcessAlarmDel AlarmDel = new ProcessAlarmDel();
        public static string mssg = "";

        InclimeterDAL.Model.inclinometer_alarmvalue model = new InclimeterDAL.Model.inclinometer_alarmvalue
        {
            name = "一级预警",
            xmname = "数据测试",
            this_disp = 0.01,
            ac_disp = 0.12,
            this_rap = 0.2

        };


        public void main()
        {
            //PointValueAlarmAdd();
            //PointValueAlarmEdit();
            //PointValueAlarmDel();
            //PointValueAlarmLoad();
            //PointValueAlarmRecordsCount();
            AlarmValues();
            ProcessPrintMssg.Print(mssg);
        }
        public void PointValueAlarmAdd()
        {

            AlarmAdd.AlarmAdd(model, out mssg);

        }
        public void PointValueAlarmEdit()
        {
            AlarmEdit.AlarmEdit(model, out mssg);
        }
        public void PointValueAlarmDel()
        {
            AlarmDel.AlarmDel(model.xmname, model.name, out mssg);
        }
        public void PointValueAlarmLoad()
        {
            pointAlarmBLL.AlarmLoad("数据测试", "name", 1, 20, "asc", out mssg);
        }
        public void AlarmValues()
        {
            string alarmNames = alarmValues.AlarmValues("数据测试","",out mssg);
            ProcessPrintMssg.Print(alarmNames);
        }
        public void PointValueAlarmRecordsCount()
        {
            alarmRecordsCountBLL.AlarmRecordsCount("数据测试", out mssg);
        }
    }
}
