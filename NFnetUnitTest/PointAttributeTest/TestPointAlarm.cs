﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnetUnitTest.PointAttributeTest
{
    public class TestPointAlarmBLL
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessSettlementdisppointAdd processSettlementdisppointAdd = new ProcessSettlementdisppointAdd();
        public ProcessSettlementdisppointDelete processSettlementdisppointDelete = new ProcessSettlementdisppointDelete();
        public ProcessSettlementdisppointTableLoad processSettlementdisppointTableLoad = new ProcessSettlementdisppointTableLoad();
        public ProcessSettlementdisppointtablecount processSettlementdisppointtablecount = new ProcessSettlementdisppointtablecount();
        public static string mssg = "";
        public PointAttribute.Model.fmos_pointalarmvalue model = new PointAttribute.Model.fmos_pointalarmvalue
        {
             xmno = 30,
             pointtype = "全站仪",
             POINT_NAME = "Sp11"

        };
        public PointAttribute.Model.fmos_settlementdisprelationship settlementdispmodel = new PointAttribute.Model.fmos_settlementdisprelationship { xmno = 106, point_name = "QD206-4", referencepoint_name="QD205-4" };



        public void main()
        {
            //TestProcessPointAlarm();
            //TestSettlementdisppointAdd();
            //TestSettlementdisppointDelete();
            //TestSettlementdisppointTableLoad();
            //TestSettlementdisppointTableCount();
            ProcessPrintMssg.Print(mssg);
        }
        public void TestProcessPointAlarm()
        {
            pointAlarmBLL.ProcessAlarmExist(model,out mssg);
            ProcessPrintMssg.Print(mssg);
        }
        public void TestSettlementdisppointAdd()
        {
            processSettlementdisppointAdd.SettlementdisppointAdd(settlementdispmodel,out mssg);
        }
        public void TestSettlementdisppointDelete()
        {
            processSettlementdisppointDelete.SettlementdisppointDelete(106, "QD206-4", out mssg);
        }
        public void TestSettlementdisppointTableLoad()
        {
            processSettlementdisppointTableLoad.SettlementdisppointTableLoad(106,"fmos_pointalarmvalue.point_name",1,50," asc ",out mssg);
        }
        public void TestSettlementdisppointTableCount()
        {
            processSettlementdisppointtablecount.Settlementdisppointtablecount("  1=1  ",106,out mssg);
        }
    }
}
