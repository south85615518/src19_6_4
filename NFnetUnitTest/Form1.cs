﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NFnet_BLL;

namespace NFnetUnitTest
{
    public partial class Form1 : Form
    {
         public ProcessSenorDataCreateBLL senorDataCreateBLL = new ProcessSenorDataCreateBLL();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pointstr = this.textBox1.Text;
            string[] points = pointstr.Split(',');
            List<double> doubleList = new List<double>();
            string [] dispstr = this.textBox3.Text.Split(',');
            double[] deeps = {0,-0.5,-1,-1.5,-2,-2.5 };

            senorDataCreateBLL.SenorDataCreate(Aspect.IndirectValue.GetXmnoFromXmname(this.comboBox1.Text), pointstr, this.comboBox2.Text, Convert.ToDouble(this.textBox4.Text), dispstr, deeps, DateTime.Now, DateTime.Now.AddDays(1), Convert.ToInt32(this.textBox2.Text));
                
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
