﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_MODAL
{
    public class result
    {
        private string pointName, cyc, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz, taskName;

        public string TaskName
        {
            get { return taskName; }
            set { taskName = value; }
        }

        public string Ac_dz
        {
            get { return ac_dz; }
            set { ac_dz = value; }
        }

        public string Ac_de
        {
            get { return ac_de; }
            set { ac_de = value; }
        }

        public string Ac_dn
        {
            get { return ac_dn; }
            set { ac_dn = value; }
        }


        public string This_dz
        {
            get { return this_dz; }
            set { this_dz = value; }
        }

        public string This_de
        {
            get { return this_de; }
            set { this_de = value; }
        }

        public string This_dn
        {
            get { return this_dn; }
            set { this_dn = value; }
        }

        public string Cyc
        {
            get { return cyc; }
            set { cyc = value; }
        }

        public string PointName
        {
            get { return pointName; }
            set { pointName = value; }
        }
    }
}