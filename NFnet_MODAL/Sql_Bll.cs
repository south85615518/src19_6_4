﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace NFnet_MODAL
{
    public class Sql_Bll : Sql_Bll_I
    {


        public string GetUnitSql_i()
        {
            return @"insert into unit(
            unitName, 
            pro, 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof,
            state,
            createTime,
            dbname
            )values(
            @unitName, 
            @pro, 
            @city, 
            @country, 
            @addr, 
            @tel, 
            @email, 
            @natrue, 
            @linkman, 
            @aptitude, 
            @police, 
            @taxproof,
            @state,
            @createTime,
            @dbname
            )";
        }


        public string GetUnitSql_u()
        {
            return @"update  unit set
            pro = @pro , 
            city = @city, 
            country = @country, 
            addr=@addr, 
            tel=@tel, 
            email=@email, 
            natrue=@natrue, 
            linkman=@linkman, 
            aptitude=@aptitude, 
            police=@police, 
            taxproof=@taxproof
             where
            unitName = @unitName
            ";
        }


        public string GetUnitSql_s()
        {
            return @"select pro , 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof,state,dbname from unit where unitName = @unitName";

        }


        public string GetUnitSql_ls()
        {
            return @"select
            unitName,
            pro , 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof from unit ";
        }


        public string GetMemberSql_i()
        {
            return @"insert into member(
            [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel],
            [email], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],
            [createTime]
            )values(
            @userId,
            @pass,
            @role, 
            @userGroup, 
            @userName, 
            @workNo, 
            @position, 
            @tel,
            @email, 
            @zczsmc, 
            @zczsbh, 
            @zczs, 
            @sgzsmc,
            @sgzsbh,
            @sgzs,
            @unitName,
            @createTime)";
        }
        /*
         */
        /*
         */
        public string GetMemberSql_u()
        {
            return @"update  member set
            [pass] = @pass , 
            [role] = @role, 
            [userGroup] = @userGroup, 
            [userName] = @userName, 
            [workNo] = @workNo, 
            [position] = @position, 
            [tel] = @tel, 
            [zczsmc] = @zczsmc , 
            [zczsbh] = @zczsbh, 
            [zczs] = @zczs, 
            [sgzsmc] = @sgzsmc,
            [sgzsbh] = @sgzsbh,
            [sgzs] = @sgzs,
            [email] = @email
             where
            [userId] = @userId
            ";
        }

        public string GetMemberSql_s()
        {
            return @"select [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],[email] from member where userId = @userId";
        }

        public string GetMemberSql_ls()
        {
            return @"select  [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],[email] from member ";
        }


        public string GetMemberSql_t()
        {
            return "select * from member where unitName = @unitName";
        }


        public string MemberLoginSql_s()
        {
            return "select * from member where userId = @userId and [pass]= @pass";
        }


        public string GetUnitSql_t()
        {
            return "select * from unit ";
        }


        public string GetInstrumentSql_i()
        {
            return @"insert into Instrument(
            [instrumentName],
            [vender],
            [instrumentNo],
            [instrumentType],
            [verificationDate],
            [validityPeriod],
            [verificationProof],
            [unitName]
            )values(
            @instrumentName,
            @vender,
            @instrumentNo,
            @instrumentType,
            @verificationDate,
            @validityPeriod,
            @verificationProof,
            @unitName
            )";
        }

        public string GetInstrumentSql_u()
        {
            throw new NotImplementedException();
        }

        public string GetInstrumentSql_s()
        {
            throw new NotImplementedException();
        }

        public string GetInstrumentSql_t()
        {
            return "select * from instrument where unitName=@unitName";
        }

        public string GetInstrumentSql_ls()
        {
            throw new NotImplementedException();
        }

        public string InstrumentLoginSql_s()
        {
            throw new NotImplementedException();
        }


        public string dbNameSql_s()
        {
            return @"SELECT SCHEMA_NAME 
FROM information_schema.SCHEMATA";
        }


        public string taskSql_s()
        {
            return @"select taskname from fmos_task";
        }


        public string projectInfoSql_u()
        {
            return @"insert into xmconnect( xmname,jcdw,dbase) values (@xmname,@jcdw,@dbase)";
        }


        public string projectSql_u()
        {
            throw new NotImplementedException();
        }


        public string studyPointSql_s()
        {
            return @"select point_name from fmos_studypoint";
        }


        public System.Data.DataTable FmosTableSql_s()
        {
            return null;
        }


        public string XminfoSql_s()
        {
            return "select * from xmconnect where xmname = @xmname ";
        }


        public string NearestSql_s()
        {
            return "select xmname from xmconnect,member where xmconnect.jcdw = member.unitName and member.userId= @userId order by  jcpgstarttime desc";
        }


        public string CGNearest_s()
        {
            return "select xmname from xmconnect  order by  jcpgstarttime desc";
        }


        public string GMXmnamesql_s()
        {
            return "select xmname from xmconnect where jcdw = @jcdw";
        }


        public string GetUnitMemberSql_ls()
        {
            return @"select  [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],[email] from member where unitName=@unitName";
        }


        public string GetUnitSql_Delu()
        {
            return "update unit set @zsname = '' where unitName = @unitName and @zsname = @url";
        }


        public string AlarmValueSql_i()
        {
            return "insert into fmos_alarmvalue(name,this_rn,this_re,this_rz,ac_rn,ac_re,ac_rz)value(@name,@this_rn,@this_re,@this_rz,@ac_rn,@ac_re,@ac_rz)";
        }

        public string AlarmValueSql_u()
        {
            return "update fmos_alarmvalue set this_rn=@this_rn,this_re=@this_re,this_rz=@this_rz,ac_rn=@ac_rn,ac_re=@ac_re,ac_rz=@ac_rz where name = @name";
        }


        public string AlarmValueSql_s()
        {
            return "select * from fmos_alarmvalue ";
        }

        public string PointAlarmValueSql_i()
        {
            return "insert into fmos_pointalarmvalue(taskName,point_name,pointtype,firstAlarmName,secondAlarmName,thirdAlarmName,remark)value(@taskName,@point_name,@pointtype,@firstAlarmName,@secondAlarmName,@thirdAlarmName,@remark)";
        }

        public string PointAlarmValueSql_u()
        {
            return "update fmos_pointalarmvalue set firstAlarmName=@firstAlarmName,secondAlarmName=@secondAlarmName,thirdAlarmName=@thirdAlarmName,remark = @remark where point_name= @point_name and taskName =@taskName";
        }

        //public string PointAlarmValueSql_s()
        //{
        //   return 
        //}


        public string AlarmValueSql_List()
        {
            return "select distinct(name) from fmos_alarmvalue";
        }


        public string PointAlarmValueSql_s()
        {
            throw new NotImplementedException();
        }

        public string PointAlarmValueSqlEmpty_i()
        {
            return "insert into fmos_pointalarmvalue (point_name,taskName) select distinct(point_name),taskName as point_name from fmos_studypoint where taskName=@taskName";
        }


        public string PointAlarmValueSqlIncreament_i()
        {
            return @"insert into fmos_pointalarmvalue (point_name,taskName)
select distinct(fmos_studypoint.POINT_NAME),fmos_studypoint.TaskName from fmos_studypoint where fmos_studypoint.POINT_NAME not in (select DISTINCT(POINT_NAME) from fmos_pointalarmvalue where TaskName=@taskName) 
and fmos_studypoint.TaskName=@taskName";
        }


        public string AlarmValueSql_del()
        {
            return "delete from fmos_alarmvalue where name= @name";
        }
        public string PointAlarmValueSql_del()
        {
            return "update  fmos_pointalarmvalue set firstAlarmName='',secondAlarmName='',thirdAlarmName='' where point_name in (@point_name) and taskName = @taskName";
        }


        public List<string> PointAlarmDelCasc()
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update fmos_pointalarmvalue set firstAlarmName='' where taskName=@taskName and firstAlarmName=@firstAlarmName");
            //二级预警
            ls.Add("update fmos_pointalarmvalue set secondAlarmName='' where taskName=@taskName and secondAlarmName=@secondAlarmName");
            //三级预警
            ls.Add("update fmos_pointalarmvalue set thirdAlarmName='' where taskName=@taskName and thirdAlarmName=@thirdAlarmName");
            return ls;
        }


        public string PointAlarmValueSql_Multiu()
        {
            return "update fmos_pointalarmvalue set firstAlarmName=@firstAlarmName,secondAlarmName=@secondAlarmName,thirdAlarmName=@thirdAlarmName,remark=@remark where   point_name in (@pointname) and taskName =@taskName or id = @id ";
        }


        public string StudyPointAdd_i()
        {
            return "insert into fmos_studypoint(point_name ,n,e,z,orglHar,orglVar,orglSd,stationName,search_time,onlyAngleUseful,mileage,iscontrolpoint,targethight,reflectorName,remark,taskName)values(@point_name ,@n,@e,@z,@orglHar,@orglVar,@orglSd,@stationName,@search_time,@onlyAngleUseful,@mileage,@iscontrolpoint,@targetheight,@reflectorName,@remark,@taskName)";
        }


        public string StudyPointSql_u()
        {
            return @"update fmos_studypoint set n = @n,e = @e,z = @z,orglHar = @orglHar,orglVar = @orglVar,orglSd = @orglSd,stationName = @stationName,onlyAngleUseful = @onlyAngleUseful,mileage = @mileage,iscontrolpoint = @iscontrolpoint,targethight = @targetheight,reflectorName = @reflectorName,remark = @remark where point_name = @point_name and taskName = @taskName";
        }


        public string StudyPointSql_del()
        {
            return "delete from fmos_studypoint where taskName=@taskName and point_name = @point_name";
        }


        public string StudyPointSql_d()
        {
            throw new NotImplementedException();
        }


        public string PointMultiSql_i()
        {
            return "insert into @tbname (@columnname,@columnxmname) values(@pointname,@xmname)";
        }



        public string PointRepeat_s()
        {
            return "select count(*) from @tbname where @columnname = @pointname and @columnxmname = @xmname";
        }


        public string jcmapspot_i()
        {
            return "insert into jcmapset (pointName,jclx,taskName,AbsX,AbsY)values(@pointName,@jclx,@taskName,@AbsX,@AbsY)";
        }


        public string jcmapspot_s()
        {
            return "select pointName,AbsX,AbsY from jcmapset where taskName = @taskName and jclx = @jclx";
        }


        public string jcmapspot_u()
        {
            return "update jcmapset set AbsX = @AbsX,AbsY = @AbsY where pointName = @pointName and taskName=@taskName and jclx = @jclx";
        }


        public string jcmapspot_del()
        {
            return "delete from jcmapset where pointName = @pointName and taskName = @taskName and jclx = @jclx ";
        }


        public string dataimport_i()
        {
            return "insert into @tableName ( @attrstr ) values( @valuestr )";
        }


        public string resultdataedit()
        {
            return @"insert into fmos_cycdirnet_gain (fmos_cycdirnet_gain.POINT_NAME,fmos_cycdirnet_gain.CYC ,
fmos_cycdirnet_gain.N,fmos_cycdirnet_gain.E,fmos_cycdirnet_gain.Z,fmos_cycdirnet_gain.Time,
fmos_cycdirnet_gain.This_dN,fmos_cycdirnet_gain.This_dE,fmos_cycdirnet_gain.This_dZ,fmos_cycdirnet_gain.Ac_dN,fmos_cycdirnet_gain.Ac_dE,fmos_cycdirnet_gain.Ac_dZ ,fmos_cycdirnet_gain.taskName)
select 
fmos_cycdirnet.POINT_NAME,@reportCyc,fmos_cycdirnet.N,fmos_cycdirnet.E,fmos_cycdirnet.Z,fmos_cycdirnet.Time,fmos_cycdirnet.This_dN,fmos_cycdirnet.This_dE,fmos_cycdirnet.This_dZ,fmos_cycdirnet.Ac_dN,fmos_cycdirnet.Ac_dE,fmos_cycdirnet.Ac_dZ ,fmos_cycdirnet.taskName from fmos_cycdirnet where fmos_cycdirnet.POINT_NAME =@pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.TaskName=@taskName";
        }



        public string IsResultRecordExits()
        {
            return "select count(*) from fmos_cycdirnet_gain where fmos_cycdirnet_gain.POINT_NAME =@pointname and fmos_cycdirnet_gain.CYC = @cyc and fmos_cycdirnet_gain.TaskName=@taskName";
        }


        public string ResultRecordUpdate()
        {
            //return "update fmos_cycdirnet_gain set ";
            return @"update fmos_cycdirnet_gain 
set 
fmos_cycdirnet_gain.N  = (select fmos_cycdirnet.N from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and       fmos_cycdirnet.taskName=@taskName),
fmos_cycdirnet_gain.E = (select fmos_cycdirnet.E from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName),
fmos_cycdirnet_gain.Z = (select fmos_cycdirnet.Z from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName),
fmos_cycdirnet_gain.Time = (select fmos_cycdirnet.Time from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName),
fmos_cycdirnet_gain.This_dN  = (select fmos_cycdirnet.This_dN from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName),
    fmos_cycdirnet_gain.This_dE = (select fmos_cycdirnet.This_dE from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName),
    fmos_cycdirnet_gain.This_dZ = (select fmos_cycdirnet.This_dZ from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName),
		fmos_cycdirnet_gain.Ac_dN = (select fmos_cycdirnet.Ac_dN from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName),
		fmos_cycdirnet_gain.Ac_dE = (select fmos_cycdirnet.Ac_dE from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName),
		fmos_cycdirnet_gain.Ac_dE = (select fmos_cycdirnet.Ac_dZ from  fmos_cycdirnet  where fmos_cycdirnet.POINT_NAME = @pointname and fmos_cycdirnet.CYC = @cyc and fmos_cycdirnet.taskName=@taskName) 
where fmos_cycdirnet_gain.POINT_NAME = @pointname and fmos_cycdirnet_gain.CYC = @reportCyc and fmos_cycdirnet_gain.taskName=@taskName";
        }
        public string ResultLawCheck()
        {
            return @"select fmos_cycdirnet_gain.cyc,fmos_cycdirnet_gain.time from fmos_cycdirnet_gain where
fmos_cycdirnet_gain.POINT_NAME = @pointName and fmos_cycdirnet_gain.taskName = @taskName and
 (fmos_cycdirnet_gain.cyc <@reportCyc and fmos_cycdirnet_gain.Time >=@time) or
(fmos_cycdirnet_gain.cyc >@reportCyc and fmos_cycdirnet_gain.Time <=@time)";
            //throw new NotImplementedException();
        }


        public string ResultDataDel()
        {
            return @"delete from fmos_cycdirnet_gain where fmos_cycdirnet_gain.POINT_NAME = @pointName and fmos_cycdirnet_gain.taskName = @taskName and fmos_cycdirnet_gain.cyc = @cyc";
        }
    }
}