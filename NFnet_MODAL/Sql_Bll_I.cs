﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NFnet_MODAL
{
    interface Sql_Bll_I
    {
        //插入机构信息
         string GetUnitSql_i();
        //更新机构信息
         string GetUnitSql_u();
        //根据机构名称和图片url置空文件路径
         string GetUnitSql_Delu();
         //获取机构信息表
         string GetUnitSql_t();
        //查询机构信息
         string GetUnitSql_s();
        //获取机构列表
         string GetUnitSql_ls();
        //插入管理员信息
         string GetMemberSql_i();
        //更新管理员信息
         string GetMemberSql_u();
        //查询管理员信息
         string GetMemberSql_s();
        //获取管理员信息表
         string GetMemberSql_t();
        //获取管理员列表
         string GetMemberSql_ls();
        //获取机构所有管理员
         string GetUnitMemberSql_ls();
        //管理员登陆验证
         string MemberLoginSql_s();
         //插入设备信息
         string GetInstrumentSql_i();
         //更新设备信息
         string GetInstrumentSql_u();
         //查询设备信息
         string GetInstrumentSql_s();
         //获取设备信息表
         string GetInstrumentSql_t();
         //获取设备列表
         string GetInstrumentSql_ls();
         //设备登陆验证
         string InstrumentLoginSql_s();
        //数据库中所有的库名
         string dbNameSql_s();
        //获取全站仪任务表中的信息
         string taskSql_s();
        //在项目表中填充名称和监测单位
         string projectSql_u(); 
        //获取表面位移（全站仪）的点号
         string studyPointSql_s();
        //获取表面位移（全站仪）表
         DataTable FmosTableSql_s();
        //获取工程信息
         string XminfoSql_s();
        //获取管理员最新监测项目的名称
         string NearestSql_s();
        //获取超管最新监测项目名称
         string CGNearest_s();
        //获取管理员的项目名称
         string GMXmnamesql_s();
        //预警值保存
         string AlarmValueSql_i();
        //预警值更新
         string AlarmValueSql_u();
        //预警值显示
         string AlarmValueSql_s();
        //预警值名称选项生成
         string AlarmValueSql_List();
        //预警删除
         string AlarmValueSql_del();
        //点号预警设置
         string PointAlarmValueSql_i();
        //点号预警更新
         string PointAlarmValueSql_u();
        //点号预警多项更新
         string PointAlarmValueSql_Multiu();
        //点号预警显示
         string PointAlarmValueSql_s(); 
        //点号预警的空插入语句
         string PointAlarmValueSqlEmpty_i();
        //点号预警新增点号
         string PointAlarmValueSqlIncreament_i(); 
        //学习点添加
         string StudyPointAdd_i();
        //学习点更新
         string StudyPointSql_u(); 
        //删除学习点
         string StudyPointSql_d();
        //监测项目
         string PointMultiSql_i(); 
        //点名重复检查
         string PointRepeat_s();
        //监测点平面
         string jcmapspot_i();
        //监测点更新
         string jcmapspot_u();
        //监测点平面热点加载
         string jcmapspot_s(); 
        ////表面位移数据上传接口数据表插入语句生成
         string dataimport_i();
        //监测点平面热点图删除
         string jcmapspot_del();
        //结果数据编辑
         string resultdataedit();
        //结果数据记录是否存在
         string IsResultRecordExits();
        //结果数据更新
         string ResultRecordUpdate(); 
        //结果数据编辑合法性检查
         string ResultLawCheck(); 
        //删除成果数据
         string ResultDataDel();
        //级联删除点号预警中已经设置的预警
         List<string> PointAlarmDelCasc();

    }
}
