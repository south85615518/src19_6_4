﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace NFnet_MODAL
{
   public interface FileManage_i
    {
        //创建项目目录
        void FileMenuCreate(string filepath);
        //显示项目目录下的所有文件
        List<string> FileTravel(string filePath);
        //显示项目目录下的所有文件夹
        //文件批量下载
        void FileDownLoad(List<string> filePathList);
        //列出文件夹下的所有的文件夹和文件
        List<file> DirectoryFilesDispaly(string fileName);

    }
}
