﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace NFnet_MODAL
{
    public interface Sql_Bll_E
    {
        //机构
         string UnitInsertExcute(Unit unit);
         string UnitUpdateExcute(Unit unit);
         Unit UnitSelectExcute(Unit unit);
         List<Unit> UnitSelectListExcute(Unit unit);
         DataTable UnitTableExcute(Unit member);
        //人员
         string MemberInsertExcute(member member);
         string MemberUpdateExcute(member member);
         DataTable MemberTableExcute(member member);
         member MemberSelectExcute(member member);
         List<member> MemberSelectListExcute(member member);
         member MemberLoginExcute(member member);
        //设备
         string InstrumentInsertExcute(Instrument Instrument);
         string InstrumentUpdateExcute(Instrument Instrument);
         DataTable InstrumentTableExcute(Instrument Instrument);
         Instrument InstrumentSelectExcute(Instrument Instrument);
         List<Instrument> InstrumentSelectListExcute(Instrument Instrument);
         Instrument InstrumentLoginExcute(Instrument Instrument);
        //数据库
         List<string> DbListExcute();
        //项目
         List<string> ProjectNameExcute();
         void projectInfoInsertExcute(xm xminfo);
         List<string> StudyPointExcute(jcxmAndjcd xminfo);
         xm XmInfoExcute(xm xm);
         string NearestXmnameExcute(member member);
         List<string> GMXmnameList(member member);
    }
}