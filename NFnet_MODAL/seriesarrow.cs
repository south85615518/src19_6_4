﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_MODAL
{
    public class seriesarrow
    {
        public string arrowname { get; set; }
        public List<serie_point> ls { get; set; }
        public List<serie_cyc> ls_serie_cyc { get; set; }
        public List<serie_cyc_null> lscyc { get; set; }
        public List<serie> lsseries { get; set; }
        public seriesarrow()
        { }
    }
}