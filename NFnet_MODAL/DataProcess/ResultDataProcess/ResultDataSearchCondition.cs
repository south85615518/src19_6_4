﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_MODAL.DataProcess.ResultDataProcess
{
    public class ResultDataSearchCondition:SearchCondition
    {
        /// <summary>
        /// 起始周期
        /// </summary>
        private int startCyc;

        public int StartCyc
        {
            get { return startCyc; }
            set { startCyc = value; }
        }
        /// <summary>
        /// 结束周期
        /// </summary>
        private int endCyc;

        public int EndCyc
        {
            get { return endCyc; }
            set { endCyc = value; }
        }
    }
}