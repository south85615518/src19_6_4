﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_MODAL.DataProcess
{
    
    public abstract class SearchCondition
    {
        /// <summary>
        /// 项目名
        /// </summary>
        private string xmname;

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        /// <summary>
        /// 排序列名
        /// </summary>
        private string colName;

        public string ColName
        {
            get { return colName; }
            set { colName = value; }
        }
        /// <summary>
        /// 起始页号
        /// </summary>
        private int pageIndex;

        public int PageIndex
        {
            get { return pageIndex; }
            set { pageIndex = value; }
        }
        /// <summary>
        /// 每页行数
        /// </summary>
        private int rows;

        public int Rows
        {
            get { return rows; }
            set { rows = value; }
        }
        /// <summary>
        /// 排序列名
        /// </summary>
        private string sord;

        public string Sord
        {
            get { return sord; }
            set { sord = value; }
        }

    }
}