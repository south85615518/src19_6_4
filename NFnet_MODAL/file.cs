﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_MODAL
{
    public class file:IComparable
    {
        private string fileName, fileType, loadAddress, size;
        private string dt;

        public string Dt
        {
            get { return dt; }
            set { dt = value; }
        }

        public string Size
        {
            get { return size; }
            set { size = value; }
        }

        public string LoadAddress
        {
            get { return loadAddress; }
            set { loadAddress = value; }
        }

        public string FileType
        {
            get { return fileType; }
            set { fileType = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public int CompareTo(object obj)
        {
            file fobj = obj as file;
            if (fobj == null || string.IsNullOrEmpty(fobj.Dt) ) return -1;

            return Convert.ToDateTime(this.Dt).CompareTo(Convert.ToDateTime(fobj.Dt));
        }
    }
}