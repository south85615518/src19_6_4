﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Data;
using NFnet_MODAL;
namespace NFnet_MODAL
{
    public  class member
    {
        private string userId,
        password,
        role,
        userGroup,
        userName,
        workNo,
        position,
        tel,
        email,
        zczsmc,
        zczsbh,
        zczs,
        sgzsmc,
        sgzsbh,
        sgzs,
        unitName;


        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string Role
        {
            get { return role; }
            set { role = value; }
        }

        public string UserGroup
        {
            get { return userGroup; }
            set { userGroup = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string WorkNo
        {
            get { return workNo; }
            set { workNo = value; }
        }

        public string Position
        {
            get { return position; }
            set { position = value; }
        }

        public string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public string Zczsmc
        {
            get { return zczsmc; }
            set { zczsmc = value; }
        }

        public string Zczsbh
        {
            get { return zczsbh; }
            set { zczsbh = value; }
        }

        public string Zczs
        {
            get { return zczs; }
            set { zczs = value; }
        }

        public string Sgzsmc
        {
            get { return sgzsmc; }
            set { sgzsmc = value; }
        }

        public string Sgzsbh
        {
            get { return sgzsbh; }
            set { sgzsbh = value; }
        }

        public string Sgzs
        {
            get { return sgzs; }
            set { sgzs = value; }
        }

        public string UnitName
        {
            get { return unitName; }
            set { unitName = value; }
        }

        /// <summary>
        /// 获取人员信息
        /// </summary>
        /// <returns></returns>
        public   member GetMember()
            ;
        /// <summary>
        /// 人员信息登录
        /// </summary>
        /// <returns></returns>
        public  member LoginMember();
        /// <summary>
        /// 获取人员信息列表
        /// </summary>
        /// <returns></returns>
        public  List<member> GetMemberList();
        /// <summary>
        /// 上传文件选择框中选择的文件
        /// </summary>
        /// <param name="fu"></param>
        public  string ImageUploadInsert(FileUpload fu, string postName);
        /// <summary>
        /// 文件删除
        /// </summary>
        /// <param name="path"></param>
        public  void ImageFileDel(string path);

        /// <summary>
        /// 绝对路径转化成相对路径
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public  string FileImagePath(string fullPath);

        public  DataTable GetMemberTable();
        /// <summary>
        /// 获取人员信息
        /// </summary>
        /// <returns></returns>
        public static string GetPosition(string userId)
        {
            return "";
        }

    }

}