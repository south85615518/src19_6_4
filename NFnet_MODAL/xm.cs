﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;

namespace NFnet_MODAL
{
    public class xm
    {

        private string jcCreateDate;//项目建立日期
        private string province, city, area, lng, lat, jcdw, wtdw, jcry, respone, telphone, xmlx, jcrq, dbase;

        public string Dbase
        {
            get { return dbase; }
            set { dbase = value; }
        }

        public string Jcrq
        {
            get { return jcrq; }
            set { jcrq = value; }
        }

        public string Xmlx
        {
            get { return xmlx; }
            set { xmlx = value; }
        }

        public string Telphone
        {
            get { return telphone; }
            set { telphone = value; }
        }

        public string Respone
        {
            get { return respone; }
            set { respone = value; }
        }

        public string Jcry
        {
            get { return jcry; }
            set { jcry = value; }
        }

        public string Wtdw
        {
            get { return wtdw; }
            set { wtdw = value; }
        }

        public string Jcdw
        {
            get { return jcdw; }
            set { jcdw = value; }
        }

        public string Lat
        {
            get { return lat; }
            set { lat = value; }
        }

        public string Lng
        {
            get { return lng; }
            set { lng = value; }
        }

        public string Area
        {
            get { return area; }
            set { area = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Province
        {
            get { return province; }
            set { province = value; }
        }
        private string id;//项目编号

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        private string xmname;//名称

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        private string xmaddress;//项目地址

        public string Xmaddress
        {
            get { return xmaddress; }
            set { xmaddress = value; }
        }
        private string xmbz;//备注

        public string Xmbz
        {
            get { return xmbz; }
            set { xmbz = value; }
        }
        private string creatMan;//创立者

        public string CreatMan
        {
            get { return creatMan; }
            set { creatMan = value; }
        }

        private string createTime;//创建时间

        public string CreateTime
        {
            get { return createTime; }
            set { createTime = value; }
        }
        private List<jclx> jcls = null;

        public List<jclx> Jcls
        {
            get { return jcls; }
            set { jcls = value; }
        }
        
    }
}