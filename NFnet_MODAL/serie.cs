﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_MODAL
{
    public class serie:ICloneable
    {
        protected string name;//曲线名称
        private string stype;//曲线类型
        private float yjV;//预警值

        public float YjV
        {
            get { return yjV; }
            set { yjV = value; }
        }
        private float bjV;//报警值

        public float BjV
        {
            get { return bjV; }
            set { bjV = value; }
        }
        private float kzV;//控制值

        public float KzV
        {
            get { return kzV; }
            set { kzV = value; }
        }
        public string Stype
        {
            get { return stype; }
            set { stype = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        protected pt[] pts;//数据点  

        public pt[] Pts
        {
            get { return pts; }
            set { pts = value; }
        }
        /// <summary>
        /// 按照指定的数值对曲线进行衰减默认衰减100个刻度
        /// </summary>
        /// <param name="s"></param>
        /// <param name="bs"></param>
        public void sjorfd(serie s,int bs)
        {
                
       
        }
        object ICloneable.Clone()
        {
            return this.Clone();
        }
        public serie Clone()
        {
            return (serie)this.MemberwiseClone();
        } 
      
    }

}