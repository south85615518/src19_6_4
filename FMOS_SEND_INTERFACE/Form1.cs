﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NFnet_Interface.DisplayDataProcess.HXYL;
namespace FMOS_SEND_INTERFACE
{
    /// <summary>
    /// 向接受模块推送数据文件
    /// </summary>
    public partial class Form1 : Form
    {
        
        public System.Timers.Timer timerrestart = new System.Timers.Timer(100000);
        public static bool sendstart = false;
        public static int i = 0;
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 生成数据文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        public void TcpClientConnectInterval()
        {
            string mdbjson = HXYLdeliver.ProcessHXYLdeliver();
            //ProcessHXYLDataImport.HXYLDataDeSerialize(mdbjson + "_HXYLDATAEND" + "times(" + DateTime.Now + ")");//(mdbjson);
           
            //连接
            string mgss = "";
            if (TcpServer.client == null || TcpServer.client.Connected == false)
            {
                TcpServer.client = new System.Net.Sockets.TcpClient();
                mgss = TcpServer.ServerConncet(this.textBox2.Text, this.textBox3.Text);
            }
            //this.label5.Text = mgss+i;
            i++;
            //发送
            //List<byte> lb = new List<byte>();
            string str = mdbjson + "_HXYLDATAEND" + "times(" + DateTime.Now + ")";
            try
            {

                //byte[] buf = strToToHexByte(str);
                TcpServer server = new TcpServer { StrFile = str };
                //MessageBox.Show("开始发送字节");

                string result = server.SendString(this.textBox2.Text, this.textBox3.Text);
                if (result.IndexOf("success") != -1) HXYLdeliver.ProcessHXYLdeliveFinishedSet(); 
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

                string a = "";
            }
            finally
            {
                TimerTick();
                
            }
        }




        private void button6_Click(object sender, EventArgs e)
        {
            sendstart = true;
            TimerTick();
        }
        public void TimerTick()
        {
            //timer.Elapsed += new System.Timers.ElapsedEventHandler(HearBeat);
            timerrestart.Interval = Convert.ToInt32(this.textBox5.Text) * 60 * 1000;
            timerrestart.Elapsed += new System.Timers.ElapsedEventHandler(Jiangesend);
            timerrestart.Enabled = sendstart;
            if (!sendstart) this.label5.Text = "发送已停止";
            //timer.Enabled = true;
            timerrestart.AutoReset = sendstart;
            //timer.AutoReset = true;
        }
        public void Jiangesend(object source, System.Timers.ElapsedEventArgs e)
        {
            string dtuportpath = Convert.ToString(source);
            timerrestart.Stop();
            //timer.Enabled = false;
            timerrestart.Enabled = false;
            //timer.AutoReset = false;
            //ExceptionLog.DTURecordWrite("开始重启监听..." + dtutcpportpath);
            //this.comboBox2.SelectedIndex = 1;
            //TcpServerStopTotal();
            //EncodingSet();
            //TcpServerStopTotal();
            //List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(dtutcpportpath);
            //"D:\\华南水电\\DTUServer\\TcpSever_MYSQL+\\TcpSever\\bin\\debug\\端口配置文件\\setting.txt"
            //ExceptionLog.DTURecordWrite("开始启动监听..."+string.Join(":",));
            //TcpserverMutilStart(ltlif);
            //TimerTick();
            

            timerrestart.Elapsed -= new System.Timers.ElapsedEventHandler(Jiangesend);
            //DTUTcpServerStart(Jiangesend);
            TcpClientConnectInterval();
            
        }



        private void button2_Click(object sender, EventArgs e)
        {
            
            try {
                this.label2.Text = "文件发送中!";
                if (TcpServer.client == null || TcpServer.client.Connected == false)
                {
                    TcpServer.client = new System.Net.Sockets.TcpClient();
                    TcpServer.ServerConncet(this.textBox2.Text, this.textBox3.Text);
                }
                TcpServer server = new TcpServer { StrFile = this.textBox1.Text };
                MessageBox.Show("开始发送文件");
                string result = server.SendFile(this.textBox2.Text, this.textBox3.Text);
              this.label2.Text = result;
              if (this.checkBox1.Checked == true)
              {
                  this.timer1.Enabled = true;
                  this.timer1.Interval = 6000;
                  this.timer1.Start();
              }
            }
            catch(Exception ex)
            {
                ExceptionLog.ExceptionWrite("服务器连接出错");
                this.label2.Text = "failed!";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
               
                TcpServer.client.Close();
                TcpServer.client = null;
            }
            catch (Exception ex)
            { 
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = Path.GetFullPath(fileDialog.FileName);
                //MessageBox.Show(path);
                this.textBox1.Text = path;
            }
        }

       


        private void button4_Click(object sender, EventArgs e)
        {
            string mgss = "";
            if (TcpServer.client == null || TcpServer.client.Connected == false)
                {
                    TcpServer.client = new System.Net.Sockets.TcpClient();
                    mgss = TcpServer.ServerConncet(this.textBox2.Text,this.textBox3.Text);
                }
            this.label5.Text = mgss;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string info = "";
            try
            {
                if (TcpServer.client == null || TcpServer.client.Connected == false)
                {
                    TcpServer.client = new System.Net.Sockets.TcpClient();
                    TcpServer.ServerConncet(this.textBox2.Text, this.textBox3.Text);
                }
                TcpServer server = new TcpServer { StrFile = this.textBox1.Text };
                //MessageBox.Show("开始发送文件");
                string result = server.SendFile(this.textBox2.Text, this.textBox3.Text);
                info = string.Format("{0}向{1}发送文件{2}成功！", DateTime.Now, this.textBox2.Text, this.textBox1.Text);
            }
            catch (Exception ex)
            {
                info = string.Format("{0}向{1}发送文件{2}失败！原因:{3}", DateTime.Now, this.textBox2.Text, this.textBox1.Text,ex.Message);
            }
            this.listBox1.Items.Add(info);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox1.Checked == false)
            {

                this.timer1.Enabled = false;
            }
        }



        private void button5_Click(object sender, EventArgs e)
        {
            List<byte> lb = new List<byte>();
            string str = this.textBox4.Text.Trim();
            try
            {

                byte[] buf = strToToHexByte(str);
                TcpServer server = new TcpServer { sendbytes = buf };
                MessageBox.Show("开始发送字节");

                string result = server.SendString(this.textBox2.Text, this.textBox3.Text);
            }
            catch (Exception ex)
            {
               MessageBox.Show(ex.Message);
            }

            //byte[] str = Encoding.Default.GetBytes("1C 00 06 00 2D 11 01 44 57 68");
            /*
             * RX:FF 1C 00 06 00 2D 11 01 44 57 68 02 8E 03 64 4E 62 80 40 30 64 00 00 00 00 31 64 00 00 00 00 32 64 00 00 00 00 33 64 00 00 00 00 4B 8C 
             */
        }
        /// <summary>  
        /// 字符串转16进制字节数组  
        /// </summary>  
        /// <param name="hexString"></param>  
        /// <returns></returns>  
        private static byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }  
   
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            sendstart = false;
        }

      
        
    }
}
