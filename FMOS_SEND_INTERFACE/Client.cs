﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace TcpSever
{
    /// <summary>
    /// 客户端
    /// </summary>
    class Client
    {
        public static TcpClient tcp = new TcpClient();
        


        //根据服务器的IP地址和侦听的端口连接
        //连接服务器
        public static string ServerConncet(string ip, string port)
        {

            tcp = new TcpClient();
            try
            {
                tcp.Connect(ip, Convert.ToInt32(port));
                if (tcp.Connected)
                {

                    //连接成功的消息机制  详细见DEMO

                    return "成功连接上了服务器!";

                }
                else
                {
                    return "服务器连接失败!";
                }
            }
            catch (Exception ex)
            {
                return "服务器连接失败!";

            }



        }
        //向服务器发送数据
        public static void SendMessToServer(string mess, string ip, string port)
        {
            if (!tcp.Connected)
            {
                tcp = new TcpClient();
                tcp.Connect(ip, Convert.ToInt32(port));
            }
            NetworkStream streamToServer = tcp.GetStream();
            byte[] buffer = Encoding.Default.GetBytes(mess); //msg为发送的字符串   
            try
            {



                streamToServer.Write(buffer, 0, buffer.Length);     // 发往服务器
                //接受服务器回发的信息
                //RecvMessServer(tcp,ip,port);
                tcp.Close();


            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }




        //接收服务器发送的数据
        public static void RecvMessServer(TcpClient tcp, string ip, string port)
        {
            if (!tcp.Connected)
            {
                tcp = new TcpClient();
                tcp.Connect(ip, Convert.ToInt32(port));
            }
            NetworkStream streamToServer = tcp.GetStream();
            byte[] buffer = new byte[1000];
            int bytesRead = streamToServer.Read(buffer, 0, buffer.Length);
            //Mssg.ServerMess = Encoding.Default.GetString(buffer);
        }
        //释放连接
        public static void DisposedTcpLink()
        {
            tcp.Close();
        }

    }
}
