﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.DisplayDataProcess.HXYL;
using NFnet_BLL.DisplayDataProcess.HXYL;
using NFnet_Interface.DisplayDataProcess.BKG;
using System.Web.Script.Serialization;
namespace FMOS_SEND_INTERFACE
{
    public class HXYLdeliver
    {
        public static ProcessHXYLPointBLL processHXYLPointBLL = new ProcessHXYLPointBLL();
        public static ProcessHXYLData processHXYLData = new ProcessHXYLData();
        public static ProcessHXYLChart processHXYLChart = new ProcessHXYLChart();
        public static ProcessHXYLIDBLL processHXYLIDBLL = new ProcessHXYLIDBLL();
        public static ProcessHXYLMaxTime processHXYLMaxTime = new ProcessHXYLMaxTime();
        public static ProcessHXYLDATALoad processHXYLDATALoad = new ProcessHXYLDATALoad();
        public static ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static string mssg = "";
        public static List<MDBDATA.Model.hxyl> ls = new List<MDBDATA.Model.hxyl>();
        public static string ProcessHXYLdeliver()
        {
            DateTime maxTime = new DateTime();
            processHXYLBLL.ProcessMaxTime(out maxTime, out mssg);
            
            //ProcessPrintMssg.Print(mssg);
            List<MDBDATA.Model.hxyl>  lsimport = processHXYLDATALoad.HXYLDATALoad(maxTime, out mssg);
            //ProcessPrintMssg.Print(mssg);
            foreach (var model in lsimport)
            {
                processHXYLBLL.ProcessHXYLAdd(model, out mssg);
            }
            processHXYLBLL.ProcessHXYLDATALoad(out ls,out mssg);

            return jss.Serialize(ls);
        }
        public static bool ProcessHXYLdeliveFinishedSet()
        {
            foreach (var model in ls)
            {
                processHXYLBLL.ProcessHXYLUpdate(model, out mssg);
            }
            return true;
        }
    }
}
