﻿/**  版本信息模板在安装目录下，可自行修改。
* dtutimetask.cs
*
* 功 能： N/A
* 类 名： dtutimetask
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//Please add references
namespace DTU.DAL
{
	/// <summary>
	/// 数据访问类:dtutimetask
	/// </summary>
	public partial class dtutimetask
	{
		
		#region  BasicMethod
        public bool FixedInclinometerTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format(@"select  deviceno as module,times,time,fixed_inclinometer_chain.xmno,applicated,hour,minute
 from  fixed_inclinometer_chain left join  dtutimetask  on  fixed_inclinometer_chain.deviceno = dtutimetask.module and fixed_inclinometer_chain.xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool FixedInclinometerXmPortTableLoad(int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format(@"select  deviceno,times,time,fixed_inclinometer_chain.xmno,'" + xmname + "' as xmname,applicated,hour,minute  from  fixed_inclinometer_chain left join  dtutimetask  on  deviceno = module and fixed_inclinometer_chain.xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }


        public bool FixedInclinometerTableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(1) from fixed_inclinometer_chain where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
            
        }

       


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

