﻿/**  版本信息模板在安装目录下，可自行修改。
* dtumodule.cs
*
* 功 能： N/A
* 类 名： dtumodule
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/13 14:08:56   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//Please add references
namespace DTU.DAL
{
    /// <summary>
    /// 数据访问类:dtumodule
    /// </summary>
    public partial class dtumodule
    {
        public static database db = new database();
        public dtumodule()
        { }
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        //public bool Exists(string addressno,string od)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from dtumodule");
        //    strSql.Append(" where addressno=@addressno and od=@od ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@addressno", OdbcType.VarChar,100),
        //            new OdbcParameter("@od", OdbcType.VarChar,100)			};
        //    parameters[0].Value = addressno;
        //    parameters[1].Value = od;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}

        public bool ExistSenorNo(int xmno, string module,string senorno)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select  count(1) from dtumodule");
            strSql.Append(" where     senorno=@senorno    and   id<>@module  ");
            OdbcParameter[] parameters = {
                        new OdbcParameter("@senorno", OdbcType.VarChar,100),
                        new OdbcParameter("@module", OdbcType.VarChar,100)
                        		};
            parameters[0].Value = senorno;
            parameters[1].Value = module;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return Convert.ToInt32(obj) > 0 ? true : false;

        }



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(DTU.Model.dtumodule model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("insert into dtumodule(");
            strSql.Append("name,id,addressno,od,port,xmno,senorno)");
            strSql.Append(" values (");
            strSql.Append("@name,@id,@addressno,@od,@port,@xmno,@senorno)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@name", OdbcType.VarChar,500),
					new OdbcParameter("@id", OdbcType.VarChar,500),
					new OdbcParameter("@addressno", OdbcType.VarChar,100),
					new OdbcParameter("@od", OdbcType.VarChar,100),
					new OdbcParameter("@port", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@senorno", OdbcType.VarChar,100)};
            parameters[0].Value = model.name;
            parameters[1].Value = model.id;
            parameters[2].Value = model.addressno;
            parameters[3].Value = model.od;
            parameters[4].Value = model.port;
            parameters[5].Value = model.xmno;
            parameters[6].Value = model.senorno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTU.Model.dtumodule model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("update dtumodule set ");
            strSql.Append("addressno=@addressno,");
            strSql.Append("od=@od,");
            strSql.Append("name=@name,");
            strSql.Append("port=@port,");
            strSql.Append("senorno=@senorno");
            strSql.Append("     where     id=@id ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@addressno", OdbcType.VarChar,500),
                    new OdbcParameter("@od", OdbcType.VarChar,500),
					new OdbcParameter("@name", OdbcType.VarChar,500),
					new OdbcParameter("@port", OdbcType.Int,11),
					new OdbcParameter("@senorno", OdbcType.VarChar,100),
					new OdbcParameter("@id", OdbcType.VarChar,100)
					};
            parameters[0].Value = model.addressno;
            parameters[1].Value = model.od;
            parameters[2].Value = model.name;
            parameters[3].Value = model.port;
            parameters[4].Value = model.senorno;
            parameters[5].Value = model.id;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int xmno, string addressno, string od)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from dtumodule ");
            strSql.Append(" where     addressno=@addressno     and      od=@od   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@addressno", OdbcType.VarChar,100),
					new OdbcParameter("@od", OdbcType.VarChar,100)			};
            parameters[0].Value = addressno;
            parameters[1].Value = od;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获取传感器编号
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool ModuleNoGet(int xmno, out string moduleStr)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select distinct(id) from dtumodule where xmno = " + xmno + " ";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            moduleStr = string.Join(";", lsFormat);
            return true;
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtumodule GetModel(string addressno, string od)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select name,id,addressno,od,port,xmno,senorno from dtumodule ");
            strSql.Append(" where addressno=@addressno and od=@od ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@addressno", OdbcType.VarChar,100),
					new OdbcParameter("@od", OdbcType.VarChar,100)			};
            parameters[0].Value = addressno;
            parameters[1].Value = od;

            DTU.Model.dtumodule model = new DTU.Model.dtumodule();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select id,name,addressno,od,port,senorno from dtumodule where xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(*) from dtumodule where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtumodule DataRowToModel(DataRow row)
        {
            DTU.Model.dtumodule model = new DTU.Model.dtumodule();
            if (row != null)
            {
                if (row["name"] != null)
                {
                    model.name = row["name"].ToString();
                }
                if (row["id"] != null)
                {
                    model.id = row["id"].ToString();
                }
                if (row["addressno"] != null)
                {
                    model.addressno = row["addressno"].ToString();
                }
                if (row["od"] != null)
                {
                    model.od = row["od"].ToString();
                }
                if (row["port"] != null && row["port"].ToString() != "")
                {
                    model.port = int.Parse(row["port"].ToString());
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["senorno"] != null)
                {
                    model.senorno = row["senorno"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select name,id,addressno,od,port,xmno,senorno ");
            strSql.Append(" FROM dtumodule ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM dtumodule ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.od desc");
            }
            strSql.Append(")AS Row, T.*  from dtumodule T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OdbcParameter[] parameters = {
                    new OdbcParameter("@tblName", OdbcType.VarChar, 255),
                    new OdbcParameter("@fldName", OdbcType.VarChar, 255),
                    new OdbcParameter("@PageSize", OdbcType.Int),
                    new OdbcParameter("@PageIndex", OdbcType.Int),
                    new OdbcParameter("@IsReCount", OdbcType.Bit),
                    new OdbcParameter("@OrderType", OdbcType.Bit),
                    new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
                    };
            parameters[0].Value = "dtumodule";
            parameters[1].Value = "od";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

