﻿/**  版本信息模板在安装目录下，可自行修改。
* dtulp.cs
*
* 功 能： N/A
* 类 名： dtulp
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/25 16:16:12   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//Please add references
namespace DTU.DAL
{
    /// <summary>
    /// 数据访问类:dtulp
    /// </summary>
    public partial class dtulp
    {
        public static database db = new database();
        public dtulp()
        { }
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int xmno,string module)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select count(1) from dtulp");
            strSql.Append(" where module=@module  and xmno = @xmno");
            OdbcParameter[] parameters = {
					new OdbcParameter("@module",OdbcType.VarChar,200),
                    new OdbcParameter("@xmno",OdbcType.Int)
                                         };
            parameters[0].Value = module;
            parameters[1].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);

            return Convert.ToInt32(obj) > 0 ? true : false;//OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(DTU.Model.dtulp model)
        {
            StringBuilder strSql = new StringBuilder();
            SingleTonOdbcSQLHelper sqlhelper = new SingleTonOdbcSQLHelper();
            sqlhelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("insert into dtulp(");
            strSql.Append("module,LP,xmno)");
            strSql.Append(" values (");
            strSql.Append("@module,1,@xmno)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@module",OdbcType.VarChar,200),	
                    new OdbcParameter("@xmno",OdbcType.Int,11)
                                         };
            parameters[0].Value = model.module;
            parameters[1].Value = model.xmno;
            int rows = sqlhelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTU.Model.dtulp model)
        {
            StringBuilder strSql = new StringBuilder();
            SingleTonOdbcSQLHelper sqlhelper = new SingleTonOdbcSQLHelper();
            sqlhelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("update dtulp set ");
            strSql.Append("LP=@LP,");
            strSql.Append("applicated=@applicated");
            strSql.Append("      where      module=@module       and        xmno = @xmno    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@LP",OdbcType.Int,11),
                    new OdbcParameter("@applicated",OdbcType.TinyInt,2),
					new OdbcParameter("@module",OdbcType.VarChar,200),
                    new OdbcParameter("@xmno",OdbcType.Int,11)
                                         };
            parameters[0].Value = model.LP;
            parameters[1].Value = model.applicated;
            parameters[2].Value = model.module;
            parameters[3].Value = model.xmno;
            int rows = sqlhelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string module,int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from dtulp ");
            strSql.Append(" where module=@module and xmno = @xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@module",OdbcType.VarChar,200),	
		            new OdbcParameter("@xmno",OdbcType.Int)
                                         };
            parameters[0].Value = module;
            parameters[1].Value = xmno;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        //public bool DeleteList(string modulelist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from dtulp ");
        //    strSql.Append(" where module in ("+modulelist + ")  ");
        //    int rows=OdbcSQLHelper.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string module, out DTU.Model.dtulp model)
        {
            model = null;
            SingleTonOdbcSQLHelper sqlhelper = new SingleTonOdbcSQLHelper();
            sqlhelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from dtulp ");
            strSql.Append(" where module = @module   and   xmno = @xmno ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@module",OdbcType.VarChar,200),
                    new OdbcParameter("@xmno",OdbcType.Int,11)
                                         };
            parameters[0].Value = module;
            parameters[1].Value = xmno;
            model = new DTU.Model.dtulp();
            DataSet ds = sqlhelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select distinct(addressno),if(LP = 1,'true','false') as lp , applicated  from dtumodule left join dtulp on dtumodule.addressno = dtulp.module where dtumodule.xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public bool XmPortTableLoad(int startPageIndex, int pageSize, int xmno,string xmname ,string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select distinct(addressno),if(LP = 1,'true','false') as lp, applicated , '"+xmname+"' as  xmname  from dtumodule left join dtulp on dtumodule.addressno = dtulp.module where dtumodule.xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(1) from dtumodule where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtulp DataRowToModel(DataRow row)
        {
            DTU.Model.dtulp model = new DTU.Model.dtulp();
            if (row != null)
            {
                if (row["module"] != null)
                {
                    model.module = row["module"].ToString();
                }
                if (row["LP"] != null && row["LP"].ToString() != "")
                {
                    model.LP = int.Parse(row["LP"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select module,LP ");
            strSql.Append(" FROM dtulp ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM dtulp ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.module desc");
            }
            strSql.Append(")AS Row, T.*  from dtulp T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OdbcParameter[] parameters = {
                    new OdbcParameter("@tblName",OdbcType.VarChar, 255),
                    new OdbcParameter("@fldName",OdbcType.VarChar, 255),
                    new OdbcParameter("@PageSize",OdbcType.Int),
                    new OdbcParameter("@PageIndex",OdbcType.Int),
                    new OdbcParameter("@IsReCount",OdbcType.Bit),
                    new OdbcParameter("@OrderType",OdbcType.Bit),
                    new OdbcParameter("@strWhere",OdbcType.VarChar,1000),
                    };
            parameters[0].Value = "dtulp";
            parameters[1].Value = "module";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

