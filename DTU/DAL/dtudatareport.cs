﻿/**  版本信息模板在安装目录下，可自行修改。
* dtudatareport.cs
*
* 功 能： N/A
* 类 名： dtudatareport
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/21 10:52:37   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//Please add references
namespace DTU.DAL
{
	/// <summary>
	/// 数据访问类:dtudatareport
	/// </summary>
	public partial class dtudatareport
	{
		public dtudatareport()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //return DbHelperMySQL.GetMaxID("xmno", "dtudatareport"); 
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string point_name,int xmno,DateTime time)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from dtudatareport");
			strSql.Append(" where point_name=@point_name and xmno=@xmno and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = point_name;
			parameters[1].Value = xmno;
			parameters[2].Value = time;

            return false;//DbHelperMySQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtudatareport model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into dtudatareport(");
			strSql.Append("point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time)");
			strSql.Append(" values (");
			strSql.Append("@point_name,@holedepth,@deep,@predeep,@thisdeep,@acdeep,@rap,@xmno,@remark,@times,@time)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@holedepth", OdbcType.Double),
					new OdbcParameter("@deep", OdbcType.Double),
					new OdbcParameter("@predeep", OdbcType.Double),
					new OdbcParameter("@thisdeep", OdbcType.Int,11),
					new OdbcParameter("@acdeep", OdbcType.Int,11),
					new OdbcParameter("@rap", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@times", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.point_name;
			parameters[1].Value = model.holedepth;
			parameters[2].Value = model.deep;
			parameters[3].Value = model.predeep;
			parameters[4].Value = model.thisdeep;
			parameters[5].Value = model.acdeep;
			parameters[6].Value = model.rap;
			parameters[7].Value = model.xmno;
			parameters[8].Value = model.remark;
			parameters[9].Value = model.times;
			parameters[10].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtudatareport model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update dtudatareport set ");
			strSql.Append("holedepth=@holedepth,");
			strSql.Append("deep=@deep,");
			strSql.Append("predeep=@predeep,");
			strSql.Append("thisdeep=@thisdeep,");
			strSql.Append("acdeep=@acdeep,");
			strSql.Append("rap=@rap,");
			strSql.Append("remark=@remark,");
			strSql.Append("times=@times");
			strSql.Append(" where point_name=@point_name and xmno=@xmno and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@holedepth", OdbcType.Double),
					new OdbcParameter("@deep", OdbcType.Double),
					new OdbcParameter("@predeep", OdbcType.Double),
					new OdbcParameter("@thisdeep", OdbcType.Int,11),
					new OdbcParameter("@acdeep", OdbcType.Int,11),
					new OdbcParameter("@rap", OdbcType.Int,11),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@times", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.holedepth;
			parameters[1].Value = model.deep;
			parameters[2].Value = model.predeep;
			parameters[3].Value = model.thisdeep;
			parameters[4].Value = model.acdeep;
			parameters[5].Value = model.rap;
			parameters[6].Value = model.remark;
			parameters[7].Value = model.times;
			parameters[8].Value = model.point_name;
			parameters[9].Value = model.xmno;
			parameters[10].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string point_name,int xmno,DateTime time)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from dtudatareport ");
			strSql.Append(" where point_name=@point_name and xmno=@xmno and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = point_name;
			parameters[1].Value = xmno;
			parameters[2].Value = time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        //public DTU.Model.dtudatareport GetModel(string point_name,int xmno,DateTime time)
        //{
			
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time from dtudatareport ");
        //    strSql.Append(" where point_name=@point_name and xmno=@xmno and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@point_name", OdbcType.VarChar,100),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@time", OdbcType.DateTime)			};
        //    parameters[0].Value = point_name;
        //    parameters[1].Value = xmno;
        //    parameters[2].Value = time;

        //    DTU.Model.dtudatareport model=new DTU.Model.dtudatareport();
        //    DataSet ds=DbHelperMySQL.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        return DataRowToModel(ds.Tables[0].Rows[0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtudatareport DataRowToModel(DataRow row)
		{
			DTU.Model.dtudatareport model=new DTU.Model.dtudatareport();
			if (row != null)
			{
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
					//model.holedepth=row["holedepth"].ToString();
					//model.deep=row["deep"].ToString();
					//model.predeep=row["predeep"].ToString();
				if(row["thisdeep"]!=null && row["thisdeep"].ToString()!="")
				{
					model.thisdeep=int.Parse(row["thisdeep"].ToString());
				}
				if(row["acdeep"]!=null && row["acdeep"].ToString()!="")
				{
					model.acdeep=int.Parse(row["acdeep"].ToString());
				}
				if(row["rap"]!=null && row["rap"].ToString()!="")
				{
					model.rap=int.Parse(row["rap"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["remark"]!=null)
				{
					model.remark=row["remark"].ToString();
				}
				if(row["times"]!=null && row["times"].ToString()!="")
				{
					model.times=int.Parse(row["times"].ToString());
				}
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
			strSql.Append(" FROM dtudatareport ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}


      



		/// <summary>
		/// 获取记录总数
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM dtudatareport ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.time desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from dtudatareport T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return DbHelperMySQL.Query(strSql.ToString());
        //}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "dtudatareport";
			parameters[1].Value = "time";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperMySQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

