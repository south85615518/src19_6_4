﻿/**  版本信息模板在安装目录下，可自行修改。
* duttaskqueue.cs
*
* 功 能： N/A
* 类 名： duttaskqueue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//Please add references
namespace DTU.DAL
{
	/// <summary>
	/// 数据访问类:duttaskqueue
	/// </summary>
	public partial class duttaskqueue
	{
        public static database db = new database();
		public duttaskqueue()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("xmno", "duttaskqueue"); 
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string commendName,int xmno)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from duttaskqueue");
        //    strSql.Append(" where commendName=@commendName and xmno=@xmno ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@commendName", OdbcType.VarChar,100),
        //            new OdbcParameter("@xmno", OdbcType.Int,11)			};
        //    parameters[0].Value = commendName;
        //    parameters[1].Value = xmno;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtutaskqueue model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into duttaskqueue(");
			strSql.Append("commendName,commendActive,commendNo,commendState,time,xmno,module)");
			strSql.Append(" values (");
			strSql.Append("@commendName,@commendActive,@commendNo,@commendState,@time,@xmno,@module)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@commendName", OdbcType.VarChar,100),
					new OdbcParameter("@commendActive", OdbcType.VarChar,100),
					new OdbcParameter("@commendNo", OdbcType.Int,11),
					new OdbcParameter("@commendState", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@module", OdbcType.VarChar,100)
                                         };
			parameters[0].Value = model.commendName;
			parameters[1].Value = model.commendActive;
			parameters[2].Value = model.commendNo;
			parameters[3].Value = model.commendState;
			parameters[4].Value = model.time;
			parameters[5].Value = model.xmno;
            parameters[6].Value = model.module;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtutaskqueue model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update duttaskqueue set ");
			strSql.Append("commendActive=@commendActive,");
			strSql.Append("commendNo=@commendNo,");
			strSql.Append("commendState=@commendState,");
			strSql.Append("time=@time");
			strSql.Append(" where commendName=@commendName and xmno=@xmno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@commendActive", OdbcType.VarChar,100),
					new OdbcParameter("@commendNo", OdbcType.Int,11),
					new OdbcParameter("@commendState", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@commendName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.commendActive;
			parameters[1].Value = model.commendNo;
			parameters[2].Value = model.commendState;
			parameters[3].Value = model.time;
			parameters[4].Value = model.commendName;
			parameters[5].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string commendName,int xmno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from duttaskqueue ");
			strSql.Append(" where commendName=@commendName and xmno=@xmno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@commendName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
			parameters[0].Value = commendName;
			parameters[1].Value = xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


        public bool DTUQueueTaskSetActived(int xmno,string module,int commendIndex)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from dtutaskqueue  ");
            strSql.Append(" where     commendNo=@commendNo    and    xmno=@xmno    and     module = @module   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@commendNo", OdbcType.Int),
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@module", OdbcType.VarChar,500)};
            parameters[0].Value = commendIndex;
            parameters[1].Value = xmno;
            parameters[2].Value = module;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtutaskqueue GetModel(string commendName,int xmno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select commendName,commendActive,commendNo,commendState,time,xmno from duttaskqueue ");
			strSql.Append(" where commendName=@commendName and xmno=@xmno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@commendName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
			parameters[0].Value = commendName;
			parameters[1].Value = xmno;

			DTU.Model.dtutaskqueue model=new DTU.Model.dtutaskqueue();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}
        public bool GetDTUQueue(int xmno,string module,out List<string> commendList )
        {
            StringBuilder strSql = new StringBuilder(256);
            commendList = new List<string>();
            OdbcConnection  conn = db.GetStanderConn(xmno);
            strSql.AppendFormat("select commendNo from dtutaskqueue where xmno =     {0}    and     module  =    '{1}'  and commendState = 0   ",xmno,module);
            commendList = querysql.querystanderlist(strSql.ToString(),conn);
            return true;
        }




		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtutaskqueue DataRowToModel(DataRow row)
		{
			DTU.Model.dtutaskqueue model=new DTU.Model.dtutaskqueue();
			if (row != null)
			{
				if(row["commendName"]!=null)
				{
					model.commendName=row["commendName"].ToString();
				}
				if(row["commendActive"]!=null)
				{
					model.commendActive=row["commendActive"].ToString();
				}
				if(row["commendNo"]!=null && row["commendNo"].ToString()!="")
				{
					model.commendNo=int.Parse(row["commendNo"].ToString());
				}
				if(row["commendState"]!=null && row["commendState"].ToString()!="")
				{
					model.commendState=int.Parse(row["commendState"].ToString());
				}
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select commendName,commendActive,commendNo,commendState,time,xmno ");
			strSql.Append(" FROM duttaskqueue ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM duttaskqueue ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.xmno desc");
			}
			strSql.Append(")AS Row, T.*  from duttaskqueue T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "duttaskqueue";
			parameters[1].Value = "xmno";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

