﻿/**  版本信息模板在安装目录下，可自行修改。
* dut.cs
*
* 功 能： N/A
* 类 名： dut
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
using Tool;
//Please add references
namespace DTU.DAL
{
    /// <summary>
    /// 数据访问类:dut
    /// </summary>
    public partial class dut
    {
        public static database db = new database();
        public dut()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("module", "dut"); 
        //}

        public bool ExistModuleNo(int xmno, string pointname ,string module)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select count(1)   from    dtu  ");
            strSql.Append(" where     module  =   @module  and point_name <> @pointname  ");
            OdbcParameter[] parameters = {
                        new OdbcParameter("@module", OdbcType.VarChar,200),
                        new OdbcParameter("@pointname", OdbcType.VarChar,200)
                        		};
            parameters[0].Value = module;
            parameters[1].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return Convert.ToInt32(obj) > 0 ? true : false;

        }



        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string point_name, int xmno)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select count(1) from dtu");
            strSql.Append(" where point_name=@point_name  and xmno=@xmno ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@point_name", OdbcType.VarChar,100),
                    new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = point_name;
            parameters[1].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return obj == null ? true : Convert.ToInt32(obj) > 0 ? true : false;
            //return true;

        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(DTU.Model.dtu model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("insert  into dtu(");
            strSql.Append("point_name,module,xmno,firstAlarm,secalarm,thirdalarm,remark,pointtype,line,holedepth)");
            strSql.Append(" values (");
            strSql.Append("@point_name,@module,@xmno,@firstAlarm,@secalarm,@thirdalarm,@remark,@pointtype,@line,@holedepth)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@module", OdbcType.VarChar,500),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@firstAlarm", OdbcType.VarChar,50),
					new OdbcParameter("@secalarm", OdbcType.VarChar,50),
					new OdbcParameter("@thirdalarm", OdbcType.VarChar,50),
					new OdbcParameter("@remark", OdbcType.VarChar,100),
                    new OdbcParameter("@pointtype", OdbcType.VarChar,100),
                    new OdbcParameter("@line", OdbcType.VarChar,100),
                    new OdbcParameter("@holedepth", OdbcType.VarChar,100)
                                         };
            parameters[0].Value = model.point_name;
            parameters[1].Value = model.module;
            parameters[2].Value = model.xmno;
            parameters[3].Value = model.firstAlarm;
            parameters[4].Value = model.secalarm;
            parameters[5].Value = model.thirdalarm;
            parameters[6].Value = model.remark;
            parameters[7].Value = model.pointtype;
            parameters[8].Value = model.line;
            parameters[9].Value = model.holedepth;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 加载DTU点名
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="ls"></param>
        /// <returns></returns>
        public bool DTUPointLoadDAL(int xmno, out List<string> ls)
        {

            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select DISTINCT(POINT_NAME) from dtu where xmno=" + xmno;
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTU.Model.dtu model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("update dtu set ");
            strSql.Append("firstAlarm=@firstAlarm,");
            strSql.Append("secalarm=@secalarm,");
            strSql.Append("thirdalarm=@thirdalarm,");
            strSql.Append("remark=@remark,");
            strSql.Append("pointtype=@pointtype,");
            strSql.Append("line=@line,");
            strSql.Append("module=@module,");
            strSql.Append("holedepth=@holedepth");
            strSql.Append("     where      id=@id     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@firstAlarm", OdbcType.VarChar,50),
					new OdbcParameter("@secalarm", OdbcType.VarChar,50),
					new OdbcParameter("@thirdalarm", OdbcType.VarChar,50),
					new OdbcParameter("@remark", OdbcType.VarChar,100),
                    new OdbcParameter("@pointtype", OdbcType.VarChar,100),
                    new OdbcParameter("@line", OdbcType.Double),
                    new OdbcParameter("@module", OdbcType.VarChar,500),
                    new OdbcParameter("@holedepth", OdbcType.VarChar,500),
					new OdbcParameter("@id", OdbcType.Int)};
            parameters[0].Value = model.firstAlarm;
            parameters[1].Value = model.secalarm;
            parameters[2].Value = model.thirdalarm;
            parameters[3].Value = model.remark;
            parameters[4].Value = model.pointtype;
            parameters[5].Value = model.line;
            parameters[6].Value = model.module;
            parameters[7].Value = model.holedepth;
            parameters[8].Value = model.id;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, DTU.Model.dtu model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update dtu set ");
            strSql.Append("FirstAlarm=@FirstAlarm,");
            strSql.Append("SecAlarm=@SecAlarm,");
            strSql.Append("ThirdAlarm=@ThirdAlarm,");
            strSql.Append(" remark=@remark ,");
            strSql.Append("pointtype=@pointtype");
            strSql.Append("   where   ");
            strSql.Append(" point_name  in ('" + pointNameStr + "')   and xmno = @xmno ");
            OdbcParameter[] parameters = {

					new OdbcParameter("@FirstAlarm", OdbcType.VarChar,120),
					new OdbcParameter("@SecAlarm", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarm", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@pointtype",OdbcType.VarChar,120),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };



            parameters[0].Value = model.firstAlarm;
            parameters[1].Value = model.secalarm;
            parameters[2].Value = model.thirdalarm;
            parameters[3].Value = model.remark;
            parameters[4].Value = model.pointtype;
            parameters[5].Value = model.xmno;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 从DTU的端口设置获取点名和模块号
        /// </summary>
        /// <returns></returns>
        public bool DTUTOPointModule(int xmno, int port, string od, string addressno, out DTU.Model.dtu model)
        {
            StringBuilder strSql = new StringBuilder();
            model = new Model.dtu();
            
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            
            strSql.Append(" select  dtu.* from  dtu,dtumodule  where   dtu.module = dtumodule.id   and   dtumodule.xmno = @xmno     and     dtumodule.port = @port   and   dtumodule.addressno = @addressno  and  dtumodule.od = @od   ");
            //strSql.Append(" select  dtu.* from  dtu,dtumodule  where   dtu.module = dtumodule.id   and   dtumodule.xmno = @xmno    and    dtumodule.addressno = @addressno  and  dtumodule.od = @od   ");
            strSql.Append("   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    //NGN对接代码修改
                    new OdbcParameter("@port", OdbcType.Int),
                    new OdbcParameter("@addressno", OdbcType.VarChar,500),
                    new OdbcParameter("@od", OdbcType.VarChar,100)
                                         };


            
            
            parameters[0].Value = xmno;
            parameters[1].Value = port;
            parameters[2].Value = addressno;
            parameters[3].Value = od;
             
            //parameters[0].Value = xmno;
            //parameters[1].Value = addressno;
            //parameters[2].Value = od;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count == 0)
                return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;

        }

        /// <summary>
        /// 从DTU的端口设置获取点名和模块号
        /// </summary>
        /// <returns></returns>
        public bool NGNTOPointModule(int xmno, int port,  string addressno, out DTU.Model.dtu model)
        {
            StringBuilder strSql = new StringBuilder();
            model = new Model.dtu();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append(" select  dtu.* from  dtu,dtumodule  where   dtu.module = dtumodule.id      and    dtumodule.addressno = @addressno  and dtumodule.port = @port  ");
            strSql.Append("   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@addressno", OdbcType.VarChar,500),
                    new OdbcParameter("@port", OdbcType.Int)
                                         };

            parameters[0].Value = addressno;
            parameters[1].Value = port;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count == 0)
                return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;

        }






        /// <summary>
        /// 从DTU的端口设置获取点名和模块号
        /// </summary>
        /// <returns></returns>
        public bool DTUTOPointModule(int xmno, int port,  string addressno, out DTU.Model.dtu model)
        {
            StringBuilder strSql = new StringBuilder();
            model = new Model.dtu();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append(" select  dtu.* from  dtu,dtumodule  where   dtu.module = dtumodule.id   and   dtumodule.xmno = @xmno     and     dtumodule.port = @port   and   dtumodule.addressno = @addressno   ");
            strSql.Append("   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@port", OdbcType.Int),
                    new OdbcParameter("@addressno", OdbcType.VarChar,500)
                                         };



            parameters[0].Value = xmno;
            parameters[1].Value = port;
            parameters[2].Value = addressno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count == 0)
                return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;

        }

       


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string point_name, string module, int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from dtu ");
            strSql.Append("      where     point_name=@point_name     and     module=@module     and     xmno=@xmno    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@module", OdbcType.VarChar,500),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = point_name;
            parameters[1].Value = module;
            parameters[2].Value = xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtu GetModel(string point_name, int module, int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select point_name,module,port,xmno,firstAlarm,secalarm,thirdalarm,remark from dut ");
            strSql.Append(" where point_name=@point_name and module=@module and xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@module", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = point_name;
            parameters[1].Value = module;
            parameters[2].Value = xmno;

            DTU.Model.dtu model = new DTU.Model.dtu();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }
        public bool DTUList(int startPageIndex, int pageSize, int xmno,string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select dtu.point_name,if((select IFNULL(max(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from dtudata where dtudata.point_name = dtu.point_name  and  dtudata.xmno = dtu.xmno ) < DATE_ADD(SYSDATE(),INTERVAL -1 day),'false','true' ) as state,if(dtulp.LP=0||ISNULL(dtulp.LP),'false','true') as lp,CONCAT( IFNULL(dtutimetask.hour,0),'时',IFNULL(dtutimetask.minute,0),'分',IFNULL(dtutimetask.times,0),'次' ) as timeinterval ,(select max(time) from dtudata where dtudata.point_name = dtu.point_name  and dtudata.xmno = dtu.xmno  ) as lastdatatime,dtumodule.addressno,dtumodule.od,dtumodule.port,'" + xmname + "' as xmname,dtu.remark,dtusenor.senorno,dtumodule.id  from dtu  left join dtumodule  on dtu.module = dtumodule.id left join dtusenor  on dtumodule.senorno = dtusenor.senorno left join dtutimetask on dtumodule.addressno = dtutimetask.module left join dtulp on dtumodule.addressno = dtulp.module    where dtu.xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            Tool.ExceptionLog.ExceptionWrite(sql);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public bool TableDTUCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(*) from dtu where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
            
        }
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select id,point_name,module,xmno,firstAlarm,secalarm,thirdalarm,remark,pointtype,line,holedepth from dtu where xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            Tool.ExceptionLog.ExceptionWrite(sql);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
       
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(*) from dtu where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
           
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, out DTU.Model.dtu model)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select point_name,module,xmno,firstAlarm,secalarm,thirdalarm,remark,holedepth,line from dtu where       xmno=@xmno   and   point_name=@point_name   ");

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,10),
                    new OdbcParameter("@point_name", OdbcType.VarChar,200)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            model = new DTU.Model.dtu();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtu DataRowToModel(DataRow row)
        {
            DTU.Model.dtu model = new DTU.Model.dtu();
            if (row != null)
            {
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                if (row["line"] != null)
                {
                    model.line = Convert.ToDouble(row["line"].ToString());
                }
                if (row["holedepth"] != null)
                {
                    model.holedepth = Convert.ToDouble(row["holedepth"].ToString());
                }
                if (row["module"] != null && row["module"].ToString() != "")
                {
                    model.module = row["module"].ToString();
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["firstAlarm"] != null)
                {
                    model.firstAlarm = row["firstAlarm"].ToString();
                }
                if (row["secalarm"] != null)
                {
                    model.secalarm = row["secalarm"].ToString();
                }
                if (row["thirdalarm"] != null)
                {
                    model.thirdalarm = row["thirdalarm"].ToString();
                }
                if (row["remark"] != null)
                {
                    model.remark = row["remark"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select point_name,module,port,xmno,firstAlarm,secalarm,thirdalarm,remark ");
            strSql.Append(" FROM dut ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM dut ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.xmno desc");
            }
            strSql.Append(")AS Row, T.*  from dut T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        



        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OdbcParameter[] parameters = {
                    new OdbcParameter("@tblName", OdbcType.VarChar, 255),
                    new OdbcParameter("@fldName", OdbcType.VarChar, 255),
                    new OdbcParameter("@PageSize", OdbcType.Int),
                    new OdbcParameter("@PageIndex", OdbcType.Int),
                    new OdbcParameter("@IsReCount", OdbcType.Bit),
                    new OdbcParameter("@OrderType", OdbcType.Bit),
                    new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
                    };
            parameters[0].Value = "dut";
            parameters[1].Value = "xmno";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

