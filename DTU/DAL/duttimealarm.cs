﻿/**  版本信息模板在安装目录下，可自行修改。
* dtutimealarm.cs
*
* 功 能： N/A
* 类 名： dtutimealarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//Please add references
namespace DTU.DAL
{
    /// <summary>
    /// 数据访问类:dtutimealarm
    /// </summary>
    public partial class dtutimealarm
    {
        public static database db = new database();
        public dtutimealarm()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("hour", "dtutimealarm"); 
        //}

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string module, int xmno)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select count(1) from dtutimealarm");
            strSql.Append(" where module=@module  and xmno=@xmno ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@module", OdbcType.VarChar,100),
                    new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = module;
            parameters[1].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return obj == null ? true : Convert.ToInt32(obj) > 0 ? true : false;
            //return true;

        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(DTU.Model.dtutimealarm model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into dtutimealarm(");
            strSql.Append("hour,minute,sec,times,module,time,xmno)");
            strSql.Append(" values (");
            strSql.Append("@hour,@minute,@sec,@times,@module,@time,@xmno)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@minute", OdbcType.Int,11),
					new OdbcParameter("@sec", OdbcType.Int,11),
					new OdbcParameter("@times", OdbcType.Int,11),
					new OdbcParameter("@module", OdbcType.VarChar,200),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
            parameters[0].Value = model.hour;
            parameters[1].Value = model.minute;
            parameters[2].Value = model.sec;
            parameters[3].Value = model.times;
            parameters[4].Value = model.module;
            parameters[5].Value = model.time;
            parameters[6].Value = model.xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTU.Model.dtutimealarm model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("update dtutimealarm set ");
            strSql.Append("hour=@hour,");
            strSql.Append("minute=@minute,");
            strSql.Append("sec=@sec,");
            strSql.Append("times=@times,");
            strSql.Append("time=@time");
            strSql.Append("     where      module=@module       and xmno=@xmno    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@minute", OdbcType.Int,11),
					new OdbcParameter("@sec", OdbcType.Int,11),
					new OdbcParameter("@times", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime),
                    new OdbcParameter("@module", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)
                                         };
            parameters[0].Value = model.hour;
            parameters[1].Value = model.minute;
            parameters[2].Value = model.sec;
            parameters[3].Value = model.times;
            parameters[4].Value = model.time;
            parameters[5].Value = model.module;
            parameters[6].Value = model.xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string module, int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from dtutimealarm ");
            strSql.Append(" where  module=@module  and  xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@module", OdbcType.VarChar,500),
					new OdbcParameter("@xmno", OdbcType.Int,11)
                                         };

            parameters[0].Value = module;
            parameters[1].Value = xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool GetModel(int xmno, string module, out DTU.Model.dtutimealarm model)
        {
            StringBuilder strSql = new StringBuilder(256);
            model = new Model.dtutimealarm();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select * from dtutimealarm where xmno =     @xmno    and     module  =    @module   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@module", OdbcType.VarChar,500)

                                         };

            parameters[0].Value = xmno;
            parameters[1].Value = module;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count == 0) return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtutimealarm GetModel(int hour, int minute, int sec, int times, string module, DateTime time, int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select hour,minute,sec,times,module,time,module,xmno from dtutimealarm ");
            strSql.Append(" where hour=@hour and minute=@minute and sec=@sec and times=@times and module=@module and time=@time and module=@module and xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@minute", OdbcType.Int,11),
					new OdbcParameter("@sec", OdbcType.Int,11),
					new OdbcParameter("@times", OdbcType.Int,11),
					new OdbcParameter("@module", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@module", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = hour;
            parameters[1].Value = minute;
            parameters[2].Value = sec;
            parameters[3].Value = times;
            parameters[4].Value = module;
            parameters[5].Value = time;
            parameters[6].Value = module;
            parameters[7].Value = xmno;

            DTU.Model.dtutimealarm model = new DTU.Model.dtutimealarm();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtutimealarm DataRowToModel(DataRow row)
        {
            DTU.Model.dtutimealarm model = new DTU.Model.dtutimealarm();
            if (row != null)
            {
                if (row["hour"] != null && row["hour"].ToString() != "")
                {
                    model.hour = int.Parse(row["hour"].ToString());
                }
                if (row["minute"] != null && row["minute"].ToString() != "")
                {
                    model.minute = int.Parse(row["minute"].ToString());
                }
                if (row["sec"] != null && row["sec"].ToString() != "")
                {
                    model.sec = int.Parse(row["sec"].ToString());
                }
                if (row["times"] != null && row["times"].ToString() != "")
                {
                    model.times = int.Parse(row["times"].ToString());
                }
                if (row["module"] != null)
                {
                    model.module = row["module"].ToString();
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }

                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select hour,minute,sec,times,module,time,module,xmno ");
            strSql.Append(" FROM dtutimealarm ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select module,times,time,xmno,hour,minute,sec from dtutimealarm where xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(1) from dtutimealarm where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
        }



        /// <summary>
        /// 获取记录总数
        /// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM dtutimealarm ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.xmno desc");
            }
            strSql.Append(")AS Row, T.*  from dtutimealarm T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return OdbcSQLHelper.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OdbcParameter[] parameters = {
                    new OdbcParameter("@tblName", OdbcType.VarChar, 255),
                    new OdbcParameter("@fldName", OdbcType.VarChar, 255),
                    new OdbcParameter("@PageSize", OdbcType.Int),
                    new OdbcParameter("@PageIndex", OdbcType.Int),
                    new OdbcParameter("@IsReCount", OdbcType.Bit),
                    new OdbcParameter("@OrderType", OdbcType.Bit),
                    new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
                    };
            parameters[0].Value = "dtutimealarm";
            parameters[1].Value = "xmno";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

