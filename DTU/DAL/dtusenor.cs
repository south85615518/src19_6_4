﻿/**  版本信息模板在安装目录下，可自行修改。
* dtusenor.cs
*
* 功 能： N/A
* 类 名： dtusenor
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/13 14:08:57   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace DTU.DAL
{
	/// <summary>
	/// 数据访问类:dtusenor
	/// </summary>
	public partial class dtusenor
	{
		public dtusenor()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string senorno)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from dtusenor");
        //    strSql.Append(" where senorno=@senorno ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@senorno", OdbcType.VarChar,100)			};
        //    parameters[0].Value = senorno;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}

        public static database db = new database();
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtusenor model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("insert into dtusenor(");
			strSql.Append("senorno,senortype,remark,xmno,senorname,time,aboundtime)");
			strSql.Append(" values (");
			strSql.Append("@senorno,@senortype,@remark,@xmno,@senorname,@time,@aboundtime)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@senorno", OdbcType.VarChar,100),
					new OdbcParameter("@senortype", OdbcType.VarChar,500),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@senorname", OdbcType.VarChar,500),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@aboundtime", OdbcType.Int,11)};
			parameters[0].Value = model.senorno;
			parameters[1].Value = model.senortype;
			parameters[2].Value = model.remark;
			parameters[3].Value = model.xmno;
			parameters[4].Value = model.senorname;
			parameters[5].Value = model.time;
			parameters[6].Value = model.aboundtime;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtusenor model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("update dtusenor set ");
			strSql.Append("senortype=@senortype,");
			strSql.Append("remark=@remark,");
			strSql.Append("xmno=@xmno,");
			strSql.Append("senorname=@senorname,");
			strSql.Append("time=@time,");
			strSql.Append("aboundtime=@aboundtime");
			strSql.Append("     where     senorno=@senorno   ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@senortype", OdbcType.VarChar,500),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@senorname", OdbcType.VarChar,500),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@aboundtime", OdbcType.Int,11),
					new OdbcParameter("@senorno", OdbcType.VarChar,100)};
			parameters[0].Value = model.senortype;
			parameters[1].Value = model.remark;
			parameters[2].Value = model.xmno;
			parameters[3].Value = model.senorname;
			parameters[4].Value = model.time;
			parameters[5].Value = model.aboundtime;
			parameters[6].Value = model.senorno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


        /// <summary>
        /// 获取传感器编号
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool SenorNoGet(int xmno, out string senornoStr)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select distinct(senorno) from dtusenor where xmno = " + xmno + "   ";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            senornoStr = string.Join(";", lsFormat);
            return true;
        }



		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string senorno)
		{
			
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("delete from dtusenor ");
			strSql.Append(" where senorno=@senorno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@senorno", OdbcType.VarChar,100)			};
			parameters[0].Value = senorno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
        //public bool DeleteList(string senornolist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from dtusenor ");
        //    strSql.Append(" where senorno in ("+senornolist + ")  ");
        //    int rows=OdbcSQLHelper.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtusenor GetModel(string senorno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select senorno,senortype,remark,xmno,senorname,time,aboundtime from dtusenor ");
			strSql.Append(" where senorno=@senorno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@senorno", OdbcType.VarChar,100)			};
			parameters[0].Value = senorno;

			DTU.Model.dtusenor model=new DTU.Model.dtusenor();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}
        
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select senorno,senortype,remark,xmno,senorname,time,aboundtime from dtusenor where xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order,xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public bool TableRowsCount( string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(*) from dtusenor where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
        }




		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtusenor DataRowToModel(DataRow row)
		{
			DTU.Model.dtusenor model=new DTU.Model.dtusenor();
			if (row != null)
			{
				if(row["senorno"]!=null)
				{
					model.senorno=row["senorno"].ToString();
				}
				if(row["senortype"]!=null)
				{
					model.senortype=row["senortype"].ToString();
				}
				if(row["remark"]!=null)
				{
					model.remark=row["remark"].ToString();
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["senorname"]!=null)
				{
					model.senorname=row["senorname"].ToString();
				}
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
				if(row["aboundtime"]!=null && row["aboundtime"].ToString()!="")
				{
					model.aboundtime=int.Parse(row["aboundtime"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select senorno,senortype,remark,xmno,senorname,time,aboundtime ");
			strSql.Append(" FROM dtusenor ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM dtusenor ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.senorno desc");
			}
			strSql.Append(")AS Row, T.*  from dtusenor T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "dtusenor";
			parameters[1].Value = "senorno";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

