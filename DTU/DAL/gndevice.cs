﻿/**  版本信息模板在安装目录下，可自行修改。
* dut.cs
*
* 功 能： N/A
* 类 名： dut
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
//Please add references
namespace DTU.DAL
{
    /// <summary>
    /// 数据访问类:dut
    /// </summary>
    public partial class dut
    {
        /// <summary>
        /// 从DTU的端口设置获取点名和模块号
        /// </summary>
        /// <returns></returns>
        public bool DeviceNoTOPointModule(int xmno, int port, string addressno, out DTU.Model.dtu model)
        {

            StringBuilder strSql = new StringBuilder();
            model = new DTU.Model.dtu();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append(" select  dtu.* from  dtu,dtumodule  where   dtu.module = dtumodule.id   and   dtumodule.xmno = @xmno     and     dtumodule.port = @port   and   dtumodule.addressno = @addressno   ");
            strSql.Append("   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@port", OdbcType.Int),
                    new OdbcParameter("@addressno", OdbcType.VarChar,500)
                                         };



            parameters[0].Value = xmno;
            parameters[1].Value = port;
            parameters[2].Value = addressno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count == 0)
                return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;

        }
    }
}

