﻿/**  版本信息模板在安装目录下，可自行修改。
* dutdata.cs
*
* 功 能： N/A
* 类 名： dutdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//Please add references
namespace DTU.DAL
{
	/// <summary>
	/// 数据访问类:dutdata
	/// </summary>
	public partial class dutdata
	{
        public static database db = new database();
        
		public dutdata()
		{}
		#region  测量数据

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("mkh", "dutdata"); 
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string point_name,int mkh,int xmno,DateTime time)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from dutdata");
        //    strSql.Append(" where point_name=@point_name and mkh=@mkh and xmno=@xmno and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@point_name", OdbcType.VarChar,100),
        //            new OdbcParameter("@mkh", OdbcType.Int,11),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@time", OdbcType.DateTime)			};
        //    parameters[0].Value = point_name;
        //    parameters[1].Value = mkh;
        //    parameters[2].Value = xmno;
        //    parameters[3].Value = time;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtudata model)
		{
			StringBuilder strSql=new StringBuilder();
            SingleTonOdbcSQLHelper sqlHelper = new SingleTonOdbcSQLHelper();
            sqlHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("insert ignore into dtudata(");
			strSql.Append("point_name,mkh,deep,groundElevation,xmno,time,dtutime)");
			strSql.Append(" values (");
			strSql.Append("@point_name,@mkh,@deep,@groundElevation,@xmno,@time,@dtutime)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@mkh", OdbcType.Int,11),
					new OdbcParameter("@deep", OdbcType.Double),
					new OdbcParameter("@groundElevation", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime),
                    new OdbcParameter("@dtutime", OdbcType.DateTime)
                                         };
			parameters[0].Value = model.point_name;
			parameters[1].Value = model.mkh;
			parameters[2].Value = model.deep;
			parameters[3].Value = model.groundElevation;
			parameters[4].Value = model.xmno;
			parameters[5].Value = model.time;
            parameters[6].Value = model.dtutime;
            int rows = sqlHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtudata model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update dutdata set ");
			strSql.Append("deep=@deep,");
			strSql.Append("groundElevation=@groundElevation");
			strSql.Append(" where point_name=@point_name and mkh=@mkh and xmno=@xmno and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@deep", OdbcType.Double),
					new OdbcParameter("@groundElevation", OdbcType.Double),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@mkh", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.deep;
			parameters[1].Value = model.groundElevation;
			parameters[2].Value = model.point_name;
			parameters[3].Value = model.mkh;
			parameters[4].Value = model.xmno;
			parameters[5].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false; 
			}
		}

        /// <summary>
        /// 更新温度数据
        /// </summary>
        public bool UpdateDegree(DTU.Model.dtudata model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update dtudata set ");
            strSql.Append("   degree=@degree  ");
            strSql.Append(" where    point_name=@point_name     and       mkh=@mkh      and       xmno=@xmno     and      time=@time    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@degree", OdbcType.Double),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@mkh", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)};
            parameters[0].Value = model.degree;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.mkh;
            parameters[3].Value = model.xmno;
            parameters[4].Value = model.time;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //public bool previousDTUtime(int xmno,string )
        //{

        //}




        //public DateTime DTUTimeInterval(DateTime DTUtime,string xmno,string point_name)
        //{

        //}



		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string point_name,int mkh,int xmno,DateTime time)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from dutdata ");
			strSql.Append(" where point_name=@point_name and mkh=@mkh and xmno=@xmno and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@mkh", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = point_name;
			parameters[1].Value = mkh;
			parameters[2].Value = xmno;
			parameters[3].Value = time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        public bool DeleteTmp(int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            SingleTonOdbcSQLHelper sqlHelper = new SingleTonOdbcSQLHelper();
            sqlHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from dtudata_tmp ");
            strSql.Append(" where xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int)
							};
            parameters[0].Value = xmno;


            int rows = sqlHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from dtudata  where   xmno=@xmno   and point_name = @point_name  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }
        public bool ResultDataReportPrint(string sql, int xmno, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            dt = querysql.querystanderdb(sql, conn);
            return true;
        }

       
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtudata GetModel(string point_name,int mkh,int xmno,DateTime time)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select point_name,mkh,deep,groundElevation,xmno,id,time from dutdata ");
			strSql.Append(" where point_name=@point_name and mkh=@mkh and xmno=@xmno and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@mkh", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = point_name;
			parameters[1].Value = mkh;
			parameters[2].Value = xmno;
			parameters[3].Value = time;

			DTU.Model.dtudata model=new DTU.Model.dtudata();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

        public bool DTUDATATempDel(int xmno)
        {
            StringBuilder strSql = new StringBuilder(256);
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from dtudata_tmp where xmno = @xmno");
            OdbcParameter[] param =  {
                 new OdbcParameter("@xmno",OdbcType.Int,11)
            };
            param[0].Value = xmno;
            int row = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),param);
            if (row > 0)
            {
                return true;
            }
            return false;
        }

       



		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtudata DataRowToModel(DataRow row)
		{
			DTU.Model.dtudata model=new DTU.Model.dtudata();
			if (row != null)
			{
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["mkh"]!=null && row["mkh"].ToString()!="")
				{
					model.mkh=int.Parse(row["mkh"].ToString());
				}
					//model.deep=row["deep"].ToString();
					//model.groundElevation=row["groundElevation"].ToString();
                if (row["deep"] != null && row["deep"].ToString() != "")
                {
                    model.deep = Convert.ToDouble(Convert.ToDouble(row["deep"]).ToString("0.000"));
                }
                if (row["groundElevation"] != null && row["groundElevation"].ToString() != "")
                {
                    model.groundElevation = Convert.ToDouble(Convert.ToDouble(row["groundElevation"]).ToString("0.000"));
                }
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
			}
			return model;
		}
        
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select point_name,mkh,deep,groundElevation,xmno,id,time ");
			strSql.Append(" FROM dutdata ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        //public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt)
        //{
        //    OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select  max(time)  from dtudata  where   xmno=@xmno   and point_name = @point_name  ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@xmno", OdbcType.Int),
        //            new OdbcParameter("@point_name", OdbcType.VarChar,120)
        //                    };
        //    parameters[0].Value = xmno;
        //    parameters[1].Value = pointname;
        //    object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
        //    if (obj == null) { dt = new DateTime(); return false; }
        //    dt = Convert.ToDateTime(obj); return true;

        //}

        public bool MaxTime(int xmno, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from dtudata  where   xmno = @xmno ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int)
                

            };
            paramters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }


        public bool MaxTime(int xmno, string point_name,out DateTime maxTime)
        {
            maxTime = new DateTime();
            SingleTonOdbcSQLHelper sqlhelper = new SingleTonOdbcSQLHelper();
            sqlhelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from dtudata  where     xmno = @xmno    and  point_name=@point_name");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int),
                new OdbcParameter("@point_name",OdbcType.VarChar,200)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = point_name;
            object obj = sqlhelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }


		/// <summary>
		/// 获取记录总数
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM dutdata ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.time desc");
			}
			strSql.Append(")AS Row, T.*  from dutdata T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        public bool GetdtudataList(int xmno,string point_name,DateTime starttime,DateTime endtime, out List<DTU.Model.dtudata> li)
        {
            li = new List<Model.dtudata>();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select point_name,mkh,deep,groundElevation,xmno,time  ");
            strSql.Append(" FROM dtudata where  xmno = @xmno       and    time   between    @starttime  and   @endtime  ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    //new OdbcParameter("@point_name", OdbcType.VarChar,200),
                    new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime)
                    
                                         };

            parameters[0].Value = xmno;
            //parameters[1].Value = point_name;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;

            while (i < ds.Tables[0].Rows.Count)
            {
                li.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

       
#endregion 
        #region 报表数据
        public bool GetModel(int xmno, string pointname, DateTime dt, out DTU.Model.dtudatareport model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
            strSql.Append(" FROM   dtudata   where xmno =   @xmno    and     point_name =  @point_name    and   time = @time   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToReportModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }
         /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(int xmno, out List<DTU.Model.dtudatareport> li)
        {
            li = new List<Model.dtudatareport>();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
            strSql.Append(" FROM dtudata_tmp where xmno = @xmno ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@xmno", OdbcType.Int,11)};

            parameters[0].Value = xmno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;

            while (i < ds.Tables[0].Rows.Count)
            {
                li.Add(DataRowToReportModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }
        public bool PointNameCycListGet(int xmno, string pointname, out List<string> ls)
        {

            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(times),time  from  dtudatareport    where         xmno=@xmno     and      {0}  order by times  asc  ", pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int)
							};
            parameters[0].Value = xmno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("第{0}次[{1}]", ds.Tables[0].Rows[i].ItemArray[0].ToString(), ds.Tables[0].Rows[i].ItemArray[1].ToString()));
                i++;
            }
            return true;

        }
        public bool ResultDataReportPrint(string sql, string xmname, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            dt = querysql.querystanderdb(sql, conn);
            return true;
        }


        public bool DTUDATATableLoad(DateTime starttime, DateTime endtime, int startPageIndex, int pageSize, int xmno, string pointname, string sord, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time  from  dtudatareport  where      xmno=@xmno   and    {0}    and     time    between    @starttime      and    @endtime      order by {1}  asc     limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@starttime", OdbcType.DateTime),
					new OdbcParameter("@endtime", OdbcType.DateTime),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool DTUTableRowsCount(DateTime starttime, DateTime endtime, int xmno, string pointname, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from dtudatareport where xmno='{2}' and {3}  and time between '{0}' and '{1}'", starttime, endtime, xmno, searchstr);
            OdbcConnection conn = db.GetStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtudatareport DataRowToReportModel(DataRow row)
        {
            DTU.Model.dtudatareport model = new DTU.Model.dtudatareport();
            if (row != null)
            {
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                //model.holedepth=row["holedepth"].ToString();
                //model.deep=row["deep"].ToString();
                //model.predeep=row["predeep"].ToString();
                if (row["thisdeep"] != null && row["thisdeep"].ToString() != "")
                {
                    model.thisdeep = int.Parse(row["thisdeep"].ToString());
                }
                if (row["acdeep"] != null && row["acdeep"].ToString() != "")
                {
                    model.acdeep = int.Parse(row["acdeep"].ToString());
                }
                if (row["rap"] != null && row["rap"].ToString() != "")
                {
                    model.rap = int.Parse(row["rap"].ToString());
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["remark"] != null)
                {
                    model.remark = row["remark"].ToString();
                }
                if (row["times"] != null && row["times"].ToString() != "")
                {
                    model.times = int.Parse(row["times"].ToString());
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
            }
            return model;
        }
        #endregion

        /*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "dutdata";
			parameters[1].Value = "time";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

