﻿/**  版本信息模板在安装目录下，可自行修改。
* duttaskqueue.cs
*
* 功 能： N/A
* 类 名： duttaskqueue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
	/// <summary>
	/// duttaskqueue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class dtutaskqueue
	{
		public dtutaskqueue()
		{}
		#region Model
		private string _commendname;
		private string _commendactive;
		private int _commendno;
		private int _commendstate;
		private DateTime _time;
		private int _xmno=0;
        private string _module = "";

        public string module
        {
            get { return _module; }
            set { _module = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string commendName
		{
			set{ _commendname=value;}
			get{return _commendname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string commendActive
		{
			set{ _commendactive=value;}
			get{return _commendactive;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int commendNo
		{
			set{ _commendno=value;}
			get{return _commendno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int commendState
		{
			set{ _commendstate=value;}
			get{return _commendstate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

