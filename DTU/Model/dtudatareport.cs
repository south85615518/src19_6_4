﻿/**  版本信息模板在安装目录下，可自行修改。
* dtudatareport.cs
*
* 功 能： N/A
* 类 名： dtudatareport
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/21 10:52:37   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
	/// <summary>
	/// dtudatareport:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class dtudatareport
	{
		public dtudatareport()
		{}
		#region Model
		private string _point_name;
		private double _holedepth;
		private double _deep;
		private double _predeep;
		private int _thisdeep;
		private int _acdeep;
		private int _rap;
		private int _xmno=0;
		private string _remark;
		private int _times;
		private DateTime _time= new DateTime();
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double holedepth
		{
			set{ _holedepth=value;}
			get{return _holedepth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double deep
		{
			set{ _deep=value;}
			get{return _deep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double predeep
		{
			set{ _predeep=value;}
			get{return _predeep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int thisdeep
		{
			set{ _thisdeep=value;}
			get{return _thisdeep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int acdeep
		{
			set{ _acdeep=value;}
			get{return _acdeep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int rap
		{
			set{ _rap=value;}
			get{return _rap;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int times
		{
			set{ _times=value;}
			get{return _times;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		#endregion Model

	}
}

