﻿/**  版本信息模板在安装目录下，可自行修改。
* dtusenor.cs
*
* 功 能： N/A
* 类 名： dtusenor
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/13 14:08:56   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
	/// <summary>
	/// dtusenor:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class dtusenor
	{
		public dtusenor()
		{}
		#region Model
		private string _senorno;
		private string _senortype;
		private string _remark;
		private int _xmno;
		private string _senorname;
		private DateTime _time;
		private int _aboundtime;
		/// <summary>
		/// 
		/// </summary>
		public string senorno
		{
			set{ _senorno=value;}
			get{return _senorno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string senortype
		{
			set{ _senortype=value;}
			get{return _senortype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string senorname
		{
			set{ _senorname=value;}
			get{return _senorname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int aboundtime
		{
			set{ _aboundtime=value;}
			get{return _aboundtime;}
		}
		#endregion Model

	}
}

