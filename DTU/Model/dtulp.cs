﻿/**  版本信息模板在安装目录下，可自行修改。
* dtulp.cs
*
* 功 能： N/A
* 类 名： dtulp
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/25 16:16:12   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
    /// <summary>
    /// dtulp:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class dtulp
    {
        public dtulp()
        { }
        #region Model
        private string _module;
        private int _lp;
        private int _xmno;
        private bool _applicated;

        public bool applicated
        {
            get { return _applicated; }
            set { _applicated = value; }
        }
        public int xmno
        {
            set { _xmno = value; }
            get { return _xmno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string module
        {
            set { _module = value; }
            get { return _module; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int LP
        {
            set { _lp = value; }
            get { return _lp; }
        }
        #endregion Model

    }
}

