﻿/**  版本信息模板在安装目录下，可自行修改。
* dut.cs
*
* 功 能： N/A
* 类 名： dut
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
    /// <summary>
    /// dut:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class dtu
    {
        public dtu()
        { }
        #region Model
        private int _id = 0;
        private string _point_name;
        private string _module = "";
        private int _xmno = 0;
        private string _firstalarm;
        private string _secalarm;
        private string _thirdalarm;
        private string _remark;
        private string _pointtype;
        private double _line;
        private double _holedepth;

        public double holedepth
        {
            get { return _holedepth; }
            set { _holedepth = value; }
        }
        public double line
        {
            get { return _line; }
            set { _line = value; }
        }
        public string pointtype
        {
            get { return _pointtype; }
            set { _pointtype = value; }
        }
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string point_name
        {
            set { _point_name = value; }
            get { return _point_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string module
        {
            set { _module = value; }
            get { return _module; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int xmno
        {
            set { _xmno = value; }
            get { return _xmno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string firstAlarm
        {
            set { _firstalarm = value; }
            get { return _firstalarm; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string secalarm
        {
            set { _secalarm = value; }
            get { return _secalarm; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string thirdalarm
        {
            set { _thirdalarm = value; }
            get { return _thirdalarm; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string remark
        {
            set { _remark = value; }
            get { return _remark; }
        }
        #endregion Model

    }
}

