﻿/**  版本信息模板在安装目录下，可自行修改。
* dtutimetask.cs
*
* 功 能： N/A
* 类 名： dtutimetask
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
	/// <summary>
	/// dtutimetask:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class dtutimetask
	{
		public dtutimetask()
		{}
		#region Model
		private int _hour=0;
		private int _minute=0;
		private int _sec=0;
		private int _times=0;
		private DateTime _time= new DateTime();
		private string _module="";
		private int _xmno=0;
        private bool _applicated;

        public bool applicated
        {
            get { return _applicated; }
            set { _applicated = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int hour
		{
			set{ _hour=value;}
			get{return _hour;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int minute
		{
			set{ _minute=value;}
			get{return _minute;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int sec
		{
			set{ _sec=value;}
			get{return _sec;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int times
		{
			set{ _times=value;}
			get{return _times;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string module
		{
			set{ _module=value;}
			get{return _module;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

