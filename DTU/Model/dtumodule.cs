﻿/**  版本信息模板在安装目录下，可自行修改。
* dtumodule.cs
*
* 功 能： N/A
* 类 名： dtumodule
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/13 14:08:56   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
	/// <summary>
	/// dtumodule:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class dtumodule
	{
		public dtumodule()
		{}
		#region Model
		private string _name;
		private string _id;
		private string _addressno;
		private string _od;
		private int _port;
		private int _xmno;
		private string _senorno;
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string addressno
		{
			set{ _addressno=value;}
			get{return _addressno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string od
		{
			set{ _od=value;}
			get{return _od;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int port
		{
			set{ _port=value;}
			get{return _port;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string senorno
		{
			set{ _senorno=value;}
			get{return _senorno;}
		}
		#endregion Model

	}
}

