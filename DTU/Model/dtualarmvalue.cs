﻿/**  版本信息模板在安装目录下，可自行修改。
* dtualarmvalue.cs
*
* 功 能： N/A
* 类 名： dtualarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:33   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
	/// <summary>
	/// dtualarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class dtualarmvalue
	{
		public dtualarmvalue()
		{}
		#region Model
		private string _alarmname;
		private double _deep;
        private double _thisdeep;
        private double _acdeep;
        private double _rap;
		private int _xmno=0;
		private int _id;
		/// <summary>
		/// 
		/// </summary>
		public string alarmname
		{
			set{ _alarmname=value;}
			get{return _alarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double deep
		{
			set{ _deep=value;}
			get{return _deep;}
		}
        /// <summary>
        /// 
        /// </summary>
        public double thisdeep
        {
            set { _thisdeep = value; }
            get { return _thisdeep; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double acdeep
        {
            set { _acdeep = value; }
            get { return _acdeep; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double rap
        {
            set { _rap = value; }
            get { return _rap; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		#endregion Model

	}
}

