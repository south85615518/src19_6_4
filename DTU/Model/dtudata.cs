﻿/**  版本信息模板在安装目录下，可自行修改。
* dutdata.cs
*
* 功 能： N/A
* 类 名： dutdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace DTU.Model
{
	/// <summary>
	/// dutdata:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class dtudata
	{
		public dtudata()
		{}
		#region Model
		private string _point_name;
		private int _mkh=0;
		private double _deep;
		private double _groundelevation;
		private int _xmno=0;
		private DateTime _time= new DateTime();
        private DateTime _dtutime = new DateTime();
        private double _degree;
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mkh
		{
			set{ _mkh=value;}
			get{return _mkh;}
		}
        /// <summary>
        /// 
        /// </summary>
        public double degree
        {
            set { _degree = value; }
            get { return _degree; }
        }
		/// <summary>
		/// 
		/// </summary>
		public double deep
		{
			set{ _deep=value;}
			get{return _deep;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double groundElevation
		{
			set{ _groundelevation=value;}
			get{return _groundelevation;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
        /// <summary>
        /// 
        /// </summary>
        public DateTime dtutime
        {
            set { _dtutime = value; }
            get { return _dtutime; }
        }
		#endregion Model

	}
}

