﻿/**  版本信息模板在安装目录下，可自行修改。
* dtualarmvalue.cs
*
* 功 能： N/A
* 类 名： dtualarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace DTU.BLL
{
	/// <summary>
	/// dtualarmvalue
	/// </summary>
	public partial class dtualarmvalue
	{
		private readonly DTU.DAL.dtualarmvalue dal=new DTU.DAL.dtualarmvalue();
		public dtualarmvalue()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //    return dal.GetMaxId();
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string alarmname,int xmno)
        //{
        //    return dal.Exists(alarmname,xmno);
        //}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtualarmvalue model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}的预警参数{1}成功", model.xmno, model.alarmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}的预警参数{1}失败", model.xmno, model.alarmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}的预警参数{1}出错,错误信息："+ex.Message, model.xmno, model.alarmname);
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtualarmvalue model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("修改项目编号{0}的预警参数{1}成功", model.xmno, model.alarmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("修改项目编号{0}的预警参数{1}失败", model.xmno, model.alarmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("修改项目编号{0}的预警参数{1}出错,错误信息：" + ex.Message, model.xmno, model.alarmname);
                return false;
            }
		}

        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的预警参数表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的预警参数表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的预警参数表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的预警参数记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的预警参数记录数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的预警参数记录数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }



		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string alarmname,int xmno,out string mssg)
		{

            try
            {
                if (dal.Delete(alarmname, xmno))
                {
                    mssg = string.Format("删除项目编号{0}预警名称{1}成功", xmno,alarmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}预警名称{1}失败", xmno,alarmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}预警名称{1}出错，错误信息："+ex.Message, xmno, alarmname);
                return false;
            }



			
		}

        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno, out string alarmValueNameStr, out string mssg)
        {
            alarmValueNameStr = "";
            try
            {
                if (dal.AlarmValueNameGet(xmno, out alarmValueNameStr))
                {
                    mssg = string.Format("获取项目{0}的水位预警名称{1}成功", xmno, alarmValueNameStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的水位预警名称失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的水位预警名称出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtualarmvalue GetModel(string alarmname,int xmno)
		{
			
			return dal.GetModel(alarmname,xmno);
		}
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string name, int xmno, out DTU.Model.dtualarmvalue model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(name, xmno, out model))
                {
                    mssg = string.Format("获取项目{0}水位预警名称{1}实体成功", xmno, name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}水位预警名称{1}实体失败", xmno, name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}水位预警名称{1}实体出错，错误信息:" + ex.Message, xmno, name);
                return false;
            }

            //return dal.GetModel(sid,name);
        }

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        //public DTU.Model.dtualarmvalue GetModelByCache(string alarmname,int xmno)
        //{
			
        //    string CacheKey = "dtualarmvalueModel-" + alarmname+xmno;
        //    object objModel = DTU.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(alarmname,xmno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = DTU.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                DTU.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (DTU.Model.dtualarmvalue)objModel;
        //}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtualarmvalue> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtualarmvalue> DataTableToList(DataTable dt)
		{
			List<DTU.Model.dtualarmvalue> modelList = new List<DTU.Model.dtualarmvalue>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTU.Model.dtualarmvalue model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

