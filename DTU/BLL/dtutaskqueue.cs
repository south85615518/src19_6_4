﻿/**  版本信息模板在安装目录下，可自行修改。
* duttaskqueue.cs
*
* 功 能： N/A
* 类 名： duttaskqueue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace DTU.BLL
{
	/// <summary>
	/// duttaskqueue
	/// </summary>
	public partial class dtutaskqueue
	{
		private readonly DTU.DAL.duttaskqueue dal=new DTU.DAL.duttaskqueue();
		public dtutaskqueue()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //    return dal.GetMaxId();
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string commendName,int xmno)
        //{
        //    return dal.Exists(commendName,xmno);
        //}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtutaskqueue model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtutaskqueue model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string commendName,int xmno)
		{
			
			return dal.Delete(commendName,xmno);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtutaskqueue GetModel(string commendName,int xmno)
		{
			
			return dal.GetModel(commendName,xmno);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        //public DTU.Model.duttaskqueue GetModelByCache(string commendName,int xmno)
        //{
			
        //    string CacheKey = "duttaskqueueModel-" + commendName+xmno;
        //    object objModel = DTU.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(commendName,xmno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = DTU.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                DTU.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (DTU.Model.duttaskqueue)objModel;
        //}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtutaskqueue> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtutaskqueue> DataTableToList(DataTable dt)
		{
			List<DTU.Model.dtutaskqueue> modelList = new List<DTU.Model.dtutaskqueue>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTU.Model.dtutaskqueue model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}
        public bool GetDTUQueue(int xmno, string module, out List<string> commendList,out string mssg)
        {
            commendList = null;
            try
            {
                if (dal.GetDTUQueue(xmno, module, out commendList))
                {
                    mssg = string.Format("获取项目编号{0}模块{1}的任务队列中的待执行命令{2}条", xmno, module, commendList.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}模块{1}的任务队列中的待执行命令失败", xmno, module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}模块{1}的任务队列中的待执行命令出错，错误信息:" + ex.Message, xmno, module);
                return false;
            }
        }
        public bool DTUQueueTaskSetActived(int xmno, string pointname, int commendIndex,out string mssg)
        {
            try
            {
                if (dal.DTUQueueTaskSetActived(xmno, pointname, commendIndex))
                {
                    mssg = string.Format("设置项目编号{0}点名{1}命令编号{2}的执行状态为已完成成功", xmno, pointname, commendIndex);
                    return true;
                }
                else
                {
                    mssg = string.Format("设置项目编号{0}点名{1}命令编号{2}的执行状态为已完成失败", xmno, pointname, commendIndex);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("设置项目编号{0}点名{1}命令编号{2}的执行状态为已完成出错，错误信息:"+ex.Message, xmno, pointname, commendIndex);
                return false;
            }
        }



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

