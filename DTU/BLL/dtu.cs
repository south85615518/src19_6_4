﻿/**  版本信息模板在安装目录下，可自行修改。
* dut.cs
*
* 功 能： N/A
* 类 名： dut
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using Tool;
namespace DTU.BLL
{
    /// <summary>
    /// dut
    /// </summary>
    public partial class dtu
    {
        private readonly DTU.DAL.dut dal = new DTU.DAL.dut();
        public dtu()
        { }
        #region  BasicMethod
        public bool ExistModuleNo(int xmno, string pointname, string module, out string mssg)
        {
            try
            {
                if (dal.ExistModuleNo(xmno, pointname, module))
                {
                    mssg = string.Format("项目编号{0}测点表中模块{1}已经在使用", xmno, module);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}测点表中模块{1}未被使用", xmno, module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测点表中模块{1}的使用状态出错，错误信息：" + ex.Message, xmno, module);
                return false;
            }

        }
        /// <summary>
        /// 得到最大ID
        /// </summary>
        //public int GetMaxId()
        //{
        //    return dal.GetMaxId();
        //}

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string point_name, int xmno, out string mssg)
        {

            try
            {
                if (dal.Exists(point_name, xmno))
                {
                    mssg = string.Format("项目编号{0}存在点{1}", xmno, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}不存在点{1}", xmno, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点{1}出错,错误信息:" + ex.Message, xmno, point_name);
                return true;
            }

        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(DTU.Model.dtu model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}水位点{1}模块号{2}的设备成功", model.xmno, model.point_name, model.module);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}水位点{1}模块号{2}的设备失败", model.xmno, model.point_name, model.module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}水位点{1}模块号{2}的设备出错，错误信息：" + ex.Message, model.xmno, model.point_name, model.module);
                return false;
            }




            //return dal.Add(model);
        }

        /// <summary>
        /// 加载DTU点名
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="ls"></param>
        /// <returns></returns>
        public bool DTUPointLoadDAL(int xmno, out List<string> ls, out string mssg)
        {
            mssg = "";
            ls = new List<string>();
            try
            {
                if (dal.DTUPointLoadDAL(xmno, out ls))
                {
                    mssg = string.Format("成功获取项目编号{0}DTU点名{1}个", xmno, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}DTU点名列表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}DTU点名列表出错，错误信息:" + ex.Message, xmno);
                return false;
            }

        }




        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DTU.Model.dtu model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}测点编号{1}的设备成功", model.xmno, model.id);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}测点编号{1}的设备失败", model.xmno, model.id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}测点编号{1}的设备出错，错误信息：" + ex.Message, model.xmno, model.id);
                return false;
            }
        }

        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointnamestr, DTU.Model.dtu model, out string mssg)
        {
            //dal.MultiUpdate(pointnamestr,model);
            try
            {
                if (dal.MultiUpdate(pointnamestr, model))
                {
                    mssg = "水位预警点批量更新成功";
                    return true;
                }
                else
                {
                    mssg = "水位预警点批量更新失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "水位预警点批量更新出错，错误信息" + ex.Message;
                return false;
            }
        }

        public bool DTUList(int startPageIndex, int pageSize, int xmno, string xmname,string colName, string sord, out DataTable dt, out string mssg)
        {

            dt = null;
            try
            {
                if (dal.DTUList(startPageIndex, pageSize, xmno,xmname,colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的设备状态信息{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的设备状态信息失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的设备状态信息出错，错误信息:" + ex.Message, xmno);
                return false;
            }

        }
        public bool TableDTUCount(string searchstring, int xmno, out int totalCont,out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的设备状态信息记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的设备状态记录数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的设备状态记录数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }


        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的点号预警表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的点号预警表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的点号预警表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的点号预警记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的点号预警记录数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的点号预警记录数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string point_name, string module, int xmno, out string mssg)
        {

            try
            {
                if (dal.Delete(point_name, module, xmno))
                {
                    mssg = string.Format("删除项目编号{0}点{1}模块编号{2}的记录成功", xmno, point_name, module);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}点{1}模块编号{2}的记录失败", xmno, point_name, module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}点{1}模块编号{2}的记录出错,错误信息：" + ex.Message, xmno, point_name, module);
                return false;
            }



            //return dal.Delete(point_name,module,xmno);
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, out DTU.Model.dtu model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, out model))
                {
                    mssg = string.Format("获取{0}的预警参数成功!", pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}的预警参数失败!", pointname);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的预警参数出错!错误信息:" + ex.Message, pointname);
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public DTU.Model.dtu GetModel(string point_name, int module, int xmno)
        {

            return dal.GetModel(point_name, module, xmno);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        //public DTU.Model.dut GetModelByCache(string point_name,int module,int xmno)
        //{

        //    string CacheKey = "dutModel-" + point_name+module+xmno;
        //    object objModel = DTU.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(point_name,module,xmno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = DTU.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                DTU.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (DTU.Model.dut)objModel;
        //}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTU.Model.dtu> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<DTU.Model.dtu> DataTableToList(DataTable dt)
        {
            List<DTU.Model.dtu> modelList = new List<DTU.Model.dtu>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                DTU.Model.dtu model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        public bool DTUTOPointModule(int xmno, int port, string od, string addressno, out DTU.Model.dtu model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.DTUTOPointModule(xmno, port, od, addressno, out model))
                {
                    mssg = string.Format("获取项目编号{0}监听端口{1}地址码{2}通道号{3}的点号对象成功", xmno, port, addressno, od);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}监听端口{1}地址码{2}通道号{3}的点号对象失败", xmno, port, addressno, od);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}监听端口{1}地址码{2}通道号{3}的点号对象出错，错误信息:" + ex.Message, xmno, port, addressno, od);
                return false;
            }
            //finally
            //{
            //    ExceptionLog.DTURecordWrite(mssg);
            //}
        }

        public bool NGNTOPointModule(int xmno, int port, string addressno, out DTU.Model.dtu model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.NGNTOPointModule(xmno, port, addressno, out model))
                {
                    mssg = string.Format("获取监听端口{0}地址码{1}的点号对象成功", port, addressno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取监听端口{0}地址码{1}的点号对象失败", port, addressno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取监听端口{0}地址码{1}的点号对象出错，错误信息:" + ex.Message, port, addressno);
                return false;
            }
            //finally
            //{
            //    ExceptionLog.DTURecordWrite(mssg);
            //}

        }



        public bool DTUTOPointModule(int xmno, int port, string addressno, out DTU.Model.dtu model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.DTUTOPointModule(xmno, port, addressno, out model))
                {
                    mssg = string.Format("获取项目编号{0}监听端口{1}地址码{2}任意通道号的点号对象成功", xmno, port, addressno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}监听端口{1}地址码{2}任意通道号的点号对象失败", xmno, port, addressno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}监听端口{1}地址码{2}任意通道号的点号对象出错，错误信息:" + ex.Message, xmno, port, addressno);
                return false;
            }

        }

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

