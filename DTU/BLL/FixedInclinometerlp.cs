﻿/**  版本信息模板在安装目录下，可自行修改。
* dtulp.cs
*
* 功 能： N/A
* 类 名： dtulp
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/25 16:16:12   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//Please add references
namespace DTU.BLL
{
    /// <summary>
    /// 数据访问类:dtulp
    /// </summary>
    public partial class dtulp
    {
        #region  BasicMethod

        public bool FixedInclinometerTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.FixedInclinometerTableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的固定测斜模块低功耗设置表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的固定测斜模块低功耗设置表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的固定测斜模块低功耗设置表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }


        public bool FixedInclinometerXmPortTableLoad(int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.FixedInclinometerXmPortTableLoad(startPageIndex, pageSize, xmno, xmname, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的固定测斜模块低功耗设置表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的固定测斜模块低功耗设置表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的固定测斜模块低功耗设置表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        public bool FixedInclinometerTableRowsCount(string searchstring, int xmno, out int totalCont,out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.FixedInclinometerTableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的固定测斜模块低功耗设置记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的固定测斜模块低功耗设置记录数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的固定测斜模块低功耗设置记录数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }




        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

