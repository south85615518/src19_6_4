﻿/**  版本信息模板在安装目录下，可自行修改。
* dtulp.cs
*
* 功 能： N/A
* 类 名： dtulp
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/25 16:16:12   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using DTU.Model;
namespace DTU.BLL
{
	/// <summary>
	/// dtulp
	/// </summary>
	public partial class dtulp
	{
		private readonly DTU.DAL.dtulp dal=new DTU.DAL.dtulp();
		public dtulp()
		{}
		#region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int xmno, string module,out string mssg)
        {
            try
            {
                if (dal.Exists(xmno, module))
                {
                    mssg = string.Format("存在项目编号{0}模块{1}的低功率设置记录", xmno, module);
                    return true;
                }
                else
                {
                    mssg = string.Format("不存在项目编号{0}模块{1}的低功率设置记录", xmno, module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}模块{1}的低功率设置记录出错,错误信息:"+ex.Message, xmno, module);
                return false;
            }
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtulp model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("设置项目编号{0}模块{1}的功耗成功", model.xmno, model.module);
                    return true;
                }
                else
                {
                    mssg = string.Format("设置项目编号{0}模块{1}的功耗失败", model.xmno, model.module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("设置项目编号{0}模块{1}的功耗出错，错误信息："+ex.Message, model.xmno, model.module);
                return false;
            }


			//return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtulp model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}模块{1}的功耗成功", model.xmno, model.module);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}模块{1}的功耗失败", model.xmno, model.module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}模块{1}的功耗出错，错误信息：" + ex.Message, model.xmno, model.module);
                return false;
            }
		}

		


        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的模块低功耗设置表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的模块低功耗设置表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的模块低功耗设置表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        public bool XmPortTableLoad(int startPageIndex, int pageSize, int xmno, string xmname,string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.XmPortTableLoad(startPageIndex, pageSize, xmno,xmname ,colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的模块低功耗设置表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的模块低功耗设置表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的模块低功耗设置表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }


        public bool TableRowsCount(string searchstring, int xmno, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的模块低功耗设置记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的模块低功耗设置记录数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的模块低功耗设置记录数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string module, out DTU.Model.dtulp model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, module, out model))
                {
                    mssg = string.Format("获取项目{0}模块编号{1}的低功率设置为{2}成功", xmno, module,model.LP == 1?"开":"关");
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}模块编号{1}的低功率设置失败", xmno, module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}模块编号{1}的低功率设置出错，错误信息："+ex.Message, xmno, module);
                return false;
            }
        }
		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool DeleteList(string modulelist )
        //{
        //    return dal.DeleteList(modulelist );
        //}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        //public DTU.Model.dtulp GetModel(string module)
        //{
			
        //    return dal.GetModel(module);
        //}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        //public DTU.Model.dtulp GetModelByCache(string module)
        //{
			
        //    string CacheKey = "dtulpModel-" + module;
        //    object objModel = DTU.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(module);
        //            if (objModel != null)
        //            {
        //                int ModelCache = DTU.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                DTU.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (DTU.Model.dtulp)objModel;
        //}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtulp> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtulp> DataTableToList(DataTable dt)
		{
			List<DTU.Model.dtulp> modelList = new List<DTU.Model.dtulp>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTU.Model.dtulp model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

