﻿/**  版本信息模板在安装目录下，可自行修改。
* dtusenor.cs
*
* 功 能： N/A
* 类 名： dtusenor
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/13 14:08:57   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace DTU.BLL
{
	/// <summary>
	/// dtusenor
	/// </summary>
	public partial class dtusenor
	{
		private readonly DTU.DAL.dtusenor dal=new DTU.DAL.dtusenor();
		public dtusenor()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string senorno)
        //{
        //    return dal.Exists(senorno);
        //}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtusenor model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}类型:{1}型号:{2}:名称{3}传感器成功", model.xmno, model.senortype, model.senorno, model.senorname);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}类型:{1}型号:{2}:名称{3}传感器失败", model.xmno, model.senortype, model.senorno, model.senorname);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}类型:{1}型号:{2}:名称{3}传感器出错,错误信息："+ex.Message, model.xmno, model.senortype, model.senorno, model.senorname);
                return false;
            }




			//return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtusenor model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}类型:{1}型号:{2}:名称{3}传感器成功", model.xmno, model.senortype, model.senorno, model.senorname);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}类型:{1}型号:{2}:名称{3}传感器失败", model.xmno, model.senortype, model.senorno, model.senorname);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}类型:{1}型号:{2}:名称{3}传感器出错,错误信息：" + ex.Message, model.xmno, model.senortype, model.senorno, model.senorname);
                return false;
            }
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string senorno,out string mssg)
		{
            try
            {
                if (dal.Delete(xmno, senorno))
                {
                    mssg = string.Format("删除项目编号{0}传感器编号{1}的记录成功", xmno, senorno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}传感器编号{1}的记录失败", xmno, senorno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}传感器编号{1}的记录出错，错误信息:"+ex.Message, xmno, senorno);
                return false;
            }
			
			//return dal.Delete(senorno);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool DeleteList(string senornolist )
        //{
        //    return dal.DeleteList(senornolist );
        //}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtusenor GetModel(string senorno)
		{
			
			return dal.GetModel(senorno);
		}

        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的传感器表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目编号{0}的传感器表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的传感器表出错，错误信息:"+ex.Message, xmno);
                return false;
            }
        }
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont,out string mssg)
        {
            totalCont = 0;
            try {
                if (dal.TableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的传感器记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的传感器记录数失败", xmno);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的传感器记录数出错，错误信息:"+ex.Message, xmno);
                return false;
            }
        }
        /// <summary>
        /// 获取传感器编号
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool SenorNoGet(int xmno, out string senornoStr,out string mssg)
        {
            senornoStr = "";
            try
            {
                if (dal.SenorNoGet(xmno, out senornoStr))
                {
                    mssg = string.Format("获取项目{0}的传感器编号{1}成功", xmno, senornoStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的传感器编号失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的传感器编号出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        //public DTU.Model.dtusenor GetModelByCache(string senorno)
        //{
			
        //    string CacheKey = "dtusenorModel-" + senorno;
        //    object objModel = DTU.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(senorno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = DTU.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                DTU.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (DTU.Model.dtusenor)objModel;
        //}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtusenor> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtusenor> DataTableToList(DataTable dt)
		{
			List<DTU.Model.dtusenor> modelList = new List<DTU.Model.dtusenor>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTU.Model.dtusenor model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

