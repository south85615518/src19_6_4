﻿/**  版本信息模板在安装目录下，可自行修改。
* dtutimetask.cs
*
* 功 能： N/A
* 类 名： dtutimetask
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace DTU.BLL
{
	/// <summary>
	/// dtutimetask
	/// </summary>
	public partial class dtutimetask
	{
		private readonly DTU.DAL.dtutimetask dal=new DTU.DAL.dtutimetask();
		public dtutimetask()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //    return dal.GetMaxId();
        //}
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string module, int xmno,out string mssg)
        {
            try
            {
                if (dal.Exists(module, xmno))
                {
                    mssg = string.Format("存在项目编号{0}模块{1}的时间间隔设置记录", xmno, module);
                    return true;
                }
                else
                {
                    mssg = string.Format("不存在项目编号{0}模块{1}的时间间隔设置记录", xmno, module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}模块{1}的时间间隔设置记录出错,错误信息:"+ex.Message, xmno, module);
                return false;
            }

        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtutimetask model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}模块{1}的时间间隔任务{2}时{3}分{4}次成功", model.xmno, model.module, model.hour, model.minute, model.times);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}模块{1}的时间间隔任务{2}时{3}分{4}次失败", model.xmno, model.module, model.hour, model.minute, model.times);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}模块{1}的时间间隔任务{2}时{3}分{4}次出错，错误信息："+ex.Message, model.xmno, model.module, model.hour, model.minute, model.times);
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtutimetask model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}模块{1}的时间间隔任务为{2}时{3}分{4}次成功", model.xmno, model.module, model.hour, model.minute, model.times);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}模块{1}的时间间隔任务为{2}时{3}分{4}次失败", model.xmno, model.module, model.hour, model.minute, model.times);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}模块{1}的时间间隔任务为{2}时{3}分{4}次出错，错误信息：" + ex.Message, model.xmno, model.module, model.hour, model.minute, model.times);
                return false;
            }
		}
        /// <summary>
        /// 更新设备设置标志
        /// </summary>
        public bool UpdateFlagSet(DTU.Model.dtutimetask model, out string mssg)
        {
            try
            {
                if (dal.UpdateSetFlag(model))
                {
                    mssg = string.Format("更新项目编号{0}模块{1}的时间间隔任务设置标志成功", model.xmno, model.module);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}模块{1}的时间间隔任务设置标志失败", model.xmno, model.module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}模块{1}的时间间隔任务设置标志出错，错误信息：" + ex.Message, model.xmno, model.module);
                return false;
            }
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int hour,int minute,int sec,int times,string module,DateTime time,int xmno)
		{

            return false;//dal.Delete(hour,minute,sec,times,module,time,module,xmno);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtutimetask GetModel(int hour,int minute,int sec,int times,string module,DateTime time,int xmno)
		{

            return null;// dal.GetModel(hour, minute, sec, times, module, time, module, xmno);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        //public DTU.Model.dtutimetask GetModelByCache(int hour,int minute,int sec,int times,string module,DateTime time,int module,int xmno)
        //{
			
        //    string CacheKey = "dtutimetaskModel-" + hour+minute+sec+times+module+time+module+xmno;
        //    object objModel = DTU.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(hour,minute,sec,times,module,time,module,xmno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = DTU.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                DTU.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (DTU.Model.dtutimetask)objModel;
        //}

        public bool GetModel(int xmno, string module, out DTU.Model.dtutimetask model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, module, out model))
                {
                    mssg = string.Format("获取项目编号{0}模块{1}的时间间隔为{2}时{3}分{4}秒采集{5}次数", xmno, module, model.hour, model.minute, model.sec, model.times);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}模块{1}的时间间隔失败", xmno, module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}模块{1}的时间间隔出错，错误信息："+ex.Message, xmno, module);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string module, int xmno,out string mssg)
        {
            try
            {
                if (dal.Delete(module, xmno))
                {
                    mssg = string.Format("删除项目编号{0}模块编号{1}的时间间隔任务成功", xmno, module);
                    return true;
                }
                else {
                    mssg = string.Format("删除项目编号{0}模块编号{1}的时间间隔任务失败", xmno, module);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}模块编号{1}的时间间隔任务出错，错误信息："+ex.Message, xmno, module);
                return false;
            }
          
        }

        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的时间间隔任务表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的时间间隔任务表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的时间间隔任务表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        public bool XmPortTableLoad(int startPageIndex, int pageSize, int xmno, string xmname ,string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.XmPortTableLoad(startPageIndex, pageSize, xmno,xmname, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的时间间隔任务表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的时间间隔任务表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的时间间隔任务表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }


        public bool TableRowsCount(string searchstring, int xmno, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的时间间隔任务记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的时间间隔任务记录数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的时间间隔任务记录数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }



		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtutimetask> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtutimetask> DataTableToList(DataTable dt)
		{
			List<DTU.Model.dtutimetask> modelList = new List<DTU.Model.dtutimetask>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTU.Model.dtutimetask model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

