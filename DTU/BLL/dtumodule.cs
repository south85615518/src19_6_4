﻿/**  版本信息模板在安装目录下，可自行修改。
* dtumodule.cs
*
* 功 能： N/A
* 类 名： dtumodule
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/13 14:08:56   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace DTU.BLL
{
	/// <summary>
	/// dtumodule
	/// </summary>
	public partial class dtumodule
	{
		private readonly DTU.DAL.dtumodule dal=new DTU.DAL.dtumodule();
		public dtumodule()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string addressno,string od)
        //{
        //    return dal.Exists(addressno,od);
        //}
        public bool ExistSenorNo(int xmno,string module ,string senorno, out string mssg)
        {
            try
            {
                if (dal.ExistSenorNo(xmno,module ,senorno))
                {
                    mssg = string.Format("项目编号{0}模块表中传感器{1}已经在使用", xmno, senorno);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}模块表中传感器{1}未被使用", xmno, senorno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}模块表中传感器{1}的使用状态出错，错误信息：" + ex.Message, xmno, senorno);
                return false;
            }

        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtumodule model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}模块编号:{1}通道:{2}传感器编号:{3}DTU采集模块成功", model.xmno,model.addressno ,model.od, model.senorno);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}模块编号:{1}通道:{2}传感器编号:{3}DTU采集模块失败", model.xmno, model.addressno, model.od, model.senorno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}模块编号:{1}通道:{2}传感器编号:{3}DTU采集模块出错，错误信息:"+ex.Message, model.xmno, model.addressno, model.od, model.senorno);
                return false;
            }
		}

        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的模块表{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的模块表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的模块表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的模块记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的模块记录数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的模块记录数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        /// <summary>
        /// 获取传感器编号
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool ModuleNoGet(int xmno, out string moduleStr, out string mssg)
        {
            moduleStr = "";
            try
            {
                if (dal.ModuleNoGet(xmno, out moduleStr))
                {
                    mssg = string.Format("获取项目{0}的模块编号{1}成功", xmno, moduleStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的模块编号失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的模块编号出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtumodule model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}模块编号:{1}通道:{2}传感器编号:{3}DTU采集模块成功", model.xmno, model.addressno, model.od, model.senorno);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}模块编号:{1}通道:{2}传感器编号:{3}DTU采集模块失败", model.xmno, model.addressno, model.od, model.senorno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}模块编号:{1}通道:{2}传感器编号:{3}DTU采集模块出错，错误信息:" + ex.Message, model.xmno, model.addressno, model.od, model.senorno);
                return false;
            }
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string addressno,string od,out string mssg)
		{
            try
            {
                if (dal.Delete(xmno, addressno,od))
                {
                    mssg = string.Format("删除项目编号{0}模块编号{1}通道{2}的记录成功", xmno,addressno,od);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}模块编号{1}通道{2}的记录失败", xmno, addressno, od);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}模块编号{1}通道{2}的记录出错，错误信息:" + ex.Message, xmno, addressno, od);
                return false;
            }



			//return dal.Delete(addressno,od);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtumodule GetModel(string addressno,string od)
		{
			
			return dal.GetModel(addressno,od);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        //public DTU.Model.dtumodule GetModelByCache(string addressno,string od)
        //{
			
        //    string CacheKey = "dtumoduleModel-" + addressno+od;
        //    object objModel = DTU.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(addressno,od);
        //            if (objModel != null)
        //            {
        //                int ModelCache = DTU.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                DTU.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (DTU.Model.dtumodule)objModel;
        //}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtumodule> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtumodule> DataTableToList(DataTable dt)
		{
			List<DTU.Model.dtumodule> modelList = new List<DTU.Model.dtumodule>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTU.Model.dtumodule model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

