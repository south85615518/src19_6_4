﻿/**  版本信息模板在安装目录下，可自行修改。
* dutdata.cs
*
* 功 能： N/A
* 类 名： dutdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 DTU Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace DTU.BLL
{
	/// <summary>
	/// dutdata
	/// </summary>
	public partial class dtudata
	{
		private readonly DTU.DAL.dutdata dal=new DTU.DAL.dutdata();
		public dtudata()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //    return dal.GetMaxId();
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string point_name,int mkh,int xmno,DateTime time)
        //{
        //    return dal.Exists(point_name,mkh,xmno,time);
        //}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(DTU.Model.dtudata model,out string mssg)
		{

            try {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}点{1}采集时间{2}的水位数据成功", model.xmno, model.point_name, model.time);
                    return true;
                }
                else
                {
                    mssg = "";// string.Format("添加项目编号{0}点{1}模块号{2}的水位数据失败", model.xmno, model.point_name, model.mkh);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("添加项目编号{0}点{1}采集时间{2}的水位数据出错,错误信息:" + ex.Message, model.xmno, model.point_name, model.time);
                return false;
            }



			
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(DTU.Model.dtudata model)
		{
			return dal.Update(model);
		}

        /// <summary>
        /// 更新温度数据
        /// </summary>
        public bool UpdateDegree(DTU.Model.dtudata model,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.UpdateDegree(model))
                {
                    mssg = string.Format("成功更新项目编号{0}点{1}{2}采集的水位数据的温度为{3}", model.xmno, model.point_name, model.time, model.degree);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}点{1}{2}采集的水位数据的温度为{3}失败", model.xmno, model.point_name, model.time, model.degree);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}点{1}{2}采集的水位数据的温度为{3}出错,错误信息:"+ex.Message, model.xmno, model.point_name, model.time, model.degree);
                return false;
            }
        }


		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string point_name,int mkh,int xmno,DateTime time)
		{
			
			return dal.Delete(point_name,mkh,xmno,time);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public DTU.Model.dtudata GetModel(string point_name,int mkh,int xmno,DateTime time)
		{
			
			return dal.GetModel(point_name,mkh,xmno,time);
		}

        public bool GetModel(int xmno, string pointname, DateTime dt, out DTU.Model.dtudatareport model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, dt, out model))
                {
                    mssg = string.Format("获取项目编号{0}点{1}采集时间{2}的数据记录成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点{1}采集时间{2}的数据记录失败", xmno, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点{1}采集时间{2}的数据记录出错，错误信息："+ex.Message, xmno, pointname, dt);
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
        //public DTU.Model.dutdata GetModelByCache(string point_name,int mkh,int xmno,DateTime time)
        //{
			
        //    string CacheKey = "dutdataModel-" + point_name+mkh+xmno+time;
        //    object objModel = DTU.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(point_name,mkh,xmno,time);
        //            if (objModel != null)
        //            {
        //                int ModelCache = DTU.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                DTU.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (DTU.Model.dutdata)objModel;
        //}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}

        


        public bool MaxTime(int xmno, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno,out maxTime))
                {
                    mssg = string.Format("获取项目{0}水位测量数据的最大日期{1}成功", xmno, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}水位测量数据的最大日期失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}水位测量数据的最大日期出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }

        public bool MaxTime(int xmno, string point_name,out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno, point_name,out maxTime))
                {
                    mssg = string.Format("获取项目{0}点名{1}水位测量数据的最大日期{2}成功", xmno,point_name ,maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}点名{1}水位测量数据的最大日期失败", xmno,point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}点名{1}水位测量数据的最大日期出错，错误信息:" + ex.Message, xmno,point_name);
                return false;
            }
        }
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtudata> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<DTU.Model.dtudata> DataTableToList(DataTable dt)
		{
			List<DTU.Model.dtudata> modelList = new List<DTU.Model.dtudata>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				DTU.Model.dtudata model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

        public bool DeleteTmp(int xmno,out string mssg)
        {
            try
            {
                if (dal.DeleteTmp(xmno))
                {
                    mssg = string.Format("删除项目编号{0}水位临时表的数据成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}水位临时表的数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}水位临时表的数据出错,错误信息：" + ex.Message, xmno);
                return false;
            }
           
        }

        public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt,out string mssg)
        {

            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(xmno, pointname, out dt))
                {
                    mssg = string.Format("获取项目编号{0}点{1}水位的最新采集时间成功", xmno, pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点{1}水位的最新采集时间失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点{1}水位的最新采集时间出错,错误信息:" + ex.Message, xmno, pointname);
                return false;
            }
        }
        public bool ResultDataReportPrint(string sql, int xmno, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultDataReportPrint(sql, xmno, out dt))
                {
                    mssg = "水位结果数据报表数据表生成成功";
                    return true;
                }
                else
                {
                    mssg = "水位结果数据报表数据表生成失败";
                    return false;
                }
            }
            catch (Exception ex)
            {

                mssg = "水位结果数据报表数据表生成出错，错误信息" + ex.Message;
                return false;

            }
        }

        public bool DTUDATATableLoad(DateTime starttime, DateTime endtime, int startPageIndex, int pageSize, int xmno, string pointname, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.DTUDATATableLoad(starttime, endtime, startPageIndex, pageSize, xmno, pointname, sord, out dt))
                {
                    if (starttime != new DateTime() && endtime != new DateTime())
                        mssg = string.Format("获取项目编号{0}从{1}至{2}水位数据{3}条成功", xmno, starttime, endtime, dt.Rows.Count);
                    else
                    {
                        mssg = string.Format("获取项目编号{0}监测次数从{1}至{2}水位数据{3}条成功", xmno, dt.Rows.Count);
                    }
                    return true;
                }
                else
                {
                    if (starttime != new DateTime() && endtime != new DateTime())
                        mssg = string.Format("获取项目编号{0}从{1}至{2}水位数据失败", xmno, starttime, endtime);
                    else
                    {
                        mssg = string.Format("获取项目编号{0}监测次数从{1}至{2}水位数据失败", xmno);
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (starttime != new DateTime() && endtime != new DateTime())
                    mssg = string.Format("获取项目编号{0}从{1}至{2}水位数据出错，错误信息：" + ex.Message, xmno, starttime, endtime);
                else
                {
                    mssg = string.Format("获取项目编号{0}监测次数从{1}至{2}水位数据出错，错误信息：" + ex.Message, xmno);
                }
                return false;
            }

        }
        public bool DTUTableRowsCount(DateTime starttime, DateTime endtime, int xmno, string pointname, out string totalCont,out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.DTUTableRowsCount(starttime, endtime, xmno, pointname, out  totalCont))
                {
                    mssg = string.Format("获取项目编号{0}从{1}至{2}之间的水位数据{3}条成功", xmno, starttime, endtime, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}从{1}至{2}之间的水位数据失败", xmno, starttime, endtime, totalCont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}从{1}至{2}之间的水位数据出错，错误信息:" + ex.Message, xmno, starttime, endtime);
                return false;
            }
        }

        public bool DTUDATATempDel(int xmno,out string mssg)
        {
            try
            {
                if (dal.DTUDATATempDel(xmno))
                {
                    mssg = string.Format("删除项目编号{0}的DTU临时表数据成功", xmno);
                    return true;
                }
                else {
                    mssg = string.Format("删除项目编号{0}的DTU临时表数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}的DTU临时表数据出错,错误信息:"+ex.Message, xmno);
                return false;
            }
            //return false;
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(int xmno, out List<DTU.Model.dtudatareport> li, out string mssg)
        {
            li = null;
            try
            {
                if (dal.GetList(xmno, out li))
                {
                    mssg = string.Format("获取项目编号{0}的临时表DTU数据{1}条成功", xmno, li.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的临时表DTU数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {

                mssg = string.Format("获取项目编号{0}的临时表DTU数据出错,错误信息:" + ex.Message, xmno);
                return false;
            }



        }
        public bool PointNameTimesListGet(int xmno, string pointname, out List<string> ls, out string mssg)
        {
            ls = new List<string>();
            try
            {
                if (dal.PointNameCycListGet(xmno, pointname, out ls))
                {
                    mssg = string.Format("获取项目编号{0}点名{1}的次数列表数量{2}成功", xmno, pointname, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点名{1}的次数列表失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点名{1}的次数列表出错,错误信息:" + ex.Message, xmno, pointname);
                return false;
            }

        }

        public bool GetdtudataList(int xmno,string point_name, DateTime starttime, DateTime endtime, out List<DTU.Model.dtudata> li, out string mssg)
        {
            li = null;
            mssg = "";
            try
            {
                if (dal.GetdtudataList(xmno, point_name, starttime, endtime, out li))
                {
                    mssg = string.Format("成功获取项目编号{0}点名{1}从{2}到{3}的数据记录{4}条", xmno, point_name, starttime, endtime, li.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点名{1}从{2}到{3}的数据记录失败", xmno, point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点名{1}从{2}到{3}的数据记录出错,错误信息:"+ex.Message, xmno, point_name, starttime, endtime);
                return false;
            }
        }
      
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

