﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NET
{
    /// <summary>
    /// mssglistcreate 的摘要说明
    /// </summary>
    public class mssglistcreate : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int i = 0;
            List<string> ls = new List<string>();
            for (i = 0; i < 10;i++ )
            {
                ls.Add("我是后台生成的随机文字信息:"+new Random().Next(99999));
            }
            context.Response.Write(string.Join("<b>",ls));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}