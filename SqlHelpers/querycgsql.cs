﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Tool;

namespace SqlHelpers
{
    /// <summary>
    /// 成果库查询
    /// </summary>
    public class querycgsql
    {
        public static database db = new database();

        public static bool  querystanderdb(string sql,string xmname,out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            dt = new DataTable();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            try
            {
                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                return false;
                throw (ex);
                
            }
            finally
            {
                conn.Close();
            }

            return true;

        }

        public static bool querystanderdb(string sql, int xmno, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderCgConn(xmno);
            dt = new DataTable();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            try
            {
                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                return false;
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return true;

        }


        public static bool querystanderstr(string sql, string xmname, out string str)
        {
            str = "";
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            if (conn.State != ConnectionState.Open)
                conn.Open();

            OdbcCommand cmd = new OdbcCommand(sql, conn);

            OdbcDataReader read = cmd.ExecuteReader();

            if (read.Read())
            {
                str = read[0].ToString();
                return true;
            }


            return false;
        }

    }
}