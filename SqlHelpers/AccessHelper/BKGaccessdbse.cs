﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data;
using System.Data.Sql;
using System.Data.OleDb;
using Tool;


namespace SqlHelpers
{
    /// <summary>
    /// 连接数据库
    /// </summary>
    public class BKGaccessdbse
    {
        public static string connstr = "Dsn=acs;dbq=C:/Users/pc/Documents/db.accdb;driverid=25;fil=MS Access;maxbuffersize=2048;pagetimeout=5;uid=admin";

        public OleDbConnection  getconn()
        {

            var dbmpath = FileHelper.ProcessGBKSettingString(System.AppDomain.CurrentDomain.BaseDirectory+"Setting/BKGAccessSetting.txt");
            
            ExceptionLog.ExceptionWrite(string.Format("\n当前权限库的路径为{0}\n",dbmpath));
            connstr = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};User ID =admin;Persist Security Info=False", dbmpath);
          OleDbConnection conn = new OleDbConnection(connstr);
          try
          {
              conn.Open();
             // Provider=Microsoft.Jet.OLEDB.4.0;Data Source=d:/swj/BGKDB(V6.1).mdb;User ID =admin;Persist Security Info=False
              ExceptionLog.ExceptionWrite(dbmpath+"数据库连接成功");
          }
          catch (Exception ex)
          {
              
              ExceptionLog.ExceptionWrite(ex);
              throw (ex);
          }
          finally {
              conn.Close();
          }
           return conn;
        }
    }
    
}