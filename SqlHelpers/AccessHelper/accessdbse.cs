﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data;
using System.Data.Sql;
using System.Data.OleDb;
using Tool;


namespace SqlHelpers
{
    /// <summary>
    /// 连接数据库
    /// </summary>
    public class accessdbse
    {
        public static string connstr = "Dsn=acs;dbq=C:/Users/pc/Documents/db.accdb;driverid=25;fil=MS Access;maxbuffersize=2048;pagetimeout=5;uid=admin";

        public OleDbConnection  getconn()
        {

            var dbmpath = FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory+"Setting/AccessSetting.txt");
            
            //ExceptionLog.ExceptionWrite(string.Format("\n当前权限库的路径为{0}\n",dbmpath));
            connstr = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};User ID =admin;Persist Security Info=False", dbmpath);
          OleDbConnection conn = new OleDbConnection(connstr);
          try
          {
              conn.Open();

          }
          catch (Exception ex)
          {
              
              ExceptionLog.ExceptionWrite(ex);
              throw (ex);
          }
          finally {
              conn.Close();
          }
           return conn;
        }
    }
    
}