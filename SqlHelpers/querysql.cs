﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Tool;
namespace SqlHelpers
{
    /// <summary>
    /// 根据sql语句查询并返回值
    /// </summary>
    public class querysql
    {
        public static database db = new database();
        public static accessdbse adb = new accessdbse();
        /// <summary>
        /// 使用access查询字符串
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string queryaccessdbstring(string sql)
        {
            string seg = "";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OleDbCommand ocmd = new OleDbCommand(sql, conn);
                OleDbDataReader reader = ocmd.ExecuteReader();
                if (reader.Read())
                {
                    seg = reader[0].ToString();
                }
                reader.Read();
            }
            catch (Exception ex)
            {
                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
            return seg;
        }
        /// <summary>
        /// 使用access查询字符串
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string queryaccessdbstring(string sql, OleDbConnection conn)
        {
            string seg = "";
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand ocmd = new OleDbCommand(sql, conn);
            OleDbDataReader reader = ocmd.ExecuteReader();
            if (reader.Read())
            {
                seg = reader[0].ToString();
            }
            reader.Read();
            conn.Close();
            return seg;
        }

        /// <summary>
        /// 使用access查询list
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static List<string> queryaccesslist(string sql)
        {
            List<string> strl = new List<string>();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OleDbCommand ocmd = new OleDbCommand(sql, conn);
                OleDbDataReader reader = ocmd.ExecuteReader();
                while (reader.Read())
                {
                    string strh = "";
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        strh += reader[i].ToString() + ",";
                    }
                    strh = strh.Substring(0, strh.LastIndexOf(","));
                    strl.Add(strh);
                }
            }
            catch (Exception ex)
            {

                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return strl;
        }

        /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static DataTable querystanderdb(string sql, OdbcConnection conn)
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            try
            {
                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                //return null;
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return dt;

        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool  querystanderdb(string sql,string xmname,out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            dt = new DataTable();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            try
            {
                DateTime dtstart = DateTime.Now;
                oda.Fill(dt);
                ExceptionLog.ExceptionWrite("本次查询用时"+(DateTime.Now -  dtstart).TotalMilliseconds);
            }
            catch (Exception ex)
            {
                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                return false;
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return true;

        }
       /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static DataTable querystanderdb(string sql, string xmname, List<string> columnNames)
        {
            ExceptionLog.ExceptionWrite("开始查询");
            OdbcConnection conn = db.GetStanderConn(xmname);
            DateTime dtstart = DateTime.Now;
            DataTable dt = new DataTable();
            columnNames.ForEach(m => dt.Columns.Add(m.Trim()));
            if (conn.State != ConnectionState.Open)
                conn.Open();

            OdbcCommand cmd = new OdbcCommand(sql, conn);
            OdbcDataReader reader = cmd.ExecuteReader();
            DataRow dr = dt.NewRow();
            try
            {
                while (reader.Read())
                {
                    columnNames.ForEach(m => dr[m.Trim()] = reader[m.Trim()]);
                    dt.Rows.Add(dr);
                    dr = dt.NewRow();
                }
            }
            catch (Exception ex)
            {
                throw(ex);
                return null;
            }
            finally
            {
                reader.Close();
                conn.Close();
            }
            ExceptionLog.ExceptionWrite("本次reader查询用时" + (DateTime.Now - dtstart).TotalMilliseconds+"查询到记录数"+dt.Rows.Count);
            return dt;


        }
    

        /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool querystanderdb(string sql, int xmno, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            dt = new DataTable();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            try
            {
                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                return false;
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return true;

        }

        /// <summary>
        /// 使用mysql查询转换GNSS表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool queryremotedb(string sql, string dbname,out DataTable dt)
        {
            OdbcConnection conn = db.GetRemoteConn(dbname);
            dt = new DataTable();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            try
            {
                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                return false;
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return true;

        }



        /// <summary>
        /// 使用mysql查询转换标准表/目标表的list
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static List<string> querystanderlist(string sql, OdbcConnection conn)
        {
            List<string> strl = new List<string>();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OdbcCommand ocmd = new OdbcCommand(sql, conn);
                OdbcDataReader reader = ocmd.ExecuteReader();
                while (reader.Read())
                {
                    string strh = "";
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        strh += reader[i].ToString() + ",";
                    }
                    strh = strh.Substring(0, strh.LastIndexOf(","));
                    strl.Add(strh);
                }
            }
            catch (Exception ex)
            {
                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return strl;
        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static string querystanderstr(string sql, OdbcConnection conn)
        {
            ExceptionLog.ExceptionWrite(sql);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            try
            {
                OdbcCommand cmd = new OdbcCommand(sql, conn);

                OdbcDataReader read = cmd.ExecuteReader();

                if (read.Read())
                {
                    return read[0].ToString();
                }

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex+""+sql);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
            return null;
        }

        /// <summary>
        /// 使用access查询表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable querytaccesstdb(string sql)
        {
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            DataTable dt = new DataTable();
            try
            {
                OleDbDataAdapter oda = new OleDbDataAdapter(sql, conn);

                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }

        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool querystanderstr(string sql, string xmname, out string str)
        {
            str = "";
            OdbcConnection conn = db.GetStanderConn(xmname);
            if (conn.State != ConnectionState.Open)
                conn.Open();

            OdbcCommand cmd = new OdbcCommand(sql, conn);

            OdbcDataReader read = cmd.ExecuteReader();

            if (read.Read())
            {
                str = read[0].ToString();
                return true;
            }


            return false;
        }

        /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool querysqlserverstanderdb(string sql,string dbname, out DataTable dt)
        {
            SqlConnection conn = TargetBaseLink.GetSqlConnect(dbname);
            dt = new DataTable();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            SqlDataAdapter oda = new SqlDataAdapter(sql, conn);
            try
            {
                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                //ExceptionLog.ExceptionWrite(ex+"sql语句:"+sql);
                return false;
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return true;

        }



    }
}