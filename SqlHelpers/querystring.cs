﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
namespace SqlHelpers
{
    /// <summary>
    /// 对于空串的查询条件处理方式
    /// </summary>
    public class querystring
    {
        public string querykey;
        public static bool  querynvl(string queryname,string queryword/*查询的字段名*/,string op,string inserth,string insertt,out string str)
        { 
         
            if (queryword == "")
            {
                str = "1 = 1";
                return true;
            }
            if ((inserth == ""||inserth==null)&&(insertt == ""||insertt==null))
            {
                str = queryname + op +" '"+queryword+"'";
                return true;
            }
            else {
                str = queryname+op+inserth + queryword + insertt;
                return true;
            }
            
        }
    }
}