﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using Tool;
namespace SqlHelpers
{
    public class sqlhelper
    {
        //读取表中的字段填充对象
        public  List<List<string>> SetEntrlFromData(string[] param,DataTable dt)
        {
            DataView dv = new DataView(dt);
            List<List<string>> lls = new List<List<string>>();
            try
            {
                foreach (DataRowView drv in dv)
                {

                    int i = 0;
                    List<string> lsVal = new List<string>();
                    for (i = 0; i < param.Length; i++)
                    {
                        lsVal.Add(drv[param[i].Trim()].ToString());
                    }
                    lls.Add(lsVal);
                }
                return lls;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("填充对象出错:"+ex.StackTrace.ToString());
                return null;
            }
            
        }
        //插入表中的记录------!!此处可能存在参数类型不匹配错误，慎用
        public int InsertEntrl(string table,string[]param,string[] condition,OdbcConnection conn)
        {
            string paramstr = string.Join(",",param);
            string conditionStr = "'"+string.Join("','",condition)+"'";
            string sql = "insert into ? (#_p)value(#_c)";
            sql = sql.Replace("?",table);
            sql = sql.Replace("#_p",paramstr);
            sql = sql.Replace("#_c",conditionStr);
            updatedb udb =new updatedb();
           return udb.UpdateStanderDB(sql,conn);
            
        }
        //更新表中的记录------!!此处可能存在参数类型不匹配错误，慎用
        public void UpdateEntrl(string table, string[] param,string[] paraVal ,string[] condition,string[] conditionVal ,OdbcConnection conn)
        {
            List<string> paramList = new List<string>();
            int i =0;
            for(i = 0;i < param.Length;i++)
            {
                string paramOption = string.Format("{0}='{1}'", param[i], paraVal[i]);
                paramList.Add(paramOption);
            }
            string paramstr = string.Join(",",paramList);
            List<string> conditionList = new List<string>();
            for (i = 0; i < condition.Length;i++ )
            {
                string conditionOption = string.Format("{0}='{1}'", condition[i], conditionVal[i]);
                conditionList.Add(conditionOption);
            }
            string conditionstr = string.Join(" and ", conditionList);
            string sql = "update  ? set #_p where #_c";
            sql = sql.Replace("?", table);
            sql = sql.Replace("#_p", paramstr);
            sql = sql.Replace("#_c", conditionstr);
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);

        }
        //更新表中的记录------!!此处可能存在参数类型不匹配错误，慎用
        public void UpdateEntrlById(string table, string[] param, string[] paraVal, string[] condition, string[] conditionVal, OdbcConnection conn)
        {
            List<string> paramList = new List<string>();
            int i = 0;
            for (i = 0; i < param.Length; i++)
            {
                string paramOption = string.Format("{0}='{1}'", param[i], paraVal[i]);
                paramList.Add(paramOption);
            }
            string paramstr = string.Join(",", paramList);
            List<string> conditionList = new List<string>();
            for (i = 0; i < condition.Length; i++)
            {
                string conditionOption = string.Format("{0} in ({1})", condition[i], conditionVal[i]);
                conditionList.Add(conditionOption);
            }
            string conditionstr = string.Join(",", conditionList);
            string sql = "update  ? set #_p where #_c";
            sql = sql.Replace("?", table);
            sql = sql.Replace("#_p", paramstr);
            sql = sql.Replace("#_c", conditionstr);
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);

        }
        //更新access表中的记录
        public void UpdateAccessEntrlById(string table, string[] param, string[] paraVal, string[] condition, string[] conditionVal)
        {
            List<string> paramList = new List<string>();
            int i = 0;
            for (i = 0; i < param.Length; i++)
            {
                string paramOption = string.Format("{0}='{1}'", param[i], paraVal[i]);
                paramList.Add(paramOption);
            }
            string paramstr = string.Join(",", paramList);
            List<string> conditionList = new List<string>();
            for (i = 0; i < condition.Length; i++)
            {
                string conditionOption = string.Format("{0} in ({1})", condition[i], conditionVal[i]);
                conditionList.Add(conditionOption);
            }
            string conditionstr = string.Join(",", conditionList);
            string sql = "update  ? set #_p where #_c";
            sql = sql.Replace("?", table);
            sql = sql.Replace("#_p", paramstr);
            sql = sql.Replace("#_c", conditionstr);
            updatedb udb = new updatedb();
            udb.updateaccess(sql);

        }
        //删除表中的记录------!!此处可能存在参数类型不匹配错误
        public void DelEntrl(string table, string[] condition, string[] conditionVal, OdbcConnection conn)
        {
            int i = 0;
            List<string> conditionList = new List<string>();
            for (i = 0; i < condition.Length; i++)
            {
                string conditionOption = string.Format("{0}='{1}'", condition[i], conditionVal[i]);
                conditionList.Add(conditionOption);
            }
            string conditionstr = string.Join(",", conditionList);
            string sql = "delete from ? where #_c";
            sql = sql.Replace("?", table);
            sql = sql.Replace("#_c", conditionstr);
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);

        }
        public bool PointNameExist(string tabName, string xmname, string pointKeyWord, string pointName)
        {

            database db = new database();
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = "select count(*) from " + tabName + " where " + pointKeyWord + "='" + pointName + "'";
            string num = querysql.querystanderstr(sql, conn);
            if (num == "0")
            {
                return false;
            }
            return true;
        }
        public bool PointNameExist(string tabName, string xmname, string[] pointKeyWord, string[] pointName)
        {

            database db = new database();
            OdbcConnection conn = db.GetStanderConn(xmname);
            int i = 0;
            List<string> paramValMap = new List<string>();
            for (i = 0; i < pointName.Length;i++ )
            {
                paramValMap.Add(pointKeyWord[i]+"='"+pointName[i]+"'");
            }
            string paramValMapStr = string.Join(" and  ",paramValMap);
            string sql = "select count(*) from " + tabName + " where "+paramValMapStr;
            string num = querysql.querystanderstr(sql, conn);
            if (num == "0")
            {
                return false;
            }
            return true;
        }
        //判断系统数据表中指定的字段的记录是否存在
        public bool PointNameExist(string tabName,string[] pointKeyWord, string[] pointName)
        {

            database db = new database();
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect("fmosjc") ;
            int i = 0;
            List<string> paramValMap = new List<string>();
            for (i = 0; i < pointName.Length; i++)
            {
                paramValMap.Add(pointKeyWord[i] + "='" + pointName[i] + "'");
            }
            string paramValMapStr = string.Join(" and  ", paramValMap);
            string sql = "select count(*) from " + tabName + " where " + paramValMapStr;
            string num = querysql.querystanderstr(sql, conn);
            if (num == "0")
            {
                return false;
            }
            return true;
        } 
        //根据工控机的编号获取ID
        public string GetIPCID(string IPCNo,string xmname)
        {
            database db = new database();
            string sql = "select id from ipc where ipcno = '" + IPCNo + "'";
            OdbcConnection conn = db.GetStanderConn(xmname);
            if (conn == null)
            {
                string a = "";
            }
            string id = querysql.querystanderstr(sql, conn);
            return id;
        }
        //根据工控机的编号ID获取工控机编号
        public string GetIPCNo(string IPCID,string xmname)
        {
            database db = new database();
            string sql = "select IPCNO from ipc where id = '" + IPCID + "'";
            OdbcConnection conn = db.GetStanderConn(xmname);
            if (conn == null) { 
                string a = "";
            }
            string id = querysql.querystanderstr(sql, conn);
            return id;
        }
        //根据组名获取组ID
        public string GetTeamID(string teamName,string IPCNo, string xmname)
        {
            database db = new database();
            string sql = "select id from team where ipcno = '" + GetIPCID(IPCNo,xmname) + "' and teamName ='"+teamName+"' ";
            OdbcConnection conn = db.GetStanderConn(xmname);
            if (conn == null)
            {
                string a = "";
            }
            string id = querysql.querystanderstr(sql, conn);
            return id;
        }
        //获取表的最大ID
        public int CreateIdOfTableName(string tabname,OdbcConnection conn)
        {
            string sql = "select max(id) from  "+tabname;
            string id = querysql.querystanderstr(sql,conn);
            
            return id == null||id==""?0:Int32.Parse(id) + 1;
        }
        ////将返回的发送状态转换为字符
        //public string RequestReturnTranst(RequestSend sendresult)
        //{
        //    switch (sendresult)
        //    {
        //        case RequestSend.SEND_SUCCESS:
        //            return "成功";
        //        case RequestSend.SEND_FAILED:
        //            return "失败";
        //        default: return "不确定";
        //    }

        //}
        
    }
}