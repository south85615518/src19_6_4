﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using Tool;

namespace SqlHelpers
{
    public class updatedb
    {
        public static accessdbse accessdb = new accessdbse();
        public static database db = new database();

        /// <summary>
        /// mysql数据库插入
        /// </summary>
        /// <param name="sql"></param>
        public int UpdateStanderDB(string sql, OdbcConnection conn)
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                int line = ocmd.ExecuteNonQuery();
                string i = "";
                ot.Commit();
                Console.WriteLine("更新"+line+"条记录成功");
                return line;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                ot.Rollback();//如操作失败，则事务回滚
                //throw (ex);
            }
            //提交事务
            //ot.Rollback();
            finally
            {
                ot.Dispose();
                conn.Close();
            }
            return 0;
        }
        /// <summary>
        /// mysql数据库插入
        /// </summary>
        /// <param name="sql"></param>
        public int UpdateStanderDB(string sql, List<MySqlParameter> lMP, MySqlConnection conn)
        {
            int i = 0;
            int line = 0;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            for (i = 0; i < lMP.Count; i++)
            {
                cmd.Parameters.Add(lMP[i]);
            }
            MySqlTransaction ot = conn.BeginTransaction();
            try
            {
                line = cmd.ExecuteNonQuery();
                ot.Commit();
                return line;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                ot.Rollback();//如操作失败，则事务回滚
                throw (ex);
            }
            //提交事务
            //ot.Rollback();
            finally
            {
                ot.Dispose();
                conn.Close();
            }
            return 0;
        }
        /// <summary>
        /// 标准数据库转库正确性验证
        /// </summary>
        /// <param name="sql"></param>
        public bool UpdateTranstStanderDB(string sql, OdbcConnection conn, int lines)
        {
            bool result = false;
            int line = 0;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                ot.Dispose();
                conn.Close();
                return result;
                //string i = "";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                ot.Rollback();//如操作失败，则事务回滚
                ot.Dispose();
                conn.Close();
                return false;
                throw (ex);
            }




        }
        /// <summary>
        /// mysql数据库插入
        /// </summary>
        /// <param name="sql"></param>
        public int insertdb(string sql)
        {
            int line;
            OdbcConnection conn = db.getdbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                string i = "";
                ot.Commit();
                return 0;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                ot.Rollback();//如操作失败，则事务回滚
                return -1;
                throw (ex);
            }
            finally
            {
                ot.Dispose();
                conn.Close();
            }
        }
        /// <summary>
        /// 更新access数据库
        /// </summary>
        public int updateaccess(string sql)
        {
            int line = 0;
            OleDbConnection conn = accessdb.getconn();
            if (conn.State != ConnectionState.Open) conn.Open();
            OleDbCommand ocmd = new OleDbCommand(sql, conn);
            OleDbTransaction ot = conn.BeginTransaction();//开始事务
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                ot.Commit();
                return line;
            }
            catch (Exception e)
            {
                ExceptionLog.ExceptionWrite(e);
                ot.Rollback();
                return -1;
                throw (e);
            }
            finally
            {
                ot.Dispose();
                conn.Close();
            }
            


        }

    }
}