﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Tool;
namespace SqlHelpers
{
    /// <summary>
    /// 根据sql语句查询并返回值
    /// </summary>
    public class querybkgsql
    {

        public static BKGaccessdbse adb = new BKGaccessdbse();
        /// <summary>
        /// 使用access查询字符串
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string queryaccessdbstring(string sql)
        {
            string seg = "";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OleDbCommand ocmd = new OleDbCommand(sql, conn);
                OleDbDataReader reader = ocmd.ExecuteReader();
                if (reader.Read())
                {
                    seg = reader[0].ToString();
                }
                reader.Read();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
            return seg;
        }
        /// <summary>
        /// 使用access查询字符串
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string queryaccessdbstring(string sql, OleDbConnection conn)
        {
            string seg = "";
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand ocmd = new OleDbCommand(sql, conn);
            OleDbDataReader reader = ocmd.ExecuteReader();
            if (reader.Read())
            {
                seg = reader[0].ToString();
            }
            reader.Read();
            conn.Close();
            return seg;
        }

        /// <summary>
        /// 使用access查询list
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static List<string> queryaccesslist(string sql)
        {
            ExceptionLog.ExceptionWrite(sql);
            List<string> strl = new List<string>();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OleDbCommand ocmd = new OleDbCommand(sql, conn);
                OleDbDataReader reader = ocmd.ExecuteReader();
                while (reader.Read())
                {
                    string strh = "";
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        strh += reader[i].ToString() + ",";
                    }
                    strh = strh.Substring(0, strh.LastIndexOf(","));
                    strl.Add(strh);
                }
            }
            catch (Exception ex)
            {

                ExceptionLog.ExceptionWrite(ex);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }

            return strl;
        }

        /// <summary>
        /// 使用access查询表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable querytaccesstdb(string sql)
        {
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            DataTable dt = new DataTable();
            try
            {
                OleDbDataAdapter oda = new OleDbDataAdapter(sql, conn);

                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }
        /// <summary>
        /// 使用access查询表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable querytbkgtdb(string sql,List<string> lsname)
        {
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            DataTable dt = new DataTable();
            OleDbCommand cmd = new OleDbCommand(sql,conn);
            OleDbDataReader read = cmd.ExecuteReader();
            DataRow drtmp = null;
            int i = 0;
            foreach(string colname in lsname)
            {
                dt.Columns.Add(colname);
            }
            try
            {
                while (read.Read())
                {

                    try
                    {
                        //ExceptionLog.ExceptionWrite( "第" + i + "行");
                        DataRow dr = dt.NewRow();
                        dr[0] = read[0];
                        dr[1] = read[1];
                        dr[2] = read[2];
                        dr[3] = read[3];
                        dt.Rows.Add(dr);
                        drtmp = dr;
                        i++;
                    }
                    catch (Exception ex)
                    {
                        ExceptionLog.ExceptionWrite(ex + "第" + i + "行");
                        List<string> exrow = new List<string>();
                        foreach (string name in lsname)
                        {
                            exrow.Add(string.Format("{0}:{1}", name, drtmp[name]));
                        }
                        ExceptionLog.ExceptionWrite("上一行：" + string.Join(",", exrow));
                        //List<string> exrow = new List<string>();
                        //foreach (string name in lsname)
                        //{
                        //    exrow.Add(string.Format("{0}:{1}",name,read[name]));
                        //}
                        //ExceptionLog.ExceptionWrite(string.Join(",",exrow));
                    }
                }

            }
            catch (Exception ex)
            {
                //List<string> exrow = new List<string>();
                //foreach (string name in lsname)
                //{
                //    exrow.Add(string.Format("{0}:{1}", name, drtmp[name]));
                //}
                //ExceptionLog.ExceptionWrite("上一行："+string.Join(",", exrow));
                ExceptionLog.ExceptionWrite(ex);
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }
       

    }
}