﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;
using Tool;
namespace 阳江不动产
{
    public class ConntectExcel
    {
        private string path;
        private string sheetName;//工作表名，不指定默认为第一个工作表
        /// <summary>
        /// Excel 版本
        /// </summary>
        public enum ExcelType
        {
            Excel2003,
            Excel2007
        }
        /// <summary>
        /// IMEX 三种模式。
        /// IMEX是用来告诉驱动程序使用Excel文件的模式，其值有0、1、2三种，分别代表导出、导入、混合模式。
        /// </summary>
        public enum IMEXType
        {
            ExportMode = 0,
            ImportMode = 1,
            LinkedMode = 2
        } 

        public string Path
        {
            get { return path; }
            set { path = value; }
        }
        public ConntectExcel(string path)
        {
            this.path = path;
        }
        public DataTable ExcelImport(string path,string sql)
        {
            OleDbConnection oleCon = null;
            try
            {
                ExcelType et = GetExcelPulish(this.path);
                string conn = GetExcelConnectstring(this.path, false, et, IMEXType.ImportMode);
                //string sql = "";
                //获取excel第一个工作簿的名称
                oleCon = new OleDbConnection(conn);
                if (oleCon.State != ConnectionState.Open)
                    oleCon.Open();
                GetExcelName();
                sql = sql.Replace("#_tab", " [" + this.sheetName + "] ");
                //sql = "select * from [" + this.sheetName + "] ";
                DataTable dt = new DataTable();
                dt = GetAccessTab(sql, oleCon);
                return dt;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return null;
            }
            finally {
                oleCon.Close();
            }
        }
        /// <summary>
        /// 根据分页的起始位置和行数查询Access表记录
        /// </summary>
        public static DataTable GetAccessTab(string sql,OleDbConnection conn)
        {
            //读取转库映射方案生成标准表
            if (sql == "select * from []")
            {
                return null;
            }
            //将标准表的列的类型全部转换成字符型
           
            if (conn.State != ConnectionState.Open) conn.Open();
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            //OleDbDataReader reader = cmd.ExecuteReader();
            OleDbDataAdapter ad = new OleDbDataAdapter(sql,conn);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            return dt;
        }
        public void GetExcelName()//此处可以指定工作表名
        {
            ExcelType et = GetExcelPulish(this.path);
            string conn = GetExcelConnectstring(this.path, false, et, IMEXType.ImportMode);

            OleDbConnection oleCon = new OleDbConnection(conn);
            if(oleCon.State != ConnectionState.Open)
            oleCon.Open();
            List<string> sheetNameList = GetSheetName(oleCon);
            string sql = "";
            foreach(string sheetName in sheetNameList)
            {
                DataTable dt = new DataTable();
                
                sql = "select top 1 * from ["+sheetName+"] ";
                OleDbDataAdapter oad = new OleDbDataAdapter(sql,oleCon);
                oad.Fill(dt);
                if (dt.Columns.Count > 1)
                {
                    string sqlCount = "select count(*) from [" + sheetName + "] ";
                    //allCount = GetStanderTab.GetAccessTabCount(sqlCount,oleCon);
                    this.sheetName = sheetName;
                    break;
                }
            }
            oleCon.Close();
            
            
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<string> GetSheetName(OleDbConnection oleCon)
        {
            
            DataTable dt = oleCon.GetSchema("Tables");
            DataView dv = new DataView(dt);
            List<string> tabnameList = new List<string>();
            foreach (DataRowView drv in dv)
            {
                tabnameList.Add(drv["TABLE_NAME"].ToString());
            }
            return tabnameList;
        }
        
        /// <summary>
        /// 获取excel版本
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public ExcelType GetExcelPulish(string path)
        {
            if (path.IndexOf(".xlsx") == -1)
            {
                return ExcelType.Excel2003;
            }
           
                return ExcelType.Excel2007;
            
        }

        /// <summary>
        /// 返回Excel 连接字符串
        /// </summary>
        /// <param name="excelPath">Excel文件 绝对路径</param>
        /// <param name="header">是否把第一行作为列名</param>
        /// <param name="eType">Excel 版本 </param>
        /// <param name="imex">IMEX模式</param>
        /// <returns>返回值</returns>
        public static string GetExcelConnectstring(string excelPath, bool header, ExcelType eType, IMEXType imex)
        {
            string connectstring;
            header = true;
            string hdr = "NO";
            if (header)
                hdr = "YES";

            if (eType == ExcelType.Excel2003)
                connectstring = "Provider=Microsoft.Jet.OleDb.4.0; data source=" + excelPath +
                                ";Extended Properties='Excel 8.0; HDR=" + hdr + "; IMEX=" + imex.GetHashCode() + "'";
            else
                connectstring = "Provider=Microsoft.ACE.OLEDB.12.0; data source=" + excelPath +
                                ";Extended Properties='Excel 12.0 Xml; HDR=" + hdr + "; IMEX=" + imex.GetHashCode() +
                                "'";

            return connectstring;
        }

    }
}