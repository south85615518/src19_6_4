﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SqlHelpers;
using MySql.Data.MySqlClient;
using System.IO;
using Tool;

namespace SqlHelpers.dbcreate
{
    /// <summary>
    /// 根据项目名称创建数据库
    /// </summary>
    public class dbcreate
    {
        public database db = new database();
        //创建水位监测数据库
        public int createdb(string dbname/*数据库名称*/)
        {
            updatedb udb = new updatedb();
            string sql = @"Create Database If Not Exists "+dbname+" Character Set GBK";
            return udb.insertdb(sql);
             
        }
        public bool DPSSQLScript(string scriptPath ,out string mssg)
        {
            try
            {

                MySqlConnection conn = db.GetDPSMYSQLConn();
                FileInfo file = new FileInfo(scriptPath);  //filename是sql脚本文件路径。  
                string sql = file.OpenText().ReadToEnd();
                MySqlScript script = new MySqlScript(conn);
                script.Query = sql;
                int count = script.Execute();
                mssg = string.Format("运行脚本程序{0}成功!", Path.GetFileName(scriptPath));
                ExceptionLog.ExceptionWrite(mssg);
                return true;
            }
            catch (Exception ex)
            {
                mssg = string.Format("运行脚本程序{0}出错，错误信息:"+ex.Message, Path.GetFileName(scriptPath));
                ExceptionLog.ExceptionWrite(mssg);
                return false;
            }
        }
    }
}