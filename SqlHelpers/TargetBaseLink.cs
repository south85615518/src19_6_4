﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data;
using MySql.Data.MySqlClient;
using Tool;
namespace SqlHelpers
{
    public class TargetBaseLink
    {
        /// <summary>
        /// sql server 目标数据库连接
        /// </summary>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public static SqlConnection GetSqlConnect(string DBName)
        {
            string sql = "select * from LinkAttr where LinkVia = '是' and engine = 'sqlserver'";
            DataTable dt = new DataTable();
            querysql query = new querysql();
            dt = query.querytaccesstdb(sql);
            DataView dv = new DataView(dt);
            SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            foreach (DataRowView drv in dv)
            {
                //scsb.DataSource = @"pc-PC\SQLEXPRESS";
                ////scsb.IntegratedSecurity = true;
                ////scsb.InitialCatalog = DBName;
                ////scsb.UserID = drv["uname"].ToString();
                ////scsb.Password = drv["pass"].ToString();
                //scsb.DataSource = DBName;
                //scsb.Trusted_Connection = "SSPI";
                break;
            }

            //获取到系统的默认数据库连接属性
            if (scsb.UserID != "")
            {
                scsb.IntegratedSecurity = true;
            }
            else
            {
                scsb.IntegratedSecurity = false;
            }
            SqlConnection myConnection = new SqlConnection("server=pc-PC\\SQLEXPRESS;database="+DBName+";Trusted_Connection=SSPI");
            if (myConnection.State != ConnectionState.Open)
                myConnection.Open();
            return myConnection;
        }
        /// <summary>
        /// 使用数据库名连接MYSQL数据库
        /// </summary>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public static OdbcConnection GetOdbcConnect(string DBName)
        {
            //ExceptionLog.ExceptionWrite("开始获取连接信息...");
            string constr = "";
            constr = constr.Replace("#", DBName);
            string sql = "select * from LinkAttr where LinkVia = '是' and engine = 'mysql'";
            DataTable dt = new DataTable();
            querysql query = new querysql();
            dt = query.querytaccesstdb(sql);
            DataView dv = new DataView(dt);
            foreach (DataRowView drv in dv)
            {
                constr = "Driver={MySQL ODBC 5.3 ANSI Driver};database=#_1;uid=#_2;password=#_3;server=#_5;port=#_4";//发布开发版
                constr = constr.Replace("#_1", DBName);
                constr = constr.Replace("#_2", drv["uname"].ToString());
                constr = constr.Replace("#_3", drv["pass"].ToString());
                constr = constr.Replace("#_4", drv["Port"].ToString());
                constr = constr.Replace("#_5", drv["sevice"].ToString());
            }
            OdbcConnection conn = new OdbcConnection(constr);
            //ExceptionLog.ExceptionWrite("开始连接..."+constr);
            if (conn.State != ConnectionState.Open)
            {
                try
                {
                    //ExceptionLog.ExceptionWrite("开始连接...");
                    conn.Open();
                    //ExceptionLog.ExceptionWrite("数据库连接成功...");
                }
                catch (Exception ex)
                {
                    //ExceptionLog.ExceptionWrite("数据库连接出错...");
                    ExceptionLog.ExceptionWrite(ex.Message + "MYSQL数据库连接出错");
                    return null;
                    throw(ex);
                }
            }
            //System.ExceptionLog.ExceptionWrite("数据库连接成功!\n");
            return conn;


        }

        /// <summary>
        /// 使用数据库名连接GPS数据库
        /// </summary>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public static OdbcConnection GetRemoteConnect(string DBName)
        {

            string constr = "";
            constr = constr.Replace("#", DBName);
            string sql = "select * from LinkAttr where LinkVia = '是' and engine = 'remote'";
            DataTable dt = new DataTable();
            querysql query = new querysql();
            dt = query.querytaccesstdb(sql);
            DataView dv = new DataView(dt);
            foreach (DataRowView drv in dv)
            {
                constr = "Driver={MySQL ODBC 5.3 ANSI Driver};database=#_1;uid=#_2;password=#_3;server=#_5;port=#_4";//发布开发版
                constr = constr.Replace("#_1", DBName);
                constr = constr.Replace("#_2", drv["uname"].ToString());
                constr = constr.Replace("#_3", drv["pass"].ToString());
                constr = constr.Replace("#_4", drv["Port"].ToString());
                constr = constr.Replace("#_5", drv["sevice"].ToString());
            }
            OdbcConnection conn = new OdbcConnection(constr);
            if (conn.State != ConnectionState.Open)
            {
                try
                {
                    conn.Open();

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex.Message + "GNSS数据库连接出错");
                    return null;
                    throw (ex);
                }
            }
            //System.Console.WriteLine("数据库连接成功!\n");
            return conn;


        }


        /// <summary>
        /// 使用数据库名连接MYSQL数据库
        /// </summary>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public static OdbcConnection GetDPSConnect(string settingName)
        {


            List<string> settingLs = FileHelper.ProcessFileInfoList(string.Format(System.AppDomain.CurrentDomain.BaseDirectory + "DBSetting/{0}setting.txt",settingName));
                string constr = "Driver={MySQL ODBC 5.3 ANSI Driver};database=#_1;uid=#_2;password=#_3;server=#_5;port=#_4";//发布开发版
                constr = constr.Replace("#_1", settingLs[4]);
                constr = constr.Replace("#_2", settingLs[1]);
                constr = constr.Replace("#_3", settingLs[2]);
                constr = constr.Replace("#_4", settingLs[3]);
                constr = constr.Replace("#_5", settingLs[0]);
          
            OdbcConnection conn = new OdbcConnection(constr);
            if (conn.State != ConnectionState.Open)
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex.Message + "MYSQL数据库连接出错");
                    return null;
                    throw (ex);
                }
            }
           
            return conn;


        }

        /// <summary>
        /// 使用数据库名连接MYSQL数据库
        /// </summary>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public static MySqlConnection GetDPSMYSQLConnect(string settingName)
        {


            List<string> settingLs = FileHelper.ProcessFileInfoList(string.Format(System.AppDomain.CurrentDomain.BaseDirectory + "DBSetting/{0}setting.txt", settingName));
            string constr = "database=#_1;uid=#_2;password=#_3;server=#_5;port=#_4";//发布开发版
            constr = constr.Replace("#_1", settingLs[4]);
            constr = constr.Replace("#_2", settingLs[1]);
            constr = constr.Replace("#_3", settingLs[2]);
            constr = constr.Replace("#_4", settingLs[3]);
            constr = constr.Replace("#_5", settingLs[0]);

            MySqlConnection conn = new MySqlConnection(constr);
            if (conn.State != ConnectionState.Open)
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex.Message + "MYSQL数据库连接出错");
                    return null;
                    throw (ex);
                }
            }

            return conn;


        }



        /// <summary>
        /// 使用数据库名连接MYSQL数据库
        /// </summary>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public static MySqlConnection GetMysqlConnect(string DBName)
        {

            string constr = "";
            constr = constr.Replace("#", DBName);
            string sql = "select * from LinkAttr where LinkVia = '是' and engine = 'mysql'";
            DataTable dt = new DataTable();
            querysql query = new querysql();
            dt = query.querytaccesstdb(sql);
            DataView dv = new DataView(dt);
            foreach (DataRowView drv in dv)
            {
                constr = "database=#_1;uid=#_2;password=#_3;server=#_5;port=#_4";//发布开发版
                constr = constr.Replace("#_1", DBName);
                constr = constr.Replace("#_2", drv["uname"].ToString());
                constr = constr.Replace("#_3", drv["pass"].ToString());
                constr = constr.Replace("#_4", drv["Port"].ToString());
                constr = constr.Replace("#_5", drv["sevice"].ToString());
            }
            MySqlConnection conn = new MySqlConnection(constr);
            if (conn.State != ConnectionState.Open)
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex.Message + "MYSQL数据库连接出错");
                    return null;
                    throw(ex);
                }
            }
            System.Console.WriteLine("数据库连接成功!\n");
            return conn;

        }
    }
}