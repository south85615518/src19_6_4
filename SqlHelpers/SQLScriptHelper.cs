﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.IO;
using Tool;
using System.Data;

namespace SqlHelpers
{
    public class SQLScriptHelper
    {
        public static database db = new database();
        public static bool SQLScriptExcute(string xmname,string sql,out string mssg)
        {
            string dbname = "";
            try
            {
                OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
                //MySqlConnection conn = db.GetMySqlConn(xmname,out dbname);
                //FileInfo file = new FileInfo(path);  //filename是sql脚本文件路径。  
                //string sql = file.OpenText().ReadToEnd();
                dbname = OdbcSQLHelper.Conn.Database;
                //MySqlScript script = new MySqlScript(conn);
                //script.Query = sql;
                //int count = script.Execute();
                OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,sql);
                mssg = string.Format("在项目{0}的项目数据库{2}上执行\r\n{1}\r\n成功!", xmname, sql, OdbcSQLHelper.Conn.Database);
               
                ExceptionLog.DBUWrite(mssg);
            }
            catch(Exception ex)
            {
                mssg = string.Format("在项目{0}的项目数据库上{2}执行\r\n{1}\r\n出错,错误信息:"+ex.Message, xmname, sql,dbname);
                ExceptionLog.DBUWrite(mssg);
            }
            try
            {
                //OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
                //MySqlConnection conn = db.GetMySqlConn(xmname,out dbname);
                //FileInfo file = new FileInfo(path);  //filename是sql脚本文件路径。  
                //string sql = file.OpenText().ReadToEnd();

                //MySqlScript script = new MySqlScript(conn);
                //script.Query = sql;
                //int count = script.Execute();
                OdbcSQLHelper.Conn = db.GetStanderCgConn(xmname);
                dbname = OdbcSQLHelper.Conn.Database;
                OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, sql);
                mssg = string.Format("\r\n在项目{0}的项目数据库{2}上执行\r\n{1}\r\n成功!", xmname, sql, OdbcSQLHelper.Conn.Database);
                ExceptionLog.DBUWrite(mssg);
                return true;
            }
            catch (Exception ex)
            {
                mssg = string.Format("在项目{0}的项目数据库上{2}执行\r\n{1}\r\n出错,错误信息:" + ex.Message, xmname, sql, dbname);
                ExceptionLog.DBUWrite(mssg);
                return false;
            }
        }
    }
}
