﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data;
using MySql.Data.MySqlClient;
using Tool;
namespace SqlHelpers
{
    /// <summary>
    /// 连接数据库
    /// </summary>D:\工具\安装环境\数据备份\gps发布版\DAY02\css\
    public class database
    {
        /// <summary>
        /// 根据项目名称获取到该项目所属数据库的连接语句
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetXmConn(string dataBaseName)
        {
            return TargetBaseLink.GetOdbcConnect(dataBaseName);
        }
        /// <summary>
        /// 根据项目名称获取到该项目所属数据库的连接语句
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetRemoteConn(string dataBaseName)
        {
            return TargetBaseLink.GetRemoteConnect(dataBaseName);
        }
        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetStanderConn(string xmname)
        {
            string dbname = "";
            if (SqlHelperWeb.ProcessWebSession.IsSurveyBase())
                dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno =" + xmname );
            else
                dbname = querysql.queryaccessdbstring("select cgdbname from xmconnect where xmno =" + xmname );
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
            //string dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno='" + xmname + "'");
            //OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            //return conn;
        }

        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetSurveyStanderConn(string xmname)
        {
            string dbname = querysql.queryaccessdbstring(string.Format("select dbname from xmconnect where xmno ={0}",xmname));
            
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
            //string dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno='" + xmname + "'");
            //OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            //return conn;
        }

        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetUnitStanderConn(string unitname)
        {
            //string dbname = querysql.queryaccessdbstring(string.Format("select dbname from unit where unitname ='{0}'", unitname));

            string dbname = "";
            if (SqlHelperWeb.ProcessWebSession.IsSurveyBase())
                dbname = querysql.queryaccessdbstring(string.Format("select dbname from unit where unitname ='{0}'", unitname));
            else
                dbname = querysql.queryaccessdbstring(string.Format("select cgdbname from unit where unitname ='{0}'", unitname));
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
            //string dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno='" + xmname + "'");
            //OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            //return conn;
        }
        public OdbcConnection GetUnitSurveyStanderConn(string unitname)
        {
            string dbname = querysql.queryaccessdbstring(string.Format("select dbname from unit where unitname ='{0}'", unitname));

            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
            //string dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno='" + xmname + "'");
            //OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            //return conn;
        }


        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetDPSConn()
        {

            OdbcConnection conn = TargetBaseLink.GetDPSConnect("DPS");
            return conn;
        }

        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public MySqlConnection GetDPSMYSQLConn()
        {

            MySqlConnection conn = TargetBaseLink.GetDPSMYSQLConnect("DPS");
            return conn;
        }


        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetSMOSConn()
        {

            OdbcConnection conn = TargetBaseLink.GetDPSConnect("Smos");
            return conn;
        }
        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetStanderConn(int xmno)
        {
            string dbname = "";
            if(SqlHelperWeb.ProcessWebSession.IsSurveyBase())
            dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno =" + xmno);
            else
            dbname = querysql.queryaccessdbstring("select cgdbname from xmconnect where xmno =" + xmno);
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
        }
        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetSurveyStanderConn(int xmno)
        {
            string dbname =  querysql.queryaccessdbstring("select dbname from xmconnect where xmno =" + xmno);
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
        }
        /// <summary>
        /// 根据项目名称获取标准成果数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetStanderCgConn(string xmname)
        {
            //return GetStanderConn(xmname);
            string dbname = querysql.queryaccessdbstring("select cgdbname from xmconnect where xmno=" + xmname );
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
        }
        /// <summary>
        /// 根据项目名称获取标准成果数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetCgStanderConn(string xmname)
        {
            //return GetStanderConn(xmname);
            string dbname = querysql.queryaccessdbstring("select cgdbname from xmconnect where xmno=" + xmname );
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
        }
        /// <summary>
        /// 根据项目名称获取标准成果数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetCgStanderConn(int xmno)
        {
            string dbname = querysql.queryaccessdbstring("select cgdbname from xmconnect where xmno =" + xmno);
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
        }
        /// <summary>
        /// 根据项目名称获取标准成果数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetStanderCgConn(int xmno)
        {
            string dbname = querysql.queryaccessdbstring("select cgdbname from xmconnect where xmno =" + xmno);
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
        }
        ///// <summary>
        ///// 根据项目名称获取标准数据库表的连接器
        ///// </summary>
        ///// <returns></returns>
        //public OdbcConnection GetStanderConn(object xmsign)
        //{
        //    var type = xmsign.GetType();
        //    if(type.ToString())
        //    string dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno='" + xmname + "'");
        //    OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
        //    return conn;
        //}
        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public MySqlConnection GetMySqlStanderConn(string dbname)
        {
          
            MySqlConnection conn = TargetBaseLink.GetMysqlConnect(dbname);
            return conn;
        }
        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public MySqlConnection GetMySqlConn(string xmname,out string dbname)
        {
            dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno=" + xmname );
            MySqlConnection conn = TargetBaseLink.GetMysqlConnect(dbname);
            return conn;
        }
        //截取字符串
        public static string SubString(string str)
        {
            return str.Substring(str.IndexOf(":") + 1);
        }
        public OdbcConnection getdbconn()
        {
            return TargetBaseLink.GetOdbcConnect("mysql");

        }
    }
}