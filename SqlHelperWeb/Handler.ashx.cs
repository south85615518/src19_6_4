﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;

namespace SqlHelperWeb
{
    /// <summary>
    /// Handler 的摘要说明
    /// </summary>
    public class Handler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            ExceptionLog.ExceptionWrite("访问请求");

            var requestparameters = context.Request.Form;
            foreach (var model in requestparameters.Keys)
            {
                ExceptionLog.ExceptionWrite(string.Format("key:{0}-value:{1}", model, context.Request.Form[model.ToString()]));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}