﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Tool;
namespace SqlHelperWeb
{
    public class ProcessWebSession
    {
         


        public static  bool IsSurveyBase()
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null || System.Web.HttpContext.Current.Session["surveybase"] == null) return true;
            object obj = System.Web.HttpContext.Current.Session["surveybase"];
            bool surveybase = Convert.ToBoolean(System.Web.HttpContext.Current.Session["surveybase"]);
            if (surveybase)
            {
               
                ExceptionLog.ExceptionWrite("当前选择的是测量库");
            }
            else
                ExceptionLog.ExceptionWrite("当前选择的是成果库");
            return System.Web.HttpContext.Current.Session["surveybase"] == null ? true : Convert.ToBoolean(System.Web.HttpContext.Current.Session["surveybase"]);


        }
    }
}