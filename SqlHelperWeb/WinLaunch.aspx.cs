﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace SqlHelperWeb
{
    public partial class WinLaunch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public struct CopyDataStruct
        {
            public IntPtr dwData;
            public int cbData;

            [MarshalAs(UnmanagedType.LPStr)]

            public string lpData;
        }

        public const int WM_COPYDATA = 0x004A;
        //当一个应用程序传递数据给另一个应用程序时发送此消息指令

        //通过窗口的标题来查找窗口的句柄 
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        private static extern int FindWindow(string lpClassName, string lpWindowName);

        //在DLL库中的发送消息函数
        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage
            (
            int hWnd,                         // 目标窗口的句柄  
            int Msg,                          // 在这里是WM_COPYDATA
            int wParam,                       // 第一个消息参数
            ref CopyDataStruct lParam        // 第二个消息参数
           );



        protected void Button1_Click(object sender, EventArgs e)
        {
            Tool.com.WinHelper.ExeRestart("D:\\桌面_19_1_1\\12_24\\src_12_24\\src\\NFnetUnitTest\\bin\\Debug\\iisexetest.exe");
            //try
            //{
            //    CallSteven();

            //    //lblMessage.Text = "完成调用";
            //    //lblMessage.ForeColor = Color.Black;
            //}
            //catch (Exception exUpdate)
            //{
            //    //lblMessage.Text = exUpdate.Message.ToString();
            //    //lblMessage.ForeColor = Color.Red;
            //}
        }

          

 //调用可执行文件的方法
      public void CallSteven()
        {
            string strCmd = "";
            DateTime dt = DateTime.Now;
            
             //注意：需要引入System.Diagnostics;
            Process prc = new Process();

            try
            {
                //指定调用的可执行文件
                strCmd += "d:/Desktop/Debug/NFnetUnitTest.exe "; 
   
                //如果可执行文件需要接收参数就加下下面这句，不同参数之间用空格隔开
                //strCmd += 参数1 + " " + 参数2 + " " + 参数n;

                //调用cmd.exe在命令提示符下执行可执行文件
                prc.StartInfo.FileName = "cmd.exe";
                prc.StartInfo.Arguments = " /c " + strCmd;
                prc.StartInfo.UseShellExecute = false;
                prc.StartInfo.RedirectStandardError = true;
                prc.StartInfo.RedirectStandardOutput = true;
                prc.StartInfo.RedirectStandardInput = true;
                prc.StartInfo.CreateNoWindow = false;

                prc.Start();

            }
            catch (Exception exU)
            {
                if (!prc.HasExited)
                {
                    prc.Close();
                }

                throw new Exception(exU.Message.ToString());
            }
        }

    }
}