﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISQLHelper
{
   public interface IPointAlarmValue
    {
        //预警点名是否存在
       public virtual bool Exist();
        //添加预警点
       public virtual bool Add();
        //更新预警点
       public virtual bool Update();
        //批量更新预警点
       public virtual bool MutilUpdate();
        //删除预警点
       public virtual bool Delete();
        //加载所有的预警点名
       public virtual bool PointNameLoad();
        //加载预警点表
       public virtual bool PointTableLoad();
        //加载预警点表记录数
       public virtual bool PointTableCountLoad();
    }
}
