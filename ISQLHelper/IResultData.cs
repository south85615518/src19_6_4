﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISQLHelper
{
   public interface IResultData
    {
        //添加
       public virtual bool Add();
        //更新
       public virtual bool Update();
        //删除临时表
       public virtual bool DeleteTmp();
        //结果数据表加载
       public virtual bool TableLoad();
        //结果数据表记录数加载
       public virtual bool TableCountLoad();
        //获取临时表中的的结果数据实体列表
       public virtual bool GetModelList();
        //获取结果数据的最大日期
       public virtual bool MaxTime();
        //获取点名数据的最大日期
       public virtual bool PointMaxTime();
        //获取点名某一时刻的数据实体
       public virtual bool GetPointTimeModel();
    }
}
