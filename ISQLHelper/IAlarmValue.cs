﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;

namespace ISQLHelper
{
    public interface IAlarmValue
    {
        public database db = new database();
        //预警参数添加
        public virtual bool Add();
        //预警参数编辑
        public virtual bool Update();
        //预警参数删除
        public virtual bool Delete();
        //点号预警级联删除
        public virtual bool Deleteasc();
        //预警参数表加载
        public virtual bool AlarmValueTableLoad();
        //预警参数表记录数加载
        public virtual bool AlarmValueTableCountLoad();
        //预警名称加载 
        public virtual bool AlarmValueNameLoad();
    }
}
