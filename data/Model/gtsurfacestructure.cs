﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacestructure.cs
*
* 功 能： N/A
* 类 名： gtsurfacestructure
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/10/5 14:14:38   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace data.Model
{
	/// <summary>
	/// gtsurfacestructure:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gtsurfacestructure
	{
		public gtsurfacestructure()
		{}
		#region Model
		private int _xmno=0;
		private data.Model.gtsensortype _datatype=0;
		private string _surfacename;
		private string _firstalarmname;
		private string _secondalarmname;
		private string _thirdalarmname;
		private string _remark;
        private double _concreteElasticityModulus;
        private double _externalDiameter;

        public double externalDiameter
        {
            get { return _externalDiameter; }
            set { _externalDiameter = value; }
        }
        private double _thick;

        public double thick
        {
            get { return _thick; }
            set { _thick = value; }
        }
        public double concreteElasticityModulus
        {
            get { return _concreteElasticityModulus; }
            set { _concreteElasticityModulus = value; }
        }
        private double _concreteConversionArea;

        public double concreteConversionArea
        {
            get { return _concreteConversionArea; }
            set { _concreteConversionArea = value; }
        }
        private double _steelElasticityModulus;

        public double steelElasticityModulus
        {
            get { return _steelElasticityModulus; }
            set { _steelElasticityModulus = value; }
        }
        private double _steelConversionArea;

        public double steelConversionArea
        {
            get { return _steelConversionArea; }
            set { _steelConversionArea = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
        public data.Model.gtsensortype datatype
		{
			set{ _datatype=value;}
			get{return _datatype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string surfacename
		{
			set{ _surfacename=value;}
			get{return _surfacename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string firstalarmname
		{
			set{ _firstalarmname=value;}
			get{return _firstalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string secondalarmname
		{
			set{ _secondalarmname=value;}
			get{return _secondalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string thirdalarmname
		{
			set{ _thirdalarmname=value;}
			get{return _thirdalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		
		#endregion Model

	}
}

