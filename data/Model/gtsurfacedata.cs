﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacedata.cs
*
* 功 能： N/A
* 类 名： gtsurfacedata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/10/5 14:14:07   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace data.Model
{
	/// <summary>
	/// gtsurfacedata:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gtsurfacedata
	{
		public gtsurfacedata()
		{}
		#region Model
		private int _xmno;
		private string _surfacename;
		private double _this_val;
		private double _ac_val;
		private double _steelchordval;
        private double _calculatepointsCont;
        private int _cyc;

        public int cyc
        {
            get { return _cyc; }
            set { _cyc = value; }
        }
        public string datatype = data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce) ;
        public double calculatepointsCont
        {
            get { return _calculatepointsCont; }
            set { _calculatepointsCont = value; }
        }
		private DateTime _time;
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string surfacename
		{
			set{ _surfacename=value;}
			get{return _surfacename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_val
		{
			set{ _this_val=value;}
			get{return _this_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_val
		{
			set{ _ac_val=value;}
			get{return _ac_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double SteelChordVal
		{
			set{ _steelchordval=value;}
			get{return _steelchordval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
        
		#endregion Model

	}
}

