﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace data.Model
{
    public class datafileinfo
    {
        public string RecTime { get; set; }
        public int FileLen { get; set; }
        public double[][] Data { get; set; }
        public double MaxVal { get; set; }
        public string FileNo { get; set; }
    }
    public class dataRequestInfo
    {
        public string sn { get; set; }
        public datafileinfo file { get; set; }
        public string Key { get; set; }
        public double[] MaxValue { get; set; }
        public string FileNo { get; set; }
        public string Type { get; set; }
    }
    //public class datafileinfo
    //{
    //    public string RecTime { get; set; }
    //    public int FileLen { get; set; }
    //    public double[][] Data { get; set; }
    //    public double MaxVal { get; set; }
    //    public int FileNo { get; set; }
    //} 
/*"{'sn':1812019035,'file':{'RecTime':'2019-05-22T19:53:36','FileLen':0,'Data':null,'MaxVal':0.0,'FileNo':null},'IsAlert':true}"*/
    public class dataAlert
    {
        public string sn { get; set; }
        public datafileinfo file { get; set; }
        public string IsAlert { get; set; }


    }

    public class vibrationdata
    {
            
    }
    /*
     * {"Data":"{\"item\":{\"chanNum\":4,\"chartData\":\"[],\"day\":27,\"hour\":11,\"maxDataVal\":[0.13708714,0.042993348,0.052095544,0.0066995686,0.0,0.0,0.0,0.0],\"minute\":14,\"month\":5,\"portDatas\":[{\"b\":0,\"full\":7169,\"k\":26.9,\"max\":0.13708714,\"rang\":\"10V\",\"rangIndex\":1,\"res\":0,\"trigLevel\":0.1,\"trigMode\":1,\"unit\":\"m/s\",\"unitIndex\":5,\"zero\":32768},{\"b\":0,\"full\":7170,\"k\":26.9,\"max\":0.042993348,\"rang\":\"10V\",\"rangIndex\":1,\"res\":0,\"trigLevel\":0.1,\"trigMode\":1,\"unit\":\"m/s\",\"unitIndex\":5,\"zero\":32764},{\"b\":0,\"full\":7170,\"k\":26.4,\"max\":0.052095544,\"rang\":\"10V\",\"rangIndex\":1,\"res\":0,\"trigLevel\":0.1,\"trigMode\":1,\"unit\":\"m/s\",\"unitIndex\":5,\"zero\":32764},{\"b\":0,\"full\":7173,\"k\":28,\"max\":0.0066995686,\"rang\":\"10V\",\"rangIndex\":1,\"res\":0,\"trigLevel\":0.1,\"trigMode\":1,\"unit\":\"m/s\",\"unitIndex\":5,\"zero\":32761}],\"recLen\":2,\"sampleRate\":5000,\"second\":32,\"trigDelay\":-100,\"type\":\"4850N\",\"year\":2019},\"ok\":true,\"msg\":\"查询成功\"}","IsSuccess":true,"Message":"调用成功，已请求接口","Model":null,"Code":1}
     */



}
