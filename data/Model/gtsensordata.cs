﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsensordata.cs
*
* 功 能： N/A
* 类 名： gtsensordata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/31 15:15:12   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace data.Model
{
	/// <summary>
	/// gtsensordata:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gtsensordata
	{
		public gtsensordata()
		{}
		#region Model
		private string _project_name;
		private string _point_name;
		private string _senorno;
		private string _datatype;
		private int _valuetype;
		private double _oregion_n;
		private double _oregion_e;
		private double _oregion_z;
		private double _single_oregion_scalarvalue;
		private double _first_oregion_scalarvalue;
		private double _sec_oregion_scalarvalue;
		private double _this_dn;
		private double _this_de;
		private double _this_dz;
		private double _ac_dn;
		private double _ac_de;
		private double _ac_dz;
		private double _single_this_scalarvalue;
		private double _single_ac_scalarvalue;
		private double _first_this_scalarvalue;
		private double _first_ac_scalarvalue;
		private double _sec_this_scalarvalue;
		private double _sec_ac_scalarvalue;
		private int _cyc;
		private DateTime _time= new DateTime();
		/// <summary>
		/// 
		/// </summary>
		public string project_name
		{
			set{ _project_name=value;}
			get{return _project_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string senorno
		{
			set{ _senorno=value;}
			get{return _senorno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string datatype
		{
			set{ _datatype=value;}
			get{return _datatype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int valuetype
		{
			set{ _valuetype=value;}
			get{return _valuetype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double oregion_N
		{
			set{ _oregion_n=value;}
			get{return _oregion_n;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double oregion_E
		{
			set{ _oregion_e=value;}
			get{return _oregion_e;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double oregion_Z
		{
			set{ _oregion_z=value;}
			get{return _oregion_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double single_oregion_scalarvalue
		{
			set{ _single_oregion_scalarvalue=value;}
			get{return _single_oregion_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double first_oregion_scalarvalue
		{
			set{ _first_oregion_scalarvalue=value;}
			get{return _first_oregion_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double sec_oregion_scalarvalue
		{
			set{ _sec_oregion_scalarvalue=value;}
			get{return _sec_oregion_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_dN
		{
			set{ _this_dn=value;}
			get{return _this_dn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_dE
		{
			set{ _this_de=value;}
			get{return _this_de;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_dZ
		{
			set{ _this_dz=value;}
			get{return _this_dz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_dN
		{
			set{ _ac_dn=value;}
			get{return _ac_dn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_dE
		{
			set{ _ac_de=value;}
			get{return _ac_de;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_dZ
		{
			set{ _ac_dz=value;}
			get{return _ac_dz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double single_this_scalarvalue
		{
			set{ _single_this_scalarvalue=value;}
			get{return _single_this_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double single_ac_scalarvalue
		{
			set{ _single_ac_scalarvalue=value;}
			get{return _single_ac_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double first_this_scalarvalue
		{
			set{ _first_this_scalarvalue=value;}
			get{return _first_this_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double first_ac_scalarvalue
		{
			set{ _first_ac_scalarvalue=value;}
			get{return _first_ac_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double sec_this_scalarvalue
		{
			set{ _sec_this_scalarvalue=value;}
			get{return _sec_this_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double sec_ac_scalarvalue
		{
			set{ _sec_ac_scalarvalue=value;}
			get{return _sec_ac_scalarvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int cyc
		{
			set{ _cyc=value;}
			get{return _cyc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		#endregion Model

	}
}

