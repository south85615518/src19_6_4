﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace data.Model
{
    public enum gtsensortype
    {
        _strain,//应变,
        _steelstress,//钢筋应力
        _prestress,//预应力
        _StrainStress,//应变应力
        _deflection,//挠度
        _displacement,//位移
        
        _windspeedanddirection,//风速风向
        _degree,//温度,
        _osmoticpressure,//渗压
        _stress,//应力
        _axialforce,
        
        //传感器类型修正
        _supportAxialForce,//混凝土支撑轴力
        _soilPressure,//土压力
        _fractureMeter,//裂缝计
        _staticLevel,//静力水准
        _layeredSettlement,//分层沉降
        _enclosureWallReinforcementStress,//围护墙钢筋应力
        _waterlevel,//水位
        _SteelSupport,//钢支撑
        //传感器类型修正
        _other,
        _inclinometer,
        _HorizontalDisplacement,
        _all,
        _StructuralStress,
        _surfaceSurpport,
        _vibration,

    }
    
}
