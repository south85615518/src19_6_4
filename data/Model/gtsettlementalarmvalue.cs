﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsettlementalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtsettlementalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/9/18 15:58:07   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace data.Model
{
	/// <summary>
	/// gtsettlementalarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gtsettlementalarmvalue
	{
		public gtsettlementalarmvalue()
		{}
		#region Model
		private int _xmno;
        private int _id;
		private double _this_val;
		private double _ac_val;
		private double _rap;
		private double _settlementdiff;
		private string _alarmname;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_val
		{
			set{ _this_val=value;}
			get{return _this_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_val
		{
			set{ _ac_val=value;}
			get{return _ac_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double rap
		{
			set{ _rap=value;}
			get{return _rap;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double settlementdiff
		{
			set{ _settlementdiff=value;}
			get{return _settlementdiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string alarmname
		{
			set{ _alarmname=value;}
			get{return _alarmname;}
		}
		#endregion Model

	}
}

