﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsettlementpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtsettlementpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/9/15 14:37:41   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace data.Model
{
	/// <summary>
	/// gtsettlementpointalarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gtsettlementpointalarmvalue
	{
		public gtsettlementpointalarmvalue()
		{}
		#region Model
		private int _xmno;
		private string _point_name;
		private int _deviceno;
		private int _addressno;
		private string _firstalarmname;
		private string _secalarmname;
		private string _thirdalarmname;
        private string _pointtype;

        public string pointtype
        {
            get { return _pointtype; }
            set { _pointtype = value; }
        }
        private string _basepointname;

        public string basepointname
        {
            get { return _basepointname; }
            set { _basepointname = value; }
        }
		private string _remark;
        private string _dtuno;

        public string dtuno
        {
            get { return _dtuno; }
            set { _dtuno = value; }
        }
        private double _initsettlementval;

        public double initsettlementval
        {
            get { return _initsettlementval; }
            set { _initsettlementval = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int deviceno
		{
			set{ _deviceno=value;}
			get{return _deviceno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int addressno
		{
			set{ _addressno=value;}
			get{return _addressno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string firstalarmname
		{
			set{ _firstalarmname=value;}
			get{return _firstalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string secalarmname
		{
			set{ _secalarmname=value;}
			get{return _secalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string thirdalarmname
		{
			set{ _thirdalarmname=value;}
			get{return _thirdalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		#endregion Model

	}
}

