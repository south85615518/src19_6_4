﻿/**  版本信息模板在安装目录下，可自行修改。
* gtpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/9/7 13:38:39   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace data.Model
{
	/// <summary>
	/// gtpointalarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gtpointalarmvalue
	{
		public gtpointalarmvalue()
		{}
		#region Model
		private int _xmno;
		private data.Model.gtsensortype _datatype;
		private string _pointname;
        private string _sensorno;

        public string sensorno
        {
            get { return _sensorno; }
            set { _sensorno = value; }
        }
		private string _firstalarmname;
		private string _secondalarmname;
		private string _thirdalarmname;
		private string _remark;
        private double _orificeHeight;
        private double _thisOrificeHeight;

        public double thisOrificeHeight
        {
            get { return _thisOrificeHeight; }
            set { _thisOrificeHeight = value; }
        }
        private double _line;

        public double line
        {
            get { return _line; }
            set { _line = value; }
        }


        public double orificeHeight
        {
            get { return _orificeHeight; }
            set { _orificeHeight = value; }
        }
        private string _surfaceName;

        public string surfaceName
        {
            get { return _surfaceName; }
            set { _surfaceName = value; }
        }
        private bool _isInCalculate;

        public bool isInCalculate
        {
            get { return _isInCalculate; }
            set { _isInCalculate = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
        public data.Model.gtsensortype datatype
		{
			set{ _datatype=value;}
			get{return _datatype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pointname
		{
			set{ _pointname=value;}
			get{return _pointname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string firstalarmname
		{
			set{ _firstalarmname=value;}
			get{return _firstalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string secondalarmname
		{
			set{ _secondalarmname=value;}
			get{return _secondalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string thirdalarmname
		{
			set{ _thirdalarmname=value;}
			get{return _thirdalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		#endregion Model

	}
}

