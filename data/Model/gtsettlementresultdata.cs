﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsettlementresultdata.cs
*
* 功 能： N/A
* 类 名： gtsettlementresultdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/9/15 14:37:17   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace data.Model
{
	/// <summary>
	/// gtsettlementresultdata:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gtsettlementresultdata
	{
		public gtsettlementresultdata()
		{}
		#region Model
		private int _xmno;
		private string _point_name;
		private double _settlementvalue;
		private double _degree;
		private double _settlementdiff;
		private double _this_val;
		private double _ac_val;
		private double _d_val;
        private double _initsettlementval;

		private DateTime _time;
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}

        public double initsettlementval
        {
            get { return _initsettlementval; }
            set { _initsettlementval = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double settlementvalue
		{
			set{ _settlementvalue=value;}
			get{return _settlementvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double degree
		{
			set{ _degree=value;}
			get{return _degree;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double settlementdiff
		{
			set{ _settlementdiff=value;}
			get{return _settlementdiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_val
		{
			set{ _this_val=value;}
			get{return _this_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_val
		{
			set{ _ac_val=value;}
			get{return _ac_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double d_val
		{
			set{ _d_val=value;}
			get{return _d_val;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		#endregion Model

	}
}

