﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacedata.cs
*
* 功 能： N/A
* 类 名： gtsurfacedata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 #SensorName# Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtsurfacedata
	/// </summary>
	public partial class gtsurfacedata
	{
        public database db = new database();
		public gtsurfacedata()
		{}
		#region  BasicMethod
        

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsurfacedata model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("replace into gtsurfacedata(");
			strSql.Append("surfacename,SteelChordVal,this_val,ac_val,time,calculatepointsCont,xmno,cyc,datatype)");
			strSql.Append(" values (");
            strSql.Append("@surfacename,@SteelChordVal,@this_val,@ac_val,@time,@calculatepointsCont,@xmno,@cyc,@datatype)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@surfacename", OdbcType.VarChar,20),
					new OdbcParameter("@SteelChordVal", OdbcType.Double),
					new OdbcParameter("@this_val", OdbcType.Double),
					new OdbcParameter("@ac_val", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime),
                    new OdbcParameter("@calculatepointsCont", OdbcType.Int),
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@cyc", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };
			parameters[0].Value = model.surfacename;
			parameters[1].Value = model.SteelChordVal;
			parameters[2].Value = model.this_val;
			parameters[3].Value = model.ac_val;
			parameters[4].Value = model.time;
            parameters[5].Value = model.calculatepointsCont;
            parameters[6].Value = model.xmno;
            parameters[7].Value = model.cyc;
            parameters[8].Value =   Convert.ToInt32( data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(data.Model.gtsurfacedata model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update   gtsurfacedata  set   ");

			strSql.Append("SteelChordVal=@SteelChordVal,");
            strSql.Append("calculatepointsCont = @calculatepointsCont,");
			strSql.Append("this_val=@this_val,");
			strSql.Append("ac_val=@ac_val  ");
			strSql.Append("    where   ");
            strSql.Append("surfacename=@surfacename  and  ");
            strSql.Append("  xmno=@xmno   and  ");
            strSql.Append("time=@time   and    ");
            strSql.Append("datatype=@datatype   ");
			OdbcParameter[] parameters = {
					
					new OdbcParameter("@SteelChordVal", OdbcType.Double),
                    new OdbcParameter("@calculatepointsCont", OdbcType.Int),
					new OdbcParameter("@this_val", OdbcType.Double),
					new OdbcParameter("@ac_val", OdbcType.Double),
                    new OdbcParameter("@surfacename", OdbcType.VarChar,200),
                    new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@time", OdbcType.DateTime),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };
			
			parameters[0].Value = model.SteelChordVal;
            parameters[1].Value = model.calculatepointsCont;
			parameters[2].Value = model.this_val;
			parameters[3].Value = model.ac_val;
            parameters[4].Value = model.surfacename;
            parameters[5].Value = model.xmno;
			parameters[6].Value = model.time;
            parameters[7].Value = Convert.ToInt32(data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
      
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,DateTime Time,data.Model.gtsensortype  datatype,string surfacename)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from gtsurfacedata ");
			strSql.Append(" where xmno=@xmno and time=@time and surfacename=@surfacename  and  datatype = @datatype ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@surfacename", OdbcType.VarChar,120),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };
			parameters[0].Value = xmno;
			parameters[1].Value = Time;
			parameters[2].Value = surfacename;
            parameters[3].Value = Convert.ToInt32(datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        public bool Delete(int xmno, data.Model.gtsensortype  datatype, int startcyc, int endcyc)
        {

            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from  gtsurfacedata  where   xmno=@xmno  and  datatype = @datatype   and  cyc   between    @startcyc      and    @endcyc  ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.VarChar,100),
                                             new OdbcParameter("@startcyc", OdbcType.Int),
                                             new OdbcParameter("@endcyc", OdbcType.Int),
                                             new OdbcParameter("@datatype", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = startcyc;
            parameters[2].Value = endcyc;
            parameters[3].Value = Convert.ToInt32(datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool DeleteTmp(int xmno,data.Model.gtsensortype datatype)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("  delete from gtsurfacedata_tmp   ");
            strSql.Append("  where xmno=@xmno  and  datatype=@datatype    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ResultdataTableLoad(int startPageIndex, int pageSize, int xmno,string pointname ,data.Model.gtsensortype datatype ,int startcyc,int endcyc,string sord ,out DataTable dt)
        {
            dt = new DataTable();
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmno);
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "   surfacename in (select surfacename  from   " + surveyconn.Database + ".gtsurfacestructure  where  xmno = " + xmno+ "  )   " : string.Format("surfacename='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            //strSql.AppendFormat(@"select surfacename,this_val,ac_val,time  from gtsurfacedata  where   xmno=" + xmno + "    and    {0}   and   time  between      '" + starttime + "'    and    '" + endtime + "'   order by {1}  asc  limit   1,100", searchstr, sord);
            strSql.AppendFormat("select surfacename,cyc,this_val,ac_val,SteelChordVal,time  from gtsurfacedata  where   xmno=@xmno  and  datatype=@datatype   and    {0}   and   cyc  between      @startcyc    and    @endcyc      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int),
                    new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                    

                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;
            parameters[4].Value = (startPageIndex - 1) * pageSize;
            parameters[5].Value = pageSize * startPageIndex;
            
            Tool.ExceptionLog.ExceptionWrite(strSql.ToString());
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(),parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;
           
        }

        public bool ReportDataView(List<string> cyclist, int startPageIndex, int pageSize, int xmno,data.Model.gtsensortype datatype ,string sord, out DataTable dt)
        {
            dt = new DataTable();

            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select surfacename,cyc,this_val,ac_val,SteelChordVal,time from gtsurfacedata  where      xmno=@xmno   and  datatype =@datatype      and    surfacename in (select distinct(surfacename) from " + surveyconn.Database + ".gtsurfacestructure where xmno =" + xmno + " )    and   CYC    in ({0})  order by {1}  asc  limit    @startPageIndex,   @endPageIndex", string.Join(",", cyclist), sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = (startPageIndex - 1) * pageSize;
            parameters[3].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }


        public bool ResultTableRowsCount(int xmno,data.Model.gtsensortype datatype ,string pointname, DateTime starttime, DateTime endtime, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  surfacename = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from gtsurfacedata where xmno='{0}'  and   datatype="+Convert.ToInt32(datatype)+"  and  time  between '{2}' and  '{3}'   and {1}",  xmno,searchstr,starttime,endtime);
            OdbcConnection conn = db.GetStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }

        public bool ResultDataReportPrint(string sql,int xmno,out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            dt = querysql.querystanderdb(sql,conn);
            return true;
        }

        public bool SurfaceDataSameTimeModel(int xmno,data.Model.gtsensortype datatype ,string surfacename, DateTime dt, out data.Model.gtsurfacedata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select distinct(surfacename) as surfacename,xmno,SteelChordVal,this_val,ac_val,calculatepointsCont,time,cyc,datatype  from gtsurfacedata ");
            strSql.Append(" where    xmno=@xmno   and  datatype=@datatype  and     surfacename=@surfacename    and    time   = @time  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int),
					new OdbcParameter("@surfacename", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = surfacename;
            parameters[3].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                if (model.calculatepointsCont == 4) { 
                    string a = "";
                };
                return true;
            }
            return false;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno,data.Model.gtsensortype datatype,out List<data.Model.gtsurfacedata> lt )
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select *  from gtsurfacedata_tmp ");
            strSql.Append(" where    xmno=@xmno   and    datatype=@datatype  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            lt = new List<Model.gtsurfacedata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i<ds.Tables[0].Rows.Count)
            {
                lt.Add( DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        public bool GetLastTimeModel(int xmno,data.Model.gtsensortype datatype ,string surfacename,  DateTime dt, out data.Model.gtsurfacedata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM   gtsurfacedata   where     xmno =   @xmno   and  datatype = @datatype    and       surfacename =  @surfacename    and   time < @time   order by time desc limit 0,2  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int),
					new OdbcParameter("@surfacename", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = surfacename;
            parameters[3].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public data.Model.gtsurfacedata DataRowToModel(DataRow row)
        {
            data.Model.gtsurfacedata model = new data.Model.gtsurfacedata();
            if (row != null)
            {
                if (row["surfacename"] != null && row["surfacename"].ToString() != "")
                {
                    model.surfacename = row["surfacename"].ToString();
                }
                if (row["SteelChordVal"] != null && row["SteelChordVal"].ToString() != "")
                {
                    model.SteelChordVal = double.Parse(row["SteelChordVal"].ToString());
                }
                if (row["this_val"] != null && row["this_val"].ToString() != "")
                {
                    model.this_val = double.Parse(row["this_val"].ToString());
                }
               if (row["ac_val"] != null && row["ac_val"].ToString() != "")
                {
                    model.ac_val = double.Parse(row["ac_val"].ToString());
                }
               if (row["calculatepointsCont"] != null && row["calculatepointsCont"].ToString() != "")
               {
                   model.calculatepointsCont = double.Parse(row["calculatepointsCont"].ToString());
               }
                if (row["xmno"] != null && row["xmno"] != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = Convert.ToDateTime(row["time"].ToString());
                }
                if (row["cyc"] != null && row["cyc"].ToString() != "")
                {
                    model.cyc = Convert.ToInt32(row["cyc"].ToString());
                }
                if (row["datatype"] != null && row["datatype"].ToString() != "")
                {
                    model.datatype =data.DAL.gtsensortype.GTSensorTypeToString( (data.Model.gtsensortype)Convert.ToInt32(row["datatype"].ToString()));
                }
            }
            return model;
        }
    
        public bool GetModel(int xmno,data.Model.gtsensortype datatype ,string pointname, DateTime dt, out data.Model.gtsurfacedata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select surfacename,SteelChordVal,this_val,ac_val,calculatepointsCont,time,cyc,datatype  from gtsurfacedata ");
            strSql.Append(" where    xmno=@xmno   and  datatype=@datatype   and   surfacename=@surfacename    and    time = @time   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int),
					new OdbcParameter("@surfacename", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = pointname;
            parameters[3].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

        public bool gtsurfacedatacycupdate(int xmno, data.Model.gtsensortype datatype, string pointname, DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("update gtsurfacedata  set cyc=(select cyc from gtsensordata where xmno = {0}   and datatype=" + Convert.ToInt32(datatype) + "  and surfacename = '{1}' and  time= '{2}'  limit 0,1   )  where xmno={0} and  surfacename='{1}' and time = '{2}'   ", xmno, pointname, dt);
            int rows  = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,sql);
            return rows > 0 ? true : false;

            

        }



        public bool MaxTime(int xmno,data.Model.gtsensortype datatype,out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtsurfacedata  where   xmno = @xmno  and  datatype = @datatype  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.VarChar,100),
                new OdbcParameter("@datatype",OdbcType.Int)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = Convert.ToInt32(datatype);
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool gtsurfacedataPointLoadDAL(int xmno, data.Model.gtsensortype datatype, out List<string> ls)
        {
           
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select DISTINCT(surfacename) from fmos_pointalarmvalue where xmno=" + xmno + "  and  datatype = " + Convert.ToInt32(datatype);
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }


        public bool PointNewestDateTimeGet(int xmno, data.Model.gtsensortype datatype, string pointname, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from gtsurfacedata    where    xmno=@xmno  and   datatype = @datatype   and surfacename = @surfacename  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int),
                    new OdbcParameter("@surfacename", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
            if (obj == null) { dt = Convert.ToDateTime("0001-1-1"); return false; }
            dt = Convert.ToDateTime(obj); return true;
            
        }

         public bool XmStateTable(int startPageIndex, int pageSize, int xmno,string xmname,string unitname ,string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder(256);
            //strSql.AppendFormat(@"SELECT '"+xmno+"' as xmno,'" + unitname + "' as unitname ,max(time) as lastrectime,iF((SELECT IFNULL(MAX(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from gtsurfacedata where xmno ='" + xmno + @"' ) > DATE_ADD(SYSDATE(),INTERVAL -IFNULL((select  hour*60+minute from #SensorName#datauploadinterval  where  xmno='"+xmno+@"'),24*60) minute),'true','false' ) as state ,(select count(1) from pointcheck  where xmno=" + xmno + " and type='表面位移') as alarmcount from gtsurfacedata where taskname='" + xmno + @"'");
            dt = querysql.querystanderdb(strSql.ToString(), conn);
            if (dt != null) return true;
            return false;
        }
         
         //public bool MaxTime(int xmno, out DateTime maxTime)
         //{
         //    maxTime = new DateTime();
         //    OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
         //    StringBuilder strSql = new StringBuilder();
         //    strSql.Append("select max(time) from gtsurfacedata  where   xmno = @xmno   and   datatype=@datatype   ");
         //    OdbcParameter[] paramters = { 
         //       new OdbcParameter("@xmno",OdbcType.VarChar,100),
         //       new OdbcParameter("@datatype",OdbcType.VarChar,100)
         //   };
         //    paramters[0].Value = xmno;
         //    object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
         //    if (obj == null || obj.ToString() == "") return false;
         //    maxTime = Convert.ToDateTime(obj);
         //    return true;
         //}
         public bool MaxTime(int xmno,data.Model.gtsensortype datatype ,string surfacename, out DateTime maxTime)
         {
             maxTime = new DateTime();
             OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
             StringBuilder strSql = new StringBuilder();
             strSql.AppendFormat("select    max(time)    from    gtsurfacedata    where   xmno = {0}   and  datatype={1}   and   surfacename='{2}'    ", xmno, Convert.ToInt32(datatype), surfacename);
            // OdbcParameter[] paramters = { 
            //    new OdbcParameter("@xmno",OdbcType.VarChar,100),
            //    new OdbcParameter("@surfacename",OdbcType.VarChar,100)
            //};
            
            // paramters[0].Value = xmno;
            // paramters[1].Value = surfacename;
             ExceptionLog.ExceptionWrite(strSql.ToString());
             object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
             
             if (obj == null || obj.ToString() == "") return false;
             maxTime = Convert.ToDateTime(obj);
             return true;
         }
         public bool MinTime(int xmno, data.Model.gtsensortype datatype , string surfacename, out DateTime MinTime)
         {
             MinTime = new DateTime();
             OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
             StringBuilder strSql = new StringBuilder();
             strSql.Append("select Min(time) from gtsurfacedata  where   xmno = @xmno     and   surfacename=@surfacename  and  datatype = @datatype  ");
             OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.VarChar,100),
                new OdbcParameter("@surfacename",OdbcType.VarChar,100),
                new OdbcParameter("@datatype",OdbcType.Int)
            };
             paramters[0].Value = xmno;
             paramters[1].Value = surfacename;
             paramters[2].Value = Convert.ToInt32(datatype);
             object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
             if (obj == null || obj.ToString() == "") return false;
             MinTime = Convert.ToDateTime(obj);
             return true;
         }
         public bool dataPointLoadDAL(int xmno, data.Model.gtsensortype datatype, out List<string> ls)
         {

             OdbcConnection conn = db.GetStanderConn(xmno);
             string sql = "select DISTINCT(POINT_NAME) from gtpointalarmvalue where xmno=" + xmno + "  and   datatype = " + Convert.ToInt32(datatype);
             ls = querysql.querystanderlist(sql, conn);
             return true;

         }
         public bool PointNameDateTimeListGet(int xmno, data.Model.gtsensortype datatype, string pointname, out List<string> ls)
         {

             OdbcConnection conn = db.GetStanderConn(xmno);
             OdbcSQLHelper.Conn = conn;
             StringBuilder strSql = new StringBuilder();
             strSql.AppendFormat("select distinct(time)  from  gtsurfacedata    where         xmno=@xmno  and    datatype=@dattype    and      {0}     order by time  asc  ", pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  surfacename=  '{0}'", pointname));
             ls = new List<string>();
             OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
							};
             parameters[0].Value = xmno;
             parameters[1].Value = Convert.ToInt32(datatype);
             DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
             if (ds == null) return false;
             int i = 0;
             while (i < ds.Tables[0].Rows.Count)
             {
                 ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                 i++;
             }
             return true;

         }

         //根据项目名获取所以的周期
         public bool CYCDateTimeListGet(int xmno,data.Model.gtsensortype datatype , out List<string> cycTimeList)
         {

             string sql = "select  distinct(cyc),time  from gtsurfacedata  where  xmno='" + xmno + "'  and   datatype = " + Convert.ToInt32(datatype) + "     order by cyc asc";
             OdbcConnection conn = db.GetStanderConn(xmno);
             DataTable dt = querysql.querystanderdb(sql, conn);
             cycTimeList = new List<string>();
             if (dt.Rows.Count == 0) return false;
             int i = 0;
             for (i = 0; i < dt.Rows.Count; i++)
             {
                 cycTimeList.Add(string.Format("{0}[{1}]", dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1]));
             }
             return true;
         }

         public bool LackPoints(int xmno, data.Model.gtsensortype datatype, int cyc, out string pointstr)
         {
             StringBuilder strSql = new StringBuilder();
             OdbcConnection conn = db.GetSurveyStanderConn(xmno);
             strSql.AppendFormat("select distinct(surfacename) from gtsurfacestructure where  xmno={0}  and gtsurfacestructure.surfacename not in ( select surfacename from gtsurfacedata_data where xmno= '{0}'  and  datatype={1}   and cyc = {2} )", xmno, Convert.ToInt32(datatype), cyc);
             List<string> ls = querysql.querystanderlist(strSql.ToString(), conn);
             pointstr = string.Join(",", ls);
             return ls.Count == 0 ? false: true;
         }

         public bool XmStateTable(int startPageIndex, int pageSize, string xmno, string xmname, string unitname, string colName, string sord, out DataTable dt)
         {
             string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
             OdbcConnection conn = db.GetStanderConn(xmno);
             StringBuilder strSql = new StringBuilder(256);
             //strSql.AppendFormat(@"SELECT '"+xmno+"' as xmno,'" + unitname + "' as unitname ,max(time) as lastrectime,iF((SELECT IFNULL(MAX(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from gtsurfacedata where xmno ='" + xmno + @"' ) > DATE_ADD(SYSDATE(),INTERVAL -IFNULL((select  hour*60+minute from datadatauploadinterval  where  xmno='"+xmno+@"'),24*60) minute),'true','false' ) as state ,(select count(1) from pointcheck  where xmno=" + xmno + " and type='表面位移') as alarmcount from gtsurfacedata where taskname='" + xmno + @"'");
             dt = querysql.querystanderdb(strSql.ToString(), conn);
             if (dt != null) return true;
             return false;
         }
         public bool IsgtsurfacedataDataExist(string xmno, data.Model.gtsensortype datatype, string pointname, DateTime dt)
         {
             OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
             StringBuilder strsql = new StringBuilder();
             strsql.Append("select    count(1)     from     gtsurfacedata    where     xmno=@xmno  and  datatype = @datatype   and     surfacename =@surfacename    and        time=@time  ");
             OdbcParameter[] parameters ={ 
                 new OdbcParameter("@xmno",OdbcType.Int),
                 new OdbcParameter("@datatype",OdbcType.Int),
                 new OdbcParameter("@surfacename",OdbcType.VarChar,100),
                 new OdbcParameter("@time",OdbcType.DateTime)
             };
             parameters[0].Value = xmno;
             parameters[1].Value = Convert.ToInt32(datatype);
             parameters[2].Value = pointname;
             parameters[3].Value = dt;
             object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
             return Convert.ToInt32(obj) > 0 ? true : false;
         }

         public bool LackPointCalculate(int xmno,data.Model.gtsensortype datatype ,out DataTable dt)
         {
             dt = null;
             OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
             string sql = @"SELECT a.surfacename,a.time,a.calculatepointsCont,cyc,(select count(1) from gtsensordata   where project_name = a.xmno  and datatype='" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "' and  time = a.time and senorno in (select  distinct(sensorno) from gtpointalarmvalue where  xmno = a.xmno  and  datatype=" + Convert.ToInt32(datatype) + "  and   surfacename = a.surfacename and   isInCalculate = 1  ) ) as pcont  FROM `gtsurfacedata` a  where  a.calculatepointsCont <> (select count(distinct(senorno)) from gtsensordata   where project_name = a.xmno  and datatype='" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "' and  time = a.time and senorno in (select  distinct(sensorno) from gtpointalarmvalue where  xmno = a.xmno  and  datatype=" + Convert.ToInt32(datatype) + "  and surfacename = a.surfacename   and   isInCalculate = 1   ) )  order by a.time,a.surfacename asc ;";
             DataSet ds = OdbcSQLHelper.Query(sql);
             if (ds.Tables[0].Rows.Count == 0) return false;
             int i = 0;
             dt = ds.Tables[0];
             return true;

         }

         public bool CycSort(int xmno, data.Model.gtsensortype datatype, out int rows)
         {
             string sql = "update gtsurfacedata  a set cyc = (select cyc from gtsensordata where  datatype ='" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "' and  time = a.time and project_name = a.xmno limit 0,1  ) where xmno=" + xmno + " ";
             SingleTonOdbcSQLHelper singleTonOdbcSQLHelper = new SingleTonOdbcSQLHelper();
             singleTonOdbcSQLHelper.Conn = db.GetStanderConn(xmno);
             rows = singleTonOdbcSQLHelper.ExecuteNonQuery(CommandType.Text,sql);
             return rows >= 0 ? true : false;
         }
         



		#endregion  BasicMethod
	}
}

