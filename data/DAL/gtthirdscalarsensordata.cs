﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using SqlHelpers;

namespace data.DAL
{
    public partial class gtsensordata
    {

        public bool ThirdScalarResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype, DateTime startTime, DateTime endTime, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select project_name,point_name,senorno,datatype,valuetype,oregion_N,oregion_E,oregion_Z,time,Millisecond,cyc,time  from   gtsensordata       where      project_name=@xmname       and    datatype=@datatype         and        time>@starttime     and     time < @endtime      and    {0}      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),
                    new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startTime;
            parameters[3].Value = endTime;
            parameters[4].Value = (startPageIndex - 1) * pageSize;
            parameters[5].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
    }
}
