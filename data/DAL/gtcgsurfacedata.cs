﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacedata.cs
*
* 功 能： N/A
* 类 名： gtsurfacedata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 #SensorName# Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtsurfacedata
	/// </summary>
	public partial class gtsurfacedata
	{
       
		#region  BasicMethod

        public bool CgPointNameDateTimeListGet(int xmno, data.Model.gtsensortype datatype, string pointname, out List<string> ls)
        {

            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  gtsurfacedata    where         xmno='{0}'      and  datatype="+Convert.ToInt32(datatype)+"    and     {1}     order by time  asc  ", xmno,  pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  surfacename=  '{0}'", pointname));
            ls = new List<string>();
            //OdbcParameter[] parameters = {
            //        new OdbcParameter("@xmno", OdbcType.VarChar,200),
            //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
            //                };
            //parameters[0].Value = xmno;
            //parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            string str = strSql.ToString();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }
        public bool CgMaxTime(int xmno, data.Model.gtsensortype datatype, string surfacename, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtsurfacedata  where   xmno = @xmno   and  datatype=" + Convert.ToInt32(datatype) + "  and     surfacename=@surfacename  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int,100),
                new OdbcParameter("@surfacename",OdbcType.VarChar,100)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = surfacename;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool CgMinTime(int xmno, data.Model.gtsensortype datatype, string surfacename, out DateTime MinTime)
        {
            MinTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Min(time) from gtsurfacedata  where   xmno = @xmno     and    datatype=" + Convert.ToInt32(datatype) + "  and surfacename=@surfacename  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int,100),
                new OdbcParameter("@surfacename",OdbcType.VarChar,100)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = surfacename;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            MinTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool AddCgData(int xmno, data.Model.gtsensortype datatype, int cyc, int importcyc, out int rows)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection Conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            OdbcConnection CgConn = db.GetStanderCgConn(xmno);
            strSql.AppendFormat("REPLACE into {0}.gtsurfacedata (surfacename,datatype,cyc,xmno,this_val,ac_val,SteelChordVal,time,calculatepointsCont) select surfacename," + Convert.ToInt32(datatype) + " as datatype," + cyc + " as    cyc ,xmno,this_val,ac_val,SteelChordVal,time,calculatepointsCont from {1}.gtsurfacedata_data   where   xmno=@xmno  and  datatype=" + Convert.ToInt32(datatype) + "    and    cyc = @cyc  and   surfacename in ( select distinct(surfacename) from  {1}.gtsurfacestructure where xmno = " + xmno + "   and  datatype=" + Convert.ToInt32(datatype) + "  ) ", CgConn.Database, Conn.Database);
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.VarChar,100),
                                              new OdbcParameter("@cyc", OdbcType.Int)
                                             
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = importcyc;

            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteCg(int xmno, data.Model.gtsensortype datatype, int startcyc, int endcyc)
        {

            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from  gtsurfacedata  where   xmno=@xmno  and  datatype=" + Convert.ToInt32(datatype) + "  and  cyc   between    @startcyc      and    @endcyc  ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.VarChar,100),
                                             new OdbcParameter("@startcyc", OdbcType.Int),
                                             new OdbcParameter("@endcyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = startcyc;
            parameters[2].Value = endcyc;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteCgTmp(int xmno,data.Model.gtsensortype datatype )
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            strSql.Append("delete  from  gtsurfacedata_tmp ");
            strSql.Append(" where    xmno=@xmno   and  datatype=" + Convert.ToInt32(datatype) );
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CgResultdataTableLoad(int startPageIndex, int pageSize, int xmno, data.Model.gtsensortype datatype, string pointname, string sord, int startcyc, int endcyc, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("surfacename='{0}'", pointname);
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"select surfacename,cyc,SteelChordVal,this_val,ac_val,calculatepointsCont,time  from gtsurfacedata  where   xmno=@xmno     and  datatype=" + Convert.ToInt32(datatype) + "  and    {0}   and   cyc  between      @startcyc    and    @endcyc      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);

            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                    

                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = startcyc;
            parameters[2].Value = endcyc;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool CgResultTableRowsCount(int xmno, data.Model.gtsensortype datatype, string pointname, int startcyc, int endcyc, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  surfacename = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from  gtsurfacedata_data where xmno='{0}'     and  datatype=" + Convert.ToInt32(datatype) + "  and  cyc  between   '{2}'  and   '{3}'     and   {1}", xmno, searchstr, startcyc, endcyc);
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgScalarGetModelList(int xmno, data.Model.gtsensortype datatype, out List<data.Model.gtsurfacedata> lt)
        {
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select *  from gtsurfacedata_tmp  ");
            strSql.Append(" where    xmno=@xmno  and  datatype=" + Convert.ToInt32(datatype) + "  order by  surfacename   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmno;
            lt = new List<Model.gtsurfacedata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


        //根据项目名获取所以的周期
        public bool CgCYCDateTimeListGet(int xmno, data.Model.gtsensortype datatype, out List<string> cycTimeList)
        {

            string sql = "select  distinct(cyc),time  from gtsurfacedata  where  xmno='" + xmno + "'   and   datatype=" + Convert.ToInt32(datatype) + "    order by cyc asc";
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            DataTable dt = querysql.querystanderdb(sql, conn);
            cycTimeList = new List<string>();
            if (dt.Rows.Count == 0) return false;
            int i = 0;
            for (i = 0; i < dt.Rows.Count; i++)
            {
                cycTimeList.Add(string.Format("{0}[{1}]", dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1]));
            }
            return true;
        }

		#endregion  BasicMethod
	}
}

