﻿/**  版本信息模板在安装目录下，可自行修改。
* gtpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:28   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace data.DAL
{
    /// <summary>
    /// 数据访问类:gtpointalarmvalue
    /// </summary>
    public partial class gtpointalarmvalue
    {
        public gtpointalarmvalue()
        { }
        public static database db = new database();
        #region  BasicMethod
        //判断点名对象是否存在
        public bool Exist(data.Model.gtpointalarmvalue model)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("select count(1) from  gtpointalarmvalue   where     xmno=@xmno    ");
            strSql.Append("   and       pointname=@pointname       ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
                    new OdbcParameter("@pointname", OdbcType.VarChar,120)
                                         };

            parameters[0].Value = model.xmno;
            parameters[1].Value = model.pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);

            return obj == null ? true : int.Parse(obj.ToString()) > 0 ? true : false;

        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into gtpointalarmvalue(");
            strSql.Append("xmno,datatype,pointname,sensorno,firstalarmname,secondalarmname,thirdalarmname,OrificeHeight,ThisOrificeHeight,surfaceName,isInCalculate,remark,line)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@datatype,@pointname,@sensorno,@firstalarmname,@secondalarmname,@thirdalarmname,@OrificeHeight,@ThisOrificeHeight,@surfaceName,@isInCalculate,@remark,@line)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
                    new OdbcParameter("@datatype", OdbcType.Int),
					new OdbcParameter("@pointname", OdbcType.VarChar,120),
                    new OdbcParameter("@sensorno", OdbcType.VarChar,120),
					new OdbcParameter("@firstalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@secondalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@thirdalarmname", OdbcType.VarChar,120),
                    new OdbcParameter("@OrificeHeight", OdbcType.Double),
                    new OdbcParameter("@ThisOrificeHeight", OdbcType.Double),
					new OdbcParameter("@surfaceName", OdbcType.VarChar,200),
					new OdbcParameter("@isInCalculate", OdbcType.TinyInt),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@line", OdbcType.VarChar,500)
                                         };
            parameters[0].Value = model.xmno;
            parameters[1].Value = Convert.ToInt32(model.datatype);
            parameters[2].Value = model.pointname;
            parameters[3].Value = model.sensorno;
            parameters[4].Value = model.firstalarmname;
            parameters[5].Value = model.secondalarmname;
            parameters[6].Value = model.thirdalarmname;
            parameters[7].Value = model.orificeHeight;
            parameters[8].Value = model.thisOrificeHeight;
            parameters[9].Value = model.surfaceName;
            parameters[10].Value = model.isInCalculate;
            parameters[11].Value = model.remark;
            parameters[12].Value = model.line;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update gtpointalarmvalue set ");
            strSql.Append("firstalarmname=@firstalarmname,");
            strSql.Append("secondalarmname=@secondalarmname,");
            strSql.Append("thirdalarmname=@thirdalarmname,");
            strSql.Append("sensorno=@sensorno,");
            strSql.Append("orificeHeight=@orificeHeight,");
            strSql.Append("thisOrificeHeight=@thisOrificeHeight,");
            strSql.Append("surfaceName=@surfaceName,");
            strSql.Append("isInCalculate=@isInCalculate,");
            strSql.Append("remark=@remark,");
            strSql.Append("line=@line");
            strSql.Append("   where   ");
            strSql.Append("xmno = @xmno   and    pointname=@pointname   and   datatype = @datatype");
            OdbcParameter[] parameters = {
					new OdbcParameter("@firstalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@secondalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@thirdalarmname", OdbcType.VarChar,120),
                    new OdbcParameter("@sensorno", OdbcType.VarChar,120),
                    new OdbcParameter("@orificeHeight", OdbcType.Double),
                    new OdbcParameter("@thisOrificeHeight", OdbcType.Double),
					new OdbcParameter("@surfaceName", OdbcType.VarChar,200),
					new OdbcParameter("@isInCalculate", OdbcType.TinyInt,2),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@line", OdbcType.Double),
                    new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@pointname", OdbcType.VarChar,120),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };

            parameters[0].Value = model.firstalarmname;
            parameters[1].Value = model.secondalarmname;
            parameters[2].Value = model.thirdalarmname;
            parameters[3].Value = model.sensorno;
            parameters[4].Value = model.orificeHeight;
            parameters[5].Value = model.thisOrificeHeight;
            parameters[6].Value = model.surfaceName;
            parameters[7].Value = model.isInCalculate;
            parameters[8].Value = model.remark;
            parameters[9].Value = model.line;
            parameters[10].Value = model.xmno;
            parameters[11].Value = model.pointname;
            parameters[12].Value = Convert.ToInt32(model.datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateInitDegree(data.Model.gtpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update gtpointalarmvalue set ");
            
            strSql.Append("   remark=@remark    ");
            strSql.Append("   where   ");
            strSql.Append("  xmno = @xmno   and   pointname=@pointname   and   datatype = @datatype");
            OdbcParameter[] parameters = {
					 new OdbcParameter("@remark", OdbcType.VarChar,20),
                    new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@pointname", OdbcType.VarChar,120),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };

           
            parameters[0].Value = model.remark;
            parameters[1].Value = model.xmno;
            parameters[2].Value = model.pointname;
            parameters[3].Value = Convert.ToInt32(model.datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, data.Model.gtpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update gtpointalarmvalue set ");
            strSql.Append("firstalarmname=@firstalarmname,");
            strSql.Append("secondalarmname=@secondalarmname,");
            strSql.Append("thirdalarmname=@thirdalarmname,");
            strSql.Append("surfaceName=@surfaceName,");
            strSql.Append(" remark=@remark ");
            strSql.Append("   where   ");
            strSql.Append(" pointname  in ('" + pointNameStr + "')   and xmno = @xmno ");
            OdbcParameter[] parameters = {

					new OdbcParameter("@firstalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@secondalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@thirdalarmname", OdbcType.VarChar,120),
                    new OdbcParameter("@surfacename", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };



            parameters[0].Value = model.firstalarmname;
            parameters[1].Value = model.secondalarmname;
            parameters[2].Value = model.thirdalarmname;
            parameters[3].Value = model.surfaceName;
            parameters[4].Value = model.remark;
            parameters[5].Value = model.xmno;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool SurfaceMultiUpdate(string pointNameStr, data.Model.gtpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update gtpointalarmvalue set ");
            strSql.Append(" surfaceName=@surfaceName ");
            strSql.Append("   where   ");
            strSql.Append(" pointname  in ('" + pointNameStr + "')   and xmno = @xmno ");
            OdbcParameter[] parameters = {

                    new OdbcParameter("@surfaceName", OdbcType.VarChar,100),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };



            parameters[0].Value = model.surfaceName;
            parameters[1].Value = model.xmno;
            


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(data.Model.gtpointalarmvalue model)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("delete from  gtpointalarmvalue  where pointname in (@pointname) and  xmno = @xmno  ");
            OdbcParameter[] parameters = {
                new OdbcParameter("@pointname", OdbcType.VarChar,120),
                new OdbcParameter("@xmno", OdbcType.Int,4)
			};
            parameters[0].Value = model.pointname;
            parameters[1].Value = model.xmno;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 点名加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="ls"></param>
        /// <returns></returns>
        public bool GTSensorDataPointLoadDAL(int xmno,Model.gtsensortype datatype ,out List<string> ls)
        {

            string sql = "select distinct(pointname) from gtpointalarmvalue where xmno='" + xmno + "'  and  datatype='"+Convert.ToInt32(datatype)+"'    order by pointname asc";

            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }

        //public bool GTSurfacePointLoadDAL(int xmno,string surfacename,)
        //{
 
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public data.Model.gtpointalarmvalue DataRowToModel(DataRow row)
        {
            data.Model.gtpointalarmvalue model = new data.Model.gtpointalarmvalue();
            if (row != null)
            {
                
                if (row["xmno"] != null)
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["pointname"] != null)
                {
                    model.pointname = row["pointname"].ToString();
                }
                if (row["datatype"] != null)
                {
                    model.datatype = (data.Model.gtsensortype)row["datatype"];
                }
                if (row["firstalarmname"] != null)
                {
                    model.firstalarmname = row["firstalarmname"].ToString();
                }
                if (row["secondalarmname"] != null)
                {
                    model.secondalarmname = row["secondalarmname"].ToString();
                }
                if (row["thirdalarmname"] != null)
                {
                    model.thirdalarmname = row["thirdalarmname"].ToString();
                }

                if (row["orificeHeight"] != null)
                {
                    model.orificeHeight = Convert.ToDouble(row["orificeHeight"].ToString());
                }
                if (row["thisOrificeHeight"] != null)
                {
                    model.thisOrificeHeight = Convert.ToDouble(row["thisOrificeHeight"].ToString());
                }
                if (row["line"] != null)
                {
                    model.line = Convert.ToDouble(row["line"].ToString());
                }
                if (row["surfaceName"] != null)
                {
                    model.surfaceName = row["surfaceName"].ToString();
                }
                if (row["isInCalculate"] != null)
                {
                    model.isInCalculate =row["isInCalculate"].ToString() == "0"?false:true;
                }
                if (row["remark"] != null)
                {
                    model.remark = row["remark"].ToString();
                }
                if (row["sensorno"] != null)
                {
                    model.sensorno = row["sensorno"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, data.Model.gtsensortype datatype ,out data.Model.gtpointalarmvalue model)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from gtpointalarmvalue where       xmno=@xmno   and  datatype=@datatype  and  pointname=@pointname   ");

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,10),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),
                    new OdbcParameter("@pointname", OdbcType.VarChar,200)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = pointname;
            model = new data.Model.gtpointalarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelist(int xmno,  data.Model.gtsensortype datatype, out List<data.Model.gtpointalarmvalue> modellist)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from gtpointalarmvalue  where       xmno=@xmno   and  datatype=@datatype    and    isInCalculate = 1    ");

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,10),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            modellist = new List<data.Model.gtpointalarmvalue>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while(i < ds.Tables[0].Rows.Count )
            {
                modellist.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return i > 0 ? true : false;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetSensornoModel(int xmno, string sensorno, out data.Model.gtpointalarmvalue model)
        {
            SingleTonOdbcSQLHelper singleTonOdbcSQLHelper = new SingleTonOdbcSQLHelper { Conn = db.GetSurveyStanderConn(xmno) };
            //OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            //SingleTonOdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from gtpointalarmvalue where       xmno=@xmno   and   sensorno=@sensorno   ");

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,10),
                    new OdbcParameter("@sensorno", OdbcType.VarChar,200)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = sensorno;
            model = new data.Model.gtpointalarmvalue();
            DataSet ds = singleTonOdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool PointAlarmValueTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord,Model.gtsensortype datatype ,out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            string sql = string.Format("select pointname,sensorno,datatype,firstalarmname,secondalarmname,thirdalarmname,OrificeHeight,ThisOrificeHeight,line,surfaceName,isInCalculate,remark from gtpointalarmvalue where xmno='{3}' and  datatype='{5}'  {4} {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring, datatype);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string colName, string sord, Model.gtsensortype datatype, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select pointname,sensorno,firstalarmname,secondalarmname,thirdalarmname,remark from gtpointalarmvalue where xmno='{3}' and   datatype='{5}'  and  {4}  {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring, Convert.ToInt32(datatype));
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointGTWaterLineTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string colName, string sord, Model.gtsensortype datatype, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select pointname,sensorno,firstalarmname,secondalarmname,thirdalarmname,OrificeHeight,thisOrificeHeight,line,remark from gtpointalarmvalue where xmno='{3}' and   datatype='{5}'  and  {4}  {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring, Convert.ToInt32(datatype));
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointGTSupportingAxialForceLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string colName, string sord, Model.gtsensortype datatype, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select pointname,sensorno,firstalarmname,secondalarmname,thirdalarmname,isInCalculate,surfaceName,OrificeHeight,remark from gtpointalarmvalue where xmno='{3}' and   datatype='{5}'  and  {4}  {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring, Convert.ToInt32(datatype));
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }


        public bool PointAlarmValueTableRowsCount( string searchstring, int xmno, Model.gtsensortype datatype, out string totalCont)
        {

            string sql = "select count(1) from gtpointalarmvalue where xmno = '" + xmno + "' and  datatype="+Convert.ToInt32(datatype)+"   ";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }
        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

