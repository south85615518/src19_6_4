﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsettlementresultdata.cs
*
* 功 能： N/A
* 类 名： gtsettlementresultdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//Please add references
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtsettlementresultdata
	/// </summary>
	public partial class gtsettlementresultdata
	{
        public static database db = new database();
        
		public gtsettlementresultdata()
		{}
		#region  测量数据

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("mkh", "gtsettlementresultdata"); 
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string point_name,int mkh,int xmno,DateTime time)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from gtsettlementresultdata");
        //    strSql.Append(" where point_name=@point_name and mkh=@mkh and xmno=@xmno and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@point_name", OdbcType.VarChar,100),
        //            new OdbcParameter("@mkh", OdbcType.Int,11),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@time", OdbcType.DateTime)			};
        //    parameters[0].Value = point_name;
        //    parameters[1].Value = mkh;
        //    parameters[2].Value = xmno;
        //    parameters[3].Value = time;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsettlementresultdata model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into gtsettlementresultdata(");
            strSql.Append("xmno,point_name,settlementvalue,degree,settlementdiff,this_val,ac_val,d_val,time,initsettlementval)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@point_name,@settlementvalue,@degree,@settlementdiff,@this_val,@ac_val,@d_val,@time,@initsettlementval)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@settlementvalue", OdbcType.Double),
					new OdbcParameter("@degree", OdbcType.Double),
					new OdbcParameter("@settlementdiff", OdbcType.Double),
					new OdbcParameter("@this_val", OdbcType.Double),
					new OdbcParameter("@ac_val", OdbcType.Double),
					new OdbcParameter("@d_val", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@initsettlementval", OdbcType.Double)};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.settlementvalue;
            parameters[3].Value = model.degree;
            parameters[4].Value = model.settlementdiff;
            parameters[5].Value = model.this_val;
            parameters[6].Value = model.ac_val;
            parameters[7].Value = model.d_val;
            parameters[8].Value = model.time;
            parameters[9].Value = model.initsettlementval;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        
        public bool AddData(int xmno, DateTime starttime, DateTime endtime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("REPLACE into gtsettlementresultdata_data (select * from gtsettlementresultdata   where   xmno=@xmno     and    time between   @starttime   and   @endtime ) ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.Int),
                                             new OdbcParameter("@starttime", OdbcType.DateTime), 
                                             new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdataSingleScalarSurveyData(int xmno, string pointname, DateTime time, string vSet_name, string vLink_name, DateTime srcdatetime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.AppendFormat("update gtsettlementresultdata_data set {1} = {1}+({0}-(select {0} from gtsettlementresultdata where xmno=gtsettlementresultdata_data.xmno and point_name= gtsettlementresultdata_data.point_name and time =     {5})), {0}=(select   {0}   from gtsettlementresultdata where xmno=gtsettlementresultdata_data.xmno and point_name= gtsettlementresultdata_data.point_name and time =    {5})  where    xmno= '{2}'    and point_name  =   '{3}'   and  time =   {4}   ", vSet_name, vLink_name, xmno, pointname, time, srcdatetime);

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //public bool SurveyPointCycDataAdd(string xmname, string pointname, DateTime starttime, DateTime endtime)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
        //    strSql.AppendFormat(@"insert into fmos_cycdirnet_data (point_name,cyc,taskname,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time) select point_name," + importcyc + " as cyc,taskname,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,(select min(time) from fmos_cycdirnet_data where taskname='" + xmname + "' and  cyc = " + importcyc + "  ) as time from  fmos_cycdirnet where taskname = '" + xmname + "'  and   point_name   =   '" + pointname + "'   and     cyc=" + cyc);

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        //public bool UpdataSurveyData(int xmno,string pointname,double offsetval, DateTime srcdatetime)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
        //    strSql.AppendFormat("update gtsettlementresultdata_data set this_val+{0},ac_val+{1},d_val+{2}  ", vSet_name, vLink_name, taskname, pointname, cyc, srcdatacyc);

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}




		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(data.Model.gtsettlementresultdata model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update gtsettlementresultdata set ");
            strSql.Append("settlementvalue=@settlementvalue,");
            strSql.Append("degree=@degree,");
            strSql.Append("settlementdiff=@settlementdiff,");
            strSql.Append("this_val=@this_val,");
            strSql.Append("ac_val=@ac_val,");
            strSql.Append("d_val=@d_val,");
            strSql.Append("initsettlementval=@initsettlementval");
            strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@settlementvalue", OdbcType.Double),
					new OdbcParameter("@degree", OdbcType.Double),
					new OdbcParameter("@settlementdiff", OdbcType.Double),
					new OdbcParameter("@this_val", OdbcType.Double),
					new OdbcParameter("@ac_val", OdbcType.Double),
					new OdbcParameter("@d_val", OdbcType.Double),
					new OdbcParameter("@initsettlementval", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)};
            parameters[0].Value = model.settlementvalue;
            parameters[1].Value = model.degree;
            parameters[2].Value = model.settlementdiff;
            parameters[3].Value = model.this_val;
            parameters[4].Value = model.ac_val;
            parameters[5].Value = model.d_val;
            parameters[6].Value = model.initsettlementval;
            parameters[7].Value = model.xmno;
            parameters[8].Value = model.point_name;
            parameters[9].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false; 
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string point_name,int mkh,int xmno,DateTime time)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from gtsettlementresultdata ");
			strSql.Append(" where point_name=@point_name  and xmno=@xmno and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@mkh", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = point_name;
			parameters[1].Value = mkh;
			parameters[2].Value = xmno;
			parameters[3].Value = time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        public bool DeleteTmp(int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            SingleTonOdbcSQLHelper sqlHelper = new SingleTonOdbcSQLHelper();
            sqlHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from gtsettlementresultdata_tmp ");
            strSql.Append(" where xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int)
							};
            parameters[0].Value = xmno;


            int rows = sqlHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from gtsettlementresultdata  where   xmno=@xmno   and point_name = @point_name  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }
        public bool ResultDataReportPrint(string sql, int xmno, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            dt = querysql.querystanderdb(sql, conn);
            return true;
        }

       
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public data.Model.gtsettlementresultdata GetModel(string point_name,int mkh,int xmno,DateTime time)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select xmno,point_name,settlementvalue,degree,settlementdiff,this_val,ac_val,d_val,time,initsettlementval from gtsettlementresultdata ");
			strSql.Append(" where point_name=@point_name and and xmno=@xmno and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)			
                                         };
			parameters[0].Value = point_name;
			parameters[1].Value = xmno;
			parameters[2].Value = time;

			data.Model.gtsettlementresultdata model=new data.Model.gtsettlementresultdata();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

        public bool dataDATATempDel(int xmno)
        {
            StringBuilder strSql = new StringBuilder(256);
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from gtsettlementresultdata_tmp where xmno = @xmno");
            OdbcParameter[] param =  {
                 new OdbcParameter("@xmno",OdbcType.Int,11)
            };
            param[0].Value = xmno;
            int row = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),param);
            if (row > 0)
            {
                return true;
            }
            return false;
        }

       



		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public data.Model.gtsettlementresultdata DataRowToModel(DataRow row)
		{
            data.Model.gtsettlementresultdata model = new data.Model.gtsettlementresultdata();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                if (row["settlementvalue"] != null || row["settlementvalue"] != "")
                {
                    model.settlementvalue = Convert.ToDouble(row["settlementvalue"].ToString());
                }
                if (row["degree"] != null)
                {
                    model.degree = Convert.ToDouble(row["degree"].ToString());
                }
                if (row["settlementdiff"] != null)
                {
                    model.settlementdiff = Convert.ToDouble(row["settlementdiff"].ToString());
                }
                if (row["this_val"] != null)
                {
                    model.this_val = Convert.ToDouble(row["this_val"].ToString());
                }
                if (row["ac_val"] != null)
                {
                    model.ac_val = Convert.ToDouble(row["ac_val"].ToString());
                }
                if (row["d_val"] != null)
                {
                    model.d_val = Convert.ToDouble(row["d_val"].ToString());
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
                //model.initsettlementval=row["initsettlementval"].ToString();
            }
            return model;
		}
        
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select point_name,mkh,settlementvalue,groundElevation,xmno,id,time ");
			strSql.Append(" FROM gtsettlementresultdata ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        //public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt)
        //{
        //    OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select  max(time)  from gtsettlementresultdata  where   xmno=@xmno   and point_name = @point_name  ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@xmno", OdbcType.Int),
        //            new OdbcParameter("@point_name", OdbcType.VarChar,120)
        //                    };
        //    parameters[0].Value = xmno;
        //    parameters[1].Value = pointname;
        //    object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
        //    if (obj == null) { dt = new DateTime(); return false; }
        //    dt = Convert.ToDateTime(obj); return true;

        //}

        public bool MaxTime(int xmno, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtsettlementresultdata  where   xmno = @xmno ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int)
                

            };
            paramters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }


        public bool MaxTime(int xmno, string point_name,out DateTime maxTime)
        {
            maxTime = new DateTime();
            SingleTonOdbcSQLHelper sqlhelper = new SingleTonOdbcSQLHelper();
            sqlhelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtsettlementresultdata  where     xmno = @xmno    and  point_name=@point_name");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int),
                new OdbcParameter("@point_name",OdbcType.VarChar,200)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = point_name;
            object obj = sqlhelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool MinTime(int xmno, string point_name, out DateTime MinTime)
        {
            MinTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Min(time) from gtsettlementresultdata  where   xmno = @xmno   and point_name=@point_name  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.VarChar,100),
                new OdbcParameter("@point_name",OdbcType.VarChar,100)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = point_name;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            MinTime = Convert.ToDateTime(obj);
            return true;
        }

		/// <summary>
		/// 获取记录总数
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM gtsettlementresultdata ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.time desc");
			}
			strSql.Append(")AS Row, T.*  from gtsettlementresultdata T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        public bool GetgtsettlementresultdataList(int xmno,string point_name,DateTime starttime,DateTime endtime, out List<data.Model.gtsettlementresultdata> li)
        {
            li = new List<Model.gtsettlementresultdata>();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select xmno,point_name,settlementvalue,degree,settlementdiff,this_val,ac_val,d_val,time,initsettlementval  ");
            strSql.Append(" FROM gtsettlementresultdata where  xmno = @xmno       and    time   between    @starttime  and   @endtime  ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    //new OdbcParameter("@point_name", OdbcType.VarChar,200),
                    new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime)
                    
                                         };

            parameters[0].Value = xmno;
            //parameters[1].Value = point_name;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;

            while (i < ds.Tables[0].Rows.Count)
            {
                li.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

       
#endregion 

        public bool GetModel(int xmno, string pointname, DateTime dt, out data.Model.gtsettlementresultdata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,point_name,settlementvalue,degree,settlementdiff,this_val,ac_val,d_val,time,initsettlementval ");
            strSql.Append(" FROM   gtsettlementresultdata   where xmno =   @xmno    and     point_name =  @point_name    and   time = @time   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

        public bool GetLastTimeModel(int xmno, string pointname, DateTime dt, out data.Model.gtsettlementresultdata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select time,xmno,point_name,settlementvalue,degree,settlementdiff,round(this_val,2) as this_val,round(ac_val,2) as ac_val,d_val,initsettlementval ");
            strSql.Append(" FROM   gtsettlementresultdata   where xmno =   @xmno    and     point_name =  @point_name    and   time < @time   order by time desc limit 0,2  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

         /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(int xmno, out List<data.Model.gtsettlementresultdata> li)
        {
            li = new List<Model.gtsettlementresultdata>();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select  xmno,point_name,settlementvalue,degree,settlementdiff,this_val,ac_val,d_val,time,initsettlementval  ");
            strSql.Append(" FROM gtsettlementresultdata_tmp where xmno = @xmno ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@xmno", OdbcType.Int,11)};

            parameters[0].Value = xmno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;

            while (i < ds.Tables[0].Rows.Count)
            {
                li.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }
        public bool PointNameDateTimeListGet(int xmno,string pointname,  out List<string> ls)
        {

            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  gtsettlementresultdata    where         xmno=@xmno    and        {0}     order by time  asc  ", pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int)
							};
            parameters[0].Value = xmno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }
        public bool ResultDataReportPrint(string sql, string xmname, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            dt = querysql.querystanderdb(sql, conn);
            return true;
        }


        public bool SettlementDATATableLoad(DateTime starttime, DateTime endtime, int startPageIndex, int pageSize, int xmno, string pointname, string sord, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select  xmno,point_name,settlementvalue,degree,settlementdiff, round(this_val,2) as this_val,round(ac_val,2) as ac_val,round(d_val,2) as d_val,time,initsettlementval  from  gtsettlementresultdata  where      xmno=@xmno   and    {0}    and     time    between    @starttime      and    @endtime      order by {1}  asc     limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@starttime", OdbcType.DateTime),
					new OdbcParameter("@endtime", OdbcType.DateTime),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool SettlementTableRowsCount(DateTime starttime, DateTime endtime, int xmno, string pointname, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from gtsettlementresultdata where xmno='{2}' and {3}  and time between '{0}' and '{1}'", starttime, endtime, xmno, searchstr);
            OdbcConnection conn = db.GetStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }

        public bool OnLinePointCont(string unitname,out string contpercent)
        {
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            contpercent = "0/0";
            string sql = "select count(distinct(gtsettlementpointalarmvalue.point_name)),(select count(1) from gtsettlementpointalarmvalue )  from gtsettlementpointalarmvalue left join gtsettlementresultdata on gtsettlementresultdata.xmno = gtsettlementpointalarmvalue.xmno  and  gtsettlementpointalarmvalue.point_name = gtsettlementresultdata.point_name  where time between DATE_ADD(SYSDATE(),INTERVAL -1 day) and SYSDATE()";
            DataSet ds = OdbcSQLHelper.Query(sql);
            if (ds == null) return false;
            DataTable dt = ds.Tables[0];
            contpercent = string.Format("{0}/{1}", dt.Rows[0].ItemArray[0], dt.Rows[0].ItemArray[1]);//querysql.querystanderstr(sql, 
            return true;
        }

		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

