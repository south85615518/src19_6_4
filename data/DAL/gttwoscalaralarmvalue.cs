﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using SqlHelpers;

namespace data.DAL
{
    public partial class gtalarmvalue
    {
        /// <summary>
        /// 双量预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TwoScalarTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, data.Model.gtsensortype datatype, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format(@"select 
                            alarmname,
                            xmno,
                            datatype,
                            valuetype,
                            first_this_scalarvalue_u,
                            first_this_scalarvalue_l,
                            first_ac_scalarvalue_u,
                            first_ac_scalarvalue_l,
                            sec_this_scalarvalue_u,
                            sec_this_scalarvalue_l,
                            sec_ac_scalarvalue_u,
                            sec_ac_scalarvalue_l     
from  gtalarmvalue    where      xmno =  '" + xmno + "' and   datatype='" + Convert.ToInt32(datatype) + "'    {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 双量预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TwoScalarTableRowsCount(string searchstring, int xmno, data.Model.gtsensortype datatype, out int totalCont)
        {
            string sql = "select count(1) from gtalarmvalue where xmno = '" + xmno + "'    and   valuetype = " + datatype;
            if (searchstring != null && searchstring.Trim() != "1=1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;

        }

        /// <summary>
        /// 得到双量预警一个对象实体
        /// </summary>
        public data.Model.gtalarmvalue TwoscalarvalueDataRowToModel(DataRow row)
        {
            data.Model.gtalarmvalue model = new data.Model.gtalarmvalue();
            if (row != null)
            {
                if (row["alarmname"] != null)
                {
                    model.alarmname = row["alarmname"].ToString();
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["datatype"] != null && row["datatype"].ToString() != "")
                {
                    model.datatype = (Model.gtsensortype)row["datatype"];
                }
                if (row["first_this_scalarvalue_u"] != null && row["first_this_scalarvalue_u"].ToString() != "")
                {
                    model.first_this_scalarvalue_u = Convert.ToDouble(Convert.ToDouble(row["first_this_scalarvalue_u"]).ToString("0.000"));
                }
                if (row["first_this_scalarvalue_l"] != null && row["first_this_scalarvalue_l"].ToString() != "")
                {
                    model.first_this_scalarvalue_l = Convert.ToDouble(Convert.ToDouble(row["first_this_scalarvalue_l"]).ToString("0.000"));
                }
                if (row["first_ac_scalarvalue_u"] != null && row["first_ac_scalarvalue_u"].ToString() != "")
                {
                    model.first_ac_scalarvalue_u = Convert.ToDouble(Convert.ToDouble(row["first_ac_scalarvalue_u"]).ToString("0.000"));
                }
                if (row["first_ac_scalarvalue_l"] != null && row["first_ac_scalarvalue_l"].ToString() != "")
                {
                    model.first_ac_scalarvalue_l = Convert.ToDouble(Convert.ToDouble(row["first_ac_scalarvalue_l"]).ToString("0.000"));
                }

                if (row["sec_this_scalarvalue_u"] != null && row["sec_this_scalarvalue_u"].ToString() != "")
                {
                    model.sec_this_scalarvalue_u = Convert.ToDouble(Convert.ToDouble(row["sec_this_scalarvalue_u"]).ToString("0.000"));
                }
                if (row["sec_this_scalarvalue_l"] != null && row["sec_this_scalarvalue_l"].ToString() != "")
                {
                    model.sec_this_scalarvalue_l = Convert.ToDouble(Convert.ToDouble(row["sec_this_scalarvalue_l"]).ToString("0.000"));
                }
                if (row["sec_ac_scalarvalue_u"] != null && row["sec_ac_scalarvalue_u"].ToString() != "")
                {
                    model.sec_ac_scalarvalue_u = Convert.ToDouble(Convert.ToDouble(row["sec_ac_scalarvalue_u"]).ToString("0.000"));
                }
                if (row["sec_ac_scalarvalue_l"] != null && row["sec_ac_scalarvalue_l"].ToString() != "")
                {
                    model.sec_ac_scalarvalue_l = Convert.ToDouble(Convert.ToDouble(row["sec_ac_scalarvalue_l"]).ToString("0.000"));
                }
            }
            return model;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public data.Model.gtalarmvalue TwoScalarvalueGetModel(string name, data.Model.gtsensortype datatype, int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select alarmname,
                            xmno,
                            datatype,
                            valuetype,
                            first_this_scalarvalue_u,
                            first_this_scalarvalue_l,
                            first_ac_scalarvalue_u,
                            first_ac_scalarvalue_l,
                            sec_this_scalarvalue_u,
                            sec_this_scalarvalue_l,
                            sec_ac_scalarvalue_u,
                            sec_ac_scalarvalue_l  from gtalarmvalue ");
            strSql.Append(" where        alarmname=@alarmname       and         xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@alarmname", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = name;
            parameters[1].Value = xmno;

            data.Model.gtalarmvalue model = new data.Model.gtalarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return TwoscalarvalueDataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到双量预警一个对象实体
        /// </summary>
        public bool GetTwoScalarvalueModel(string name, int xmno, data.Model.gtsensortype datatype, out data.Model.gtalarmvalue model)
        {
            model = new Model.gtalarmvalue();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append(@"select alarmname,
                            xmno,
                            datatype,
                            valuetype,
                            first_this_scalarvalue_u,
                            first_this_scalarvalue_l,
                            first_ac_scalarvalue_u,
                            first_ac_scalarvalue_l,
                            sec_this_scalarvalue_u,
                            sec_this_scalarvalue_l,
                            sec_ac_scalarvalue_u,
                            sec_ac_scalarvalue_l
from gtalarmvalue ");
            strSql.Append(" where        alarmname=@alarmname       and         xmno=@xmno   and   datatype=@datatype  ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@alarmname", OdbcType.VarChar,100),
			        new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };
            parameters[0].Value = name;
            parameters[1].Value = xmno;
            parameters[2].Value = datatype;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = TwoscalarvalueDataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
