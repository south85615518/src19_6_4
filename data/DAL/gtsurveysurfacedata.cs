﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacedata.cs
*
* 功 能： N/A
* 类 名： gtsurfacedata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 #SensorName# Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtsurfacedata
	/// </summary>
	public partial class gtsurfacedata
	{
        public bool AddData(int xmno,data.Model.gtsensortype datatype,int cyc)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("REPLACE into gtsurfacedata_data (select * from gtsurfacedata   where   xmno=@xmno   and  datatype = @datatype   and    cyc = @cyc  and   surfacename in ( select distinct(surfacename) from  gtsurfacestructure where xmno = " + xmno + "  and  datatype = @_datatype  ) )");
            ExceptionLog.ExceptionWrite(strSql.ToString());
            OdbcParameter[] parameters = { new OdbcParameter("@xmno", OdbcType.VarChar),
                                           new OdbcParameter("@datatype", OdbcType.Int),
                                           new OdbcParameter("@cyc", OdbcType.Int),
                                           new OdbcParameter("@_datatype", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = cyc;
            parameters[3].Value = Convert.ToInt32(datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdataSurveySurfaceData(data.Model.gtsurfacedata model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.AppendFormat(@"update gtsurfacedata_data set this_val = this_val - ac_val +{0},
                ac_val = {0}  where    xmno= '{1}'  and  datatype = {2}   and surfacename = '{3}'     and  cyc =   {4}   ", model.ac_val, model.xmno,Convert.ToInt32(model.datatype) ,model.surfacename,  model.cyc);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdataSurveySurfaceDataNextCYCThis(data.Model.gtsurfacedata model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.AppendFormat(@"update gtsurfacedata_data set this_val = ac_val - {0}
                where    xmno= '{1}'  and   datatype = {2}    and  surfacename='{3}'    and  cyc =   {4}   ", model.ac_val, model.xmno, Convert.ToInt32(model.datatype) ,model.surfacename,  model.cyc+1);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteSurvey(int xmno,data.Model.gtsensortype datatype ,int startcyc, int endcyc)
        {

            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from  gtsurfacedata_data  where   xmno=@xmno   and  datatype = @datatype   and  cyc   between    @startcyc      and    @endcyc  ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.Int),
                                             new OdbcParameter("@datatype", OdbcType.Int),
                                             new OdbcParameter("@startcyc", OdbcType.Int),
                                             new OdbcParameter("@endcyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = Convert.ToInt32(datatype);
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public bool SurveySurfaceFaceTimeDataAdd(int xmno, string pointname, DateTime time, DateTime importtime)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
        //    strSql.AppendFormat(@"insert into gtsurfacedata_data (xmno,surfacename,first_oregion_scalarvalue,time) select xmno,surfacename,datatype,senorno,first_oregion_scalarvalue,time from  gtsurfacedata_data where xmno = '" + xmno + "'  and  datatype='{1}'  and   surfacename   =   '" + pointname + "'   and     time='{0}'", importtime);

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        public bool UpdataSurveyData(int xmno, string pointname, DateTime time,string vSet_name, string vLink_name, DateTime srcdatetime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.AppendFormat("update gtsurfacedata_data set {1} = {1}+({0}-(select {0} from gtsurfacedata where xmno=gtsurfacedata_data.xmno and surfacename= gtsurfacedata_data.surfacename and time =     '{5}')), {0}=(select   {0}   from gtsurfacedata where xmno=gtsurfacedata_data.xmno and surfacename= gtsurfacedata_data.surfacename and time =    '{5}')  where       xmno= '{2}'     and  surfacename  =   '{3}'   and  time =   '{4}'   ", vSet_name, vLink_name, xmno, pointname, time, srcdatetime);
            string str = strSql.ToString();
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SurveyResultTableRowsCount(int xmno,data.Model.gtsensortype datatype ,string pointname,  int startcyc, int endcyc, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  surfacename = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from  gtsurfacedata_data where xmno='{0}' and  datatype = {4}  and  cyc  between   '{2}'  and   '{3}'     and   {1}", xmno, searchstr, startcyc, endcyc, Convert.ToInt32(datatype));
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteSurveyData(int xmno, string surfacename, DateTime starttime, DateTime endtime)
        {
            string searchstr = surfacename == "" || surfacename == null || (surfacename.IndexOf("全部") != -1) ? "  1=1  " : string.Format("surfacename='{0}'", surfacename);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from gtsurfacedata_data ");
            strSql.AppendFormat(" where    xmno=@xmno    and    {0}        and time  between     @starttime    and     @endtime ", searchstr);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,100),
					new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool PointNameSurveyDateTimeListGet(int xmno, data.Model.gtsensortype datatype, string pointname, out List<string> ls)
        {

            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  gtsurfacedata_data    where         xmno='{0}'  and  datatype = "+Convert.ToInt32(datatype)+"   and     {1}     order by time  asc  ", xmno, pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  surfacename=  '{0}'", pointname));
            ls = new List<string>();
            //OdbcParameter[] parameters = {
            //        new OdbcParameter("@xmno", OdbcType.VarChar,200),
            //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
            //                };
            //parameters[0].Value = xmno;
            //parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            string str = strSql.ToString();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }

        //根据项目名获取所以的周期
        public bool SurveyCYCDateTimeListGet(int xmno,data.Model.gtsensortype datatype ,out List<string> cycTimeList)
        {

            string sql = "select  distinct(cyc),time  from gtsurfacedata_data  where  xmno='" + xmno + "'  and  datatype=" + Convert.ToInt32(datatype) + "   order by cyc asc";
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            DataTable dt = querysql.querystanderdb(sql, conn);
            cycTimeList = new List<string>();
            if (dt.Rows.Count == 0) return false;
            int i = 0;
            for (i = 0; i < dt.Rows.Count; i++)
            {
                cycTimeList.Add(string.Format("{0}[{1}]", dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1]));
            }
            return true;
        }
        public bool SurveyResultdataTableLoad(int startPageIndex, int pageSize, int xmno, string pointname, data.Model.gtsensortype datatype, string sord, int startcyc, int endcyc, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("surfacename='{0}'", pointname);
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"select  surfacename,cyc,SteelChordVal,this_val,ac_val,calculatepointsCont,time  from gtsurfacedata_data  where   xmno=@xmno  and  datatype ="+Convert.ToInt32(datatype)+"   and    {0}   and   cyc  between      @startcyc    and    @endcyc      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);

            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                    

                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = startcyc;
            parameters[2].Value = endcyc;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
		
	}
}

