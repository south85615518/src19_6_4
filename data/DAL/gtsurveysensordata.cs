﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;
using Tool;

namespace data.DAL
{
    public partial class gtsensordata
    {
        public bool AddData(string xmname,int cyc, data.Model.gtsensortype datatype)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            strSql.Append("REPLACE into gtsensordata_data (select * from gtsensordata   where   project_name=@taskname     and    cyc = @cyc  and  datatype = @datatype  and   point_name in ( select distinct(point_name) from  gtpointalarmvalue where xmno = " + xmname + " and datatype=@_datatype  ) )");
            ExceptionLog.ExceptionWrite(strSql.ToString());
            OdbcParameter[] parameters = { new OdbcParameter("@taskname", OdbcType.VarChar),
                                           new OdbcParameter("@cyc", OdbcType.Int),
                                           new OdbcParameter("@datatype", OdbcType.VarChar),
                                           new OdbcParameter("@_datatype", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = cyc;
            parameters[2].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[3].Value =  Convert.ToInt32(datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SurveyPointTimeDataAdd(string xmname, string pointname, data.Model.gtsensortype datatype, DateTime time, DateTime importtime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            strSql.AppendFormat(@"insert into gtsensordata_data (project_name,point_name,datatype,senorno,first_oregion_scalarvalue,time) select project_name,point_name,datatype,senorno,first_oregion_scalarvalue,time from  gtsensordata_data where project_name = '" + xmname + "'  and  datatype='{1}'  and   point_name   =   '" + pointname + "'   and     time='{0}'", importtime, data.DAL.gtsensortype.GTSensorTypeToString(datatype));

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdataSurveyData(data.Model.gtsensordata model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.project_name);
            strSql.AppendFormat(@"update gtsensordata_data set single_this_scalarvalue = single_this_scalarvalue - single_ac_scalarvalue +{0},
                single_ac_scalarvalue = {0}  where    project_name= '{1}'  and point_name = '{2}'    and  datatype='{3}'  and  cyc =   {4}   ", model.single_ac_scalarvalue, model.project_name,model.point_name ,model.datatype, model.cyc);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdataSurveyDataNextCYCThis(data.Model.gtsensordata model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.project_name);
            strSql.AppendFormat(@"update gtsensordata_data set single_this_scalarvalue = single_ac_scalarvalue - {0}
                where    project_name= '{1}'  and  point_name='{2}'  and  datatype='{3}'  and  cyc =   {4}   ", model.single_ac_scalarvalue, model.project_name,model.point_name ,model.datatype, model.cyc);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SurveyResultTableRowsCount(string project_name, string pointname, string sord, Model.gtsensortype datatype, DateTime startTime, DateTime endTime, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from  gtsensordata_data where project_name='{0}' and datatype= '{2}'  and  time>'{3}' and  time < '{4}'     and   {1}", project_name, searchstr, datatype, startTime, endTime);
            OdbcConnection conn = db.GetStanderConn(project_name);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteSurveyData(string xmname, data.Model.gtsensortype datatype, int startcyc, int endcyc)
        {
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from  gtsensordata_data  where   project_name=@project_name  and  datatype=@datatype    and  cyc   between    @startcyc      and    @endcyc  ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@project_name", OdbcType.VarChar,100),
                                             new OdbcParameter("@datatype", OdbcType.VarChar,100),
                                             new OdbcParameter("@startcyc", OdbcType.Int),
                                             new OdbcParameter("@endcyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool PointNameSurveyDateTimeListGet(string xmname, string pointname, data.Model.gtsensortype datatype, out List<string> ls)
        {

            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  gtsensordata_data    where         project_name='{0}'    and   datatype='{1}'   and     {2}     order by time  asc  ", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            //OdbcParameter[] parameters = {
            //        new OdbcParameter("@xmname", OdbcType.VarChar,200),
            //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
            //                };
            //parameters[0].Value = xmname;
            //parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            string str = strSql.ToString();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }
        public bool SurveyResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype, DateTime startTime, DateTime endTime, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"select project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue,sec_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue ,cyc,time  from gtsensordata_data  where   project_name=@xmname     and    datatype=@datatype    and      time
          between  @starttime      and    @endtime       and    {0}      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),


                    new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime),

                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startTime;
            parameters[3].Value = endTime;
            parameters[4].Value = (startPageIndex - 1) * pageSize;
            parameters[5].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool LostPoints(int xmno, string xmname,data.Model.gtsensortype datatype ,int cyc, out string pointstr)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            strSql.AppendFormat("select distinct(POINTNAME) from gtpointalarmvalue where  xmno={0}  and  datatype= {1}   and gtpointalarmvalue.POINTNAME not in ( select POINT_NAME from gtsensordata_data where project_name= '{2}' and  cyc = {3} and  datatype='{4}' )", xmno, Convert.ToInt32(datatype), xmname, cyc, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
            ExceptionLog.ExceptionWrite(strSql.ToString());
            List<string> ls = querysql.querystanderlist(strSql.ToString(), conn);
            pointstr = string.Join(",", ls);
            return true;
        }

        //根据项目名获取所以的周期
        public bool SurveyCycTimeList(string xmname, data.Model.gtsensortype datatype, out List<string> cycTimeList)
        {

            string sql = "select  distinct(cyc),time  from gtsensordata_data  where  project_name='" + xmname + "' and  datatype = '" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "'  order by cyc asc";
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            DataTable dt = querysql.querystanderdb(sql, conn);
            cycTimeList = new List<string>();
            if (dt.Rows.Count == 0) return false;
            int i = 0;
            for (i = 0; i < dt.Rows.Count; i++)
            {
                cycTimeList.Add(string.Format("{0}[{1}]", dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1]));
            }
            return true;
        }
    }
}
