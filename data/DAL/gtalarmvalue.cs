﻿/**  版本信息模板在安装目录下，可自行修改。
* gtalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:27   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using System.Collections.Generic;
using SqlHelpers;
namespace data.DAL
{
	/// <summary>
	/// 
	/// </summary>
	public partial class gtalarmvalue
	{
        public static database db = new database();
		public gtalarmvalue()
		{}
        #region  BasicMethod
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.Append("insert into gtalarmvalue (");
            strSql.Append(@"   xmno,
                            datatype,
                            valuetype,
                            single_this_scalarvalue_u,
                            single_this_scalarvalue_l,
                            single_ac_scalarvalue_u,
                            single_ac_scalarvalue_l,
                            first_this_scalarvalue_u,
                            first_this_scalarvalue_l,
                            sec_this_scalarvalue_u,
                            sec_this_scalarvalue_l,
                            first_ac_scalarvalue_u,
                            first_ac_scalarvalue_l,
                            sec_ac_scalarvalue_u,
                            sec_ac_scalarvalue_l,
                            alarmname  

)");
            strSql.Append(" values (");
            strSql.Append(@"
                            @xmno,
                            @datatype,
                            @valuetype,
                            @single_this_scalarvalue_u,
                            @single_this_scalarvalue_l,
                            @single_ac_scalarvalue_u,
                            @single_ac_scalarvalue_l,
                            @first_this_scalarvalue_u,
                            @first_this_scalarvalue_l,
                            @sec_this_scalarvalue_u,
                            @sec_this_scalarvalue_l,
                            @first_ac_scalarvalue_u,
                            @first_ac_scalarvalue_l,
                            @sec_ac_scalarvalue_u,
                            @sec_ac_scalarvalue_l,
                            @alarmname
)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,100),
					new OdbcParameter("@datatype", OdbcType.Int),
                    new OdbcParameter("@valuetype", OdbcType.Int),
                    new OdbcParameter("@single_this_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@single_this_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@single_ac_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@single_ac_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@first_this_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@first_this_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@sec_this_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@sec_this_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@first_ac_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@first_ac_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@sec_ac_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@sec_ac_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@alarmname", OdbcType.VarChar,100)

                                         };
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.datatype;
            parameters[2].Value = model.valuetype;

            parameters[3].Value = model.single_this_scalarvalue_u;
            parameters[4].Value = model.single_this_scalarvalue_l;
            parameters[5].Value = model.single_ac_scalarvalue_u;
            parameters[6].Value = model.single_ac_scalarvalue_l;
            parameters[7].Value = model.first_this_scalarvalue_u;
            parameters[8].Value = model.first_this_scalarvalue_l;
            parameters[9].Value = model.sec_this_scalarvalue_u;
            parameters[10].Value = model.sec_this_scalarvalue_l;

            parameters[11].Value = model.first_ac_scalarvalue_u;
            parameters[12].Value = model.first_ac_scalarvalue_l;
            parameters[13].Value = model.sec_ac_scalarvalue_u;
            parameters[14].Value = model.sec_ac_scalarvalue_l;

            parameters[15].Value = model.alarmname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            strSql.Append("update gtalarmvalue set ");
             strSql.Append(@"   
                            
                            valuetype = @valuetype,
                            single_this_scalarvalue_u = @single_this_scalarvalue_u,
                            single_this_scalarvalue_l = @single_this_scalarvalue_l,
                            single_ac_scalarvalue_u = @single_ac_scalarvalue_u,
                            single_ac_scalarvalue_l = @single_ac_scalarvalue_l,
                            first_this_scalarvalue_u = @first_this_scalarvalue_u,
                            first_this_scalarvalue_l = @first_this_scalarvalue_l,
                            sec_this_scalarvalue_u = @sec_this_scalarvalue_u,
                            sec_this_scalarvalue_l = @sec_this_scalarvalue_l,
                            first_ac_scalarvalue_u = @first_ac_scalarvalue_u,
                            first_ac_scalarvalue_l = @first_ac_scalarvalue_l,
                            sec_ac_scalarvalue_u = @sec_ac_scalarvalue_u,
                            sec_ac_scalarvalue_l = @sec_ac_scalarvalue_l   ");
            strSql.Append("     where     alarmname=@alarmname     and      xmno=@xmno  and  datatype = @datatype ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@valuetype", OdbcType.Int),
                    new OdbcParameter("@single_this_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@single_this_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@single_ac_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@single_ac_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@first_this_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@first_this_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@sec_this_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@sec_this_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@first_ac_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@first_ac_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@sec_ac_scalarvalue_u", OdbcType.Double),
                    new OdbcParameter("@sec_ac_scalarvalue_l", OdbcType.Double),
                    new OdbcParameter("@alarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@xmno", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.Int)
					};
            
            
            parameters[0].Value = model.valuetype;
            parameters[1].Value = model.single_this_scalarvalue_u;
            parameters[2].Value = model.single_this_scalarvalue_l;
            parameters[3].Value = model.single_ac_scalarvalue_u;
            parameters[4].Value = model.single_ac_scalarvalue_l;
            parameters[5].Value = model.first_this_scalarvalue_u;
            parameters[6].Value = model.first_this_scalarvalue_l;
            parameters[7].Value = model.sec_this_scalarvalue_u;
            parameters[8].Value = model.sec_this_scalarvalue_l;

            parameters[9].Value = model.first_ac_scalarvalue_u;
            parameters[10].Value = model.first_ac_scalarvalue_l;
            parameters[11].Value = model.sec_ac_scalarvalue_u;
            parameters[12].Value = model.sec_ac_scalarvalue_l;

            parameters[13].Value = model.alarmname;
            parameters[14].Value = model.xmno;
            parameters[15].Value = Convert.ToInt32(model.datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int xmno, string alarmname)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from gtalarmvalue ");
            strSql.Append("     where     alarmname=@alarmname    and    xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@alarmname", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
            parameters[0].Value = alarmname;
            parameters[1].Value = xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                PointAlarmValueDelCasc(alarmname, xmno);
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(string alarmname, int xmno)
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update gtpointalarmvalue set firstalarmname='' where xmno=@xmno and firstalarmname=@firstalarmname");
            //二级预警
            ls.Add("update gtpointalarmvalue set secondalarmname='' where xmno=@xmno and secondalarmname=@secondalarmname");
            //三级预警
            ls.Add("update gtpointalarmvalue set thirdalarmname='' where xmno=@xmno and thirdalarmname=@thirdalarmname");
            List<string> cascSql = ls;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@xmno", "'" + xmno + "'");
                temp = temp.Replace("@firstalarmname", "'" + alarmname + "'");
                temp = temp.Replace("@secondalarmname", "'" + alarmname + "'");
                temp = temp.Replace("@thirdalarmname", "'" + alarmname + "'");

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(temp, conn);


            }
            return true;
        }
         /// <summary>
        /// 单量预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool SingleScalarTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord,data.Model.gtsensortype datatype , out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format(@"select 
                            alarmname,
                            xmno,
                            datatype,
                            valuetype,
                            single_this_scalarvalue_u,
                            single_this_scalarvalue_l,
                            single_ac_scalarvalue_u,
                            single_ac_scalarvalue_l  
from  gtalarmvalue    where      xmno =  '" + xmno + "' and   datatype='"+Convert.ToInt32(datatype)+"'    {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 单量预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool SingleScalarTableRowsCount(string searchstring, int xmno, data.Model.gtsensortype datatype, out int totalCont)
        {
            string sql = "select count(1) from gtalarmvalue where xmno = '" + xmno + "'    and   valuetype = "+datatype;
            if (searchstring != null&&searchstring.Trim() != "1=1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
            
        }

        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno,data.Model.gtsensortype datatype ,out string alarmValueNameStr)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select distinct(alarmname) from gtalarmvalue where xmno = " + xmno + "  and  datatype="+Convert.ToInt32(datatype);
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            alarmValueNameStr = string.Join(";", lsFormat);
            return true;
        }
        /// <summary>
        /// 得到单量预警一个对象实体
        /// </summary>
        public data.Model.gtalarmvalue SinglescalarvalueDataRowToModel(DataRow row)
        {
            data.Model.gtalarmvalue model = new data.Model.gtalarmvalue();
            /*
             *  alarmname,
                            xmno,
                            datatype,
                            valuetype,
                            single_this_scalarvalue_u,
                            single_this_scalarvalue_l,
                            single_ac_scalarvalue_u,
                            single_ac_scalarvalue_l 
             */
            if (row != null)
            {
                if (row["alarmname"] != null)
                {
                    model.alarmname= row["alarmname"].ToString();
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["datatype"] != null && row["datatype"].ToString() != "")
                {
                    model.datatype = (Model.gtsensortype)row["datatype"];
                }
                if (row["single_this_scalarvalue_u"] != null && row["single_this_scalarvalue_u"].ToString() != "")
                {
                    model.single_this_scalarvalue_u = Convert.ToDouble(Convert.ToDouble(row["single_this_scalarvalue_u"]).ToString("0.000"));
                }
                if (row["single_this_scalarvalue_l"] != null && row["single_this_scalarvalue_l"].ToString() != "")
                {
                    model.single_this_scalarvalue_l = Convert.ToDouble(Convert.ToDouble(row["single_this_scalarvalue_l"]).ToString("0.000"));
                }
                 if (row["single_ac_scalarvalue_u"] != null && row["single_ac_scalarvalue_u"].ToString() != "")
                {
                    model.single_ac_scalarvalue_u = Convert.ToDouble(Convert.ToDouble(row["single_ac_scalarvalue_u"]).ToString("0.000"));
                }
                 if (row["single_ac_scalarvalue_l"] != null && row["single_ac_scalarvalue_l"].ToString() != "")
                {
                    model.single_ac_scalarvalue_l = Convert.ToDouble(Convert.ToDouble(row["single_ac_scalarvalue_l"]).ToString("0.000"));
                }
            }
            return model;
        }
        /// <summary>
        /// 得到单量预警一个对象实体
        /// </summary>
        public bool GetSingleScalarvalueModel(string name, int xmno, data.Model.gtsensortype datatype, out data.Model.gtalarmvalue model)
        {
            model = new Model.gtalarmvalue();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append(@"select alarmname,
                            xmno,
                            datatype,
                            valuetype,
                            single_this_scalarvalue_u,
                            single_this_scalarvalue_l,
                            single_ac_scalarvalue_u,
                            single_ac_scalarvalue_l from gtalarmvalue ");
            strSql.Append(" where        alarmname=@alarmname       and         xmno=@xmno   and  datatype = @datatype  ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@alarmname", OdbcType.VarChar,100),
			        new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };
            parameters[0].Value = name;
            parameters[1].Value = xmno;
            parameters[2].Value = Convert.ToInt32(datatype);
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = SinglescalarvalueDataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion  BasicMethod
	}
}

