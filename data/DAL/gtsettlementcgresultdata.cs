﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Data;

namespace data.DAL
{
    public partial class gtsettlementresultdata
    {
        public bool CgPointNameDateTimeListGet(int xmno, string pointname,  out List<string> ls)
        {

            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  gtsettlementresultdata    where         xmno='{0}'    and   {1} order by time  asc  ", xmno, pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            //OdbcParameter[] parameters = {
            //        new OdbcParameter("@xmno", OdbcType.VarChar,200),
            //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
            //                };
            //parameters[0].Value = xmno;
            //parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            string str = strSql.ToString();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }
        public bool CgMaxTime(int xmno, string point_name, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtsettlementresultdata  where   xmno = @xmno  and point_name=@point_name  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.VarChar,100),
                new OdbcParameter("@point_name",OdbcType.VarChar,100)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = point_name;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool CgMinTime(int xmno, string point_name, out DateTime MinTime)
        {
            MinTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Min(time) from gtsettlementresultdata  where   xmno = @xmno   and point_name=@point_name  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.VarChar,100),
                new OdbcParameter("@point_name",OdbcType.VarChar,100)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = point_name;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            MinTime = Convert.ToDateTime(obj);
            return true;
        }



        public bool AddCgData(int xmno,string point_name,DateTime starttime, DateTime endtime ,out int rows)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection Conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            OdbcConnection CgConn = db.GetStanderCgConn(xmno);
            strSql.AppendFormat("REPLACE into {0}.gtsettlementresultdata  select * from {1}.gtsettlementresultdata_data   where   xmno=@xmno  and  point_name=@point_name    and   time  between   @starttime  and   @endtime  ", CgConn.Database, Conn.Database);
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.VarChar,100),
                                             new OdbcParameter("@point_name", OdbcType.VarChar,100),
                                             new OdbcParameter("@starttime", OdbcType.DateTime),
                                             new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = point_name;
            parameters[2].Value = starttime;
            parameters[3].Value = endtime;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除成果数据
        /// </summary>
        public bool DeleteCg(int xmno, string pointname,  DateTime starttime, DateTime endtime)
        {
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from  gtsettlementresultdata  where   xmno=@xmno  and    {0}    and     time    between    @starttime      and    @endtime  ", searchstr);
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.VarChar,100),
                                             new OdbcParameter("@starttime", OdbcType.DateTime),
                                             new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteCgTmp(int xmno)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            strSql.Append("delete  from  gtsettlementresultdata_tmp ");
            strSql.Append(" where xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CgResultdataTableLoad(int startPageIndex, int pageSize, int xmno, string pointname, string sord,  DateTime startTime, DateTime endTime, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select  xmno,point_name,settlementvalue,degree,settlementdiff,this_val,ac_val,d_val,time,initsettlementval  from  gtsettlementresultdata  where      xmno=@xmno   and    {0}    and     time    between    @starttime      and    @endtime      order by {1}  asc     limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@starttime", OdbcType.DateTime),
					new OdbcParameter("@endtime", OdbcType.DateTime),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = startTime;
            parameters[2].Value = endTime;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool CgResultTableRowsCount(int xmno, string pointname, string sord, DateTime startTime, DateTime endTime, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from  gtsettlementresultdata where xmno='{0}'   and  time>'{2}' and  time < '{3}'     and   {1}", xmno, searchstr, startTime, endTime);
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgGetModelList(int xmno, out List<data.Model.gtsettlementresultdata> lt)
        {
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,point_name,settlementvalue,degree,settlementdiff,this_val,ac_val,d_val,time,initsettlementval  from gtsettlementresultdata_tmp  ");
            strSql.Append(" where    xmno=@xmno   order by  point_name   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmno;
            lt = new List<Model.gtsettlementresultdata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


    }
}
