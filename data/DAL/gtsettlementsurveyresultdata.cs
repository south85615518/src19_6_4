﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;
using Tool;

namespace data.DAL
{
    public partial class gtsettlementresultdata
    {
        public bool AddData(int xmno, string point_name, DateTime starttime, DateTime endtime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("REPLACE into gtsettlementresultdata_data select * from gtsettlementresultdata   where   xmno=@xmno      and  point_name=@point_name  and  time  between   @starttime   and   @endtime   ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@xmno", OdbcType.VarChar,100),
                                             new OdbcParameter("@point_name", OdbcType.VarChar,100),
                                             new OdbcParameter("@starttime", OdbcType.DateTime), 
                                             new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = point_name;
            parameters[2].Value = starttime;
            parameters[3].Value = endtime;
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SurveyPointTimeDataAdd(int xmno, string pointname, DateTime time, DateTime importtime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.AppendFormat(@"insert into gtsettlementresultdata_data  select * from  gtsettlementresultdata_data where xmno = '" + xmno + "'    and   point_name   =   '" + pointname + "'   and     time='{0}'", importtime);

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdataSurveyData(int xmno , string pointname, DateTime time, string vSet_name, string vLink_name, DateTime srcdatetime)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.AppendFormat("update gtsettlementresultdata_data set {1} = {1}+({0}-(select {0} from gtsettlementresultdata where xmno=gtsettlementresultdata_data.xmno and point_name= gtsettlementresultdata_data.point_name and time =     '{5}')), {0}=(select   {0}   from gtsettlementresultdata where xmno=gtsettlementresultdata_data.xmno and point_name= gtsettlementresultdata_data.point_name and time =    '{5}')  where       xmno= '{2}'    and  point_name  =   '{3}'   and  time =   '{4}'   ", vSet_name, vLink_name, xmno, pointname, time, srcdatetime);
            string str = strSql.ToString();
            ExceptionLog.ExceptionWrite(str);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteSurveyData(int xmno, string point_name, DateTime starttime, DateTime endtime)
        {
            string searchstr = point_name == "" || point_name == null || (point_name.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", point_name);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from gtsettlementresultdata_data ");
            strSql.AppendFormat(" where    xmno=@xmno    and    {0}     and     time  between     @starttime    and     @endtime ", searchstr);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,100),
					new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool PointNameSurveyDateTimeListGet(int xmno, string pointname, out List<string> ls)
        {

            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  gtsettlementresultdata_data    where         xmno='{0}'       and     {1}     order by time  asc  ", xmno,  pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            //OdbcParameter[] parameters = {
            //        new OdbcParameter("@xmno", OdbcType.VarChar,200),
            //        new OdbcParameter("@datatype", OdbcType.VarChar,200)
            //                };
            //parameters[0].Value = xmno;
            //parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            string str = strSql.ToString();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }
        public bool SettlementSurveyDATATableLoad(DateTime starttime, DateTime endtime, int startPageIndex, int pageSize, int xmno, string pointname, string sord, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select  xmno,point_name,settlementvalue,degree,settlementdiff,this_val,ac_val,d_val,time,initsettlementval  from  gtsettlementresultdata_data  where      xmno=@xmno   and    {0}    and     time    between    @starttime      and    @endtime      order by {1}  asc     limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int),
					new OdbcParameter("@starttime", OdbcType.DateTime),
					new OdbcParameter("@endtime", OdbcType.DateTime),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = starttime;
            parameters[2].Value = endtime;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;
            //ExceptionLog.ExceptionWrite(strSql.ToString());
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }
        public bool SettlementSurveyTableRowsCount(DateTime starttime, DateTime endtime, int xmno, string pointname, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from gtsettlementresultdata where xmno='{2}' and {3}  and time between '{0}' and '{1}'", starttime, endtime, xmno, searchstr);
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }

    }
}
