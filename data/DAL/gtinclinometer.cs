﻿/**  版本信息模板在安装目录下，可自行修改。
* gtinclinometer.cs
*
* 功 能： N/A
* 类 名： gtinclinometer
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 8:51:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtinclinometer
	/// </summary>
	public partial class gtinclinometer
	{
        public static database db = new database();
		public gtinclinometer()
		{}
		#region  BasicMethod

		

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtinclinometer model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            strSql.Append("replace into gtinclinometer(");
            strSql.Append("xmno,holename,deepth,a_nagtive,a_positive,b_nagtive,b_positive,a_planediff,a_avediff,b_planediff,b_avediff,a_ac_diff,b_ac_diff,a_cmp_diff,b_cmp_diff,Time)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@holename,@deepth,@a_nagtive,@a_positive,@b_nagtive,@b_positive,@a_planediff,@a_avediff,@b_planediff,@b_avediff,@a_ac_diff,@b_ac_diff,@a_cmp_diff,@b_cmp_diff,@Time)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
					new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@a_nagtive", OdbcType.Double),
					new OdbcParameter("@a_positive", OdbcType.Double),
					new OdbcParameter("@b_nagtive", OdbcType.Double),
					new OdbcParameter("@b_positive", OdbcType.Double),
					new OdbcParameter("@a_planediff", OdbcType.Double),
					new OdbcParameter("@a_avediff", OdbcType.Double),
					new OdbcParameter("@b_planediff", OdbcType.Double),
					new OdbcParameter("@b_avediff", OdbcType.Double),
					new OdbcParameter("@a_ac_diff", OdbcType.Double),
					new OdbcParameter("@a_cmp_diff", OdbcType.Double),
                    new OdbcParameter("@b_ac_diff", OdbcType.Double),
					new OdbcParameter("@b_cmp_diff", OdbcType.Double),
					new OdbcParameter("@Time", OdbcType.VarChar,100)};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.holename;
            parameters[2].Value = model.deepth;
            parameters[3].Value = model.a_nagtive;
            parameters[4].Value = model.a_positive;
            parameters[5].Value = model.b_nagtive;
            parameters[6].Value = model.b_positive;
            parameters[7].Value = model.a_planediff;
            parameters[8].Value = model.a_avediff;
            parameters[9].Value = model.b_planediff;
            parameters[10].Value = model.b_avediff;
            parameters[11].Value = model.a_ac_diff;
            parameters[12].Value = model.a_cmp_diff;
            parameters[13].Value = model.b_ac_diff;
            parameters[14].Value = model.b_cmp_diff;
            parameters[15].Value = model.time;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(data.Model.gtinclinometer model)
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("delete from gtinclinometer ");
			strSql.Append(" where xmno=@xmno and holename=@holename     and  time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
                    new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = model.xmno;
            parameters[1].Value = model.holename;
            parameters[2].Value = model.deepth;
            parameters[3].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 删除临时表数据
        /// </summary>
        public bool Delete_tmp(int xmno,out int cont)
        {

            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from gtinclinometer_tmp ");
            strSql.Append(" where xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
                                         };
            parameters[0].Value = xmno;
            cont = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (cont >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool GetModel(int xmno,string holename,double deepth,DateTime time,out data.Model.gtinclinometer model )
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select    xmno,holename,deepth,a_nagtive,a_positive,b_nagtive,b_positive,a_planediff,a_avediff,b_planediff,b_avediff,a_ac_diff,a_cmp_diff,b_ac_diff,b_cmp_diff,Time from gtinclinometer ");
			strSql.Append("   where     xmno=@xmno    and   holename=@holename  and  deepth=@deepth  and   time=@time  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
                    new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)			
                                         };
			parameters[0].Value = xmno;
			parameters[1].Value = holename;
            parameters[2].Value = deepth;
			parameters[3].Value = time;

			model=new data.Model.gtinclinometer();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetLastTimeModel(int xmno, string holename, double deepth, DateTime time, out data.Model.gtinclinometer model)
        {

            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select    xmno,holename,deepth,a_nagtive,a_positive,b_nagtive,b_positive,a_planediff,a_avediff,b_planediff,b_avediff,a_ac_diff,a_cmp_diff,b_ac_diff,b_cmp_diff,Time from gtinclinometer ");
            strSql.Append("   where     xmno=@xmno    and   holename=@holename  and   deepth=@deepth   and   time<@time   order by time desc   limit 0,10");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
                    new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)			
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = holename;
            parameters[2].Value = deepth;
            parameters[3].Value = time;

            model = new data.Model.gtinclinometer();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool GetSumAcDiff(int xmno, string holename, double deepth, DateTime time, out data.Model.gtinclinometer model)
        {
            StringBuilder strSql = new StringBuilder(); 
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select    sum(a_avediff) as a_ac_diff,sum(b_avediff) as b_ac_diff  from gtinclinometer ");
            strSql.Append("   where     xmno=@xmno    and   holename=@holename  and  deepth=@deepth  and   time<@time order by time desc   limit 0,10");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@holename", OdbcType.VarChar,100),
                    new OdbcParameter("@deepth", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)			
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = holename;
            parameters[2].Value = deepth;
            parameters[3].Value = time;
            model = new data.Model.gtinclinometer();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
            
        }


        /// <summary>
        /// 得到一个测斜段对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, out List<data.Model.gtinclinometer> lt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,holename,deepth,a_nagtive,a_positive,b_nagtive,b_positive,a_planediff,a_avediff,b_planediff,b_avediff,ac_diff,cmp_diff,Time from gtinclinometer_tmp ");
            strSql.Append(" where    xmno=@xmno   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;

            lt = new List<data.Model.gtinclinometer>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public data.Model.gtinclinometer DataRowToModel(DataRow row)
        {
            data.Model.gtinclinometer model = new data.Model.gtinclinometer();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["holename"] != null)
                {
                    model.holename = row["holename"].ToString();
                }
                if (row["deepth"] != null)
                {
                    model.deepth = Convert.ToDouble(row["deepth"].ToString());
                }
                if (row["a_nagtive"] != null)
                {
                    model.a_nagtive = Convert.ToDouble(row["a_nagtive"].ToString());
                } 
                if (row["a_positive"] != null)
                {
                    model.a_positive = Convert.ToDouble(row["a_positive"].ToString());
                } 
                if (row["b_nagtive"] != null)
                {
                    model.b_nagtive = Convert.ToDouble(row["b_nagtive"].ToString());
                }
                if (row["b_positive"] != null)
                {
                    model.b_positive = Convert.ToDouble(row["b_positive"].ToString());
                }
                if (row["a_planediff"] != null)
                {
                    model.a_planediff = Convert.ToDouble(row["a_planediff"].ToString());
                }
                if (row["a_avediff"] != null)
                {
                    model.a_avediff = Convert.ToDouble(row["a_avediff"].ToString());
                }
                if (row["a_ac_diff"] != null)
                {
                    model.a_ac_diff = Convert.ToDouble(row["a_ac_diff"].ToString());
                }
                if (row["a_cmp_diff"] != null)
                {
                    model.a_cmp_diff = Convert.ToDouble(row["a_cmp_diff"].ToString());
                } 
                if (row["b_ac_diff"] != null)
                {
                    model.b_ac_diff = Convert.ToDouble(row["b_ac_diff"].ToString());
                }
                if (row["b_cmp_diff"] != null)
                {
                    model.b_cmp_diff = Convert.ToDouble(row["b_cmp_diff"].ToString());
                }
                if (row["Time"] != null)
                {
                    model.time = Convert.ToDateTime(row["Time"].ToString());
                }
            }
            return model;
        }

        public bool PointMaxDateTimeGet(int xmno, out DateTime dt)
        {
            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select max(time) from gtinclinometer ");
            strSql.Append("   where     xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
						};
            parameters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(),parameters);
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }






		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

