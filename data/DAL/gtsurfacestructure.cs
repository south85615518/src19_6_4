﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacestructure.cs
*
* 功 能： N/A
* 类 名： gtsurfacestructure
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:28   N/A    初版
*
* Copyright (c) 2012 #SensorName# Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace data.DAL
{
    /// <summary>
    /// 数据访问类:gtsurfacestructure
    /// </summary>
    public partial class gtsurfacestructure
    {
        public gtsurfacestructure()
        { }
        public static database db = new database();
        #region  BasicMethod
        //判断点名对象是否存在
        public bool Exist(data.Model.gtsurfacestructure model)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("select count(1) from  gtsurfacestructure   where     xmno=@xmno    ");
            strSql.Append("   and       surfacename=@surfacename       ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
                    new OdbcParameter("@surfacename", OdbcType.VarChar,120)
                                         };
            
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.surfacename;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);

            return obj == null ? true : int.Parse(obj.ToString()) > 0 ? true : false;

        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtsurfacestructure model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into gtsurfacestructure(");
            strSql.Append("xmno,surfacename,firstalarmname,secondalarmname,thirdalarmname,remark,concreteElasticityModulus,concreteConversionArea,steelElasticityModulus,steelConversionArea,externalDiameter,thick,datatype)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@surfacename,@firstalarmname,@secondalarmname,@thirdalarmname,@remark,@concreteElasticityModulus,@concreteConversionArea,@steelElasticityModulus,@steelConversionArea,@externalDiameter,@thick,@datatype)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
					new OdbcParameter("@surfacename", OdbcType.VarChar,120),
					new OdbcParameter("@firstalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@secondalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@thirdalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@concreteElasticityModulus", OdbcType.Double),
					new OdbcParameter("@concreteConversionArea", OdbcType.Double),
                    new OdbcParameter("@steelElasticityModulus", OdbcType.Double),
					new OdbcParameter("@steelConversionArea", OdbcType.Double),
                    new OdbcParameter("@externalDiameter", OdbcType.Double),
					new OdbcParameter("@thick", OdbcType.Double),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.surfacename;
            parameters[2].Value = model.firstalarmname;
            parameters[3].Value = model.secondalarmname;
            parameters[4].Value = model.thirdalarmname;
            parameters[5].Value = model.remark;
            parameters[6].Value = model.concreteElasticityModulus;
            parameters[7].Value = model.concreteConversionArea;
            parameters[8].Value = model.steelElasticityModulus;
            parameters[9].Value = model.steelConversionArea;
            parameters[10].Value = model.externalDiameter;
            parameters[11].Value = model.thick;
            parameters[12].Value = Convert.ToInt32(model.datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtsurfacestructure model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update gtsurfacestructure set ");
            strSql.Append("firstalarmname=@firstalarmname,");
            strSql.Append("secondalarmname=@secondalarmname,");
            strSql.Append("thirdalarmname=@thirdalarmname,");
            strSql.Append("concreteElasticityModulus=@concreteElasticityModulus,");
            strSql.Append("concreteConversionArea=@concreteConversionArea,");
            strSql.Append("steelElasticityModulus=@steelElasticityModulus,");
            strSql.Append("steelConversionArea=@steelConversionArea,");
            strSql.Append("remark=@remark,");
            strSql.Append("externalDiameter=@externalDiameter,");
            strSql.Append("thick=@thick");
            strSql.Append("   where   ");
            strSql.Append("surfacename=@surfacename  and  datatype = @datatype ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@firstalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@secondalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@thirdalarmname", OdbcType.VarChar,120),
                    new OdbcParameter("@concreteElasticityModulus", OdbcType.Double),
					new OdbcParameter("@concreteConversionArea", OdbcType.Double),
                    new OdbcParameter("@steelElasticityModulus", OdbcType.Double),
					new OdbcParameter("@steelConversionArea", OdbcType.Double),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@externalDiameter", OdbcType.Double),
					new OdbcParameter("@thick", OdbcType.Double),
					new OdbcParameter("@surfacename", OdbcType.VarChar,120),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };

            parameters[0].Value = model.firstalarmname;
            parameters[1].Value = model.secondalarmname;
            parameters[2].Value = model.thirdalarmname;
            parameters[3].Value = model.concreteElasticityModulus;
            parameters[4].Value = model.concreteConversionArea;
            parameters[5].Value = model.steelElasticityModulus;
            parameters[6].Value = model.steelConversionArea;
            parameters[7].Value = model.remark;
            parameters[8].Value = model.externalDiameter;
            parameters[9].Value = model.thick;
            parameters[10].Value = model.surfacename;
            parameters[11].Value = Convert.ToInt32(model.datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, data.Model.gtsurfacestructure model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update gtsurfacestructure set ");
            strSql.Append("firstalarmname=@firstalarmname,");
            strSql.Append("secondalarmname=@secondalarmname,");
            strSql.Append("thirdalarmname=@thirdalarmname,");
            strSql.Append(" remark=@remark ");
            strSql.Append("   where   ");
            strSql.Append(" surfacename  in ('" + pointNameStr + "')   and xmno = @xmno  and  datatype=@datatype ");
            OdbcParameter[] parameters = {

					new OdbcParameter("@firstalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@secondalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@thirdalarmname", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
                                         };



            parameters[0].Value = model.firstalarmname;
            parameters[1].Value = model.secondalarmname;
            parameters[2].Value = model.thirdalarmname;
            parameters[3].Value = model.remark;
            parameters[4].Value = model.xmno;
            parameters[5].Value = Convert.ToInt32(model.datatype);

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(data.Model.gtsurfacestructure model)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("delete from  gtsurfacestructure  where surfacename in (@surfacename) and  xmno = @xmno  and datatype = @datatype  ");
            OdbcParameter[] parameters = {
                new OdbcParameter("@surfacename", OdbcType.VarChar,120),
                new OdbcParameter("@xmno", OdbcType.Int,4),
                new OdbcParameter("@datatype", OdbcType.Int)
			};
            parameters[0].Value = model.surfacename;
            parameters[1].Value = model.xmno;
            parameters[2].Value = Convert.ToInt32(model.datatype);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 点名加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="ls"></param>
        /// <returns></returns>
        public bool gtsurfacestructurePointLoadDAL(int xmno,data.Model.gtsensortype datatype ,out List<string> ls)
        {

            string sql = "select distinct(surfacename) from gtsurfacestructure where xmno='" + xmno + "'  and  datatype=" + Convert.ToInt32(datatype) + "   order by surfacename asc";

            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public data.Model.gtsurfacestructure GetModel()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select ID,xmno,surfacename,firstalarmname,secondalarmname,thirdalarmname,remark,pointtype from gtsurfacestructure ");
        //    strSql.Append(" where ");
        //    OdbcParameter[] parameters = {
        //    };

        //    data.Model.gtsurfacestructure model=new data.Model.gtsurfacestructure();
        //    DataSet ds=DbHelperOdbc.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        return DataRowToModel(ds.Tables[0].Rows[0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public data.Model.gtsurfacestructure DataRowToModel(DataRow row)
        {
            data.Model.gtsurfacestructure model = new data.Model.gtsurfacestructure();
            if (row != null)
            {
                
                if (row["xmno"] != null)
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["surfacename"] != null)
                {
                    model.surfacename = row["surfacename"].ToString();
                }
                if (row["firstalarmname"] != null)
                {
                    model.firstalarmname = row["firstalarmname"].ToString();
                }
                if (row["secondalarmname"] != null)
                {
                    model.secondalarmname = row["secondalarmname"].ToString();
                }
                if (row["thirdalarmname"] != null)
                {
                    model.thirdalarmname = row["thirdalarmname"].ToString();
                }
                if (row["concreteElasticityModulus"] != null)
                {
                    model.concreteElasticityModulus = Convert.ToDouble(row["concreteElasticityModulus"].ToString());
                }
                if (row["concreteConversionArea"] != null)
                {
                    model.concreteConversionArea = Convert.ToDouble(row["concreteConversionArea"].ToString());
                }
                if (row["steelElasticityModulus"] != null)
                {
                    model.steelElasticityModulus = Convert.ToDouble(row["steelElasticityModulus"].ToString());
                }
                if (row["steelConversionArea"] != null )
                {
                    model.steelConversionArea = Convert.ToDouble(row["steelConversionArea"].ToString());
                }
                if (row["externalDiameter"] != null && row["externalDiameter"].ToString() != "")
                {
                    model.externalDiameter = Convert.ToDouble(row["externalDiameter"].ToString());
                }
                if (row["thick"] != null && row["thick"].ToString() != "")
                {
                    model.thick = Convert.ToDouble(row["thick"].ToString());
                }
                if (row["remark"] != null)
                {
                    model.remark = row["remark"].ToString();
                }
                if (row["datatype"] != null && row["datatype"].ToString() != "")
                {
                    model.datatype = (data.Model.gtsensortype)Convert.ToInt32(row["datatype"]);
                }
            }
            return model;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string surfacename,data.Model.gtsensortype datatype ,out data.Model.gtsurfacestructure model)
        {
            SingleTonOdbcSQLHelper singleTonOdbcSQLHelper = new SingleTonOdbcSQLHelper();
            //OdbcConnection conn = db.GetStanderConn(xmno);
            singleTonOdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,surfacename,firstalarmname,secondalarmname,thirdalarmname,concreteElasticityModulus,concreteConversionArea,steelElasticityModulus,steelConversionArea,externalDiameter,thick,remark,datatype from gtsurfacestructure   where       xmno=@xmno   and   surfacename=@surfacename    and     datatype = @datatype  ");

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,10),
                    new OdbcParameter("@surfacename", OdbcType.VarChar,200),
                    new OdbcParameter("@datatype", OdbcType.Int,10)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = surfacename;
            parameters[2].Value = Convert.ToInt32(datatype);
            model = new data.Model.gtsurfacestructure();
            DataSet ds = singleTonOdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool PointAlarmValueTableLoad(string searchstring,data.Model.gtsensortype datatype ,int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            string sql = string.Format("select surfacename,firstalarmname,secondalarmname,thirdalarmname,concreteElasticityModulus,concreteConversionArea,steelElasticityModulus,steelConversionArea,externalDiameter,thick,remark from gtsurfacestructure where xmno='{3}' and  datatype={5}   and {4} {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring, Convert.ToInt32(datatype));
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointTableLoad(string searchstring, data.Model.gtsensortype datatype, int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select surfacename,firstalarmname,secondalarmname,thirdalarmname,concreteElasticityModulus,concreteConversionArea,steelElasticityModulus,steelConversionArea,externalDiameter,thick,remark from gtsurfacestructure where xmno='{3}' and  datatype={5}  and {4}  {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring, Convert.ToInt32(datatype));
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointAlarmValueTableRowsCount(string searchstring, data.Model.gtsensortype datatype, int xmno, out string totalCont)
        {

            string sql = "select count(*) from gtsurfacestructure where xmno = '" + xmno + "' and  datatype="+Convert.ToInt32(datatype);
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }
        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

