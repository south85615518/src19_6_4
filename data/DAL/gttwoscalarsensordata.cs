﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsensordata.cs
*
* 功 能： N/A
* 类 名： gtsensordata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/31 15:15:12   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
//Please add references
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtsensordata
	/// </summary>
	public partial class gtsensordata
	{
        public bool TwoScalarResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype,DateTime startTime,DateTime endTime, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_oregion_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc,time  from   gtsensordata       where      project_name=@xmname       and    datatype=@datatype         and        time>@starttime     and     time < @endtime      and    {0}      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),
                    new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startTime;
            parameters[3].Value = endTime;
            parameters[4].Value = (startPageIndex - 1) * pageSize;
            parameters[5].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool ScalarResultTableRowsCount(string xmname, string pointname, string sord, Model.gtsensortype datatype, int startcyc, int endcyc, out int totalCont)
        {
            totalCont = 0;
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmname);
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "   point_name in (select pointname from   " + surveyconn.Database + ".gtpointalarmvalue  where  xmno = " + xmname + "  and  datatype=" + Convert.ToInt32(datatype) + "  )   " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"select count(1)  from gtsensordata  where   project_name=@xmname     and    datatype=@datatype    and      cyc
          between  @startcyc      and    @endcyc       and    {0}     ", searchstr);

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),


                    new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;


            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            totalCont = obj== null ? 0 : Convert.ToInt32(obj);
            return true;
        }

        /// <summary>
        /// 双量结果数据得到一个对象实体
        /// </summary>
        public bool TwoScalarGetModelList(string project_name, Model.gtsensortype datatype, out List<data.Model.gtsensordata> lt)
        {
            OdbcConnection conn = db.GetStanderConn(project_name);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_oregion_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc,time  from gtsensordata_tmp ");
            strSql.Append(" where    project_name=@project_name    and  datatype=@datatype    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
							};
            parameters[0].Value = project_name;
            parameters[1].Value = datatype;
            lt = new List<Model.gtsensordata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(TwoScalarDataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        /// <summary>
        /// 单量结果数据得到一个对象实体
        /// </summary>
        public data.Model.gtsensordata TwoScalarDataRowToModel(DataRow row)
        {
            data.Model.gtsensordata model = new data.Model.gtsensordata();
            if (row != null)
            {
                if (row["project_name"] != null)
                {
                    model.project_name = row["project_name"].ToString();
                }
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                if (row["senorno"] != null)
                {
                    model.senorno = row["senorno"].ToString();
                }
                if (row["datatype"] != null)
                {
                    model.datatype = row["datatype"].ToString();
                }
                if (row["valuetype"] != null && row["valuetype"].ToString() != "")
                {
                    model.valuetype = int.Parse(row["valuetype"].ToString());
                }
                if (row["first_oregion_scalarvalue"] != null && row["first_oregion_scalarvalue"].ToString() != "")
                {
                    model.first_oregion_scalarvalue = double.Parse(row["first_oregion_scalarvalue"].ToString());
                }
                if (row["first_this_scalarvalue"] != null && row["first_this_scalarvalue"].ToString() != "")
                {
                    model.first_this_scalarvalue = double.Parse(row["first_this_scalarvalue"].ToString());
                }
                if (row["first_ac_scalarvalue"] != null && row["first_ac_scalarvalue"].ToString() != "")
                {
                    model.first_ac_scalarvalue = double.Parse(row["first_ac_scalarvalue"].ToString());
                }
                if (row["sec_oregion_scalarvalue"] != null && row["sec_oregion_scalarvalue"].ToString() != "")
                {
                    model.sec_oregion_scalarvalue = double.Parse(row["sec_oregion_scalarvalue"].ToString());
                }
                if (row["sec_this_scalarvalue"] != null && row["sec_this_scalarvalue"].ToString() != "")
                {
                    model.sec_this_scalarvalue = double.Parse(row["sec_this_scalarvalue"].ToString());
                }
                if (row["sec_ac_scalarvalue"] != null && row["sec_ac_scalarvalue"].ToString() != "")
                {
                    model.sec_ac_scalarvalue = double.Parse(row["sec_ac_scalarvalue"].ToString());
                }
                if (row["cyc"] != null && row["cyc"].ToString() != "")
                {
                    model.cyc = int.Parse(row["cyc"].ToString());
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }


            }
            return model;
        }

        public bool TwoScalarGetModel(string project_name, string pointname, DateTime dt, Model.gtsensortype datatype, out data.Model.gtsensordata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(project_name);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_oregion_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc,time  from gtsensordata ");
            strSql.Append(" where    project_name=@project_name   and  datatype=@datatype  and  point_name=@point_name    and    time = @time   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = project_name;
            parameters[0].Value = datatype;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = TwoScalarDataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

	}
}

