﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsettlementalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtsettlementalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:33   N/A    初版
*
* Copyright (c) 2012 gtsettlementpointalarmvalue Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//using MySql.Data.MySqlClient;
//using gtsettlementpointalarmvalue.DBUtility;//Please add references
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtsettlementalarmvalue
	/// </summary>
	public partial class gtsettlementalarmvalue
	{
        public static database db = new database();
        public gtsettlementalarmvalue()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("xmno", "gtsettlementalarmvalue"); 
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string alarmname,int xmno)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from gtsettlementalarmvalue");
        //    strSql.Append(" where alarmname=@alarmname and xmno=@xmno ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@alarmname", OdbcType.VarChar,100),
        //            new OdbcParameter("@xmno", OdbcType.Int,11)			};
        //    parameters[0].Value = alarmname;
        //    parameters[1].Value = xmno;
           
        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsettlementalarmvalue model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
			strSql.Append("insert into gtsettlementalarmvalue(");
			strSql.Append("alarmname,settlementdiff,this_val,ac_val,rap,xmno,id)");
			strSql.Append(" values (");
            strSql.Append("@alarmname,@settlementdiff,@this_val,@ac_val,@rap,@xmno,@id)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@alarmname", OdbcType.VarChar,100),
					new OdbcParameter("@settlementdiff", OdbcType.Double),
                    new OdbcParameter("@this_val", OdbcType.Double),
                    new OdbcParameter("@ac_val", OdbcType.Double),
                    new OdbcParameter("@rap", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@id", OdbcType.Int,11)

                                         };
			parameters[0].Value = model.alarmname;
			parameters[1].Value = model.settlementdiff;
            parameters[2].Value = model.this_val;
            parameters[3].Value = model.ac_val;
            parameters[4].Value = model.rap;
			parameters[5].Value = model.xmno;
			parameters[6].Value = model.id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(data.Model.gtsettlementalarmvalue model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
			strSql.Append("update gtsettlementalarmvalue set ");
			strSql.Append("settlementdiff=@settlementdiff,");
            strSql.Append("this_val=@this_val,");
            strSql.Append("ac_val=@ac_val,");
            strSql.Append("rap=@rap,");

			strSql.Append("id=@id");
			strSql.Append("     where     alarmname=@alarmname     and      xmno=@xmno   ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@settlementdiff", OdbcType.Double),
                    new OdbcParameter("@this_val", OdbcType.Double),
                    new OdbcParameter("@ac_val", OdbcType.Double),
                    new OdbcParameter("@rap", OdbcType.Double),
					new OdbcParameter("@id", OdbcType.Int,11),
					new OdbcParameter("@alarmname", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.settlementdiff;
            parameters[1].Value = model.this_val;
            parameters[2].Value = model.ac_val;
            parameters[3].Value = model.rap;
			parameters[4].Value = model.id;
			parameters[5].Value = model.alarmname;
			parameters[6].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string alarmname,int xmno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from gtsettlementalarmvalue ");
			strSql.Append("     where     alarmname=@alarmname    and    xmno=@xmno  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@alarmname", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
			parameters[0].Value = alarmname;
			parameters[1].Value = xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
                PointAlarmValueDelCasc(alarmname,xmno);
				return true;
			}
			else
			{
				return false;
			}
		}
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(string alarmname, int xmno)
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update gtsettlementpointalarmvalue set firstAlarmName='' where xmno=@xmno and firstAlarmName=@firstAlarmName");
            //二级预警
            ls.Add("update gtsettlementpointalarmvalue set secAlarmName='' where xmno=@xmno and secAlarmName=@secAlarmName");
            //三级预警
            ls.Add("update gtsettlementpointalarmvalue set thirdAlarmName='' where xmno=@xmno and thirdAlarmName=@thirdAlarmName");
            List<string> cascSql = ls;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@xmno", "'" + xmno + "'");
                temp = temp.Replace("@firstAlarmName", "'" + alarmname + "'");
                temp = temp.Replace("@secAlarmName", "'" + alarmname + "'");
                temp = temp.Replace("@thirdAlarmName", "'" + alarmname + "'");

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(temp, conn);


            }
            return true;
        }




        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select alarmname,settlementdiff,this_val,ac_val,rap,id  from  gtsettlementalarmvalue    where      xmno =  '"+xmno+"'    {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(1) from gtsettlementalarmvalue where xmno = '" + xmno + "'";
            if (searchstring.Trim() != "1=1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public data.Model.gtsettlementalarmvalue GetModel(string alarmname,int xmno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select alarmname,settlementdiff,this_val,ac_val,rapsettlementdiff,xmno,id from gtsettlementalarmvalue ");
			strSql.Append(" where        alarmname=@alarmname       and         xmno=@xmno  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@alarmname", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
			parameters[0].Value = alarmname;
			parameters[1].Value = xmno;

			data.Model.gtsettlementalarmvalue model=new data.Model.gtsettlementalarmvalue();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno, out string alarmValueNameStr)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select distinct(alarmname) from gtsettlementalarmvalue where xmno = "+xmno+"";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            alarmValueNameStr = string.Join(";", lsFormat);
            return true;
        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public data.Model.gtsettlementalarmvalue DataRowToModel(DataRow row)
		{
			data.Model.gtsettlementalarmvalue model=new data.Model.gtsettlementalarmvalue();
			if (row != null)
			{
				if(row["alarmname"]!=null)
				{
					model.alarmname=row["alarmname"].ToString();
				}
					//model.settlementdiff=row["settlementdiff"].ToString();
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
                if (row["settlementdiff"] != null && row["settlementdiff"].ToString() != "")
                {
                    model.settlementdiff = Convert.ToDouble(Convert.ToDouble(row["settlementdiff"]).ToString("0.000"));
                }
                if (row["this_val"] != null && row["this_val"].ToString() != "")
                {
                    model.this_val = Convert.ToDouble(Convert.ToDouble(row["this_val"]).ToString("0.000"));
                }
                if (row["ac_val"] != null && row["ac_val"].ToString() != "")
                {
                    model.ac_val = Convert.ToDouble(Convert.ToDouble(row["ac_val"]).ToString("0.000"));
                }
                if (row["rap"] != null && row["rap"].ToString() != "")
                {
                    model.rap = Convert.ToDouble(Convert.ToDouble(row["rap"]).ToString("0.000"));
                }
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
			}
			return model;
		}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string name, int xmno, out data.Model.gtsettlementalarmvalue model)
        {
            model = new Model.gtsettlementalarmvalue();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select alarmname,settlementdiff,this_val,ac_val,rap,xmno,id from gtsettlementalarmvalue ");
            strSql.Append(" where       alarmname=@name      and      xmno=@xmno    ");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@name", OdbcType.VarChar,100),
			        new OdbcParameter("@xmno", OdbcType.Int)
                                         };
            parameters[0].Value = name;
            parameters[1].Value = xmno;
            //InclimeterDAL.Model.inclinometer_alarmvalue model=new InclimeterDAL.Model.inclinometer_alarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select alarmname,settlementdiff,xmno,id ");
			strSql.Append(" FROM gtsettlementalarmvalue ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM gtsettlementalarmvalue ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.xmno desc");
			}
			strSql.Append(")AS Row, T.*  from gtsettlementalarmvalue T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "gtsettlementalarmvalue";
			parameters[1].Value = "xmno";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

