﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace data.DAL
{
    public class gtsensortype
    {
        public static string GTSensorTypeToString(Model.gtsensortype datatype)
        {
            switch(datatype)
            {
                case Model.gtsensortype._degree: return "温度";
                case Model.gtsensortype._deflection: return "挠度";
                case Model.gtsensortype._displacement: return "位移";
                case Model.gtsensortype._prestress: return "预应力";
                case Model.gtsensortype._steelstress: return "钢筋应力";
                case Model.gtsensortype._strain: return "应变";
                case Model.gtsensortype._StrainStress: return "应变应力";
                case Model.gtsensortype._windspeedanddirection: return "风速风向";
                case Model.gtsensortype._axialforce: return "轴力";
                case Model.gtsensortype._osmoticpressure: return "渗压";
                case Model.gtsensortype._stress: return "应力";
                case Model.gtsensortype._surfaceSurpport: return "应变";
                case Model.gtsensortype._supportAxialForce: return "混凝土支撑内力";
                case Model.gtsensortype._soilPressure: return "土压力";
                case Model.gtsensortype._fractureMeter: return "位移";
                case Model.gtsensortype._staticLevel: return "静力水准";
                case Model.gtsensortype._layeredSettlement: return "分层沉降";
                case Model.gtsensortype._enclosureWallReinforcementStress: return "围护墙钢筋应力";
                case Model.gtsensortype._waterlevel: return "水位";
                case Model.gtsensortype._SteelSupport: return "钢支撑";
                case Model.gtsensortype._inclinometer: return "测斜";
                case Model.gtsensortype._HorizontalDisplacement: return "水平位移";
                case Model.gtsensortype._StructuralStress: return "结构应力";
                case Model.gtsensortype._vibration: return "振动";
                default: return "";
            }
        }


        public static string GTSensorTypeToReportString(Model.gtsensortype datatype)
        {
            switch (datatype)
            {
                case Model.gtsensortype._degree: return "温度";
                case Model.gtsensortype._deflection: return "挠度";
                case Model.gtsensortype._displacement: return "位移";
                case Model.gtsensortype._prestress: return "预应力";
                case Model.gtsensortype._steelstress: return "钢筋应力";
                case Model.gtsensortype._strain: return "应变";
                case Model.gtsensortype._StrainStress: return "应变应力";
                case Model.gtsensortype._windspeedanddirection: return "风速风向";
                case Model.gtsensortype._axialforce: return "轴力";
                case Model.gtsensortype._osmoticpressure: return "渗压";
                case Model.gtsensortype._stress: return "应力";
                case Model.gtsensortype._surfaceSurpport: return "应变";
                case Model.gtsensortype._supportAxialForce: return "支撑内力(混凝土)";
                case Model.gtsensortype._soilPressure: return "土压力";
                case Model.gtsensortype._fractureMeter: return "建筑地表裂缝";
                case Model.gtsensortype._staticLevel: return "静力水准";
                case Model.gtsensortype._layeredSettlement: return "分层沉降";
                case Model.gtsensortype._enclosureWallReinforcementStress: return "墙(桩)身钢筋应力";
                case Model.gtsensortype._waterlevel: return "水位";
                case Model.gtsensortype._SteelSupport: return "支撑内力(钢支撑)";
                case Model.gtsensortype._inclinometer: return "测斜";
                case Model.gtsensortype._HorizontalDisplacement: return "水平(竖向)位移";
                case Model.gtsensortype._vibration: return "振动";
                default: return "";
            }
        }




        public static string GTSensorTypeToAlarmString(Model.gtsensortype datatype)
        {
            switch (datatype)
            {
                case Model.gtsensortype._degree: return "温度";
                case Model.gtsensortype._deflection: return "挠度";
                case Model.gtsensortype._displacement: return "位移";
                case Model.gtsensortype._prestress: return "预应力";
                case Model.gtsensortype._steelstress: return "钢筋应力";
                case Model.gtsensortype._strain: return "应变";
                case Model.gtsensortype._StrainStress: return "应变应力";
                case Model.gtsensortype._windspeedanddirection: return "风速风向";
                case Model.gtsensortype._axialforce: return "轴力";
                case Model.gtsensortype._osmoticpressure: return "渗压";
                case Model.gtsensortype._stress: return "应力";
                case Model.gtsensortype._surfaceSurpport: return "应变";
                case Model.gtsensortype._supportAxialForce: return "混凝土支撑内力";
                case Model.gtsensortype._soilPressure: return "土压力";
                case Model.gtsensortype._fractureMeter: return "位移";
                case Model.gtsensortype._staticLevel: return "静力水准";
                case Model.gtsensortype._layeredSettlement: return "分层沉降";
                case Model.gtsensortype._enclosureWallReinforcementStress: return "围护墙钢筋应力";
                case Model.gtsensortype._waterlevel: return "水位";
                case Model.gtsensortype._SteelSupport: return "钢支撑";
                case Model.gtsensortype._inclinometer: return "测斜";
                case Model.gtsensortype._HorizontalDisplacement: return "表面位移";
                case Model.gtsensortype._vibration: return "振动";
                default: return "";
            }
        }

        public static data.Model.gtsensortype GTReportStringToSensorType(string datatype)
        {
            switch (datatype)
            {
                case "温度": return Model.gtsensortype._degree;
                case "挠度": return Model.gtsensortype._deflection;
                case "位移1": return Model.gtsensortype._displacement;
                case "预应力": return Model.gtsensortype._prestress;
                case "钢筋应力": return Model.gtsensortype._steelstress;
                case "应变1": return Model.gtsensortype._strain;
                case "应变应力": return Model.gtsensortype._StrainStress;
                case "风速风向": return Model.gtsensortype._windspeedanddirection;
                case "渗压1": return Model.gtsensortype._osmoticpressure;
                case "应力1": return Model.gtsensortype._stress;
                case "轴力1": return Model.gtsensortype._axialforce;

                case "应变": return Model.gtsensortype._surfaceSurpport;
                case "支撑内力(混凝土)": return Model.gtsensortype._supportAxialForce;
                case "土压力": return Model.gtsensortype._soilPressure;
                case "位移": return Model.gtsensortype._fractureMeter;
                case "静力水准仪":
                case "静力水准": return Model.gtsensortype._staticLevel;
                case "分层沉降": return Model.gtsensortype._layeredSettlement;
                case "应力":
                case "墙(桩)身钢筋应力": return Model.gtsensortype._enclosureWallReinforcementStress;
                case "渗压":
                case "水位计":
                case "水位": return Model.gtsensortype._waterlevel;
                case "轴力":
                case "钢支撑": return Model.gtsensortype._SteelSupport;
                case "测斜": return Model.gtsensortype._inclinometer;
                case "水平(竖向)位移": return Model.gtsensortype._HorizontalDisplacement;
                case "振动": return Model.gtsensortype._vibration;
                default: return Model.gtsensortype._other;
            }
        }

        public static data.Model.gtsensortype GTStringToSensorType(string datatype)
        {
            switch (datatype)
            {
                case "温度":  return Model.gtsensortype._degree;
                case "挠度":  return Model.gtsensortype._deflection;
                case "位移1": return Model.gtsensortype._displacement; 
                case "预应力":  return Model.gtsensortype._prestress;
                case "钢筋应力":  return Model.gtsensortype._steelstress;
                case "应变1":  return Model.gtsensortype._strain;
                case "应变应力":  return Model.gtsensortype._StrainStress;
                case "风速风向": return Model.gtsensortype._windspeedanddirection;
                case "渗压1": return Model.gtsensortype._osmoticpressure;
                case "应力1": return Model.gtsensortype._stress;
                case "轴力1": return Model.gtsensortype._axialforce;

                case "应变": return Model.gtsensortype._surfaceSurpport;
                case "混凝土支撑内力": return Model.gtsensortype._supportAxialForce;
                case "土压力": return Model.gtsensortype._soilPressure;
                case "位移": return Model.gtsensortype._fractureMeter;
                case "静力水准仪":
                case "静力水准": return Model.gtsensortype._staticLevel;
                case "分层沉降": return Model.gtsensortype._layeredSettlement;
                case "应力":
                case "围护墙钢筋应力": return Model.gtsensortype._enclosureWallReinforcementStress;
                case "渗压":
                case "水位计":
                case "水位": return Model.gtsensortype._waterlevel;
                case "轴力":
                case "钢支撑": return Model.gtsensortype._SteelSupport;
                case "测斜": return Model.gtsensortype._inclinometer;
                case "水平位移": return Model.gtsensortype._HorizontalDisplacement;
                case "结构应力": return Model.gtsensortype._StructuralStress;
                default: return Model.gtsensortype._other;
            }
        }
        public static string ImportStringToGTString(string datatype)
        {
            switch (datatype)
            {
                case "温度_": return "";//Model.gtsensortype._degree;
                case "挠度_": return "";//Model.gtsensortype._deflection;
                case "位移1_": return "";//Model.gtsensortype._displacement;
                case "预应力_": return "";//Model.gtsensortype._prestress;
                case "钢筋应力计": return "围护墙钢筋应力";//Model.gtsensortype._steelstress;
                case "应变1_": return "";//Model.gtsensortype._strain;
                case "应变应力_": return "";//Model.gtsensortype._StrainStress;
                case "风速风向_": return "";//Model.gtsensortype._windspeedanddirection;
                case "渗压计": return "渗压";//Model.gtsensortype._osmoticpressure;
                case "应力1——": return "";//Model.gtsensortype._stress;
                case "轴力1——": return "";//Model.gtsensortype._axialforce;

                case "应变_":
                case "混凝土支撑内力": return "混凝土支撑内力";//Model.gtsensortype._supportAxialForce;
                case "土压力_": return "";//Model.gtsensortype._soilPressure;
                case "建筑地表裂缝": return "位移";//Model.gtsensortype._fractureMeter;
                case "静力水准_": return "";//Model.gtsensortype._staticLevel;
                case "分层沉降_": return "";//Model.gtsensortype._layeredSettlement;
                case "应力_":
                case "围护墙钢筋应力": return "围护墙钢筋应力";//Model.gtsensortype._enclosureWallReinforcementStress;
                case "渗压_":
                case "水位_": return "";//Model.gtsensortype._waterlevel;
                case "轴力_":
                case "钢支撑_": return "";//Model.gtsensortype._SteelSupport;
                default: return "";//Model.gtsensortype._other;
            }
        }

    }
}
