﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsettlementpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtsettlementpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/30 16:57:58   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtsettlementpointalarmvalue
	/// </summary>
	public partial class gtsettlementpointalarmvalue
	{
		public gtsettlementpointalarmvalue()
		{}
		#region  BasicMethod

        public database db = new database();

        public bool Exist(string point_name, int xmno)
        {
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno); ;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from  gtsettlementpointalarmvalue where    point_name=@point_name    and     xmno = @xmno");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)
					
					
                                         };

            parameters[0].Value = point_name;
            parameters[1].Value = xmno;

            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return Convert.ToInt32(obj) == 0 ? false : true;

        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsettlementpointalarmvalue model)
		{
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
			strSql.Append("insert into gtsettlementpointalarmvalue(");
			strSql.Append("xmno,point_name,addressno,deviceno,dtuno,firstalarmname,secalarmname,thirdalarmname,pointtype,basepointname,remark,initsettlementval)");
			strSql.Append(" values (");
            strSql.Append("@xmno,@point_name,@addressno,@deviceno,@dtuno,@firstalarmname,@secalarmname,@thirdalarmname,@pointtype,@basepointname,@remark,@initsettlementval)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@addressno", OdbcType.VarChar,100),
					new OdbcParameter("@deviceno", OdbcType.VarChar,100),
                    new OdbcParameter("@dtuno", OdbcType.VarChar,100),
                    new OdbcParameter("@firstalarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@secalarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@thirdalarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@pointtype", OdbcType.VarChar,120),
					new OdbcParameter("@basepointname", OdbcType.VarChar,120),
                    new OdbcParameter("@remark", OdbcType.VarChar,100),
                    new OdbcParameter("@initsettlementval", OdbcType.VarChar,100)
                                         };
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.addressno;
			parameters[3].Value = model.deviceno;
            parameters[4].Value = model.dtuno;
            parameters[5].Value = model.firstalarmname;
            parameters[6].Value = model.secalarmname;
            parameters[7].Value = model.thirdalarmname;
            parameters[8].Value = model.pointtype;
            parameters[9].Value = model.basepointname;
            parameters[10].Value = model.remark;
            parameters[11].Value = model.initsettlementval;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtsettlementpointalarmvalue model)
        {
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update gtsettlementpointalarmvalue set ");
            strSql.Append("deviceno=@deviceno,");
            strSql.Append("addressno=@addressno,");
            strSql.Append("firstalarmname=@firstalarmname,");
            strSql.Append("secalarmname=@secalarmname,");
            strSql.Append("thirdalarmname=@thirdalarmname,");
            strSql.Append("pointtype=@pointtype,");
            strSql.Append("basepointname=@basepointname,");
            strSql.Append("remark=@remark,");
            strSql.Append("initsettlementval=@initsettlementval");
            strSql.Append("    where    xmno=@xmno     and     point_name=@point_name    and      dtuno=@dtuno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@deviceno", OdbcType.VarChar,100),
					new OdbcParameter("@addressno", OdbcType.VarChar,100),
					new OdbcParameter("@firstalarmname", OdbcType.VarChar,100),
					new OdbcParameter("@secalarmname", OdbcType.VarChar,100),
					new OdbcParameter("@thirdalarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@pointtype", OdbcType.VarChar,120),
					new OdbcParameter("@basepointname", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@initsettlementval", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@dtuno", OdbcType.VarChar,255)};
            parameters[0].Value = model.deviceno;
            parameters[1].Value = model.addressno;
            parameters[2].Value = model.firstalarmname;
            parameters[3].Value = model.secalarmname;
            parameters[4].Value = model.thirdalarmname;
            parameters[5].Value = model.pointtype;
            parameters[6].Value = model.basepointname;
            parameters[7].Value = model.remark;
            parameters[8].Value = model.initsettlementval;
            parameters[9].Value = model.xmno;
            parameters[10].Value = model.point_name;
            parameters[11].Value = model.dtuno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string point_name)
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
			strSql.Append("delete   from   gtsettlementpointalarmvalue ");
			strSql.Append(" where    xmno=@xmno   and    point_name=@point_name   ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100)
							};
			parameters[0].Value = xmno;
			parameters[1].Value = point_name;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public bool GetModel(int xmno, int addressno, int deviceno, out data.Model.gtsettlementpointalarmvalue model)
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select point_name,addressno,deviceno,dtuno,xmno,firstalarmname,secalarmname,thirdalarmname,remark,initsettlementval from gtsettlementpointalarmvalue ");
			strSql.Append(" where    xmno=@xmno    and    addressno=@addressno    and    deviceno=@deviceno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@addressno", OdbcType.VarChar,100),
					new OdbcParameter("@deviceno", OdbcType.VarChar,100)			};
			parameters[0].Value = xmno;
			parameters[1].Value = addressno;
			parameters[2].Value = deviceno;

			model=new data.Model.gtsettlementpointalarmvalue();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno,string pointname, out data.Model.gtsettlementpointalarmvalue model)
        {

            StringBuilder strSql = new StringBuilder(); 
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select point_name,addressno,deviceno,dtuno,xmno,firstalarmname,secalarmname,thirdalarmname,pointtype,basepointname,remark,initsettlementval from gtsettlementpointalarmvalue ");
            strSql.Append(" where    xmno=@xmno    and    point_name=@point_name     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            

            model = new data.Model.gtsettlementpointalarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetBasePointBridgeModellist(int xmno, string basepointname, out List<data.Model.gtsettlementpointalarmvalue> modellist)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append("select point_name,addressno,deviceno,dtuno,xmno,firstalarmname,secalarmname,thirdalarmname,pointtype,basepointname,remark,initsettlementval from gtsettlementpointalarmvalue ");
            strSql.Append(" where    xmno=@xmno    and    basepointname=@basepointname     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@basepointname", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = basepointname;


            modellist = new List<data.Model.gtsettlementpointalarmvalue>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count == 0) return false;
            int i = 0;
            while(i < ds.Tables[0].Rows.Count)
            {
                modellist.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public data.Model.gtsettlementpointalarmvalue DataRowToModel(DataRow row)
		{
			data.Model.gtsettlementpointalarmvalue model=new data.Model.gtsettlementpointalarmvalue();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
                if (row["point_name"] != null && row["point_name"] != "")
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["addressno"]!=null && row["addressno"].ToString()!="")
				{
					model.addressno=int.Parse(row["addressno"].ToString());
				}
				if(row["deviceno"]!=null && row["deviceno"].ToString()!="")
				{
					model.deviceno=int.Parse(row["deviceno"].ToString());
				}
                if (row["dtuno"] != null && row["dtuno"].ToString() != "")
                {
                    model.dtuno = row["dtuno"].ToString();
                }
                if (row["firstalarmname"] != null && row["firstalarmname"] != "")
                {
                    model.firstalarmname = row["firstalarmname"].ToString();
                }
                if (row["secalarmname"] != null && row["secalarmname"] != "")
                {
                    model.secalarmname = row["secalarmname"].ToString();
                }
                if (row["thirdalarmname"] != null && row["thirdalarmname"] != "")
                {
                    model.thirdalarmname = row["thirdalarmname"].ToString();
                }
                if (row["pointtype"] != null && row["pointtype"] != "")
                {
                    model.pointtype = row["pointtype"].ToString();
                }
                if (row["basepointname"] != null && row["basepointname"] != "")
                {
                    model.basepointname = row["basepointname"].ToString();
                }
                if (row["remark"] != null && row["remark"].ToString() != "")
                {
                    model.remark = row["remark"].ToString();
                }
                if (row["initsettlementval"] != null && row["initsettlementval"].ToString() != "")
                {
                    model.initsettlementval = Convert.ToDouble(row["initsettlementval"].ToString());
                }
			}
			return model;
		}
        /// <summary>
        /// 加载测斜链表
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);;
            string sql = string.Format("select point_name,pointtype,basepointname,addressno,deviceno,dtuno,firstalarmname,secalarmname,thirdalarmname,initsettlementval,remark  from gtsettlementpointalarmvalue where xmno='{3}'   {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        /// <summary>
        /// 加载测斜链数量
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableCountLoad( int xmno,  out string cont)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno); 
            string sql = string.Format("select count(1) from gtsettlementpointalarmvalue where xmno='{0}'",xmno);
            
            cont = querysql.querystanderstr(sql, conn);
            
            return true;
        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateAlarm(data.Model.gtsettlementpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update gtsettlementpointalarmvalue set ");
            strSql.Append("FirstAlarmName=@FirstAlarmName,");
            strSql.Append("SecondAlarmName=@SecondAlarmName,");
            strSql.Append("ThirdAlarmName=@ThirdAlarmName");
            strSql.Append("   where   ");
            strSql.Append(" point_name=@point_name");
            OdbcParameter[] parameters = {
					

					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
					
                    
                                         };


            parameters[0].Value = model.firstalarmname;
            parameters[1].Value = model.secalarmname;
            parameters[2].Value = model.thirdalarmname;
            parameters[3].Value = model.point_name;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, data.Model.gtsettlementpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update gtsettlementpointalarmvalue set ");
            strSql.Append("FirstAlarmName=@FirstAlarmName,");
            strSql.Append("SecAlarmName=@SecAlarmName,");
            strSql.Append("ThirdAlarmName=@ThirdAlarmName,");
            strSql.Append("remark=@remark,");
            strSql.Append("pointtype=@pointtype,");
            strSql.Append("basepointname=@basepointname");
            strSql.Append("   where   ");
            strSql.Append("point_name in ('" + pointNameStr + "') ");
            OdbcParameter[] parameters = {
					

					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,120),
                    new OdbcParameter("@pointtype", OdbcType.VarChar,120),
                    new OdbcParameter("@basepointname", OdbcType.VarChar,120)
                                         };


            parameters[0].Value = model.firstalarmname;
            parameters[1].Value = model.secalarmname;
            parameters[2].Value = model.thirdalarmname;
            parameters[3].Value = model.remark;
            parameters[4].Value = model.pointtype;
            parameters[5].Value = model.basepointname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SettlementPointLoadDAL(int xmno, out List<string> chainnamelist)
        {
            chainnamelist = new List<string>();
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select distinct(point_name) from gtsettlementpointalarmvalue where  xmno='" + xmno + "' ";
            chainnamelist = querysql.querystanderlist(sql, conn);

            //List<string> lsFormat = new List<string>();
            //foreach (string name in lsFormat)
            //{
            //    chainnamelist.Add(name + ":" + name);
            //}
            //chainnamelsit = lsFormat;//string.Join(";", lsFormat);
            return true;
        }


        /// <summary>
        /// 从测斜仪的端口和设备号设置获取点名
        /// </summary>
        /// <returns></returns>
        public bool SettlementTOPointModule(int xmno, string  deviceno, string addressno, out data.Model.gtsettlementpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder(255);
            model = new data.Model.gtsettlementpointalarmvalue();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            strSql.Append(" select  *   from     gtsettlementpointalarmvalue    where   xmno  =  @xmno     and   deviceno =     @deviceno  and     addressno =   @addressno  ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@deviceno", OdbcType.VarChar,100),
                    new OdbcParameter("@addressno", OdbcType.VarChar,100)
                                         };



            parameters[0].Value = xmno;
            parameters[1].Value = deviceno;
            parameters[2].Value = addressno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count == 0)
                return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;

        }

        /// <summary>
        /// 清空关联的预警参数
        /// </summary>
        public bool DeleteAlarmValue(int xmno, string alarmname)
        {
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update gtsettlementpointalarmvalue  set firstalarmname = ''   ");
            strSql.Append(" where  xmno =@xmno and firstalarmname=@firstalarmname ");
            OdbcParameter[] parametersfirst = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@firstalarmname", OdbcType.VarChar,100)
			};
            parametersfirst[0].Value = xmno;
            parametersfirst[1].Value = alarmname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parametersfirst);
            strSql = new StringBuilder();
            strSql.Append("update gtsettlementpointalarmvalue  set secalarmname = ''   ");
            strSql.Append(" where  xmno =@xmno and secalarmname=@secalarmname ");
            OdbcParameter[] parameterssecond = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@secalarmname", OdbcType.VarChar,100)
			};
            parameterssecond[0].Value = xmno;
            parameterssecond[1].Value = alarmname;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameterssecond);
            strSql = new StringBuilder();
            strSql.Append("update gtsettlementpointalarmvalue  set thirdalarmname = ''   ");
            strSql.Append(" where  xmno =@xmno and thirdalarmname=@thirdalarmname ");
            OdbcParameter[] parametersthird = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@thirdalarmname", OdbcType.VarChar,100)
			};
            parametersthird[0].Value = xmno;
            parametersthird[1].Value = alarmname;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parametersthird);

            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        

        //*************************************************

        //public bool FixedInclinometerStateTabLoad(int startPageIndex, int pageSize, int xmno, string xmname, string unitname, string colName, string sord, out DataTable dt)
        //{
        //    string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
        //    OdbcConnection conn = db.GetSurveyStanderConn(xmno);
        //    string sql = string.Format("select fixed_inclinometer_device.chain,fixed_inclinometer_device.point_name,CONCAT(fixed_inclinometer_device.deep,'m') as deep,if((select IFNULL(max(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from fixed_inclinometer_orgldata where fixed_inclinometer_orgldata.point_name = fixed_inclinometer_device.point_name  and  fixed_inclinometer_orgldata.xmno = fixed_inclinometer_device.xmno ) < DATE_ADD(SYSDATE(),INTERVAL -1 day),'false','true' ) as state,if(dtulp.LP=0||ISNULL(dtulp.LP),'false','true') as lp,CONCAT( IFNULL(dtutimetask.hour,0),'时',IFNULL(dtutimetask.minute,0),'分',IFNULL(dtutimetask.times,0),'次' ) as timeinterval ,(select max(time) from fixed_inclinometer_orgldata where fixed_inclinometer_orgldata.point_name = fixed_inclinometer_device.point_name  and fixed_inclinometer_orgldata.xmno = fixed_inclinometer_device.xmno  ) as lastdatatime,fixed_inclinometer_device.deviceno,fixed_inclinometer_device.address,fixed_inclinometer_device.`port`,'" + xmname + "' as xmname,'" + unitname + "' as unitname   from  fixed_inclinometer_device   left join dtutimetask on fixed_inclinometer_device.deviceno = dtutimetask.module left join dtulp on dtutimetask.module = dtulp.module    where fixed_inclinometer_device.xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
        //    Tool.ExceptionLog.ExceptionWrite(sql);
        //    dt = querysql.querystanderdb(sql, conn);
        //    if (dt != null) return true;
        //    return false;
        //}
        public bool SettlementBasePointLoad(int xmno, out List<string> basepointnamelist)
        {

            basepointnamelist = new List<string>();
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select distinct(point_name) from gtsettlementpointalarmvalue where  xmno='" + xmno + "' and  basepointname='' ";
            basepointnamelist = querysql.querystanderlist(sql, conn);
            return true;
        }




		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

