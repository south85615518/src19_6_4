﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsensordata.cs
*
* 功 能： N/A
* 类 名： gtsensordata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/31 15:15:12   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
//Please add references
namespace data.DAL
{
	/// <summary>
	/// 数据访问类:gtsensordata
	/// </summary>
	public partial class gtsensordata
	{
        public static database db = new database();
		public gtsensordata()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string project_name,string senorno,string datatype,DateTime time)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
			strSql.Append("  select count(1) from gtsensordata  ");
            strSql.Append("   where   project_name=@project_name   and    senorno=@senorno   and    datatype=@datatype   and    time=@time  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100),
					new OdbcParameter("@senorno", OdbcType.VarChar,100),
					new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = project_name;
            parameters[1].Value = senorno;
			parameters[2].Value = datatype;
			parameters[3].Value = time;

            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(),parameters);
            if (obj == null) return false;
            return Convert.ToInt32(obj) > 0 ? true : false;
		}
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsensordata model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.project_name);
			strSql.Append("replace into gtsensordata(");
			strSql.Append("project_name,point_name,senorno,datatype,valuetype,oregion_N,oregion_E,oregion_Z,single_oregion_scalarvalue,first_oregion_scalarvalue,sec_oregion_scalarvalue,this_dN,this_dE,this_dZ,ac_dN,ac_dE,ac_dZ,single_this_scalarvalue,single_ac_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc,time)");
			strSql.Append(" values (");
			strSql.Append("@project_name,@point_name,@senorno,@datatype,@valuetype,@oregion_N,@oregion_E,@oregion_Z,@single_oregion_scalarvalue,@first_oregion_scalarvalue,@sec_oregion_scalarvalue,@this_dN,@this_dE,@this_dZ,@ac_dN,@ac_dE,@ac_dZ,@single_this_scalarvalue,@single_ac_scalarvalue,@first_this_scalarvalue,@first_ac_scalarvalue,@sec_this_scalarvalue,@sec_ac_scalarvalue,@cyc,@time)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@senorno", OdbcType.VarChar,100),
					new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@valuetype", OdbcType.Int,11),
					new OdbcParameter("@oregion_N", OdbcType.Double),
					new OdbcParameter("@oregion_E", OdbcType.Double),
					new OdbcParameter("@oregion_Z", OdbcType.Double),
					new OdbcParameter("@single_oregion_scalarvalue", OdbcType.Double),
					new OdbcParameter("@first_oregion_scalarvalue", OdbcType.Double),
					new OdbcParameter("@sec_oregion_scalarvalue", OdbcType.Double),
					new OdbcParameter("@this_dN", OdbcType.Double),
					new OdbcParameter("@this_dE", OdbcType.Double),
					new OdbcParameter("@this_dZ", OdbcType.Double),
					new OdbcParameter("@ac_dN", OdbcType.Double),
					new OdbcParameter("@ac_dE", OdbcType.Double),
					new OdbcParameter("@ac_dZ", OdbcType.Double),
					new OdbcParameter("@single_this_scalarvalue", OdbcType.Double),
					new OdbcParameter("@single_ac_scalarvalue", OdbcType.Double),
					new OdbcParameter("@first_this_scalarvalue", OdbcType.Double),
					new OdbcParameter("@first_ac_scalarvalue", OdbcType.Double),
					new OdbcParameter("@sec_this_scalarvalue", OdbcType.Double),
					new OdbcParameter("@sec_ac_scalarvalue", OdbcType.Double),
					new OdbcParameter("@cyc", OdbcType.Int,11),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.project_name;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.senorno;
			parameters[3].Value = model.datatype;
			parameters[4].Value = model.valuetype;
			parameters[5].Value = model.oregion_N;
			parameters[6].Value = model.oregion_E;
			parameters[7].Value = model.oregion_Z;
			parameters[8].Value = model.single_oregion_scalarvalue;
			parameters[9].Value = model.first_oregion_scalarvalue;
			parameters[10].Value = model.sec_oregion_scalarvalue;
			parameters[11].Value = model.this_dN;
			parameters[12].Value = model.this_dE;
			parameters[13].Value = model.this_dZ;
			parameters[14].Value = model.ac_dN;
			parameters[15].Value = model.ac_dE;
			parameters[16].Value = model.ac_dZ;
			parameters[17].Value = model.single_this_scalarvalue;
			parameters[18].Value = model.single_ac_scalarvalue;
			parameters[19].Value = model.first_this_scalarvalue;
			parameters[20].Value = model.first_ac_scalarvalue;
			parameters[21].Value = model.sec_this_scalarvalue;
			parameters[22].Value = model.sec_ac_scalarvalue;
			parameters[23].Value = model.cyc;
			parameters[24].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(data.Model.gtsensordata model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update gtsensordata set ");
			strSql.Append("senorno=@senorno,");
			strSql.Append("valuetype=@valuetype,");
			strSql.Append("oregion_N=@oregion_N,");
			strSql.Append("oregion_E=@oregion_E,");
			strSql.Append("oregion_Z=@oregion_Z,");
			strSql.Append("single_oregion_scalarvalue=@single_oregion_scalarvalue,");
			strSql.Append("first_oregion_scalarvalue=@first_oregion_scalarvalue,");
			strSql.Append("sec_oregion_scalarvalue=@sec_oregion_scalarvalue,");
			strSql.Append("this_dN=@this_dN,");
			strSql.Append("this_dE=@this_dE,");
			strSql.Append("this_dZ=@this_dZ,");
			strSql.Append("ac_dN=@ac_dN,");
			strSql.Append("ac_dE=@ac_dE,");
			strSql.Append("ac_dZ=@ac_dZ,");
			strSql.Append("single_this_scalarvalue=@single_this_scalarvalue,");
			strSql.Append("single_ac_scalarvalue=@single_ac_scalarvalue,");
			strSql.Append("first_this_scalarvalue=@first_this_scalarvalue,");
			strSql.Append("first_ac_scalarvalue=@first_ac_scalarvalue,");
			strSql.Append("sec_this_scalarvalue=@sec_this_scalarvalue,");
			strSql.Append("sec_ac_scalarvalue=@sec_ac_scalarvalue,");
			strSql.Append("cyc=@cyc");
			strSql.Append(" where project_name=@project_name and point_name=@point_name and datatype=@datatype and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@senorno", OdbcType.VarChar,100),
					new OdbcParameter("@valuetype", OdbcType.Int,11),
					new OdbcParameter("@oregion_N", OdbcType.Double),
					new OdbcParameter("@oregion_E", OdbcType.Double),
					new OdbcParameter("@oregion_Z", OdbcType.Double),
					new OdbcParameter("@single_oregion_scalarvalue", OdbcType.Double),
					new OdbcParameter("@first_oregion_scalarvalue", OdbcType.Double),
					new OdbcParameter("@sec_oregion_scalarvalue", OdbcType.Double),
					new OdbcParameter("@this_dN", OdbcType.Double),
					new OdbcParameter("@this_dE", OdbcType.Double),
					new OdbcParameter("@this_dZ", OdbcType.Double),
					new OdbcParameter("@ac_dN", OdbcType.Double),
					new OdbcParameter("@ac_dE", OdbcType.Double),
					new OdbcParameter("@ac_dZ", OdbcType.Double),
					new OdbcParameter("@single_this_scalarvalue", OdbcType.Double),
					new OdbcParameter("@single_ac_scalarvalue", OdbcType.Double),
					new OdbcParameter("@first_this_scalarvalue", OdbcType.Double),
					new OdbcParameter("@first_ac_scalarvalue", OdbcType.Double),
					new OdbcParameter("@sec_this_scalarvalue", OdbcType.Double),
					new OdbcParameter("@sec_ac_scalarvalue", OdbcType.Double),
					new OdbcParameter("@cyc", OdbcType.Int,11),
					new OdbcParameter("@project_name", OdbcType.VarChar,100),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.senorno;
			parameters[1].Value = model.valuetype;
			parameters[2].Value = model.oregion_N;
			parameters[3].Value = model.oregion_E;
			parameters[4].Value = model.oregion_Z;
			parameters[5].Value = model.single_oregion_scalarvalue;
			parameters[6].Value = model.first_oregion_scalarvalue;
			parameters[7].Value = model.sec_oregion_scalarvalue;
			parameters[8].Value = model.this_dN;
			parameters[9].Value = model.this_dE;
			parameters[10].Value = model.this_dZ;
			parameters[11].Value = model.ac_dN;
			parameters[12].Value = model.ac_dE;
			parameters[13].Value = model.ac_dZ;
			parameters[14].Value = model.single_this_scalarvalue;
			parameters[15].Value = model.single_ac_scalarvalue;
			parameters[16].Value = model.first_this_scalarvalue;
			parameters[17].Value = model.first_ac_scalarvalue;
			parameters[18].Value = model.sec_this_scalarvalue;
			parameters[19].Value = model.sec_ac_scalarvalue;
			parameters[20].Value = model.cyc;
			parameters[21].Value = model.project_name;
			parameters[22].Value = model.point_name;
			parameters[23].Value = model.datatype;
			parameters[24].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string project_name,string point_name,data.Model.gtsensortype datatype,DateTime starttime,DateTime endtime)
		{
            string searchstr = point_name == "" || point_name == null || (point_name.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", point_name);
            OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from gtsensordata   ");
            strSql.AppendFormat("   where    project_name=@project_name    and {0} and    datatype=@datatype    and time  between    @starttime   and   @endtime ", searchstr);
			OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100),
					new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@starttime", OdbcType.DateTime),
                    new OdbcParameter("@endtime", OdbcType.DateTime)
                                         };
			parameters[0].Value = project_name;
			parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = starttime;
            parameters[3].Value = endtime;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public data.Model.gtsensordata GetModel(string project_name,string point_name,string datatype,DateTime time)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select project_name,point_name,senorno,datatype,valuetype,oregion_N,oregion_E,oregion_Z,single_oregion_scalarvalue,first_oregion_scalarvalue,sec_oregion_scalarvalue,this_dN,this_dE,this_dZ,ac_dN,ac_dE,ac_dZ,single_this_scalarvalue,single_ac_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc,time from gtsensordata ");
			strSql.Append(" where project_name=@project_name and point_name=@point_name and datatype=@datatype and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = project_name;
			parameters[1].Value = point_name;
			parameters[2].Value = datatype;
			parameters[3].Value = time;

			data.Model.gtsensordata model=new data.Model.gtsensordata();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
                return null;//DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        //public data.Model.gtsensordata DataRowToModel(DataRow row)
        //{
        //    data.Model.gtsensordata model=new data.Model.gtsensordata();
        //    if (row != null)
        //    {
        //        if(row["project_name"]!=null)
        //        {
        //            model.project_name=row["project_name"].ToString();
        //        }
        //        if(row["point_name"]!=null)
        //        {
        //            model.point_name=row["point_name"].ToString();
        //        }
        //        if(row["senorno"]!=null)
        //        {
        //            model.senorno=row["senorno"].ToString();
        //        }
        //        if(row["datatype"]!=null)
        //        {
        //            model.datatype=row["datatype"].ToString();
        //        }
        //        if(row["valuetype"]!=null && row["valuetype"].ToString()!="")
        //        {
        //            model.valuetype=int.Parse(row["valuetype"].ToString());
        //        }
        //            //model.oregion_N=row["oregion_N"].ToString();
        //            //model.oregion_E=row["oregion_E"].ToString();
        //            //model.oregion_Z=row["oregion_Z"].ToString();
        //            //model.single_oregion_scalarvalue=row["single_oregion_scalarvalue"].ToString();
        //            //model.first_oregion_scalarvalue=row["first_oregion_scalarvalue"].ToString();
        //            //model.sec_oregion_scalarvalue=row["sec_oregion_scalarvalue"].ToString();
        //            //model.this_dN=row["this_dN"].ToString();
        //            //model.this_dE=row["this_dE"].ToString();
        //            //model.this_dZ=row["this_dZ"].ToString();
        //            //model.ac_dN=row["ac_dN"].ToString();
        //            //model.ac_dE=row["ac_dE"].ToString();
        //            //model.ac_dZ=row["ac_dZ"].ToString();
        //            //model.single_this_scalarvalue=row["single_this_scalarvalue"].ToString();
        //            //model.single_ac_scalarvalue=row["single_ac_scalarvalue"].ToString();
        //            //model.first_this_scalarvalue=row["first_this_scalarvalue"].ToString();
        //            //model.first_ac_scalarvalue=row["first_ac_scalarvalue"].ToString();
        //            //model.sec_this_scalarvalue=row["sec_this_scalarvalue"].ToString();
        //            //model.sec_ac_scalarvalue=row["sec_ac_scalarvalue"].ToString();
        //        if(row["cyc"]!=null && row["cyc"].ToString()!="")
        //        {
        //            model.cyc=int.Parse(row["cyc"].ToString());
        //        }
        //        if(row["time"]!=null && row["time"].ToString()!="")
        //        {
        //            model.time=DateTime.Parse(row["time"].ToString());
        //        }
        //    }
        //    return model;
        //}
        /// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string project_name,DateTime Time,data.Model.gtsensortype datatype,string POINT_NAME)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from gtsensordata ");
			strSql.Append(" where project_name=@project_name  and  datatype = @datatype   and time=@time and point_name=@point_name ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@point_name", OdbcType.VarChar,120)			};
            parameters[0].Value = project_name;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
			parameters[2].Value = Time;
			parameters[3].Value = POINT_NAME;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string xmname, data.Model.gtsensortype datatype, int startcyc, int endcyc)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from  gtsensordata  where   project_name=@project_name  and  datatype=@datatype    and  cyc   between    @startcyc      and    @endcyc  ");
            OdbcParameter[] parameters = { 
                                             new OdbcParameter("@project_name", OdbcType.VarChar,100),
                                             new OdbcParameter("@datatype", OdbcType.VarChar,100),
                                             new OdbcParameter("@startcyc", OdbcType.Int),
                                             new OdbcParameter("@endcyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        

        public bool DeleteTmp(string project_name,Model.gtsensortype datatype)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
            strSql.Append("delete from gtsensordata_tmp ");
            strSql.Append(" where project_name=@project_name   and   datatype=@datatype ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100)
							};
            parameters[0].Value = project_name;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteTmp(string project_name)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
            strSql.Append("delete from gtsensordata_tmp ");
            strSql.Append(" where project_name=@project_name   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100)
							};
            parameters[0].Value = project_name;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SingleScalarResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype, int startcyc, int endcyc, out DataTable dt)
        {
            dt = new DataTable();
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmname);
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "   point_name in (select pointname from   " + surveyconn.Database + ".gtpointalarmvalue  where  xmno = " + xmname + "  and  datatype=" + Convert.ToInt32(datatype) + "  )   " : string.Format("point_name='{0}'", pointname);
            
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"select project_name,point_name,senorno,datatype,valuetype,single_oregion_scalarvalue,sec_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue ,cyc,time  from gtsensordata  where   project_name=@xmname     and    datatype=@datatype    and      cyc
          between  @startcyc      and    @endcyc       and    {0}      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),


                    new OdbcParameter("@starttime", OdbcType.Int),
                    new OdbcParameter("@endtime", OdbcType.Int),

                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;
            parameters[4].Value = (startPageIndex - 1) * pageSize;
            parameters[5].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(),parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;
           
        }

        public bool SingleScalarOrgldataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype, int startcyc, int endcyc, out DataTable dt)
        {
            dt = new DataTable();
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmname);
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "   point_name in (select pointname from   " + surveyconn.Database + ".gtpointalarmvalue  where  xmno = " + xmname + "  and  datatype=" + Convert.ToInt32(datatype) + "  )   " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"select project_name,point_name,senorno,datatype,single_oregion_scalarvalue,first_oregion_scalarvalue,sec_oregion_scalarvalue,cyc,time  from gtsensordata  where   project_name=@xmname     and    datatype=@datatype    and      cyc
          between  @startcyc      and    @endcyc       and    {0}      order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),


                    new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int),

                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;
            parameters[4].Value = (startPageIndex - 1) * pageSize;
            parameters[5].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }


//public bool SingleScalarResultTableRowsCount(string project_name,string pointname ,string sord,Model.gtsensortype datatype ,out string totalCont)
        //{
        //    string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
        //    string sql = string.Format("select count(1) from  gtsensordata where project_name='{0}' and  datatype='"+datatype+"' and  {1}",  project_name,searchstr);
        //    OdbcConnection conn = db.GetStanderConn(project_name);
        //    totalCont = querysql.querystanderstr(sql, conn);
        //    return true;
        //}
        public bool ResultDataReportPrint(string sql,string project_name,out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(project_name);
            dt = querysql.querystanderdb(sql,conn);
            return true;
        }
        /// <summary>
        /// 单量结果数据得到一个对象实体
        /// </summary>
        public bool SingleScalarGetModelList(string project_name,Model.gtsensortype datatype,out List<data.Model.gtsensordata> lt )
        {
            OdbcConnection conn = db.GetStanderConn(project_name);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue,sec_oregion_scalarvalue,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,cyc,time  from gtsensordata_tmp ");
            strSql.Append(" where    project_name=@project_name   and   datatype=@datatype  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int)
							};
            parameters[0].Value = project_name;
            parameters[1].Value = datatype;
            lt = new List<Model.gtsensordata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i<ds.Tables[0].Rows.Count)
            {
                lt.Add(SingleScalarDataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        /// <summary>
        /// 单量结果数据得到一个对象实体
        /// </summary>
        public bool SurfacedataSameTimeModelListGet(string project_name,string surfacename ,DateTime dt,out List<data.Model.gtsensordata> lt)
        {
            OdbcConnection conn = db.GetStanderConn(project_name);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue,sec_oregion_scalarvalue,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,cyc,time  from gtsensordata  ");
            strSql.AppendFormat(@" where  time = @time  and   project_name=@project_name   and   datatype=@datatype   and   senorno  in (select sensorno from gtpointalarmvalue where xmno ={0} and datatype={1} and surfacename = '{2}')  order by senorno,time   ",project_name,Convert.ToInt32(data.Model.gtsensortype._supportAxialForce),surfacename);
            OdbcParameter[] parameters = {
                    new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@project_name", OdbcType.VarChar,200),
                    new OdbcParameter("@datatype", OdbcType.VarChar,200)
							};
            parameters[0].Value = dt;
            parameters[1].Value = project_name;
            parameters[2].Value = data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce);
            lt = new List<Model.gtsensordata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(SingleScalarDataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return i > 0 ? true:false;
        }


        //根据项目名获取所以的周期
        public bool CycTimeList(string xmname, data.Model.gtsensortype datatype, out List<string> cycTimeList)
        {

            string sql = "select  distinct(cyc),time  from gtsensordata  where  project_name='" + xmname + "' and  datatype = '"+data.DAL.gtsensortype.GTSensorTypeToString(datatype)+"'  order by cyc asc";
            OdbcConnection conn = db.GetStanderConn(xmname);
            DataTable dt = querysql.querystanderdb(sql, conn);
            cycTimeList = new List<string>();
            if (dt.Rows.Count == 0) return false;
            int i = 0;
            for (i = 0; i < dt.Rows.Count; i++)
            {
                cycTimeList.Add(string.Format("{0}[{1}]", dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1]));
            }
            return true;
        }

        public bool ScalarGetModelList(string project_name,out List<data.Model.gtsensordata> lt)
        {
            OdbcConnection conn = db.GetStanderConn(project_name);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select project_name,point_name,senorno,datatype,valuetype,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,first_oregion_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_oregion_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc,time  from gtsensordata_tmp  ");
            strSql.Append(" where    project_name=@project_name   order by  datatype ,point_name   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100)
							};
            parameters[0].Value = project_name;
            lt = new List<Model.gtsensordata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(ScalarDataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }
        /// <summary>
        /// 单量结果数据得到一个对象实体
        /// </summary>
        public data.Model.gtsensordata SingleScalarDataRowToModel(DataRow row)
        {
            data.Model.gtsensordata model = new data.Model.gtsensordata();
            if (row != null)
            {
                if(row["project_name"]!=null)
				{
					model.project_name=row["project_name"].ToString();
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["senorno"]!=null)
				{
					model.senorno=row["senorno"].ToString();
				}
				if(row["datatype"]!=null)
				{
					model.datatype=row["datatype"].ToString();
				}
				if(row["valuetype"]!=null && row["valuetype"].ToString()!="")
				{
					model.valuetype=int.Parse(row["valuetype"].ToString());
				}
                if (row["first_oregion_scalarvalue"] != null && row["first_oregion_scalarvalue"].ToString() != "")
                {
                    model.first_oregion_scalarvalue = double.Parse(row["first_oregion_scalarvalue"].ToString());
                }
                if(row["single_oregion_scalarvalue"]!=null && row["single_oregion_scalarvalue"].ToString()!="")
                {
				model.single_oregion_scalarvalue=double.Parse(row["single_oregion_scalarvalue"].ToString());
                }
                if(row["single_this_scalarvalue"]!=null && row["single_this_scalarvalue"].ToString()!="")
                {
				model.single_this_scalarvalue=double.Parse(row["single_this_scalarvalue"].ToString());
                }
                if(row["single_ac_scalarvalue"]!=null && row["single_ac_scalarvalue"].ToString()!="")
                {
				model.single_ac_scalarvalue=double.Parse(row["single_ac_scalarvalue"].ToString());
                }
                if (row["sec_oregion_scalarvalue"] != null && row["sec_oregion_scalarvalue"].ToString() != "")
                {
                    model.sec_oregion_scalarvalue = double.Parse(row["sec_oregion_scalarvalue"].ToString());
                }
				if(row["cyc"]!=null && row["cyc"].ToString()!="")
				{
					model.cyc=int.Parse(row["cyc"].ToString());
				}
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
               
               
            }
            return model;
        }

        public data.Model.gtsensordata ScalarDataRowToModel(DataRow row)
        {
            data.Model.gtsensordata model = new data.Model.gtsensordata();
            if (row != null)
            {
                if (row["project_name"] != null)
                {
                    model.project_name = row["project_name"].ToString();
                }
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                if (row["senorno"] != null)
                {
                    model.senorno = row["senorno"].ToString();
                }
                if (row["datatype"] != null)
                {
                    model.datatype = row["datatype"].ToString();
                }
                if (row["valuetype"] != null && row["valuetype"].ToString() != "")
                {
                    model.valuetype = int.Parse(row["valuetype"].ToString());
                }
                if (row["single_oregion_scalarvalue"] != null && row["single_oregion_scalarvalue"].ToString() != "")
                {
                    model.single_oregion_scalarvalue = double.Parse(row["single_oregion_scalarvalue"].ToString());
                }
                if (row["single_this_scalarvalue"] != null && row["single_this_scalarvalue"].ToString() != "")
                {
                    model.single_this_scalarvalue = double.Parse(row["single_this_scalarvalue"].ToString());
                }
                if (row["single_ac_scalarvalue"] != null && row["single_ac_scalarvalue"].ToString() != "")
                {
                    model.single_ac_scalarvalue = double.Parse(row["single_ac_scalarvalue"].ToString());
                }

                if (row["first_oregion_scalarvalue"] != null && row["first_oregion_scalarvalue"].ToString() != "")
                {
                    model.first_oregion_scalarvalue = double.Parse(row["first_oregion_scalarvalue"].ToString());
                }
                if (row["first_this_scalarvalue"] != null && row["first_this_scalarvalue"].ToString() != "")
                {
                    model.first_this_scalarvalue = double.Parse(row["first_this_scalarvalue"].ToString());
                }
                if (row["first_ac_scalarvalue"] != null && row["first_ac_scalarvalue"].ToString() != "")
                {
                    model.first_ac_scalarvalue = double.Parse(row["first_ac_scalarvalue"].ToString());
                }

                if (row["sec_oregion_scalarvalue"] != null && row["sec_oregion_scalarvalue"].ToString() != "")
                {
                    model.sec_oregion_scalarvalue = double.Parse(row["sec_oregion_scalarvalue"].ToString());
                }
                if (row["sec_this_scalarvalue"] != null && row["sec_this_scalarvalue"].ToString() != "")
                {
                    model.sec_this_scalarvalue = double.Parse(row["sec_this_scalarvalue"].ToString());
                }
                if (row["sec_ac_scalarvalue"] != null && row["sec_ac_scalarvalue"].ToString() != "")
                {
                    model.sec_ac_scalarvalue = double.Parse(row["sec_ac_scalarvalue"].ToString());
                }

                if (row["cyc"] != null && row["cyc"].ToString() != "")
                {
                    model.cyc = int.Parse(row["cyc"].ToString());
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }


            }
            return model;
        }

        public bool GetLastTimeModel(string xmname, string pointname, Model.gtsensortype datatype, DateTime dt, out data.Model.gtsensordata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM   gtsensordata   where project_name =   @project_name  and   datatype=@datatype  and     point_name =  @point_name    and   time < @time   order by time desc limit 0,2  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,200),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = pointname;
            parameters[3].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = ScalarDataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

        public bool GetInitTimeModel(string xmname, string pointname, Model.gtsensortype datatype, out data.Model.gtsensordata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select min(time) as time,project_name,point_name,senorno,datatype,valuetype,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,first_oregion_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_oregion_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc  ");
            strSql.Append(" FROM   gtsensordata   where project_name =   @project_name  and   datatype=@datatype  and     point_name =  @point_name   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100),
					new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = pointname;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = ScalarDataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

        public bool PointNameDateTimeListGet(string xmname, string pointname,data.Model.gtsensortype datatype, out List<string> ls)
        {

            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(time)  from  gtsensordata    where         project_name=@xmname    and   datatype=@datatype    and      {0}     order by time  asc  ", pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
                    new OdbcParameter("@datatype", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(string.Format("[{0}]", ds.Tables[0].Rows[i].ItemArray[0].ToString()));
                i++;
            }
            return true;

        }
        public bool SingleScalarGetModel(string project_name, string pointname, DateTime dt,Model.gtsensortype datatype ,out data.Model.gtsensordata model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(project_name);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue,sec_oregion_scalarvalue,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,cyc,time  from gtsensordata ");
            strSql.Append(" where    project_name=@project_name   and  datatype=@datatype  and   point_name=@point_name    and    time = @time   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.Int),
                    new OdbcParameter("@datatype", OdbcType.Int),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = project_name;
            parameters[0].Value = datatype;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = SingleScalarDataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }
        public bool MaxTime(string project_name, Model.gtsensortype datatype, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtsensordata  where   project_name = @project_name   and   datatype=@datatype   ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@project_name",OdbcType.VarChar,100),
                new OdbcParameter("@datatype",OdbcType.VarChar,100)
            };
            paramters[0].Value = project_name;
            paramters[1].Value =data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),paramters);
            if (obj == null||obj.ToString() == "") return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool MaxTime(string project_name, Model.gtsensortype datatype,string point_name ,out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from gtsensordata  where   project_name = @project_name   and   datatype=@datatype  and point_name=@point_name  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@project_name",OdbcType.VarChar,100),
                new OdbcParameter("@datatype",OdbcType.VarChar,100),
                new OdbcParameter("@point_name",OdbcType.VarChar,100)
            };
            paramters[0].Value = project_name;
            paramters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            paramters[2].Value = point_name;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool MinTime(string project_name, Model.gtsensortype datatype, string point_name, out DateTime MinTime)
        {
            MinTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Min(time) from gtsensordata  where   project_name = @project_name   and   datatype=@datatype  and point_name=@point_name  ");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@project_name",OdbcType.VarChar,100),
                new OdbcParameter("@datatype",OdbcType.VarChar,100),
                new OdbcParameter("@point_name",OdbcType.VarChar,100)
            };
            paramters[0].Value = project_name;
            paramters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            paramters[2].Value = point_name;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null || obj.ToString() == "") return false;
            MinTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool dataPointLoadDAL(int xmno, Model.gtsensortype datatype, out List<string> ls)
        {
           
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select DISTINCT(POINT_NAME) from gtpointalarmvalue where xmno=" + xmno +" and  datatype='"+datatype+"'";
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }
        public bool PointNewestDateTimeGet(string project_name, string pointname, Model.gtsensortype datatype, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from gtsensordata    where    project_name=@project_name  and  datatype=@datatype  and point_name = @point_name     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@project_name", OdbcType.VarChar,120),
                    new OdbcParameter("@datatype", OdbcType.Int),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = project_name;
            parameters[0].Value = datatype;
            parameters[1].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
            if (obj == null) { dt = Convert.ToDateTime("0001-1-1"); return false; }
            dt = Convert.ToDateTime(obj); return true;
            
        }
        public bool XmStateTable(int startPageIndex, int pageSize, string project_name,string xmname,string unitname ,string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(project_name);
            StringBuilder strSql = new StringBuilder(256);
            //strSql.AppendFormat(@"SELECT '"+project_name+"' as project_name,'" + unitname + "' as unitname ,max(time) as lastrectime,iF((SELECT IFNULL(MAX(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from gtsensordata where project_name ='" + project_name + @"' ) > DATE_ADD(SYSDATE(),INTERVAL -IFNULL((select  hour*60+minute from datadatauploadinterval  where  project_name='"+project_name+@"'),24*60) minute),'true','false' ) as state ,(select count(1) from pointcheck  where project_name=" + project_name + " and type='表面位移') as alarmcount from gtsensordata where taskname='" + project_name + @"'");
            dt = querysql.querystanderdb(strSql.ToString(), conn);
            if (dt != null) return true;
            return false;
        }
        public bool IsgtsensordataDataExist(string project_name,string pointname,DateTime dt)
         {
             OdbcSQLHelper.Conn = db.GetStanderConn(project_name);
             StringBuilder strsql = new StringBuilder();
             strsql.Append("select    count(1)     from     gtsensordata    where     project_name=@project_name     and     point_name =@point_name    and        time=@time  ");
             OdbcParameter[] parameters ={ 
                 new OdbcParameter("@project_name",OdbcType.Int),
                 new OdbcParameter("@point_name",OdbcType.VarChar,100),
                 new OdbcParameter("@time",OdbcType.DateTime)
             };
             parameters[0].Value = project_name;
             parameters[1].Value = pointname;
             parameters[2].Value = dt;
             object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
             return Convert.ToInt32(obj) > 0 ? true : false;
         }

        public bool OnLinePointCont(string unitname,List<string> finishedxmnolist,out string contpercent)
        {
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            OdbcConnection SurveyConn = db.GetUnitStanderConn(unitname);
            contpercent = "0/0";
            string sql = string.Format("select count(distinct(gtpointalarmvalue.pointname)),(select count(1) from gtpointalarmvalue )  from gtpointalarmvalue left join gtsensordata on gtsensordata.project_name = gtpointalarmvalue.xmno    and  gtpointalarmvalue.pointname = gtsensordata.point_name    where  {0}  and  time between DATE_ADD(SYSDATE(),INTERVAL -1 day) and SYSDATE()", finishedxmnolist.Count == 0?" 1=1 " : " gtsensordata.project_name not in ("+string.Join(",",finishedxmnolist)+")");
            ExceptionLog.ExceptionWrite(sql);
            DataSet ds = OdbcSQLHelper.Query(sql);
            if (ds == null) return false;
            DataTable dt = ds.Tables[0];
            contpercent = string.Format("{0}/{1}", dt.Rows[0].ItemArray[0], dt.Rows[0].ItemArray[1]);//querysql.querystanderstr(sql, 
            return true;
        }

        public bool SiblingDataCycGet(string xmname, DateTime dt,data.Model.gtsensortype datatype, List<string> points, double mobileStationInteval, out  int cyc)
        {
            cyc = -1;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.AppendFormat("select  distinct(cyc)  from gtsensordata   where     time   between   '{0}'    and     '{1}'    and     project_name='{2}' and  datatype='{3}'  and cyc not in (select  cyc  from    gtsensordata    where     time   between   '{0}'    and     '{1}'    and     project_name='{2}' and  datatype='{3}'  and  point_name  in ('{4}')  group by cyc  )  order by cyc asc ", dt.AddHours(-mobileStationInteval / 2), dt.AddHours(mobileStationInteval / 2), xmname,data.DAL.gtsensortype.GTSensorTypeToString(datatype), string.Join("','", points));
            string sql = strsql.ToString();
            ExceptionLog.ExceptionWrite(sql);
            List<string> ls = querysql.querystanderlist(strsql.ToString(), OdbcSQLHelper.Conn);
            if (ls == null || ls.Count == 0) return false;
            cyc = Convert.ToInt32(ls[0]);
            return true;
        }
        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(string xmname, DateTime dt, data.Model.gtsensortype datatype)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select count(1) from gtsensordata where    time > @time    and   datatype = @datatype    and    project_name=@taskname  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@time",OdbcType.DateTime),
                 new OdbcParameter("@datatype",OdbcType.VarChar,100),
                 new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = dt;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = xmname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);

            return Convert.ToInt32(obj) > 0 ? true : false;
        }
        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(string xmname,DateTime dt,data.Model.gtsensortype datatype ,out  int cyc)
        {
            cyc = -1;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select    min(cyc)    from    gtsensordata    where     time>@time    and   datatype=@datatype  and   project_name=@taskname ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@time",OdbcType.DateTime),
                 new OdbcParameter("@datatype",OdbcType.VarChar,100),
                  new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = dt;
            parameters[1].Value =data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = xmname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            if (obj == null) return false;
            cyc = Convert.ToInt32(obj);
            return true;
        }
        public bool ReportDataView(List<string> cyclist, int startPageIndex, int pageSize, string xmname,data.Model.gtsensortype datatype ,string sord, out DataTable dt)
        {
            dt = new DataTable();

            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select point_name,cyc,single_oregion_scalarvalue,single_this_scalarvalue,single_ac_scalarvalue,time from gtsensordata  where      project_name=@project_name         and    point_name in (select distinct(point_name) from " + surveyconn.Database + ".gtpointalarmvalue where xmno =" + xmname + " )  and  datatype='"+data.DAL.gtsensortype.GTSensorTypeToString(datatype)+"'  and   CYC    in ({0})  order by {1}  asc  limit    @startPageIndex,   @endPageIndex", string.Join(",", cyclist), sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = (startPageIndex - 1) * pageSize;
            parameters[2].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool InsertCycStep(string xmname, data.Model.gtsensortype datatype ,int startcyc)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("update    gtsensordata    set     cyc = cyc+1     where      cyc>@cyc   and   datatype=@datatype     and       project_name=@taskname ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@cyc",OdbcType.Int),
                   new OdbcParameter("@datatype",OdbcType.VarChar,100),
                  new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = startcyc - 1;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = xmname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strsql.ToString(), parameters);
            return rows > 0 ? true : false;
        }

        public bool IsCycdirnetDataExist(string xmname, string pointname, DateTime dt,data.Model.gtsensortype datatype)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select    count(1)     from     gtsensordata    where     project_name=@taskname     and     point_name =@point_name  and  datatype = @datatype   and        time=@time  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@taskname",OdbcType.VarChar,100),
                 new OdbcParameter("@point_name",OdbcType.VarChar,100),
                 new OdbcParameter("@point_name",OdbcType.VarChar,100),
                 new OdbcParameter("@time",OdbcType.DateTime)
             };
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            parameters[2].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[3].Value = dt;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            return Convert.ToInt32(obj) > 0 ? true : false;
        }

        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(string xmname, int cyc,data.Model.gtsensortype datatype)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select count(1) from gtsensordata  where    cyc =@cyc    and datatype=@datatype    and    project_name=@taskname  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@cyc",OdbcType.Int),
                 new OdbcParameter("@datatype",OdbcType.VarChar,100),
                 new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = cyc;
            parameters[1].Value = data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            parameters[2].Value = xmname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            return Convert.ToInt32(obj) > 0 ? true : false;
        }

        //根据项目名称获取端点周期
        public bool ExtremelyCycGet(string xmname, data.Model.gtsensortype datatype, string DateFunction, out string ExtremelyCyc)
        {

            string sql = "select  " + DateFunction + "(cyc)  from gtsensordata where project_name='" + xmname + "'  and  datatype = '" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "'   ";
            OdbcConnection conn = db.GetStanderConn(xmname);
            ExtremelyCyc = querysql.querystanderstr(sql, conn);
            if (ExtremelyCyc == "") ExtremelyCyc = "0";
            return ExtremelyCyc != null ? true : false;

        }
		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "gtsensordata";
			parameters[1].Value = "time";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

