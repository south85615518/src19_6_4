﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using SqlHelpers;

namespace data.BLL
{
    public partial class gtsensordata
    {

        public bool ThirdScalarResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype, DateTime startTime, DateTime endTime, out DataTable dt,out string mssg)
        {
            dt = new DataTable();
            mssg = "";
            try
            {
                if (dal.TwoScalarResultdataTableLoad(startPageIndex, pageSize, xmname, pointname, sord, datatype, startTime, endTime, out dt))
                {
                    mssg = string.Format("成功获取到三量结果数据表记录数{0}条", dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到三量结果数据表记录失败", dt.Rows.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到三量结果数据表记录出错,错误信息:" + ex.Message, dt.Rows.Count);
                return false;
            }

        }
    }
}
