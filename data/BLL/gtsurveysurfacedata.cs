﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacedata.cs
*
* 功 能： N/A
* 类 名： gtsurfacedata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using Tool;
using SqlHelpers;
namespace data.BLL
{
	/// <summary>
	/// gtsurfacedata
	/// </summary>
	public partial class gtsurfacedata
	{
        
		
		#region  BasicMethod
        public bool AddSurveyData(int xmno,data.Model.gtsensortype  datatype,int importcyc,  out string mssg)
        {
            try
            {
                if (dal.AddData(xmno, datatype, importcyc))
                {
                    mssg = string.Format("添加项目编号{0}{1}第{2}周期的数据到编辑库成功", xmno, "混凝土支撑内力", importcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}{1}第{2}周期的数据到编辑库失败", xmno, "混凝土支撑内力", importcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}{1}第{2}周期的数据到编辑库出错,错误信息:" + ex.Message, xmno, "混凝土支撑内力", importcyc);
                return false;
            }
        }

        //public bool SurveyPointTimeDataAdd(int xmno,  string pointname, DateTime time, DateTime importtime, out string mssg)
        //{
        //    try
        //    {
        //        if (dal.SurveyPointTimeDataAdd(xmno, pointname,time, importtime))
        //        {
        //            mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据成功", xmno, pointname, time, importtime);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据失败", xmno, pointname, time, importtime);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据出错,错误信息:" + ex.Message, xmno, pointname, time, importtime);
        //        return false;
        //    }
        //}


        public bool DeleteSurveyData(int xmno, data.Model.gtsensortype datatype, int startcyc, int endcyc, out string mssg)
        {
            try
            {
                if (dal.DeleteSurvey(xmno, datatype, startcyc, endcyc))
                {
                    mssg = string.Format("删除项目编辑库{0}{1}从{2}到{3}周期的数据成功", xmno, "混凝土支撑内力", startcyc, endcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编辑库{0}{1}从{2}到{3}周期的数据失败", xmno, "混凝土支撑内力", startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编辑库{0}{1}从{2}到{3}周期的数据出错,错误信息:" + ex.Message, xmno, "混凝土支撑内力", startcyc, endcyc);
                return false;
            }
        }

        public bool UpdataSurveySurfaceData(data.Model.gtsurfacedata model, out string mssg)
        {
            try
            {
                if (dal.UpdataSurveySurfaceData(model))
                {
                    mssg = string.Format("更新项目{0}{1}点{2}{3}数据成功", model.xmno,"混凝土支撑内力", model.surfacename, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目{0}{1}点{2}{3}数据失败", model.xmno, "混凝土支撑内力", model.surfacename, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目{0}{1}点{2}{3}数据出错，错误信息:" + ex.Message, model.xmno, "混凝土支撑内力", model.surfacename, model.time);
                return false;
            }
        }
        public bool UpdataSurveySurfaceDataNextCYCThis(data.Model.gtsurfacedata model, out string mssg)
        {
            try
            {
                if (dal.UpdataSurveySurfaceDataNextCYCThis(model))
                {
                    mssg = string.Format("更新项目{0}{1}点{2}{3}数据成功", model.xmno, "混凝土支撑内力", model.surfacename, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目{0}{1}点{2}{3}数据失败", model.xmno, "混凝土支撑内力", model.surfacename, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目{0}{1}点{2}{3}数据出错，错误信息:" + ex.Message, model.xmno, "混凝土支撑内力", model.surfacename, model.time);
                return false;
            }
        }

        public bool LostPoints(int xmno, data.Model.gtsensortype datatype, int cyc, out string pointstr, out string mssg)
        {
            mssg = "";
            pointstr = "";
            try
            {
                if (dal.LackPoints(xmno, datatype, cyc, out pointstr))
                {
                    mssg = string.Format("项目{0}{1}第{2}周期有{3}点缺失", xmno, "混凝土支撑内力", cyc, pointstr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}第{2}周期缺失点失败", xmno, "混凝土支撑内力", cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}第{2}周期缺失点出错，错误信息:" + ex.Message, xmno, "混凝土支撑内力", cyc);
                return false;

            }
        }

        //public bool DeleteSurveyData(int xmno, data.Model.gtsensortype datatype, string point_name, DateTime starttime, DateTime endtime, out string mssg)
        //{
        //    try
        //    {
        //        if (dal.DeleteSurveyData(xmno, datatype, point_name, starttime, endtime))
        //        {
        //            mssg = string.Format("删除项目编辑库{0}点名{1}从{2}到{3}的数据成功", xmno,  point_name, starttime, endtime);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("删除项目编辑库{0}点名{1}从{2}到{3}的数据失败", xmno,  point_name, starttime, endtime);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("删除项目编辑库{0}点名{1}从{2}到{3}的数据出错,错误信息：" + ex.Message, xmno,  point_name, starttime, endtime);
        //        return false;
        //    }
        //}
        public bool PointNameSurveyDateTimeListGet(int xmno, data.Model.gtsensortype datatype, string pointname, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.PointNameSurveyDateTimeListGet(xmno, datatype, pointname, out ls))
                {
                    mssg = string.Format("获取到项目{0}点{1}编辑库的时间列表记录数{2}成功", xmno, pointname, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}点{1}编辑库的时间列表失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}点{1}编辑库的时间列表出错,错误信息:" + ex.Message, xmno, pointname);
                return false;
            }


        }
        //根据项目名获取的周期
        public bool SurveyCYCDateTimeListGet(int xmno, data.Model.gtsensortype datatype, out List<string> cyctimelist, out string mssg)
        {

            cyctimelist = null;
            try
            {

                if (dal.SurveyCYCDateTimeListGet(xmno, datatype, out cyctimelist))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }
        public bool SurveyResultdataTableLoad(int startPageIndex, int pageSize, int xmno, data.Model.gtsensortype datatype, string pointname, string sord, int startcyc, int endcyc, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SurveyResultdataTableLoad(startPageIndex, pageSize, xmno, pointname, datatype, sord, startcyc, endcyc, out  dt))
                {
                    mssg = string.Format("{0}结果数据表加载成功!", xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}结果数据表加载失败!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}结果数据表加载出错，出错信息" + ex.Message, xmno);
                return false;
            }

        }
        public bool SurveyResultTableRowsCount(int xmno, data.Model.gtsensortype datatype, string pointname, int startcyc, int endcyc, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.SurveyResultTableRowsCount(xmno, datatype, pointname, startcyc, endcyc, out totalCont))
                {
                    mssg = string.Format("{0}编辑库结果数据表记录数加载成功!", xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}编辑库结果数据表记录数加载成功!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}编辑库结果数据表加载出错，出错信息" + ex.Message, xmno);
                return false;
            }
        }
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

