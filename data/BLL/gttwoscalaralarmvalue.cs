﻿/**  版本信息模板在安装目录下，可自行修改。
* gtalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:27   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using System.Collections.Generic;
using SqlHelpers;
namespace data.BLL
{
	/// <summary>
	/// 
	/// </summary>
	public partial class gtalarmvalue
	{
        /// <summary>
        /// 双量预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TwoScalarTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, data.Model.gtsensortype datatype, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TwoScalarTableLoad(startPageIndex, pageSize, xmno, colName, sord, datatype, out  dt))
                {
                    mssg = string.Format("加载项目{0}的{2}预警参数{1}条成功", xmno, dt.Rows.Count, datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("加载项目{0}的{1}预警参数失败", xmno, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目{0}的{1}预警参数出错，错误信息:" + ex.Message, xmno, datatype);
                return false;
            }
        }
        /// <summary>
        /// 双量预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TwoScalarTableRowsCount(string searchstring, int xmno, data.Model.gtsensortype datatype, out int totalCont,out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TwoScalarTableRowsCount(searchstring, xmno, datatype, out totalCont))
                {
                    mssg = string.Format("加载项目{0}{2}预警参数表记录数{1}成功!", xmno, totalCont);
                    return true;

                }
                else
                {
                    mssg = string.Format("加载项目{0}{1}预警参数表记录数失败!", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载项目{0}{1}预警参数表加载出错，出错信息" + ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 得到双量预警一个对象实体
        /// </summary>
        public bool GetTwoScalarvalueModel(string name, int xmno, data.Model.gtsensortype datatype, out data.Model.gtalarmvalue model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetTwoScalarvalueModel(name, xmno, datatype, out model))
                {
                    mssg = string.Format("获取项目{0}{2}预警名称{1}实体成功", xmno, name, datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{2}预警名称{1}实体失败", xmno, name, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{2}预警名称{1}实体出错，错误信息:" + ex.Message, xmno, name, datatype);
                return false;
            }
        }
        
	}
}

