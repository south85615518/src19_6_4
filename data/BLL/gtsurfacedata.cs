﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacedata.cs
*
* 功 能： N/A
* 类 名： gtsurfacedata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using Tool;
using SqlHelpers;
namespace data.BLL
{
	/// <summary>
	/// gtsurfacedata
	/// </summary>
	public partial class gtsurfacedata
	{
        
		private readonly data.DAL.gtsurfacedata dal=new data.DAL.gtsurfacedata();
		public gtsurfacedata()
		{}
		#region  BasicMethod
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsurfacedata model,out string mssg)
		{
             try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目{0}混凝土支撑轴面{1}测量时间{2}结果数据添加成功",model.xmno,model.surfacename,model.time);
                    return true;
                }
                else {
                    mssg =  string.Format("项目{0}混凝土支撑轴面{1}测量时间{2}结果数据添加失败",model.xmno,model.surfacename,model.time);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("项目{0}混凝土支撑轴面{1}测量时间{2}结果数据添加出错，错误信息:"+ex.Message,model.xmno,model.surfacename,model.time);
                return false;
            }
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(data.Model.gtsurfacedata model,out string mssg)
		{

            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("项目{0}混凝土支撑轴面{1}测量时间{2}结果数据更新成功",model.xmno,model.surfacename,model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}混凝土支撑轴面{1}测量时间{2}结果数据更新失败", model.xmno, model.surfacename, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}混凝土支撑轴面{1}测量时间{2}结果数据更新出错,错误信息:"+ex.Message, model.xmno, model.surfacename, model.time);
                return false;
            }



			return dal.Update(model);
		}

        public bool DeleteData(int xmno, data.Model.gtsensortype datatype, int startcyc, int endcyc, out string mssg)
        {
            try
            {
                if (dal.Delete(xmno, datatype, startcyc, endcyc))
                {
                    mssg = string.Format("删除项目{0}{1}从{3}到{4}周期的数据成功", xmno, "混凝土支撑内力", startcyc, endcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}{1}从{3}到{4}周期的数据失败", xmno, "混凝土支撑内力", startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}{1}从{3}到{4}周期的数据出错,错误信息:" + ex.Message, xmno, "混凝土支撑内力", startcyc, endcyc);
                return false;
            }
        }


		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool Delete(int xmno,DateTime time,string point_name)
        //{
			
        //    return dal.Delete(project_name,time,point_name);
        //}
        //public bool Delete(int xmno, string point_name, data.Model.gtsensortype datatype, DateTime starttime, DateTime endtime, out string mssg)
        //{
        //    try
        //    {
        //        if (dal.Delete(project_name, point_name, starttime, endtime))
        //        {
        //            mssg = string.Format("删除项目{0}{1}混凝土支撑轴面{2}从{3}到{4}的数据成功", project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("删除项目{0}{1}混凝土支撑轴面{2}从{3}到{4}的数据失败", project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("删除项目{0}{1}混凝土支撑轴面{2}从{3}到{4}的数据出错,错误信息："+ex.Message, project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), point_name, starttime, endtime);
        //        return false;
        //    }
        //}

        public bool SurfaceDataSameTimeModel(int xmno, data.Model.gtsensortype datatype, string surfacename, DateTime dt, out data.Model.gtsurfacedata model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.SurfaceDataSameTimeModel(xmno, datatype, surfacename, dt, out model))
                {
                    mssg = string.Format("获取项目编号{0}{1}混凝土支撑轴面{2}前的实体对象成功", xmno, surfacename, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}混凝土支撑轴面{2}前的实体对象失败", xmno, surfacename, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}混凝土支撑轴面{2}前的实体对象出错,错误信息：" + ex.Message, xmno, surfacename, dt);
                return false;
            }
        }


        public bool GetLastTimeModel(int xmno, data.Model.gtsensortype datatype, string pointname, DateTime dt, out data.Model.gtsurfacedata model, out string mssg)
        {
            model = null;
            try {
                if (dal.GetLastTimeModel(xmno, datatype, pointname, dt, out model))
                {
                    mssg = string.Format("获取项目编号{0}混凝土支撑轴面{1}{2}前的实体对象成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}混凝土支撑轴面{1}{2}前的实体对象失败", xmno,  pointname, dt);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("获取项目编号{0}混凝土支撑轴面{1}{2}前的实体对象出错,错误信息："+ex.Message, xmno,  pointname, dt);
                return false;
            }
        }

        public bool gtsurfacedatacycupdate(int xmno, data.Model.gtsensortype datatype, string pointname, DateTime dt, out string mssg)
        {
            try
            {
                if (dal.gtsurfacedatacycupdate(xmno, datatype, pointname, dt))
                {
                    mssg = string.Format("更新项目{0}混凝土支撑轴面{1}{2}的周期成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目{0}混凝土支撑轴面{1}{2}的周期失败", xmno, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目{0}混凝土支撑轴面{1}{2}的周期出错,错误信息:"+ex.Message, xmno, pointname, dt);
                return false;
            }




        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteTmp(int xmno, data.Model.gtsensortype datatype, out string mssg)
        {
            try
            {
                if (dal.DeleteTmp(xmno,datatype))
                {
                    mssg = string.Format("删除项目{0}临时表的数据成功",xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}临时表的数据失败",xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}临时表的数据出错,错误信息："+ex.Message,xmno);
                return false;
            }

        }

        public bool ResultdataTableLoad(int startPageIndex, int pageSize, int xmno, data.Model.gtsensortype datatype, string pointname, string sord, int startcyc, int endcyc, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultdataTableLoad(startPageIndex, pageSize, xmno, pointname, datatype, startcyc, endcyc, sord, out  dt))
                {
                    mssg = string.Format("{0}结果数据表加载成功!", xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}结果数据表加载失败!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}结果数据表加载出错，出错信息" + ex.Message, xmno);
                return false;
            }
        }

        public bool ResultTableRowsCount(int xmno, data.Model.gtsensortype datatype, string pointname, DateTime startTime, DateTime endTime, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.ResultTableRowsCount(xmno, datatype, pointname, startTime, endTime, out totalCont))
                {
                    mssg = string.Format("{0}结果数据表记录数加载成功!",xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}结果数据表记录数加载成功!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}结果数据表加载出错，出错信息" + ex.Message, xmno);
                return false;
            }
        }

        public bool ReportDataView(List<string> cyclist, int startPageIndex, int pageSize, int xmno, data.Model.gtsensortype datatype, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.ReportDataView(cyclist, startPageIndex, pageSize, xmno, datatype, sord, out  dt))
                {
                    mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表成功", xmno, "混凝土支撑内力", string.Join("、", cyclist));
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表失败", xmno, "混凝土支撑内力", string.Join("、", cyclist));
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表出错，错误信息:" + ex.Message, xmno, "混凝土支撑内力", string.Join("、", cyclist));
                return false;
            }
        }


        //public bool ResultDataReportPrint(string sql, int xmno, out DataTable dt,out string mssg)
        //{
        //    dt = null;
        //    try
        //    {
        //        if (dal.ResultDataReportPrint(sql, project_name, out dt))
        //        {
        //            mssg = "{0}结果数据报表数据表生成成功";
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = "{0}结果数据报表数据表生成失败";
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
                
        //            mssg = "{0}结果数据报表数据表生成出错，错误信息"+ex.Message;
        //            return false;
                
        //    }
        //}
        public bool PointNameDateTimeListGet(int xmno, data.Model.gtsensortype datatype, string pointname, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.PointNameDateTimeListGet(xmno, datatype, pointname, out ls))
                {
                    mssg = string.Format("获取到项目{0}{1}混凝土支撑轴面的时间列表记录数{2}成功", xmno, pointname,  ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}混凝土支撑轴面的时间列表失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}混凝土支撑轴面的时间列表出错,错误信息:"+ex.Message, xmno, pointname);
                return false;
            }


        }

        //根据项目名获取的周期
        //根据项目名获取的周期
        public bool CYCDateTimeListGet(int xmno, data.Model.gtsensortype datatype, out List<string> cyctimelist, out string mssg)
        {

            cyctimelist = null;
            try
            {

                if (dal.CYCDateTimeListGet(xmno, datatype, out cyctimelist))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }

        public bool MaxTime(int xmno, data.Model.gtsensortype datatype, out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno, datatype, out maxtime))
                {
                    mssg = string.Format("获取项目{0}测量数据的最大日期{1}成功", xmno, maxtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}测量数据的最大日期失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}测量数据的最大日期出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        public bool MaxTime(int xmno, data.Model.gtsensortype datatype, string point_name, out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno, datatype, point_name, out maxtime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据混凝土支撑轴面{1}的最大日期{1}成功", xmno, maxtime, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{2}测量数据混凝土支撑轴面的最大日期失败", xmno, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{2}测量数据混凝土支撑轴面的最大日期出错，错误信息:" + ex.Message, xmno,point_name);
                return false;
            }
        }
        public bool MinTime(int xmno, data.Model.gtsensortype datatype, string point_name, out DateTime Mintime, out string mssg)
        {
            Mintime = new DateTime();
            try
            {
                if (dal.MinTime(xmno, datatype, point_name, out Mintime))
                {
                    mssg = string.Format("获取项目{0}测量数据混凝土支撑轴面{2}的最小日期{1}成功", xmno, Mintime, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据混凝土支撑轴面的最小日期失败", xmno,  point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据混凝土支撑轴面的最小日期出错，错误信息:" + ex.Message, xmno, point_name);
                return false;
            }
        }
        
         //<summary>
         //得到一个对象列表
         //</summary>
        public bool GetModelList(int xmno, data.Model.gtsensortype datatype, out List<data.Model.gtsurfacedata> lt, out string mssg)
        {
            lt = new List<Model.gtsurfacedata>();
            try
            {
                if (dal.GetModelList(xmno, datatype, out lt))
                {
                    mssg = string.Format("获取项目{0}临时表中的结果数据成功!", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}临时表中的结果数据失败!", xmno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}临时表中的结果数据出错!错误信息:" + ex.Message, xmno);
                return false;
            }

        }

        public bool dataPointLoadBLL(int xmno, data.Model.gtsensortype datatype, out List<string> ls, out string mssg)
        {
            ls = null;
            try
            {
                if (dal.dataPointLoadDAL(xmno, datatype, out ls))
                {
                    mssg = string.Format("项目编号{0}混凝土支撑轴面号加载成功",xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("项目编号{0}混凝土支撑轴面号加载失败", xmno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}混凝土支撑轴面号加载出错，错误信息" + ex.Message,xmno);
                return false;
            }

        }
        public bool PointNewestDateTimeGet(int xmno, data.Model.gtsensortype datatype, string pointname, out DateTime dt, out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(xmno, datatype, pointname, out dt))
                {
                    mssg = string.Format("获取项目{0}{1}混凝土支撑轴面的最新数据的测量时间{2}成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}混凝土支撑轴面的最新数据的测量时间失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}}混凝土支撑轴面的最新数据的测量时间出错，错误信息:" + ex.Message, xmno, pointname);
                return false;
            }

        }
        //public bool XmStateTable(int startPageIndex, int pageSize, int xmno,string xmname, string unitname, string colName, string sord, out DataTable dt,out string mssg)
        //{
        //    dt = null;
        //    mssg = "";
        //    try
        //    {
        //        if (dal.XmStateTable(startPageIndex, pageSize, project_name, xmname, unitname, colName, sord, out  dt))
        //        {
        //            mssg = string.Format("获取编号为{0}项目名{1}的{3}项目状态表记录数{2}条成功", project_name, xmname, dt.Rows.Count);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("获取编号为{0}项目名{1}的{3}项目状态表失败", project_name, xmname);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("获取编号为{0}项目名{1}的{3}项目状态表出错,错误信息:"+ex.Message, project_name, xmname);
        //        return false;
        //    }
        //}
        public bool LackPointCalculate(int xmno,data.Model.gtsensortype datatype ,out DataTable dt,out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.LackPointCalculate(xmno,datatype ,out dt))
                {
                    mssg = string.Format("获取到项目编号{0}{1}结算点缺失的结算点{2}个", xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype), dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("经检查项目编号{0}{1}结算点没有缺失情况", xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("检查项目编号{0}{1}结算点没有缺失情况出错错误信息:" + ex.Message, xmno,data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }

        }

        public bool CycSort(int xmno, data.Model.gtsensortype datatype, out string mssg)
        {
            mssg = "";
            int rows = 0;
            try
            {
                if (dal.CycSort(xmno, datatype, out rows))
                {
                    mssg = string.Format("对项目{0}{1}数据表中的{1}条记录周期排序成功", xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype), rows);
                    return true;
                }
                else
                {
                    mssg = string.Format("对项目{0}{1}数据表中的记录周期排序失败", xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("对项目{0}{1}数据表中的记录周期排序出错,错误信息:" + ex.Message, xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
        }

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

