﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacestructure.cs
*
* 功 能： N/A
* 类 名： gtsurfacestructure
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:28   N/A    初版
*
* Copyright (c) 2012 #SensorName# Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace data.BLL
{
    /// <summary>
    /// 数据访问类:gtsurfacestructure
    /// </summary>
    public partial class gtsurfacestructure
    {
        public gtsurfacestructure()
        { }
        public static data.DAL.gtsurfacestructure dal = new data.DAL.gtsurfacestructure();
        #region  BasicMethod
        //判断钢筋支撑面名对象是否存在
        public bool Exist(data.Model.gtsurfacestructure model, out string mssg)
        {

            try
            {
                if (dal.Exist(model))
                {
                    mssg = string.Format("项目编号{0}{2}{1}钢筋支撑面已经存在", model.xmno, model.surfacename, model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}{2}{1}钢筋支撑面不存在", model.xmno, model.surfacename, model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}{2}{1}钢筋支撑面查询出错,错误信息:" + ex.Message, model.xmno, model.surfacename, model.datatype);
                return true;
            }

        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtsurfacestructure model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("{0}预警钢筋支撑面新增成功", model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}预警钢筋支撑面新增失败", model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}预警钢筋支撑面新增出错，错误信息" + ex.Message, model.datatype);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtsurfacestructure model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("{1}预警钢筋支撑面{0}更新成功", model.surfacename, model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("{1}预警钢筋支撑面{0}更新失败", model.surfacename, model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}预警钢筋支撑面更新出错，错误信息" + ex.Message, model.datatype);
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, data.Model.gtsurfacestructure model, out string mssg)
        {
            try
            {
                if (dal.MultiUpdate(pointNameStr, model))
                {
                    mssg = string.Format("{0}预警钢筋支撑面批量更新成功", pointNameStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}预警钢筋支撑面批量更新失败", pointNameStr);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}预警钢筋支撑面批量更新出错，错误信息" + ex.Message, model.datatype);
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(data.Model.gtsurfacestructure model, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("{0}预警钢筋支撑面删除成功", model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}预警钢筋支撑面删除失败", model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}预警钢筋支撑面删除出错，错误信息" + ex.Message, model.datatype);
                return false;
            }
        }

        public bool GTsurfacestructurePointLoadDAL(int xmno,data.Model.gtsensortype datatype ,out List<string> ls, out string mssg)
        {
            mssg = "";
            ls = new List<string>();
            try
            {
                if (dal.gtsurfacestructurePointLoadDAL(xmno, datatype, out ls))
                {
                    mssg = string.Format("获取到项目编号{0}的{1}支撑面数{2}个", xmno,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}的{1}支撑面失败", xmno,data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}的{1}支撑面出错,错误信息:" + ex.Message, xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, data.Model.gtsensortype datatype, string pointname, out data.Model.gtsurfacestructure model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, datatype, out model))
                {
                    mssg = string.Format("获取{0}的{1}的预警参数成功!", pointname, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}的预警参数失败!", pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的{1}的预警参数出错!错误信息:" + ex.Message, pointname, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
        }

        public bool PointAlarmValueTableLoad(string searchstring, data.Model.gtsensortype datatype, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.PointAlarmValueTableLoad(searchstring, datatype, startPageIndex, pageSize, xmno, xmname, colName, sord, out  dt))
                {
                    mssg = string.Format("项目{0}{1}支撑面预警参数加载成功！", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}{1}支撑面预警参数加载失败！", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}{1}支撑面预警参数加载出错，出错信息" + ex.Message, xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
        }

        public bool PointTableLoad(string searchstring,data.Model.gtsensortype datatype, int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.PointTableLoad(searchstring,datatype, startPageIndex, pageSize, xmno, colName, sord, out  dt))
                {
                    mssg = string.Format("项目编号{0}{1}支撑面号表{2}条记录加载成功！", xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype), dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}{1}支撑面号表加载失败！", xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}{1}支撑面号表加载出错,错误信息:" + ex.Message, xmno, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
        }

        public bool PointAlarmValueTableRowsCount(string searchstring, data.Model.gtsensortype datatype, int xmno, out string totalCont, out string mssg)
        {

            totalCont = "";
            try
            {
                if (dal.PointAlarmValueTableRowsCount(searchstring, datatype, xmno, out  totalCont))
                {
                    mssg = string.Format("{0}支撑面号预警参数记录数{1}加载成功！",  data.DAL.gtsensortype.GTSensorTypeToString(datatype), totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}支撑面号预警参数记录数加载失败！", data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}支撑面号预警参数记录数加载出错，出错信息" + ex.Message, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
        }
        #endregion
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

