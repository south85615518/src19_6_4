﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsensordata.cs
*
* 功 能： N/A
* 类 名： gtsensordata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using Tool;
using SqlHelpers;
namespace data.BLL
{
	/// <summary>
	/// gtsensordata
	/// </summary>
	public partial class gtsensordata
	{
        
		private readonly data.DAL.gtsensordata dal=new data.DAL.gtsensordata();
		public gtsensordata()
		{}
		#region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string project_name, string sensorno, string datatype, DateTime time,out string mssg)
        {
            try
            {
                if (dal.Exists(project_name, sensorno, datatype, time))
                {
                    mssg = string.Format("存在项目{0}{1}传感器编号{2}采集时间{3}的数据记录", project_name, datatype, sensorno, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("不存在项目{0}{1}传感器编号{2}采集时间{3}的数据记录", project_name, datatype, sensorno, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}传感器编号{2}采集时间{3}的数据记录出错,错误信息:"+ex.Message, project_name, datatype, sensorno, time);
                return false;
            }
        }
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsensordata model,out string mssg)
		{
             try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目{0}点名{1}测量时间{2}的{3}结果数据添加成功",model.project_name,model.point_name,model.time,model.datatype);
                    return true;
                }
                else {
                    mssg = string.Format("项目{0}点名{1}测量时间{2}的{3}结果数据添加失败", model.project_name, model.point_name, model.time, model.datatype);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("项目{0}点名{1}测量时间{2}的{3}结果数据添加出错，错误信息:" + ex.Message, model.project_name, model.point_name, model.time, model.datatype);
                return false;
            }
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(data.Model.gtsensordata model,out string mssg)
		{

            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("项目{0}点{1}测量时间{2}{3}结果数据更新成功",model.project_name,model.point_name,model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}点{1}测量时间{2}{3}结果数据更新失败", model.project_name, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}点{1}测量时间{2}{3}结果数据更新出错,错误信息:"+ex.Message, model.project_name, model.point_name, model.time);
                return false;
            }



			return dal.Update(model);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool Delete(string project_name,DateTime time,string point_name)
        //{
			
        //    return dal.Delete(project_name,time,point_name);
        //}


        public bool GetLastTimeModel(string xmname, string pointname, Model.gtsensortype datatype, DateTime dt, out data.Model.gtsensordata model,out string mssg)
        {
            model = null;
            try {
                if (dal.GetLastTimeModel(xmname, pointname, datatype, dt, out model))
                {
                    mssg = string.Format("获取项目{0}{1}点{2}{3}前的实体对象成功", xmname, datatype, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}点{2}{3}前的实体对象失败", xmname, datatype, pointname, dt);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}点{2}{3}前的实体对象出错,错误信息："+ex.Message, xmname, datatype, pointname, dt);
                return false;
            }
        }
        public bool GetInitTimeModel(string xmname, string pointname, Model.gtsensortype datatype, out data.Model.gtsensordata model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetInitTimeModel(xmname, pointname, datatype, out model))
                {
                    mssg = string.Format("获取项目编号{0}点{1}初始实体对象成功", xmname, datatype, pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点{1}初始实体对象失败", xmname, datatype, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点{1}初始实体对象出错,错误信息：" + ex.Message, xmname, datatype, pointname);
                return false;
            }
        }

        //根据项目名获取的周期
        //根据项目名获取的周期
        public bool CycTimeList(string xmname, data.Model.gtsensortype datatype, out List<string> cyctimelist, out string mssg)
        {

            cyctimelist = null;
            try
            {

                if (dal.CycTimeList(xmname,datatype ,out cyctimelist))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }
        public bool Delete(string project_name, data.Model.gtsensortype datatype, int startcyc, int endcyc, out string mssg)
        {
            try
            {
                if (dal.Delete(project_name, datatype, startcyc, endcyc))
                {
                    mssg = string.Format("删除项目{0}{1}从{3}到{4}周期的数据成功", project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), startcyc, endcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}{1}从{3}到{4}周期的数据失败", project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}{1}从{3}到{4}周期的数据出错,错误信息:" + ex.Message, project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), startcyc, endcyc);
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteTmp(string project_name, Model.gtsensortype datatype, out string mssg)
        {
            try
            {
                if (dal.DeleteTmp(project_name,datatype))
                {
                    mssg = string.Format("删除项目{0}{1}临时表的数据成功", project_name,datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}{1}临时表的数据失败", project_name, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}{1}临时表的数据出错,错误信息：" + ex.Message, project_name, datatype);
                return false;
            }

        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteTmp(string project_name, out string mssg)
        {
            try
            {
                if (dal.DeleteTmp(project_name))
                {
                    mssg = string.Format("删除项目{0}临时表的数据成功", project_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}临时表的数据失败", project_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}临时表的数据出错,错误信息：" + ex.Message, project_name);
                return false;
            }

        }
        public bool SingleScalarResultdataTableLoad(int startPageIndex, int pageSize, string project_name, string pointname, string sord, Model.gtsensortype datatype, int startcyc, int endcyc, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SingleScalarResultdataTableLoad(startPageIndex, pageSize, project_name, pointname, sord, datatype, startcyc, endcyc, out  dt))
                {
                    mssg = string.Format("{0}结果数据表加载成功!",project_name);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}结果数据表加载失败!", project_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}结果数据表加载出错，出错信息" + ex.Message,project_name);
                return false;
            }
        }

        public bool SingleScalarOrgldataTableLoad(int startPageIndex, int pageSize, string project_name, string pointname, string sord, Model.gtsensortype datatype, int startcyc, int endcyc, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SingleScalarOrgldataTableLoad(startPageIndex, pageSize, project_name, pointname, sord, datatype, startcyc, endcyc, out  dt))
                {
                    mssg = string.Format("{0}原始数据表加载成功!", project_name);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}原始数据表加载失败!", project_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}原始数据表加载出错，出错信息" + ex.Message, project_name);
                return false;
            }
        }


        public bool ScalarResultTableRowsCount(string project_name, string pointname, string sord, Model.gtsensortype datatype, int startcyc, int endcyc, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.ScalarResultTableRowsCount(project_name, pointname, sord, datatype, startcyc, endcyc, out totalCont))
                {
                    mssg = string.Format("{0}结果数据表记录数加载成功!",project_name);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}结果数据表记录数加载成功!", project_name);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}结果数据表加载出错，出错信息" + ex.Message,project_name);
                return false;
            }
        }
        public bool ResultDataReportPrint(string sql, string project_name, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultDataReportPrint(sql, project_name, out dt))
                {
                    mssg = "{0}结果数据报表数据表生成成功";
                    return true;
                }
                else
                {
                    mssg = "{0}结果数据报表数据表生成失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                
                    mssg = "{0}结果数据报表数据表生成出错，错误信息"+ex.Message;
                    return false;
                
            }
        }
        public bool PointNameDateTimeListGet(string xmname, string pointname, data.Model.gtsensortype datatype, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.PointNameDateTimeListGet(xmname, pointname, datatype, out ls))
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的时间列表记录数{3}成功", xmname, pointname, datatype, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的时间列表失败", xmname, pointname, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}点{2}的时间列表出错,错误信息:"+ex.Message, xmname, pointname, datatype);
                return false;
            }


        }
        public bool MaxTime(string project_name,Model.gtsensortype datatype ,out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.MaxTime(project_name,datatype ,out maxtime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据的最大日期{1}成功", project_name, maxtime,datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据的最大日期失败", project_name,datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据的最大日期出错，错误信息:" + ex.Message, project_name,datatype);
                return false;
            }
        }
        public bool MaxTime(string project_name, Model.gtsensortype datatype,string point_name ,out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.MaxTime(project_name, datatype,point_name, out maxtime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据点名{3}的最大日期{1}成功", project_name, maxtime, datatype,point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最大日期失败", project_name, datatype,point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最大日期出错，错误信息:" + ex.Message, project_name, datatype,point_name);
                return false;
            }
        }
        public bool MinTime(string project_name, Model.gtsensortype datatype, string point_name, out DateTime Mintime, out string mssg)
        {
            Mintime = new DateTime();
            try
            {
                if (dal.MinTime(project_name, datatype, point_name, out Mintime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据点名{3}的最小日期{1}成功", project_name, Mintime, datatype, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最小日期失败", project_name, datatype, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最小日期出错，错误信息:" + ex.Message, project_name, datatype, point_name);
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool SingleScalarGetModelList(string project_name, Model.gtsensortype datatype, out List<data.Model.gtsensordata> lt,out string mssg)
        {
            lt = new List<Model.gtsensordata>();
            mssg = "";
            try
            {
                if (dal.SingleScalarGetModelList(project_name,datatype ,out lt))
                {
                    mssg = string.Format("获取项目{0}{1}临时表中的{2}结果数据成功!", project_name, project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype),lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}临时表中的结果数据失败!", project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}临时表中的结果数据出错!错误信息:" + ex.Message, project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
           
        }

        public bool SurfacedataSameTimeModelListGet(string project_name, string surfacename,DateTime dt ,out List<data.Model.gtsensordata> lt, out string mssg)
        {
            lt = new List<Model.gtsensordata>();
            try
            {
                if (dal.SurfacedataSameTimeModelListGet(project_name, surfacename,dt ,out lt))
                {
                    mssg = string.Format("获取项目{0}{1} {2}传感器数据表中的{3}条结果数据成功!", project_name,data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce),dt,lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1} {2}传感器数据表中的结果数据失败!", project_name,data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce),dt);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}  {2}传感器数据表中的结果数据出错!错误信息:" + ex.Message, project_name, data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce),dt);
                return false;
            }

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool ScalarGetModelList(string project_name,  out List<data.Model.gtsensordata> lt, out string mssg)
        {
            lt = new List<Model.gtsensordata>();
            try
            {
                if (dal.ScalarGetModelList(project_name,  out lt))
                {
                    mssg = string.Format("获取项目{0}临时表中的结果数据成功!", project_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}临时表中的结果数据失败!", project_name);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}临时表中的结果数据出错!错误信息:" + ex.Message, project_name);
                return false;
            }

        }




        public bool SingleScalarGetModel(string project_name, string pointname, DateTime dt, Model.gtsensortype datatype, out data.Model.gtsensordata model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.SingleScalarGetModel(project_name, pointname,dt,datatype,out model))
                {
                    mssg = string.Format("获取{0}{1}点{2}的{3}测量数据成功", project_name, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}点{2}的{3}测量数据失败", project_name, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}{1}点{2}的{3}测量数据出错,错误信息:" + ex.Message, project_name, pointname, dt);
                return false;
            }
        }
        public bool dataPointLoadBLL(int xmno, Model.gtsensortype datatype, out List<string> ls, out string mssg)
        {
            ls = null;
            try
            {
                if (dal.dataPointLoadDAL(xmno,datatype, out ls))
                {
                    mssg = string.Format("项目编号{0}{1}点号加载成功",xmno,datatype);
                    return true;

                }
                else
                {
                    mssg = string.Format("项目编号{0}{1}点号加载失败", xmno,datatype);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}{1}点号加载出错，错误信息" + ex.Message,xmno,datatype);
                return false;
            }

        }
        public bool PointNewestDateTimeGet(string project_name, string pointname, Model.gtsensortype datatype, out DateTime dt,out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(project_name, pointname,datatype ,out dt))
                {
                    mssg = string.Format("获取项目{0}{3}{1}点的最新数据的测量时间{2}成功", project_name, pointname, dt,datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{2}{1}点的最新数据的测量时间失败", project_name, pointname, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{2}{1}点的最新数据的测量时间出错，错误信息:" + ex.Message, project_name, pointname, datatype);
                return false;
            }

        }
        public bool XmStateTable(int startPageIndex, int pageSize, string project_name,string xmname, string unitname, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.XmStateTable(startPageIndex, pageSize, project_name, xmname, unitname, colName, sord, out  dt))
                {
                    mssg = string.Format("获取编号为{0}项目名{1}的{3}项目状态表记录数{2}条成功", project_name, xmname, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取编号为{0}项目名{1}的{3}项目状态表失败", project_name, xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取编号为{0}项目名{1}的{3}项目状态表出错,错误信息:"+ex.Message, project_name, xmname);
                return false;
            }
        }

        public bool ReportDataView(List<string> cyclist, int startPageIndex, int pageSize, string xmname,data.Model.gtsensortype datatype ,string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.ReportDataView(cyclist, startPageIndex, pageSize, xmname,datatype ,sord, out  dt))
                {
                    mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表成功", xmname,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,string.Join("、", cyclist));
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表失败", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), string.Join("、", cyclist));
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}以{2}生成的周期报表出错，错误信息:" + ex.Message, xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), string.Join("、", cyclist));
                return false;
            }
        }

        public bool OnLinePointCont(string unitname,List<string> finishedxmnolist,out string contpercent, out string mssg)
        {
            mssg = "";
            contpercent = "";
            try
            {
                if (dal.OnLinePointCont(unitname,finishedxmnolist,out contpercent))
                {
                    mssg = string.Format("成功获取在线点情况为{0}", contpercent);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取在线点情况失败", contpercent);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取在线点情况出错,错误信息:" + ex.Message, contpercent);
                return false;
            }
        }
        public bool SiblingDataCycGet(string xmname, DateTime dt, data.Model.gtsensortype datatype, List<string> points, double mobileStationInteval, out  int cyc, out string mssg)
        {
            mssg = "";
            cyc = -1;
            try
            {
                if (dal.SiblingDataCycGet(xmname, dt,datatype ,points, mobileStationInteval, out cyc))
                {
                    mssg = string.Format("获取项目编号{0}{1}的接入周期为{2}", xmname,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}的接入周期失败", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}的接入周期出错,错误信息:" + ex.Message, xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
        }
        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(string xmname, DateTime dt, data.Model.gtsensortype datatype, out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.IsInsertData(xmname, dt,datatype))
                {
                    mssg = string.Format("经查库发现项目{0}{1}采集的{2}数据是接段数据", xmname, dt, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return true;
                }
                else
                {
                    mssg = string.Format("经查库发现项目{0}{1}采集的{2}数据是最新数据", xmname, dt, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}采集的{2}数据的性质出错，错误信息：" + ex.Message, xmname, dt, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
        }
        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(string xmname, DateTime dt,data.Model.gtsensortype datatype ,out int cyc, out string mssg)
        {
            mssg = "";
            cyc = -1;
            try
            {
                if (dal.InsertDataCycGet(xmname, dt, datatype,out cyc))
                {
                    mssg = string.Format("获取项目编号{0}{1}的{2}段接周期为{3}", xmname,dt,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}的{2}段接周期失败", xmname, dt, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}的{2}段接周期出错,错误信息:" + ex.Message, xmname, dt, data.DAL.gtsensortype.GTSensorTypeToString(datatype));
                return false;
            }
        }
        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool InsertCycStep(string xmname, int startcyc,data.Model.gtsensortype datatype ,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.InsertCycStep(xmname,datatype ,startcyc))
                {
                    mssg = string.Format("项目{0}{1}周期{2}后的所有周期后移成功", xmname,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,startcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}{1}周期{2}后的所有周期后移失败", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), startcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}{1}周期{2}后的所有周期后移出错,错误信息:" + ex.Message, xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), startcyc);
                return false;
            }
        }
        //根据项目名称获取端点周期
        public bool ExtremelyCycGet(string xmname,data.Model.gtsensortype datatype ,string DateFunction, out string mssg, out string ExtremelyCyc)
        {
            ExtremelyCyc = null;
            try
            {

                if (dal.ExtremelyCycGet(xmname, datatype,DateFunction, out ExtremelyCyc))
                {
                    mssg = "端点周期获取成功";
                    return true;
                }
                else
                {
                    mssg = "端点周期获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "端点周期获取出错，错误信息：" + ex.Message;
                return false;
            }

        }

        /// <summary>
        /// 判断记录是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool IsCycdirnetDataExist(string xmname, string pointname, DateTime dt,data.Model.gtsensortype datatype ,out string mssg)
        {
            try
            {
                if (dal.IsCycdirnetDataExist(xmname, pointname, dt,datatype))
                {
                    mssg = string.Format("项目{0}{1}点名{2}采集时间{3}的记录已经存在", xmname, pointname,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}{1}不存在点名{2}采集时间{3}的记录", xmname, pointname,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}点名{2}采集时间{3}的记录出错,错误信息:" + ex.Message, xmname, pointname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), dt);
                return false;
            }

        }

        ///// <summary>
        ///// 是否插入数据
        ///// </summary>
        ///// <returns></returns>
        //public bool IsInsertData(string xmname, DateTime dt)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        ///// <summary>
        ///// 插入数据的周期生成
        ///// </summary>
        ///// <returns></returns>
        //public bool InsertDataCycGet(string xmname, DateTime dt, out  int cyc)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        ////根据项目名称获取端点周期
        //public bool ExtremelyCycGet(string xmname, string DateFunction, out string ExtremelyCyc)
        //{

        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(string xmname, int cyc,data.Model.gtsensortype datatype ,out string mssg)
        {
            try
            {
                if (dal.IsInsertCycExist(xmname, cyc,datatype))
                {
                    mssg = string.Format("项目{0}{1}已经存在周期{2}", xmname,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}{1}不存在周期{2}", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}周期{2}的存在与否出错,错误信息:" + ex.Message, xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), cyc);
                return false;
            }
        }



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

