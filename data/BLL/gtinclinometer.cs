﻿/**  版本信息模板在安装目录下，可自行修改。
* gtinclinometer.cs
*
* 功 能： N/A
* 类 名： gtinclinometer
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 8:51:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using data.Model;
namespace data.BLL
{
	/// <summary>
	/// gtinclinometer
	/// </summary>
	public partial class gtinclinometer
	{
		private readonly data.DAL.gtinclinometer dal=new data.DAL.gtinclinometer();
		public gtinclinometer()
		{}
		#region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtinclinometer model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}测斜孔名{1}深度{2}采集时间{3}数据记录录入成功", model.xmno, model.holename, model.deepth,model.time );
                    return true;
                }
                else {
                    mssg = string.Format("项目编号{0}测斜孔名{1}深度{2}采集时间{3}数据记录录入失败", model.xmno, model.holename, model.deepth, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}测斜孔名{1}深度{2}采集时间{3}数据记录录入出错,错误信息:" + ex.Message, model.xmno, model.holename, model.deepth, model.time);
                return false;
            }
        }
        ///// <summary>
        ///// 更新一条数据
        ///// </summary>
        //public bool Update(data.Model.gtinclinometer model)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("update gtinclinometer set ");
        //    strSql.Append("f=@f,");
        //    strSql.Append("disp=@disp");
        //    strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@f", OdbcType.Double),
        //            new OdbcParameter("@disp", OdbcType.Double),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@point_name", OdbcType.VarChar,100),
        //            new OdbcParameter("@time", OdbcType.DateTime)};
        //    parameters[0].Value = model.f;
        //    parameters[1].Value = model.disp;
        //    parameters[2].Value = model.xmno;
        //    parameters[3].Value = model.holename;
        //    parameters[4].Value = model.time;

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(data.Model.gtinclinometer model, out string mssg)
        {

            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("删除项目编号{0}点名{1}深度{2}测量时间{3}数据成功", model.xmno, model.holename, model.deepth, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}点名{1}深度{2}测量时间{3}数据失败", model.xmno, model.holename, model.deepth, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}点名{1}深度{2}测量时间{3}数据出错,错误信息:" + ex.Message, model.xmno, model.holename, model.deepth, model.time);
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string point_name,double deepth ,DateTime time, out data.Model.gtinclinometer model,out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(xmno, point_name,deepth ,time, out model))
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象成功", xmno, point_name, model.deepth, time);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象失败", xmno, point_name, model.deepth, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象出错,错误信息:" + ex.Message, xmno, point_name, model.deepth, time);
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetLastTimeModel(int xmno, string point_name, double deepth, DateTime time, out data.Model.gtinclinometer model, out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetLastTimeModel(xmno, point_name, deepth, time, out model))
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象成功", xmno, point_name, model.deepth, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象失败", xmno, point_name, model.deepth, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象出错,错误信息:" + ex.Message, xmno, point_name, model.deepth, time);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetSumAcDiff(int xmno, string point_name, double deepth, DateTime time, out data.Model.gtinclinometer model, out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetLastTimeModel(xmno, point_name, deepth, time, out model))
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据的实体对象成功", xmno, point_name, model.deepth, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象失败", xmno, point_name, model.deepth, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜孔名{1}深度{2}采集时间{3}的数据实体对象出错,错误信息:" + ex.Message, xmno, point_name, model.deepth, time);
                return false;
            }
        }

        /// <summary>
        /// 删除临时表数据
        /// </summary>
        public bool Delete_tmp(int xmno, out int cont,out string mssg)
        {
            cont = 0;
            mssg = "";
            try
            {
                if (dal.Delete_tmp(xmno, out cont))
                {
                    mssg = string.Format("删除项目编号{0}固定测斜段临时表数据{1}条成功", xmno, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}固定测斜段临时表数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}固定测斜段临时表数据出错,错误信息:"+ex.Message, xmno);
                return false;
            }
            
        }


        /// <summary>
        /// 得到一个测斜段数据对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, out List<data.Model.gtinclinometer> lt,out string mssg)
        {
            lt = null;
            mssg = "";
            try
            {
                if (dal.GetModelList(xmno, out lt))
                {
                    mssg = string.Format("成功获取项目编号{0}测斜采集数据对象{1}条", xmno, lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("成功获取项目编号{0}测斜采集数据对象列表失败", xmno, lt.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("成功获取项目编号{0}测斜采集数据对象列表出错,错误信息:"+ex.Message, xmno, lt.Count);
                return false;
            }
        }



        public bool PointMaxDateTimeGet(int xmno,  out DateTime dt,out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointMaxDateTimeGet(xmno, out dt))
                {
                    mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间为{1}", xmno, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间失败", xmno);
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间出错，错误信息:"+ex.Message, xmno);
                return false;
            }

        }



       
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

