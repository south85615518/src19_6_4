﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsensordata.cs
*
* 功 能： N/A
* 类 名： gtsensordata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/31 15:15:12   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace data.BLL
{
	/// <summary>
	/// gtsensordata
	/// </summary>
	public partial class gtsensordata
	{
		private readonly data.DAL.gtsensordata dal=new data.DAL.gtsensordata();
		public gtsensordata()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string project_name,string point_name,string datatype,DateTime time)
		{
			return dal.Exists(project_name,point_name,datatype,time);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsensordata model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目{0}点名{1}采集时间{2}{3}数据成功", model.project_name, model.point_name, model.time, model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目{0}点名{1}采集时间{2}{3}数据失败", model.project_name, model.point_name, model.time, model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目{0}点名{1}采集时间{2}{3}数据出错,错误信息:"+ex.Message, model.project_name, model.point_name, model.time, model.datatype);
                return false;
            }

		}

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtsensordata model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("更新项目{0}点名{1}采集时间{2}{3}数据成功", model.project_name, model.point_name, model.time, model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目{0}点名{1}采集时间{2}{3}数据失败", model.project_name, model.point_name, model.time, model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目{0}点名{1}采集时间{2}{3}数据出错,错误信息:" + ex.Message, model.project_name, model.point_name, model.time, model.datatype);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string project_name, string point_name, string datatype, DateTime time)
        {

           
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public data.Model.gtsensordata GetModel(string project_name, string point_name, string datatype, DateTime time)
        {

           
        }



        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string project_name, DateTime Time, string POINT_NAME)
        {

           
        }

        public bool DeleteTmp(string project_name, Model.gtsensortype datatype)
        {
            
        }

        public bool SingleScalarResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype, out DataTable dt)
        {
           
        }

        public bool SingleScalarResultTableRowsCount(string project_name, string pointname, string sord, Model.gtsensortype datatype, out string totalCont)
        {
           
        }
        public bool ResultDataReportPrint(string sql, string project_name, out DataTable dt)
        {
           
        }
        /// <summary>
        /// 单量结果数据得到一个对象实体
        /// </summary>
        public bool SingleScalarGetModelList(string project_name, Model.gtsensortype datatype, out List<data.Model.gtsensordata> lt)
        {
           
        }
       
        public bool SingleScalarGetModel(string project_name, string pointname, DateTime dt, Model.gtsensortype datatype, out data.Model.gtsensordata model)
        {
           
        }

 


        public bool MaxTime(string project_name, Model.gtsensortype datatype, out DateTime maxTime)
        {
           
        }
        public bool dataPointLoadDAL(int xmno, Model.gtsensortype datatype, out List<string> ls)
        {

           

        }
        public bool PointNewestDateTimeGet(string project_name, string pointname, Model.gtsensortype datatype, out DateTime dt)
        {
           

        }
        public bool XmStateTable(int startPageIndex, int pageSize, string project_name, string xmname, string unitname, string colName, string sord, out DataTable dt)
        {
           
        }
        public bool IsgtsensordataDataExist(string project_name, string pointname, DateTime dt)
        {
           
        }
		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

