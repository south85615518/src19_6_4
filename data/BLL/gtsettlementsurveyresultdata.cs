﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace data.BLL
{
    public partial class gtsettlementresultdata
    {
        public bool AddSurveyData(int xmno, string point_name, DateTime starttime, DateTime endtime, out string mssg)
        {
            try
            {
                if (dal.AddData(xmno, point_name, starttime, endtime))
                {
                    mssg = string.Format("添加项目编号{0}点名{1}从{2}到{3}之间的数据到编辑库成功", xmno,  point_name, starttime, endtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}点名{1}从{2}到{3}之间的数据到编辑库失败", xmno,  point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}点名{1}从{2}到{3}之间的数据到编辑库出错,错误信息:" + ex.Message, xmno,  point_name, starttime, endtime);
                return false;
            }
        }

        public bool SurveyPointTimeDataAdd(int xmno, string pointname, DateTime time, DateTime importtime, out string mssg)
        {
            try
            {
                if (dal.SurveyPointTimeDataAdd(xmno, pointname,  time, importtime))
                {
                    mssg = string.Format("将项目编号{0}点{1}时间{2}的数据添加成编辑库{3}的数据成功", xmno,  pointname, time, importtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("将项目编号{0}点{1}时间{2}的数据添加成编辑库{3}的数据失败", xmno,  pointname, time, importtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("将项目编号{0}点{1}时间{2}的数据添加成编辑库{3}的数据出错,错误信息:" + ex.Message, xmno, pointname, time, importtime);
                return false;
            }
        }

        public bool UpdataSurveyData(int xmno, string pointname, DateTime time, string vSet_name, string vLink_name, DateTime srcdatetime, out string mssg)
        {
            try
            {
                if (dal.UpdataSurveyData(xmno, pointname, time,  vSet_name, vLink_name, srcdatetime))
                {
                    mssg = string.Format("设置项目编号{0}点{1}日期{2}的{3}{4}日期{2}的值成功", xmno, pointname, time, vSet_name, vLink_name, srcdatetime);
                    return true;
                }
                else
                {
                    mssg = string.Format("设置项目编号{0}点{1}日期{2}{3}{4}日期{2}的值失败", xmno, pointname, time, vSet_name, vLink_name, srcdatetime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("设置项目编号{0}点{1}日期{2}{3}为{4}日期{2}的值出错,错误信息:" + ex.Message, xmno, pointname, time, vSet_name, vLink_name, srcdatetime);
                return false;
            }
        }
        public bool DeleteSurveyData(int xmno, string point_name, DateTime starttime, DateTime endtime, out string mssg)
        {
            try
            {
                if (dal.DeleteSurveyData(xmno, point_name,starttime, endtime))
                {
                    mssg = string.Format("删除项目编号编辑库{0}点名{1}从{2}到{3}的数据成功", xmno,  point_name, starttime, endtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号编辑库{0}点名{1}从{2}到{3}的数据失败", xmno,  point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号编辑库{0}点名{1}从{2}到{3}的数据出错,错误信息：" + ex.Message, xmno,  point_name, starttime, endtime);
                return false;
            }
        }
        public bool PointNameSurveyDateTimeListGet(int xmno, string pointname, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.PointNameSurveyDateTimeListGet(xmno, pointname,  out ls))
                {
                    mssg = string.Format("获取到项目编号{0}点{1}编辑库的时间列表记录数{2}成功", xmno, pointname,  ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}点{1}编辑库的时间列表失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}点{1}编辑库的时间列表出错,错误信息:" + ex.Message, xmno, pointname);
                return false;
            }


        }

        public bool SettlementSurveyDATATableLoad(DateTime starttime, DateTime endtime, int startPageIndex, int pageSize, int xmno, string pointname, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SettlementSurveyDATATableLoad(starttime, endtime, startPageIndex, pageSize, xmno, pointname, sord, out  dt))
                {
                    mssg = "沉降编辑库结果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "沉降编辑库结果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "沉降编辑库结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool SettlementSurveyDATATableRowsCount(DateTime starttime, DateTime endtime, int xmno, string pointname, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.SettlementSurveyTableRowsCount(starttime, endtime, xmno, pointname, out totalCont))
                {
                    mssg = "沉降编辑库结果数据表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "沉降编辑库结果数据表记录数加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "编辑库结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }
    }
}
