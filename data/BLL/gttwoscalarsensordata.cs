﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsensordata.cs
*
* 功 能： N/A
* 类 名： gtsensordata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/31 15:15:12   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace data.BLL
{
	/// <summary>
	/// gtsensordata
	/// </summary>
	public partial class gtsensordata
	{
		#region  BasicMethod
        public bool TwoScalarResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype, DateTime startTime, DateTime endTime, out DataTable dt, out string mssg)
        {
            dt = new DataTable();
            mssg = "";
            try
            {
                if (dal.TwoScalarResultdataTableLoad(startPageIndex, pageSize, xmname, pointname, sord, datatype,  startTime,  endTime, out dt))
                {
                    mssg = string.Format("成功获取到双量结果数据表记录数{0}条", dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到双量结果数据表记录失败", dt.Rows.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到双量结果数据表记录出错,错误信息:"+ex.Message, dt.Rows.Count);
                return false;
            }

        }

        //public bool TwoScalarResultTableRowsCount(string project_name, string pointname, string sord, Model.gtsensortype datatype, out string totalCont,out string mssg)
        //{
        //    totalCont = "0";
        //    try
        //    {
        //        if (dal.TwoScalarResultTableRowsCount( project_name,  pointname,  sord, datatype, out  totalCont))
        //        {
        //            mssg = string.Format("成功获取到双量结果数据表记录数{0}条", totalCont);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("获取到双量结果数据表记录失败");
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("获取到双量结果数据表记录出错,错误信息:" + ex.Message);
        //        return false;
        //    }
          
        //}

        /// <summary>
        /// 双量结果数据得到一个对象实体
        /// </summary>
        public bool TwoScalarGetModelList(string project_name, Model.gtsensortype datatype, out List<data.Model.gtsensordata> lt,out string mssg)
        {
            mssg = "";
            lt = null;
            try
            {
                if (dal.TwoScalarGetModelList(project_name, datatype, out lt))
                {
                    mssg = string.Format("获取到项目{0}{1}的双量结果数据对象列表{2}条", project_name, datatype, lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}的双量结果数据对象列表{2}条", project_name, datatype, lt.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}的双量结果数据对象列表出错，错误信息:"+ex.Message, project_name, datatype, lt.Count);
                return false;
            }

        }



        public bool TwoScalarGetModel(string project_name, string pointname, DateTime dt, Model.gtsensortype datatype, out data.Model.gtsensordata model,out string mssg)
        {

            mssg = "";
            model = null;
            try
            {
                if (dal.TwoScalarGetModel(project_name,pointname,dt,datatype, out model))
                {
                    mssg = string.Format("成功获取到项目{0}{1}的双量结果数据对象", project_name, datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}的双量结果数据对象", project_name, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}的双量结果数据对象列表出错，错误信息:" + ex.Message, project_name, datatype);
                return false;
            }


        }

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

