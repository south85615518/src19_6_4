﻿/**  版本信息模板在安装目录下，可自行修改。
* gtpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:28   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace data.BLL
{
    /// <summary>
    /// 数据访问类:gtpointalarmvalue
    /// </summary>
    public partial class gtpointalarmvalue
    {
        public data.DAL.gtpointalarmvalue dal = new data.DAL.gtpointalarmvalue();

        public gtpointalarmvalue()
        { }
        #region  BasicMethod
        //判断点名对象是否存在
        public bool Exist(data.Model.gtpointalarmvalue model,out string mssg)
        {

            try
            {
                if (dal.Exist(model))
                {
                    mssg = string.Format("项目编号{0}{2}{1}点已经存在", model.xmno, model.pointname,model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}{2}{1}点不存在", model.xmno, model.pointname, model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}{2}{1}点查询出错,错误信息:" + ex.Message,model.xmno,model.pointname,model.datatype);
                return true;
            }

        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtpointalarmvalue model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("{0}预警点新增成功",model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}预警点新增失败", model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg =string.Format("{0}预警点新增出错，错误信息" + ex.Message,model.datatype);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtpointalarmvalue model,out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("{1}预警点{0}更新成功", model.pointname,model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("{1}预警点{0}更新失败", model.pointname,model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}预警点更新出错，错误信息" + ex.Message,model.datatype);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateInitDegree(data.Model.gtpointalarmvalue model, out string mssg)
        {
            try
            {
                if (dal.UpdateInitDegree(model))
                {
                    mssg = string.Format("{1}预警点{0}初始温度更新成功", model.pointname, model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("{1}预警点{0}初始温度更新失败", model.pointname, model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}预警点初始温度更新出错，错误信息" + ex.Message, model.datatype);
                return false;
            }
        }



        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, data.Model.gtpointalarmvalue model,out string mssg)
        {
           try
            {
                if (dal.MultiUpdate(pointNameStr, model))
                {
                    mssg = string.Format("{0}预警点批量更新成功",pointNameStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}预警点批量更新失败", pointNameStr);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}预警点批量更新出错，错误信息" + ex.Message,model.datatype);
                return false;
            }
        }

        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool SurfaceMultiUpdate(string pointNameStr, data.Model.gtpointalarmvalue model,out string mssg)
        {
            try
            {
                if (dal.SurfaceMultiUpdate(pointNameStr, model))
                {
                    mssg = string.Format("将点{0}关联到面{1}成功", pointNameStr, model.surfaceName);
                    return true;
                }
                else
                {
                    mssg = string.Format("将点{0}关联到面{1}失败", pointNameStr, model.surfaceName);
                    return false;
                } 
                
            }
            catch (Exception ex)
            {
                mssg = string.Format("将点{0}关联到面{1}出错,错误信息："+ex.Message, pointNameStr, model.surfaceName);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// 
        public bool Delete(data.Model.gtpointalarmvalue model,out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("{0}预警点删除成功",model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}预警点删除失败",model.datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}预警点删除出错，错误信息" + ex.Message, model.datatype);
                return false;
            }
        }

        public bool GTSensorDataPointLoadDAL(int xmno, Model.gtsensortype datatype, out List<string> ls,out string mssg)
        {
            mssg = "";
            ls = new List<string>();
            try
            {
                if (dal.GTSensorDataPointLoadDAL(xmno, datatype, out ls))
                {
                    mssg = string.Format("获取到项目编号{0}{1}的测点数{2}个", xmno, datatype, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}{1}的测点失败", xmno, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}{1}的测点出错,错误信息:"+ex.Message, xmno, datatype);
                return false;
            }

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname,data.Model.gtsensortype datatype ,out data.Model.gtpointalarmvalue model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, datatype, out model))
                {
                    mssg = string.Format("获取{0}的{1}{2}的预警参数成功!",xmno ,pointname,model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}的预警参数失败!",xmno ,pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的{1}{2}的预警参数出错!错误信息:" + ex.Message,xmno ,pointname,model.datatype);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno,  data.Model.gtsensortype datatype, out List<data.Model.gtpointalarmvalue> modellist, out string mssg)
        {
            modellist = null;
            try
            {
                if (dal.GetModelist(xmno, datatype, out modellist))
                {
                    mssg = string.Format("获取{0}的{1}的预警参数列表{2}条成功!", xmno,  datatype,modellist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}的预警参数失败!", xmno, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的{1}的预警参数出错!错误信息:" + ex.Message, xmno, datatype);
                return false;
            }
        }





        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetSensornoModel(int xmno, string sensorno, out data.Model.gtpointalarmvalue model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetSensornoModel(xmno, sensorno, out model))
                {
                    mssg = string.Format("获取{0}的{1}的预警参数成功!", sensorno, model.datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}的预警参数失败!", sensorno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的{1}的预警参数出错!错误信息:" + ex.Message, sensorno, model.datatype);
                return false;
            }
        }

        public bool PointAlarmValueTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, Model.gtsensortype datatype, out DataTable dt, out string mssg)
        {
           dt = null;
            try
            {
                if (dal.PointAlarmValueTableLoad(searchstring, startPageIndex, pageSize, xmno,xmname,colName, sord , datatype , out  dt))
                {
                    mssg = string.Format("项目{0}{1}点号预警参数加载成功！",xmname,datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}{1}点号预警参数加载失败！",xmname,datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}{1}点号预警参数加载出错，出错信息" + ex.Message,xmname,datatype);
                return false;
            }
        }

        public bool PointTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string colName, string sord, Model.gtsensortype datatype, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.PointTableLoad(searchstring, startPageIndex, pageSize, xmno,colName,sord,datatype, out  dt))
                {
                    mssg = string.Format("项目编号{0}{2}点号表{1}条记录加载成功！", xmno, dt.Rows.Count,datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}{1}点号表加载失败！", xmno,datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}{1}点号表加载出错,错误信息:" + ex.Message, xmno,datatype);
                return false;
            }
        }

        public bool PointGTWaterLineTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string colName, string sord, Model.gtsensortype datatype, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.PointGTWaterLineTableLoad(searchstring, startPageIndex, pageSize, xmno, colName, sord, datatype, out  dt))
                {
                    mssg = string.Format("项目编号{0}{2}水位点号表{1}条记录加载成功！", xmno, dt.Rows.Count, datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}{1}水位点号表加载失败！", xmno, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}{1}水位点号表加载出错,错误信息:" + ex.Message, xmno, datatype);
                return false;
            }
        }

        public bool PointGTSupportingAxialForceLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string colName, string sord, Model.gtsensortype datatype, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.PointGTSupportingAxialForceLoad(searchstring, startPageIndex, pageSize, xmno, colName, sord, datatype, out  dt))
                {
                    mssg = string.Format("项目编号{0}{2}支撑轴力点号表{1}条记录加载成功！", xmno, dt.Rows.Count, datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}{1}支撑轴力点号表加载失败！", xmno, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}{1}支撑轴力点号表加载出错,错误信息:" + ex.Message, xmno, datatype);
                return false;
            }
        }

        public bool PointAlarmValueTableRowsCount(string searchstring, int xmno,Model.gtsensortype datatype ,out string totalCont,out string mssg)
        {

           totalCont = "";
            try
            {
                if (dal.PointAlarmValueTableRowsCount(searchstring, xmno, datatype, out  totalCont))
                {
                    mssg = string.Format("{1}点号预警参数记录数{0}加载成功！", totalCont,datatype);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}点号预警参数记录数加载失败！", datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}点号预警参数记录数加载出错，出错信息" + ex.Message, datatype);
                return false;
            }
        }
        #endregion  BasicMethod
    }
}

