﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace data.BLL
{
    public partial class gtsettlementresultdata
    {
        public bool CgMaxTime(int xmno,  string point_name, out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.CgMaxTime(xmno,  point_name, out maxtime))
                {
                    mssg = string.Format("获取项目编号{0}测量数据点名{2}的最大日期{1}成功", xmno, maxtime,point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测量数据点名{1}的最大日期失败", xmno,  point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测量数据点名{1}的最大日期出错，错误信息:" + ex.Message, xmno, point_name);
                return false;
            }
        }
        public bool CgMinTime(int xmno,string point_name, out DateTime Mintime, out string mssg)
        {
            Mintime = new DateTime();
            try
            {
                if (dal.CgMinTime(xmno,  point_name, out Mintime))
                {
                    mssg = string.Format("获取项目编号{0}测量数据点名{2}的最小日期{1}成功", xmno, Mintime,  point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测量数据点名{1}的最小日期失败", xmno,  point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测量数据点名{1}的最小日期出错，错误信息:" + ex.Message, xmno,  point_name);
                return false;
            }
        }

        public bool CgPointNameDateTimeListGet(int xmno, string pointname, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.CgPointNameDateTimeListGet(xmno, pointname,  out ls))
                {
                    mssg = string.Format("获取到项目编号{0}点{1}的成果数据时间列表记录数{2}成功", xmno, pointname,  ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}点{1}的成果数据时间列表失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}点{1}的成果数据时间列表出错,错误信息:" + ex.Message, xmno, pointname);
                return false;
            }


        }

        public bool AddCgData(int xmno, string point_name, DateTime starttime, DateTime endtime,out string mssg)
        {
            mssg = "";
            int rows = 0;
            try
            {
                if (dal.AddCgData(xmno, point_name,  starttime, endtime, out rows))
                {
                    mssg = string.Format("添加项目编号{0}点名{1}从{2}-{3}的数据{4}条成功", xmno,  point_name, starttime, endtime, rows);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}点名{1}从{2}-{3}的数据失败", xmno,  point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}点名{1}从{2}-{3}的数据出错,错误信息:" + ex.Message, xmno,  point_name, starttime, endtime);
                return false;
            }
        }
        /// <summary>
        /// 删除成果数据
        /// </summary>
        public bool DeleteCg(int xmno, string point_name, DateTime starttime, DateTime endtime,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.DeleteCg(xmno, point_name,starttime, endtime))
                {
                    mssg = string.Format("删除项目编号{0}点名{1}从{2}-{3}的数据成功", xmno, point_name, starttime, endtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}点名{1}从{2}-{3}的数据失败", xmno, point_name, starttime, endtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}点名{1}从{2}-{3}的数据出错,错误信息:" + ex.Message, xmno,  point_name, starttime, endtime);
                return false;
            }
        }
        public bool DeleteCgTmp(int xmno,out string mssg)
        {
            try
            {
                if (dal.DeleteCgTmp(xmno))
                {
                    mssg = string.Format("删除项目编号{0}{1}成果临时表的数据成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}{1}成果临时表的数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}{1}成果临时表的数据出错,错误信息：" + ex.Message, xmno);
                return false;
            }
            
        }
        public bool CgResultdataTableLoad(int startPageIndex, int pageSize, int xmno, string pointname, string sord,  DateTime startTime, DateTime endTime, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.CgResultdataTableLoad(startPageIndex, pageSize, xmno, pointname, sord,startTime, endTime, out  dt))
                {
                    mssg = string.Format("{0}成果数据表加载成功!", xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}成果数据表加载失败!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}成果数据表加载出错，出错信息" + ex.Message, xmno);
                return false;
            }
        }

        public bool CgResultTableRowsCount(int xmno, string pointname, string sord,  DateTime startTime, DateTime endTime, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.CgResultTableRowsCount(xmno, pointname, sord,  startTime, endTime, out totalCont))
                {
                    mssg = string.Format("{0}成果数据表记录数加载成功!", xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}成果数据表记录数加载成功!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}成果数据表加载出错，出错信息" + ex.Message, xmno);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgGetModelList(int xmno, out List<data.Model.gtsettlementresultdata> lt,out string mssg)
        {
            lt = new List<data.Model.gtsettlementresultdata>();
            try
            {
                if (dal.CgGetModelList(xmno, out lt))
                {
                    mssg = string.Format("获取项目编号{0}成果临时表中的结果数据成功!", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}成果临时表中的结果数据失败!", xmno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}成果临时表中的结果数据出错!错误信息:" + ex.Message, xmno);
                return false;
            }
        }

    }
}
