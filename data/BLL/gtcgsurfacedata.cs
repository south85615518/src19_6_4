﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsurfacedata.cs
*
* 功 能： N/A
* 类 名： gtsurfacedata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using Tool;
using SqlHelpers;
using System.Text;
namespace data.BLL
{
	/// <summary>
	/// gtsurfacedata
	/// </summary>
    public partial class gtsurfacedata
	{

        public bool CgMaxTime(int xmno, data.Model.gtsensortype datatype, string point_name, out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.CgMaxTime(xmno, datatype, point_name, out maxtime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据点名{3}的最大日期{1}成功", xmno, maxtime, "", point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最大日期失败", xmno, "", point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最大日期出错，错误信息:" + ex.Message, xmno, "", point_name);
                return false;
            }
        }
        public bool CgMinTime(int xmno, data.Model.gtsensortype datatype, string point_name, out DateTime Mintime, out string mssg)
        {
            Mintime = new DateTime();
            try
            {
                if (dal.CgMinTime(xmno, datatype, point_name, out Mintime))
                {
                    mssg = string.Format("获取项目{0}{2}测量数据点名{3}的最小日期{1}成功", xmno, Mintime, "", point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最小日期失败", xmno, "", point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}测量数据点名{2}的最小日期出错，错误信息:" + ex.Message, xmno, "", point_name);
                return false;
            }
        }

        public bool CgPointNameDateTimeListGet(int xmno, data.Model.gtsensortype datatype, string pointname, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.CgPointNameDateTimeListGet(xmno, datatype, pointname, out ls))
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表记录数{3}成功", xmno, pointname, "", ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表失败", xmno, pointname, "");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}点{2}的成果数据时间列表出错,错误信息:" + ex.Message, xmno, pointname, "");
                return false;
            }


        }


        public bool AddCgData(int xmno, data.Model.gtsensortype datatype, int cyc, int importcyc, out string mssg)
        {
            mssg = "";
            int rows = 0;
            try
            {
                if (dal.AddCgData(xmno, datatype, cyc, importcyc, out rows))
                {
                    mssg = string.Format("添加项目{0}混凝土支撑内力测量库中第{1}周期的数据为成果库第{2}周期{3}条成功", xmno, importcyc, cyc, rows);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目{0}混凝土支撑内力测量库中第{1}周期的数据为成果库第{2}周期失败", xmno,  importcyc, cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目{0}混凝土支撑内力测量库中第{1}周期的数据为成果库第{2}周期出错,错误信息:" + ex.Message, xmno,  importcyc, cyc);
                return false;
            }
        }

       
        /// <summary>
        /// 删除成果数据
        /// </summary>
        public bool DeleteCg(int xmno, data.Model.gtsensortype datatype, int startcyc, int endcyc, out string mssg)
        {
            try
            {
                if (dal.DeleteCg(xmno, datatype, startcyc, endcyc))
                {
                    mssg = string.Format("删除项目成果库{0}{1}从{2}到{3}周期的数据成功", xmno, "混凝土支撑内力", startcyc, endcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目成果库{0}{1}从{2}到{3}周期的数据失败", xmno, "混凝土支撑内力", startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目成果库{0}{1}从{2}到{3}周期的数据出错,错误信息:" + ex.Message, xmno, "混凝土支撑内力", startcyc, endcyc);
                return false;
            }
        }
        public bool DeleteCgTmp(int xmno, data.Model.gtsensortype datatype, out string mssg)
        {
            try
            {
                if (dal.DeleteCgTmp(xmno,datatype))
                {
                    mssg = string.Format("删除项目{0}{3}成果临时表的数据成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}{3}成果临时表的数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}{3}成果临时表的数据出错,错误信息：" + ex.Message, xmno);
                return false;
            }

        }
        public bool CgSurfaceDataResultdataTableLoad(int startPageIndex, int pageSize, int xmno, data.Model.gtsensortype datatype, string pointname, string sord, int startcyc, int endcyc, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.CgResultdataTableLoad(startPageIndex, pageSize, xmno, datatype, pointname, sord, startcyc, endcyc, out  dt))
                {
                    mssg = string.Format("{0}成果数据表加载成功!", xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}成果数据表加载失败!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}成果数据表加载出错，出错信息" + ex.Message, xmno);
                return false;
            }
        }

        public bool CgScalarResultTableRowsCount(int xmno, data.Model.gtsensortype datatype, string pointname, string sord, int startcyc, int endcyc, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.CgResultTableRowsCount(xmno, datatype, pointname, endcyc, endcyc, out totalCont))
                {
                    mssg = string.Format("{0}成果数据表记录数加载成功!", xmno);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}成果数据表记录数加载成功!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}成果数据表加载出错，出错信息" + ex.Message, xmno);
                return false;
            }
        }

        //根据项目名获取的周期
        public bool CgCYCDateTimeListGet(int xmno, data.Model.gtsensortype datatype, out List<string> cyctimelist, out string mssg)
        {

            cyctimelist = null;
            try
            {

                if (dal.CgCYCDateTimeListGet(xmno, datatype, out cyctimelist))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool CgSurfaceDataGetModelList(int xmno, data.Model.gtsensortype datatype, out List<data.Model.gtsurfacedata> lt, out string mssg)
        {
            lt = new List<Model.gtsurfacedata>();
            try
            {
                if (dal.CgScalarGetModelList(xmno, datatype, out lt))
                {
                    mssg = string.Format("获取项目{0}成果临时表中的结果数据成功!", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}成果临时表中的结果数据失败!", xmno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}成果临时表中的结果数据出错!错误信息:" + ex.Message, xmno);
                return false;
            }
        }
		

	}
}

