﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace data.BLL
{
    public partial class gtsensordata
    {
        public bool AddSurveyData(string xmname,int cyc , data.Model.gtsensortype datatype, out string mssg)
        {
            try
            {
                if (dal.AddData(xmname, cyc, datatype))
                {
                    mssg = string.Format("添加项目编号{0}{1}第{2}周期的数据到编辑库成功", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}{1}第{2}周期的数据到编辑库失败", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}{1}第{2}周期的数据到编辑库出错,错误信息:"+ex.Message, xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), cyc);
                return false;
            }
        }

        public bool SurveyPointTimeDataAdd(string xmname, data.Model.gtsensortype datatype, string pointname, DateTime time, DateTime importtime, out string mssg)
        {
            try
            {
                if (dal.SurveyPointTimeDataAdd(xmname, pointname, datatype, time, importtime))
                {
                    mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据成功", xmname, datatype, pointname, time, importtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据失败", xmname, datatype, pointname, time, importtime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("将项目{0}{1}点{2}时间{3}的数据添加成编辑库{4}的数据出错,错误信息:" + ex.Message, xmname, datatype, pointname, time, importtime);
                return false;
            }
        }
        public bool LostPoints(int xmno, string xmname, data.Model.gtsensortype datatype ,int cyc, out string pointstr, out string mssg)
        {
            mssg = "";
            pointstr = "";
            try
            {
                if (dal.LostPoints(xmno, xmname, datatype ,cyc, out pointstr))
                {
                    mssg = string.Format("项目{0}{1}第{2}周期有{3}点缺失", xmname,data.DAL.gtsensortype.GTSensorTypeToString(datatype) ,cyc, pointstr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}{1}第{2}周期缺失点失败", xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}第{2}周期缺失点出错，错误信息:" + ex.Message, xmname, data.DAL.gtsensortype.GTSensorTypeToString(datatype), cyc);
                return false;

            }
        }

        public bool UpdataSurveyData(data.Model.gtsensordata model,out string mssg)
        {
            try
            {
                if (dal.UpdataSurveyData(model))
                {
                    mssg = string.Format("更新项目{0}{1}点{2}{3}数据成功", model.project_name, model.datatype,model.point_name,model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目{0}{1}点{2}{3}数据失败", model.project_name, model.datatype, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目{0}{1}点{2}{3}数据出错，错误信息:"+ex.Message, model.project_name, model.datatype, model.point_name, model.time);
                return false;
            }
        }
        public bool UpdataSurveyDataNextThis(data.Model.gtsensordata model,out string mssg)
        {
            try
            {
                if (dal.UpdataSurveyData(model))
                {
                    mssg = string.Format("更新项目{0}{1}点{2}{3}的下一条数据成功", model.project_name, model.datatype, model.point_name, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目{0}{1}点{2}{3}的下一条数据失败", model.project_name, model.datatype, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目{0}{1}点{2}{3}的下一条数据出错，错误信息:" + ex.Message, model.project_name, model.datatype, model.point_name, model.time);
                return false;
            }
        }

        public bool SurveyCycTimeList(string xmname, data.Model.gtsensortype datatype, out List<string> cyctimelist, out string mssg)
        {

            cyctimelist = null;
            try
            {

                if (dal.SurveyCycTimeList(xmname, datatype, out cyctimelist))
                {
                    mssg = "编辑库周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "编辑库周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "编辑库周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }
        public bool DeleteSurveyData(string project_name,  data.Model.gtsensortype datatype, int startcyc, int endcyc, out string mssg)
        {
            try
            {
                if (dal.DeleteSurveyData(project_name,  datatype, startcyc, endcyc))
                {
                    mssg = string.Format("删除项目编辑库{0}{1}从{3}到{4}周期的数据成功", project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), startcyc,endcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编辑库{0}{1}从{3}到{4}周期的数据失败", project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编辑库{0}{1}从{3}到{4}周期的数据出错,错误信息:"+ex.Message, project_name, data.DAL.gtsensortype.GTSensorTypeToString(datatype), startcyc, endcyc);
                return false;
            }
        }
        public bool PointNameSurveyDateTimeListGet(string xmname, string pointname, data.Model.gtsensortype datatype, out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.PointNameSurveyDateTimeListGet(xmname, pointname, datatype, out ls))
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}编辑库的时间列表记录数{3}成功", xmname, pointname, datatype, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}{1}点{2}编辑库的时间列表失败", xmname, pointname, datatype);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}{1}点{2}编辑库的时间列表出错,错误信息:" + ex.Message, xmname, pointname, datatype);
                return false;
            }


        }
        public bool SurveyResultdataTableLoad(int startPageIndex, int pageSize, string xmname, string pointname, string sord, Model.gtsensortype datatype, DateTime startTime, DateTime endTime, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SurveyResultdataTableLoad(startPageIndex, pageSize, xmname, pointname, sord, datatype, startTime, endTime, out  dt))
                {
                    mssg = string.Format("{0}结果数据表加载成功!", xmname);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}结果数据表加载失败!", xmname);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}结果数据表加载出错，出错信息" + ex.Message, xmname);
                return false;
            }

        }
        public bool SurveyResultTableRowsCount(string project_name, string pointname, string sord, Model.gtsensortype datatype, int startcyc, int endcyc, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.ScalarResultTableRowsCount(project_name, pointname, sord, datatype, startcyc, endcyc, out totalCont))
                {
                    mssg = string.Format("{0}编辑库结果数据表记录数加载成功!", project_name);
                    return true;

                }
                else
                {
                    mssg = string.Format("{0}编辑库结果数据表记录数加载成功!", project_name);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}编辑库结果数据表加载出错，出错信息" + ex.Message, project_name);
                return false;
            }
        }
    }
}
