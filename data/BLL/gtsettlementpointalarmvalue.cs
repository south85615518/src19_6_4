﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsettlementpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： gtsettlementpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/12/12 11:43:34   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using Tool;
namespace data.BLL
{
    /// <summary>
    /// gtsettlementpointalarmvalue
    /// </summary>
    public partial class gtsettlementpointalarmvalue
    {
        private readonly data.DAL.gtsettlementpointalarmvalue dal = new data.DAL.gtsettlementpointalarmvalue();
        public gtsettlementpointalarmvalue()
        { }
        #region  BasicMethod

        public bool Exist(string point_name, int xmno, out string mssg)
        {

            try
            {
                if (dal.Exist(point_name, xmno))
                {
                    mssg = string.Format("项目编号{0}存在沉降点{1}", xmno, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}不存在沉降点{1}", xmno, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}沉降点{1}出错,错误信息:" + ex.Message, xmno, point_name);
                return true;
            }

        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0}沉降点{1}DTU号{2}的设备成功", model.xmno, model.point_name, model.dtuno);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0}沉降点{1}DTU号{2}的设备失败", model.xmno, model.point_name, model.dtuno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0}沉降点{1}DTU号{2}的设备出错，错误信息：" + ex.Message, model.xmno, model.point_name, model.dtuno);
                return false;
            }




            //return dal.Add(model);
        }

        /// <summary>
        /// 加载data点名
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="ls"></param>
        /// <returns></returns>
        public bool SettlementPointLoadDAL(int xmno, out List<string> ls, out string mssg)
        {
            mssg = "";
            ls = new List<string>();
            try
            {
                if (dal.SettlementPointLoadDAL(xmno, out ls))
                {
                    mssg = string.Format("成功获取项目编号{0}沉降点名{1}个", xmno, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}沉降点名列表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}沉降点名列表出错，错误信息:" + ex.Message, xmno);
                return false;
            }

        }




        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}测点{1}的设备成功", model.xmno, model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}测点{1}的设备失败", model.xmno, model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}测点{1}的设备出错，错误信息：" + ex.Message, model.xmno, model.point_name);
                return false;
            }
        }

        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointnamestr, data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            //dal.MultiUpdate(pointnamestr,model);
            try
            {
                if (dal.MultiUpdate(pointnamestr, model))
                {
                    mssg = "沉降预警点批量更新成功";
                    return true;
                }
                else
                {
                    mssg = "沉降预警点批量更新失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "沉降预警点批量更新出错，错误信息" + ex.Message;
                return false;
            }
        }

        public bool PointTableLoad(int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt, out string mssg)
        {

            dt = null;
            try
            {
                if (dal.PointTableLoad(startPageIndex, pageSize, xmno,colName, sord, out dt))
                {
                    mssg = string.Format("成功获取项目编号{0}的设备状态信息{1}条记录", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的设备状态信息失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的设备状态信息出错，错误信息:" + ex.Message, xmno);
                return false;
            }

        }
        public bool PointTableCountLoad(string searchstring, int xmno, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.PointTableCountLoad(xmno, out totalCont))
                {
                    mssg = string.Format("成功获取项目编号{0}的设备状态信息记录数{1}", xmno, totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的设备状态记录数失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的设备状态记录数出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }


        //public bool PointTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        //{
        //    dt = null;
        //    try
        //    {
        //        if (dal.PointTableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
        //        {
        //            mssg = string.Format("成功获取项目编号{0}的点号预警表{1}条记录", xmno, dt.Rows.Count);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("获取项目编号{0}的点号预警表失败", xmno);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("获取项目编号{0}的点号预警表出错，错误信息:" + ex.Message, xmno);
        //        return false;
        //    }
        //}
        //public bool PointTableCountLoad(string searchstring, int xmno, out string totalCont, out string mssg)
        //{
        //    totalCont = "0";
        //    try
        //    {
        //        if (dal.PointTableCountLoad( xmno, out totalCont))
        //        {
        //            mssg = string.Format("成功获取项目编号{0}的点号预警记录数{1}", xmno, totalCont);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("获取项目编号{0}的点号预警记录数失败", xmno);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("获取项目编号{0}的点号预警记录数出错，错误信息:" + ex.Message, xmno);
        //        return false;
        //    }
        //}

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string point_name,  int xmno, out string mssg)
        {

            try
            {
                if (dal.Delete( xmno,point_name))
                {
                    mssg = string.Format("删除项目编号{0}点{1}的记录成功", xmno, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}点{1}的记录失败", xmno, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}点{1}的记录出错,错误信息：" + ex.Message, xmno, point_name);
                return false;
            }



            //return dal.Delete(point_name,module,xmno);
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, out data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, out model))
                {
                    mssg = string.Format("获取{0}的预警参数成功!", pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}的预警参数失败!", pointname);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的预警参数出错!错误信息:" + ex.Message, pointname);
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetBasePointBridgeModellist(int xmno, string basepointname, out List<data.Model.gtsettlementpointalarmvalue> modellist,out string mssg)
        {
            mssg = "";
            modellist = null;
            try
            {
                if (dal.GetBasePointBridgeModellist(xmno, basepointname, out modellist))
                {
                    mssg = string.Format("获取到项目编号{0}以{1}为根点的沉降点桥有{2}条", xmno, basepointname, modellist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("没有获取到项目编号{0}任何以{1}为根点的沉降点桥", xmno, basepointname, modellist.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}任何以{1}为根点的沉降点桥出错,错误信息:"+ex.Message, xmno, basepointname, modellist.Count);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        //public data.Model.gtsettlementpointalarmvalue GetModel(string point_name, int module, int xmno)
        //{

        //    return dal.GetModel(point_name, module, xmno);
        //}

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        //public data.Model.gtsettlementpointalarmvalue GetModelByCache(string point_name,int module,int xmno)
        //{

        //    string CacheKey = "gtsettlementpointalarmvalueModel-" + point_name+module+xmno;
        //    object objModel = data.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(point_name,module,xmno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = data.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                data.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (data.Model.gtsettlementpointalarmvalue)objModel;
        //}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    return dal.GetList(strWhere);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<data.Model.gtsettlementpointalarmvalue> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        /// <summary>
        /// 获得数据列表
        /// </summary>
        //public List<data.Model.gtsettlementpointalarmvalue> DataTableToList(DataTable dt)
        //{
        //    List<data.Model.gtsettlementpointalarmvalue> modelList = new List<data.Model.gtsettlementpointalarmvalue>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        data.Model.gtsettlementpointalarmvalue model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        //}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        public bool SettlementTOPointModule(int xmno,  string deviceno, string addressno, out data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.SettlementTOPointModule(xmno, deviceno,addressno, out model))
                {
                    mssg = string.Format("获取项目编号{0}设备号{1}通道号{2}的点号对象成功", xmno, deviceno, addressno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}设备号{2}通道号{3}的点号对象失败", xmno, deviceno, addressno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}设备号{2}通道号{3}的点号对象出错，错误信息:" + ex.Message, xmno, deviceno, addressno);
                return false;
            }
            //finally
            //{
            //    ExceptionLog.dataRecordWrite(mssg);
            //}
        }

        public bool SettlementBasePointLoad(int xmno, out List<string> basepointnamelist,out string mssg)
        {
            basepointnamelist = new List<string>();
            try
            {
                if (dal.SettlementBasePointLoad(xmno, out basepointnamelist))
                {
                    mssg = string.Format("成功获取到项目编号{0}的沉降基准点数量{1}", xmno, basepointnamelist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}的沉降基准点数量{1}", xmno, basepointnamelist.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}的沉降基准点名，错误信息："+ex.Message, xmno);
                return false;
            }
            
        }


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

