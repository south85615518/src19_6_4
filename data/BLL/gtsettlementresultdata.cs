﻿/**  版本信息模板在安装目录下，可自行修改。
* gtsettlementresultdata.cs
*
* 功 能： N/A
* 类 名： gtsettlementresultdata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 data Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using Tool;
using SqlHelpers;
namespace data.BLL
{
	/// <summary>
	/// gtsettlementresultdata
	/// </summary>
	public partial class gtsettlementresultdata
	{
        
		private readonly data.DAL.gtsettlementresultdata dal=new data.DAL.gtsettlementresultdata();
		public gtsettlementresultdata()
		{}
		#region  BasicMethod
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(data.Model.gtsettlementresultdata model,out string mssg)
		{
             try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}点名{1}测量时间{2}的沉降结果数据添加成功",model.xmno,model.point_name,model.time);
                    return true;
                }
                else {
                    mssg =  string.Format("项目编号{0}点名{1}测量时间{2}的沉降结果数据添加失败",model.xmno,model.point_name,model.time);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("项目编号{0}点名{1}测量时间{2}的沉降结果数据添加出错，错误信息:"+ex.Message,model.xmno,model.point_name,model.time);
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(data.Model.gtsettlementresultdata model,out string mssg)
		{

            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("项目编号{0}点{1}测量时间{2}沉降结果数据更新成功",model.xmno,model.point_name,model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}点{1}测量时间{2}沉降结果数据更新失败", model.xmno, model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}点{1}测量时间{2}沉降结果数据更新出错,错误信息:"+ex.Message, model.xmno, model.point_name, model.time);
                return false;
            }



			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool Delete(int xmno,DateTime time,string point_name)
        //{
			
        //    return dal.Delete(xmno,time,point_name);
        //}
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteTmp(int xmno,out string mssg)
        {
            try
            {
                if (dal.DeleteTmp(xmno))
                {
                    mssg = string.Format("删除项目编号{0}沉降临时表的数据成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}沉降临时表的数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}沉降临时表的数据出错,错误信息："+ex.Message, xmno);
                return false;
            }

        }
        public bool SettlementDATATableLoad(DateTime starttime, DateTime endtime, int startPageIndex, int pageSize, int xmno, string pointname, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SettlementDATATableLoad(starttime,endtime,startPageIndex, pageSize, xmno, pointname, sord,out  dt))
                {
                    mssg = "沉降结果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "沉降结果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "沉降结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool SettlementDATATableRowsCount(DateTime starttime,DateTime endtime,int xmno, string pointname, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.SettlementTableRowsCount(starttime,endtime,xmno,pointname,out totalCont))
                {
                    mssg = "沉降结果数据表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "沉降结果数据表记录数加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool ResultDataReportPrint(string sql, int xmno, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultDataReportPrint(sql, xmno, out dt))
                {
                    mssg = "沉降结果数据报表数据表生成成功";
                    return true;
                }
                else
                {
                    mssg = "沉降结果数据报表数据表生成失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                
                    mssg = "沉降结果数据报表数据表生成出错，错误信息"+ex.Message;
                    return false;
                
            }
        }
       
        

        public bool MaxTime(int xmno, out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno, out maxtime))
                {
                    mssg = string.Format("获取项目编号{0}沉降测量数据的最大日期{1}成功", xmno, maxtime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}沉降测量数据的最大日期失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}沉降测量数据的最大日期出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        public bool MinTime(int xmno, string point_name, out DateTime Mintime, out string mssg)
        {
            Mintime = new DateTime();
            try
            {
                if (dal.MinTime(xmno, point_name, out Mintime))
                {
                    mssg = string.Format("获取项目编号{0}测量数据点名{2}的最小日期{1}成功", xmno, Mintime, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测量数据点名{1}的最小日期失败", xmno, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测量数据点名{1}的最小日期出错，错误信息:" + ex.Message, xmno, point_name);
                return false;
            }
        }

        public bool MaxTime(int xmno, string point_name, out DateTime maxtime, out string mssg)
        {
            maxtime = new DateTime();
            try
            {
                if (dal.MaxTime(xmno, point_name, out maxtime))
                {
                    mssg = string.Format("获取项目编号{0}测量数据点名{2}的最大日期{1}成功", xmno, maxtime, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测量数据点名{1}的最大日期失败", xmno, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测量数据点名{1}的最大日期出错，错误信息:" + ex.Message, xmno, point_name);
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno,out List<data.Model.gtsettlementresultdata> model ,out string mssg)
        {
            List<data.Model.gtsettlementresultdata> lt = null;
            model = null;
            try
            {
                if (dal.GetList(xmno,out lt))
                {
                    mssg = string.Format("获取项目编号{0}沉降临时表中的沉降结果数据成功!", xmno);
                    model = lt;
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}沉降临时表中的沉降结果数据失败!", xmno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}沉降临时表中的沉降结果数据出错!错误信息:" + ex.Message, xmno);
                return false;
            }
           
        }


        public bool PointNameDateTimeListGet(int xmno, string pointname,out List<string> ls, out string mssg)
        {
            ls = null;
            mssg = "";
            try
            {
                if (dal.PointNameDateTimeListGet(xmno, pointname, out ls))
                {
                    mssg = string.Format("获取到项目{0}沉降点{1}的时间列表记录数{2}成功", xmno, pointname, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}沉降点{1}的时间列表失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}沉降点{1}的时间列表出错,错误信息:" + ex.Message, xmno, pointname);
                return false;
            }


        }
     

        public bool GetModel(int xmno, string pointname, DateTime dt, out data.Model.gtsettlementresultdata model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, dt, out model))
                {
                    mssg = string.Format("获取{0}{1}点{2}的沉降测量数据成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}点{2}的沉降测量数据失败", xmno, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}{1}点{2}的沉降测量数据出错,错误信息:" + ex.Message, xmno, pointname, dt);
                return false;
            }
        }

        public bool GetLastTimeModel(int xmno, string pointname, DateTime dt, out data.Model.gtsettlementresultdata model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetLastTimeModel(xmno, pointname, dt, out model))
                {
                    mssg = string.Format("获取{0}{1}点{2}前的沉降测量数据成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}点{2}前的沉降测量数据失败", xmno, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}{1}点{2}前的沉降测量数据出错,错误信息:" + ex.Message, xmno, pointname, dt);
                return false;
            }
        }
      
        //public bool SettlementPointLoadBLL(int xmno, out List<string> ls, out string mssg)
        //{
        //    ls = null;
        //    try
        //    {
        //        if (dal.(xmno, out ls))
        //        {
        //            mssg = "沉降点号加载成功";
        //            return true;

        //        }
        //        else
        //        {
        //            mssg = "沉降点号加载失败";
        //            return true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = "沉降点号加载出错，错误信息" + ex.Message;
        //        return true;
        //    }

        //}

       

        public bool PointNewestDateTimeGet(int xmno, string pointname, out DateTime dt,out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(xmno, pointname, out dt))
                {
                    mssg = string.Format("获取项目编号{0}沉降{1}点的最新数据的测量时间{2}成功", xmno, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}沉降{1}点的最新数据的测量时间失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}沉降{1}点的最新数据的测量时间出错，错误信息:"+ex.Message, xmno, pointname);
                return false;
            }

        }
        public bool OnLinePointCont(string unitname, out string contpercent, out string mssg)
        {
            mssg = "";
            contpercent = "";
            try
            {
                if (dal.OnLinePointCont(unitname, out contpercent))
                {
                    mssg = string.Format("成功获取静力水准-压力式在线点情况为{0}", contpercent);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取静力水准-压力式在线点情况失败", contpercent);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取静力水准-压力式在线点情况出错,错误信息:" + ex.Message, contpercent);
                return false;
            }
        }
        //public bool XmStateTable(int startPageIndex, int pageSize, int xmno,string xmname, string unitname, string colName, string sord, out DataTable dt,out string mssg)
        //{
        //    dt = null;
        //    mssg = "";
        //    try
        //    {
        //        if (dal.XmStateTable(startPageIndex, pageSize, xmno, xmname, unitname, colName, sord, out  dt))
        //        {
        //            mssg = string.Format("获取编号为{0}项目编号名{1}的沉降项目编号状态表记录数{2}条成功", xmno, xmname, dt.Rows.Count);
        //            return true;
        //        }
        //        else
        //        {
        //            mssg = string.Format("获取编号为{0}项目编号名{1}的沉降项目编号状态表失败", xmno, xmname);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mssg = string.Format("获取编号为{0}项目编号名{1}的沉降项目编号状态表出错,错误信息:"+ex.Message, xmno, xmname);
        //        return false;
        //    }
        //}



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

