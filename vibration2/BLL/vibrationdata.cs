﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vibration2.Model;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;

namespace vibration2.BLL
{
    public partial class vibrationdata
    {
        private readonly vibration2.DAL.vibrationdata dal = new DAL.vibrationdata();

        //添加测振仪数据
        public bool AddVibration(vibration model, out string mssg)
        {
            try
            {
                if (dal.AddVibration(model))
                {
                    mssg = string.Format("添加点名为{0}时间为{1}的测振仪数据成功", model.point_name, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加点名为{0}时间为{1}的测振仪数据失败", model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加点名为{0}时间为{1}的测振仪数据出错, 错误信息为{2}", model.point_name, model.time, ex.Message);
                return false;
            }

        }

        //更新数据
        public bool UpdateVibration(vibration model, out string mssg)
        {
            try
            {
                if (dal.UpdateVibration(model))
                {
                    mssg = string.Format("更新点名为{0}时间为{1}的测振仪数据成功", model.point_name, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新点名为{0}时间为{1}的测振仪数据失败", model.point_name, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新点名为{0}时间为{1}的测振仪数据出错， 错误信息为{2}", model.point_name, model.time,
                    ex.Message);
                return false;
            }

        }

        //根据项目编号 点名 时间 分页获取测振仪数据List
        public bool GetVibrationList(int xmno, string point_name, string beginTime, string endTime, int pageIndex, int pageSize,string orderName, string order, out List<vibration> vibrationList, out string mssg)
        {
            vibrationList = null;
            mssg = "";
            try
            {
                if (dal.GetVibrationList(xmno, point_name, beginTime, endTime, pageIndex, pageSize, orderName, order, out vibrationList))
                {
                    mssg = mssg = string.Format("获取点名为{0}时间为{1}到{2}的测振仪数据成功", point_name, beginTime, endTime);
                    return true;
                }
                else
                {
                    mssg = mssg = string.Format("获取点名为{0}时间为{1}到{2}的测振仪数据失败", point_name, beginTime, endTime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = mssg = string.Format("获取点名为{0}时间为{1}到{2}的测振仪数据出错， 错误信息为{3}", point_name, beginTime, endTime, ex.Message);
                return false;
            }
        }

        //根据项目编号 点名 时间 分页获取测振仪数据List总行数
        public bool GetVibrationListCount(int xmno, string point_name, string beginTime, string endTime, out int count, out string mssg)
        {
            count = 0;
            try
            {
                if (dal.GetVibrationListCount(xmno, point_name, beginTime, endTime, out count))
                {
                    mssg = mssg = string.Format("获取点名为{0}时间为{1}到{2}的测振仪数据成功, 共{3}条记录", point_name, beginTime, endTime, count);
                    return true;
                }
                else
                {
                    mssg = mssg = string.Format("获取点名为{0}时间为{1}到{2}的测振仪数据失败", point_name, beginTime, endTime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = mssg = string.Format("获取点名为{0}时间为{1}到{2}的测振仪数据出错， 错误信息为{3}", point_name, beginTime, endTime, ex.Message);
                return false;
            }
        }

    }
}
