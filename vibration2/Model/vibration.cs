﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vibration2.Model
{
    public class vibration
    {
        public vibration()
        { }
        #region Model
        private int? _xmno;
        private string _point_name;
        private DateTime? _time;
        private double? _xmax;
        private double? _ymax;
        private double? _zmax;
        private string _datafilepath;
        private double? _millionsec;
        /// <summary>
        /// 
        /// </summary>
        public int? xmno
        {
            set { _xmno = value; }
            get { return _xmno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string point_name
        {
            set { _point_name = value; }
            get { return _point_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? time
        {
            set { _time = value; }
            get { return _time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double? XMax
        {
            set { _xmax = value; }
            get { return _xmax; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double? YMax
        {
            set { _ymax = value; }
            get { return _ymax; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double? ZMax
        {
            set { _zmax = value; }
            get { return _zmax; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DataFilePath
        {
            set { _datafilepath = value; }
            get { return _datafilepath; }
        }
        /// <summary>
        /// 
        /// </summary>
        public double? millionsec
        {
            set { _millionsec = value; }
            get { return _millionsec; }
        }
        #endregion Model
    }
}
