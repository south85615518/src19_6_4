﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vibration2.Model;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;

namespace vibration2.DAL
{
    public partial class vibrationdata
    {
        //数据库连接
        public database db = new database();

        //添加测振仪数据
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool AddVibration(vibration model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno.ToString());
            strSql.Append("insert into vibration(");
            strSql.Append("xmno,point_name,time,XMax,YMax,ZMax,DataFilePath,millionsec)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@point_name,@time,@XMax,@YMax,@ZMax,@DataFilePath,@millionsec)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,200),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@XMax", OdbcType.Double),
					new OdbcParameter("@YMax", OdbcType.Double),
					new OdbcParameter("@ZMax", OdbcType.Double),
					new OdbcParameter("@DataFilePath", OdbcType.VarChar,200),
					new OdbcParameter("@millionsec", OdbcType.Double)};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.time;
            parameters[3].Value = model.XMax;
            parameters[4].Value = model.YMax;
            parameters[5].Value = model.ZMax;
            parameters[6].Value = model.DataFilePath;
            parameters[7].Value = model.millionsec;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //根据点名更新测振仪数据
        public bool UpdateVibration(vibration model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno.ToString());
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update vibration set ");
            strSql.Append(" xmno=@xmno,");
            //  strSql.Append("point_name=@point_name,");
            strSql.Append("time=@time,");
            strSql.Append("XMax=@XMax,");
            strSql.Append("YMax=@YMax,");
            strSql.Append(" ZMax=@ZMax, ");
            strSql.Append(" DataFilePath=@DataFilePath, ");
            strSql.Append(" millionsec=@millionsec ");
            strSql.Append("  where  ");
            //根据点名
            strSql.Append("  point_name =  @point_name ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,200),
					new OdbcParameter("@time", OdbcType.DateTime),
					new OdbcParameter("@XMax", OdbcType.Double),
					new OdbcParameter("@YMax", OdbcType.Double),
					new OdbcParameter("@ZMax", OdbcType.Double),
					new OdbcParameter("@DataFilePath", OdbcType.VarChar,200),
					new OdbcParameter("@millionsec", OdbcType.Double)};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.time;
            parameters[3].Value = model.XMax;
            parameters[4].Value = model.YMax;
            parameters[5].Value = model.ZMax;
            parameters[6].Value = model.DataFilePath;
            parameters[7].Value = model.millionsec;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public vibration DataRowToModel(DataRow row)
        {
            vibration model = new vibration();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
                //model.XMax=row["XMax"].ToString();
                //model.YMax=row["YMax"].ToString();
                //model.ZMax=row["ZMax"].ToString();
                if (row["DataFilePath"] != null)
                {
                    model.DataFilePath = row["DataFilePath"].ToString();
                }
                //model.millionsec=row["millionsec"].ToString();
            }
            return model;
        }

        //根据项目编号 点名 时间 分页获取测振仪数据List
        public bool GetVibrationList(int xmno, string point_name, string beginTime, string endTime, int pageIndex, int pageSize, string ordername, string order, out List<vibration> vibrationList)
        {
            vibrationList = new List<vibration>();

            OdbcSQLHelper.Conn = db.GetStanderConn(xmno.ToString());

            string sql = string.Format("select * from vibration  where  xmno = {0} and   point_name   = '{1}'    and time  >=   '{2}'  and   time  <=  '{3}'  order     by     '{4}' '{5}'   limit     {6}   ,   {7}", xmno, point_name, beginTime, endTime, ordername, order, (pageIndex - 1) * pageSize, pageSize);

            //    StringBuilder strsql = new StringBuilder(255);
            //strsql.Append("select * from vibration     where      ");
            //strsql.Append("     xmno   =   @xmno     and   point_name   =    @point_name    and    ");
            //strsql.Append("    time  >=   @beginTime   and   time  <=   @endTime    ");
            //strsql.Append("    order     by     @ordername       ");
            //strsql.Append("     limit     @page   ,   @pageSize   ");

            //OdbcParameter[] paras = {
            //        new OdbcParameter("@xmno", OdbcType.Int,11),              
            //        new OdbcParameter("@point_name", OdbcType.VarChar,200),       
            //        new OdbcParameter("@beginTime", OdbcType.DateTime),
            //        new OdbcParameter("@endTime", OdbcType.DateTime),
            //        new OdbcParameter("@page", OdbcType.Int, 10),
            //        new OdbcParameter("@ordername", OdbcType.VarChar, 5),

            //        new OdbcParameter("@order", OdbcType.VarChar,10),
            //        new OdbcParameter("@pageSize", OdbcType.Int, 5)};
            //paras[0].Value = xmno;
            //paras[1].Value = point_name;
            //paras[2].Value = beginTime;
            //paras[3].Value = endTime;
            //paras[4].Value = (pageIndex-1) * pageSize;
            //paras[5].Value = ordername;
            //paras[6].Value = order;
            //paras[7].Value = pageSize;

            //DataSet ds = OdbcSQLHelper.Query(strsql.ToString(), paras);
            DataSet ds = OdbcSQLHelper.Query(sql.ToString());
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vibration model = DataRowToModel(dt.Rows[i]);
                    vibrationList.Add(model);
                }
                return true;
            }
            else
                return false;
        }

        //根据项目编号 点名 时间 分页获取测振仪数据List总行数
        public bool GetVibrationListCount(int xmno, string point_name, string beginTime, string endTime, out int count)
        {
            count = 0;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno.ToString());
            StringBuilder strsql = new StringBuilder(255);
            strsql.Append("select * from vibration   where   ");
            strsql.Append("  xmno  =  @xmno  and  point_name  =  @point_name  and  ");
            strsql.Append("  time  >=  @beginTime  and  time  <=  @endTime  ");

            OdbcParameter[] paras = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,200),
					new OdbcParameter("@beginTime", OdbcType.DateTime),
                    new OdbcParameter("@endTime", OdbcType.DateTime)};

            paras[0].Value = xmno;
            paras[1].Value = point_name;
            paras[2].Value = beginTime;
            paras[3].Value = endTime;


            DataSet ds = OdbcSQLHelper.Query(strsql.ToString(), paras);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                count = dt.Rows.Count;
                return true;
            }
            else
                return false;
        }
    }
}
