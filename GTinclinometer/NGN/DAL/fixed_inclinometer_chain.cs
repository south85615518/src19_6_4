﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chain.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chain
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/30 16:57:58   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:fixed_inclinometer_chain
	/// </summary>
	public partial class fixed_inclinometer_chain
	{
		public fixed_inclinometer_chain()
		{}
		#region  BasicMethod

        public database db = new database();

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(NGN.Model.fixed_inclinometer_chain model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("insert into fixed_inclinometer_chain(");
			strSql.Append("xmno,point_name,port,deviceno,firstalarmname,secondalarmname,thirdalarmname)");
			strSql.Append(" values (");
            strSql.Append("@xmno,@point_name,@port,@deviceno,@firstalarmname,@secondalarmname,@thirdalarmname)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@port", OdbcType.Int,11),
					new OdbcParameter("@deviceno", OdbcType.Int,11),
                    new OdbcParameter("@firstalarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@secondalarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@thirdalarmname", OdbcType.VarChar,100)
                                         };
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.chain_name;
			parameters[2].Value = model.port;
			parameters[3].Value = model.deviceno;
            parameters[4].Value = model.firstAlarmName;
            parameters[5].Value = model.secondAlarmName;
            parameters[6].Value = model.thirdAlarmName;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string point_name)
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("delete   from   fixed_inclinometer_chain ");
			strSql.Append(" where    xmno=@xmno   and    point_name=@point_name   ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100)
							};
			parameters[0].Value = xmno;
			parameters[1].Value = point_name;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public bool GetModel(int xmno, int port, int deviceno, out NGN.Model.fixed_inclinometer_chain model)
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("select xmno,point_name,port,deviceno from fixed_inclinometer_chain ");
			strSql.Append(" where    xmno=@xmno    and    port=@port    and    deviceno=@deviceno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@port", OdbcType.Int,11),
					new OdbcParameter("@deviceno", OdbcType.Int,11)			};
			parameters[0].Value = xmno;
			parameters[1].Value = port;
			parameters[2].Value = deviceno;

			model=new NGN.Model.fixed_inclinometer_chain();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno,string pointname, out NGN.Model.fixed_inclinometer_chain model)
        {

            StringBuilder strSql = new StringBuilder(); 
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select xmno,point_name,port,deviceno, firstalarmname,secondalarmname,thirdalarmname from fixed_inclinometer_chain ");
            strSql.Append(" where    xmno=@xmno    and    point_name=@point_name     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            

            model = new NGN.Model.fixed_inclinometer_chain();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.fixed_inclinometer_chain DataRowToModel(DataRow row)
		{
			NGN.Model.fixed_inclinometer_chain model=new NGN.Model.fixed_inclinometer_chain();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
                if (row["point_name"] != null && row["point_name"] != "")
				{
					model.chain_name=row["point_name"].ToString();
				}
				if(row["port"]!=null && row["port"].ToString()!="")
				{
					model.port=int.Parse(row["port"].ToString());
				}
				if(row["deviceno"]!=null && row["deviceno"].ToString()!="")
				{
					model.deviceno=int.Parse(row["deviceno"].ToString());
				}
                if (row["firstalarmname"] != null && row["firstalarmname"] != "")
                {
                    model.firstAlarmName = row["firstalarmname"].ToString();
                }
                if (row["secondalarmname"] != null && row["secondalarmname"] != "")
                {
                    model.secondAlarmName = row["secondalarmname"].ToString();
                }
                if (row["thirdalarmname"] != null && row["thirdalarmname"] != "")
                {
                    model.thirdAlarmName = row["thirdalarmname"].ToString();
                }
			}
			return model;
		}
        /// <summary>
        /// 加载测斜链表
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);;
            string sql = string.Format("select point_name,port,deviceno,xmno,firstalarmname,secondalarmname,thirdalarmname from fixed_inclinometer_chain where xmno='{3}'   {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        /// <summary>
        /// 加载测斜链数量
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableCountLoad( int xmno,  out string cont)
        {
            OdbcConnection conn = db.GetStanderConn(xmno); 
            string sql = string.Format("select count(1) from fixed_inclinometer_chain where xmno='{0}'",xmno);
            
            cont = querysql.querystanderstr(sql, conn);
            
            return true;
        }



        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateAlarm(NGN.Model.fixed_inclinometer_chain model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update fixed_inclinometer_chain set ");
            strSql.Append("FirstAlarmName=@FirstAlarmName,");
            strSql.Append("SecondAlarmName=@SecondAlarmName,");
            strSql.Append("ThirdAlarmName=@ThirdAlarmName");
            strSql.Append("   where   ");
            strSql.Append(" point_name=@point_name");
            OdbcParameter[] parameters = {
					

					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
					
                    
                                         };


            parameters[0].Value = model.firstAlarmName;
            parameters[1].Value = model.secondAlarmName;
            parameters[2].Value = model.thirdAlarmName;
            parameters[3].Value = model.chain_name;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdateAlarm(string pointNameStr, NGN.Model.fixed_inclinometer_chain model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update fixed_inclinometer_chain set ");
            strSql.Append("FirstAlarmName=@FirstAlarmName,");
            strSql.Append("SecondAlarmName=@SecondAlarmName,");
            strSql.Append("ThirdAlarmName=@ThirdAlarmName");
            strSql.Append("   where   ");
            strSql.Append("point_name in ('" + pointNameStr + "')");
            OdbcParameter[] parameters = {
					

					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120)
					
                    
                                         };


            parameters[0].Value = model.firstAlarmName;
            parameters[1].Value = model.secondAlarmName;
            parameters[2].Value = model.thirdAlarmName;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ChainNameLoad(int xmno,out List<string> chainnamelist )
        {
            chainnamelist = new List<string>();
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select distinct(point_name) from fixed_inclinometer_chain where  xmno='" + xmno + "' ";
            chainnamelist = querysql.querystanderlist(sql, conn);

            //List<string> lsFormat = new List<string>();
            //foreach (string name in lsFormat)
            //{
            //    chainnamelist.Add(name + ":" + name);
            //}
            //chainnamelsit = lsFormat;//string.Join(";", lsFormat);
            return true;
        }


        /// <summary>
        /// 清空关联的预警参数
        /// </summary>
        public bool DeleteAlarmValue(int xmno, string alarmname)
        {
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update fixed_inclinometer_chain  set firstAlarmName = ''   ");
            strSql.Append(" where  xmno =@xmno and firstAlarmName=@firstAlarmName ");
            OdbcParameter[] parametersfirst = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@firstAlarmName", OdbcType.VarChar,100)
			};
            parametersfirst[0].Value = xmno;
            parametersfirst[1].Value = alarmname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parametersfirst);
            strSql = new StringBuilder();
            strSql.Append("update fixed_inclinometer_chain  set secondAlarmName = ''   ");
            strSql.Append(" where  xmno =@xmno and secondAlarmName=@secondAlarmName ");
            OdbcParameter[] parameterssecond = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@secondAlarmName", OdbcType.VarChar,100)
			};
            parameterssecond[0].Value = xmno;
            parameterssecond[1].Value = alarmname;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameterssecond);
            strSql = new StringBuilder();
            strSql.Append("update fixed_inclinometer_chain  set thirdAlarmName = ''   ");
            strSql.Append(" where  xmno =@xmno and thirdAlarmName=@thirdAlarmName ");
            OdbcParameter[] parametersthird = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@thirdAlarmName", OdbcType.VarChar,100)
			};
            parametersthird[0].Value = xmno;
            parametersthird[1].Value = alarmname;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parametersthird);

            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

