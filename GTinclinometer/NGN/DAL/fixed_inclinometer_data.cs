﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_data.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/28 15:23:51   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//using Odbc.Data.OdbcClient;
//using Maticsoft.DBUtility;//Please add references
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:fixed_inclinometer_data
	/// </summary>
	public partial class fixed_inclinometer_data
	{
		public fixed_inclinometer_data()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(NGN.Model.fixed_inclinometer_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into fixed_inclinometer_data(");
			strSql.Append("xmno,point_name,disp,time)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@point_name,@disp,@time)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@disp", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.disp;
			parameters[3].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(NGN.Model.fixed_inclinometer_data model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update fixed_inclinometer_data set ");
			strSql.Append("xmno=@xmno,");
			strSql.Append("point_name=@point_name,");
			strSql.Append("disp=@disp,");
			strSql.Append("time=@time");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@disp", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.disp;
			parameters[3].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from fixed_inclinometer_data ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.fixed_inclinometer_data GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select xmno,point_name,disp,time from fixed_inclinometer_data ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			NGN.Model.fixed_inclinometer_data model=new NGN.Model.fixed_inclinometer_data();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.fixed_inclinometer_data DataRowToModel(DataRow row)
		{
			NGN.Model.fixed_inclinometer_data model=new NGN.Model.fixed_inclinometer_data();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["disp"]!=null && row["disp"].ToString()!="")
				{
					model.disp=double.Parse(row["disp"].ToString());
				}
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select xmno,point_name,disp,time ");
			strSql.Append(" FROM fixed_inclinometer_data ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

