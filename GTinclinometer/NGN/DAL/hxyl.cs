﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.OleDb;
using Tool;
using System.Data;
using System.Data.Odbc;

namespace NGN.DAL
{
    public class HXYL
    {

        public database db = new database();
        /// <summary>
        /// 增加一条数据
        /// </summary>
        //public bool Add(MDBDATA.Model.hxyl model)
        //{
        //    //OleDbSQLHelper.Conn = db.GetStanderConn(model.xmno);
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("insert  into ngn_hxyldata(");
        //    strSql.Append("id,[time],val)");
        //    strSql.Append(" values (");
        //    strSql.Append("@id,@time,@val)");
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@id", OleDbType.VarChar,200),
        //            new OleDbParameter("@time", OleDbType.DBTimeStamp),
        //            new OleDbParameter("@val", OleDbType.Double)};
        //    parameters[0].Value = model.id;
        //    parameters[1].Value = model.time;
        //    parameters[2].Value = model.val;

        //    int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// 向云平台的数据中增加一条记录
        /// </summary>
        public bool Addhxyl(NGN.Model.hxyl model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("replace  into hxyldata(");
            strSql.Append("id,year,month,day,hour,minute,val,ac_val,xmno)");
            strSql.Append(" values (");
            strSql.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8})", model.id, model.time.Year - 2000, model.time.Month, model.time.Day, model.time.Hour, model.time.Minute, model.val, model.ac_val,model.xmno);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows >= 0)
            {
                return true;
            }
            return false;


        }
        public bool PreAcVal(NGN.Model.hxyl model, out NGN.Model.hxyl pretimemodel)
        {
            pretimemodel = new NGN.Model.hxyl();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            string sql = @"select id,val,ac_val,DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i') as time from hxyldata  where id in (" + string.Join(",", model.id) + @") and 
DATE_FORMAT('"+model.time+ @"','%Y/%m/%d %H:%i') >=
DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i') order by DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i') desc limit 0,1 ";
            DataSet ds = OdbcSQLHelper.Query(sql, "hxyldata");
            if (ds == null) return false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                pretimemodel = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

//        /// <summary>
//        /// 更新已传送标志
//        /// </summary>
//        public bool update(MDBDATA.Model.hxyl model)
//        {
//            //OleDbSQLHelper.Conn = db.GetStanderConn(model.xmno);
//            StringBuilder strSql = new StringBuilder();
//            strSql.Append("update  hxyl");
//            strSql.Append(" set send  = 1");
//            strSql.Append(" where ");
//            strSql.Append("   id = @id    and     time=format('"+model.time+"','yyyy/mm/dd HH:mm:ss')");
//            OleDbParameter[] parameters = {
//                    new OleDbParameter("@id", OleDbType.Numeric)};
//                    //new OleDbParameter("@time", OleDbType.VarChar,100)};
//            parameters[0].Value = model.id;
//            //parameters[1].Value = model.time;
//            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
//            if (rows > 0)
//            {
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//        }

        //public bool maxTime(out DateTime maxTime)
        //{

        //    OleDbConnection conn = db.GetStanderConn(xmno);
        //    maxTime = new DateTime();
        //    StringBuilder strSql = new StringBuilder();

        //    string sql = "select max([time]) from hxyl";
        //    string maxtime = querysql.queryaccessdbstring(sql);
        //    if (maxtime == "") return false;
        //    maxTime = Convert.ToDateTime(maxtime);
        //    return true;


        //    return false;
        //}

        //public bool HXYLDATALoad(out List<NGN.Model.hxyl> hxyllist)
        //{
        //    hxyllist = new List<NGN.Model.hxyl>();
        //    //OleDbSQLHelper.Conn = db.GetStanderConn(xmno);
        //    string sql = "select * from hxyl where send = 0";
        //    querysql query = new querysql();
        //    DataTable dt = query.querytaccesstdb(sql);
        //    int i = 0;
        //    while (i < dt.Rows.Count)
        //    {
        //        hxyllist.Add(DataRowToModel(dt.Rows[i]));
        //        i++;
        //    }

        //    return true;
        //}
//        public bool GetList(List<string>idList,out List<hxyl> hxyllist)
//        { 
//            hxyllist = new List<hxyl>();
//            //OleDbSQLHelper.Conn = db.GetStanderConn(xmno);
//            string sql = @"select  distinct([设备编号]) as id,分钟雨量 
//as val,format( cstr(年份)+'/'+cstr(月份)+'/'+cstr(日)+' '+cstr(时)+':'+cstr(分),
//'yyyy/mm/dd HH:mm:ss') as [time],0 as send from 雨量历史数据  where 设备编号 in ("+string.Join(",",idList)+@") order by
//format( cstr(年份)+'/'+cstr(月份)+'/'+cstr(日)+' '+cstr(时)+':'+cstr(分),
//'yyyy/mm/dd HH:mm:ss') desc,设备编号 desc ";
//            queryylsql querysql = new queryylsql();
//            DataSet ds = OleDbSQLHXYLHelper.Query(sql,"雨量历史数据"); //querysql.querytaccesstdb(sql);
//            if (ds == null) return false;
//            int i = 0;
//            List<string> idhashtable = new List<string>();
//            while (i < ds.Tables[0].Rows.Count && i < idList.Count)
//            {
//                if (!idhashtable.Contains(ds.Tables[0].Rows[i]["id"]))
//                    idhashtable.Add(ds.Tables[0].Rows[i]["id"].ToString());
//                else
//                    break;
//                hxyllist.Add(DataRowToModel(ds.Tables[0].Rows[i]));
//                i++;
                
//            }

//            return true;
//        }

//        public bool GetAlarmTableCont(List<string>idList,out int cont)
//        {
//            cont = 0;

//            //StringBuilder strSql = new StringBuilder();
//            //OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
//            //strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
//            //strSql.Append(" FROM dtudata_tmp where xmno = @xmno ");
//             string sql = @"select  count(1) from 雨量历史数据  where 设备编号 in ("+string.Join(",",idList)+@")  ";
//            //OdbcParameter[] parameters = {

//            //        new OdbcParameter("@xmno", OdbcType.Int,11)};

//            //parameters[0].Value = xmno;
//            string contstr = queryylsql.queryaccessdbstring(sql);
//            //DataTable dt  = querybkgsql.
//            cont = int.Parse(contstr);
//            return true;
//        }
    



//        ///// <summary>
//        ///// 得到一个对象实体
//        ///// </summary>
//        //public MDBDATA.Model.hxyl DataRowToModel(DataRow row)
//        //{
//        //    MDBDATA.Model.hxyl model = new MDBDATA.Model.hxyl();
//        //    if (row != null)
//        //    {
//        //        if (row["id"] != null)
//        //        {
//        //            model.id = row["id"].ToString();
//        //        }
//        //        //model.val=row["val"].ToString();
//        //        if (row["time"] != null && row["time"].ToString() != "")
//        //        {
//        //            model.time = DateTime.Parse(row["time"].ToString());
//        //        }
//        //        if (row["val"] != null && row["val"].ToString() != "")
//        //        {
//        //            model.val = Double.Parse(row["val"].ToString());
//        //        }
               
//        //        if (row["send"] != null && row["send"].ToString() != "")
//        //        {
//        //            model.send = row["send"].ToString() == "0"?false:true;
//        //        }
//        //    }
//        //    return model;
//        //}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public NGN.Model.hxyl DataRowToModel(DataRow row)
        {
            NGN.Model.hxyl model = new NGN.Model.hxyl();
            if (row != null)
            {
                if (row["id"] != null)
                {
                    model.id = row["id"].ToString();
                }
                //model.val=row["val"].ToString();
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
                if (row["val"] != null && row["val"].ToString() != "")
                {
                    model.val = Double.Parse(row["val"].ToString());
                }
                if (row["ac_val"] != null && row["ac_val"].ToString() != "")
                {
                    model.ac_val = Double.Parse(row["ac_val"].ToString());
                }
                
            }
            return model;
        }
//        /////// <summary>
//        /////// 获得数据列表
//        /////// </summary>
//        ////public bool GetList(List<int> idList, out List<MDBDATA.Model.mcudata> li)
//        ////{
//        ////    li = new List<MDBDATA.Model.mcudata>();

//        ////    //StringBuilder strSql = new StringBuilder();
//        ////    //OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
//        ////    //strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
//        ////    //strSql.Append(" FROM dtudata_tmp where xmno = @xmno ");
//        ////    string sql = "select sname,r1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='应力' and sname in ('" + string.Join("','", pointnamelist) + "')  order by dt desc ,sname desc";
//        ////    //OdbcParameter[] parameters = {

//        ////    //        new OdbcParameter("@xmno", OdbcType.Int,11)};

//        ////    //parameters[0].Value = xmno;
//        ////    DataSet ds = OleDbSQLBKGHelper.Query(sql, "T_DATAMEAS");
//        ////    //DataTable dt  = querybkgsql.
//        ////    if (ds == null) return false;
//        ////    int i = 0;

//        ////    while (i < ds.Tables[0].Rows.Count && i < pointnamelist.Count)
//        ////    {
//        ////        li.Add(DataRowToReportModel(ds.Tables[0].Rows[i]));
//        ////        i++;
//        ////    }
//        ////    return true;
//        ////}
        public bool HXYLPointLoad(int xmno, out List<string> pointnamelist)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            querysql query = new querysql();
            string sql = "select point_name from ylpoint where xmno='" + xmno + "'";
            pointnamelist = querysql.querystanderlist(sql, conn);
            return true;
        }
        public bool HXYLID(int xmno, string pointname, out string id)
        {
            //ExceptionLog.
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select id from ylpoint where xmno='" + xmno + "' and point_name='" + pointname + "'";
            id = querysql.querystanderstr(sql, conn);
            return true;
        }
        public bool HXYLPointname(int xmno, string id, out string pointname)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select point_name from ylpoint where xmno='" + xmno + "' and id='" + id + "'";
            pointname = querysql.querystanderstr(sql, conn);
            return true;
        }
        public bool HXYLMaxTime(string pointnamenostr,int xmno, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcConnection conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            string sql = string.Format("select max( DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i')) from hxyldata where id in ({0}) ", pointnamenostr);
            string maxtime = querysql.querystanderstr(sql, conn);
            if (maxtime == "") return false;
            maxTime = Convert.ToDateTime(maxtime);
            return true;
        }

//        /// <summary>
//        /// 删除数据点
//        /// </summary>
//        public bool Delete(string id,DateTime time)
//        {
//            //OleDbSQLHelper.Conn = db.GetStanderConn(model.xmno);
//            StringBuilder strSql = new StringBuilder();
//            strSql.Append("delete  from 雨量历史数据  ");
//            strSql.Append("  where  ");
//            strSql.Append("   [设备编号] = @id    and     format( cstr(年份)+'/'+cstr(月份)+'/'+cstr(日)+' '+cstr(时)+':'+cstr(分),'yyyy/mm/dd HH:mm:ss')=format('" + time + "','yyyy/mm/dd HH:mm:ss')");
//            OleDbParameter[] parameters = {
//                    new OleDbParameter("@id", OleDbType.Integer)};
//            //new OleDbParameter("@time", OleDbType.VarChar,100)};
//            ExceptionLog.DTUPortInspectionWrite(strSql.ToString());
//            parameters[0].Value = id;
//            //parameters[1].Value = model.time;
//            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
//            if (rows > 0)
//            {
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//        }

        public bool PointNewestDateTimeGet(int pointid, out DateTime dt)
        {
            //OdbcSQLHelper.Conn = db.GetStanderConn(xmno);

            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"select max( DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i')) from hxyldata where id in ({0})", pointid);
            object obj = OleDbSQLHXYLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }
        public bool GetModel(int pointid, DateTime dt, out   NGN.Model.hxyl model)
        {
            model = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select   id,val,DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i') as time, from hxyldata  where id =" + pointid + @" and  DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i') = DATE_FORMAT('"+dt+"'),'%Y/%m/%d %H:%i') ");
            //strSql.Append(" FROM   senor_data   where xmno =   @xmno    and     point_name =  @point_name    and   time = @time   ");

            DataSet ds = OleDbSQLHXYLHelper.Query(strSql.ToString(), "hxyldata");
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }



    }
}
