﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chaindata.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chaindata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/30 16:58:19   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:fixed_inclinometer_chaindata
	/// </summary>
	public partial class fixed_inclinometer_chaindata
	{
        public static database db = new database();
		public fixed_inclinometer_chaindata()
		{}
		#region  BasicMethod
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(NGN.Model.fixed_inclinometer_chaindata model)
		{
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("insert ignore into fixed_inclinometer_chaindata(");
			strSql.Append("xmno,point_name,disp,time,taskid)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@point_name,@disp,@time,@taskid)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@disp", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime),
                    new OdbcParameter("@taskid", OdbcType.VarChar,100)
                                         };
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.disp;
			parameters[3].Value = model.time;
            parameters[4].Value = model.taskid;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(NGN.Model.fixed_inclinometer_chaindata model)
		{
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("update fixed_inclinometer_chaindata set ");
			strSql.Append("disp=@disp");
			strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@disp", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.disp;
			parameters[1].Value = model.xmno;
			parameters[2].Value = model.point_name;
			parameters[3].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string point_name,DateTime time)
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("delete from fixed_inclinometer_chaindata ");
			strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = xmno;
			parameters[1].Value = point_name;
			parameters[2].Value = time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        /// <summary>
        /// 删除临时表数据
        /// </summary>
        public bool Delete_tmp(int xmno,out int cont)
        {

            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from fixed_inclinometer_chaindata_tmp ");
            strSql.Append(" where xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
                                         };
            parameters[0].Value = xmno;
            cont = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);

            if (cont >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool  GetModel(int xmno,string point_name,DateTime time, out NGN.Model.fixed_inclinometer_chaindata model )
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);;
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("select xmno,point_name,disp,time from fixed_inclinometer_chaindata ");
			strSql.Append(" where    xmno=@xmno    and    point_name=@point_name   and    time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = xmno;
			parameters[1].Value = point_name;
			parameters[2].Value = time;

			model=new NGN.Model.fixed_inclinometer_chaindata();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


        /// <summary>
        /// 得到一个测斜链数据对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_chaindata> lt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,point_name,disp,time  from fixed_inclinometer_chaindata_tmp ");
            strSql.Append(" where    xmno=@xmno   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;

            lt = new List<NGN.Model.fixed_inclinometer_chaindata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.fixed_inclinometer_chaindata DataRowToModel(DataRow row)
		{
			NGN.Model.fixed_inclinometer_chaindata model=new NGN.Model.fixed_inclinometer_chaindata();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
                if (row["disp"] != null)
                {
                    model.disp =double.Parse(row["disp"].ToString());
                }
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
			}
			return model;
		}

        public bool MaxTime(int xmno, string chainname, out DateTime maxTime)
        {
            maxTime = new DateTime();
            SingleTonOdbcSQLHelper sqlhelper = new SingleTonOdbcSQLHelper();
            sqlhelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from fixed_inclinometer_orgldata  where     xmno = @xmno    and  chain_name=@chain_name");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmno",OdbcType.Int),
                new OdbcParameter("@chain_name",OdbcType.VarChar,200)
            };
            paramters[0].Value = xmno;
            paramters[1].Value = chainname;
            object obj = sqlhelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

