﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_device.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_device
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/28 15:23:51   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//using Odbc.Data.OdbcClient;
//using Maticsoft.DBUtility;//Please add references
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:fixed_inclinometer_device
	/// </summary>
	public partial class fixed_inclinometer_device
	{
		public fixed_inclinometer_device()
		{}
        public database db = new database();
		#region  BasicMethod

        public bool Exist(string point_name,int xmno)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from  fixed_inclinometer_device where point_name=@point_name and xmno = @xmno");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)
					
					
                                         };
            
            parameters[1].Value = point_name;
            parameters[0].Value = xmno;

            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(),parameters);
            return Convert.ToInt32(obj) == 0 ? false : true;
          
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_device model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert ignore into fixed_inclinometer_device(");
            strSql.Append("xmno,point_name,deviceno,modulesA,modulesB,modulesC,modulesD,Wheeldistance,chain,address,port,deep,sensorno,firstalarmname,secondalarmname,thirdalarmname)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@point_name,@deviceno,@modulesA,@modulesB,@modulesC,@modulesD,@Wheeldistance,@chain,@address,@port,@deep,@sensorno,@firstalarmname,@secondalarmname,@thirdalarmname)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@deviceno", OdbcType.Int,11),
					new OdbcParameter("@modulesA", OdbcType.Double),
					new OdbcParameter("@modulesB", OdbcType.Double),
					new OdbcParameter("@modulesC", OdbcType.Double),
					new OdbcParameter("@modulesD", OdbcType.Double),
					new OdbcParameter("@Wheeldistance", OdbcType.Double),
					new OdbcParameter("@chain", OdbcType.VarChar,100),
					new OdbcParameter("@address", OdbcType.Int,11),
					new OdbcParameter("@port", OdbcType.Int,11),
                    new OdbcParameter("@deep", OdbcType.Double),
                    new OdbcParameter("@sensorno", OdbcType.VarChar,100),
                    new OdbcParameter("@firstalarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@secondalarmname", OdbcType.VarChar,100),
                    new OdbcParameter("@thirdalarmname", OdbcType.VarChar,100)
                                         };
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.deviceno;
            parameters[3].Value = model.modulesA;
            parameters[4].Value = model.modulesB;
            parameters[5].Value = model.modulesC;
            parameters[6].Value = model.modulesD;
            parameters[7].Value = model.Wheeldistance;
            parameters[8].Value = model.chain;
            parameters[9].Value = model.address;
            parameters[10].Value = model.port;
            parameters[11].Value = model.deep;
            parameters[12].Value = model.sensorno;
            parameters[13].Value = model.firstAlarmName;
            parameters[14].Value = model.secondAlarmName;
            parameters[15].Value = model.thirdAlarmName;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(NGN.Model.fixed_inclinometer_device model)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update fixed_inclinometer_device set ");
            strSql.Append("xmno=@xmno,");
            strSql.Append("point_name=@point_name,");
            strSql.Append("modulesA=@modulesA,");
            strSql.Append("modulesB=@modulesB,");
            strSql.Append("modulesC=@modulesC,");
            strSql.Append("modulesD=@modulesD,");
            strSql.Append("Wheeldistance=@Wheeldistance,");
            strSql.Append("chain=@chain,");
            strSql.Append("deep=@deep,");
            strSql.Append("sensorno=@sensorno,");
            strSql.Append("firstAlarmname=@firstAlarmname,");
            strSql.Append("secondAlarmname=@secondAlarmname,");
            strSql.Append("thirdAlarmname=@thirdAlarmname");
            strSql.Append("    where   address=@address    and      port=@port    and  deviceno = @deviceno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@modulesA", OdbcType.Double),
					new OdbcParameter("@modulesB", OdbcType.Double),
					new OdbcParameter("@modulesC", OdbcType.Double),
					new OdbcParameter("@modulesD", OdbcType.Double),
					new OdbcParameter("@Wheeldistance", OdbcType.Double),
					new OdbcParameter("@chain", OdbcType.VarChar,100),
                    new OdbcParameter("@deep", OdbcType.Double),
                    new OdbcParameter("@sensorno", OdbcType.VarChar,100),

                    new OdbcParameter("@firstAlarmname", OdbcType.VarChar,100),
					new OdbcParameter("@secondAlarmname", OdbcType.VarChar,100),
					new OdbcParameter("@thirdAlarmname", OdbcType.VarChar,100),

					
					new OdbcParameter("@address", OdbcType.Int,11),
					new OdbcParameter("@port", OdbcType.Int,11),
                    new OdbcParameter("@deviceno", OdbcType.Int)
                                         };

            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.modulesA;
            parameters[3].Value = model.modulesB;
            parameters[4].Value = model.modulesC;
            parameters[5].Value = model.modulesD;
            parameters[6].Value = model.Wheeldistance;
            parameters[7].Value = model.chain;
            parameters[8].Value = model.deep;
            parameters[9].Value = model.sensorno;

            parameters[10].Value = model.firstAlarmName;
            parameters[11].Value = model.secondAlarmName;
            parameters[12].Value = model.thirdAlarmName;


            parameters[13].Value = model.address;
            parameters[14].Value = model.port;
            parameters[15].Value = model.deviceno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(NGN.Model.fixed_inclinometer_device model)
		{
			//该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from fixed_inclinometer_device ");
            strSql.Append(" where    xmno =@xmno   and    point_name=@point_name ");
			OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100)
			};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
       


        /// <summary>
        /// 清空关联的测斜链
        /// </summary>
        public bool DeleteChain(int xmno,string chainname)
        {
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update fixed_inclinometer_device  set chain = ''   ");
            strSql.Append(" where  xmno =@xmno and chain=@chainname ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@chainname", OdbcType.VarChar,100)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = chainname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 清空关联的预警参数
        /// </summary>
        public bool DeleteAlarmValue(int xmno, string alarmname)
        {
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update fixed_inclinometer_device  set firstAlarmName = ''   ");
            strSql.Append(" where  xmno =@xmno and firstAlarmName=@firstAlarmName ");
            OdbcParameter[] parametersfirst = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@firstAlarmName", OdbcType.VarChar,100)
			};
            parametersfirst[0].Value = xmno;
            parametersfirst[1].Value = alarmname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parametersfirst);
            strSql = new StringBuilder();
            strSql.Append("update fixed_inclinometer_device  set secondAlarmName = ''   ");
            strSql.Append(" where  xmno =@xmno and secondAlarmName=@secondAlarmName ");
            OdbcParameter[] parameterssecond = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@secondAlarmName", OdbcType.VarChar,100)
			};
            parameterssecond[0].Value = xmno;
            parameterssecond[1].Value = alarmname;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameterssecond);
            strSql = new StringBuilder();
            strSql.Append("update fixed_inclinometer_device  set thirdAlarmName = ''   ");
            strSql.Append(" where  xmno =@xmno and thirdAlarmName=@thirdAlarmName ");
            OdbcParameter[]  parametersthird = {
                    new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@thirdAlarmName", OdbcType.VarChar,100)
			};
            parametersthird[0].Value = xmno;
            parametersthird[1].Value = alarmname;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parametersthird);

            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool GetModel(int xmno,int deviceno,int address,int port,out NGN.Model.fixed_inclinometer_device model  )
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select xmno,point_name,deviceno,modulesA,modulesB,modulesC,modulesD,Wheeldistance,chain,address,port,deep,firstAlarmName,secondAlarmName,thirdAlarmName from fixed_inclinometer_device ");
			strSql.Append(" where  xmno = @xmno  and  deviceno=@deviceno  and address=@address  and  port = @port ");
			OdbcParameter[] parameters = {

                        new OdbcParameter("@xmno", OdbcType.Int,11),
                        new OdbcParameter("@deviceno", OdbcType.Int,11),
                        new OdbcParameter("@address", OdbcType.Int,11),
                        new OdbcParameter("@port", OdbcType.Int,11)


			};

            parameters[0].Value = xmno;
            parameters[1].Value = deviceno;
            parameters[2].Value = address;
            parameters[3].Value = port;

			model=new NGN.Model.fixed_inclinometer_device();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetNGN_FixedInclinometerModel(int xmno, int deviceno, int address, int port, out NGN.Model.fixed_inclinometer_device model)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select xmno,point_name,deviceno,modulesA,modulesB,modulesC,modulesD,Wheeldistance,chain,address,port,firstAlarmName,secondAlarmName,thirdAlarmName from fixed_inclinometer_device ");
            strSql.Append(" where   deviceno=@deviceno  and address=@address  and  port = @port ");
            OdbcParameter[] parameters = {
                        new OdbcParameter("@deviceno", OdbcType.Int,11),
                        new OdbcParameter("@address", OdbcType.Int,11),
                        new OdbcParameter("@port", OdbcType.Int,11)


			};
            parameters[0].Value = deviceno;
            parameters[1].Value = address;
            parameters[2].Value = port;

            model = new NGN.Model.fixed_inclinometer_device();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, out NGN.Model.fixed_inclinometer_device model)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select * from fixed_inclinometer_device ");
            strSql.Append(" where    xmno=@xmno    and    point_name=@point_name     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;


            model = new NGN.Model.fixed_inclinometer_device();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, string chain_name, out List<NGN.Model.fixed_inclinometer_device> modellist)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select * from fixed_inclinometer_device ");
            strSql.Append(" where    xmno=@xmno    and    chain_name=@chain_name     ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@chain_name", OdbcType.VarChar,100)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = chain_name;


            modellist = new List<NGN.Model.fixed_inclinometer_device>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                modellist.Add(DataRowToModel(ds.Tables[0].Rows[++i]));
                
            }
            return true;
        }



		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.fixed_inclinometer_device DataRowToModel(DataRow row)
		{
			NGN.Model.fixed_inclinometer_device model=new NGN.Model.fixed_inclinometer_device();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["deviceno"]!=null && row["deviceno"].ToString()!="")
				{
					model.deviceno=int.Parse(row["deviceno"].ToString());
				}
				if(row["modulesA"]!=null && row["modulesA"].ToString()!="")
				{
					model.modulesA=double.Parse(row["modulesA"].ToString());
				}
				if(row["modulesB"]!=null && row["modulesB"].ToString()!="")
				{
					model.modulesB=double.Parse(row["modulesB"].ToString());
				}
				if(row["modulesC"]!=null && row["modulesC"].ToString()!="")
				{
					model.modulesC=double.Parse(row["modulesC"].ToString());
				}
				if(row["modulesD"]!=null && row["modulesD"].ToString()!="")
				{
					model.modulesD=double.Parse(row["modulesD"].ToString());
				}
				if(row["Wheeldistance"]!=null && row["Wheeldistance"].ToString()!="")
				{
					model.Wheeldistance=double.Parse(row["Wheeldistance"].ToString());
				}
                if (row["chain"] != null && row["chain"].ToString() != "")
                {
                    model.chain = row["chain"].ToString();
                }
                if (row["deep"] != null && row["deep"].ToString() != "")
                {
                    model.deep = double.Parse(row["deep"].ToString());
                }
                if (row["address"] != null && row["address"].ToString() != "")
                {
                    model.address = int.Parse(row["address"].ToString());
                }
                if (row["port"] != null && row["port"].ToString() != "")
                {
                    model.port = int.Parse(row["port"].ToString());
                }
                if (row["firstalarmname"] != null && row["firstalarmname"] != "")
                {
                    model.firstAlarmName = row["firstalarmname"].ToString();
                }
                if (row["secondalarmname"] != null && row["secondalarmname"] != "")
                {
                    model.secondAlarmName = row["secondalarmname"].ToString();
                }
                if (row["thirdalarmname"] != null && row["thirdalarmname"] != "")
                {
                    model.thirdAlarmName = row["thirdalarmname"].ToString();
                }
			}
			return model;
		}

        /// <summary>
        /// 关联多个测斜段
        /// </summary>
        public bool ChainMultiUpdate(string pointNameStr, NGN.Model.fixed_inclinometer_chain model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update fixed_inclinometer_device set ");
            strSql.Append("  chain=@chain,  ");
            strSql.Append("  port=@port,  ");
            strSql.Append("  deviceno=@deviceno  ");
            strSql.Append("   where   ");
            strSql.Append(" point_name  in ('" + pointNameStr + "')   and xmno = @xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@chain", OdbcType.VarChar,100),
					new OdbcParameter("@port", OdbcType.Int,11),
					new OdbcParameter("@deviceno", OdbcType.Int,11),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };



            parameters[0].Value = model.chain_name;
            parameters[1].Value = model.port;
            parameters[2].Value = model.deviceno;
            parameters[3].Value = model.xmno;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateAlarm(NGN.Model.fixed_inclinometer_device model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update fixed_inclinometer_device set ");
            strSql.Append("FirstAlarmName=@FirstAlarmName,");
            strSql.Append("SecondAlarmName=@SecondAlarmName,");
            strSql.Append("ThirdAlarmName=@ThirdAlarmName");
            strSql.Append("   where   ");
            strSql.Append("point_name=@point_name");
            OdbcParameter[] parameters = {
					

					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120),
                    new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120)
					
                    
                                         };


            parameters[0].Value = model.firstAlarmName;
            parameters[1].Value = model.secondAlarmName;
            parameters[2].Value = model.thirdAlarmName;
            parameters[3].Value = model.point_name;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdateAlarm(string pointNameStr, NGN.Model.fixed_inclinometer_device model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update fixed_inclinometer_device set ");
            strSql.Append("FirstAlarmName=@FirstAlarmName,");
            strSql.Append("SecondAlarmName=@SecondAlarmName,");
            strSql.Append("ThirdAlarmName=@ThirdAlarmName");
            strSql.Append("   where   ");
            strSql.Append("point_name in ('" + pointNameStr + "')");
            OdbcParameter[] parameters = {
					

					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120)
					
                    
                                         };


            parameters[0].Value = model.firstAlarmName;
            parameters[1].Value = model.secondAlarmName;
            parameters[2].Value = model.thirdAlarmName;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




        /// <summary>
        /// 加载测斜链的所有测斜段
        /// </summary>
        /// <returns></returns>
        public bool ChainFixed_inclinometerLoad(string chain_name, int xmno, out List<string> devicenamelist)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select point_name from fixed_inclinometer_device where chain='" + chain_name + "' and xmno='" + xmno + "' ";
            devicenamelist = querysql.querystanderlist(sql, conn);
            return true;

        }

        /// <summary>
        /// 加载测斜链表
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select * from fixed_inclinometer_device where xmno='{3}'   {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }


        /// <summary>
        /// 从测斜仪的端口和设备号设置获取点名
        /// </summary>
        /// <returns></returns>
        public bool FixedInclinometerTOPointModule(int xmno, int port, string addressno, out NGN.Model.fixed_inclinometer_device model)
        {
            StringBuilder strSql = new StringBuilder(255);
            model = new Model.fixed_inclinometer_device();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append(" select  *   from     fixed_inclinometer_device    where   xmno  =  @xmno     and     port =   @port   and   deviceno =     @deviceno   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@port", OdbcType.Int),
                    new OdbcParameter("@deviceno", OdbcType.VarChar,500)
                                         };



            parameters[0].Value = xmno;
            parameters[1].Value = port;
            parameters[2].Value = addressno;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count == 0)
                return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;

        }

        public bool FixedInclinometerStateTabLoad(int startPageIndex, int pageSize, int xmno, string xmname,string unitname , string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select fixed_inclinometer_device.chain,fixed_inclinometer_device.point_name,CONCAT(fixed_inclinometer_device.deep,'m') as deep,if((select IFNULL(max(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from fixed_inclinometer_orgldata where fixed_inclinometer_orgldata.point_name = fixed_inclinometer_device.point_name  and  fixed_inclinometer_orgldata.xmno = fixed_inclinometer_device.xmno ) < DATE_ADD(SYSDATE(),INTERVAL -1 day),'false','true' ) as state,if(dtulp.LP=0||ISNULL(dtulp.LP),'false','true') as lp,CONCAT( IFNULL(dtutimetask.hour,0),'时',IFNULL(dtutimetask.minute,0),'分',IFNULL(dtutimetask.times,0),'次' ) as timeinterval ,(select max(time) from fixed_inclinometer_orgldata where fixed_inclinometer_orgldata.point_name = fixed_inclinometer_device.point_name  and fixed_inclinometer_orgldata.xmno = fixed_inclinometer_device.xmno  ) as lastdatatime,fixed_inclinometer_device.deviceno,fixed_inclinometer_device.address,fixed_inclinometer_device.`port`,'" + xmname + "' as xmname,'" + unitname + "' as unitname   from  fixed_inclinometer_device   left join dtutimetask on fixed_inclinometer_device.deviceno = dtutimetask.module left join dtulp on dtutimetask.module = dtulp.module    where fixed_inclinometer_device.xmno={3} {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno);
            Tool.ExceptionLog.ExceptionWrite(sql);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
		
        /*select fixed_inclinometer_device.point_name ,if((select IFNULL(max(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from fixed_inclinometer_orgldata where fixed_inclinometer_orgldata.point_name = fixed_inclinometer_device.point_name  and  fixed_inclinometer_orgldata.xmno = fixed_inclinometer_device.xmno ) < DATE_ADD(SYSDATE(),INTERVAL -1 day),'false','true' ) as state,if(dtulp.LP=0||ISNULL(dtulp.LP),'false','true') as lp,CONCAT( IFNULL(dtutimetask.hour,0),'时',IFNULL(dtutimetask.minute,0),'分',IFNULL(dtutimetask.times,0),'次' ) as timeinterval ,(select max(time) from fixed_inclinometer_orgldata where fixed_inclinometer_orgldata.point_name = fixed_inclinometer_device.point_name  and fixed_inclinometer_orgldata.xmno = fixed_inclinometer_device.xmno  ) as lastdatatime,dtumodule.addressno,dtumodule.od,dtumodule.port,'UTS1' as xmname,dtusenor.senorno,dtumodule.id  from fixed_inclinometer_device  left join dtumodule  on fixed_inclinometer_device.deviceno = dtumodule.id left join dtusenor  on dtumodule.senorno = dtusenor.senorno left join dtutimetask on dtumodule.addressno = dtutimetask.module left join dtulp on dtumodule.addressno = dtulp.module    where fixed_inclinometer_device.xmno=29 order by fixed_inclinometer_device.point_name asc limit 1,100*/





		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

