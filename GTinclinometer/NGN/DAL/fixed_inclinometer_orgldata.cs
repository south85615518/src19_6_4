﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_orgldata.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_orgldata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 8:51:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:fixed_inclinometer_orgldata
	/// </summary>
	public partial class fixed_inclinometer_orgldata
	{
        public static database db = new database();
		public fixed_inclinometer_orgldata()
		{}
		#region  BasicMethod

		

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(NGN.Model.fixed_inclinometer_orgldata model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("replace into fixed_inclinometer_orgldata(");
			strSql.Append("xmno,point_name,f,disp,time,taskid,chain_name,deep)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@point_name,@f,@disp,@time,@taskid,@chain_name,@deep)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@f", OdbcType.Double),
					new OdbcParameter("@disp", OdbcType.Double),
					new OdbcParameter("@time", OdbcType.DateTime),
                    new OdbcParameter("@taskid", OdbcType.VarChar,100),
					new OdbcParameter("@chain_name", OdbcType.VarChar,100),
                    new OdbcParameter("@deep", OdbcType.VarChar,100)

                                         };
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.f;
			parameters[3].Value = model.disp;
			parameters[4].Value = model.time;
            parameters[5].Value = model.taskid;
            parameters[6].Value = model.chain_name;
            parameters[7].Value = model.deep;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(NGN.Model.fixed_inclinometer_orgldata model)
		{
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("update fixed_inclinometer_orgldata set ");
			strSql.Append("f=@f,");
			strSql.Append("disp=@disp");
			strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@f", OdbcType.Double),
					new OdbcParameter("@disp", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)};
			parameters[0].Value = model.f;
			parameters[1].Value = model.disp;
			parameters[2].Value = model.xmno;
			parameters[3].Value = model.point_name;
			parameters[4].Value = model.time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string point_name,DateTime time)
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("delete from fixed_inclinometer_orgldata ");
			strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = xmno;
			parameters[1].Value = point_name;
			parameters[2].Value = time;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 删除临时表数据
        /// </summary>
        public bool Delete_tmp(int xmno,out int cont)
        {

            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("delete from fixed_inclinometer_orgldata_tmp ");
            strSql.Append(" where xmno=@xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
                                         };
            parameters[0].Value = xmno;
            cont = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (cont >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool GetModel(int xmno,string point_name,DateTime time,out NGN.Model.fixed_inclinometer_orgldata model )
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("select xmno,point_name,f,disp,time from fixed_inclinometer_orgldata ");
			strSql.Append("   where     xmno=@xmno    and   point_name=@point_name   and   time=@time  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@time", OdbcType.DateTime)			};
			parameters[0].Value = xmno;
			parameters[1].Value = point_name;
			parameters[2].Value = time;

			model=new NGN.Model.fixed_inclinometer_orgldata();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


        

        /// <summary>
        /// 得到一个测斜段对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,point_name,chain_name,disp,deep,time,f  from fixed_inclinometer_orgldata_tmp ");
            strSql.Append(" where    xmno=@xmno   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;

            lt = new List<NGN.Model.fixed_inclinometer_orgldata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        /// <summary>
        /// 得到一个测斜段对象实体列表
        /// </summary>
        public bool GetModelList(int xmno,string chainname,DateTime lasttime,out List<NGN.Model.fixed_inclinometer_orgldata> lt)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,point_name,chain_name,disp,deep,time,f  from fixed_inclinometer_orgldata ");
            strSql.Append(" where    xmno=@xmno  and chain_name = @chain_name  and  time = @time    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
                    new OdbcParameter("@chain_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmno;
            parameters[1].Value = chainname;
            parameters[2].Value = lasttime;
            lt = new List<NGN.Model.fixed_inclinometer_orgldata>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.fixed_inclinometer_orgldata DataRowToModel(DataRow row)
		{
			NGN.Model.fixed_inclinometer_orgldata model=new NGN.Model.fixed_inclinometer_orgldata();
			if (row != null)
			{
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["point_name"] != null && row["point_name"].ToString() != "")
                {
                    model.point_name = row["point_name"].ToString();
                }
                if (row["f"] != null && row["f"].ToString() != "")
                {
                    model.f = double.Parse(row["f"].ToString());
                }
                if (row["disp"] != null && row["disp"].ToString() != "")
                {
                    model.disp = double.Parse(row["disp"].ToString());
                }
                if (row["deep"] != null && row["deep"].ToString() != "")
                {
                    model.deep = double.Parse(row["deep"].ToString());
                }
                if (row["chain_name"] != null && row["chain_name"].ToString() != "")
                {
                    model.chain_name = row["chain_name"].ToString();
                }
                //model.f = row["f"].ToString();
                //model.disp = row["disp"].ToString();
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
                    model.datetimestr = row["time"].ToString();
				}
			}
			return model;
		}

        public bool PointMaxDateTimeGet(int xmno, out DateTime dt)
        {
            StringBuilder strSql = new StringBuilder(); OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select max(time) from fixed_inclinometer_orgldata ");
            strSql.Append("   where     xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)
						};
            parameters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(),parameters);
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }






		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

