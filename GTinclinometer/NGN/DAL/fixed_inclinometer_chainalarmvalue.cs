﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chainalarmvalue.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chainalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 15:48:55   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace NGN.DAL
{
	/// <summary>
	/// 数据访问类:fixed_inclinometer_chainalarmvalue
	/// </summary>
	public partial class fixed_inclinometer_chainalarmvalue
	{
        public static database db = new database();
		public fixed_inclinometer_chainalarmvalue()
		{}

		#region  BasicMethod

	


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(NGN.Model.fixed_inclinometer_chainalarmvalue model)
		{
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("insert into fixed_inclinometer_chainalarmvalue(");
			strSql.Append("xmno,alarmName,dispU,dispL)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@alarmName,@dispU,@dispL)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@alarmName", OdbcType.VarChar,100),
					new OdbcParameter("@dispU", OdbcType.Double),
					new OdbcParameter("@dispL", OdbcType.Double)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.alarmName;
			parameters[2].Value = model.dispU;
			parameters[3].Value = model.dispL;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(NGN.Model.fixed_inclinometer_chainalarmvalue model)
		{
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("update fixed_inclinometer_chainalarmvalue set ");
			strSql.Append("dispU=@dispU,");
			strSql.Append("dispL=@dispL");
			strSql.Append("    where    xmno=@xmno    and     alarmName=@alarmName  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@dispU", OdbcType.Double),
					new OdbcParameter("@dispL", OdbcType.Double),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@alarmName", OdbcType.VarChar,100)};
			parameters[0].Value = model.dispU;
			parameters[1].Value = model.dispL;
			parameters[2].Value = model.xmno;
			parameters[3].Value = model.alarmName;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string alarmName)
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("delete  from   fixed_inclinometer_chainalarmvalue ");
			strSql.Append(" where   xmno=@xmno    and    alarmName=@alarmName ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@alarmName", OdbcType.VarChar,100)			};
			parameters[0].Value = xmno;
			parameters[1].Value = alarmName;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool GetModel(int xmno,string alarmName,out NGN.Model.fixed_inclinometer_chainalarmvalue model )
		{
			
			StringBuilder strSql=new StringBuilder();OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("select xmno,alarmName,dispU,dispL from fixed_inclinometer_chainalarmvalue ");
			strSql.Append("    where    xmno=@xmno     and     alarmName=@alarmName   ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@alarmName", OdbcType.VarChar,100)			};
			parameters[0].Value = xmno;
			parameters[1].Value = alarmName;

			model=new NGN.Model.fixed_inclinometer_chainalarmvalue();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public NGN.Model.fixed_inclinometer_chainalarmvalue DataRowToModel(DataRow row)
		{
			NGN.Model.fixed_inclinometer_chainalarmvalue model=new NGN.Model.fixed_inclinometer_chainalarmvalue();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
                if (row["alarmName"] != null && row["alarmName"] != "")
				{
					model.alarmName=row["alarmName"].ToString();
				}
                if (row["dispL"] != null && row["dispL"] != "")
                {
                    model.dispL = double.Parse(row["dispL"].ToString());
                }
                if (row["dispU"] != null && row["dispU"] != "")
                {
                    model.dispU = double.Parse(row["dispU"].ToString());
                }
               


					//model.dispU=row["dispU"].ToString();
					//model.dispL=row["dispL"].ToString();
			}
			return model;
		}

        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);;
            string sql = string.Format("select xmno,alarmName,dispU,dispL from fixed_inclinometer_chainalarmvalue    where      xmno =  '" + xmno + "'    {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        ///// <summary>
        ///// 预警参数表记录数加载
        ///// </summary>
        ///// <param name="xmname"></param>
        ///// <param name="totalCont"></param>
        ///// <returns></returns>
        //public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        //{
        //    string sql = "select count(1) from dtualarmvalue where xmno = '" + xmno + "'";
        //    if (searchstring.Trim() != "1=1") sql = sql + "  and  " + searchstring;
        //    OdbcConnection conn = db.GetStanderConn(xmno);;
        //    string cont = querysql.querystanderstr(sql, conn);
        //    totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
        //    return true;
        //}

        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno, out string alarmValueNameStr)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);;
            string sql = "select distinct(alarmname) from fixed_inclinometer_chainalarmvalue where xmno = " + xmno + "";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            alarmValueNameStr = string.Join(";", lsFormat);
            return true;
        }

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

