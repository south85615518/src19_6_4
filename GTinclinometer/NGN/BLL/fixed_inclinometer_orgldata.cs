﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_orgldata.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_orgldata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 8:51:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using NGN.Model;
namespace NGN.BLL
{
	/// <summary>
	/// fixed_inclinometer_orgldata
	/// </summary>
	public partial class fixed_inclinometer_orgldata
	{
		private readonly NGN.DAL.fixed_inclinometer_orgldata dal=new NGN.DAL.fixed_inclinometer_orgldata();
		public fixed_inclinometer_orgldata()
		{}
		#region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_orgldata model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}测斜点名{1}采集时间{2}原始模数值为{3}位移值为{4}数据记录录入成功", model.xmno, model.point_name, model.time, model.f, model.disp);
                    return true;
                }
                else {
                    mssg = string.Format("项目编号{0}测斜点名{1}采集时间{2}原始模数值为{3}位移值为{4}的数据记录录入失败", model.xmno, model.point_name, model.time, model.f, model.disp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}测斜点名{1}采集时间{2}原始模数值为{3}位移值为{4}的数据记录录入出错,错误信息:"+ex.Message, model.xmno, model.point_name, model.time, model.f, model.disp);
                return false;
            }
        }
        ///// <summary>
        ///// 更新一条数据
        ///// </summary>
        //public bool Update(NGN.Model.fixed_inclinometer_orgldata model)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("update fixed_inclinometer_orgldata set ");
        //    strSql.Append("f=@f,");
        //    strSql.Append("disp=@disp");
        //    strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@f", OdbcType.Double),
        //            new OdbcParameter("@disp", OdbcType.Double),
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@point_name", OdbcType.VarChar,100),
        //            new OdbcParameter("@time", OdbcType.DateTime)};
        //    parameters[0].Value = model.f;
        //    parameters[1].Value = model.disp;
        //    parameters[2].Value = model.xmno;
        //    parameters[3].Value = model.point_name;
        //    parameters[4].Value = model.time;

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool Delete(int xmno, string point_name, DateTime time)
        //{

        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("delete from fixed_inclinometer_orgldata ");
        //    strSql.Append(" where xmno=@xmno and point_name=@point_name and time=@time ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@xmno", OdbcType.Int,11),
        //            new OdbcParameter("@point_name", OdbcType.VarChar,100),
        //            new OdbcParameter("@time", OdbcType.DateTime)			};
        //    parameters[0].Value = xmno;
        //    parameters[1].Value = point_name;
        //    parameters[2].Value = time;

        //    int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string point_name, DateTime time, out NGN.Model.fixed_inclinometer_orgldata model,out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(xmno, point_name, time, out model))
                {
                    mssg = string.Format("获取项目编号{0}测斜点名{1}采集时间{2}的模数值为{3}位移值为{4}", xmno, point_name, time, model.f, model.disp);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目编号{0}测斜点名{1}采集时间{2}的数据记录失败", xmno, point_name, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜点名{1}采集时间{2}的数据记录出错,错误信息:"+ex.Message, xmno, point_name, time);
                return false;
            }
        }

        /// <summary>
        /// 删除临时表数据
        /// </summary>
        public bool Delete_tmp(int xmno, out int cont,out string mssg)
        {
            cont = 0;
            mssg = "";
            try
            {
                if (dal.Delete_tmp(xmno, out cont))
                {
                    mssg = string.Format("删除项目编号{0}固定测斜临时表数据{1}条成功", xmno, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}固定测斜临时表数据失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}固定测斜临时表数据出错,错误信息:"+ex.Message, xmno);
                return false;
            }
            
        }


        /// <summary>
        /// 得到一个测斜段数据对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, out List<NGN.Model.fixed_inclinometer_orgldata> lt,out string mssg)
        {
            lt = null;
            mssg = "";
            try
            {
                if (dal.GetModelList(xmno, out lt))
                {
                    mssg = string.Format("成功获取项目编号{0}测斜段采集数据对象{1}条", xmno, lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("成功获取项目编号{0}测斜段采集数据对象列表失败", xmno, lt.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("成功获取项目编号{0}测斜段采集数据对象列表出错,错误信息:"+ex.Message, xmno, lt.Count);
                return false;
            }
        }

        /// <summary>
        /// 得到一个测斜段对象实体列表
        /// </summary>
        public bool GetModelList(int xmno, string chainname, DateTime lasttime, out List<NGN.Model.fixed_inclinometer_orgldata> lt,out string mssg)
        {
            mssg = "";
            lt = new List<Model.fixed_inclinometer_orgldata>();
            try
            {
                if (dal.GetModelList(xmno, chainname, lasttime, out lt))
                {
                    mssg = string.Format("成功获取项目编号{0}链名{1}时间{2}测斜数据实体{3}条", xmno, chainname, lasttime, lt.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}链名{1}时间{2}测斜数据实体失败", xmno, chainname, lasttime);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}链名{1}时间{2}测斜数据实体出错,错误信息："+ex.Message, xmno, chainname, lasttime);
                return false;
            }
        }

        public bool PointMaxDateTimeGet(int xmno,  out DateTime dt,out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointMaxDateTimeGet(xmno, out dt))
                {
                    mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间为{1}", xmno, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间失败", xmno);
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}测斜数据的最新采集时间出错，错误信息:"+ex.Message, xmno);
                return false;
            }

        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public NGN.Model.fixed_inclinometer_orgldata DataRowToModel(DataRow row)
        {
            NGN.Model.fixed_inclinometer_orgldata model = new NGN.Model.fixed_inclinometer_orgldata();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                //model.f=row["f"].ToString();
                //model.disp=row["disp"].ToString();
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
            }
            return model;
        }
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

