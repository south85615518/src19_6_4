﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chainalarmvalue.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chainalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 15:48:55   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using Maticsoft.Common;
using NGN.Model;
namespace NGN.BLL
{
	/// <summary>
	/// fixed_inclinometer_chainalarmvalue
	/// </summary>
	public partial class fixed_inclinometer_chainalarmvalue
	{
		private readonly NGN.DAL.fixed_inclinometer_chainalarmvalue dal=new NGN.DAL.fixed_inclinometer_chainalarmvalue();
		public fixed_inclinometer_chainalarmvalue()
		{}
		#region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_chainalarmvalue model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}测斜链预警名称{1}的信息录入成功", model.xmno, model.alarmName);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}测斜链预警名称{1}的信息录入失败", model.xmno, model.alarmName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}测斜链预警名称{1}的信息录入出错,错误信息:" + ex.Message, model.xmno, model.alarmName);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(NGN.Model.fixed_inclinometer_chainalarmvalue model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("项目编号{0}测斜链预警名称{1}的信息更新成功", model.xmno, model.alarmName);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}测斜链预警名称{1}的信息更新失败", model.xmno, model.alarmName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}测斜链预警名称{1}的信息更新出错,错误信息:" + ex.Message, model.xmno, model.alarmName);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int xmno, string alarmName, out string mssg)
        {
            try
            {
                if (dal.Delete(xmno, alarmName))
                {
                    mssg = string.Format("删除项目编号{0}测斜链预警名称{1}的信息成功", xmno, alarmName);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}测斜链预警名称{1}的信息失败", xmno, alarmName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}测斜链预警名称{1}的信息出错,错误信息:" + ex.Message, xmno, alarmName);
                return false;
            }

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string alarmName, out NGN.Model.fixed_inclinometer_chainalarmvalue model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, alarmName, out model))
                {
                    mssg = string.Format("获取项目编号{0}测斜链预警名称{1}的参数信息成功", xmno, alarmName);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}测斜链预警名称{1}的参数信息失败", xmno, alarmName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜链预警名称{1}的参数信息出错,错误信息:" + ex.Message, xmno, alarmName);
                return false;
            }

        }


        /// <summary>
        /// 测斜链预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out  dt))
                {
                    mssg = string.Format("成功获取到项目编号{0}的测斜链预警参数记录数{1}条", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的测斜链预警参数记录失败", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的测斜链预警参数记录出错,错误信息:" + ex.Message, xmno);
                return true;
            }
        }
        ///// <summary>
        ///// 测斜链预警参数表记录数加载
        ///// </summary>
        ///// <param name="xmname"></param>
        ///// <param name="totalCont"></param>
        ///// <returns></returns>
        //public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        //{
        //    string sql = "select count(1) from dtualarmvalue where xmno = '" + xmno + "'";
        //    if (searchstring.Trim() != "1=1") sql = sql + "  and  " + searchstring;
        //    OdbcConnection conn = db.GetStanderConn(xmno);
        //    string cont = querysql.querystanderstr(sql, conn);
        //    totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
        //    return true;
        //}

        /// <summary>
        /// 获取测斜链预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno, out string alarmValueNameStr, out string mssg)
        {
            alarmValueNameStr = "";
            mssg = "";
            try
            {
                if (dal.AlarmValueNameGet(xmno, out alarmValueNameStr))
                {
                    mssg = string.Format("获取到项目编号{0}的狱警参数名称选择项{1}成功", xmno, alarmValueNameStr);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}的狱警参数名称选择项失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}的狱警参数名称选择项出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

