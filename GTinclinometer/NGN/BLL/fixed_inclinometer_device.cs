﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_device.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_device
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/28 15:23:51   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using NGN.Model;
namespace NGN.BLL
{
	/// <summary>
	/// fixed_inclinometer_device
	/// </summary>
	public partial class fixed_inclinometer_device
	{
		private readonly NGN.DAL.fixed_inclinometer_device dal=new NGN.DAL.fixed_inclinometer_device();
		public fixed_inclinometer_device()
		{}
        #region  BasicMethod

        public bool Exist(string point_name, int xmno,out string mssg)
        {

            try
            {
                if (dal.Exist(point_name, xmno))
                {
                    mssg = string.Format("项目编号{0}已经存在{1}测斜点", xmno, point_name);
                    return true;
                }
                else {
                    mssg = string.Format("项目编号{0}不存在{1}测斜点", xmno, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}测斜点{1}的记录出错,错误信息:"+ex.Message, xmno, point_name);
                return false;
            }

        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_device model,out string mssg )
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("项目编号{0}测斜点名为{1}端口号{2}设备号{3}地址码{4}系数a:{5}系数b:{6}系数c:{7}系数d:{8}测斜链名为{9}的信息录入成功", model.xmno, model.point_name, model.port, model.deviceno, model.address, model.modulesA, model.modulesB, model.modulesC, model.modulesD, model.chain);
                    return true;
                }
                else {
                    mssg = string.Format("项目编号{0}测斜点名为{1}端口号{2}设备号{3}地址码{4}系数a:{5}系数b:{6}系数c:{7}系数d:{8}测斜链名为{9}的信息录入失败", model.xmno, model.point_name, model.port, model.deviceno, model.address, model.modulesA, model.modulesB, model.modulesC, model.modulesD, model.chain);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目编号{0}测斜点名为{1}端口号{2}设备号{3}地址码{4}系数a:{5}系数b:{6}系数c:{7}系数d:{8}测斜链名为{9}的信息录入出错,错误信息:"+ex.Message, model.xmno, model.point_name, model.port, model.deviceno, model.address, model.modulesA, model.modulesB, model.modulesC, model.modulesD, model.chain);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(NGN.Model.fixed_inclinometer_device model,out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}测斜点名{1}的端口号{2}设备号{3}地址码{4}系数a:{5}系数b:{6}系数c:{7}系数d:{8}测斜链名为{9}的信息成功", model.xmno, model.point_name, model.port, model.deviceno, model.address, model.modulesA, model.modulesB, model.modulesC, model.modulesD, model.chain);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}测斜点名{1}端口号{2}设备号{3}地址码{4}系数a:{5}系数b:{6}系数c:{7}系数d:{8}测斜链名为{9}的信息失败", model.xmno, model.point_name, model.port, model.deviceno, model.address, model.modulesA, model.modulesB, model.modulesC, model.modulesD, model.chain);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}测斜点名{1}端口号{2}设备号{3}地址码{4}系数a:{5}系数b:{6}系数c:{7}系数d:{8}测斜链名为{9}的信息出错,错误信息:" + ex.Message, model.xmno, model.point_name, model.port, model.deviceno, model.address, model.modulesA, model.modulesB, model.modulesC, model.modulesD, model.chain);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(NGN.Model.fixed_inclinometer_device model,out string mssg)
        {
            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("删除项目编号{0}测斜点{1}的记录成功", model.xmno, model.point_name);
                    return true;
                }
                else {
                    mssg = string.Format("删除项目编号{0}测斜点{1}的记录失败", model.xmno, model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}测斜点{1}的记录出错,错误信息："+ex.Message, model.xmno, model.point_name);
                return false;
            }
        }

        /// <summary>
        /// 清空关联的测斜链
        /// </summary>
        public bool DeleteChain(int xmno, string chainname,out string mssg)
        {
            try
            {
                if (dal.DeleteChain(xmno, chainname))
                {
                    mssg = string.Format("更新项目编号{0}关联测斜链名{1}的测斜点的测斜链信息成功", xmno, chainname);
                    return true;
                }
                else {
                    mssg = string.Format("更新项目编号{0}关联测斜链名{1}的测斜点的测斜链信息失败", xmno, chainname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}关联测斜链名{1}的测斜点的测斜链信息出错,错误信息:"+ex.Message, xmno, chainname);
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, int deviceno, int address, int port, out NGN.Model.fixed_inclinometer_device model,out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, deviceno, address, port, out model))
                {
                    mssg = string.Format("获取到项目编号{0}设备号{1}地址码{2}端口{3}的测斜点名为{4}", xmno, deviceno, address, port, model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}设备号{1}地址码{2}端口{3}的测斜点信息失败", xmno, deviceno, address, port);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}设备号{1}地址码{2}端口{3}的测斜点信息出错,错误信息:"+ex.Message, xmno, deviceno, address, port);
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetNGN_FixedInclinometerModel(int xmno, int deviceno, int address, int port, out NGN.Model.fixed_inclinometer_device model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetNGN_FixedInclinometerModel(xmno, deviceno, address, port, out model))
                {
                    mssg = string.Format("获取到设备号{0}地址码{1}端口{2}的项目编号为{3}测斜点名为{4}", deviceno, address, port,model.xmno,model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到设备号{0}地址码{1}端口{2}的项目编号,测斜点信息失败", deviceno, address, port);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到设备号{0}地址码{1}端口{2}的项目编号,测斜点信息出错,错误信息:" + ex.Message, deviceno, address, port);
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string point_name, out NGN.Model.fixed_inclinometer_device model, out string mssg)
        {
            mssg = "";
            model = null;
            try
            {
                if (dal.GetModel(xmno, point_name, out model))
                {
                    mssg = string.Format("获取项目编号{0}段名为{1}的测斜段成功", xmno, point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}段名为{1}的测斜段失败", xmno, point_name);
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}段名为{1}的测斜段出错,错误信息:" + ex.Message, xmno, point_name);
                return false;
            }

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(int xmno, string chain_name, out List<NGN.Model.fixed_inclinometer_device> modellist, out string mssg)
        {
            mssg = "";
            modellist = null;
            try
            {
                if (dal.GetModelList(xmno, chain_name, out modellist))
                {
                    mssg = string.Format("获取到项目编号{0}链名为{1}的测斜段数量{2}", xmno, chain_name,modellist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}链名为{1}的测斜段失败", xmno, chain_name);
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}链名为{1}的测斜段列表出错,错误信息:"+ex.Message, xmno, chain_name);
                return false;
            }

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public NGN.Model.fixed_inclinometer_device DataRowToModel(DataRow row)
        {
            NGN.Model.fixed_inclinometer_device model = new NGN.Model.fixed_inclinometer_device();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                if (row["deviceno"] != null && row["deviceno"].ToString() != "")
                {
                    model.deviceno = int.Parse(row["deviceno"].ToString());
                }
                if (row["modulesA"] != null && row["modulesA"].ToString() != "")
                {
                    model.modulesA = double.Parse(row["modulesA"].ToString());
                }
                if (row["modulesB"] != null && row["modulesB"].ToString() != "")
                {
                    model.modulesB = double.Parse(row["modulesB"].ToString());
                }
                if (row["modulesC"] != null && row["modulesC"].ToString() != "")
                {
                    model.modulesC = double.Parse(row["modulesC"].ToString());
                }
                if (row["modulesD"] != null && row["modulesD"].ToString() != "")
                {
                    model.modulesD = double.Parse(row["modulesD"].ToString());
                }
                if (row["Wheeldistance"] != null && row["Wheeldistance"].ToString() != "")
                {
                    model.Wheeldistance = double.Parse(row["Wheeldistance"].ToString());
                }
                if (row["chain"] != null && row["chain"].ToString() != "")
                {
                    model.chain = row["chain"].ToString();
                }
                if (row["address"] != null && row["address"].ToString() != "")
                {
                    model.address = int.Parse(row["address"].ToString());
                }
                if (row["port"] != null && row["port"].ToString() != "")
                {
                    model.port = int.Parse(row["port"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 关联多个测斜段
        /// </summary>
        public bool ChainMultiUpdate(string pointNameStr, NGN.Model.fixed_inclinometer_chain model,out string mssg)
        {
            try
            {
                if (dal.ChainMultiUpdate(pointNameStr, model))
                {
                    mssg = string.Format("关联测斜段{0}到测斜链{1}成功", pointNameStr, model.chain_name);
                    return true;
                }
                else {
                    mssg = string.Format("关联测斜段{0}到测斜链{1}失败", pointNameStr, model.chain_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("关联测斜段{0}到测斜链{1}出错,错误信息："+ex.Message, pointNameStr, model.chain_name);
                return false;
            }
        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateAlarm(NGN.Model.fixed_inclinometer_device model,out string mssg)
        {
            try
            {
                if (dal.UpdateAlarm(model))
                {
                    mssg = string.Format("更新项目编号{0}测斜点名{1}的预警参数成功", model.xmno, model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}测斜点名{1}的预警参数失败", model.xmno, model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}测斜点名{1}的预警参数出错,错误信息："+ex.Message, model.xmno, model.point_name);
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdateAlarm(string pointNameStr, NGN.Model.fixed_inclinometer_device model,out string mssg)
        {
            try
            {
                if (dal.MultiUpdateAlarm(pointNameStr,model))
                {
                    mssg = string.Format("批量更新项目编号{0}测斜点名{1}的预警参数成功", model.xmno, model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("批量更新项目编号{0}测斜点名{1}的预警参数失败", model.xmno, model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("批量更新项目编号{0}测斜点名{1}的预警参数出错,错误信息：" + ex.Message, model.xmno, model.point_name);
                return false;
            }
        }




        /// <summary>
        /// 加载测斜链的所有测斜段
        /// </summary>
        /// <returns></returns>
        public bool ChainFixed_inclinometerLoad(string chain_name, int xmno, out List<string> devicenamelist,out string mssg)
        {
            mssg = "";
            devicenamelist = null;
            try
            {
                if (dal.ChainFixed_inclinometerLoad(chain_name, xmno, out devicenamelist))
                {
                    mssg = string.Format("获取到项目{0}的测斜点名称有{1}个", xmno, devicenamelist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的测斜点名称失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的测斜点名称出错,错误信息:"+ex.Message, xmno);
                return false;
            }

        }

        /// <summary>
        /// 加载测斜链表
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.PointTableLoad(startPageIndex, pageSize, xmno, colName, sord, out dt))
                {
                    mssg = string.Format("获取到项目编号{0}的测斜点记录{1}条", xmno, dt.Rows.Count);
                    return true;
                }
                else {
                    mssg = string.Format("获取到项目编号{0}的测斜点记录失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}的测斜点记录出错,错误信息："+ex.Message, xmno);
                return false;
            }
        }

        /// <summary>
        /// 清空关联的预警参数
        /// </summary>
        public bool DeleteAlarmValue(int xmno, string alarmname, out string mssg)
        {
            try
            {
                if (dal.DeleteAlarmValue(xmno, alarmname))
                {
                    mssg = string.Format("更新项目编号{0}测斜点中关联了预警名称{1}的测斜点的预警参数名称成功", xmno, alarmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}测斜点中关联了预警名称{1}的测斜点的预警参数名称失败", xmno, alarmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}测斜点中关联了预警名称{1}的测斜点的预警参数名称出错,错误信息:" + ex.Message, xmno, alarmname);
                return false;
            }
        }

        /// <summary>
        /// 从测斜仪的端口和设备号设置获取点名
        /// </summary>
        /// <returns></returns>
        public bool FixedInclinometerTOPointModule(int xmno, int port, string addressno, out NGN.Model.fixed_inclinometer_device model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.FixedInclinometerTOPointModule(xmno, port, addressno, out model))
                {
                    mssg = string.Format("获取到项目编号{0}端口号{1}设备号{2}的一个测斜段名为{3}", xmno, port, addressno, model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}端口号{1}设备号{2}的测斜段名失败", xmno, port, addressno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}端口号{1}设备号{2}的测斜段名出错,错误信息:"+ex.Message, xmno, port, addressno);
                return false;
            }

        }

        public bool FixedInclinometerStateTabLoad(int startPageIndex, int pageSize, int xmno, string xmname,string unitname, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.FixedInclinometerStateTabLoad(startPageIndex, pageSize, xmno, xmname, unitname ,colName, sord, out dt))
                {
                    mssg = string.Format("获取项目{0}固定测斜设备信息记录数{1}成功", xmname, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}固定测斜设备信息失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}固定测斜设备信息出错,错误信息:"+ex.Message, xmname);
                return false;
            }

        }


        #endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

