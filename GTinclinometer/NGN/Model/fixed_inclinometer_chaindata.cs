﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chaindata.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chaindata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/30 16:58:19   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace NGN.Model
{
	/// <summary>
	/// fixed_inclinometer_chaindata:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fixed_inclinometer_chaindata
	{
		public fixed_inclinometer_chaindata()
		{}
		#region Model
		private int _xmno=0;
		private string _point_name;
		private double _disp;
		private DateTime _time= new DateTime();
        private string _taskid;
        /// <summary>
        /// 
        /// </summary>
        public string taskid
        {
            set { _taskid = value; }
            get { return _taskid; }
        }
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double disp
		{
			set{ _disp=value;}
			get{return _disp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		#endregion Model

	}
}

