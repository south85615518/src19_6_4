﻿/**  版本信息模板在安装目录下，可自行修改。
* fixed_inclinometer_chainalarmvalue.cs
*
* 功 能： N/A
* 类 名： fixed_inclinometer_chainalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/5/31 15:48:55   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace NGN.Model
{
	/// <summary>
	/// fixed_inclinometer_chainalarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fixed_inclinometer_chainalarmvalue
	{
		public fixed_inclinometer_chainalarmvalue()
		{}
		#region Model
		private int _xmno=0;
		private string _alarmname;
		private double _dispu;
		private double _displ;
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string alarmName
		{
			set{ _alarmname=value;}
			get{return _alarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dispU
		{
			set{ _dispu=value;}
			get{return _dispu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double dispL
		{
			set{ _displ=value;}
			get{return _displ;}
		}
		#endregion Model

	}
}

