﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using sensorDAL.DAL;

namespace sensorDAL.BLL
{
    public class InclinometerBLL
    {
        private readonly InclinometerDAL dal = new InclinometerDAL();
        public static database db = new database();
        public bool InclinometerPointLoadBLL(int xmno, out List<string> ls, out string mssg)
        {


            ls = null;
            try
            {
                if (dal.InclinometerPointLoadDAL(xmno, out ls))
                {
                    mssg = "测斜点号加载成功";
                    return true;

                }
                else
                {
                    mssg = "测斜点号加载失败";
                    return true;
                }

            }
            catch (Exception ex)
            {
                mssg = "测斜点号加载出错，错误信息" + ex.Message;
                return true;
            }



        }
    }
}
