﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using sensorDAL.DAL;

namespace sensorDAL.BLL
{
    public class SwBLL
    {
        private readonly SwDAL dal = new SwDAL();
        public static database db = new database();
        public bool SwPointLoadBLL(string xmname,out List<string> ls,out string mssg)
        {

        
            ls = null;
            try
            {
                if (dal.SwPointLoadDAL(xmname, out ls))
                {
                    mssg = "水位点号加载成功";
                    return true;

                }
                else
                {
                    mssg = "水位点号加载失败";
                    return true;
                }

            }
            catch (Exception ex)
            {
                mssg = "水位点号加载出错，错误信息"+ex.Message;
                return true;
            }

        

        }
    }
}
