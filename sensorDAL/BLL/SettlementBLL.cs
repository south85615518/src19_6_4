﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using sensorDAL.DAL;

namespace sensorDAL.BLL
{
    public class SettlementBLL
    {
        private readonly SettlementDAL dal = new SettlementDAL();
        public static database db = new database();
        public bool SettlementPointLoadBLL(string xmname, out List<string> ls, out string mssg)
        {


            ls = null;
            try
            {
                if (dal.SettlementPointLoadDAL(xmname, out ls))
                {
                    mssg = "沉降点号加载成功";
                    return true;

                }
                else
                {
                    mssg = "沉降点号加载失败";
                    return true;
                }

            }
            catch (Exception ex)
            {
                mssg = "沉降点号加载出错，错误信息" + ex.Message;
                return true;
            }



        }
    }
}
