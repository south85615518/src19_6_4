﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using sensorDAL.DAL;

namespace sensorDAL.BLL
{
    public class StrainBLL
    {
        private readonly StrainDAL dal = new StrainDAL();
        public static database db = new database();
        public bool StrainPointLoadBLL(string xmname, out List<string> ls, out string mssg)
        {


            ls = null;
            try
            {
                if (dal.StrainPointLoadDAL(xmname, out ls))
                {
                    mssg = "应力点号加载成功";
                    return true;

                }
                else
                {
                    mssg = "应力点号加载失败";
                    return true;
                }

            }
            catch (Exception ex)
            {
                mssg = "应力点号加载出错，错误信息" + ex.Message;
                return true;
            }



        }
    }
}
