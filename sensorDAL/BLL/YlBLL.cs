﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using sensorDAL.DAL;

namespace sensorDAL.BLL
{
    public class YlBLL
    {
        private readonly YlDAL dal = new YlDAL();
        public static database db = new database();
        public bool YlPointLoadBLL(string xmname, out List<string> ls, out string mssg)
        {


            ls = null;
            try
            {
                if (dal.YlPointLoadDAL(xmname, out ls))
                {
                    mssg = "雨量点号加载成功";
                    return true;

                }
                else
                {
                    mssg = "雨量点号加载失败";
                    return true;
                }

            }
            catch (Exception ex)
            {
                mssg = "雨量点号加载出错，错误信息" + ex.Message;
                return true;
            }



        }
    }
}
