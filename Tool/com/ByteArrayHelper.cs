﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.com
{
    public class ByteArrayHelper
    {
        public static List<byte[]> ByteArrayToArrayList(byte[] bytes, byte[] splitor)
        {

            List<byte[]> lb = new List<byte[]>();
            int i = 0, j = 0;
            bool splitorarray = false;
            int len = bytes.Count(), splitorlen = splitor.Count();
            List<byte> data = new List<byte>();
            for (i = 0; i < len; i++)
            {

                for (j = 0; j < splitorlen; j++)
                {

                    if (bytes[i + j] == splitor[j])
                    {
                        splitorarray = true;
                        continue;
                    }
                    splitorarray = false;
                    break;

                }
                if (splitorarray)
                {
                    if (i > splitor.Count())
                    {
                        string datastr = Tool.com.HexHelper.ByteToHexString(data.ToArray());
                        lb.Add(HexHelper.ByteTrim(data.ToArray(), new byte[2] { 0xff, 0xff }, new byte[2] { 0xff, 0xff }));
                        data = new List<byte>();

                    }


                    i += splitorlen - 1;
                    data.AddRange(splitor.ToList());

                }
                else
                {
                    data.Add(bytes[i]);

                }
            }

            lb.Add(HexHelper.ByteTrim(data.ToArray(),null, new byte[2] { 0xff, 0xff }));
            return lb;
        }
    }
}
