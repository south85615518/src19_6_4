﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.com
{
    public class descodehelper
    {
        //gzddygly     123456
        public string descodeusername(string username, string pass)
        {

            int codeindex = (username.Length-1) > (pass.Length-1) ? (username.Length-1) % (pass.Length-1) : (pass.Length-1) % (username.Length-1);
            List<string> lcharhead = new List<string>();
            List<string> lchar = new List<string>();
            int headstartindex = username.Length - codeindex - 1;
            int startindex = 0;
            
            for (; headstartindex < username.Length-1; headstartindex++)
            {
                lcharhead.Add(username[headstartindex].ToString());
            }
            for (; startindex < username.Length - codeindex - 1; startindex++)
            {
                lchar.Add(username[startindex].ToString());
            }

            lcharhead.Reverse();
            lchar.Reverse();
            return string.Join("", lcharhead) + string.Join("", lchar);


        }

        public string descodepass(string username, string pass)
        {

            int codeindex = username.Length > pass.Length ? (username.Length-1) % (pass.Length-1) : (pass.Length-1) % (username.Length-1);
            List<string> lcharhead = new List<string>();
            List<string> lchar = new List<string>();
            int headstartindex = pass.Length - codeindex - 1;
            int startindex = 0;

            for (; headstartindex < pass.Length-1; headstartindex++)
            {
                lcharhead.Add(pass[headstartindex].ToString());
            }
            for (; startindex < pass.Length - codeindex - 1; startindex++)
            {
                lchar.Add(pass[startindex].ToString());
            }

            lcharhead.Reverse();
            lchar.Reverse();
            return string.Join("", lcharhead) + string.Join("", lchar);


        }


        public string encodeusername(string username,string pass)
        {

            int codeindex = username.Length > pass.Length ? username.Length % pass.Length : pass.Length % username.Length;
            List<string> lcharhead = new List<string>();
            List<string> lchar = new List<string>();
            int headstartindex =  codeindex;
            int startindex = 0;

            for (; headstartindex < username.Length; headstartindex++)
            {
                lcharhead.Add(username[headstartindex].ToString());
            }
            for (; startindex <  codeindex; startindex++)
            {
                lchar.Add(username[startindex].ToString());
            }

            lcharhead.Reverse();
            lchar.Reverse();
            return string.Join("", lcharhead) + string.Join("", lchar)+(char)(codeindex%26+Convert.ToInt32('a'));

        }

        public string encodepass(string username, string pass)
        {

            int codeindex = username.Length > pass.Length ? username.Length % pass.Length : pass.Length % username.Length;
            List<string> lcharhead = new List<string>();
            List<string> lchar = new List<string>();
            int headstartindex = codeindex;
            int startindex = 0;

            for (; headstartindex < pass.Length; headstartindex++)
            {
                lcharhead.Add(pass[headstartindex].ToString());
            }
            for (; startindex < codeindex; startindex++)
            {
                lchar.Add(pass[startindex].ToString());
            }

            lcharhead.Reverse();
            lchar.Reverse();
            return string.Join("", lcharhead) + string.Join("", lchar) + codeindex % 9;

        }

        public string JsonDataCode(string jsonstr,char usercodekey)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(jsonstr);
            //bytesprint(bytes);
            List<byte> bytelist = new List<byte>();
            foreach (byte b in bytes)
            {
                bytelist.Add((byte)(Convert.ToInt32(b)+Convert.ToInt32(usercodekey)));
            }
            //bytesprint(bytelist);
            byte[] crcbyte = CRC16Helper.CRC16(bytelist.ToArray());
            return string.Join(",", crcbyte);
        }
        public string JsonDataDescode(string codejsonstr,char usercodekey)
        {
            string mssg = "";
            string[] strarry = codejsonstr.Split(',');
            List<byte> bytelistsrc = new List<byte>();
            foreach (string str in strarry)
            {
                bytelistsrc.Add((byte)Convert.ToInt32(str));
            }
            //bytesprint(bytelistsrc);

            if (!Tool.com.CRC16Helper.IsDataCorrectCRC16(bytelistsrc.ToArray(), out mssg)) return "";
            int i = 0;
            List<byte> bytelist = new List<byte>();
            foreach (byte b in bytelistsrc)
            {
                if (i == bytelistsrc.Count - 2) break;
                bytelist.Add((byte)(Convert.ToInt32(b) - Convert.ToInt32(usercodekey)));
                i++;
            }
            //bytesprint(bytelist);
            string jsonstr = Encoding.UTF8.GetString(bytelist.ToArray());
            return jsonstr;
        }
        public void bytesprint(byte[] bytes)
        {
            int i = 0;
            foreach(var b in bytes)
            {
                if (i == 0||i == 10) Console.Write("\n");
                Console.Write(Convert.ToInt32(b)+" ");
                i++;
            }
            Console.Write("\n");
        }
        public void bytesprint(List<byte> bytes)
        {
            int i = 0;
            foreach (var b in bytes)
            {
                if (i == 10) Console.Write("\n");
                Console.Write(Convert.ToInt32(b)+" ");
                i++;
            }
            Console.Write("\n");
        }
    }
}
