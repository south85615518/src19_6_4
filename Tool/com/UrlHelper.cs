﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Text.RegularExpressions;

namespace Tool.com
{
    public class UrlHelper
    {
        public static Dictionary<string, string> GetRequestQueryStringFromUrl(string url)
        {
            Dictionary<string, string> dss = new Dictionary<string, string>();
            if (url.IndexOf("?") == -1) return dss;
            
            try
            {
                
                string paramterstring = url.Substring(url.LastIndexOf("?"));
                List<string> paramters = paramterstring.Split('&').ToList();
                paramters.ForEach(m => {
                    Match match = Regex.Match(m, "(?<参数名>\\w+\\d*)\\s*=\\s*(?<值>\\w*\\d*)");
                    if (match.Success)
                    {
                        dss.Add(match.Groups["参数名"].ToString(), match.Groups["值"].ToString());
                    }
                });
                ExceptionLog.ExceptionWrite("从URL中提取到请求参数数量"+dss.Count);
                return dss;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("从URL中提取请求参数出错,错误信息:" + ex.Message);
                return dss;
            }


        }
    }
}
