﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.com
{
   public class Offset
    {
       public string pointname { get; set; }
       public double offsetX { get; set; }
       public double offsetY { get; set; }
       public double offsetZ { get; set; }
    }
   public class OffsetHelper
   {
       public static List<Offset> OffsetModelListLoad(List<string> offsetstringList )
       {
           List<Offset> loffset = new List<Offset>();
           Offset offset = new Offset();
           foreach(string offsetstring in offsetstringList)
           {
               string[] strs = offsetstring.Split('\t');
               if (strs.Length == 3 && strs[0] != "") { if (offset.pointname!=null)loffset.Add(offset); offset = new Offset(); offset.pointname = strs[0]; offset.offsetX = double.Parse(strs[2]); continue; }
               if (strs[1].ToLower().IndexOf("y") != -1) offset.offsetY = double.Parse(strs[2]);
               if (strs[1].ToLower().IndexOf("z") != -1) offset.offsetZ = double.Parse(strs[2]);
           }
           loffset.Add(offset);
           return loffset;
       }

       public static DataTable ProcessTotalStationResultDataOffset(DataTable DataTableSrc ,string offsetpath)
       {

           List<string> ls = Tool.FileHelper.ProcessFileInfoList(offsetpath);
           List<Tool.com.Offset> offsetlist = Tool.com.OffsetHelper.OffsetModelListLoad(ls);

           DataView dv = new DataView(DataTableSrc);

           foreach (Offset offset in offsetlist)
           {
               dv.RowFilter = "point_name ='"+offset.pointname+"'";

               foreach(DataRowView drv  in dv )
               {
                   drv["N"] = Convert.ToDouble(drv["N"]) + offset.offsetX/1000;
                   drv["E"] = Convert.ToDouble(drv["E"]) + offset.offsetY/1000;
                   drv["Z"] = Convert.ToDouble(drv["Z"]) + offset.offsetZ/1000;

                   //drv["This_dN"] = Convert.ToDouble(drv["This_dN"]);
                   //drv["This_dE"] = Convert.ToDouble(drv["This_dE"]);
                   //drv["This_dZ"] = Convert.ToDouble(drv["This_dZ"]);

                   //drv["Ac_dN"] = Convert.ToDouble(drv["Ac_dN"]);
                   //drv["Ac_dE"] = Convert.ToDouble(drv["Ac_dE"]);
                   //drv["Ac_dZ"] = Convert.ToDouble(drv["Ac_dZ"]);
               }
           }
           dv.RowFilter = "1=1";
           return dv.ToTable();
       }

       

   }
}
