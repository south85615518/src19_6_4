﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Tool.com
{
    public class StringHelper
    {
        public static string GetNumFromString(string strsrc)
        {
            string filename = Path.GetFileNameWithoutExtension(strsrc);
            List<char> charlist = new List<char>();
            int i = 0;
            //char ch = filename[filename.Length - 1 - i];
            //int d = ch - '0';
            while ((filename[filename.Length - 1 - i] - '0') > -1 && (filename[filename.Length - 1 - i] - '0') < 10)
            {
                charlist.Insert(0, filename[filename.Length - 1 - i]);
                i++;
            }
            return new string(charlist.ToArray());
        }
        public static bool SameExSort(List<string> ls,out List<string> lssort)
        {
            List<string> lssorttmp = new List<string>();
            string extmp = "";
            List<int> indexList = new List<int>();
            lssort = null;
            try
            {
                foreach (var str in ls)
                {

                    if (extmp == "")
                    {
                        extmp = str.Substring(0, str.IndexOf("-"));
                        indexList.Add(Convert.ToInt32(str.Substring(str.LastIndexOf("-")+1)));
                        continue;
                    }
                    if (extmp != str.Substring(0, str.IndexOf("-")))
                    {
                        indexList.Sort();
                        indexList.ForEach(m => { lssorttmp.Add(extmp + "-" + m); });
                        extmp = str.Substring(0, str.IndexOf("-"));
                        indexList = new List<int>();

                    }
                    indexList.Add(Convert.ToInt32(str.Substring(str.LastIndexOf("-")+1)));


                }
                indexList.Sort();
                indexList.ForEach(m => { lssorttmp.Add(extmp + "-" + m); });
                
            }
            catch(Exception ex)
            {
                return false;
            }
            lssort = lssorttmp;
            return true;
        }
    }
}
