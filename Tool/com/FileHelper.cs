﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;



namespace Tool
{
    public class FileHelper
    {
        public static string ProcessSettingString(string settingpath)
        {
            FileStream fs = null;
            StreamReader sr = null;
            string ipstr = "";
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(settingpath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(settingpath));
                    return "";
                }
            TxtEncoding.GetType(settingpath);    
            fs = new FileStream(settingpath, FileMode.Open, FileAccess.Read, FileShare.Read );
            sr = new StreamReader(fs, TxtEncoding.Encoding);
                ipstr = sr.ReadLine();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
                if (sr != null)
                    sr.Close();
            }
            return ipstr;

        }


        public static string ProcessStringRead(string settingpath)
        {
            FileStream fs = null;
            StreamReader sr = null;
            string ipstr = "";
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(settingpath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(settingpath));
                    return "";
                }
                TxtEncoding.GetType(settingpath);
                fs = new FileStream(settingpath, FileMode.Open, FileAccess.Read, FileShare.Read);
                sr = new StreamReader(fs, TxtEncoding.Encoding);
                ipstr = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
                if (sr != null)
                    sr.Close();
            }
            return ipstr;

        }


        public static string ProcessGBKSettingString(string settingpath)
        {
            FileStream fs = File.Open(settingpath, FileMode.Open, FileAccess.Read, FileShare.Read);
            StreamReader sr = new StreamReader(fs, Encoding.Default);
            string ipstr = "";
            try
            {
                //sr = File.OpenText(settingpath);
                ipstr = sr.ReadLine();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
            }
            finally
            {

                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
            }
            return ipstr;

        }



        public static List<string> ProcessFileInfoList(string settingpath)
        {
            List<string> ls = new List<string>();
            TxtEncoding.GetType(settingpath);
            FileStream fs = null;
            StreamReader sr = null;
            string tmpstr = "";
            string ipstr = "";
            try
            {
                //sr = File.OpenText(settingpath);
                if (!Directory.Exists(Path.GetDirectoryName(settingpath))) { Directory.CreateDirectory(Path.GetDirectoryName(settingpath)); return ls; }
                fs = new FileStream(settingpath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
                sr = new StreamReader(fs, TxtEncoding.Encoding);
                //ipstr = sr.ReadLine();
                while (!sr.EndOfStream)
                {
                    ipstr = sr.ReadLine();
                    tmpstr = ipstr;
                    //if (ipstr.IndexOf("\0") == -1)
                    if (ipstr.IndexOf("\0") != -1)
                        ls.Add(ipstr.Substring(0, ipstr.IndexOf("\0")));
                    else
                        ls.Add(ipstr);
                   
                }
                if (ipstr == null) return ls;


                return ls;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
                return ls;
            }
            finally
            {
                if (sr != null)
                    sr.Close();

            }


        }
        public static void FileInfoListWrite(List<string> ls, string settingpath)
        {
            //List<string> ls = new List<string>();
            FileStream fs = null;
            if(File.Exists(settingpath))
            fs = new FileStream(settingpath, FileMode.Truncate, FileAccess.Write, FileShare.Read);
            else
            fs = new FileStream(settingpath, FileMode.CreateNew, FileAccess.Write, FileShare.Read);
            fs.Close();
            TxtEncoding.GetType(settingpath);
            fs = new FileStream(settingpath, FileMode.Open, FileAccess.Write, FileShare.Read);
            StreamWriter sr = new StreamWriter(fs, TxtEncoding.Encoding);
            string tmpstr = "";
            string ipstr = "";
            try
            {
                //sr = File.OpenText(settingpath);
                //ipstr = sr.ReadLine();
                foreach (string item in ls)
                {
                    sr.WriteLine(item);
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
                //return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();

            }
        }


       


        public static void FileStringWrite(string str, string settingpath)
        {
            //List<string> ls = new List<string>();
            FileStream fs = null;
            if (!Directory.Exists(Path.GetDirectoryName(settingpath))) Directory.CreateDirectory(Path.GetDirectoryName(settingpath));
            if (File.Exists(settingpath))
                fs = new FileStream(settingpath, FileMode.Truncate, FileAccess.Write, FileShare.Read);
            else
                fs = new FileStream(settingpath, FileMode.CreateNew, FileAccess.Write, FileShare.Read);
            fs.Close();
            TxtEncoding.GetType(settingpath);
            fs = new FileStream(settingpath, FileMode.Open, FileAccess.Write, FileShare.Read);
            StreamWriter sr = new StreamWriter(fs, TxtEncoding.Encoding);
            string tmpstr = "";
            string ipstr = "";
            try
            {
                
                    sr.WriteLine(str);
                
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
                //return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();

            }
        }

        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void FileStringAppendWrite(string sql, string path)
        {
            string tx = string.Format("{0}\r\n{1}\r\n", DateTime.Now.ToString(), sql);
            string realPath = path;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(realPath));
                }
                fs = File.Open(realPath , FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }


        public static string ProcessTxtLog(string settingpath)
        {
            //List<string> ls = new List<string>();
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                StringBuilder strbuilder = new StringBuilder();
                //TxtEncoding.GetType(settingpath);
                fs = new FileStream(settingpath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                sr = new StreamReader(fs, Encoding.UTF8);
                string ipstr = "";

                //sr = File.OpenText(settingpath);
                ipstr = sr.ReadLine();
                string tmpstr = "";
                strbuilder.Append(ipstr);
                while (ipstr != null)
                {

                    tmpstr = ipstr;

                    ipstr = sr.ReadLine();
                    if (tmpstr == null && ipstr == null) break;
                    strbuilder.Insert(0, ipstr + "\r\n");
                }
                return strbuilder.ToString();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
                return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();

            }


        }

        public static void FileCopy(string dirpath, string unionpath)
        {
            string[] filenames = Directory.GetFiles(dirpath);
            //TxtEncoding.GetType(unionpath);
            FileStream fs = File.Open(unionpath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

            StreamWriter sw = null;
            sw = new StreamWriter(fs, Encoding.Default);
            fs.Seek(0, SeekOrigin.End);
            try
            {

                foreach (var filename in filenames)
                {
                    if (filename.IndexOf(".sql") == -1) continue;
                    List<string> ls = ProcessFileInfoList(filename);

                    var encoding = sw.Encoding;
                    sw.WriteLine("/************************************" + filename + "***********************************/");
                    foreach (var txtline in ls)
                    {
                        sw.WriteLine(txtline);
                    }

                }
            }
            catch (Exception ex)
            {
                if (fs != null)
                {
                    sw.Close();
                    fs.Close();

                }
            }
            finally
            {
                if (fs != null)
                {
                    sw.Close();
                    fs.Close();

                }
            }

        }

        public static void Copy(string srcpath, string destpath)
        {
            if (!Directory.Exists(Path.GetDirectoryName(destpath))) Directory.CreateDirectory(Path.GetDirectoryName(destpath));
            try
            {
                File.Copy(srcpath,destpath,true);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("将文件{0}拷贝到{1}出错，错误信息："+ex.Message,srcpath,destpath));
            }

        }

        public static string ProcessHtmlTxtLog(string settingpath)
        {
            //List<string> ls = new List<string>();
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                StringBuilder strbuilder = new StringBuilder();
                //TxtEncoding.GetType(settingpath);
                fs = new FileStream(settingpath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                sr = new StreamReader(fs, Encoding.UTF8);
                string ipstr = "";

                //sr = File.OpenText(settingpath);
                ipstr = sr.ReadLine();
                string tmpstr = "";
                strbuilder.Append(ipstr);
                while (ipstr != null)
                {

                    tmpstr = ipstr;

                    ipstr = sr.ReadLine();
                    if (tmpstr == null && ipstr == null) break;
                    strbuilder.Insert(0, "<p>" + ipstr);
                }
                return strbuilder.ToString();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
                return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();

            }


        }


        public static string unZipFile(string TargetFile, string fileDir)
        {
            //if (Directory.Exists(fileDir)) { Directory.Move(fileDir, fileDir + DateTime.Now.ToFileTime()); }
            ///int i = 0;
            //while (Directory.Exists(fileDir + "\\" + TargetFile.Substring(TargetFile.LastIndexOf("\\"), TargetFile.LastIndexOf(".zip") - TargetFile.LastIndexOf("\\"))))
            //{
            //    i++;

            //}
            //fileDir = fileDir + "\\" + TargetFile.Substring(TargetFile.LastIndexOf("\\"), TargetFile.LastIndexOf(".zip") - TargetFile.LastIndexOf("\\"));
            //Directory.CreateDirectory(fileDir);
            ICSharpCode.SharpZipLib.Zip.ZipInputStream s = null;
            string rootFile = " ";
            try
            {
                //读取压缩文件(zip文件)，准备解压缩
               s = new ICSharpCode.SharpZipLib.Zip.ZipInputStream(File.OpenRead(TargetFile.Trim()));
                ICSharpCode.SharpZipLib.Zip.ZipEntry theEntry;
                string path = fileDir;
                //解压出来的文件保存的路径

                string rootDir = " ";
                
                


                //根目录下的第一个子文件夹的名称
                while ((theEntry = s.GetNextEntry()) != null)
                {
                    //ExceptionLog.ExceptionWrite(theEntry.Name);
                    rootDir = Path.GetDirectoryName(theEntry.Name);
                    //得到根目录下的第一级子文件夹的名称
                    if (rootDir.IndexOf("\\") >= 0)
                    {
                        rootDir = rootDir.Substring(0, rootDir.IndexOf("\\") + 1);
                    }
                    string dir = Path.GetDirectoryName(theEntry.Name);
                    //根目录下的第一级子文件夹的下的文件夹的名称
                    string fileName = Path.GetFileName(theEntry.Name);
                    //根目录下的文件名称
                    if (dir != " ")
                    //创建根目录下的子文件夹,不限制级别
                    {
                        if (!Directory.Exists(fileDir + "\\" + dir))
                        {
                            path = fileDir + "\\" + dir;
                            //在指定的路径创建文件夹
                            Directory.CreateDirectory(path);
                        }
                    }
                    else if (dir == " " && fileName != "")
                    //根目录下的文件
                    {
                        path = fileDir;
                        rootFile = fileName;
                    }
                    else if (dir != " " && fileName != "")
                    //根目录下的第一级子文件夹下的文件
                    {
                        if (dir.IndexOf("\\") > 0)
                        //指定文件保存的路径
                        {
                            path = fileDir + "\\" + dir;
                        }
                    }

                    if (dir == rootDir)
                    //判断是不是需要保存在根目录下的文件
                    {
                        path = fileDir + "\\" + rootDir;
                    }

                    //以下为解压缩zip文件的基本步骤
                    //基本思路就是遍历压缩文件里的所有文件，创建一个相同的文件。
                    if (fileName != String.Empty&&fileName.IndexOf("lpk.dll") == -1)
                    {
                        if (File.Exists(path + "\\" + fileName)) continue;
                        FileStream streamWriter = File.Create(path + "\\" + fileName);

                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }

                        streamWriter.Close();
                    }
                }
                s.Close();

                return rootFile;
            }
            catch (Exception ex)
            {
                if(s!=null)
                s.Close();
                ExceptionLog.ExceptionWrite(string.Format("解压{0}出错，错误信息:" + ex.Message, TargetFile));
                return "1; " + ex.Message;
            }
        }
        /// <summary>
        /// 遍历文件夹下的所有文件
        /// </summary>
        public static List<string> DirTravel(string dirpath)
        {

            List<string> filenameList = new List<string>();
            string[] dirs = Directory.GetDirectories(dirpath);
            
            string[] files = Directory.GetFiles(dirpath);
            filenameList.AddRange(files.ToList());
            foreach (string dir in dirs)
            {
                if (Directory.Exists(dir))
                {
                    //List<string> filenamelist = new List<string>();
                    filenameList.AddRange(DirTravel(dir));

                }

            }


            return FileSort(filenameList);
        }

        //public static List<string> DirTravelFileQuery(string dirpath,DateTime starttime,DateTime endtime)
        //{

        //    List<string> filenameList = new List<string>();
        //    string[] dirs = Directory.GetDirectories(dirpath);

        //    string[] files = Directory.GetFiles(dirpath);

        //    filenameList.AddRange(files.ToList());
        //    foreach (string dir in dirs)
        //    {
        //        if (Directory.Exists(dir))
        //        {
        //            //List<string> filenamelist = new List<string>();
        //            filenameList.AddRange(DirTravel(dir));

        //        }

        //    }


        //    return FileSort(filenameList);
        //}


        public static List<string>  FileSort(List<string> filenames)
        {
            List<SortFile> SortFiles = new List<SortFile>();
            int sec = 0;
            foreach(var filename in filenames )
            {
                 try{

                     SortFiles.Add(new  SortFile(  File.GetLastWriteTime(filename), filename));
                 }
                catch(Exception ex)
                {

                }
            
            }
            var filemodellist = (from m in SortFiles orderby m.dt select m.filename).ToList();
            return filemodellist;
        }
        public class SortFile{
            public DateTime dt { get; set; }
            public string filename { get; set; }
            public SortFile(DateTime dt, string filename)
            {
                this.dt = dt;
                this.filename = filename;
            }
        }


        

        public static void FileZip(string dirpath)
        {
            try
            {
                string[] filenames = Directory.GetFiles(dirpath);
                using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile(string.Format(dirpath + "/" + "日志文件备份{0}年_{1}月_{2}日_{3}时_{4}分.zip", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute), System.Text.Encoding.UTF8))//解决中文乱码问题  
                {
                    foreach (string path in filenames)
                    {
                        if (Path.GetExtension(path) != ".txt") continue;
                        string currentDirectory = Directory.GetCurrentDirectory();
                        string dir = path.Substring(0, path.LastIndexOf("\\"));
                        Directory.SetCurrentDirectory(dir);
                        string fileName = path.Substring(path.LastIndexOf("\\") + 1);
                        zip.AddFile(fileName);
                        Directory.SetCurrentDirectory(currentDirectory);
                        //break;
                    }

                    //this.Response.Headers["Content-Length"] = ;
                    zip.Save();

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(dirpath + "日志文件清理出错，错误信息:" + ex.Message);
            }
            finally
            {
                Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            }

        }

        public static void FileZipPack(string filepath)
        {
            try
            {
                //string[] filenames = Directory.GetFiles(dirpath);
                using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile( string.Format("{0}.zip", filepath.Substring(0,filepath.LastIndexOf("."))),Encoding.UTF8))//解决中文乱码问题  
                {
                    //foreach (string path in null)
                    //{
                    //    if (Path.GetExtension(path) != ".txt") continue;
                    string currentDirectory = Path.GetDirectoryName(filepath);
                    //string dir = filepath.Substring(0, filepath.LastIndexOf("\\"));
                    //Directory.SetCurrentDirectory(dir);
                    Directory.SetCurrentDirectory(currentDirectory);
                    string fileName =Path.GetFileName(filepath);
                    zip.AddFile(fileName);
                    
                    //    //break;
                    //}
                    //zip.AddFile(filepath);
                    //this.Response.Headers["Content-Length"] = ;
                    zip.Save();

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("" + "托管报表打包出错，错误信息:" + ex.Message);
            }

        }

        public static void DirZipPack(string dirpath)
        {
            try
            {
                string[] filenames = Directory.GetFiles(dirpath);
                using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile(string.Format("{0}.zip",dirpath), System.Text.Encoding.UTF8))//解决中文乱码问题  
                {
                    int i = 0;
                    string currentDirectory = Directory.GetCurrentDirectory();
                    foreach (string path in filenames)
                    {
                        try
                        {
                            if (Path.GetExtension(path) != ".xlsx" || path.IndexOf("$") != -1 ) continue;

                            string dir = path.Substring(0, path.LastIndexOf("\\"));
                            Directory.SetCurrentDirectory(dir);
                            string fileName = path.Substring(path.LastIndexOf("\\") + 1);
                            ExceptionLog.ExceptionWrite("文件名" + ++i + " " + fileName);
                            zip.AddFile(fileName);

                        }
                        catch (Exception ex)
                        {
                            ExceptionLog.ExceptionWrite(string.Format("{0}压缩出错，错误信息:"+ex.Message,path));
                        }
                        //break;
                    }

                    //this.Response.Headers["Content-Length"] = ;
                    zip.Save();
                    Directory.SetCurrentDirectory(currentDirectory);
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(dirpath + "日志文件清理出错，错误信息:" + ex.Message);
            }
            finally
            {
                Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            }
        }
        public static void DirZip(string path, string zippathname, string processname)
        {
            Ionic.Zip.ZipFile zip = null;
            ExceptionLog.ExceptionWrite(string.Format("文件夹路径{0}压缩文件路径{1}", path,zippathname));
            try
            {
                LoadLineHelper.currentfilename[processname] = "打包文件 " + path;
                //string[] filenames = Directory.GetFiles(dirpath);

                if (Directory.Exists(path))
                {
                    using (zip = new Ionic.Zip.ZipFile((zippathname + ".zip"), System.Text.Encoding.UTF8))//解决中文乱码问题  
                    {

                        


                        ////if (Path.GetExtension(path) != ".txt") continue;
                        //string currentDirectory = Directory.GetCurrentDirectory();
                        //string dir = path.Substring(0, path.LastIndexOf("\\"));
                        //Directory.SetCurrentDirectory(dir);
                        //string fileName = path.Substring(path.LastIndexOf("\\") + 1);
                        zip.AddDirectory(path);
                        Directory.SetCurrentDirectory(Path.GetDirectoryName(zippathname));
                        zip.Save();



                    }
                    LoadLineHelper.currentfilename[processname] = "文件打包完成!";
                }

                //this.Response.Headers["Content-Length"] = ;

            }
            catch (Exception ex)
            {
                
                ExceptionLog.ExceptionWrite(zippathname + "版本文件生成，错误信息:" + ex.Message+"位置:"+ex.StackTrace);
            }

        }

        /// <summary>
        /// 复制文件夹
        /// </summary>       /// <param name="sourceFolderName">源文件夹目录</param>
        /// <param name="destFolderName">目标文件夹目录</param>
        /// <param name="overwrite">允许覆盖文件</param>
        public static void Copy(string sourceFolderName, string destFolderName, bool overwrite, string processname, string exceptioncopy)
        {
            var sourceFilesPath = Directory.GetFileSystemEntries(sourceFolderName);

            for (int i = 0; i < sourceFilesPath.Length; i++)
            {
                var sourceFilePath = sourceFilesPath[i];
                var directoryName = Path.GetDirectoryName(sourceFilePath);
                var forlders = directoryName.Split('\\');
                var lastDirectory = forlders[forlders.Length - 1];
                var dest = Path.Combine(destFolderName, lastDirectory);

                if (File.Exists(sourceFilePath))
                {
                    var sourceFileName = Path.GetFileName(sourceFilePath);
                    if (!Directory.Exists(dest))
                    {
                        Directory.CreateDirectory(dest);
                    }
                    if (sourceFolderName.IndexOf(exceptioncopy) == -1)
                        File.Copy(sourceFilePath, Path.Combine(dest, sourceFileName), overwrite);
                    else
                    {
                        string a = "";
                    }
                    LoadLineHelper.currentfilename[processname] = "复制文件 " + Path.Combine(dest, sourceFileName);
                }
                else
                {
                    Copy(sourceFilePath, dest, overwrite, processname, exceptioncopy);
                }
            }
        }



        public static void Copy(string sourceFolderName, string destFolderName, bool overwrite)
        {
            
            var sourceFilesPath = Directory.GetFileSystemEntries(sourceFolderName);

            for (int i = 0; i < sourceFilesPath.Length; i++)
            {
                var sourceFilePath = sourceFilesPath[i];
                var directoryName = Path.GetDirectoryName(sourceFilePath);
                var forlders = directoryName.Split('\\');
                var lastDirectory = forlders[forlders.Length - 1];
                var dest = Path.Combine(destFolderName, lastDirectory);

                if (File.Exists(sourceFilePath))
                {
                    var sourceFileName = Path.GetFileName(sourceFilePath);
                    if (!Directory.Exists(dest))
                    {
                        Directory.CreateDirectory(dest);
                    }
                    File.Copy(sourceFilePath, Path.Combine(dest, sourceFileName), overwrite);
                   
                }
                else
                {
                    Copy(sourceFilePath, dest, overwrite);
                }
            }
        }

        public static void DirFilesDel(string dirpath)
        {
            string[] filenames = Directory.GetFiles(dirpath);
            string filenametmp = "";
            try
            {
                foreach (string filename in filenames)
                {
                    if (Path.GetExtension(filename) != ".txt") continue;
                    filenametmp = filename;
                    File.Delete(filename);

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(filenametmp + " 日志文件清除出错，错误信息:" + ex.Message);
            }
            

        }

        public static void DirRename(string dirname,string distname)
        {
            try
            {
                //if (!Directory.Exists(distname))Directory.
                if (!Directory.Exists(dirname)) return;
                Directory.Move(dirname,distname);
            }
            catch (Exception ex)
            {

                Copy(dirname,distname,true);
                //Directory.Delete(dirname);
                DirDelete(dirname);
            }
        }
        public static void DirDelete(string folderpath)
        {
            try
            {
                //if (!Directory.Exists(distname))Directory.
                Directory.Delete(folderpath,true);
                Directory.Delete(folderpath);
            }
            catch (Exception ex)
            {

                ExceptionLog.ExceptionWrite("文件夹删除出错，错误信息："+ex.Message);
            }
        }
        public static void FileDelete(string filepath)
        {
            try
            {
                //if (!Directory.Exists(distname))Directory.
                File.Delete(filepath);
            }
            catch (Exception ex)
            {
                
                ExceptionLog.ExceptionWrite("文件删除出错，错误信息：" + ex.Message);
                throw (ex);
            }
        }
        public static void FileClear(string filepath)
        {
            FileStream fs = null;
            try
            {
                //if (!Directory.Exists(distname))Directory.
                //File.Delete(filepath);
                fs= File.Open(filepath, FileMode.Truncate, FileAccess.ReadWrite, FileShare.ReadWrite);

            }
            catch (Exception ex)
            {

                ExceptionLog.ExceptionWrite("文件删除出错，错误信息：" + ex.Message);
                throw (ex);
            }
            finally
            {
                fs.Close();
            }
        }
        public static long  FileLength(string filepath)
        {
            try
            {
                //if (!Directory.Exists(distname))Directory.
                //File.Delete(filepath);
              FileStream fs =  File.Open(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
              return fs.Length / 1024;
            }
            catch (Exception ex)
            {

                ExceptionLog.ExceptionWrite("文件删除出错，错误信息：" + ex.Message);
                throw (ex);
            }
        }
        public static string DirectoryNameGetFromFilePath(string filepath,int layoutcount)
        {
            int i=0;
            DirectoryInfo info = null;
            string currentdirectoryname = Path.GetDirectoryName(filepath);
            string currentdirectorfullyname = filepath;
            while (++i < layoutcount && (info = Directory.GetParent(currentdirectorfullyname)) != null)
            {

                currentdirectorfullyname = info.FullName;
                currentdirectoryname = info.Name;
            }
            return currentdirectoryname;
        }

       

        public static List<string> LocationFileRead(string filepath,string mark)
        {
            List<string> ls = new List<string>();
            TxtEncoding.GetType(filepath);
            FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            bool ismarkfind = false; 
            string tmpstr = "";
            string str = "";
            try
            {
                //sr = File.OpenText(settingpath);
                str = sr.ReadLine();
                if (mark == "") ismarkfind = true;
                while (!sr.EndOfStream)
                {
                    if (!ismarkfind)
                    {
                        if (str.IndexOf(mark) == -1)
                        {
                            str = sr.ReadLine();
                            continue;
                        }
                         ismarkfind = true;
                        
                    }
                    str = sr.ReadLine();
                    ls.Add(str);
                }
                return ls;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("读取IP配置文件出错，错误信息:" + ex.Message);
                return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();

            }
        }
        
       

    }


}
