﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;

namespace Tool
{
    public class MailHelper
    {
        public List<MailSendServer> mailservers;
        public MailHelper()
        {
            mailservers = new List<MailSendServer>();
            mailservers.Add(new MailSendServer("1123329225@qq.com", "yppoipgfwwumgehe"));
            mailservers.Add(new MailSendServer("703317572@qq.com", "jmumdhvcsmkqbefb"));
        }


        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="title">邮箱内容</param>
        /// <param name="Email">邮箱地址</param>
        /// <param name="value">收件人姓名</param>
        /// <returns></returns>
        public bool Small(string value, string Email,  string title, out string mssg)
        {
            mssg = "";
            foreach (MailSendServer server in mailservers)
            {
                MailMessage mail;
                SmtpClient client;
                string EmailTitle = /*"1123329225@qq.com"*/server.serveMail;
                mail = new MailMessage(EmailTitle, Email);
                mail.Body = value;//内容
                mail.Subject = title;//标题
                mail.IsBodyHtml = true;
                mail.SubjectEncoding = System.Text.Encoding.Default;
                mail.BodyEncoding = System.Text.Encoding.Default;
                //mail.Attachments.Add();
                //mail.Headers.Add("X-Priority", "3");
                //mail.Headers.Add("X-MSMail-Priority", "Normal");
                //mail.Headers.Add("X-Mailer", "Microsoft Outlook Express 6.00.2900.2869");
                //mail.Headers.Add("X-MimeOLE", "Produced By Microsoft MimeOLE V6.00.2900.2869");
                client = new SmtpClient("smtp.qq.com");
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new System.Net.NetworkCredential(/*"1123329225@qq.com"*/server.serveMail, /*"yppoipgfwwumgehe"*/server.authma);
                client.EnableSsl = true;
                //   client.Host = "smtp.qq.com";
                //System.Net.NetworkCredential crea = new System.Net.NetworkCredential();
                //crea.UserName = "用户名";
                //crea.Password = "密码";
                //System.IO.StreamWriter writer = new System.IO.StreamWriter("D:\\log.txt", true);
                try
                {
                    client.Send(mail);
                    //发送成功
                    //writer.WriteLine("邮件发送成功");
                    //writer.Close();
                    mssg = string.Format("{1}向{0}邮件发送成功", Email,server.serveMail);
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                catch (Exception e)
                {
                    //writer.WriteLine("邮件发送失败,错误信息:" + e.ToString());
                    //writer.Close();
                    mssg = string.Format("{1}向{0}邮件发送失败,失败原因:" + e.Message, Email,server.serveMail);
                    ExceptionLog.ExceptionWrite(mssg);
                    //return false;
                }
            }
            return false;
            
        }






        /// <summary>
        /// 群发邮件
        /// </summary>
        /// <param name="title">邮箱内容</param>
        /// <param name="Email">邮箱地址</param>
        /// <param name="value">收件人姓名</param>
        /// <returns></returns>
        public bool Small(string value, List<string> Emails,string title,out string mssg)
        {
            mssg = "";
            foreach (MailSendServer server in mailservers)
            {
                if (Emails.Count == 0)
                {
                    mssg = "请给出收件人地址";
                    return false;
                }
                MailMessage mail;
                SmtpClient client;
                string EmailTitle = server.serveMail /*"1123329225@qq.com"*/;
                mail = new MailMessage(EmailTitle, "28723002494@qq.com");
                mail.Body = value;//内容
                mail.Subject = title;//标题
                mail.IsBodyHtml = true;
                mail.SubjectEncoding = System.Text.Encoding.Default;
                mail.BodyEncoding = System.Text.Encoding.Default;
                //mail.Headers.Add("X-Priority", "3");
                //mail.Headers.Add("X-MSMail-Priority", "Normal");
                //mail.Headers.Add("X-Mailer", "Microsoft Outlook Express 6.00.2900.2869");
                //mail.Headers.Add("X-MimeOLE", "Produced By Microsoft MimeOLE V6.00.2900.2869");
                foreach (string mailaddr in Emails)
                {
                    mail.CC.Add(mailaddr);
                }
                client = new SmtpClient("smtp.qq.com");
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new System.Net.NetworkCredential(server.serveMail, server.authma /*"yppoipgfwwumgehe"*/);
                client.EnableSsl = true;
                //   client.Host = "smtp.qq.com";
                //System.Net.NetworkCredential crea = new System.Net.NetworkCredential();
                //crea.UserName = "用户名";
                //crea.Password = "密码";
                System.IO.StreamWriter writer = null;
                try
                {
                    //writer = new System.IO.StreamWriter("D:\\log.txt", true);
                    client.Send(mail);
                    //发送成功
                    //writer.WriteLine("邮件发送成功");
                    //writer.Close();
                    mssg = string.Format("{1}向{0}邮件发送成功",  string.Join(",", Emails),server.serveMail);
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                catch (Exception e)
                {
                    //writer.WriteLine("邮件发送失败,错误信息:" + e.ToString());
                    //writer.Close();
                    mssg = string.Format("{1}向{0}发送邮件失败,失败原因:" + e.Message, string.Join(",", Emails),server.serveMail);
                    ExceptionLog.ExceptionWrite(mssg);
                    //return false;
                }
                finally
                {
                    if (writer != null) writer.Close();
                }
            }
            return false;

           
        }


        /// <summary>
        /// 发送携带附件的邮件
        /// </summary>
        /// <param name="title">邮箱内容</param>
        /// <param name="Email">邮箱地址</param>
        /// <param name="value">收件人姓名</param>
        /// <returns></returns>
        public bool Small(string value, string Email, string title, FileStream attachfile, out string mssg)
        {

            mssg = "";
            System.Net.Mail.Attachment attachment = new Attachment(attachfile, title);
            foreach (MailSendServer server in mailservers)
            {
                MailMessage mail;
                SmtpClient client;
                string EmailTitle = /*"1123329225@qq.com"*/server.serveMail;
                mail = new MailMessage(EmailTitle, Email);
                mail.Body = value;//内容
                mail.Subject = title;//标题
                mail.IsBodyHtml = true;
                mail.SubjectEncoding = System.Text.Encoding.Default;
                mail.BodyEncoding = System.Text.Encoding.Default;

                mail.Attachments.Add(attachment);
                //mail.Headers.Add("X-Priority", "3");
                //mail.Headers.Add("X-MSMail-Priority", "Normal");
                //mail.Headers.Add("X-Mailer", "Microsoft Outlook Express 6.00.2900.2869");
                //mail.Headers.Add("X-MimeOLE", "Produced By Microsoft MimeOLE V6.00.2900.2869");
                client = new SmtpClient("smtp.qq.com");
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new System.Net.NetworkCredential(/*"1123329225@qq.com"*/server.serveMail, /*"yppoipgfwwumgehe"*/server.authma);
                client.EnableSsl = true;
                //   client.Host = "smtp.qq.com";
                //System.Net.NetworkCredential crea = new System.Net.NetworkCredential();
                //crea.UserName = "用户名";
                //crea.Password = "密码";
                //System.IO.StreamWriter writer = new System.IO.StreamWriter("D:\\log.txt", true);
                try
                {
                    client.Send(mail);
                    //发送成功
                    //writer.WriteLine("邮件发送成功");
                    //writer.Close();
                    mssg = string.Format("{1}向{0}邮件发送成功", Email, server.serveMail);
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                catch (Exception e)
                {
                    //writer.WriteLine("邮件发送失败,错误信息:" + e.ToString());
                    //writer.Close();
                    mssg = string.Format("{1}向{0}邮件发送失败,失败原因:" + e.Message, Email, server.serveMail);
                    ExceptionLog.ExceptionWrite(mssg);
                    //return false;
                }
            }
            return false; 

        }
        /// <summary>
        /// 群发携带附件的邮件
        /// </summary>
        /// <param name="title">邮箱内容</param>
        /// <param name="Email">邮箱地址</param>
        /// <param name="value">收件人姓名</param>
        /// <returns></returns>
        public bool Small(string value, List<string> Emails, string title,List<System.Net.Mail.Attachment> attachments , out string mssg)
        {
            mssg = "";
            

            foreach (MailSendServer server in mailservers)
            {
                if (Emails.Count == 0)
                {
                    mssg = "请给出收件人地址";
                    return false;
                }
                MailMessage mail;
                SmtpClient client;
                string EmailTitle = server.serveMail /*"1123329225@qq.com"*/;
                mail = new MailMessage(EmailTitle, "2872302494@qq.com");
                mail.Body = value;//内容
                mail.Subject = title;//标题
                mail.IsBodyHtml = true;
                mail.SubjectEncoding = System.Text.Encoding.Default;
                mail.BodyEncoding = System.Text.Encoding.Default;
                //mail.Headers.Add("X-Priority", "3");
                //mail.Headers.Add("X-MSMail-Priority", "Normal");
                //mail.Headers.Add("X-Mailer", "Microsoft Outlook Express 6.00.2900.2869");
                //mail.Headers.Add("X-MimeOLE", "Produced By Microsoft MimeOLE V6.00.2900.2869");
                foreach(var attachment in attachments )
                {
                mail.Attachments.Add(attachment);
                }
                foreach (string mailaddr in Emails)
                {
                    mail.CC.Add(mailaddr);
                }
                client = new SmtpClient("smtp.qq.com");
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new System.Net.NetworkCredential(server.serveMail, server.authma /*"yppoipgfwwumgehe"*/);
                client.EnableSsl = true;
                //   client.Host = "smtp.qq.com";
                //System.Net.NetworkCredential crea = new System.Net.NetworkCredential();
                //crea.UserName = "用户名";
                //crea.Password = "密码";
                System.IO.StreamWriter writer = null;
                try
                {
                    //writer = new System.IO.StreamWriter("D:\\log.txt", true);
                    client.Send(mail);
                    //发送成功
                    //writer.WriteLine("邮件发送成功");
                    //writer.Close();
                    mssg = string.Format("{1}向{0}邮件发送成功", string.Join(",", Emails), server.serveMail);
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                catch (Exception e)
                {
                    //writer.WriteLine("邮件发送失败,错误信息:" + e.ToString());
                    //writer.Close();
                    mssg = string.Format("{1}向{0}发送邮件失败,失败原因:" + e.Message, string.Join(",", Emails), server.serveMail);
                    ExceptionLog.ExceptionWrite(mssg);
                    //return false;
                }
                finally
                {
                    if (writer != null) writer.Close();
                }
            }
            return false;


        }

        public class MailSendServer
        {
            public string serveMail {get;set; }
            public string authma { get; set; }
            public MailSendServer(string serveMail, string authma)
            {
                this.serveMail = serveMail;
                this.authma = authma;
            }
        }

        public class messprotocol
        {
            public string context { get; set; }
            public string title { get; set; }
            public List<string> lsrecmail { get; set; }
        }






    }
}
