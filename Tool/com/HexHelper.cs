﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.com
{
    public class HexHelper
    {
        public static string oxdata(byte[] data, int startbyteindex, int len)
        {
            StringBuilder strbuilder = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                strbuilder.Append(DecimalToHex(data[startbyteindex + i]));
            }
            string datastr = string.Format("0x{0}", strbuilder.ToString());
            return datastr;
        }
        public static string DecimalToHex(byte data)
        {
            int datadecimal = Convert.ToInt32(data.ToString());
            return datadecimal > 15 ? Convert.ToString(datadecimal, 16) : string.Format("0{0}", Convert.ToString(datadecimal, 16));

        }
        public static string ByteToHexString(byte[] bytes)
        {
            var hexList = (from b in bytes
                           select (
                               Convert.ToInt32(b.ToString()) > 15 ?
                               Convert.ToString(Convert.ToInt32(b.ToString()), 16) :
                               string.Format("0{0}", Convert.ToString(Convert.ToInt32(b.ToString()), 16)))
                               ).ToList();
            return string.Join(" ", hexList);
        }
        public static byte[] ByteTrim(byte[] bytes, byte[] start, byte[] end)
        {
            bool hasstart = false;
            bool hasend = false;
            int i = 0;
            List<byte> lb = new List<byte>();
            int startindex = 0, endindex = bytes.Length;
            if (start != null)
            {
                for (i = 0; i < bytes.Length; i++)
                {
                    int j = 0;
                    if (bytes[i] == start[j])
                    {
                        for (j = 1; j < start.Length; j++)
                        {
                            if (i + j < bytes.Length && bytes[i + j] == start[j])
                            {
                                hasstart = true;
                                continue;
                            }
                            hasstart = false;
                            break;
                        }
                        if (hasstart && j == start.Length)
                        {
                            startindex = i + start.Length;

                        }

                    }

                }
            }
            if (end != null)
            {

                for (i = startindex; i < bytes.Length; i++)
                {
                    int j = 0;
                    if (bytes[i] == end[j])
                    {
                        for (j = 1; j < end.Length; j++)
                        {
                            if (i + j < bytes.Length && bytes[i + j] == end[j])
                            {
                                hasend = true;
                                continue;
                            }
                            hasend = false;
                            break;
                        }
                        if (hasend && j == end.Length)
                        {
                            endindex = i;

                        }

                    }

                }
            }
           
            for (i = startindex; i < endindex; i++)
            {
                lb.Add(bytes[i]);
            }
            return lb.ToArray();


        }

        public static bool IsByteContainByteArray(byte[] srcbytearray, byte[] bytearray)
        {
            int i = 0;
            bool contained = false;
            for (i = 0; i < srcbytearray.Length; i++)
            {
                int j = 0;
                if (srcbytearray[i] == bytearray[j])
                {
                    for (j = 1; j < bytearray.Length; j++)
                    {
                        if (i + j < srcbytearray.Length && srcbytearray[i + j] == bytearray[j])
                        {
                            contained = true;
                            continue;
                        }
                        contained = false;
                        break;
                    }
                    if (contained&&j == bytearray.Length)
                    {
                        return true;

                    }

                }

            }
            return false;
        }

        public static int ByteIndexByteArray(byte[] srcbytearray, byte[] bytearray)
        {
            int i = 0;
            bool contained = false;
            for (i = 0; i < srcbytearray.Length; i++)
            {
                int j = 0;
                if (srcbytearray[i] == bytearray[j])
                {
                    for (j = 1; j < bytearray.Length; j++)
                    {
                        if (i + j < srcbytearray.Length && srcbytearray[i + j] == bytearray[j])
                        {
                            contained = true;
                            continue;
                        }
                        contained = false;
                        break;
                    }
                    if (contained && j == bytearray.Length)
                    {
                        return i;

                    }

                }

            }
            return -1;
        }
        public static string GetHexStrFromString(string str,int startindex,int len)
        {
            if (str.Length < startindex + len) return "";
            string hexstr = str.Substring(startindex,len);
            return "0x" + hexstr;
        }
        public static string GetHexDateTimeFromTimeStr(string str)
        {
            int hour = Convert.ToInt32(str,16) / 0x0800;
            int minute = (Convert.ToInt32(str,16)&0x07e0) / 0x0020;
            int seconds = (Convert.ToInt32(str, 16) & 0x001f)*2;
            return string.Format("{0}:{1}:{2}", hour.ToString("00"), minute.ToString("00"), seconds.ToString("00"));
        }
    }
}
