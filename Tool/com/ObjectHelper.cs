﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
namespace Tool
{
    public class ObjectHelper
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public  static void ClassSerialization<T>( List<T> lt,string path )
        {
            StreamWriter sw = null;
            FileStream fs = null;
                
            try
            {
                fs = File.Open(path, FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs);
                string serialization = "";
                foreach (T t in lt)
                {
                    serialization = jss.Serialize(t);
                    sw.WriteLine(serialization);
                }
                if(sw!=null)
                sw.Close();
                if(fs!=null)
                fs.Close();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("对象序列化输出出错，错误信息："+ex.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();
            }

        }
        public static bool ProcessTextFileCheck<T>(string path, out string mssg)
        {
            int i = 0;
            bool ispass = true;
            mssg = "";
            path = path.Substring(0, path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(path);
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);

            List<string> lshead = new List<string>();
            string strTemp = sr.ReadLine();

            mssg = "文件第{0}行存在错误\n";
            int k = 0;

            while (strTemp != null && strTemp.Trim() != "" && strTemp.IndexOf("\0") == -1)
            {
                try
                {

                    T obj = (T)jss.Deserialize(strTemp, typeof(T));
                    if (obj == null) { mssg = string.Format(mssg, i + 1 + "、{0}", "\n"); k++; }
                    i++;
                    strTemp = sr.ReadLine();
                }
                catch (Exception ex)
                {
                    int la = strTemp.Trim().Length;
                    mssg = string.Format(mssg, i + 1 + "、{0}", "\n");
                    k++;
                    strTemp = sr.ReadLine();
                    i++;
                    ispass = false;
                    //return false;
                }
            }
            mssg += string.Format("共导入{0}行", i - k);
            mssg = mssg.Replace("、{0}", "");
            if (sr != null)
                sr.Close();
            if (fs != null)
                fs.Close();

            ExceptionLog.ExceptionWrite(mssg);
            return ispass;

        }

        public static bool ProcessTextFileModelListCheck<T>(string path, out string mssg)
        {
            int i = 0;
            bool ispass = true;
            mssg = "";
            path = path.Substring(0, path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(path);
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);

            List<string> lshead = new List<string>();
            string strTemp = sr.ReadLine();

            mssg = "文件第{0}行存在错误\n";
            StringBuilder str = new StringBuilder();
            str.Append(strTemp);
            int k = 0;

            while (strTemp != null && strTemp.Trim() != "" && strTemp.IndexOf("\0") == -1)
            {
                try
                {
                    strTemp = sr.ReadLine();
                    str.Append(strTemp);
                }
                catch (Exception ex)
                {
                    int la = strTemp.Trim().Length;
                    mssg = string.Format(mssg, i + 1 + "、{0}", "\n");
                    k++;
                    strTemp = sr.ReadLine();
                    i++;
                    ispass = false;
                    //return false;
                }
            }
            string strdata = str.ToString();
            T model = (T)jss.Deserialize<T>(str.ToString());
            mssg += string.Format("共导入{0}行", i - k);
            mssg = mssg.Replace("、{0}", "");
            if (sr != null)
                sr.Close();
            if (fs != null)
                fs.Close();


            return ispass;

        }

        public static T[] ObjectArraryToTArrary<T>(object[] objarrary)
        {

            Type t = typeof(T);
            dynamic tararry = null;
            if (t.Name == "Double")
            {
                tararry = new double[objarrary.Length];
            }
            int i = 0;
            for (; i < objarrary.Count(); i++)
            {
                if (t.Name == "Double")
                    tararry[i] =Convert.ToDouble(objarrary[i]);
            }
            return tararry;
        }

    } 
    

}
