﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool
{
    public class SenorDataTableHelper
    {
        public static DataTable ProcessDataTableCopy(DataTable originalTab)
        {
            DataTable dt = new DataTable();
            foreach (DataColumn dc in originalTab.Columns)
            {
                dt.Columns.Add(dc.ColumnName, dc.DataType);
            }
            return dt;
        }
        
        public static List<DataTable> ProcessDataTableSplitSPMT(DataTable originalTab)
        {
            ExceptionLog.ExceptionWrite("单点多时段分表");
            List<DataTable> ldt = new List<DataTable>();
            DataRow tmp = originalTab.Rows[0];
            DataTable dt = new DataTable();
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(ProcessDataTableCopy(originalTab));

            foreach (DataRow dr in originalTab.Rows)
            {

                if (tmp.ItemArray[1].ToString() != dr.ItemArray[1].ToString() || tmp.ItemArray[9].ToString() != dr.ItemArray[9].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );

                    //continue;
                    ldt.Add(ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr;
                }
                ldt[i].ImportRow(dr);

            }
            return ldt;
        }
        public static List<DataTable> ProcessFixedInclinometerDataTableSplitSPMT(DataTable originalTab)
        {
            ExceptionLog.ExceptionWrite("单点多时段分表");
            List<DataTable> ldt = new List<DataTable>();
            DataRow tmp = originalTab.Rows[0];
            DataTable dt = new DataTable();
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(ProcessDataTableCopy(originalTab));

            foreach (DataRow dr in originalTab.Rows)
            {

                if (tmp.ItemArray[1].ToString() != dr.ItemArray[1].ToString() || tmp.ItemArray[7].ToString() != dr.ItemArray[7].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );

                    //continue;
                    ldt.Add(ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr;
                }
                ldt[i].ImportRow(dr);

            }
            return ldt;
        }


        public static int ProcessDataTableRowsCount(DataTable originalTab,string filterName)
        {
            DataView dv = new DataView(originalTab);
            return dv.ToTable(originalTab.TableName, true, filterName).Rows.Count;
            
        }


        //public static List<DataTable> ProcessDataTableSplitMPST(DataTable originalTab )
        //{
        //    ExceptionLog.ExceptionWrite("多点单时段分表");
        //    List<DataTable> ldt = new List<DataTable>();
            
        //    DataTable dt = new DataTable();
        //    int i = 0;
        //    //List<printtable> lp = new List<printtable>();
        //    ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
        //    DataView dv = new DataView(originalTab);
        //    dv.Sort = "time,point_name asc";
        //    //originalTab.DefaultView.Sort = " cyc,point_name asc ";
        //    DataRowView tmp = dv[0]; //originalTab.Rows[0];
        //    foreach (DataRowView dr in dv)
        //    {

        //        if (tmp[1].ToString() != dr[1].ToString())
        //        {
        //            //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );
        //            ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
        //            ++i;
        //            tmp = dr; 

        //        }
        //        ldt[i].ImportRow(dr.Row);

        //    }
        //    return ldt;
        //}
        public static List<string> ProcessDataTableFilter(DataTable originalTab,string filterName)
        {
            if(originalTab == null) return new List<string>();
            DataView dv = new DataView(originalTab);
            DataTable dt = dv.ToTable(originalTab.TableName, true, filterName);
            //DataView dvrow = new DataView(dt);
            List<string> pnames = new List<string>();
            int i = 0;
            for (i = 0; i < dt.Rows.Count;i++ )
            {
                pnames.Add(dt.Rows[i].ItemArray[0].ToString());
            }
            return pnames;
        }
        public static DataTable SenorDataMPMTTabCreate(List<DataTable> ls,int columnIdx)
        {
            DataTable dt = new DataTable();
            int len = 0;
            
            int r = 0;
            dt.Columns.Add("time", typeof(DateTime));
            foreach (DataTable ta in ls)
            {
                len = ta.Rows.Count > len ? ta.Rows.Count : len;
               dt.Columns.Add(ta.Rows[0].ItemArray[0].ToString()+"_"+ta.Columns[columnIdx].ColumnName,ta.Columns[columnIdx].DataType);

            }
            int i = 0,k=0;
            for (i = 0; i < len; i++)
            {
                
                

                    
                      DataRow dr = dt.NewRow();
                      dt.Rows.InsertAt(dr, 0);
                      dr["time"] = Convert.ToDateTime(ls[0].Rows[i].ItemArray[1].ToString());  
                      for (k = 0; k < ls.Count; k++)
                      {
                          string val = ls[k].Rows[i].ItemArray[columnIdx].ToString();
                          dr[k+1] = Convert.ToDouble(val);
                          //string f = dr.ItemArray[k].ToString();
                      }
                      

               
            }
            return dt;
        }
        //public class TabListArrow
        //{
        //    public DataTable dt { get; set; }
        //    public string regionName { get; set; }
        //    public TabListArrow(DataTable dt)
        //    {
        //        this.dt = dt;
        //        this.regionName = dt.Rows[0].ItemArray[0].ToString();
        //    }
        //}

     
    }
}
