﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.Script.Serialization;
using System.Reflection;
namespace Tool
{
   public class JsonHelper
    {
       public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static bool ProcessDataIntoJson(DataTable dt, int totalCont, int startPageIndex, int pageSize, out string json)
        {
            DataView dv = new DataView(dt);
            List<string> rows = new List<string>();
            int i = 0;
            foreach (DataRowView drv in dv)
            {

                List<string> row = new List<string>();
                for (i = 0; i < dt.Columns.Count; i++)
                {
                    row.Add("\"" + drv[i].ToString() + "\"");
                }
                string temp = string.Join(",", row);
                rows.Add("{\"cell\":[" + temp + "]}");
            }
            int totalPageNum = totalCont / pageSize == 0 ? totalCont / pageSize : totalCont / pageSize + 1;
            string rowstr = string.Format("[{0}]", string.Join(", ", rows));
            string template = "{\"page\":" + startPageIndex + ",\"total\":" + totalPageNum + ",\"records\":" + totalCont + ",\"rows\":" + rowstr + ",\"userdata\":{\"amount\":3330,\"tax\":342,\"total\":3564,\"name\":\"Totals:\"}}";


            json = template;
            return true;
        }
        public static bool ProcessDataIntoJsonWhithSort(DataTable dt, int totalCont, int startPageIndex, int pageSize,string sortName,string sord ,out string json)
        {
            DataView dv = new DataView(dt);
            if(!string.IsNullOrEmpty(sortName)&&sortName!="id")  dv.Sort = string.Format("{0} {1}",sortName,sord);  
            List<string> rows = new List<string>();
            int i = 0;
            foreach (DataRowView drv in dv)
            {

                List<string> row = new List<string>();
                for (i = 0; i < dt.Columns.Count; i++)
                {
                    row.Add("\"" + drv[i].ToString() + "\"");
                }
                string temp = string.Join(",", row);
                rows.Add("{\"cell\":[" + temp + "]}");
            }
            int totalPageNum = totalCont / pageSize == 0 ? totalCont / pageSize : totalCont / pageSize + 1;
            string rowstr = string.Format("[{0}]", string.Join(", ", rows));
            string template = "{\"page\":" + startPageIndex + ",\"total\":" + totalPageNum + ",\"records\":" + totalCont + ",\"rows\":" + rowstr + ",\"userdata\":{\"amount\":3330,\"tax\":342,\"total\":3564,\"name\":\"Totals:\"}}";


            json = template;
            return true;
        }
        public static T ObjectDeserialize<T>(string jsonstr) where T:new ()
        {
            try
            {
                var model = jss.Deserialize<T>(jsonstr);
                return model == null ? new T() : model;
            }
            catch (Exception ex)
            {
                
                return new T();
            }
        }
        public static object DeserializeObject(string jsonstr)
        {   try
            {
                var model = jss.DeserializeObject(jsonstr);
                return model;
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public static string ObjectSerialize<T>(T t) where T : new()
        {
            try
            {
                //var model = jss.Deserialize<T>(jsonstr);
                return jss.Serialize(t);//model == null ? new T() : model;
            }
            catch (Exception ex)
            {

                return "";
            }
        }
        public static object ObjectSerialize(string datastr) 
        {
            try
            {
                //var model = jss.Deserialize<T>(jsonstr);
                return jss.DeserializeObject(datastr);//model == null ? new T() : model;
            }
            catch (Exception ex)
            {

                return "";
            }
        }
        public static string SerializeObject(Object objectdata)
        {
            try
            {
                //var model = jss.Deserialize<T>(jsonstr);
                Type t = objectdata.GetType();
                if (t.GetType().Name.IndexOf("string") != -1) return t.ToString();
                return jss.Serialize(objectdata);//model == null ? new T() : model;
            }
            catch (Exception ex)
            {
                return "";
            }
        }




        public static string JsonDataDescrialize(object sourcestr,string keyword,int n)
        {
            Type type = sourcestr.GetType();
            Dictionary<string, object> dso = null;
            if(type.Name.IndexOf("String") != -1)
            dso = (Dictionary<string, object>)ObjectSerialize(sourcestr.ToString());
            if(type.Name.IndexOf("Dictionary") != -1)
            dso = (Dictionary<string, object>)(sourcestr);
            if (dso.Keys.Contains(keyword)) return ObjectSerialize(dso[keyword]);
            foreach (var key in dso.Keys)
            {
                Console.WriteLine(string.Format("进行第{0}层遍历:key:{1}----value:{2}", n, key, dso[key].ToString()));

                return JsonDataDescrialize(dso[key], keyword, n + 1);
            }
            return "";
        }

        public static TSource JsonDataToModel<TSource>(string jsonstr) where TSource:new()
        {
            TSource model = new TSource();
            PropertyInfo[] properties = typeof(TSource).GetProperties();
            properties.ToList().ForEach(m =>
            {
                try
                {
                        if (m.PropertyType.Name == "String")
                            m.SetValue(model, Tool.JsonHelper.JsonDataDescrialize(jsonstr, m.Name, 0), null);
                        else if (m.PropertyType.Name == "Int32")
                            m.SetValue(model, Convert.ToInt32(Tool.JsonHelper.JsonDataDescrialize(jsonstr, m.Name, 0)), null);
                        else if (m.PropertyType.Name == "Double")
                            m.SetValue(model, Convert.ToDouble(Tool.JsonHelper.JsonDataDescrialize(jsonstr, m.Name, 0)), null);
                        else if (m.PropertyType.Name == "Boolean")
                            m.SetValue(model, Convert.ToBoolean(Tool.JsonHelper.JsonDataDescrialize(jsonstr, m.Name, 0)), null);
                        else if (m.PropertyType.Name == "DateTime")
                            m.SetValue(model, Convert.ToDateTime(Tool.JsonHelper.JsonDataDescrialize(jsonstr, m.Name, 0)), null);
                        else if (m.PropertyType.IsEnum)
                        {
                            m.SetValue(model, Convert.ToInt32(Tool.JsonHelper.JsonDataDescrialize(jsonstr, m.Name, 0)), null);
                        }

                    
                }
                catch (Exception ex)
                {
                    string a = "";
                }




            });



            return model;
        }


    }
}
