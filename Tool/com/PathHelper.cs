﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool
{
    public class PathHelper
    {
        public static string GetCommonPartInPath(string pathn,string pathm)
        {
            string[] pathns = pathn.Split(',');
            string[] pathms = pathm.Split(',');
            int i = pathns.Length-1, j = pathms.Length-1;
            List<string> compathlist = new List<string>();
            for(;i>0&&j>0;i--,j--)
            {
                if (pathns[i] == pathms[j]) compathlist.Insert(0, pathns[i]);
            }
            return string.Join("\\",compathlist);
        }
    }
}
