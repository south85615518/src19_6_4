﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Tool
{
   public class DateHelper
    {
       public static string DateTimeToString(DateTime dt)
       {
           return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}", dt.Year % 2000, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"), dt.Millisecond.ToString("00"), new Random().Next(99));
       }
       public static string DateTimeToStringReportTime(DateTime dt)
       {
           return string.Format("{0}{1}{2}{3}{4}", dt.Year % 2000, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"));
       }
       public static string DateTimeToStringCHN(DateTime dt)
       {
           return string.Format("{0}_{1}_{2} {3}_{4}_{5}_{6}_{7}", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Millisecond, new Random().Next(99));
       }
       /// <summary>
       /// 获取指定日期，在为一年中为第几周
       /// </summary>
       /// <param name="dt">指定时间</param>
       /// <reutrn>返回第几周</reutrn>
       public static int GetWeekOfYear(DateTime dt)
       {
           GregorianCalendar gc = new GregorianCalendar();
           int weekOfYear = gc.GetWeekOfYear(dt, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
           return weekOfYear;
       }

       public static DateTime Unixtimestamp(long datetimevalue)
       {

           //DateTime dt = new DateTime(datetimevalue);

           DateTime start;
           DateTime date;
           start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
           date = start.AddMilliseconds(datetimevalue*1000).ToLocalTime();
           return date;

       }

       public static DateTime Localtimestamp(long datetimevalue)
       {

           //DateTime dt = new DateTime(datetimevalue);

           DateTime start;
           DateTime date;
           start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
           date = start.AddMilliseconds(datetimevalue * 1000).ToLocalTime().AddHours(8);
           return date;

       }
       public static DateTime DateTimePreciseToMinute(DateTime dt)
       {
           return Convert.ToDateTime(string.Format("{0}/{1}/{2} {3}:{4}",dt.Year,dt.Month,dt.Day,dt.Hour,dt.Minute));
       }
       public static DateTime GetMinDateTimeFromTimeList(List<DateTime> timelist,int interval)
       {
           timelist.Sort();
           DateTime maxTime = timelist[timelist.Count-1];
           return timelist.First(m => m > maxTime.AddHours(-interval));
       }

    }
}
