﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.com
{
    public class CRC16Helper
    {
        public static byte[] CRC16(byte[] data)
        {

            byte[] temdata = new byte[data.Length + 2];
            int xda, xdapoly;
            int i, j, xdabit;
            xda = 0xFFFF;
            xdapoly = 0xA001;
            for (i = 0; i < data.Length; i++)
            {
                xda ^= data[i];
                for (j = 0; j < 8; j++)
                {
                    xdabit = (int)(xda & 0x01);
                    xda >>= 1;
                    if (xdabit == 1)
                        xda ^= xdapoly;
                }
            }
            Array.Copy(data, 0, temdata, 0, data.Length);
            temdata[temdata.Length - 2] = (byte)(xda & 0xFF);
            temdata[temdata.Length - 1] = (byte)(xda >> 8);
            return temdata;
        }

        public static bool IsDataCorrectCRC16(byte[] data,out string mssg)
        {
            if (data.Length == 2) { mssg = string.Format("{0}CRC校验失败,不进行解析...", HexHelper.ByteToHexString(data)); return false; }
            mssg = "";
            byte[] temdata = new byte[data.Length];
            int xda, xdapoly;
            int i, j, xdabit;
            xda = 0xFFFF;
            xdapoly = 0xA001;
            
            for (i = 0; i < data.Length-2; i++)
            {
                xda ^= data[i];
                for (j = 0; j < 8; j++)
                {
                    xdabit = (int)(xda & 0x01);
                    xda >>= 1;
                    if (xdabit == 1)
                        xda ^= xdapoly;
                }
            }
            Array.Copy(data, 0, temdata, 0, data.Length);
            temdata[temdata.Length - 2] = (byte)(xda & 0xFF);
            temdata[temdata.Length - 1] = (byte)(xda >> 8);
            if (data[data.Length - 2] == temdata[temdata.Length - 2] && data[data.Length - 1] == temdata[temdata.Length - 1])
            {
                mssg = string.Format("{0}CRC校验成功,准备进行数据解析...",HexHelper.ByteToHexString(data));
                return true;
            }
            mssg = string.Format("{0}CRC校验失败,不进行解析...", HexHelper.ByteToHexString(data));
            return false;
        }


    }
}
