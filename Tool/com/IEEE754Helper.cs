﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.com
{
    public class IEEE754Helper
    {
        public double INT0XToIEEE754(string data0x)
        {
            string datastr = string.Format("0x{0}{1}{2}{3}{4}{5}{6}{7}", data0x[8], data0x[9], data0x[6], data0x[7], data0x[4], data0x[5], data0x[2], data0x[3]);
            //datastr = data0x;
            string binarystring = oxToFloat(Convert.ToInt32(datastr,16));
            IEE754 iEE754 = IEE754GetFromBinaryString(binarystring);
            return IEE745real4Float(iEE754);
            //ProcessPrintMssg.Print(string.Format("{0}转换成IEEE754浮点数是{1}", Convert.ToString(data0x, 16), value));
        }
        public string ConvertToOX(int data)
        {
            return Convert.ToString(data, 16);
        }

        public double BinaryToNagativeFloat(int n)
        {

            return Math.Pow(2, -1 * n);

        }
        public void BinaryToNagativeFloatTest()
        {
            double e = BinaryToNagativeFloat(8) + BinaryToNagativeFloat(11) + BinaryToNagativeFloat(12) + BinaryToNagativeFloat(17);
            //ProcessPrintMssg.Print("e:" + e.ToString());
            double value = Math.Pow(-1, 0) * Math.Pow(2, -126) * e;
            //ProcessPrintMssg.Print("value:" + value);
            ////ProcessPrintMssg.Print( string.Format("2的-{0}次幂为{1}", 4, BinaryToNagativeFloat(4)));
        }

        public string oxToFloat(int data)
        {
            //int data = 0x02950000;
            string binarystring = Convert.ToString(data, 2);
            StringBuilder strbuilder = new StringBuilder(256);
            int length = binarystring.Length;
            int i = 0;
            for (i = 32 - length; i > 0; i--)
            {
                strbuilder.Append("0");
            }
            strbuilder.Append(binarystring);
            return strbuilder.ToString();
            ////ProcessPrintMssg.Print(string.Format("{0}转换为二进制数{1}", Convert.ToString(data, 16), strbuilder.ToString()));
        }

        public class IEE754
        {
            public int s { get; set; }
            public int e { get; set; }
            public double m { get; set; }
        }
        public IEE754 IEE754GetFromBinaryString(string binarystring)
        {
            bool isequal = binarystring == "01000010100110000000000000000000";
            IEE754 iEE754 = new IEE754();
            iEE754.s = Convert.ToInt32(binarystring.Substring(0, 1));
            //01000010100110000000000000000000
            iEE754.e = Convert.ToInt32(binarystring.Substring(1, 8), 2);
            string mstr = binarystring.Substring(9);
            iEE754.m = BinaryToFloat("0." + mstr);
            if (iEE754.e > 0 && iEE754.e != Convert.ToInt32("11111111", 2))
            {
                iEE754.m += 1;
            }

            return iEE754;


        }

        public double IEE745real4Float(IEE754 iEE754)
        {

            if (iEE754.e == 0) return Math.Pow(-1, iEE754.s) * Math.Pow(2, -126) * iEE754.m;
            return Math.Pow(-1, iEE754.s) * Math.Pow(2, iEE754.e - 127) * iEE754.m;
        }

        public double BinaryToFloat(string binarystr)
        {
            int dotpos = binarystr.IndexOf(".");
            int iInterger = dotpos, indxdot = dotpos;
            double integerValue = 0, dotvalue = 0;
            for (iInterger = dotpos - 1; iInterger >= 0; iInterger--)
            {
                integerValue += Convert.ToDouble(binarystr[iInterger].ToString()) * Math.Pow(2, dotpos - iInterger - 1);
            }
            for (indxdot = dotpos + 1; indxdot < binarystr.Length; indxdot++)
            {
                dotvalue += Convert.ToDouble(binarystr[indxdot].ToString()) * Math.Pow(2, -1 * (indxdot - dotpos));
            }
            integerValue += dotvalue;
            //ProcessPrintMssg.Print(string.Format("{0}转换成十进制是{1}", binarystr, integerValue));
            return integerValue;

        }
    }
}
