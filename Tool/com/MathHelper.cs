﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.com
{
    public class MathHelper
    {
        public static double MathAbs(object val)
        {
            if (val == "") return 0;
            double d = Convert.ToDouble(val);
            return d > 0 ? d : -1 * d;
        }
    }
}
