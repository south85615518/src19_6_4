﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.Office.Interop.Excel;

namespace Tool
{
    partial class ExcelHelperHandle
    {

        //public  void main()
        //{
        //    ExcelInit();
        //    ////ExcelWrite();
        //    //BorderSet(1, 1, 1, 3);
        //    //ExcelMergedRegion(3, 3, 3, 5);
        //    //save();
        //}
        public int fontsize = 10;
        #region xsheet
        public  void ExcelWrite(int rownum,int colnum,object value)
        {

            xSheet.Cells[rownum, colnum] = value;
            xSheet.Cells[rownum, colnum].HorizontalAlignment = XlHAlign.xlHAlignCenter;
            xSheet.Cells[rownum, colnum].VerticalAlignment = XlVAlign.xlVAlignCenter;
            xSheet.Cells[rownum, colnum].Font.Size = fontsize; //XlVAlign.xlVAlignCenter;
        }
        public void ExcelWriteWrapText(int rownum, int colnum, object value)
        {

            xSheet.Cells[rownum, colnum] = value;
            xSheet.Cells[rownum, colnum].HorizontalAlignment = XlHAlign.xlHAlignCenter;
            xSheet.Cells[rownum, colnum].VerticalAlignment = XlVAlign.xlVAlignCenter;
            xSheet.Cells[rownum, colnum].WrapText =true;
            xSheet.Cells[rownum, colnum].Font.Size = fontsize;
             //xSheet.Cells[rownum, colnum].
            //xSheet.Cells[rownum, colnum]. =  //XlVAlign.xlVAlignCenter;
        }
        public void ExcelWriteWrapText(int rownum, int colnum, object value, XlHAlign xlHAlign)
        {

            xSheet.Cells[rownum, colnum] = value;
            xSheet.Cells[rownum, colnum].HorizontalAlignment = XlHAlign.xlHAlignCenter;
            //xSheet.Cells[rownum, colnum].VerticalAlignment = xlHAlign;//XlVAlign.xlVAlignCenter;
            xSheet.Cells[rownum, colnum].WrapText = true;
            xSheet.Cells[rownum, colnum].Font.Size = fontsize;
            //xSheet.Cells[rownum, colnum]. =  //XlVAlign.xlVAlignCenter;
        }
        public  void ExcelWrite(int rownum, int colnum, object value,bool border)
        {
            BorderSet(xSheet.Cells[rownum, colnum]);
            xSheet.Cells[rownum, colnum] = value;
            xSheet.Cells[rownum, colnum].HorizontalAlignment = XlHAlign.xlHAlignCenter;
            xSheet.Cells[rownum, colnum].VerticalAlignment = XlVAlign.xlVAlignCenter;
        }
        public  void ExcelWriteTitle(int rownum, int colnum, object value, bool border)
        {
            BorderSet(xSheet.Cells[rownum, colnum]);
            xSheet.Cells[rownum, colnum] = value;
            xSheet.Cells[rownum, colnum].HorizontalAlignment = XlHAlign.xlHAlignLeft;

        }

#endregion
        #region xdataSheet
        public void DataExcelWrite(int rownum, int colnum, object value)
        {

            xDataSheet.Cells[rownum, colnum] = value;
            xDataSheet.Cells[rownum, colnum].HorizontalAlignment = XlHAlign.xlHAlignCenter;
            xDataSheet.Cells[rownum, colnum].VerticalAlignment = XlVAlign.xlVAlignCenter;
            xDataSheet.Cells[rownum, colnum].Font.Size = fontsize; //XlVAlign.xlVAlignCenter;
        }
       

        #endregion


       

        //区域添加边框
        public  void BorderSet(int startrow,int startcol ,int endrow,int endcol)
        {
            int Rowcount = xSheet.UsedRange.CurrentRegion.Rows.Count;
            Range range = xSheet.Range[xSheet.Cells[startrow, startcol], xSheet.Cells[endrow, endcol]];//11指的是列数
            range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
            range.VerticalAlignment =  XlVAlign.xlVAlignCenter;
            range.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
        }
        //区域添加边框
        public void BorderSet(Worksheet xSheet,int startrow, int startcol, int endrow, int endcol)
        {
            int Rowcount = xSheet.UsedRange.CurrentRegion.Rows.Count;
            Range range = xSheet.Range[xSheet.Cells[startrow, startcol], xSheet.Cells[endrow, endcol]];//11指的是列数
            range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
            range.VerticalAlignment = XlVAlign.xlVAlignCenter;
            range.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
        }
        //单元格添加边框
        public  void BorderSet(Range range )
        {
            range.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
        }
        //设置列宽
        public  void ColumnWidthSet(Worksheet sheet,int colnum,int width)
        {
            Range excelRange = sheet.Columns[colnum];
            excelRange.ColumnWidth = width;
            excelRange = null;  
        }
        //设置行高
        public  void RowHeightSet(Worksheet sheet, int rownum, int height)
        {
            Range excelRange = sheet.Rows[rownum];
            excelRange.RowHeight = height;
            excelRange = null;
        }
        //设置列的日期格式
        public  void DateFormat(Worksheet sheet, int colnum, string format)
        {
            Range excelRange = sheet.Columns[colnum];
            excelRange.NumberFormat = format;// "yyyy-mm-dd h:s:00";
            excelRange = null;  
        }
        //合并单元格
        public  void ExcelMergedRegion(int startrow, int startcol, int endrow, int endcol)
        {
            int Rowcount = xSheet.UsedRange.CurrentRegion.Rows.Count;
            Range range = xSheet.Range[xSheet.Cells[startrow, startcol], xSheet.Cells[endrow, endcol]];//11指的是列数
            range.Merge(Missing.Value);
        }
        public double ExcelColMax(int startrow, int colindex, int rows)
        {
            double tmp = xSheet.Cells[startrow, colindex].Value == null?0:xSheet.Cells[startrow + 0, colindex].Value, val = 0;
            int i = 0;
            for (i = 0; i < rows; i++)
            {
                val = xSheet.Cells[startrow + i, colindex].Value == null?0:xSheet.Cells[startrow + i, colindex].Value;
                tmp = val > tmp ? val : tmp;
            }
            return tmp;
        }
        public double ExcelColMin(int startrow, int colindex, int rows)
        {
            double tmp = xSheet.Cells[startrow, colindex].Value == null ? 0 : xSheet.Cells[startrow + 0, colindex].Value, val = 0;
            int i = 0;
            for (i = 0; i < rows; i++)
            {
                val = xSheet.Cells[startrow + i, colindex].Value == null ? 0 : xSheet.Cells[startrow + i, colindex].Value;
                tmp = val < tmp ? val : tmp;
            }
            return tmp;

        }
        public double DataExcelColMax(int startrow, int colindex, int rows)
        {
            double tmp = xDataSheet.Cells[startrow, colindex].Value == null ? 0 : xDataSheet.Cells[startrow + 0, colindex].Value, val = 0;
            int i = 0;
            for (i = 0; i < rows; i++)
            {
                val = xDataSheet.Cells[startrow + i, colindex].Value == null ? 0 : xDataSheet.Cells[startrow + i, colindex].Value;
                tmp = val > tmp ? val : tmp;
            }
            return tmp;
        }
        public double DataExcelColMin(int startrow, int colindex, int rows)
        {
            double tmp = xDataSheet.Cells[startrow, colindex].Value == null ? 0 : xDataSheet.Cells[startrow + 0, colindex].Value, val = 0;
            int i = 0;
            for (i = 0; i < rows; i++)
            {
                val = xDataSheet.Cells[startrow + i, colindex].Value == null ? 0 : xDataSheet.Cells[startrow + i, colindex].Value;
                tmp = val < tmp ? val : tmp;
            }
            return tmp;

        }
        
        
    }
}
