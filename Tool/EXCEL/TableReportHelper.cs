﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool
{
    public partial class ExcelHelper
    {
        public static string savepath = "";
        
        /// <summary>
        /// 单点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValSPMC(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, string et, string xlsPath)
        {


            int setupHei = 0;
            int odd = -1;
            int len = 0;
            List<DataTable> ldt = Tool.DataTableHelper.ProcessDataTableSplitSPMC(originaldt);
            string[] tabName = tabHead.Split(',');
            int cnt = 0;
            int reportNo = new Random().Next(9999);
            int pageindexlen = 0;
            int datastartlen = 0;
            foreach (DataTable dt in ldt)
            {
                string tmp = st;
                tmp = "点名:" + dt.Rows[0].ItemArray[0].ToString();


                DataView dv = new DataView(dt);
                sheetName = dt.Rows[0].ItemArray[0].ToString();
                


                //if (dt.Columns.Count > 10) xSheet. = true;
                xSheet.Header.Left = "南方测绘";
                xSheet.Header.Right = DateTime.Now.ToString();
                xSheet.Footer.Center = "第&p页";
                xSheet.PrintSetup.PaperSize = 9;
                //sheet1.Workbook.SetRepeatingRowsAndColumns(0, 0, dt.Columns.Count, len+1, len+2);
                xSheet.FitToPage = false;
                xSheet.PrintSetup.Scale = 100;
                xSheet.HorizontallyCenter = true;
                //int k = 0;
                //IRow row = sheet1.CreateRow(len);
                //ICellStyle styleCell = GetICellStyle();
                //ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);


                pageindexlen = len + 7;
                len += (dt.Rows.Count % 31 == 0 ? dt.Rows.Count / 31 : dt.Rows.Count / 31 + 1) * 35;
                datastartlen += dt.Rows.Count + 1;
                int pageIndex = len % 35 == 0 ? len / 35 : len / 35 + 1;
                len = len % 35 == 0 ? (len / 35) * 35 + 70 : (len / 35) * 35 + 105;

                setupHei = 485 + (pageIndex - 1) * 475 + (odd * 5); 
                ExcelHelper.ChartAdd(Microsoft.Office.Interop.Excel.XlChartType.xlLine, 0, setupHei,600,350);
                setupHei = setupHei + 475 + (odd * -5);
                ExcelHelper.ChartAdd(Microsoft.Office.Interop.Excel.XlChartType.xlLine, 0, setupHei,600,350);
                cnt++;
            }

            




        }

        public static void ReportTemplateCreate(string xmname, string[] tabName, DataTable dt,  string sheetName, int len, int datastartlen)
        {


            int pagecnt = dt.Rows.Count % 31 == 0 ? dt.Rows.Count / 31 : dt.Rows.Count / 31 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            IRow datarow = null;
            ChartHelper chartHelper = new ChartHelper();
            int l = dt.Rows.Count;
            bool init = false;
            int cnt = 0;
            int cstartlen = 0;
            int cendlen = 0;
            //int reportNo = new Random().Next(9999);

            for (cnt = 0; cnt < pagecnt; cnt++)
            {

                //sheet1.SetRowBreak(len);
                //IRow row = null;
                //添加标题

                IRow row = null;//sheet1.CreateRow(len);
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                //CellBorderSet(row.CreateCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                //CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);

                //row.GetCell(0).SetCellValue(string.Format("{0}GNSS监测报表   报表编号{4}   {1}年{2}月{3}日", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo)
                //    );

                ////CellFontSet(row.GetCell(0), "宋体", 120, 14, false);
                //len++;
                int k = 0;
                //}

                //row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                //row.GetCell(0).SetCellValue(sheetName
                //    );
                //len++;

                //k = 0;
                //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                //CreateRow(len+1, sheet1, tabName.Length);//创建复表头行
                //sheet1.AddMergedRegion(new CellRangeAddress(len , len +1, 0, 0));
                //row.CreateCell(0).SetCellValue("点名");
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("高度（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("温度（°C）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(10).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                //CellFontSet(row.GetCell(10), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                ////return;
                ////k = 0;
                //len++;
                ICellStyle styleCell = GetICellStyle();
                ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

                //CreateRow(len, sheet1, tabName.Length);//创建复表头行
                // row = CreateRow(len , sheet1, tabName.Length);//创建复表头行
                ////sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                ////row.CreateCell(0).SetCellValue("时间");
                ////CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                ////CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(0).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("观测值（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("本次变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(7).SetCellValue("累计变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 7, 9));
                //CellFontSet(row.GetCell(7), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);


                //return;
                //k = 0;
                //len++;
                if (cnt == -1)
                {
                    row = CreateRow(len, sheet1, tabName.Length);//创建表头行


                    k = 0;

                    foreach (string name in tabName)
                    {
                        //if (k == 0) { k++; continue; }

                        if (k == dt.Columns.Count - 1)
                        {

                        }
                        //CellBorderSet(row.GetCell(k), styleBorder, true);
                        else
                        {
                            row.CreateCell(k).SetCellValue(name);
                            CellBorderSolid(row.GetCell(k), styleCell, true);
                        }
                        k++;
                    }

                    len++;
                }
                int i = 0;
                for (; i < 31; i++)
                {

                    if (30 * cnt + i > dt.Rows.Count - 1)
                    {
                        CreateRow(len, sheet1, 10);
                        len++;
                        continue;
                    }
                    styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                    //row = sheet1.CreateRow(len);
                    row = CreateRow(len, sheet1, 10);
                    datarow = sheet.CreateRow(datastartlen);
                    datastartlen++;
                    if (30 * cnt + i == 0)
                    {


                        datarow.CreateCell(0).SetCellValue("时间");
                        datarow.CreateCell(1).SetCellValue("ΔX");
                        datarow.CreateCell(2).SetCellValue("ΔY");
                        datarow.CreateCell(3).SetCellValue("ΔZ");
                        datarow.CreateCell(4).SetCellValue("ΣX");
                        datarow.CreateCell(5).SetCellValue("ΣY");
                        datarow.CreateCell(6).SetCellValue("ΣZ");
                        datarow.CreateCell(7).SetCellValue(sheetName);
                        //datastartlen++;
                        datarow = sheet.CreateRow(datastartlen);
                        datastartlen++;
                        datarow.CreateCell(0).SetCellValue(dt.Rows[30 * cnt + i].ItemArray[10].ToString());
                        datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[4]));
                        datarow.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[5]));
                        datarow.CreateCell(3).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[6]));
                        datarow.CreateCell(4).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[7]));
                        datarow.CreateCell(5).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[8]));
                        datarow.CreateCell(6).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[9]));

                    }
                    else
                    {
                        datarow.CreateCell(0).SetCellValue(dt.Rows[29 * cnt + i].ItemArray[10].ToString());
                        datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[4]));
                        datarow.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[5]));
                        datarow.CreateCell(3).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[6]));
                        datarow.CreateCell(4).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[7]));
                        datarow.CreateCell(5).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[8]));
                        datarow.CreateCell(6).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[9]));
                    }
                    int j = 0, t = 0;
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        //break;
                        //if (i == 0 && cnt == 0)
                        //{

                        if (j == 0) continue;
                        if (j == 10)
                        {
                            if (dt.Columns[j].DataType == Type.GetType("System.Double"))
                                row.CreateCell(0).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[j]));
                            else
                                row.CreateCell(0).SetCellValue(dt.Rows[30 * cnt + i].ItemArray[j].ToString());

                            CellFontSet(row.GetCell(0), "仿宋", 300, 16, false);
                            if (j == dt.Columns.Count - 1)

                                CellBorderSet(row.GetCell(0), styleBorder, false);
                            else
                                CellBorderSolid(row.GetCell(0), styleCell, true);
                        }
                        else
                        {
                            if (dt.Columns[j].DataType == Type.GetType("System.Double"))
                                row.CreateCell(t + 1).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[j]));
                            else
                                row.CreateCell(t + 1).SetCellValue(dt.Rows[30 * cnt + i].ItemArray[j].ToString());

                            CellFontSet(row.GetCell(j), "仿宋", 300, 16, false);
                            if (j == dt.Columns.Count - 1)

                                CellBorderSet(row.GetCell(t + 1), styleBorder, false);
                            else
                                CellBorderSolid(row.GetCell(t + 1), styleCell, true);
                        }
                        //}
                        //else
                        //{
                        //    if (dt.Columns[j].DataType == Type.GetType("System.Double"))
                        //        row.CreateCell(t).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i +1].ItemArray[j]));
                        //    else
                        //        row.CreateCell(t).SetCellValue(dt.Rows[30 * cnt + i+1].ItemArray[j].ToString());
                        //    CellFontSet(row.GetCell(j), "仿宋", 300, 16, false);
                        //    if (j == dt.Columns.Count - 1)

                        //        CellBorderSet(row.GetCell(t), styleBorder, false);
                        //    else
                        //        CellBorderSolid(row.GetCell(t), styleCell, true);
                        //}




                        //if (j == 1 || j == 2)
                        //{
                        //if (t == 0)
                        //{
                        //    row.CreateCell(t).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[3]));
                        //}
                        //else if (t == 1 || t == 3)
                        //{
                        //    row.CreateCell(t).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[2]));
                        //}
                        //else 
                        //if (i == 0 && t == 0) mindeep = Convert.ToDouble(dt.Rows[i].ItemArray[j]);
                        //    row.CreateCell(t).SetCellValue(Convert.ToDouble(dt.Rows[30 * cnt + i].ItemArray[j]));
                        //}
                        //else
                        //    row.CreateCell(t).SetCellValue(dt.Rows[30 * cnt + i].ItemArray[j].ToString());
                        //CellFontSet(row.GetCell(t), "仿宋", 300, 16, false);
                        //if (j == dt.Columns.Count - 1)

                        //    CellBorderSet(row.GetCell(t), styleBorder, false);
                        //else
                        //    CellBorderSolid(row.GetCell(t), styleCell, true);
                        t++;

                    }
                    len++;
                    //i++;

                }
                tmplen += 35;



            }

        }
    }
}
