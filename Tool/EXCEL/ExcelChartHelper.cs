﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using System.Collections;

namespace Tool
{
    partial class ExcelHelperHandle
    {
       
        public void main()
        {
            ExcelInit();
            //ChartAdd();
            GetChart();
            save();
        }
       
        //添加图表
        public  void ChartAdd()
        {
            for (int i = 0; i < 2; i++)
            {
                Microsoft.Office.Interop.Excel.Shape shape = xSheet.Shapes.AddChart(XlChartType.xlLine, 0, 100*i, 600, 350);
                Chart chart = shape.Chart;
                chart.Name = "曲线_" + i;
            }
        }
        public  Chart ChartAdd(XlChartType chartType,int left,double top,int width,int height )
        {
            Microsoft.Office.Interop.Excel.Shape shape = xSheet.Shapes.AddChart(chartType, left, top, width, height);
            Chart chart = shape.Chart;
            //Range cellRange = (Range)xSheet.get_Range("A" + 1, "A" + 1);
            //chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            return chart;
            //chart.Name = "曲线_" + new Random().Next(999);
        }
        
       


        //获取图表对象
        public  void GetChart()
        {
            object index = 1;
            Microsoft.Office.Interop.Excel.Shapes shapes = xSheet.Shapes;

            Console.Write(string.Format("有{0}个图形", shapes.Count));

            //var shapcollections = shapes.GetEnumerator();

            IEnumerator enumer = shapes.GetEnumerator();//这句返回从第二个元素起,找5个元素.结果为 2,3,4,5,6.如果此处换为GetEnumerator(),则返回所有

            while (enumer.MoveNext()) //一定要先movenext,不然会有异常
            {
                //Console.WriteLine(enumer.Current.ToString());
                Shape shape = (Shape)enumer.Current;
                Chart chart = shape.Chart;
                //chart.Activate();
                //chart.HasTitle = true;
                //chart.ti = "曲线图标题";
                chart.ChartType= XlChartType.xl3DPie;
            }
            //Console.ReadLine();

            //Chart chart = (Chart)shapes.GetEnumerator();
            //chart.Name = "第一个曲线图";

        }
        public  Chart GetChart(int index)
        {
            
            Microsoft.Office.Interop.Excel.Shapes shapes = xSheet.Shapes;

            //Console.Write(string.Format("有{0}个图形", shapes.Count));

            //var shapcollections = shapes.GetEnumerator();

            IEnumerator enumer = shapes.GetEnumerator();//这句返回从第二个元素起,找5个元素.结果为 2,3,4,5,6.如果此处换为GetEnumerator(),则返回所有
            int i = 0;
            while (enumer.MoveNext()) //一定要先movenext,不然会有异常
            {
                //Console.WriteLine(enumer.Current.ToString());
                Shape shape = (Shape)enumer.Current;
                Chart chart = shape.Chart;
                if (i == index) return chart;
                i++;
                //chart.Activate();
                //chart.HasTitle = true;
                //chart.ti = "曲线图标题";
                //chart.ChartType = XlChartType.xl3DPie;
            }
            return null;
            //Console.ReadLine();

            //Chart chart = (Chart)shapes.GetEnumerator();
            //chart.Name = "第一个曲线图";
        }

        public  void SeriesMarkStyleSet(Series s, int i)
        {
            switch (i)
            {
                case 1: s.MarkerStyle = XlMarkerStyle.xlMarkerStyleTriangle; break;
                case 2: s.MarkerStyle = XlMarkerStyle.xlMarkerStylePlus; break;
                case 3: s.MarkerStyle = XlMarkerStyle.xlMarkerStyleCircle; break;
            }
        }

        public  void SeriesColorSet(Series s, int i)
        {
            //switch (i)
            //{
            //    case 1: s.InvertColor = XlMarkerStyle.xlMarkerStyleTriangle; break;
            //    case 2: s.MarkerStyle = XlMarkerStyle.xlMarkerStylePlus; break;
            //    case 3: s.MarkerStyle = XlMarkerStyle.xlMarkerStyleCircle; break;
            //}
        }
        public static double setupHei = 0;
        /// <summary>
        /// 创建累计变化统计图         
        /// </summary>
        public void CreateAcChart(string title, int lenstart, int lenend, int colcont,  double mindeep, int pageIndex)
        {
            //xlSheet.Rows[breakRow+29].PageBreak = 1;
            double odd = -1;
            setupHei = (pageIndex - 1) * 729 + odd * 5 * 0;
            //setupHei = setupHei + 729;
            Microsoft.Office.Interop.Excel.Shape shape = xSheet.Shapes.AddChart(XlChartType.xlXYScatterLines,386, setupHei + 729 * 0.16863,115, 730.4 * 41.8 / 54);
            odd *= -1;
            //setupHei = setupHei + 732;
            Chart chart = shape.Chart;
            Range cellRange = (Range)(xDataSheet.get_Range("A" + lenstart, IndexColumn(colcont).ToString() + lenend));
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            chart.ChartArea.RoundedCorners = false;
            
            double absmax = 0;
            absmax = DataExcelColMax(lenstart + 1, 1, lenend - lenstart);
            //double maxdisp = ExcelColMax(lenstart+1, 3, lenend - lenstart);
            //absmax = maxpreviousdisp > maxdisp ? maxpreviousdisp : maxdisp;
            //cellRange = (Range)xlSheet.get_Range("D" + lenstart, "E" + lenend);
            //chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            absmax = absmax > 0 ? absmax : 0;
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //chart.PlotBy = XlRowCol.xlRows;
            chart.HasTitle = true;
            chart.ChartTitle.Text = "";
            //chart.Name = string.Format("{0}累计变化量曲线", xlSheet.Name);
            



            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);

            //Series s = (Series)grp.SeriesCollection(1);
            //s.Delete();
            //chart.Visible = XlSheetVisibility.
            grp.GapWidth = 20;

            grp.VaryByCategories = false;
            i = 1;
            for (i = 1; i < colcont-1 ; i++)
            {
                var s = (Series)grp.SeriesCollection(i);


                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
               
                s.Smooth = true;
                s.Name = "第"+(xDataSheet.Cells[lenstart-1, i+1].Value).ToString()+"周期";
                
                s.MarkerStyle = XlMarkerStyle.xlMarkerStyleNone;
                //s.MarkerSize = 4 ;
                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                chart.Legend.Position = XlLegendPosition.xlLegendPositionBottom;

                //SeriesMarkStyleSet(s, i);
                //s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.MinorTickMark = XlTickMark.xlTickMarkOutside;
                valueAxis.TickLabels.Font.Size = 10;
                valueAxis.TickLabels.Font.Bold = false;
                valueAxis.HasMajorGridlines = true;
                valueAxis.HasMinorGridlines = true;
                //valueAxis.HasTitle = true;
                //valueAxis.AxisTitle.Text = "深度(m)";
                valueAxis.CrossesAt = Convert.ToDouble(mindeep.ToString("0.00"));
                valueAxis.MinimumScaleIsAuto = false;
                valueAxis.MaximumScaleIsAuto = false;
                valueAxis.MaximumScale = 0;
                valueAxis.MinimumScale = Convert.ToDouble(mindeep.ToString("0.00")); ;
                valueAxis.MajorUnitIsAuto = false;
                valueAxis.MajorUnit = 1;
                valueAxis.MinorUnitIsAuto = false;
                valueAxis.MinorUnit = 0.5;
                valueAxis.MajorTickMark = XlTickMark.xlTickMarkOutside;
                valueAxis.MinorTickMark = XlTickMark.xlTickMarkOutside;

                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                //categoryAxis.HasTitle = true;
                //categoryAxis.AxisTitle.Text = "位移(mm)";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1; categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                categoryAxis.HasMinorGridlines = true;
                categoryAxis.CrossesAt = -5;
                categoryAxis.MinimumScaleIsAuto = false;
                categoryAxis.MaximumScaleIsAuto = false;
                categoryAxis.MaximumScale = 5;
                categoryAxis.MinimumScale = -5;//位移核心技术提升平台专业性
            }

        }
        public char IndexColumn(int colindex)
        {
            char word =  (char)((int)'A' + colindex - 2);
            return word; 
        }
      
    }
}
