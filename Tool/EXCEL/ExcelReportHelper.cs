﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using System.Collections;

namespace Tool
{
    public partial class ExcelHelperHandle
    {
        public  Microsoft.Office.Interop.Excel.Application xApp = null;
        public  Microsoft.Office.Interop.Excel.Worksheet xSheet = null;
        public  Microsoft.Office.Interop.Excel.Worksheet xDataSheet = null;
        public  Microsoft.Office.Interop.Excel.Workbook xBook = null;
        public  string workbookpath;
        public  void programmain()
        {
            try
            {

                //ExcelChartAdd();
                //ExcelWordsWrite();
                //ExcelChartDataBind();
                //save();
                workbookpath = "d:/白云石丰路131720377363296708713.xlsx";
                ExcelInit();
                //ColumnWidthSet(xSheet,1,50);
                //DateFormat(xSheet, 1, "yy/mm/dd hh:MM:ss");
                //BorderSet(1,1,4,6);
                ExcelColMax(1,1,5);
                save();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public  void ExcelChartAdd()
        {
            ExcelInit();
            ChartAdd(XlChartType.xlLine, 0, 100, 600, 400);
            ChartAdd(XlChartType.xlLine, 0, 200, 600, 400);
        }
        public  void ExcelWordsWrite()
        {
            int i = 1, j = 1;
            for (; i < 4; i++)
            {
                for (j = 1; j < 5; j++)
                {
                    ExcelWrite(i, j, i * j);
                }
            }

        }
        public  void ExcelChartDataBind()
        {
            Chart chart = GetChart(1);
            Range cellRange = (Range)xDataSheet.get_Range("A" + 1, "D" + 3);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            //chart.Name = "1";
        }
        public  void ExcelChartDataBind(int index, string startcell, string endcell)
        {
            Chart chart = GetChart(index);
            Range cellRange = (Range)xDataSheet.get_Range(startcell, startcell);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);

        }
        public  void ExcelChartDataBind(Chart chart, string startcell, string endcell)
        {

            Range cellRange = (Range)xDataSheet.get_Range(startcell, endcell);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);

        }

        public  void ExcelInit()
        {
            xApp = new Microsoft.Office.Interop.Excel.Application();

            xApp.Visible = true;
            //得到WorkBook对象, 可以用两种方式之一: 下面的是打开已有的文件 
            xBook = xApp.Workbooks._Open(workbookpath,
              Missing.Value, Missing.Value, Missing.Value, Missing.Value
              , Missing.Value, Missing.Value, Missing.Value, Missing.Value
              , Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            xSheet = (Microsoft.Office.Interop.Excel.Worksheet)xBook.Sheets[1];
            xSheet.Activate();
        }
        public  void SheetInit(int rindex, int dindex)
        {
            xSheet = (Microsoft.Office.Interop.Excel.Worksheet)xBook.Sheets[rindex];
            xDataSheet = (Microsoft.Office.Interop.Excel.Worksheet)xBook.Sheets[dindex];
        }
        public void SheetInit(int rindex)
        {
            xSheet = (Microsoft.Office.Interop.Excel.Worksheet)xBook.Sheets[rindex];
            xSheet.Activate();
        }
        public void SheetInit(string sheetname)
        {
            xSheet = (Microsoft.Office.Interop.Excel.Worksheet)xBook.Sheets[sheetname];
            xSheet.Activate();
        }
        //保存
        public  void save()
        {
            xBook.Save();
            xSheet = null;
            xBook = null;
            xApp.Quit(); //这一句是非常重要的，否则Excel对象不能从内存中退出 
            xApp = null;
        }

        public void SeriesCharacterCreate(Chart chart, string title, string[] seriesName, int[] indexRange, XlMarkerStyle markStyle, bool secaxies)
        {
            //string[] seriesName = seriesNameStr.Split(',');
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;

            int len = indexRange.Count();
            chart.HasLegend = len == 1 ? false : true;
            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            //Series sv = (Series)grp.SeriesCollection(1);
            //sv = (Series)grp.SeriesCollection(2);
            //sv = (Series)grp.SeriesCollection(3);
            //IEnumerator enumer = grp.SeriesCollection(1);//这句返回从第二个元素起,找5个元素.结果为 2,3,4,5,6.如果此处换为GetEnumerator(),则返回所有
            //while (enumer.MoveNext()) //一定要先movenext,不然会有异常
            //{
            //    Series s = (Series)enumer.Current;
            //    if(!indexRange.Contains(i))s.Delete();
            //}
            for (idx = 1; idx < indexRange[0]; idx++)
            {

                Series s = (Series)grp.SeriesCollection(1);
                s.Delete();

            }

            grp.GapWidth = 20;

            grp.VaryByCategories = true;

            for (i = 1; i < len + 1; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);
                if (len == 1) s.InvertColorIndex = 1;
                s.Smooth = true;
                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                s.Name = seriesName[i - 1];
                if (len > 1)
                {
                    SeriesMarkStyleSet(s, i);
                    s.MarkerSize = 4;
                }
                else
                    s.MarkerStyle = XlMarkerStyle.xlMarkerStyleNone;

                if (len == 2 && i == 2)
                    s.AxisGroup = XlAxisGroup.xlSecondary;
                s.Format.Line.Weight = 2;
                if (chart.HasLegend)
                    chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = false;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1; categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = false;
            }


        }

        public  void SeriesCharacterCreate(Chart chart, string title, string[] seriesName, int[] indexRange, XlMarkerStyle markStyle, bool secaxies,string unitX,string unitY)
        {
            //string[] seriesName = seriesNameStr.Split(',');
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;

            int len = indexRange.Count();
            chart.HasLegend = len == 1 ? false : true;
            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            //Series sv = (Series)grp.SeriesCollection(1);
            //sv = (Series)grp.SeriesCollection(2);
            //sv = (Series)grp.SeriesCollection(3);
            //IEnumerator enumer = grp.SeriesCollection(1);//这句返回从第二个元素起,找5个元素.结果为 2,3,4,5,6.如果此处换为GetEnumerator(),则返回所有
            //while (enumer.MoveNext()) //一定要先movenext,不然会有异常
            //{
            //    Series s = (Series)enumer.Current;
            //    if(!indexRange.Contains(i))s.Delete();
            //}
            for (idx = 1; idx < indexRange[0]; idx++)
            {

                Series s = (Series)grp.SeriesCollection(1);
                s.Delete();

            }

            grp.GapWidth = 20;

            grp.VaryByCategories = true;

            for (i = 1; i < len + 1; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);
                if (len == 1) s.InvertColorIndex = 1;
                s.Smooth = true;
                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                s.Name = seriesName[i - 1];
                if (len > 1)
                {
                    SeriesMarkStyleSet(s, i);
                    s.MarkerSize = 4;
                }
                else
                    s.MarkerStyle = XlMarkerStyle.xlMarkerStyleNone;

                if (len == 2&&i==2)
                    s.AxisGroup = XlAxisGroup.xlSecondary;
                s.Format.Line.Weight = 2;
                if (chart.HasLegend)
                    chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
                valueAxis.HasTitle = true;
                valueAxis.AxisTitle.Text = unitY;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = false;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                categoryAxis.HasTitle = true;
                categoryAxis.AxisTitle.Text = unitX;
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1; categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = false;
            }


        }
        public void SeriesCharacterCreate(Chart chart, string title, string[] seriesName, int[] indexRange, XlMarkerStyle markStyle, bool secaxies, bool markstyle, string unitX, string unitY)
        {
            //string[] seriesName = seriesNameStr.Split(',');
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;

            int len = indexRange.Count();
            chart.HasLegend = len == 1 ? false : true;
            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            //Series sv = (Series)grp.SeriesCollection(1);
            //sv = (Series)grp.SeriesCollection(2);
            //sv = (Series)grp.SeriesCollection(3);
            //IEnumerator enumer = grp.SeriesCollection(1);//这句返回从第二个元素起,找5个元素.结果为 2,3,4,5,6.如果此处换为GetEnumerator(),则返回所有
            //while (enumer.MoveNext()) //一定要先movenext,不然会有异常
            //{
            //    Series s = (Series)enumer.Current;
            //    if(!indexRange.Contains(i))s.Delete();
            //}
            for (idx = 1; idx < indexRange[0]; idx++)
            {

                Series s = (Series)grp.SeriesCollection(1);
                s.Delete();

            }

            grp.GapWidth = 20;

            grp.VaryByCategories = true;

            for (i = 1; i < len + 1; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);
                if (len == 1) s.InvertColorIndex = 1;
                s.Smooth = true;
                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                s.Name = seriesName[i - 1];
                if (markstyle)
                {
                    SeriesMarkStyleSet(s, i);
                    s.MarkerSize = 4;
                }
                else
                    s.MarkerStyle = XlMarkerStyle.xlMarkerStyleNone;

                if (len == 2 && i == 2)
                    s.AxisGroup = XlAxisGroup.xlSecondary;
                s.Format.Line.Weight = 2;
                if (chart.HasLegend)
                    chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
                valueAxis.HasTitle = true;
                valueAxis.AxisTitle.Text = unitY;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = false;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                categoryAxis.HasTitle = true;
                categoryAxis.AxisTitle.Text = unitX;
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1; categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = false;
            }


        }
        //public  void DoubleAxiesSeriesCharacterCreate(Chart chart, string title, string[] seriesName, int[] indexRange, XlMarkerStyle markStyle)
        //{
        //    chart.HasTitle = true;
        //    chart.ChartTitle.Text = title;
        //    int len = indexRange.Count();
        //    int i = 0;
        //    ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
        //    int idx = 0;
        //    for (idx = 1; idx < indexRange[0]; idx++)
        //    {

        //        Series s = (Series)grp.SeriesCollection(1);
        //        s.Delete();

        //    }

        //    grp.GapWidth = 20;

        //    grp.VaryByCategories = true;

        //    for (i = 1; i < len + 1; i++)
        //    {

        //        Series s = (Series)grp.SeriesCollection(i);
        //        s.BarShape = XlBarShape.xlCylinder;
        //        s.HasDataLabels = false;
        //        s.Name = seriesName[i - 1];
        //        if (i == 2)
        //            s.AxisGroup = XlAxisGroup.xlSecondary;
        //        s.Format.Line.Weight = 2;
        //        chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //        s.MarkerSize = 4;
        //        Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
        //        valueAxis.TickLabels.Font.Size = 12;
        //        valueAxis.TickLabels.Font.Bold = true;
        //        valueAxis.HasMajorGridlines = true;
        //        Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
        //        categoryAxis.TickLabels.Font.Size = 10;
        //        categoryAxis.TickLabels.Font.Bold = false;
        //        categoryAxis.TickLabelSpacing = 1; categoryAxis.TickLabelSpacingIsAuto = true;
        //        categoryAxis.CategoryType = XlCategoryType.xlTimeScale;
        //        categoryAxis.BaseUnitIsAuto = true;
        //        categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
        //        categoryAxis.HasMajorGridlines = true;
        //    }


        //}
        public void RowFormat(int len, int colCnt)
        {
            
            for (int i = 0; i < colCnt + 1; i++)
            {

                BorderSet(len, i + 1, len, i + 1);

            }

        }


    }
}
