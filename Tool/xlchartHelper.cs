﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;

namespace Tool
{
    class xlchartHelper
    {
        public Application ThisApplication = null;
        public Workbooks m_objBooks = null;
        public _Workbook ThisWorkbook = null;
        public Worksheet xlSheet = null;
        #region 柱状图
        public void main()
        {
            //CreateChart();
            button4_Click("E:\\数据测试_表面位移_第1周期-第6周期数据.xls", 6, "周期");
        }

        public void DeleteSheet()
        {
            foreach (Worksheet ws in ThisWorkbook.Worksheets)
                if (ws != ThisApplication.ActiveSheet)
                {
                    ws.Delete();
                }
            foreach (Chart cht in ThisWorkbook.Charts)

                cht.Delete();
        }
        private void AddDatasheet(string sheetName)
        {

            xlSheet = ThisWorkbook.Sheets.get_Item(1);
            //(Worksheet)ThisWorkbook.

            //    Worksheets.Add(Type.Missing, ThisWorkbook.ActiveSheet,

            //    Type.Missing, Type.Missing);
            xlSheet.Activate();


            //xlSheet.Name = "数据";

        }
        private void LoadData()
        {

            Random ran = new Random();

            for (int i = 1; i <= 12; i++)
            {

                xlSheet.Cells[i, 1] = i.ToString() + "月";

                xlSheet.Cells[i, 2] = ran.Next(2000).ToString();

            }

        }

        private void LoadDatadb()
        {



            Random ran = new Random();



            for (int i = 1; i <= 12; i++)
            {

                xlSheet.Cells[i, 1] = i + "周期";

                xlSheet.Cells[i, 2] = ran.Next(2000).ToString();

                xlSheet.Cells[i, 3] = ran.Next(1500).ToString();

                xlSheet.Cells[i, 4] = ran.Next(1500).ToString();

                xlSheet.Cells[i + 1, 8] = i + "周期";

                xlSheet.Cells[i + 1, 9] = ran.Next(2000).ToString();

                xlSheet.Cells[i + 1, 10] = ran.Next(1500).ToString();

                xlSheet.Cells[i + 1, 11] = ran.Next(1500).ToString();

            }

        }

        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateThisChart(int rowcnt, string xAixeTitle)
        {
            Chart xlChart = (Chart)ThisWorkbook.Charts.
                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);

            Range cellRange = (Range)xlSheet.get_Range("A3", "J" + 2 + rowcnt);
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            xlChart.ChartWizard(cellRange,
                XlChartType.xlLine, Type.Missing,
                XlRowCol.xlColumns, 1, 0, true,
               Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
                "");

            xlChart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);

            string[] seriesName = { "ΔN", "ΔE", "ΔZ" };

            int i = 0;
            ChartGroup grp = (ChartGroup)xlChart.ChartGroups(1);
            int idx = 0;
            for (idx = 1; idx < 10; idx++)
            {
                if (idx < 4)
                {
                    Series s = (Series)grp.SeriesCollection(1);
                    s.Delete();
                }
                if (idx > 6)
                {
                    Series s = (Series)grp.SeriesCollection(4);
                    s.Delete();
                }
            }
            grp.GapWidth = 20;

            grp.VaryByCategories = true;
            for (i = 1; i < 4; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = true;
                s.Name = seriesName[i - 1];

                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                xlChart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                s.MarkerSize = 6;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)xlChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
                valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 18;
                valueAxis.TickLabels.Font.Bold = true;
                Axis categoryAxis = (Axis)xlChart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 18;
                categoryAxis.TickLabels.Font.Bold = true;
                categoryAxis.TickLabelSpacing = 1;
                categoryAxis.CategoryType = XlCategoryType.xlTimeScale;
                categoryAxis.BaseUnitIsAuto = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }




        }
        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateAcChart(int rowcnt, string xAixeTitle)
        {
            Chart xlChart = (Chart)ThisWorkbook.Charts.
                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);

            Range cellRange = (Range)xlSheet.get_Range("A3", "J" + 2 + rowcnt);
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            xlChart.ChartWizard(cellRange,
                XlChartType.xlLine, Type.Missing,
                XlRowCol.xlColumns, 1, 0, true,
               Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "累计变化量",
                "");

            xlChart.Name = string.Format("{0}累计变化量曲线", xlSheet.Name);

            string[] seriesName = { "ΣΔN(m)", "ΣΔE", "ΣΔZ" };

            int i = 0;
            ChartGroup grp = (ChartGroup)xlChart.ChartGroups(1);
            int idx = 0;
            for (idx = 1; idx < 10; idx++)
            {
                if (idx < 7)
                {
                    Series s = (Series)grp.SeriesCollection(1);
                    s.Delete();
                }

            }
            grp.GapWidth = 20;

            grp.VaryByCategories = true;
            for (i = 1; i < 4; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = true;
                s.Name = seriesName[i - 1];

                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                xlChart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                s.MarkerSize = 6;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)xlChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
                valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 18;
                valueAxis.TickLabels.Font.Bold = true;
                Axis categoryAxis = (Axis)xlChart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 18;
                categoryAxis.TickLabels.Font.Bold = true;
                categoryAxis.TickLabelSpacing = 1;
                categoryAxis.CategoryType = XlCategoryType.xlTimeScale;
                categoryAxis.BaseUnitIsAuto = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }




        }
        public void chartcreate(string sheetName, int rowcnt, string xAixeTitle)
        {

            ThisApplication.DisplayAlerts = false;
            xlSheet = ThisWorkbook.Sheets.get_Item(sheetName);
            xlSheet.Activate();
            //this.DeleteSheet();
            //this.AddDatasheet(sheetName);
            //this.LoadDatadb();
            //this.WorkSheetCopy("D5","D5");
            this.CreateThisChart(rowcnt, xAixeTitle);
            this.CreateAcChart(rowcnt, xAixeTitle);
        }
        public void ReportChart(string path, int rowcnt, string xAixsTitle)
        {
            try
            {
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add(path));
                List<string> sheetsName = new List<string>();
                foreach (Worksheet sheet in ThisWorkbook.Sheets)
                {
                    sheetsName.Add(sheet.Name);
                }
                foreach (string sheetName in sheetsName)
                {
                    chartcreate(sheetName, rowcnt, xAixsTitle);
                }


                ThisWorkbook.SaveAs("E:\\Book2.xls", Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                string a = "";
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();
                //this.Close();
            }

        }
        #endregion
        #region 曲线图






        #endregion
    }
}
