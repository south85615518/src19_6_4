﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool
{
   public class DataProcessHelper
    {
        public static string Compare(string arg0,string arg1 ,object x, object y,out bool result,bool resultRegion)
        {
            result = resultRegion;
            //ExceptionLog.ExceptionWrite(Convert.ToDouble(x) + " 的绝对值>=" + Convert.ToDouble(y) + "?" + (Math.Abs(Convert.ToDouble(x))  >= Convert.ToDouble(y)).ToString());
            if (Math.Abs(Convert.ToDouble(x)) >= Convert.ToDouble(y))  result = true;
            
            string resultInfo = Math.Abs(Convert.ToDouble(x)) >= Convert.ToDouble(y) ? string.Format("{0}", (Convert.ToDouble(x) / Math.Abs(Convert.ToDouble(x))) * (double)Math.Round( (decimal)(Math.Abs(Convert.ToDouble(x)) - Convert.ToDouble(y)),1,MidpointRounding.AwayFromZero)) : "0";
            return string.Format("{0}{1}{2}", arg0, arg1, resultInfo);
        }
        public static string ColorDispaly(int alarm)
        {
            switch (alarm)
            {

                case 1: return "黄";
                case 2: return "橙";
                case 3: return "红";
                default: return "绿";
            }

        }
        public static string RangeCompareU(string arg0, string arg1, object x, object y, out bool result, bool resultRegion)
        {
            result = resultRegion;

            if (Convert.ToDouble(x) >= Convert.ToDouble(y)) { 
                result = true;
                ExceptionLog.ExceptionWrite(string.Format("值{0}上限预警值{1}",x,y));
            }
            
            string resultInfo = Convert.ToDouble(x) >= Convert.ToDouble(y) ? string.Format("{0}", (double)Math.Round((decimal)(Convert.ToDouble(x) - Convert.ToDouble(y)), 1, MidpointRounding.AwayFromZero)) : "0";
            return string.Format("{0}{1}{2}", arg0, arg1, resultInfo);
        }
        public static string RangeCompareL(string arg0, string arg1, object x, object y, out bool result, bool resultRegion)
        {
            result = resultRegion;
            if (Convert.ToDouble(x) < Convert.ToDouble(y)) { 
                result = true;
                ExceptionLog.ExceptionWrite(string.Format("值{0}下限预警值{1}", x, y));
            }

            string resultInfo = Convert.ToDouble(x) < Convert.ToDouble(y) ? string.Format("{0}", (double)Math.Round((decimal)(Convert.ToDouble(x) - Convert.ToDouble(y)), 1, MidpointRounding.AwayFromZero)) : "0";
            return string.Format("{0}{1}{2}", arg0, arg1, resultInfo);
        }
    }
}
