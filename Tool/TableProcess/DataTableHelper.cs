﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool
{
    public class DataTableHelper
    {
        public static DataTable ProcessDataTableCopy(DataTable originalTab)
        {
            DataTable dt = new DataTable();
            foreach (DataColumn dc in originalTab.Columns)
            {
                dt.Columns.Add(dc.ColumnName, dc.DataType);
            }
            return dt;
        }
        public static List<DataTable> ProcessDataTableSplitSPMC(DataTable originalTab)
        {
            ExceptionLog.ExceptionWrite("单点多周期分表");
            if (originalTab.Rows.Count == 0) return new List<DataTable>();
            List<DataTable> ldt = new List<DataTable>();
            DataRow tmp = originalTab.Rows[0];
            DataTable dt = new DataTable();
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));

            foreach (DataRow dr in originalTab.Rows)
            {

                if (tmp.ItemArray[0].ToString() != dr.ItemArray[0].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );
                    ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr;

                }
                ldt[i].ImportRow(dr);

            }
            return ldt;
        }
        public static int ProcessDataTableRowsCount(DataTable originalTab,string filterName)
        {
            DataView dv = new DataView(originalTab);
            return dv.ToTable(originalTab.TableName, true, filterName).Rows.Count;
            
        }

        public static List<DataTable> ProcessGTSensorDataDataTableSplit(DataTable originalTab)
        {
            List<DataTable> ldt = new List<DataTable>();

            DataTable dt = new DataTable();
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
            DataView dv = new DataView(originalTab);
            dv.Sort = "  datatype,time,point_name asc  ";
            //originalTab.DefaultView.Sort = " cyc,point_name asc ";
            DataRowView tmp = dv[0]; //originalTab.Rows[0];
            foreach (DataRowView dr in dv)
            {

                if (tmp[1].ToString() != dr[1].ToString() && tmp[2].ToString() != dr[2].ToString() && tmp[5].ToString() != dr[5].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );
                    ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr;

                }
                ldt[i].ImportRow(dr.Row);

            }
            return ldt;
        }



        public static List<DataTable> ProcessGTInclinometerDataTableSplit(DataTable originalTab)
        {

            if (originalTab == null) return new List<DataTable>();

            List<string> colnamelist = new List<string>();
            int i = 0;
            for (i = 0; i < originalTab.Columns.Count; i++)
            {
                colnamelist.Add(originalTab.Columns[i].ColumnName);
            }
            DataView dv = new DataView(originalTab);
            List<DataTable> ldt = new List<DataTable>();

            DataTable dt = new DataTable();
            i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
            dv = new DataView(originalTab);
            int colindex = 0;
            if (colnamelist.Contains("point_name"))
            {
                dv.Sort = "point_name,time,deepth asc  ";
                colindex = 3;
            }
            else
            {
                dv.Sort = "holename,time,deepth asc  ";
                colindex = 2;
            }
            //originalTab.DefaultView.Sort = " cyc,point_name asc ";
            DataRowView tmp = dv[0]; //originalTab.Rows[0];
            foreach (DataRowView dr in dv)
            {

                if (tmp[0].ToString() != dr[0].ToString() || tmp[colindex].ToString() != dr[colindex].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );
                    ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr;

                }
                ldt[i].ImportRow(dr.Row);

            }
            return ldt;
        }


        public static List<DataTable> ProcessDataTableSplitMPSC(DataTable originalTab )
        {
            List<DataTable> ldt = new List<DataTable>();
            
            DataTable dt = new DataTable();
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
            DataView dv = new DataView(originalTab);
            dv.Sort = "cyc,point_name asc";
            //originalTab.DefaultView.Sort = " cyc,point_name asc ";
            DataRowView tmp = dv[0]; //originalTab.Rows[0];
            foreach (DataRowView dr in dv)
            {

                if (tmp[1].ToString() != dr[1].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );
                    ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr; 

                }
                ldt[i].ImportRow(dr.Row);

            }
            return ldt;
        }

        public static List<DataTable> ProcessSurfaceDataDataTableSplitMPSC(DataTable originalTab)
        {
            List<DataTable> ldt = new List<DataTable>();

            DataTable dt = new DataTable();
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
            DataView dv = new DataView(originalTab);
            dv.Sort = "cyc,surfacename asc";
            //originalTab.DefaultView.Sort = " cyc,point_name asc ";
            DataRowView tmp = dv[0]; //originalTab.Rows[0];
            foreach (DataRowView dr in dv)
            {

                if (tmp[1].ToString() != dr[1].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );
                    ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr;

                }
                ldt[i].ImportRow(dr.Row);

            }
            return ldt;
        }



        public static List<DataTable> ProcessDataTableSplitInclinometerSPMC(DataTable originalTab)
        {
            List<DataTable> ldt = new List<DataTable>();

            DataTable dt = new DataTable();
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
            DataView dv = new DataView(originalTab);
            dv.Sort = "chain_name,taskid,deep asc";
            //originalTab.DefaultView.Sort = " cyc,point_name asc ";
            DataRowView tmp = dv[0]; //originalTab.Rows[0];
            foreach (DataRowView dr in dv)
            {

                if (tmp[1].ToString() != dr[1].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );
                    ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr;

                }
                ldt[i].ImportRow(dr.Row);

            }
            return ldt;
        }

        public static List<DataTable> ProcessGTSensorDataTableSplitSPMT(DataTable originalTab)
        {
            List<DataTable> ldt = new List<DataTable>();

            DataTable dt = new DataTable();
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
            DataView dv = new DataView(originalTab);
            dv.Sort = "point_name,time asc";
            //originalTab.DefaultView.Sort = " cyc,point_name asc ";
            DataRowView tmp = dv[0]; //originalTab.Rows[0];
            foreach (DataRowView dr in dv)
            {

                if (tmp[0].ToString() != dr[0].ToString())
                {
                    //lp.Add(new PrintTableCondition(tmp.ItemArray[0].ToString(),ldt[i]) );
                    ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
                    ++i;
                    tmp = dr;

                }
                ldt[i].ImportRow(dr.Row);

            }
            return ldt;
        }

        public static List<string> ProcessDataTableFilter(DataTable originalTab,string filterName)
        {

            if(originalTab == null) return new List<string>();

            List<string> colnamelist = new List<string>();
            int i = 0;
            for (i = 0; i < originalTab.Columns.Count;i++ )
            {
                colnamelist.Add(originalTab.Columns[i].ColumnName);
            }
            DataView dv = new DataView(originalTab);
            if(filterName.IndexOf("cyc") != -1)
            if(colnamelist.Contains("surfacename"))
            dv.Sort = "cyc,surfacename asc";
            else if (colnamelist.Contains("point_name"))
            dv.Sort = "cyc,point_name asc";
            else if (colnamelist.Contains("holename"))
            dv.Sort = "cyc,holename,deepth asc";
            DataTable dt = dv.ToTable(originalTab.TableName, true, filterName);
            //DataView dvrow = new DataView(dt);
            List<string> pnames = new List<string>();
             i = 0;
            for (i = 0; i < dt.Rows.Count;i++ )
            {
                
                
               pnames.Add(dt.Rows[i].ItemArray[0].ToString());
                
               
            }
            return pnames;
            //List<string> ls = new List<string>();
            //foreach (int p in pnames)
            //{
            //    ls.Add(p.ToString());
            //}
            //return ls;
        }

        public static DataTable ProcessDataTableMerge(List<DataTable> ldt)
        {
            DataTable dt = ProcessDataTableCopy(ldt[0]);
            int i = 0;
            //List<printtable> lp = new List<printtable>();
            //ldt.Add(Tool.DataTableHelper.ProcessDataTableCopy(originalTab));
            //DataView dv = new DataView(originalTab);
            //dv.Sort = "cyc,point_name asc";
            //originalTab.DefaultView.Sort = " cyc,point_name asc ";
            //DataRowView tmp = dv[0]; //originalTab.Rows[0];
            foreach (DataTable tb in ldt)
            {
                DataView dv = new DataView(tb);
                foreach(DataRowView drv in dv)
                dt.ImportRow(drv.Row);

            }
            return dt;
        }

        /// <summary>
        /// 根据表的分页单位获取当前记录在表中的页数
        /// </summary>
        public static int PageIndexFromTab(string pointname, string date, DataTable dt, string pointnameStr, string dateStr, int pageSize)
        {

            DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr];
                ExceptionLog.ExceptionWrite(a);
                if (drv[pointnameStr].ToString() == pointname && Convert.ToDateTime(date.Trim()) == Convert.ToDateTime(drv[dateStr].ToString()))
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }


        /// <summary>
        /// 根据表的分页单位获取当前记录在表中的页数
        /// </summary>
        public static int PageIndexFromTab(string chain, double deep ,string date, DataTable dt, string chainStr,string deepstr ,string dateStr, int pageSize)
        {

            DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            //string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[chainStr].ToString() + "=" + chain + "|" + deep + "=" + drv[deepstr] + "|" + date + "=" + drv[dateStr].ToString().Substring(0, drv[dateStr].ToString().LastIndexOf(":"));
                ExceptionLog.ExceptionWrite(a);
                if (drv[chainStr].ToString() == chain && Convert.ToDouble(drv[deepstr].ToString()) == deep && Convert.ToDateTime(date.Trim()) == Convert.ToDateTime(drv[dateStr].ToString()))
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }

        
    }
}
