﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Linq;
namespace Tool
{
    /// <summary>
    /// 异常文档
    /// </summary>
    public class ExceptionLog
    {

        public static string xmPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "项目日志";


        public static string defaultPath =System.AppDomain.CurrentDomain.BaseDirectory + "\\"+"异常日志";
        public static string alarmPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "预警日志";
        public static string checkPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "自检日志";
        public static string dTUPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU日志";
        
        public static string dTUCommendPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU命令日志";
        public static string dTUInspectionPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU端口巡检日志";

        public static string ngnPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU日志";
        public static string ngnCommendPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU命令日志";
        public static string ngnInspectionPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "DTU端口巡检日志";

        public static string gtsettlementPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "广铁沉降数据接收日志";



        public static string ngnFixedInclinometerPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "固定测斜日志";
        public static string ngnFixedInclinometerCommendPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "固定测斜命令日志";
        public static string ngnFixedInclinometerInspectionPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "固定测斜端口巡检日志";

        public static string ngnhxylPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "NGN雨量日志";
        public static string ngnhxylCommendPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "NGN雨量命令日志";
        public static string ngnhxylInspectionPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "NGN雨量端口巡检日志";


        public static string pointcheckvedioPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "自检影像";
        public static string pointalarmvedioPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "预警影像";

        public static string versionpath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "版本日志";
        public static string versionupdpath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "版本更新日志";
        public static string dbupdpath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据库更新日志";
        public static string reportupdpath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "报表日志";
        public static string reportprocesspath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据报表";
        public static string reportprocessdelegaterecpath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "报表托管接收日志";
        public static string reportcreatedelegaterecpath = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "报表托管生成日志";
        public static string reportrecpathsetting = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\reportrecsetting.txt";

        public static bool logwrite = true;
        public const int CLEARSIZE = 500; 


        #region 预警
        public static void TotalSationPointCheckVedioWrite(string mssg)
        {
            string tx = string.Format("{0}\r\n{1}\r\n", DateTime.Now.ToString(), mssg);
            string realPath = pointcheckvedioPath + "\\表面位移";
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        public static void TotalSationPointAlarmVedioWrite(string mssg)
        {
            string tx = string.Format("{0}\r\n{1}\r\n", DateTime.Now.ToString(), mssg);
            string realPath = pointalarmvedioPath + "\\表面位移";
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        #endregion


        public static void ExceptionWrite(Exception ex)
        {
            if (!logwrite) return;
            FileStream fs = null;
            try
            {

                string tx = string.Format("{0} \r\n{1}\r\n {2}\r\n", DateTime.Now.ToString(), ex.Message, ex.StackTrace.ToString());
                string realPath = defaultPath;
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ExceptionWrite(string sql)
        {
            bool isneedclear = false;
            if (!logwrite) return;
            string tx = string.Format("{0}\r\n{1}\r\n", DateTime.Now.ToString(), sql);
            string realPath = defaultPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                if (fs.Length > 1024 * CLEARSIZE)
                {
                    isneedclear = true;
                }
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
                if (isneedclear)
                {
                    FileHelper.FileZip(realPath);
                    Tool.FileHelper.DirFilesDel(realPath);
                }
            }
        }

        /// <summary>
        /// 日志分项目
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void XmRecordWrite(string sql, string xmname)
        {
            bool isneedclear = false;
            if (!logwrite) return;
            string tx = string.Format("{0}\r\n   {1}", sql, DateTime.Now);
            string realPath = xmPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/" + xmname + ".txt", FileMode.OpenOrCreate, FileAccess.Write);
                if (fs.Length > 1024 * CLEARSIZE)
                {
                    isneedclear = true;
                }
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
                if (isneedclear)
                {
                    FileHelper.FileZip(realPath);
                    Tool.FileHelper.DirFilesDel(realPath);
                }
            }
        }
      
        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ReportLineWrite(string sql,string reportname)
        {
            string tx = string.Format("{0}\r\n{1}\r\n", DateTime.Now.ToString(), sql);
            string realPath = string.Format("{0}\\{1}.txt", reportprocesspath, reportname);
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(realPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(realPath));
                }
                fs = File.Open(realPath, FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ReportLineWrite(int tabcount, string reportname)
        {

            //reportname = Path.GetFileNameWithoutExtension(reportname) + ".txt";
            FileStream fs = null;
            try
            {
                string realPath = string.Format("{0}\\{1}.txt", reportprocesspath, Path.GetFileNameWithoutExtension(reportname));
                if (!File.Exists(realPath))
                {
                    try
                    {
                        fs = File.Open(realPath, FileMode.Create, FileAccess.Write);
                        fs.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        fs.Close();
                    }
                }
                string countstr = Tool.FileHelper.ProcessSettingString(realPath);
                int count = countstr == null || countstr == "" ? 0 : Convert.ToInt32(countstr.Split('/')[0]);
                string tx = string.Format("{0}/{1} {2}", count + 1 > tabcount ? tabcount : count + 1, tabcount, DateTime.Now.ToString());
                Console.WriteLine(tx);
                if (!Directory.Exists(Path.GetDirectoryName(realPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(realPath));
                }
                fs = File.Open(realPath, FileMode.Truncate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        public static void ReportLineWrite(int tabcount,string indexshot ,string reportname)
        {

            //reportname = Path.GetFileNameWithoutExtension(reportname) + ".txt";
            FileStream fs = null;
            try
            {
                string realPath = string.Format("{0}\\{1}.txt", reportprocesspath, Path.GetFileNameWithoutExtension(reportname));
                if (!File.Exists(realPath))
                {
                    try
                    {
                        fs = File.Open(realPath, FileMode.Create, FileAccess.Write);
                        fs.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        fs.Close();
                    }
                }
                string countstr = Tool.FileHelper.ProcessSettingString(realPath);
                int count = countstr == null || countstr == "" ? 0 : Convert.ToInt32(countstr.Split('/')[0]);
                string tx = string.Format("{0}/{1} {2} {3}", count + 1 > tabcount ? tabcount : count + 1, tabcount, DateTime.Now.ToString(), indexshot);
                Console.WriteLine(tx);
                if (!Directory.Exists(Path.GetDirectoryName(realPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(realPath));
                }
                fs = File.Open(realPath, FileMode.Truncate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ExceptionVersionUPDWrite(string sql)
        {
            string tx = string.Format("{0}\r\n{1}\r\n", DateTime.Now.ToString(), sql);
            string realPath = versionupdpath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// 报表托管接收日志
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ExceptionReportCreateDelegateWrite(string sql)
        {
            
            string tx = string.Format("{0}\r\n{1}\r\n", DateTime.Now.ToString(), sql);
            string realPath = reportcreatedelegaterecpath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// 报表托管接收日志
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ExceptionReportDelegateWrite(string sql)
        {
           
            string tx = string.Format("{0}\r\n{1}\r\n", DateTime.Now.ToString(), sql);
            string realPath = reportprocessdelegaterecpath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// 版本日志
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void VersionRecordWrite(string sql)
        {
            
            string tx = string.Format("{0}\r\n", sql);
            string realPath = versionpath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        /// <summary>
        /// 数据库更新日志
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void DBUWrite(string sql)
        {
            
            string tx = string.Format("{1}  {0}\r\n", sql, DateTime.Now);
            string realPath = dbupdpath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        #region DTU
        /// <summary>
        /// DTU通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void DTURecordWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath =dTUPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        
        /// <summary>
        /// DTU日志分端口
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void DTURecordWrite(string sql,int port)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = dTUPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/"+port+".txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }





        /// <summary>
        /// DTU通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void DTUCommendRecordWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = dTUCommendPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// DTU巡检
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void DTUPortInspectionWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = dTUInspectionPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        #endregion

        #region 广铁沉降数据接收日志
        /// <summary>
        /// DTU通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void gtsettlemntdataRecordWrite(string sql)
        {
            string tx = string.Format("{1} {0}\r\n\r\n", sql,DateTime.Now);
            string realPath = gtsettlementPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        
        #endregion

        #region NGN
        /// <summary>
        /// NGN通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNRecordWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN日志分端口
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNRecordWrite(string sql, int port)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/" + port + ".txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNCommendRecordWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnCommendPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN巡检
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNPortInspectionWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnInspectionPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        #endregion

        #region NGN雨量
        /// <summary>
        /// NGN雨量通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNHXYLRecordWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnhxylPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN雨量日志分端口
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNHXYLRecordWrite(string sql, int port)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnhxylPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/" + port + ".txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN雨量通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNHXYLCommendRecordWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnhxylCommendPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN雨量巡检
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNHXYLPortInspectionWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnhxylInspectionPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        #endregion

        #region NGN固定测斜
        /// <summary>
        /// NGN通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNFixedInclinometerRecordWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnFixedInclinometerPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN日志分端口
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNFixedInclinometerRecordWrite(string sql, int port)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnFixedInclinometerPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/" + port + ".txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                string a = "";
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN通信
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNFixedInclinometerCommendRecordWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnFixedInclinometerCommendPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// NGN巡检
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void NGNFixedInclinometerPortInspectionWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = ngnFixedInclinometerInspectionPath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        #endregion

        /// <summary>
        /// 自动报表日志
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void AutoReportWrite(string sql)
        {
            string tx = string.Format("{0}\r\n", sql);
            string realPath = reportupdpath;
            FileStream fs = null;
            try
            {
                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[100];

                byte[] bts = System.Text.Encoding.UTF8.GetBytes(tx);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bts, 0, bts.Length);
                fs.Close();
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        /// <summary>
        /// 字符列表写入文件
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ExceptionWriteTemp(List<string> ls)
        {

            StreamWriter sw = null;
            string realPath = alarmPath;
            FileStream fs = null;
            try
            {

                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/temp.txt", FileMode.OpenOrCreate, FileAccess.Write);
                fs.Seek(0, SeekOrigin.End);
                sw = new StreamWriter(fs);

                foreach (string lineTxt in ls)
                {
                    sw.WriteLine(lineTxt);
                }
                sw.Close();
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                if (sw != null)
                    sw.Close();

                if (fs != null)
                    fs.Close();
            }

        }
        /// <summary>
        /// 字符列表写入临时文件
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ExceptionWrite(List<string> ls)
        {

            var lsNotEmpty = ls.Where(s => (s!="")).ToList(); //筛选


            StreamWriter sw = null;
            string realPath = alarmPath;
            FileStream fs = null;
            try
            {

                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                fs.Seek(0, SeekOrigin.End);
                sw = new StreamWriter(fs);

                foreach (string lineTxt in lsNotEmpty)
                {
                    sw.WriteLine(lineTxt);
                }
                sw.Close();
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                if (sw != null)
                    sw.Close();

                if (fs != null)
                    fs.Close();
            }

        }
        /// <summary>
        /// 字符列表写入临时文件
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="sql"></param>
        public static void ExceptionWriteCheck(List<string> ls)
        {

            var lsNotEmpty = ls.Where(s => (s != "")).ToList(); //筛选


            StreamWriter sw = null;
            string realPath = checkPath;
            FileStream fs = null;
            try
            {

                if (!Directory.Exists(realPath))
                {
                    Directory.CreateDirectory(realPath);
                }
                fs = File.Open(realPath + "/log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                fs.Seek(0, SeekOrigin.End);
                sw = new StreamWriter(fs);

                foreach (string lineTxt in lsNotEmpty)
                {
                    sw.WriteLine(lineTxt);
                }
                sw.Close();
            }
            catch (Exception ex)
            {

                //throw (ex);
            }
            finally
            {
                if (sw != null)
                    sw.Close();

                if (fs != null)
                    fs.Close();
            }

        }

        public static string IsReportRecFinished(string settingpath)
        {
            string path = Tool.FileHelper.ProcessSettingString(reportrecpathsetting);

            if (File.Exists(path + "\\" + settingpath))
            {
                return path + "\\" + settingpath;
            }
            return null;

        }







    }
}