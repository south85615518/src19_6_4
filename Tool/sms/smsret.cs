﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.sms
{
    public class smsret
    {
        public static string retstrdescode(string code)
        {
            switch (code)
            {
                case "-1": return "-1#没有该用户账户";
                case "-2": return "-1#接口密钥不正确";
                case "-21": return "-1#MD5接口密钥加密不正确";
                case "-3": return "-1#短信数量不足";
                case "-11": return "-1#该用户被禁用";
                case "-14": return "-1#短信内容出现非法字符";
                case "-4": return "-1#手机号格式不正确";
                case "-41": return "-1#手机号码为空";
                case "-42": return "-1#短信内容为空";
                case "-51": return "-1#短信签名格式不正确";
                case "-6": return "-1#IP限制";
                default: return string.Format("{0}#成功发送{0}条短信",1,code);
            }
            
        }
    }
}
