﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.sms
{

    public class smsconfig
    {
        //是否启用此方式
        public bool on { get; set; }
        //短信平台URL
        public string url { get; set; }
        //目标电话号码
        public string tel { get; set; }
        //短信内容
        public string smstext { get; set; }
        //


    }
    //
    public class smsConfig
    {
        public enum Mode
        {
            com =0,
            web =1
        }
        //
        Mode sendMode = Mode.com;
        public Mode SendMode
        {
            get { return sendMode; }
            set { sendMode = value; }
        }
        //
        string com = "COM1";
        public string Com
        {
            get { return com; }
            set { com = value; }
        }
        //
        string url = "http://xxxxxx/xxxxx/tel={0}&msg={1}";
        public string Url
        {
            get { return url; }
            set { url = value; }
        }

    }
}
