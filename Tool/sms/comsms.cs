﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace Tool.sms
{
    public class comsms:Isendsms
    {
        
        public SerialPort sender;
        public string mssg;
        public string pducommand = "AT#AT+CMGF=0#AT+CSCS=\"GSM\"#AT+CMGW=19#{0}";
        public string command = "AT#AT+CMGF=1#AT+CSCS=\"GSM\"#AT+CMGS=\"{0}\"#{1}";
        public comsms(string com)
        {

                sender = new SerialPort(com);
                sender.BaudRate = 115200;
                sender.Parity = Parity.None;
                sender.DataBits = 8;
                sender.StopBits = StopBits.One;
        }

        public bool smssend(string tel, string msg,out string mssg)
        {
            mssg = "";
            try
            {
                command = string.Format(command,tel,msg);
                List<string> cls = command.Split('#').ToList();

                foreach (string cmd in cls)
                {
                    Console.Write("现在发送:{0}\n", cmd);
                    if (cls.IndexOf(cmd) == cls.Count - 1)
                    {
                        TestComWrite(string.Format("{0}", cmd));
                        Thread.Sleep(5000);
                        TestComWrite("0X1A");
                        Console.Write("{0}返回消息{1}\n", sender.PortName, TestComRead());
                        string result = TestComRead();
                        if (result.IndexOf("OK") != -1)
                            mssg = string.Format("成功向{0}发送1条短信",tel);
                        else
                            mssg = string.Format("向{0}发送1条短信失败", tel);
                        Console.WriteLine(mssg);
                        Console.ReadLine();
                    }
                    else
                    {
                        TestComWrite(string.Format("{0}\r\n", cmd));
                        Thread.Sleep(3000);
                        
                        Console.Write("{0}返回消息{1}\n", sender.PortName, TestComRead());
                    }
                }


                sender.Close();
                return true;
            }
            catch { return false; }
        }

        public bool pdusmssend(string tel,string msg, out string mssg)
        {
            mssg = "";
            try
            {
                pducommand = string.Format(pducommand, msg);
                List<string> cls = pducommand.Split('#').ToList();

                foreach (string cmd in cls)
                {
                    Console.Write("现在发送:{0}\n", cmd);
                    if (cls.IndexOf(cmd) == cls.Count - 1)
                    {
                        TestComWrite(string.Format("{0}", cmd));
                        
                        TestComWrite("0X1A");
                        Thread.Sleep(5000);
                        string result = TestComRead();
                        Console.Write("{0}返回消息{1}\n", sender.PortName, result);
                        //string result = TestComRead();
                        if (result.IndexOf("OK") != -1)
                            mssg = string.Format("成功向{0}发送1条短信", tel);
                        else
                            mssg = string.Format("向{0}发送1条短信失败", tel);
                        Console.WriteLine(mssg);
                        Console.ReadLine();
                    }
                    else
                    {
                        TestComWrite(string.Format("{0}\r\n", cmd));
                        Thread.Sleep(3000);

                        Console.Write("{0}返回消息{1}\n", sender.PortName, TestComRead());
                    }
                }


                sender.Close();
                return true;
            }
            catch { return false; }
        }

        public bool TestComWrite(string sendsms)
        {
            byte[] byteArr = null;
            if (sendsms.IndexOf("0X1A") != -1)
            {
                byteArr = new byte[] { 0X1A };
                Thread.Sleep(2000);
            }
            else
            {
                byteArr = Encoding.UTF8.GetBytes( sendsms);
            }

            try
            {
                if (!sender.IsOpen) sender.Open();
                sender.Write(byteArr, 0, byteArr.Length);
                return true;
            }
            catch (Exception ex)
            {
                mssg = string.Format("从com口写入出错，出错原因：" + ex.Message);
                return false;
            }

        }

        public bool ProcessComBinaryWrite(string sendsms)
        {
            byte[] byteArr = null;
            if (sendsms.IndexOf("0X1A") != -1)
            {
                byteArr = new byte[] { 0X1A };
                Thread.Sleep(2000);
            }
            else
            {
                byteArr = Encoding.UTF8.GetBytes(sendsms);
            }

            try
            {
                if (!sender.IsOpen) sender.Open();
                sender.Write(byteArr, 0, byteArr.Length);
                return true;
            }
            catch (Exception ex)
            {
                mssg = string.Format("从com口写入出错，出错原因：" + ex.Message);
                return false;
            }

        }
        public string TestComRead()
        {
            string recsms = "";
            byte[] byteArr = new byte[100];
            try
            {
                if (!sender.IsOpen) sender.Open();
                sender.Read(byteArr, 0, byteArr.Length);
                recsms = Encoding.UTF8.GetString(byteArr);
                //PDUEncoding
                return recsms;
            }
            catch (Exception ex)
            {
                mssg = string.Format("从com口读取出错，出错原因：" + ex.Message);
                return "";
            }
        }

        /// <summary>
        /// UCS2编码
        /// </summary>
        /// <param name="src"> UTF-16BE编码的源串</param>
        /// <returns>编码后的UCS2串 </returns>
        public static string EncodeUCS2(string src)
        {
            StringBuilder builer = new StringBuilder();
            builer.Append("000800");
            byte[] tmpSmsText = Encoding.Unicode.GetBytes(src);
            builer.Append(tmpSmsText.Length.ToString("X2")); //正文内容长度
            for (int i = 0; i < tmpSmsText.Length; i += 2) //高低字节对调 
            {
                builer.Append(tmpSmsText[i + 1].ToString("X2"));//("X2")转为16进制
                builer.Append(tmpSmsText[i].ToString("X2"));
            }
            builer = builer.Remove(0, 8);

            return builer.ToString();
        }   



    }
}
