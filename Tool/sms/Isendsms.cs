﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.sms
{
    public interface Isendsms
    {
        bool smssend(string tel, string msg,out string mssg);
        bool pdusmssend(string tel, string msg, out string mssg);
    }
}
