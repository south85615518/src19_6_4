﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Threading;

namespace Tool.sms
{
    public class websms : Isendsms
    {


        string url;
        //
        public websms(string urlPartten)
        {
            url = urlPartten;
        }
        //
        public bool smssend(string tel, string msg,out string mssg)
        {
            var turl = string.Format(url, tel, msg);
            //msg = msg.Replace('\n',';');
            int length = msg.Length;
            msg = length > 400 ? msg.Substring(0, 399) : msg;
            if (turl == null || turl.Trim().ToString() == "")
            {
                mssg = "短信发送失败";
                return false;
            }
            string targeturl = turl.Trim().ToString();
            try
            {
                HttpWebRequest hr = (HttpWebRequest)WebRequest.Create(targeturl);
                hr.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)";
                hr.Method = "GET";
                hr.Timeout = 30 * 60 * 1000;
                WebResponse hs = hr.GetResponse();
                Stream sr = hs.GetResponseStream();
                StreamReader ser = new StreamReader(sr, Encoding.Default);
                mssg =  ser.ReadToEnd();
                mssg = messresultdescode(Convert.ToInt32(mssg));
                Thread.Sleep(1000);
                return true;
            }
            catch (Exception ex)
            {
                mssg = ex.Message;
                return false;
            }

        }
        public string messresultdescode(int result )
        {
            if (result > 0) return "短信发送成功";
            switch (result)
            {
                case -1:
                    return "没有该用户账户";
                case -2: 
                    return "接口密钥不正确 [查看密钥]不是账户登陆密码";
                case -21:
                    return "MD5接口密钥加密不正确";
                case -3:
                    return "短信数量不足";
                case -11:
                    return "该用户被禁用";
                case -14:
                    return "短信内容出现非法字符";
                case -4:
                    return "手机号格式不正确";
                case -41:
                    return "手机号码为空";
                case -42:
                    return "短信内容为空";
                case -51: 
                    return "短信签名格式不正确接口签名格式为：【签名内容】";
                case -52: 
                    return "短信签名太长建议签名10个字符以内";
                case -6:
                    return "IP限制";
                default:
                    return "";
                
            }
        }




        public bool pdusmssend(string tel, string msg, out string mssg)
        {
            throw new NotImplementedException();
        }
    }

}
