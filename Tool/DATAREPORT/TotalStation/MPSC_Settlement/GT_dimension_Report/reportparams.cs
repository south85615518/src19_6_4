﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.DATAREPORT.TotalStation.MPSC.GT_dimension_Report
{
    public class reportparams
    {
        public string xmaddress { get; set; }
        public string n_firstalrmacdisp { get; set; }
        public string e_firstalrmacdisp { get; set; }
        public string z_firstalrmacdisp { get; set; }
        public string n_secalrmacdisp { get; set; }
        public string e_secalrmacdisp { get; set; }
        public string z_secalrmacdisp { get; set; }
        public reportparams()
        {
            
        }

    }
}
