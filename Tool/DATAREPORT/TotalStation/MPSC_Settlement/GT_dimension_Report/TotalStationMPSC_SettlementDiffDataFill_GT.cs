﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.TotalStation
{
    public partial class TotalStationDataFill
    {

        public void DataFillMPSC_GT_dimension_SettlementDiff_Report_WithoutSettlement(string xmname, string tabHead, List<string> pointnamelist, List<DataTable> datatablelist, string reportname,DataTable origldatatable, string dimensionName, string dimensionVariableName, Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams reportparams,string indexshot)
        {
            
            string[] tabName = tabHead.Split(',');
            int i = 0;
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 1, 16);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 2, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 3, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 7, 8);
            //for (i = 0; i < pointnamelist.Count; i++)
            //{
            //    ExceptionLog.ReportLineWrite(string.Format("开始打印{0}第{2}个周期周期{1}数据...", xmname, lrf[i].dt.Rows[0].ItemArray[1].ToString(), (i+1) + "/" + lrf.Count), reportname);
            //    ExcelHelper.TotalStationReportTemplateCreateMPSC_GT_DAYReport_WithoutSettlement(xmname, tabName, lrf[i].dt, lrf[i].index);
            //}
            ExcelHelper.TotalStationReportTemplateCreateMPSC_GT_dimension_settlementdiff_Report_WithoutSettlement(xmname, pointnamelist, datatablelist, origldatatable, dimensionName, dimensionVariableName, reportparams, reportname, indexshot);
            
        }
    }
}
