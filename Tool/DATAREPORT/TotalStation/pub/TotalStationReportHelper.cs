﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;
using Tool.DATAREPORT.Pub;
using Tool.DATAREPORT.TotalStation;


namespace Tool
{
    public partial class TotalStationReportHelper
    {

        public  XSSFWorkbook xssfworkbook;
        public  HSSFWorkbook hssfworkbook;
        public  int vsion = 0;//版本标志 
        public  void WriteToFile(string ldpath)
        {
            if (!Directory.Exists(Path.GetDirectoryName(ldpath))) Directory.CreateDirectory(Path.GetDirectoryName(ldpath));
            FileStream file = new FileStream(ldpath, FileMode.Create, FileAccess.ReadWrite);
            if (hssfworkbook != null)
            {
                hssfworkbook.Write(file);
            }
            else
            {
                xssfworkbook.Write(file);
            }
            file.Close();

        }
        public  void HInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added. 
            Exception e = null;
            FileStream file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
            try
            {
                hssfworkbook = new HSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                hssfworkbook.DocumentSummaryInformation = dsi;
                vsion = 0;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                hssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (e != null)
                    throw e;
            }
        }
        public  void XInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            Exception e = null;
            FileStream file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
            try
            {
                xssfworkbook = new XSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                //xssfworkbook .DocumentSummaryInformation = dsi;
                vsion = 1;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                //xssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (e != null)
                    throw e;
            }
        }
        public ExcelHelperHandle ExcelHelper = new ExcelHelperHandle();
        public  void PublicCompatibleInitializeWorkbook(string tppath)
        {
            try
            {
                HInitializeWorkbook(tppath);
            }
            catch (Exception ex)
            {
                XInitializeWorkbook(tppath);
            }
        }
        public void ColumnFormat()
        {
            throw new NotImplementedException();
        }
    }
}