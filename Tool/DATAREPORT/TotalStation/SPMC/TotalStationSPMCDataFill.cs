﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.TotalStation
{
    public partial class TotalStationDataFill
    {
        
        public  void DataFillSPMC(string xmname, string tabHead, List<ReportFillEnviroment> lrf,string reportname)
        {
            
            string[] tabName = tabHead.Split(',');
            int i = 0;
            ExcelHelper.DateFormat(ExcelHelper.xSheet,11, "yy/mm/dd hh:MM");
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 1, 6);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 2, 10);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 3, 10);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 11);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 7, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 8, 11);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 9, 11);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 10, 11);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 11, 15);
            for (i = 0; i < lrf.Count; i++)
            {
                ExceptionLog.ReportLineWrite(string.Format("开始打印{0}第{2}个点名{1}数据...", xmname,lrf[i].dt.Rows[0].ItemArray[0].ToString(),(i+1)+"/"+lrf.Count), reportname);
                ExcelHelper.TotalStationReportTemplateCreateSPMC(xmname, tabName, lrf[i].dt, lrf[i].index);
            }
            
        }
    }
}
