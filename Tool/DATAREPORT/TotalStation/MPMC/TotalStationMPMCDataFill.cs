﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.TotalStation
{
    public partial class TotalStationDataFill
    {
     
        public    void DataFillMPMC(string xmname, string tabHead, List<ReportFillEnviroment> lrf)
        {



            string[] tabName = tabHead.Split(',');
            int i = 0;
            //ExcelHelper.DateFormat(ExcelHelper.xSheet, 1, "yy/mm/dd hh:MM");

            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 1, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 2, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 3, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 7, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 8, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 9, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 10, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 11, 7);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 12, 15);
            ExcelHelper.DateFormat(ExcelHelper.xSheet, 12, "yy/mm/dd hh:MM");
            for (i = 0; i < lrf.Count; i++)
            {
                ExcelHelper.TotalStationOrglDataReportTemplateCreateMPMC(xmname, tabName, lrf[i].dt, lrf[i].index);
            }

           
            
        }
    }
}
