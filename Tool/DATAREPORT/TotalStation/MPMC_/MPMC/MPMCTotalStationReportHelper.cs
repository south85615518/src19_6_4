﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;
using Tool.DATAREPORT.Pub;
using Tool.DATAREPORT.TotalStation;


namespace Tool
{
    public partial class TotalStationReportHelper 
    {
        
        public  void ReportDataFillMPMC(string xmname,  DataTable dt, ISheet sheet, string sheetName, int len, int datastartlen)
        {


            int pagecnt = dt.Rows.Count % 31 == 0 ? dt.Rows.Count / 31 : dt.Rows.Count / 31 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            IRow datarow = null;
            ChartHelper chartHelper = new ChartHelper();
            int l = dt.Rows.Count;
            int cnt = 0;

            int i = 0;
            for (; i < l; i++)
            {


                datarow = sheet.CreateRow(datastartlen);
                datastartlen++;
               
                if(i == 0)
                {
                    datarow.CreateCell(0).SetCellValue("时间");
                    datarow.CreateCell(1).SetCellValue("ΔX");
                    datarow.CreateCell(2).SetCellValue("ΔY");
                    datarow.CreateCell(3).SetCellValue("ΔZ");
                    datarow.CreateCell(4).SetCellValue("ΣX");
                    datarow.CreateCell(5).SetCellValue("ΣY");
                    datarow.CreateCell(6).SetCellValue("ΣZ");
                    datarow.CreateCell(7).SetCellValue(sheetName);
                    //datastartlen++;
                    datarow = sheet.CreateRow(datastartlen);
                    datastartlen++;
                    datarow.CreateCell(0).SetCellValue(dt.Rows[i].ItemArray[10].ToString());
                    datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[4]));
                    datarow.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[5]));
                    datarow.CreateCell(3).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[6]));
                    datarow.CreateCell(4).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[7]));
                    datarow.CreateCell(5).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[8]));
                    datarow.CreateCell(6).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[9]));

                }
                else
                {
                    datarow.CreateCell(0).SetCellValue(dt.Rows[i].ItemArray[10].ToString());
                    datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[4]));
                    datarow.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[5]));
                    datarow.CreateCell(3).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[6]));
                    datarow.CreateCell(4).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[7]));
                    datarow.CreateCell(5).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[8]));
                    datarow.CreateCell(6).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[9]));
                }


            }

        }
        public void ChartDataFillMPMC(string xmname, string tabHead, DataTable originaldt, out List<ChartCreateEnviroment> lce, out List<ReportFillEnviroment> lrf)
        {
            int len = 0;
            List<DataTable> ldt = new List<DataTable>();
            ldt.Add(originaldt);
            //int t = 0;
            string sheetName = "";
            lce = new List<ChartCreateEnviroment>();
            lrf = new List<ReportFillEnviroment>();
            string[] tabName = tabHead.Split(',');
            int reportNo = new Random().Next(9999);
            int pageindexlen = 0;
            int datastartlen = 0;
            ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("Sheet1") : xssfworkbook.GetSheet("Sheet1");
            ISheet sheet = vsion == 0 ? hssfworkbook.GetSheet("Sheet") : xssfworkbook.GetSheet("Sheet");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue(1);
            //foreach (DataTable dt in ldt)
            //{
                
            //    string tmp = "点名:" + dt.Rows[0].ItemArray[0].ToString();
            //    DataView dv = new DataView(dt);
            //    sheetName = dt.Rows[0].ItemArray[0].ToString();

            //    sheet1.PrintSetup.Landscape = true;
            //    sheet1.Header.Left = "南方测绘";
            //    sheet1.Header.Right = DateTime.Now.ToString();
            //    sheet1.Footer.Center = "第&p页";
            //    sheet1.PrintSetup.PaperSize = 9;
            //    sheet1.FitToPage = false;
            //    sheet1.PrintSetup.Scale = 100;
            //    sheet1.HorizontallyCenter = true;
            //    int k = 0;
            //    //IRow row = sheet1.CreateRow(len);
            //    pageindexlen = len + 7;
            //    lrf.Add(new ReportFillEnviroment(dt, len));
                
            //    //sheet.CreateRow(0).CreateCell(0).SetCellValue(1);
            //    //ReportDataFillMPMC(xmname, dt, sheet, sheetName, len, datastartlen);
            //    //len += (dt.Rows.Count % 31 == 0 ? dt.Rows.Count / 31 : dt.Rows.Count / 31 + 1) * 35;
            //    //datastartlen += dt.Rows.Count + 1;
            //    //int pageIndex = len % 35 == 0 ? len / 35 : len / 35 + 1;
            //    //len = len % 35 == 0 ? (len / 35) * 35 + 70 : (len / 35) * 35 + 105;
            //    //lce.Add(new ChartCreateEnviroment(datastartlen + 1 - dt.Rows.Count, datastartlen, sheetName, pageIndex));
            //}
        }

        public void ChartDataBindMPMC(List<ChartCreateEnviroment> lce)
        {
           // TotalStationChartBind.ChartBindMPMC(lce);
        }

        public void ReportDataFillMPMC(string xmname, string tabHead, List<ReportFillEnviroment> lrf)
        {
            TotalStationDataFill totalStationDataFill = new TotalStationDataFill { ExcelHelper = ExcelHelper };
            totalStationDataFill.DataFillMPMC(xmname, tabHead, lrf);
        }

        public void gtorgldataMainMPMC(string xmname, string tabHead, DataTable originaldt,string xlpath)
        {
            List<ChartCreateEnviroment> lce = null;
            List<ReportFillEnviroment> lrf = null;
            ChartDataFillMPMC( xmname,  tabHead,  originaldt,out lce, out  lrf);
            WriteToFile(xlpath);
            ExcelHelper.workbookpath = xlpath;
            ExcelHelper.ExcelInit();
            ExcelHelper.SheetInit(1,8);
            //ChartDataBindMPMC(lce);
            ReportDataFillMPMC(xmname,tabHead,lrf);
            ExcelHelper.save();
        }

       
    }
}