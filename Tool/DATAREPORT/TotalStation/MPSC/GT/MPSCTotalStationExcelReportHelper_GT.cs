﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {
        
        public    void TotalStationReportTemplateCreateMPSC_GT(string xmname, string[] tabName, DataTable dt,int len,string reportname)
        {

            List<string> points = Tool.DataTableHelper.ProcessDataTableFilter(dt,"point_name");
            List<string> pointssort = null;
            if (Tool.com.StringHelper.SameExSort(points, out pointssort))
            {
                try{
                dt.Columns.Add("sort");
                DataView dvsort = new DataView(dt);
                foreach(DataRowView drv in dvsort)
                {
                    drv["sort"] = pointssort.IndexOf(drv["point_name"].ToString());
                }
                dvsort.Sort = " sort,time asc ";
                dt = dvsort.ToTable();
                dt.Columns.Remove("sort");
                }
                catch(Exception ex)
                {
                    ExceptionLog.ExceptionWrite("对"+reportname+"数据表点名排序出错");
                }
                
            }

            string pointname = dt.Rows[0].ItemArray[1].ToString();
            int pagecnt = dt.Rows.Count % 32 == 0 ? dt.Rows.Count / 32 : dt.Rows.Count / 32 + 1;
            int tmplen = len;
            string reportNo =  Tool.com.StringHelper.GetNumFromString(reportname);
            int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            int rangerowstart = len;

            tabName = new string[7] { "桥墩测点", "纵向位移(北向)", "横向位移(东向)", "沉降", "纵向位移(北向)", "横向位移(东向)", "沉降" };
            DateTime time = Convert.ToDateTime(dt.Rows[0].ItemArray[11]);
            int diffsettlememntcount = 0;
            List<double> difflist = new List<double>();
            for (cnt = 0; cnt < pagecnt; cnt++)
            {
                rangerowstart = len;
                //添加标题
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                this.ExcelMergedRegion(len, 1, len, 9);
                
                this.ExcelWrite(len, 1,xmname);
                len++;
                this.ExcelMergedRegion(len, 1, len, 9);
                this.ExcelWrite(len, 1, "既有桥墩位移监测日报表");
                len++;
                this.ExcelMergedRegion(len, 1, len, 9);
                this.ExcelWrite(len, 1, string.Format("第{0}次",pointname));
                len++;
                this.ExcelWrite(len, 1, "工程名称:");
                this.ExcelMergedRegion(len, 2, len, 4);
                this.ExcelWrite(len, 2, xmname);
                this.ExcelWrite(len, 5, "天气:");
                this.ExcelWrite(len, 6, "");
                len++;
                this.ExcelWrite(len, 1, "观测方法:");
                this.ExcelWrite(len, 2, "自动全站仪测量");
                this.ExcelWrite(len, 5, "观测时间");
                this.ExcelWrite(len, 6, string.Format("{0}/{1}/{2}",time.Year,time.Month,time.Day) );
                len++;
                int k = 0;
                //}
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                //this.ExcelMergedRegion(len, 1, len, dt.Columns.Count - 1);
                ////this.BorderSet(len, 1, len, 1);
                //this.ExcelWrite(len, 1, pointname, true);
                //len++;
                //row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                //row.GetCell(0).SetCellValue(pointname
                //    );
                //len++;

                //k = 0;
                //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                //CreateRow(len+1, sheet1, tabName.Length);//创建复表头行

                //sheet1.AddMergedRegion(new CellRangeAddress(len , len +1, 0, 0));
                //row.CreateCell(0).SetCellValue("点名");
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("高度（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("温度（°C）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(10).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                //CellFontSet(row.GetCell(10), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                ////return;
                ////k = 0;
                //len++;
                //ICellStyle styleCell = GetICellStyle();
                //ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

                //CreateRow(len, sheet1, tabName.Length);//创建复表头行
                // row = CreateRow(len , sheet1, tabName.Length);//创建复表头行
                //this.BorderSet(len, 1, len + 1, 1);
                this.ExcelMergedRegion(len, 1, len + 1, 1);
                this.ExcelWrite(len, 1, "桥墩测点");

                //this.BorderSet(len, 5, len, 7);
                this.ExcelMergedRegion(len, 2, len, 4);
                this.ExcelWrite(len, 2, "本次变化量(mm)");

                //this.BorderSet(len, 8, len, 10);
                this.ExcelMergedRegion(len, 5, len, 7);
                this.ExcelWrite(len, 5, "累计变化量(mm)");

                this.ExcelMergedRegion(len, 8, len + 1, 8);
                this.ExcelWrite(len, 8, "测点");

                this.ExcelMergedRegion(len, 9, len + 1, 9);
                this.ExcelWrite(len, 9, "沉降差");


                this.ExcelMergedRegion(len, 12, len, 14);
                this.ExcelWrite(len, 12, "本次变化范围（mm）");

                this.ExcelWrite(len + 1, 12, "纵向位移");
                this.ExcelWrite(len + 1, 13, "横向位移");
                this.ExcelWrite(len + 1, 14, "沉降");
                this.ExcelWrite(len + 2, 11, "min");
                this.ExcelWrite(len + 2, 12, SortValueMin(dt,"This_dN") );
                this.ExcelWrite(len + 2, 13, SortValueMin(dt, "This_dE"));
                this.ExcelWrite(len + 2, 14, SortValueMin(dt, "This_dZ"));
                this.ExcelWrite(len + 2, 15, SortValueMin(dt, "Ac_dN"));
                this.ExcelWrite(len + 2, 16, SortValueMin(dt, "Ac_dE"));
                this.ExcelWrite(len + 2, 17, SortValueMin(dt, "Ac_dZ"));
                this.ExcelMergedRegion(len, 15, len, 17);
                this.ExcelWrite(len, 15, "累计变化范围（mm）");
                
                this.ExcelWrite(len + 1, 15, "纵向位移");
                this.ExcelWrite(len + 1, 16, "横向位移");
                this.ExcelWrite(len + 1, 17, "沉降");
                this.ExcelWrite(len + 3, 11, "max");
                this.ExcelWrite(len + 3, 12, SortValueMax(dt, "This_dN"));
                this.ExcelWrite(len + 3, 13, SortValueMax(dt, "This_dE"));
                this.ExcelWrite(len + 3, 14, SortValueMax(dt, "This_dZ"));
                this.ExcelWrite(len + 3, 15, SortValueMax(dt, "Ac_dN"));
                this.ExcelWrite(len + 3, 16, SortValueMax(dt, "Ac_dE"));
                this.ExcelWrite(len + 3, 17, SortValueMax(dt, "Ac_dZ"));
                this.ExcelMergedRegion(len, 15, len, 17);
                this.ExcelWrite(len, 18, "沉降差");

               


                //本次变化范围（mm）
                ////sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                ////row.CreateCell(0).SetCellValue("时间");
                ////CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                ////CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(0).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("观测值（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("本次变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(7).SetCellValue("累计变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 7, 9));
                //CellFontSet(row.GetCell(7), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);


                //return;
                k = 0;
                len+=1;

                //row = CreateRow(len, sheet1, tabName.Length);//创建表头行


                k = 0;

                foreach (string name in tabName)
                {
                    //if (k == 0) { k++; continue; }

                    if (k == dt.Columns.Count - 1)
                    {

                    }
                    //CellBorderSet(row.GetCell(k), styleBorder, true);
                    else
                    {
                        this.ExcelWrite(len, k + 1, name);
                        //row.CreateCell(k).SetCellValue(name);
                        //CellBorderSolid(row.GetCell(k), styleCell, true);
                    }
                    k++;
                }

                len++;

                int i = 0;
                try
                {
                    for (; i < 32; i++)
                    {

                        if (31 * cnt + i-5 > dt.Rows.Count - 1)
                        {
                            //RowFormat(len, 10);
                            //CreateRow(len, sheet1, 10);
                            len++;
                            continue;
                        }
                        int j = 0, t = 1;
                        for (j = 0; j < 8; j++)
                        {

                            if (j == 1) continue;
                            if (t==1||t==11)
                            {

                                try
                                {
                                    ExcelWrite(len, t, dt.Rows[31 * cnt + i].ItemArray[j]);
                                }
                                catch (Exception)
                                {
                                    
                                    string a = "";
                                }

                            }
                            else
                            {
                                try
                                {
                                    ExcelWrite(len, t, Convert.ToDouble(dt.Rows[31 * cnt + i].ItemArray[j + 3]));
                                }
                                catch (Exception ex)
                                {
                                    string a = "";
                                }

                            }

                            t++;

                        }
                        if (i == 24) { 
                            string a = ""; 
                        }
                        double diff = DiffSettlement(dt, dt.Rows[31 * cnt + i].ItemArray[0].ToString(), dt.Rows[31 * cnt + i].ItemArray[dt.Columns.Count-1].ToString());
                        if (diff != -9.99)
                        {
                            ExcelWrite(rangerowstart+7+diffsettlememntcount, 8, string.Format("{0}：{1}", dt.Rows[31 * cnt + i].ItemArray[0].ToString(), dt.Rows[31 * cnt + i].ItemArray[dt.Columns.Count - 1].ToString()));
                            ExcelWrite(rangerowstart + 7 + diffsettlememntcount, 9, diff);
                            difflist.Add(diff);
                            diffsettlememntcount++;
                        }
                        len++;
                       
                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                    Console.WriteLine(ex.StackTrace);
                }
                if (difflist != null && difflist.Count > 0)
                {
                    difflist.Sort();
                    ExcelWrite(rangerowstart + 7, 18, difflist[0]);
                    ExcelWrite(rangerowstart + 8, 18, difflist[difflist.Count - 1]);
                }
                len++;
                this.ExcelMergedRegion(len, 1, len + 2, 7);

                this.ExcelWrite(len, 1, string.Format(@"监测小结：本次变化量，桥墩顶纵向水平位移在{0}~{1}mm内，横向水平位移在{2}~{3}mm内，桥墩沉降在{4}~{5}mm内；累计变化量，桥墩顶纵向水平位移在{6}~{7}mm内，横向水平位移在{8}~{9}mm内，桥墩沉降在{10}~{11}mm内，沉降差在{12}~{13}mm内，累计位移及沉降差均未达到报警值。", xSheet.Cells[rangerowstart + 7, 12].Value, xSheet.Cells[rangerowstart + 8, 12].Value, xSheet.Cells[rangerowstart + 7, 13].Value, xSheet.Cells[rangerowstart + 8, 13].Value, xSheet.Cells[rangerowstart + 7, 14].Value, xSheet.Cells[rangerowstart + 8, 14].Value, xSheet.Cells[rangerowstart + 7, 15].Value, xSheet.Cells[rangerowstart + 8, 15].Value, xSheet.Cells[rangerowstart + 7, 16].Value, xSheet.Cells[rangerowstart + 8, 16].Value, xSheet.Cells[rangerowstart + 7, 17].Value, xSheet.Cells[rangerowstart + 8, 17].Value, xSheet.Cells[rangerowstart + 7, 18].Value, xSheet.Cells[rangerowstart + 8, 18].Value));
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 3;
                this.ExcelMergedRegion(len, 1, len + 1, 7);
                this.ExcelWrite(len, 1, @"注：纵向位移以“+”表示往既有线路大里程方向偏移，“-”往小里程方向偏移；横向位移以“+”表示面向既有线路大里程方向往右侧偏移，“-”往左侧偏移；沉降以“+”表示抬升，“-”表示下沉。");
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 2;
                this.ExcelWrite(len, 1, "测量:");
                this.ExcelWrite(len, 4, "校核:");
                len++;
                this.ExcelMergedRegion(len, 5, len, 7);
                this.ExcelWrite(len, 5, "广州铁路科技开发有限公司");
                len++;
                this.ExcelMergedRegion(len, 6, len, 7);
                this.ExcelWrite(len, 6, string.Format("{0}/{1}/{2}", time.Year, time.Month, time.Day));
                len++;
                this.BorderSet(rangerowstart + 5, 1, rangerowstart + 6 + diffsettlememntcount, 9);
                this.BorderSet(rangerowstart + 5, 1, len - 4, 7);

                diffsettlememntcount = 0;
            }
            

        }
        public double DiffSettlement(DataTable dt,string pointname,string siblingpointname)
        {
            try{
                if (siblingpointname == "") return -9.99;
            DataView dvS = new DataView(dt);
            dvS.RowFilter = "point_name='"+pointname+"'";
            DataView dvD = new DataView(dt);
            dvD.RowFilter = "point_name='" + siblingpointname + "'";
            return Convert.ToDouble(dvS[0]["Ac_dZ"]) - Convert.ToDouble(dvD[0]["Ac_dZ"]);
            }
            catch(Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("获取点{0}:{1}的沉降差出错，错误信息："+ex.Message,pointname,siblingpointname));
            }
            return -9.99 ;
        }

        public double SortValueMax(DataTable dt,string coluname)
        {
            DataView dv = new DataView(dt);
            dv.Sort = coluname + " desc ";
            return Convert.ToDouble(dv[0][coluname]);
        }
        public double SortValueMin(DataTable dt, string coluname)
        {
            DataView dv = new DataView(dt);
            dv.Sort = coluname + " asc ";
            return Convert.ToDouble(dv[0][coluname]);
        }
        //public void DeleteDiffSettlementSpaceRow(int startRow,int endRow)
        //{
        //    int i = 0; int spacecount = 0;
        //    for (i = startRow; i < endRow;i++ )
        //    {
        //        if (xSheet.Cells[i, 8].Value == "")
        //        {
        //            spacecount++;
        //            continue;
        //        }
        //        if (xSheet.Cells[i, 8].Value != "" && spacecount > 0)
        //        {
 
        //        }
        //    }
        //}
        public DataTable TimeSpanThisDataTable(DataTable dtstart, DataTable dtend)
        {

            DataView dvStart = new DataView(dtstart);
            DataView dvEnd = new DataView(dtend.Copy());
            foreach (DataRowView drv in dvEnd)
            {
                dvStart.RowFilter = " point_name ='" + drv["point_name"] + "' ";
                if (dvStart.Count == 0) continue;
                drv["ac_dn"] = dvStart[0]["ac_dn"] == "" || dvStart[0]["ac_dn"] == null ? 0 : Convert.ToDouble(drv["ac_dn"]) - Convert.ToDouble(dvStart[0]["ac_dn"]);
                drv["ac_de"] = dvStart[0]["ac_de"] == "" || dvStart[0]["ac_de"] == null ? 0 : Convert.ToDouble(drv["ac_de"]) - Convert.ToDouble(dvStart[0]["ac_de"]);
                drv["ac_dz"] = dvStart[0]["ac_dz"] == "" || dvStart[0]["ac_dz"] == null ? 0 : Convert.ToDouble(drv["ac_dz"]) - Convert.ToDouble(dvStart[0]["ac_dz"]);
            }
            dvEnd.RowFilter = " 1=1 ";
            return dvEnd.ToTable();

        }

      
    }
}
