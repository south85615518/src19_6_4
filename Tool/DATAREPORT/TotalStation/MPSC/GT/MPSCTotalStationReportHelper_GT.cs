﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;
using Tool.DATAREPORT.Pub;
using Tool.DATAREPORT.TotalStation;


namespace Tool
{
    public partial class TotalStationReportHelper
    {

      
        public  void ReportDataFillMPSC_GT(string xmname,  DataTable dt, ISheet sheet, string sheetName, int len, int datastartlen)
        {


            int pagecnt = dt.Rows.Count % 31 == 0 ? dt.Rows.Count / 31 : dt.Rows.Count / 31 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            IRow datarow = null;
            ChartHelper chartHelper = new ChartHelper();
            int l = dt.Rows.Count;
            int cnt = 0;

            int i = 0;
            for (; i < l; i++)
            {


                datarow = sheet.CreateRow(datastartlen);
                datastartlen++;
               
                if(i == 0)
                {
                    datarow.CreateCell(0).SetCellValue("点名");
                    datarow.CreateCell(1).SetCellValue("ΔX");
                    datarow.CreateCell(2).SetCellValue("ΔY");
                    datarow.CreateCell(3).SetCellValue("ΔZ");
                    datarow.CreateCell(4).SetCellValue("ΣX");
                    datarow.CreateCell(5).SetCellValue("ΣY");
                    datarow.CreateCell(6).SetCellValue("ΣZ");
                    datarow.CreateCell(7).SetCellValue(sheetName);
                    //datastartlen++;
                    datarow = sheet.CreateRow(datastartlen);
                    datastartlen++;
                    datarow.CreateCell(0).SetCellValue(dt.Rows[i].ItemArray[0].ToString());
                    datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[5]));
                    datarow.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[6]));
                    datarow.CreateCell(3).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[7]));
                    datarow.CreateCell(4).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[8]));
                    datarow.CreateCell(5).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[9]));
                    datarow.CreateCell(6).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[10]));

                }
                else
                {
                    datarow.CreateCell(0).SetCellValue(dt.Rows[i].ItemArray[0].ToString());
                    datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[5]));
                    datarow.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[6]));
                    datarow.CreateCell(3).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[7]));
                    datarow.CreateCell(4).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[8]));
                    datarow.CreateCell(5).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[9]));
                    datarow.CreateCell(6).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[10]));
                }


            }

        }
        public void ChartDataFillMPSC_GT(string xmname, string tabHead, DataTable originaldt, out List<ChartCreateEnviroment> lce, out List<ReportFillEnviroment> lrf)
        {
            int len = 0;
            List<DataTable> ldt = Tool.DataTableHelper.ProcessDataTableSplitMPSC(originaldt);
            //int t = 0;
            string sheetName = "";
            lce = new List<ChartCreateEnviroment>();
            lrf = new List<ReportFillEnviroment>();
            string[] tabName = tabHead.Split(',');
            int reportNo = new Random().Next(9999);
            int pageindexlen = 0;
            int datastartlen = 0;
            ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("Sheet") : xssfworkbook.GetSheet("Sheet");
            ISheet sheet = vsion == 0 ? hssfworkbook.GetSheet("Sheet1") : xssfworkbook.GetSheet("Sheet1");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue(1);
            foreach (DataTable dt in ldt)
            {

              
                DataView dv = new DataView(dt);
                sheetName = string.Format("第{0}期", dt.Rows[0].ItemArray[1]);

                if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
                sheet1.Header.Left = "广州铁路科技开发有限公司";
                sheet1.Header.Right = DateTime.Now.ToString();
                sheet1.Footer.Center = "第&p页";
                sheet1.PrintSetup.PaperSize = 9;
                sheet1.FitToPage = false;
                sheet1.PrintSetup.Scale = 100;
                sheet1.HorizontallyCenter = true;
                int k = 0;
                //IRow row = sheet1.CreateRow(len);
                pageindexlen = len + 7;
                lrf.Add(new ReportFillEnviroment(dt, len));
                //sheet.CreateRow(0).CreateCell(0).SetCellValue(1);
                ReportDataFillMPSC_GT(xmname, dt, sheet, sheetName, len, datastartlen);
                len += (dt.Rows.Count % 31 == 0 ? dt.Rows.Count / 31 : dt.Rows.Count / 31 + 1) * 40;
                datastartlen += dt.Rows.Count + 1;
                int pageIndex = len % 40 == 0 ? len / 40 : len / 40 + 1;
                //len = len % 38 == 0 ? (len / 38) * 38 + 70 : (len / 35) * 35 + 105;
                lce.Add(new ChartCreateEnviroment(datastartlen + 1 - dt.Rows.Count, datastartlen, sheetName, pageIndex));
            }
        }

        public void ChartDataBindMPSC_GT(List<ChartCreateEnviroment> lce)
        {
            TotalStationChartBind totalStationChartBind = new TotalStationChartBind { ExcelHelper = ExcelHelper };
            totalStationChartBind.ChartBindMPSC(lce);
        }

        public void ReportDataFillMPSC_GT(string xmname, string tabHead, List<ReportFillEnviroment> lrf, string reportname)
        {
            TotalStationDataFill totalStationDataFill = new TotalStationDataFill { ExcelHelper = ExcelHelper };
            totalStationDataFill.DataFillMPSC_GT(xmname, tabHead, lrf,reportname);
        }

        public void MainMPSC_GT(string xmname, string tabHead, DataTable originaldt, string xlpath)
        {
            List<ChartCreateEnviroment> lce = null;
            List<ReportFillEnviroment> lrf = null;
            ChartDataFillMPSC_GT( xmname,  tabHead,  originaldt,out lce, out  lrf);
            WriteToFile(xlpath);
            ExcelHelper.workbookpath = xlpath;
            ExcelHelper.ExcelInit();
            ExcelHelper.SheetInit(1,2);
            //ChartDataBindMPSC(lce);
            //string filename = Path.GetFileNameWithoutExtension(xlpath);
            ReportDataFillMPSC_GT(xmname, tabHead, lrf, Path.GetFileNameWithoutExtension(xlpath));
            ExcelHelper.save();
        }

    }
}