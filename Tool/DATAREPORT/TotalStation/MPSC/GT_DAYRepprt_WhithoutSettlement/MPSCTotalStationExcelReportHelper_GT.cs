﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {

        public void TotalStationReportTemplateCreateMPSC_GT_DAYReport_WithoutSettlement(string xmname, List<string> pointnamelist, List<DataTable> dtlist, DataTable dt, string reportname, int tabrowscount, int startlen, DataTable dttimespan, string timeunitname, Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams reportparams,int datarowscnt)
        {
            if (dtlist.Count == 0) return;
            int len = startlen;
            //string pointname = dt.Rows[0].ItemArray[1].ToString();
            //int pagecnt = dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1;
            int tmplen = len;
            string reportNo =  Tool.com.StringHelper.GetNumFromString(reportname);
            //int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            int rangerowstart = 1;
            int widthcol = 2 * 3+1;
            //tabName = new string[7] { "测点", "纵向位移(北)", "横向位移(东)", "沉降", "纵向位移(北)", "横向位移(东)", "沉降" };
            DateTime time = DateTime.Now;//Convert.ToDateTime(dt.Rows[0].ItemArray[11]);
            int diffsettlememntcount = 0;
            List<double> difflist = new List<double>();

            //int datarowcnt = reportparams.alarmmess.Count == 0 ? 37 : 36;
            fontsize = 9;

            for (cnt = 0; datarowscnt * cnt < pointnamelist.Count; cnt++)
            {
                rangerowstart = len;
                //添加标题
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                this.ExcelMergedRegion(len, 1, len, widthcol);

                this.ExcelWriteWrapText(len, 1, xmname);
                len++;
                this.ExcelMergedRegion(len, 1, len, widthcol);
                this.ExcelWriteWrapText(len, 1, string.Format("位移监测{0}报表 NO.{1}", timeunitname,reportNo));
                len++;
                this.ExcelWriteWrapText(len, 1, "工程名称:");
                this.ExcelMergedRegion(len, 2, len, (widthcol - 1) / 2+1);
                this.ExcelWriteWrapText(len, 2, xmname);
                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 2, "监测周期:");
                this.ExcelMergedRegion(len, (widthcol - 1) / 2 + 3, len, widthcol);
                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 3, string.Format("{0}~{1}", dtlist[0].Rows[0].ItemArray[1], dtlist[dtlist.Count - 1].Rows[0].ItemArray[1]));
                len++;
                this.ExcelWriteWrapText(len, 1, "观测方法:");
                this.ExcelMergedRegion(len, 2, len, (widthcol - 1) / 2);
                this.ExcelWriteWrapText(len, 2, "自动全站仪测量");

                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 1, "观测时间");
                this.ExcelMergedRegion(len, (widthcol - 1) / 2 + 2, len, widthcol);
                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 2, string.Format("{0}~{1}",  dtlist[0].Rows[0].ItemArray[dtlist[0].Columns.Count - 1], dtlist[dtlist.Count - 1].Rows[0].ItemArray[dtlist[dtlist.Count - 1].Columns.Count - 1]));
                len++;
                this.ExcelMergedRegion(len, 1, len + 1,1);
                this.ExcelWriteWrapText(len, 1, "预警值:");
                this.ExcelMergedRegion(len, 2, len+1, (widthcol - 1) / 2+2);
                this.ExcelWriteWrapText(len, 2, string.Format("累计纵向位移上/下限:{0}；累计横向位移上/下限:{1}；累计沉降上/下限:{2}", reportparams.n_firstalrmacdisp, reportparams.e_firstalrmacdisp, reportparams.z_firstalrmacdisp));
                this.ExcelMergedRegion(len + 2, 1, len + 3, 1);
                this.ExcelWriteWrapText(len + 2, 1, "报警值:");
                this.ExcelMergedRegion(len + 2, 2, len + 3, (widthcol - 1) / 2+2);
                this.ExcelWriteWrapText(len + 2, 2, string.Format("累计纵向位移上/下限:{0}；累计横向位移上/下限:{1}；累计沉降上/下限:{2}", reportparams.n_secalrmacdisp, reportparams.e_secalrmacdisp, reportparams.z_secalrmacdisp));
                this.ExcelMergedRegion(len, (widthcol - 1) / 2 + 3, len + 3, 6);
                this.ExcelWriteWrapText(len, 6, "是否预警（报警）");
                this.ExcelMergedRegion(len, 7, len + 3, 7);
                this.ExcelWriteWrapText(len, 7, reportparams.alarmmess.Count > 0 ? "是" : "否");
                len += 4;

                int k = 0;
                //}
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                //this.ExcelMergedRegion(len, 1, len, dt.Columns.Count - 1);
                ////this.BorderSet(len, 1, len, 1);
                //this.ExcelWriteWrapText(len, 1, pointname, true);
                //len++;
                //row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                //row.GetCell(0).SetCellValue(pointname
                //    );
                //len++;

                //k = 0;
                //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                //CreateRow(len+1, sheet1, tabName.Length);//创建复表头行

                //sheet1.AddMergedRegion(new CellRangeAddress(len , len +1, 0, 0));
                //row.CreateCell(0).SetCellValue("点名");
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("高度（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("温度（°C）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(10).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                //CellFontSet(row.GetCell(10), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                ////return;
                ////k = 0;
                //len++;
                //ICellStyle styleCell = GetICellStyle();
                //ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

                //CreateRow(len, sheet1, tabName.Length);//创建复表头行
                // row = CreateRow(len , sheet1, tabName.Length);//创建复表头行
                //this.BorderSet(len, 1, len + 1, 1);
                this.ExcelMergedRegion(len, 1, len + 2, 1);
                this.ExcelWriteWrapText(len, 1, "测点");


                //this.BorderSet(len, 8, len, 10);
                this.ExcelMergedRegion(len, 2, len, widthcol);
                this.ExcelWriteWrapText(len, 2, "累计变化量(mm)");

                //this.ExcelMergedRegion(len, 8, len + 1, 8);
                //this.ExcelWriteWrapText(len, 8, "测点");

                //this.ExcelMergedRegion(len, 9, len + 1, 9);
                //this.ExcelWriteWrapText(len, 9, "沉降差");


                //this.ExcelMergedRegion(len, 12, len, 14);
                //this.ExcelWriteWrapText(len, 12, "本日变化范围（mm）");

                //this.ExcelWriteWrapText(len + 1, 12, "纵向位移");
                //this.ExcelWriteWrapText(len + 1, 13, "横向位移");
                //this.ExcelWriteWrapText(len + 1, 14, "沉降");
                //this.ExcelWriteWrapText(len + 2, 11, "min");
                //最小
                //this.ExcelWriteWrapText(len + 2, 12, SortValueMin(dt, "This_dN"));
                //this.ExcelWriteWrapText(len + 2, 13, SortValueMin(dt, "This_dE"));
                //this.ExcelWriteWrapText(len + 2, 14, SortValueMin(dt, "This_dZ"));
                //this.ExcelWriteWrapText(len + 2, 15, SortValueMin(dt, "Ac_dN"));
                //this.ExcelWriteWrapText(len + 2, 16, SortValueMin(dt, "Ac_dE"));
                //this.ExcelWriteWrapText(len + 2, 17, SortValueMin(dt, "Ac_dZ"));
                //this.ExcelMergedRegion(len, 15, len, 17);
                //this.ExcelWriteWrapText(len, 15, "累计变化范围（mm）");

                //this.ExcelWriteWrapText(len + 1, 15, "纵向位移");
                //this.ExcelWriteWrapText(len + 1, 16, "横向位移");
                //this.ExcelWriteWrapText(len + 1, 17, "沉降");
                //this.ExcelWriteWrapText(len + 3, 11, "max");
                //最大
                //this.ExcelWriteWrapText(len + 3, 12, SortValueMax(dt, "This_dN"));
                //this.ExcelWriteWrapText(len + 3, 13, SortValueMax(dt, "This_dE"));
                //this.ExcelWriteWrapText(len + 3, 14, SortValueMax(dt, "This_dZ"));
                //this.ExcelWriteWrapText(len + 3, 15, SortValueMax(dt, "Ac_dN"));
                //this.ExcelWriteWrapText(len + 3, 16, SortValueMax(dt, "Ac_dE"));
                //this.ExcelWriteWrapText(len + 3, 17, SortValueMax(dt, "Ac_dZ"));
                //this.ExcelMergedRegion(len, 15, len, 17);
                //this.ExcelWriteWrapText(len, 18, "沉降差");




                //本日变化范围（mm）
                ////sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                ////row.CreateCell(0).SetCellValue("时间");
                ////CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                ////CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(0).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("观测值（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("本日变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(7).SetCellValue("累计变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 7, 9));
                //CellFontSet(row.GetCell(7), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);


                //return;
                k = 0;
                len += 1;

                //row = CreateRow(len, sheet1, tabName.Length);//创建表头行


                k = 0;
                int i = 0;
                int icol = 2;
                for (k = 0; k < dtlist.Count; k++)
                {
                    //生成表头

                    this.ExcelMergedRegion(len, icol, len, icol + 2);
                    this.ExcelWriteWrapText(len, icol, string.Format("第{0}期", dtlist[k].Rows[0].ItemArray[1]));
                    this.ExcelWriteWrapText(len + 1, icol, "纵向位移(北)");
                    this.ExcelWriteWrapText(len + 1, icol + 1, "横向位移(东)");
                    this.ExcelWriteWrapText(len + 1, icol + 2, "沉降");
                    icol += 3;
                }


                len += 2;

                icol = 1;

                

                DataView dv = null;
                try
                {
                    for (; i < datarowscnt; i++)
                    {
                        if (len == 31) { 
                            string a = ""; 
                        };
                        icol = 1;

                        if (datarowscnt * cnt + i > pointnamelist.Count - 1)
                        {
                            //RowFormat(len, 10);
                            //CreateRow(len, sheet1, 10);
                            len++;
                            continue;
                        }
                        this.ExcelWriteWrapText(len, icol, pointnamelist[datarowscnt * cnt + i]);
                        icol++;
                        for (k = 0; k < dtlist.Count; k++)
                        {
                            //生成表头
                            ExceptionLog.ReportLineWrite(tabrowscount, reportname);
                            dv = new DataView(dtlist[k]);
                            dv.RowFilter = " point_name ='" + pointnamelist[datarowscnt * cnt + i] + "' ";
                            if (dv.Count == 0) { icol += 2; continue; }
                            this.ExcelWriteWrapText(len, icol, dv[0]["ac_dn"]);
                            this.ExcelWriteWrapText(len, icol + 1, dv[0]["ac_de"]);
                            this.ExcelWriteWrapText(len, icol + 2, dv[0]["ac_dz"]);
                            icol += 3;
                        }
                        len++;

                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                    Console.WriteLine(ex.StackTrace);
                }
                //if (difflist != null && difflist.Count > 0)
                //{
                //    difflist.Sort();
                //    ExcelWriteWrapText(rangerowstart + 7, 18, difflist[0]);
                //    ExcelWriteWrapText(rangerowstart + 8, 18, difflist[difflist.Count - 1]);
                //}
                


                
                if (reportparams.alarmmess.Count == 0)
                {
                    this.ExcelMergedRegion(len, 1, len + 2, widthcol);
                    this.ExcelWriteWrapText(len, 1, string.Format(@"监测小结：本" + timeunitname + "变化量，纵向水平位移在{0}~{1}mm之间，横向水平位移在{2}~{3}mm之间，沉降在{4}~{5}mm之间；累计变化量，纵向水平位移在{6}~{7}mm之间，横向水平位移在{8}~{9}mm之间，沉降在{10}~{11}mm之间，累计位移均未达到报警值。", SortValueMin(dttimespan, "Ac_dN"), SortValueMax(dttimespan, "Ac_dN"), SortValueMin(dttimespan, "Ac_dE"), SortValueMax(dttimespan, "Ac_dE"), SortValueMin(dttimespan, "Ac_dZ"), SortValueMax(dttimespan, "Ac_dZ"), SortValueMin(dtlist[dtlist.Count - 1], "Ac_dN"), SortValueMax(dtlist[dtlist.Count - 1], "Ac_dN"), SortValueMin(dtlist[dtlist.Count - 1], "Ac_dE"), SortValueMax(dtlist[dtlist.Count - 1], "Ac_dE"), SortValueMin(dtlist[dtlist.Count - 1], "Ac_dZ"), SortValueMax(dtlist[dtlist.Count - 1], "Ac_dZ")), Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft
                        );
                    len += 3;
                }
                else
                {
                    this.ExcelMergedRegion(len, 1, len + 3, widthcol);
                    this.ExcelWriteWrapText(len, 1, string.Format(@"监测小结：本" + timeunitname + "变化量，纵向水平位移在{0}~{1}mm之间，横向水平位移在{2}~{3}mm之间，沉降在{4}~{5}mm之间；累计变化量，纵向水平位移在{6}~{7}mm之间，横向水平位移在{8}~{9}mm之间，沉降在{10}~{11}mm之间，{12}。", SortValueMin(dttimespan, "Ac_dN"), SortValueMax(dttimespan, "Ac_dN"), SortValueMin(dttimespan, "Ac_dE"), SortValueMax(dttimespan, "Ac_dE"), SortValueMin(dttimespan, "Ac_dZ"), SortValueMax(dttimespan, "Ac_dZ"), SortValueMin(dtlist[dtlist.Count - 1], "Ac_dN"), SortValueMax(dtlist[dtlist.Count - 1], "Ac_dN"), SortValueMin(dtlist[dtlist.Count - 1], "Ac_dE"), SortValueMax(dtlist[dtlist.Count - 1], "Ac_dE"), SortValueMin(dtlist[dtlist.Count - 1], "Ac_dZ"), SortValueMax(dtlist[dtlist.Count - 1], "Ac_dZ"), "预警信息:" + string.Join("；", reportparams.alarmmess)), Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft
                           );
                    len += 4;
                }
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                
                this.ExcelMergedRegion(len, 1, len + 1, widthcol);
                this.ExcelWriteWrapText(len, 1, @"注：纵向位移以“+”表示往既有线路大里程方向偏移，“-”往小里程方向偏移；横向位移以“+”表示面向既有线路大里程方向往右侧偏移，“-”往左侧偏移；沉降以“+”表示抬升，“-”表示下沉。", Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft);
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 2;
                this.ExcelWriteWrapText(len, 1, "测量:");
                this.ExcelWriteWrapText(len, widthcol - 4, "校核:");
                len++;
                this.ExcelMergedRegion(len, widthcol - 4, len, widthcol);
                this.ExcelWriteWrapText(len, widthcol - 4, "广州铁路科技开发有限公司");
                len++;
                this.ExcelMergedRegion(len, widthcol - 1, len, widthcol);
                this.ExcelWriteWrapText(len, widthcol - 1, string.Format("{0}/{1}/{2}", time.Year, time.Month, time.Day));
                len++;
                //this.BorderSet(rangerowstart + widthcol - 1 - 1, 1, rangerowstart + widthcol - 1 + diffsettlememntcount, widthcol - 1+1);
                this.BorderSet(rangerowstart, 1, len - 4, widthcol);

                diffsettlememntcount = 0;
               
            }

        }
        

     
    


      
    }
}
