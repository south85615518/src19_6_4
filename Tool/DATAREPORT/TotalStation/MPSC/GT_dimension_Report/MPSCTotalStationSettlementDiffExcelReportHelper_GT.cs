﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {

        public void TotalStationReportTemplateCreateMPSC_GT_dimension_settlementdiff_Report_WithoutSettlement(string xmname, List<string> pointnamelist, List<DataTable> dtlist, DataTable dt, string dimensionName, string dimensionVariableName, Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams reportparams,string reportname,string indexshot)
        {
            fontsize = 9;
            int settlementDiffListcount = SettlementDiffListCountCreate(dt);
            if (settlementDiffListcount == 0) return;
            int pointwithoutsettlementcount = 0;
            int len = 0;
            //string pointname = dt.Rows[0].ItemArray[1].ToString();
            //int pagecnt = dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1;
            int tmplen = len;
            string reportNo =  Tool.com.StringHelper.GetNumFromString(reportname);
            //int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            int siblingcont = 0;
            int rangerowstart = 1;
            int widthcol = dtlist.Count * 2 + 1;
            //tabName = new string[7] { "测点", "纵向位移(北)", "横向位移(东)", "沉降", "纵向位移(北)", "横向位移(东)", "沉降" };
            DateTime time = DateTime.Now;//Convert.ToDateTime(dt.Rows[0].ItemArray[11]);
            int diffsettlememntcount = 0;
            List<double> difflist = new List<double>();
            string acdimensionVariableName = "ac_d" + dimensionVariableName;
            double min = SettlementDiffSortMin(dt);
            double max = SettlementDiffSortMax(dt);
            for (cnt = 0; 40 * cnt < pointnamelist.Count; cnt++)
            {
                rangerowstart = len;
                //添加标题
                this.ExcelMergedRegion(len, 1, len, widthcol);
                this.ExcelWriteWrapText(len, 1, string.Format("桥墩水平位移（{0}）监测成果表", dimensionName));
                len++;
                this.ExcelWriteWrapText(len, 1, "工程名称:");
                this.ExcelMergedRegion(len, 2, len, widthcol);
                this.ExcelWriteWrapText(len, 2, xmname);
                this.ExcelWriteWrapText(len + 1, 1, "工程地点:");
                this.ExcelMergedRegion(len + 1, 2, len + 1, widthcol);
                this.ExcelWriteWrapText(len + 1, 2, reportparams.xmaddress);
                len += 2;
                this.ExcelWriteWrapText(len, 1, "监测单位:");
                this.ExcelMergedRegion(len, 2, len, (widthcol - 1) / 2);
                this.ExcelWriteWrapText(len, 2, "广州铁路科技开发有限公司");

                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 1, "监测仪器");
                this.ExcelMergedRegion(len, (widthcol - 1) / 2 + 2, len, widthcol);
                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 2, "莱卡全站仪TS60");
                len++;
                this.ExcelWriteWrapText(len, 1, "黄色预警值:");
                this.ExcelMergedRegion(len, 2, len, (widthcol - 1) / 2 + 1);
                this.ExcelWriteWrapText(len, 2, "累计沉降上/下限" + dimensionalarmvaluestrGet(reportparams, "Z_firstalarm") + "；累计位移上/下限:" + dimensionalarmvaluestrGet(reportparams, dimensionVariableName + "_firstalarm"));
                this.ExcelWriteWrapText(len + 1, 1, "红色预警值:");
                this.ExcelMergedRegion(len + 1, 2, len + 1, (widthcol - 1) / 2 + 1);
                this.ExcelWriteWrapText(len + 1, 2, "累计沉降上/下限" + dimensionalarmvaluestrGet(reportparams, "Z_secalarm") + "；累计位移上/下限:" + dimensionalarmvaluestrGet(reportparams, dimensionVariableName + "_secalarm"));
                this.ExcelMergedRegion(len, (widthcol - 1) / 2 + 2, len + 1, widthcol - 2);
                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 2, "是否预警（黄色/红色预警）");
                this.ExcelMergedRegion(len, widthcol - 1, len + 1, widthcol);
                this.ExcelWriteWrapText(len, widthcol - 1, reportparams.alarmmess.Count > 0 ? "是" : "否");
                len += 2;
                int k = 0;
                //}
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                //this.ExcelMergedRegion(len, 1, len, dt.Columns.Count - 1);
                ////this.BorderSet(len, 1, len, 1);
                //this.ExcelWriteWrapText(len, 1, pointname, true);
                //len++;
                //row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                //row.GetCell(0).SetCellValue(pointname
                //    );
                //len++;

                //k = 0;
                //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                //CreateRow(len+1, sheet1, tabName.Length);//创建复表头行

                //sheet1.AddMergedRegion(new CellRangeAddress(len , len +1, 0, 0));
                //row.CreateCell(0).SetCellValue("点名");
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("高度（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("温度（°C）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(10).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                //CellFontSet(row.GetCell(10), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                ////return;
                ////k = 0;
                //len++;
                //ICellStyle styleCell = GetICellStyle();
                //ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

                //CreateRow(len, sheet1, tabName.Length);//创建复表头行
                // row = CreateRow(len , sheet1, tabName.Length);//创建复表头行
                //this.BorderSet(len, 1, len + 1, 1);
                this.ExcelMergedRegion(len, 1, len + 1, 1);
                this.ExcelWriteWrapText(len, 1, "测点");


                //this.BorderSet(len, 8, len, 10);
                this.ExcelWriteWrapText(len, 2, "监测日期");
                this.ExcelMergedRegion(len, 3, len, widthcol / 2 + 1);
                this.ExcelWriteWrapText(len, 3, DateTime.Now.Date);
                this.ExcelWriteWrapText(len, widthcol / 2 + 2, "天气");
                this.ExcelMergedRegion(len, widthcol / 2 + 3, len, widthcol);
                this.ExcelWriteWrapText(len, widthcol / 2 + 3, "");
                //this.ExcelMergedRegion(len, 8, len + 1, 8);
                //this.ExcelWriteWrapText(len, 8, "测点");

                //this.ExcelMergedRegion(len, 9, len + 1, 9);
                //this.ExcelWriteWrapText(len, 9, "沉降差");


                //this.ExcelMergedRegion(len, 12, len, 14);
                //this.ExcelWriteWrapText(len, 12, "本日变化范围（mm）");

                //this.ExcelWriteWrapText(len + 1, 12, "纵向位移");
                //this.ExcelWriteWrapText(len + 1, 13, "横向位移");
                //this.ExcelWriteWrapText(len + 1, 14, "沉降");
                //this.ExcelWriteWrapText(len + 2, 11, "min");
                //最小
                //this.ExcelWriteWrapText(len + 2, 12, SortValueMin(dt, "This_dN"));
                //this.ExcelWriteWrapText(len + 2, 13, SortValueMin(dt, "This_dE"));
                //this.ExcelWriteWrapText(len + 2, 14, SortValueMin(dt, "This_dZ"));
                //this.ExcelWriteWrapText(len + 2, 15, SortValueMin(dt, "dimension_dN"));
                //this.ExcelWriteWrapText(len + 2, 16, SortValueMin(dt, "dimension_dE"));
                //this.ExcelWriteWrapText(len + 2, 17, SortValueMin(dt, "dimension_dZ"));
                //this.ExcelMergedRegion(len, 15, len, 17);
                //this.ExcelWriteWrapText(len, 15, "累计变化范围（mm）");

                //this.ExcelWriteWrapText(len + 1, 15, "纵向位移");
                //this.ExcelWriteWrapText(len + 1, 16, "横向位移");
                //this.ExcelWriteWrapText(len + 1, 17, "沉降");
                //this.ExcelWriteWrapText(len + 3, 11, "max");
                //最大
                //this.ExcelWriteWrapText(len + 3, 12, SortValueMax(dt, "This_dN"));
                //this.ExcelWriteWrapText(len + 3, 13, SortValueMax(dt, "This_dE"));
                //this.ExcelWriteWrapText(len + 3, 14, SortValueMax(dt, "This_dZ"));
                //this.ExcelWriteWrapText(len + 3, 15, SortValueMax(dt, "dimension_dN"));
                //this.ExcelWriteWrapText(len + 3, 16, SortValueMax(dt, "dimension_dE"));
                //this.ExcelWriteWrapText(len + 3, 17, SortValueMax(dt, "dimension_dZ"));
                //this.ExcelMergedRegion(len, 15, len, 17);
                //this.ExcelWriteWrapText(len, 18, "沉降差");




                //本日变化范围（mm）
                ////sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                ////row.CreateCell(0).SetCellValue("时间");
                ////CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                ////CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(0).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("观测值（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("本日变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(7).SetCellValue("累计变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 7, 9));
                //CellFontSet(row.GetCell(7), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);


                //return;
                k = 0;
                len += 1;
                string siblingpointname = "";
                //row = CreateRow(len, sheet1, tabName.Length);//创建表头行

                double settlementvalue = 0;
                double siblingsettlementvalue = 0;
                k = 0;
                int i = 0;
                int icol = 2;
                for (k = 0; k < dtlist.Count; k++)
                {
                    //生成表头

                    //this.ExcelMergedRegion(len, icol, len, icol + 1);
                    //this.ExcelWriteWrapText(len, icol, string.Format("第{0}次", k+1));
                    this.ExcelMergedRegion(len , icol, len , icol + 1);
                    this.ExcelWriteWrapText(len , icol, string.Format("第{0}周期", dtlist[k].Rows[0].ItemArray[1]));
                    icol += 2;
                }
                this.ExcelWriteWrapText(len + 1, icol, "最小值(mm)");
                this.ExcelWriteWrapText(len + 1, icol + 1, "最大值(mm)");
                this.ExcelWriteWrapText(len + 2, icol, min);
                this.ExcelWriteWrapText(len + 2, icol + 1, max);


                len ++;

                icol = 1;
                DataView dv = new DataView(dt);
                try
                {
                    for (; i < 40; i++)
                    {
                        if (i == 17) { string a = ""; };

                        
                        icol = 1;
                        if (i == 23) { string a = ""; };
                        if (40 * cnt + i > pointnamelist.Count - 1)
                        {

                            int mlen = 0;
                            
                            for (mlen = 0; mlen < pointwithoutsettlementcount;mlen++ )
                            {
                                icol = 2;
                                for (k = 0; k < dtlist.Count; k++)
                                {
                                    this.ExcelMergedRegion(len, icol, len, icol + 1);
                                    icol += 2; continue;


                                }
                                len++;
                            }
                            if (mlen == pointwithoutsettlementcount && pointwithoutsettlementcount>0) { pointwithoutsettlementcount = 0; continue; };
                                
                                
                                if (i == 39)
                                {
                                    icol = 1;
                                    this.ExcelWriteWrapText(len, icol, "变化最大值");
                                    icol++;
                                    for (k = 0; k < dtlist.Count; k++)
                                    {
                                        this.ExcelMergedRegion(len, icol, len, icol + 1);

                                        this.ExcelWriteWrapText(len, icol, SettlementDiffSortMax(dtlist[k]));
                                        icol += 2; continue;


                                    }
                                    len++;
                                    continue;
                                }
                                icol++;
                                for (k = 0; k < dtlist.Count; k++)
                                {
                                    this.ExcelMergedRegion(len, icol, len, icol + 1);
                                   

                                    icol += 2; continue;


                                }

                                len++;
                                continue;
                            
                        }
                        dv.RowFilter = " point_name ='" + pointnamelist[39 * cnt + i] + "' ";
                        if (dv.Count == 0) { pointwithoutsettlementcount++; continue; }
                        siblingpointname = dv[0]["siblingpoint_name"].ToString();
                        dv.RowFilter = " point_name ='" + dv[0]["siblingpoint_name"] + "' ";
                        if (dv.Count == 0) { pointwithoutsettlementcount++; continue; }
                       
                        if (i == 39)
                            this.ExcelWriteWrapText(len, icol, "变化最大值");
                        else
                            this.ExcelWriteWrapText(len, icol, pointnamelist[i] + ":" + siblingpointname);
                        icol++;

                        for (k = 0; k < dtlist.Count; k++)
                        {
                            //生成表头
                            if (i == 37)
                            {
                                this.ExcelMergedRegion(len, icol, len, icol + 1);
                                this.ExcelWriteWrapText(len, icol, SettlementDiffSortMax(dtlist[k]));
                                icol += 2; continue;
                            }
                            dv = new DataView(dtlist[k]);
                            //生成表头
                            
                            dv.RowFilter = " point_name ='" + pointnamelist[i] + "' ";
                            if (dv.Count == 0) { icol += 2; continue; }
                            settlementvalue = Convert.ToDouble(dv[0]["ac_dz"]);
                            
                            this.ExcelMergedRegion(len, icol, len, icol + 1);
                            dv.RowFilter = " point_name ='" + dv[0]["siblingpoint_name"] + "' ";
                            if (dv.Count == 0) { icol += 2; continue; }
                            siblingsettlementvalue = Convert.ToDouble(dv[0]["ac_dz"]);
                            this.ExcelWriteWrapText(len, icol,settlementvalue - siblingsettlementvalue);
                            icol += 2;
                        }
                        if (len == 12) { string a = ""; }
                        len++;
                        ExceptionLog.ReportLineWrite(pointnamelist.Count, indexshot, reportname);
                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                    Console.WriteLine(ex.StackTrace);
                }
                //if (difflist != null && difflist.Count > 0)
                //{
                //    difflist.Sort();
                //    ExcelWriteWrapText(rangerowstart + 7, 18, difflist[0]);
                //    ExcelWriteWrapText(rangerowstart + 8, 18, difflist[difflist.Count - 1]);
                //}





                this.ExcelMergedRegion(len, 1, len + 3, widthcol);

                this.ExcelWriteWrapText(len, 1, string.Format(@"监测小结：本日监测中，" + pointnamelist.Count + "个桥墩水平位移测点中,累计变化量在{0}~{1}mm，均未达到预警值。",min,max ));
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 4;
                this.ExcelMergedRegion(len, 1, len + 1, widthcol);
                this.ExcelWriteWrapText(len, 1, @"备注:表中沉降值，正值的为相对上升，负值的为相对下沉。");
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 2;
                this.ExcelWriteWrapText(len, 1, "监测:");

                this.BorderSet(rangerowstart, 1, len - 4, widthcol);

                diffsettlememntcount = 0;

            }

        }
        public int SettlementDiffListCountCreate(DataTable dt)
        {
            //List<string> settlementDiffList = new List<string>();
            //DataView dv = new DataView(dt);
            //dv.RowFilter = " distinct(point_name),siblingpoint_name ";
            
            List<string> siblingpoints = Tool.DataTableHelper.ProcessDataTableFilter(dt,"siblingpoint_name");
            siblingpoints.Remove("0");
            return siblingpoints.Count;
            //return settlementDiffList;
        }
        public double SettlementDiffSortMax(DataTable dt)
        {
            double max = 0;
            DataView dv = new DataView(dt);
            DataView dvsibling = new DataView(dt);
            bool valseted = false;
            foreach (DataRowView drv in dv)
            {
                if (drv["siblingpoint_name"].ToString() == "" || drv["siblingpoint_name"].ToString() == "0") continue;
                dvsibling.RowFilter = " point_name = '" + drv["siblingpoint_name"] + "'  and  cyc = " + drv["cyc"];
                if (dvsibling.Count == 0) { continue; }
                if (!valseted) { max = Convert.ToDouble(drv["ac_dz"]) - Convert.ToDouble(dvsibling[0]["ac_dz"]); valseted = !valseted; continue; };
                max = max > (Convert.ToDouble(drv["ac_dz"]) - Convert.ToDouble(dvsibling[0]["ac_dz"])) ? max : (Convert.ToDouble(drv["ac_dz"]) - Convert.ToDouble(dvsibling[0]["ac_dz"]));

                //dvsibling.RowFilter = " 1=1 ";
            }
            return max;
        }

        public double SettlementDiffSortMin(DataTable dt)
        {
            double min = 0;
            DataView dv = new DataView(dt);
            DataView dvsibling = new DataView(dt);
            bool valseted = false;
            foreach (DataRowView drv in dv)
            {
                if (drv["siblingpoint_name"].ToString() == "" || drv["siblingpoint_name"].ToString() =="0") continue;
                dvsibling.RowFilter = " point_name = '" + drv["siblingpoint_name"] + "'  and  cyc = " + drv["cyc"];
                if (dvsibling.Count == 0) { continue; }
                if (!valseted) { min = Convert.ToDouble(drv["ac_dz"]) - Convert.ToDouble(dvsibling[0]["ac_dz"]); valseted = !valseted; continue; };
                min = min < (Convert.ToDouble(drv["ac_dz"]) - Convert.ToDouble(dvsibling[0]["ac_dz"])) ? min : (Convert.ToDouble(drv["ac_dz"]) - Convert.ToDouble(dvsibling[0]["ac_dz"]));
                //dvsibling.RowFilter = " 1=1 ";
            }
            return min;
        }

        public double SettlementDiffValueGet(DataTable dt, string point_name, string siblingpoint_name)
        {
            double min = 0;
            DataView dvsibling = new DataView(dt);
            dvsibling.RowFilter = " point_name = '" + siblingpoint_name+"' " ;

            return Convert.ToDouble(dvsibling[0]["ac_dz"]);

        }


    }
}
