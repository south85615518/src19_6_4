﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {

        public void TotalStationReportTemplateCreateMPSC_GT_WithoutSettlement(string xmname, string[] tabName, DataTable dt, int len,Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams reportparams,string reportname)
        {
            List<string> points = Tool.DataTableHelper.ProcessDataTableFilter(dt, "point_name");
            List<string> pointssort = null;
            if (Tool.com.StringHelper.SameExSort(points, out pointssort))
            {
                try
                {
                    dt.Columns.Add("sort",typeof(int));
                    DataView dvsort = new DataView(dt);
                    foreach (DataRowView drv in dvsort)
                    {
                        drv["sort"] = pointssort.IndexOf(drv["point_name"].ToString());
                    }
                    dvsort.Sort = " sort asc ";
                    dt = dvsort.ToTable();
                    dt.Columns.Remove("sort");
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("对" + reportname + "数据表点名排序出错");
                }

            }


            string pointname = dt.Rows[0].ItemArray[1].ToString();
            int pagecnt = dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1;
            int tmplen = len;
            ExceptionLog.ExceptionWrite("报表:" + reportname);
            string reportNo = reportname;// Tool.com.StringHelper.GetNumFromString(reportname);
            int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            int rangerowstart = len;
            int widthcol = 7;
            tabName = new string[7] { "测点", "纵向位移(北)", "横向位移(东)", "沉降", "纵向位移(北)", "横向位移(东)", "沉降" };
            DateTime time = Convert.ToDateTime(dt.Rows[0].ItemArray[11]);
            int diffsettlememntcount = 0;
            List<double> difflist = new List<double>();
            for (cnt = 0; 36*cnt < dt.Rows.Count ; cnt++)
            {
                rangerowstart = len;
                //添加标题
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                this.ExcelMergedRegion(len, 1, len, widthcol);

                this.ExcelWriteWrapText(len, 1, xmname);
                len++;
                this.ExcelMergedRegion(len, 1, len, widthcol);
                this.ExcelWriteWrapText(len, 1, string.Format("位移监测周期报表 NO.{0}",reportNo));
                len++;
                this.ExcelWriteWrapText(len, 1, "工程名称:");
                this.ExcelMergedRegion(len, 2, len, (widthcol - 1) / 2 + 1);
                this.ExcelWriteWrapText(len, 2, xmname);
                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 2, "监测周期:");
                this.ExcelMergedRegion(len, (widthcol - 1) / 2 + 3, len, widthcol);
                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 3, string.Format("第{0}周期", dt.Rows[0].ItemArray[1]));
                len++;
                this.ExcelWriteWrapText(len, 1, "观测方法:");
                this.ExcelMergedRegion(len, 2, len, (widthcol - 1) / 2);
                this.ExcelWriteWrapText(len, 2, "自动全站仪测量");

                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 1, "观测时间");
                this.ExcelMergedRegion(len, (widthcol - 1) / 2 + 2, len, widthcol);
                this.ExcelWriteWrapText(len, (widthcol - 1) / 2 + 2, string.Format("{0}~{1}", dt.Rows[0].ItemArray[11], dt.Rows[dt.Rows.Count-1].ItemArray[11]));
                len++;
                this.ExcelMergedRegion(len, 1, len + 1, 1);
                this.ExcelWriteWrapText(len, 1, "预警值:");
                this.ExcelMergedRegion(len, 2, len + 1, (widthcol - 1) / 2 + 2);
                this.ExcelWriteWrapText(len, 2, string.Format("累计纵向位移上/下限:{0}；横向位移上/下限:{1}；沉降上/下限:{2}；本次纵向位移:{3}；横向位移{4}；沉降{5}", reportparams.n_firstalrmacdisp, reportparams.e_firstalrmacdisp, reportparams.z_firstalrmacdisp, reportparams.n_firstalrmthisdisp, reportparams.e_firstalrmthisdisp, reportparams.z_firstalrmthisdisp));
                this.ExcelMergedRegion(len + 2, 1, len + 3, 1);
                this.ExcelWriteWrapText(len + 2, 1, "报警值:");
                this.ExcelMergedRegion(len + 2, 2, len + 3, (widthcol - 1) / 2 + 2);
                this.ExcelWriteWrapText(len + 2, 2, string.Format("累计纵向位移上/下限:{0}；横向位移上/下限:{1}；沉降上/下限:{2}；本次纵向位移:{3}；横向位移{4}；沉降{5}", reportparams.n_secalrmacdisp, reportparams.e_secalrmacdisp, reportparams.z_secalrmacdisp, reportparams.n_secalrmthisdisp, reportparams.e_secalrmthisdisp, reportparams.z_secalrmthisdisp));
                this.ExcelMergedRegion(len, (widthcol - 1) / 2 + 3, len + 3, 6);
                this.ExcelWriteWrapText(len, 6, "是否预警（报警）");
                this.ExcelMergedRegion(len, 7, len + 3, 7);
                this.ExcelWriteWrapText(len, 7, reportparams.alarmmess.Count>0?"是":"否");
                len += 4;
                //}
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                //this.ExcelMergedRegion(len, 1, len, dt.Columns.Count - 1);
                ////this.BorderSet(len, 1, len, 1);
                //this.ExcelWriteWrapText(len, 1, pointname, true);
                //len++;
                //row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                //row.GetCell(0).SetCellValue(pointname
                //    );
                //len++;

                //k = 0;
                //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                //CreateRow(len+1, sheet1, tabName.Length);//创建复表头行

                //sheet1.AddMergedRegion(new CellRangeAddress(len , len +1, 0, 0));
                //row.CreateCell(0).SetCellValue("点名");
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("高度（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("温度（°C）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(10).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                //CellFontSet(row.GetCell(10), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                ////return;
                ////k = 0;
                //len++;
                //ICellStyle styleCell = GetICellStyle();
                //ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

                //CreateRow(len, sheet1, tabName.Length);//创建复表头行
                // row = CreateRow(len , sheet1, tabName.Length);//创建复表头行
                //this.BorderSet(len, 1, len + 1, 1);
                this.ExcelMergedRegion(len, 1, len + 1, 1);
                this.ExcelWriteWrapText(len, 1, "测点");

                //this.BorderSet(len, 5, len, 7);
                this.ExcelMergedRegion(len, 2, len, 4);
                this.ExcelWriteWrapText(len, 2, "本次变化量(mm)");

                //this.BorderSet(len, 8, len, 10);
                this.ExcelMergedRegion(len, 5, len, 7);
                this.ExcelWriteWrapText(len, 5, "累计变化量(mm)");

                //this.ExcelMergedRegion(len, 8, len + 1, 8);
                //this.ExcelWriteWrapText(len, 8, "测点");

                //this.ExcelMergedRegion(len, 9, len + 1, 9);
                //this.ExcelWriteWrapText(len, 9, "沉降差");


                //this.ExcelMergedRegion(len, 12, len, 14);
                //this.ExcelWriteWrapText(len, 12, "本次变化范围（mm）");

                //this.ExcelWriteWrapText(len + 1, 12, "纵向位移");
                //this.ExcelWriteWrapText(len + 1, 13, "横向位移");
                //this.ExcelWriteWrapText(len + 1, 14, "沉降");
                //this.ExcelWriteWrapText(len + 2, 11, "min");
                //最小
                //this.ExcelWriteWrapText(len + 2, 12, SortValueMin(dt, "This_dN"));
                //this.ExcelWriteWrapText(len + 2, 13, SortValueMin(dt, "This_dE"));
                //this.ExcelWriteWrapText(len + 2, 14, SortValueMin(dt, "This_dZ"));
                //this.ExcelWriteWrapText(len + 2, 15, SortValueMin(dt, "Ac_dN"));
                //this.ExcelWriteWrapText(len + 2, 16, SortValueMin(dt, "Ac_dE"));
                //this.ExcelWriteWrapText(len + 2, 17, SortValueMin(dt, "Ac_dZ"));
                //this.ExcelMergedRegion(len, 15, len, 17);
                //this.ExcelWriteWrapText(len, 15, "累计变化范围（mm）");
                
                //this.ExcelWriteWrapText(len + 1, 15, "纵向位移");
                //this.ExcelWriteWrapText(len + 1, 16, "横向位移");
                //this.ExcelWriteWrapText(len + 1, 17, "沉降");
                //this.ExcelWriteWrapText(len + 3, 11, "max");
                //最大
                //this.ExcelWriteWrapText(len + 3, 12, SortValueMax(dt, "This_dN"));
                //this.ExcelWriteWrapText(len + 3, 13, SortValueMax(dt, "This_dE"));
                //this.ExcelWriteWrapText(len + 3, 14, SortValueMax(dt, "This_dZ"));
                //this.ExcelWriteWrapText(len + 3, 15, SortValueMax(dt, "Ac_dN"));
                //this.ExcelWriteWrapText(len + 3, 16, SortValueMax(dt, "Ac_dE"));
                //this.ExcelWriteWrapText(len + 3, 17, SortValueMax(dt, "Ac_dZ"));
                //this.ExcelMergedRegion(len, 15, len, 17);
                //this.ExcelWriteWrapText(len, 18, "沉降差");

               


                //本次变化范围（mm）
                ////sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                ////row.CreateCell(0).SetCellValue("时间");
                ////CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                ////CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(0).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("观测值（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("本次变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(7).SetCellValue("累计变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 7, 9));
                //CellFontSet(row.GetCell(7), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);


                //return;
                int k = 0;
                len+=1;

                //row = CreateRow(len, sheet1, tabName.Length);//创建表头行


                k = 0;

                foreach (string name in tabName)
                {
                    //if (k == 0) { k++; continue; }

                    if (k == dt.Columns.Count - 1)
                    {

                    }
                    //CellBorderSet(row.GetCell(k), styleBorder, true);
                    else
                    {
                        this.ExcelWriteWrapText(len, k + 1, name);
                        //row.CreateCell(k).SetCellValue(name);
                        //CellBorderSolid(row.GetCell(k), styleCell, true);
                    }
                    k++;
                }

                len++;

                int i = 0;
                try
                {
                    for (; i < 36; i++)
                    {

                        if (36 * cnt + i-5 > dt.Rows.Count - 1)
                        {
                            //RowFormat(len, 10);
                            //CreateRow(len, sheet1, 10);
                            len++;
                            continue;
                        }
                        int j = 0, t = 1;
                        for (j = 0; j < 8; j++)
                        {

                            if (j == 1) continue;
                            if (t==1||t==11)
                            {

                                try
                                {
                                    ExcelWriteWrapText(len, t, dt.Rows[36 * cnt + i].ItemArray[j]);
                                }
                                catch (Exception)
                                {
                                    
                                    string a = "";
                                }

                            }
                            else
                            {
                                try
                                {
                                    ExcelWriteWrapText(len, t, Convert.ToDouble(dt.Rows[36 * cnt + i].ItemArray[j + 3]));
                                }
                                catch (Exception ex)
                                {
                                    string a = "";
                                }

                            }

                            t++;

                        }
                        if (i == 24) { 
                            string a = ""; 
                        }
                        //double diff = DiffSettlement(dt, dt.Rows[31 * cnt + i].ItemArray[0].ToString(), dt.Rows[31 * cnt + i].ItemArray[dt.Columns.Count-1].ToString());
                        //if (diff != -9.99)
                        //{
                        //    ExcelWriteWrapText(rangerowstart+7+diffsettlememntcount, 8, string.Format("{0}：{1}", dt.Rows[31 * cnt + i].ItemArray[0].ToString(), dt.Rows[31 * cnt + i].ItemArray[dt.Columns.Count - 1].ToString()));
                        //    ExcelWriteWrapText(rangerowstart + 7 + diffsettlememntcount, 9, diff);
                        //    difflist.Add(diff);
                        //    diffsettlememntcount++;
                        //}
                        len++;
                       
                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                    Console.WriteLine(ex.StackTrace);
                }
                //if (difflist != null && difflist.Count > 0)
                //{
                //    difflist.Sort();
                //    ExcelWriteWrapText(rangerowstart + 7, 18, difflist[0]);
                //    ExcelWriteWrapText(rangerowstart + 8, 18, difflist[difflist.Count - 1]);
                //}
                
                this.ExcelMergedRegion(len, 1, len + 2, 7);

                this.ExcelWriteWrapText(len, 1, string.Format(@"监测小结：本次变化量，纵向水平位移在{0}~{1}mm内，横向水平位移在{2}~{3}mm内，沉降在{4}~{5}mm内；累计变化量，纵向水平位移在{6}~{7}mm内，横向水平位移在{8}~{9}mm内，沉降在{10}~{11}mm内,累计位移未达到报警值。", SortValueMin(dt, "This_dN"), SortValueMax(dt, "This_dN"), SortValueMin(dt, "This_dE"), SortValueMax(dt, "This_dE"), SortValueMin(dt, "This_dZ"), SortValueMax(dt, "This_dZ"), SortValueMin(dt, "Ac_dN"), SortValueMax(dt, "Ac_dN"), SortValueMin(dt, "Ac_dE"), SortValueMax(dt, "Ac_dE"), SortValueMin(dt, "Ac_dZ"), SortValueMax(dt, "Ac_dZ")));
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 3;
                this.ExcelMergedRegion(len, 1, len + 1, 7);
                this.ExcelWriteWrapText(len, 1, @"注：纵向位移以“+”表示往既有线路大里程方向偏移，“-”往小里程方向偏移；横向位移以“+”表示面向既有线路大里程方向往右侧偏移，“-”往左侧偏移；沉降以“+”表示抬升，“-”表示下沉。");
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 2;
                this.ExcelWriteWrapText(len, 1, "测量:");
                this.ExcelWriteWrapText(len, 4, "校核:");
                len++;
                this.ExcelMergedRegion(len, 5, len, 7);
                this.ExcelWriteWrapText(len, 5, "广州铁路科技开发有限公司");
                len++;
                this.ExcelMergedRegion(len, 6, len, 7);
                this.ExcelWriteWrapText(len, 6, string.Format("{0}/{1}/{2}", time.Year, time.Month, time.Day));
                len++;
                this.BorderSet(rangerowstart, 1, len - 4, 7);
                //this.BorderSet(rangerowstart + 10, 1, len - 4, 7);

                diffsettlememntcount = 0;
            }
            

        }

     
    


      
    }
}
