﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;


namespace Tool
{
    public partial class GTXLSHelper
    {




        public static string ExcelValueFormatCompatible(ICell cell)
        {

            try
            {
                return cell.StringCellValue.ToString();
            }
            catch
            {
                try
                {
                    return cell.NumericCellValue.ToString();
                }
                catch
                {
                    try
                    {
                        return cell.DateCellValue.ToString();
                    }
                    catch
                    {
                        try
                        {
                            return cell.BooleanCellValue.ToString();
                        }
                        catch
                        {
                            try
                            {
                                return cell.RichStringCellValue.ToString();
                            }
                            catch
                            {
                                try
                                {

                                    return cell.CellFormula.ToString();
                                }
                                catch
                                {
                                    return "";
                                }
                            }
                        }
                    }
                }
            }




        }

        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void GetDataFromGTSensorData(string xlspath, ISheet sheet1, DataTable dt, out string mssg)
        {


            int i = 1;
            mssg = "";
            try
            {

                while (i < 100000)
                {

                    try
                    {
                        if (sheet1.GetRow(i) == null) return;

                        DataRow dr = dt.NewRow();
                        dr["point_name"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(0));
                        dr["datatype"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(1));
                        if (sheet1.GetRow(i).GetCell(2) != null)
                            dr["single_region_scalarvalue"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(2));
                        if (sheet1.GetRow(i).GetCell(3) != null)
                            dr["first_region_scalarvalue"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(3));
                        if (sheet1.GetRow(i).GetCell(4) != null)
                            dr["sec_region_scalarvalue"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(4));
                        if (sheet1.GetRow(i).GetCell(5) != null && sheet1.GetRow(i).GetCell(6) != null)
                        {
                            string timestr = Convert.ToDateTime(sheet1.GetRow(i).GetCell(5).StringCellValue).Date.Add(Convert.ToDateTime(sheet1.GetRow(i).GetCell(6).StringCellValue).TimeOfDay).ToString();
                            dr["time"] = timestr;
                        }
                        dt.Rows.Add(dr);

                    }
                    catch (Exception ex)
                    {
                        mssg += "\r\n" + string.Format("文件{0}工作表{1}第{2}行数据读取出错,错误信息:" + ex.Message, xlspath, sheet1.SheetName, i);
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                string a = "";
            }







        }


        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void DeleteDataFromGTSensorData(string xlspath, ISheet sheet1, out DeleteDataFromGTSensorDataOutInfoModel model, out string mssg)
        {
            model = new DeleteDataFromGTSensorDataOutInfoModel();
            int i = 7;
            mssg = "";
            try
            {
                if ((sheet1.GetRow(3) == null) || sheet1.GetRow(2) == null || sheet1.GetRow(4) == null) { mssg = "\r\n" + string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName); return; }
                model.xmname = sheet1.GetRow(3).GetCell(1).StringCellValue;// ExcelHelper.xSheet.Cells[4, 2].Value;
                DateTime time = Convert.ToDateTime(sheet1.GetRow(4).GetCell(5).DateCellValue);
                model.dt = time;//Convert.ToInt32(sheet1.GetRow(2).GetCell(0).NumericCellValue);

            }
            catch (Exception ex)
            {
                string a = "";
            }

        }
        public class DeleteDataFromGTSensorDataOutInfoModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public DeleteDataFromGTSensorDataOutInfoModel(string xmname, int cyc)
            {
                this.xmname = xmname;
                this.dt = dt;
            }
            public DeleteDataFromGTSensorDataOutInfoModel()
            {

            }
        }





        public static bool GTSensorsorDataFromCheck(ISheet sheet1)
        {

            int resultPass = 0;
            int i = 0;
            string cellvalue = "";
            CellType type = new CellType();
            while (i < 1000)
            {
                try
                {
                    if (sheet1.GetRow(0) == null || sheet1.GetRow(0).GetCell(i) == null) break;
                    type = sheet1.GetRow(0).GetCell(i).CellType;
                    if (type == CellType.STRING)
                    {
                        cellvalue = sheet1.GetRow(0).GetCell(i).StringCellValue;
                        //点名

                        //传感器编号
                        if (sheet1.GetRow(0).GetCell(i).StringCellValue.IndexOf("传感器编号") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        //传感器类型
                        if (sheet1.GetRow(0).GetCell(i).StringCellValue.IndexOf("传感器类型") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        //传感器类型
                        if (sheet1.GetRow(0).GetCell(i).StringCellValue.IndexOf("数据1") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        //传感器类型
                        if (sheet1.GetRow(0).GetCell(i).StringCellValue.IndexOf("数据2") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        else
                        {
                            i++;
                            continue;
                        }

                    }

                    i++;
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("测斜数据excel文件导入出错,出错信息：" + ex.Message);
                    i++;

                }


            }
            return resultPass == 4 ? true : false;




        }

        /// <summary>
        /// excel数据文件导入
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessGTSensorDataDataTableImport(string xlspath, out DataTable dt, out string mssg)
        {

            mssg = "";
            bool result = true;
            int i = 0;
            dt = new DataTable();
            dt.Columns.Add("point_name");
            dt.Columns.Add("datatype");
            dt.Columns.Add("single_region_scalarvalue");
            dt.Columns.Add("first_region_scalarvalue");
            dt.Columns.Add("sec_region_scalarvalue");
            dt.Columns.Add("time");

            ISheet sheet1 = null;
            while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {

                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                string sheetmssg = "";
                if (GTSensorsorDataFromCheck(sheet1))
                    GetDataFromGTSensorData(xlspath, sheet1, dt, out sheetmssg);
                else
                    sheetmssg = string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName);
                if (sheetmssg != "")
                    mssg += "\r\n" + sheetmssg;
                i++;
            }
            return true;
        }

        /// <summary>
        /// excel数据文件导入
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessGTSensorDataCycDataDelete(string xlspath, out List<DeleteDataFromGTSensorDataOutInfoModel> modellist, out string mssg)
        {

            mssg = "";
            bool result = true;
            int i = 0;
            modellist = new List<DeleteDataFromGTSensorDataOutInfoModel>();
            ISheet sheet1 = null;
            while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {

                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                string sheetmssg = "";
                DeleteDataFromGTSensorDataOutInfoModel model = new DeleteDataFromGTSensorDataOutInfoModel();
                if (GTSensorsorDataFromCheck(sheet1))
                {
                    DeleteDataFromGTSensorData(xlspath, sheet1, out model, out sheetmssg);
                    modellist.Add(model);
                }
                else
                    sheetmssg = string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName);
                if (sheetmssg != "")
                    mssg += "\r\n" + sheetmssg;
                i++;
            }
            return true;
        }
        /// <summary>
        /// excel文件格式检查
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessGTSensorDataDataImportCheck(string upzipfilePath, out string mssg)
        {


            bool result = true;
            mssg = "";
            int i = 0;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(upzipfilePath);

            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
                {
                    PublicCompatibleInitializeWorkbook(filename);
                    i = 0;
                    ISheet sheet1 = null;
                    while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
                    {
                        sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                        if (!GTSensorsorDataFromCheck(sheet1))
                        {
                            mssg += string.Format("{0}工作表{1}导入失败 ", Path.GetFileName(filename) + "、\r\n{0}", sheet1.SheetName);
                            result = false;
                        }
                        i++;
                    }


                }
                else
                {
                    mssg += string.Format("{0}不是EXCEL可执行文件。 ", Path.GetFileName(filename) + "、\r\n{0}");
                    result = false;
                }

            }
            mssg = mssg.Replace("、\r\n{0}", "");
            return result;

        }
        public static bool GTSensorDataFileDataImportCheck(string filename, out string mssg)
        {
            PublicCompatibleInitializeWorkbook(filename);
            mssg = "";
            int i = 0;
            ISheet sheet1 = null;
            while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {
                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                if (!GTSensorsorDataFromCheck(sheet1))
                {
                    mssg += string.Format("{0}工作表{1}导入失败 ", Path.GetFileName(filename) + "、\r\n{0}", sheet1.SheetName);

                }
                i++;
            }
            return true;
        }

        #region DTU传感器
        /// <summary>
        /// excel数据文件导入
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessSensorDataDataTableImport(string xlspath, out DataTable dt, out string mssg)
        {

            mssg = "";
            bool result = true;
            int i = 0;
            dt = new DataTable();
            dt.Columns.Add("传感器编号");
            dt.Columns.Add("标定系数K");


            ISheet sheet1 = null;
            while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {

                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                string sheetmssg = "";
                //if (SensorsorDataFromCheck(sheet1))
                GetDataFromSensorData(xlspath, sheet1, dt, out sheetmssg);
                //else
                // sheetmssg = string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName);
                if (sheetmssg != "")
                    mssg += "\r\n" + sheetmssg;
                i++;
            }
            return true;
        }

        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void GetDataFromSensorData(string xlspath, ISheet sheet1, DataTable dt, out string mssg)
        {


            int i = 5;
            mssg = "";
            try
            {

                while (i < 100000)
                {

                    try
                    {
                        if (sheet1.GetRow(i) == null) return;

                        DataRow dr = dt.NewRow();
                        dr["传感器编号"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(1));
                        dr["标定系数K"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(2));

                        dt.Rows.Add(dr);

                    }
                    catch (Exception ex)
                    {
                        mssg += "\r\n" + string.Format("文件{0}工作表{1}第{2}行数据读取出错,错误信息:" + ex.Message, xlspath, sheet1.SheetName, i);
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                string a = "";
            }







        }



        #endregion

        #region 模块
        /// <summary>
        /// excel数据文件导入
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessModuleDataTableImport(string xlspath, out DataTable dt, out string mssg)
        {

            mssg = "";
            bool result = true;
            int i = 0;
            dt = new DataTable();
            dt.Columns.Add("点名");
            dt.Columns.Add("应力值");
            //dt.Columns.Add("模块号");
            //dt.Columns.Add("通道号");
            //dt.Columns.Add("传感器编号");

            ISheet sheet1 = null;
            if (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {

                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                string sheetmssg = "";
                //if (SensorsorDataFromCheck(sheet1))
                GetDataFromModuleData(xlspath, sheet1, dt, out sheetmssg);
                //else
                // sheetmssg = string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName);
                if (sheetmssg != "")
                    mssg += "\r\n" + sheetmssg;
                i++;
            }
            return true;
        }

        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void GetDataFromModuleData(string xlspath, ISheet sheet1, DataTable dt, out string mssg)
        {


            int i = 1;
            mssg = "";
            try
            {

                while (i < 100000)
                {

                    try
                    {
                        if (sheet1.GetRow(i) == null) return;

                        DataRow dr = dt.NewRow();
                        dr["点名"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(0));
                        dr["应力值"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(5));
                        //dr["模块号"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(2));
                        //dr["通道号"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(3));
                        //dr["传感器编号"] = ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(4));
                        dt.Rows.Add(dr);

                    }
                    catch (Exception ex)
                    {
                        mssg += "\r\n" + string.Format("文件{0}工作表{1}第{2}行数据读取出错,错误信息:" + ex.Message, xlspath, sheet1.SheetName, i);
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                string a = "";
            }







        }



        #endregion
    }
}