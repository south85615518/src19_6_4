﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.TotalStation
{
    public partial class TotalStationDataFill
    {

        public void GTinclinometer_timespanFillMPSC(string xmname,string holename ,string tabHead,  string reportname, DataTable origldatatable, string timeunitname, Tool.DATAREPORT.TotalStation.MPSC.GT.reportparams reportparams)
        {

            string[] tabName = tabHead.Split(',');
            int i = 0;
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 1, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 2, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 3, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 7, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 8, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 9, 10);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 10, 10);
            //for (i = 0; i < pointnamelist.Count; i++)
            //{
            //    ExceptionLog.ReportLineWrite(string.Format("开始打印{0}第{2}个周期周期{1}数据...", xmname, lrf[i].dt.Rows[0].ItemArray[1].ToString(), (i+1) + "/" + lrf.Count), reportname);
            //    ExcelHelper.TotalStationReportTemplateCreateMPSC_GT_DAYReport_WithoutSettlement(xmname, tabName, lrf[i].dt, lrf[i].index);
            //}
            

            int datarowscnt = reportparams.alarmmess.Count == 0 ? 33 : 33;
            List<DataTable> dtlist = new List<DataTable>();

            //for (i = 0; i < datatablelist.Count; i++)
            //{

            //        //if (i == datatablelist.Count - 1) dtlist = new List<DataTable>();
            //        dtlist.Add(datatablelist[i]);
            //        ExcelHelper.GTsensordata_timespanReportTemplateCreateMPSC(xmname, pointnamelist, dtlist, origldatatable, reportname, origldatatable.Rows.Count, (pointnamelist.Count / datarowscnt + 1) * 54 * i, dttimespan, timeunitname, reportparams, datarowscnt);
            //        dtlist = new List<DataTable>();
            //}
            int xdatalen = 1;
            int xlen = 0;
            int chartidex = 1;
            //foreach (var holename in holenamelist)
            //{
                
                DataView dv = new DataView(origldatatable);
                dtlist = new List<DataTable>();
                dv.RowFilter = " holename = '" + holename + "' ";
                List<DataTable> datatablelist = Tool.DataTableHelper.ProcessGTInclinometerDataTableSplit(dv.ToTable());
                DataTable dttimespan = ExcelHelper.GTInclinometerTimeSpanThisDataTable(datatablelist[0], datatablelist[datatablelist.Count - 1]);
                List<string> pointnamelist = Tool.DataTableHelper.ProcessDataTableFilter(dv.ToTable(), "deepth");
                pointnamelist.Reverse();
                int j = 0;
                int colindex = 1;
                ExcelHelper.DataExcelWrite(xdatalen,colindex,holename);
                xdatalen++;
                j++;
                colindex++;
                foreach (DataTable dt in datatablelist)
                {
                    DataView dvdata = new DataView(dt);
                    ExcelHelper.DataExcelWrite(xdatalen-j,colindex,dvdata[0]["cyc"]);
                    
                    foreach (DataRowView drv in dvdata)
                    {
                        ExcelHelper.DataExcelWrite(xdatalen, 1, drv["a_ac_diff"]);
                        ExcelHelper.DataExcelWrite(xdatalen, colindex, drv["deepth"]);
                        j++;
                        xdatalen++;
                    }
                    
                    colindex ++;
                }
                int chartcont = 0;
                for (chartcont = 0; chartcont < (datatablelist.Count % 2 == 0 ? datatablelist.Count / 2 : datatablelist.Count / 2 + 1) * ((pointnamelist.Count % datarowscnt == 0 ? pointnamelist.Count / datarowscnt : pointnamelist.Count / datarowscnt) + 1); chartcont++, chartidex++)
                {
                    ExcelHelper.CreateAcChart(holename + "深部位移累计变化曲线", xdatalen - j + 1, xdatalen - 1, colindex, Convert.ToDouble(pointnamelist[pointnamelist.Count-1]), chartidex);
                }

                for (i = 0; i < datatablelist.Count; i++,xlen++)
                {
                    if (i % 2 == 1)
                    {
                        //if (i == datatablelist.Count - 1) dtlist = new List<DataTable>();
                        //ExcelHelper.CreateAcChart(holename + "深部位移累计变化曲线", xdatalen - j + 1, xdatalen - 1, colindex, Convert.ToDouble(pointnamelist[0]), pointnamelist.Count / datarowscnt);

                        dtlist.Add(datatablelist[i]);
                        ExcelHelper.GTinclinometer_timespanReportTemplateCreateMPSC(xmname, pointnamelist, dtlist, origldatatable, reportname, origldatatable.Rows.Count, (pointnamelist.Count / datarowscnt + 1) * 54 * (xlen / 2), dttimespan, timeunitname, reportparams, datarowscnt);
                        dtlist = new List<DataTable>();
                    }
                    else
                    {
                        dtlist.Add(datatablelist[i]);
                    }



                }
                ExcelHelper.GTinclinometer_timespanReportTemplateCreateMPSC(xmname, pointnamelist, dtlist, origldatatable, reportname, origldatatable.Rows.Count, (pointnamelist.Count / datarowscnt + 1) * 54 * (xlen / 2), dttimespan, timeunitname, reportparams, datarowscnt);
                
            //}

        }
    }
}
