﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;
using Tool.DATAREPORT.Pub;
using Tool.DATAREPORT.TotalStation;


namespace Tool
{
    public partial class TotalStationReportHelper
    {



        public void GTinclinometer_timespanChartDataFillMPSC(string xmname, string tabHead, DataTable originaldt, out List<string> pointnamelist)
        {
            int i = 1;
            
            //int t = 0;
            pointnamelist = Tool.DataTableHelper.ProcessDataTableFilter(originaldt,"holename");
            string sheetName = "";
            string[] tabName = tabHead.Split(',');
            int reportNo = new Random().Next(9999);
            int pageindexlen = 0;
            int datastartlen = 0;
            while (0 < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {

                if (vsion == 0) hssfworkbook.RemoveSheetAt(0); else xssfworkbook.RemoveSheetAt(0);
                
            }
            for (i = 1; i < pointnamelist.Count + 1; i++)
            {
                ISheet sheet1 = vsion == 0 ? hssfworkbook.CreateSheet(pointnamelist[i - 1]) : xssfworkbook.CreateSheet(pointnamelist[i - 1]);
                ISheet sheet = vsion == 0 ? hssfworkbook.CreateSheet(pointnamelist[i - 1] + "曲线数据") : xssfworkbook.CreateSheet(pointnamelist[i - 1] + "曲线数据");
                sheet1.CreateRow(0).CreateCell(0).SetCellValue(1);
            }


           
            //foreach (DataTable dt in ldt)
            //{


            //    DataView dv = new DataView(dt);
            //    sheetName = string.Format("第{0}期", dt.Rows[0].ItemArray[1]);

            //    sheet1.PrintSetup.Landscape = false;
            //    sheet1.Header.Left = "广州铁路科技开发有限公司";
            //    sheet1.Header.Right = DateTime.Now.ToString();
            //    sheet1.Footer.Center = "第&p页";
            //    sheet1.PrintSetup.PaperSize = 9;
            //    sheet1.FitToPage = false;
            //    sheet1.PrintSetup.Scale = 100;
            //    sheet1.HorizontallyCenter = true;
            //    int k = 0;
            //    //IRow row = sheet1.CreateRow(len);
            //    //pageindexlen = len + 7;
            //    lrf.Add(new ReportFillEnviroment(dt, len));
            //    ////sheet.CreateRow(0).CreateCell(0).SetCellValue(1);
            //    ////ReportDataFillMPSC_GT(xmname, dt, sheet, sheetName, len, datastartlen);


                
            //    //len += (dt.Rows.Count % 40 == 0 ? dt.Rows.Count / 40 : dt.Rows.Count / 40 + 1) * 54;
            //    //datastartlen += dt.Rows.Count + 1;
            //    //int pageIndex = len % 54 == 0 ? len / 54 : len / 54 + 1;
            //    ////len = len % 38 == 0 ? (len / 38) * 38 + 70 : (len / 35) * 35 + 105;
            //    //lce.Add(new ChartCreateEnviroment(datastartlen + 1 - dt.Rows.Count, datastartlen, sheetName, pageIndex));
            //    //len += (dt.Rows.Count % 31 == 0 ? dt.Rows.Count / 31 : dt.Rows.Count / 31 + 1) * 40;
            //    //tring tabHead, List<ReportFillEnviroment> lrf, strin//int pageIndex = len % 40 == 0 ? len / 40 : len / 40 + 1;
            //    //aFill = new TotalStationDataFill { ExcelHelper = ExcelHelper };
            //    //onDataFill.DataFillMPSC_GT_WithoutSettlement(xmname, tabHead, lrf, reportname);
            //}
        }

       


        public void GTinclinometer_timespanReportDataFillMPSC(string xmname,string holename ,string tabHead, string reportname, DataTable origldatatable, string timeunitname, Tool.DATAREPORT.TotalStation.MPSC.GT.reportparams reportparams)
        {
            TotalStationDataFill totalStationDataFill = new TotalStationDataFill { ExcelHelper = ExcelHelper };
            totalStationDataFill.GTinclinometer_timespanFillMPSC(xmname,holename ,tabHead, reportname, origldatatable, timeunitname, reportparams);
        }
        public void GTinclinometer_timespanMainMPSC(string xmname, string tabHead, DataTable originaldt, string xlpath, string timeunitname, Tool.DATAREPORT.TotalStation.MPSC.GT.reportparams reportparams)
        {
            List<string> pointnamelist = null;
            GTinclinometer_timespanChartDataFillMPSC(xmname, tabHead, originaldt, out pointnamelist);
            WriteToFile(xlpath);
            ExcelHelper.workbookpath = xlpath;
            ExcelHelper.ExcelInit();
            int i = 0;
            for (i = 1; i < pointnamelist.Count*2;i+=2 )
            {
                //if (i == 1) continue;
                ExcelHelper.SheetInit(i, i+1);
                GTinclinometer_timespanReportDataFillMPSC(xmname, pointnamelist[i/2], tabHead, Path.GetFileNameWithoutExtension(xlpath), originaldt, timeunitname, reportparams);
                
            }
            ExcelHelper.save();

           
        }
        

    }
}