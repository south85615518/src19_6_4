﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;


namespace Tool.GTInclinometer
{
    public partial class GTXLSHelper
    {


   


        ///// <summary>
        ///// 多点多周期报表
        ///// </summary>
        ///// <param name="tabHead"></param>
        ///// <param name="dt"></param>
        ///// <param name="tltle"></param>
        ///// <param name="sheetName"></param>
        //public static void GetDataFromXls(string xlspath, ISheet sheet1, DataTable dt, out string mssg)
        //{


        //    int i = 7;
        //    mssg = "";
        //    try
        //    {
        //        //if ((sheet1.GetRow(3) == null) || sheet1.GetRow(2) == null || sheet1.GetRow(4) == null) { mssg = "\r\n" + string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName); return; }
        //        //string xmname = sheet1.GetRow(3).GetCell(1).StringCellValue;// ExcelHelper.xSheet.Cells[4, 2].Value;

        //        //int cyc = 0;
        //        //if (sheet1.GetRow(2).GetCell(0).CellType == CellType.STRING)
        //        //{
        //        //    string cycstr = sheet1.GetRow(2).GetCell(0).StringCellValue;
        //        //    cyc = Convert.ToInt32(cycstr.Substring(cycstr.IndexOf("第") + 1, cycstr.LastIndexOf("次") - cycstr.IndexOf("第") - 1));
        //        //}
        //        //else
        //        //    cyc = Convert.ToInt32(sheet1.GetRow(2).GetCell(0).NumericCellValue);
        //        //DateTime time = Convert.ToDateTime(sheet1.GetRow(4).GetCell(5).DateCellValue);
        //        while (i < 1000)
        //        {

        //            try
        //            {
        //                // if (sheet1.GetRow(i) == null || sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("监测小结") != -1) return;

        //                DataRow dr = dt.NewRow();
        //                dr["point_name"] = sheet1.GetRow(60).GetCell(2).StringCellValue;
        //                dr["xmname"] = "新白广城际铁路下穿京广高铁施工监测";
        //                dr["time"] = DateTime.Now;
        //                dr["this_dn"] = sheet1.GetRow(45).GetCell(3);
        //                dr["this_de"] = sheet1.GetRow(46).GetCell(3);
        //                dr["this_dz"] = sheet1.GetRow(47).GetCell(3);
        //                dr["ac_dn"] = sheet1.GetRow(48).GetCell(3);
        //                //dr["ac_de"] = sheet1.GetRow(i).GetCell(5).NumericCellValue;
        //                //dr["ac_dz"] = sheet1.GetRow(i).GetCell(6).NumericCellValue;
        //                //dr["cyc"] = cyc;
        //                //dr["siblingpointname"] = SiblingPointNameGet(sheet1, sheet1.GetRow(i).GetCell(0).StringCellValue);

        //                dt.Rows.Add(dr);
        //                break;

        //            }
        //            catch (Exception ex)
        //            {
        //                mssg += "\r\n" + string.Format("文件{0}工作表{1}第{2}行数据读取出错,错误信息:" + ex.Message, xlspath, sheet1.SheetName, i);
        //            }
        //            i++;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string a = "";
        //    }
        //}
        //}
        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void GetgtInclinometerDataFromXls(string xlspath, ISheet sheet1, DataTable dt, out string mssg)
        {


            int i = 0;
            mssg = "";
            try
            {
                if ((sheet1.GetRow(0) == null) || sheet1.GetRow(1) == null ||sheet1.GetRow(2) == null || sheet1.GetRow(3) == null || sheet1.GetRow(4) == null) { mssg = "\r\n" + string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName); return; }

                string pointname =Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(0).GetCell(2)).ToString();
                //string datettimestr = string.Format("{0}", sheet1.GetRow(1).GetCell(2).DateCellValue.Date.Add(sheet1.GetRow(1).GetCell(3).DateCellValue.TimeOfDay));
                
                i = 1;
                DateTime time = new DateTime();
                while (i < 10000)
                {

                    try
                    {
                        IRow row= sheet1.GetRow(i);
                        if (sheet1.GetRow(i) == null ) break;
                        if ((sheet1.GetRow(i).GetCell(0) != null && sheet1.GetRow(i).GetCell(1) != null )&&( sheet1.GetRow(i).GetCell(2) == null || sheet1.GetRow(i).GetCell(3) == null || sheet1.GetRow(i).GetCell(4) == null || sheet1.GetRow(i).GetCell(5) == null || sheet1.GetRow(i).GetCell(6) == null))
                        {
                            string timestr =Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(1));
                            time = Convert.ToDateTime(timestr);
                            i++;
                            continue;
                        }

                        DataRow dr = dt.NewRow();
                        dr["point_name"] = pointname;
                        dr["time"] = time;
                        dr["deepth"] =Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(0));
                        dr["a_nagtive"] = Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(1));
                        dr["a_positive"] = Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(2));
                        dr["b_nagtive"] = Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(3));
                        dr["b_positive"] = Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(4));
                        dr["a_ac"] = Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(5));
                        dr["b_ac"] = Tool.GTXLSHelper.ExcelValueFormatCompatible(sheet1.GetRow(i).GetCell(6));
                        dt.Rows.Add(dr);
                         
                    }
                    catch (Exception ex)
                    {
                        mssg += "\r\n" + string.Format("文件{0}工作表{1}第{2}行数据读取出错,错误信息:" + ex.Message, xlspath, sheet1.SheetName, i);
                    }
                    i++;
                    if (i == 1212) { 
                        string a = "";
                    }
                }
            }
            catch (Exception ex)
            {
                string a = "";
            }







        }


        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void DeletegtInclinometerDataFromXls(string xlspath, ISheet sheet1,out DeleteDataFromXlsOutInfoModel model , out string mssg)
        {
            model = new DeleteDataFromXlsOutInfoModel();
            int i = 19;
            mssg = "";
            try
            {
                if ((sheet1.GetRow(1) == null) || sheet1.GetRow(2) == null || sheet1.GetRow(3) == null || sheet1.GetRow(4) == null || sheet1.GetRow(5) == null) { mssg = "\r\n" + string.Format("文件{0}工作表{1}读取失败！", xlspath, sheet1.SheetName); return; }

                string pointname = sheet1.GetRow(4).GetCell(2).StringCellValue;
                DateTime time = Convert.ToDateTime(sheet1.GetRow(4).GetCell(5).DateCellValue);
                model.pointname = pointname;
                model.time = time;
            }
            catch (Exception ex)
            {
                string a = "";
            }

        }
    

   


  


        public static bool GtInclinometerDataFromXlsCheck(ISheet sheet1)
        {

            int resultPass = 0;
            int i = 0;
            string cellvalue = "";
            CellType type = new CellType();
            while (i < 1000)
            {
                try
                {
                    if (sheet1.GetRow(2) == null || sheet1.GetRow(2).GetCell(i) == null) break;
                    type = sheet1.GetRow(2).GetCell(i).CellType;
                    if (type == CellType.STRING)
                    {
                        cellvalue = sheet1.GetRow(2).GetCell(i).StringCellValue;
                        //点名

                        //传感器编号
                        if (sheet1.GetRow(2).GetCell(i).StringCellValue.IndexOf("深度") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        //传感器类型
                        if (sheet1.GetRow(2).GetCell(i).StringCellValue.IndexOf("A0数据") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        //传感器类型
                        if (sheet1.GetRow(2).GetCell(i).StringCellValue.IndexOf("A180数据") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        //传感器类型
                        if (sheet1.GetRow(2).GetCell(i).StringCellValue.IndexOf("B0数据") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        //传感器类型
                        if (sheet1.GetRow(2).GetCell(i).StringCellValue.IndexOf("B180数据") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        //传感器类型
                        if (sheet1.GetRow(2).GetCell(i).StringCellValue.IndexOf("A轴从底部累加数据") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        if (sheet1.GetRow(2).GetCell(i).StringCellValue.IndexOf("B轴从底部累加数据") != -1)
                        {

                            i++;
                            resultPass++;
                            continue;
                        }
                        else
                        {
                            i++;
                            continue;
                        }

                    }

                    i++;
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("测斜数据excel文件导入出错,出错信息：" + ex.Message);
                    i++;

                }


            }
            return resultPass == 7 ? true : true;




        }





        /// <summary>
        /// excel数据文件导入
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessgtInclinometerXlsDataTableImport(string xlspath,out DataTable dt,out string mssg)
        {

            mssg = "";
            bool result = true;
            int i = 0;
            dt = new DataTable();
            dt.Columns.Add("point_name");
            dt.Columns.Add("deepth");
            dt.Columns.Add("xmno");
            dt.Columns.Add("time");
            dt.Columns.Add("a_nagtive");
            dt.Columns.Add("b_nagtive");
            dt.Columns.Add("a_ac");
            dt.Columns.Add("b_ac");
            ISheet sheet1 = null;
            while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {

                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                string sheetmssg = "";
                if (DataFromXlsCheck(sheet1))
                    GetDataFromXls(xlspath, sheet1, dt, out sheetmssg);
                //else if()
                //{

                //}
                else 
                    sheetmssg = string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName);
                if (sheetmssg!="")
                mssg += "\r\n"+sheetmssg;
                i++;
            }
            return true;
        }

      
        //调用曲线填充函数
        //public void SeriesCreate(HttpContext context)
        //{
        //    //InitializeWorkbook("D:\\ReportExcel.xls");
        //    List<serie> ls = (List<serie>)context.Session["series"];
        //    //分拣出本次和累计
        //    List<serie> lsThis = new List<serie>();
        //    List<serie> lsAc = new List<serie>();
        //    foreach (serie s in ls)
        //    {
        //        if (s.Stype.IndexOf("累计") != -1)
        //        {
        //            lsAc.Add(s);
        //        }
        //        else
        //        {
        //            lsThis.Add(s);
        //        }

        //    }
        //    if (lsThis.Count > 0)
        //    {
        //        ISheet sheet = xssfworkbook.CreateSheet("本次变化曲线生成数据");
        //        //本次曲线
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsThis);
        //        List<string> legends = LegendFill(sheet, lsThis);
        //        SerieDataPointFill(sheet, lsThis, ldt, legends);
        //    }
        //    if (lsAc.Count > 0)
        //    {
        //        //累计曲线
        //        ISheet sheet = xssfworkbook.CreateSheet("累计变化曲线生成数据");
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsAc);
        //        List<string> legends = LegendFill(sheet, lsAc);
        //        SerieDataPointFill(sheet, lsAc, ldt, legends);
        //    }
        //}

    }
}