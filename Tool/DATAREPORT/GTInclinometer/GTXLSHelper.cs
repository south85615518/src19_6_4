﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;


namespace Tool.GTInclinometer
{
    public partial class GTXLSHelper
    {

        public static XSSFWorkbook xssfworkbook;
        public static HSSFWorkbook hssfworkbook;
        public static int vsion = 0;//版本标志 
        public static void WriteToFile(string ldpath)
        {
            //Write the stream data of workbook to the root directory
            FileStream file = new FileStream(ldpath, FileMode.Create, FileAccess.ReadWrite);
            if (hssfworkbook != null)
            {
                hssfworkbook.Write(file);
            }
            else 
            {
            xssfworkbook.Write(file);
            }
            file.Close();
            
        }
       
        public static void HInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added. 
            Exception e = null;
            FileStream file = null; 
            try
            {
                file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
                hssfworkbook = new HSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                hssfworkbook.DocumentSummaryInformation = dsi;
                vsion = 0;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                hssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                file.Close();
                if (e != null)
                    throw e;
            }
        }
        
        public static void  XInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            Exception e = null;
            FileStream file = null; 
            try
            {
                file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
                xssfworkbook = new XSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                //xssfworkbook .DocumentSummaryInformation = dsi;
                vsion = 1;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                //xssfworkbook.SummaryInformation = si;
            }
            catch (Exception  ex)
            {
                e =ex;
            }
            finally
            {
                if(file!=null)
                file.Close();
                if (e != null)
                {
                    string a = "";
                    //throw e;
                }
            }
        }
        public static void PublicCompatibleInitializeWorkbook(string tppath)
        {
            try
            {
               HInitializeWorkbook(tppath);
            }
            catch(Exception ex)
            {
                XInitializeWorkbook(tppath);
            }
        }
   

        public static IRow CreateRow(int len, ISheet sheet,int colCnt)
        {
            IRow row = sheet.CreateRow(len);
            for (int i = 0; i < colCnt; i++)
            {
                CellFontSet(row.CreateCell(i), "仿宋", 200, 16, true);
                CellBorderSet(row.CreateCell(i), GetBorderSetStyle(true, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                
            }
            return row;
        }


        ///// <summary>
        ///// 多点多周期报表
        ///// </summary>
        ///// <param name="tabHead"></param>
        ///// <param name="dt"></param>
        ///// <param name="tltle"></param>
        ///// <param name="sheetName"></param>
        //public static void GetDataFromXls(string xlspath, ISheet sheet1, DataTable dt, out string mssg)
        //{


        //    int i = 7;
        //    mssg = "";
        //    try
        //    {
        //        //if ((sheet1.GetRow(3) == null) || sheet1.GetRow(2) == null || sheet1.GetRow(4) == null) { mssg = "\r\n" + string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName); return; }
        //        //string xmname = sheet1.GetRow(3).GetCell(1).StringCellValue;// ExcelHelper.xSheet.Cells[4, 2].Value;

        //        //int cyc = 0;
        //        //if (sheet1.GetRow(2).GetCell(0).CellType == CellType.STRING)
        //        //{
        //        //    string cycstr = sheet1.GetRow(2).GetCell(0).StringCellValue;
        //        //    cyc = Convert.ToInt32(cycstr.Substring(cycstr.IndexOf("第") + 1, cycstr.LastIndexOf("次") - cycstr.IndexOf("第") - 1));
        //        //}
        //        //else
        //        //    cyc = Convert.ToInt32(sheet1.GetRow(2).GetCell(0).NumericCellValue);
        //        //DateTime time = Convert.ToDateTime(sheet1.GetRow(4).GetCell(5).DateCellValue);
        //        while (i < 1000)
        //        {

        //            try
        //            {
        //                // if (sheet1.GetRow(i) == null || sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("监测小结") != -1) return;

        //                DataRow dr = dt.NewRow();
        //                dr["point_name"] = sheet1.GetRow(60).GetCell(2).StringCellValue;
        //                dr["xmname"] = "新白广城际铁路下穿京广高铁施工监测";
        //                dr["time"] = DateTime.Now;
        //                dr["this_dn"] = sheet1.GetRow(45).GetCell(3);
        //                dr["this_de"] = sheet1.GetRow(46).GetCell(3);
        //                dr["this_dz"] = sheet1.GetRow(47).GetCell(3);
        //                dr["ac_dn"] = sheet1.GetRow(48).GetCell(3);
        //                //dr["ac_de"] = sheet1.GetRow(i).GetCell(5).NumericCellValue;
        //                //dr["ac_dz"] = sheet1.GetRow(i).GetCell(6).NumericCellValue;
        //                //dr["cyc"] = cyc;
        //                //dr["siblingpointname"] = SiblingPointNameGet(sheet1, sheet1.GetRow(i).GetCell(0).StringCellValue);

        //                dt.Rows.Add(dr);
        //                break;

        //            }
        //            catch (Exception ex)
        //            {
        //                mssg += "\r\n" + string.Format("文件{0}工作表{1}第{2}行数据读取出错,错误信息:" + ex.Message, xlspath, sheet1.SheetName, i);
        //            }
        //            i++;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string a = "";
        //    }
        //}
        //}
        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void GetDataFromXls(string xlspath, ISheet sheet1, DataTable dt, out string mssg)
        {


            int i = 19;
            mssg = "";
            try
            {
                if ((sheet1.GetRow(0) == null) || sheet1.GetRow(1) == null ||sheet1.GetRow(2) == null || sheet1.GetRow(3) == null || sheet1.GetRow(4) == null) { mssg = "\r\n" + string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName); return; }

                string pointname = sheet1.GetRow(4).GetCell(1).StringCellValue;
                DateTime time = Convert.ToDateTime(sheet1.GetRow(7).GetCell(1).DateCellValue);
                while (i < 1000)
                {

                    try
                    {
                        if (sheet1.GetRow(0) == null || sheet1.GetRow(1) == null ||sheet1.GetRow(2) == null || sheet1.GetRow(3) == null || sheet1.GetRow(4) == null) return;

                        DataRow dr = dt.NewRow();
                        dr["point_name"] = pointname;
                        dr["time"] = time;
                        dr["deepth"] = sheet1.GetRow(i).GetCell(0).NumericCellValue;
                        dr["a_nagtive"] = sheet1.GetRow(i).GetCell(1).NumericCellValue;
                        dr["a_positive"] = sheet1.GetRow(i).GetCell(2).NumericCellValue;
                        dr["b_nagtive"] = sheet1.GetRow(i).GetCell(3).NumericCellValue;
                        dr["b_positive"] = sheet1.GetRow(i).GetCell(4).NumericCellValue;
                        dt.Rows.Add(dr);

                    }
                    catch (Exception ex)
                    {
                        mssg += "\r\n" + string.Format("文件{0}工作表{1}第{2}行数据读取出错,错误信息:" + ex.Message, xlspath, sheet1.SheetName, i);
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                string a = "";
            }







        }


        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void DeleteDataFromXls(string xlspath, ISheet sheet1,out DeleteDataFromXlsOutInfoModel model , out string mssg)
        {
            model = new DeleteDataFromXlsOutInfoModel();
            int i = 19;
            mssg = "";
            try
            {
                if ((sheet1.GetRow(1) == null) || sheet1.GetRow(2) == null || sheet1.GetRow(3) == null || sheet1.GetRow(4) == null || sheet1.GetRow(5) == null) { mssg = "\r\n" + string.Format("文件{0}工作表{1}读取失败！", xlspath, sheet1.SheetName); return; }

                string pointname = sheet1.GetRow(4).GetCell(2).StringCellValue;
                DateTime time = Convert.ToDateTime(sheet1.GetRow(4).GetCell(5).DateCellValue);
                model.pointname = pointname;
                model.time = time;
            }
            catch (Exception ex)
            {
                string a = "";
            }

        }
        public class DeleteDataFromXlsOutInfoModel
        {
            public int xmno{ get;set;}
            public string pointname { get; set; }
            public DateTime time{get;set;}
            public DeleteDataFromXlsOutInfoModel(int xmno, DateTime time,string pointname)
            {
                this.xmno = xmno;
                this.time = time;
                this.pointname = pointname;
            }
            public DeleteDataFromXlsOutInfoModel()
            {
                
            }
        }


        public static  string SiblingPointNameGet(ISheet sheet1,string pointname)
        {
            int i = 7;
            while (sheet1.GetRow(i).GetCell(7) != null && sheet1.GetRow(i).GetCell(7).StringCellValue != "" && sheet1.GetRow(i).GetCell(7).StringCellValue != null)
            {
                string siblingpointstr = sheet1.GetRow(i).GetCell(7).StringCellValue;
                if (siblingpointstr.IndexOf(string.Format("{0}：", pointname)) != -1)
                {
                    return sheet1.GetRow(i).GetCell(7).StringCellValue.Split('：')[1];
                }
                i++;
            }
            return "";
        }


        public static bool DataFromXlsCheck(ISheet sheet1)
        {
            
            int resultPass = 0;
            int i = 0;
            string cellvalue = "";
            CellType type = new CellType();
            while (i<1000)
            {
                try
                {
                    if (sheet1.GetRow(i) == null|| sheet1.GetRow(i).GetCell(0) == null) break;
                    type = sheet1.GetRow(i).GetCell(0).CellType;
                    if (type == CellType.STRING)
                    {
                        cellvalue = sheet1.GetRow(i).GetCell(0).StringCellValue;
                        //点名
                       
                        //上次监测时间
                        if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("RST Digital Inclinometer Data") != -1)
                        {
                            
                            i++;
                            resultPass++;
                            continue;
                        }
                        //本次监测时间
                        if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("File Type") != -1)
                        {
                           
                            i++;
                            resultPass++;
                            continue;
                        }
                        else
                        {
                            i++;
                            continue;
                        }
                       
                    }
                    
                    i++;
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("广铁测斜数据excel文件导入出错,出错信息：" + ex.Message);
                    i++;
                    
                }
                

            }
            return resultPass == 2 ? true : false;

            


        }








        /// <summary>
        /// excel数据文件导入
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessXlsDataTableImport(string xlspath,out DataTable dt,out string mssg)
        {

            mssg = "";
            bool result = true;
            int i = 0;
            dt = new DataTable();
            dt.Columns.Add("point_name");
            dt.Columns.Add("deepth");
            dt.Columns.Add("xmno");
            dt.Columns.Add("time",typeof(DateTime));
            dt.Columns.Add("a_nagtive");
            dt.Columns.Add("b_nagtive");
            dt.Columns.Add("a_positive");
            dt.Columns.Add("b_positive");
            dt.Columns.Add("a_ac");
            dt.Columns.Add("b_ac");
            ISheet sheet1 = null;
            while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {

                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                string sheetmssg = "";
                if (DataFromXlsCheck(sheet1))
                    GetDataFromXls(xlspath, sheet1, dt, out sheetmssg);
                else if (GtInclinometerDataFromXlsCheck(sheet1))
                    GetgtInclinometerDataFromXls(xlspath, sheet1, dt, out sheetmssg);
                else
                    sheetmssg = string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName);
                if (sheetmssg!="")
                mssg += "\r\n"+sheetmssg;
                i++;
            }
            return true;
        }

        /// <summary>
        /// excel数据文件导入
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessXlsCycDataDelete(string xlspath,out List<DeleteDataFromXlsOutInfoModel>  modellist ,  out string mssg)
        {

            mssg = "";
            bool result = true;
            int i = 0;
            modellist = new List<DeleteDataFromXlsOutInfoModel>();
            ISheet sheet1 = null;
            while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {

                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                string sheetmssg = "";
                DeleteDataFromXlsOutInfoModel model = new DeleteDataFromXlsOutInfoModel();
                if (DataFromXlsCheck(sheet1))
                {
                    DeleteDataFromXls(xlspath, sheet1, out model, out sheetmssg);
                    modellist.Add(model);
                }
                else
                    sheetmssg = string.Format("文件{0}工作表{1}导入失败！", xlspath, sheet1.SheetName);
                if (sheetmssg != "")
                    mssg += "\r\n" + sheetmssg;
                i++;
            }
            return true;
        }
        /// <summary>
        /// excel文件格式检查
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessXlsDataImportCheck(string upzipfilePath, out string mssg)
        {


            bool result = true;
            mssg = "";
            int i = 0;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(upzipfilePath);

            foreach (string filename in filenamelist)
            {
                string tmpname = "";
                if (Path.GetExtension(filename) == ".csv") { tmpname = Tool.EXCEL.ExcelHelperHandle.CSVSaveasXLS(filename); }
                if (Path.GetExtension(tmpname) == ".xls" || Path.GetExtension(tmpname) == ".xlsx")
                {
                    PublicCompatibleInitializeWorkbook(tmpname);
                    i = 0;
                    ISheet sheet1 = null;
                    while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets)) 
                    {
                        sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                        if (DataFromXlsCheck(sheet1))
                        {
                            mssg += string.Format("{0}工作表{1}导入成功 ", Path.GetFileName(tmpname) + "、\r\n{0}", sheet1.SheetName);
                            result = true;
                        }
                        else
                            result = true;
                        i++;
                    }
                    
                   
                }
                else
                {
                    mssg += string.Format("{0}不是EXCEL可执行文件。 ", Path.GetFileName(tmpname) + "、\r\n{0}");
                    result = false;
                }

            }
            mssg = mssg.Replace("、\r\n{0}", "");
            return result;

        }
        public static bool XlsFileDataImportCheck(string filename,out string mssg)
        {
            string tmpname = "";
            if (Path.GetExtension(filename) == ".csv") { tmpname = Tool.EXCEL.ExcelHelperHandle.CSVSaveasXLS(filename); }
            else
                tmpname = filename;
            PublicCompatibleInitializeWorkbook(tmpname);
            mssg = "";
            int i = 0;
            ISheet sheet1 = null;
            while (i < (vsion == 0 ? hssfworkbook.NumberOfSheets : xssfworkbook.NumberOfSheets))
            {
                sheet1 = vsion == 0 ? hssfworkbook.GetSheetAt(i) : xssfworkbook.GetSheetAt(i);
                if (!DataFromXlsCheck(sheet1))
                {
                    mssg += string.Format("{0}工作表{1}导入失败 ", Path.GetFileName(tmpname) + "、\r\n{0}", sheet1.SheetName);
                    
                }
                i++;
            }
            return true;
        }

        //将单元格设置为有边框的
        public static void CellBorderSolid(ICell cell, ICellStyle style, bool wrap)
        {
            //ICellStyle style = xssfworkbook.CreateCellStyle();
            style.WrapText = wrap;
            cell.CellStyle = style;

        }


        //创建表格样式
        public static ICellStyle GetICellStyle()
        {
            ICellStyle style = null;
            if(xssfworkbook!=null)
            style = xssfworkbook.CreateCellStyle();
            else
            style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            style.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            return style;

        }
        //设置边框样式
        public static ICellStyle GetBorderSetStyle(bool solid, HorizontalAlignment h, VerticalAlignment v)
        {
            ICellStyle style = null;
            if (xssfworkbook != null)
                style = xssfworkbook.CreateCellStyle();
            else
                style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderLeft = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderRight = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderTop = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.Alignment = h;
            style.VerticalAlignment = v;
            return style;
        }

        //设置单元格的边框
        public static void CellBorderSet(ICell cell, ICellStyle style, bool wrap)
        {

            try
            {
                style.WrapText = wrap;
                cell.CellStyle = style;
            }
            catch (Exception ex)
            {
                string a = "";
            }
           
        }
        //设置字体样式
        public static void CellFontSet(ICell cell, string fontName,
            short fontWeight, short fontHeight, bool italic)
        {
            try
            {
                ICellStyle style = null;
                IFont font = null;
                if (xssfworkbook != null)
                {
                    style = xssfworkbook.CreateCellStyle();
                    font = xssfworkbook.CreateFont();
                }
                else
                {
                    style = hssfworkbook.CreateCellStyle();
                    font = hssfworkbook.CreateFont();
                }
                
                font.FontName = fontName;
                font.Boldweight = fontWeight;
                font.FontHeightInPoints = fontHeight;
                font.IsItalic = italic;
                
                style.SetFont(font);
                cell.CellStyle = style;
            }
            catch (Exception ex)
            {
                string a = "";
            }
        }
        //日期格式转换
        public static string DateFormatConverter(string dateStr)
        {
            if (dateStr == null || dateStr.Trim() == "") return "";
            char[] splits = { '-', ' ', '/', ':' };
            string[] dateStrs = dateStr.Split(splits);
            return dateStrs[0].Substring(dateStrs[0].IndexOf("20") + 2) + "年" + dateStrs[1] + "月" + dateStrs[2] + "日";
        }
        //填充曲线表
        /// <summary>
        /// 对不同监测项目的日期进行排序,生成X轴
        /// </summary>
        /// <param name="dt"></param>
        public static List<DateTime> GetDateTimeFromSeriesInDiffJcxm(ISheet sheet, List<serie> series)
        {
            List<DateTime> ldt = new List<DateTime>();
            List<string> lType = new List<string>();
            int i = 0, j = 0;
            for (i = 0; i < series.Count; i++)
            {

                for (j = 0; j < series[i].Pts.Length; j++)
                {
                    ldt.Add(series[i].Pts[j].Dt);
                }

            }
            ldt = ldt.Distinct().ToList();
            ldt.Sort();
            IRow row = sheet.CreateRow(0);
            for (i = 1; i < ldt.Count + 1; i++)
            {
                row.CreateCell(i).SetCellValue(ldt[i - 1]);
            }
            return ldt;
        }
        /// <summary>
        /// 填充图例
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        /// <returns></returns>
        public static List<string> LegendFill(ISheet sheet, List<serie> series)
        {
            List<string> legends = new List<string>();
            int i = 0;
            for (i = 1; i < series.Count + 1; i++)
            {
                IRow row = sheet.CreateRow(i);
                row.CreateCell(0).SetCellValue(series[i - 1].Name);
                legends.Add(series[i - 1].Name);
            }
            return legends;
        }
        /// <summary>
        /// 安装曲线数据点
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        public static void SerieDataPointFill(ISheet sheet, List<serie> series, List<DateTime> ldt, List<string> legends)
        {

            int y = 0, t = 0, posy = 0, posx = 0;
            for (y = 0; y < series.Count; y++)
            {
                IRow row = sheet.GetRow(y);
                posy = 1 + legends.IndexOf(series[y].Name);
                for (t = 0; t < series[y].Pts.Length; t++)
                {
                    posx = 1 + ldt.IndexOf(series[y].Pts[t].Dt);
                    row.CreateCell(posx).SetCellValue(series[y].Pts[t].Yvalue1);
                }
            }
        }
        //调用曲线填充函数
        //public void SeriesCreate(HttpContext context)
        //{
        //    //InitializeWorkbook("D:\\ReportExcel.xls");
        //    List<serie> ls = (List<serie>)context.Session["series"];
        //    //分拣出本次和累计
        //    List<serie> lsThis = new List<serie>();
        //    List<serie> lsAc = new List<serie>();
        //    foreach (serie s in ls)
        //    {
        //        if (s.Stype.IndexOf("累计") != -1)
        //        {
        //            lsAc.Add(s);
        //        }
        //        else
        //        {
        //            lsThis.Add(s);
        //        }

        //    }
        //    if (lsThis.Count > 0)
        //    {
        //        ISheet sheet = xssfworkbook.CreateSheet("本次变化曲线生成数据");
        //        //本次曲线
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsThis);
        //        List<string> legends = LegendFill(sheet, lsThis);
        //        SerieDataPointFill(sheet, lsThis, ldt, legends);
        //    }
        //    if (lsAc.Count > 0)
        //    {
        //        //累计曲线
        //        ISheet sheet = xssfworkbook.CreateSheet("累计变化曲线生成数据");
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsAc);
        //        List<string> legends = LegendFill(sheet, lsAc);
        //        SerieDataPointFill(sheet, lsAc, ldt, legends);
        //    }
        //}

    }
}