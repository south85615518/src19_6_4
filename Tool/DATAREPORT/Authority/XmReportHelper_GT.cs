﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;
using Tool.DATAREPORT.Pub;
using Tool.DATAREPORT.TotalStation;


namespace Tool
{
    public partial class TotalStationReportHelper
    {



        public void Xm_GT_Report_Init(string unitname,DataTable dt)
        {
            ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("Sheet1") : xssfworkbook.GetSheet("Sheet1");
            if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
            sheet1.Header.Left = "广州铁路科技开发有限公司";
            sheet1.Header.Right = DateTime.Now.ToString();
            sheet1.Footer.Center = "第&p页";
            sheet1.PrintSetup.PaperSize = 9;
            sheet1.FitToPage = false;
            sheet1.PrintSetup.Scale = 100;
            sheet1.HorizontallyCenter = true;
     
        }
        public void Xm_GT_Report(string unitname, DataTable dt)
        {
            TotalStationDataFill totalStationDataFill = new TotalStationDataFill { ExcelHelper = ExcelHelper };
            totalStationDataFill.Xm_GT_Report(unitname,dt);
        }
        public void MainXm_GT_Report(string unitname,DataTable dt, string xlpath)
        {
            Xm_GT_Report_Init(unitname, dt);
            WriteToFile(xlpath);
            ExcelHelper.workbookpath = xlpath;
            ExcelHelper.ExcelInit();
            ExcelHelper.SheetInit("Sheet1");
            //ChartDataBindMPSC(lce);
            //string filename = Path.GetFileNameWithoutExtension(xlpath);
            Xm_GT_Report(unitname,dt);
            ExcelHelper.save();
        }
        

    }
}