﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {

        public void XmTemplateCreate_GT(string unitname, DataTable dt, int datarowscnt)
        {


            int cnt = 0;
            int rangerowstart = 1;
            int widthcol = 12;
            //tabName = new string[7] { "测点", "纵向位移(北)", "横向位移(东)", "沉降", "纵向位移(北)", "横向位移(东)", "沉降" };
            DateTime time = DateTime.Now;//Convert.ToDateTime(dt.Rows[0].ItemArray[11]);
            int diffsettlememntcount = 0;
            List<double> difflist = new List<double>();
            int len = 1;
            //int datarowcnt = reportparams.alarmmess.Count == 0 ? 37 : 36;
            fontsize = 9;
            DataView dv = new DataView(dt);
            int i = 0;
            int spacelen = 0;
            DateTime reportprinttime = DateTime.Now;
            for (cnt = 0; datarowscnt * cnt < dv.Count*2; cnt++)
            {
                rangerowstart = len;
                //添加标题
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                this.ExcelMergedRegion(len, 1, len+1, widthcol-3);

                this.ExcelWriteWrapText(len, 1, string.Format("{0}监测项目情况表", unitname));
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
                xSheet.Cells[len, 1].Font.Size = 18;
                xSheet.Cells[len, 1].Font.Bold = true;
                this.ExcelMergedRegion(len, widthcol - 2, len + 1, widthcol);

                this.ExcelWriteWrapText(len, widthcol - 2, string.Format("打印时间:{0}", reportprinttime));
                xSheet.Cells[len, widthcol - 2].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
                xSheet.Cells[len, widthcol - 2].Font.Size = 8;
                xSheet.Cells[len, widthcol - 2].Font.Bold = true;
                len+=2;
                xSheet.Rows[len].Font.Bold = true;
                this.ExcelWriteWrapText(len, 1, "序号");

                this.ExcelWriteWrapText(len, 2, "项目名称");

                this.ExcelWriteWrapText(len, 3, "负责人");

                this.ExcelWriteWrapText(len, 4, "项目地址");

                this.ExcelWriteWrapText(len, 5, "委托单位");

                this.ExcelWriteWrapText(len, 6, "开始时间");

                this.ExcelWriteWrapText(len, 7, "已开工");

                this.ExcelWriteWrapText(len, 8, "结束时间");

                this.ExcelWriteWrapText(len, 9, "离完工");

                this.ExcelWriteWrapText(len, 10, "工期");

                this.ExcelWriteWrapText(len, 11, "监测日期");

                this.ExcelWriteWrapText(len, 12, "状态");
                len ++;
                try
                {
                    for (; i*2 < datarowscnt*(cnt+1); i++)
                    {
                        if (len == 31)
                        {
                            string a = "";
                        };

                        if (i > (dv.Count - 1))
                        {
                            //RowFormat(len, 10);
                            //CreateRow(len, sheet1, 10);
                            spacelen++;
                            len++;
                            continue;
                        }

                        this.ExcelMergedRegion(len, 1, len + 1, 1);
                        this.ExcelMergedRegion(len, 2, len + 1, 2);
                        this.ExcelMergedRegion(len, 3, len + 1, 3);
                        this.ExcelMergedRegion(len, 4, len + 1, 4);
                        this.ExcelMergedRegion(len, 5, len + 1, 5);
                        this.ExcelMergedRegion(len, 6, len + 1, 6);
                        this.ExcelMergedRegion(len, 7, len + 1, 7);
                        this.ExcelMergedRegion(len, 8, len + 1, 8);
                        this.ExcelMergedRegion(len, 9, len + 1, 9);
                        this.ExcelMergedRegion(len, 10, len + 1, 10);
                        this.ExcelMergedRegion(len, 11, len + 1, 11);
                        this.ExcelMergedRegion(len, 12, len + 1, 12);
                        this.ExcelWriteWrapText(len, 1, i+1);

                        this.ExcelWriteWrapText(len, 2, dv[i]["xmname"]);

                        this.ExcelWriteWrapText(len, 3, dv[i]["ResponseMan"]);

                        this.ExcelWriteWrapText(len, 4, dv[i]["xmaddress"]);

                        this.ExcelWriteWrapText(len, 5, dv[i]["delegateDw"]);

                        this.ExcelWriteWrapText(len, 6, dv[i]["jcpgStartDate"]);

                        this.ExcelWriteWrapText(len, 7, dv[i]["RunDays"]);

                        this.ExcelWriteWrapText(len, 8, dv[i]["jcpgEndDate"]);

                        this.ExcelWriteWrapText(len, 9, dv[i]["DaysAprochalFinished"]);

                        this.ExcelWriteWrapText(len, 10, dv[i]["totaldays"]);

                        this.ExcelWriteWrapText(len, 11, dv[i]["monitordate"]);

                        this.ExcelWriteWrapText(len, 12, dv[i]["state"]);

                        len+=2;

                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                    Console.WriteLine(ex.StackTrace);
                }
 
                this.BorderSet(rangerowstart, 1, len - spacelen-1, widthcol);

                diffsettlememntcount = 0;

            }

        }







    }
}
