﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool
{
    
    public class TotalChartCreateEnviroment:ChartCreateEnviroment
    {

        public List<string> pnames { get; set; }
        public TotalChartCreateEnviroment(int cstartlen, int cendlen, string pointname, int pageIndex,List<string> pnames)
        {
            this.cstartlen = cstartlen;
            this.cendlen = cendlen;
            this.pointname =  pointname;
            this.pageIndex = pageIndex;
            this.pnames = pnames;
        }

    }
}
