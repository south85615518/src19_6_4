﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.Pub
{
    public class reportprotocol
    {
        public string lobj { get; set; }
        public string expath { get; set; }
        public string xmname { get; set; }
        public printtype printtype { get; set; }
        

    }
    public enum printtype
    {
        TOTALSTATION_SPMC, TOTALSTATION_MPSC, GNSS, GAUGE, MCU, MCUANGLE
    }
}
