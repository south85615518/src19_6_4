﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.Pub
{
    interface ReportCreate
    {
        void ChartDataFill(string xmname, string tabHead, DataTable originaldt, out List<ChartCreateEnviroment> lce, out List<ReportFillEnviroment> lrf);
        void ChartDataBind(List<ChartCreateEnviroment> lce);
        void ReportDataFill(string xmname, string tabHead, List<ReportFillEnviroment> lrf);
        void ColumnFormat();
    }
}
