﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool
{
    
    public class ChartCreateEnviroment
    {
        
        public int cstartlen{get;set;}
        public int cendlen { get; set; }
        public string pointname { get; set; }
        public int pageIndex { get; set; }
        public ChartCreateEnviroment(int cstartlen, int cendlen,string pointname,int pageIndex)
        {
            this.cstartlen = cstartlen;
            this.cendlen = cendlen;
            this.pointname =  pointname;
            this.pageIndex = pageIndex;
        }
        public ChartCreateEnviroment()
        {
           
        }

    }
}
