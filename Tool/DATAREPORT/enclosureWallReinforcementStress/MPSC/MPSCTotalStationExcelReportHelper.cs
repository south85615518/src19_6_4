﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {

        public void EnclosureWallReinforcementStressReportTemplateCreateMPSC(string xmname, string[] tabName, DataTable dt, int len)
        {

            string pointname = dt.Rows[0].ItemArray[1].ToString();
            int pagecnt = dt.Rows.Count % 31 == 0 ? dt.Rows.Count / 31 : dt.Rows.Count / 31 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            int rangerowstart = len;
            
            for (cnt = 0; cnt < pagecnt; cnt++)
            {
                //添加标题
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                this.ExcelMergedRegion(len, 1, len, dt.Columns.Count - 1);
                
                this.ExcelWrite(len, 1, string.Format("{0}表面位移监测报表   报表编号{4}   {1}年{2}月{3}日                                                 第{5}期", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo, pointname));
                len++;
                int k = 0;
                //}
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                //this.ExcelMergedRegion(len, 1, len, dt.Columns.Count - 1);
                ////this.BorderSet(len, 1, len, 1);
                //this.ExcelWrite(len, 1, pointname, true);
                //len++;
                //row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                //row.GetCell(0).SetCellValue(pointname
                //    );
                //len++;

                //k = 0;
                //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                //CreateRow(len+1, sheet1, tabName.Length);//创建复表头行

                //sheet1.AddMergedRegion(new CellRangeAddress(len , len +1, 0, 0));
                //row.CreateCell(0).SetCellValue("点名");
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("高度（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("温度（°C）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(10).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                //CellFontSet(row.GetCell(10), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                ////return;
                ////k = 0;
                //len++;
                //ICellStyle styleCell = GetICellStyle();
                //ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

                //CreateRow(len, sheet1, tabName.Length);//创建复表头行
                // row = CreateRow(len , sheet1, tabName.Length);//创建复表头行
                //this.BorderSet(len, 1, len + 1, 1);
                this.ExcelMergedRegion(len, 1, len + 1, 1);
                this.ExcelWrite(len, 1, "点名");

                //this.BorderSet(len, 2, len, 4);
                this.ExcelMergedRegion(len, 2, len, 4);
                this.ExcelWrite(len, 2, "应力值(Mpa)");

                //this.BorderSet(len, 5, len, 7);
                this.ExcelMergedRegion(len, 5, len, 7);
                this.ExcelWrite(len, 5, "本次变化值(Mpa)");

                //this.BorderSet(len, 8, len, 10);
                this.ExcelMergedRegion(len, 8, len, 10);
                this.ExcelWrite(len, 8, "累计变化值(Mpa)");

                //this.BorderSet(len, 11, len + 1, 11);
                this.ExcelMergedRegion(len, 11, len + 1, 11);
                this.ExcelWrite(len, 11, "时间");



                ////sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                ////row.CreateCell(0).SetCellValue("时间");
                ////CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                ////CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(0).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("观测值（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("本次变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(7).SetCellValue("累计变化值（mm）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 7, 9));
                //CellFontSet(row.GetCell(7), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);


                //return;
                k = 0;
                len+=1;

                //row = CreateRow(len, sheet1, tabName.Length);//创建表头行


                k = 0;

                foreach (string name in tabName)
                {
                    //if (k == 0) { k++; continue; }

                    if (k == dt.Columns.Count - 1)
                    {

                    }
                    //CellBorderSet(row.GetCell(k), styleBorder, true);
                    else
                    {
                        this.ExcelWrite(len, k + 1, name);
                        //row.CreateCell(k).SetCellValue(name);
                        //CellBorderSolid(row.GetCell(k), styleCell, true);
                    }
                    k++;
                }

                len++;

                int i = 0;
                try
                {
                    for (; i < 32; i++)
                    {

                        if (31 * cnt + i > dt.Rows.Count - 1)
                        {
                            //RowFormat(len, 10);
                            //CreateRow(len, sheet1, 10);
                            len++;
                            continue;
                        }
                        int j = 0, t = 1;
                        for (j = 0; j < dt.Columns.Count; j++)
                        {

                            if (j == 1) continue;
                            if (t==1||t==11)
                            {

                                
                                ExcelWrite(len,t,dt.Rows[31* cnt + i].ItemArray[j]);

                            }
                            else
                            {
                                ExcelWrite(len, t, Convert.ToDouble(dt.Rows[31 * cnt + i].ItemArray[j]));

                            }

                            t++;

                        }
                        len++;
                        //i++;

                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                }


            }
            this.BorderSet(rangerowstart, 1, len-1, dt.Columns.Count - 1);

        }
      
    }
}
