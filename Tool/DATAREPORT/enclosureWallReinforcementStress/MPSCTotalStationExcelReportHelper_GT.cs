﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {
        
        public    void EnclosureWallReinforcementStressReportTemplateCreate_GT_WithoutSettlement(string xmname, string[] tabName, DataTable dt,int len,string reportname,int tabrowscount)
        {
            try{
            string pointname = dt.Rows[0].ItemArray[0].ToString();
            int pagecnt = dt.Rows.Count % 41 == 0 ? dt.Rows.Count / 41 : dt.Rows.Count / 41 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            int rangerowstart = len;

            tabName = new string[3] { "时间", "本次变化(MPa)", "累计变化(MPa)" };
            DateTime time = Convert.ToDateTime(dt.Rows[0].ItemArray[1]);
            int diffsettlememntcount = 0;
            List<double> difflist = new List<double>();
            for (cnt = 0; 41 * cnt < dt.Rows.Count; cnt++)
            {
                rangerowstart = len;
                //添加标题
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                this.ExcelMergedRegion(len, 1, len, 6);

                this.ExcelWriteWrapText(len, 1, xmname);
                len++;
                this.ExcelMergedRegion(len, 1, len, 6);
                this.ExcelWriteWrapText(len, 1, string.Format("地下围护墙钢筋应力监测报表"));
                len++;

                this.ExcelWriteWrapText(len, 1, "工程名称:");
                this.ExcelMergedRegion(len, 2, len, 3);
                this.ExcelWriteWrapText(len, 2, xmname);
                this.ExcelWriteWrapText(len, 4, "报表编号:");
                this.ExcelWriteWrapText(len, 5, Tool.DateHelper.DateTimeToStringReportTime(DateTime.Now));
                len++;
                //this.ExcelWriteWrapText(len, 1, "观测方法:");
                //this.ExcelWriteWrapText(len, 2, "自动全站仪测量");
                this.ExcelWriteWrapText(len, 1, "时间范围:");
                this.ExcelMergedRegion(len, 2, len, 4);
                this.ExcelWriteWrapText(len, 2, string.Format("{0} - {1}", dt.Rows[0].ItemArray[1], dt.Rows[dt.Rows.Count - 1].ItemArray[1]));
                this.ExcelWriteWrapText(len, 5, "点名:");
                this.ExcelWriteWrapText(len, 6, pointname);
                len++;
                int k = 0;
                //}
                //this.BorderSet(len, 1, len, dt.Columns.Count - 1);
                //this.ExcelMergedRegion(len, 1, len, dt.Columns.Count - 1);
                ////this.BorderSet(len, 1, len, 1);
                //this.ExcelWriteWrapText(len, 1, pointname, true);
                //len++;
                //row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                //row.GetCell(0).SetCellValue(pointname
                //    );
                //len++;

                //k = 0;
                //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                //CreateRow(len+1, sheet1, tabName.Length);//创建复表头行

                //sheet1.AddMergedRegion(new CellRangeAddress(len , len +1, 0, 0));
                //row.CreateCell(0).SetCellValue("点名");
                //CellFontSet(row.GetCell(0), "黑体", 300, 41, false);
                //CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("高度（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 41, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("温度（°C）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 41, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(10).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                //CellFontSet(row.GetCell(10), "黑体", 300, 41, false);
                //CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                ////return;
                ////k = 0;
                //len++;
                //ICellStyle styleCell = GetICellStyle();
                //ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

                //CreateRow(len, sheet1, tabName.Length);//创建复表头行
                // row = CreateRow(len , sheet1, tabName.Length);//创建复表头行
                //this.BorderSet(len, 1, len + 1, 1);
                this.ExcelMergedRegion(len, 1, len, 2);
                this.ExcelWriteWrapText(len, 1, "时间");

                //this.BorderSet(len, 5, len, 7);
                this.ExcelMergedRegion(len, 3, len, 4);
                this.ExcelWriteWrapText(len, 3, "本次变化量(MPa)");

                //this.BorderSet(len, 8, len, 10);
                this.ExcelMergedRegion(len, 5, len, 6);
                this.ExcelWriteWrapText(len, 5, "累计变化量(MPa)");

                //this.ExcelMergedRegion(len, 8, len + 1, 8);
                //this.ExcelWriteWrapText(len, 8, "测点");

                //this.ExcelMergedRegion(len, 9, len + 1, 9);
                //this.ExcelWriteWrapText(len, 9, "沉降差");


                //this.ExcelMergedRegion(len, 12, len, 14);
                //this.ExcelWriteWrapText(len, 12, "本次变化范围（MPa）");

                //this.ExcelWriteWrapText(len + 1, 12, "纵向位移");
                //this.ExcelWriteWrapText(len + 1, 13, "横向位移");
                //this.ExcelWriteWrapText(len + 1, 14, "沉降");
                //this.ExcelWriteWrapText(len + 2, 11, "min");
                //最小
                //this.ExcelWriteWrapText(len + 2, 12, SortValueMin(dt, "This_dN"));
                //this.ExcelWriteWrapText(len + 2, 13, SortValueMin(dt, "This_dE"));
                //this.ExcelWriteWrapText(len + 2, 14, SortValueMin(dt, "This_dZ"));
                //this.ExcelWriteWrapText(len + 2, 15, SortValueMin(dt, "Ac_dN"));
                //this.ExcelWriteWrapText(len + 2, 16, SortValueMin(dt, "Ac_dE"));
                //this.ExcelWriteWrapText(len + 2, 17, SortValueMin(dt, "Ac_dZ"));
                //this.ExcelMergedRegion(len, 15, len, 17);
                //this.ExcelWriteWrapText(len, 15, "累计变化范围（MPa）");

                //this.ExcelWriteWrapText(len + 1, 15, "纵向位移");
                //this.ExcelWriteWrapText(len + 1, 16, "横向位移");
                //this.ExcelWriteWrapText(len + 1, 17, "沉降");
                //this.ExcelWriteWrapText(len + 3, 11, "max");
                //最大
                //this.ExcelWriteWrapText(len + 3, 12, SortValueMax(dt, "This_dN"));
                //this.ExcelWriteWrapText(len + 3, 13, SortValueMax(dt, "This_dE"));
                //this.ExcelWriteWrapText(len + 3, 14, SortValueMax(dt, "This_dZ"));
                //this.ExcelWriteWrapText(len + 3, 15, SortValueMax(dt, "Ac_dN"));
                //this.ExcelWriteWrapText(len + 3, 16, SortValueMax(dt, "Ac_dE"));
                //this.ExcelWriteWrapText(len + 3, 17, SortValueMax(dt, "Ac_dZ"));
                //this.ExcelMergedRegion(len, 15, len, 17);
                //this.ExcelWriteWrapText(len, 18, "沉降差");




                //本次变化范围（MPa）
                ////sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                ////row.CreateCell(0).SetCellValue("时间");
                ////CellFontSet(row.GetCell(0), "黑体", 300, 41, false);
                ////CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(0).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                //CellFontSet(row.GetCell(0), "黑体", 300, 41, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("观测值（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 41, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("本次变化值（MPa）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 41, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(7).SetCellValue("累计变化值（MPa）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 7, 9));
                //CellFontSet(row.GetCell(7), "黑体", 300, 41, false);
                //CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);


                //return;
                k = 0;
                len += 1;

                ////row = CreateRow(len, sheet1, tabName.Length);//创建表头行


                //k = 0;

                //foreach (string name in tabName)
                //{
                //    //if (k == 0) { k++; continue; }

                //    if (k == dt.Columns.Count - 1)
                //    {

                //    }
                //    //CellBorderSet(row.GetCell(k), styleBorder, true);
                //    else
                //    {
                //        this.ExcelWriteWrapText(len, k + 1, name);
                //        //row.CreateCell(k).SetCellValue(name);
                //        //CellBorderSolid(row.GetCell(k), styleCell, true);
                //    }
                //    k++;
                //}

                //len++;

                int i = 0;
                try
                {
                    for (; i < 41; i++)
                    {
                        this.ExcelMergedRegion(len, 1, len, 2);
                        this.ExcelMergedRegion(len, 3, len, 4);
                        this.ExcelMergedRegion(len, 5, len, 6);
                        if (41 * cnt + i == 93) { string a = ""; }
                        if (41 * cnt + i > dt.Rows.Count - 1)
                        {
                            //RowFormat(len, 10);
                            //CreateRow(len, sheet1, 10);
                            len++;
                            continue;
                        }
                        ExceptionLog.ReportLineWrite(tabrowscount, reportname);
                        int j = 0, t = 1;
                        

                        ExcelWriteWrapText(len, 1, Convert.ToDateTime(dt.Rows[41 * cnt + i].ItemArray[1]));
                        ExcelWriteWrapText(len, 3, Convert.ToDouble(dt.Rows[41 * cnt + i].ItemArray[2]));
                        ExcelWriteWrapText(len, 5, Convert.ToDouble(dt.Rows[41 * cnt + i].ItemArray[3]));

                        //double diff = DiffSettlement(dt, dt.Rows[31 * cnt + i].ItemArray[0].ToString(), dt.Rows[31 * cnt + i].ItemArray[dt.Columns.Count-1].ToString());
                        //if (diff != -9.99)
                        //{
                        //    ExcelWriteWrapText(rangerowstart+7+diffsettlememntcount, 8, string.Format("{0}：{1}", dt.Rows[31 * cnt + i].ItemArray[0].ToString(), dt.Rows[31 * cnt + i].ItemArray[dt.Columns.Count - 1].ToString()));
                        //    ExcelWriteWrapText(rangerowstart + 7 + diffsettlememntcount, 9, diff);
                        //    difflist.Add(diff);
                        //    diffsettlememntcount++;
                        //}
                        len++;

                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                    Console.WriteLine(ex.StackTrace);
                }
                //if (difflist != null && difflist.Count > 0)
                //{
                //    difflist.Sort();
                //    ExcelWriteWrapText(rangerowstart + 7, 18, difflist[0]);
                //    ExcelWriteWrapText(rangerowstart + 8, 18, difflist[difflist.Count - 1]);
                //}

                this.ExcelMergedRegion(len, 1, len + 2, 6);

                this.ExcelWriteWrapText(len, 1, string.Format(@"监测小结：钢筋应力本次变化量在{0}~{1}MPa之间，累计变化量在{2}~{3}MPa之间,累计变化量未达到报（预）警值。", SortValueMin(dt, "single_this_scalarvalue"), SortValueMax(dt, "single_this_scalarvalue"), SortValueMin(dt, "single_ac_scalarvalue"), SortValueMax(dt, "single_ac_scalarvalue")));
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 3;
                this.ExcelMergedRegion(len, 1, len + 1, 6);
                this.ExcelWriteWrapText(len, 1, @"注：“+”表示应力变小，“-”表示应力变大。");
                xSheet.Cells[len, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                len += 2;
                this.ExcelWriteWrapText(len, 1, "测量:");
                this.ExcelWriteWrapText(len, 4, "校核:");
                len++;
                this.ExcelMergedRegion(len, 5, len, 6);
                this.ExcelWriteWrapText(len, 5, "广州铁路科技开发有限公司");
                len++;
                this.ExcelMergedRegion(len, 6, len, 6);
                this.ExcelWriteWrapText(len, 6, string.Format("{0}/{1}/{2}", time.Year, time.Month, time.Day));
                len++;
                //this.BorderSet(rangerowstart + 5, 1, rangerowstart + 6 + diffsettlememntcount, 7);
                this.BorderSet(rangerowstart + 4, 1, len - 4, 6);

                diffsettlememntcount = 0;
            }
            }
                catch(Exception ex)
            {
                ExceptionLog.ExceptionWrite("钢筋应力报表输出出错错误信息："+ex.Message);
            }
            

        }

     
    


      
    }
}
