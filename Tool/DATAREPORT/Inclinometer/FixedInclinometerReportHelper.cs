﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;
using Tool.DATAREPORT.Pub;


namespace Tool
{
    public class FixedInclinometerReportHelper : ReportCreate
    {

        public    XSSFWorkbook xssfworkbook;
        public    HSSFWorkbook hssfworkbook;
        public    int vsion = 0;//版本标志 
        public ExcelHelperHandle ExcelHelper = new ExcelHelperHandle();
        public    void WriteToFile(string ldpath)
        {
            FileStream file = new FileStream(ldpath, FileMode.Create, FileAccess.ReadWrite);
            if (hssfworkbook != null)
            {
                hssfworkbook.Write(file);
            }
            else
            {
                xssfworkbook.Write(file);
            }
            file.Close();

        }
        public    void HInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added. 
            Exception e = null;
           
            FileStream file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
            try
            {
                hssfworkbook = new HSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                hssfworkbook.DocumentSummaryInformation = dsi;
                vsion = 0;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                hssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (e != null)
                    throw e;
            }
        }
        public    void XInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            Exception e = null;
            //string pathtmp = Path.GetDirectoryName(tppath) + DateTime.Now.ToFileTime() + new Random(0).Next(999) + ".xlsx";
            //File.Copy(tppath, pathtmp);
            //tppath = pathtmp;
            FileStream file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
            try
            {
                xssfworkbook = new XSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                //xssfworkbook .DocumentSummaryInformation = dsi;
                vsion = 1;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                //xssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (e != null)
                    throw e;
            }
        }
        public    void PublicCompatibleInitializeWorkbook(string tppath)
        {
            try
            {
                HInitializeWorkbook(tppath);
            }
            catch (Exception ex)
            {
                XInitializeWorkbook(tppath);
            }
        }
        /// <summary>
        /// NOPI填充曲线数据表
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="tabName"></param>
        /// <param name="dt"></param>
        /// <param name="sheet1"></param>
        /// <param name="sheet"></param>
        /// <param name="sheetName"></param>
        /// <param name="len"></param>
        /// <param name="datastartlen"></param>
        public void ReportDataFill(string xmname, DataTable dt, ISheet sheet, string sheetName, int len, int datastartlen)
        {



            IRow datarow = null;
            int l = dt.Rows.Count;

            int i = 0;
            for (; i < l; i++)
            {


                datarow = sheet.CreateRow(datastartlen);
                datastartlen++;

                if (i == 0)
                {
                    datarow.CreateCell(0).SetCellValue("时间");
                    datarow.CreateCell(1).SetCellValue("#标量值#");
                    datarow.CreateCell(2).SetCellValue("本次变化量");
                    datarow.CreateCell(3).SetCellValue("累计变化量");
                    datarow.CreateCell(4).SetCellValue("变化速率");
                    datarow.CreateCell(5).SetCellValue(sheetName);
                    datarow = sheet.CreateRow(datastartlen);
                    datastartlen++;
                    datarow.CreateCell(0).SetCellValue(dt.Rows[i].ItemArray[6].ToString());
                    datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[2]));
                    datarow.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[3]));
                    datarow.CreateCell(3).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[4]));
                    datarow.CreateCell(4).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[5]));
                    //点名,上次水深,本次水深,本次变化量,累计变化量,变化速率,时间

                }
                else
                {
                    datarow.CreateCell(0).SetCellValue(dt.Rows[i].ItemArray[6].ToString());
                    datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[2]));
                    datarow.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[3]));
                    datarow.CreateCell(3).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[4]));
                    datarow.CreateCell(4).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[5]));
                }


            }

        }
        public void ChartDataFill(string xmname, string tabHead, DataTable originaldt, out List<ChartCreateEnviroment> lce, out List<ReportFillEnviroment> lrf)
        {
            int len = 0;
            List<DataTable> ldt = Tool.DataTableHelper.ProcessDataTableSplitSPMC(originaldt);
            //int t = 0;
            string sheetName = "";
            lce = new List<ChartCreateEnviroment>();
            lrf = new List<ReportFillEnviroment>();
            string[] tabName = tabHead.Split(',');
            int reportNo = new Random().Next(9999);
            int pageindexlen = 0;
            int datastartlen = 0;
            ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("#传感器名称#") : xssfworkbook.GetSheet("#传感器名称#");
            ISheet sheet = vsion == 0 ? hssfworkbook.GetSheet("Sheet1") : xssfworkbook.GetSheet("Sheet1");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue(1);
            foreach (DataTable dt in ldt)
            {

                string tmp = "点名:" + dt.Rows[0].ItemArray[0].ToString();
                DataView dv = new DataView(dt);
                sheetName = dt.Rows[0].ItemArray[0].ToString();

                if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
                sheet1.Header.Left = "南方测绘";
                sheet1.Header.Right = DateTime.Now.ToString();
                sheet1.Footer.Center = "第&p页";
                sheet1.PrintSetup.PaperSize = 9;
                sheet1.FitToPage = false;
                sheet1.PrintSetup.Scale = 100;
                sheet1.HorizontallyCenter = true;
                int k = 0;
                //IRow row = sheet1.CreateRow(len);
                pageindexlen = len + 7;
                lrf.Add(new ReportFillEnviroment(dt, len));

                //sheet.CreateRow(0).CreateCell(0).SetCellValue(1);
                ReportDataFill(xmname, dt, sheet, sheetName, len, datastartlen);
                len += (dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1) * 54;
                datastartlen += dt.Rows.Count + 1;
                int pageIndex = len % 54 == 0 ? len / 54 : len / 54 + 1;
                len = len % 54 == 0 ? (len / 54) * 54 + 108 : (len / 54) * 54 + 162;
                lce.Add(new ChartCreateEnviroment(datastartlen + 1 - dt.Rows.Count, datastartlen, sheetName, pageIndex));
            }
        }

        public void ChartDataBind(List<ChartCreateEnviroment> lce)
        {
            FixedInclinometerChartBind gaugeChartBind = new FixedInclinometerChartBind { ExcelHelper = ExcelHelper };
            gaugeChartBind.ChartBind(lce);
        }

        public void ReportDataFill(string xmname, string tabHead, List<ReportFillEnviroment> lrf)
        {
            FixedInclinometerDataFill gaugeDataFill = new FixedInclinometerDataFill { ExcelHelper = ExcelHelper };
            gaugeDataFill.DataFill(xmname, tabHead, lrf);
        }

        public void Main(string xmname, string tabHead, DataTable originaldt, string xlpath)
        {
            List<ChartCreateEnviroment> lce = null;
            List<ReportFillEnviroment> lrf = null;
            ChartDataFill(xmname, tabHead, originaldt, out lce, out  lrf);
            WriteToFile(xlpath);
            ExcelHelper.workbookpath = xlpath;
            ExcelHelper.ExcelInit();
            ExcelHelper.SheetInit(6, 8);
            ColumnFormat();
            ChartDataBind(lce);
            ReportDataFill(xmname, tabHead, lrf);
            ExcelHelper.save();
        }





        public void ColumnFormat()
        {
            ExcelHelper.DateFormat(ExcelHelper.xSheet, 1, "yy/mm/dd hh:MM");
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 1, 20);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 2, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 3, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 12);
        }
    }
}