﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {
        public  void FixedInclinometerReportTemplateCreate(string xmname, string[] tabName, DataTable dt,int len)
        {

            string pointname = dt.Rows[0].ItemArray[0].ToString();
            int pagecnt = dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            int rangerowstart = len;
            for (cnt = 0; cnt < pagecnt; cnt++)
            {
                //添加标题
                //this.BorderSet(len, 1, len, 6);
                this.ExcelMergedRegion(len, 1, len, 6);

                this.ExcelWrite(len, 1, string.Format("{0}#传感器名称#监测报表   报表编号{4}   {1}年{2}月{3}日    点名:{5}", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo, pointname));
                len++;
                int k = 0;
                
                //this.BorderSet(len, 1, len, 1);
                //this.BorderSet(len+1, 1, len+1, 1);
                this.ExcelMergedRegion(len, 1, len+1, 1);
                this.ExcelWrite(len, 1, "时间");
                
                //this.BorderSet(len, 2, len,2);
                this.ExcelWrite(len, 2, "上次#标量值#");
                this.ExcelWrite(len + 1, 2, "m");
                this.ExcelWrite(len, 3, "本次水位");
                this.ExcelWrite(len + 1, 3, "m");
                this.ExcelWrite(len, 4, "本次变化");
                this.ExcelWrite(len + 1, 4, "mm");
                this.ExcelWrite(len, 5, "累计变化");
                this.ExcelWrite(len + 1, 5, "mm");
                this.ExcelWrite(len, 6, "变化速率");
                this.ExcelWrite(len + 1, 6, "mm/d");
                len+=2;
                int i = 0;
                try
                {
                    for (; i < 51; i++)
                    {

                        if (51 * cnt + i > dt.Rows.Count - 1)
                        {
                            RowFormat(len, 5);
                            len++;
                            continue;
                        }
                        int j = 0, t = 1;
                        for (j = 0; j < dt.Columns.Count-2; j++)
                        {


                            if (j == 0) { continue; }
                            if (j == 1) ExcelWrite(len, 1, dt.Rows[50 * cnt + i].ItemArray[6]);
                            else
                            ExcelWrite(len,t, dt.Rows[50 * cnt + i].ItemArray[t-1]);
                            t++;

                        }
                        len++;

                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                }
                this.BorderSet(rangerowstart, 1, len - 1, 6);

            }

        }
       
    }
}
