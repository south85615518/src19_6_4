﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;

namespace Tool.DATAREPORT.FixedInclinometer
{
    public class FixedInclinometerChartBind
    {
        public ExcelHelperHandle ExcelHelper { get; set; }
        public  int odd = -1;
        public  int setupHei = 0;
        public  void ChartBind(List<ChartCreateEnviroment> lce)
        {
            
            foreach (ChartCreateEnviroment cce in lce)
            {
                setupHei =cce.pageIndex * 730;
                ReportChartCreate(cce.cstartlen, cce.cendlen, string.Format("{0}#传感器名称#过程变化曲线", cce.pointname), "A", "B", new string[1] { "#标量值#" }, new int[1] { 1}, XlMarkerStyle.xlMarkerStyleNone);
                setupHei += 350;
                ReportChartCreate(cce.cstartlen, cce.cendlen, string.Format("{0}#传感器名称#本次变化曲线", cce.pointname), "A", "C", new string[1] { "本次变化" }, new int[1] { 2 }, XlMarkerStyle.xlMarkerStyleNone);
                setupHei += 380;
                ReportChartCreate(cce.cstartlen, cce.cendlen, string.Format("{0}#传感器名称#累计变化曲线", cce.pointname), "A", "D", new string[1] { "累计变化" }, new int[1] { 3 }, XlMarkerStyle.xlMarkerStyleNone);
                setupHei += 350;
                ReportChartCreate(cce.cstartlen, cce.cendlen, string.Format("{0}#传感器名称#速率变化曲线", cce.pointname), "A", "E", new string[1] { "变化速率" }, new int[1] { 4 }, XlMarkerStyle.xlMarkerStyleNone);
            }
        }
        /// <summary>
        /// GPS数据曲线生成
        /// </summary>
        /// <param name="cce"></param>
        /// <param name="setupHei"></param>
        /// <param name="oddinterval"></param>
        /// <param name="title"></param>
        /// <param name="seriesName"></param>
        /// <param name="indexRange"></param>
        /// <param name="markStyle"></param>
        public  void ReportChartCreate(int cstartlen, int cendlen, string title, string colstart, string colend, string[] seriesName, int[] indexRange, XlMarkerStyle markStyle)
        {
            
            
            Chart chart = ExcelHelper.ChartAdd(XlChartType.xlLine, 0, setupHei, 495, 350);
            ExcelHelper.ExcelChartDataBind(chart, colstart + cstartlen, colend + cendlen);
            ExcelHelper.SeriesCharacterCreate(chart, title, seriesName, indexRange, markStyle,true);
        }

    }
}
