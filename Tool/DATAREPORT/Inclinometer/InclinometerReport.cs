﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.Inclinometer
{
    public  class SenorReport
    {
        public string xmname { get; set; }
        public string jclx { get; set; }
        public string addr { get; set; }
        public string instrument { get; set; }
        public string jcdw { get; set; }
        public string wtdw { get; set; }
        public string oneLevel{ get;set;}
        public string twoLevel { get; set; }
        public string threeLevel { get; set; }
        public string tabHead { get; set; }
        //public bool hasAlarm { get; set; }
        public SenorReport()
        {
            
        }
        

    }
}
