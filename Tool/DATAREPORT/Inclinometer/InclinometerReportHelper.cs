﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;


namespace Tool.Inclinometer
{
    public class SenorReportHelper
    {

        public static XSSFWorkbook xssfworkbook;
        public static HSSFWorkbook hssfworkbook;
        public static int vsion = 0;//版本标志 
        public static void WriteToFile(string ldpath)
        {
            //Write the stream data of workbook to the root directory
            FileStream file = new FileStream(ldpath, FileMode.Create, FileAccess.ReadWrite);
            if (hssfworkbook != null)
            {
                hssfworkbook.Write(file);
            }
            else
            {
                xssfworkbook.Write(file);
            }
            file.Close();

        }

        public static void HInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added. 
            Exception e = null;
            FileStream file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
            try
            {
                hssfworkbook = new HSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                hssfworkbook.DocumentSummaryInformation = dsi;
                vsion = 0;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                hssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (e != null)
                    throw e;
            }
        }

        public static void XInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            Exception e = null;
            FileStream file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
            try
            {
                xssfworkbook = new XSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                //xssfworkbook .DocumentSummaryInformation = dsi;
                vsion = 1;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                //xssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (e != null)
                    throw e;
            }
        }
        public static void PublicCompatibleInitializeWorkbook(string tppath)
        {
            try
            {
                HInitializeWorkbook(tppath);
            }
            catch (Exception ex)
            {
                XInitializeWorkbook(tppath);
            }
        }
        /// <summary>
        /// 单点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValSPMT(SenorReport senorReport, DataTable originaldt, string xlsPath, List<Tool.Inclinometer.SenorReportHelper.inclinometeralarm> ltsi, out List<Tool.Inclinometer.SenorChartHelper.SenorChartCreateEnvironment> lce)
        {



            int len = 0;
            List<DataTable> ldt = Tool.SenorDataTableHelper.ProcessFixedInclinometerDataTableSplitSPMT(originaldt);



            //int t = 0;
            lce = new List<Tool.Inclinometer.SenorChartHelper.SenorChartCreateEnvironment>();

            string[] tabName = senorReport.tabHead.Split(',');
            //ChartHelper chartHelper = new ChartHelper();
            //InitializeWorkbook(xlsPath);
            int cnt = 0;
            int datastartlen = 0;
            int reportNo = new Random().Next(9999);
            
            int pageindexlen = 0;
            foreach (DataTable dt in ldt)
            {
                //InitializeWorkbook(xlsPath);
                //if(vsion == 0)
                ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("深部位移") : xssfworkbook.GetSheet("深部位移");
                ISheet sheet = vsion == 0 ? hssfworkbook.GetSheet("Sheet1") : xssfworkbook.GetSheet("Sheet1");
                string tmp = "";//st;
                //tmp =  "点名:"+dt.Rows[0].ItemArray[0].ToString();
                //if (len > 0) len--;
                pageindexlen = len + 9;
                DataView dv = new DataView(dt);
                string sheetName = dt.Rows[0].ItemArray[1].ToString();

                if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
                sheet1.Header.Left = "南方测绘";
                sheet1.Header.Right = DateTime.Now.ToString();
                sheet1.Footer.Center = "第&p页";
                sheet1.PrintSetup.PaperSize = 9;
                //sheet1.Workbook.SetRepeatingRowsAndColumns(0, 0, dt.Columns.Count, len+1, len+2);
                sheet1.FitToPage = false;
                sheet1.PrintSetup.Scale = 100;
                sheet1.HorizontallyCenter = true;
                int k = 0;
                //报表头部
                ICellStyle styleCell = GetICellStyle();
                ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                sheet1.SetColumnWidth(0, 10 * 256);
                sheet1.SetColumnWidth(1, 10 * 256);
                sheet1.SetColumnWidth(2, 10 * 256);
                sheet1.SetColumnWidth(3, 10 * 256);
                sheet1.SetColumnWidth(4, 10 * 256);
                sheet1.SetColumnWidth(5, 37 * 256);
                
                //int startlen = 0;
                pageindexlen = len + 7;
                SenorChartHelper.SenorChartCreateEnvironment senorChartCreateEnvironment = null;
                //Tool.Inclinometer.SenorReportHelper.inclinometeralarm alarmmodel = (from m in ltsi where m.pointname == sheetName select m).ToList()[0];
                ReportTemplateCreate(senorReport,tabName,dt,sheet1,sheet,sheetName,len,datastartlen);
                len += (dt.Rows.Count % 37 == 0 ? dt.Rows.Count / 37 : dt.Rows.Count / 37 + 1)*52; 
                datastartlen += 2*dt.Rows.Count + 1;



                int pageIndex = pageindexlen % 52 == 0 ? pageindexlen / 52 : pageindexlen / 52 + 1;
                //cnt++;
                //len = (len - cnt * 52) % 52 == 0 ? len : ((len - cnt * 52) / 52 + 1) * 52 + cnt * 52;
                //cnt--;

                //sheet1.SetRowBreak(len+29);
                //sheet1.SetRowBreak(len+58);
                //sheet1.CreateRow(len + 1).CreateCell(0).SetCellValue("从此处中断");
                //sheet1.PrintSetup.Landscape = false;
                //ReportHelper.WriteToFile(xlsPath);
                //chartHelper.ChartReportHelper(xlsPath, sheetName, cstartlen, cendlen, pageIndex);
                ExceptionLog.ExceptionWrite("("+(cnt+1)+"/"+ldt.Count+")"+"点名" + sheetName + "本次时间" + dt.Rows[0].ItemArray[9].ToString() + "累计时间" + dt.Rows[0].ItemArray[8].ToString());
                lce.Add(new Tool.Inclinometer.SenorChartHelper.SenorChartCreateEnvironment(datastartlen - 2 * dt.Rows.Count, datastartlen, dt.Rows.Count, Convert.ToDouble(dt.Rows[dt.Rows.Count -1].ItemArray[2].ToString()), sheetName, pageIndex, dt.Rows[0].ItemArray[9].ToString(), dt.Rows[0].ItemArray[8].ToString()));
                //lce.Add(senorChartCreateEnvironment);
                cnt++;

            }
            //sheet1.ForceFormulaRecalculation = true;
            //xssfworkbook.RemoveName("表面位移");
            ExceptionLog.ExceptionWrite("单点多周期表格生成");




        }

        public static void ReportTemplateCreate(SenorReport senorReport, string[] tabName, DataTable dt, ISheet sheet1, ISheet sheet, string sheetName, int len ,int datastartlen)
        {
            int pagecnt = dt.Rows.Count % 39 == 0 ? dt.Rows.Count / 39 : dt.Rows.Count / 39 + 1;
            IRow datarow = null;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            DataView dv = new DataView(dt);
            int l = dt.Rows.Count;
            List<double> previousVal = (from DataRowView m in dv select Convert.ToDouble(m["previous_disp"])).ToList();//new List<double>();
            List<double> acDisp = (from DataRowView m in dv select Convert.ToDouble(m["ac_disp"])).ToList();
            List<double> thisDisp = (from DataRowView m in dv select Convert.ToDouble(m["this_disp"])).ToList();
            List<double> rap = (from DataRowView m in dv select Convert.ToDouble(m["this_rap"])).ToList();
           

            previousVal.Sort();
            acDisp.Sort();
            thisDisp.Sort();
            rap.Sort();
            int i = 0,cnt=0;
            //startlen = 0;
            ICellStyle styleCell = GetICellStyle();
            ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
            int k = 0;
            //int startpageindex = 0;
            //int len = 0;
            bool alarm = false;
            for (cnt = 0; cnt < pagecnt; cnt++)
            {

                IRow row = CreateRow(len, sheet1, 6);//sheet1.CreateRow(len);
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, 5));
                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("{0}测斜管位移监测报表", sheetName));
                len++;
                row = CreateRow(len, sheet1, 6);//sheet1.CreateRow(len);
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, 5));
                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(true, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(DateTime.Now.ToString());
                len++;
                row = CreateRow(len, sheet1, 6);// sheet1.CreateRow(len);
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, 3));
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 5));
                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(4), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("工程名称:{0}", senorReport.xmname));
                row.GetCell(4).SetCellValue(string.Format("项目名称:{0}", senorReport.jclx));
                len++;
                row = CreateRow(len, sheet1, 6);//sheet1.CreateRow(len);
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, 3));
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 5));
                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(4), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("工程地址:{0}", senorReport.addr));
                row.GetCell(4).SetCellValue(string.Format("监测仪器:{0}", senorReport.instrument));
                len++;
                row = CreateRow(len, sheet1, 6);//sheet1.CreateRow(len);
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, 3));
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 5));
                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(4), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("监测单位:{0}", senorReport.jcdw));
                row.GetCell(4).SetCellValue(string.Format("委托单位:{0}", senorReport.wtdw));
                len++;
                row = CreateRow(len, sheet1, 6);//sheet1.CreateRow(len);
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, 2));
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 3, 5));
                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(3), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("本次监测时间:{0}", dt.Rows[0].ItemArray[9].ToString()));
                row.GetCell(3).SetCellValue(string.Format("与上次相隔时间:{0}", (Convert.ToDateTime(dt.Rows[0].ItemArray[9].ToString()) - Convert.ToDateTime(dt.Rows[0].ItemArray[8].ToString())).TotalDays));
                len++;
                row = CreateRow(len, sheet1, 6);//sheet1.CreateRow(len);
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, 2));
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 3, 5));
                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(3), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("上次监测时间:{0}", dt.Rows[0].ItemArray[8].ToString()));
                row.GetCell(3).SetCellValue(string.Format("第{0}次", dt.Rows[0].ItemArray[7].ToString()));
                len++;
                //报表头部

                k = 0;
                row = CreateRow(len, sheet1, 6);//创建表头行
                len++;
                
                //row = sheet1.GetRow(len-1);
                foreach (string name in tabName)
                {
                    //if (k == 0) { k++; continue; }
                    row.CreateCell(k).SetCellValue(name);
                    if (k == dt.Columns.Count - 1)
                        CellBorderSet(row.GetCell(k), styleBorder, false);
                    else
                        CellBorderSolid(row.GetCell(k), styleCell, true);
                    k++;
                }
                
                //int frezelen = 0;
                for (; i < 37*(cnt+1); i++)
                {
                    if (i > l - 1)
                    {
                        //CreateRow(len, sheet1, 6);
                        len++;
                        continue;
                    }
                    styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                    //row = sheet1.CreateRow(len);
                    row = CreateRow(len, sheet1, 6);
                    if (i == 0)
                    {
                        datarow = sheet.CreateRow(datastartlen);
                        datarow.CreateCell(0).SetCellValue(sheetName);
                        datarow.CreateCell(1).SetCellValue(dt.Rows[0].ItemArray[8].ToString());
                        datarow.CreateCell(2).SetCellValue(dt.Rows[0].ItemArray[9].ToString());
                        datastartlen++;
                        var datarowprevious = sheet.CreateRow(datastartlen);
                        var datarowthis = sheet.CreateRow(datastartlen + dt.Rows.Count);
                        datarowprevious.CreateCell(0).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[3].ToString()));
                        datarowprevious.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[2].ToString()));
                        datarowthis.CreateCell(0).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[4].ToString()));
                        datarowthis.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[2].ToString()));
                        datastartlen++;

                    }
                    else
                    {
                        var datarowprevious = sheet.CreateRow(datastartlen);
                        var datarowthis = sheet.CreateRow(datastartlen + dt.Rows.Count);
                        datarowprevious.CreateCell(0).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[3].ToString()));
                        datarowprevious.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[2].ToString()));
                        datarowthis.CreateCell(0).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[4].ToString()));
                        datarowthis.CreateCell(2).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[2].ToString()));
                        datastartlen++;
                    }
                    int j = 0, t = 0;
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j == 0 || j == 1 || j == 7 || j == 8 || j == 9) continue;



                        if (dt.Columns[j].DataType == Type.GetType("System.Double") || dt.Columns[j].DataType == Type.GetType("System.Decimal"))
                        {
                           
                            row.CreateCell(t).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[j]));
                        }
                        else
                            row.CreateCell(t).SetCellValue(dt.Rows[i].ItemArray[j].ToString());
                        CellFontSet(row.GetCell(t), "仿宋", 300, 16, false);
                        if (j == dt.Columns.Count - 1)

                            CellBorderSet(row.GetCell(t), styleBorder, false);
                        else
                            CellBorderSolid(row.GetCell(t), styleCell, true);
                        t++;

                    }
                    len++;
                    //i++;

                }
               
                row = CreateRow(len, sheet1, 6);
                row.GetCell(0).SetCellValue("最大值");
                row.GetCell(1).SetCellValue(previousVal[previousVal.Count -1]);
                row.GetCell(2).SetCellValue(acDisp[acDisp.Count - 1]);
                row.GetCell(3).SetCellValue(thisDisp[thisDisp.Count -1]);
                row.GetCell(4).SetCellValue(rap[rap.Count -1]);
                len++;
                row = CreateRow(len, sheet1, 6);
                row.GetCell(0).SetCellValue("最小值");
                row.GetCell(1).SetCellValue(previousVal[0]);
                row.GetCell(2).SetCellValue(acDisp[0]);
                row.GetCell(3).SetCellValue(thisDisp[0]);
                row.GetCell(4).SetCellValue(rap[0]);
                len++;
                row = CreateRow(len, sheet1, 6);
                row.GetCell(0).SetCellValue("预警情况");
                row.GetCell(4).SetCellValue("");
                row.GetCell(1).SetCellValue("未预警");
                //alarm |= ProcessAlarmCellValueSet(row.GetCell(3), Convert.ToDouble(sheet1.GetRow(len - 2).GetCell(3).NumericCellValue), alarmmodel.sec_acdisp, alarmmodel.sec_thisdisp, alarmmodel.sec_rap);
                //alarm |= ProcessAlarmCellValueSet(row.GetCell(2), Convert.ToDouble(sheet1.GetRow(len - 2).GetCell(4).NumericCellValue), alarmmodel.first_acdisp, alarmmodel.first_thisdisp, alarmmodel.first_rap);
                row.GetCell(3).SetCellValue("");
                row.GetCell(2).SetCellValue("");
                len++;
                row = CreateRow(len, sheet1, 6);
                row.GetCell(0).SetCellValue("一级预警");
                row.GetCell(1).SetCellValue("\\");
                row.GetCell(2).SetCellValue("");
                row.GetCell(3).SetCellValue("");
                row.GetCell(4).SetCellValue("");
                len++;
                row = CreateRow(len, sheet1, 6);
                row.GetCell(0).SetCellValue("二级预警");
                row.GetCell(1).SetCellValue("\\");
                row.GetCell(2).SetCellValue("");
                row.GetCell(3).SetCellValue("");
                row.GetCell(4).SetCellValue("");
                len++;
                row = CreateRow(len, sheet1, 6);
                row.GetCell(0).SetCellValue("三级预警");
                row.GetCell(1).SetCellValue("\\");
                row.GetCell(2).SetCellValue("");
                row.GetCell(3).SetCellValue("");
                row.GetCell(4).SetCellValue("");
                len++;
                CreateRow(len, sheet1, 6);
                len++;
                sheet1.AddMergedRegion(new CellRangeAddress(tmplen +7, tmplen + 50, 5, 5));
                sheet1.AddMergedRegion(new CellRangeAddress(tmplen + 51,tmplen + 51, 0, 4));
                CellBorderSet(sheet1.GetRow(tmplen + 51).GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), true);
                sheet1.GetRow(tmplen + 51).GetCell(0).SetCellValue("“+”表示向基坑内位移；“-”表示向基坑外位移");
                //"“+”表示向基坑内位移；“-”表示向基坑外位移\n
                CellBorderSet(sheet1.GetRow(tmplen + 51).GetCell(5), GetBorderSetStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER), true);
                sheet1.GetRow(tmplen + 51).GetCell(5).SetCellValue("是否产生红黄预警：" + (alarm ? "是" : "否"));
                tmplen += 52;
                
            }
           

        }


        public static string HasTriggerAlarm(double maxvalue, double firstvalue, double secondvalue, double thirdvalue)
        {
            if (maxvalue > thirdvalue)
            {
                return "三级预警";

            }
            else if (maxvalue > secondvalue)
            {
                return "二级预警";
            }
            else if (maxvalue > firstvalue)
            {
                return "一级预警";
            }
            else
            { return "未预警"; };
        }

        public static bool ProcessAlarmCellValueSet(ICell cell, double maxvalue, double firstvalue, double secondvalue, double thirdvalue)
        {
            if (maxvalue > thirdvalue)
            {
                cell.SetCellValue("三级预警");
                //ICellStyle style = null;
                //IFont font = null;
                //cell.CellStyle.BorderDiagonalColor = 
                //style = xssfworkbook.CreateCellStyle();
                //CellFontSet(cell, "宋体", 200, 12, false);
                //cell.CellStyle.FillBackgroundColor = IndexedColors.RED.Index;
                //cell.CellStyle.BorderDiagonalColor = IndexedColors.RED.Index;
                //90;//NPOI.HSSF.Util.HSSFColor.RED.index; 
                //style = hssfworkbook.CreateCellStyle();
                //cell.CellStyle.FillBackgroundColor = NPOI.XSSF.Util..RED.index;
                //CellStyleSet(cell,IndexedColors.RED);
                //style.LeftBorderColor = 0;
                return true;

            }
            else if (maxvalue > secondvalue)
            {
                cell.SetCellValue("二级预警");
                //ICellStyle style = null;
                //IFont font = null;
                //CellStyleSet(cell, IndexedColors.ORANGE);
                //style = xssfworkbook.CreateCellStyle();
                //cell.CellStyle.FillBackgroundColor = IndexedColors.ORANGE.Index ;//60;//NPOI.HSSF.Util.HSSFColor.ORANGE.index;
                return true;
            }
            else if (maxvalue > firstvalue)
            {
                cell.SetCellValue("一级预警");
                //ICellStyle style = null;
                //IFont font = null;
                //CellStyleSet(cell, IndexedColors.YELLOW);
                //style = xssfworkbook.CreateCellStyle();
                //cell.CellStyle.FillBackgroundColor = IndexedColors.YELLOW.Index;//0;//NPOI.HSSF.Util.HSSFColor.YELLOW.index;
                return true;
            }
            else
            { cell.SetCellValue("未预警"); return false; };

        }


        public class inclinometeralarm
        {
            public string pointname { get; set; }
            public double first_thisdisp { get; set; }
            public double sec_thisdisp { get; set; }
            public double third_thisdisp { get; set; }
            public double first_acdisp { get; set; }
            public double sec_acdisp { get; set; }
            public double third_acdisp { get; set; }
            public double first_rap { get; set; }
            public double sec_rap { get; set; }
            public double third_rap { get; set; }
            //public inclinometeralarm()
            //{

            //}
        }

        



        public static IRow CreateRow(int len, ISheet sheet, int colCnt)
        {
            IRow row = sheet.CreateRow(len);
            for (int i = 0; i < colCnt; i++)
            {
                CellFontSet(row.CreateCell(i), "仿宋", 200, 16, true);
                CellBorderSet(row.CreateCell(i), GetBorderSetStyle(true, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);

            }
            return row;
        }
        


        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValMPMT(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, string et, int index, string xlsPath, out List<ChartCreateEnviroment> lce)
        {



            int len = 0;
            List<DataTable> ldt = new List<DataTable>();
            List<DataTable> ldtArrow = Tool.SenorDataTableHelper.ProcessDataTableSplitSPMT(originaldt);
            ldt.Add(Tool.SenorDataTableHelper.SenorDataMPMTTabCreate(ldtArrow, 2));
            ldt.Add(Tool.SenorDataTableHelper.SenorDataMPMTTabCreate(ldtArrow, 3));
            ldt.Add(Tool.SenorDataTableHelper.SenorDataMPMTTabCreate(ldtArrow, 4));
            List<string> ls = new List<string>();
            ls.Add("本次变化值");
            ls.Add("累计变化值");
            ls.Add("本次变化速率");
            //int t = 0;
            lce = new List<ChartCreateEnviroment>();

            string[] tabName = tabHead.Split(',');
            ChartHelper chartHelper = new ChartHelper();
            //InitializeWorkbook(xlsPath);
            bool init = false;
            int cnt = 0;
            int cstartlen = 0;
            int cendlen = 0;
            int reportNo = new Random().Next(9999);

            foreach (DataTable dt in ldt)
            {
                //InitializeWorkbook(xlsPath);
                //if(vsion == 0)
                ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("深部位移") : xssfworkbook.GetSheet("深部位移");
                string tmp = st;
                tmp = "点名:" + ls[ldt.IndexOf(dt)];


                DataView dv = new DataView(dt);
                sheetName = dt.Rows[0].ItemArray[0].ToString();

                if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
                sheet1.Header.Left = "南方测绘";
                sheet1.Header.Right = DateTime.Now.ToString();
                sheet1.Footer.Center = "第&p页";
                sheet1.PrintSetup.PaperSize = 9;
                //sheet1.Workbook.SetRepeatingRowsAndColumns(0, 0, dt.Columns.Count, len+1, len+2);
                sheet1.FitToPage = false;
                sheet1.PrintSetup.Scale = 100;
                sheet1.HorizontallyCenter = true;
                int k = 0;
                IRow row = sheet1.CreateRow(len);
                ICellStyle styleCell = GetICellStyle();
                ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                //if (len < 10000)
                //{


                sheet1.SetColumnWidth(0, 27 * 256);
                //添加标题
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 1));

                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("{0}深部位移监测报表   报表编号{4}   {1}年{2}月{3}日", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo)
                    );
                CellFontSet(row.GetCell(0), "宋体", 200, 14, false);
                len++;
                k = 0;
                //}

                row = CreateRow(len, sheet1, dt.Columns.Count);//创建点号行
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 1));

                row.GetCell(0).SetCellValue(tmp
                    );
                len++;

                row = CreateRow(len, sheet1, dt.Columns.Count);//创建表头行


                k = 0;
                while (k < dt.Columns.Count)
                {
                    //if (k == 0) { k++; continue; }
                    row.CreateCell(k).SetCellValue(dt.Columns[k].ColumnName);
                    if (k == dt.Columns.Count)
                        CellBorderSet(row.GetCell(k), styleBorder, false);
                    else
                        CellBorderSolid(row.GetCell(k), styleCell, true);
                    k++;

                }
                len++;
                int i = 2;
                foreach (DataRowView drv in dv)
                {
                    styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                    row = sheet1.CreateRow(len);
                    int j = 0;
                    for (j = 0; j < dt.Columns.Count; j++)
                    {

                        if (dt.Columns[j].DataType == Type.GetType("System.Double"))
                            row.CreateCell(j).SetCellValue(Convert.ToDouble(dt.Rows[i - 2].ItemArray[j]));
                        else

                            row.CreateCell(j).SetCellValue(dt.Rows[i - 2].ItemArray[j].ToString());
                        CellFontSet(row.GetCell(j), "仿宋", 300, 16, false);
                        if (j == dt.Columns.Count - 1)

                            CellBorderSet(row.GetCell(j), styleBorder, false);
                        else
                            CellBorderSolid(row.GetCell(j), styleCell, true);

                    }
                    len++;
                    i++;
                }


                if (cnt == 0)
                {
                    cstartlen = 4;
                    cendlen = 1 + i;
                }
                else
                {

                    cstartlen = len - i + 3;
                    cendlen = len;

                }
                //sheet1.SetRowBreak(len);
                int pageIndex = len % 54 == 0 ? (len / 54) : (len / 54) + 1;
                len = len % 55 == 0 ? (len / 54) * 54 + 54 : (len / 54) * 54 + 108;


                //sheet1.SetRowBreak(len+29);
                //sheet1.SetRowBreak(len+58);
                //sheet1.CreateRow(len + 1).CreateCell(0).SetCellValue("从此处中断");
                //sheet1.PrintSetup.Landscape = false;
                //ReportHelper.WriteToFile(xlsPath);
                //chartHelper.ChartReportHelper(xlsPath, sheetName, cstartlen, cendlen, pageIndex);
                lce.Add(new ChartCreateEnviroment(cstartlen, cendlen, sheetName, pageIndex));
                cnt++;

            }
            //sheet1.ForceFormulaRecalculation = true;
            //xssfworkbook.RemoveName("表面位移");
            ExceptionLog.ExceptionWrite("单点多周期表格生成");




        }



        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValMPMT(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, string et, int index)
        {



            DataTable dt = originaldt;
            List<DataTable> ldt = new List<DataTable>();
            List<DataTable> ldtArrow = Tool.SenorDataTableHelper.ProcessDataTableSplitSPMT(originaldt);
            ldt.Add(Tool.SenorDataTableHelper.SenorDataMPMTTabCreate(ldtArrow, 2));
            ldt.Add(Tool.SenorDataTableHelper.SenorDataMPMTTabCreate(ldtArrow, 3));
            ldt.Add(Tool.SenorDataTableHelper.SenorDataMPMTTabCreate(ldtArrow, 4));
            //return;
            //originaldt.DefaultView.Sort = " point_name,cyc asc ";
            int t = 0;

            ISheet sheet1 = null;

            sheet1 = vsion == 0 ? hssfworkbook.GetSheet("表面位移") : xssfworkbook.GetSheet("表面位移");

            if (dt.Columns.Count > 11) sheet1.PrintSetup.Landscape = true;
            sheet1.Header.Left = "南方测绘";
            sheet1.Header.Right = DateTime.Now.ToString();
            sheet1.Footer.Center = "第&p页";
            sheet1.PrintSetup.PaperSize = 9;
            sheet1.Workbook.SetRepeatingRowsAndColumns(index, 0, dt.Columns.Count, 0, 1);
            //sheet1.PrintSetup.Scale = 400;
            sheet1.HorizontallyCenter = true;

            string tmp = string.Format(st, dt.Rows[0].ItemArray[0].ToString());

            string[] tabName = tabHead.Split(',');
            DataView dv = new DataView(dt);

            //sheetName = dt.Rows[0].ItemArray[0].ToString();

            //create cell on rows, since rows do already exist,it's not necessary to create rows again.

            int k = 0;
            IRow row = sheet1.CreateRow(0);
            row = sheet1.CreateRow(0);
            ICellStyle styleCell = GetICellStyle();
            ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
            //CellBorderSolid(row.CreateCell(1));
            foreach (string name in tabName)
            {

                row.CreateCell(k).SetCellValue(title);
                ICellStyle style = styleCell;
                k++;
            }
            sheet1.SetColumnWidth(dt.Columns.Count - 1, 27 * 256);
            //添加标题
            sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, dt.Columns.Count - 1));
            CellFontSet(row.GetCell(0), "仿宋", 200, 16, false);
            CellBorderSet(row.GetCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
            CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
            ////stl.WrapText = true;
            ////row.GetCell(0).SetCellValue(xmname);
            //CellFontSet(row.CreateCell(k-1), "仿宋", 200, 5, false);
            //CellBorderSet(row.GetCell(k-1), GetBorderSetStyle(false), false);
            row.GetCell(0).SetCellValue(tmp + "至" + et
                );


            CellBorderSet(row.GetCell(k - 1), styleBorder, false);
            //创建表头
            row = sheet1.CreateRow(1);
            k = 0;
            foreach (string name in tabName)
            {

                row.CreateCell(k).SetCellValue(name);
                //if (k == dt.Columns.Count - 1)
                //    CellBorderSet(row.GetCell(k), styleBorder, false);
                //else
                CellBorderSolid(row.GetCell(k), styleCell, true);
                k++;

            }

            int i = 2;
            foreach (DataRowView drv in dv)
            {
                styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                row = sheet1.CreateRow(i);
                int j = 0;
                for (j = 0; j < dt.Columns.Count; j++)
                {
                    row.CreateCell(j).SetCellType(CellType.NUMERIC);
                    row.CreateCell(j).SetCellValue(dt.Rows[i - 2].ItemArray[j].ToString());
                    if (j == dt.Columns.Count - 1)
                        CellBorderSet(row.GetCell(j), styleBorder, false);
                    else
                        CellBorderSolid(row.GetCell(j), styleCell, true);
                }
                i++;
            }
            sheet1.ForceFormulaRecalculation = true;


            //xssfworkbook.RemoveName("表面位移");





        }
        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValMPMTF(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, string et, int index, string xlsPath, out ChartCreateEnviroment lce)
        {



            int len = 0;
            //List<DataTable> ldt = Tool.SenorDataTableHelper.ProcessDataTableSplitSPMC(originaldt);
            List<string> pnames = Tool.SenorDataTableHelper.ProcessDataTableFilter(originaldt, "point_name");
            lce = new ChartCreateEnviroment();
            //int t = 0;
            //lce = new ChartCreateEnviroment();
            DataTable dt = originaldt;
            string[] tabName = tabHead.Split(',');
            ChartHelper chartHelper = new ChartHelper();
            //InitializeWorkbook(xlsPath);
            bool init = false;
            int cnt = 0;
            int cstartlen = 0;
            int cendlen = 0;
            int reportNo = new Random().Next(9999);
            //foreach (DataTable dt in ldt)
            //{
            //InitializeWorkbook(xlsPath);
            //if(vsion == 0)
            ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("表面位移") : xssfworkbook.GetSheet("表面位移");
            string tmp = st;
            tmp = "点名:" + dt.Rows[0].ItemArray[0].ToString();


            DataView dv = new DataView(dt);
            sheetName = dt.Rows[0].ItemArray[0].ToString();

            if (dt.Columns.Count > 11) sheet1.PrintSetup.Landscape = true;
            sheet1.Header.Left = "南方测绘";
            sheet1.Header.Right = DateTime.Now.ToString();
            sheet1.Footer.Center = "第&p页";
            sheet1.PrintSetup.PaperSize = 9;
            sheet1.Workbook.SetRepeatingRowsAndColumns(0, 0, dt.Columns.Count - 1, 1, 4);
            sheet1.FitToPage = false;
            sheet1.PrintSetup.Scale = 100;
            sheet1.HorizontallyCenter = true;
            int k = 0;
            IRow row = sheet1.CreateRow(len);
            ICellStyle styleCell = GetICellStyle();
            ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
            //if (len < 10000)
            //{


            sheet1.SetColumnWidth(dt.Columns.Count - 1, 27 * 256);
            //添加标题
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 1));

            CellBorderSet(row.CreateCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
            CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
            row.GetCell(0).SetCellValue(string.Format("{0}水平位移和竖向位移监测报表   报表编号{4}   {1}年{2}月{3}日", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo)
                );
            CellFontSet(row.GetCell(0), "宋体", 200, 14, false);
            len++;
            k = 0;
            //}

            row = CreateRow(len, sheet1, tabName.Length);//创建点号行
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 1));

            //row.GetCell(0).SetCellValue(tmp
            //    );
            len++;

            //k = 0;
            row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
            sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
            row.CreateCell(0).SetCellValue("点名");
            CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

            //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
            sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 1, 1));
            row.CreateCell(1).SetCellValue("周期");
            CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

            row.CreateCell(2).SetCellValue("观测值（mm）");
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 2, 4));
            CellFontSet(row.GetCell(2), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(2), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
            row.CreateCell(5).SetCellValue("本次变化值（mm）");
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 5, 7));
            CellFontSet(row.GetCell(5), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(5), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
            row.CreateCell(8).SetCellValue("累计变化值（mm）");
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 8, 10));
            CellFontSet(row.GetCell(8), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(8), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
            row.CreateCell(11).SetCellValue("时间");
            sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 11, 11));
            CellFontSet(row.GetCell(11), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(11), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
            //return;
            //k = 0;
            len++;
            row = CreateRow(len, sheet1, tabName.Length);//创建表头行


            k = 0;
            foreach (string name in tabName)
            {
                //if (k == 0) { k++; continue; }
                row.CreateCell(k).SetCellValue(name);
                if (k == dt.Columns.Count)
                    CellBorderSet(row.GetCell(k), styleBorder, false);
                else
                    CellBorderSolid(row.GetCell(k), styleCell, true);
                k++;
            }

            len++;
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                row = sheet1.CreateRow(len);
                int j = 0;
                for (j = 0; j < dt.Columns.Count; j++)
                {

                    if (dt.Columns[j].DataType == Type.GetType("System.Double"))
                        row.CreateCell(j).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[j]));
                    else

                        row.CreateCell(j).SetCellValue(dt.Rows[i].ItemArray[j].ToString());
                    CellFontSet(row.GetCell(j), "仿宋", 300, 16, false);
                    if (j == dt.Columns.Count - 1)

                        CellBorderSet(row.GetCell(j), styleBorder, false);
                    else
                        CellBorderSolid(row.GetCell(j), styleCell, true);

                }
                len++;
                i++;
            }


            if (cnt == 0)
            {
                cstartlen = 9;
                cendlen = 9 + i;
            }
            else
            {

                cstartlen = len - i;
                cendlen = len;

            }
            //sheet1.SetRowBreak(len);
            int pageIndex = len % 35 == 0 ? (len / 35) : (len / 35) + 1;
            len = len % 35 == 0 ? (len / 35) * 35 + 70 : (len / 35) * 35 + 105;


            //sheet1.SetRowBreak(len+29);
            //sheet1.SetRowBreak(len+58);
            //sheet1.CreateRow(len + 1).CreateCell(0).SetCellValue("从此处中断");
            //sheet1.PrintSetup.Landscape = false;
            //ReportHelper.WriteToFile(xlsPath);
            //chartHelper.ChartReportHelper(xlsPath, sheetName, cstartlen, cendlen, pageIndex);
            lce = new ChartCreateEnviroment(cstartlen, cendlen, sheetName, pageIndex);




            //sheet1.ForceFormulaRecalculation = true;
            //xssfworkbook.RemoveName("表面位移");
            ExceptionLog.ExceptionWrite("单点多周期表格生成");




        }
        /// <summary>
        ///多点单周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        //public static void SetTabValMPST(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, int index)
        //{




        //    List<DataTable> ldt = Tool.SenorDataTableHelper.ProcessDataTableSplitMPST(originaldt);
        //    int t = 0;

        //    foreach (DataTable dt in ldt)
        //    {

        //        string tmp = string.Format("第{0}周期", dt.Rows[0].ItemArray[1].ToString());

        //        string[] tabName = tabHead.Split(',');
        //        DataView dv = new DataView(dt);
        //        ISheet sheet1 = null;
        //        sheetName = string.Format("第{0}周期", dt.Rows[0].ItemArray[1].ToString());
        //        if (xssfworkbook.GetSheet("表面位移") != null)
        //        {
        //            xssfworkbook.SetSheetName(0, sheetName);
        //            sheet1 = xssfworkbook.GetSheet(sheetName);
        //        }
        //        else if (xssfworkbook.GetSheet(sheetName) == null)
        //            sheet1 = xssfworkbook.CreateSheet(sheetName);

        //        else
        //            sheet1 = xssfworkbook.GetSheet(sheetName);
        //        //create cell on rows, since rows do already exist,it's not necessary to create rows again.
        //        if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
        //        sheet1.Header.Left = "南方测绘";
        //        sheet1.Header.Right = DateTime.Now.ToString();
        //        sheet1.Footer.Center = "第&p页";
        //        sheet1.PrintSetup.PaperSize = 9;
        //        sheet1.Workbook.SetRepeatingRowsAndColumns(index, 0, dt.Columns.Count, 0, 1);
        //        //sheet1.PrintSetup.Scale = 400;
        //        sheet1.HorizontallyCenter = true;
        //        int k = 0;
        //        IRow row = sheet1.CreateRow(0);
        //        row = sheet1.CreateRow(0);
        //        ICellStyle styleCell = GetICellStyle();
        //        ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        //        //CellBorderSolid(row.CreateCell(1));
        //        foreach (string name in tabName)
        //        {

        //            row.CreateCell(k).SetCellValue(title);
        //            ICellStyle style = styleCell;
        //            k++;
        //        }
        //        sheet1.SetColumnWidth(dt.Columns.Count - 2, 27 * 256);
        //        //添加标题
        //        sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, dt.Columns.Count - 2));
        //        CellFontSet(row.GetCell(0), "仿宋", 200, 16, false);
        //        CellBorderSet(row.GetCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
        //        CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
        //        ////stl.WrapText = true;
        //        ////row.GetCell(0).SetCellValue(xmname);
        //        //CellFontSet(row.CreateCell(k-1), "仿宋", 200, 5, false);
        //        //CellBorderSet(row.GetCell(k-1), GetBorderSetStyle(false), false);
        //        row.GetCell(0).SetCellValue(tmp
        //            );


        //        CellBorderSet(row.GetCell(k - 1), styleBorder, false);
        //        //创建表头
        //        row = sheet1.CreateRow(1);
        //        k = 0;
        //        foreach (string name in tabName)
        //        {

        //            row.CreateCell(k).SetCellValue(name);
        //            if (k == dt.Columns.Count - 1)
        //                CellBorderSet(row.GetCell(k), styleBorder, false);
        //            else
        //                CellBorderSolid(row.GetCell(k), styleCell, true);
        //            k++;
        //        }

        //        int i = 2;
        //        foreach (DataRowView drv in dv)
        //        {
        //            styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        //            row = sheet1.CreateRow(i);
        //            int j = 0;
        //            int r = 0;
        //            for (j = 0; j < dt.Columns.Count; j++)
        //            {
        //                if (j == 1) continue;
        //                //row.CreateCell(r).SetCellType(CellType.NUMERIC);

        //                if (dt.Columns[j].DataType == Type.GetType("System.Double"))
        //                    row.CreateCell(r).SetCellValue(Convert.ToDouble(dt.Rows[i - 2].ItemArray[j]));
        //                else
        //                    row.CreateCell(r).SetCellValue(dt.Rows[i - 2].ItemArray[j].ToString());
        //                if (j == dt.Columns.Count - 1)
        //                    CellBorderSet(row.GetCell(r), styleBorder, false);
        //                else
        //                    CellBorderSolid(row.GetCell(r), styleCell, true);
        //                r++;

        //            }
        //            i++;
        //        }
        //        sheet1.ForceFormulaRecalculation = true;

        //    }
        //    //xssfworkbook.RemoveName("表面位移");





        //}






        //将单元格设置为有边框的
        public static void CellBorderSolid(ICell cell, ICellStyle style, bool wrap)
        {
            //ICellStyle style = xssfworkbook.CreateCellStyle();
            
            style.WrapText = wrap;
            //return;
            cell.CellStyle = style;

        }


        //创建表格样式
        public static ICellStyle GetICellStyle()
        {
            ICellStyle style = null;
            if (xssfworkbook != null)
                style = xssfworkbook.CreateCellStyle();
            else
                style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            style.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            return style;

        }
        //设置边框样式
        public static ICellStyle GetBorderSetStyle(bool solid, HorizontalAlignment h, VerticalAlignment v)
        {
            ICellStyle style = null;
            if (xssfworkbook != null)
                style = xssfworkbook.CreateCellStyle();
            else
                style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderLeft = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderRight = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderTop = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.Alignment = h;
            style.VerticalAlignment = v;
            return style;
        }

        //设置单元格的边框
        public static void CellBorderSet(ICell cell, ICellStyle style, bool wrap)
        {
            //return;
            try
            {
                style.WrapText = wrap;
                cell.CellStyle = style;
            }
            catch (Exception ex)
            {
                string a = "";
            }

        }
        //设置字体样式
        public static void CellFontSet(ICell cell, string fontName,
            short fontWeight, short fontHeight, bool italic)
        {
            try
            {
                ICellStyle style = null;
                IFont font = null;
                if (xssfworkbook != null)
                {
                    style = xssfworkbook.CreateCellStyle();
                    font = xssfworkbook.CreateFont();
                }
                else
                {
                    style = hssfworkbook.CreateCellStyle();
                    font = hssfworkbook.CreateFont();
                }

                font.FontName = fontName;
                font.Boldweight = fontWeight;
                font.FontHeightInPoints = fontHeight;
                font.IsItalic = italic;
                font.Color = IndexedColors.RED.Index;
                style.SetFont(font);
                cell.CellStyle = style;
            }
            catch (Exception ex)
            {
                string a = "";
            }
        }

        //设置字体样式
        public static void CellStyleSet(ICell cell, IndexedColors color)
        {
            try
            {
                ICellStyle style = null;
                IFont font = null;
                if (xssfworkbook != null)
                {
                    style = xssfworkbook.CreateCellStyle();
                    font = xssfworkbook.CreateFont();
                }
                else
                {
                    style = hssfworkbook.CreateCellStyle();
                    font = hssfworkbook.CreateFont();
                }
                font.Color = color.Index;
                //style.FillForegroundColor = color.Index;
                cell.CellStyle.SetFont(font);
            }
            catch (Exception ex)
            {
                string a = "";
            }
        }


        //日期格式转换
        public static string DateFormatConverter(string dateStr)
        {
            if (dateStr == null || dateStr.Trim() == "") return "";
            char[] splits = { '-', ' ', '/', ':' };
            string[] dateStrs = dateStr.Split(splits);
            return dateStrs[0].Substring(dateStrs[0].IndexOf("20") + 2) + "年" + dateStrs[1] + "月" + dateStrs[2] + "日";
        }
        //填充曲线表
        /// <summary>
        /// 对不同监测项目的日期进行排序,生成X轴
        /// </summary>
        /// <param name="dt"></param>
        public static List<DateTime> GetDateTimeFromSeriesInDiffJcxm(ISheet sheet, List<serie> series)
        {
            List<DateTime> ldt = new List<DateTime>();
            List<string> lType = new List<string>();
            int i = 0, j = 0;
            for (i = 0; i < series.Count; i++)
            {

                for (j = 0; j < series[i].Pts.Length; j++)
                {
                    ldt.Add(series[i].Pts[j].Dt);
                }

            }
            ldt = ldt.Distinct().ToList();
            ldt.Sort();
            IRow row = sheet.CreateRow(0);
            for (i = 1; i < ldt.Count + 1; i++)
            {
                row.CreateCell(i).SetCellValue(ldt[i - 1]);
            }
            return ldt;
        }
        /// <summary>
        /// 填充图例
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        /// <returns></returns>
        public static List<string> LegendFill(ISheet sheet, List<serie> series)
        {
            List<string> legends = new List<string>();
            int i = 0;
            for (i = 1; i < series.Count + 1; i++)
            {
                IRow row = sheet.CreateRow(i);
                row.CreateCell(0).SetCellValue(series[i - 1].Name);
                legends.Add(series[i - 1].Name);
            }
            return legends;
        }
        /// <summary>
        /// 安装曲线数据点
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        public static void SerieDataPointFill(ISheet sheet, List<serie> series, List<DateTime> ldt, List<string> legends)
        {

            int y = 0, t = 0, posy = 0, posx = 0;
            for (y = 0; y < series.Count; y++)
            {
                IRow row = sheet.GetRow(y);
                posy = 1 + legends.IndexOf(series[y].Name);
                for (t = 0; t < series[y].Pts.Length; t++)
                {
                    posx = 1 + ldt.IndexOf(series[y].Pts[t].Dt);
                    row.CreateCell(posx).SetCellValue(series[y].Pts[t].Yvalue1);
                }
            }
        }
        //调用曲线填充函数
        //public void SeriesCreate(HttpContext context)
        //{
        //    //InitializeWorkbook("D:\\ReportExcel.xls");
        //    List<serie> ls = (List<serie>)context.Session["series"];
        //    //分拣出本次和累计
        //    List<serie> lsThis = new List<serie>();
        //    List<serie> lsAc = new List<serie>();
        //    foreach (serie s in ls)
        //    {
        //        if (s.Stype.IndexOf("累计") != -1)
        //        {
        //            lsAc.Add(s);
        //        }
        //        else
        //        {
        //            lsThis.Add(s);
        //        }

        //    }
        //    if (lsThis.Count > 0)
        //    {
        //        ISheet sheet = xssfworkbook.CreateSheet("本次变化曲线生成数据");
        //        //本次曲线
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsThis);
        //        List<string> legends = LegendFill(sheet, lsThis);
        //        SerieDataPointFill(sheet, lsThis, ldt, legends);
        //    }
        //    if (lsAc.Count > 0)
        //    {
        //        //累计曲线
        //        ISheet sheet = xssfworkbook.CreateSheet("累计变化曲线生成数据");
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsAc);
        //        List<string> legends = LegendFill(sheet, lsAc);
        //        SerieDataPointFill(sheet, lsAc, ldt, legends);
        //    }
        //}

    }
}