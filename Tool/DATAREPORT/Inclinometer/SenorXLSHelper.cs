﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;


namespace Tool
{
    public class SenorXLSHelper
    {

        public static XSSFWorkbook xssfworkbook;
        public static HSSFWorkbook hssfworkbook;
        public static int vsion = 0;//版本标志 
        public static void WriteToFile(string ldpath)
        {
            //Write the stream data of workbook to the root directory
            FileStream file = new FileStream(ldpath, FileMode.Create, FileAccess.ReadWrite);
            if (hssfworkbook != null)
            {
                hssfworkbook.Write(file);
            }
            else 
            {
            xssfworkbook.Write(file);
            }
            file.Close();
            
        }
       
        public static void HInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added. 
            Exception e = null;
            FileStream file = null; 
            try
            {
                file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
                hssfworkbook = new HSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                hssfworkbook.DocumentSummaryInformation = dsi;
                vsion = 0;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                hssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                file.Close();
                if (e != null)
                    throw e;
            }
        }
        
        public static void  XInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            Exception e = null;
            FileStream file = null; 
            try
            {
                file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
                xssfworkbook = new XSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                //xssfworkbook .DocumentSummaryInformation = dsi;
                vsion = 1;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                //xssfworkbook.SummaryInformation = si;
            }
            catch (Exception  ex)
            {
                e =ex;
            }
            finally
            {
                if(file!=null)
                file.Close();
                if (e != null)
                    throw e;
            }
        }
        public static void PublicCompatibleInitializeWorkbook(string tppath)
        {
            try
            {
               HInitializeWorkbook(tppath);
            }
            catch(Exception ex)
            {
                XInitializeWorkbook(tppath);
            }
        }
   

        public static IRow CreateRow(int len, ISheet sheet,int colCnt)
        {
            IRow row = sheet.CreateRow(len);
            for (int i = 0; i < colCnt; i++)
            {
                CellFontSet(row.CreateCell(i), "仿宋", 200, 16, true);
                CellBorderSet(row.CreateCell(i), GetBorderSetStyle(true, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                
            }
            return row;
        }



        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static  DataTable  CetDataFromXls()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("deep");
            dt.Columns.Add("this_disp");
            dt.Columns.Add("ac_disp");
            dt.Columns.Add("this_rap");
            dt.Columns.Add("time");
            dt.Columns.Add("point_name");
            dt.Columns.Add("previous_disp");
            dt.Columns.Add("previous_time");
            dt.Columns.Add("mtimes");
            ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("数据") : xssfworkbook.GetSheet("数据");
            
            int i = 0;
            DateTime time = new DateTime();
            DateTime previous_time = new DateTime();
            int mtimes = 0;
            string pointname = "";
            while (true)
            {
                try{
                if (sheet1.GetRow(i) == null) break;
                if (sheet1.GetRow(i).GetCell(0).CellType == CellType.STRING)
                {
                    //点名
                    if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("测斜管位移") != -1)
                    {
                        string pointnametr = sheet1.GetRow(i).GetCell(0).StringCellValue;
                        pointname = pointnametr.Substring(0, pointnametr.LastIndexOf("测斜管"));
                        i++;
                        continue;
                    }
                    //上次监测时间
                    if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("上次监测时间") != -1)
                    {
                        var timestr = sheet1.GetRow(i).GetCell(1).StringCellValue;
                        var a = sheet1.GetRow(i).GetCell(2).DateCellValue;
                        //var b = sheet1.GetRow(i).GetCell(3).StringCellValue;
                        //var c = sheet1.GetRow(i).GetCell(4).StringCellValue;
                        previous_time = Convert.ToDateTime(a);
                        i++;
                        continue;
                    }
                    //本次监测时间
                    if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("本次监测时间") != -1)
                    {
                        var timestr = sheet1.GetRow(i).GetCell(1).StringCellValue;
                        var a = sheet1.GetRow(i).GetCell(2).DateCellValue;
                        //var b = sheet1.GetRow(i).GetCell(3).StringCellValue;
                        //var c = sheet1.GetRow(i).GetCell(4).StringCellValue;
                        time = Convert.ToDateTime(a);
                        i++;
                        continue;
                    }
                    if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("第") != -1)
                    {
                        var mtimestr = sheet1.GetRow(i).GetCell(0).StringCellValue;
                        mtimes = Convert.ToInt32(mtimestr.Substring(mtimestr.IndexOf("第") + 1, mtimestr.LastIndexOf("次") - mtimestr.IndexOf("第")-1)); 
                        i++;
                        continue;
                    }
                    if (sheet1.GetRow(i).GetCell(0).StringCellValue != "深度")
                    {
                        i++;
                        continue;

                    }
                    else
                    {
                        i++; continue;
                    }
                    continue;
                }
                DataRow dr = dt.NewRow();
                dr[0] = sheet1.GetRow(i).GetCell(0).NumericCellValue;
                dr[1] = sheet1.GetRow(i).GetCell(3).NumericCellValue;
                dr[2] = sheet1.GetRow(i).GetCell(2).NumericCellValue;
                dr[3] = sheet1.GetRow(i).GetCell(4).NumericCellValue;
                dr[4] = time;
                dr[5] = pointname;
                dr[6] = sheet1.GetRow(i).GetCell(1).NumericCellValue;
                dr[7] = previous_time;
                dr[8] = mtimes;
                dt.Rows.Add(dr );
                }
                catch(Exception ex)
                {
                    ExceptionLog.ExceptionWrite("测斜数据excel文件导入出错,出错信息："+ex.Message);
                }
                i++;
                
            }

            return dt;


        }

        public static bool DataFromXlsCheck()
        {
            
            
            ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("数据") : xssfworkbook.GetSheet("数据");
            if (sheet1 == null) return false;
            int i = 0;
            DateTime time = new DateTime();
            DateTime previous_time = new DateTime();
            int resultPass = 0;
            int mtimes = 0;
            string pointname = "";
            while (true)
            {
                try
                {
                    if (sheet1.GetRow(i) == null) break;
                    if (sheet1.GetRow(i).GetCell(0).CellType == CellType.STRING)
                    {
                        //点名
                        if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("测斜管位移") != -1)
                        {
                            string pointnametr = sheet1.GetRow(i).GetCell(0).StringCellValue;
                            pointname = pointnametr.Substring(0, pointnametr.LastIndexOf("测斜管"));
                            i++;
                            resultPass++;
                            continue;
                        }
                        //上次监测时间
                        if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("上次监测时间") != -1)
                        {
                            var timestr = sheet1.GetRow(i).GetCell(1).StringCellValue;
                            var a = sheet1.GetRow(i).GetCell(2).DateCellValue;
                            //var b = sheet1.GetRow(i).GetCell(3).StringCellValue;
                            //var c = sheet1.GetRow(i).GetCell(4).StringCellValue;
                            previous_time = Convert.ToDateTime(a);
                            i++;
                            resultPass++;
                            continue;
                        }
                        //本次监测时间
                        if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("本次监测时间") != -1)
                        {
                            var timestr = sheet1.GetRow(i).GetCell(1).StringCellValue;
                            var a = sheet1.GetRow(i).GetCell(2).DateCellValue;
                            //var b = sheet1.GetRow(i).GetCell(3).StringCellValue;
                            //var c = sheet1.GetRow(i).GetCell(4).StringCellValue;
                            time = Convert.ToDateTime(a);
                            i++;
                            resultPass++;
                            continue;
                        }
                        if (sheet1.GetRow(i).GetCell(0).StringCellValue.IndexOf("第") != -1)
                        {
                            var mtimestr = sheet1.GetRow(i).GetCell(0).StringCellValue;
                            mtimes = Convert.ToInt32(mtimestr.Substring(mtimestr.IndexOf("第") + 1, mtimestr.LastIndexOf("次") - mtimestr.IndexOf("第") - 1));
                            i++;
                            resultPass++;
                            continue;
                        }
                        if (sheet1.GetRow(i).GetCell(0).StringCellValue != "深度")
                        {
                            i++;
                            
                            continue;

                        }
                        else
                        {
                            i++;
                            resultPass++;
                            continue;
                        }
                       
                    }
                    
                    
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("测斜数据excel文件导入出错,出错信息：" + ex.Message);
                }
                i++;

            }
            return resultPass == 5 ? true : false;

            


        }


        /// <summary>
        /// excel文件格式检查
        /// </summary>
        /// <param name="xlspath"></param>
        /// <param name="xmno"></param>
        /// <returns></returns>
        public static bool ProcessXlsDataImportCheck(string upzipfilePath, int xmno, out string mssg)
        {


            bool result = true;
            mssg = "";
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(upzipfilePath);

            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
                {
                    PublicCompatibleInitializeWorkbook(filename);
                    if (!DataFromXlsCheck())
                    {
                        mssg += string.Format("{0}导入失败 ",Path.GetFileName(filename) + "、{0}");
                        result = false;
                    }
                }
                else
                {
                    mssg += string.Format("{0}不是EXCEL可执行文件。 ", Path.GetFileName(filename) + "、{0}");
                    result = false;
                }

            }
            mssg = mssg.Replace("、{0}", "");
            return result;

        }


        //将单元格设置为有边框的
        public static void CellBorderSolid(ICell cell, ICellStyle style, bool wrap)
        {
            //ICellStyle style = xssfworkbook.CreateCellStyle();
            style.WrapText = wrap;
            cell.CellStyle = style;

        }


        //创建表格样式
        public static ICellStyle GetICellStyle()
        {
            ICellStyle style = null;
            if(xssfworkbook!=null)
            style = xssfworkbook.CreateCellStyle();
            else
            style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            style.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            return style;

        }
        //设置边框样式
        public static ICellStyle GetBorderSetStyle(bool solid, HorizontalAlignment h, VerticalAlignment v)
        {
            ICellStyle style = null;
            if (xssfworkbook != null)
                style = xssfworkbook.CreateCellStyle();
            else
                style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderLeft = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderRight = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderTop = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.Alignment = h;
            style.VerticalAlignment = v;
            return style;
        }

        //设置单元格的边框
        public static void CellBorderSet(ICell cell, ICellStyle style, bool wrap)
        {

            try
            {
                style.WrapText = wrap;
                cell.CellStyle = style;
            }
            catch (Exception ex)
            {
                string a = "";
            }
           
        }
        //设置字体样式
        public static void CellFontSet(ICell cell, string fontName,
            short fontWeight, short fontHeight, bool italic)
        {
            try
            {
                ICellStyle style = null;
                IFont font = null;
                if (xssfworkbook != null)
                {
                    style = xssfworkbook.CreateCellStyle();
                    font = xssfworkbook.CreateFont();
                }
                else
                {
                    style = hssfworkbook.CreateCellStyle();
                    font = hssfworkbook.CreateFont();
                }
                
                font.FontName = fontName;
                font.Boldweight = fontWeight;
                font.FontHeightInPoints = fontHeight;
                font.IsItalic = italic;
                
                style.SetFont(font);
                cell.CellStyle = style;
            }
            catch (Exception ex)
            {
                string a = "";
            }
        }
        //日期格式转换
        public static string DateFormatConverter(string dateStr)
        {
            if (dateStr == null || dateStr.Trim() == "") return "";
            char[] splits = { '-', ' ', '/', ':' };
            string[] dateStrs = dateStr.Split(splits);
            return dateStrs[0].Substring(dateStrs[0].IndexOf("20") + 2) + "年" + dateStrs[1] + "月" + dateStrs[2] + "日";
        }
        //填充曲线表
        /// <summary>
        /// 对不同监测项目的日期进行排序,生成X轴
        /// </summary>
        /// <param name="dt"></param>
        public static List<DateTime> GetDateTimeFromSeriesInDiffJcxm(ISheet sheet, List<serie> series)
        {
            List<DateTime> ldt = new List<DateTime>();
            List<string> lType = new List<string>();
            int i = 0, j = 0;
            for (i = 0; i < series.Count; i++)
            {

                for (j = 0; j < series[i].Pts.Length; j++)
                {
                    ldt.Add(series[i].Pts[j].Dt);
                }

            }
            ldt = ldt.Distinct().ToList();
            ldt.Sort();
            IRow row = sheet.CreateRow(0);
            for (i = 1; i < ldt.Count + 1; i++)
            {
                row.CreateCell(i).SetCellValue(ldt[i - 1]);
            }
            return ldt;
        }
        /// <summary>
        /// 填充图例
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        /// <returns></returns>
        public static List<string> LegendFill(ISheet sheet, List<serie> series)
        {
            List<string> legends = new List<string>();
            int i = 0;
            for (i = 1; i < series.Count + 1; i++)
            {
                IRow row = sheet.CreateRow(i);
                row.CreateCell(0).SetCellValue(series[i - 1].Name);
                legends.Add(series[i - 1].Name);
            }
            return legends;
        }
        /// <summary>
        /// 安装曲线数据点
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        public static void SerieDataPointFill(ISheet sheet, List<serie> series, List<DateTime> ldt, List<string> legends)
        {

            int y = 0, t = 0, posy = 0, posx = 0;
            for (y = 0; y < series.Count; y++)
            {
                IRow row = sheet.GetRow(y);
                posy = 1 + legends.IndexOf(series[y].Name);
                for (t = 0; t < series[y].Pts.Length; t++)
                {
                    posx = 1 + ldt.IndexOf(series[y].Pts[t].Dt);
                    row.CreateCell(posx).SetCellValue(series[y].Pts[t].Yvalue1);
                }
            }
        }
        //调用曲线填充函数
        //public void SeriesCreate(HttpContext context)
        //{
        //    //InitializeWorkbook("D:\\ReportExcel.xls");
        //    List<serie> ls = (List<serie>)context.Session["series"];
        //    //分拣出本次和累计
        //    List<serie> lsThis = new List<serie>();
        //    List<serie> lsAc = new List<serie>();
        //    foreach (serie s in ls)
        //    {
        //        if (s.Stype.IndexOf("累计") != -1)
        //        {
        //            lsAc.Add(s);
        //        }
        //        else
        //        {
        //            lsThis.Add(s);
        //        }

        //    }
        //    if (lsThis.Count > 0)
        //    {
        //        ISheet sheet = xssfworkbook.CreateSheet("本次变化曲线生成数据");
        //        //本次曲线
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsThis);
        //        List<string> legends = LegendFill(sheet, lsThis);
        //        SerieDataPointFill(sheet, lsThis, ldt, legends);
        //    }
        //    if (lsAc.Count > 0)
        //    {
        //        //累计曲线
        //        ISheet sheet = xssfworkbook.CreateSheet("累计变化曲线生成数据");
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsAc);
        //        List<string> legends = LegendFill(sheet, lsAc);
        //        SerieDataPointFill(sheet, lsAc, ldt, legends);
        //    }
        //}

    }
}