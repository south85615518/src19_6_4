﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace Tool.Inclinometer
{
    public class SenorChartHelper
    {
        public Application ThisApplication = null;
        public Workbooks m_objBooks = null;
        public _Workbook ThisWorkbook = null;
        public Worksheet xlSheet = null;
        public Worksheet xlDataSheet = null;
        #region 柱状图
        public void main()
        {
            //CreateChart();
            //ChartReportHelper("E:\\数据测试_表面位移_第1周期-第6周期数据.xls",6,"周期");
        }

        public void DeleteSheet()
        {
            foreach (Worksheet ws in ThisWorkbook.Worksheets)
                if (ws != ThisApplication.ActiveSheet)
                {
                    ws.Delete();
                }
            foreach (Chart cht in ThisWorkbook.Charts)

                cht.Delete();
        }
        private void AddDatasheet()
        {

            xlSheet = ThisWorkbook.Sheets.get_Item(1);
            //(Worksheet)ThisWorkbook.

            //    Worksheets.Add(Type.Missing, ThisWorkbook.ActiveSheet,

            //    Type.Missing, Type.Missing);
            xlSheet.Activate();


            xlSheet.Name = "曲线";

        }
        private void LoadData()
        {

            Random ran = new Random();

            for (int i = 1; i <= 12; i++)
            {

                xlSheet.Cells[i, 1] = i.ToString() + "月";

                xlSheet.Cells[i, 2] = ran.Next(2000).ToString();

            }

        }

        private void LoadDatadb()
        {



            Random ran = new Random();



            for (int i = 1; i <= 12; i++)
            {

                xlSheet.Cells[i, 1] = i + "周期";

                xlSheet.Cells[i, 2] = ran.Next(2000).ToString();

                xlSheet.Cells[i, 3] = ran.Next(1500).ToString();

                xlSheet.Cells[i, 4] = ran.Next(1500).ToString();

                xlSheet.Cells[i + 1, 8] = i + "周期";

                xlSheet.Cells[i + 1, 9] = ran.Next(2000).ToString();

                xlSheet.Cells[i + 1, 10] = ran.Next(1500).ToString();

                xlSheet.Cells[i + 1, 11] = ran.Next(1500).ToString();

            }

        }
        public void CreateChart(string pointname, int lenstart, int lenend, int breakRow, int chartIndex)
        {


            CreateThisChart(string.Format("{0}本次变化", pointname), lenstart, lenend, breakRow, chartIndex);

            //CreateAcChart(string.Format("{0}累计变化", pointname), lenstart, lenend, breakRow, ,chartIndex);
            CreateRapChart(string.Format("{0}本次变化速率", pointname), lenstart, lenend, breakRow, chartIndex);
        }

        public static double setupHei = 0;


        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateThisChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {

            //Chart xlChart = (Chart)ThisWorkbook.Charts.
            //                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);
            setupHei = 742 + (pageIndex - 1) * 732 + (odd * 5);
            //.Rows[8].PageBreak = 1;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 500, 350);

            Chart chart = shape.Chart;
            Range cellRange = (Range)xlSheet.get_Range("A" + lenstart, "D" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            

            //Range cellRange = (Range)xlSheet.get_Range("A3", "J" + (2 + rowcnt));
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //if (xlChart.HasTitle == false)
            //    xlChart.ChartTitle = "";
            //try
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       title /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            //catch
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            
            //chart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);

            string[] seriesName = { "ΔL" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;

            Series s = (Series)grp.SeriesCollection(1);
            s.Delete();
            s = (Series)grp.SeriesCollection(1);
            s.Delete();
            s = (Series)grp.SeriesCollection(1);
            s.Delete();
            grp.GapWidth = 20;

            grp.VaryByCategories = false;
            i = 1;
            s = (Series)grp.SeriesCollection(i);

            s.BarShape = XlBarShape.xlCylinder;
            s.HasDataLabels = true;
            s.Name = seriesName[i - 1];
            
            //s.MarkerSize = 20;
            s.Format.Line.Weight = 2;
            //s.Format.Line.Weight = 15;
            chart.Legend.Position = XlLegendPosition.xlLegendPositionBottom;
            SeriesMarkStyleSet(s, i);
            s.MarkerSize = 4;
            //xlChart.ChartTitle.Font.Size = 24;
            //xlChart.ChartTitle.Shadow = true;
            //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

            Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
            valueAxis.HasTitle = true;
            valueAxis.AxisTitle.Text = "位移(cm)";
            //valueAxis.AxisTitle.Orientation = -90;
            valueAxis.TickLabels.Font.Size = 12;
            valueAxis.TickLabels.Font.Bold = true;
            valueAxis.HasMajorGridlines = true;
            Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
            //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
            categoryAxis.HasTitle = true;
            categoryAxis.AxisTitle.Text = "深度(m)";
            categoryAxis.TickLabels.Font.Size = 10;
            categoryAxis.TickLabels.Font.Bold = false;
            categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
            categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
            categoryAxis.BaseUnitIsAuto = true;
            categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
            categoryAxis.HasMajorGridlines = true;
            //categoryAxis.MinimumScaleIsAuto = true;
            //categoryAxis.MaximumScaleIsAuto = true;
            chart.Rotation = 180;




        }
        /// <summary>
        /// 创建总表统计图         
        /// </summary>
        private void CreateTotalThisChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {

            //Chart xlChart = (Chart)ThisWorkbook.Charts.
            //                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);
            setupHei = 485 + (pageIndex - 1) * 475 + (odd * 5);
            //.Rows[8].PageBreak = 1;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 600, 350);

            Chart chart = shape.Chart;
            Range cellRange = (Range)xlSheet.get_Range("B" + lenstart, "G" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);


            //Range cellRange = (Range)xlSheet.get_Range("A3", "J" + (2 + rowcnt));
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //if (xlChart.HasTitle == false)
            //    xlChart.ChartTitle = "";
            //try
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       title /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            //catch
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);

            string[] seriesName = { "ΔN", "ΔE", "ΔZ" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            for (idx = 1; idx < 4; idx++)
            {
                if (idx < 4)
                {
                    Series s = (Series)grp.SeriesCollection(1);
                    s.Delete();
                }

            }
            grp.GapWidth = 20;

            grp.VaryByCategories = true;
            for (i = 1; i < lenend; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = true;
                s.Name = seriesName[i - 1];

                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }




        }
        public static void SeriesMarkStyleSet(Series s, int i)
        {
            switch (i)
            {
                case 1: s.MarkerStyle = XlMarkerStyle.xlMarkerStyleTriangle; break;
                case 2: s.MarkerStyle = XlMarkerStyle.xlMarkerStylePlus; break;
                case 3: s.MarkerStyle = XlMarkerStyle.xlMarkerStyleCircle; break;
            }
        }
        public static int odd = -1;
        /// <summary>
        /// 创建累计变化统计图         
        /// </summary>
        private void CreateAcChart(string title, int lenstart, int lenend ,int dtlen,double mindeep,int pageIndex,  string time, string previous_time, int chartIndex)
        {
            //xlSheet.Rows[breakRow+29].PageBreak = 1;
            
            setupHei = (pageIndex - 1) * 729 + odd * 5 * 0;
            //setupHei = setupHei + 729;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlXYScatterLines, 300, setupHei + 729 * 0.12963, 222, 728.4 * 31 / 54);
            odd *= -1;
            //setupHei = setupHei + 732;
            Chart chart = shape.Chart;
            Range cellRange = (Range)(xlDataSheet.get_Range("A" + lenstart, "C" + lenend));
            chart.SetSourceData(cellRange,  XlRowCol.xlColumns);
            chart.ChartArea.RoundedCorners = false;

            double absmax = 0;
            absmax = ExcelColMax(lenstart+1, 1, lenend - lenstart);
            //double maxdisp = ExcelColMax(lenstart+1, 3, lenend - lenstart);
            //absmax = maxpreviousdisp > maxdisp ? maxpreviousdisp : maxdisp;
            //cellRange = (Range)xlSheet.get_Range("D" + lenstart, "E" + lenend);
            //chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            absmax = absmax>50?absmax:50;
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //chart.PlotBy = XlRowCol.xlRows;
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}累计变化量曲线", xlSheet.Name);


            string[] seriesName = { previous_time, time };
            
            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);

            //Series s = (Series)grp.SeriesCollection(1);
            //s.Delete();
            //chart.Visible = XlSheetVisibility.
            grp.GapWidth = 20;
            
            grp.VaryByCategories = false;
            i = 1;
            for (i = 1; i < 3; i++)
            {
                var s = (Series)grp.SeriesCollection(i);

                
                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                s.Smooth = true;
                s.Name = seriesName[i - 1].Replace("/","一"); //Convert.ToDateTime(seriesName[i - 1]).ToShortDateString();
                s.MarkerSize = 4;
                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                chart.Legend.Position = XlLegendPosition.xlLegendPositionBottom;
                
                //SeriesMarkStyleSet(s, i);
                //s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.MinorTickMark = XlTickMark.xlTickMarkOutside;
                valueAxis.TickLabels.Font.Size = 10;
                valueAxis.TickLabels.Font.Bold = false;
                valueAxis.HasMajorGridlines = true;
                valueAxis.HasMinorGridlines = true;
                valueAxis.HasTitle = true;
                valueAxis.AxisTitle.Text = "深度(m)";
                valueAxis.CrossesAt =Convert.ToDouble(mindeep.ToString("0.00"));
                valueAxis.MinimumScaleIsAuto = false;
                valueAxis.MaximumScaleIsAuto = false;
                valueAxis.MaximumScale = 0;
                valueAxis.MinimumScale = Convert.ToDouble(mindeep.ToString("0.00")); ;
                valueAxis.MajorUnitIsAuto = false;
                valueAxis.MajorUnit = 1;
                valueAxis.MinorUnitIsAuto = false;
                valueAxis.MinorUnit = 0.5;
                valueAxis.MajorTickMark = XlTickMark.xlTickMarkOutside;
                valueAxis.MinorTickMark = XlTickMark.xlTickMarkOutside;
                
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.HasTitle = true;
                categoryAxis.AxisTitle.Text = "位移(mm)";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                categoryAxis.HasMinorGridlines = true;
                categoryAxis.CrossesAt = -500;
                categoryAxis.MinimumScaleIsAuto = false;
                categoryAxis.MaximumScaleIsAuto = false;
                categoryAxis.MaximumScale = 500;
                categoryAxis.MinimumScale = -500;//位移核心技术提升平台专业性
            }

        }
        /// <summary>
        /// 创建本次变化速率统计图         
        /// </summary>
        private void CreateRapChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {
            //xlSheet.Rows[breakRow+29].PageBreak = 1;
            //setupHei = setupHei + 732 + (odd * -5);
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 500, 350);
            odd *= -1;
            setupHei = setupHei + 732 + (odd * -5);
            Chart chart = shape.Chart;
            Range cellRange = (Range)xlSheet.get_Range("A" + lenstart, "E" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);

            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}累计变化量曲线", xlSheet.Name);


            string[] seriesName = { "ΔV" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);

            Series s = (Series)grp.SeriesCollection(1);
            s.Delete();
            s = (Series)grp.SeriesCollection(1);
            s.Delete();
            s = (Series)grp.SeriesCollection(1);
            s.Delete();
            s = (Series)grp.SeriesCollection(1);
            s.Delete();
            grp.GapWidth = 20;

            grp.VaryByCategories = false;
            i = 1;
            s = (Series)grp.SeriesCollection(i);

            s.BarShape = XlBarShape.xlCylinder;
            s.HasDataLabels = true;
            s.Name = seriesName[i - 1];

            //s.MarkerSize = 20;
            s.Format.Line.Weight = 2;
            //s.Format.Line.Weight = 15;
            chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
            SeriesMarkStyleSet(s, i);
            s.MarkerSize = 4;
            //xlChart.ChartTitle.Font.Size = 24;
            //xlChart.ChartTitle.Shadow = true;
            //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

            Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
            //valueAxis.AxisTitle.Orientation = -90;
            valueAxis.TickLabels.Font.Size = 12;
            valueAxis.TickLabels.Font.Bold = true;
            valueAxis.HasMajorGridlines = true;
            Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
            //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
            categoryAxis.TickLabels.Font.Size = 10;
            categoryAxis.TickLabels.Font.Bold = false;
            //categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
            //categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
            categoryAxis.BaseUnitIsAuto = true;
            categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
            categoryAxis.HasMajorGridlines = true;
            //categoryAxis.MinimumScaleIsAuto = true;
            //categoryAxis.MaximumScaleIsAuto = true;


            //xlSheet.Rows[breakRow + 58].PageBreak = 1;


        }


        public bool ChartReportHelper(string path, int rowcnt, string xAixsTitle, int lenstart, int lentend, int colCont, out int len)
        {
            len = 0;
            int a = 1;
            if (a > 0)
                return true;
            try
            {
                //path = "d:\\数据测试_表面位移_第1周期-第4周期数据.xls";
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add(path));


                ThisApplication.DisplayAlerts = false;


                xlSheet = ThisWorkbook.Sheets.get_Item(0);

                //this.CreateChart(xAixsTitle, lenstart, lenend, colcnt);
                ExceptionLog.ExceptionWrite("开始保存");
                ThisWorkbook.SaveAs(path, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                ExceptionLog.ExceptionWrite("保存成功");
                return true;

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                ExceptionLog.ExceptionWrite("数据表格生成曲线失败，失败原因" + ex.Message);
                return false;
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();
                //this.Close();
            }


        }

        public bool ChartSingleReportHelper(string path, ChartCreateEnviroment cce)
        {
            try
            {
                //path = "d:\\数据测试_表面位移_第1周期-第4周期数据.xls";
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add(path));

                ThisApplication.DisplayAlerts = false;


                //this.DeleteSheet();
                //this.AddDatasheet();
                //this.LoadDatadb();
                //this.WorkSheetCopy("D5","D5");
                //List<string> sheetsName = new List<string>();
                //foreach (Worksheet sheet in ThisWorkbook.Sheets)
                //{
                //    sheetsName.Add(sheet.Name);
                //}

                int n = 1;
                xlSheet = ThisWorkbook.Sheets.get_Item(1);
                ThisApplication.DisplayAlerts = false;


                //xlSheet = ThisWorkbook.Sheets.get_Item(sheetName);
                //this.CreateChart(xAixsTitle, lenstart, lenend, breakRow);
                this.CreateChart(cce.pointname, cce.cstartlen, cce.cendlen, cce.pageIndex, n);
                n++;


                //foreach (string sheetName in sheetsName)
                //{
                //    chartcreate(sheetName, rowcnt, xAixsTitle,n);
                //    break;
                //}

                ExceptionLog.ExceptionWrite("开始保存");
                ThisWorkbook.SaveAs(path, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                ExceptionLog.ExceptionWrite("保存成功");
                return true;

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                ExceptionLog.ExceptionWrite("数据表格生成曲线失败，失败原因" + ex.Message);
                return false;
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();

                //this.Close();
            }

            return true;
        }

        public bool ChartListReportHelper(string path, List<Tool.Inclinometer.SenorChartHelper.SenorChartCreateEnvironment> lce)
        {

            try
            {
                //path = "d:\\131565068128984778.xlsx";
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add(path));

                ThisApplication.DisplayAlerts = false;


                //this.DeleteSheet();
                //this.AddDatasheet();
                //this.LoadDatadb();
                //this.WorkSheetCopy("D5","D5");
                //List<string> sheetsName = new List<string>();
                //foreach (Worksheet sheet in ThisWorkbook.Sheets)
                //{
                //    sheetsName.Add(sheet.Name);
                //}

                int n = 1;
                xlSheet = ThisWorkbook.Sheets.get_Item(2);
                //ExcelHelperHandle excelhelper = new ExcelHelperHandle();
                //excelhelper.BorderSet(xlSheet, 1, 1, 100, 8);
                xlDataSheet = ThisWorkbook.Sheets.get_Item(8);
                ThisApplication.DisplayAlerts = false;
                int pagecnt = 0;
                int i = 0;
                ExcelHelperHandle excelhelper = new ExcelHelperHandle();
                int t = 0;
                for (t = 0; t < lce[lce.Count - 1].pageIndex; t++)
                {
                    excelhelper.BorderSet(xlSheet, t * 52+2,1, t * 52+52, 6);
                }
                   
                foreach (var cce in lce)
                {

                    //xlSheet = ThisWorkbook.Sheets.get_Item(sheetName);
                    //this.CreateChart(xAixsTitle, lenstart, lenend, breakRow);
                    //this.CreateChart(cce.pointname, cce.cstartlen, cce.cendlen, cce.pageIndex, n);

                    //CreateThisChart(string.Format("{0}{1}本次变化",  cce.pointname,cce.time), cce.cstartlen, cce.cendlen,cce.pageIndex,n);
                    
                    
                    pagecnt = cce.dtlen % 37 == 0 ? cce.dtlen / 37 : cce.dtlen / 37 + 1;
                    //int i=0;
                    for (i = 0; i < pagecnt;i++ )
                    {
                        ExceptionLog.ExceptionWrite("曲线:" + cce.previous_time + "," + cce.time);
                        CreateAcChart(string.Format("{0}累计变化", cce.pointname), cce.cstartlen, cce.cendlen, cce.dtlen, cce.mindeep, cce.pageIndex+i, cce.previous_time, cce.time, n);
                     }
                    //CreateRapChart(string.Format("{0}本次变化速率", cce.pointname, cce.time), cce.cstartlen, cce.cendlen, cce.pageIndex, n);
                


                    n++;

                }
                //foreach (string sheetName in sheetsName)
                //{
                //    chartcreate(sheetName, rowcnt, xAixsTitle,n);
                //    break;
                //}

                ExceptionLog.ExceptionWrite("开始保存");
                ThisWorkbook.SaveAs(path, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                ExceptionLog.ExceptionWrite("保存成功");
                return true;

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                ExceptionLog.ExceptionWrite("数据表格生成曲线失败，失败原因" + ex.Message);
                return false;
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();

                //this.Close();
            }

            return true;
        }

        public double ExcelColMax(int startrow, int colindex, int rows)
        {
            double tmp = Tool.com.MathHelper.MathAbs(xlDataSheet.Cells[startrow, colindex].Value), val = 0;
            int i = 0;
            
            for (i = 0; i < rows; i++)
            {
                if (i == 5) { string a = ""; }
                val = Tool.com.MathHelper.MathAbs(xlDataSheet.Cells[startrow + i, colindex].Value);
                tmp = Tool.com.MathHelper.MathAbs(val) > tmp ? val : tmp;
            }
            return tmp;
        }
        public double ExcelColMin(int startrow, int colindex, int rows)
        {
            double tmp = Tool.com.MathHelper.MathAbs(xlDataSheet.Cells[startrow, colindex].Value), val = 0;
            int i = 0;
            for (i = 0; i < rows; i++)
            {
                val = Tool.com.MathHelper.MathAbs(xlDataSheet.Cells[startrow + i, colindex].Value);
                tmp = Tool.com.MathHelper.MathAbs(val) < Tool.com.MathHelper.MathAbs(tmp) ? val : tmp;
            }
            return tmp;

        }



        #endregion
        #region 多点多周期

        public void CreateChart(string pointname, int lenstart, int lenend, int breakRow, int chartIndex, int colCnt)
        {


            CreateThisChart(string.Format("{0}本次变化", pointname), lenstart, lenend, breakRow, chartIndex);

            //CreateAcChart(string.Format("{0}累计变化", pointname), lenstart, lenend, breakRow, chartIndex);
            CreateRapChart(string.Format("{0}本次变化速率", pointname), lenstart, lenend, breakRow, chartIndex);
        }

        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateThisChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex, int colcnt)
        {

            //Chart xlChart = (Chart)ThisWorkbook.Charts.
            //                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);
            setupHei = 742 + (pageIndex - 1) * 732 + (odd * 5);
            //.Rows[8].PageBreak = 1;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 500, 350);

            Chart chart = shape.Chart;

            Range cellRange = (Range)xlSheet.get_Range("A" + lenstart, Convert.ToChar(98 + colcnt).ToString() + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);


            //Range cellRange = (Range)xlSheet.get_Range("A3", "J" + (2 + rowcnt));
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //if (xlChart.HasTitle == false)
            //    xlChart.ChartTitle = "";
            //try
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       title /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            //catch
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);

            string[] seriesName = { "ΔL" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            i = 1;


            grp.GapWidth = 20;
            Series s = null;
            grp.VaryByCategories = false;
            for (i = 1; i < colcnt + 1; i++)
            {
                s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = true;
                s.Name = seriesName[0];

                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }




        }

        /// <summary>
        /// 创建累计变化统计图         
        /// </summary>
        private void CreateAcChart(string title, int lenstart, int lenend,double mindeep ,int pagestartlen, int pageendlen, int colcnt)
        {
            //xlSheet.Rows[breakRow+29].PageBreak = 1;
            
            setupHei = setupHei + 732 + (odd * -5);
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 500, 350);
            odd *= -1;
            setupHei = setupHei + 732 + (odd * -5);
            Chart chart = shape.Chart;
            Range cellRange = (Range)xlSheet.get_Range("A" + lenstart, Convert.ToChar(98 + colcnt).ToString() + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);

            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}累计变化量曲线", xlSheet.Name);


            string[] seriesName = { "ΣΔN" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            grp.GapWidth = 20;
            Series s = null;
            grp.VaryByCategories = false;
            for (i = 1; i < colcnt + 1; i++)
            {
                s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = true;
                s.Name = seriesName[0];

                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }


        }
        /// <summary>
        /// 创建本次变化速率统计图         
        /// </summary>
        private void CreateRapChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex, int colcnt)
        {
            //xlSheet.Rows[breakRow+29].PageBreak = 1;
            //setupHei = setupHei + 732 + (odd * -5);
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 500, 350);
            odd *= -1;
            setupHei = setupHei + 732 + (odd * -5);
            Chart chart = shape.Chart;
            Range cellRange = (Range)xlSheet.get_Range("A" + lenstart, Convert.ToChar(98 + colcnt).ToString() + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);

            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}累计变化量曲线", xlSheet.Name);


            string[] seriesName = { "ΔV" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            grp.GapWidth = 20;
            Series s = null;
            grp.VaryByCategories = false;
            for (i = 1; i < colcnt + 1; i++)
            {
                s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = true;
                s.Name = seriesName[0];

                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }


        }

        public bool ChartListReportHelper(string path, List<ChartCreateEnviroment> lce, int colCnt)
        {

            try
            {
                //path = "d:\\数据测试_表面位移_第1周期-第4周期数据.xls";
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add(path));

                ThisApplication.DisplayAlerts = false;


                //this.DeleteSheet();
                //this.AddDatasheet();
                //this.LoadDatadb();
                //this.WorkSheetCopy("D5","D5");
                //List<string> sheetsName = new List<string>();
                //foreach (Worksheet sheet in ThisWorkbook.Sheets)
                //{
                //    sheetsName.Add(sheet.Name);
                //}

                int n = 1;
                xlSheet = ThisWorkbook.Sheets.get_Item(1);
                ThisApplication.DisplayAlerts = false;
                foreach (ChartCreateEnviroment cce in lce)
                {

                    //xlSheet = ThisWorkbook.Sheets.get_Item(sheetName);
                    //this.CreateChart(xAixsTitle, lenstart, lenend, breakRow);
                    this.CreateChart(cce.pointname, cce.cstartlen, cce.cendlen, cce.pageIndex, n, colCnt);
                    n++;

                }
                //foreach (string sheetName in sheetsName)
                //{
                //    chartcreate(sheetName, rowcnt, xAixsTitle,n);
                //    break;
                //}

                ExceptionLog.ExceptionWrite("开始保存");
                ThisWorkbook.SaveAs(path, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                ExceptionLog.ExceptionWrite("保存成功");
                return true;

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                ExceptionLog.ExceptionWrite("数据表格生成曲线失败，失败原因" + ex.Message);
                return false;
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();

                //this.Close();
            }

            return true;
        }


        public class SenorChartCreateEnvironment : ChartCreateEnviroment
        {
            public string time { get; set; }
            public string previous_time { get; set; }
            public int dtlen { get; set; }
            public double mindeep { get;set;}
            public SenorChartCreateEnvironment(int cstartlen, int cendlen,int dtlen ,double mindeep ,string pointname, int pageIndex, string time, string previous_time)
            {
                this.cstartlen = cstartlen;
                this.cendlen = cendlen;
                this.pointname = pointname;
                this.pageIndex = pageIndex;
                this.time = time;
                this.previous_time = previous_time;
                this.mindeep = mindeep;
                this.dtlen = dtlen;
            }
        }

        #endregion
    }
}
