﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {
        
        public  void DTUReportTemplateCreate(SenorReport senorReport,string xmname, string[] tabName, DataTable dt,int len)
        {


            DataView dv = new DataView(dt);
            dv.Sort = " predeep asc ";
            List<double> previousVal = new List<double>();//new List<double>();
            previousVal.Add(Convert.ToDouble(dv[0].Row["predeep"].ToString()));
            previousVal.Add(Convert.ToDouble(dv[dv.Count - 1].Row["predeep"].ToString()));
            dv.Sort = " acdeep asc ";
            List<double> acDisp = new List<double>();
            acDisp.Add(Convert.ToDouble(dv[0].Row["acdeep"].ToString()));
            acDisp.Add(Convert.ToDouble(dv[dv.Count - 1].Row["acdeep"].ToString()));
            dv.Sort = " thisdeep asc ";
            List<double> thisDisp = new List<double>();
            thisDisp.Add(Convert.ToDouble(dv[0].Row["thisdeep"].ToString()));
            thisDisp.Add(Convert.ToDouble(dv[dv.Count - 1].Row["thisdeep"].ToString()));
            dv.Sort = " deep asc ";
            List<double> deep = new List<double>();
            deep.Add(Convert.ToDouble(dv[0].Row["deep"].ToString()));
            deep.Add(Convert.ToDouble(dv[dv.Count - 1].Row["deep"].ToString()));
            dv.Sort = " rap asc ";
            List<double> rap = new List<double>();
            rap.Add(Convert.ToDouble(dv[0].Row["rap"].ToString()));
            rap.Add(Convert.ToDouble(dv[dv.Count - 1].Row["rap"].ToString()));
            string pointname = dt.Rows[0].ItemArray[0].ToString();
            int pagecnt = dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            
            for (cnt = 0; cnt < pagecnt; cnt++)
            {
                //添加标题
                RowFormat(len, 5);
                RowFormat(len+1, 5);
                RowFormat(len+2, 5);
                RowFormat(len+3, 5);
                RowFormat(len+4, 5);
                //this.BorderSet(len, 1, len, 6);
                this.ExcelMergedRegion(len, 1, len, 6);
                
                this.ExcelWrite(len, 1, string.Format("{0}投入式水位计监测报表 {1}", dt.Rows[0].ItemArray[0],DateTime.Now));
                len++;
                int k = 0;
                
                this.ExcelMergedRegion(len, 1, len, 3);
                this.ExcelWriteTitle(len, 1,"工程名:"+senorReport.xmname , true);
                this.ExcelMergedRegion(len, 4, len, 6);
                this.ExcelWriteTitle(len, 4, "项目名称:地下水位", true);
                this.ExcelMergedRegion(len+1, 1, len+1, 6);
                this.ExcelWriteTitle(len+1, 1, "工程地址:"+senorReport.addr, true);
                this.ExcelMergedRegion(len+2, 1, len+2, 3);
                this.ExcelWriteTitle(len+2, 1, "监测单位:" + senorReport.jcdw, true);
                this.ExcelMergedRegion(len+2, 4, len+2, 6);
                this.ExcelWriteTitle(len+2, 4, "委托单位:" + senorReport.wtdw, true);
                this.ExcelMergedRegion(len + 3, 1, len + 3, 3);
                this.ExcelWriteTitle(len+3, 1, "开始时间:" + dt.Rows[0].ItemArray[6].ToString(), true);
                this.ExcelMergedRegion(len + 3, 4, len + 3, 6);
                this.ExcelWriteTitle(len+3, 4, "结束时间:" + dt.Rows[dt.Rows.Count - 1].ItemArray[6].ToString(), true);
                len += 4;
                ////this.BorderSet(len, 1, len, 1);
                ////this.BorderSet(len+1, 1, len+1, 1);
                int rangerowstart = len;
                this.ExcelMergedRegion(len, 1, len+1, 1);
                this.ExcelWrite(len, 1, "时间");
                this.ExcelWrite(len, 2, "本次水位值");
                this.ExcelWrite(len + 1, 2, "m");
                this.ExcelWrite(len, 3, "上次水位");
                this.ExcelWrite(len + 1, 3, "m");
                this.ExcelWrite(len, 4, "本次变化");
                this.ExcelWrite(len + 1, 4, "mm");
                this.ExcelWrite(len, 5, "累计变化");
                this.ExcelWrite(len + 1, 5, "mm");
                this.ExcelWrite(len, 6, "变化速率");
                this.ExcelWrite(len + 1, 6, "mm/d");
                len+=2;
                
                int i = 0;
                try
                {
                    for (; i < 39; i++)
                    {

                        if (39 * cnt + i > dt.Rows.Count - 1)
                        {
                            RowFormat(len, 5);
                            len++;
                            continue;
                        }
                        int j = 0, t = 1;
                        for (j = 0; j < dt.Columns.Count-2; j++)
                        {


                            if (j == 0) { continue; }
                            if (j == 1) ExcelWrite(len, 1, dt.Rows[39  * cnt + i].ItemArray[6]);
                            else
                            ExcelWrite(len,t, dt.Rows[39 * cnt + i].ItemArray[t-1]);
                            t++;

                        }
                        len++;

                    }
                    RowFormat(len, 5);
                    RowFormat(len+1, 5);
                    RowFormat(len+2, 5);
                    RowFormat(len+3, 5);
                    RowFormat(len+4, 5);
                    RowFormat(len+5, 5);
                    RowFormat(len+6, 5);
                    this.ExcelWrite(len, 1, "最大值");
                    this.ExcelWrite(len, 2, previousVal[1]);
                    this.ExcelWrite(len, 3, deep[1] );
                    this.ExcelWrite(len, 4, thisDisp[1]);
                    this.ExcelWrite(len, 5, acDisp[1]);
                    this.ExcelWrite(len, 6, rap[1]);
                    this.ExcelWrite(len+1, 1, "最小值");
                    this.ExcelWrite(len+1, 2, previousVal[0]);
                    this.ExcelWrite(len+1, 3, deep[0]);
                    this.ExcelWrite(len+1, 4, thisDisp[0]);
                    this.ExcelWrite(len+1, 5, acDisp[0]);
                    this.ExcelWrite(len+1, 6, rap[0]);
                    this.ExcelWrite(len+2, 1, "预警情况");
                    this.ExcelWrite(len + 3, 1, "一级");
                    this.ExcelWrite(len + 4, 1, "二级");
                    this.ExcelWrite(len + 5, 1, "三级");
                    this.ExcelMergedRegion(len+4, 6, len + 6, 6);
                    this.ExcelWrite(len + 4, 6, "是否产生\r\n红黄预警：");
                    this.ExcelMergedRegion(len + 6, 1, len + 6, 5);
                    this.ExcelWrite(len + 6, 1, "备注:1、表中变形值,\" - \"表示水位上升，\" + \"表示水位下降.");
                    len += 7;
                }
                catch (Exception ex)
                {
                    string a = "";
                }
                this.BorderSet(rangerowstart, 1, len - 1, 6);

            }

        }
       
    }
}
