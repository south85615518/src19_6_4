﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;


namespace Tool
{
    public partial class ExcelHelperHandle
    {
        public  void McuReportTemplateCreate(string xmname, string[] tabName, DataTable dt,int len)
        {

            string pointname = dt.Rows[0].ItemArray[0].ToString();
            int pagecnt = dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            int l = dt.Rows.Count;
            int cnt = 0;
            len++;
            int rangerowstart = len;
            for (cnt = 0; cnt < pagecnt; cnt++)
            {
                //添加标题
                //this.BorderSet(len, 1, len, 3);
                this.ExcelMergedRegion(len, 1, len, 3);

                //this.ExcelWrite(len, 1, string.Format("{0}浸润线监测报表   报表编号{4}   {1}年{2}月{3}日    点名:{5}", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo, pointname), true);
                this.ExcelWrite(len, 1, string.Format("{0}浸润线监测报表   报表编号{4}   {1}年{2}月{3}日    点名:{5}", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo, pointname));
                len++;
                int k = 0;
                
                //this.BorderSet(len, 1, len, 1);
                this.ExcelWrite(len, 1, "时间");
                //this.BorderSet(len, 2, len,2);
                this.ExcelWrite(len, 2, "水位(m)");
                //this.BorderSet(len, 3, len, 3);
                this.ExcelWrite(len, 3, "温度(°C)");

                k = 0;
                len++;
                int i = 0;
                try
                {
                    for (; i < 52; i++)
                    {

                        if (52 * cnt + i > dt.Rows.Count - 1)
                        {
                            RowFormat(len, 2);
                            len++;
                            continue;
                        }
                        int j = 0, t = 1;
                        for (j = 0; j < dt.Columns.Count; j++)
                        {


                            if (j == 0) continue;
                            if (j == 1) ExcelWrite(len, 1, dt.Rows[51 * cnt + i].ItemArray[3]);
                            else
                            ExcelWrite(len,t, dt.Rows[51 * cnt + i].ItemArray[t-1]);
                            t++;

                        }
                        len++;

                    }
                }
                catch (Exception ex)
                {
                    string a = "";
                }
                this.BorderSet(rangerowstart, 1, len - 1, 3);

            }

        }
       
    }
}
