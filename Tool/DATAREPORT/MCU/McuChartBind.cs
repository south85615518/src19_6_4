﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;

namespace Tool.DATAREPORT.MCU
{
    public class McuChartBind
    {
        public    int odd = -1;
        public    int setupHei = 0;
        public ExcelHelperHandle ExcelHelper { get; set; }
        public    void ChartBind(List<ChartCreateEnviroment> lce)
        {
            
            foreach (ChartCreateEnviroment cce in lce)
            {
                setupHei = 730 + (cce.pageIndex - 1) * 730;
                //ReportChartCreate(cce.cstartlen, cce.cendlen, string.Format("{0}浸润线曲线图", cce.pointname),"A","C",new string[2]{ "水位","温度"},new int[2]{1,2},XlMarkerStyle.xlMarkerStyleNone);
                ReportChartCreate(cce.cstartlen, cce.cendlen, string.Format("{0}浸润线曲线图", cce.pointname), "A", "C", new string[2] { "水位", "温度" }, new int[2] { 1, 2 }, XlMarkerStyle.xlMarkerStyleNone);
            }
        }
        /// <summary>
        /// GPS数据曲线生成
        /// </summary>
        /// <param name="cce"></param>
        /// <param name="setupHei"></param>
        /// <param name="oddinterval"></param>
        /// <param name="title"></param>
        /// <param name="seriesName"></param>
        /// <param name="indexRange"></param>
        /// <param name="markStyle"></param>
        public    void ReportChartCreate(int cstartlen, int cendlen, string title, string colstart, string colend, string[] seriesName, int[] indexRange, XlMarkerStyle markStyle)
        {
            
            
            Chart chart = ExcelHelper.ChartAdd(XlChartType.xlLine, 0, setupHei, 490, 300);
            ExcelHelper.ExcelChartDataBind(chart, colstart + cstartlen, colend + cendlen);
            ExcelHelper.SeriesCharacterCreate(chart, title, seriesName, indexRange, markStyle,true);
        }

    }
}
