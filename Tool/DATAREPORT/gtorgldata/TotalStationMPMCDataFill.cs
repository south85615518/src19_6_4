﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.TotalStation
{
    public partial class TotalStationDataFill
    {
     
        public    void gtOrglDataFillMPMC(string xmname,string datatype ,string tabHead,DataTable oregiondatatable,string xlpath)
        {

            List<string> tabName = tabHead.Split(',').ToList();
            int i = 0;
            //ExcelHelper.DateFormat(ExcelHelper.xSheet, 1, "yy/mm/dd hh:MM");

            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 1, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 2, 14);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 3, 14);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 14);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 14);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 16);
           
            ExcelHelper.DateFormat(ExcelHelper.xSheet, 9, "yy/mm/dd hh:MM");
            //for (i = 0; i < lrf.Count; i++)
            //{
            ExcelHelper.gtorgldataReportTemplateCreateMPMC(xmname, datatype, tabName, xlpath, oregiondatatable);
            //}
            
        }
    }
}
