﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.TotalStation
{
    public partial class TotalStationDataFill
    {

        public void GTsensordataData_timespanFillMPSC(string xmname, string tabHead, List<string> pointnamelist, List<DataTable> datatablelist, string reportname, DataTable origldatatable, string timeunitname, Tool.DATAREPORT.TotalStation.MPSC.GT.reportparams reportparams)
        {
            
            string[] tabName = tabHead.Split(',');
            int i = 0;
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 1, 10);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 2, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 3, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 12);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 7, 12);
            //for (i = 0; i < pointnamelist.Count; i++)
            //{
            //    ExceptionLog.ReportLineWrite(string.Format("开始打印{0}第{2}个周期周期{1}数据...", xmname, lrf[i].dt.Rows[0].ItemArray[1].ToString(), (i+1) + "/" + lrf.Count), reportname);
            //    ExcelHelper.TotalStationReportTemplateCreateMPSC_GT_DAYReport_WithoutSettlement(xmname, tabName, lrf[i].dt, lrf[i].index);
            //}
            DataTable dttimespan = ExcelHelper.GTsensordataTimeSpanThisDataTable(datatablelist[0], datatablelist[datatablelist.Count - 1]);
            
            int datarowscnt = reportparams.alarmmess.Count == 0 ? 36 : 35;
            List<DataTable> dtlist = new List<DataTable>();

            //for (i = 0; i < datatablelist.Count; i++)
            //{
                
            //        //if (i == datatablelist.Count - 1) dtlist = new List<DataTable>();
            //        dtlist.Add(datatablelist[i]);
            //        ExcelHelper.GTsensordata_timespanReportTemplateCreateMPSC(xmname, pointnamelist, dtlist, origldatatable, reportname, origldatatable.Rows.Count, (pointnamelist.Count / datarowscnt + 1) * 54 * i, dttimespan, timeunitname, reportparams, datarowscnt);
            //        dtlist = new List<DataTable>();
            //}

            for (i = 0; i < datatablelist.Count; i++)
            {
                if (i % 3 == 2)
                {
                    //if (i == datatablelist.Count - 1) dtlist = new List<DataTable>();
                    dtlist.Add(datatablelist[i]);
                    ExcelHelper.GTsensordata_timespanReportTemplateCreateMPSC(xmname, pointnamelist, dtlist, origldatatable, reportname, origldatatable.Rows.Count, (pointnamelist.Count / datarowscnt + 1) * 54 * (i / 3), dttimespan, timeunitname, reportparams, datarowscnt);
                    dtlist = new List<DataTable>();
                }
                else
                {

                    dtlist.Add(datatablelist[i]);
                }



            }
            ExcelHelper.GTsensordata_timespanReportTemplateCreateMPSC(xmname, pointnamelist, dtlist, origldatatable, reportname, origldatatable.Rows.Count, (pointnamelist.Count / datarowscnt + 1) * 54 * (i / 3), dttimespan, timeunitname, reportparams, datarowscnt);
            
        }
    }
}
