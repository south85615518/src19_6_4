﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.TotalStation
{
    public partial class TotalStationDataFill
    {

        public void DataFillMPSC_GT_WithoutSettlement(string xmname, string tabHead, List<ReportFillEnviroment> lrf, string reportname, Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams reportparams)
        {
            
            string[] tabName = tabHead.Split(',');
            int i = 0;
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 1, 10);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 2, 14);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 3, 14);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 8);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 14);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 14);
            ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 7, 8);
            int sumcnt = 0; 
            for (i = 0; i < lrf.Count; i++)
            {
                ExceptionLog.ReportLineWrite(string.Format("开始打印{0}第{2}个周期周期{1}数据...", xmname, lrf[i].dt.Rows[0].ItemArray[1].ToString(), (i+1) + "/" + lrf.Count), reportname);
                ExcelHelper.TotalStationReportTemplateCreateMPSC_GT_WithoutSettlement(xmname, tabName, lrf[i].dt, (sumcnt % 36 == 0 ? (sumcnt / 36)*54 : (sumcnt / 36+1)*54), reportparams);
                sumcnt += lrf[i].dt.Rows.Count;
            }
            
        }
    }
}
