﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;

namespace Tool.DATAREPORT.TotalStation
{
    public partial class TotalStationChartBind
    {
        
        public  void ChartBindMPSC(List<ChartCreateEnviroment> lce)
        {
            double i = -2.5;
            int index = 0;
            foreach (ChartCreateEnviroment cce in lce)
            {
                if (cce.pageIndex < 5) i = -1;
                else
                    i = -2.5;
                setupHei = (cce.pageIndex) * (475 + i);
                ReportChartCreateMPSC(cce.cstartlen, cce.cendlen, string.Format("{0}累计变化", cce.pointname), "A", "G", new string[3] { "ΣΔX", "ΣΔY", "ΣΔZ" }, new int[3] { 4, 5, 6 }, XlMarkerStyle.xlMarkerStyleNone);
                setupHei = (cce.pageIndex +1)* (475 + i );
                ReportChartCreateMPSC(cce.cstartlen, cce.cendlen, string.Format("{0}本次变化", cce.pointname),"A","D",new string[3]{ "ΔX","ΔY","ΔZ"},new int[3]{1,2,3},XlMarkerStyle.xlMarkerStyleNone);

                
                //i = i + -2;
               index++;
            }
        }
        /// <summary>
        /// GPS数据曲线生成
        /// </summary>
        /// <param name="cce"></param>
        /// <param name="setupHei"></param>
        /// <param name="oddinterval"></param>
        /// <param name="title"></param>
        /// <param name="seriesName"></param>
        /// <param name="indexRange"></param>
        /// <param name="markStyle"></param>
        public  void ReportChartCreateMPSC(int cstartlen, int cendlen, string title, string colstart, string colend, string[] seriesName, int[] indexRange, XlMarkerStyle markStyle)
        {
            
            
            Chart chart = ExcelHelper.ChartAdd(XlChartType.xlLine, 0, setupHei, 535, 350);
            ExcelHelper.ExcelChartDataBind(chart, colstart + cstartlen, colend + cendlen);
            ExcelHelper.SeriesCharacterCreate(chart, title, seriesName, indexRange, markStyle,false);
        }

    }
}
