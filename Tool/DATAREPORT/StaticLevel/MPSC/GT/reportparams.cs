﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tool.DATAREPORT.TotalStation.MPSC.GT
{
    public class reportparams
    {
        public string xmaddress { get; set; }
        public string firstalrmacdisp { get; set; }
        public string firstalrmthisdisp { get; set; }
        public string secalrmacdisp { get; set; }
        public string secalrmthisdisp { get; set; }
        public List<string> alarmmess { get; set; }
        public reportparams()
        {
            
        }

    }
}
