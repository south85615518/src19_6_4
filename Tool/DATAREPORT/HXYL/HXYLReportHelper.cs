﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Data;
using NPOI.SS.UserModel;
using NFnet_MODAL;
using NPOI.SS.Util;
using System.IO;
using NPOI.HPSF;
using System.Data.Odbc;
using NPOI.XSSF.UserModel;


namespace Tool
{
    public class HXYLReportHelper
    {

        public static XSSFWorkbook xssfworkbook;
        public static HSSFWorkbook hssfworkbook;
        public static int vsion = 0;//版本标志 
        public static void WriteToFile(string ldpath)
        {
            //Write the stream data of workbook to the root directory
            FileStream file = new FileStream(ldpath, FileMode.Create, FileAccess.ReadWrite);
            if (hssfworkbook != null)
            {
                hssfworkbook.Write(file);
            }
            else
            {
                xssfworkbook.Write(file);
            }
            file.Close();

        }

        public static void HInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added. 
            Exception e = null;
            FileStream file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
            try
            {
                hssfworkbook = new HSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                hssfworkbook.DocumentSummaryInformation = dsi;
                vsion = 0;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                hssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (e != null)
                    throw e;
            }
        }

        public static void XInitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            Exception e = null;
            FileStream file = new FileStream(tppath, FileMode.OpenOrCreate, FileAccess.Read);
            try
            {
                xssfworkbook = new XSSFWorkbook(file);
                //删除所有的工作簿
                int i = 0;
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI Team";
                //xssfworkbook .DocumentSummaryInformation = dsi;
                vsion = 1;
                //create a entry of SummaryInformation
                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Subject = "NPOI SDK Example";
                //xssfworkbook.SummaryInformation = si;
            }
            catch (Exception ex)
            {
                e = ex;
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (e != null)
                    throw e;
            }
        }
        public static void PublicCompatibleInitializeWorkbook(string tppath)
        {
            try
            {
                HInitializeWorkbook(tppath);
            }
            catch (Exception ex)
            {
                XInitializeWorkbook(tppath);
            }
        }
        /// <summary>
        /// 单点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValSPMC(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, string et, int index, string xlsPath, out List<ChartCreateEnviroment> lce)
        {



            int len = 0;
            List<DataTable> ldt = Tool.DTUDataTableHelper.ProcessDataTableSplitSPMT(originaldt);



            //int t = 0;
            lce = new List<ChartCreateEnviroment>();

            string[] tabName = tabHead.Split(',');
            //ChartHelper chartHelper = new ChartHelper();
            //InitializeWorkbook(xlsPath);
            int cnt = 0;
            int datastartlen = 0;
            int reportNo = new Random().Next(9999);

            int pageindexlen = 0;
            foreach (DataTable dt in ldt)
            {

                //InitializeWorkbook(xlsPath);
                //if(vsion == 0)
                ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("雨量") : xssfworkbook.GetSheet("雨量");
                ISheet sheet = vsion == 0 ? hssfworkbook.GetSheet("Sheet1") : xssfworkbook.GetSheet("Sheet1");
                string tmp = "";//st;
                //tmp =  "点名:"+dt.Rows[0].ItemArray[0].ToString();
                //if (len > 0) len--;
                //pageindexlen = len + 9;
                DataView dv = new DataView(dt);
                sheetName = dt.Rows[0].ItemArray[0].ToString();

                if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
                sheet1.Header.Left = "南方测绘";
                sheet1.Header.Right = DateTime.Now.ToString();
                sheet1.Footer.Center = "第&p页";
                sheet1.PrintSetup.PaperSize = 9;
                //sheet1.Workbook.SetRepeatingRowsAndColumns(0, 0, dt.Columns.Count, len+1, len+2);
                sheet1.FitToPage = false;
                sheet1.PrintSetup.Scale = 100;
                sheet1.HorizontallyCenter = true;
                int k = 0;
                //报表头部
                ICellStyle styleCell = GetICellStyle();
                ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);

                sheet1.SetColumnWidth(0, 15 * 256);
                sheet1.SetColumnWidth(1, 20 * 256);
                sheet1.SetColumnWidth(2, 40 * 256);
                
               

                //int startlen = 0;
                pageindexlen = len + 7;
                SenorChartHelper.SenorChartCreateEnvironment senorChartCreateEnvironment = null;
                //Tool.DTUReportHelper.dtualarm alarmmodel = (from m in ltsi where m.pointname == sheetName select m).ToList()[0];
                ReportTemplateCreate(xmname,tabName, dt, sheet1, sheet, sheetName, len, datastartlen);
                len += (dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1) * 54;
                datastartlen += dt.Rows.Count + 1;



                int pageIndex = len % 54 == 0 ? len / 54 : len / 54 + 1;
                len = len % 54 == 0 ? (len / 54) * 54 + 108 : (len / 54) * 54 + 162;
                lce.Add(new ChartCreateEnviroment(datastartlen + 1 - dt.Rows.Count, datastartlen, sheetName, pageIndex));
                //cnt++;
                //len = (len - cnt * 52) % 52 == 0 ? len : ((len - cnt * 52) / 52 + 1) * 52 + cnt * 52;
                //cnt--;

                //sheet1.SetRowBreak(len+29);
                //sheet1.SetRowBreak(len+58);
                //sheet1.CreateRow(len + 1).CreateCell(0).SetCellValue("从此处中断");
                //sheet1.PrintSetup.Landscape = false;
                //ReportHelper.WriteToFile(xlsPath);
                //chartHelper.ChartReportHelper(xlsPath, sheetName, cstartlen, cendlen, pageIndex);
                //ExceptionLog.ExceptionWrite("本次时间" + dt.Rows[0].ItemArray[5].ToString() + "累计时间" + dt.Rows[0].ItemArray[5].ToString());
                //lce.Add(new Tool.SenorChartHelper.SenorChartCreateEnvironment(datastartlen - 2 * dt.Rows.Count, datastartlen, dt.Rows.Count, Convert.ToDouble(dt.Rows[0].ItemArray[2].ToString()), sheetName, pageIndex, dt.Rows[0].ItemArray[9].ToString(), dt.Rows[0].ItemArray[8].ToString()));
                //lce.Add(senorChartCreateEnvironment);
                cnt++;

            }
            //sheet1.ForceFormulaRecalculation = true;
            //xssfworkbook.RemoveName("表面位移");
            ExceptionLog.ExceptionWrite("单点多周期表格生成");





        }


        public static void ReportTemplateCreate( string xmname,string[] tabName, DataTable dt, ISheet sheet1, ISheet sheet, string sheetName, int len, int datastartlen)
        {


            int pagecnt = dt.Rows.Count % 50 == 0 ? dt.Rows.Count / 50 : dt.Rows.Count / 50 + 1;
            int tmplen = len;
            int reportNo = new Random().Next(9999);
            //List<DataTable> ldt = Tool.DataTableHelper.ProcessDataTableSplitSPMC(originaldt);
            ////int t = 0;
            //lce = new List<ChartCreateEnviroment>();

            //string[] tabName = tabHead.Split(',');
            ChartHelper chartHelper = new ChartHelper();
            //InitializeWorkbook(xlsPath);
            IRow datarow = null;
            int l = dt.Rows.Count;
            bool init = false;
            int cnt = 0;
            int cstartlen = 0;
            int cendlen = 0;
            //int reportNo = new Random().Next(9999);
            
            for (cnt = 0;cnt < pagecnt;cnt++ )
            {
                
                IRow row = sheet1.CreateRow(len);
               
                //添加标题
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 1));

                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("{0}雨量监测报表   报表编号{4}   {1}年{2}月{3}日", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo)
                    );
                //CellFontSet(row.GetCell(0), "宋体", 120, 14, false);
                len++;
                int k = 0;
                //}

                row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 1));

                row.GetCell(0).SetCellValue(sheetName
                    );
                len++;

                //k = 0;
                //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                //CreateRow(len+1, sheet1, tabName.Length);//创建复表头行
                //sheet1.AddMergedRegion(new CellRangeAddress(len , len +1, 0, 0));
                //row.CreateCell(0).SetCellValue("点名");
                //CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(1).SetCellValue("高度（m）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                //CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(4).SetCellValue("温度（°C）");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                //CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                //row.CreateCell(10).SetCellValue("时间");
                //sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                //CellFontSet(row.GetCell(10), "黑体", 300, 20, false);
                //CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                ////return;
                ////k = 0;
                //len++;
                ICellStyle styleCell = GetICellStyle();
                ICellStyle styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                row = CreateRow(len, sheet1, tabName.Length);//创建表头行


                k = 0;
                foreach (string name in tabName)
                {
                    //if (k == 0) { k++; continue; }

                   
                    
                        row.CreateCell(k).SetCellValue(name);
                        CellBorderSolid(row.GetCell(k), styleCell, true);
                    
                    k++;
                }
                len++;
                int i = 0;
                for (; i < 51; i++)
                {
                    if (51 * cnt + i > dt.Rows.Count-1)
                    {
                        CreateRow(len, sheet1, 3);
                        len++;
                        continue;
                    }
                    styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                    //row = sheet1.CreateRow(len);
                    //row = CreateRow(len, sheet1, 3);
                    row = CreateRow(len, sheet1, 3);

                    datarow = sheet.CreateRow(datastartlen);
                    datastartlen++;
                    if (51 * cnt + i == 0)
                    {


                        datarow.CreateCell(0).SetCellValue("时间");
                        datarow.CreateCell(1).SetCellValue("雨量");
                        //datarow.CreateCell(2).SetCellValue("本次");
                        //datarow.CreateCell(3).SetCellValue("累计");
                        //datarow.CreateCell(4).SetCellValue("速率");
                        datarow.CreateCell(2).SetCellValue(sheetName);
                        //datastartlen++;
                        datarow = sheet.CreateRow(datastartlen);
                        datastartlen++;
                        datarow.CreateCell(0).SetCellValue(dt.Rows[50 * cnt + i].ItemArray[2].ToString());
                        datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[50 * cnt + i].ItemArray[1]));

                    }
                    else
                    {
                        datarow.CreateCell(0).SetCellValue(dt.Rows[50 * cnt + i].ItemArray[2].ToString());
                        datarow.CreateCell(1).SetCellValue(Convert.ToDouble(dt.Rows[50 * cnt + i].ItemArray[1]));
                    }




                    int j = 0, t = 0;
                    for (j = 0; j < 3; j++)
                    {




                        if (j == 1)
                        {
                            //if (t == 0)
                            //{
                            //    row.CreateCell(t).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[3]));
                            //}
                            //else if (t == 1 || t == 3)
                            //{
                            //    row.CreateCell(t).SetCellValue(Convert.ToDouble(dt.Rows[i].ItemArray[2]));
                            //}
                            //else 
                            //if (i == 0 && t == 0) mindeep = Convert.ToDouble(dt.Rows[i].ItemArray[j]);
                            row.CreateCell(t).SetCellValue(Convert.ToDouble(dt.Rows[50 * cnt + i].ItemArray[j]));
                        }
                        else
                            row.CreateCell(t).SetCellValue(dt.Rows[50 * cnt + i].ItemArray[j].ToString());
                        CellFontSet(row.GetCell(t), "仿宋", 300, 16, false);
                        if (j == dt.Columns.Count - 1)

                            CellBorderSet(row.GetCell(t), styleBorder, false);
                        else
                            CellBorderSolid(row.GetCell(t), styleCell, true);
                        t++;

                    }
                    len++;
                    //i++;

                }
                tmplen += 54;
                
               

            }

        }



        public static IRow CreateRow(int len, ISheet sheet, int colCnt)
        {
            IRow row = sheet.CreateRow(len);
            for (int i = 0; i < colCnt; i++)
            {
                CellFontSet(row.CreateCell(i), "仿宋", 200, 16, true);
                CellBorderSet(row.CreateCell(i), GetBorderSetStyle(true, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);

            }
            return row;
        }



        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValMPMC(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, string et, int index)
        {



            DataTable dt = originaldt;
            //List<DataTable> ldt = Tool.DataTableHelper.ProcessDataTableSplitSPMC(originaldt);
            originaldt.DefaultView.Sort = " point_name,cyc asc ";
            int t = 0;

            ISheet sheet1 = null;

            sheet1 = vsion == 0 ? hssfworkbook.GetSheet("表面位移") : xssfworkbook.GetSheet("表面位移");

            if (dt.Columns.Count > 11) sheet1.PrintSetup.Landscape = true;
            sheet1.Header.Left = "南方测绘";
            sheet1.Header.Right = DateTime.Now.ToString();
            sheet1.Footer.Center = "第&p页";
            sheet1.PrintSetup.PaperSize = 9;
            sheet1.Workbook.SetRepeatingRowsAndColumns(index, 0, dt.Columns.Count, 0, 1);
            //sheet1.PrintSetup.Scale = 400;
            sheet1.HorizontallyCenter = true;

            string tmp = string.Format(st, dt.Rows[0].ItemArray[0].ToString());

            string[] tabName = tabHead.Split(',');
            DataView dv = new DataView(dt);

            //sheetName = dt.Rows[0].ItemArray[0].ToString();

            //create cell on rows, since rows do already exist,it's not necessary to create rows again.

            int k = 0;
            IRow row = sheet1.CreateRow(0);
            row = sheet1.CreateRow(0);
            ICellStyle styleCell = GetICellStyle();
            ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
            //CellBorderSolid(row.CreateCell(1));
            foreach (string name in tabName)
            {

                row.CreateCell(k).SetCellValue(title);
                ICellStyle style = styleCell;
                k++;
            }
            sheet1.SetColumnWidth(dt.Columns.Count - 1, 27 * 256);
            //添加标题
            sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, dt.Columns.Count - 1));
            CellFontSet(row.GetCell(0), "仿宋", 200, 16, false);
            CellBorderSet(row.GetCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
            CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
            ////stl.WrapText = true;
            ////row.GetCell(0).SetCellValue(xmname);
            //CellFontSet(row.CreateCell(k-1), "仿宋", 200, 5, false);
            //CellBorderSet(row.GetCell(k-1), GetBorderSetStyle(false), false);
            row.GetCell(0).SetCellValue(tmp + "至" + et
                );


            CellBorderSet(row.GetCell(k - 1), styleBorder, false);
            //创建表头
            row = sheet1.CreateRow(1);
            k = 0;
            foreach (string name in tabName)
            {

                row.CreateCell(k).SetCellValue(name);
                //if (k == dt.Columns.Count - 1)
                //    CellBorderSet(row.GetCell(k), styleBorder, false);
                //else
                CellBorderSolid(row.GetCell(k), styleCell, true);
                k++;

            }

            int i = 2;
            foreach (DataRowView drv in dv)
            {
                styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                row = sheet1.CreateRow(i);
                int j = 0;
                for (j = 0; j < dt.Columns.Count; j++)
                {
                    row.CreateCell(j).SetCellType(CellType.NUMERIC);
                    row.CreateCell(j).SetCellValue(dt.Rows[i - 2].ItemArray[j].ToString());
                    if (j == dt.Columns.Count - 1)
                        CellBorderSet(row.GetCell(j), styleBorder, false);
                    else
                        CellBorderSolid(row.GetCell(j), styleCell, true);
                }
                i++;
            }
            sheet1.ForceFormulaRecalculation = true;


            //xssfworkbook.RemoveName("表面位移");





        }
        /// <summary>
        /// 多点多周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValMPMC(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, string et, int index, string xlsPath, out ChartCreateEnviroment lce)
        {



            int len = 0;
            //List<DataTable> ldt = Tool.DataTableHelper.ProcessDataTableSplitSPMC(originaldt);
            List<string> pnames = Tool.DataTableHelper.ProcessDataTableFilter(originaldt, "point_name");
            lce = new ChartCreateEnviroment();
            //int t = 0;
            //lce = new ChartCreateEnviroment();
            DataTable dt = originaldt;
            string[] tabName = tabHead.Split(',');
            ChartHelper chartHelper = new ChartHelper();
            //InitializeWorkbook(xlsPath);
            bool init = false;
            int cnt = 0;
            int cstartlen = 0;
            int cendlen = 0;
            int reportNo = new Random().Next(9999);
            //foreach (DataTable dt in ldt)
            //{
            //InitializeWorkbook(xlsPath);
            //if(vsion == 0)
            ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("表面位移") : xssfworkbook.GetSheet("表面位移");
            string tmp = st;
            tmp = "点名:" + dt.Rows[0].ItemArray[0].ToString();


            DataView dv = new DataView(dt);
            sheetName = dt.Rows[0].ItemArray[0].ToString();

            if (dt.Columns.Count > 11) sheet1.PrintSetup.Landscape = true;
            sheet1.Header.Left = "南方测绘";
            sheet1.Header.Right = DateTime.Now.ToString();
            sheet1.Footer.Center = "第&p页";
            sheet1.PrintSetup.PaperSize = 9;
            sheet1.Workbook.SetRepeatingRowsAndColumns(0, 0, dt.Columns.Count - 1, 1, 4);
            sheet1.FitToPage = false;
            sheet1.PrintSetup.Scale = 100;
            sheet1.HorizontallyCenter = true;
            int k = 0;
            IRow row = sheet1.CreateRow(len);
            ICellStyle styleCell = GetICellStyle();
            ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
            //if (len < 10000)
            //{


            sheet1.SetColumnWidth(dt.Columns.Count - 1, 27 * 256);
            //添加标题
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 1));

            CellBorderSet(row.CreateCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
            CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
            row.GetCell(0).SetCellValue(string.Format("{0}水平位移和竖向位移监测报表   报表编号{4}   {1}年{2}月{3}日", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo)
                );
            CellFontSet(row.GetCell(0), "宋体", 200, 14, false);
            len++;
            k = 0;
            //}

            row = CreateRow(len, sheet1, tabName.Length);//创建点号行
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 1));

            //row.GetCell(0).SetCellValue(tmp
            //    );
            len++;

            //k = 0;
            row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
            sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
            row.CreateCell(0).SetCellValue("点名");
            CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

            //row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
            sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 1, 1));
            row.CreateCell(1).SetCellValue("周期");
            CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

            row.CreateCell(2).SetCellValue("观测值（mm）");
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 2, 4));
            CellFontSet(row.GetCell(2), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(2), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
            row.CreateCell(5).SetCellValue("本次变化值（mm）");
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 5, 7));
            CellFontSet(row.GetCell(5), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(5), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
            row.CreateCell(8).SetCellValue("累计变化值（mm）");
            sheet1.AddMergedRegion(new CellRangeAddress(len, len, 8, 10));
            CellFontSet(row.GetCell(8), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(8), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
            row.CreateCell(11).SetCellValue("时间");
            sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 11, 11));
            CellFontSet(row.GetCell(11), "黑体", 300, 20, false);
            CellBorderSet(row.GetCell(11), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
            //return;
            //k = 0;
            len++;
            row = CreateRow(len, sheet1, tabName.Length);//创建表头行


            k = 0;
            foreach (string name in tabName)
            {
                //if (k == 0) { k++; continue; }
                row.CreateCell(k).SetCellValue(name);
                if (k == dt.Columns.Count)
                    CellBorderSet(row.GetCell(k), styleBorder, false);
                else
                    CellBorderSolid(row.GetCell(k), styleCell, true);
                k++;
            }

            len++;
            int i = 2;
            foreach (DataRowView drv in dv)
            {
                styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                row = sheet1.CreateRow(len);
                int j = 0;
                for (j = 0; j < dt.Columns.Count; j++)
                {

                    if (dt.Columns[j].DataType == Type.GetType("System.Double"))
                        row.CreateCell(j).SetCellValue(Convert.ToDouble(dt.Rows[i - 2].ItemArray[j]));
                    else

                        row.CreateCell(j).SetCellValue(dt.Rows[i - 2].ItemArray[j].ToString());
                    CellFontSet(row.GetCell(j), "仿宋", 300, 16, false);
                    if (j == dt.Columns.Count - 1)

                        CellBorderSet(row.GetCell(j), styleBorder, false);
                    else
                        CellBorderSolid(row.GetCell(j), styleCell, true);

                }
                len++;
                i++;
            }


            if (cnt == 0)
            {
                cstartlen = 5;
                cendlen = 2 + i;
            }
            else
            {

                cstartlen = len - i + 3;
                cendlen = len;

            }
            //sheet1.SetRowBreak(len);
            int pageIndex = len % 35 == 0 ? (len / 35) : (len / 35) + 1;
            len = len % 35 == 0 ? (len / 35) * 35 + 70 : (len / 35) * 35 + 105;


            //sheet1.SetRowBreak(len+29);
            //sheet1.SetRowBreak(len+58);
            //sheet1.CreateRow(len + 1).CreateCell(0).SetCellValue("从此处中断");
            //sheet1.PrintSetup.Landscape = false;
            //ReportHelper.WriteToFile(xlsPath);
            //chartHelper.ChartReportHelper(xlsPath, sheetName, cstartlen, cendlen, pageIndex);
            lce = new ChartCreateEnviroment(cstartlen, cendlen, sheetName, pageIndex);




            //sheet1.ForceFormulaRecalculation = true;
            //xssfworkbook.RemoveName("表面位移");
            ExceptionLog.ExceptionWrite("单点多周期表格生成");




        }
        /// <summary>
        ///多点单周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValMPSC(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, int index)
        {




            List<DataTable> ldt = Tool.DataTableHelper.ProcessDataTableSplitMPSC(originaldt);
            int t = 0;

            foreach (DataTable dt in ldt)
            {

                string tmp = string.Format("第{0}周期", dt.Rows[0].ItemArray[1].ToString());

                string[] tabName = tabHead.Split(',');
                DataView dv = new DataView(dt);
                ISheet sheet1 = null;
                sheetName = string.Format("第{0}周期", dt.Rows[0].ItemArray[1].ToString());
                if (xssfworkbook.GetSheet("表面位移") != null)
                {
                    xssfworkbook.SetSheetName(0, sheetName);
                    sheet1 = xssfworkbook.GetSheet(sheetName);
                }
                else if (xssfworkbook.GetSheet(sheetName) == null)
                    sheet1 = xssfworkbook.CreateSheet(sheetName);

                else
                    sheet1 = xssfworkbook.GetSheet(sheetName);
                //create cell on rows, since rows do already exist,it's not necessary to create rows again.
                if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
                sheet1.Header.Left = "南方测绘";
                sheet1.Header.Right = DateTime.Now.ToString();
                sheet1.Footer.Center = "第&p页";
                sheet1.PrintSetup.PaperSize = 9;
                sheet1.Workbook.SetRepeatingRowsAndColumns(index, 0, dt.Columns.Count, 0, 1);
                //sheet1.PrintSetup.Scale = 400;
                sheet1.HorizontallyCenter = true;
                int k = 0;
                IRow row = sheet1.CreateRow(0);
                row = sheet1.CreateRow(0);
                ICellStyle styleCell = GetICellStyle();
                ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                //CellBorderSolid(row.CreateCell(1));
                foreach (string name in tabName)
                {

                    row.CreateCell(k).SetCellValue(title);
                    ICellStyle style = styleCell;
                    k++;
                }
                sheet1.SetColumnWidth(dt.Columns.Count - 2, 27 * 256);
                //添加标题
                sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, dt.Columns.Count - 2));
                CellFontSet(row.GetCell(0), "仿宋", 200, 16, false);
                CellBorderSet(row.GetCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                ////stl.WrapText = true;
                ////row.GetCell(0).SetCellValue(xmname);
                //CellFontSet(row.CreateCell(k-1), "仿宋", 200, 5, false);
                //CellBorderSet(row.GetCell(k-1), GetBorderSetStyle(false), false);
                row.GetCell(0).SetCellValue(tmp
                    );


                CellBorderSet(row.GetCell(k - 1), styleBorder, false);
                //创建表头
                row = sheet1.CreateRow(1);
                k = 0;
                foreach (string name in tabName)
                {

                    row.CreateCell(k).SetCellValue(name);
                    if (k == dt.Columns.Count - 1)
                        CellBorderSet(row.GetCell(k), styleBorder, false);
                    else
                        CellBorderSolid(row.GetCell(k), styleCell, true);
                    k++;
                }

                int i = 2;
                foreach (DataRowView drv in dv)
                {
                    styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                    row = sheet1.CreateRow(i);
                    int j = 0;
                    int r = 0;
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j == 1) continue;
                        //row.CreateCell(r).SetCellType(CellType.NUMERIC);

                        if (dt.Columns[j].DataType == Type.GetType("System.Double"))
                            row.CreateCell(r).SetCellValue(Convert.ToDouble(dt.Rows[i - 2].ItemArray[j]));
                        else
                            row.CreateCell(r).SetCellValue(dt.Rows[i - 2].ItemArray[j].ToString());
                        if (j == dt.Columns.Count - 1)
                            CellBorderSet(row.GetCell(r), styleBorder, false);
                        else
                            CellBorderSolid(row.GetCell(r), styleCell, true);
                        r++;

                    }
                    i++;
                }
                sheet1.ForceFormulaRecalculation = true;

            }
            //xssfworkbook.RemoveName("表面位移");





        }

        /// <summary>
        /// 多点单周期报表
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public static void SetTabValMPSC(string xmname, string tabHead, DataTable originaldt, string title, string sheetName, string st, string et, int index, string xlsPath, out List<ChartCreateEnviroment> lce)
        {



            int len = 0;
            List<DataTable> ldt = Tool.DataTableHelper.ProcessDataTableSplitMPSC(originaldt);
            //int t = 0;
            lce = new List<ChartCreateEnviroment>();
            int reportNo = new Random().Next(9999);
            string[] tabName = tabHead.Split(',');
            ChartHelper chartHelper = new ChartHelper();
            //InitializeWorkbook(xlsPath);
            bool init = false;
            int cnt = 0;
            int cstartlen = 0;
            int cendlen = 0;
            foreach (DataTable dt in ldt)
            {
                //InitializeWorkbook(xlsPath);
                //if(vsion == 0)

                ISheet sheet1 = vsion == 0 ? hssfworkbook.GetSheet("表面位移") : xssfworkbook.GetSheet("表面位移");
                string tmp = st;
                tmp = string.Format("第{0}期", dt.Rows[0].ItemArray[1]);


                DataView dv = new DataView(dt);
                sheetName = dt.Rows[0].ItemArray[1].ToString();

                if (dt.Columns.Count > 10) sheet1.PrintSetup.Landscape = true;
                sheet1.Header.Left = "南方测绘";
                sheet1.Header.Right = DateTime.Now.ToString();
                sheet1.Footer.Center = "第&p页";
                sheet1.PrintSetup.PaperSize = 9;
                //sheet1.Workbook.SetRepeatingRowsAndColumns(0, 0, dt.Columns.Count, len+1, len+2);
                sheet1.FitToPage = false;
                sheet1.PrintSetup.Scale = 100;
                sheet1.HorizontallyCenter = true;
                int k = 0;
                IRow row = sheet1.CreateRow(len);
                ICellStyle styleCell = GetICellStyle();
                ICellStyle styleBorder = GetBorderSetStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                //if (len < 10000)
                //{


                sheet1.SetColumnWidth(dt.Columns.Count - 2, 27 * 256);
                //添加标题
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                CellBorderSet(row.CreateCell(0), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                CellBorderSet(row.CreateCell(1), GetBorderSetStyle(false, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER), false);
                row.GetCell(0).SetCellValue(string.Format("{0}水平位移和竖向位移监测报表   报表编号{4}   {1}年{2}月{3}日", xmname, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, reportNo)
                    );
                CellFontSet(row.GetCell(0), "宋体", 200, 14, false);
                len++;
                k = 0;
                //}

                row = CreateRow(len, sheet1, tabName.Length);//创建点号行
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 0, dt.Columns.Count - 2));

                row.GetCell(0).SetCellValue(tmp
                    );
                len++;

                //k = 0;
                row = CreateRow(len, sheet1, tabName.Length);//创建复表头行
                sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 0, 0));
                row.CreateCell(0).SetCellValue("点名");
                CellFontSet(row.GetCell(0), "黑体", 300, 20, false);
                CellBorderSet(row.GetCell(0), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                row.CreateCell(1).SetCellValue("观测值（mm）");
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 1, 3));
                CellFontSet(row.GetCell(1), "黑体", 300, 20, false);
                CellBorderSet(row.GetCell(1), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                row.CreateCell(4).SetCellValue("本次变化值（mm）");
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 4, 6));
                CellFontSet(row.GetCell(4), "黑体", 300, 20, false);
                CellBorderSet(row.GetCell(4), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                row.CreateCell(7).SetCellValue("累计变化值（mm）");
                sheet1.AddMergedRegion(new CellRangeAddress(len, len, 7, 9));
                CellFontSet(row.GetCell(7), "黑体", 300, 20, false);
                CellBorderSet(row.GetCell(7), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);
                row.CreateCell(10).SetCellValue("时间");
                sheet1.AddMergedRegion(new CellRangeAddress(len, len + 1, 10, 10));
                CellFontSet(row.GetCell(10), "黑体", 300, 20, false);
                CellBorderSet(row.GetCell(10), GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER), false);

                //k = 0;
                len++;
                row = CreateRow(len, sheet1, tabName.Length);//创建表头行


                k = 0;
                foreach (string name in tabName)
                {
                    //if (k == 0) { k++; continue; 
                    row.CreateCell(k).SetCellValue(name);
                    if (k == dt.Columns.Count - 1)
                        CellBorderSet(row.GetCell(k), styleBorder, false);
                    else
                        CellBorderSolid(row.GetCell(k), styleCell, true);
                    k++;
                }
                len++;
                int i = 2;
                foreach (DataRowView drv in dv)
                {
                    styleBorder = GetBorderSetStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
                    row = sheet1.CreateRow(len);
                    int j = 0;
                    int r = 0;
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j == 1) continue;
                        if (dt.Columns[j].DataType == Type.GetType("System.Double"))
                            row.CreateCell(r).SetCellValue(Convert.ToDouble(dt.Rows[i - 2].ItemArray[j]));
                        else

                            row.CreateCell(r).SetCellValue(dt.Rows[i - 2].ItemArray[j].ToString());
                        CellFontSet(row.GetCell(r), "仿宋", 300, 16, false);
                        if (j == dt.Columns.Count - 1)

                            CellBorderSet(row.GetCell(r), styleBorder, false);
                        else
                            CellBorderSolid(row.GetCell(r), styleCell, true);
                        r++;
                    }
                    len++;
                    i++;
                }
                if (cnt == 0)
                {
                    cstartlen = 5;
                    cendlen = 2 + i;
                }
                else
                {

                    cstartlen = len - i + 3;
                    cendlen = len;

                }
                //sheet1.SetRowBreak(len);
                int pageIndex = len % 35 == 0 ? (len / 35) : (len / 35) + 1;
                len = len % 35 == 0 ? (len / 35) * 35 + 70 : (len / 35) * 35 + 105;


                //sheet1.SetRowBreak(len+29);
                //sheet1.SetRowBreak(len+58);
                //sheet1.CreateRow(len + 1).CreateCell(0).SetCellValue("从此处中断");
                //sheet1.PrintSetup.Landscape = false;
                //ReportHelper.WriteToFile(xlsPath);
                //chartHelper.ChartReportHelper(xlsPath, sheetName, cstartlen, cendlen, pageIndex);
                lce.Add(new ChartCreateEnviroment(cstartlen, cendlen, string.Format("第{0}周期", sheetName), pageIndex));
                cnt++;

            }
            //sheet1.ForceFormulaRecalculation = true;
            //xssfworkbook.RemoveName("表面位移");
            ExceptionLog.ExceptionWrite("单点多周期表格生成");




        }




        //将单元格设置为有边框的
        public static void CellBorderSolid(ICell cell, ICellStyle style, bool wrap)
        {
            //ICellStyle style = xssfworkbook.CreateCellStyle();
            style.WrapText = wrap;
            cell.CellStyle = style;

        }


        //创建表格样式
        public static ICellStyle GetICellStyle()
        {
            ICellStyle style = null;
            if (xssfworkbook != null)
                style = xssfworkbook.CreateCellStyle();
            else
                style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            style.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            return style;

        }
        //设置边框样式
        public static ICellStyle GetBorderSetStyle(bool solid, HorizontalAlignment h, VerticalAlignment v)
        {
            ICellStyle style = null;
            if (xssfworkbook != null)
                style = xssfworkbook.CreateCellStyle();
            else
                style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderLeft = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderRight = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.BorderTop = solid ? BorderStyle.THIN : BorderStyle.NONE;
            style.Alignment = h;
            style.VerticalAlignment = v;
            return style;
        }

        //设置单元格的边框
        public static void CellBorderSet(ICell cell, ICellStyle style, bool wrap)
        {

            try
            {
                style.WrapText = wrap;
                cell.CellStyle = style;
            }
            catch (Exception ex)
            {
                string a = "";
            }

        }
        //设置字体样式
        public static void CellFontSet(ICell cell, string fontName,
            short fontWeight, short fontHeight, bool italic)
        {
            try
            {
                ICellStyle style = null;
                IFont font = null;
                if (xssfworkbook != null)
                {
                    style = xssfworkbook.CreateCellStyle();
                    font = xssfworkbook.CreateFont();
                }
                else
                {
                    style = hssfworkbook.CreateCellStyle();
                    font = hssfworkbook.CreateFont();
                }

                font.FontName = fontName;
                font.Boldweight = fontWeight;
                font.FontHeightInPoints = fontHeight;
                font.IsItalic = italic;

                style.SetFont(font);
                cell.CellStyle = style;
            }
            catch (Exception ex)
            {
                string a = "";
            }
        }
        //日期格式转换
        public static string DateFormatConverter(string dateStr)
        {
            if (dateStr == null || dateStr.Trim() == "") return "";
            char[] splits = { '-', ' ', '/', ':' };
            string[] dateStrs = dateStr.Split(splits);
            return dateStrs[0].Substring(dateStrs[0].IndexOf("20") + 2) + "年" + dateStrs[1] + "月" + dateStrs[2] + "日";
        }
        //填充曲线表
        /// <summary>
        /// 对不同监测项目的日期进行排序,生成X轴
        /// </summary>
        /// <param name="dt"></param>
        public static List<DateTime> GetDateTimeFromSeriesInDiffJcxm(ISheet sheet, List<serie> series)
        {
            List<DateTime> ldt = new List<DateTime>();
            List<string> lType = new List<string>();
            int i = 0, j = 0;
            for (i = 0; i < series.Count; i++)
            {

                for (j = 0; j < series[i].Pts.Length; j++)
                {
                    ldt.Add(series[i].Pts[j].Dt);
                }

            }
            ldt = ldt.Distinct().ToList();
            ldt.Sort();
            IRow row = sheet.CreateRow(0);
            for (i = 1; i < ldt.Count + 1; i++)
            {
                row.CreateCell(i).SetCellValue(ldt[i - 1]);
            }
            return ldt;
        }
        /// <summary>
        /// 填充图例
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        /// <returns></returns>
        public static List<string> LegendFill(ISheet sheet, List<serie> series)
        {
            List<string> legends = new List<string>();
            int i = 0;
            for (i = 1; i < series.Count + 1; i++)
            {
                IRow row = sheet.CreateRow(i);
                row.CreateCell(0).SetCellValue(series[i - 1].Name);
                legends.Add(series[i - 1].Name);
            }
            return legends;
        }
        /// <summary>
        /// 安装曲线数据点
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        public static void SerieDataPointFill(ISheet sheet, List<serie> series, List<DateTime> ldt, List<string> legends)
        {

            int y = 0, t = 0, posy = 0, posx = 0;
            for (y = 0; y < series.Count; y++)
            {
                IRow row = sheet.GetRow(y);
                posy = 1 + legends.IndexOf(series[y].Name);
                for (t = 0; t < series[y].Pts.Length; t++)
                {
                    posx = 1 + ldt.IndexOf(series[y].Pts[t].Dt);
                    row.CreateCell(posx).SetCellValue(series[y].Pts[t].Yvalue1);
                }
            }
        }
        //调用曲线填充函数
        //public void SeriesCreate(HttpContext context)
        //{
        //    //InitializeWorkbook("D:\\ReportExcel.xls");
        //    List<serie> ls = (List<serie>)context.Session["series"];
        //    //分拣出本次和累计
        //    List<serie> lsThis = new List<serie>();
        //    List<serie> lsAc = new List<serie>();
        //    foreach (serie s in ls)
        //    {
        //        if (s.Stype.IndexOf("累计") != -1)
        //        {
        //            lsAc.Add(s);
        //        }
        //        else
        //        {
        //            lsThis.Add(s);
        //        }

        //    }
        //    if (lsThis.Count > 0)
        //    {
        //        ISheet sheet = xssfworkbook.CreateSheet("本次变化曲线生成数据");
        //        //本次曲线
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsThis);
        //        List<string> legends = LegendFill(sheet, lsThis);
        //        SerieDataPointFill(sheet, lsThis, ldt, legends);
        //    }
        //    if (lsAc.Count > 0)
        //    {
        //        //累计曲线
        //        ISheet sheet = xssfworkbook.CreateSheet("累计变化曲线生成数据");
        //        List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsAc);
        //        List<string> legends = LegendFill(sheet, lsAc);
        //        SerieDataPointFill(sheet, lsAc, ldt, legends);
        //    }
        //}

    }
}