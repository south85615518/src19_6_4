﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace Tool
{
    public class GaugeChartHelper
    {
        public Application ThisApplication = null;
        public Workbooks m_objBooks = null;
        public _Workbook ThisWorkbook = null;
        public Worksheet xlSheet = null;
        public Worksheet xlDataSheet = null;
        #region 柱状图
        public void main()
        {
            //CreateChart();
            //ChartReportHelper("E:\\数据测试_表面位移_第1周期-第6周期数据.xls",6,"周期");
        }

        public void DeleteSheet()
        {
            foreach (Worksheet ws in ThisWorkbook.Worksheets)
                if (ws != ThisApplication.ActiveSheet)
                {
                    ws.Delete();
                }
            foreach (Chart cht in ThisWorkbook.Charts)

                cht.Delete();
        }
        private void AddDatasheet()
        {

            xlSheet = ThisWorkbook.Sheets.get_Item(1);
            //(Worksheet)ThisWorkbook.

            //    Worksheets.Add(Type.Missing, ThisWorkbook.ActiveSheet,

            //    Type.Missing, Type.Missing);
            xlSheet.Activate();


            xlSheet.Name = "曲线";

        }
        private void LoadData()
        {

            Random ran = new Random();

            for (int i = 1; i <= 12; i++)
            {

                xlSheet.Cells[i, 1] = i.ToString() + "月";

                xlSheet.Cells[i, 2] = ran.Next(2000).ToString();

            }

        }

        private void LoadDatadb()
        {



            Random ran = new Random();



            for (int i = 1; i <= 12; i++)
            {

                xlSheet.Cells[i, 1] = i + "周期";

                xlSheet.Cells[i, 2] = ran.Next(2000).ToString();

                xlSheet.Cells[i, 3] = ran.Next(1500).ToString();

                xlSheet.Cells[i, 4] = ran.Next(1500).ToString();

                xlSheet.Cells[i + 1, 8] = i + "周期";

                xlSheet.Cells[i + 1, 9] = ran.Next(2000).ToString();

                xlSheet.Cells[i + 1, 10] = ran.Next(1500).ToString();

                xlSheet.Cells[i + 1, 11] = ran.Next(1500).ToString();

            }

        }
        public void CreateChart(string pointname, int lenstart, int lenend, int breakRow, int chartIndex)
        {

            CreateDeepChart(string.Format("{0}库水位过程变化曲线", pointname), lenstart, lenend, breakRow, chartIndex);
            CreateDeepThisChart(string.Format("{0}库水位本次变化曲线", pointname), lenstart, lenend, breakRow, chartIndex);
            CreateDeepAcChart(string.Format("{0}库水位累计变化曲线", pointname), lenstart, lenend, breakRow, chartIndex);
            CreateDeepRapChart(string.Format("{0}库水位速率变化曲线", pointname), lenstart, lenend, breakRow, chartIndex);
            //CreateAcChart(string.Format("{0}累计变化", pointname), lenstart, lenend, breakRow, chartIndex);
        }

        public static int setupHei = 0;

        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateDeepChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {

            setupHei = 730 + (pageIndex - 1) * 730 ;//+(odd * 5);;
            //.Rows[8].PageBreak = 1;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei,480,350);

            Chart chart = shape.Chart;
            Range cellRange = (Range)xlDataSheet.get_Range("A" + lenstart, "B" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);


            //Range cellRange = (Range)xlSheet.get_Range("A3", "J" + (2 + rowcnt));
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //if (xlChart.HasTitle == false)
            //    xlChart.ChartTitle = "";
            //try
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       title /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            //catch
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);

            //string[] seriesName = { "水深", "ΔY", "ΔZ" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            //for (idx = 1; idx < 2; idx++)
            //{
            //    if (idx < 2)
            //    {
            //        Series s = (Series)grp.SeriesCollection(1);
            //        s.Delete();
            //    }

            //}
            grp.GapWidth = 20;
            chart.HasLegend = false;
            grp.VaryByCategories = true;
            for (i = 1; i < 2; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                //s.Name = seriesName[i - 1];
                
                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                //chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlTimeScale;
                categoryAxis.BaseUnitIsAuto = false;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }



        }
        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateDeepThisChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {

            //Chart xlChart = (Chart)ThisWorkbook.Charts.
            //                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);
            setupHei = 730 + (pageIndex - 1) * 730 + 350;//+ (odd * 5);
            //.Rows[8].PageBreak = 1;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 480, 350);

            Chart chart = shape.Chart;
            Range cellRange = (Range)xlDataSheet.get_Range("A" + lenstart, "C" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);


            //Range cellRange = (Range)xlSheet.get_Range("A3", "J" + (2 + rowcnt));
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //if (xlChart.HasTitle == false)
            //    xlChart.ChartTitle = "";
            //try
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       title /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            //catch
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);
            chart.HasLegend = false;
            string[] seriesName = { "本次" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            for (idx = 1; idx < 2; idx++)
            {
                if (idx < 2)
                {
                    Series s = (Series)grp.SeriesCollection(1);
                    s.Delete();
                }

            }
            grp.GapWidth = 20;
            
            grp.VaryByCategories = true;
            for (i = 1; i < 2; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);
                //if(i == 1)

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                s.Name = seriesName[i - 1];
                if (i == 2)
                    s.AxisGroup = XlAxisGroup.xlSecondary;
                //s.XValues = 2;    
                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                //chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;
                //Axis valueAxis = null;


                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                //Axis categoryAxis = null;

                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);

                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlTimeScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }




        }
        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateDeepAcChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {

            //Chart xlChart = (Chart)ThisWorkbook.Charts.
            //                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);
            setupHei = 730 + (pageIndex) * 730;// +(odd * 5);
            //.Rows[8].PageBreak = 1;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 480, 350);

            Chart chart = shape.Chart;
            Range cellRange = (Range)xlDataSheet.get_Range("A" + lenstart, "D" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);


            //Range cellRange = (Range)xlSheet.get_Range("A3", "J" + (2 + rowcnt));
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //if (xlChart.HasTitle == false)
            //    xlChart.ChartTitle = "";
            //try
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       title /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            //catch
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);
            chart.HasLegend = false;
            string[] seriesName = { "累计" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            for (idx = 1; idx < 3; idx++)
            {
                if (idx < 3)
                {
                    Series s = (Series)grp.SeriesCollection(1);
                    s.Delete();
                }

            }
            grp.GapWidth = 20;

            grp.VaryByCategories = true;
            for (i = 1; i < 2; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);
                //if(i == 1)

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                s.Name = seriesName[i - 1];
                if (i == 2)
                    s.AxisGroup = XlAxisGroup.xlSecondary;
                //s.XValues = 2;    
                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                //chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;
                //Axis valueAxis = null;


                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                //Axis categoryAxis = null;

                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);

                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlTimeScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }




        }
        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateDeepRapChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {

            //Chart xlChart = (Chart)ThisWorkbook.Charts.
            //                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);
            setupHei = 730 + (pageIndex) * 730 + 350;// +(odd * 5);
            //.Rows[8].PageBreak = 1;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 480, 350);

            Chart chart = shape.Chart;
            Range cellRange = (Range)xlDataSheet.get_Range("A" + lenstart, "E" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);


            //Range cellRange = (Range)xlSheet.get_Range("A3", "J" + (2 + rowcnt));
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //if (xlChart.HasTitle == false)
            //    xlChart.ChartTitle = "";
            //try
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       title /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            //catch
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);
            chart.HasLegend = false;
            string[] seriesName = { "速率" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            for (idx = 1; idx < 4; idx++)
            {
                if (idx < 4)
                {
                    Series s = (Series)grp.SeriesCollection(1);
                    s.Delete();
                }

            }
            grp.GapWidth = 20;

            grp.VaryByCategories = true;
            for (i = 1; i < 2; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);
                //if(i == 1)

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                s.Name = seriesName[i - 1];
                if (i == 2)
                    s.AxisGroup = XlAxisGroup.xlSecondary;
                //s.XValues = 2;    
                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                //chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;
                //Axis valueAxis = null;


                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                //Axis categoryAxis = null;

                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);

                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlTimeScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }




        }
        /// <summary>
        /// 创建总表统计图         
        /// </summary>
        private void CreateTotalThisChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {

            //Chart xlChart = (Chart)ThisWorkbook.Charts.
            //                Add(Type.Missing, xlSheet, Type.Missing, Type.Missing);
            setupHei = 485 + (pageIndex - 1) * 475 + (odd * 5);
            //.Rows[8].PageBreak = 1;
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 600, 350);

            Chart chart = shape.Chart;
            Range cellRange = (Range)xlSheet.get_Range("B" + lenstart, "G" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);


            //Range cellRange = (Range)xlSheet.get_Range("A3", "J" + (2 + rowcnt));
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);
            //if (xlChart.HasTitle == false)
            //    xlChart.ChartTitle = "";
            //try
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       title /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            //catch
            //{
            //    xlChart.ChartWizard(cellRange,
            //        XlChartType.xlLine, Type.Missing,
            //        XlRowCol.xlColumns, 1, 0, true,
            //       Type.Missing /*"访问量比较(dahuzizyd.cnblogs.com)"*/, xAixeTitle, "本次变化量",
            //        "");
            //}
            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}本次变化量曲线", xlSheet.Name);

            string[] seriesName = { "ΔN", "ΔE", "ΔZ" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            for (idx = 1; idx < 4; idx++)
            {
                if (idx < 4)
                {
                    Series s = (Series)grp.SeriesCollection(1);
                    s.Delete();
                }

            }
            grp.GapWidth = 20;

            grp.VaryByCategories = true;
            for (i = 1; i < lenend; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = true;
                s.Name = seriesName[i - 1];

                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                //chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);

                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlCategoryScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }




        }
        public static void SeriesMarkStyleSet(Series s, int i)
        {
            switch (i)
            {
                case 1: s.MarkerStyle = XlMarkerStyle.xlMarkerStyleTriangle; break;
                case 2: s.MarkerStyle = XlMarkerStyle.xlMarkerStylePlus; break;
                case 3: s.MarkerStyle = XlMarkerStyle.xlMarkerStyleCircle; break;
                //case 4: s.MarkerStyle = XlMarkerStyle.; break;
            }
        }
        public static int odd = -1;
        /// <summary>
        /// 创建统计图         
        /// </summary>
        private void CreateAcChart(string title, int lenstart, int lenend, int pageIndex, int chartIndex)
        {
            //xlSheet.Rows[breakRow+29].PageBreak = 1;
            setupHei = setupHei + 475 + (odd * -5);
            Microsoft.Office.Interop.Excel.Shape shape = xlSheet.Shapes.AddChart(XlChartType.xlLine, 0, setupHei, 600, 350);
            odd *= -1;
            setupHei = setupHei + 475 + (odd * -5);
            Chart chart = shape.Chart;
            Range cellRange = (Range)xlDataSheet.get_Range("A" + lenstart, "G" + lenend);
            chart.SetSourceData(cellRange, XlRowCol.xlColumns);
            //Range cellRanged = (Range)xlSheet.Cells[4,11];
            //Range range = cellRanges.get_Range(cellRanges, cellRanged);

            chart.HasTitle = true;
            chart.ChartTitle.Text = title;
            //chart.Name = string.Format("{0}累计变化量曲线", xlSheet.Name);


            string[] seriesName = { "ΣΔX", "ΣΔY", "ΣΔZ" };

            int i = 0;
            ChartGroup grp = (ChartGroup)chart.ChartGroups(1);
            int idx = 0;
            for (idx = 1; idx < 10; idx++)
            {
                if (idx < 4)
                {
                    Series s = (Series)grp.SeriesCollection(1);
                    s.Delete();
                }

            }
            grp.GapWidth = 20;

            grp.VaryByCategories = true;
            for (i = 1; i < 4; i++)
            {

                Series s = (Series)grp.SeriesCollection(i);

                s.BarShape = XlBarShape.xlCylinder;
                s.HasDataLabels = false;
                s.Name = seriesName[i - 1];

                //s.MarkerSize = 20;
                s.Format.Line.Weight = 2;
                //s.Format.Line.Weight = 15;
                //chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
                SeriesMarkStyleSet(s, i);
                s.MarkerSize = 4;
                //xlChart.ChartTitle.Font.Size = 24;
                //xlChart.ChartTitle.Shadow = true;
                //xlChart.ChartTitle.Border.LineStyle = XlLineStyle.xlContinuous;

                Axis valueAxis = (Axis)chart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary);
                //valueAxis.AxisTitle.Orientation = -90;
                valueAxis.TickLabels.Font.Size = 12;
                valueAxis.TickLabels.Font.Bold = true;
                valueAxis.HasMajorGridlines = true;
                Axis categoryAxis = (Axis)chart.Axes(XlAxisType.xlCategory, XlAxisGroup.xlPrimary);
                //categoryAxis.AxisTitle.Font.Name = "MS UI Gothic";
                categoryAxis.TickLabels.Font.Size = 10;
                categoryAxis.TickLabels.Font.Bold = false;
                categoryAxis.TickLabelSpacing = 1;categoryAxis.TickLabelSpacingIsAuto = true;
                categoryAxis.CategoryType = XlCategoryType.xlTimeScale;
                categoryAxis.BaseUnitIsAuto = true;
                categoryAxis.TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow;
                categoryAxis.HasMajorGridlines = true;
                //categoryAxis.MinimumScaleIsAuto = true;
                //categoryAxis.MaximumScaleIsAuto = true;
            }

            //xlSheet.Rows[breakRow + 58].PageBreak = 1;


        }



        public bool ChartReportHelper(string path, int rowcnt, string xAixsTitle, int lenstart, int lentend, int colCont, out int len)
        {
            len = 0;
            int a = 1;
            if (a > 0)
                return true;
            try
            {
                //path = "d:\\数据测试_表面位移_第1周期-第4周期数据.xls";
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add(path));


                ThisApplication.DisplayAlerts = false;


                xlSheet = ThisWorkbook.Sheets.get_Item(0);

                //this.CreateChart(xAixsTitle, lenstart, lenend, colcnt);
                ExceptionLog.ExceptionWrite("开始保存");
                ThisWorkbook.SaveAs(path, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                ExceptionLog.ExceptionWrite("保存成功");
                return true;

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                ExceptionLog.ExceptionWrite("数据表格生成曲线失败，失败原因" + ex.Message);
                return false;
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();
                //this.Close();
            }


        }

        public bool ChartSingleReportHelper(string path, ChartCreateEnviroment cce)
        {
            try
            {
                //path = "d:\\数据测试_表面位移_第1周期-第4周期数据.xls";
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add(path));

                ThisApplication.DisplayAlerts = false;


                //this.DeleteSheet();
                //this.AddDatasheet();
                //this.LoadDatadb();
                //this.WorkSheetCopy("D5","D5");
                //List<string> sheetsName = new List<string>();
                //foreach (Worksheet sheet in ThisWorkbook.Sheets)
                //{
                //    sheetsName.Add(sheet.Name);
                //}

                int n = 1;
                xlSheet = ThisWorkbook.Sheets.get_Item(1);
                ThisApplication.DisplayAlerts = false;


                //xlSheet = ThisWorkbook.Sheets.get_Item(sheetName);
                //this.CreateChart(xAixsTitle, lenstart, lenend, breakRow);
                this.CreateChart(cce.pointname, cce.cstartlen, cce.cendlen, cce.pageIndex, n);
                n++;


                //foreach (string sheetName in sheetsName)
                //{
                //    chartcreate(sheetName, rowcnt, xAixsTitle,n);
                //    break;
                //}

                ExceptionLog.ExceptionWrite("开始保存");
                ThisWorkbook.SaveAs(path, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                ExceptionLog.ExceptionWrite("保存成功");
                return true;

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                ExceptionLog.ExceptionWrite("数据表格生成曲线失败，失败原因" + ex.Message);
                return false;
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();

                //this.Close();
            }

            return true;
        }

        public bool ChartListReportHelper(string path, List<ChartCreateEnviroment> lce)
        {

            try
            {
                //path = "d:\\数据测试_表面位移_第1周期-第4周期数据.xls";
                ThisApplication = new Application();
                m_objBooks = (Workbooks)ThisApplication.Workbooks;
                ThisWorkbook = (_Workbook)(m_objBooks.Add(path));

                ThisApplication.DisplayAlerts = false;


                //this.DeleteSheet();
                //this.AddDatasheet();
                //this.LoadDatadb();
                //this.WorkSheetCopy("D5","D5");
                //List<string> sheetsName = new List<string>();
                //foreach (Worksheet sheet in ThisWorkbook.Sheets)
                //{
                //    sheetsName.Add(sheet.Name);
                //}

                int n = 1;
                xlSheet = ThisWorkbook.Sheets.get_Item(6);
                xlDataSheet = ThisWorkbook.Sheets.get_Item(8);
                ThisApplication.DisplayAlerts = false;
                foreach (ChartCreateEnviroment cce in lce)
                {

                    //xlSheet = ThisWorkbook.Sheets.get_Item(sheetName);
                    //this.CreateChart(xAixsTitle, lenstart, lenend, breakRow);
                    this.CreateChart(cce.pointname, cce.cstartlen, cce.cendlen, cce.pageIndex, n);
                    n++;

                }
                //foreach (string sheetName in sheetsName)
                //{
                //    chartcreate(sheetName, rowcnt, xAixsTitle,n);
                //    break;
                //}

                ExceptionLog.ExceptionWrite("开始保存");
                ThisWorkbook.SaveAs(path, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                ExceptionLog.ExceptionWrite("保存成功");
                return true;

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                ExceptionLog.ExceptionWrite("数据表格生成曲线失败，失败原因" + ex.Message);
                return false;
            }
            finally
            {
                ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
                ThisApplication.Workbooks.Close();

                ThisApplication.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
                ThisWorkbook = null;
                ThisApplication = null;
                GC.Collect();

                //this.Close();
            }

            return true;
        }



        //public bool ChartReportHelper(string xAixsTitle,int lenstart,int lenend,int breakRow)
        //{
        //    //int a = 1;
        //    //if(a>0)
        //    //return true;
        //    try
        //    {
        //        //path = "d:\\数据测试_表面位移_第1周期-第4周期数据.xls";
        //        ThisApplication = new Application();
        //        m_objBooks = (Workbooks)ThisApplication.Workbooks;
        //        ThisWorkbook = (_Workbook)(m_objBooks.Add(path));

        //        ThisApplication.DisplayAlerts = false;


        //        //this.DeleteSheet();
        //        //this.AddDatasheet();
        //        //this.LoadDatadb();
        //        //this.WorkSheetCopy("D5","D5");
        //        List<string> sheetsName = new List<string>();
        //        foreach (Worksheet sheet in ThisWorkbook.Sheets)
        //        {
        //            sheetsName.Add(sheet.Name);
        //        }

        //        int n = 1;
        //        foreach (string sheetName in sheetsName)
        //        {
        //            ThisApplication.DisplayAlerts = false;
        //            xlSheet = ThisWorkbook.Sheets.get_Item(sheetName);
        //            this.CreateChart(xAixsTitle, lenstart, lenend, breakRow);


        //        }
        //        //foreach (string sheetName in sheetsName)
        //        //{
        //        //    chartcreate(sheetName, rowcnt, xAixsTitle,n);
        //        //    break;
        //        //}

        //        ExceptionLog.ExceptionWrite("开始保存");
        //        ThisWorkbook.SaveAs(path, Type.Missing, Type.Missing,
        //                    Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
        //                Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //        ExceptionLog.ExceptionWrite("保存成功");
        //        return true;

        //    }
        //    catch (Exception ex)
        //    {
        //        //MessageBox.Show(ex.Message);
        //        ExceptionLog.ExceptionWrite("数据表格生成曲线失败，失败原因" + ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        ThisWorkbook.Close(Type.Missing, Type.Missing, Type.Missing);
        //        ThisApplication.Workbooks.Close();

        //        ThisApplication.Quit();
        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisWorkbook);
        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(ThisApplication);
        //        ThisWorkbook = null;
        //        ThisApplication = null;
        //        GC.Collect();

        //        //this.Close();
        //    }


        //}
        #endregion
        #region 曲线图






        #endregion
    }
}
