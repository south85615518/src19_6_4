﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Tool.DATAREPORT.MCU
{
    public class GaugeDataFill
    {
        public ExcelHelperHandle ExcelHelper { get; set; }
        public  void DataFill(string xmname, string tabHead, List<ReportFillEnviroment> lrf )
        {
            
            string[] tabName = tabHead.Split(',');
            int i = 0;
            ExcelHelper.xSheet.Activate();
            //ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 4, 12);
            //ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 5, 12);
            //ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 6, 12);
            //ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 7, 12);
            //ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 8, 12);
            //ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 9, 12);
            //ExcelHelper.ColumnWidthSet(ExcelHelper.xSheet, 10, 12);
            for (i = 0; i < lrf.Count; i++)
            {
                ExcelHelper.GaugeReportTemplateCreate(xmname, tabName, lrf[i].dt, lrf[i].index);
            }
            
        }
    }
}
