﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace Tool.TCP
{
    public class TcpHelper
    {

        public static bool TcpPortTest(string ip,int port,out string mssg)
        {
            mssg = "";
            TcpListener tcpListener = null;
            try
            {
                tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(ip), port));
                tcpListener.Start();
                mssg = string.Format("启动IP:{0}端口:{1}成功", ip, port);
                return true;
            }
            catch (Exception ex)
            {
                mssg = string.Format("IP:{0}端口:{1}启动出错,错误信息:" + ex.Message, ip, port);
                return false;
            }
            finally
            {
                tcpListener.Server.Close();
                tcpListener.Stop();
            }

        }


    }
}
