﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
namespace Tool
{
    public class TcpServer
    {
        private string strFile;

        public string StrFile
        {
            get { return strFile; }
            set { strFile = value; }
        }
        public byte[] sendbytes { get; set; }
        private bool isFile = true;
        private bool ready = true;
        public static TcpClient client = new TcpClient();//客户端
        public static TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 13352));

        //根据服务器的IP地址和侦听的端口连接
        //连接服务器
        public static string ServerConncet(string ip, string port)
        {

            //tcpListener.Start();
            try
            {
                if (client == null)
                {
                    client = new TcpClient();
                }
                if (client.Connected)
                {

                    //连接成功的消息机制  详细见DEMO

                    return "成功连接上了服务器!";

                }
                else
                {
                    client.Connect(ip, Convert.ToInt32(port));
                    return "成功连接上了服务器!";
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("服务器连接出错");
                
                return "服务器连接失败!";

            }



        }
        //向服务器发送数据
        public static void SendMessToServer(string mess, string ip, string port)
        {
            if (!client.Connected)
            {
                client = new TcpClient();
                client.Connect(ip, Convert.ToInt32(port));
            }
            NetworkStream streamToServer = client.GetStream();
            byte[] buffer = Encoding.UTF8.GetBytes(mess); //msg为发送的字符串   
            try
            {



                streamToServer.Write(buffer, 0, buffer.Length);     // 发往服务器
                //接受服务器回发的信息
                //RecvMessServer(tcp,ip,port);
                client.Close();


            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //侦听
        //发送文件
        public string SendBytes(string ip, string port)
        {
            //if (strFile != "")//strFile是全局变量，用于保存发送发的文件路径
            //{
            //    //发送传送文件请求
            //    isFile = true;//isFile是个bool类型的全局变量，初始值为false，当为true是在P2P另一端弹出是否接收文件对话框
            //}
            //对方允许接收文件
            if (ready == true)//ready是bool类型全局变量，初始值为false，用于判断对方是否接收文件
            {
                //准备发送
                FileStream file = null;
                try
                {
                    //file = new FileStream(strFile, FileMode.Open, FileAccess.Read);
                    //BinaryReader binaryReader = new BinaryReader(file, Encoding.UTF8);

                    client.Client.Send(sendbytes, sendbytes.Length, SocketFlags.None);

                    client.Client.Shutdown(SocketShutdown.Send);
                    //等待服务器返回的传输结果标志
                    string result = ReciveServerMssg(ip, port);
                    //binaryReader.Close();
                    //file.Close();

                    return result;
                }
                catch (Exception ex)
                {
                    if (file != null)
                        file.Close();
                    throw (ex);
                }
            }
            else
            {
                return "success";
                ExceptionLog.ExceptionWrite("对方拒绝接收文件!");
            }
        }

        //侦听
        //发送文件
        public string SendString(string ip, string port)
        {
            //if (strFile != "")//strFile是全局变量，用于保存发送发的文件路径
            //{
            //    //发送传送文件请求
            //    isFile = true;//isFile是个bool类型的全局变量，初始值为false，当为true是在P2P另一端弹出是否接收文件对话框
            //}
            //对方允许接收文件
            if (ready == true)//ready是bool类型全局变量，初始值为false，用于判断对方是否接收文件
            {
                //准备发送
                FileStream file = null;
                try
                {
                    //file = new FileStream(strFile, FileMode.Open, FileAccess.Read);
                    //BinaryReader binaryReader = new BinaryReader(file, Encoding.UTF8);
                    sendbytes = Encoding.UTF8.GetBytes(this.strFile);
                    client.Client.Send(sendbytes, sendbytes.Length, SocketFlags.None);

                    client.Client.Shutdown(SocketShutdown.Send);
                    //等待服务器返回的传输结果标志
                    string result = ReciveServerMssg(ip, port);
                    //binaryReader.Close();
                    //file.Close();

                    return result;
                }
                catch (Exception ex)
                {
                    if (file != null)
                        file.Close();
                    throw (ex);
                }
            }
            else
            {
                return "success";
                ExceptionLog.ExceptionWrite("对方拒绝接收文件!");
            }
        }
        //侦听
        //发送文件
        public string SendFile(string ip, string port)
        {
            if (strFile != "")//strFile是全局变量，用于保存发送发的文件路径
            {
                //发送传送文件请求
                isFile = true;//isFile是个bool类型的全局变量，初始值为false，当为true是在P2P另一端弹出是否接收文件对话框
            }
            //对方允许接收文件
            if (ready == true)//ready是bool类型全局变量，初始值为false，用于判断对方是否接收文件
            {
                //准备发送
                FileStream file = null;
                try
                {
                    file = new FileStream(strFile, FileMode.Open, FileAccess.Read);
                    BinaryReader binaryReader = new BinaryReader(file, Encoding.UTF8);
                    byte[] b = new byte[4098];
                    int data;
                    while ((data = binaryReader.Read(b, 0, 4098)) != 0)
                    {
                        client.Client.Send(b, data, SocketFlags.None);
                    }
                    client.Client.Shutdown(SocketShutdown.Send);

                    //等待服务器返回的传输结果标志
                    string result = ReciveServerMssg(ip, port);
                    binaryReader.Close();
                    file.Close();

                    return result;
                }
                catch (Exception ex)
                {
                    if (file != null)
                        file.Close();
                    //throw (ex);
                    return ex.Message;
                }
            }
            else
            {
                return "success";
                ExceptionLog.ExceptionWrite("对方拒绝接收文件!");
            }
        }
        //接收服务器返回的标志
        public static string ReciveServerMssg(string ip, string port)
        {
            TcpServer.ServerConncet(ip, port);
            // string result = client.re
            byte[] buff = new byte[100];
            //client.Client.Connect();
            client.ReceiveTimeout =500000;
            client.Client.Receive(buff, 100, SocketFlags.None);
            return Encoding.UTF8.GetString(buff);
        }
        //接收文件
        public void ReceiveFile()
        {


            //对方发送文件
            if (isFile == true)
            {

                //开始接收文件
                FileStream fs = new FileStream("D:\\接收文件.txt", FileMode.OpenOrCreate, FileAccess.Write);
                BinaryWriter binaryWrite = new BinaryWriter(fs, Encoding.UTF8);
                Socket s = tcpListener.AcceptSocket();
                int count;
                byte[] b = new byte[4098];

                while ((count = s.Receive(b, 4098, SocketFlags.None)) != 0)
                {
                    binaryWrite.Write(b, 0, count);
                }
                binaryWrite.Close();
                fs.Close();
            }
        }
        //停止服务
        public void TcpServerStop()
        {
            tcpListener.Stop();
        }



    }
}
