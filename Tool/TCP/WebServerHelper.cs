﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FmosWebServer;

namespace Tool
{
   public  class WebServerHelper
    {
       
       public static WebServer server{get;set;}

       public WebServerHelper()
       {

           server.Start();
       }

       /// <summary>
       /// 全站仪时间任务设置
       /// </summary>
       /// <param name="xmname"></param>
       /// <param name="json"></param>
       /// <param name="mssg"></param>
       /// <returns></returns>
       public static bool MySetTime(string xmname,List<string> json,out string mssg)
       {
           return server.SetTime(xmname, json, out mssg);
           
       }
       public static bool MySetSetting(string xmname,string settingJson,out string mssg)
       {
           return server.SetSetting(xmname, settingJson, out mssg);
       }
       public static bool MyServerStart(string ip)
       {
           try
           {
               server = new WebServer(ip, 8195);
               server.Start();
               ExceptionLog.ExceptionWrite(string.Format("{0}8195端口启动成功",ip));
               return true;
           }
           catch(Exception ex)
           {
               ExceptionLog.ExceptionWrite(string.Format("{0}8195端口启动失败,失败原因:"+ex.Message,ip));
               return false;
           }

       }
       public static bool MyServerStop()
       {
           try
           {
               server.Stop();
               return true;
           }
           catch (Exception ex)
           {
               return false;
           }

       }



    }
}
