﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
namespace Tool.http
{
   public class HttpHelper
    {
       
        private string serverIp;//服务器IP

        public string ServerIp
        {
            get { return serverIp; }
            set { serverIp = value; }
        }
        private string userName;//用户名


        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        private string password;//密码

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        private string token;

        public string Token
        {
            get { return token; }
            set { token = value; }
        }
        private string port;//端口

        public string Port
        {
          get { return port; }
          set { port = value; }
        }
        /// <summary>
        /// 登录系统
        /// </summary>
        public string SendMessageToWeb()
        {
            WebClient client = new WebClient();
            string url = @"http://120.25.75.62:8001/Machine/Account/Login?UserName=huitao1&Password=htqwe&Token=y8u65aczyfwzf2fhjolyouf475xhozb4
";
            string word = client.OpenReadWithHttps(url,"");
            return word;
        }

        /// <summary>
        /// 请求网页
        /// </summary>
        public string SendMessageToWeb(string url)
        {
            WebClient client = new WebClient();
            string word = client.OpenReadWithHttps(url, "");
            return word;
        }
        /// <summary>
        /// 请求网页
        /// </summary>
        public void PostMessageToWeb(string url)
        {
            WebClient client = new WebClient();
            client.OpenReadWithHttpsPost(url, "");
            
        }
        /// <summary>
        /// 请求网页
        /// </summary>
        public string PostMessageToWeb(string url,string postdatastr)
        {
            WebClient client = new WebClient();
            return client.OpenReadWithHttps(url, postdatastr);

        }
        /// <summary>
        /// 请求网页
        /// </summary>
        public string PostMessageToWeb(string method,string url, string postdatastr)
        {
            WebClient client = new WebClient();
            return client.OpenReadWithHttps(method,url,postdatastr);

        }
        public HttpHelper()
        { 
        }
        public HttpHelper(string ip,string port,string useName,string password,string token)
        {
            this.ServerIp = ip;
            this.Port = port;
            this.UserName = useName;
            this.Password = password;
            this.Token = token;
        }
        public void FileUpload(string url,string path)
        {
            WebClient client = new WebClient();
            string word = client.Upload_Request(url, path, Path.GetFileName(path));
        }
        /// <summary>
        /// 获取监测工程列表
        /// </summary>
        /// <returns></returns>
       public string GetPList( )
       {
           WebClient client = new WebClient();
           string url = @"http://120.25.75.62:8001/Machine/Project/GetList?iDisplayStart=&iDisplayLength=&State=&IsAlarm=&keyword=
";
           string word = client.OpenReadWithHttps(url, "");
          
           return word;
       }
       public string GetJcxmList(string projectId)
       {
           WebClient client = new WebClient();
           string url = string.Format("http://120.25.75.62:8001/Machine/ProjectSubject/List?ProjectID={0}",projectId);
           string word = client.OpenReadWithHttps(url, "");

           return word;
       }
        //上传数据文件
       public string FileUpload(string path)
       {
           WebClient client = new WebClient();
           string url = "http://120.25.75.62:8001/Machine/Upload/UploadFile?fileType=TestPointData";
           string word = client.Upload_Request(url,path,Path.GetFileName(path));
           return word;
       }
       public string DataImport(string path,string subID,string proID)
       {
           WebClient client = new WebClient();
           string url = string.Format("http://120.25.75.62:8001/Machine/MonitorInfo/Add?ProjectID={1}&SubjectID={0}&testPointDataSourceFile={2}"
               ,proID,subID,path);
           string word = client.OpenReadWithHttps(url, "");
           return word;
       }

    }
}
