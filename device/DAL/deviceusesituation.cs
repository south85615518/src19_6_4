﻿/**  版本信息模板在安装目录下，可自行修改。
* deviceusesituation.cs
*
* 功 能： N/A
* 类 名： deviceusesituation
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/23 11:08:37   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//Please add references
namespace device.DAL
{
	/// <summary>
	/// 数据访问类:deviceusesituation
	/// </summary>
	public partial class deviceusesituation
	{
		public deviceusesituation()
		{}
		#region  BasicMethod
        public static database db = new database();
		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("id", "deviceusesituation"); 
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from deviceusesituation");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;

            return false;// OdbcSQLHelper.Exists(strSql.ToString(), parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(device.Model.deviceusesituation model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(model.unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into deviceusesituation(");
			strSql.Append("monitorID,monitorName,xmno,recivetime,passmanname,xmname,hasinspection,inspectionproofPath,inspectiontimes,inspectiontime,inspectresult,deviceno,remark,address,nextinspectiontime)");
			strSql.Append(" values (");
			strSql.Append("@monitorID,@monitorName,@xmno,@recivetime,@passmanname,@xmname,@hasinspection,@inspectionproofPath,@inspectiontimes,@inspectiontime,@inspectresult,@deviceno,@remark,@address,@nextinspectiontime)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@monitorID", OdbcType.VarChar,100),
					new OdbcParameter("@monitorName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@recivetime", OdbcType.DateTime),
					new OdbcParameter("@passmanname", OdbcType.VarChar,100),
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
					new OdbcParameter("@hasinspection", OdbcType.TinyInt,1),
					new OdbcParameter("@inspectionproofPath", OdbcType.VarChar,500),
					new OdbcParameter("@inspectiontimes", OdbcType.Int,11),
					new OdbcParameter("@inspectiontime", OdbcType.DateTime),
					new OdbcParameter("@inspectresult", OdbcType.VarChar,100),
					new OdbcParameter("@deviceno", OdbcType.VarChar,100),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@address", OdbcType.VarChar,200),
					new OdbcParameter("@nextinspectiontime", OdbcType.DateTime,100)};
			parameters[0].Value = model.monitorID;
			parameters[1].Value = model.monitorName;
			parameters[2].Value = model.xmno;
			parameters[3].Value = model.recivetime;
			parameters[4].Value = model.passmanname;
			parameters[5].Value = model.xmname;
			parameters[6].Value = model.hasinspection;
			parameters[7].Value = model.inspectionproofPath;
			parameters[8].Value = model.inspectiontimes;
			parameters[9].Value = model.inspectiontime;
			parameters[10].Value = model.inspectresult;
			parameters[11].Value = model.deviceno;
			parameters[12].Value = model.remark;
			parameters[13].Value = model.address;
			parameters[14].Value = model.nextinspectiontime;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(device.Model.deviceusesituation model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(model.unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update deviceusesituation set ");
			strSql.Append("monitorID=@monitorID,");
			strSql.Append("monitorName=@monitorName,");
			strSql.Append("xmno=@xmno,");
			strSql.Append("recivetime=@recivetime,");
			strSql.Append("passmanname=@passmanname,");
			strSql.Append("xmname=@xmname,");
			strSql.Append("hasinspection=@hasinspection,");
			strSql.Append("inspectionproofPath=@inspectionproofPath,");
			strSql.Append("inspectiontimes=@inspectiontimes,");
			strSql.Append("inspectiontime=@inspectiontime,");
			strSql.Append("inspectresult=@inspectresult,");
			strSql.Append("deviceno=@deviceno,");
			strSql.Append("remark=@remark,");
			strSql.Append("address=@address,");
			strSql.Append("nextinspectiontime=@nextinspectiontime");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@monitorID", OdbcType.VarChar,100),
					new OdbcParameter("@monitorName", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@recivetime", OdbcType.DateTime),
					new OdbcParameter("@passmanname", OdbcType.VarChar,100),
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
					new OdbcParameter("@hasinspection", OdbcType.TinyInt,1),
					new OdbcParameter("@inspectionproofPath", OdbcType.VarChar,500),
					new OdbcParameter("@inspectiontimes", OdbcType.Int,11),
					new OdbcParameter("@inspectiontime", OdbcType.DateTime),
					new OdbcParameter("@inspectresult", OdbcType.VarChar,100),
					new OdbcParameter("@deviceno", OdbcType.VarChar,100),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@address", OdbcType.VarChar,200),
					new OdbcParameter("@nextinspectiontime", OdbcType.DateTime,100),
					new OdbcParameter("@id", OdbcType.Int,11)};
			parameters[0].Value = model.monitorID;
			parameters[1].Value = model.monitorName;
			parameters[2].Value = model.xmno;
			parameters[3].Value = model.recivetime;
			parameters[4].Value = model.passmanname;
			parameters[5].Value = model.xmname;
			parameters[6].Value = model.hasinspection;
			parameters[7].Value = model.inspectionproofPath;
			parameters[8].Value = model.inspectiontimes;
			parameters[9].Value = model.inspectiontime;
			parameters[10].Value = model.inspectresult;
			parameters[11].Value = model.deviceno;
			parameters[12].Value = model.remark;
			parameters[13].Value = model.address;
			parameters[14].Value = model.nextinspectiontime;
			parameters[15].Value = model.id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string unitname,int id)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from deviceusesituation ");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        ///// <summary>
        ///// 批量删除数据
        ///// </summary>
        //public bool DeleteList(string idlist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from deviceusesituation ");
        //    strSql.Append(" where id in ("+idlist + ")  ");
        //    int rows=OdbcSQLHelper.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool GetModel(string unitname,int id,out device.Model.deviceusesituation model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,monitorID,monitorName,xmno,recivetime,passmanname,xmname,hasinspection,inspectionproofPath,inspectiontimes,inspectiontime,inspectresult,deviceno,remark,address,nextinspectiontime from deviceusesituation ");
			strSql.Append(" where id=@id");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int)
			};
			parameters[0].Value = id;

			model=new device.Model.deviceusesituation();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model =  DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public device.Model.deviceusesituation DataRowToModel(DataRow row)
		{
			device.Model.deviceusesituation model=new device.Model.deviceusesituation();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["monitorID"]!=null)
				{
					model.monitorID=row["monitorID"].ToString();
				}
				if(row["monitorName"]!=null)
				{
					model.monitorName=row["monitorName"].ToString();
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["recivetime"]!=null && row["recivetime"].ToString()!="")
				{
					model.recivetime=DateTime.Parse(row["recivetime"].ToString());
				}
				if(row["passmanname"]!=null)
				{
					model.passmanname=row["passmanname"].ToString();
				}
				if(row["xmname"]!=null)
				{
					model.xmname=row["xmname"].ToString();
				}
				if(row["hasinspection"]!=null && row["hasinspection"].ToString()!="")
				{
					model.hasinspection=int.Parse(row["hasinspection"].ToString());
				}
				if(row["inspectionproofPath"]!=null)
				{
					model.inspectionproofPath=row["inspectionproofPath"].ToString();
				}
				if(row["inspectiontimes"]!=null && row["inspectiontimes"].ToString()!="")
				{
					model.inspectiontimes=int.Parse(row["inspectiontimes"].ToString());
				}
				if(row["inspectiontime"]!=null && row["inspectiontime"].ToString()!="")
				{
					model.inspectiontime=DateTime.Parse(row["inspectiontime"].ToString());
				}
				if(row["inspectresult"]!=null)
				{
					model.inspectresult=row["inspectresult"].ToString();
				}
				if(row["deviceno"]!=null)
				{
					model.deviceno=row["deviceno"].ToString();
				}
				if(row["remark"]!=null)
				{
					model.remark=row["remark"].ToString();
				}
				if(row["address"]!=null)
				{
					model.address=row["address"].ToString();
				}
				if(row["nextinspectiontime"]!=null)
				{
					model.nextinspectiontime=Convert.ToDateTime(row["nextinspectiontime"]);
				}
			}
			return model;
		}

        //获取设备表
        public bool UnitdeviceusesituationTableLoad(string unitname, int startPageIndex, int pageSize,List<string> searchlist ,string sord, out DataTable dt)
        {
            dt = new DataTable();
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select   *  from  deviceusesituation  where  {0}   order by   recivetime asc limit @startPageIndex,@endPageIndex ",string.Join("  and   ",searchlist));
            //ExceptionLog.ExceptionWrite(strSql.ToString());
            OdbcParameter[] parameters = {
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = (startPageIndex - 1) * pageSize;
            parameters[1].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool UnitdeviceusesituationTableCountLoad(string unitname, List<string> searchlist, out string totalCont)
        {
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            string sql = string.Format("select count(1) from  deviceusesituation  where {0} ", string.Join("  and   ", searchlist));
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }
		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

