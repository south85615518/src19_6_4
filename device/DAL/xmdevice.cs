﻿/**  版本信息模板在安装目录下，可自行修改。
* xmdevice.cs
*
* 功 能： N/A
* 类 名： xmdevice
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/22 14:28:18   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
//Please add references
namespace device.DAL
{
	/// <summary>
	/// 数据访问类:xmdevice
	/// </summary>
	public partial class xmdevice
	{
		public xmdevice()
		{}
		#region  BasicMethod

        public static database db = new database();

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(device.Model.xmdevice model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(model.unitname);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into xmdevice(");
            strSql.Append("unitname,senortype,senorno,senorname,producetiontime,effectionyears,useing,buyfrom,state,inspectionalarmvalue,alarm,currentaddress,lastinspectiontime,inspectiontimes,managerID,monitorID,monitorname,xmno,xmname,inspectionproof,lastinspectionman,granted)");
            strSql.Append(" values (");
            strSql.Append("@unitname,@senortype,@senorno,@senorname,@producetiontime,@effectionyears,@useing,@buyfrom,@state,@inspectionalarmvalue,@alarm,@currentaddress,@lastinspectiontime,@inspectiontimes,@managerID,@monitorID,@monitorname,@xmno,@xmname,@inspectionproof,@lastinspectionman,@granted)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@unitname", OdbcType.VarChar,200),
					new OdbcParameter("@senortype", OdbcType.VarChar,100),
					new OdbcParameter("@senorno", OdbcType.VarChar,200),
					new OdbcParameter("@senorname", OdbcType.VarChar,100),
					new OdbcParameter("@producetiontime", OdbcType.DateTime),
					new OdbcParameter("@effectionyears", OdbcType.Int,11),
					new OdbcParameter("@useing", OdbcType.TinyInt,4),
					new OdbcParameter("@buyfrom", OdbcType.VarChar,100),
					new OdbcParameter("@state", OdbcType.VarChar,100),
					new OdbcParameter("@inspectionalarmvalue", OdbcType.Int,11),
					new OdbcParameter("@alarm", OdbcType.TinyInt,2),
					new OdbcParameter("@currentaddress", OdbcType.VarChar,200),
					new OdbcParameter("@lastinspectiontime", OdbcType.DateTime),
					new OdbcParameter("@inspectiontimes", OdbcType.Int,11),
					new OdbcParameter("@managerID", OdbcType.VarChar,200),
					new OdbcParameter("@monitorID", OdbcType.VarChar,100),
					new OdbcParameter("@monitorname", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@xmname", OdbcType.VarChar,200),
					new OdbcParameter("@inspectionproof", OdbcType.VarChar,500),
					new OdbcParameter("@lastinspectionman", OdbcType.VarChar,200),
                    new OdbcParameter("@granted", OdbcType.TinyInt,2),
                                         };
            parameters[0].Value = model.unitname;
            parameters[1].Value = model.senortype;
            parameters[2].Value = model.senorno;
            parameters[3].Value = model.senorname;
            parameters[4].Value = model.producetiontime;
            parameters[5].Value = model.effectionyears;
            parameters[6].Value = model.useing;
            parameters[7].Value = model.buyfrom;
            parameters[8].Value = model.state;
            parameters[9].Value = model.inspectionalarmvalue;
            parameters[10].Value = model.alarm;
            parameters[11].Value = model.currentaddress;
            parameters[12].Value = model.lastinspectiontime;
            parameters[13].Value = model.inspectiontimes;
            parameters[14].Value = model.managerID;
            parameters[15].Value = model.monitorID;
            parameters[16].Value = model.monitorname;
            parameters[17].Value = model.xmno;
            parameters[18].Value = model.xmname;
            parameters[19].Value = model.inspectionproof;
            parameters[20].Value = model.lastinspectionman;
            parameters[21].Value = model.granted;
			int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        ///// <summary>
        ///// 更新一条数据
        ///// </summary>
        //public bool Update(device.Model.xmdevice model)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(model.unitname);
        //    strSql.Append("update xmdevice set ");
        //    strSql.Append("senortype=@senortype,");
        //    strSql.Append("senorno=@senorno,");
        //    strSql.Append("senorname=@senorname,");
        //    strSql.Append("producetiontime=@producetiontime,");
        //    strSql.Append("effectionyears=@effectionyears,");
        //    strSql.Append("useing=@useing,");
        //    strSql.Append("buyfrom=@buyfrom,");
        //    strSql.Append("state=@state,");
        //    strSql.Append("inspectionalarmvalue=@inspectionalarmvalue,");
        //    strSql.Append("inspectiontimes=@inspectiontimes, ");
        //    strSql.Append("lastinspectiontime=@lastinspectiontime, ");
        //    strSql.Append("monitorID=@monitorID, ");
        //    strSql.Append("monitorName=@monitorName, ");
        //    strSql.Append("xmno=@xmno, ");
        //    strSql.Append("xmname=@xmname, ");
        //    strSql.Append("address=@address ");
        //    strSql.Append("  where id=@id ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@senortype", OdbcType.VarChar,100),
        //            new OdbcParameter("@senorno", OdbcType.VarChar,200),
        //            new OdbcParameter("@senorname", OdbcType.VarChar,100),
        //            new OdbcParameter("@producetiontime", OdbcType.DateTime),
        //            new OdbcParameter("@effectionyears", OdbcType.Int,11),
        //            new OdbcParameter("@useing", OdbcType.TinyInt,4),
        //            new OdbcParameter("@buyfrom", OdbcType.VarChar,100),
        //            new OdbcParameter("@state", OdbcType.VarChar,100),
        //            new OdbcParameter("@inspectionalarmvalue", OdbcType.Int),
        //            new OdbcParameter("@inspectiontimes", OdbcType.Int),
        //            new OdbcParameter("@lastinspectiontime", OdbcType.DateTime),
        //            new OdbcParameter("@monitorID",OdbcType.VarChar,100),
        //            new OdbcParameter("@monitorName", OdbcType.VarChar,100),
        //            new OdbcParameter("@xmno", OdbcType.Int),
        //            new OdbcParameter("@xmname", OdbcType.VarChar,100),
        //            new OdbcParameter("@address", OdbcType.VarChar,100),
        //            new OdbcParameter("@id", OdbcType.Int,11)
        //                                 };
        //    parameters[0].Value = model.senortype;
        //    parameters[1].Value = model.senorno;
        //    parameters[2].Value = model.senorname;
        //    parameters[3].Value = model.producetiontime;
        //    parameters[4].Value = model.effectionyears;
        //    parameters[5].Value = model.useing;
        //    parameters[6].Value = model.buyfrom;
        //    parameters[7].Value = model.state;
        //    parameters[8].Value = model.inspectionalarmvalue;
        //    parameters[9].Value = model.inspectiontimes;
        //    parameters[10].Value = model.lastinspectiontime;
        //    parameters[11].Value = model.monitorID;
        //    parameters[12].Value = model.monitorName;
        //    parameters[13].Value = model.xmno;
        //    parameters[14].Value = model.xmname;
        //    parameters[15].Value = model.address;
        //    parameters[16].Value = model.id;
        //    int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(device.Model.xmdevice model)
        {
            
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(model.unitname);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update xmdevice set ");
            strSql.Append("unitname=@unitname,");
            strSql.Append("senortype=@senortype,");
            strSql.Append("senorno=@senorno,");
            strSql.Append("senorname=@senorname,");
            strSql.Append("producetiontime=@producetiontime,");
            strSql.Append("effectionyears=@effectionyears,");
            strSql.Append("useing=@useing,");
            strSql.Append("buyfrom=@buyfrom,");
            strSql.Append("state=@state,");
            strSql.Append("inspectionalarmvalue=@inspectionalarmvalue,");
            strSql.Append("alarm=@alarm,");
            strSql.Append("currentaddress=@currentaddress,");
            strSql.Append("lastinspectiontime=@lastinspectiontime,");
            strSql.Append("inspectiontimes=@inspectiontimes,");
            strSql.Append("managerID=@managerID,");
            strSql.Append("monitorID=@monitorID,");
            strSql.Append("monitorname=@monitorname,");
            strSql.Append("xmno=@xmno,");
            strSql.Append("xmname=@xmname,");
            strSql.Append("inspectionproof=@inspectionproof,");
            strSql.Append("lastinspectionman=@lastinspectionman,");
            strSql.Append("granted=@granted ");
            strSql.Append(" where id=@id");
            OdbcParameter[] parameters = {
					new OdbcParameter("@unitname", OdbcType.VarChar,200),
					new OdbcParameter("@senortype", OdbcType.VarChar,100),
					new OdbcParameter("@senorno", OdbcType.VarChar,200),
					new OdbcParameter("@senorname", OdbcType.VarChar,100),
					new OdbcParameter("@producetiontime", OdbcType.DateTime),
					new OdbcParameter("@effectionyears", OdbcType.Int,11),
					new OdbcParameter("@useing", OdbcType.TinyInt,4),
					new OdbcParameter("@buyfrom", OdbcType.VarChar,100),
					new OdbcParameter("@state", OdbcType.VarChar,100),
					new OdbcParameter("@inspectionalarmvalue", OdbcType.Int,11),
					new OdbcParameter("@alarm", OdbcType.TinyInt,2),
					new OdbcParameter("@currentaddress", OdbcType.VarChar,200),
					new OdbcParameter("@lastinspectiontime", OdbcType.DateTime),
					new OdbcParameter("@inspectiontimes", OdbcType.Int,11),
					new OdbcParameter("@managerID", OdbcType.VarChar,200),
					new OdbcParameter("@monitorID", OdbcType.VarChar,100),
					new OdbcParameter("@monitorname", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@xmname", OdbcType.VarChar,200),
					new OdbcParameter("@inspectionproof", OdbcType.VarChar,500),
					new OdbcParameter("@lastinspectionman", OdbcType.VarChar,200),
                    new OdbcParameter("@granted", OdbcType.TinyInt),
					new OdbcParameter("@id", OdbcType.Int,11)};
            parameters[0].Value = model.unitname;
            parameters[1].Value = model.senortype;
            parameters[2].Value = model.senorno;
            parameters[3].Value = model.senorname;
            parameters[4].Value = model.producetiontime;
            parameters[5].Value = model.effectionyears;
            parameters[6].Value = model.useing;
            parameters[7].Value = model.buyfrom;
            parameters[8].Value = model.state;
            parameters[9].Value = model.inspectionalarmvalue;
            parameters[10].Value = model.alarm;
            parameters[11].Value = model.currentaddress;
            parameters[12].Value = model.lastinspectiontime;
            parameters[13].Value = model.inspectiontimes;
            parameters[14].Value = model.managerID;
            parameters[15].Value = model.monitorID;
            parameters[16].Value = model.monitorname;
            parameters[17].Value = model.xmno;
            parameters[18].Value = model.xmname;
            parameters[19].Value = model.inspectionproof;
            parameters[20].Value = model.lastinspectionman;
            parameters[21].Value = model.granted;
            parameters[22].Value = model.id;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string unitname ,int id)
		{
			//该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from xmdevice ");
			strSql.Append(" where   id in (@id)  ");
			OdbcParameter[] parameters = {
                                             new OdbcParameter("@id",OdbcType.VarChar,100)

			};
            parameters[0].Value = id;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool  GetModel(string unitname,int id,out device.Model.xmdevice model)
        {
            //该表无主键信息，请自定义主键/条件字段
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select *  from xmdevice ");
            strSql.Append("  where id = @id   ");
            OdbcParameter[] parameters = {
                        new OdbcParameter("@id",OdbcType.Int)

			};
            parameters[0].Value = id;
            model = new device.Model.xmdevice();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        //设置设备的当前是否可用的状态
        public bool SetState(string unitname,int id,string state)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("update xmdevice set state = '{0}' where id = {1} ",state,id);
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        //设置设备的当前是否在用的状态
        public bool SetUseState(string unitname,int id, bool isusing)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("update   xmdevice    set    useing = @useing     where    id= {0} ",id);
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            OdbcParameter[] parameters = { new OdbcParameter("@useing",OdbcType.TinyInt,2) };
            parameters[0].Value = isusing;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        //获取设备表
        public bool XmDeviceTableLoad(string unitname,int startPageIndex, int pageSize,  List<string> searchList , string sord, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = string.Join("   and   ", searchList);
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select   unitname,senortype,senorno,senorname,producetiontime,effectionyears,useing,buyfrom,state,id,inspectionalarmvalue,alarm,currentaddress,lastinspectiontime,inspectiontimes,managerID,monitorID,monitorname,xmno,xmname,inspectionproof,lastinspectionman ,if( MONTH(SYSDATE()-lastinspectiontime)>effectionyears - inspectionalarmvalue,true,false ) as alarm,granted  from  xmdevice   where     {0}      order by  id  {1}    limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            OdbcParameter[] parameters = {
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = (startPageIndex - 1) * pageSize;
            parameters[1].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool XmDeviceModellistLoad(int xmno, out List<device.Model.xmdevice> xmdevicelist)
        {
            
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select   *  from  xmdevice   where  senorno in (select distinct(deviceno) from deviceusesituation where xmno = {0}  )    order  by  id  asc  ", xmno);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            xmdevicelist = new List<Model.xmdevice>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if(ds == null&&ds.Tables[0].Rows.Count == 0) return false;
            int i = 0;
            while(i < ds.Tables[0].Rows.Count)
            {
               xmdevicelist.Add(DataRowToModel(ds.Tables[0].Rows[i]));
               i++;
            }
            return true;

        }



        public bool XmDeviceTableCountLoad(string unitname, List<string> searchList, out string totalCont)
        {
            string searchstr = string.Join("   and   ", searchList);
            string sql = string.Format("select count(1) from  xmdevice  where   {0}",  searchstr);
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }




        //设备预警
        //获取设备表
        public bool XmDeviceAlarm(string unitname,out List<device.Model.xmdevice> modellist)
        {
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            int i = 0;
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select *,if( MONTH(SYSDATE()-lastinspectiontime)>effectionyears - inspectionalarmvalue,1,0 ) as alarm from  xmdevice   where    MONTH(SYSDATE()-lastinspectiontime)>effectionyears - inspectionalarmvalue ");
            modellist = new List<Model.xmdevice>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null || ds.Tables[0].Rows.Count == 0) return false;
            while(i < ds.Tables[0].Rows.Count)
            {

                modellist.Add(DataRowToModel(ds.Tables[0].Rows[i]));

            }
            return true;

        }
        


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public device.Model.xmdevice DataRowToModel(DataRow row)
		{
			device.Model.xmdevice model=new device.Model.xmdevice();
			if (row != null)
			{
                if (row["unitname"] != null)
                {
                    model.unitname = row["unitname"].ToString();
                }
                if (row["senortype"] != null)
                {
                    model.senortype = row["senortype"].ToString();
                }
                if (row["senorno"] != null)
                {
                    model.senorno = row["senorno"].ToString();
                }
                if (row["senorname"] != null)
                {
                    model.senorname = row["senorname"].ToString();
                }
                if (row["producetiontime"] != null && row["producetiontime"].ToString() != "")
                {
                    model.producetiontime = DateTime.Parse(row["producetiontime"].ToString());
                }
                if (row["effectionyears"] != null && row["effectionyears"].ToString() != "")
                {
                    model.effectionyears = int.Parse(row["effectionyears"].ToString());
                }
                if (row["useing"] != null && row["useing"].ToString() != "")
                {
                    model.useing =row["useing"].ToString() == "0"? false:true;
                }
                if (row["buyfrom"] != null)
                {
                    model.buyfrom = row["buyfrom"].ToString();
                }
                if (row["state"] != null)
                {
                    model.state = row["state"].ToString();
                }
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.id = int.Parse(row["id"].ToString());
                }
                if (row["inspectionalarmvalue"] != null && row["inspectionalarmvalue"].ToString() != "")
                {
                    model.inspectionalarmvalue = int.Parse(row["inspectionalarmvalue"].ToString());
                }
                if (row["alarm"] != null && row["alarm"].ToString() != "")
                {
                    model.alarm = row["alarm"].ToString() == "0"?false:true;
                }
                if (row["currentaddress"] != null)
                {
                    model.currentaddress = row["currentaddress"].ToString();
                }
                if (row["lastinspectiontime"] != null && row["lastinspectiontime"].ToString() != "")
                {
                    model.lastinspectiontime = DateTime.Parse(row["lastinspectiontime"].ToString());
                }
                if (row["inspectiontimes"] != null && row["inspectiontimes"].ToString() != "")
                {
                    model.inspectiontimes = int.Parse(row["inspectiontimes"].ToString());
                }
                if (row["managerID"] != null)
                {
                    model.managerID = row["managerID"].ToString();
                }
                if (row["monitorID"] != null)
                {
                    model.monitorID = row["monitorID"].ToString();
                }
                if (row["monitorname"] != null)
                {
                    model.monitorname = row["monitorname"].ToString();
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["xmname"] != null)
                {
                    model.xmname = row["xmname"].ToString();
                }
                if (row["inspectionproof"] != null)
                {
                    model.inspectionproof = row["inspectionproof"].ToString();
                }
                if (row["lastinspectionman"] != null)
                {
                    model.lastinspectionman = row["lastinspectionman"].ToString();
                }
                if (row["lastinspectionman"] != null)
                {
                    model.lastinspectionman = row["lastinspectionman"].ToString();
                }
                if (row["granted"] != null)
                {
                    model.granted = row["granted"].ToString() == "0"? false: true;
                }
			}
			return model;
		}

		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

