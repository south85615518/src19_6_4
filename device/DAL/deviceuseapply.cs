﻿/**  版本信息模板在安装目录下，可自行修改。
* deviceuseapply.cs
*
* 功 能： N/A
* 类 名： deviceuseapply
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/21 19:19:15   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
namespace device.DAL
{
	/// <summary>
	/// 数据访问类:deviceuseapply
	/// </summary>
	public partial class deviceuseapply
	{
		public deviceuseapply()
		{}
		#region  BasicMethod
        public static database db = new database();
        ///// <summary>
        ///// 得到最大ID
        ///// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("id", "deviceuseapply"); 
        //}

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        //public bool Exists(int id)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from deviceuseapply");
        //    strSql.Append(" where id=@id ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@id", OdbcType.Int,11)			};
        //    parameters[0].Value = id;

        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(device.Model.deviceuseapply model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(model.unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into deviceuseapply(");
			strSql.Append("id,deviceid,monitorID,ProessState,applytime,processtime,xmno,remark,xmname)");
			strSql.Append(" values (");
			strSql.Append("@id,@deviceid,@monitorID,@ProessState,@applytime,@processtime,@xmno,@remark,@xmname)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,11),
					new OdbcParameter("@deviceid", OdbcType.Int,11),
					new OdbcParameter("@monitorID", OdbcType.VarChar,200),
					new OdbcParameter("@ProessState", OdbcType.TinyInt,1),
					new OdbcParameter("@applytime", OdbcType.DateTime),
					new OdbcParameter("@processtime", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@xmname", OdbcType.VarChar,100)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.deviceid;
			parameters[2].Value = model.monitorID;
			parameters[3].Value = model.ProessState;
			parameters[4].Value = model.applytime;
			parameters[5].Value = model.processtime;
			parameters[6].Value = model.xmno;
			parameters[7].Value = model.remark;
			parameters[8].Value = model.xmname;

			int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(device.Model.deviceuseapply model)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(model.unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update deviceuseapply set ");
			strSql.Append("deviceid=@deviceid,");
			strSql.Append("monitorID=@monitorID,");
			strSql.Append("ProessState=@ProessState,");
			strSql.Append("applytime=@applytime,");
			strSql.Append("processtime=@processtime,");
			strSql.Append("xmno=@xmno,");
			strSql.Append("remark=@remark,");
			strSql.Append("xmname=@xmname");
			strSql.Append(" where id=@id ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@deviceid", OdbcType.Int,11),
					new OdbcParameter("@monitorID", OdbcType.VarChar,200),
					new OdbcParameter("@ProessState", OdbcType.TinyInt,1),
					new OdbcParameter("@applytime", OdbcType.DateTime),
					new OdbcParameter("@processtime", OdbcType.DateTime),
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
					new OdbcParameter("@id", OdbcType.Int,11)};
			parameters[0].Value = model.deviceid;
			parameters[1].Value = model.monitorID;
			parameters[2].Value = model.ProessState;
			parameters[3].Value = model.applytime;
			parameters[4].Value = model.processtime;
			parameters[5].Value = model.xmno;
			parameters[6].Value = model.remark;
			parameters[7].Value = model.xmname;
			parameters[8].Value = model.id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string unitname,int id)
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from deviceuseapply ");
			strSql.Append(" where id=@id ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,11)			};
			parameters[0].Value = id;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
        //public bool DeleteList(string idlist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from deviceuseapply ");
        //    strSql.Append(" where id in ("+idlist + ")  ");
        //    int rows=OdbcSQLHelper.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool  GetModel(string unitname,int id,out device.Model.deviceuseapply model )
		{
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,deviceid,monitorID,ProessState,applytime,processtime,xmno,remark,xmname from deviceuseapply ");
			strSql.Append(" where id=@id ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,11)			};
			parameters[0].Value = id;

			 model=new device.Model.deviceuseapply();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}
        //获取设备表
        public bool UnitDeviceApplyTableLoad(string unitname, int startPageIndex, int pageSize,  string sord, out DataTable dt)
        {
            dt = new DataTable();
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select   id,deviceid,monitorID,if(ProessState = 0,'待处理','已处理') ProessState,applytime,processtime,xmno,remark,xmname  from  deviceuseapply  order by  applytime asc limit @startPageIndex,@endPageIndex ");
            //ExceptionLog.ExceptionWrite(strSql.ToString());
            OdbcParameter[] parameters = {
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = (startPageIndex - 1) * pageSize;
            parameters[1].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool UnitDeviceApplyTableCountLoad(string unitname, out string totalCont)
        {

            string sql = string.Format("select count(1) from  deviceuseapply ");
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }

        //获取设备表
        public bool MonitorDeviceApplyTableLoad(string unitname,string monitorID ,int startPageIndex, int pageSize, string sord, out DataTable dt)
        {
            dt = new DataTable();
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select   id,deviceid,monitorID,if(ProessState = 0,'待处理','已处理'),ifnull(monitorID,'可申请','不可申请') ProessState,applytime,processtime,xmno,remark,xmname  from  deviceuseapply  where  monitorID=@monitorID  order by  applytime asc limit @startPageIndex,@endPageIndex ");
            //ExceptionLog.ExceptionWrite(strSql.ToString());
            OdbcParameter[] parameters = {
                    new OdbcParameter("@monitorID", OdbcType.VarChar,100),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = monitorID;
            parameters[1].Value = (startPageIndex - 1) * pageSize;
            parameters[2].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool MonitorDeviceApplyTableCountLoad(string unitname,string monitorID, out string totalCont)
        {
            string sql = string.Format("select count(1) from  deviceuseapply monitorID= '"+monitorID+"'  ");
            OdbcConnection conn = db.GetUnitSurveyStanderConn(unitname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public device.Model.deviceuseapply DataRowToModel(DataRow row)
		{
			device.Model.deviceuseapply model=new device.Model.deviceuseapply();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["deviceid"]!=null && row["deviceid"].ToString()!="")
				{
					model.deviceid=int.Parse(row["deviceid"].ToString());
				}
				if(row["monitorID"]!=null)
				{
					model.monitorID=row["monitorID"].ToString();
				}
				if(row["ProessState"]!=null && row["ProessState"].ToString()!="")
				{
					model.ProessState=int.Parse(row["ProessState"].ToString());
				}
				if(row["applytime"]!=null && row["applytime"].ToString()!="")
				{
					model.applytime=DateTime.Parse(row["applytime"].ToString());
				}
				if(row["processtime"]!=null && row["processtime"].ToString()!="")
				{
					model.processtime=DateTime.Parse(row["processtime"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["remark"]!=null)
				{
					model.remark=row["remark"].ToString();
				}
				if(row["xmname"]!=null)
				{
					model.xmname=row["xmname"].ToString();
				}
			}
			return model;
		}

		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

