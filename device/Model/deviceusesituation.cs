﻿/**  版本信息模板在安装目录下，可自行修改。
* deviceusesituation.cs
*
* 功 能： N/A
* 类 名： deviceusesituation
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/23 11:08:37   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace device.Model
{
	/// <summary>
	/// deviceusesituation:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class deviceusesituation
	{
		public deviceusesituation()
		{}
		#region Model
		private int _id;
        private string _unitname;

        public string unitname
        {
            get { return _unitname; }
            set { _unitname = value; }
        }
		private string _monitorid;
		private string _monitorname;
		private int _xmno;
		private DateTime _recivetime;
		private string _passmanname;
		private string _xmname;
		private int _hasinspection;
		private string _inspectionproofpath;
		private int _inspectiontimes;
		private DateTime _inspectiontime;
		private string _inspectresult;
		private string _deviceno;
		private string _remark;
		private string _address;
		private DateTime _nextinspectiontime;
		/// <summary>
		/// auto_increment
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string monitorID
		{
			set{ _monitorid=value;}
			get{return _monitorid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string monitorName
		{
			set{ _monitorname=value;}
			get{return _monitorname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime recivetime
		{
			set{ _recivetime=value;}
			get{return _recivetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string passmanname
		{
			set{ _passmanname=value;}
			get{return _passmanname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xmname
		{
			set{ _xmname=value;}
			get{return _xmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int hasinspection
		{
			set{ _hasinspection=value;}
			get{return _hasinspection;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string inspectionproofPath
		{
			set{ _inspectionproofpath=value;}
			get{return _inspectionproofpath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int inspectiontimes
		{
			set{ _inspectiontimes=value;}
			get{return _inspectiontimes;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime inspectiontime
		{
			set{ _inspectiontime=value;}
			get{return _inspectiontime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string inspectresult
		{
			set{ _inspectresult=value;}
			get{return _inspectresult;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string deviceno
		{
			set{ _deviceno=value;}
			get{return _deviceno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime nextinspectiontime
		{
			set{ _nextinspectiontime=value;}
			get{return _nextinspectiontime;}
		}
		#endregion Model

	}
}

