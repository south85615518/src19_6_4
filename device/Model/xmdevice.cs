﻿/**  版本信息模板在安装目录下，可自行修改。
* xmdevice.cs
*
* 功 能： N/A
* 类 名： xmdevice
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/22 14:28:18   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace device.Model
{
	/// <summary>
	/// xmdevice:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class xmdevice
	{
		public xmdevice()
		{}
		#region Model
		private string _unitname;
		private string _senortype;
		private string _senorno;
		private string _senorname;
		private DateTime _producetiontime;
		private int _effectionyears;
		private bool _useing;
		private string _buyfrom;
		private string _state;
		private int _id;
		private int _inspectionalarmvalue;
		private bool _alarm=false;
		private string _currentaddress;
		private DateTime _lastinspectiontime;
		private int _inspectiontimes=0;
		private string _managerid;
		private string _monitorid;
		private string _monitorname;
		private int _xmno;
		private string _xmname;
		private string _inspectionproof;
		private string _lastinspectionman;
        private bool _granted;

        public bool granted
        {
            get { return _granted; }
            set { _granted = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string unitname
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string senortype
		{
			set{ _senortype=value;}
			get{return _senortype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string senorno
		{
			set{ _senorno=value;}
			get{return _senorno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string senorname
		{
			set{ _senorname=value;}
			get{return _senorname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime producetiontime
		{
			set{ _producetiontime=value;}
			get{return _producetiontime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int effectionyears
		{
			set{ _effectionyears=value;}
			get{return _effectionyears;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool useing
		{
			set{ _useing=value;}
			get{return _useing;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string buyfrom
		{
			set{ _buyfrom=value;}
			get{return _buyfrom;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string state
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// auto_increment
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int inspectionalarmvalue
		{
			set{ _inspectionalarmvalue=value;}
			get{return _inspectionalarmvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool alarm
		{
			set{ _alarm=value;}
			get{return _alarm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string currentaddress
		{
			set{ _currentaddress=value;}
			get{return _currentaddress;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime lastinspectiontime
		{
			set{ _lastinspectiontime=value;}
			get{return _lastinspectiontime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int inspectiontimes
		{
			set{ _inspectiontimes=value;}
			get{return _inspectiontimes;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string managerID
		{
			set{ _managerid=value;}
			get{return _managerid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string monitorID
		{
			set{ _monitorid=value;}
			get{return _monitorid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string monitorname
		{
			set{ _monitorname=value;}
			get{return _monitorname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xmname
		{
			set{ _xmname=value;}
			get{return _xmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string inspectionproof
		{
			set{ _inspectionproof=value;}
			get{return _inspectionproof;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string lastinspectionman
		{
			set{ _lastinspectionman=value;}
			get{return _lastinspectionman;}
		}
		#endregion Model



    }
}

