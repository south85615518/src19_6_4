﻿/**  版本信息模板在安装目录下，可自行修改。
* deviceuseapply.cs
*
* 功 能： N/A
* 类 名： deviceuseapply
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/21 19:19:15   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace device.Model
{
	/// <summary>
	/// deviceuseapply:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class deviceuseapply
	{
		public deviceuseapply()
		{}
		#region Model
		private int _id=0;
        private string _unitname;

        public string unitname
        {
            get { return _unitname; }
            set { _unitname = value; }
        }
		private int _deviceid;
		private string _monitorid;
		private int _proessstate;
		private DateTime _applytime;
		private DateTime _processtime;
		private int _xmno;
		private string _remark;
		private string _xmname;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int deviceid
		{
			set{ _deviceid=value;}
			get{return _deviceid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string monitorID
		{
			set{ _monitorid=value;}
			get{return _monitorid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProessState
		{
			set{ _proessstate=value;}
			get{return _proessstate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime applytime
		{
			set{ _applytime=value;}
			get{return _applytime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime processtime
		{
			set{ _processtime=value;}
			get{return _processtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xmname
		{
			set{ _xmname=value;}
			get{return _xmname;}
		}
		#endregion Model

	}
}

