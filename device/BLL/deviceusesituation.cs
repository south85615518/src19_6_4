﻿/**  版本信息模板在安装目录下，可自行修改。
* deviceusesituation.cs
*
* 功 能： N/A
* 类 名： deviceusesituation
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/23 11:08:37   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace device.BLL
{
	/// <summary>
	/// deviceusesituation
	/// </summary>
	public partial class deviceusesituation
	{
        private readonly device.DAL.deviceusesituation dal = new device.DAL.deviceusesituation();
		public deviceusesituation()
		{}
		#region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(device.Model.deviceusesituation model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加设备号{0}的使用记录成功",model.deviceno);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加设备号为{0}的使用记录失败", model.deviceno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加设备号为{0}的设备出错,错误信息:" + ex.Message, model.deviceno);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(device.Model.deviceusesituation model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("添加编号{0}的使用记录成功", model.id);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加编号为{0}的使用记录失败", model.id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加编号为{0}的设备出错,错误信息:" + ex.Message, model.id);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string unitname, int id, out string mssg)
        {
            try
            {
                if (dal.Delete(unitname, id))
                {
                    mssg = string.Format("删除编号为{0}的设备使用记录成功", id);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除编号为{0}的设备使用记录失败", id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除编号为{0}的设备使用记录出错,错误信息:" + ex.Message, id);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string unitname, int id, out device.Model.deviceusesituation model, out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(unitname, id, out model))
                {
                    mssg = string.Format("成功获取编号{0}的使用记录", id);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取编号{0}的使用记录失败", id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取编号{0}的使用记录出错,错误信息:" + ex.Message, id);
                return false;
            }
        }

      
        //获取设备使用情况表
        public bool DeviceUseSituationTableLoad(string unitname, int startPageIndex, int pageSize, List<string> searchList, string sord, out DataTable dt, out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.UnitdeviceusesituationTableLoad(unitname, startPageIndex, pageSize, searchList, sord, out  dt))
                {
                    mssg = string.Format("获取到的设备使用情况表记录数{0}条", dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到设备使用情况表的记录失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的设备使用情况表的记录出错,错误信息：" + ex.Message);
                return false;
            }

        }
        //获取设备使用情况表记录数
        public bool DeviceUseSituationTableCountLoad(string unitname, List<string> searchList, out string totalCont, out string mssg)
        {
            mssg = "";
            totalCont = null;
            try
            {
                if (dal.UnitdeviceusesituationTableCountLoad(unitname, searchList, out totalCont))
                {
                    mssg = string.Format("获取到的设备使用情况表记录数{0}条", totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取设备使用情况表记录失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的设备使用情况表记录数出错,错误信息:"+ex.Message, totalCont);
                return false;
            }
        }

		#endregion  ExtensionMethod
	}
}

