﻿/**  版本信息模板在安装目录下，可自行修改。
* deviceuseapply.cs
*
* 功 能： N/A
* 类 名： deviceuseapply
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/11/21 19:19:15   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
namespace device.BLL
{
	/// <summary>
	/// deviceuseapply
	/// </summary>
	public partial class deviceuseapply
	{
		private readonly device.DAL.deviceuseapply dal=new device.DAL.deviceuseapply();
		public deviceuseapply()
		{}
		#region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(device.Model.deviceuseapply model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加监测员{0}项目编号{1}设备编号{2}的申请成功", model.monitorID, model.xmno, model.deviceid);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加监测员{0}项目编号{1}设备编号{2}的申请失败", model.monitorID, model.xmno, model.deviceid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加监测员{0}项目编号{1}设备编号{2}的申请出错，错误信息："+ex.Message, model.monitorID, model.xmno, model.deviceid);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(device.Model.deviceuseapply model,out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新监测员{0}项目编号{1}设备编号{2}的申请成功", model.monitorID, model.xmno, model.deviceid);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新监测员{0}项目编号{1}设备编号{2}的申请失败", model.monitorID, model.xmno, model.deviceid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新监测员{0}项目编号{1}设备编号{2}的申请出错，错误信息：" + ex.Message, model.monitorID, model.xmno, model.deviceid);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string unitname, int id,out string mssg)
        {
            try
            {
                if (dal.Delete(unitname,id))
                {
                    mssg = string.Format("删除编号{0}的申请记录成功", id);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除编号{0}的申请记录失败", id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除编号{0}的申请记录出错，错误信息：" + ex.Message, id);
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        //public bool DeleteList(string idlist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from deviceuseapply ");
        //    strSql.Append(" where id in ("+idlist + ")  ");
        //    int rows=OdbcSQLHelper.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string unitname, int id,out device.Model.deviceuseapply model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(unitname, id,out model))
                {
                    mssg = string.Format("获取编号{0}的申请记录实体对象成功", id);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除编号{0}的申请记录失败", id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除编号{0}的申请记录出错，错误信息：" + ex.Message, id);
                return false;
            }
        }
        //获取设备表
        public bool UnitDeviceApplyTableLoad(string unitname, int startPageIndex, int pageSize, string sord, out DataTable dt,out string mssg)
        {

            mssg = "";
            dt = null;
            try
            {
                if (dal.UnitDeviceApplyTableLoad(unitname, startPageIndex, pageSize, sord, out  dt))
                {
                    mssg = string.Format("获取到的设备申请表中记录数{0}条", dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到设备表申请的记录失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的设备表申请的记录出错,错误信息：" + ex.Message);
                return false;
            }
        }

        public bool UnitDeviceApplyTableCountLoad(string unitname, out string totalCont,out string mssg)
        {
            mssg = "";
            totalCont = null;
            try
            {
                if (dal.UnitDeviceApplyTableCountLoad(unitname,  out totalCont))
                {
                    mssg = string.Format("获取到的设备表申请表的记录数为{0}", totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到的设备申请表的记录数失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的设备申请表的记录数出错,错误信息：" + ex.Message);
                return false;
            }
          
        }

        //获取设备表
        public bool MonitorDeviceApplyTableLoad(string unitname, string monitorID, int startPageIndex, int pageSize, string sord, out DataTable dt,out string mssg)
        {

            mssg = "";
            dt = null;
            try
            {
                if (dal.MonitorDeviceApplyTableLoad(unitname, monitorID,startPageIndex, pageSize, sord, out  dt))
                {
                    mssg = string.Format("获取到监测员{0}的设备申请表中记录数{1}条",monitorID ,dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到监测员{0}设备表申请的记录失败",monitorID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的监测员{0}设备表申请的记录出错,错误信息：" + ex.Message,monitorID);
                return false;
            }
        }

        public bool MonitorDeviceApplyTableCountLoad(string unitname, string monitorID, out string totalCont,out string mssg)
        {
            mssg = "";
            totalCont = null;
            try
            {
                if (dal.MonitorDeviceApplyTableCountLoad(unitname, monitorID, out totalCont))
                {
                    mssg = string.Format("获取到监测员{0}的设备申请表中记录数{1}条", monitorID,totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到监测员{0}设备表申请的记录失败", monitorID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的监测员{0}设备表申请的记录出错,错误信息：" + ex.Message, monitorID);
                return false;
            }
        }

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

