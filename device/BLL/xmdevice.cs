﻿/**  版本信息模板在安装目录下，可自行修改。
* xmdevice.cs
*
* 功 能： N/A
* 类 名： xmdevice
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/22 14:28:18   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace device.BLL
{
	/// <summary>
	/// xmdevice
	/// </summary>
	public partial class xmdevice
	{
		private readonly device.DAL.xmdevice dal=new device.DAL.xmdevice();
		public xmdevice()
		{}
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(device.Model.xmdevice model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加设备号为{0}的设备成功",model.id);
                    return true;
                }
                else {
                    mssg = string.Format("添加设备号为{0}的设备失败", model.id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加设备号为{0}的设备出错,错误信息:"+ex.Message,model.id);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(device.Model.xmdevice model,out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新设备号为{0}的设备成功",model.id);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新设备号为{0}的设备失败", model.id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新设备号为{0}的设备出错，错误信息:"+ex.Message, model.id);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string unitname,int id,out string mssg)
        {
            try
            {
                if (dal.Delete(unitname,id))
                {
                    mssg = string.Format("成功第{0}号设备信息记录", id);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除第{0}号设备信息记录失败",id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除第{0}号设备出错,错误信息:"+ex.Message,id);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string unitname, int id, out device.Model.xmdevice model,out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(unitname,id,out model))
                {
                    mssg = string.Format("成功获取设备号{0}的实体对象",id);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取设备号{0}的实体对象失败",id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取设备号{0}的实体对象出错,错误信息:" + ex.Message,id);
                return false;
            }
        }

        //设置设备的当前是否可用的状态
        public bool SetState(string unitname ,int id, string state,out string mssg)
        {
            try
            {
                if (dal.SetState(unitname, id, state))
                {
                    mssg = string.Format("设置设备号{1}的可用状态为{2}成功", id, state);
                    return true;
                }
                else
                {
                    mssg = string.Format("设置设备号{1}的可用状态为{2}失败",  id, state);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("设置设备号{1}的可用状态为{2}出错,错误信息:"+ex.Message,  id, state);
                return false;
            }

        }

        //设置设备的当前是否在用的状态
        public bool SetUseState(string unitname, int id, bool isusing,out string mssg)
        {
            try
            {
                if (dal.SetUseState(unitname, id, isusing))
                {
                    mssg = string.Format("设置设备号{1}的在用状态为{2}成功",unitname, id, isusing?"在用":"已停用");
                    return true;
                }
                else
                {
                    mssg = string.Format("设置设备号{1}的在用状态为{2}失败", unitname, id, isusing?"在用":"已停用");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("设置设备号{1}的在用状态为{2}出错,错误信息:" + ex.Message, unitname, id, isusing ? "在用" : "已停用");
                return false;
            }
        }
        //获取设备表
        public bool XmDeviceTableLoad(string unitname,int startPageIndex, int pageSize,  List<string> searchList, string sord, out DataTable dt,out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.XmDeviceTableLoad(unitname,startPageIndex, pageSize,  searchList, sord, out  dt))
                {
                    mssg = string.Format("获取到的设备表中记录数{0}条",  dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到设备表的记录失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的设备表的记录出错,错误信息："+ex.Message);
                return false;
            }

        }

        //获取设备表
        public bool XmDeviceModellistLoad(int xmno, out  List<device.Model.xmdevice> xmdevicelist, out string mssg)
        {
            mssg = "";
            xmdevicelist = null;
            try
            {
                if (dal.XmDeviceModellistLoad(xmno, out xmdevicelist))
                {
                    mssg = string.Format("获取到的设备列表中记录数{0}条", xmdevicelist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到设备列表的记录失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的设备列表的记录出错,错误信息：" + ex.Message);
                return false;
            }

        }


        public bool XmDeviceTableCountLoad(string unitname, List<string> searchList, out string totalCont,out string mssg)
        {
            mssg = "";
            totalCont = null;
            try
            {
                if (dal.XmDeviceTableCountLoad( unitname, searchList, out totalCont))
                {
                    mssg = string.Format("获取到的设备表的记录数为{0}", totalCont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到的设备表的记录数失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到的设备表的记录数出错,错误信息：" + ex.Message);
                return false;
            }
        }

        //设备预警
        public bool XmDeviceAlarm(string unitname,out List<device.Model.xmdevice> modellist, out string mssg)
        {
            mssg = "";
            modellist = null;
            try
            {
                if (dal.XmDeviceAlarm(unitname, out modellist))
                {
                    mssg = string.Format("经巡查发现有{0}台设备需要质检", modellist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("经巡查发现目前还没有设备到质检预警时限");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("巡查设备的质检预警情况出错,错误信息:"+ex.Message);
                return false;
            }

        }
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

