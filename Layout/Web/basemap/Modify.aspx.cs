﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Maticsoft.Common;
using LTP.Accounts.Bus;
namespace Maticsoft.Web.basemap
{
    public partial class Modify : Page
    {       

        		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					int id=(Convert.ToInt32(Request.Params["id"]));
					ShowInfo(id);
				}
			}
		}
			
	private void ShowInfo(int id)
	{
		Maticsoft.BLL.basemap bll=new Maticsoft.BLL.basemap();
		Maticsoft.Model.basemap model=bll.GetModel(id);
		this.lblid.Text=model.id.ToString();
		this.txtfolderUrl.Text=model.folderUrl;
		this.txtremark.Text=model.remark;
		this.txtname.Text=model.name;

	}

		public void btnSave_Click(object sender, EventArgs e)
		{
			
			string strErr="";
			if(this.txtfolderUrl.Text.Trim().Length==0)
			{
				strErr+="folderUrl不能为空！\\n";	
			}
			if(this.txtremark.Text.Trim().Length==0)
			{
				strErr+="remark不能为空！\\n";	
			}
			if(this.txtname.Text.Trim().Length==0)
			{
				strErr+="name不能为空！\\n";	
			}

			if(strErr!="")
			{
				MessageBox.Show(this,strErr);
				return;
			}
			int id=int.Parse(this.lblid.Text);
			string folderUrl=this.txtfolderUrl.Text;
			string remark=this.txtremark.Text;
			string name=this.txtname.Text;


			Maticsoft.Model.basemap model=new Maticsoft.Model.basemap();
			model.id=id;
			model.folderUrl=folderUrl;
			model.remark=remark;
			model.name=name;

			Maticsoft.BLL.basemap bll=new Maticsoft.BLL.basemap();
			bll.Update(model);
			Maticsoft.Common.MessageBox.ShowAndRedirect(this,"保存成功！","list.aspx");

		}


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
