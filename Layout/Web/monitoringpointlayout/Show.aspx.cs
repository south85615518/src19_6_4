﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
namespace Maticsoft.Web.monitoringpointlayout
{
    public partial class Show : Page
    {        
        		public string strid=""; 
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					strid = Request.Params["id"];
					int nu=(Convert.ToInt32(strid));
					ShowInfo(nu);
				}
			}
		}
		
	private void ShowInfo(int nu)
	{
		Maticsoft.BLL.monitoringpointlayout bll=new Maticsoft.BLL.monitoringpointlayout();
		Maticsoft.Model.monitoringpointlayout model=bll.GetModel(nu);
		this.lblnu.Text=model.nu.ToString();
		this.lblid.Text=model.id.ToString();
		this.lblpointName.Text=model.pointName;
		this.lbljclx.Text=model.jclx;
		this.lbljcoption.Text=model.jcoption;
		this.lblAbsX.Text=model.AbsX.ToString();
		this.lblAbsY.Text=model.AbsY.ToString();
		this.lblxmno.Text=model.xmno.ToString();

	}


    }
}
