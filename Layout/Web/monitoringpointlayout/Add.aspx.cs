﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Maticsoft.Common;
using LTP.Accounts.Bus;
namespace Maticsoft.Web.monitoringpointlayout
{
    public partial class Add : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                       
        }

        		protected void btnSave_Click(object sender, EventArgs e)
		{
			
			string strErr="";
			if(!PageValidate.IsNumber(txtid.Text))
			{
				strErr+="id格式错误！\\n";	
			}
			if(this.txtpointName.Text.Trim().Length==0)
			{
				strErr+="pointName不能为空！\\n";	
			}
			if(this.txtjclx.Text.Trim().Length==0)
			{
				strErr+="jclx不能为空！\\n";	
			}
			if(this.txtjcoption.Text.Trim().Length==0)
			{
				strErr+="jcoption不能为空！\\n";	
			}
			if(!PageValidate.IsNumber(txtAbsX.Text))
			{
				strErr+="AbsX格式错误！\\n";	
			}
			if(!PageValidate.IsNumber(txtAbsY.Text))
			{
				strErr+="AbsY格式错误！\\n";	
			}
			if(!PageValidate.IsNumber(txtxmno.Text))
			{
				strErr+="xmno格式错误！\\n";	
			}

			if(strErr!="")
			{
				MessageBox.Show(this,strErr);
				return;
			}
			int id=int.Parse(this.txtid.Text);
			string pointName=this.txtpointName.Text;
			string jclx=this.txtjclx.Text;
			string jcoption=this.txtjcoption.Text;
			int AbsX=int.Parse(this.txtAbsX.Text);
			int AbsY=int.Parse(this.txtAbsY.Text);
			int xmno=int.Parse(this.txtxmno.Text);

			Maticsoft.Model.monitoringpointlayout model=new Maticsoft.Model.monitoringpointlayout();
			model.id=id;
			model.pointName=pointName;
			model.jclx=jclx;
			model.jcoption=jcoption;
			model.AbsX=AbsX;
			model.AbsY=AbsY;
			model.xmno=xmno;

			Maticsoft.BLL.monitoringpointlayout bll=new Maticsoft.BLL.monitoringpointlayout();
			bll.Add(model);
			Maticsoft.Common.MessageBox.ShowAndRedirect(this,"保存成功！","add.aspx");

		}


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
