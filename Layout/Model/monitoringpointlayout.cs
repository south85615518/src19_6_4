﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoringpointlayout.cs
*
* 功 能： N/A
* 类 名： monitoringpointlayout
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 9:39:59   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Layout.Model
{
	/// <summary>
	/// monitoringpointlayout:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class monitoringpointlayout
	{
		public monitoringpointlayout()
		{}
		#region Model
		private int _nu;
		private int? _id;
		private string _pointname;
		private string _jclx;
		private string _jcoption;
		private int? _absx;
		private int? _absy;
		private int _xmno;
        private int _color;
        private int _cgcolor;
        private string _xmname;

        public string xmname
        {
            get { return _xmname; }
            set { _xmname = value; }
        }

        public int cgcolor
        {
            get { return _cgcolor; }
            set { _cgcolor = value; }
        }
        public int color
        {
            get { return _color; }
            set { _color = value; }
        }
		/// <summary>
		/// auto_increment
		/// </summary>
		public int nu
		{
			set{ _nu=value;}
			get{return _nu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pointName
		{
			set{ _pointname=value;}
			get{return _pointname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jclx
		{
			set{ _jclx=value;}
			get{return _jclx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jcoption
		{
			set{ _jcoption=value;}
			get{return _jcoption;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AbsX
		{
			set{ _absx=value;}
			get{return _absx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AbsY
		{
			set{ _absy=value;}
			get{return _absy;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

