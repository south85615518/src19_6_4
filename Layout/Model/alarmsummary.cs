﻿/**  版本信息模板在安装目录下，可自行修改。
* tmp.cs
*
* 功 能： N/A
* 类 名： tmp
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/9/13 9:27:33   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Layout.Model
{
	/// <summary>
	/// tmp:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class alarmsummary
	{
        public alarmsummary()
		{}
		#region Model
		private int _id;
        private int _forderUrl;
        private string _name;
		private string _point_name;
		private string _type;
		private int _alarm;
		private DateTime _time;
		private DateTime _atime;
		private string _alarmcontext;
        private string _remark;

        
        private int _acont;
        public string remark
        {
            get { return _remark; }
            set { _remark = value; }
        }
        public int acont
        {
            get { return _acont; }
            set { _acont = value; }
        }
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int forderUrl
        {
            get { return _forderUrl; }
            set { _forderUrl = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int alarm
		{
			set{ _alarm=value;}
			get{return _alarm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime atime
		{
			set{ _atime=value;}
			get{return _atime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string alarmContext
		{
			set{ _alarmcontext=value;}
			get{return _alarmcontext;}
		}
		#endregion Model

	}
}

