﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Layout.Model
{
    public class basemapIdx
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 原图路径
        /// </summary>
        public string src { get; set; }
        /// <summary>
        /// 平面图的路径
        /// </summary>
        public string sizesrc { get; set; }
        public basemapIdx(int xmno,string src,string sizesrc)
        {
            this.xmno = xmno;
            this.src = src;
            this.sizesrc = sizesrc;
        }
        public basemapIdx()
        {
           
        }
    }
}
