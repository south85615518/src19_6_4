﻿/**  版本信息模板在安装目录下，可自行修改。
* basemap.cs
*
* 功 能： N/A
* 类 名： basemap
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 9:39:59   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Layout.Model
{
	/// <summary>
	/// basemap:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class basemap
	{
        public basemap()
        { }
        #region Model
        private int _id;
        private string _folderurl;
        private string _remark;
        private string _name;
        private int _xmno;
        private int _acont;

        public int acont
        {
            get { return _acont; }
            set { _acont = value; }
        }
        
        /// <summary>
        /// auto_increment
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string folderUrl
        {
            set { _folderurl = value; }
            get { return _folderurl; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string remark
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string name
        {
            set { _name = value; }
            get { return _name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int xmno
        {
            set { _xmno = value; }
            get { return _xmno; }
        }
        #endregion Model

	}
}

