﻿/**  版本信息模板在安装目录下，可自行修改。
* basemap.cs
*
* 功 能： N/A
* 类 名： basemap
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 9:39:59   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.Odbc;
using System.Collections.Generic;
using SqlHelpers;
using System;
namespace Layout.DAL
{
    /// <summary>
    /// 数据访问类:basemap
    /// </summary>
    public partial class basemap
    {
        #region  BasicMethod

        ///// <summary>
        ///// 得到最大ID
        ///// </summary>
        //public int GetMaxId()
        //{
        //return DbHelperOdbc.GetMaxID("id", "basemap"); 
        //}

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        //public bool Exists(int id)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select count(1) from basemap");
        //    strSql.Append(" where id=@id");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@id", OdbcType.Int)
        //    };
        //    parameters[0].Value = id;

        //    return false;//Odbc.Exists(strSql.ToString(),parameters);
        //}


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool CgAdd(Layout.Model.basemap model)
        {
            OdbcConnection conn = db.GetCgStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into basemap(");
            strSql.Append("folderUrl,remark,name,xmno)");
            strSql.Append(" values (");
            strSql.Append("@folderUrl,@remark,@name,@xmno)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@folderUrl", OdbcType.VarChar,500),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@name", OdbcType.VarChar,500),
                    new OdbcParameter("@xmno", OdbcType.Int)};
            parameters[0].Value = model.folderUrl;
            parameters[1].Value = model.remark;
            parameters[2].Value = model.name;
            parameters[3].Value = model.xmno;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool CgUpdate(Layout.Model.basemap model)
        {
            OdbcConnection conn = db.GetCgStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update basemap set ");
            strSql.Append("folderUrl=@folderUrl,");
            strSql.Append("remark=@remark,");
            strSql.Append("name=@name");
            strSql.Append(" where id=@id");
            OdbcParameter[] parameters = {
					new OdbcParameter("@folderUrl", OdbcType.VarChar,500),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@name", OdbcType.VarChar,500),
					new OdbcParameter("@id", OdbcType.Int,11)};
            parameters[0].Value = model.folderUrl;
            parameters[1].Value = model.remark;
            parameters[2].Value = model.name;
            parameters[3].Value = model.id;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool CgDelete(string path, string xmname)
        {
            OdbcConnection conn = db.GetCgStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete basemap,basemap_idx  from basemap,basemap_idx ");
            strSql.AppendFormat(" where basemap.folderUrl = basemap_idx.sizeurl and basemap.xmno = basemap_idx.xmno  and basemap_idx.srcurl = '{0}' ", path);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        ///// <summary>
        ///// 批量删除数据
        ///// </summary>
        //public bool DeleteList(string idlist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from basemap ");
        //    strSql.Append(" where id in ("+idlist + ")  ");
        //    int rows=DbHelperOdbc.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        public bool CgGetModel(int id, string xmname, out Layout.Model.basemap model)
        {
            model = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,folderUrl,remark,name,xmno from basemap ");
            strSql.Append(" where id=@id");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@id", OdbcType.Int)
            };
            parameters[0].Value = id;

            OdbcConnection conn = db.GetCgStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "basemap", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CgFolderUrlGet(string xmname, string path, out string folderUrl)
        {
            OdbcConnection conn = db.GetCgStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            folderUrl = "";
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select folderUrl from basemap ");
            strSql.AppendFormat(" where folderUrl   like   '{0}' ", string.Format("{0}_%", path));
            Object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            if (obj != null)
            {
                folderUrl = Convert.ToString(obj);
                return true;
            }

            return false;

        }

        public bool CgBaseMapGet(int xmno, string jclx, string pointname, out Layout.Model.basemap basemapmodel)
        {
            basemapmodel = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select b.*  from monitoringpointlayout a,basemap  b where  a.id = b.id and  a.xmno=@xmname  and   a.jclx = @jclx     and   a.pointname=@pointname  ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@jclx", OdbcType.VarChar,50),
                    new OdbcParameter("@pointname", OdbcType.VarChar,50)
            };
            parameters[0].Value = xmno;
            parameters[1].Value = jclx;
            parameters[2].Value = pointname;
            OdbcConnection conn = db.GetCgStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "basemap", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                basemapmodel = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }

        }

        ///// <summary>
        ///// 得到所有底图对象实体
        ///// </summary>
        public bool CgGetModelList(string xmname, int id, out List<Layout.Model.basemap> lmb)
        {

            lmb = new List<Model.basemap>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,folderUrl,remark,name ,xmno from basemap ");
            strSql.Append(" where  xmno=@xmno  ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@id", OdbcType.Int)
            };
            parameters[0].Value = id;
            OdbcConnection conn = db.GetCgStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            Layout.Model.basemap model = new Layout.Model.basemap();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "basemap", parameters);
            int i = 0;
            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //return DataRowToModel(ds.Tables[0].Rows[0]);
                lmb.Add(DataRowToModel(ds.Tables[0].Rows[i]));
            }
            return true;
        }

       

        public bool CgBaseMapMaxIdGet( int xmno ,out int maxId)
        {
            maxId = -1;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(id) from basemap where xmno = @xmno ");
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmno);
            OdbcParameter[] parameters = { 
                 new OdbcParameter("@xmno", OdbcType.Int)
            };
            parameters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
            if (obj == null) return false;
            maxId = Convert.ToInt32(obj); return true;
        }





        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select id,folderUrl,remark,name ");
        //    strSql.Append(" FROM basemap ");
        //    if (strWhere.Trim() != "")
        //    {
        //        strSql.Append(" where " + strWhere);
        //    }
        //    return DbHelperOdbc.Query(strSql.ToString());
        //}

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM basemap ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.id desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from basemap T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return DbHelperOdbc.Query(strSql.ToString());
        //}

        ///*
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@tblName", OdbcType.VarChar, 255),
        //            new OdbcParameter("@fldName", OdbcType.VarChar, 255),
        //            new OdbcParameter("@PageSize", OdbcType.Int),
        //            new OdbcParameter("@PageIndex", OdbcType.Int),
        //            new OdbcParameter("@IsReCount", OdbcType.Bit),
        //            new OdbcParameter("@OrderType", OdbcType.Bit),
        //            new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
        //            };
        //    parameters[0].Value = "basemap";
        //    parameters[1].Value = "id";
        //    parameters[2].Value = PageSize;
        //    parameters[3].Value = PageIndex;
        //    parameters[4].Value = 0;
        //    parameters[5].Value = 0;
        //    parameters[6].Value = strWhere;	
        //    return DbHelperOdbc.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        //}*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

