﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;
namespace Layout.DAL
{
   public class basemapIdx
    {
       public static database db = new database();
       public bool Add(Layout.Model.basemapIdx model)
       {
           OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
           StringBuilder strSql = new StringBuilder(256);
           strSql.Append("insert into basemap_Idx (srcUrl,sizeUrl,xmno) values (@srcUrl,@sizeUrl,@xmno)");
           OdbcParameter[] parameters = {
             new OdbcParameter("@srcUrl",OdbcType.VarChar,500 ),
             new OdbcParameter("@sizeUrl",OdbcType.VarChar,500 ),
             new OdbcParameter("@xmno",OdbcType.Int)
                                        };
           parameters[0].Value = model.src;
           parameters[1].Value = model.sizesrc;
           parameters[2].Value = model.xmno;
           int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(),parameters);
           if (rows > 0)
           {
               return true;
           }
           else
           {
               return false;
           }
               
       }



    }
}
