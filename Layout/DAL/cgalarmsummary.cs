﻿/**  版本信息模板在安装目录下，可自行修改。
* tmp.cs
*
* 功 能： N/A
* 类 名： tmp
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/9/13 9:27:33   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Data.Odbc;
using SqlHelpers;
namespace Layout.DAL
{
	/// <summary>
	/// 数据访问类:tmp
	/// </summary>
	public partial class cgalarmsummary
	{
        public cgalarmsummary()
		{}
        public database db = new database();
		#region  BasicMethod


        /// <summary>
        /// 加载项目监测平面图的所有预警点
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="llm"></param>
        /// <returns></returns>
        public bool GetModelList(int xmno, out  List<Layout.Model.alarmsummary> llm)
        {
            OdbcConnection cgconn = db.GetStanderCgConn(xmno);
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = cgconn;
            llm = new List<Model.alarmsummary>();
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"SELECT d.*,c.id,b.point_name,b.type,b.alarm,b.time,b.atime,a.alarmContext FROM  {0}.basemap d  left join  {0}.monitoringpointlayout c  on d.id = c.id  left join pointalarm b on c.xmno = b.xmno and c.jclx= b.type and c.pointName = b.point_name left join cgalarmsplitondate a on  b.xmno = a.xmno and b.type = a.jclx and b.point_name = a.pointName and  b.time = a.time 
where d.xmno = @xmno",conn.Database);
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int)
            };
            parameters[0].Value = xmno;
            Layout.Model.monitoringpointlayout model = new Layout.Model.monitoringpointlayout();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //return DataRowToModel(ds.Tables[0].Rows[0]);
                llm.Add(DataRowToModel(ds.Tables[0].Rows[i]));
            }
            return true;

        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Layout.Model.alarmsummary DataRowToModel(DataRow row)
		{
            Layout.Model.alarmsummary model = new Layout.Model.alarmsummary();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
                if (row["name"] != null && row["name"].ToString() != "")
                {
                    model.name = row["name"].ToString();
                }
                if (row["folderUrl"] != null && row["folderUrl"].ToString() != "")
                {
                    model.name = row["folderUrl"].ToString();
                }
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["type"]!=null)
				{
					model.type=row["type"].ToString();
				}
				if(row["alarm"]!=null && row["alarm"].ToString()!="")
				{
					model.alarm=int.Parse(row["alarm"].ToString());
				}
				if(row["time"]!=null && row["time"].ToString()!="")
				{
					model.time=DateTime.Parse(row["time"].ToString());
				}
				if(row["atime"]!=null && row["atime"].ToString()!="")
				{
					model.atime=DateTime.Parse(row["atime"].ToString());
				}
                if (row["alarmContext"] != null && row["alarmContext"].ToString() != "")
				{
					model.alarmContext=row["alarmContext"].ToString();
				}
              
			}
			return model;
		}

		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

