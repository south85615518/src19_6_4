﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoringpointlayout.cs
*
* 功 能： N/A
* 类 名： monitoringpointlayout
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 9:39:59   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using System.Collections.Generic;
using SqlHelpers;




namespace Layout.DAL
{
    /// <summary>
    /// 数据访问类:monitoringpointlayout
    /// </summary>
    public partial class monitoringpointlayout
    {
        public alarmsummary summarydal = new alarmsummary();
        public monitoringpointlayout()
        { }
        #region  BasicMethod

        ///// <summary>
        ///// 得到最大ID
        ///// </summary>
        //public int GetMaxId()
        //{
        //return DbHelperOdbc.GetMaxID("nu", "monitoringpointlayout"); 
        //}
        public database db = new database();
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string pointName, string jclx, int xmno)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from monitoringpointlayout");
            strSql.Append(" where pointName=@pointName  and jclx = @jclx  and xmno = @xmno ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@pointName", OdbcType.VarChar,200),
                    new OdbcParameter("@jclx", OdbcType.VarChar,200),
                    new OdbcParameter("@xmno", OdbcType.Int)
            };
            parameters[0].Value = pointName;
            parameters[1].Value = jclx;
            parameters[2].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (Convert.ToInt32(obj) > 0) return true;
            return false;
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Layout.Model.monitoringpointlayout model)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into monitoringpointlayout(");
            strSql.Append("id,pointName,jclx,jcoption,AbsX,AbsY,xmno,xmname)");
            strSql.Append(" values (");
            strSql.Append("@id,@pointName,@jclx,@jcoption,@AbsX,@AbsY,@xmno,@xmname)");


            OdbcParameter[] parameters = {
					new OdbcParameter("@id", OdbcType.Int,11),
					new OdbcParameter("@pointName", OdbcType.VarChar,100),
					new OdbcParameter("@jclx", OdbcType.VarChar,500),
					new OdbcParameter("@jcoption", OdbcType.VarChar,500),
					new OdbcParameter("@AbsX", OdbcType.Int,11),
					new OdbcParameter("@AbsY", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@xmname", OdbcType.VarChar,100)

                                         };
            parameters[0].Value = model.id;
            parameters[1].Value = model.pointName;
            parameters[2].Value = model.jclx;
            parameters[3].Value = model.jcoption == null ? "" : model.jcoption;
            parameters[4].Value = model.AbsX;
            parameters[5].Value = model.AbsY;
            parameters[6].Value = model.xmno;
            parameters[7].Value = model.xmname;


            int rows = 0;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Layout.Model.monitoringpointlayout model)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update monitoringpointlayout set ");
            strSql.Append("AbsX=@AbsX,");
            strSql.Append("AbsY=@AbsY  ");
            strSql.Append("  where xmno=@xmno  and  pointName=@pointName   and  jclx = @jclx  and  id = @id   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@AbsX", OdbcType.Int,11),
					new OdbcParameter("@AbsY", OdbcType.Int,11),
                    new OdbcParameter("@xmno", OdbcType.Int,11),
                    new OdbcParameter("@pointName", OdbcType.VarChar,100),
                    new OdbcParameter("@jclx", OdbcType.VarChar,500),
					new OdbcParameter("@id", OdbcType.Int,11)
					};
            parameters[0].Value = model.AbsX;
            parameters[1].Value = model.AbsY;
            parameters[2].Value = model.xmno;
            parameters[3].Value = model.pointName;
            parameters[4].Value = model.jclx;
            parameters[5].Value = model.id;
            int rows = 0;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
       
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string pointName, string jclx, int xmno)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from monitoringpointlayout ");
            strSql.Append(" where pointName = @pointName  and jclx = @jclx  and xmno = @xmno  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@pointName", OdbcType.VarChar,500),
                    new OdbcParameter("@jclx", OdbcType.VarChar,50),
                    new OdbcParameter("@xmno", OdbcType.Int)
			};
            parameters[0].Value = pointName;
            parameters[1].Value = jclx;
            parameters[2].Value = xmno;

            int rows = 0;
            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Layout.Model.monitoringpointlayout GetModel(int nu, string xmname)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select nu,id,pointName,jclx,jcoption,AbsX,AbsY,xmno,color from monitoringpointlayout ");
            strSql.Append(" where nu=@nu");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@nu", OdbcType.Int)
            };
            parameters[0].Value = nu;

            Layout.Model.monitoringpointlayout model = new Layout.Model.monitoringpointlayout();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "monitoringpointlayout", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }
       
        /// <summary>
        /// 得到一张监测平面地图上的对象实体
        /// </summary>
        public bool GetModelList(int xmno, string jclx, out  List<Layout.Model.monitoringpointlayout> llm)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            llm = new List<Model.monitoringpointlayout>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select nu,id,pointName,jclx,jcoption,AbsX,AbsY,xmno,color from monitoringpointlayout ");
            strSql.Append(" where xmno=@xmno  and  jclx = @jclx");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@jclx", OdbcType.VarChar,200)
            };
            parameters[0].Value = xmno;
            parameters[1].Value = jclx;
            Layout.Model.monitoringpointlayout model = new Layout.Model.monitoringpointlayout();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "monitoringpointlayout", parameters);
            int i = 0;
            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //return DataRowToModel(ds.Tables[0].Rows[0]);
                llm.Add(DataRowToModel(ds.Tables[0].Rows[i]));
            }
            return true;

        }

        /// <summary>
        /// 得到一张监测平面地图上的对象实体
        /// </summary>
        public bool GetModelList(int xmno, string jclx, int basemapid, out  List<Layout.Model.monitoringpointlayout> llm)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            llm = new List<Model.monitoringpointlayout>();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select nu,id,pointName,jclx,jcoption,AbsX,AbsY,xmno,color from monitoringpointlayout ");
            strSql.Append(" where xmno = @xmno  and  jclx = @jclx  and  id = @id  ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@jclx", OdbcType.VarChar,200),
                    new OdbcParameter("@basemapid", OdbcType.Int)
            };
            parameters[0].Value = xmno;
            parameters[1].Value = jclx;
            parameters[2].Value = basemapid;
            Layout.Model.monitoringpointlayout model = new Layout.Model.monitoringpointlayout();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "monitoringpointlayout", parameters);
            int i = 0;
            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //return DataRowToModel(ds.Tables[0].Rows[0]);
                llm.Add(DataRowToModel(ds.Tables[0].Rows[i]));
            }
            return true;

        }

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        public Layout.Model.monitoringpointlayout DataRowToModel(DataRow row)
        {
            Layout.Model.monitoringpointlayout model = new Layout.Model.monitoringpointlayout();
            if (row != null)
            {
                if (row["nu"] != null && row["nu"].ToString() != "")
                {
                    model.nu = int.Parse(row["nu"].ToString());
                }
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.id = int.Parse(row["id"].ToString());
                }
                if (row["pointName"] != null)
                {
                    model.pointName = row["pointName"].ToString();
                }
                if (row["jclx"] != null)
                {
                    model.jclx = row["jclx"].ToString();
                }
                if (row["jcoption"] != null)
                {
                    model.jcoption = row["jcoption"].ToString();
                }
                if (row["AbsX"] != null && row["AbsX"].ToString() != "")
                {
                    model.AbsX = int.Parse(row["AbsX"].ToString());
                }
                if (row["AbsY"] != null && row["AbsY"].ToString() != "")
                {
                    model.AbsY = int.Parse(row["AbsY"].ToString());
                }
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["color"] != null && row["color"].ToString() != "")
                {
                    model.color = int.Parse(row["color"].ToString());
                }


            }
            return model;
        }

        /// <summary>
        /// 更新自检热点用颜色表示的预警状态
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="jclx"></param>
        /// <param name="jcoption"></param>
        /// <param name="pointname"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public bool UpdateHotPotColor(int xmno, string jclx, string jcoption, string pointname, int color)
        {

            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update monitoringpointlayout  ");
            strSql.Append("  set color = @color  ");
            strSql.Append(" where  ");
            strSql.Append("    xmno = @xmno    and    jclx = @jclx     and    jcoption = @jcoption    and    pointname  =  @pointname    ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@color", OdbcType.Int),
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@jclx", OdbcType.VarChar,200),
                    new OdbcParameter("@jcoption", OdbcType.VarChar,200),
                    new OdbcParameter("@pointname", OdbcType.VarChar,100)
            };
            parameters[0].Value = color;
            parameters[1].Value = xmno;
            parameters[2].Value = jclx;
            parameters[3].Value = jcoption;
            parameters[4].Value = pointname;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool BasemapAlarmPointCount(int xmno, int basemapid, out int cont)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("SELECT count(1) FROM monitoringpointlayout where   color>0 and     xmno=@xmno    and    monitoringpointlayout.id = @basemapid");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno",OdbcType.Int,10),
			        new OdbcParameter("@basemapid",OdbcType.Int,10)
                                         };
            parameters[0].Value = xmno;
            parameters[1].Value = basemapid;
            cont = -1;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null)
            {

                cont = Convert.ToInt32(obj.ToString());
                return true;
            }
            return false;
        }
        public bool BasemapJclxList(int xmno, int basemapid, out List<string> jclxList)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(xmno);
            strSql.Append("SELECT distinct(jclx)  FROM  monitoringpointlayout where   id='" + basemapid + "' and     xmno='"+xmno+"'");
            jclxList = querysql.querystanderlist(strSql.ToString(),conn);
            if (jclxList!=null)
            return true;
            return false;
        }

       



        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

