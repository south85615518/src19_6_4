﻿/**  版本信息模板在安装目录下，可自行修改。
* alarmsummary.cs
*
* 功 能： N/A
* 类 名： alarmsummary
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/9/13 9:27:33   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;

namespace Layout.BLL
{
	/// <summary>
	/// alarmsummary
	/// </summary>
	public partial class alarmsummary
	{
		private readonly Layout.DAL.alarmsummary dal=new Layout.DAL.alarmsummary();
		public alarmsummary()
		{}
		#region  BasicMethod

		
		/// <summary>
		/// 获得数据列表
		/// </summary>
        public bool GetModelList(int xmno, out  List<Layout.Model.alarmsummary> llm,out string mssg)
		{
            llm = null;
            try
            {
                if (dal.GetModelList(xmno, out llm))
                {
                    mssg = string.Format("获取项目编号{0}的监测平面图预警汇总成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的监测平面图预警汇总失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的监测平面图预警汇总出错，错误信息:"+ex.Message, xmno);
                return false;
            }
		}
		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

