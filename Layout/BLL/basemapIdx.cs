﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Layout.BLL
{
    public class basemapIdx
    {
        private readonly Layout.DAL.basemapIdx dal = new Layout.DAL.basemapIdx();
        public bool Add(Layout.Model.basemapIdx model,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("创建项目编号{0}的原图路径{1}--平面图路径{2}成功", model.xmno, model.src, model.sizesrc);
                    return true;
                }
                else
                {
                    mssg = string.Format("创建项目编号{0}的原图路径{1}--平面图路径{2}失败", model.xmno, model.src, model.sizesrc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("创建项目编号{0}的原图路径{1}--平面图路径{2}出错，错误信息："+ex.Message, model.xmno, model.src, model.sizesrc);
                return false;
            }

        }
    }
}
