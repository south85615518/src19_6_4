﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoringpointlayout.cs
*
* 功 能： N/A
* 类 名： monitoringpointlayout
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 9:39:59   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using Layout.Common;
using Layout.Model;
namespace Layout.BLL
{
	/// <summary>
	/// monitoringpointlayout
	/// </summary>
	public partial class monitoringpointlayout
	{
	
		#region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool CgExists(string pointName,string jclx,int xmno,out string mssg )
        {
            try
            {
                if (dal.CgExists(pointName, jclx, xmno))
                {
                    mssg = "成果监测平面图上已经存在"+pointName;
                    return true;
                }
                else
                {
                    mssg = "成果监测平面图上不存在" + pointName;
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = "在成果监测平面图上查询点" + pointName+"存在出错,错误信息:"+ex.Message;
                    return false;
            }
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool CgAdd(Layout.Model.monitoringpointlayout model,out string mssg)
		{
            try
            {
                if (dal.CgAdd(model))
                {
                    mssg = "成果监测平面图热点添加成功";
                    return true;
                }
                else
                {
                    mssg = "成果监测平面图热点添加失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "成果监测平面图热点添加出错,错误信息："+ex.Message;
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool CgUpdate(Layout.Model.monitoringpointlayout model, out string mssg)
		{
            try
            {
                if (dal.CgUpdate(model))
                {
                    mssg = "成果监测平面图热点更新成功";
                    return true;
                }
                else
                {
                    mssg = "成果监测平面图热点更新失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "成果监测平面图热点更新出错,错误信息：" + ex.Message;
                return false;
            }
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool CgDelete(string pointName,string jclx,int xmno,out string mssg)
		{
            try
            {
                if (dal.CgDelete(pointName,jclx,xmno))
                {
                    mssg = "成果监测平面图热点删除成功";
                    return true;
                }
                else
                {
                    mssg = "成果监测平面图热点删除失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "成果监测平面图热点成功出错,错误信息：" + ex.Message;
                return false;
            }
			 
		}
        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool DeleteList(string nulist )
        //{
        //    return dal.DeleteList(nulist );
        //}

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public Layout.Model.monitoringpointlayout GetModel(int nu)
        //{
			
        //    return dal.GetModel(nu);
        //}



        /// <summary>
        /// 得到一张监测平面地图上的对象实体
        /// </summary>
        public bool  CgGetModelList(int xmno, string jclx, out List<Layout.Model.monitoringpointlayout> llm,out string mssg)
        {
            llm = null;
            try
            {
                if (dal.CgGetModelList(xmno, jclx, out llm))
                {
                    mssg = "成果监测平面图热点加载成功";
                    return true;
                }
                else
                {
                    mssg = "成果监测平面图热点加载失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "成果监测平面图热点加载出错，错误信息："+ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 得到一张监测平面地图上的对象实体
        /// </summary>
        public bool CgGetModels(int xmno, string jclx, int basemapid, out  List<Layout.Model.monitoringpointlayout> llm,out string mssg)
        {
            llm = null;
            try
            {
                if (dal.CgGetModelList(xmno, jclx, basemapid, out llm))
                {
                    mssg = "获取成果监测平面图热点成功";
                    return true;
                }
                else
                {
                    mssg = "获取成果监测平面图热点失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取成果监测平面图热点出错,错误信息:"+ex.Message;
                return false;
            }

        }
      

        /// <summary>
        /// 更新自检热点用颜色表示的预警状态
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="jclx"></param>
        /// <param name="jcoption"></param>
        /// <param name="pointname"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public bool CgUpdateHotPotColor(int xmno, string jclx, string jcoption, string pointname, int color,out string mssg)
        {
            try
            {
                if (dal.CgUpdateHotPotColor(xmno, jclx, jcoption,pointname,color))
                {
                    mssg = string.Format("更新项目编号{0}类型{1}分项{2}成果监测平面图热点{3}的预警状态颜色为{4}成功",xmno,jclx,jcoption,pointname,color);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}类型{1}分项{2}成果监测平面图热点{3}的预警状态颜色为{4}失败", xmno, jclx, jcoption, pointname, color);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}类型{1}分项{2}成果监测平面图热点{3}的预警状态颜色为{4}错误，错误信息："+ex.Message, xmno, jclx, jcoption, pointname, color);
                return false;
            }
         
        }

        /// <summary>
        /// 自检预警点数
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="basemapid"></param>
        /// <param name="cont"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool CgBasemapAlarmPointCount(int xmno, int basemapid, out int cont,out string mssg)
        {
            mssg = "";
            cont = 0;
            try
            {
                if (dal.CgBasemapAlarmPointCount(xmno, basemapid, out cont))
                {
                    mssg = string.Format("获取项目编号{0}成果监测平面图编号{1}的预警点数为{2}成功", xmno, basemapid, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}成果监测平面图编号{1}的预警点数为{2}失败", xmno, basemapid, cont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}成果监测平面图编号{1}的预警点数出错,错误信息:"+ex.Message, xmno, basemapid);
                return false;
            }
        }

        public bool CgBasemapJclxList(int xmno, int basemapid, out List<string> jclxList,out string mssg)
        {
            jclxList = new List<string>();
            try
            {
                if (dal.CgBasemapJclxList(xmno, basemapid, out jclxList))
                {
                    mssg = string.Format("获取项目编号{0}平面图编号{1}的监测类型有{2}", xmno, basemapid, string.Join(",", jclxList));
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}平面图编号{1}的监测类型失败", xmno, basemapid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}平面图编号{1}的监测类型出错,错误信息:"+ex.Message, xmno, basemapid);
                return false;
            }
        }


      

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

