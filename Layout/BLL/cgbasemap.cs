﻿/**  版本信息模板在安装目录下，可自行修改。
* basemap.cs
*
* 功 能： N/A
* 类 名： basemap
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 9:39:59   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
//using Layout.Common;
using SqlHelpers;
namespace Layout.BLL
{
    /// <summary>
    /// basemap
    /// </summary>
    public partial class basemap
    {
       
        #region  BasicMethod

        ///// <summary>
        ///// 得到最大ID
        ///// </summary>
        //public int GetMaxId()
        //{
        //    return dal.GetMaxId();
        //}

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        //public bool CgExists(int id)
        //{
        //    return dal.Exists(id);
        //}

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool CgAdd(Layout.Model.basemap model, out string mssg)
        {
            try
            {
                if (dal.CgAdd(model))
                {
                    mssg = "添加成果监测平面图成功";
                    return true;
                }
                else
                {
                    mssg = "添加成果监测平面图失败";
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = "添加成果监测平面底图信息出错！错误信息：" + ex.Message;
                return false;
            }




        }

        public bool CgFolderUrlGet(string xmname, string path, out string folderUrl, out string mssg)
        {
            folderUrl = "";
            try
            {
                if (dal.CgFolderUrlGet(xmname, path, out folderUrl))
                {
                    mssg = string.Format("获取项目{0}成果监测平面图路径{1}的图片路径成功", xmname, path);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}成果监测平面图路径{1}的图片路径失败", xmname, path);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}成果监测平面图路径{1}的图片路径出错,错误信息:" + ex.Message, xmname, path);
                return false;
            }
        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool CgUpdate(Layout.Model.basemap model, string xmname)
        {
            return false;//dal.Update(model,xmname);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool CgDelete(string path, string xmname, out string mssg)
        {

            try
            {
                if (dal.CgDelete(path, xmname))
                {
                    mssg = string.Format("删除路径{0}的平面图成功", path);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除路径{0}的平面图失败", path);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除路径{0}的平面图出错,错误信息" + ex.Message, path);
                return false;
            }


        }
        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool DeleteList(string idlist )
        //{
        //    return dal.DeleteList(idlist );
        //}

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        public bool CgGetModel(string xmname, int id, out Layout.Model.basemap model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.CgGetModel(id, xmname, out model))
                {
                    mssg = "获取监测底图实体对象成功";
                    return true;
                }
                else
                {
                    mssg = "获取监测底图实体对象失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取监测底图实体对象出错，错误信息" + ex.Message;
                return false;
            }
        }

        public bool CgBaseMapGet(int xmno, string jclx, string pointname, out Layout.Model.basemap basemapmodel, out string mssg)
        {
            basemapmodel = null;
            try
            {
                if (dal.CgBaseMapGet(xmno, jclx, pointname, out basemapmodel))
                {
                    mssg = string.Format("获取项目编号{0}{1}{2}的成果监测平面图成功", xmno, jclx, pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}{1}{2}的成果监测平面图失败", xmno, jclx, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}{1}{2}的成果监测平面图出错，错误信息:" + ex.Message, xmno, jclx, pointname);

                return true;
            }

        }
        public bool CgGetModelList(string xmname, int id, out List<Layout.Model.basemap> lmb, out string mssg)
        {
            lmb = null;
            try
            {
                if (dal.CgGetModelList(xmname, id, out lmb))
                {
                    mssg = "成果监测平面底图列表加载成功";
                    return true;
                }
                else
                {
                    mssg = "成果监测平面底图列表加载失败";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "成果监测平面底图列表加载出错,错误信息：" + ex.Message;
                return false;
            }

        }
        public bool CgBaseMapMaxIdGet(int xmno, out int maxId,out string mssg)
        {
            maxId = -1;
            try
            {
                if (dal.CgBaseMapMaxIdGet(xmno, out maxId))
                {
                    mssg = string.Format("获取项目编号{0}成果监测平面图最大Id{1}成功", xmno, maxId);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}成果监测平面图Id失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}成果监测平面图出错，错误信息:"+ex.Message, xmno);
                return false;
            }
        }


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

