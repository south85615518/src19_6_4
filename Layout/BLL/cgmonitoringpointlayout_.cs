﻿/**  版本信息模板在安装目录下，可自行修改。
* monitoringpointlayout.cs
*
* 功 能： N/A
* 类 名： monitoringpointlayout
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 9:39:59   N/A    初版
*
* Copyright (c) 2012 Layout Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using Layout.Common;
using Layout.Model;
namespace Layout.BLL
{
	/// <summary>
	/// monitoringpointlayout
	/// </summary>
	public partial class cgmonitoringpointlayout
	{
		private readonly Layout.DAL.cgmonitoringpointlayout dal=new Layout.DAL.cgmonitoringpointlayout();
		public cgmonitoringpointlayout()
		{}
		#region  BasicMethod


        /// <summary>
        /// 得到一张监测平面地图上的对象实体
        /// </summary>
        public bool  GetModelList(int xmno, string jclx, out List<Layout.Model.monitoringpointlayout> llm,out string mssg)
        {
            llm = null;
            try
            {
                if (dal.GetModelList(xmno, jclx, out llm))
                {
                    mssg = "监测平面图热点加载成功";
                    return true;
                }
                else
                {
                    mssg = "监测平面图热点加载失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "监测平面图热点加载出错，错误信息："+ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 得到一张监测平面地图上的对象实体
        /// </summary>
        public bool GetModels(int xmno, string jclx, int basemapid, out  List<Layout.Model.monitoringpointlayout> llm,out string mssg)
        {
            llm = null;
            try
            {
                if (dal.GetModelList(xmno, jclx, basemapid, out llm))
                {
                    mssg = "获取平面图热点成功";
                    return true;
                }
                else
                {
                    mssg = "获取平面图热点失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取平面图热点出错,错误信息:"+ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 更新成果预警用颜色表示的预警状态
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="jclx"></param>
        /// <param name="jcoption"></param>
        /// <param name="pointname"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public bool UpdateCgHotPotColor(int xmno, string jclx, string jcoption, string pointname, int color, out string mssg)
        {
            try
            {
                if (dal.UpdateCgHotPotColor(xmno, jclx, jcoption, pointname, color))
                {
                    mssg = string.Format("更新项目编号{0}类型{1}分项{2}热点{3}的成果预警状态颜色为{4}成功", xmno, jclx, jcoption, pointname, color);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}类型{1}分项{2}热点{3}的成果预警状态失败", xmno, jclx, jcoption, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}类型{1}分项{2}热点{3}的成果预警状态出错，错误信息：" + ex.Message, xmno, jclx, jcoption, pointname);
                return false;
            }
            return false;
        }


        /// <summary>
        /// 成果预警点数
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="basemapid"></param>
        /// <param name="cont"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool BasemapCgAlarmPointCount(int xmno, int basemapid, out int cont, out string mssg)
        {
            mssg = "";
            cont = 0;
            try
            {
                if (dal.BasemapCgAlarmPointCount(xmno, basemapid, out cont))
                {
                    mssg = string.Format("获取项目编号{0}监测平面图编号{1}的成果预警点数为{2}成功", xmno, basemapid, cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}监测平面图编号{1}的成果预警点数为{2}失败", xmno, basemapid, cont);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}监测平面图编号{1}的成果预警点数出错,错误信息:" + ex.Message, xmno, basemapid);
                return false;
            }
        }

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

