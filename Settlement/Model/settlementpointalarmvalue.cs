﻿/**  版本信息模板在安装目录下，可自行修改。
* settlementpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： settlementpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/1 8:51:05   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Settlement.Model
{
	/// <summary>
	/// settlementpointalarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class settlementpointalarmvalue
	{
		public settlementpointalarmvalue()
		{}
		#region Model
		private string _point_name;
		private int _xmno;
		private string _firstalarmname;
		private string _secondalarmname;
		private string _thirdalarmname;
        private string _remark;
        private string _pointtype;
        private string _basepointname;
        public string pointtype
        {
            get { return _pointtype; }
            set { _pointtype = value; }
        }
        public string basepointname
        {
            get { return _basepointname; }
            set { _basepointname = value; }
        }
        public string remark
        {
            get { return _remark; }
            set { _remark = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string firstAlarmName
		{
			set{ _firstalarmname=value;}
			get{return _firstalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string secondAlarmName
		{
			set{ _secondalarmname=value;}
			get{return _secondalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string thirdAlarmName
		{
			set{ _thirdalarmname=value;}
			get{return _thirdalarmname;}
		}
		#endregion Model

	}
}

