﻿/**  版本信息模板在安装目录下，可自行修改。
* settlementpointalarmvalue.cs
*
* 功 能： N/A
* 类 名： settlementpointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:28   N/A    初版
*
* Copyright (c) 2012 Settlement Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Collections.Generic;
namespace Settlement.DAL
{
    /// <summary>
    /// 数据访问类:settlementpointalarmvalue
    /// </summary>
    public partial class settlementpointalarmvalue
    {
        public settlementpointalarmvalue()
        { }
        public static database db = new database();
        #region  BasicMethod
        //判断点名对象是否存在
        public bool Exist(Settlement.Model.settlementpointalarmvalue model)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("select count(1) from  settlementpointalarmvalue   where     xmno=@xmno    ");
            strSql.Append("   and       point_name=@point_name       ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
                                         };

            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);

            return obj == null ? true : int.Parse(obj.ToString()) > 0 ? true : false;

        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Settlement.Model.settlementpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into settlementpointalarmvalue(");
            strSql.Append("xmno,POINT_NAME,firstAlarmName,secondAlarmName,thirdAlarmName,pointtype,basepointname,remark)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@POINT_NAME,@firstAlarmName,@secondAlarmName,@thirdAlarmName,@pointtype,@basepointname,@remark)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@firstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@secondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@thirdAlarmName", OdbcType.VarChar,120),
                    new OdbcParameter("@pointtype", OdbcType.VarChar,120),
					new OdbcParameter("@basepointname", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500)};
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.point_name;
            parameters[2].Value = model.firstAlarmName;
            parameters[3].Value = model.secondAlarmName;
            parameters[4].Value = model.thirdAlarmName;
            parameters[5].Value = model.remark;
            parameters[6].Value = model.pointtype;
            parameters[7].Value = model.basepointname;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Settlement.Model.settlementpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update settlementpointalarmvalue set ");
            strSql.Append("firstAlarmName=@firstAlarmName,");
            strSql.Append("secondAlarmName=@secondAlarmName,");
            strSql.Append("thirdAlarmName=@thirdAlarmName,");
            strSql.Append("pointtype=@pointtype,");
            strSql.Append("basepointname=@basepointname,");
            strSql.Append("remark=@remark");
            strSql.Append("   where   ");
            strSql.Append("POINT_NAME=@POINT_NAME");
            OdbcParameter[] parameters = {
					new OdbcParameter("@firstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@secondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@thirdAlarmName", OdbcType.VarChar,120),
                    new OdbcParameter("@pointtype", OdbcType.VarChar,120),
					new OdbcParameter("@basepointname", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120)
                                         };

            parameters[0].Value = model.firstAlarmName;
            parameters[1].Value = model.secondAlarmName;
            parameters[2].Value = model.thirdAlarmName;
            parameters[3].Value = model.pointtype;
            parameters[4].Value = model.basepointname;
            parameters[5].Value = model.remark;
            parameters[6].Value = model.point_name;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, Settlement.Model.settlementpointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update settlementpointalarmvalue set ");
            strSql.Append("firstAlarmName=@firstAlarmName,");
            strSql.Append("secondAlarmName=@secondAlarmName,");
            strSql.Append("thirdAlarmName=@thirdAlarmName,");
            strSql.Append(" remark=@remark,");
            strSql.Append(" basepointname=@basepointname ");
            strSql.Append("   where   ");
            strSql.Append(" point_name  in ('" + pointNameStr + "')   and xmno = @xmno ");
            OdbcParameter[] parameters = {

					new OdbcParameter("@firstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@secondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@thirdAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@basepointname", OdbcType.VarChar,500),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };



            parameters[0].Value = model.firstAlarmName;
            parameters[1].Value = model.secondAlarmName;
            parameters[2].Value = model.thirdAlarmName;
            parameters[3].Value = model.remark;
            parameters[4].Value = model.basepointname;
            parameters[5].Value = model.xmno;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(Settlement.Model.settlementpointalarmvalue model)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("delete from  settlementpointalarmvalue  where point_name in (@point_name) and  xmno = @xmno  ");
            OdbcParameter[] parameters = {
                new OdbcParameter("@point_name", OdbcType.VarChar,120),
                new OdbcParameter("@xmno", OdbcType.Int,4)
			};
            parameters[0].Value = model.point_name;
            parameters[1].Value = model.xmno;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 点名加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="ls"></param>
        /// <returns></returns>
        public bool TotalStationPointLoadDAL(int xmno, out List<string> ls)
        {

            string sql = "select distinct(point_name) from settlementpointalarmvalue where xmno='" + xmno + "' order by point_name asc";

            OdbcConnection conn = db.GetStanderConn(xmno);
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public Settlement.Model.settlementpointalarmvalue GetModel()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select ID,xmno,POINT_NAME,firstAlarmName,secondAlarmName,thirdAlarmName,remark,pointtype from settlementpointalarmvalue ");
        //    strSql.Append(" where ");
        //    OdbcParameter[] parameters = {
        //    };

        //    Settlement.Model.settlementpointalarmvalue model=new Settlement.Model.settlementpointalarmvalue();
        //    DataSet ds=DbHelperOdbc.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        return DataRowToModel(ds.Tables[0].Rows[0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Settlement.Model.settlementpointalarmvalue DataRowToModel(DataRow row)
        {
            Settlement.Model.settlementpointalarmvalue model = new Settlement.Model.settlementpointalarmvalue();
            if (row != null)
            {
                
                if (row["xmno"] != null)
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["POINT_NAME"] != null)
                {
                    model.point_name = row["POINT_NAME"].ToString();
                }
                if (row["firstAlarmName"] != null)
                {
                    model.firstAlarmName = row["firstAlarmName"].ToString();
                }
                if (row["secondAlarmName"] != null)
                {
                    model.secondAlarmName = row["secondAlarmName"].ToString();
                }
                if (row["thirdAlarmName"] != null)
                {
                    model.thirdAlarmName = row["thirdAlarmName"].ToString();
                }
                if (row["remark"] != null)
                {
                    model.remark = row["remark"].ToString();
                }
               
            }
            return model;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, out Settlement.Model.settlementpointalarmvalue model)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,POINT_NAME,firstAlarmName,secondAlarmName,thirdAlarmName,remark from settlementpointalarmvalue where       xmno=@xmno   and   point_name=@point_name   ");

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,10),
                    new OdbcParameter("@point_name", OdbcType.VarChar,200)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            model = new Settlement.Model.settlementpointalarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool PointAlarmValueTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select point_name,firstAlarmName,secondAlarmName,thirdAlarmName,remark from settlementpointalarmvalue where xmno='{3}' and {4} {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select point_name,firstAlarmName,secondAlarmName,thirdAlarmName,remark from settlementpointalarmvalue where xmno='{3}' and {4}  {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointAlarmValueTableRowsCount(string xmname, string searchstring, int xmno, out string totalCont)
        {

            string sql = "select count(*) from settlementpointalarmvalue where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }
        


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

