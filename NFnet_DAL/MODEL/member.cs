﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Data;
using NFnet_DAL;
using NFnet_MODAL;
using System.Data.Odbc;
namespace NFnet_DAL.MODEL
{
    public class member
    {
        private string userId,
       password,
       role,
       userGroup,
       userName,
       workNo,
       position,
       tel,
       email,
       zczsmc,
       zczsbh,
       zczs,
       sgzsmc,
       sgzsbh,
       sgzs,
       unitName;


        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string Role
        {
            get { return role; }
            set { role = value; }
        }

        public string UserGroup
        {
            get { return userGroup; }
            set { userGroup = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string WorkNo
        {
            get { return workNo; }
            set { workNo = value; }
        }

        public string Position
        {
            get { return position; }
            set { position = value; }
        }

        public string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public string Zczsmc
        {
            get { return zczsmc; }
            set { zczsmc = value; }
        }

        public string Zczsbh
        {
            get { return zczsbh; }
            set { zczsbh = value; }
        }

        public string Zczs
        {
            get { return zczs; }
            set { zczs = value; }
        }

        public string Sgzsmc
        {
            get { return sgzsmc; }
            set { sgzsmc = value; }
        }

        public string Sgzsbh
        {
            get { return sgzsbh; }
            set { sgzsbh = value; }
        }

        public string Sgzs
        {
            get { return sgzs; }
            set { sgzs = value; }
        }

        public string UnitName
        {
            get { return unitName; }
            set { unitName = value; }
        }
        
    }

}