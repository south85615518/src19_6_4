﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_alarmvalue_gt.cs
*
* 功 能： N/A
* 类 名： fmos_alarmvalue_gt
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/10/29 13:45:08   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace NFnet_DAL.MODEL
{
	/// <summary>
	/// fmos_alarmvalue_gt:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class alarmvalue
	{
		public alarmvalue()
		{}
		#region Model
		private int _id=0;
		private string _name;
		private double _this_rn;
		private double _this_re;
		private double _this_rz;
		private double _ac_rn_u;
		private double _ac_rn_l=0;
		private double _ac_re_u;
		private double _ac_re_l=0;
		private double _ac_rz_u;
		private double _ac_rz_l=0;
		private int _xmno;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string NAME
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double This_rN
		{
			set{ _this_rn=value;}
			get{return _this_rn;}
		}
	
		/// <summary>
		/// 
		/// </summary>
		public double This_rE
		{
			set{ _this_re=value;}
			get{return _this_re;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public double This_rZ
		{
			set{ _this_rz=value;}
			get{return _this_rz;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public double Ac_rN_U
		{
			set{ _ac_rn_u=value;}
			get{return _ac_rn_u;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_rN_L
		{
			set{ _ac_rn_l=value;}
			get{return _ac_rn_l;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_rE_U
		{
			set{ _ac_re_u=value;}
			get{return _ac_re_u;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_rE_L
		{
			set{ _ac_re_l=value;}
			get{return _ac_re_l;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_rZ_U
		{
			set{ _ac_rz_u=value;}
			get{return _ac_rz_u;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double Ac_rZ_L
		{
			set{ _ac_rz_l=value;}
			get{return _ac_rz_l;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

