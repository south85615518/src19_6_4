﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_DAL.MODEL
{
    public class alarmvalue_
    {
        private string id, name, this_rn, this_re, this_rz, ac_rn, ac_re, ac_rz;
        private int xmno;

        public int Xmno
        {
            get { return xmno; }
            set { xmno = value; }
        }

        public string Ac_rz
        {
            get { return ac_rz; }
            set { ac_rz = value; }
        }

        public string Ac_re
        {
            get { return ac_re; }
            set { ac_re = value; }
        }

        public string Ac_rn
        {
            get { return ac_rn; }
            set { ac_rn = value; }
        }

        public string This_rz
        {
            get { return this_rz; }
            set { this_rz = value; }
        }

        public string This_re
        {
            get { return this_re; }
            set { this_re = value; }
        }

        public string This_rn
        {
            get { return this_rn; }
            set { this_rn = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

    }
}