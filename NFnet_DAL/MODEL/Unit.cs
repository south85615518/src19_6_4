﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Odbc;

namespace NFnet_DAL.MODEL
{
    public class Unit
    {
        private string unitName, pro, city, country, addr, tel, email, natrue, linkman, aptitude, police, taxproof, state, dbName, cgdbName;

        public string CgdbName
        {
            get { return cgdbName; }
            set { cgdbName = value; }
        }

        public string DbName
        {
            get { return dbName; }
            set { dbName = value; }
        }

        public string State
        {
            get { return state; }
            set { state = value; }
        }
        
        
        public string Taxproof
        {
            get { return taxproof; }
            set { taxproof = value; }
        }

        public string Police
        {
            get { return police; }
            set { police = value; }
        }

        public string Aptitude
        {
            get { return aptitude; }
            set { aptitude = value; }
        }

        public string Linkman
        {
            get { return linkman; }
            set { linkman = value; }
        }

        public string Natrue
        {
            get { return natrue; }
            set { natrue = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public string Addr
        {
            get { return addr; }
            set { addr = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Pro
        {
            get { return pro; }
            set { pro = value; }
        }
        
        public string UnitName
        {
            get { return unitName; }
            set { unitName = value; }
        }
        
    }
}