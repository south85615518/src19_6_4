﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_DAL.MODEL
{
    public class pointalarmvalue
    {
        private string id, pointtype,  taskName, pointName, firstAlarmName, secondAlarmName, thirdAlarmName, remark;
        private int xmno;

        public int Xmno
        {
            get { return xmno; }
            set { xmno = value; }
        }

        public string Pointtype
        {
            get { return pointtype; }
            set { pointtype = value; }
        }

        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        public string ThirdAlarmName
        {
            get { return thirdAlarmName; }
            set { thirdAlarmName = value; }
        }

        public string SecondAlarmName
        {
            get { return secondAlarmName; }
            set { secondAlarmName = value; }
        }

        public string FirstAlarmName
        {
            get { return firstAlarmName; }
            set { firstAlarmName = value; }
        }

        public string PointName
        {
            get { return pointName; }
            set { pointName = value; }
        }

        public string TaskName
        {
            get { return taskName; }
            set { taskName = value; }
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}