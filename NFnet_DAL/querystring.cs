﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_DAL
{
    /// <summary>
    /// 对于空串的查询条件处理方式
    /// </summary>
    public class querystring
    {
        public string querykey;
        public static string  querynvl(string queryname,string queryword/*查询的字段名*/,string op,string inserth,string insertt)
        { 
         
            if (queryword == "")
            {
                return "1 = 1";
            }
            if ((inserth == ""||inserth==null)&&(insertt == ""||insertt==null))
            {
                return queryname + op +" '"+queryword+"'";
            }
            else {
                return queryname+op+inserth + queryword + insertt;
            }
        }
    }
}