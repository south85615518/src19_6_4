﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;
using System.Data.Odbc;
using NFnet_DAL;
using NFnet_MODAL;
namespace NFnet_DAL
{
    public class Sql_DAL_E : Sql_Bll_E
    {
        public static accessdbse adb = new accessdbse();
        public static database db = new database();
        /// <summary>
        /// 单位机构信息添加
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public string UnitInsertExcute(Unit unit)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_i();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            cmd.CommandText = sql;
            cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            cmd.Parameters.Add("@pro", OleDbType.VarChar).Value = unit.Pro;
            cmd.Parameters.Add("@city", OleDbType.VarChar).Value = unit.City;
            cmd.Parameters.Add("@country", OleDbType.VarChar).Value = unit.Country;
            cmd.Parameters.Add("@adr", OleDbType.VarChar).Value = unit.Addr;
            cmd.Parameters.Add("@tel", OleDbType.VarChar).Value = unit.Tel;
            cmd.Parameters.Add("@email", OleDbType.VarChar).Value = unit.Email;
            cmd.Parameters.Add("@nature", OleDbType.VarChar).Value = unit.Natrue;
            cmd.Parameters.Add("@linkman", OleDbType.VarChar).Value = unit.Linkman;
            cmd.Parameters.Add("@aptitude", OleDbType.VarChar).Value = unit.Aptitude;
            cmd.Parameters.Add("@police", OleDbType.VarChar).Value = unit.Police;
            cmd.Parameters.Add("@taxproof", OleDbType.VarChar).Value = unit.Taxproof;
            cmd.Parameters.Add("@state", OleDbType.VarChar).Value = "未通过审核";
            cmd.Parameters.Add("@createTime", OleDbType.Date).Value = DateTime.Now.Date;
            cmd.Parameters.Add("@dbname", OleDbType.VarChar).Value = unit.DbName;
            updatedb udb = new updatedb();
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }
        /// <summary>
        /// 单位机构信息修改
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public string UnitUpdateExcute(Unit unit)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_u();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            cmd.Parameters.Add("@pro", OleDbType.VarChar).Value = unit.Pro;
            cmd.Parameters.Add("@city", OleDbType.VarChar).Value = unit.City;
            cmd.Parameters.Add("@country", OleDbType.VarChar).Value = unit.Country;
            cmd.Parameters.Add("@adr", OleDbType.VarChar).Value = unit.Addr;
            cmd.Parameters.Add("@tel", OleDbType.VarChar).Value = unit.Tel;
            cmd.Parameters.Add("@email", OleDbType.VarChar).Value = unit.Email;
            cmd.Parameters.Add("@nature", OleDbType.VarChar).Value = unit.Natrue;
            cmd.Parameters.Add("@linkman", OleDbType.VarChar).Value = unit.Linkman;
            cmd.Parameters.Add("@aptitude", OleDbType.VarChar).Value = unit.Aptitude;
            cmd.Parameters.Add("@police", OleDbType.VarChar).Value = unit.Police;
            cmd.Parameters.Add("@taxproof", OleDbType.VarChar).Value = unit.Taxproof;
            updatedb udb = new updatedb();
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }


        public Unit UnitSelectExcute(Unit unit)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_s();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@unitName","'"+unit.UnitName+"'");
            //OleDbCommand cmd = new OleDbCommand(sql,conn);
            //cmd.CommandText = sql;
            //cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            List<string> ls = querysql.queryaccesslist(sql);

            foreach (string str in ls)
            {
                string[] vals = str.Split(',');
                Unit unitEntrl = new Unit
                {
                    UnitName = unit.UnitName,
                     Pro = vals[0] , 
                    City = vals[1], 
                    Country = vals[2], 
                    Addr = vals[3], 
                    Tel = vals[4], 
                    Email = vals[5], 
                    Natrue = vals[6], 
                    Linkman = vals[7], 
                    Aptitude = vals[8], 
                    Police = vals[9], 
                    Taxproof = vals[10],
                    State = vals[11],
                     DbName = vals[12]
                   
                };
                return unitEntrl;
            }
            return null;
        }
        /// <summary>
        /// 机构信息列表
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public List<Unit> UnitSelectListExcute(Unit unit)
        {
            Sql_Bll bll = new Sql_Bll();
            List<Unit> lu = new List<Unit>();
            string sql = bll.GetUnitSql_s();
            OleDbConnection conn = adb.getconn();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            List<string> ls = querysql.queryaccesslist(sql);

            Unit unitEntrl = new Unit
            {
                UnitName = ls[0],
                Pro = ls[1],
                Addr = ls[2],
                Aptitude = ls[3],
                City = ls[4],
                Country = ls[5],
                Email = ls[6],
                Linkman = ls[7],
                Natrue = ls[8],
                Police = ls[9],
                Taxproof = ls[10],
                Tel = ls[11]
            };
            lu.Add(unitEntrl);

            return lu;
        }

        /// <summary>
        /// 人员信息添加
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public string MemberInsertExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_i();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@userId","'"+member.UserId+"'");
            sql = sql.Replace("@pass","'"+member.Password+"'");
            sql = sql.Replace("@role","'"+member.Role+"'");
            sql = sql.Replace("@userGroup","'"+member.UserGroup+"'");
            sql = sql.Replace("@userName","'"+member.UserName+"'");
            sql = sql.Replace("@workNo","'"+member.WorkNo+"'");
            sql = sql.Replace("@position","'"+member.Position+"'");
            sql = sql.Replace("@tel","'"+member.Tel+"'");
            sql = sql.Replace("@email","'"+member.Email+"'");
            sql = sql.Replace("@zczsmc","'"+member.Zczsmc+"'");
            sql = sql.Replace("@zczsbh","'"+member.Zczsbh+"'");
            sql = sql.Replace("@zczs","'"+member.Zczs+"'");
            sql = sql.Replace("@sgzsmc","'"+member.Sgzsmc+"'");
            sql = sql.Replace("@sgzsbh","'"+member.Sgzsbh+"'");
            sql = sql.Replace("@sgzs","'"+member.Sgzs+"'");
            sql = sql.Replace("@unitName","'"+member.UnitName+"'");
            sql = sql.Replace("@createTime", "'" + DateTime.Now.Date + "'");
            OleDbCommand cmd = new OleDbCommand(sql,conn);
            //cmd.CommandText = sql;
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }
        /// <summary>
        /// 人员信息更新
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public string MemberUpdateExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_u();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@userId", "'" + member.UserId + "'");
            sql = sql.Replace("@pass", "'" + member.Password + "'");
            sql = sql.Replace("@role", "'" + member.Role + "'");
            sql = sql.Replace("@userGroup", "'" + member.UserGroup + "'");
            sql = sql.Replace("@userName", "'" + member.UserName + "'");
            sql = sql.Replace("@workNo", "'" + member.WorkNo + "'");
            sql = sql.Replace("@position", "'" + member.Position + "'");
            sql = sql.Replace("@tel", "'" + member.Tel + "'");
            sql = sql.Replace("@email", "'" + member.Email + "'");
            sql = sql.Replace("@zczsmc", "'" + member.Zczsmc + "'");
            sql = sql.Replace("@zczsbh", "'" + member.Zczsbh + "'");
            sql = sql.Replace("@zczs", "'" + member.Zczs + "'");
            sql = sql.Replace("@sgzsmc", "'" + member.Sgzsmc + "'");
            sql = sql.Replace("@sgzsbh", "'" + member.Sgzsbh + "'");
            sql = sql.Replace("@sgzs", "'" + member.Sgzs + "'");
            sql = sql.Replace("@unitName", "'" + member.UnitName + "'");
            sql = sql.Replace("@createTime", "'" + DateTime.Now.Date + "'");
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }
        /// <summary>
        /// 人员信息获取
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public member MemberSelectExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_s();
            OleDbConnection conn = adb.getconn();
            sql = sql.Replace("@userId", "'"+member.UserId+"'");
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "userId", 
            "pass", 
            "role", 
            "userGroup", 
            "userName", 
            "workNo", 
            "position", 
            "tel", 
            "zczsmc", 
            "zczsbh", 
            "zczs", 
            "sgzsmc",
            "sgzsbh",
            "sgzs",
            "unitName",
             "email"};
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<member> lds = new List<member>();
            foreach (List<string> ls in lls)
            {
                member memberEntrl = new member
                {
                    UserId = ls[0],
                    Password = ls[1],
                    Role = ls[2],
                    UserGroup = ls[3],
                    UserName = ls[4],
                    WorkNo = ls[5],
                    Position = ls[6],
                    Tel = ls[7],
                    Zczsmc = ls[8],
                    Zczsbh = ls[9],
                    Zczs = ls[10],
                    Sgzsmc = ls[11],
                    Sgzsbh = ls[12],
                    Sgzs = ls[13],
                    UnitName = ls[14],
                     Email = ls[15]

                };
                return memberEntrl;
            }
            return null;
        }
        /// <summary>
        /// 人员信息列表获取
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public List<member> MemberSelectListExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_s();
            List<member> lm = new List<member>();
            OleDbConnection conn = adb.getconn();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add("@userId", OleDbType.VarChar).Value = member.UserId;
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "userId", 
            "password", 
            "role", 
            "userGroup", 
            "userName", 
            "workNo", 
            "position", 
            "tel", 
            "zczsmc", 
            "zczsbh", 
            "zczs", 
            "sgzsmc",
            "sgzsbh",
            "sgzs",
            "unitName" };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<member> lds = new List<member>();
            foreach (List<string> ls in lls)
            {
                member memberEntrl = new member
                {
                    UserId = ls[0],
                    Password = ls[1],
                    Role = ls[2],
                    UserGroup = ls[3],
                    UserName = ls[4],
                    WorkNo = ls[5],
                    Position = ls[6],
                    Tel = ls[7],
                    Zczsmc = ls[8],
                    Zczsbh = ls[9],
                    Zczs = ls[10],
                    Sgzsmc = ls[11],
                    Sgzsbh = ls[12],
                    Sgzs = ls[13],
                    UnitName = ls[14]

                };
                lds.Add(memberEntrl);
            }
            return lds;

        }


        public DataTable MemberTableExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_t();
            List<member> lm = new List<member>();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@unitName","'"+member.UnitName+"'");
            DataTable dt = new DataTable();
            OleDbDataAdapter adpter = new OleDbDataAdapter(sql,conn);
            try
            {
                adpter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex,"人员信表获取出错");
            }
            finally
            {
                conn.Close();
            }
            return null;
        }


        public member MemberLoginExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.MemberLoginSql_s();
            OleDbConnection conn = adb.getconn();

            sql = sql.Replace("@userId", "'"+member.UserId+"'");
            sql = sql.Replace("@pass","'"+member.Password+"'");
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "userId", 
            "pass", 
            "role", 
            "userGroup", 
            "userName", 
            "workNo", 
            "position", 
            "tel", 
            "zczsmc", 
            "zczsbh", 
            "zczs", 
            "sgzsmc",
            "sgzsbh",
            "sgzs",
            "unitName" };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<member> lds = new List<member>();
            foreach (List<string> ls in lls)
            {
                member memberEntrl = new member
                {
                    UserId = ls[0],
                    Password = ls[1],
                    Role = ls[2],
                    UserGroup = ls[3],
                    UserName = ls[4],
                    WorkNo = ls[5],
                    Position = ls[6],
                    Tel = ls[7],
                    Zczsmc = ls[8],
                    Zczsbh = ls[9],
                    Zczs = ls[10],
                    Sgzsmc = ls[11],
                    Sgzsbh = ls[12],
                    Sgzs = ls[13],
                    UnitName = ls[14]

                };
                return memberEntrl;
            }
            return null;
        }


        public DataTable UnitTableExcute(Unit member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_t();
            List<member> lm = new List<member>();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            DataTable dt = new DataTable();
            OleDbDataAdapter adpter = new OleDbDataAdapter(sql, conn);
            try
            {
                adpter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, "机构信息表获取出错");
            }
            finally
            {
                conn.Close();
            }
            return null;
        }


        public string InstrumentInsertExcute(Instrument instrument)
        {

            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_i();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@instrumentName", "'" + instrument.InstrumentName + "'");
            sql = sql.Replace("@vender", "'" + instrument.Vender + "'");
            sql = sql.Replace("@instrumentNo ", "'" + instrument.InstrumentNo + "'");
            sql = sql.Replace("@instrumentType ", "'" + instrument.InstrumentType + "'");
            sql = sql.Replace("@verificationDate ", "'" + instrument.VerificationDate + "'");
            sql = sql.Replace("@validityPeriod ", "'" + instrument.ValidityPeriod + "'");
            sql = sql.Replace("@verificationProof ", "'" + instrument.VerificationProof + "'");
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            //cmd.CommandText = sql;

            updatedb udb = new updatedb();
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }


            return ""; 
        }

        public string InstrumentUpdateExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        public DataTable InstrumentTableExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        public Instrument InstrumentSelectExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        public List<Instrument> InstrumentSelectListExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        public Instrument InstrumentLoginExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 数据库列表加载
        /// </summary>
        /// <returns></returns>
        public List<string> DbListExcute()
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.dbNameSql_s();
            OdbcConnection conn = db.GetXmConn("information_schema");
            List<string> ls = querysql.querystanderlist(sql,conn);
            return ls;
        }



        public List<string> ProjectNameExcute(Unit unit)
        {
            
            Sql_Bll bll = new Sql_Bll();
            //unit = UnitSelectExcute(unit);
            string sql = bll.taskSql_s();

            OdbcConnection conn = null;
            try
            {
                conn = db.GetXmConn(unit.DbName);
                List<string> ls = querysql.querystanderlist(sql, conn);
                return ls;
            }
            catch(Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return null;
            }
        }

        public List<string> ProjectNameExcute()
        {
            throw new NotImplementedException();
        }

        public string projectInfoInsertExcute(xm xminfo)
        {
             Sql_Bll bll = new Sql_Bll();
            string sql = bll.projectInfoSql_u();
            sql = sql.Replace("@xmname","'"+xminfo.Xmname+"'");
            sql = sql.Replace("@jcdw", "'"+xminfo.Jcdw+"'");
            sql = sql.Replace("@dbase", "'"+xminfo.Dbase+"'");
            updatedb udb = new updatedb();
            try
            {
                udb.updateaccess(sql);
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }


        void Sql_Bll_E.projectInfoInsertExcute(xm xminfo)
        {
            throw new NotImplementedException();
        }


        public List<string> StudyPointExcute(jcxmAndjcd jclxandjcd,string sql)
        {
            Sql_Bll bll = new Sql_Bll();
            OdbcConnection conn = db.GetStanderConn(jclxandjcd.Xmname);
            List<string> ls = querysql.querystanderlist(sql, conn);
            return ls;
        }


        public List<string> StudyPointExcute(jcxmAndjcd xminfo)
        {
            throw new NotImplementedException();
        }


        public xm XmInfoExcute(xm xminfo)
        {
            return null;
        }


        public string NearestXmnameExcute(member member)
        {
             Sql_Bll bll = new Sql_Bll();
             string sql = ""; 
             member = MemberSelectExcute(member);
             if (member.Position == "系统管理员")
             {
                 sql = bll.CGNearest_s();

             }
             else
             {
                 sql = bll.NearestSql_s();
                 sql = sql.Replace("@userId","'"+member.UserId+"'");
             }
             List<string> xmnameList = querysql.queryaccesslist(sql);
             return xmnameList[0];
        }


        public List<string> GMXmnameList(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GMXmnamesql_s();
            sql = sql.Replace("@jcdw","'"+member.UnitName+"'");
            List<string> ls = querysql.queryaccesslist(sql);
            return ls;
        }
    }
}