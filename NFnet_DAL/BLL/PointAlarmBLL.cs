﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.MODEL;
using NFnet_DAL.DAL;
using System.Data;

namespace NFnet_DAL.BLL
{
    public class PointAlarmBLL
    {
        private readonly PointAlarmDAL dal = new PointAlarmDAL();
        public bool PointAlarmValueInsert(pointalarmvalue alarm, string xmname,out string mssg)
        {
            try
            {
                if (dal.PointAlarmValueInsert(alarm, xmname))
                {
                    mssg = "点号预警参数添加成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数添加失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数添加出错，出错信息"+ex.Message;
                return false;
            }
        }

        public bool PointAlarmValueUpdate(pointalarmvalue alarm, string xmname,out string mssg)
        {

            try
            {
                if (dal.PointAlarmValueUpdate(alarm, xmname))
                {
                    mssg = "点号预警参数更新成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数更新失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数更新出错，出错信息" + ex.Message;
                return false;
            }

        }


        public bool PointAlarmValueEmptyInsert(string xmname,out string mssg)
        {

            try
            {
                if (dal.PointAlarmValueEmptyInsert(xmname))
                {
                    mssg = "点号预警参数判空新增成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数判空新增失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数判空新增出错，出错信息" + ex.Message;
                return false;
            }
        }


        public bool PointAlarmValueIncrement(string xmname,out string mssg)
        {
            try
            {
                if (dal.PointAlarmValueIncrement(xmname))
                {
                    mssg = "点号预警参数增量新增成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数增量新增失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数增量新增出错，出错信息" + ex.Message;
                return false;
            }
        }
        public bool PointAlarmValueDel(pointalarmvalue alarm,out string mssg)
        {

            try
            {
                if (dal.PointAlarmValueDel(alarm))
                {
                    mssg = "点号预警参数删除成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数删除失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数删除出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool PointAlarmValueDelCasc(alarmvalue alarm, string xmname,out string mssg)
        {
            try
            {
                if (dal.PointAlarmValueDelCasc(alarm, xmname))
                {
                    mssg = "点号预警参数级联删除成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数级联删除失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数级联删除出错，出错信息" + ex.Message;
                return false;
            }
        }


        public bool PointAlarmValueMultilUpdate(pointalarmvalue alarm, string xmname,out string mssg)
        {
            try
            {
                if (dal.PointAlarmValueMultilUpdate(alarm, xmname))
                {
                    mssg = "点号预警参数多点更新成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数多点更新失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数多点更新出错，出错信息" + ex.Message;
                return false;
            }

        }

        public bool PointAlarmValueTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.PointAlarmValueTableLoad( searchstring,  startPageIndex,  pageSize,  xmno,  xmname,  colName,  sord, out  dt ))
                {
                    mssg = "点号预警参数加载成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数加载失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数加载出错，出错信息" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 表面位移点表
        /// </summary>
        /// <param name="searchstring"></param>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool PointTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.PointTableLoad(searchstring, startPageIndex, pageSize, xmno, xmname, colName, sord, out  dt))
                {
                    mssg = string.Format("项目{0}点号表加载成功！",xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}点号表加载失败！", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}点号表加载出错,错误信息:"+ex.Message, xmname);
                return false;
            }
        }


        public bool PointAlarmValueTableRowsCount(string xmname, string searchstring, int xmno, out string totalCont,out string mssg)
        {
            totalCont = "";
            try
            {
                if (dal.PointAlarmValueTableRowsCount( xmname,  searchstring,  xmno, out  totalCont))
                {
                    mssg = "点号预警参数记录数加载成功！";
                    return true;
                }
                else
                {
                    mssg = "点号预警参数记录数加载失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "点号预警参数记录数加载出错，出错信息" + ex.Message;
                return false;
            }
        }

    }
}