﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SqlHelpers;
using NFnet_DAL.MODEL;
using System.Data;
using NFnet_DAL.DAL;
using System.Data.Odbc;

namespace NFnet_DAL.BLL
{
    public class AlarmBLL
    {
        public static database db = new database();
        private readonly AlarmDAL dal = new AlarmDAL();
        public bool AlarmValueInsert(alarmvalue alarm,out string mssg)
        {
            try
            {
                if (dal.AlarmValueInsert(alarm))
                {
                    mssg = "预警参数添加成功！";
                    return true;
                }
                else
                {
                    mssg = "预警参数添加失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "预警参数添加出错，出错信息" + ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns>1表示更新成功,-1表示程序出错</returns>
        public bool AlarmValueUpdate(alarmvalue alarm, out string mssg)
        {
            try
            {
                if (dal.AlarmValueUpdate(alarm))
                {
                    mssg = "预警参数更新成功！";
                    return true;
                }
                else
                {
                    mssg = "预警参数更新失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "预警参数更新出错，出错信息" + ex.Message;
                return false;
            }



        }


        public bool AlarmValueSelect(int xmno, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.AlarmValueSelect(xmno, out  dt))
                {
                    mssg = "预警参数列表获取成功！";
                    return true;
                }
                else
                {
                    mssg = "预警参数列表获取失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "预警参数列表获取出错，出错信息" + ex.Message;
                return false;
            }
           

        }

        public bool AlarmValueDel(alarmvalue alarm,out string mssg)
        {
            try
            {
                if (dal.AlarmValueDel(alarm))
                {
                    mssg = "预警参数删除成功！";
                    return true;
                }
                else
                {
                    mssg = "预警参数删除失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "预警参数删除出错，出错信息" + ex.Message;
                return false;
            }
          

        }
        public bool PointAlarmValueDelCasc(alarmvalue alarm,out string mssg)
        {
            try
            {
                if (dal.PointAlarmValueDelCasc(alarm))
                {
                    mssg = "预警参数级联删除成功！";
                    return true;
                }
                else
                {
                    mssg = "预警参数级联删除失败！";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "预警参数级联删除出错，出错信息" + ex.Message;
                return false;
            }
        }


        public bool AlarmValueTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.AlarmValueTableLoad(startPageIndex, pageSize, xmno, colName, sord, out  dt))
                {
                    mssg = "预警参数表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "预警参数表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "预警参数表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool AlarmValueTableRowsCount( string colName, int xmno, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.AlarmValueTableRowsCount(xmno, out totalCont))
                {
                    mssg = "预警参数表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "预警参数表记录数加载失败!";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "预警参数表加载出错，出错信息" + ex.Message;
                return false;
            }
        }
        public bool AlarmValueNameGet(int xmno, out string alarmValueNameStr,out string mssg)
        {
            alarmValueNameStr = "";
            try
            {
                if (dal.AlarmValueNameGet(xmno, out alarmValueNameStr))
                {
                    mssg = "预警名选项加载成功";
                    return true;
                }
                else
                {
                    mssg = "预警名选项加载失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "预警名选项加载出错，错误信息"+ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno,string NAME, out alarmvalue model,out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(xmno, NAME, out model))
                {
                    mssg = string.Format("预警名{0}对象加载成功",NAME);
                    return true;
                }
                else
                {
                    mssg = string.Format("预警名{0}对象加载失败", NAME);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("预警名{0}对象加载失败，错误信息" + ex.Message,NAME);
                return false;
            }
           
        }

    }
}