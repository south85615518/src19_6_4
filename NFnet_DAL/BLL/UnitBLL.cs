﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Odbc;
using Tool;
using SqlHelpers.dbcreate;
using SqlHelpers;
using MySql.Data.MySqlClient;

namespace NFnet_DAL.BLL
{
    
    public class UnitBLL
    {
        private NFnet_DAL.DAL.UnitDAL dal = new NFnet_DAL.DAL.UnitDAL();
        /// <summary>
        /// 保存机构信息
        /// </summary>
        public bool SaveUnitInfo( NFnet_DAL.MODEL.Unit unit ,out string errmsg)
        {

            try
            {
                if (dal.UnitInsertExcute(unit))
                {
                    errmsg = "机构信息保存成功!";
                    return true;
                }
                else
                {
                    errmsg = "机构信息保存失败!";
                    return true;
                }

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("机构信息保存失败,错误信息:" + ex.Message);
                errmsg = "机构信息保存失败,错误信息:"+ex.Message;
                return false;
            }
            
        }
        /// <summary>
        /// 更新机构信息
        /// </summary>
        /// <returns></returns>
        public bool UpdateUnitInfo(NFnet_DAL.MODEL.Unit unit, out string errmsg)
       {

           try
           {
               if (dal.UnitUpdateExcute(unit))
               {
                   errmsg = "机构信息更新成功!";
                   return true;
               }
               else
               {
                   errmsg = "机构信息更新失败!";
                   return true;
               }
           }
           catch (Exception ex)
           {
               ExceptionLog.ExceptionWrite(ex);
               errmsg = "机构信息更新失败,错误信息:"+ex.Message;
               return false;
           }
       }
        /// <summary>
        /// 数据库列表加载
        /// </summary>
        /// <returns></returns>
        public bool DbListExcute(out List<string> ls,out string mssg)
        {
            ls = null;
            try
            {
                if (dal.DbListExcute(out ls))
                {
                    mssg = "数据库列表加载成功";
                    return true;
                }
                else
                {
                    mssg = "数据库列表加载失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "数据库列加载出错,错误信息："+ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 获取机构信息
        /// </summary>
        /// <returns></returns>
        public bool GetUnit(string unitName, out NFnet_DAL.MODEL.Unit unitout,out string messg)
       {
           unitout = null;
           try
           {
               if (dal.UnitSelectExcute(unitName, out unitout))
               {
                   messg = "获取机构信息成功！";
                   return true;
               }
               else
               {
                   messg = "获取机构信息失败！";
                   return false;
               }

           }
           catch (Exception ex)
           {
               ExceptionLog.ExceptionWrite(ex);
               messg = "获取机构信息出错,错误信息:"+ex.Message;
               return false;
           }
       }
        public bool UnitTableExcute(NFnet_DAL.MODEL.Unit unit, out DataTable dt,out string mssg)
        {
            dt = null;
            try {
                if (dal.UnitTableExcute(unit, out dt))
                {
                    mssg = "机构信息获取成功!";
                    return true;
                }
                else
                {
                    mssg = "没有任何机构信息!";
                    return true;
                }
            }
            catch(Exception ex)
            {
                ExceptionLog.ExceptionWrite("机构信息获取出错!"+ex.Message);
                mssg = "机构信息获取出错,错误信息:"+ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 上传文件选择框中选择的文件
        /// </summary>
        /// <param name="fu"></param>
        public string ImageUploadInsert(FileUpload fu, string unitName, string postName)
        {
            string extend = Path.GetExtension(fu.FileName);
            string path = postName + "\\" + unitName + extend;
            ImageFileDel(path);
            fu.SaveAs(path);
            return FileImagePath(path);


        }
        /// <summary>
        /// 文件删除
        /// </summary>
        /// <param name="path"></param>
        public void ImageFileDel(string path)
        {
            string filepath = path;//此处应用绝对路径
            try
            {
                if (File.Exists(filepath))
                {
                    File.Delete(filepath);
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
            }
        }
        /// <summary>
        /// 绝对路径转化成相对路径
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public string FileImagePath(string fullPath)
        {
            try
            {
                return fullPath.Substring(fullPath.LastIndexOf("\\") + 1);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                return null;
            }
        }
        /// <summary>
        /// 创建机构数据库
        /// </summary>
        /// <param name="dbname"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool UnitDbCreate(string dbname, string path,out string mssg)
        {


            return dal.UnitDbCreate(dbname, path, out mssg);
               
           

        }
        public  bool ProcessTriggle(string dbname, List<string> ls,string triggername)
        {
            try
            {
                if (dal.ProcessTriggle(dbname, ls))
                {
                    ExceptionLog.ExceptionWrite(string.Format("创建触发器{0}成功", triggername));
                    return true;
                }
                else
                {
                    ExceptionLog.ExceptionWrite(string.Format("创建触发器{0}失败", triggername));
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("创建触发器{0}出错，错误信息:"+ex.Message, triggername));
                return false;
            }



            //return dal.ProcessTriggle(dbname,ls);
        }
        public List<string> ProcessTriggersListLoad(string path)
        {
            return Tool.FileHelper.ProcessFileInfoList(path);
        }
        public  bool ProcessTriggersCreate(ProcessTriggersCreateModel model)
        {
            List<string> ls = ProcessTriggersListLoad(model.path);
            foreach(string filename in ls)
            {
                List<string> triggersql = Tool.FileHelper.ProcessFileInfoList(Path.GetDirectoryName(model.path)+"\\"+filename);
                ProcessTriggle(model.dbname, triggersql,filename);
            }
            return true;
        }
        public class ProcessTriggersCreateModel
        {
            public string path { get; set; }
            public string dbname { get; set; }
            public ProcessTriggersCreateModel(string path, string dbname)
            {
                this.path = path;
                this.dbname = dbname;
            }
        }

        public bool UnitNameExist(string unitName,out string mssg)
        {
            try
            {
                if (dal.UnitNameExist(unitName))
                {
                    mssg = "存在"+unitName+"单位名称";
                    return true;
                }
                else
                {
                    mssg = "机构名称"+unitName+"不存在";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "机构名称查询出错，错误信息："+ex.Message;
                return false;
            }
        }
        
    }
}