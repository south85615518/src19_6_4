﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.DAL;
using NFnet_DAL.MODEL;
using System.IO;
using System.Data;
using Tool;
using System.Web.UI.WebControls;

namespace NFnet_DAL.BLL
{
    public class administratorBLL
    {
        public administratorDAL dal = new administratorDAL();
        /// <summary>
        /// 保存人员信息
        /// </summary>
        public bool SaveMemberInfo(NFnet_DAL.MODEL.member member, out string mssg)
        {

            try
            {
                if (dal.MemberInsertExcute(member))
                {
                    mssg = "管理员"+member.UserName+"信息保存成功";
                    return true;
                }
                else
                {
                    mssg = "管理员" + member.UnitName + "保存失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "保存管理员" + member.UnitName + "信息出错，出错信息:" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 更新人员信息
        /// </summary>
        /// <returns></returns>
        public bool UpdateMemberInfo(NFnet_DAL.MODEL.member member, out string mssg)
        {

            try
            {
                if (dal.MemberUpdateExcute(member))
                {
                    mssg = "成功";
                    return true;
                }
                else
                {
                    mssg = "失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "更新管理员信息出错，出错信息:" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 获取人员信息
        /// </summary>
        /// <returns></returns>
        public bool GetMember(string userId, out member outMember, out string mssg)
        {
            outMember = null;
            try
            {
                if (dal.MemberSelectExcute(userId, out outMember))
                {
                    mssg = "获取管理员" + userId + "信息成功";
                    return true;
                }
                else
                {
                    mssg = "没有获取到任何人员信息";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询管理员信息出错，出错信息:" + ex.Message;
                return false;
            }
        }






        public bool GetMemberTable(string unitName, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.MemberTableExcute(unitName, out dt))
                {
                    mssg = "获取"+unitName+"管理员列表成功";
                    return true;
                }
                else
                {
                    mssg = "获取" + unitName + "管理员列表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取" + unitName + "管理员列表出错，出错信息:" + ex.Message;
                return false;
            }
        }


    }
}