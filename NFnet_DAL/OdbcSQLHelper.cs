﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using NFnet_MODAL;
using System.Text.RegularExpressions;
namespace NFnet_DAL
{
    public class OdbcSQLHelper
    {
        //public static readonly string ConnectionStringLocalTransaction = ConfigurationManager.AppSettings["SQLConnString1"];
        //public static readonly string ConnectionStringInventoryDistributedTransaction = ConfigurationManager.AppSettings["SQLConnString2"];
        //public static readonly string ConnectionStringOrderDistributedTransaction = ConfigurationManager.AppSettings["SQLConnString3"];
        //public static readonly string ConnectionStringProfile = ConfigurationManager.AppSettings["SQLProfileConnString"];

        //// Hashtable to store cached parameters
        //private static Hashtable parmCache = Hashtable.Synchronized(new Hashtable());
        private static OdbcConnection conn;

        public static OdbcConnection Conn
        {
            get { return OdbcSQLHelper.conn; }
            set { OdbcSQLHelper.conn = value; }
        }
        /// <summary>
        /// Execute a OdbcCommand (that returns no resultset) against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new OdbcParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">a valid connection string for a OdbcConnection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of OdbcParamters used to execute the command</param>
        /// <returns>an int representing the number of rows affected by the command</returns>
        public static int ExecuteNonQuery(CommandType cmdType, string cmdText, params OdbcParameter[] commandParameters)
        {

            OdbcCommand cmd = new OdbcCommand();

            OdbcConnection conn = Conn;

            PrepareCommand(cmd, conn, cmdType, cmdText, commandParameters);
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            cmd.Transaction = ot;
            //OdbcCommand ocmd = new OdbcCommand(sql, conn);
            try
            {
                int val = cmd.ExecuteNonQuery();
                //提交事务
                ot.Commit();
                return val;
            }
            catch (Exception e)
            {
                ExceptionLog.ExceptionWrite(e, cmdText);
                ot.Rollback();//如操作失败，则事务回滚
                return -1;
            }
            finally
            {
                cmd.Parameters.Clear();
                ot.Dispose();
                //conn.Close();

            }




            //return val;

        }
        //查询结果集
        public static DataSet Query(string SQLString, params OdbcParameter[] cmdParms)
        {
            OdbcConnection connection = Conn;
            OdbcCommand cmd = new OdbcCommand();
            PrepareCommand(cmd, connection, CommandType.Text, SQLString, cmdParms);
            using (OdbcDataAdapter da = new OdbcDataAdapter(cmd))
            {
                DataSet ds = new DataSet();
                try
                {
                    da.Fill(ds, "ds");
                    cmd.Parameters.Clear();
                }
                catch (OdbcException ex)
                {
                    //throw new Exception(ex.Message);
                    ExceptionLog.ExceptionWrite("结果集获取出错:" + SQLString);
                }
                return ds;
            }

        }
        //查询结果集
        public static DataSet Query(string SQLString, string tabname, params OdbcParameter[] cmdParms)
        {
            OdbcConnection connection = Conn;
            OdbcCommand cmd = new OdbcCommand();
            PrepareCommand(cmd, connection, CommandType.Text, SQLString, cmdParms);
            using (OdbcDataAdapter da = new OdbcDataAdapter(cmd))
            {
                DataSet ds = new DataSet();
                try
                {
                    da.Fill(ds, tabname);
                    cmd.Parameters.Clear();
                }
                catch (OdbcException ex)
                {
                    //throw new Exception(ex.Message);
                    ExceptionLog.ExceptionWrite("结果集获取出错:" + SQLString);
                }
                return ds;
            }

        }
        /// <summary>
        /// Execute a OdbcCommand (that returns no resultset) against an existing database connection 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new OdbcParameter("@prodid", 24));
        /// </remarks>
        /// <param name="conn">an existing database connection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of OdbcParamters used to execute the command</param>
        /// <returns>an int representing the number of rows affected by the command</returns>
        public static int ExecuteNonQuery(OdbcConnection connection, CommandType cmdType, string cmdText, params OdbcParameter[] commandParameters)
        {

            OdbcCommand cmd = new OdbcCommand();

            PrepareCommand(cmd, connection, null, cmdType, cmdText, commandParameters);
            int val = cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            return val;
        }

        /// <summary>
        /// Execute a OdbcCommand (that returns no resultset) using an existing SQL Transaction 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new OdbcParameter("@prodid", 24));
        /// </remarks>
        /// <param name="trans">an existing sql transaction</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of OdbcParamters used to execute the command</param>
        /// <returns>an int representing the number of rows affected by the command</returns>
        public static int ExecuteNonQuery(OdbcTransaction trans, CommandType cmdType, string cmdText, params OdbcParameter[] commandParameters)
        {

            try
            {
                OdbcCommand cmd = new OdbcCommand();
                PrepareCommand(cmd, trans.Connection, trans, cmdType, cmdText, commandParameters);
                int val = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                return val;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, cmdText);
                return 0;
            }

        }

        /// <summary>
        /// Execute a OdbcCommand that returns a resultset against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  OdbcDataReader r = ExecuteReader(connString, CommandType.StoredProcedure, "PublishOrders", new OdbcParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">a valid connection string for a OdbcConnection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of OdbcParamters used to execute the command</param>
        /// <returns>A OdbcDataReader containing the results</returns>
        public static OdbcDataReader ExecuteReader(string connectionString, CommandType cmdType, string cmdText, params OdbcParameter[] commandParameters)
        {
            OdbcCommand cmd = new OdbcCommand();
            OdbcConnection conn = Conn;

            // we use a try/catch here because if the method throws an exception we want to 
            // close the connection throw code, because no datareader will exist, hence the 
            // commandBehaviour.CloseConnection will not work
            try
            {
                PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                OdbcDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
                return rdr;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        /// <summary>
        /// Execute a OdbcCommand that returns the first column of the first record against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  Object obj = ExecuteScalar(connString, CommandType.StoredProcedure, "PublishOrders", new OdbcParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">a valid connection string for a OdbcConnection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of OdbcParamters used to execute the command</param>
        /// <returns>An object that should be converted to the expected type using Convert.To{Type}</returns>
        public static object ExecuteScalar(string connectionString, CommandType cmdType, string cmdText, params OdbcParameter[] commandParameters)
        {
            OdbcCommand cmd = new OdbcCommand();

            OdbcConnection connection = Conn;

            PrepareCommand(cmd, connection, cmdType, cmdText, commandParameters);
            object val = cmd.ExecuteScalar();
            cmd.Parameters.Clear();
            return val;

        }

        /// <summary>
        /// Execute a OdbcCommand that returns the first column of the first record against an existing database connection 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  Object obj = ExecuteScalar(connString, CommandType.StoredProcedure, "PublishOrders", new OdbcParameter("@prodid", 24));
        /// </remarks>
        /// <param name="conn">an existing database connection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of OdbcParamters used to execute the command</param>
        /// <returns>An object that should be converted to the expected type using Convert.To{Type}</returns>
        public static object ExecuteScalar(OdbcConnection connection, CommandType cmdType, string cmdText, params OdbcParameter[] commandParameters)
        {

            try
            {
                OdbcCommand cmd = new OdbcCommand();

                PrepareCommand(cmd, connection, cmdType, cmdText, commandParameters);
                object val = cmd.ExecuteScalar();
                cmd.Parameters.Clear();
                return val;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, cmdText);
                return null;
            }
        }

        /// <summary>
        /// add parameter array to the cache
        /// </summary>
        /// <param name="cacheKey">Key to the parameter cache</param>
        /// <param name="cmdParms">an array of OdbcParamters to be cached</param>
        public static void CacheParameters(string cacheKey, params OdbcParameter[] commandParameters)
        {
            //parmCache[cacheKey] = commandParameters;
        }

        /// <summary>
        /// Retrieve cached parameters
        /// </summary>
        /// <param name="cacheKey">key used to lookup parameters</param>
        /// <returns>Cached OdbcParamters array</returns>
        public static OdbcParameter[] GetCachedParameters(string cacheKey)
        {
            //OdbcParameter[] cachedParms = (OdbcParameter[])parmCache[cacheKey];

            //if (cachedParms == null)
            //    return null;

            //OdbcParameter[] clonedParms = new OdbcParameter[cachedParms.Length];

            //for (int i = 0, j = cachedParms.Length; i < j; i++)
            //    clonedParms[i] = (OdbcParameter)((ICloneable)cachedParms[i]).Clone();

            return null;
        }

        /// <summary>
        /// Prepare a command for execution
        /// </summary>
        /// <param name="cmd">OdbcCommand object</param>
        /// <param name="conn">OdbcConnection object</param>
        /// <param name="trans">OdbcTransaction object</param>
        /// <param name="cmdType">Cmd type e.g. stored procedure or text</param>
        /// <param name="cmdText">Command text, e.g. Select * from Products</param>
        /// <param name="cmdParms">OdbcParameters to use in the command</param>
        private static void PrepareCommand(OdbcCommand cmd, OdbcConnection conn, OdbcTransaction trans, CommandType cmdType, string cmdText, OdbcParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            cmd.Connection = conn;
            cmd.CommandText = cmdText;

            if (trans != null)
                cmd.Transaction = trans;

            cmd.CommandType = cmdType;

            if (cmdParms != null)
            {
                foreach (OdbcParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }
        /// <summary>
        /// Prepare a command for execution
        /// </summary>
        /// <param name="cmd">OdbcCommand object</param>
        /// <param name="conn">OdbcConnection object</param>
        /// <param name="trans">OdbcTransaction object</param>
        /// <param name="cmdType">Cmd type e.g. stored procedure or text</param>
        /// <param name="cmdText">Command text, e.g. Select * from Products</param>
        /// <param name="cmdParms">OdbcParameters to use in the command</param>
        private static void PrepareCommand(OdbcCommand cmd, OdbcConnection conn, CommandType cmdType, string cmdText, OdbcParameter[] cmdParms)
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();

            cmd.Connection = conn;
            cmdText = reBuildCmd(cmdText);
            cmd.CommandText = cmdText;

            //if (trans != null)


            //cmd.CommandType = cmdType;

            if (cmdParms != null)
            {
                foreach (OdbcParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }

        }
        //替换SQL语句命令文本
        public static string reBuildCmd(string cmdText)
        {
            var str = Regex.Replace(cmdText, @"@\w+\s?", "?");
            //str = Regex.Replace(cmdText, @"\@\w+", "? ");
            //
            //var lowerstr = str.ToLower();
            //if (lowerstr.Contains("create") && lowerstr.Contains("trigger") && lowerstr.Contains("begin"))
            //{
            //    str = lowerstr.Replace("for each row begin", "as begin");
            //}
            return str;
        }

    }
}