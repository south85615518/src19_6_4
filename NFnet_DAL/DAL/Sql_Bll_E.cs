﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_MODAL;
namespace NFnet_BLL
{
    public interface Sql_Bll_E
    {
        //机构
         string UnitInsertExcute(Unit unit);
         string UnitUpdateExcute(Unit unit);
         Unit UnitSelectExcute(Unit unit);
         List<Unit> UnitSelectListExcute(Unit unit);
         DataTable UnitTableExcute(Unit member);
         //void UnitZsDelExcute(string unitName ,string zsname,string zsurl);
        //人员
         string MemberInsertExcute(member member);
         string MemberUpdateExcute(member member);
         DataTable MemberTableExcute(member member);
         member MemberSelectExcute(member member);
         List<member> MemberSelectListExcute(member member);
         member MemberLoginExcute(member member);
         List<member> UnitMemberExcute(string unitName);
        //设备
         string InstrumentInsertExcute(Instrument Instrument);
         string InstrumentUpdateExcute(Instrument Instrument);
         DataTable InstrumentTableExcute(Instrument Instrument);
         Instrument InstrumentSelectExcute(Instrument Instrument);
         List<Instrument> InstrumentSelectListExcute(Instrument Instrument);
         Instrument InstrumentLoginExcute(Instrument Instrument);
        //数据库
         List<string> DbListExcute();
         string UnitDbCreate(string dbname);
        //项目
         List<string> ProjectNameExcute();
         void projectInfoInsertExcute(xm xminfo);
         List<string> StudyPointExcute(jcxmAndjcd xminfo);
         xm XmInfoExcute(xm xm);
         string NearestXmnameExcute(member member);
         List<string> GMXmnameList(member member);
         //预警
         int AlarmValueInsert(alarmvalue alarm,string xmname);
         int AlarmValueUpdate(alarmvalue alarm,string xmname);
         DataTable AlarmValueSelect(string xmname);
         List<string> AlarmValueListSelect(string xmname);
         int AlarmValueDel(alarmvalue alarm, string xmname);
        //学习点
         int StudyPointInsert(studypoint point,string xmname);
         int StudyPointUpdate(studypoint point,string xmname);
         int StudyPointDelete(studypoint point,string xmname);
        //点号预警
         int PointAlarmValueInsert(pointalarmvalue pointalarm, string xmname);
         int PointAlarmValueUpdate(pointalarmvalue pointalarm, string xmname);
         int PointAlarmValueMultilUpdate(pointalarmvalue pointalarm, string xmname);
         int PointAlarmValueEmptyInsert(string xmname);
         int PointAlarmValueIncrement(string xmname);
         int PointAlarmValueDel(pointalarmvalue alarm, string xmname);
         int PointAlarmValueDelCasc(alarmvalue alarm, string xmname);
        //监测项目
         int PointMutilInsert(string pointnamestr,string tabname,string repeatkey,string xmname,string columnxmname);
        //数据导入SQL生成
         List<string> dataImportInsert(string tabname,List<string>attrs,List<string>repeatkey);
        //监测点平面
         int JcmapSpotInsert(jcmap mp);
         //int JcmapSpotUpdate(jcmap mp);
         List<jcmap> JcmapSpotLoad(jcmap mp);
         int JcmapSpotDelete(jcmap mp);
         //结果数据
         int ResultDataEditExcute(result result,string reportCyc);
         //结果记录是否存在
         bool IsResultRecord(result result,string reportCyc);
         //结果数据更新
         int ResultDataRecordUpdate(result result, string reportCyc);
         //成果数据删除
         int ResultDataDel(result result);
        //结果数据合法性检查
         string ResultDataLowCheck(result result,string reportCyc,string Time);
    }
}