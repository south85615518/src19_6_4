﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.MODEL;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;
using Tool;
using System.Text;

namespace NFnet_DAL.DAL
{
    public class AlarmDAL
    {
        public static database db = new database();
        /// <summary>
        /// 新增预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool AlarmValueInsert(alarmvalue alarm)
        {
            string sql = "insert into fmos_alarmvalue_gt(name,this_rn,this_re,this_rz,ac_rn_l,ac_re_l,ac_rz_l,ac_rn_u,ac_re_u,ac_rz_u,xmno)value(@name,@this_rn,@this_re,@this_rz,@ac_rn_l,@ac_re_l,@ac_rz_l,@ac_rn_u,@ac_re_u,@ac_rz_u,@xmno)";
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.xmno);
            sql = sql.Replace("@name", "'" + alarm.NAME + "'");
            sql = sql.Replace("@this_rn", "'" + alarm.This_rN+ "'");
            sql = sql.Replace("@this_re", "'" + alarm.This_rE + "'");
            sql = sql.Replace("@this_rz", "'" + alarm.This_rZ + "'");


            sql = sql.Replace("@ac_rn_l", "'" + alarm.Ac_rN_L + "'");
            sql = sql.Replace("@ac_re_l", "'" + alarm.Ac_rE_L + "'");
            sql = sql.Replace("@ac_rz_l", "'" + alarm.Ac_rZ_L + "'");
            sql = sql.Replace("@ac_rn_u", "'" + alarm.Ac_rN_U + "'");
            sql = sql.Replace("@ac_re_u", "'" + alarm.Ac_rE_U + "'");
            sql = sql.Replace("@ac_rz_u", "'" + alarm.Ac_rZ_U + "'");

            sql = sql.Replace("@xmno", "'" + alarm.xmno + "'");
            //判断
            string sqlExist = "select count(*) from fmos_alarmvalue_gt where name='" + alarm.NAME + "'  and  xmno='" + alarm.xmno + "'  ";

            string cont = querysql.querystanderstr(sqlExist, conn);
            if (cont == "0")
            {
                updatedb udb = new updatedb();
                int sqlCont = udb.UpdateStanderDB(sql, conn);
                return sqlCont > 0 ? true : false;
            }
            return false;

        }
        /// <summary>
        /// 预警参数更新
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns>1表示更新成功,-1表示程序出错</returns>
        public bool AlarmValueUpdate(alarmvalue alarm)
        {

            string namestr = alarm.NAME;
            string[] names = namestr.Split(',');

            string sql = @"update fmos_alarmvalue_gt set 
            name = @_name,
            this_rn = @this_rn,                               
            this_re = @this_re,
            this_rz = @this_rz,
            ac_rn_l = @ac_rn_l,
            ac_re_l = @ac_re_l,
            ac_rz_l = @ac_rz_l,
            ac_rn_u = @ac_rn_u,
            ac_re_u = @ac_re_u,
            ac_rz_u = @ac_rz_u 
            where name = @name and xmno=@xmno";
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.xmno);
            sql = sql.Replace("@_name", "'" + names[1] + "'");
            sql = sql.Replace("@this_rn", "'" + alarm.This_rN + "'");
            sql = sql.Replace("@this_re", "'" + alarm.This_rE + "'");
            sql = sql.Replace("@this_rz", "'" + alarm.This_rZ + "'");
            sql = sql.Replace("@ac_rn_l", "'" + alarm.Ac_rN_L + "'");
            sql = sql.Replace("@ac_re_l", "'" + alarm.Ac_rE_L + "'");
            sql = sql.Replace("@ac_rz_l", "'" + alarm.Ac_rZ_L + "'");
            sql = sql.Replace("@ac_rn_u", "'" + alarm.Ac_rN_U + "'");
            sql = sql.Replace("@ac_re_u", "'" + alarm.Ac_rE_U + "'");
            sql = sql.Replace("@ac_rz_u", "'" + alarm.Ac_rZ_U + "'");
            sql = sql.Replace("@name", "'" + names[0] + "'");
            sql = sql.Replace("@xmno", "'" + alarm.xmno + "'");

            updatedb udb = new updatedb();
            int cont = udb.UpdateStanderDB(sql, conn);
            return cont > 0 ? true : false;


        }
        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool AlarmValueTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select id,name,this_rn,this_re,this_rz,ac_rn_l,ac_rn_u,ac_re_l,ac_re_u,ac_rz_l,ac_rz_u from fmos_alarmvalue_gt  where   xmno='" + xmno + "'      {2}  limit  {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            ExceptionLog.ExceptionWrite(sql);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool AlarmValueTableRowsCount( int xmno,out string totalCont)
        {
            string sql = string.Format("select count(*) from fmos_alarmvalue_gt where xmno='"+xmno+"'");
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }
        /// <summary>
        /// 获取所有的预警参数
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool AlarmValueSelect(int xmno, out DataTable dt)
        {

            string sql = "select * from fmos_alarmvalue_gt where xmno='"+xmno+"' ";
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);

            dt = querysql.querystanderdb(sql, conn);
            return dt != null ? true : false;

        }
        /// <summary>
        /// 删除预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool AlarmValueDel(alarmvalue alarm)
        {

            string sql = "delete from fmos_alarmvalue_gt where name= @name  and  xmno=@xmno";
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.xmno);
            sql = sql.Replace("@name", "'" + alarm.NAME + "'");
            sql = sql.Replace("@xmno", "'" + alarm.xmno + "'");
            ExceptionLog.ExceptionWrite(sql);
                updatedb udb = new updatedb();

                int result = udb.UpdateStanderDB(sql, conn);
                PointAlarmValueDelCasc(alarm);
                return result > 0? true:false;

        }
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(alarmvalue alarm)
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update fmos_pointalarmvalue set firstAlarmName='' where xmno=@xmno and firstAlarmName=@firstAlarmName");
            //二级预警
            ls.Add("update fmos_pointalarmvalue set secondAlarmName='' where xmno=@xmno and secondAlarmName=@secondAlarmName");
            //三级预警
            ls.Add("update fmos_pointalarmvalue set thirdAlarmName='' where xmno=@xmno and thirdAlarmName=@thirdAlarmName");
            List<string> cascSql = ls;
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.xmno);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@xmno", "'" + alarm.xmno + "'");
                temp = temp.Replace("@firstAlarmName", "'" + alarm.NAME + "'");
                temp = temp.Replace("@secondAlarmName", "'" + alarm.NAME + "'");
                temp = temp.Replace("@thirdAlarmName", "'" + alarm.NAME + "'");

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(temp, conn);


            }
            return true;
        }
        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno,out string alarmValueNameStr)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select distinct(name) from fmos_alarmvalue_gt where xmno='"+xmno+"'";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            alarmValueNameStr = string.Join(";", lsFormat);
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno,string NAME,out alarmvalue model)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,NAME,this_rn,this_re,this_rz,ac_rn_l,ac_re_l,ac_rz_l,ac_rn_u,ac_re_u,ac_rz_u,xmno from fmos_alarmvalue_gt ");
            strSql.Append(" where NAME=@NAME  and  xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@NAME", OdbcType.VarChar,120),
			        new OdbcParameter("@xmno", OdbcType.Int)
                                         };
            parameters[0].Value = NAME;
            parameters[1].Value = xmno;
            model = new alarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public alarmvalue DataRowToModel(DataRow row)
        {
            NFnet_DAL.MODEL.alarmvalue model = new NFnet_DAL.MODEL.alarmvalue();
            if (row != null)
            {
                if (row["ID"] != null && row["ID"].ToString() != "")
                {
                    model.ID = int.Parse(row["ID"].ToString());
                }
                if (row["NAME"] != null)
                {
                    model.NAME = row["NAME"].ToString();
                }
                model.This_rN = Convert.ToDouble(row["This_rN"].ToString());
                model.This_rE = Convert.ToDouble(row["This_rE"].ToString());
                model.This_rZ = Convert.ToDouble(row["This_rZ"].ToString());
                model.Ac_rN_U = Convert.ToDouble(row["Ac_rN_U"].ToString());
                model.Ac_rN_L = Convert.ToDouble(row["Ac_rN_L"].ToString());
                model.Ac_rE_U = Convert.ToDouble(row["Ac_rE_U"].ToString());
                model.Ac_rE_L = Convert.ToDouble(row["Ac_rE_L"].ToString());
                model.Ac_rZ_U = Convert.ToDouble(row["Ac_rZ_U"].ToString());
                model.Ac_rZ_L = Convert.ToDouble(row["Ac_rZ_L"].ToString());
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
            }
            return model;
        }



    }
}