﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.MODEL;
using System.Data.OleDb;
using System.Data;
using SqlHelpers;
using Tool;


namespace NFnet_DAL.DAL
{
    public class administratorDAL
    {
        public accessdbse adb = new accessdbse();
        /// <summary>
        /// 人员信息添加
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public bool MemberInsertExcute(member member)
        {
            string sql = @"insert into member(
            [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel],
            [email], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],
            [createTime]
            )values(
            @userId,
            @pass,
            @role, 
            @userGroup, 
            @userName, 
            @workNo, 
            @position, 
            @tel,
            @email, 
            @zczsmc, 
            @zczsbh, 
            @zczs, 
            @sgzsmc,
            @sgzsbh,
            @sgzs,
            @unitName,
            @createTime)";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@userId", "'" + member.UserId + "'");
            sql = sql.Replace("@pass", "'" + member.Password + "'");
            sql = sql.Replace("@role", "'" + member.Role + "'");
            sql = sql.Replace("@userGroup", "'" + member.UserGroup + "'");
            sql = sql.Replace("@userName", "'" + member.UserName + "'");
            sql = sql.Replace("@workNo", "'" + member.WorkNo + "'");
            sql = sql.Replace("@position", "'" + member.Position + "'");
            sql = sql.Replace("@tel", "'" + member.Tel + "'");
            sql = sql.Replace("@email", "'" + member.Email + "'");
            sql = sql.Replace("@zczsmc", "'" + member.Zczsmc + "'");
            sql = sql.Replace("@zczsbh", "'" + member.Zczsbh + "'");
            sql = sql.Replace("@zczs", "'" + member.Zczs + "'");
            sql = sql.Replace("@sgzsmc", "'" + member.Sgzsmc + "'");
            sql = sql.Replace("@sgzsbh", "'" + member.Sgzsbh + "'");
            sql = sql.Replace("@sgzs", "'" + member.Sgzs + "'");
            sql = sql.Replace("@unitName", "'" + member.UnitName + "'");
            sql = sql.Replace("@createTime", "'" + DateTime.Now.Date + "'");
            OleDbCommand cmd = new OleDbCommand(sql, conn);
         
              int cont = cmd.ExecuteNonQuery();
              if (cont > 0)
              {
                  return true;
              }
              else {
                  return false;
              }
          
        }
        /// <summary>
        /// 人员信息更新
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public bool MemberUpdateExcute(member member)
        {
            string sql = @"update  member set
            [pass] = @pass , 
            [role] = @role, 
            [userGroup] = @userGroup, 
            [userName] = @userName, 
            [workNo] = @workNo, 
            [position] = @position, 
            [tel] = @tel, 
            [zczsmc] = @zczsmc , 
            [zczsbh] = @zczsbh, 
            [zczs] = @zczs, 
            [sgzsmc] = @sgzsmc,
            [sgzsbh] = @sgzsbh,
            [sgzs] = @sgzs,
            [email] = @email
             where
            [userId] = @userId
            ";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@userId", "'" + member.UserId + "'");
            sql = sql.Replace("@pass", "'" + member.Password + "'");
            sql = sql.Replace("@role", "'" + member.Role + "'");
            sql = sql.Replace("@userGroup", "'" + member.UserGroup + "'");
            sql = sql.Replace("@userName", "'" + member.UserName + "'");
            sql = sql.Replace("@workNo", "'" + member.WorkNo + "'");
            sql = sql.Replace("@position", "'" + member.Position + "'");
            sql = sql.Replace("@tel", "'" + member.Tel + "'");
            sql = sql.Replace("@email", "'" + member.Email + "'");
            sql = sql.Replace("@zczsmc", "'" + member.Zczsmc + "'");
            sql = sql.Replace("@zczsbh", "'" + member.Zczsbh + "'");
            sql = sql.Replace("@zczs", "'" + member.Zczs + "'");
            sql = sql.Replace("@sgzsmc", "'" + member.Sgzsmc + "'");
            sql = sql.Replace("@sgzsbh", "'" + member.Sgzsbh + "'");
            sql = sql.Replace("@sgzs", "'" + member.Sgzs + "'");
            sql = sql.Replace("@unitName", "'" + member.UnitName + "'");
            sql = sql.Replace("@createTime", "'" + DateTime.Now.Date + "'");
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            
               int cont = cmd.ExecuteNonQuery();
               if (cont > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
            
        }
        /// <summary>
        /// 人员信息获取
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public bool  MemberSelectExcute(string userId,out member outMember)
        {
            outMember = null;
            string sql = @"select [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],[email] from member where userId = @userId";
            OleDbConnection conn = adb.getconn();
            sql = sql.Replace("@userId", "'" + userId + "'");
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "userId", 
            "pass", 
            "role", 
            "userGroup", 
            "userName", 
            "workNo", 
            "position", 
            "tel", 
            "zczsmc", 
            "zczsbh", 
            "zczs", 
            "sgzsmc",
            "sgzsbh",
            "sgzs",
            "unitName",
             "email"};
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<member> lds = new List<member>();
            foreach (List<string> ls in lls)
            {
                member memberEntrl = new member
                {
                    UserId = ls[0],
                    Password = ls[1],
                    Role = ls[2],
                    UserGroup = ls[3],
                    UserName = ls[4],
                    WorkNo = ls[5],
                    Position = ls[6],
                    Tel = ls[7],
                    Zczsmc = ls[8],
                    Zczsbh = ls[9],
                    Zczs = ls[10],
                    Sgzsmc = ls[11],
                    Sgzsbh = ls[12],
                    Sgzs = ls[13],
                    UnitName = ls[14],
                    Email = ls[15]

                };
                outMember = memberEntrl;
            }
            return true;
        }
        

        public bool  MemberTableExcute(string unitName,out DataTable dt)
        {
            dt = new DataTable();
            string sql = "select * from member where unitName = @unitName";
            List<member> lm = new List<member>();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@unitName", "'" + unitName + "'");
            OleDbDataAdapter adpter = new OleDbDataAdapter(sql, conn);
            
               int cont = adpter.Fill(dt);
               if (cont > 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           
        }
    }
}