﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.MODEL;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;
using Tool;
using System.Text;

namespace NFnet_DAL.DAL
{
    public class AlarmDAL
    {
        public static database db = new database();
        /// <summary>
        /// 新增预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool AlarmValueInsert(alarmvalue alarm)
        {
            string sql = "insert into fmos_alarmvalue(name,this_rn,this_re,this_rz,ac_rn,ac_re,ac_rz,xmno)value(@name,@this_rn,@this_re,@this_rz,@ac_rn,@ac_re,@ac_rz,@xmno)";
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.Xmno);
            sql = sql.Replace("@name", "'" + alarm.Name + "'");
            sql = sql.Replace("@this_rn", "'" + alarm.This_rn + "'");
            sql = sql.Replace("@this_re", "'" + alarm.This_re + "'");
            sql = sql.Replace("@this_rz", "'" + alarm.This_rz + "'");
            sql = sql.Replace("@ac_rn", "'" + alarm.Ac_rn + "'");
            sql = sql.Replace("@ac_re", "'" + alarm.Ac_re + "'");
            sql = sql.Replace("@ac_rz", "'" + alarm.Ac_rz + "'");
            sql = sql.Replace("@xmno", "'" + alarm.Xmno + "'");
            //判断
            string sqlExist = "select count(*) from fmos_alarmvalue where name='" + alarm.Name + "'  and  xmno='" + alarm.Xmno + "'  ";

            string cont = querysql.querystanderstr(sqlExist, conn);
            if (cont == "0")
            {
                updatedb udb = new updatedb();
                int sqlCont = udb.UpdateStanderDB(sql, conn);
                return sqlCont > 0 ? true : false;
            }
            return false;

        }
        /// <summary>
        /// 预警参数更新
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns>1表示更新成功,-1表示程序出错</returns>
        public bool AlarmValueUpdate(alarmvalue alarm)
        {
            string sql = "update fmos_alarmvalue set this_rn=@this_rn,this_re=@this_re,this_rz=@this_rz,ac_rn=@ac_rn,ac_re=@ac_re,ac_rz=@ac_rz where name = @name and xmno=@xmno";
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.Xmno);
            sql = sql.Replace("@name", "'" + alarm.Name + "'");
            sql = sql.Replace("@this_rn", "'" + alarm.This_rn + "'");
            sql = sql.Replace("@this_re", "'" + alarm.This_re + "'");
            sql = sql.Replace("@this_rz", "'" + alarm.This_rz + "'");
            sql = sql.Replace("@ac_rn", "'" + alarm.Ac_rn + "'");
            sql = sql.Replace("@ac_re", "'" + alarm.Ac_re + "'");
            sql = sql.Replace("@ac_rz", "'" + alarm.Ac_rz + "'");
            sql = sql.Replace("@xmno", "'" + alarm.Xmno + "'");

            updatedb udb = new updatedb();
            int cont = udb.UpdateStanderDB(sql, conn);
            return cont > 0 ? true : false;


        }
        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool AlarmValueTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select id,name ,this_rn,this_re,this_rz,ac_rn,ac_re,ac_rz from fmos_alarmvalue  where   xmno='"+xmno+"'      {2}  limit  {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool AlarmValueTableRowsCount( int xmno,out string totalCont)
        {
            string sql = string.Format("select count(*) from fmos_alarmvalue where xmno='"+xmno+"'");
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }
        /// <summary>
        /// 获取所有的预警参数
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool AlarmValueSelect(int xmno, out DataTable dt)
        {

            string sql = "select * from fmos_alarmvalue where xmno='"+xmno+"' ";
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);

            dt = querysql.querystanderdb(sql, conn);
            return dt != null ? true : false;

        }
        /// <summary>
        /// 删除预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool AlarmValueDel(alarmvalue alarm)
        {

            string sql = "delete from fmos_alarmvalue where name= @name  and  xmno=@xmno";
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.Xmno);
            sql = sql.Replace("@name", "'" + alarm.Name + "'");
            sql = sql.Replace("@xmno", "'" + alarm.Xmno + "'");
            ExceptionLog.ExceptionWrite(sql);
                updatedb udb = new updatedb();

                int result = udb.UpdateStanderDB(sql, conn);
                PointAlarmValueDelCasc(alarm);
                return result > 0? true:false;

        }
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(alarmvalue alarm)
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update fmos_pointalarmvalue set firstAlarmName='' where xmno=@xmno and firstAlarmName=@firstAlarmName");
            //二级预警
            ls.Add("update fmos_pointalarmvalue set secondAlarmName='' where xmno=@xmno and secondAlarmName=@secondAlarmName");
            //三级预警
            ls.Add("update fmos_pointalarmvalue set thirdAlarmName='' where xmno=@xmno and thirdAlarmName=@thirdAlarmName");
            List<string> cascSql = ls;
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.Xmno);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@xmno", "'" + alarm.Xmno + "'");
                temp = temp.Replace("@firstAlarmName", "'" + alarm.Name + "'");
                temp = temp.Replace("@secondAlarmName", "'" + alarm.Name + "'");
                temp = temp.Replace("@thirdAlarmName", "'" + alarm.Name + "'");

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(temp, conn);


            }
            return true;
        }
        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno,out string alarmValueNameStr)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select distinct(name) from fmos_alarmvalue where xmno='"+xmno+"'";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            alarmValueNameStr = string.Join(";", lsFormat);
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno,string NAME,out alarmvalue model)
        {
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,NAME,This_rN,This_rE,This_rZ,Ac_rN,Ac_rE,Ac_rZ,xmno from fmos_alarmvalue ");
            strSql.Append(" where NAME=@NAME  and  xmno=@xmno ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@NAME", OdbcType.VarChar,120),
			        new OdbcParameter("@xmno", OdbcType.Int)
                                         };
            parameters[0].Value = NAME;
            parameters[1].Value = xmno;
            model = new alarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public alarmvalue DataRowToModel(DataRow row)
        {
            alarmvalue model = new alarmvalue();
            if (row != null)
            {
                if (row["ID"] != null && row["ID"].ToString() != "")
                {
                    model.Id = row["ID"].ToString();
                }
                if (row["NAME"] != null)
                {
                    model.Name = row["NAME"].ToString();
                }
                model.This_rn = row["This_rN"].ToString();
                model.This_re = row["This_rE"].ToString();
                model.This_rz = row["This_rZ"].ToString();
                model.Ac_rn = row["Ac_rN"].ToString();
                model.Ac_re = row["Ac_rE"].ToString();
                model.Ac_rz = row["Ac_rZ"].ToString();
                model.Xmno = Convert.ToInt32(row["xmno"].ToString());
            }
            return model;
        }



    }
}