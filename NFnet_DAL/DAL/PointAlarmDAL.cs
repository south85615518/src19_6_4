﻿using System;
using System.Collections.Generic;
using NFnet_DAL.MODEL;
using System.Data.Odbc;
using SqlHelpers;
using Tool;
using System.Data;

namespace NFnet_DAL.DAL
{
    public class PointAlarmDAL
    {
        public database db = new database();
        public bool PointAlarmValueInsert(pointalarmvalue alarm, string xmname)
        {
            string sql = "insert into fmos_pointalarmvalue(taskName,point_name,pointtype,firstAlarmName,secondAlarmName,thirdAlarmName,remark)value(@taskName,@point_name,@pointtype,@firstAlarmName,@secondAlarmName,@thirdAlarmName,@remark)";
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + alarm.TaskName + "'");
            sql = sql.Replace("@pointtype", "'" + alarm.Pointtype + "'");
            sql = sql.Replace("@point_name", "'" + alarm.PointName + "'");
            sql = sql.Replace("@firstAlarmName", "'" + alarm.FirstAlarmName + "'");
            sql = sql.Replace("@secondAlarmName", "'" + alarm.SecondAlarmName + "'");
            sql = sql.Replace("@thirdAlarmName", "'" + alarm.ThirdAlarmName + "'");
            sql = sql.Replace("@remark", "'" + alarm.Remark + "'");
            //判断
            string sqlExist = "select count(*) from fmos_pointalarmvalue where taskName='" + alarm.TaskName + "' and point_name = '" + alarm.PointName + "' ";

            string cont = querysql.querystanderstr(sqlExist, conn);
            if (cont == "0")
            {
                updatedb udb = new updatedb();
                int sqlCont = udb.UpdateStanderDB(sql, conn);
                if (sqlCont > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;

        }

        public bool PointAlarmValueUpdate(pointalarmvalue alarm, string xmname)
        {

            string sql = "update fmos_pointalarmvalue set firstAlarmName=@firstAlarmName,secondAlarmName=@secondAlarmName,thirdAlarmName=@thirdAlarmName,remark = @remark where point_name= @point_name and taskName =@taskName";
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + alarm.TaskName + "'");
            sql = sql.Replace("@point_name", "'" + alarm.PointName + "'");
            sql = sql.Replace("@firstAlarmName", "'" + alarm.FirstAlarmName + "'");
            sql = sql.Replace("@secondAlarmName", "'" + alarm.SecondAlarmName + "'");
            sql = sql.Replace("@thirdAlarmName", "'" + alarm.ThirdAlarmName + "'");
            sql = sql.Replace("@remark", "'" + alarm.Remark + "'");


            updatedb udb = new updatedb();
            int cont = udb.UpdateStanderDB(sql, conn);
            return cont > 0 ? true : false;


        }


        public bool PointAlarmValueEmptyInsert(string xmname)
        {

            string sql = "insert into fmos_pointalarmvalue (point_name,taskName) select distinct(point_name),taskName as point_name from fmos_studypoint where taskName=@taskName";
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + xmname + "'");

            updatedb udb = new updatedb();
            int cont = udb.UpdateStanderDB(sql, conn);
            return cont > 0 ? true : false;
        }


        public bool PointAlarmValueIncrement(string xmname)
        {
            string sql = @"insert into fmos_pointalarmvalue (point_name,taskName)
select distinct(fmos_studypoint.POINT_NAME),fmos_studypoint.TaskName from fmos_studypoint where fmos_studypoint.POINT_NAME not in (select DISTINCT(POINT_NAME) from fmos_pointalarmvalue where TaskName=@taskName) 
and fmos_studypoint.TaskName=@taskName";
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + xmname + "'");

            updatedb udb = new updatedb();
            int cont = udb.UpdateStanderDB(sql, conn);
            return cont > 0 ? true : false;
        }
        public bool PointAlarmValueDel(pointalarmvalue alarm)
        {
            string sql = "delete from  fmos_pointalarmvalue  where point_name in (@point_name) and  xmno = @xmno ";
            OdbcConnection conn = db.GetSurveyStanderConn(alarm.Xmno);
            sql = sql.Replace("@point_name", "'" + alarm.PointName + "'");
            sql = sql.Replace("@xmno", "'" + alarm.Xmno + "'");

            updatedb udb = new updatedb();
            int cont = udb.UpdateStanderDB(sql, conn);
            return cont > 0 ? true : false;

        }

        public bool PointAlarmValueDelCasc(alarmvalue alarm, string xmname)
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update fmos_pointalarmvalue set firstAlarmName='' where taskName=@taskName and firstAlarmName=@firstAlarmName");
            //二级预警
            ls.Add("update fmos_pointalarmvalue set secondAlarmName='' where taskName=@taskName and secondAlarmName=@secondAlarmName");
            //三级预警
            ls.Add("update fmos_pointalarmvalue set thirdAlarmName='' where taskName=@taskName and thirdAlarmName=@thirdAlarmName");
            List<string> cascSql = ls;
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@taskName", "'" + xmname + "'");
                temp = temp.Replace("@firstAlarmName", "'" + alarm.NAME + "'");
                temp = temp.Replace("@secondAlarmName", "'" + alarm.NAME + "'");
                temp = temp.Replace("@thirdAlarmName", "'" + alarm.NAME + "'");
                try
                {

                    ExceptionLog.ExceptionWrite("级联删除点号预警" + temp);
                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(temp, conn);

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("执行点号预警删除语句出错");
                    return false;
                    throw (ex);
                }
            }
            return true;
        }

        public bool PointAlarmValueTableLoad(string searchstring,int startPageIndex,int pageSize,int xmno ,string xmname, string colName, string sord, out DataTable dt)
        {
            ExceptionLog.ExceptionWrite("现在执行SQL语句");
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select id,point_name,pointtype,firstAlarmName,secondAlarmName,thirdAlarmName,remark from fmos_pointalarmvalue where xmno='{3}' and {4} {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring);
            ExceptionLog.ExceptionWrite(sql);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            string sql = string.Format("select id,point_name,pointtype,remark from fmos_pointalarmvalue where xmno='{3}' and {4}  {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointAlarmValueTableRowsCount(string xmname, string searchstring,int xmno ,out string totalCont)
        {
            string sql = "select count(*) from fmos_pointalarmvalue where xmno = '" +xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }



        public bool PointAlarmValueMultilUpdate(pointalarmvalue alarm, string xmname)
        {
            string sql = "update fmos_pointalarmvalue set firstAlarmName=@firstAlarmName,secondAlarmName=@secondAlarmName,thirdAlarmName=@thirdAlarmName,remark=@remark where   point_name in (@pointname) and taskName =@taskName or id = @id ";
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            sql = sql.Replace("@id", "'" + alarm.Id + "'");
            sql = sql.Replace("@pointname", "'" + alarm.PointName + "'");
            sql = sql.Replace("@taskName", "'" + alarm.TaskName + "'");
            sql = sql.Replace("@firstAlarmName", "'" + alarm.FirstAlarmName + "'");
            sql = sql.Replace("@secondAlarmName", "'" + alarm.SecondAlarmName + "'");
            sql = sql.Replace("@thirdAlarmName", "'" + alarm.ThirdAlarmName + "'");
            sql = sql.Replace("@remark", "'" + alarm.Remark + "'");


            updatedb udb = new updatedb();
            int cont = udb.UpdateStanderDB(sql, conn);
            return cont > 0 ? true : false;

        }
    }
}