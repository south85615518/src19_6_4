﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Data.OleDb;
using SqlHelpers;
using Tool;
using MySql.Data.MySqlClient;
using SqlHelpers.dbcreate;
using NFnet_DAL.MODEL;
using System.Data.Odbc;
using System.Linq;

namespace NFnet_DAL.DAL
{
    public class UnitDAL
    {
        public static accessdbse adb = new accessdbse();
        public static database db = new database();
        /// <summary>
        /// 单位机构信息添加
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public bool UnitInsertExcute(NFnet_DAL.MODEL.Unit unit)
        {
            string sql = @"insert into unit(unitName,
            pro, 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof,
            state,
            createTime,
            dbname,
            cgdbname
            )values(
            @unitName, 
            @pro, 
            @city, 
            @country, 
            @addr, 
            @tel, 
            @email, 
            @natrue, 
            @linkman, 
            @aptitude, 
            @police, 
            @taxproof,
            @state,
            @createTime,
            @dbname,
            @cgdbname
            )";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            cmd.CommandText = sql;
            cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            cmd.Parameters.Add("@pro", OleDbType.VarChar).Value = unit.Pro;
            cmd.Parameters.Add("@city", OleDbType.VarChar).Value = unit.City;
            cmd.Parameters.Add("@country", OleDbType.VarChar).Value = unit.Country;
            cmd.Parameters.Add("@adr", OleDbType.VarChar).Value = unit.Addr;
            cmd.Parameters.Add("@tel", OleDbType.VarChar).Value = unit.Tel;
            cmd.Parameters.Add("@email", OleDbType.VarChar).Value = unit.Email;
            cmd.Parameters.Add("@nature", OleDbType.VarChar).Value = unit.Natrue;
            cmd.Parameters.Add("@linkman", OleDbType.VarChar).Value = unit.Linkman;
            cmd.Parameters.Add("@aptitude", OleDbType.VarChar).Value = unit.Aptitude;
            cmd.Parameters.Add("@police", OleDbType.VarChar).Value = unit.Police;
            cmd.Parameters.Add("@taxproof", OleDbType.VarChar).Value = unit.Taxproof;
            cmd.Parameters.Add("@state", OleDbType.VarChar).Value = "未通过审核";
            cmd.Parameters.Add("@createTime", OleDbType.Date).Value = DateTime.Now.Date;
            cmd.Parameters.Add("@dbname", OleDbType.VarChar).Value = unit.DbName;
            cmd.Parameters.Add("@cgdbname", OleDbType.VarChar).Value = unit.CgdbName;
            updatedb udb = new updatedb();
            int cont =  cmd.ExecuteNonQuery();
            if (cont > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
               
        }
        /// <summary>
        /// 单位机构信息修改
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public bool UnitUpdateExcute(NFnet_DAL.MODEL.Unit unit)
        {
            string sql = @"update  unit set
            pro = @pro , 
            city = @city, 
            country = @country, 
            addr=@addr, 
            tel=@tel, 
            email=@email, 
            natrue=@natrue, 
            linkman=@linkman, 
            aptitude=@aptitude, 
            police=@police, 
            taxproof=@taxproof
             where
            unitName = @unitName
            ";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand cmd = new OleDbCommand(sql, conn);

            sql = sql.Replace("@unitName", "'" + unit.UnitName + "'");
            sql = sql.Replace("@pro", "'" + unit.Pro + "'");
            sql = sql.Replace("@city", "'" + unit.City + "'");
            sql = sql.Replace("@country", "'" + unit.Country + "'");
            sql = sql.Replace("@addr", "'" + unit.Addr + "'");
            sql = sql.Replace("@tel", "'" + unit.Tel + "'");
            sql = sql.Replace("@email", "'" + unit.Email + "'");
            sql = sql.Replace("@natrue", "'" + unit.Natrue + "'");
            sql = sql.Replace("@linkman", "'" + unit.Linkman + "'");
            sql = sql.Replace("@aptitude", "'" + unit.Aptitude + "'");
            sql = sql.Replace("@police", "'" + unit.Police + "'");
            sql = sql.Replace("@taxproof", "'" + unit.Taxproof + "'");
            cmd.CommandText = sql;
            updatedb udb = new updatedb();
            int cont = cmd.ExecuteNonQuery();
            if (cont > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
               
        }


        public bool  UnitSelectExcute(string unitName,out NFnet_DAL.MODEL.Unit outUnit )
        {
            outUnit = null;
            string sql = @"select pro , 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof,state,dbname from unit where unitName = @unitName";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@unitName", "'" + unitName + "'");
            List<string> ls = querysql.queryaccesslist(sql);

            foreach (string str in ls)
            {
                string[] vals = str.Split(',');
                NFnet_DAL.MODEL.Unit unitEntrl = new NFnet_DAL.MODEL.Unit
                {
                    UnitName = unitName,
                    Pro = vals[0],
                    City = vals[1],
                    Country = vals[2],
                    Addr = vals[3],
                    Tel = vals[4],
                    Email = vals[5],
                    Natrue = vals[6],
                    Linkman = vals[7],
                    Aptitude = vals[8],
                    Police = vals[9],
                    Taxproof = vals[10],
                    State = vals[11],
                    DbName = vals[12]

                };
                outUnit = unitEntrl;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 机构信息列表
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public bool UnitSelectListExcute(NFnet_DAL.MODEL.Unit unit,out List<NFnet_DAL.MODEL.Unit> lunit)
        {
            List<NFnet_DAL.MODEL.Unit> lu = new List<NFnet_DAL.MODEL.Unit>();
            string sql = @"select
            unitName,
            pro , 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof from unit ";
            OleDbConnection conn = adb.getconn();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            //DataTable dt = 
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = {"unitName",
            "pro ", 
            "addr", 
            "aptitude",
            "city", 
            "country", 
            "email", 
            "linkman",
            "natrue", 
            " police",
            " taxproof",
            "tel", 
            };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);

            foreach (List<string> ls in lls)
            {
                NFnet_DAL.MODEL.Unit unitEntrl = new NFnet_DAL.MODEL.Unit
                {
                    UnitName = ls[0],
                    Pro = ls[1],
                    Addr = ls[2],
                    Aptitude = ls[3],
                    City = ls[4],
                    Country = ls[5],
                    Email = ls[6],
                    Linkman = ls[7],
                    Natrue = ls[8],
                    Police = ls[9],
                    Taxproof = ls[10],
                    Tel = ls[11]
                };
                lu.Add(unitEntrl);
            }
            lunit = lu;
            return true;
        }
        public bool UnitTableExcute(Unit unit,out DataTable dt)
        {

            string sql = @"select * from unit where unitName <>'thshd6@#$#@$#@uuudfiu%$'  ";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            dt = new DataTable();
            OleDbDataAdapter adpter = new OleDbDataAdapter(sql, conn);

            int cont = adpter.Fill(dt);

            if (cont > 0) return true;
            return false;

           
        }

        /// <summary>
        /// 数据库列表加载
        /// </summary>
        /// <returns></returns>
        public bool  DbListExcute(out List<string> ls )
        {
            string sql = @"SELECT SCHEMA_NAME 
FROM information_schema.SCHEMATA"; 
            OdbcConnection conn = db.GetXmConn("information_schema");
            ls = querysql.querystanderlist(sql, conn);
            if (ls == null) return false;
            return true;
        }

        public bool ProcessTriggle(string dbname,List<string> ls)
        {
           
                updatedb udb = new updatedb();
                OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
                var droptriggercmd = ls[0];
                udb.UpdateStanderDB(ls[0], conn);
                var triggercontext = ls.Where(p => ls.IndexOf(p) > 0).ToList();
                udb.UpdateStanderDB(string.Format(string.Join(" ", triggercontext), string.Format("{0}{1}", "cg", dbname)), conn);
                return true;
           
        }
        



        public bool UnitDbCreate(string dbname, string path,out string mssg)
        {

            try
            {
                dbcreate dbc = new dbcreate();
                dbc.createdb(dbname);
                //string sql = " SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'fmosdata'";


                MySqlConnection conn = TargetBaseLink.GetMysqlConnect(dbname);
                FileInfo file = new FileInfo(path);  //filename是sql脚本文件路径。  
                string sql = file.OpenText().ReadToEnd();
                MySqlScript script = new MySqlScript(conn);
                script.Query = sql;
                int count = script.Execute();
                mssg = string.Format("创建数据库{0}成功!", dbname);
                ExceptionLog.ExceptionWrite(mssg);
                return true;
            }
            catch(Exception ex)
            {
                mssg = string.Format("创建数据库{0}出错!错误信息："+ex.Message, dbname);
                ExceptionLog.ExceptionWrite(mssg);
                return false;
            }


        }

        public bool UnitNameExist(string unitName)
        {
            string sql = "select count(*) from unit where unitName='"+unitName+"' ";
            string cont = querysql.queryaccessdbstring(sql);
            return Convert.ToInt32(cont) > 0 ? true : false;
        }

    }
}