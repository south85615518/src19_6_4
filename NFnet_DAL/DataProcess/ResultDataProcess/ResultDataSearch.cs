﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL.DataProcess.ResultDataProcess;
using System.Data.Odbc;
using System.Data;

namespace NFnet_DAL.ResultDataProcess.DataProcess
{
    public class ResultDataSearch
    {
        public database db = new database();
        public bool List(ResultDataSearchCondition model, out string jsondata)
        {
            string order = model.ColName == "id" ? "" : "order by " + model.ColName + "  " + model.Sord;
            OdbcConnection conn = db.GetStanderConn(model.Xmname);
            string sql = string.Format("select count(*) from fmos_orgldata where taskName='{3}' and CYC between {0} and {1}  {2}", model.StartCyc, model.EndCyc, order, model.Xmname);
            string totalCont = querysql.querystanderstr(sql, conn);
            sql = string.Format("select  point_name,CYC,point_har,point_var,sd,N,E,Z,dN,dE,dZ,search_Time from fmos_orgldata where taskName='{5}' and CYC between {0} and {1}  {4} limit {2},{3}", model.StartCyc, model.EndCyc, (model.PageIndex - 1) * model.Rows, model.PageIndex * model.Rows, order, model.Xmname);
            DataTable dt = new DataTable();
            dt = querysql.querystanderdb(sql, conn);
            DataView dv = new DataView(dt);
            List<string> rows = new List<string>();
            int i = 0;
            foreach (DataRowView drv in dv)
            {

                List<string> row = new List<string>();
                for (i = 0; i < dt.Columns.Count; i++)
                {
                    row.Add("\"" + drv[i].ToString() + "\"");
                }
                string temp = string.Join(",", row);
                rows.Add("{\"cell\":[" + temp + "]}");
            }
            int totalPageNum = Int32.Parse(totalCont) / model.Rows == 0 ? Int32.Parse(totalCont) / model.Rows : Int32.Parse(totalCont) / model.Rows + 1;
            string rowstr = string.Format("[{0}]", string.Join(", ", rows));
            string template = "{\"page\":" + model.PageIndex + ",\"total\":" + totalPageNum + ",\"records\":" + totalCont + ",\"rows\":" + rowstr + ",\"userdata\":{\"amount\":3330,\"tax\":342,\"total\":3564,\"name\":\"Totals:\"}}";


            jsondata = template;
            return true;
        }
    }
}