﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.XmInfo.pub;

namespace NFnet_Interface.XmInfo
{
    public class ProcessLayoutBaseMapJclxList
    {
        public ProcessLayoutBLL processLayoutBLL = new ProcessLayoutBLL();
        public List<string> LayoutBaseMapJclxList(int xmno,int basemapid,out string mssg)
        {
            var processBasemapJclxListModel = new ProcessLayoutBLL.ProcessBasemapJclxListModel(xmno,basemapid);
            if (processLayoutBLL.ProcessBasemapJclxList(processBasemapJclxListModel, out mssg))
            {
                return processBasemapJclxListModel.jclxlist;
            }
            return new List<string>();
        }
    }
}
