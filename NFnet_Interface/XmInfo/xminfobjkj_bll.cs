﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using NFnet_DAL;
using NFnet_MODAL;
using System.Text;

namespace NFnet_BLL
{
    public class xminfobjkj_bll : System.Web.UI.Page
    {
        public void FillDropDownList(DropDownList ddl, string userId)
        {
            member member = new member { UserId = userId };
            member = member.GetMember();
            Sql_DAL_E dal = new Sql_DAL_E();
            List<member> ls = dal.UnitMemberExcute(member.UnitName);
            ddl.DataSource = ls;
            ddl.DataTextField = "UserName";
            ddl.DataBind();

        }
        public void Super_FillDropDownList(DropDownList ddl, string unitName)
        {

            Sql_DAL_E dal = new Sql_DAL_E();
            List<member> ls = dal.UnitMemberExcute(unitName);
            ddl.DataSource = ls;
            ddl.DataTextField = "UserName";
            ddl.DataBind();

        }
        public void FillDropDownList(DropDownList ddl)
        {

            Unit unit = new Unit();
            Sql_DAL_E dal = new Sql_DAL_E();
            List<Unit> ls = unit.GetUnitList();
            ddl.DataSource = ls;
            ddl.DataTextField = "unitName";
            ddl.DataBind();
            ddl.Items[0].Selected = true;

        }
        public void FillPanel(string xmname,string role,TextBox tb)
        {

            StringBuilder strSql = new StringBuilder();
            switch (role)
            {
                
                case "监测人员":
                    strSql.Append("select [unitmember._call] from xmconnect,xm_member,unitmember where xmconnect.xmno = xm_member.xmno and xm_member.memberno = unitmember.memberno and xmconnect.xmname='"+xmname+"'  ");
                    break;
                case "监督人员":
                    strSql.Append("select monitormember.name+'['+monitormember.unitcall+']' from xmconnect,xm_monitor,monitormember where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = monitormember.userno and xmconnect.xmname='"+xmname+"' ");
                    break;
            }

            List<string> ls = querysql.queryaccesslist(strSql.ToString());



            tb.Text = string.Join(",",ls);
        }
        public void PanelFillList(List<string> ls, Panel pl, string cssClass,string msSelectId)
        {
            
            foreach (string name in ls)
            {
                if (name == "" || name == null) continue;
                Panel plson = new Panel();//div
                //html.Add("<div class='responsePeoples'><span>"+name+"</span><i onclick='deleteClass(this,\'responseMan\')'>×</i></div>");         
                plson.CssClass = cssClass;
                Label lb = new Label();
                lb.Text = name;
                HyperLink hl = new HyperLink();
                hl.ID = pl.ID+"_" + ls.IndexOf(name);
                hl.NavigateUrl = "javascript:deleteClassId('" + hl.ID + "','" + msSelectId + "')";
                hl.CssClass = "close";
                hl.Text = "×";
                plson.Controls.Add(lb);
                plson.Controls.Add(hl);
                pl.Controls.Add(plson);
                //plson_1.
            }
            // string htmlstr = string.Join("",html);

        }
        public void CreateUnit(Table tab, string xmname)
        {
            try
            {
                string sql = "select buildInstrutment from xmconnect where xmname='" + xmname + "'";
                string creatUnitStr = querysql.queryaccessdbstring(sql);
                string[] units = creatUnitStr.Split('|');
                foreach (string unit in units)
                {
                    string[] attrs = unit.Split(',');
                    TableRow tr = new TableRow();
                    TableCell tc_0 = new TableCell();
                    tc_0.Controls.Add(new Label { Text = "单位名称:" });
                    TextBox tb0 = new TextBox();
                    tb0.Text = attrs[0];
                    tc_0.Controls.Add(tb0);
                    TableCell tc_1 = new TableCell();
                    tc_1.Controls.Add(new Label { Text = "负责人:" });
                    TextBox tb1 = new TextBox();
                    tb1.Text = attrs[1];
                    tc_1.Controls.Add(tb1);
                    TableCell tc_2 = new TableCell();
                    tc_2.Controls.Add(new Label { Text = "联系电话:" });
                    TextBox tb2 = new TextBox();
                    tb2.Text = attrs[2];
                    tc_2.Controls.Add(tb2);
                    tr.Controls.Add(tc_0);
                    tr.Controls.Add(tc_1);
                    tr.Controls.Add(tc_2);
                    tab.Controls.Add(tr);
                }
            }
            catch(Exception ex)
            {
                ExceptionLog.ExceptionWrite("xminfobjkj_bll:"+ex.Message);
            }
        }
    }
}