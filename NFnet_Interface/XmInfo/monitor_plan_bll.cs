﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
namespace NFnet_BLL
{
    public class monitor_plan_bll
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pl">文件块</param>
        /// <param name="path">文件夹路径</param>
        public void FileListLoad(Panel pl ,string path )
        {
            string dirPath = path;
            List<string> fileNameList = new List<string>(); 
            //if (!Directory.Exists(dirPath)) { Directory.CreateDirectory(dirPath); return; }
            DirectoryInfo folder = new DirectoryInfo(dirPath);
            //string fileName = Path.GetFileNameWithoutExtension(path);
            //string fileNameKey = fileName.IndexOf("_Time_") != -1 ? fileName.Substring(0, fileName.IndexOf("_Time_")) : fileName;
            string ext = "";
            if (!Directory.Exists(dirPath)) return;
            foreach (FileInfo fi in folder.GetFiles())
            {
                //Style style = new Style { };
                //最外层div
                Panel Parent = new Panel
                {

                    CssClass = "file_class_1 ",
                    



                };
                
                //里层第一个div
               // pl.Controls.Add(Parent);
                Panel plson1 = new Panel
                {
                    CssClass = "file_class_2 clearfix"
                };
                //第一个div的儿子
                Panel plgrade = new Panel {
                    CssClass = "file_class_3"
                };
                Label lb = new Label { CssClass = "file_c", Text = Path.GetFileNameWithoutExtension(fi.FullName) };
                //里层第二个div
                Panel plson2 = new Panel
                {
                    CssClass = "hide_opr"
                };
                ext = fi.Extension;
                string cssClass =FileIconSet(ext);
                //为每个文件创建下载/删除/查看链接
                Image img = new Image { 
                    ImageUrl = "../文件管理/file_format.png",
                    CssClass = cssClass,
                     
                     
                };

                HyperLink hl = new HyperLink { CssClass = "downloap" ,Text = "下载", NavigateUrl = string.Format("../文件下载/loadwin.aspx?url={0}",fi.FullName) };
                
                HyperLink h2 = new HyperLink { CssClass = "delete" ,Text = "删除",   NavigateUrl = "#" };

                HyperLink h3 = new HyperLink { CssClass = "check", Text = "查看", NavigateUrl = "javascript:ImgView('" + fi.FullName.Replace("\\", "/") + "')", Enabled = IsFileViewAble(fi.Extension) };
                plgrade.Controls.Add(img);
                plson1.Controls.Add(plgrade);
                plson1.Controls.Add(lb);
                plson2.Controls.Add(hl);
                plson2.Controls.Add(h2);
                plson2.Controls.Add(h3);
                Parent.Controls.Add(plson1);
                Parent.Controls.Add(plson2);
                pl.Controls.Add(Parent);
            }
            /*
              <div class="file_class_1" onmousemove="Show(this);" onmouseout="Hide(this);">
                    <div class="file_class_2 clearfix">
                        <div class="file_class_3">
                            <img src="../img/file_format.png" alt="" />
                        </div>
                        <span class="file_c">txt.doc</span>
                    </div>
                    <div class="hide_opr" style="display:none;">
                        <a class="check" href="#">查看</a>
                        <a class="downloap" href="#">下载</a>
                        <a class="delete" href="#">删除</a>
                    </div>
                </div>
             */

        }
        //根据文件的类型设置文件是否可直接查看
        public bool IsFileViewAble(string ext)
        {
            if (ext == ".txt" || ext == ".png" || ext == ".jpg" || ext == ".jpeg" || ext == ".gif") return true;
            return false;
        }
        //根据文件扩展名选择文件图标
        public string FileIconSet(string ext)
        { 
            switch(ext)
            {
                case ".txt": return "txtClass";
                case ".xlsx":
                case ".xls": 
                             return "excelClass";
                case ".doc":
                case ".docx": 
                    return "wordClass";
                //图片
                case ".png":
                case ".jpg":
                case ".jpeg":
                case ".gif": return "imgClass";
                case ".zip": return "zipClass";
                //数据库
                case ".psc":
                case ".mdb":
                    return "dbClass";
                default: return "othersClass";
            }
        }
    
    }
}