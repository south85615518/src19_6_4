﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;

namespace NFnet_BLL
{
    public class xmlbkj_bll : System.Web.UI.Page
    {
        public static int _i = 0;
        public accessdbse adb = new accessdbse();
        public void filldb(GridView gv, string unitName, Label currentRecord, Label totalRecord)
        {
            OleDbConnection conn = adb.getconn();
           
            string sql = "";
            
                sql = "SELECT * from xmconnect where jcdw = '" + unitName + "'";
            
         
            OleDbDataAdapter oad = new OleDbDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            oad.Fill(dt);
            gv.DataSource = dt;
            gv.DataBind();
            recordNumSet(currentRecord, totalRecord, gv, dt.Rows.Count);
        }
        /// <summary>
        /// 为表格条件鼠标读数事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void GridView1_RowDataBound_bll(object sender, GridViewRowEventArgs e)
        {
            //int _i = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("id", _i.ToString());
                e.Row.Attributes.Add("onKeyDown", "SelectRow();");
                e.Row.Attributes.Add("onMouseOver", "MarkRow(" + _i.ToString() + ");");
                e.Row.Attributes.Add("onMouseLeave", "Markdel(" + _i.ToString() + ");");
                _i++;
            }
        }
        public void GridView_RowCommand_bll(object sender, GridViewCommandEventArgs e,string userId,Label currentRecord, Label totalRecord)
        {
            GridView gv = sender as GridView;
            TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
            tb.Text = (Int32.Parse(tb.Text) - 1).ToString();
            if (e.CommandName == "Page")
            {
                //首页
                if (e.CommandArgument.ToString() == "First")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    tb.Text = "1";
                    gv.PageIndex = 0;

                }
                else if (e.CommandArgument.ToString() == "Next")
                {


                    gv.PageIndex = (Int32.Parse(tb.Text) + 1) > gv.PageCount ? gv.PageCount : Int32.Parse(tb.Text) + 1;
                    tb.Text = gv.PageIndex.ToString();

                }
                else if (e.CommandArgument.ToString() == "-2")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = Int32.Parse(tb.Text) > gv.PageCount ? gv.PageCount : Int32.Parse(tb.Text) < 0 ? 0 : Int32.Parse(tb.Text);
                    tb.Text = gv.PageIndex.ToString();


                }
                else if (e.CommandArgument.ToString() == "Last")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = gv.PageCount;
                    tb.Text = gv.PageCount.ToString();

                }
                else if (e.CommandArgument.ToString() == "Prev")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = (Int32.Parse(tb.Text) - 1) < 0 ? 0 : Int32.Parse(tb.Text) - 1;
                    tb.Text = gv.PageIndex.ToString();

                }
                //获取GV的ID
                string id = gv.ClientID;
                switch (id)
                {
                    case "GridView1": filldb(gv,userId,currentRecord,totalRecord);
                        break;


                }
            }
        }

        public void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        public void recordNumSet(Label currentRecord, Label totalRecord, GridView gv, int totalCont)
        {
            int pageSize = gv.PageSize;
            int pageIndex = gv.PageIndex;
            totalRecord.Text = totalCont.ToString();
            //如果是最后一页
            currentRecord.Text = pageIndex >= gv.PageCount - 1 ? pageSize * pageIndex + 1 + "-" + totalCont.ToString() : pageSize * pageIndex + 1 + "-" + (pageSize * (pageIndex + 1)).ToString();
        }
    }
}