﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;

namespace NFnet_Interface.UserProcess
{
    public class ProcessDTUPortXm
    {
        public ProcessDTUXmPortBLL processDTUXmPortBLL = new ProcessDTUXmPortBLL();
        public Authority.Model.DTUXmPort DTUPortXm(int port,out string mssg)
        {
            var processDTUXmPortBLLModel = new ProcessDTUXmPortBLL.ProcessDTUPortXmBLLModel(port);
            if (processDTUXmPortBLL.ProcessDTUPortXmBLL(processDTUXmPortBLLModel, out mssg))
            {
                return processDTUXmPortBLLModel.model;
            }
            return new Authority.Model.DTUXmPort();
        }
       
    }
}
