﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.ProjectInfo;

namespace NFnet_Interface.UserProcess
{
    public class ProcessXmList
    {
        public static ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public List<string> XmList(out string mssg)
        {
            List<string> ls =null;
            if (processXmBLL.ProcessXmList(out ls, out mssg))
            {
                return ls;
            }
            return new List<string>();
        }
    }
}
