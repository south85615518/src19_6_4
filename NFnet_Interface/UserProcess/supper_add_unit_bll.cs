﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using NFnet_DAL;

namespace NFnet_BLL
{
    public class supper_add_unit_bll : System.Web.UI.Page
    {
        public void save_Click_bll(Unit unit,Label mess)
        {
            try
            {
                string result = unit.SaveUnitInfo();
                if (result.IndexOf("由于将在索引") != -1)
                {
                    result = "公司名称已经存在！";
                }
                else if (result == "success")
                {

                    List<string> ls = unit.GetProjectNameList();
                    //if(ls == null)
                    foreach (string projectName in ls)
                    {
                        DirectoryCreate("../../文件管理/全部文件/" + unit.UnitName + "/" + projectName + "/" + "全站仪");
                        DirectoryCreate("../../文件管理/全部文件/" + unit.UnitName + "/" + projectName + "/" + "GPS");
                        DirectoryCreate("../../文件管理/全部文件/" + unit.UnitName + "/" + projectName + "/" + "水位");
                    }
                    unit.ProjectInitSet(ls);
                    result = "保存成功！";
                }
                mess.Text = result;
            }
            catch (Exception ex){ 
            }
        }
        public void DirectoryCreate(string path)
        {
            if (!Directory.Exists( Server.MapPath(path)))
            {
                Directory.CreateDirectory(Server.MapPath(path));
            }
        }
        public void DataBaseLoad_bll(DropDownList DropDownList2, DropDownList DropDownList3,TextBox tb,Label dbLabel)
        {
            if (DropDownList2.Text == "指定已有数据库")
            {
                DropDownList3.Style.Add("display","inline");
                tb.Style.Add("display","none");
                dbLabel.Style.Add("display", "none");
            }
            else if (DropDownList2.Text == "创建新数据库")
            {
                DropDownList3.Style.Add("display", "none");
                tb.Style.Add("display", "inline");
                dbLabel.Style.Add("display", "inline");
            }
        }
        public void DataBaseLoad_bll(DropDownList DropDownList3)
        {
          
                Sql_DAL_E dal = new Sql_DAL_E();
                List<string> ls = dal.DbListExcute();
                DropDownList3.DataSource = ls;
                DropDownList3.DataBind();
                //DropDownList3.Enabled = true;

          
        }
        public int dbcreate(Unit unit, string dbname, DropDownList DropDownList3,string path)
        {
            if (!DropDownList3.Items.Contains(new ListItem(dbname)))
            { 
                return Int32.Parse(unit.UnitDbCreate(dbname,path)); 
                //return 0; 
            }
            else
            {
                return 0;
            }
        }
    }
}