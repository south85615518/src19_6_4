﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;

namespace NFnet_Interface.UserProcess
{
    public class ProcessFixedInclinometerXmList
    {
        public ProcessDTUXmPortBLL processDTUXmPortBLL = new ProcessDTUXmPortBLL();
        public List<string> FixedInclinometerXmList(out string mssg)
        {
            List<string> dtuxmList = new List<string>();
            if (processDTUXmPortBLL.ProcessFixedInclinometerXmList(out dtuxmList, out mssg))
            {
                return dtuxmList;

            }
            return new List<string>();
        }
    }
}
