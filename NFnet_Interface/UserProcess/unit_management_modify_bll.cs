﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NFnet_BLL
{
    public class unit_management_modify_bll: System.Web.UI.Page
    {
        public void save_Click_bll(Unit unit,Label mess)
        {
            string result = unit.UpdateUnitInfo();
            if (result.IndexOf("由于将在索引") != -1)
            {
                result = "公司名称已经存在！";
            }
            else if (result == "success") result = "保存成功！";
            mess.Text = result;
        }
        
    }

}