﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;

namespace NFnet_Interface.UserProcess
{
    public class ProcessClientmnameXmnoGet
    {
        public ProcessXmReference processXmReference = new ProcessXmReference();
        public int ClientmnameXmnoGet(string clientxmname,out string mssg)
        {
            var model = new ProcessXmReference.ProcessClient_xmnameXmnoGetModel(clientxmname);
            if (processXmReference.ProcessClient_xmnameXmnoGet(model, out mssg))
                return model.xmno;
            return -1;

        }
    }
}
