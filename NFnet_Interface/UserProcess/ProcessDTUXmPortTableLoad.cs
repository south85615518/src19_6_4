﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.UserProcess;

namespace NFnet_Interface.UserProcess
{
    public class ProcessDTUXmPortTableLoad
    {
        public ProcessDTUXmPortBLL processDTUXmPortBLL = new ProcessDTUXmPortBLL();
        public DataTable DTUXmPortTableLoad(out string mssg)
        {
            DataTable dt = new DataTable();
            if (processDTUXmPortBLL.ProcessDTUXmPortTableLoad(out dt, out mssg))
            {
                return dt;
            }
            return null;
        }
    }
}
