﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.UserProcess;

namespace NFnet_Interface.UserProcess
{
    public class ProcessXmListLoad
    {
        public static ProcessAdministratrorBLL administratorBLL = new ProcessAdministratrorBLL();
        public static ProcessMonitorBLL monitorBLL = new ProcessMonitorBLL();
        public static ProcessSuperviseBLL superviseBLL = new ProcessSuperviseBLL();
        public List<string> XmListLoad(string userID, Role role, out string mssg)
        {
            mssg = "";
            var model = new ProcessPeopleXmnameListCondition(userID);
            switch (role)
            {

                case Role.administratrorModel:


                    if (!administratorBLL.ProcessAdministratorXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        return model.xmnameList;
                    }
                    break;
                case Role.monitorModel:
                    if (!monitorBLL.ProcessMonitorXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        return model.xmnameList;
                    }
                    break;
                case Role.superviseModel:
                    if (!superviseBLL.ProcessSuperviseXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        return model.xmnameList;
                    }
                    break;

            }
            return new List<string>();

        }
    }
}
