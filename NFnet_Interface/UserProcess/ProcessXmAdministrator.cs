﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;
using Authority.Model;

namespace NFnet_Interface.UserProcess
{
    public class ProcessXmAdministrator
    {
        public ProcessAdministratrorBLL processAdministratrorBLL = new ProcessAdministratrorBLL();
        public member XmAdministrator(string xmname,out string mssg)
        {
            var processXmAdministatorModel = new ProcessAdministratrorBLL.ProcessXmAdministatorModel(xmname);
            if (ProcessAdministratrorBLL.ProcessXmAdministator(processXmAdministatorModel, out mssg))
            {
                return processXmAdministatorModel.model;
            }
            return null;
        }
    }
}
