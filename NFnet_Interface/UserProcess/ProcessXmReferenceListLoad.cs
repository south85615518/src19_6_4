﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;

namespace NFnet_Interface.UserProcess
{
    public class ProcessXmReferenceListLoad
    {
        public ProcessXmReference processXmReference = new ProcessXmReference();
        public List<string> XmReferenceListLoad(int xmno,out string mssg)
        {
            var model = new ProcessXmReference.ProcessXmReferenceListLoadModel(xmno);
            if (processXmReference.ProcessXmReferenceListLoad(model, out mssg))
                return model.client_xmnamelist;
            return new List<string>();
        }
    }
}
