﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using NFnet_DAL;
using NFnet_MODAL;
using System.Text;
using NFnet_BLL.LoginProcess;
using SqlHelpers;

namespace NFnet_BLL
{
    public class load_bll : System.Web.UI.Page
    {
        public static accessdbse adb = new accessdbse();
        public static int _i, _j = 0;
        public static database db = new database();
        public static load_bll bll = new load_bll();
        //读取项目信息
        public void filldb(string id, Role role,Panel pl)
        {

            StringBuilder strSql = new StringBuilder();
            switch (role)
            {
                case Role.superAdministratrorModel:
                    strSql.Append("select * from xmconnect");
                    break;
                case Role.administratrorModel:
                    strSql.Append("select * from xmconnect where createman='"+id+"' ");
                    break;
                case Role.monitorModel:
                    strSql.Append("select xmconnect.* from xmconnect,xm_member where xmconnect.xmno =xm_member.xmno and xm_member.memberno = " + id + " order by xmconnect.xmno ");
                    break;
                case Role.superviseModel:
                    strSql.Append("select xmconnect.* from xmconnect,xm_monitor where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = " + id + " order by xmconnect.xmno ");
                    break;
            }
            //querysql query = new querysql();
            //DataTable dt = query.querytaccesstdb(strSql.ToString());
            //return dt;
            filldb(strSql.ToString(),pl,role);
        }

        //按照SQL语句查询表
        public int filldb(string sql, Panel pl,string role)
        {

            OleDbConnection conn = adb.getconn();
            //根据用户名先查取范围
            //根据范围查询项目
            OleDbDataAdapter ods = new OleDbDataAdapter(sql, conn);
            conn.Close();
            DataTable dt = new DataTable();
            conn.Close();
            ods.Fill(dt);
            ProjectListLoad(dt, pl,role);
            return 0;
        }
        public void multiKeySearch_bll(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area, Panel projectList)
        {
            member member = new member { UserId = userID };
            member = member.GetMember();
            string unitName = member.UnitName;
            string sql = "";
            if (unitName == "")
            {
                try
                {
                    //管理员的工程查询
                    string safeLevel = gcjb.TrimEnd() == "不限" ? " 1=1 and " : " safeLevel= '" + gcjb.TrimEnd() + "' and  ";
                    gclx = gclx.TrimEnd() == "不限" ? " 1=1 and " : " xmType ='" + gclx.TrimEnd() + "' and ";
                    string gcName = gcname.TrimEnd() == "" ? " 1=1 and" : " xmname like '%" + gcname.TrimEnd() + "%' and ";
                    pro = pro.TrimEnd() == "" ? " 1=1 and" : " provincial= '" + pro + "'  and  ";
                    city = city.TrimEnd() == "" ? " 1=1 and" : " cities= '" + city + "' and ";
                    area = area.TrimEnd() == "" ? " 1=1 " : " countries= '" + area + "'";
                    sql = "select '" + userID + "' as userID,'\"'+xmname+'\"' as xmno,* from xmconnect where  " + safeLevel + gclx + gcName + pro + city + area;
                    filldb(sql, projectList,"");

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex.Message);

                }
            }
            else
            {
                try
                {
                    //管理员的工程查询
                    string safeLevel = gcjb.TrimEnd() == "不限" ? " 1=1 and " : " safeLevel='" + gcjb.TrimEnd() + "' and  ";
                    gclx = gclx.TrimEnd() == "不限" ? " 1=1 and " : " xmType = '" + gclx.TrimEnd() + "' and ";
                    string gcName = gcname.TrimEnd() == "" ? " 1=1 and" : " xmname like '%" + gcname.TrimEnd() + "%' and ";
                    pro = pro.TrimEnd() == "" ? " 1=1 and" : " provincial= '" + pro + "'  and  ";
                    city = city.TrimEnd() == "" ? " 1=1 and" : " cities= '" + city + "' and ";
                    area = area.TrimEnd() == "" ? " 1=1 " : " countries= '" + area + " '";
                    sql = "select '" + userID + "' as userID,'\"'+xmname+'\"' as xmno,* from xmconnect where jcdw='" + unitName + "' and  " + safeLevel + gclx + gcName + pro + city + area;
                    filldb(sql, projectList,"");

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex.Message);

                }
            }

        }
        public void ProjectListLoad(DataTable dt, Panel projectList,string role)
        {
            DataView dv = new DataView(dt);
            projectList.Controls.Clear();
            string url = "";
            if (role == "监督人员")
            {
                url = "../框架页/gbframe.aspx";
            }
            else if(role == "监测人员")
            {
                url = "../框架页/monitor_frame.aspx";
            }
            foreach (DataRowView drv in dv)
            {
                HyperLink hl = new HyperLink();
                hl.Text = drv["xmname"].ToString();
                hl.NavigateUrl = "javascript:TopPageRedirct('"+url+"?xmname=" + drv["xmname"].ToString() + "&xmid=" + drv["xmno"].ToString() + "')";
                projectList.Controls.Add(hl);
                hl.CssClass = "brClass1";
                _j++;
                HyperLink h2 = new HyperLink();
                h2.Text = "地址:" + drv["xmaddress"].ToString();
                h2.NavigateUrl = "javascript:info(" + drv["jd"] + "," + drv["wd"] + ",'" + drv["xmname"].ToString() + "','" + drv["type"].ToString() + "','" + drv["jcpgStartDate"].ToString() + "')";
                projectList.Controls.Add(h2);
                h2.CssClass = "brClass2";
                TextBox tb = new TextBox();
                tb.CssClass = "posAddress";
                tb.Text = drv["xmname"].ToString() + "," + drv["jd"].ToString() + "," + drv["wd"].ToString();
                tb.Style.Add("display", "none");
                projectList.Controls.Add(tb);
            }
        }
        //条件替换
        public string ConditionStrReplace(string conditionstr)
        {
            if (conditionstr.IndexOf("省份") != -1 || conditionstr.IndexOf("城市") != -1 || conditionstr.IndexOf("地区") != -1)
            {
                return "";
            }
            else
            {
                return conditionstr;
            }
        }

    }
}