﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Other;
using System.Data;
using NFnet_BLL.LoginProcess;

namespace NFnet_Interface.Other
{
    public class ProcessStanderDBLoad
    {
        public ProcessComBLLCom comBLLcom = new ProcessComBLLCom();
        public DataTable StanderDBLoad(string sql, string xmname, Role role, bool tmpRole, out string mssg)
        {
            var model = new QuerystanderdbModel(sql, xmname);
            var standerDBLoadModel = new ProcessComBLLCom.StanderDBLoadModel(model, role, tmpRole);
            if (comBLLcom.Processquerystanderdb(standerDBLoadModel, out mssg))
            {
                return standerDBLoadModel.model.dt;
            }
            return null;
            //if(comBLLcom.Processquerystanderdb())
        }
        public DataTable StanderMDBLoad(string sql, string xmname, Role role, bool tmpRole, out string mssg)
        {
            var model = new QuerystanderdbModel(sql, xmname);
            var standerDBLoadModel = new ProcessComBLLCom.StanderDBLoadModel(model, role, tmpRole);
            if (comBLLcom.Processquerystanderdb(standerDBLoadModel, out mssg))
            {
                return standerDBLoadModel.model.dt;
            }
            return null;
            //if(comBLLcom.Processquerystanderdb())
        }
    }
}
