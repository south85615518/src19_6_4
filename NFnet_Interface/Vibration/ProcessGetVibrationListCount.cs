﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vibration2.Model;
namespace NFnet_Interface.Vibration
{
    public class ProcessGetVibrationListCount
    {
        private readonly NFnet_BLL.Vibration.ProcessVibrationBLL bll = new NFnet_BLL.Vibration.ProcessVibrationBLL();

        //根据项目编号 点名 时间 分页获取测振仪数据List总行数
        public int GetVibrationListCount(int xmno, string point_name, DateTime beginTime, DateTime endTime,
            out string mssg)
        {
            var model = new NFnet_BLL.Vibration.ProcessVibrationBLL.ProcessGetVIbrationModel() 
            {
                xmno = xmno,
                point_name = point_name,
                beginTime = beginTime,
                endTime = endTime
            };
            if (bll.GetVibrationListCount(model, out mssg))
                return model.count;
            else
                return 0;
        }
    }
}
