﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using vibration2.Model;

namespace NFnet_Interface.Vibration
{
    //根据项目编号 点名 时间 分页获取测振仪数据List
    public class ProcessGetVibrationList
    {
        private readonly NFnet_BLL.Vibration.ProcessVibrationBLL bll = new NFnet_BLL.Vibration.ProcessVibrationBLL();

        public List<vibration> GetVibrationList(int xmno, string point_name, DateTime beginTime, DateTime endTime,
            int pageIndex, int pageSize, string orderName, string sort, out string mssg)
        {
            var model = new NFnet_BLL.Vibration.ProcessVibrationBLL.ProcessGetVIbrationModel
                (xmno, point_name, beginTime, endTime, orderName, sort, pageIndex, pageSize);
            if (bll.ProcessGetVibrationList(model, out mssg))
                return model.modelList;
            else
                return null;
        }
    }
}
