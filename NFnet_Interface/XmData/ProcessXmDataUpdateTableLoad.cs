﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.XmData;

namespace NFnet_Interface.XmData
{
    public class ProcessXmDataUpdateTableLoad
    {
        public static ProcessXmDataUpdateBLL processXmDataUpdateBLL = new ProcessXmDataUpdateBLL();
        public List<SystemData.Model.xmdataupdatetime> XmDataUpdateTableLoad(string unitname,out string mssg)
        {
            var model = new ProcessXmDataUpdateBLL.ProcessXmDataUpdateInfoLoadModel(unitname);
            if (processXmDataUpdateBLL.ProcessXmDataUpdateInfoLoad(model, out mssg))
                return model.modelist;
            return new List<SystemData.Model.xmdataupdatetime>();
        }
    }
}
