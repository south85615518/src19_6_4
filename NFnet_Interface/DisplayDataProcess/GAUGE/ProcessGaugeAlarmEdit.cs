﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GAUGE;
//using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeAlarmEdit
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmEdit(Gauge.Model.reservoirwaterlevel model, out string mssg)
        {
            //var processAlarmEditModel = new ProcessAlarmBLL.(model, xmname);
            //return alarmBLL.ProcessAlarmEdit(processAlarmEditModel, out mssg);
            return alarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
