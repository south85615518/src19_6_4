﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using NFnet_Interface.DisplayDataProcess.GAUGE;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugePlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string GaugePlotLineAlarm(string pointnamestr, string xmname, int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<GaugePlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessGaugeDataAlarmBLL processGaugeDataAlarmBLL = new ProcessGaugeDataAlarmBLL(xmname, xmno, pointnamelist);
                List<GaugePlotlineAlarmModel> lmm = new List<GaugePlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    Gauge.Model.reservoirwaterlevelalarmvalue pointvalue = processGaugeDataAlarmBLL.TestPointAlarmValue(pointname);
                    List<Gauge.Model.reservoirwaterlevel> alarmList = processGaugeDataAlarmBLL.TestAlarmValueList(pointvalue);
                    GaugePlotlineAlarmModel model = new GaugePlotlineAlarmModel
                    {
                        pointname = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("库水位预警参数加载出错,错误信息:" + ex.Message);
                return null;
            }
        }
    }
}
