﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
   public class ProcessGAUGEPointNewestDateTimeGet
    {
       public ProcessGaugeBLL processGAUGEBLL = new ProcessGaugeBLL();
       //public ProcessInclinometerCom inlcinometerCom = new ProcessInclinometerCom();
       public DateTime GAUGEPointNewestDateTimeGet(string pointname,out string mssg)
       {
           var inclinometerPointNewestDateTimeCondition = new InclinometerPointNewestDateTimeCondition(0, pointname);
           if (!processGAUGEBLL.ProcessPointNewestDateTimeGet(inclinometerPointNewestDateTimeCondition, out mssg))
               return new DateTime();
           return inclinometerPointNewestDateTimeCondition.dt;

       }
    }
}
