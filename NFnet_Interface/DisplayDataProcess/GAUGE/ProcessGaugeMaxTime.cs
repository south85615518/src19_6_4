﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.GNSS;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeMaxTime
    {
        public ProcessGaugeDATABLL processGaugeDATABLL = new ProcessGaugeDATABLL();
        public DateTime GaugeMaxTime(string pointnamestr, out string mssg)
        {
            var processGaugeDATAMaxTimeBLLModel = new ProcessGaugeDATABLL.ProcessGaugeDATAMaxTimeBLLModel(pointnamestr);
            //var processSenorMaxTimeModel = new ProcessDTUSenorCom.ProcessSenorMaxTimeModel(senorMaxTimeCondition, role, tmpRole);
            if (processGaugeDATABLL.ProcessGaugeDATAMaxTimeBLL(processGaugeDATAMaxTimeBLLModel, out mssg))
            {
                return processGaugeDATAMaxTimeBLLModel.dt;
            }
            return Convert.ToDateTime("1970-01-01");
        } 
        
    }
}
