﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GAUGE;
//using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugePointAlarmValueDel
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueDel( Gauge.Model.reservoirwaterlevelalarmvalue model ,out string mssg)
        {
            
            pointAlarmBLL.ProcessAlarmDel(model, out mssg);
        }
    }
}
