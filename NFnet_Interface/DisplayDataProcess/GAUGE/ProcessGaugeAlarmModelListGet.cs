﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GAUGE;


namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeAlarmModelListGet
    {
        public ProcessGaugeDATABLL processGaugeDATABLL = new ProcessGaugeDATABLL();
        public List<Gauge.Model.gaugedatareport> GaugeAlarmModelListGet( List<string> pointnamelist, out string mssg)
        {
            var processGaugeAlarmModelListGetModel = new ProcessGaugeDATABLL.ProcessgaugeAlarmModelListGetModel(pointnamelist);
            if (processGaugeDATABLL.ProcessgaugeAlarmModelListGet(processGaugeAlarmModelListGetModel, out mssg))
            {
                return processGaugeAlarmModelListGetModel.model;
            }
            return new List<Gauge.Model.gaugedatareport>();
        }
    }
}