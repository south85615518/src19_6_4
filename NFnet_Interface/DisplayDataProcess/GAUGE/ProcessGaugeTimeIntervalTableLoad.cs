﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessGaugeTimeIntervalTableLoad
    {
        public ProcessGaugeBLL processGaugeBLL = new ProcessGaugeBLL();
        public DataTable GaugeTimeIntervalTableLoad(int xmno, string searchstring, int pageIndex, int rows, string sord, out string mssg)
        {
            var mcutimeintervalModel = new ProcessGaugeBLL.gaugetimeintervalModel(xmno,pageIndex,rows,sord);
            if (processGaugeBLL.ProcessGaugeTimeInterval(mcutimeintervalModel, out mssg))
            {
                return mcutimeintervalModel.dt;
            }
            return new DataTable();
        }
    }
}
