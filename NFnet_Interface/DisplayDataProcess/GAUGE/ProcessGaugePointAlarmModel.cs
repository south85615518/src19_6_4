﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugePointAlarmModel
    {
        public ProcessPointAlarmBLL processGaugePointAlarmBLL = new ProcessPointAlarmBLL();
        public Gauge.Model.reservoirwaterlevelalarmvalue GaugePointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processGaugePointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processGaugePointAlarmBLL.ProcessPointAlarmModelGet(processGaugePointAlarmModel, out mssg))
            {
                return processGaugePointAlarmModel.model;
            }
            return new Gauge.Model.reservoirwaterlevelalarmvalue();
        }
    }
}
