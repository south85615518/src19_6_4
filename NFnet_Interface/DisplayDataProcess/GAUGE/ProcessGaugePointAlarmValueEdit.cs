﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GAUGE;
//using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugePointAlarmValueEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueEdit(Gauge.Model.reservoirwaterlevelalarmvalue model,out string mssg)
        {
             pointAlarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
