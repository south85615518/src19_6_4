﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugePointLoadBLL
    {
        public ProcessGaugeBLL processGaugeBLL = new ProcessGaugeBLL();
        public List<string> GaugePointLoadBLL(int xmno,out string mssg)
        {
            var processGaugePointLoadModel = new NFnet_BLL.DisplayDataProcess.GAUGE.ProcessGaugeBLL.ProcessGaugePointLoadModel(xmno);
            if (processGaugeBLL.ProcessGaugePointLoad(processGaugePointLoadModel, out mssg))
            {
                return processGaugePointLoadModel.pointlist;
            }
            return new List<string>();
        }
    }
}
