﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeDateTime
    {
        public ProcessGaugeBLL processGaugeBLL = new ProcessGaugeBLL();

        public Gauge.Model.gaugedatareport GaugeDateTime(string pointname,DateTime dt,out string mssg)
        {
            var gaugeDataTimeCondition = new ProcessGaugeBLL.ProcessSenorDataTimeModel(pointname, dt);
            if (!processGaugeBLL.ProcessSenorDataTime(gaugeDataTimeCondition, out mssg)) return new Gauge.Model.gaugedatareport();
            return gaugeDataTimeCondition.model;

        }


    }
}
