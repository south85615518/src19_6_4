﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeChart
    {
        public ProcessGaugeChartBLL processGaugeChartBLL = new ProcessGaugeChartBLL();
        public List<serie> SerializestrGauge(object sql, object xmname, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmname,pointname);

            if (ProcessGaugeChartBLL.ProcessSerializestrGauge(processChartCondition, out mssg))
                return processChartCondition.series;
            return new List<serie>();


        }
    }
}
