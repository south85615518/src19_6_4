﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Tool;
using System.IO;
using InclimeterDAL.Model;
//using NFnet_Interface.DisplayDataProcess.GAUGE;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public List<string> pointnamelist { get; set; }
        //public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessGaugeDataAlarmBLL()
        {
        }
        public ProcessGaugeDataAlarmBLL(string xmname, int xmno, List<string> pointnamelist)
        {
            this.xmname = xmname;
            this.xmno = xmno;
            this.pointnamelist = pointnamelist;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static ProcessPointAlarmBLL processGaugePointAlarmBLL = new ProcessPointAlarmBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";

        public bool main()
        {

            return TestGaugeModelList();

        }
        public ProcessGaugeAlarmModelListGet processGaugeAlarmModelListGet = new ProcessGaugeAlarmModelListGet();
        public ProcessGaugePointAlarmModel processGaugePointAlarmModel = new ProcessGaugePointAlarmModel();
        public bool TestGaugeModelList()
        {


            List<Gauge.Model.gaugedatareport> lc = processGaugeAlarmModelListGet.GaugeAlarmModelListGet( pointnamelist, out mssg);
            //ProcessPrintMssg.Print(mssg);
            GaugePointAlarm(lc);
            return true;
            //ProcessPrintMssg.Print(mssg);

        }
        public Gauge.Model.reservoirwaterlevelalarmvalue TestPointAlarmValue(string pointName)
        {
            return processGaugePointAlarmModel.GaugePointAlarmModel(xmno, pointName, out mssg);
        }



        public static ProcessGaugeAlarmValueModel processGaugeAlarmValueModel = new ProcessGaugeAlarmValueModel();
        public List<Gauge.Model.reservoirwaterlevel> TestAlarmValueList(Gauge.Model.reservoirwaterlevelalarmvalue pointalarm)
        {
            List<Gauge.Model.reservoirwaterlevel> alarmvalueList = new List<Gauge.Model.reservoirwaterlevel>();

            //ProcessPrintMssg.Print("一级：" + mssg);
            alarmvalueList.Add(processGaugeAlarmValueModel.GaugeAlarmValueModel(pointalarm.firstalarm, xmno, out mssg));
            alarmvalueList.Add(processGaugeAlarmValueModel.GaugeAlarmValueModel(pointalarm.secalarm, xmno, out mssg));
            alarmvalueList.Add(processGaugeAlarmValueModel.GaugeAlarmValueModel(pointalarm.thirdalarm, xmno, out mssg));
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<Gauge.Model.reservoirwaterlevel> levelalarmvalue, Gauge.Model.gaugedatareport resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (processGaugePointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                //processGaugePointAlarmBLL.ProcessPointAlarmfilterInformation();
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        //public ProcessGaugeIDBLL processGaugeIDBLL = new ProcessGaugeIDBLL();
        //public ProcessGaugeIDPointName processGaugeIDPointName = new ProcessGaugeIDPointName();
        public bool GaugePointAlarm(List<Gauge.Model.gaugedatareport> lc)
        {
            alarmInfoList = new List<string>();
            string pointname = "";
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format(" {0} ", DateTime.Now));
            ls.Add(string.Format(" {0} ", xmname));
            ls.Add(string.Format(" {0} ", "水位--库水位--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (Gauge.Model.gaugedatareport cl in lc)
            {
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname))) return false;

               Gauge.Model.reservoirwaterlevelalarmvalue pointvalue = TestPointAlarmValue(cl.point_name);
                List<Gauge.Model.reservoirwaterlevel> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> GaugeDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        public void GaugePointAlarm(Gauge.Model.gaugedatareport data)
        {

           Gauge.Model.reservoirwaterlevelalarmvalue pointvalue = TestPointAlarmValue(data.point_name);
            if (pointvalue == null) return;
            List<Gauge.Model.reservoirwaterlevel> alarmList = TestAlarmValueList(pointvalue);
            TestPointAlarmfilterInformation(alarmList, data);
        }

        //public Tool.DTUDataTableHelper ProcessPointAlarmValue(string pointname, int xmno)
        //{

        //    MDBDATA.Model.Gauge pointvalue = TestPointAlarmValue(pointname);
        //    List<MDBDATA.Model.Gaugealarmvalue> alarmList = TestAlarmValueList(pointvalue);
        //    var model = new Tool.GaugeReportHelper.Gaugealarm();
        //    model.pointname = pointname;
        //    if (alarmList[2] != null)
        //    {
        //        model.third_acdisp = alarmList[2].acdeep;
        //        model.third_thisdisp = alarmList[2].thisdeep;
        //        model.third_deep = alarmList[2].deep;
        //        model.third_rap = alarmList[2].rap;
        //    }
        //    if (alarmList[1] != null)
        //    {

        //        model.sec_acdisp = alarmList[1].acdeep;
        //        model.sec_thisdisp = alarmList[1].thisdeep;
        //        model.sec_deep = alarmList[1].deep;
        //        model.sec_rap = alarmList[1].rap;
        //    }
        //    if (alarmList[0] != null)
        //    {

        //        model.first_acdisp = alarmList[0].acdeep;
        //        model.first_thisdisp = alarmList[0].thisdeep;
        //        model.first_deep = alarmList[0].deep;
        //        model.first_rap = alarmList[0].rap;


        //    }


        //    return model;
        //}


    }
}