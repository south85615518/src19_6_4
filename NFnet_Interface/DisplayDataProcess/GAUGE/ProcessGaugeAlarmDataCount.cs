﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeAlarmDataCount
    {
        public ProcessGaugeDATABLL processGaugeData = new ProcessGaugeDATABLL();
        //public string mssg = "";
        public int GaugeAlarmDataCount(List<string>pointnamelist,out string mssg)
        {
            var getAlarmTableContModel = new ProcessGaugeDATABLL.GetAlarmTableContModel(pointnamelist);
            if (processGaugeData.GetAlarmTableCont(getAlarmTableContModel, out mssg))
            {
                return getAlarmTableContModel.cont;
            }
            return 0;
        }
    }
}
