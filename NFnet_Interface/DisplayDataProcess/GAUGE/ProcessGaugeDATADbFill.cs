﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.BKG;
//using NFnet_BLL.DisplayDataProcess.Gauge;
using NFnet_BLL.DisplayDataProcess.GAUGE;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
   public class ProcessGaugeDATADbFill
    {
       public ProcessGaugeDATABLL processGaugeDATABLL = new ProcessGaugeDATABLL();
       public FillInclinometerDbFillCondition GaugeDbFill(string pointnamestr, string rqConditionStr,  int xmno)
       {
           //sql = "";
           var model = new FillInclinometerDbFillCondition(pointnamestr,rqConditionStr,xmno);


           if (processGaugeDATABLL.ProcessGaugeDbFill(model))
           {
               
               return model;
           }

           return null;
       }
    }
}
