﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GAUGE;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeDATADelete
    {
        public ProcessGaugeDATABLL processBKGGaugeDATABLL = new ProcessGaugeDATABLL();
        public bool GaugeDATABLLDelete(string xmname, string pointname, DateTime dt,out string mssg)
        {
            var dataDeleteCondition = new DataDeleteCondition(xmname,pointname,dt);
            return processBKGGaugeDATABLL.ProcessGaugeDataDelete(dataDeleteCondition,out mssg);
        }
    }
}
