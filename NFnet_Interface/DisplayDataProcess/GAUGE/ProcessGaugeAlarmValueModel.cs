﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GAUGE;
//using NFnet_BLL.DisplayDataProcess.GaugeBKG;

namespace NFnet_Interface.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeAlarmValueModel
    {
        public ProcessAlarmBLL processGaugeAlarmValueBLL = new ProcessAlarmBLL();
        public Gauge.Model.reservoirwaterlevel GaugeAlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var processGaugeAlarmValueModelGetModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (processGaugeAlarmValueBLL.ProcessAlarmModelGetByName(processGaugeAlarmValueModelGetModel, out mssg))
            {
                return processGaugeAlarmValueModelGetModel.model;
            }
            return new Gauge.Model.reservoirwaterlevel();
        }
    }
}
