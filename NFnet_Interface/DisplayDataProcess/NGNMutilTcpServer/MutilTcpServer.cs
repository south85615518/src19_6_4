﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using Tool;
using NFnet_BLL.DisplayDataProcess;
using System.Threading;

namespace NFnet_Interface.DisplayDataProcess.NGNMutilTcpServer
{
    public class MutilTcpServer
    {
        public static TcpListener tcpListener = null;
        public delegate void ClientConnectEvent(IAsyncResult ar);
        private event ClientConnectEvent conEvent;
        public System.Timers.Timer timerrestart = new System.Timers.Timer(3600000);
        public System.Timers.Timer timerresInspection = new System.Timers.Timer(600000);
        public static List<TcpListener> tcpListenerdts = new List<TcpListener>();
        public string mssg = "";
        public static string NGNtcpportpath = "";
        #region 启动服务器监听
        public void MultiTcpServerStart(string NGNportpath)
        {
            try
            {
                TcpServerStopTotal();
                List<tcpclientIPInfo> ltlif = NFnet_BLL.DisplayDataProcess.WaterLevel.ProcessInfoImportBLL.MachineSettingInfoLoad(NGNportpath);
                TcpserverMutilStart(ltlif);
                NGNtcpportpath = NGNportpath;
                TimerTick();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite("服务器监听失败!");
            }
        }
        public void TcpserverMutilStart(List<tcpclientIPInfo> ltlif)
        {
            try
            {
                foreach (tcpclientIPInfo tif in ltlif)
                {

                    TcpListener tcpListener = new TcpListener(new System.Net.IPEndPoint(System.Net.IPAddress.Parse(tif.ip), Convert.ToInt32(tif.port)));
                    tcpListenstart(tcpListener, tif.port, tif.ip);
                }
                ExceptionLog.NGNPortInspectionWrite("所有端口启动完成");
            }
            catch (Exception ex)
            {

            }


        }
        public string tcpListenstart(TcpListener tcpListener, int port, string ip)
        {
            ExceptionLog.NGNPortInspectionWrite("开始启动监听端口:" + port);
            string mss = "";
            try
            {
                conEvent += clientConnect;
                tcpListener.Start();
                ExceptionLog.NGNPortInspectionWrite(string.Format("端口{0}监听成功!{1}", port, DateTime.Now));
                tcpListenerdts.Add(tcpListener);
                IAsyncResult ar;
                tcpListener.BeginAcceptSocket(new AsyncCallback(conEvent), tcpListener);
                conEvent -= clientConnect;

            }
            catch (Exception ex)
            {
                mss = ex.Message;
                ExceptionLog.NGNRecordWrite(string.Format("{0}端口监听出错!错误原因:" + ex.Message + "现在重启", port));
            }
            return mss;
        }
        public delegate void AcceptEvent(AsyncCallback ar, TcpListener tcpListener);
        public event AcceptEvent evt;
        public static int contconnect = 0;
        private void clientConnect(IAsyncResult ar)
        {

            try
            {

                TcpListener listener = (TcpListener)ar.AsyncState;
                if (listener == null) return;
                if (listener.Server == null) return;
                if (listener.Server.LocalEndPoint == null) return;
                Socket s = listener.EndAcceptSocket(ar);
                if (s == null || clientInfo(s) == null) return;
                string mssg = string.Format("远程客户端{0}上线 {1} ", clientInfo(s), DateTime.Now);
                ExceptionLog.NGNRecordWrite(mssg);
                conEvent += clientConnect;
                listener.BeginAcceptSocket(new AsyncCallback(conEvent), listener);
                conEvent -= clientConnect;
                if (s == null || clientInfo(s) == null) return;
                DataRec(s);
               
            }

            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("位置链接异步事件:" + ex.Message);
            }

        }


        #endregion

        #region 数据通信

        public bool DataRec(Socket s)
        {

            int port = Convert.ToInt32(clientInfo(s).ToString().Split(':')[1]);
            //要一条数据
            StringBuilder datastr = new StringBuilder(256);
            byte[] bytearray = new byte[1024];
            //设置20秒等待时间
            s.ReceiveTimeout = 600000;
            List<byte> bytelist = new List<byte>();
            try
            {
                ExceptionLog.NGNRecordWrite(string.Format("等待{0}发回数据...  {1}", clientInfo(s), DateTime.Now), port);
                int cont = 0;
                while (true)
                {
                    cont = s.Receive(bytearray, 1024, SocketFlags.None);
                    if (cont == 0) break;
                    if (cont == 1024)
                    {
                        bytelist.AddRange(bytearray);
                        continue;
                    }
                    else
                    {
                        byte[] buff = bytearray.ToList().GetRange(0, cont).ToArray();
                        bytelist.AddRange(buff);
                        datadescodedata descodedata = new datadescodedata
                        {
                            port = Convert.ToInt32(clientInfo(s).ToString().Split(':')[1]),
                            bytelist = bytelist
                        };
                        Thread tngn = new Thread(new ParameterizedThreadStart(NGNDataImport));
                        tngn.Start(descodedata);
                        Thread thxyl = new Thread(new ParameterizedThreadStart(HXYLDataImport));
                        thxyl.Start(descodedata);
                        Thread tfixedinclinometer = new Thread(new ParameterizedThreadStart(FixedInclinometerDataImport));
                        tfixedinclinometer.Start(descodedata);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite(string.Format("NGN数据接收出错{0}...  {1},错误信息:" + ex.Message + "位置:" + ex.StackTrace, clientInfo(s), DateTime.Now), port);
                ExceptionLog.NGNRecordWrite(string.Format("等待{0}发回数据超时...  {1}", clientInfo(s), DateTime.Now), port);
                return true;
            }
            return true;

        }




        #endregion



        #region 数据接入
        #region 水位
        public void NGNDataImport(object data)
        {
            datadescodedata s = data as datadescodedata;
            NGNDataGet(s.bytelist, s.port);

        }


        public NFnet_BLL.DisplayDataProcess.ngn.ProcessNGNCommendBLL processNGNCommendBLL = new NFnet_BLL.DisplayDataProcess.ngn.ProcessNGNCommendBLL();
        public static NFnet_BLL.DisplayDataProcess.WaterLevel.ProcessDTUDATABLL processNGNDATABLL = new NFnet_BLL.DisplayDataProcess.WaterLevel.ProcessDTUDATABLL();
        public NFnet_BLL.DisplayDataProcess.ngn.ProcessNGNDataBLL processNGNDataBLL = new NFnet_BLL.DisplayDataProcess.ngn.ProcessNGNDataBLL();
        #region 数据解析
        public bool NGNDataGet(List<byte> bytelist, int port)
        {


            double line = 0;
            double holedepth = 0;
            //要一条数据
            StringBuilder datastr = new StringBuilder(256);
            int xmno = 0;
            string point_name = "";
            try
            {
                string str = Tool.com.HexHelper.ByteToHexString(bytelist.ToArray());

                ExceptionLog.NGNRecordWrite(string.Format("接收到数据{0}{1}...  {2}", port, str, DateTime.Now), port);

                List<byte[]> lb = Tool.com.ByteArrayHelper.ByteArrayToArrayList(bytelist.ToArray(), new byte[2] { 0xff, 0x1c });

                foreach (var b in lb)
                {

                    if (!(processNGNCommendBLL.ProcessNGNCommendCheck(b, out mssg) && Tool.com.CRC16Helper.IsDataCorrectCRC16(b, out mssg)))
                    {
                        ExceptionLog.NGNRecordWrite(mssg, port);
                        continue;
                    }
                    NFnet_BLL.DisplayDataProcess.ngn.ProcessNGNCommendBLL.GroudWater groundwater = new NFnet_BLL.DisplayDataProcess.ngn.ProcessNGNCommendBLL.GroudWater();
                    processNGNCommendBLL.PrcoessNGNDataDescode(b, null, out groundwater);
                    int currentxmno = GetNGNXmnoFromPort(port);

                    //根据设备号和端口获取点名
                    NGNModulePointName(currentxmno, port, groundwater.id.ToString(), out point_name, out line, out holedepth, out xmno);
                    ExceptionLog.NGNRecordWrite(string.Format("项目编号{0}水位点名{1}当前电压{2} {3}", xmno,point_name, groundwater.voltage, DateTime.Now), port);
                    //计算当前水位值
                    ExceptionLog.NGNRecordWrite(string.Format("获取到设备号{0}的点名为{1}线长{2}初始水位{3}", groundwater.id, point_name, line, holedepth), port);
                    if (point_name == "" || point_name == "0" || string.IsNullOrEmpty(point_name)) continue;
                    DTU.Model.dtudata model = new DTU.Model.dtudata
                    {
                        point_name = point_name,
                        mkh = groundwater.id,
                        deep = Convert.ToDouble((Convert.ToDouble(groundwater.deep) / 1000).ToString("0.000")),
                        time = Convert.ToDateTime(groundwater.dt) > DateTime.Now ? DateTime.Now : Convert.ToDateTime(groundwater.dt),
                        xmno = xmno,
                        dtutime = DateTime.Now,
                        groundElevation = Convert.ToDouble((Convert.ToDouble(groundwater.deep) / 1000).ToString("0.000")),
                        degree = Math.Round(groundwater.degree, 3, MidpointRounding.AwayFromZero)

                    };
                    if (model.deep != 0)
                        processNGNDATABLL.ProcessDTUDATAInsertBLL(model, out mssg);
                    if (model.degree != 0)
                        processNGNDataBLL.ProcessNGNDataDegreeUpdate(model, out mssg);
                    if (mssg != "")
                        ExceptionLog.NGNRecordWrite(mssg, port);
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite("水位数据解析出错，错误信息：" + ex.Message+"位置:"+ex.StackTrace, port);
            }


            return true;

        }
        #endregion
        #region 数据处理
        public static NFnet_Interface.UserProcess.ProcessDTUPortXm processNGNPortXm = new NFnet_Interface.UserProcess.ProcessDTUPortXm();

        public static int GetNGNXmnoFromPort(int port)
        {
            string mssg = "";
            return processNGNPortXm.DTUPortXm(port, out mssg).xmno;
        }


        public static NFnet_Interface.DisplayDataProcess.ngn.ProcesNGNPointModule processNGNPointModule = new NFnet_Interface.DisplayDataProcess.ngn.ProcesNGNPointModule();
        public static bool NGNModulePointName(int xmno, int port, string addressno, out string pointname, out double line, out double holedepth, out int correspondxmno)
        {
            string mssg = "";
            DTU.Model.dtu model = processNGNPointModule.NGNPointModule(xmno, port, addressno, out mssg);
            ////ProcessPrintMssg.Print(mssg);
            ExceptionLog.NGNRecordWrite(mssg, port);
            pointname = model.point_name;
            line = model.line;
            holedepth = model.holedepth;
            correspondxmno = model.xmno;
            return true;
        }

        #endregion
        #endregion

        #region 雨量

        public void HXYLDataImport(object data)
        {
            datadescodedata s = data as datadescodedata;
            HXYLDataGet(s.bytelist, s.port);

        }
        public static NFnet_Interface.DisplayDataProcess.NGN_HXYL.ProcessHXYLPreAcVal processHXYLPreAcVal = new NFnet_Interface.DisplayDataProcess.NGN_HXYL.ProcessHXYLPreAcVal();
        public NFnet_BLL.DisplayDataProcess.NGN_HXYL.ProcessNGNCommendBLL processHXYLCommendBLL = new NFnet_BLL.DisplayDataProcess.NGN_HXYL.ProcessNGNCommendBLL();
        public static double preacval = 0;
        public NFnet_BLL.DisplayDataProcess.NGN_HXYL.ProcessHXYLBLL processHXYLBLL = new NFnet_BLL.DisplayDataProcess.NGN_HXYL.ProcessHXYLBLL();
        #region 数据解析

        public bool HXYLDataGet(List<byte> bytelist, int port)
        {

            //要一条数据
            StringBuilder datastr = new StringBuilder(256);
            byte[] bytearray = new byte[1024];
            //设置20秒等待时间
            try
            {


                string str = Tool.com.HexHelper.ByteToHexString(bytelist.ToArray());

                ExceptionLog.NGNHXYLRecordWrite(string.Format("接收到数据{0}{1}...  {2}", port, str, DateTime.Now), port);

                List<byte[]> lb = Tool.com.ByteArrayHelper.ByteArrayToArrayList(bytelist.ToArray(), new byte[2] { 0xff, 0x1c });

                foreach (var b in lb)
                {

                    if (!(processHXYLCommendBLL.ProcessNGNCommendCheck(b, out mssg) && Tool.com.CRC16Helper.IsDataCorrectCRC16(b, out mssg)))
                    {
                        ExceptionLog.NGNHXYLRecordWrite(mssg, port);
                        continue;
                    }
                    NFnet_BLL.DisplayDataProcess.NGN_HXYL.ProcessNGNCommendBLL.hxyl hxyl = new NFnet_BLL.DisplayDataProcess.NGN_HXYL.ProcessNGNCommendBLL.hxyl();
                    processHXYLCommendBLL.PrcoessNGNDataDescode(b, null, out hxyl);
                    int currentxmno = GetNGNXmnoFromPort(port);
                    ExceptionLog.NGNHXYLRecordWrite(string.Format("雨量计设备{0}当前电压{1} {2}",hxyl.id, hxyl.voltage, DateTime.Now),port);
                    //根据设备号和端口获取点名
                    var model = new NGN.Model.hxyl
                    {
                        ac_val = hxyl.ylval,
                        id = hxyl.id.ToString(),
                        time = hxyl.dt > DateTime.Now ? DateTime.Now : hxyl.dt,
                        xmno = currentxmno

                    };

                    var premodel = processHXYLPreAcVal.HXYLPreAcVal(model, out mssg);
                    //Console.WriteLine(string.Format(mssg));
                    model.val = preacval == 0 ? 0 : model.ac_val - preacval >= 0 ? model.ac_val - preacval : model.ac_val;
                    preacval = model.ac_val;
                    model.ac_val = premodel.ac_val + model.val;
                    processHXYLBLL.ProcessHXYLAdd(model, out mssg);
                    //Console.WriteLine(string.Format("设备{0},累计雨量:{1},分钟雨量{4},时间{2}  {3}", hxyl.id, hxyl.ylval, hxyl.dt, DateTime.Now, model.val));
                    ExceptionLog.NGNHXYLRecordWrite(string.Format("设备{0},累计雨量:{1},分钟雨量{4},时间{2}  {3}", hxyl.id, hxyl.ylval, hxyl.dt, DateTime.Now, model.val), port);
                }


            }
            catch (Exception ex)
            {
                ExceptionLog.NGNHXYLRecordWrite("雨量数据解析出错，错误信息：" + ex.Message + "位置:" + ex.StackTrace, port);
                return true;
            }
            return true;

        }
        #endregion

        #endregion

        #region 测斜

        public void FixedInclinometerDataImport(object data)
        {
            datadescodedata s = data as datadescodedata;
            FixedInclinometerDataGet(s.bytelist, s.port);
        }
        public NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer.ProcessNGNCommendBLL processInclinometerCommendBLL = new NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer.ProcessNGNCommendBLL();
        public NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer.ProcessFixed_Inclinometer_orglDataBLL processFixed_Inclinometer_orglDataBLL = new NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer.ProcessFixed_Inclinometer_orglDataBLL();
        #region 数据解析

        public bool FixedInclinometerDataGet(List<byte> bytelist, int port)
        {
           
            //要一条数据
            StringBuilder datastr = new StringBuilder(256);
            byte[] bytearray = new byte[1024];
            int xmno = 0;
            //设置20秒等待时间
            try
            {
                
                        string str = Tool.com.HexHelper.ByteToHexString(bytelist.ToArray());

                        ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("接收到数据{0}{1}...  {2}", port, str, DateTime.Now), port);

                        List<byte[]> lb = Tool.com.ByteArrayHelper.ByteArrayToArrayList(bytelist.ToArray(), new byte[2] { 0xff, 0x1c });

                        foreach (var b in lb)
                        {

                            if (!(processInclinometerCommendBLL.ProcessNGNCommendCheck(b, out mssg) && Tool.com.CRC16Helper.IsDataCorrectCRC16(b, out mssg)))
                            {
                                ExceptionLog.NGNFixedInclinometerRecordWrite(mssg, port);
                                continue;
                            }
                            List<NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer.ProcessNGNCommendBLL.fixedinclinometer> fixedList = new List<NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer.ProcessNGNCommendBLL.fixedinclinometer>();
                            processInclinometerCommendBLL.PrcoessNGNDataDescode(b, out fixedList);
                            //根据端口号获取项目编号
                            xmno = NFnet_Interface.DisplayDataProcess.FixedInclinometer.ProcessInterface.GetNGNXmnoFromPort(port);
                            double chainS = 0;
                            foreach (var devicemodel in fixedList)
                            {
                                //根据端口和设备号地址码获取测斜仪的点名信息
                                var device = NFnet_Interface.DisplayDataProcess.FixedInclinometer.ProcessInterface.NGNFixedInclinometerDeviceModel(xmno, port, devicemodel.deviceno, devicemodel.address, out mssg);

                                double devicedisp = device.Wheeldistance * Math.Sin(device.modulesA + device.modulesB * devicemodel.f + device.modulesC * Math.Pow(devicemodel.f, 2) + device.modulesD * Math.Pow(devicemodel.f, 3));
                                chainS += devicedisp;
                                NGN.Model.fixed_inclinometer_orgldata orgldataModel = new NGN.Model.fixed_inclinometer_orgldata
                                {
                                    point_name = device.point_name,
                                    disp = devicedisp,
                                    f = devicemodel.f,
                                    time = devicemodel.time,
                                    xmno = xmno,
                                    chain_name = device.chain,
                                    taskid = devicemodel.taskid
                                };
                                processFixed_Inclinometer_orglDataBLL.Add(orgldataModel, out mssg);
                                ExceptionLog.NGNFixedInclinometerRecordWrite(mssg, port);

                            }


                       
                }

            }
            catch (Exception ex)
            {

                ExceptionLog.NGNFixedInclinometerRecordWrite("固定测斜数据解析出错，错误信息：" + ex.Message + "位置:" + ex.StackTrace, port);
               
                return true;

            }
            return true;

        }

        #endregion
        #endregion
        #endregion
        #region 处理类
        public string clientInfo(Socket s)
        {
            return string.Format("{0}:{1}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, s.LocalEndPoint.ToString().Split(':')[1]);
        }
        public class datadescodedata
        {
            public int port { get; set; }
            public List<byte> bytelist { get; set; }
        }
        #endregion
        #region 端口巡查

        public void TimerTick()
        {
            timerresInspection.Elapsed += new System.Timers.ElapsedEventHandler(TcpPortActiveCheck);
            timerresInspection.Enabled = true;
            timerresInspection.AutoReset = true;
        }

        #region 关闭/重启
        //重新开启监听
        //停止服务
        public void TcpServerStopTotal()
        {
            try
            {
                foreach (TcpListener tcpListener in tcpListenerdts)
                {

                    tcpListener.Server.Close();
                    tcpListener.Stop();



                }
                tcpListenerdts.Clear();
            }
            catch (Exception ex)
            {


            }
        }
        public static int inspectCont = 0;
        public void TcpPortActiveCheck(object source, System.Timers.ElapsedEventArgs e)
        {
            ExceptionLog.NGNPortInspectionWrite(string.Format("{0}开始执行第{1}次端口巡检", DateTime.Now, ++inspectCont));
            List<tcpclientIPInfo> ltlif = NFnet_BLL.DisplayDataProcess.WaterLevel.ProcessInfoImportBLL.MachineSettingInfoLoad(NGNtcpportpath);
            foreach (tcpclientIPInfo tif in ltlif)
            {
                if (!NFnet_Interface.DisplayDataProcess.WaterLevel.ProcessTcpStateServer.IsUsedIPEndPoint(tif.port))
                {
                    ExceptionLog.NGNPortInspectionWrite(string.Format("巡检发现监听端口{0}已经关闭!现在启动", tif.port));
                    TcpListener tcpListener = new TcpListener(new System.Net.IPEndPoint(System.Net.IPAddress.Parse(tif.ip), Convert.ToInt32(tif.port)));
                    tcpListenstart(tcpListener, tif.port, tif.ip);
                }
            }

        }
        public void TcpServerStop(TcpListener tcpListener)
        {
            try
            {


                tcpListener.Server.Close();
                tcpListener.Stop();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite("关闭服务器端口{0}监听出错,错误信息：" + ex.Message);

            }
        }
        private object useSocketLocker = new object();
        public void SocketClose(Socket s)
        {
            System.Threading.Monitor.Enter(useSocketLocker);
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite("关闭远程客户端出错!出错原因:" + ex.Message);
            }
            finally
            {
                System.Threading.Monitor.Exit(useSocketLocker);
            }
        }
        
        /// <summary>
        /// 重启所有服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        public void TcpListenerRestartTotal(object source, System.Timers.ElapsedEventArgs e)
        {
            string NGNportpath = Convert.ToString(source);
            timerrestart.Stop();
            timerrestart.Enabled = false;
            timerrestart.Elapsed -= new System.Timers.ElapsedEventHandler(TcpListenerRestartTotal);
            TcpServerStopTotal();
            List<tcpclientIPInfo> ltlif = NFnet_BLL.DisplayDataProcess.WaterLevel.ProcessInfoImportBLL.MachineSettingInfoLoad(NGNportpath);
            TcpserverMutilStart(ltlif);
            NGNtcpportpath = NGNportpath;
        }

        #endregion
        #endregion


        


    }
}
