﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurveyDataTableCountLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public string SurveyDataTableCountLoad(string xmname,string pointname ,data.Model.gtsensortype datatype,DateTime starttime,DateTime endtime,out string mssg)
        {
            var gtResultDataCountLoadCondition = new GTResultDataCountLoadCondition(xmname,pointname,datatype,starttime,endtime);
            if (processResultDataBLL.SurveyResultdataTableRecordsCount(gtResultDataCountLoadCondition, out mssg))
                return gtResultDataCountLoadCondition.totalCont;
            return "";
        }
    }
}
