﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_gtalarmvaluebll.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class Process_twoscalar_AlarmRecordsCount
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public int twoscalar_AlarmRecordsCount(int xmno, data.Model.gtsensortype datatype, out string mssg)
        {
            var processAlarmRecordsCountModel = new ProcessAlarmValueBLL.ProcessAlarmRecordsCountModel(xmno,datatype);



            if (alarmBLL.TwoScalarTableRowsCount(processAlarmRecordsCountModel, out mssg))
            {
                return Convert.ToInt32(processAlarmRecordsCountModel.totalCont);

            }
            else
            {
                //计算结果数据数量出错信息反馈
                return 0;
            }
        }
    }
}
