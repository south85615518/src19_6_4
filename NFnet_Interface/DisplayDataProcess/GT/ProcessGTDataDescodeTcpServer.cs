﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Tool;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_Interface.UserProcess;
using System.Threading;
using NFnet_BLL.LoginProcess;
using System.Web.Script.Serialization;
using NFnet_BLL.GTSensorServer;

namespace NFnet_BLL.DisplayDataProcess
{
    public class ProcessGTDataDescodeServer
    {
        public int rows { get; set; }
        public System.Windows.Forms.ListBox listbox { get; set; }
        public System.Timers.Timer timermain = new System.Timers.Timer(60000);
        public JavaScriptSerializer jss = new JavaScriptSerializer();
        public string mssg = "";
        public void TimerMain()
        {
            timermain.Elapsed += new System.Timers.ElapsedEventHandler(DataImportServer);
            timermain.Enabled = true;
            timermain.AutoReset = true;
        }
        public void DataImportServer(object source, System.Timers.ElapsedEventArgs e)
        {
            string filenametmp = "";
            try
            {
                Console.WriteLine(mssg = "现在停止定时器进入导入任务执行...");
                this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg + " " + DateTime.Now); }));
                timermain.Enabled = false;
                List<string> ls = Tool.FileHelper.DirTravel(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "\\采集端数据文件任务表");
                if (ls.Count == 0) { Console.WriteLine(mssg = "目前不存在采集任务！"); return; }
                this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg + " " + DateTime.Now); }));
                Console.WriteLine(mssg = "目前数据导入任务库中一共有" + ls.Count + "个数据导入任务");
                this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg + " " + DateTime.Now); }));
                foreach (string filename in ls)
                {
                    try
                    {

                        

                        filenametmp = filename;
                        string modelstring = Tool.FileHelper.ProcessSettingString(filename);
                        Console.WriteLine(mssg = "现在执行数据导入任务:【" + modelstring + "】  "+ DateTime.Now);
                        this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg + " " + DateTime.Now); }));
                        NFnet_BLL.DataImport.ProcessFile.ProcessGTSensorDataBLL.ProcessFileDecodeTxtModel model = jss.Deserialize<NFnet_BLL.DataImport.ProcessFile.ProcessGTSensorDataBLL.ProcessFileDecodeTxtModel>(modelstring);
                        
                       
                       

                        switch (model.fileType)
                        {
                            case "金马设备数据":
                                NFnet_BLL.DataImport.ProcessFile.ProcessGTSensorDataBLL processSensorBLL = new NFnet_BLL.DataImport.ProcessFile.ProcessGTSensorDataBLL();
                                processSensorBLL.ProcessFileDecodeTxtDescode(model, out mssg);
                                break;
                            case "测斜原始数据":
                                NFnet_BLL.DataImport.ProcessFile.GTSensorServer.ProcessGTInclinometer processGTInclinometerBLL = new DataImport.ProcessFile.GTSensorServer.ProcessGTInclinometer();
                                var inclinometerModel = jss.Deserialize<NFnet_BLL.DataImport.ProcessFile.GTSensorServer.ProcessGTInclinometer.ProcessFileDecodeTxtModel>(modelstring);
                                processGTInclinometerBLL.ProcessFileDecodeTxtDescode(inclinometerModel,out mssg);
                                break;
                            default:
                                NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL processTotalStationBLL = new DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL();
                                NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessFileDecodeTxtModel decodemodel = new DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessFileDecodeTxtModel { fileType = model.fileType, path = model.path, xmname = model.xmname, xmno = model.xmno };
                                processTotalStationBLL.ProcessFileDecodeTxtDescode(decodemodel, out mssg);
                                break;

                        }
                        Console.WriteLine(mssg = "导入任务执行完成，现在从数据导入任务库中删除【" + modelstring + "】" + DateTime.Now);
                        this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg + " " + DateTime.Now); }));
                        Tool.FileHelper.FileDelete(filename);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(mssg = "数据文件导入出错,任务文件名【" + filename + "】" + DateTime.Now);
                        this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg + " " + DateTime.Now); }));
                        Tool.FileHelper.FileDelete(filename);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(mssg = string.Format("数据文件{0}导入出错！错误信息:" + ex.Message, filenametmp));
                this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg + " " + DateTime.Now); }));
            }
            finally
            {
                Console.WriteLine(mssg = "所有导入任务执行完成现在开启定时器...");
                this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg + " " + DateTime.Now); }));
                timermain.Enabled = true;
                timermain.AutoReset = true;
            }


        }







    }
}

