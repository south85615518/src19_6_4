﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT
{
   public class ProcessResultDataMaxTime
    {

        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public  DateTime ResultDataMaxTime(string xmname,data.Model.gtsensortype datatype,out string mssg)
        {
            var processResultDataMaxTime = new GTSensorMaxTimeCondition(xmname,datatype);
            
            if (!resultDataBLL.ProcessdataResultDataMaxTime(processResultDataMaxTime, out mssg)) return new DateTime();
            return processResultDataMaxTime.dt;
            

        }
    }
}
