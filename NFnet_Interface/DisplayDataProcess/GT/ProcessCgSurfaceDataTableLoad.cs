﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL;using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.GT;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceCgResultDataTableLoad
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public DataTable CgScalarResultDataTableLoad(int xmno, string pointname, int pageIndex, int rows, string sord,  DateTime starttime, DateTime endtime, out  string mssg)
        {
            var model = new NFnet_BLL.GTResultDataLoadCondition(xmno, pointname, pageIndex, rows, sord, starttime, endtime);
            if (processResultDataBLL.CgSurfaceResultdataTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
}
