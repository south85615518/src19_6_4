﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTWaterLinePointAlarmValueLoad
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public DataTable GTWaterLinePointAlarmValueLoad(int xmno, string searchstring, string colName, int pageIndex, int rows, string sord, data.Model.gtsensortype datatype, out string mssg)
        {
            var processPointAlarmLoadModel = new ProcessPointAlarmBLL.ProcessAlarmLoadModel( xmno, searchstring, colName, pageIndex, rows, sord,datatype);
            if (pointAlarmBLL.ProcessGTWaterLinePointLoad(processPointAlarmLoadModel, out mssg))
            {
                return processPointAlarmLoadModel.dt;
            }
            else
            {
                //加载结果数据出错错误信息反馈
                return new DataTable();
            }
        }
    }
}
