﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessTimeDataDelete
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool CYCDataDelete(string xmname, int startcyc, int endcyc, data.Model.gtsensortype datatype, out string mssg)
        {
            var model = new ProcessResultDataBLL.DeleteModel(xmname, startcyc, endcyc, datatype);
            return processResultDataBLL.Delete(model, out mssg);
        }
    }
}
