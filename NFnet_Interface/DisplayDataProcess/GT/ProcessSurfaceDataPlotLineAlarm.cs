﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using Tool;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT
{
    public class ProcessSurfaceDataPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string scalar_ResultDataPlotLineAlarm(string pointnamestr,data.Model.gtsensortype datatype ,string xmname, int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<GTPlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcesSurfaceDataAlarmBLL processResultDataAlarmBLL = new ProcesSurfaceDataAlarmBLL(xmname, xmno);
                List<GTPlotlineAlarmModel> lmm = new List<GTPlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    var pointvalue = processResultDataAlarmBLL.TestPointAlarmValue(pointname,datatype);
                    if (pointvalue == null) continue;
                    var alarmList = processResultDataAlarmBLL.TestAlarmValueList(pointvalue);
                    GTPlotlineAlarmModel model = new GTPlotlineAlarmModel
                    {
                         pointname  = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                    break;
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("混凝土支撑轴面预警参数加载出错,错误信息:" + ex.Message);
                return null;
            }
        }
    }
}
