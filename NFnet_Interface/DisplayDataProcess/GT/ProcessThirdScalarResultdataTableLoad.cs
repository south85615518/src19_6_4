﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using System.Data;
using NFnet_BLL;
namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessThirdScalarResultdataTableLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public DataTable ThirdScalarResultdataTableLoad(string xmname,string point_name,int pageIndex,int rows,string sord,data.Model.gtsensortype sensortype, DateTime starttime, DateTime endtime,out string mssg)
        {
            //int xmno, string pointname, int pageIndex, int rows, string sord, DateTime starttime, DateTime endtime
            var model = new GTResultDataLoadCondition(xmname, point_name, pageIndex, rows, sord, sensortype, starttime, endtime);
            if(processResultDataBLL.ThirdScalarResultdataTableLoad(model,out  mssg))
            {
                return model.dt;
            }
            return null;
        }
    }
}
