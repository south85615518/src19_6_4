﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfacePointAlarmValueDel
    {
        public ProcessSurfacePointAlarmBLL SurfacePointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public void SurfacePointAlarmValueDel( global::data.Model.gtsurfacestructure model ,out string mssg)
        {
            
            SurfacePointAlarmBLL.ProcessAlarmDel(model, out mssg);
        }
    }
}
