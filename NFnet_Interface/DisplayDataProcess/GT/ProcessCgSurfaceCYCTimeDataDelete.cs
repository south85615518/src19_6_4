﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceCgCYCTimeDataDelete
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public bool CgCYCTimeDataDelete( int xmno, data.Model.gtsensortype datatype,int startcyc,int endcyc, out string mssg)
        {
            var model = new ProcessSurfaceDataBLL.DeleteModel( xmno,datatype, startcyc,endcyc);
            return processResultDataBLL.DeleteCg(model, out mssg);
        }
    }
}
