﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_gtalarmvaluebll.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GTBKG;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class Process_singlescalar_AlarmValueModel
    {
        public ProcessAlarmValueBLL processGTAlarmValueBLL = new ProcessAlarmValueBLL();
        public global::data.Model.gtalarmvalue GTAlarmValueModel(string alarmname,int xmno,data.Model.gtsensortype datatype,out string mssg)
        {
            var processGTAlarmValueModelGetModel = new ProcessAlarmValueBLL.ScalarvalueModelGetModel(xmno, alarmname,datatype);
            if (processGTAlarmValueBLL.ProcessSingleScalarvalueModelGet(processGTAlarmValueModelGetModel, out mssg))
            {
                return processGTAlarmValueModelGetModel.model;
            }
            return new global::data.Model.gtalarmvalue();
        }
    }
}
