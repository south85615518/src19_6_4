﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_gtalarmvaluebll.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class Process_twoscalar_AlarmLoad
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public DataTable twoscalar_AlarmLoad(int xmno, string colName, int pageIndex, int rows, string sord, data.Model.gtsensortype datatype, out string mssg)
        {
            var processAlarmLoadModel = new ProcessAlarmValueBLL.ProcessAlarmLoadModel(xmno, colName, pageIndex, rows, sord,datatype);


            if (alarmBLL.TwoScalarTableLoad(processAlarmLoadModel, out mssg))
            {
                return processAlarmLoadModel.dt;
            }

            //加载结果数据出错错误信息反馈
            return new DataTable();

        }
    }
}
