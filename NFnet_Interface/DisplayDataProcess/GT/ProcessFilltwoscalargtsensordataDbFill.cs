﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessFilltwoscalargtsensordataDbFill
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition FillTotalStationDbFill(string pointstr, string sql, string xmname)
        {
            
            var fillTotalStationDbFillCondition = new NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition(pointstr, sql, xmname);

            processResultDataBLL.ProcessTwoScalarfillTotalStationDbFill(fillTotalStationDbFillCondition);
            return fillTotalStationDbFillCondition;
        }
        
    }
}
