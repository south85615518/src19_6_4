﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_MODAL;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class Processgtsensordata_pointChart
    {
        
        public List<serie_point> Serializestrgtsensordata_point(object sql, object xmname, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmname,pointname);
            ExceptionLog.ExceptionWrite(processChartCondition.xmname);
            if (ProcessGTSensorDataChartBLL.ProcessSerializestrgtsensordata_point(processChartCondition, out mssg))
                return processChartCondition.serie_points;
            return new List<serie_point>();


        }
        public List<serie_point> Serializestrgtsensordata_point(object sql, object xmname, object pointname,int rows,int pageIndex, out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmname, pointname,rows,pageIndex);
            ExceptionLog.ExceptionWrite(processChartCondition.xmname);
            if (ProcessGTSensorDataChartBLL.ProcessSerializestrgtsensordata_point(processChartCondition, out mssg))
                return processChartCondition.serie_points;
            return new List<serie_point>();


        }
    }
}
