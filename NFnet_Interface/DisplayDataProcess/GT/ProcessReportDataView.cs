﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessReportDataView
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public DataTable ReportDataView(List<string> cyclist,int pageIndex, int pageSize,  string xmname, data.Model.gtsensortype datatype,string sord, out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessReportDataViewModel(cyclist,pageIndex,pageSize,xmname,datatype,sord);
            if (processResultDataBLL.ProcessReportDataView(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
