﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessTwoScalarResultDataTableLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public DataTable TwoScalarResultDataTableLoad(string xmname, string pointname, int pageIndex, int rows, string sord, data.Model.gtsensortype datatype,DateTime starttime,DateTime endtime, out  string mssg)
        {
            var model = new NFnet_BLL.GTResultDataLoadCondition(xmname, pointname, pageIndex, rows, sord, datatype,starttime,endtime);
            if (processResultDataBLL.TwoScalarResultdataTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
    
}
