﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceSurveyPointTimeDataAdd
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public bool SurveyPointTimeDataAdd(int xmno, string pointname,data.Model .gtsensortype datatype ,DateTime dt, DateTime importdt,out string mssg)
        {
            mssg = "";
            var model = new ProcessSurfaceDataBLL.ProcessSurveyPointTimeDataAddModel(xmno, datatype, pointname, dt, importdt);
            return false;//processResultDataBLL.ProcessSurveyPointTimeDataAdd(model,out mssg);
        }
    }
}
