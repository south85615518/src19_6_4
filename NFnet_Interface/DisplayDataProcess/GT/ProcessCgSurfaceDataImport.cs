﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceCgResultDataImport
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public bool CgResultDataImport(int xmno,data.Model.gtsensortype datatype, int cyc, int importcyc, out string mssg)
        {
            var model = new ProcessSurfaceDataBLL.AddSurveyDataModel(xmno, datatype, cyc, importcyc);
            return processResultDataBLL.ProcessCgDataImport(model,out mssg);
        }
    }
}
