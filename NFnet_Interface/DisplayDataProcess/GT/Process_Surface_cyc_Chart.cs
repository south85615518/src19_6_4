﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_MODAL;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class Processgtsurface_cycdataChart
    {
        
        public List<serie_cyc> Serializestrgtsurface_cycdata(object sql, object xmno, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmno,pointname,0);

            if (ProcessGTSensorDataChartBLL.ProcessSerializestrGTSurfaceSensorData_cyc(processChartCondition, out mssg))
                return processChartCondition.serie_cyc;
            return new List<serie_cyc>();


        }
    }
}
