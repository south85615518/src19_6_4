﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTSurfacePointAlarmRecordsCount
    {
        public ProcessSurfacePointAlarmBLL pointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public int PointAlarmRecordsCount(string xmname,int xmno,data.Model.gtsensortype datatype,string searchstring,out string mssg)
        {
            var ProcessSurfacePointAlarmRecordsCountModel = new ProcessSurfacePointAlarmBLL.ProcessAlarmRecordsCountModel(xmname, xmno,datatype ,searchstring);
            if (pointAlarmBLL.ProcessPointAlarmRecordsCount(ProcessSurfacePointAlarmRecordsCountModel, out mssg))
            {
                return Convert.ToInt32(ProcessSurfacePointAlarmRecordsCountModel.totalCont);

            }
            else
            {
                //计算结果数据数量出错信息反馈
                return 0;
            }
        }
    }
}
