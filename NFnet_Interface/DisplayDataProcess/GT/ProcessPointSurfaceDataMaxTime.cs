﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT
{
   public class ProcessPointSurfaceDataMaxTime
    {

       public ProcessSurfaceDataBLL resultDataBLL = new ProcessSurfaceDataBLL();
       public DateTime ResultDataMaxTime(int xmno, data.Model.gtsensortype datatype, string pointname, out string mssg)
        {
            var processResultDataMaxTime = new GTSurfaceTimeCondition(xmno, pointname, datatype);

            if (!resultDataBLL.ProcesssurfacedataResultDataMaxTime(processResultDataMaxTime, out mssg)) return new DateTime();
            return processResultDataMaxTime.dt;
            

        }
    }
}
