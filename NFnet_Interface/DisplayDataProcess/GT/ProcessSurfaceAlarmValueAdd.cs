﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfacePointAlarmValueAdd
    {
        public ProcessSurfacePointAlarmBLL SurfacePointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public bool SurfacePointAlarmValueAdd(global::data.Model.gtsurfacestructure model,out string mssg)
        {
             return SurfacePointAlarmBLL.ProcessAlarmAdd(model,out mssg);
        }
    }
}
