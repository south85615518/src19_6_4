﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT
{
    public class ProcessCgResultDataTimeCycLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<string> CgResultDataTimeCycLoad(string xmname,data.Model.gtsensortype datatype,out string mssg)
        {
            var processResultDataTimeCycLoadModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessResultDataBLL.ProcessResultDataTimeCycLoadModel(xmname,datatype);
            if (processResultDataBLL.ProcessCgResultDataTimeCycLoad(processResultDataTimeCycLoadModel, out mssg))
            {
                return processResultDataTimeCycLoadModel.cyctimelist;
            }
            return new List<string>();
        }
    }
}
