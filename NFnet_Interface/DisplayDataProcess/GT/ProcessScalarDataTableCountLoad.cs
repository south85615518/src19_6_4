﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessScalarDataTableCountLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public string ScalarDataTableCountLoad(string xmname,string pointname ,data.Model.gtsensortype datatype,int startcyc,int endcyc,out string mssg)
        {
            var gtResultDataCountLoadCondition = new GTResultDataCountLoadCondition(xmname,pointname,datatype,startcyc,endcyc);
            if (processResultDataBLL.ScalarResultdataTableRecordsCount(gtResultDataCountLoadCondition, out mssg))
                return gtResultDataCountLoadCondition.totalCont;
            return "";
        }
    }
}
