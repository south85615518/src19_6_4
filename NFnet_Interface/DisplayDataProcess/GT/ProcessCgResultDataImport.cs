﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessCgResultDataImport
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool CgResultDataImport(string xmname, data.Model.gtsensortype datatype,int cyc,int importcyc , out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessCgDataImportModel(xmname, cyc, importcyc, datatype);
            return processResultDataBLL.ProcessCgDataImport(model,out mssg);
        }
    }
}
