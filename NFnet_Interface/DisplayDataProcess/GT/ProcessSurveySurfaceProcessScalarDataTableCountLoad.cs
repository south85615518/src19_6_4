﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL;
using NFnet_BLL.DataProcess;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceSurveyDataTableCountLoad
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public string SurveyDataTableCountLoad(int xmno, data.Model.gtsensortype datatype, string pointname, int startcyc, int endcyc, out string mssg)
        {
            var gtResultDataCountLoadCondition = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.GTSurfaceDataCountLoadCondition(xmno,datatype,pointname,startcyc,endcyc); // (xmno,pointname,starttime,endtime);
            if (processResultDataBLL.SurveyResultdataTableRecordsCount(gtResultDataCountLoadCondition, out mssg))
                return gtResultDataCountLoadCondition.totalCont;
            return "";
        }
    }
}
