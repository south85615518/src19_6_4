﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfacePointAlarmValueLoad
    {
        public ProcessSurfacePointAlarmBLL pointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public DataTable PointAlarmValueLoad(int xmno,data.Model.gtsensortype datatype,string searchstring,string colName,int pageIndex,int rows,string sord,out string mssg)
        {
            var ProcessSurfacePointAlarmLoadModel = new ProcessSurfacePointAlarmBLL.ProcessAlarmLoadModel( xmno, datatype,searchstring, colName, pageIndex, rows, sord);
            if (pointAlarmBLL.ProcessPointLoad(ProcessSurfacePointAlarmLoadModel, out mssg))
            {
                return ProcessSurfacePointAlarmLoadModel.dt;
            }
            else
            {
                //加载结果数据出错错误信息反馈
                return new DataTable();
            }
        }
    }
}
