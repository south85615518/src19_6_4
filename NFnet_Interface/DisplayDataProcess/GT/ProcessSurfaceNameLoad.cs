﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceNameLoad
    {
        public ProcessSurfacePointAlarmBLL processSurfacePointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public List<string> SurfaceNameLoad(int xmno,data.Model.gtsensortype datatype,out string mssg)
        {
            var gTSensorDataPointLoadModel = new ProcessSurfacePointAlarmBLL.GTSensorDataPointLoadModel(xmno,datatype);
            if (processSurfacePointAlarmBLL.GTSensorDataSurfaceLoad(gTSensorDataPointLoadModel, out mssg))
            {
                return gTSensorDataPointLoadModel.ls;
            }
            return new List<string>();
        }
    }
}
