﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTPointLoad
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public List<string> GTPointLoadBLL(int xmno,data.Model.gtsensortype datatype,out string mssg)
        {
            mssg = "";
            var processGTPointLoadModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessPointAlarmBLL.GTSensorDataPointLoadModel(xmno,datatype);
            if (pointAlarmBLL.GTSensorDataPointLoad(processGTPointLoadModel, out mssg))
            {
                return processGTPointLoadModel.ls;
            }
            return new List<string>();
        }
    }
}
