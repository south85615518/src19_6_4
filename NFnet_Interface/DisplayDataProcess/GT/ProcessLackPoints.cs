﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessLackPoints
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public string LackPoints(string xmname, int xmno, data.Model.gtsensortype datatype,int cyc,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessLostPointsModel(xmname, xmno, datatype, cyc);
            if (processResultDataBLL.ProcessLostPoints(model, out mssg)) 
                return model.pointstr; 
                return "";
        }
    }
}
