﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfacePointAlarmModel
    {
        public ProcessSurfacePointAlarmBLL ProcessSurfacePointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public global::data.Model.gtsurfacestructure SurfacePointAlarmModel(int xmno,string pointname,data.Model.gtsensortype datatype,out string mssg)
        {
            var ProcessSurfacePointAlarmModel = new ProcessSurfacePointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname,datatype);
            if (ProcessSurfacePointAlarmBLL.ProcessPointAlarmModelGet(ProcessSurfacePointAlarmModel, out mssg))
            {
                return ProcessSurfacePointAlarmModel.model;
            }
            return new global::data.Model.gtsurfacestructure();
        }
    }
}
