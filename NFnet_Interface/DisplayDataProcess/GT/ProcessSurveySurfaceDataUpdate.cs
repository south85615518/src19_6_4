﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceSurveyDataUpdate
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public bool SurveyDataUpdate(int xmno, data.Model.gtsensortype datatype, string pointname, DateTime dt, string vSet_name, string vLink_name, DateTime srcdatatime, out string mssg)
        {
            var model = new ProcessSurfaceDataBLL.UpdataSurveyDataModel(xmno, datatype, pointname, dt, vSet_name, vLink_name, srcdatatime);
            return processResultDataBLL.UpdataSurveyData(model,out mssg);
        }
    }
}
