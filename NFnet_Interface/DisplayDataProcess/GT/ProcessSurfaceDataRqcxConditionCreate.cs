﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT
{
    public class ProcessSurfaceDataRqcxConditionCreate
    {
        public ProcessSurfaceDataBLL resultDataBLL = new ProcessSurfaceDataBLL();
        public ResultDataRqcxConditionCreateCondition SurfaceDataRqcxConditionCreate(string starttime ,string endtime,QueryType QT,string range ,string xmname, DateTime maxTime)
        {
            List<string> ls = new List<string>();

            var processsinglescalarResultDataRqcxConditionCreateModel = new ResultDataRqcxConditionCreateCondition(starttime, endtime, QT, range, xmname, maxTime);
            resultDataBLL.ProcessResultDataRqcxConditionCreate(processsinglescalarResultDataRqcxConditionCreateModel);
            return processsinglescalarResultDataRqcxConditionCreateModel;
        }
        
        
    }
}
