﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessPointNameSurfaceNameMultiEdit
    {
        public ProcessPointAlarmBLL PointAlarmBLL = new ProcessPointAlarmBLL();
        public void SurfacePointNameMutilEdit(global::data.Model.gtpointalarmvalue model,string pointstr ,out string mssg)
        {
            var processAlarmMultiUpdateModel = new ProcessPointAlarmBLL.ProcessAlarmMultiUpdateModel(pointstr, model);
            PointAlarmBLL.ProcessSurfaceMultiUpdate(processAlarmMultiUpdateModel, out mssg);
        }
    }
}
