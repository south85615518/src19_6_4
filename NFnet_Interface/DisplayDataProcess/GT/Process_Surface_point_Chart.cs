﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_MODAL;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class Processgtsurface_pointdataChart
    {
        
        public List<serie_point> Serializestrgtsurface_pointdata(object sql, object xmno, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmno,pointname,0);

            if (ProcessGTSensorDataChartBLL.ProcessSerializestrgtsurfacedata_point(processChartCondition, out mssg))
                return processChartCondition.serie_points;
            return new List<serie_point>();


        }
        public List<serie_point> Serializestrgtsurface_pointdata(object sql, object xmno, object pointname,int rows,int pageIndex, out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmno, pointname, 0,rows,pageIndex);

            if (ProcessGTSensorDataChartBLL.ProcessSerializestrgtsurfacedata_point(processChartCondition, out mssg))
                return processChartCondition.serie_points;
            return new List<serie_point>();


        }
    }
}
