﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurveyTimeList
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<string> SurveyTimeList(string xmname,string point_name,data.Model.gtsensortype datatype,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessResultDataTimeListLoadModel(xmname,point_name,datatype);
            if (processResultDataBLL.ProcessSurveyResultDataTimeListLoad(model, out mssg))
                return model.ls;
            return null;
        }
    }
}
