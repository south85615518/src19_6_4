﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessPointAlarmModel
    {
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public global::data.Model.gtpointalarmvalue PointAlarmModel(int xmno,string pointname,data.Model.gtsensortype datatype,out string mssg)
        {
            var processPointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname,datatype);
            if (processPointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModel, out mssg))
            {
                return processPointAlarmModel.model;
            }
            return new global::data.Model.gtpointalarmvalue();
        }
    }
}
