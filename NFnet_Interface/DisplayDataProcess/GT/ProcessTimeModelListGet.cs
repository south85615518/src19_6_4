﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessCycModelListGet
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<global::TotalStation.Model.fmos_obj.cycdirnet> TimeModelListGet(string xmname,int cyc,out string mssg)
        {
            var processCycModelListModel = new ProcessResultDataBLL.ProcessCycModelListModel(xmname,cyc);
            if (processResultDataBLL.ProcessResultDataCycModelList(processCycModelListModel, out mssg))
            {
                return processCycModelListModel.cycdirnetlist;
            }
            return new List<global::TotalStation.Model.fmos_obj.cycdirnet>();
        }
    }
}
