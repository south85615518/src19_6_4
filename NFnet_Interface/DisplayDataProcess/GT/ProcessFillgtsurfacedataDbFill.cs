﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessFillgtsurfaceataDbFill
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition FillTotalStationDbFill(string pointstr, string sql, int xmno,data.Model.gtsensortype datatype)
        {
            
            var fillTotalStationDbFillCondition = new NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition(pointstr, sql, xmno,datatype);

            processResultDataBLL.ProcessfillTotalStationDbFill(fillTotalStationDbFillCondition);
            return fillTotalStationDbFillCondition;
        }
        
    }
}
