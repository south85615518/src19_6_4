﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_BLL_INTERFACE.DisplayDataProcess.GT
{
    public class ProcessPointResultDataTimeModel
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public data.Model.gtsensordata PointResultDataTimeModelGet(string xmname,string pointname,data.Model.gtsensortype datatype,DateTime dt,out string mssg)
        {
            var processGetInitTimeModelGetModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessResultDataBLL.ProcessGetLastTimeModelGetModel(xmname,pointname,datatype,dt);
            if (processResultDataBLL.ProcessGetLastTimeModel(processGetInitTimeModelGetModel, out mssg))
            {
                return processGetInitTimeModelGetModel.model;
            }
            return new data.Model.gtsensordata();
        }
    }
}