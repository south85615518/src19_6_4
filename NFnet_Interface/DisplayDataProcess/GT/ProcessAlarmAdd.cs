﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_gtalarmvaluebll.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTAlarmAdd
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public bool AlarmAdd(global::data.Model.gtalarmvalue model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessAlarmValueBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.Add(model, out mssg);
        }
    }
}
