﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessPointDataDateTimeStampLoad
    {
        public ProcessDateTimeStampBLL processDateTimeStampBLL = new ProcessDateTimeStampBLL();
        public PointDataDateTimeStamp PointDataDateTimeStampLoad(string xmname, string pointname,data.Model.gtsensortype datatype, PointDataDateTimeStamp timestamp, out string mssg)
        {
            var processPointDataDateTimeStampLoadModel = new NFnet_BLL.DisplayDataProcess.DateTimeStamp.ProcessDateTimeStampBLL.ProcessPointDataDateTimeStampLoadModel(xmname, datatype, pointname, timestamp);
            processDateTimeStampBLL.ProcessPointDataDateTimeStampLoad(processPointDataDateTimeStampLoadModel,out mssg);
            return processPointDataDateTimeStampLoadModel.timestamp;
        }
    
    }
}
