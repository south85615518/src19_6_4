﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurveyDataLackDate
    {
        public ProcessDateTimeStampBLL processDateTimeStampBLL = new ProcessDateTimeStampBLL();
        public void SurveyDatalackdate(string xmname, string point_name,data.Model.gtsensortype datatype, PointDataDateTimeStamp timestamp, out string mssg)
        {
            var processDatalackdateModel = new NFnet_BLL.DisplayDataProcess.DateTimeStamp.ProcessDateTimeStampBLL.ProcessDatalackdateModel(xmname,point_name,datatype,timestamp);
            processDateTimeStampBLL.ProcessSurveyDatalackdate(processDatalackdateModel,out mssg);
        }
    }
}
