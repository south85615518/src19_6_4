﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceSurveyResultDataTableLoad
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public DataTable SurveyResultDataTableLoad(int xmno, data.Model.gtsensortype datatype, string pointname, int pageIndex, int rows, string sord,int startcyc,int endcyc,out  string mssg)
        {
            var model = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.GTSurfaceDataLoadCondition(xmno, datatype, pointname, pageIndex, rows, sord, startcyc, endcyc);
            if (processResultDataBLL.SurveyResultdataTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
    
}
