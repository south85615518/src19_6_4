﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurveyPointTimeDataAdd
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyPointTimeDataAdd(string xmname, string pointname,data.Model .gtsensortype datatype ,DateTime dt, DateTime importdt,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessSurveyPointTimeDataAddModel(xmname, datatype, pointname, dt, importdt);
            return processResultDataBLL.ProcessSurveyPointTimeDataAdd(model,out mssg);
        }
    }
}
