﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_gtalarmvaluebll.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTAlarmValues
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public string AlarmValues(int xmno,data.Model.gtsensortype datatype, out string mssg)
        {
            var alarmvaluename = new ProcessAlarmValueBLL.ProcessAlarmValueNameModel(xmno,datatype);
            if (alarmBLL.ProcessAlarmValueName(alarmvaluename, out mssg))
            {
                return alarmvaluename.alarmValueNameStr;
            }
             return mssg;          
        }
    }
}
