﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceSurveyTimeList
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public List<string> SurveyTimeList(int xmno, data.Model.gtsensortype datatype, string point_name, out string mssg)
        {
            var model = new ProcessSurfaceDataBLL.ProcessResultDataTimeListLoadModel(xmno, point_name, datatype);
            if (processResultDataBLL.ProcessSurveyResultDataTimeListLoad(model, out mssg))
                return model.ls;
            return null;
        }
    }
}
