﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_MODAL;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessgtsensordataChart
    {
        
        public List<serie_cyc> Serializestrgtsensordata(object sql, object xmname, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmname,pointname);
            ExceptionLog.ExceptionWrite(processChartCondition.xmname);
            if (ProcessGTSensorDataChartBLL.ProcessSerializestrGTSensorData_cyc(processChartCondition, out mssg))
                return processChartCondition.serie_cyc;
            return new List<serie_cyc>();


        }
        
    }
}
