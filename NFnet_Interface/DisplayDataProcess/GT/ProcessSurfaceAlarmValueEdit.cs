﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
//using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfacePointAlarmValueEdit
    {
        public ProcessSurfacePointAlarmBLL SurfacePointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public void SurfacePointAlarmValueEdit(global::data.Model.gtsurfacestructure model,out string mssg)
        {
             SurfacePointAlarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
