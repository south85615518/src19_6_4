﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessCgSurfaceCYCTimeList
    {
        public ProcessSurfaceDataBLL processSurfaceDataBLL = new ProcessSurfaceDataBLL();
        public List<string> CgSurfaceCYCTimeList(int xmno, data.Model.gtsensortype datatype, out string mssg)
        {
            var model = new ProcessSurfaceDataBLL.ProcessSurfaceDataTimeCycLoadModel(xmno,datatype);
            if (processSurfaceDataBLL.ProcessCgSurfaceDataTimeCycLoad(model, out mssg))
                return model.cyctimelist;
            return null;
        }
    }
}
