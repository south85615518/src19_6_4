﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_MODAL;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class Processtwoscalar_Chart
    {
        public List<serie> Serializestrtwoscalar_Chart(object sql, object xmname, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmname,pointname);

            if (ProcessGTSensorDataChartBLL.ProcessTwoScalarSerializestrGTSensorData(processChartCondition, out mssg))
                return processChartCondition.series;
            return new List<serie>();


        }
    }
}
