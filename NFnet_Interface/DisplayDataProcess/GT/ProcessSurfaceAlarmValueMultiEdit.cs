﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTSurfacePointAlarmValueMutilEdit
    {
        public ProcessSurfacePointAlarmBLL SurfacePointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public void SurfacePointAlarmValueMutilEdit(global::data.Model.gtsurfacestructure model,string pointstr ,out string mssg)
        {
            var processAlarmMultiUpdateModel = new ProcessSurfacePointAlarmBLL.ProcessAlarmMultiUpdateModel(pointstr, model);
            SurfacePointAlarmBLL.ProcessAlarmMultiUpdate(processAlarmMultiUpdateModel, out mssg);
        }
    }
}
