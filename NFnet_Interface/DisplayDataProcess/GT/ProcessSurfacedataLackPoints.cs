﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;
namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfacedataLackPoints
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public string LackPoints(int xmno,data.Model.gtsensortype datatype ,int cyc,out string mssg)
        {
            var model = new ProcessSurfaceDataBLL.ProcessLostPointsModel( xmno,datatype ,cyc);
            if (processResultDataBLL.ProcessLostPoints(model, out mssg)) 
                return model.pointstr; 
                return "";
        }
    }
}
