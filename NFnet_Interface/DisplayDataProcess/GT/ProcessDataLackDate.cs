﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessDataLackDate
    {
        public ProcessDateTimeStampBLL processDateTimeStampBLL = new ProcessDateTimeStampBLL();
        public void Datalackdate(string xmname, string point_name,data.Model.gtsensortype datatype, PointDataDateTimeStamp timestamp, out string mssg)
        {
            var processDatalackdateModel = new NFnet_BLL.DisplayDataProcess.DateTimeStamp.ProcessDateTimeStampBLL.ProcessDatalackdateModel(xmname,point_name,datatype,timestamp);
            processDateTimeStampBLL.ProcessDatalackdate(processDatalackdateModel,out mssg);
        }
    }
}
