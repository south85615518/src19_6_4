﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT
{
   public class ProcessPointResultDataMaxTime
    {

        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public  DateTime PointResultDataMaxTime(string xmname,string pointname,data.Model.gtsensortype datatype,out string mssg)
        {
            var gTSensorMaxTimeCondition = new GTSensorMaxTimeCondition(xmname,pointname ,datatype);

            if (!resultDataBLL.ProcesspointdataResultDataMaxTime(gTSensorMaxTimeCondition, out mssg)) return new DateTime();
            return gTSensorMaxTimeCondition.dt;
            

        }
    }
}
