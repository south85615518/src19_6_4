﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceDataReportDataView
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public DataTable SurfaceDataReportDataView(List<string> cyclist, data.Model.gtsensortype datatype,int pageIndex, int pageSize,  int xmno, string sord, out string mssg)
        {
            var model = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessReportDataViewModel(cyclist,datatype ,pageIndex, pageSize,xmno ,sord);
            if (processResultDataBLL.ProcessReportDataView(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
