﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceSurveyDataAdd
    {
        public ProcessSurfaceDataBLL processResultDataBLL = new ProcessSurfaceDataBLL();
        public bool SurveyDataAdd(int xmno, data.Model.gtsensortype datatype, int cyc,out  string mssg)
        {
            var model = new ProcessSurfaceDataBLL.AddSurveyDataModel(xmno, datatype, cyc);
            return processResultDataBLL.AddSurveyData(model,out mssg);
        }
    }
}
