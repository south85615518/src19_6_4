﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT
{
   public class ProcessSurfaceDataMaxTime
    {

       public ProcessSurfaceDataBLL resultDataBLL = new ProcessSurfaceDataBLL();
        public  DateTime ResultDataMaxTime(int xmno,data.Model.gtsensortype datatype,out string mssg)
        {
            var processResultDataMaxTime = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessSurfaceDataMaxTimeModel(xmno, datatype);
            
            if (!resultDataBLL.ProcessSurfaceDataMaxTime(processResultDataMaxTime, out mssg)) return new DateTime();
            return processResultDataMaxTime.dt;
            

        }
    }
}
