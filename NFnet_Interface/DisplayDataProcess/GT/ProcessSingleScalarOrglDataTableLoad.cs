﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSingleScalarOrglDataTableLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public DataTable SingleScalarOrglDataTableLoad(string xmname, string pointname, int pageIndex, int rows, string sord, data.Model.gtsensortype datatype,int startcyc,int endcyc,out  string mssg)
        {
            var model = new NFnet_BLL.GTResultDataLoadCondition(xmname, pointname, pageIndex, rows, sord, datatype, startcyc, endcyc);
            if (processResultDataBLL.SingleScalarOrgldataTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
    
}
