﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurfaceDataLackDate
    {
        public ProcessGTSurfaceDataDateTimeStampBLL processGTSurfaceDataDateTimeStampBLL = new ProcessGTSurfaceDataDateTimeStampBLL();
        public void Datalackdate(int xmno, string point_name, PointDataDateTimeStamp timestamp, out string mssg)
        {
            var processDatalackdateModel = new NFnet_BLL.DisplayDataProcess.DateTimeStamp.ProcessGTSurfaceDataDateTimeStampBLL.ProcessDatalackdateModel(xmno,point_name,timestamp);
            processGTSurfaceDataDateTimeStampBLL.ProcessDatalackdate(processDatalackdateModel, out mssg);
        }
    }
}
