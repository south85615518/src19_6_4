﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessSurveyDataAdd
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyDataAdd(string xmname, int cyc, data.Model.gtsensortype datatype, out string mssg)
        {
            var model = new ProcessResultDataBLL.AddSurveyDataModel(xmname, cyc, datatype);
            return processResultDataBLL.AddSurveyData(model,out mssg);
        }
    }
}
