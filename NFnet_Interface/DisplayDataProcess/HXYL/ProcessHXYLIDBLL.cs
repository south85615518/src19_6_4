﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{

   public class ProcessHXYLIDBLL
    {
       public ProcessHXYLBLL hXYLBLL = new ProcessHXYLBLL();
       public string HXYLPointID(int xmno,string pointname,out string mssg)
       {
           NFnet_BLL.DisplayDataProcess.HXYL.ProcessHXYLBLL.HXYLIDmodel model = new ProcessHXYLBLL.HXYLIDmodel(xmno,pointname);
           if (hXYLBLL.HXYLID(model, out mssg))
               return model.id;
           return "-1";
       }
    }
}
