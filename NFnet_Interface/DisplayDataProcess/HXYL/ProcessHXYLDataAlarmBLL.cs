﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Tool;
using System.IO;
using InclimeterDAL.Model;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public List<string> pointnamelist { get; set; }
        //public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessHXYLDataAlarmBLL()
        {
        }
        public ProcessHXYLDataAlarmBLL(string xmname, int xmno, List<string> pointnamelist)
        {
            this.xmname = xmname;
            this.xmno = xmno;
            this.pointnamelist = pointnamelist;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static ProcessPointAlarmBLL processHXYLPointAlarmBLL = new ProcessPointAlarmBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";

        public bool main()
        {

            return TestHXYLModelList();

        }
        public ProcessHXYLAlarmModelListGet processHXYLAlarmModelListGet = new ProcessHXYLAlarmModelListGet();
        public ProcessHXYLPointAlarmModel processHXYLPointAlarmModel = new ProcessHXYLPointAlarmModel();
        public bool TestHXYLModelList()
        {


            List<MDBDATA.Model.hxyl> lc = (List<MDBDATA.Model.hxyl>)processHXYLAlarmModelListGet.HXYLAlarmModelListGet(pointnamelist, out mssg);
            //ProcessPrintMssg.Print(mssg);
            HXYLPointAlarm(lc);
            return true;
            //ProcessPrintMssg.Print(mssg);

        }
        public MDBDATA.Model.hxylpointalarmvalue TestPointAlarmValue(string pointName)
        {
            return processHXYLPointAlarmModel.HXYLPointAlarmModel(xmno, pointName, out mssg);
        }



        public static ProcessHXYLAlarmValueModel processHXYLAlarmValueModel = new ProcessHXYLAlarmValueModel();
        public List<MDBDATA.Model.hxylalarmvalue> TestAlarmValueList(MDBDATA.Model.hxylpointalarmvalue pointalarm)
        {
            List<MDBDATA.Model.hxylalarmvalue> alarmvalueList = new List<MDBDATA.Model.hxylalarmvalue>();

            //ProcessPrintMssg.Print("一级：" + mssg);
            alarmvalueList.Add(processHXYLAlarmValueModel.HXYLAlarmValueModel(xmno, pointalarm.firstalarm, out mssg));
            alarmvalueList.Add(processHXYLAlarmValueModel.HXYLAlarmValueModel(xmno, pointalarm.secalarm, out mssg));
            alarmvalueList.Add(processHXYLAlarmValueModel.HXYLAlarmValueModel(xmno, pointalarm.thirdalarm, out mssg));
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<MDBDATA.Model.hxylalarmvalue> levelalarmvalue, MDBDATA.Model.hxyl resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (processHXYLPointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                //processHXYLPointAlarmBLL.ProcessPointAlarmfilterInformation();
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public ProcessHXYLIDBLL processHXYLIDBLL = new ProcessHXYLIDBLL();
        public ProcessHXYLIDPointName processHXYLIDPointName = new ProcessHXYLIDPointName();
        public bool HXYLPointAlarm(List<MDBDATA.Model.hxyl> lc)
        {
            alarmInfoList = new List<string>();
            string pointname = "";
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format(" {0} ", DateTime.Now));
            ls.Add(string.Format(" {0} ", xmname));
            ls.Add(string.Format(" {0} ", "雨量--雨量计--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (MDBDATA.Model.hxyl cl in lc)
            {
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname))) return false;
                pointname = processHXYLIDPointName.HXYLPointname(xmno, cl.id, out mssg);
                cl.point_name = pointname;
                MDBDATA.Model.hxylpointalarmvalue pointvalue = TestPointAlarmValue(cl.point_name);
                List<MDBDATA.Model.hxylalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> HXYLDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        public void HXYLPointAlarm(MDBDATA.Model.hxyl data)
        {

            MDBDATA.Model.hxylpointalarmvalue pointvalue = TestPointAlarmValue(data.point_name);
            if (pointvalue == null) return;
            List<MDBDATA.Model.hxylalarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestPointAlarmfilterInformation(alarmList, data);
        }

        //public Tool.DTUDataTableHelper ProcessPointAlarmValue(string pointname, int xmno)
        //{

        //    MDBDATA.Model.HXYL pointvalue = TestPointAlarmValue(pointname);
        //    List<MDBDATA.Model.HXYLalarmvalue> alarmList = TestAlarmValueList(pointvalue);
        //    var model = new Tool.HXYLReportHelper.HXYLalarm();
        //    model.pointname = pointname;
        //    if (alarmList[2] != null)
        //    {
        //        model.third_acdisp = alarmList[2].acdeep;
        //        model.third_thisdisp = alarmList[2].thisdeep;
        //        model.third_deep = alarmList[2].deep;
        //        model.third_rap = alarmList[2].rap;
        //    }
        //    if (alarmList[1] != null)
        //    {

        //        model.sec_acdisp = alarmList[1].acdeep;
        //        model.sec_thisdisp = alarmList[1].thisdeep;
        //        model.sec_deep = alarmList[1].deep;
        //        model.sec_rap = alarmList[1].rap;
        //    }
        //    if (alarmList[0] != null)
        //    {

        //        model.first_acdisp = alarmList[0].acdeep;
        //        model.first_thisdisp = alarmList[0].thisdeep;
        //        model.first_deep = alarmList[0].deep;
        //        model.first_rap = alarmList[0].rap;


        //    }


        //    return model;
        //}


    }
}
