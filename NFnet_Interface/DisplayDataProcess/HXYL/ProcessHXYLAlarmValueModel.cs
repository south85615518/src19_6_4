﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;
//using NFnet_BLL.DisplayDataProcess.MCUBKG;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLAlarmValueModel
    {
        public ProcessAlarmBLL processMCUAlarmValueBLL = new ProcessAlarmBLL();
        public MDBDATA.Model.hxylalarmvalue HXYLAlarmValueModel(int xmno, string alarmname, out string mssg)
        {
            var processMCUAlarmValueModelGetModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (processMCUAlarmValueBLL.ProcessAlarmModelGetByName(processMCUAlarmValueModelGetModel, out mssg))
            {
                return processMCUAlarmValueModelGetModel.model;
            }
            return new MDBDATA.Model.hxylalarmvalue();
        }
    }
}
