﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{

   public class ProcessHXYLPointBLL
    {
       public ProcessHXYLBLL hXYLBLL = new ProcessHXYLBLL();
       public List<string> HXYLPoint(int xmno,out string mssg)
       {
           NFnet_BLL.DisplayDataProcess.HXYL.ProcessHXYLBLL.HXYLPointLoadModel model = new ProcessHXYLBLL.HXYLPointLoadModel(xmno);
           if (hXYLBLL.HXYLPointLoad(model, out mssg))
               return model.pointnamelist;
           return new List<string>();
       }
    }
}
