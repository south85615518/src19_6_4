﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLDATALoad
    {
        public ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public static string mssg = "";
        public List<MDBDATA.Model.hxyl> HXYLDATALoad(DateTime maxTime,out string mssg)
        {
            var processHXYLDATALoadModel = new ProcessHXYLBLL.ProcessHXYLDATALoadModel(maxTime);
            if (processHXYLBLL.ProcessHXYLDATALoad(processHXYLDATALoadModel, out mssg))
            {
                return processHXYLDATALoadModel.ls;
            }
            return new List<MDBDATA.Model.hxyl>();
        }
    }
}
