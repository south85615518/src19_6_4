﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{

   public class ProcessHXYLIDPointBLL
    {
       public ProcessHXYLBLL hXYLBLL = new ProcessHXYLBLL();
       public string HXYLPointIDPoint(int xmno,string id,out string mssg)
       {
           NFnet_BLL.DisplayDataProcess.HXYL.ProcessHXYLBLL.HXYLPointmodel model = new ProcessHXYLBLL.HXYLPointmodel(xmno,id);
           if (hXYLBLL.HXYLPointname(model, out mssg))
               return model.id;
           return "";
       }
    }
}
