﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
   public class ProcessHXYLDataRqcxConditionCreateDay
    {
       public ProcessHXYLData processHXYLData = new ProcessHXYLData();
       public ResultDataRqcxConditionCreateCondition ResultDataRqcxConditionCreateDay(string startTime, string endTime, QueryType QT, string unit, string xmname, DateTime maxTime)
       {
           var resultDataRqcxConditionCreateCondition = new ResultDataRqcxConditionCreateCondition(startTime, endTime, QT, unit, xmname, maxTime);
           processHXYLData.ProcessResultDataRqcxConditionCreateDay(resultDataRqcxConditionCreateCondition);
           return resultDataRqcxConditionCreateCondition;
       }
    }
}
