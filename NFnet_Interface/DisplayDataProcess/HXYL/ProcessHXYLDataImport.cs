﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.HXYL;
namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLDataImport
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public static string mssg = "";
        public static void HXYLDataDeSerialize(string hxyldatastr)
        {
            if (hxyldatastr.IndexOf("_") != -1)
                hxyldatastr = hxyldatastr.Substring(0, hxyldatastr.LastIndexOf("_"));
            List<MDBDATA.Model.hxyl> ls = jss.Deserialize<List<MDBDATA.Model.hxyl>>(hxyldatastr);
            foreach (var model in ls)
            {
                //processHXYLBLL.ProcessHXYLAddMDB(model,out mssg);
            }
        }
    }
}
