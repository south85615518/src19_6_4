﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessHXYLMaxTime
    {
        public ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public DateTime HXYLMaxTime(string pointnamenostr, out string mssg)
        {
            var model = new ProcessHXYLBLL.ProcessHXYLMaxTimeModel(pointnamenostr);
            if (processHXYLBLL.ProcessHXYLMaxTime(model, out mssg))
            {
                return model.maxTime;
            }
            return new DateTime();
        }
    }
}
