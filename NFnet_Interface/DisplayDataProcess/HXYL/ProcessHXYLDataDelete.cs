﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLDataDelete
    {
        public ProcessHXYLData processBKGHXYLData = new ProcessHXYLData();
        public bool HXYLDataDelete(string xmname, string id, DateTime dt,out string mssg)
        {
            var dataDeleteCondition = new DataDeleteCondition(xmname,id,dt);
            return processBKGHXYLData.ProcessHXYLDataDelete(dataDeleteCondition,out mssg);
        }
    }
}
