﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLAlarmModelListGet
    {
        public ProcessHXYLData processHXYLData = new ProcessHXYLData();
        public List<MDBDATA.Model.hxyl> HXYLAlarmModelListGet(List<string> pointnamelist, out string mssg)
        {
            var processMCUAngleAlarmModelListGetModel = new ProcessHXYLData.ProcesshxylAlarmModelListGetModel(pointnamelist);
            if (processHXYLData.ProcesshxylAlarmModelListGet(processMCUAngleAlarmModelListGetModel, out mssg))
            {
                return processMCUAngleAlarmModelListGetModel.model;
            }
            return new List<MDBDATA.Model.hxyl>();
        }
    }
}