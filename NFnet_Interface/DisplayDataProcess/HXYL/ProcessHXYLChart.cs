﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessHXYLChart
    {
        public ProcessHXYLChartBLL hxylchartbll = new ProcessHXYLChartBLL();
        public List<serie> SerializestrHXYL(object sql, object xmno, object pointname,out string mssg)
        {
            ExceptionLog.ExceptionWrite("_项目编号:"+xmno);
            var serializestrBKGMCUModel = new SerializestrSWCondition(sql, xmno, pointname, 0);

            if (ProcessHXYLChartBLL.ProcessSerializestrHXYL(serializestrBKGMCUModel, out mssg))
                return serializestrBKGMCUModel.series;
            return new List<serie>();


        }
    }
}
