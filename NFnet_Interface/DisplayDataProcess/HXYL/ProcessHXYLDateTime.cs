﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLDateTime
    {
        public ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();

        public MDBDATA.Model.hxyl HXYLDateTime(int pointid,DateTime dt,out string mssg)
        {
            var HXYLDataTimeCondition = new ProcessHXYLBLL.HXYLDataTimeCondition(pointid, dt);
            if (!processHXYLBLL.ProcessHXYLDataTime(HXYLDataTimeCondition, out mssg)) return new MDBDATA.Model.hxyl();
            return HXYLDataTimeCondition.model;

        }


    }
}
