﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLPreAcVal
    {
        public ProcessHXYLData processHXYLData = new ProcessHXYLData();
        public MDBDATA.Model.hxyl HXYLPreAcVal(MDBDATA.Model.hxyl model,out string mssg)
        {
            var preAcValModel = new ProcessHXYLData.PreAcValModel(model);
            if(processHXYLData.PreAcVal(preAcValModel,out  mssg))
            {
                return preAcValModel.pretimemodel;
            }
            return new MDBDATA.Model.hxyl();
        }
    }
}
