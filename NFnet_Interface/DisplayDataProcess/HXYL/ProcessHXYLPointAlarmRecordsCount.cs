﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLPointAlarmRecordsCount
    {
        public int PointAlarmRecordsCount(string xmname,int xmno,string searchstring,out string mssg)
        {

            ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
            var processPointAlarmRecordsCountModel = new ProcessPointAlarmBLL.ProcessAlarmRecordsCountModel(xmname, xmno, searchstring);
            if (pointAlarmBLL.ProcessPointAlarmRecordsCount(processPointAlarmRecordsCountModel, out mssg))
            {
                return Convert.ToInt32(processPointAlarmRecordsCountModel.totalCont);

            }
            else
            {
                //计算结果数据数量出错信息反馈
                return 0;
            }
        }
    }
}
