﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;


namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLAlarm
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool Alarm(MDBDATA.Model.hxylpointalarmvalue model, out string mssg)
        {
            return pointAlarmBLL.ProcessAlarm(model,out mssg); 
        }
    }
}
