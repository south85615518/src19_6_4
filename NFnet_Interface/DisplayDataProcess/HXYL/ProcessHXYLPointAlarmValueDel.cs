﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;
//using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLPointAlarmValueDel
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueDel( MDBDATA.Model.hxylpointalarmvalue model ,out string mssg)
        {
            
            pointAlarmBLL.ProcessAlarmDel(model, out mssg);
        }
    }
}
