﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;
//using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLAlarmDel
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmDel(int xmno, string name,out string mssg)
        {
            var processAlarmDelModel = new ProcessAlarmBLL.ProcessAlarmDelModel( xmno,name);
            return alarmBLL.ProcessAlarmDel(xmno,name, out mssg);
        }
    }
}
