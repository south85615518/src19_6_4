﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLPointAlarmModel
    {
        public ProcessPointAlarmBLL processMCUPointAlarmBLL = new ProcessPointAlarmBLL();
        public MDBDATA.Model.hxylpointalarmvalue HXYLPointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processMCUPointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processMCUPointAlarmBLL.ProcessPointAlarmModelGet(processMCUPointAlarmModel, out mssg))
            {
                return processMCUPointAlarmModel.model;
            }
            return new MDBDATA.Model.hxylpointalarmvalue();
        }
    }
}
