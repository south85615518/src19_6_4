﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.HXYL;

namespace NFnet_Interface.DisplayDataProcess.HXYL
{
    public class ProcessHXYLAlarmDataCount
    {
        public ProcessHXYLData processHXYLData = new ProcessHXYLData();
        //public string mssg = "";
        public int HXYLAlarmDataCount(string xmname,List<string>idlist,out string mssg)
        {
            var getAlarmTableContModel = new ProcessHXYLData.GetAlarmTableContModel(idlist);
            if (processHXYLData.GetAlarmTableCont(getAlarmTableContModel, out mssg))
            {
                return getAlarmTableContModel.cont;
            }
            return 0;
        }
    }
}
