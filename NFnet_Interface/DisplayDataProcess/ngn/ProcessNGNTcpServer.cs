﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Tool;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_Interface.UserProcess;
using System.Threading;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.ngn;
using NFnet_Interface.DisplayDataProcess.ngn;


namespace NFnet_BLL.DisplayDataProcess
{
    public class ProcessNGNTcpServer
    {
        public static TcpListener tcpListener = null;
        public delegate void ClientConnectEvent(IAsyncResult ar);
        private event ClientConnectEvent conEvent;
        public System.Timers.Timer timerrestart = new System.Timers.Timer(3600000);
        public System.Timers.Timer timerresInspection = new System.Timers.Timer(600000);
        public static List<TcpListener> tcpListenerdts = new List<TcpListener>();
        public static string NGNtcpportpath = "";
        public void NGNTcpServerStart(string NGNportpath)
        {
            try
            {
                TcpServerStopTotal();
                List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(NGNportpath);
                TcpserverMutilStart(ltlif);
                NGNtcpportpath = NGNportpath;
                TimerTick();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite("服务器监听失败!");
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            TcpServerStopTotal();
        }
        #region 启动服务器监听
        public void TcpserverMutilStart(List<tcpclientIPInfo> ltlif)
        {
            try
            {
                foreach (tcpclientIPInfo tif in ltlif)
                {

                    TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(tif.ip), Convert.ToInt32(tif.port)));
                    tcpListenstart(tcpListener, tif.port, tif.ip);
                }
                ExceptionLog.NGNPortInspectionWrite("所有端口启动完成");
            }
            catch (Exception ex)
            {

            }


        }
        public string tcpListenstart(TcpListener tcpListener, int port, string ip)
        {
            ExceptionLog.NGNPortInspectionWrite("开始启动监听端口:" + port);
            string mss = "";
            try
            {
                conEvent += clientConnect;
                tcpListener.Start();
                ExceptionLog.NGNPortInspectionWrite(string.Format("端口{0}监听成功!{1}", port, DateTime.Now));


                tcpListenerdts.Add(tcpListener);
                IAsyncResult ar;
                tcpListener.BeginAcceptSocket(new AsyncCallback(conEvent), tcpListener);
                conEvent -= clientConnect;

            }
            catch (Exception ex)
            {
                mss = ex.Message;
                ExceptionLog.NGNRecordWrite(string.Format("{0}端口监听出错!错误原因:" + ex.Message + "现在重启", port));
            }
            return mss;
        }
        public delegate void AcceptEvent(AsyncCallback ar, TcpListener tcpListener);
        public event AcceptEvent evt;
        public static int contconnect = 0;
        private void clientConnect(IAsyncResult ar)
        {

            try
            {

                TcpListener listener = (TcpListener)ar.AsyncState;
                if (listener == null) return;
                if (listener.Server == null) return;
                if (listener.Server.LocalEndPoint == null) return;
                Socket s = listener.EndAcceptSocket(ar);
                if (s == null || clientInfo(s) == null) return;
                string mssg = string.Format("远程客户端{0}上线 {1} ", clientInfo(s), DateTime.Now);
                ExceptionLog.NGNRecordWrite(mssg);
                conEvent += clientConnect;
                listener.BeginAcceptSocket(new AsyncCallback(conEvent), listener);
                conEvent -= clientConnect;
                if (s == null || clientInfo(s) == null) return;
                //double hcont = 0; string datastr = "";
                //NGNDataGet(s, out hcont, out datastr);
                //ReceiveMess(s, hcont, datastr);
                Thread t = new Thread(new ParameterizedThreadStart(DataImport));
                t.Start(s);
            }

            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("位置链接异步事件:" + ex.Message);
            }

        }


        #endregion
        #region 心跳包
        public void TimerTick()
        {
            timerresInspection.Elapsed += new System.Timers.ElapsedEventHandler(TcpPortActiveCheck);
            timerresInspection.Enabled = true;
            timerresInspection.AutoReset = true;
        }
        private static object UsingPrinterLocker = new object();
        private static object UsingHistoryContLocker = new object();
        List<TcpListener> dtsDelete = new List<TcpListener>();
        public bool IsSocketAlive(Socket s)
        {
            //byte[] buff = new byte[100];

            if (s == null) return false;
            try
            {

                //s.Send(Encoding.UTF8.GetBytes("心跳包测试"));

                ExceptionLog.NGNCommendRecordWrite(string.Format("向{0}发送命令{1} {2}", clientInfo(s), "心跳包测试", DateTime.Now));
                if (clientInfo(s) != null)
                    return true;
                return false;

            }
            catch (Exception ex)
            {

                return false;
            }

        }

        #endregion
        #region 窗体控件信息显示
        public void ClientConnectFlush(string mssg)
        {
            //DateTime dt = DateTime.Now;

            //this.listBox2.Items.Add(mssg);


        }
        public void ClientDataFlush(string mssg)
        {

            //DateTime dt = DateTime.Now;

            //this.listBox1.Items.Add(mssg);


        }


        #endregion
        #region 数据通信
        /// <summary>
        /// 数据解析入库线程
        /// </summary>
        public class NGNdatabag
        {
            public Socket s { get; set; }
            public int cont { get; set; }
            public string datastr { get; set; }
            public NGNdatabag(Socket s)
            {
                this.s = s;

            }
        }
        public void DataImport(object obj)
        {
            Socket s = obj as Socket;
            //NGNdatabag bag = obj as NGNdatabag;
            double hcont = 0; string datastr = "";
            NGNDataGet(s, out hcont, out datastr);
            //ReceiveMess(s, hcont, datastr);

        }
        public void ReceiveMess(Socket s, double cont, string NGNdata)
        {
            int port = Convert.ToInt32(clientInfo(s).ToString().Split(':')[1]);
            ExceptionLog.NGNRecordWrite(string.Format("{2} 开始解析地下水位数据...【{0}】,历史数据条数【{1}】", NGNdata, cont, clientInfo(s)), port);
            //对方发送文件
            DateTime dt = DateTime.Now;
            string client = "";
            try
            {

                StringBuilder datastr = new StringBuilder(256);
                int count;
                byte[] b = new byte[4096];
                s.ReceiveTimeout = 300000;// -1;
                client = clientInfo(s);
                //有历史数据，NGN没有进行本次采集
                if (cont > 0 && NGNdata == "")
                {
                    ExceptionLog.NGNRecordWrite("NGN在规定时间内没有进行采集，而还存在历史数据，现在发送获取历史数据指令...", port);
                    return;

                    s.Send(Encoding.UTF8.GetBytes(string.Format("get data:{0}", Convert.ToInt32(cont))));
                    ExceptionLog.NGNCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", client, string.Format("get data:{0}", Convert.ToInt32(cont)), DateTime.Now));
                    if (cont == 1) cont = 2;
                    ReceiveMess(s, cont, "1");
                    return;
                }
                //无历史数据，NGN发回了本次采集的数据，直接存库
                else if ((cont == 0 || cont == 1) && NGNdata != "")
                {
                    ExceptionLog.NGNRecordWrite(string.Format("NGN进行了本次采集，不存在历史数据,【{0}】直接入库...", NGNdata), port);
                    IsThisSendFinish(client, NGNdata, port, 0);
                    return;
                }
                //有历史数据，NGN只发回了本次数据，发送历史数据获取指令，此时本次采集数据也是历史数据
                else if (cont > 1 && NGNdata != "")
                {
                    if (NGNdata == "1")
                        ExceptionLog.NGNRecordWrite("现在发送历史数据获取指令,获取完整数据...", port);
                    else
                        ExceptionLog.NGNRecordWrite("NGN进行了本次采集，而发现中间还有漏掉没有上传到平台的数据...", port);

                    s.Send(Encoding.UTF8.GetBytes(string.Format("get data:{0}", Convert.ToInt32(cont))));
                    ExceptionLog.NGNCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), string.Format("get data:{0}", Convert.ToInt32(cont)), DateTime.Now));
                    while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                    {
                        var str = Encoding.UTF8.GetString(b, 0, count);
                        ExceptionLog.NGNRecordWrite(string.Format("接收到数据【{0}】", str.TrimEnd()), port);
                        datastr.Append(str);
                        if (datastr.ToString().IndexOf("the end") != -1 && datastr.ToString().IndexOf("the data") == -1 && cont > 0)
                        {
                            datastr = new StringBuilder();
                        }
                        if (IsThisSendFinish(client, datastr.ToString(), port, cont))
                        {

                            cont = 0;
                            ExceptionLog.NGNRecordWrite(string.Format("数据【{0}】入库完成，清零历史数据记录数{1}", datastr.ToString().TrimEnd(), cont), port);
                            datastr = new StringBuilder(256);
                            return;
                        }

                        b = new byte[4096];
                        s.ReceiveTimeout = 300000;
                    }
                }

                ExceptionLog.NGNRecordWrite("数据接收完成,现在退出...", port);
            }
            catch (Exception ex)
            {

                ExceptionLog.NGNRecordWrite(string.Format("{0}  接收出错 ,出错原因:{1}", clientInfo(s), ex.Message), port);

                return;
            }
            finally
            {

                if (NGNdata.IndexOf("od") != -1)
                {
                    ExceptionLog.NGNRecordWrite(string.Format("直接存储本次采集数据【{0}】...", NGNdata), port);
                    IsThisSendFinish(client, NGNdata, port, 0);

                }
            }

        }

        public bool IsThisSendFinish(string client, string str, int port, double cont)
        {
            if (str.IndexOf("the all") != -1 || str.IndexOf("the end") != -1)
            {
                try
                {
                    ExceptionLog.NGNRecordWrite(string.Format("开始解析数据【{0}】", str.TrimEnd()), port);
                    if (str.IndexOf("the all") == -1 && cont > 0 && str.IndexOf("the data") == -1)
                    {
                        ExceptionLog.NGNRecordWrite(string.Format("对于历史条数记录有{0}条而字符串{1}中既不包含 the all，the data两个标志字符的数据不是NGN数据", cont, str), port);
                        return false;
                    }
                    ExceptionLog.NGNRecordWrite(string.Format("开始导入数据【{0}】", str.TrimEnd()), port);
                    NGNDATAImport(str, port);
                    ExceptionLog.NGNRecordWrite(string.Format("{1} 接收{0}发来的数据【{2}】  ", client, DateTime.Now, str.TrimEnd()), port);
                    return true;
                }
                catch (Exception ex)
                {
                    ExceptionLog.NGNRecordWrite("数据录入出错,错误信息:" + ex.Message + ex.StackTrace, port);
                    return true;
                }
            }
            return false;
        }

        public bool GetNGNIDFromFirstData(Socket s, string str, int port, out string addressno, out string point, out int xmno)
        {

            xmno = 0;
            point = "";
            addressno = "";
            try
            {
                addressno = str.Split(':')[0];
                string[] attrs = str.Split('\r');
                int i = 0;
                //string pointname = "";
                double line = 0;
                double holedepth = 0;
                xmno = GetNGNXmnoFromPort(port);
                //for (i = 1; i < attrs.Length - 1; i++)
                //{

                string datastr = "";
                bool isdatestr = false;
                foreach (string tmpstr in attrs)
                {

                    if (tmpstr.Length < 19) continue;
                    if (tmpstr.IndexOf("2017/") != -1)
                    {

                        datastr = tmpstr.Substring(tmpstr.IndexOf("2017/"), 19);
                        isdatestr = true;
                        break;
                    }
                    else if (tmpstr.IndexOf("2018/") != -1)
                    {
                        datastr = tmpstr.Substring(tmpstr.IndexOf("2018/"), 19);
                        isdatestr = true;
                        break;
                    }
                }
                if (!isdatestr) return false;
                //str = attrs[0];
                //if (str.IndexOf("2017/") == -1 || str.IndexOf("2018/") == -1) return false;

                string[] dataary = datastr.Split(' ');
                ExceptionLog.NGNRecordWrite("获取到的NGN时间为" + datastr, port);
                NGNModulePointName(xmno, port, addressno, out point);
                DateTime NGNtime = Convert.ToDateTime(dataary[0] + " " + dataary[1]);
                if (Math.Abs((DateTime.Now - NGNtime).TotalMinutes) > 10)
                {

                    //FlushEvent += ClientDataFlush;();
                    ExceptionLog.NGNRecordWrite(string.Format("NGN当前时间是{1}系统时间是{2}现在向{0}发送时间校准指令", clientInfo(s), NGNtime, DateTime.Now), port);
                    //NGNDATAImport(datastr);
                    //this.listBox1.EndInvoke(ar);
                    //FlushEvent -= ClientDataFlush;
                    s.Send(Encoding.UTF8.GetBytes(string.Format("set rtc:{0},{1},{2},{3},{4},{5}", DateTime.Now.Year, DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"))));
                    ExceptionLog.NGNCommendRecordWrite(string.Format("向{0}发送命令{1} {2}", clientInfo(s), string.Format("set rtc:{0},{1},{2},{3},{4},{5}", DateTime.Now.Year, DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00")), DateTime.Now));
                    byte[] buf = new byte[4096];
                    int ct = 0;
                    int revc = 0;
                    s.ReceiveTimeout = 60000;
                    while ((ct = s.Receive(buf, SocketFlags.None)) > 0)
                    {

                        string strrev = Encoding.UTF8.GetString(buf, 0, ct);
                        buf = new byte[4096];
                        if (strrev.IndexOf("modify time success") != -1)
                        {
                            //FlushEvent += ClientDataFlush;
                            ExceptionLog.NGNRecordWrite(string.Format("校准{0}时间成功", clientInfo(s)), port);
                            return true;
                            //NGNDATAImport(datastr);
                            //this.listBox1.EndInvoke(ar);
                            //FlushEvent -= ClientDataFlush;
                        }
                        revc++;
                        if (revc > 3) return false;

                        //FlushEvent += ClientDataFlush;
                        ExceptionLog.NGNRecordWrite(string.Format("校准{0}时间返回{1}", clientInfo(s), strrev), port);
                        //return false;
                        //NGNDATAImport(datastr);
                        //this.listBox1.EndInvoke(ar);
                        //FlushEvent -= ClientDataFlush;

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite(string.Format("从{0}校准NGN的时间出错,错误信息：" + ex.Message, str == null ? "" : str), port);
                return false;
            }


        }
        public bool DataRec(Socket s, int port)
        {

            return false;

        }
        //服务器等待客户端的数据超时进行主动获取
        public void NGNDATARequestSend(Socket s, int port)
        {
            int i = 0;
            s.SendTimeout = 120000;
            IAsyncResult ar = null;
            //连续请求三次
            for (i = 0; i < 3; i++)
            {
                try
                {
                    //设置10秒等待时间

                    s.Send(Encoding.UTF8.GetBytes("set jiange:00,01,01"));
                    ExceptionLog.NGNRecordWrite(string.Format("向客户端{0}发送数据获取请求成功,现在等待客户端数据回传...", clientInfo(s)), port);

                    int count;
                    StringBuilder datastr = new StringBuilder(256);
                    byte[] b = new byte[4096];
                    //设置20秒等待时间
                    s.ReceiveTimeout = 180000;
                    while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                    {
                        var str = Encoding.UTF8.GetString(b, 0, count);
                        datastr.Append(str);
                        if (IsThisSendFinish("", datastr.ToString(), port, 0))
                        {
                            datastr = new StringBuilder(256);
                        }
                        b = new byte[4096];

                    }
                }
                catch (Exception ex)
                {
                    ExceptionLog.NGNRecordWrite(string.Format("向客户端{0}发送数据请求命令失败,失败原因{1} 请求次数{2}", clientInfo(s), ex.Message, i), port);
                    continue;
                }

                SocketClose(s);
                ExceptionLog.NGNRecordWrite(string.Format("向{0}  从{2}请求数据获取超时!设备采集时间间隔超过了规定时间或者设备已经损坏", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address));
            }
        }

        #endregion
        #region 数据处理
        /// <summary>
        /// 数据导入
        /// </summary>
        /// <param name="str"></param>
        public void NGNDATAImport(string datastr, int port)
        {

            //NGNDATADescode(port, datastr);

        }
        /// <summary>
        /// 数据校验
        /// </summary>
        /// <returns></returns>
        public bool messvalidate()
        {
            return true;
        }
        public string point_name = "";
        public int xmno = 0;
        public static string mssg = "";
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        #endregion
        #region 处理类
        public ProcessNGNCommendBLL processNGNCommendBLL = new ProcessNGNCommendBLL();
        public static ProcessDTUDATABLL processNGNDATABLL = new ProcessDTUDATABLL();
        public ProcessNGNDataBLL processNGNDataBLL = new ProcessNGNDataBLL();
        public bool NGNDataGet(Socket s, out double hcont, out string NGNdata)
        {

            int port = Convert.ToInt32(clientInfo(s).ToString().Split(':')[1]);
            NGNdata = "";
            string pointname = "";
            hcont = 0;
            double line = 0;
            double holedepth = 0;
            //要一条数据
            StringBuilder datastr = new StringBuilder(256);
            int count;
            byte[] bytearray = new byte[1024];
            int xmno = 0;
            string point_name = "";
            string module = "";
            //设置20秒等待时间
            s.ReceiveTimeout = 600000;
            List<byte> bytelist = new List<byte>();
            try
            {
                ExceptionLog.NGNRecordWrite(string.Format("等待{0}发回数据...  {1}", clientInfo(s), DateTime.Now), port);
                int cont = 0;
                while (true)
                {
                    cont = s.Receive(bytearray, 1024, SocketFlags.None);
                    if (cont == 0) break;
                    if (cont == 1024)
                    {
                        bytelist.AddRange(bytearray);
                        continue;
                    }
                    else
                    {
                        byte[] buff = bytearray.ToList().GetRange(0, cont).ToArray();
                        bytelist.AddRange(buff);
                        string str = Tool.com.HexHelper.ByteToHexString(bytelist.ToArray());

                        ExceptionLog.NGNRecordWrite(string.Format("接收到数据{0}{1}...  {2}", clientInfo(s), str, DateTime.Now), port);

                        List<byte[]> lb = Tool.com.ByteArrayHelper.ByteArrayToArrayList(bytelist.ToArray(), new byte[2] { 0xff, 0x1c });





                        foreach (var b in lb)
                        {

                            if (!(processNGNCommendBLL.ProcessNGNCommendCheck(b, out mssg)&&Tool.com.CRC16Helper.IsDataCorrectCRC16(b, out mssg)))
                            {
                                ExceptionLog.NGNRecordWrite(mssg, port);
                                continue; 
                            }
                            ProcessNGNCommendBLL.GroudWater groundwater = new ProcessNGNCommendBLL.GroudWater();
                            processNGNCommendBLL.PrcoessNGNDataDescode(b, null, out groundwater);
                            int currentxmno = GetNGNXmnoFromPort(port);
                            
                            //根据设备号和端口获取点名
                            NGNModulePointName(currentxmno, port, groundwater.id.ToString(), out point_name, out line, out holedepth,out xmno);
                            //计算当前水位值
                            ExceptionLog.NGNRecordWrite(string.Format("获取到设备号{0}的点名为{1}线长{2}初始水位{3}", groundwater.id, point_name, line, holedepth), port);
                            if (point_name == "" || point_name == "0" || string.IsNullOrEmpty(point_name)) continue;
                            DTU.Model.dtudata model = new DTU.Model.dtudata
                            {
                                point_name = point_name,
                                mkh = groundwater.id,
                                deep = Convert.ToDouble((Convert.ToDouble(groundwater.deep) / 1000).ToString("0.000")),
                                time = Convert.ToDateTime(groundwater.dt) > DateTime.Now ? DateTime.Now : Convert.ToDateTime(groundwater.dt),
                                xmno = xmno,
                                dtutime = DateTime.Now,
                                groundElevation = Convert.ToDouble((Convert.ToDouble(groundwater.deep) / 1000).ToString("0.000")),
                                degree = Math.Round(groundwater.degree, 3, MidpointRounding.AwayFromZero)

                            };
                            if (model.deep != 0)
                                processNGNDATABLL.ProcessDTUDATAInsertBLL(model, out mssg);
                            if (model.degree != 0)
                                processNGNDataBLL.ProcessNGNDataDegreeUpdate(model, out mssg);
                            if (mssg != "")
                                ExceptionLog.NGNRecordWrite(mssg, port);


                        }
                    }
                }


               
                
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite(string.Format("NGN数据接收出错{0}...  {1},错误信息:"+ex.Message+"位置:"+ex.StackTrace, clientInfo(s), DateTime.Now), port);
                ExceptionLog.NGNRecordWrite(string.Format("等待{0}发回数据超时...  {1}", clientInfo(s), DateTime.Now), port);
                return true;
            }
            return true;

        }

        public static ProcessDTUPortXm processNGNPortXm = new ProcessDTUPortXm();

        public static int GetNGNXmnoFromPort(int port)
        {
            return processNGNPortXm.DTUPortXm(port, out mssg).xmno;
        }
        public static ProcessDTUPortXmname processNGNPortXmname = new ProcessDTUPortXmname();
        public static string GetNGNXmnameFromPort(int port)
        {
            return processNGNPortXmname.DTUPortXm(port, out mssg);
        }
        public static ProcesNGNPointModule processNGNPointModule = new ProcesNGNPointModule();
        public static bool NGNModulePointName(int xmno, int port, string addressno, out string pointname, out double line, out double holedepth, out int correspondxmno)
        {
            DTU.Model.dtu model = processNGNPointModule.NGNPointModule(xmno, port, addressno, out mssg);
            ////ProcessPrintMssg.Print(mssg);
            ExceptionLog.NGNRecordWrite(mssg,port);
            pointname = model.point_name;
            line = model.line;
            holedepth = model.holedepth;
            correspondxmno = model.xmno;
            return true;
        }
        public static ProcessDTUPointModuleWhithoutOd processNGNPointModuleWhithoutOd = new ProcessDTUPointModuleWhithoutOd();
        public static bool NGNModulePointName(int xmno, int port, string addressno, out string pointname)
        {
            DTU.Model.dtu model = processNGNPointModuleWhithoutOd.DTUPointModule(xmno, port, addressno, out mssg);
            pointname = model.point_name;
            return true;
        }
        public string clientInfo(Socket s)
        {
            return string.Format("{0}:{1}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, s.LocalEndPoint.ToString().Split(':')[1]);
        }
        #endregion
        #region 关闭/重启
        //重新开启监听
        //停止服务
        public void TcpServerStopTotal()
        {
            try
            {
                foreach (TcpListener tcpListener in tcpListenerdts)
                {
                    //if (tcpListenerdts[tcpListener] != null)
                    //{
                    //    //关闭连接
                    //    Socket s = tcpListenerdts[tcpListener];
                    //    s.Shutdown(SocketShutdown.Both);
                    //    //s.Dispose();
                    //    s = null;
                    //}
                    //tcpListener.Server.LocalEndPoint;


                    tcpListener.Server.Close();
                    tcpListener.Stop();



                }
                tcpListenerdts.Clear();
            }
            catch (Exception ex)
            {


            }
        }
        public static int inspectCont = 0;
        public void TcpPortActiveCheck(object source, System.Timers.ElapsedEventArgs e)
        {
            ExceptionLog.NGNPortInspectionWrite(string.Format("{0}开始执行第{1}次端口巡检", DateTime.Now, ++inspectCont));
            List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(NGNtcpportpath);
            foreach (tcpclientIPInfo tif in ltlif)
            {
                if (!ProcessTcpStateServer.IsUsedIPEndPoint(tif.port))
                {
                    ExceptionLog.NGNPortInspectionWrite(string.Format("巡检发现监听端口{0}已经关闭!现在启动", tif.port));
                    TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(tif.ip), Convert.ToInt32(tif.port)));
                    tcpListenstart(tcpListener, tif.port, tif.ip);
                }
            }

        }
        public void TcpServerStop(TcpListener tcpListener)
        {
            try
            {


                tcpListener.Server.Close();
                tcpListener.Stop();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite("关闭服务器端口{0}监听出错,错误信息：" + ex.Message);

            }
        }
        private object useSocketLocker = new object();
        public void SocketClose(Socket s)
        {
            System.Threading.Monitor.Enter(useSocketLocker);
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite("关闭远程客户端出错!出错原因:" + ex.Message);
            }
            finally
            {
                System.Threading.Monitor.Exit(useSocketLocker);
            }
        }
        /// <summary>
        /// 重启单个服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        /*oid TcpListenerRestart(TcpListener listenser)
        {
            List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad("端口配置文件\\setting.txt");
            int port = tcpListenerport[listenser];
            var ip = (from m in ltlif where m.port == port select m.ip).ToList()[0];
            TcpServerStop(listenser);
            try
            {
                TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(port)));
                tcpListenstart(tcpListener, port);
                ExceptionLog.NGNRecordWrite(string.Format("重启服务器端口{0}成功!", port));
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNRecordWrite(string.Format("重启服务器端口{0}出错，错误信息:" + ex.Message, port));
            }

        }*/
        /// <summary>
        /// 重启所有服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        public void TcpListenerRestartTotal(object source, System.Timers.ElapsedEventArgs e)
        {
            string NGNportpath = Convert.ToString(source);
            timerrestart.Stop();
            //timer.Enabled = false;
            timerrestart.Enabled = false;
            //timer.AutoReset = false;
            //ExceptionLog.NGNRecordWrite("开始重启监听..." + NGNtcpportpath);
            //this.comboBox2.SelectedIndex = 1;
            //TcpServerStopTotal();
            //EncodingSet();
            //TcpServerStopTotal();
            //List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(NGNtcpportpath);
            //"D:\\华南水电\\NGNServer\\TcpSever_MYSQL+\\TcpSever\\bin\\debug\\端口配置文件\\setting.txt"
            //ExceptionLog.NGNRecordWrite("开始启动监听..."+string.Join(":",));
            //TcpserverMutilStart(ltlif);
            //TimerTick();
            timerrestart.Elapsed -= new System.Timers.ElapsedEventHandler(TcpListenerRestartTotal);
            //NGNTcpServerStart(NGNtcpportpath);
            TcpServerStopTotal();
            //EncodingSet();
            List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(NGNportpath);
            //"D:\\华南水电\\NGNServer\\TcpSever_MYSQL+\\TcpSever\\bin\\debug\\端口配置文件\\setting.txt"
            //ExceptionLog.NGNRecordWrite("开始启动监听..."+string.Join(":",));
            TcpserverMutilStart(ltlif);
            NGNtcpportpath = NGNportpath;
        }





        //private void TcpServerReStart(string NGNportpath)
        //{
        //    string mss = "";
        //    try
        //    {
        //        //从端口配置文件读取端口
        //        TcpServerStopTotal();
        //        List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(NGNportpath);
        //        foreach (tcpclientIPInfo tcpclientIPInfo in ltlif)
        //        {
        //            try {

        //            }
        //        }
        //        tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(this.textBox1.Text), Int32.Parse(this.textBox2.Text)));
        //        mss = this.start();
        //        this.label4.Text = mss;
        //        if (mss.IndexOf("套接字地址") != -1)
        //        { MessageBox.Show("端口被占用！"); }
        //        else if ((mss.IndexOf("有效值的范围") != -1))
        //        {
        //            MessageBox.Show("请设置有效端口！");
        //        }
        //        else
        //        {
        //            MachineSettingInfoWrite();
        //            MessageBox.Show("重启成功！");
        //            this.timer1.Start();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if ((ex.Message.IndexOf("有效值的范围") != -1))
        //        {
        //            MessageBox.Show("请设置有效端口！");
        //        }
        //        else
        //            MessageBox.Show(ex.Message);
        //        throw (ex);
        //    }


        //}
        #endregion

    }
}
