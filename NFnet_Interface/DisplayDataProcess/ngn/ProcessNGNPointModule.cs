﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.ngn
{
    public class ProcesNGNPointModule
    {
        public ProcessDTUPointBLL processDTUPointBLL = new ProcessDTUPointBLL();
        public DTU.Model.dtu NGNPointModule(int xmno, int port,string addressno ,out string mssg)
        {
            var processDTUPointModuleModel = new NFnet_BLL.DisplayDataProcess.WaterLevel.ProcessDTUPointBLL.ProcessNGNPointModuleModel(xmno, port,addressno);
            if (processDTUPointBLL.ProcessNGNPointModule(processDTUPointModuleModel, out mssg))
            {
                return processDTUPointModuleModel.model;
            }
            return new DTU.Model.dtu();
        }
    }
}
