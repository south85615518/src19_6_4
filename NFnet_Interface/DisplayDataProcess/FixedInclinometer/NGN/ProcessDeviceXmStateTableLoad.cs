﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using System.Data;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess
{
    public class ProcessFixedInclinometerXmStateTableLoad
    {

        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public DataTable FixedInclinometerXmStateTableLoad(int xmno, string xmname,string unitname ,string colName, int pageIndex, int rows, string sord, out string mssg)
        {
            var model = new XmStateTableLoadCondition(pageIndex, rows, xmno,xmname,unitname, colName, sord);
            if (processFixed_Inclinomater_deviceBLL.ProcessXmStateTableLoad(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }


    }
}
