﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainNameLoad
    {
        public ProcessFixed_Inclinomater_chainBLL processFixed_Inclinomater_chainBLL = new ProcessFixed_Inclinomater_chainBLL();
        public List<string>  ChainNameLoad(int xmno,out string mssg)
        {
            
            var model = new ProcessFixed_Inclinomater_chainBLL.ChainNameLoadModel(xmno);
            if(processFixed_Inclinomater_chainBLL.ChainNameLoad(model,out mssg))
                return model.chainnamelsit;
            return new List<string>();
        }
    }
}
