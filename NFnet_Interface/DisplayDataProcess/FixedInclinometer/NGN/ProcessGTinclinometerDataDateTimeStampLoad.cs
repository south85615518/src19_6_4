﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTInclinometerDataDateTimeStampLoad
    {
        public ProcessGTinclinometerDateTimeStampBLL processDateTimeStampBLL = new ProcessGTinclinometerDateTimeStampBLL();
        public PointDataDateTimeStamp PointDataDateTimeStampLoad(int xmno, string pointname, PointDataDateTimeStamp timestamp, out string mssg)
        {
            var processPointDataDateTimeStampLoadModel = new NFnet_BLL.DisplayDataProcess.DateTimeStamp.ProcessGTinclinometerDateTimeStampBLL.ProcessPointDataDateTimeStampLoadModel(xmno, pointname, timestamp);
            processDateTimeStampBLL.ProcessPointDataDateTimeStampLoad(processPointDataDateTimeStampLoadModel,out mssg);
            return processPointDataDateTimeStampLoadModel.timestamp;
        }
    
    }
}
