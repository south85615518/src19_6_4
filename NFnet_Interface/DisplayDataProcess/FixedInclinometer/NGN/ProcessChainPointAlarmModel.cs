﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessChainPointAlarmModel
    {
        public ProcessFixed_Inclinomater_chainBLL processFixed_Inclinomater_chainBLL = new ProcessFixed_Inclinomater_chainBLL();
        public global::NGN.Model.fixed_inclinometer_chain ChainPointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processChainPointAlarmModel = new ProcessFixed_Inclinomater_chainBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processFixed_Inclinomater_chainBLL.ProcessPointAlarmModelGet(processChainPointAlarmModel, out mssg))
            {
                return processChainPointAlarmModel.model;
            }
            return new global::NGN.Model.fixed_inclinometer_chain();
        }
    }
}
