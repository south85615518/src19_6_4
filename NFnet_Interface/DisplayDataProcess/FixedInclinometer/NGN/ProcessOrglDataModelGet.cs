﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public  class ProcessOrglDataModelGet
    {
        public ProcessFixed_Inclinometer_orglDataBLL orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public NGN.Model.fixed_inclinometer_orgldata OrglDataModelGet(int xmno, string point_name,double deepth, DateTime time,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.fixed_inclinometer_orgldataModelGetModel( xmno,  point_name,deepth , time);
            if (orglDataBLL.GetModel(model, out mssg))
                return model.model;
            return new NGN.Model.fixed_inclinometer_orgldata();
        }
    }
}
