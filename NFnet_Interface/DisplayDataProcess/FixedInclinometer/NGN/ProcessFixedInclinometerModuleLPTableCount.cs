﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixedInclinometerModuleLPTableCount
    {
        public ProcessDTUModuleLPBLL processDTUModuleLPBLL = new ProcessDTUModuleLPBLL();
        public int FixedInclinometerModuleLPTableCount(int xmno, string searstring, out string mssg)
        {
            var processDTUModuleLPTableCountModel = new ProcessDTUModuleLPBLL.ProcessDTUModuleLPTableCountModel(xmno, searstring);
            if (processDTUModuleLPBLL.ProcessFixedInclinometerModuleLPTableCount(processDTUModuleLPTableCountModel, out mssg))
            {
                return processDTUModuleLPTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
