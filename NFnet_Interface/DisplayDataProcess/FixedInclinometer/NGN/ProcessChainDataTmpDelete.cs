﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessChainDataTmpDelete
    {
        public ProcessFixed_Inclinometer_chainDataBLL chainDataBLL = new ProcessFixed_Inclinometer_chainDataBLL();
        public int ChainDataTmpDelete(int xmno,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_chainDataBLL.deletetmpmodel(xmno);
            if (chainDataBLL.delete_tmp(model, out mssg))
                return model.cont;
            return 0;
        }
    }
}
