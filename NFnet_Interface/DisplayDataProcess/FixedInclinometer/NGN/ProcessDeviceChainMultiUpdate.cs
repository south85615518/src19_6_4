﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessDeviceChainMultiUpdate
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public bool DeviceChainMultiUpdate(string pointNameStr, NGN.Model.fixed_inclinometer_chain model,out string mssg)
        {
            var chainMultiUpdateModel = new ProcessFixed_Inclinomater_deviceBLL.ChainMultiUpdateModel(pointNameStr, model);
            return processFixed_Inclinomater_deviceBLL.ChainMultiUpdate(chainMultiUpdateModel, out mssg);
                
        }
    }
}
