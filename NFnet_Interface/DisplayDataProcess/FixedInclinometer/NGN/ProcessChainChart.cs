﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer
{
    public class ProcessChainChart
    {
        public ProcessChainChartBLL bkgmuc = new ProcessChainChartBLL();
        public List<serie> SerializestrChain(object sql, object xmno, object pointname,out string mssg)
        {

            var serializestrChainModel = new SerializestrSWCondition(sql, xmno, pointname,0);

            if (ProcessChainChartBLL.ProcessSerializestrChain(serializestrChainModel, out mssg))
                return serializestrChainModel.series;
            return new List<serie>();


        }
    }
}
