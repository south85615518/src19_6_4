﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTInclinometerSurveyPointCYCDataAdd
    {
        public ProcessFixed_Inclinometer_orglDataBLL processResultDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public bool SurveyPointCYCDataAdd(int xmno, string pointname,int startcyc, int endcyc,out string mssg)
        {
            mssg = "";
            var model = new ProcessFixed_Inclinometer_orglDataBLL.AddSurveyDataModel(xmno, pointname, startcyc, endcyc);
            return processResultDataBLL.AddSurveyData(model,out mssg);
        }
    }
}
