﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessChainDeviceModelListGet
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public List<global::NGN.Model.fixed_inclinometer_device> ChainDeviceModelListGet(int xmno,string chainname,out string mssg)
        {
            var fixed_inclinometer_chainDeviceModelListGetModel = new ProcessFixed_Inclinomater_deviceBLL.fixed_inclinometer_chainDeviceModelListGetModel(xmno,chainname);
            if (processFixed_Inclinomater_deviceBLL.GetChainDeviceModelList(fixed_inclinometer_chainDeviceModelListGetModel, out mssg))
            {
                return fixed_inclinometer_chainDeviceModelListGetModel.modellist;
            }
            return new List<global::NGN.Model.fixed_inclinometer_device>();
        }
    }
}
