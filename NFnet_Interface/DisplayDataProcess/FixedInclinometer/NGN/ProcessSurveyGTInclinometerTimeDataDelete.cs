﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTInclinometerSurveyTimeDataDelete
    {
        public ProcessFixed_Inclinometer_orglDataBLL processResultDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public bool SurveyTimeDataDelete( int xmno, string pointname, int startcyc, int endcyc,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.DeleteModel(xmno,pointname,startcyc,endcyc);
            return processResultDataBLL.DeleteSurveyData(model, out mssg);
        }
    }
}
