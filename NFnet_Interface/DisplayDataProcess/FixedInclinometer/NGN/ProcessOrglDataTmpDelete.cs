﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessorglDataTmpDelete
    {
        public ProcessFixed_Inclinometer_orglDataBLL orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public int OrglDataTmpDelete(int xmno,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.deletetmpmodel(xmno);
            if (orglDataBLL.delete_tmp(model, out mssg))
                return model.cont;
            return 0;
        }
    }
}
