﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
   public class ProcessChainModelGet
    {
        public ProcessFixed_Inclinomater_chainBLL processFixed_Inclinomater_chainBLL = new ProcessFixed_Inclinomater_chainBLL();
        public NGN.Model.fixed_inclinometer_chain ChainModelGet(int xmno, int deviceno,  int port, out string mssg)
        {
            var model = new ProcessFixed_Inclinomater_chainBLL.fixed_inclinometer_chainModelGetModel(xmno,deviceno,port);
            if (processFixed_Inclinomater_chainBLL.GetModel(model, out mssg))
                return model.model;
            return new NGN.Model.fixed_inclinometer_chain();

        }
    }
}
