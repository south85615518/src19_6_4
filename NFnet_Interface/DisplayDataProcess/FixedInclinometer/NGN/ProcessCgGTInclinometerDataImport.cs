﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTInclinometerCgResultDataImport
    {
        public ProcessFixed_Inclinometer_orglDataBLL processResultDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public bool CgResultDataImport(int xmno, string point_name, int startcyc, int importcyc, out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.SurveyDataAddModel(xmno, point_name, startcyc, importcyc);
            return processResultDataBLL.ProcessCgDataImport(model,out mssg);
       
            return false;
        }
    }
}
