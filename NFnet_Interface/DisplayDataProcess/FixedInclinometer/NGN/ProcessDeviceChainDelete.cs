﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessDeviceChainDelete
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public bool DeviceChainDelete(int xmno, string chainname,out string mssg)
        {
            var model = new ProcessFixed_Inclinomater_deviceBLL.DeleteChainModel(xmno,chainname);
            return processFixed_Inclinomater_deviceBLL.DeleteChain(model,out mssg);
        }
    }

}
