﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessChainDataMaxTime
    {
        public ProcessFixed_Inclinometer_chainDataBLL processFixed_Inclinometer_chainDataBLL = new ProcessFixed_Inclinometer_chainDataBLL();
        public DateTime ChainDataMaxTime(int xmno,string chainname,out string mssg)
        {
            var processFixedInclinometerMaxTimeModel = new ProcessFixed_Inclinometer_chainDataBLL.ProcessFixedInclinometerMaxTimeModel(xmno,chainname);
            if (processFixed_Inclinometer_chainDataBLL.ProcessFixedInclinometerMaxTime(processFixedInclinometerMaxTimeModel,out mssg))
            {
                return processFixedInclinometerMaxTimeModel.maxTime;
            }
            return new DateTime();

        }

    }
}
