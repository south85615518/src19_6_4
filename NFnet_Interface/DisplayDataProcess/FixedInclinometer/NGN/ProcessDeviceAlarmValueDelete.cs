﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessdeviceAlarmValueDelete
    {
        public ProcessFixed_Inclinometer_devicealarmvalueBLL devicealarmvalueBLL = new ProcessFixed_Inclinometer_devicealarmvalueBLL();
        public bool deviceAlarmValueDelete(int xmno, string alarmName,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_devicealarmvalueBLL.DeleteModel(xmno,alarmName);
            return devicealarmvalueBLL.Delete(model,out mssg);
        }
    }
}
