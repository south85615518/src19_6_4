﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessReportDataView
    {
        public ProcessFixed_Inclinometer_orglDataBLL fixed_Inclinometer_orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public DataTable ReportDataView(List<string> cyclist,int pageIndex, int pageSize,  int xmno, string sord, out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.ProcessReportDataViewModel(cyclist, pageIndex, pageSize, xmno, sord);
            if (fixed_Inclinometer_orglDataBLL.ProcessReportDataView(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
