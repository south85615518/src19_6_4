﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessDeviceAlarmMultiUpdate
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public bool deviceAlarmMultiUpdate(string pointnamestr, NGN.Model.fixed_inclinometer_device model, out string mssg)
        {
            var mutilupdatemodel = new ProcessFixed_Inclinomater_deviceBLL.MultiUpdateAlarmModel(pointnamestr, model);
            return processFixed_Inclinomater_deviceBLL.MultiUpdateAlarm(mutilupdatemodel, out mssg);

        }
    }
}
