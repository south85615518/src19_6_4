﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainTableLoad
    {
        public ProcessFixed_Inclinomater_chainBLL processFixed_Inclinomater_chainBLL = new ProcessFixed_Inclinomater_chainBLL();
        public DataTable ChainTableLoad(int xmno, int pageIndex, int pagesize, string colName, string sord, out string mssg)
        {

            var model = new ProcessFixed_Inclinomater_chainBLL.Fixed_inclinometerPointChainTableLoadModel(xmno,  pageIndex,  pagesize,  colName,  sord);
            if (processFixed_Inclinomater_chainBLL.PointTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
}
