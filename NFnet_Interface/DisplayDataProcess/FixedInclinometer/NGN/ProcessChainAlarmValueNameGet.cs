﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainAlarmValueNameGet
    {
        public ProcessFixed_Inclinometer_chainalarmvalueBLL chainalarmvalueBLL = new ProcessFixed_Inclinometer_chainalarmvalueBLL();
        public string ChainAlarmValueNameGet(int xmno,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_chainalarmvalueBLL.AlarmValueNameGetModel(xmno);
            if (chainalarmvalueBLL.AlarmValueNameGet(model, out mssg))
                return model.alarmValueNameStr;
            return "";
        }
    }
}
