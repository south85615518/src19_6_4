﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessDeviceTableLoad
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public DataTable deviceTableLoad(int xmno, int pageIndex, int pagesize, string colName, string sord, out string mssg)
        {

            var model = new ProcessFixed_Inclinomater_deviceBLL.Fixed_inclinometerPointDeviceTableLoadModel(xmno, pageIndex, pagesize, colName, sord);
            if (processFixed_Inclinomater_deviceBLL.PointTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
}
