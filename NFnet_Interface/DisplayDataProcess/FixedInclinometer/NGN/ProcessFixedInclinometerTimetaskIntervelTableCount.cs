﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixedInclinometerTimetaskIntervelTableCount
    {
        public ProcessDTUTimeIntervalBLL processDTUTimeIntervalBLL = new ProcessDTUTimeIntervalBLL();
        public int FixedInclinometerTimetaskIntervelTableCount(int xmno, string searstring, out string mssg)
        {
            var processDTUTimeIntervalTableCountModel = new ProcessDTUTimeIntervalBLL.ProcessDTUSenorTableCountModel(xmno,searstring);
            if (processDTUTimeIntervalBLL.ProcessFixedInclinometerTimeIntervelTableCount(processDTUTimeIntervalTableCountModel, out mssg))
            {
                return processDTUTimeIntervalTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
