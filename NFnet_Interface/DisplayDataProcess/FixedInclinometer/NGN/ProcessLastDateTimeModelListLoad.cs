﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessLastDateTimeModelListLoad
    {
        public ProcessFixed_Inclinometer_orglDataBLL processFixed_Inclinometer_orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public List<global::NGN.Model.fixed_inclinometer_orgldata> LastDateTimeModelListLoad(int xmno,string chain_name,DateTime lastdatetime,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.ProcessLastDateTimeModelListModel(xmno,chain_name,lastdatetime);
            if (processFixed_Inclinometer_orglDataBLL.ProcessLastDateTimeModelList(model, out mssg))
            {
                return model.modellist;
            }
            return null;

            
        }
    }
}
