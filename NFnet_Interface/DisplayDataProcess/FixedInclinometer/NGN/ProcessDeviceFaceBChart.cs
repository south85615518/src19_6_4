﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessdeviceFaceBChart
    {
        public ProcessdeviceChartBLL bkgmuc = new ProcessdeviceChartBLL();
        public List<serie_point> Serializestrdevice(object sql, object xmno, object pointname,out string mssg)
        {

            var serializestrdeviceModel = new SerializestrSBWY_POINTCondition(sql, xmno, pointname);

            if (ProcessdeviceChartBLL.ProcessFaceBSerializestrdevice(serializestrdeviceModel, out mssg))
                return serializestrdeviceModel.serie_points;
            return new List<serie_point>();


        }

    }
}
