﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainAlarmValueDelete
    {
        public ProcessFixed_Inclinometer_chainalarmvalueBLL chainalarmvalueBLL = new ProcessFixed_Inclinometer_chainalarmvalueBLL();
        public bool ChainAlarmValueDelete(int xmno, string alarmName,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_chainalarmvalueBLL.DeleteModel(xmno,alarmName);
            return chainalarmvalueBLL.Delete(model,out mssg);
        }
    }
}
