﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using Tool;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.NGN_FixedInclinometer;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessDevicePlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static ProcessChainDeviceModelListGet processChainDeviceModelListGet = new ProcessChainDeviceModelListGet();
        public string mssg = "";
        public string DevicePlotLineAlarm(string pointnamestr, string xmname, int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<DevicePlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                List<DevicePlotlineAlarmModel> lmm = new List<DevicePlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {

                    List<NGN.Model.fixed_inclinometer_device> pointvaluelist = processChainDeviceModelListGet.ChainDeviceModelListGet(xmno,pointname, out mssg);
                    List<NGN.Model.fixed_inclinometer_alarmvalue> alarmList = processDeviceDataAlarmBLL.TestAlarmValueList(pointvalue);
                    DevicePlotlineAlarmModel model = new DevicePlotlineAlarmModel
                    {
                        pointname = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("测斜段预警参数加载出错,错误信息:" + ex.Message);
                return null;
            }
        }
    }
}
