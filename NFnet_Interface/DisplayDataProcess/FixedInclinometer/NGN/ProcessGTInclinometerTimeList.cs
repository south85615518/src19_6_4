﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTInclinometerTimeList
    {
        public ProcessFixed_Inclinometer_orglDataBLL processResultDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public List<string> SurveyTimeList(int xmno, data.Model.gtsensortype datatype, string point_name, out string mssg)
        {
            var model = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessResultDataTimeListLoadModel(xmno,point_name,datatype);
            if (processResultDataBLL.ProcessResultDataTimeListLoad(model, out mssg))
                return model.ls;
            return null;
        }
    }
}
