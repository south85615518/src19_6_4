﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTInclinometerProcessScalarDataTableCountLoad
    {
        public ProcessFixed_Inclinometer_orglDataBLL processResultDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public string DataTableCountLoad(int xmno,string pointname ,int startcyc,int endcyc,out string mssg)
        {
            var gtResultDataCountLoadCondition = new GTResultDataCountLoadCondition(xmno, pointname, startcyc, endcyc);
            if (processResultDataBLL.ResultFixedInclinometerTableRecordsCount(gtResultDataCountLoadCondition, out mssg))
                return gtResultDataCountLoadCondition.totalCont;
            return "";
        }
    }
}
