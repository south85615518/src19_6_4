﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTInclinometerDataLackDate
    {
        public ProcessGTinclinometerDateTimeStampBLL processGTGTInclinometerDataDateTimeStampBLL = new ProcessGTinclinometerDateTimeStampBLL();
        public void Datalackdate(int xmno, string point_name, PointDataDateTimeStamp timestamp, out string mssg)
        {
            var processDatalackdateModel = new ProcessGTinclinometerDateTimeStampBLL.ProcessDatalackdateModel(xmno, point_name, timestamp);
            processGTGTInclinometerDataDateTimeStampBLL.ProcessDatalackdate(processDatalackdateModel, out mssg);
        }
    }
}
