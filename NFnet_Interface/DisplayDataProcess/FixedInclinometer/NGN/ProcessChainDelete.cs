﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainDelete
    {
        public ProcessFixed_Inclinomater_chainBLL processFixed_Inclinomater_chainBLL = new ProcessFixed_Inclinomater_chainBLL();
        public bool ChainDelete(int xmno,string chainname,out string mssg)
        {
            var model = new ProcessFixed_Inclinomater_chainBLL.fixed_inclinometer_chaindeleteModel(xmno,chainname);
            return processFixed_Inclinomater_chainBLL.Delete(model,out mssg);

        }
    }
}
