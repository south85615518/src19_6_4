﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessdeviceAlarmValueNameGet
    {
        public ProcessFixed_Inclinometer_devicealarmvalueBLL devicealarmvalueBLL = new ProcessFixed_Inclinometer_devicealarmvalueBLL();
        public string deviceAlarmValueNameGet(int xmno, out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_devicealarmvalueBLL.AlarmValueNameGetModel(xmno);
            if (devicealarmvalueBLL.AlarmValueNameGet(model, out mssg))
                return model.alarmValueNameStr;
            return "";
        }
    }
}
