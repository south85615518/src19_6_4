﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixed_InclinometerDeviceExist
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public bool Fixed_InclinometerDeviceExist(string point_name, int xmno,out string mssg)
        {
            var model = new ProcessFixed_Inclinomater_deviceBLL.IsFixed_Inclinomater_deviceNameExistModel(point_name,xmno);
            return processFixed_Inclinomater_deviceBLL.Exist(model,out mssg);
        }
    }
}
