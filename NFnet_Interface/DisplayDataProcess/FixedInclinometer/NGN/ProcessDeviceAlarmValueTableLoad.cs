﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
   public class ProcessdeviceAlarmValueTableLoad
    {
       public ProcessFixed_Inclinometer_devicealarmvalueBLL devicealarmvalueBLL = new ProcessFixed_Inclinometer_devicealarmvalueBLL();
       public DataTable deviceAlarmValueTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord,out string mssg)
       {
           var model = new ProcessFixed_Inclinometer_devicealarmvalueBLL.Fixed_Inclinometer_deviceTableLoadModel( startPageIndex,  pageSize,  xmno,  colName,  sord);
           if (devicealarmvalueBLL.TableLoad(model, out mssg))
               return model.dt;
           return new DataTable();
       }
    }
}
