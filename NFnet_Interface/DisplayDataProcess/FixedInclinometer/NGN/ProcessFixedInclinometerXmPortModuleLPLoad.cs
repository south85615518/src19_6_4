﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixedInclinometerXmPortModuleLPLoad
    {
        public ProcessDTUModuleLPBLL processDTUModuleLPBLL = new ProcessDTUModuleLPBLL();
        public DataTable FixedInclinometerXmPortModuleLPLoad(int pageIndex, int rows, int xmno, string xmname, string colName, string sord, out string mssg)
        {
            var processDTUModuleLPLoadBLLModel = new ProcessDTUModuleLPBLL.ProcessDTUModuleLPLoadBLLModel(xmno,xmname ,colName, pageIndex, rows, sord);
            if (processDTUModuleLPBLL.ProcessFixedInclinometerXmPortModuleLPLoadBLL(processDTUModuleLPLoadBLLModel, out mssg))
            {
                return processDTUModuleLPLoadBLLModel.dt;
            }
            return new DataTable();
        }
    }
}
