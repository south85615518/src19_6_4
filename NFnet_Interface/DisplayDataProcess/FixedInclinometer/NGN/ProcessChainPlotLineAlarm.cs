﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using Tool;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.NGN_FixedInclinometer;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessChainPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string ChainPlotLineAlarm(string pointnamestr, string xmname, int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<ChainPlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessChainDataAlarmBLL processChainDataAlarmBLL = new ProcessChainDataAlarmBLL(xmname, xmno);
                List<ChainPlotlineAlarmModel> lmm = new List<ChainPlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    NGN.Model.fixed_inclinometer_chain pointvalue = processChainDataAlarmBLL.TestPointAlarmValue(pointname);
                    List<NGN.Model.fixed_inclinometer_chainalarmvalue> alarmList = processChainDataAlarmBLL.TestAlarmValueList(pointvalue);
                    ChainPlotlineAlarmModel model = new ChainPlotlineAlarmModel
                    {
                        pointname = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("测斜链预警参数加载出错,错误信息:" + ex.Message);
                return null;
            }
        }
    }
}
