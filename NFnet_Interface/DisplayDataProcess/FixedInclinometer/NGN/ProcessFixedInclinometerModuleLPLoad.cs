﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixedInclinometerModuleLPLoad
    {
        public ProcessDTUModuleLPBLL processDTUModuleLPBLL = new ProcessDTUModuleLPBLL();
        public DataTable FixedInclinometerModuleLPLoad(int pageIndex, int rows, int xmno, string colName, string sord, out string mssg)
        {
            var processDTUModuleLPLoadBLLModel = new ProcessDTUModuleLPBLL.ProcessDTUModuleLPLoadBLLModel(xmno, colName, pageIndex, rows, sord);
            if (processDTUModuleLPBLL.ProcessFixedInclinometerModuleLPLoadBLL(processDTUModuleLPLoadBLLModel, out mssg))
            {
                return processDTUModuleLPLoadBLLModel.dt;
            }
            return new DataTable();
        }
    }
}
