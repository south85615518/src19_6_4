﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessDTU_FixedInclinometerPointLoad
    {
        public ProcessChainNameLoad chainNameLoad = new ProcessChainNameLoad();
        public ProcessDeviceChainDeviceNameLoad chainDeviceNameLoad = new ProcessDeviceChainDeviceNameLoad();
        public class DTU_FixedInclinometerPointModel
        {
            public string chainname { get; set; }
            public List<string> pointnamelist{ get; set; }
            public DTU_FixedInclinometerPointModel(string chainname, List<string> pointnamelist)
            {
                this.chainname = chainname;
                this.pointnamelist = pointnamelist;
            }
        }
        public List<DTU_FixedInclinometerPointModel> DTU_FixedInclinometerPointLoad(int xmno,out string mssg)
        {
            List<string> chainnamelist = chainNameLoad.ChainNameLoad(xmno,out mssg);
            List<DTU_FixedInclinometerPointModel> fixedInclinometerPointModelList = new List<DTU_FixedInclinometerPointModel>();
            foreach (var chainname in chainnamelist)
            {
                List<string> chainpointlist = chainDeviceNameLoad.ChainDeviceNameListLoad(chainname,xmno,out mssg);
                fixedInclinometerPointModelList.Add(new DTU_FixedInclinometerPointModel(chainname,chainpointlist) );
            }
            return fixedInclinometerPointModelList;
        }
    }
}
