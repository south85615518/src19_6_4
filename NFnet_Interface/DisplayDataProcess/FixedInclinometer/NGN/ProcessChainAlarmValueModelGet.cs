﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainAlarmValueModelGet
    {
        public ProcessFixed_Inclinometer_chainalarmvalueBLL chainalarmvalueBLL = new ProcessFixed_Inclinometer_chainalarmvalueBLL();
        public NGN.Model.fixed_inclinometer_chainalarmvalue ChainAlarmValueModelGet(int xmno, string alarmName, out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_chainalarmvalueBLL.Fixed_Inclinometer_chainModelGetModel(xmno, alarmName);
            if (chainalarmvalueBLL.GetModel(model, out mssg))
                return model.model;
            return new NGN.Model.fixed_inclinometer_chainalarmvalue();
        }
    }
}
