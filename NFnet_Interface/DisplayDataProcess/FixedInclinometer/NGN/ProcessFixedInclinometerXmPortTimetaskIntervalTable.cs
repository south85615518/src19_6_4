﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixedInclinometerXmPortTimetaskIntervalTable
    {
        public ProcessDTUTimeIntervalBLL processDTUTimeIntervalBLL = new ProcessDTUTimeIntervalBLL();
        public DataTable FixedInclinometerXmPortTimetaskIntervalTable(int pageIndex, int rows, int xmno, string xmname, string colName, string sord, out string mssg)
        {
            var processDTUTimeIntervalLoadBLLModel = new ProcessDTUTimeIntervalBLL.ProcessDTUSenorLoadBLLModel(xmno,xmname,colName,pageIndex,rows,sord);
            if (processDTUTimeIntervalBLL.ProcessFixedInclinometerXmPortTimeIntervelLoadBLL(processDTUTimeIntervalLoadBLLModel, out mssg))
            {
                return processDTUTimeIntervalLoadBLLModel.dt;
            }
            return new DataTable();
        }
    }
}
