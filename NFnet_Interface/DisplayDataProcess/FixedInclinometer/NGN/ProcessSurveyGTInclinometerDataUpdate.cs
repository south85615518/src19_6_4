﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTInclinometerSurveyDataUpdate
    {
        public ProcessFixed_Inclinometer_orglDataBLL processResultDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public bool SurveyDataUpdate(int xmno, string pointname, DateTime dt,string vSet_name, string vLink_name, DateTime srcdatatime,out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.UpdataSurveyDataModel(xmno, pointname, dt,vSet_name, vLink_name, srcdatatime);
            return processResultDataBLL.UpdataSurveyData(model,out mssg);
        }
    }
}
