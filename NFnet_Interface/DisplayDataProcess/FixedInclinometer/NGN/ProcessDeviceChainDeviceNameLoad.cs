﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessDeviceChainDeviceNameLoad
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public List<string> ChainDeviceNameListLoad(string chain_name, int xmno,out string mssg)
        {
            var model = new ProcessFixed_Inclinomater_deviceBLL.ChainFixed_inclinometerNameLoadModel(chain_name,xmno);
            if (processFixed_Inclinomater_deviceBLL.ChainFixed_inclinometerNameLoad(model, out mssg))
                return model.devicenamelist;
            return new List<string>();
        }
    }
}
