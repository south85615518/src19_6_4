﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
   public class ProcessOrglDataRqcxConditionCreate
    {
       public static ProcessOrglDataBLL processOrglData = new ProcessOrglDataBLL();
       public ResultDataRqcxConditionCreateCondition ResultDataRqcxConditionCreate(string startTime, string endTime, QueryType QT, string unit, int xmno, DateTime maxTime)
       {
           var resultDataRqcxConditionCreateCondition = new ResultDataRqcxConditionCreateCondition(startTime, endTime, QT, unit, xmno, maxTime);
           processOrglData.ProcessFaceResultDataRqcxConditionCreate(resultDataRqcxConditionCreateCondition);
           return resultDataRqcxConditionCreateCondition;
       }
    }
}
