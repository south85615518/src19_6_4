﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;


namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessCgGTInclinometerResultDataTableLoad
    {
        public ProcessFixed_Inclinometer_orglDataBLL processResultDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public DataTable CgResultDataTableLoad(int  xmno, string pointname, int pageIndex, int rows, string sord,  int startcyc, int endcyc, out  string mssg)
        {
            var model = new NFnet_BLL.GTResultDataLoadCondition(xmno, pointname, pageIndex, rows, sord,  startcyc, endcyc);
            if (processResultDataBLL.CgResultFixedInclinometerTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
}
