﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
   public class ProcessChainAlarmValueTableLoad
    {
        public ProcessFixed_Inclinometer_chainalarmvalueBLL devicealarmvalueBLL = new ProcessFixed_Inclinometer_chainalarmvalueBLL();
        public DataTable deviceAlarmValueTableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_chainalarmvalueBLL.Fixed_Inclinometer_chainTableLoadModel(startPageIndex, pageSize, xmno, colName, sord);
            if (devicealarmvalueBLL.TableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
}
