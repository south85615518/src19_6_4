﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessDeviceModelGet
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public NGN.Model.fixed_inclinometer_device DeviceModelGet(int xmno, int deviceno, int address, int port,out string mssg)
        {
            var model = new ProcessFixed_Inclinomater_deviceBLL.fixed_inclinometer_deviceModelGetModel( xmno,  deviceno,  address,  port);
            if (processFixed_Inclinomater_deviceBLL.GetModel(model, out mssg))
                return model.model;
            return new NGN.Model.fixed_inclinometer_device();
        }
    }
}
