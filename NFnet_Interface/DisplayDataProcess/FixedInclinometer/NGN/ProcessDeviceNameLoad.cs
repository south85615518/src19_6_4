﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessDeviceNameLoad
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public List<string> deviceNameLoad(int xmno,string chain_name, out string mssg)
        {

            var model = new ProcessFixed_Inclinomater_deviceBLL.ChainFixed_inclinometerNameLoadModel(chain_name,xmno);
            if (processFixed_Inclinomater_deviceBLL.ChainFixed_inclinometerNameLoad(model, out mssg))
                return model.devicenamelist;
            return new List<string>();
        }
    }
}
