﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainDataModelGet
    {
        public ProcessFixed_Inclinometer_chainDataBLL chainDataBLL = new ProcessFixed_Inclinometer_chainDataBLL();
        public NGN.Model.fixed_inclinometer_chaindata chainDataModelGet(int xmno, string point_name, DateTime time, out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_chainDataBLL.Fixed_Inclinometer_chainDataModelGetModel(xmno, point_name, time);
            if (chainDataBLL.GetModel(model, out mssg))
                return model.model;
            return new NGN.Model.fixed_inclinometer_chaindata();
        }
    }
}
