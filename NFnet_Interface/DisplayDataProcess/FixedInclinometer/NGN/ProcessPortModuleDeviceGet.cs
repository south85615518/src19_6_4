﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN
{
    public class ProcessPortModuleDeviceGet
    {
        public ProcessFixed_Inclinomater_deviceBLL deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public global::NGN.Model.fixed_inclinometer_device PortModuleDeviceGet(int xmno,int port,string addressno,out string mssg)
        {
            var processFixedInclinometerTOPointModuleGetModel = new ProcessFixed_Inclinomater_deviceBLL.ProcessFixedInclinometerTOPointModuleGetModel(xmno,port,addressno);
            if (deviceBLL.ProcessFixedInclinometerTOPointModule(processFixedInclinometerTOPointModuleGetModel, out mssg))
                return processFixedInclinometerTOPointModuleGetModel.devicemodel;
            return new global::NGN.Model.fixed_inclinometer_device();
        }
    }
}
