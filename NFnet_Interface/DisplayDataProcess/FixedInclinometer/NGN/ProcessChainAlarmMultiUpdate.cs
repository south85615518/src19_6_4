﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainAlarmMultiUpdate
    {
        public ProcessFixed_Inclinomater_chainBLL processFixed_Inclinomater_chainBLL = new ProcessFixed_Inclinomater_chainBLL();
        public bool ChainAlarmMultiUpdate(string pointnamestr,global::NGN.Model.fixed_inclinometer_chain model, out string mssg)
        {
            var mutilupdatemodel = new ProcessFixed_Inclinomater_chainBLL.MultiUpdateAlarmModel(pointnamestr,model);
            return processFixed_Inclinomater_chainBLL.MultiUpdateAlarm(mutilupdatemodel, out mssg);

        }
    }
}
