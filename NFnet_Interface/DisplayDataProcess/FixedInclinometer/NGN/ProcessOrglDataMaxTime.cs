﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessOrglDataMaxTime
    {
        public ProcessFixed_Inclinometer_orglDataBLL processFixed_Inclinometer_orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public DateTime OrglDataMaxTime(int xmno, out string mssg)
        {
            var model = new ProcessFixed_Inclinometer_orglDataBLL.PointMaxDateTimeGetModel(xmno);
            if (processFixed_Inclinometer_orglDataBLL.PointMaxDateTimeGet(model, out mssg))
            {
                return model.dt;
            }
            return new DateTime();
        }
    }
}
