﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessNGNDeviceModelGet
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public NGN.Model.fixed_inclinometer_device NGNDeviceModelGet(int xmno, int deviceno, int address, int port,out string mssg)
        {
            var model = new ProcessFixed_Inclinomater_deviceBLL.NGN_FixedInclinometerdeviceModelGetModel( xmno,  deviceno,  address,  port);
            if (processFixed_Inclinomater_deviceBLL.GetNGN_FixedInclinometerdeviceModel(model, out mssg))
                return model.model;
            return new NGN.Model.fixed_inclinometer_device();
        }
    }
}
