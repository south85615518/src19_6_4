﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Tool;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_Interface.UserProcess;
using System.Threading;
using NFnet_BLL.LoginProcess;
using NFnet_Interface.DisplayDataProcess.FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.NGN_FixedInclinometer;


namespace NFnet_BLL.DisplayDataProcess.DTUFixed_Inclinometer
{
    public class ProcessDTU_FixedInclinometerTcpServer
    {
        public static TcpListener tcpListener = null;
        public delegate void ClientConnectEvent(IAsyncResult ar);
        private event ClientConnectEvent conEvent;
        //public delegate void TextBoxFlush(string filename);
        //public event TextBoxFlush FlushEvent;
        //public System.Timers.Timer timer = new System.Timers.Timer(60000);
        public System.Timers.Timer timerrestart = new System.Timers.Timer(3600000);
        public System.Timers.Timer timerresInspection = new System.Timers.Timer(600000);
        //public System.Timers.Timer timerrestart = new System.Timers.Timer(86400000);
        //public static string ip;
        //public static string port;
        public static List<TcpListener> tcpListenerdts = new List<TcpListener>();
        public static string dtutcpportpath = "";
        public void DTUTcpServerStart(string dtuportpath)
        {
            try
            {
                TcpServerStopTotal();
                List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(dtuportpath);
                TcpserverMutilStart(ltlif);
                dtutcpportpath = dtuportpath;
                TimerTick();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNFixedInclinometerRecordWrite("服务器监听失败!");
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            TcpServerStopTotal();
        }
        #region 启动服务器监听
        public void TcpserverMutilStart(List<tcpclientIPInfo> ltlif)
        {
            try
            {
                foreach (tcpclientIPInfo tif in ltlif)
                {

                    TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(tif.ip), Convert.ToInt32(tif.port)));
                    tcpListenstart(tcpListener, tif.port, tif.ip);
                }
                ExceptionLog.NGNPortInspectionWrite("所有端口启动完成");
            }
            catch (Exception ex)
            {

            }


        }
        public string tcpListenstart(TcpListener tcpListener, int port, string ip)
        {
            ExceptionLog.NGNPortInspectionWrite("开始启动监听端口:" + port);
            string mss = "";
            try
            {
                conEvent += clientConnect;
                tcpListener.Start();
                ExceptionLog.NGNPortInspectionWrite(string.Format("端口{0}监听成功!{1}", port, DateTime.Now));


                tcpListenerdts.Add(tcpListener);
                IAsyncResult ar;
                tcpListener.BeginAcceptSocket(new AsyncCallback(conEvent), tcpListener);
                conEvent -= clientConnect;

            }
            catch (Exception ex)
            {
                mss = ex.Message;
                ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("{0}端口监听出错!错误原因:" + ex.Message + "现在重启", port));
            }
            return mss;
        }
        public delegate void AcceptEvent(AsyncCallback ar, TcpListener tcpListener);
        public event AcceptEvent evt;
        public static int contconnect = 0;
        private void clientConnect(IAsyncResult ar)
        {

            try
            {

                TcpListener listener = (TcpListener)ar.AsyncState;
                if (listener == null) return;
                if (listener.Server == null) return;
                if (listener.Server.LocalEndPoint == null) return;
                Socket s = listener.EndAcceptSocket(ar);
                if (s == null || clientInfo(s) == null) return;
                string mssg = string.Format("远程客户端{0}上线 {1} ", clientInfo(s), DateTime.Now);
                ExceptionLog.NGNFixedInclinometerRecordWrite(mssg);
                conEvent += clientConnect;
                listener.BeginAcceptSocket(new AsyncCallback(conEvent), listener);
                conEvent -= clientConnect;
                if (s == null || clientInfo(s) == null) return;
                //double hcont = 0; string datastr = "";
                //DTUDataGet(s, out hcont, out datastr);
                //ReceiveMess(s, hcont, datastr);
                Thread t = new Thread(new ParameterizedThreadStart(DataImport));
                t.Start(s);
            }

            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("位置链接异步事件:" + ex.Message);
            }

        }


        #endregion
        #region 心跳包
        public void TimerTick()
        {
            timerresInspection.Elapsed += new System.Timers.ElapsedEventHandler(TcpPortActiveCheck);
            timerresInspection.Enabled = true;
            timerresInspection.AutoReset = true;
        }
        private static object UsingPrinterLocker = new object();
        private static object UsingHistoryContLocker = new object();
        List<TcpListener> dtsDelete = new List<TcpListener>();
        public bool IsSocketAlive(Socket s)
        {
            //byte[] buff = new byte[100];

            if (s == null) return false;
            try
            {

                //s.Send(Encoding.UTF8.GetBytes("心跳包测试"));

                ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1} {2}", clientInfo(s), "心跳包测试", DateTime.Now));
                if (clientInfo(s) != null)
                    return true;
                return false;

            }
            catch (Exception ex)
            {

                return false;
            }

        }

        #endregion
        #region 窗体控件信息显示
        public void ClientConnectFlush(string mssg)
        {
            //DateTime dt = DateTime.Now;

            //this.listBox2.Items.Add(mssg);


        }
        public void ClientDataFlush(string mssg)
        {

            //DateTime dt = DateTime.Now;

            //this.listBox1.Items.Add(mssg);


        }


        #endregion
        #region 数据通信
        /// <summary>
        /// 数据解析入库线程
        /// </summary>
        public class dtudatabag
        {
            public Socket s { get; set; }
            public int cont { get; set; }
            public string datastr { get; set; }
            public dtudatabag(Socket s)
            {
                this.s = s;

            }
        }
        public void DataImport(object obj)
        {
            Socket s = obj as Socket;
            //dtudatabag bag = obj as dtudatabag;
            double hcont = 0; string datastr = "";
            DTUDataGet(s, out hcont, out datastr);
            ReceiveMess(s, hcont, datastr);

        }
        public void ReceiveMess(Socket s, double cont, string dtudata)
        {
            int port = Convert.ToInt32(clientInfo(s).ToString().Split(':')[1]);
            ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("{2} 开始解析DTU数据...【{0}】,历史数据条数【{1}】", dtudata, cont, clientInfo(s)), port);
            //对方发送文件
            DateTime dt = DateTime.Now;
            string client = "";
            try
            {

                StringBuilder datastr = new StringBuilder(256);
                int count;
                byte[] b = new byte[4096];
                s.ReceiveTimeout = 300000;// -1;
                client = clientInfo(s);
                //有历史数据，DTU没有进行本次采集
                if (cont > 0 && dtudata == "")
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite("DTU在规定时间内没有进行采集，而还存在历史数据，现在发送获取历史数据指令...", port);
                    return;

                    s.Send(Encoding.UTF8.GetBytes(string.Format("get data:{0}", Convert.ToInt32(cont))));
                    ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", client, string.Format("get data:{0}", Convert.ToInt32(cont)), DateTime.Now));
                    if (cont == 1) cont = 2;
                    ReceiveMess(s, cont, "1");
                    return;
                }
                //无历史数据，DTU发回了本次采集的数据，直接存库
                else if ((cont == 0 || cont == 1) && dtudata != "")
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("DTU进行了本次采集，不存在历史数据,【{0}】直接入库...", dtudata), port);
                    IsThisSendFinish(client, dtudata, port, 0);
                    return;
                }
                //有历史数据，DTU只发回了本次数据，发送历史数据获取指令，此时本次采集数据也是历史数据
                else if (cont > 1 && dtudata != "")
                {
                    if (dtudata == "1")
                        ExceptionLog.NGNFixedInclinometerRecordWrite("现在发送历史数据获取指令,获取完整数据...", port);
                    else
                        ExceptionLog.NGNFixedInclinometerRecordWrite("DTU进行了本次采集，而发现中间还有漏掉没有上传到平台的数据...", port);

                    s.Send(Encoding.UTF8.GetBytes(string.Format("get data:{0}", Convert.ToInt32(cont))));
                    ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), string.Format("get data:{0}", Convert.ToInt32(cont)), DateTime.Now));
                    while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                    {
                        var str = Encoding.UTF8.GetString(b, 0, count);
                        ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("接收到数据【{0}】", str.TrimEnd()), port);
                        datastr.Append(str);
                        if (datastr.ToString().IndexOf("the end") != -1 && datastr.ToString().IndexOf("the data") == -1 && cont > 0)
                        {
                            datastr = new StringBuilder();
                        }
                        if (IsThisSendFinish(client, datastr.ToString(), port, cont))
                        {

                            cont = 0;
                            ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("数据【{0}】入库完成，清零历史数据记录数{1}", datastr.ToString().TrimEnd(), cont), port);
                            datastr = new StringBuilder(256);
                            return;
                        }

                        b = new byte[4096];
                        s.ReceiveTimeout = 300000;
                    }
                }

                ExceptionLog.NGNFixedInclinometerRecordWrite("数据接收完成,现在退出...", port);
            }
            catch (Exception ex)
            {

                ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("{0}  接收出错 ,出错原因:{1}", clientInfo(s), ex.Message), port);

                return;
            }
            finally
            {

                if (dtudata.IndexOf("od") != -1)
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("直接存储本次采集数据【{0}】...", dtudata), port);
                    IsThisSendFinish(client, dtudata, port, 0);

                }
            }

        }

        public bool IsThisSendFinish(string client, string str, int port, double cont)
        {
            if (str.IndexOf("the all") != -1 || str.IndexOf("the end") != -1)
            {
                try
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("开始解析数据【{0}】", str.TrimEnd()), port);
                    if (str.IndexOf("the all") == -1 && cont > 0 && str.IndexOf("the data") == -1)
                    {
                        ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("对于历史条数记录有{0}条而字符串{1}中既不包含 the all，the data两个标志字符的数据不是DTU数据", cont, str), port);
                        return false;
                    }
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("开始导入数据【{0}】", str.TrimEnd()), port);
                    DTUDATAImport(str, port);
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("{1} 接收{0}发来的数据【{2}】  ", client, DateTime.Now, str.TrimEnd()), port);
                    return true;
                }
                catch (Exception ex)
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite("数据录入出错,错误信息:" + ex.Message + ex.StackTrace, port);
                    return true;
                }
            }
            return false;
        }

        public bool GetDTUIDFromFirstData(Socket s, string str, int port, out string addressno, out string point, out int xmno)
        {

            xmno = 0;
            point = "";
            addressno = "";
            try
            {
                addressno = str.Split(':')[0];
                string[] attrs = str.Split('\r');
                int i = 0;
                //string pointname = "";
                double line = 0;
                double holedepth = 0;
                xmno = GetDTUXmnoFromPort(port);
                //for (i = 1; i < attrs.Length - 1; i++)
                //{

                string datastr = "";
                bool isdatestr = false;
                foreach (string tmpstr in attrs)
                {

                    if (tmpstr.Length < 19) continue;
                    if (tmpstr.IndexOf("2017/") != -1)
                    {

                        datastr = tmpstr.Substring(tmpstr.IndexOf("2017/"), 19);
                        isdatestr = true;
                        break;
                    }
                    else if (tmpstr.IndexOf("2018/") != -1)
                    {
                        datastr = tmpstr.Substring(tmpstr.IndexOf("2018/"), 19);
                        isdatestr = true;
                        break;
                    }
                }
                if (!isdatestr) return false;
                //str = attrs[0];
                //if (str.IndexOf("2017/") == -1 || str.IndexOf("2018/") == -1) return false;

                string[] dataary = datastr.Split(' ');
                ExceptionLog.NGNFixedInclinometerRecordWrite("获取到的DTU时间为" + datastr, port);
                ProcessInterface.NGNModuleChainName(xmno,Convert.ToInt32(addressno), port, out point);
                DateTime dtutime = Convert.ToDateTime(dataary[0] + " " + dataary[1]);
                ExceptionLog.NGNFixedInclinometerRecordWrite("获取到链名" + point, port);
                /*调试注释*/
                //return true;
                if (Math.Abs((DateTime.Now - dtutime).TotalMinutes) > 10)
                {

                    //FlushEvent += ClientDataFlush;();
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("DTU当前时间是{1}系统时间是{2}现在向{0}发送时间校准指令", clientInfo(s), dtutime, DateTime.Now), port);
                    //DTUDATAImport(datastr);
                    //this.listBox1.EndInvoke(ar);
                    //FlushEvent -= ClientDataFlush;
                    s.Send(Encoding.UTF8.GetBytes(string.Format("set rtc:{0},{1},{2},{3},{4},{5}", DateTime.Now.Year, DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"))));
                    ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1} {2}", clientInfo(s), string.Format("set rtc:{0},{1},{2},{3},{4},{5}", DateTime.Now.Year, DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00")), DateTime.Now));
                    byte[] buf = new byte[4096];
                    int ct = 0;
                    int revc = 0;
                    s.ReceiveTimeout = 60000;
                    while ((ct = s.Receive(buf, SocketFlags.None)) > 0)
                    {

                        string strrev = Encoding.UTF8.GetString(buf, 0, ct);
                        buf = new byte[4096];
                        if (strrev.IndexOf("modify time success") != -1)
                        {
                            //FlushEvent += ClientDataFlush;
                            ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("校准{0}时间成功", clientInfo(s)), port);
                            return true;
                            //DTUDATAImport(datastr);
                            //this.listBox1.EndInvoke(ar);
                            //FlushEvent -= ClientDataFlush;
                        }
                        revc++;
                        if (revc > 3) return false;

                        //FlushEvent += ClientDataFlush;
                        ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("校准{0}时间返回{1}", clientInfo(s), strrev), port);
                        //return false;
                        //DTUDATAImport(datastr);
                        //this.listBox1.EndInvoke(ar);
                        //FlushEvent -= ClientDataFlush;

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("从{0}校准DTU的时间出错,错误信息：" + ex.Message, str == null ? "" : str), port);
                return false;
            }


        }
        public bool DataRec(Socket s, int port)
        {

            return false;

        }
        //服务器等待客户端的数据超时进行主动获取
        public void DTUDATARequestSend(Socket s, int port)
        {
            int i = 0;
            s.SendTimeout = 120000;
            IAsyncResult ar = null;
            //连续请求三次
            for (i = 0; i < 3; i++)
            {
                try
                {
                    //设置10秒等待时间

                    s.Send(Encoding.UTF8.GetBytes("set jiange:00,01,01"));
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("向客户端{0}发送数据获取请求成功,现在等待客户端数据回传...", clientInfo(s)), port);

                    int count;
                    StringBuilder datastr = new StringBuilder(256);
                    byte[] b = new byte[4096];
                    //设置20秒等待时间
                    s.ReceiveTimeout = 180000;
                    while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                    {
                        var str = Encoding.UTF8.GetString(b, 0, count);
                        datastr.Append(str);
                        if (IsThisSendFinish("", datastr.ToString(), port, 0))
                        {
                            datastr = new StringBuilder(256);
                        }
                        b = new byte[4096];

                    }
                }
                catch (Exception ex)
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("向客户端{0}发送数据请求命令失败,失败原因{1} 请求次数{2}", clientInfo(s), ex.Message, i), port);
                    continue;
                }

                SocketClose(s);
                ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("向{0}  从{2}请求数据获取超时!设备采集时间间隔超过了规定时间或者设备已经损坏", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address));
            }
        }

        #endregion
        #region 数据处理
        /// <summary>
        /// 数据导入
        /// </summary>
        /// <param name="str"></param>
        public void DTUDATAImport(string datastr, int port)
        {

            DTUDATADescode(port, datastr);

        }
        /// <summary>
        /// 数据校验
        /// </summary>
        /// <returns></returns>
        public bool messvalidate()
        {
            return true;
        }
        public string point_name = "";
        public int xmno = 0;
        public static ProcessChainDataTmpDelete processChainDataTmpDelete = new ProcessChainDataTmpDelete();
        public static ProcessorglDataTmpDelete processorglDataTmpDelete = new ProcessorglDataTmpDelete();
        public static ProcessDTUTaskQueueFinishedSet processDTUTaskQueueFinishedSetBLL = new ProcessDTUTaskQueueFinishedSet();
        public static ProcessOrglDataAlarmBLL processOrglDataAlarmBLL = new ProcessOrglDataAlarmBLL();
        public static string mssg = "";
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public ProcessFixed_Inclinometer_orglDataBLL processFixed_Inclinometer_orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public ProcessFixed_Inclinometer_chainDataBLL processFixed_Inclinometer_chainDataBLL = new ProcessFixed_Inclinometer_chainDataBLL();
        //DTU数据解析入库
        public void DTUDATADescode(int port, string str)
        {
            string addressno = str.Split(':')[0];
            string[] attrs = str.Split('\r');
            int i = 0;
            string pointname = "";
            double line = 0;
            double holedepth = 0;
            int xmno = GetDTUXmnoFromPort(port);
            string xmname = GetDTUXmnameFromPort(port);
            processorglDataTmpDelete.OrglDataTmpDelete(xmno, out mssg);
            ExceptionLog.NGNFixedInclinometerRecordWrite(mssg, port);
            processChainDataTmpDelete.ChainDataTmpDelete(xmno, out mssg);
            ExceptionLog.NGNFixedInclinometerRecordWrite(mssg, port);
            //测斜段数据录入
            for (i = 0; i < attrs.Length; i++)
            {

                try
                {
                    str = attrs[i];
                    if (str.IndexOf("od") == -1/*||str.IndexOf("2017/") == -1 && str.IndexOf("2018/") == -1*/)
                    {
                        ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("{0}不符合格式要求,不予入库", str), port);
                        continue;
                    }

                    string datastr = "";
                    if (str.IndexOf("\r\n") != -1)
                        str = str.Substring(str.IndexOf("\r\n") + 2);
                    //if (str.IndexOf("2017/") != -1)
                    //    datastr = str.Substring(str.IndexOf("2017/"));
                    //else
                    //    datastr = str.Substring(str.IndexOf("2018/"));
                    datastr = str.Substring(str.IndexOf(addressno + ":")+addressno.Length+1+1);
                    string[] dataary = datastr.Split(' ');

                    var device = ProcessInterface.NGNDeviceModel(xmno, port,Convert.ToInt32(addressno),Convert.ToInt32(dataary[3].Replace("od","").ToString()), out mssg);
                    //DTUModulePointName(xmno, port, dataary[2], addressno, out pointname, out line, out holedepth);
                    ExceptionLog.NGNFixedInclinometerRecordWrite(mssg, port);
                    if (device == null || device.point_name == "" || device.point_name == "0" || string.IsNullOrEmpty(device.point_name)) continue;

                    /*
                    4850020: 0000000001 2018/06/07 11:55:21 od01 1354
                    4850020: 0000000001 2018/06/07 11:55:22 od02 0
                    4850020: 0000000001 2018/06/07 11:55:22 od03 813
                     */


                    if (Convert.ToDouble(dataary[4]) > 10000) return;
                    double devicedisp = device.Wheeldistance*Math.Sin(device.modulesA + device.modulesB * Convert.ToDouble(dataary[4]) + device.modulesC * Math.Pow(Convert.ToDouble(dataary[4]), 2) + device.modulesD * Math.Pow(Convert.ToDouble(dataary[4]), 3));
                    //chainS += devicedisp;
                    NGN.Model.fixed_inclinometer_orgldata orgldataModel = new NGN.Model.fixed_inclinometer_orgldata
                    {
                        point_name = device.point_name,
                        disp = devicedisp,
                        f = Convert.ToDouble(dataary[4]),
                        time = Convert.ToDateTime(dataary[1] + " " + dataary[2]) > DateTime.Now ? DateTime.Now : Convert.ToDateTime(dataary[1] + " " + dataary[2]),
                        chain_name = device.chain,
                        taskid = dataary[0],
                        xmno = xmno,
                        deep = device.deep
                    };
                    processFixed_Inclinometer_orglDataBLL.Add(orgldataModel, out mssg);
                    ExceptionLog.NGNFixedInclinometerRecordWrite(mssg, port);

                }
                catch (Exception ex)
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite(str + "数据录入出错,错误信息" + mssg, port);
                }
            }
            //测斜链数据生成
            List<string> alarmInfoList = processOrglDataAlarmBLL.ResultDataAlarm(xmname, xmno);
            emailSendBLL.ProcessEmailSend(alarmInfoList, xmname, xmno, out mssg);
        }
        #endregion
        #region 处理类
        public static ProcessDTUPortXm processDTUPortXm = new ProcessDTUPortXm();
        public static int GetDTUXmnoFromPort(int port)
        {
            return processDTUPortXm.DTUPortXm(port, out mssg).xmno;
        }
        public static ProcessDTUPortXmname processDTUPortXmname = new ProcessDTUPortXmname();
        public static string GetDTUXmnameFromPort(int port)
        {
            return processDTUPortXmname.DTUPortXm(port, out mssg);
        }
        public static ProcessDTUPointModule processDTUPointModule = new ProcessDTUPointModule();
        public static bool DTUModulePointName(int xmno, int port, string od, string addressno, out string pointname, out double line, out double holedepth)
        {
            DTU.Model.dtu model = processDTUPointModule.DTUPointModule(xmno, port, od, addressno, out mssg);
            ////ProcessPrintMssg.Print(mssg);
            pointname = model.point_name;
            line = model.line;
            holedepth = model.holedepth;
            return true;
            //return model.point_name;
        }
        public static ProcessDTUPointModuleWhithoutOd processDTUPointModuleWhithoutOd = new ProcessDTUPointModuleWhithoutOd();
        public static bool DTUModulePointName(int xmno, int port, string addressno, out string pointname)
        {
            DTU.Model.dtu model = processDTUPointModuleWhithoutOd.DTUPointModule(xmno, port, addressno, out mssg);
            pointname = model.point_name;
            return true;
        }
        public string clientInfo(Socket s)
        {
            return string.Format("{0}:{1}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, s.LocalEndPoint.ToString().Split(':')[1]);
        }
        #endregion
        #region 关闭/重启
        //重新开启监听
        //停止服务
        public void TcpServerStopTotal()
        {
            try
            {
                foreach (TcpListener tcpListener in tcpListenerdts)
                {
                    //if (tcpListenerdts[tcpListener] != null)
                    //{
                    //    //关闭连接
                    //    Socket s = tcpListenerdts[tcpListener];
                    //    s.Shutdown(SocketShutdown.Both);
                    //    //s.Dispose();
                    //    s = null;
                    //}
                    //tcpListener.Server.LocalEndPoint;


                    tcpListener.Server.Close();
                    tcpListener.Stop();



                }
                tcpListenerdts.Clear();
            }
            catch (Exception ex)
            {


            }
        }
        public static int inspectCont = 0;
        public void TcpPortActiveCheck(object source, System.Timers.ElapsedEventArgs e)
        {
            ExceptionLog.NGNPortInspectionWrite(string.Format("{0}开始执行第{1}次端口巡检", DateTime.Now, ++inspectCont));
            List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(dtutcpportpath);
            foreach (tcpclientIPInfo tif in ltlif)
            {
                if (!ProcessTcpStateServer.IsUsedIPEndPoint(tif.port))
                {
                    ExceptionLog.NGNPortInspectionWrite(string.Format("巡检发现监听端口{0}已经关闭!现在启动", tif.port));
                    TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(tif.ip), Convert.ToInt32(tif.port)));
                    tcpListenstart(tcpListener, tif.port, tif.ip);
                }
            }

        }
        public void TcpServerStop(TcpListener tcpListener)
        {
            try
            {


                tcpListener.Server.Close();
                tcpListener.Stop();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNFixedInclinometerRecordWrite("关闭服务器端口{0}监听出错,错误信息：" + ex.Message);

            }
        }
        private object useSocketLocker = new object();
        public void SocketClose(Socket s)
        {
            System.Threading.Monitor.Enter(useSocketLocker);
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNFixedInclinometerRecordWrite("关闭远程客户端出错!出错原因:" + ex.Message);
            }
            finally
            {
                System.Threading.Monitor.Exit(useSocketLocker);
            }
        }
        /// <summary>
        /// 重启单个服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        /*oid TcpListenerRestart(TcpListener listenser)
        {
            List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad("端口配置文件\\setting.txt");
            int port = tcpListenerport[listenser];
            var ip = (from m in ltlif where m.port == port select m.ip).ToList()[0];
            TcpServerStop(listenser);
            try
            {
                TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(port)));
                tcpListenstart(tcpListener, port);
                ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("重启服务器端口{0}成功!", port));
            }
            catch (Exception ex)
            {
                ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("重启服务器端口{0}出错，错误信息:" + ex.Message, port));
            }

        }*/
        /// <summary>
        /// 重启所有服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        public void TcpListenerRestartTotal(object source, System.Timers.ElapsedEventArgs e)
        {
            string dtuportpath = Convert.ToString(source);
            timerrestart.Stop();
            //timer.Enabled = false;
            timerrestart.Enabled = false;
            //timer.AutoReset = false;
            //ExceptionLog.NGNFixedInclinometerRecordWrite("开始重启监听..." + dtutcpportpath);
            //this.comboBox2.SelectedIndex = 1;
            //TcpServerStopTotal();
            //EncodingSet();
            //TcpServerStopTotal();
            //List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(dtutcpportpath);
            //"D:\\华南水电\\DTUServer\\TcpSever_MYSQL+\\TcpSever\\bin\\debug\\端口配置文件\\setting.txt"
            //ExceptionLog.NGNFixedInclinometerRecordWrite("开始启动监听..."+string.Join(":",));
            //TcpserverMutilStart(ltlif);
            //TimerTick();
            timerrestart.Elapsed -= new System.Timers.ElapsedEventHandler(TcpListenerRestartTotal);
            //DTUTcpServerStart(dtutcpportpath);
            TcpServerStopTotal();
            //EncodingSet();
            List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(dtuportpath);
            //"D:\\华南水电\\DTUServer\\TcpSever_MYSQL+\\TcpSever\\bin\\debug\\端口配置文件\\setting.txt"
            //ExceptionLog.NGNFixedInclinometerRecordWrite("开始启动监听..."+string.Join(":",));
            TcpserverMutilStart(ltlif);
            dtutcpportpath = dtuportpath;
        }





        //private void TcpServerReStart(string dtuportpath)
        //{
        //    string mss = "";
        //    try
        //    {
        //        //从端口配置文件读取端口
        //        TcpServerStopTotal();
        //        List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(dtuportpath);
        //        foreach (tcpclientIPInfo tcpclientIPInfo in ltlif)
        //        {
        //            try {

        //            }
        //        }
        //        tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(this.textBox1.Text), Int32.Parse(this.textBox2.Text)));
        //        mss = this.start();
        //        this.label4.Text = mss;
        //        if (mss.IndexOf("套接字地址") != -1)
        //        { MessageBox.Show("端口被占用！"); }
        //        else if ((mss.IndexOf("有效值的范围") != -1))
        //        {
        //            MessageBox.Show("请设置有效端口！");
        //        }
        //        else
        //        {
        //            MachineSettingInfoWrite();
        //            MessageBox.Show("重启成功！");
        //            this.timer1.Start();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if ((ex.Message.IndexOf("有效值的范围") != -1))
        //        {
        //            MessageBox.Show("请设置有效端口！");
        //        }
        //        else
        //            MessageBox.Show(ex.Message);
        //        throw (ex);
        //    }


        //}
        #endregion
        #region 设置命令
        public DTUCommend CommendCreate(int commendIndex, int xmno, string point_name)
        {
            DTUCommend dtucommend = null;
            switch (commendIndex)
            {
                //设置时间间隔
                case 0:

                    string commendstr = GetTimeInterval(xmno, point_name);
                    dtucommend = new DTUCommend("设置时间间隔", "success", "failed", 60000, 60000, commendstr, 0);
                    return dtucommend;
                //设置时间间隔
                case 1:

                    commendstr = GetTimeAlarm(xmno, point_name);
                    dtucommend = new DTUCommend("设置定时", "success", "failed", 60000, 60000, commendstr, 1);
                    return dtucommend;
                //设置时间间隔
                case 2:

                    commendstr = GetLP(xmno, point_name);
                    dtucommend = new DTUCommend("设置低功耗", "success", "failed", 60000, 60000, commendstr, 2);
                    return dtucommend;
                //break;
            }
            return new DTUCommend();
        }
        public enum CommendList
        {
            TIME_INTERVAL, TIME_TASK, LP
        };


        public string GetTimeInterval(int xmno, string module)
        {
            ProcessDTUTimeIntervalLoad dtuTimeIntervalLoad = new ProcessDTUTimeIntervalLoad();
            var model = dtuTimeIntervalLoad.DTUTimeIntervalLoad(xmno, module, out mssg);
            if (model == null)
                return "";
            return string.Format("set jiange:{0},{1},{2}", model.hour.ToString("00"), model.minute.ToString("00"), model.times.ToString("00"));
        }

        public string GetTimeAlarm(int xmno, string module)
        {
            ProcessDTUTimeAlarmLoad dtuTimeAlarmLoad = new ProcessDTUTimeAlarmLoad();
            var model = dtuTimeAlarmLoad.DTUTimeAlarmLoad(xmno, module, out mssg);
            if (model == null)
                return "";
            return string.Format("set alarm:{0},{1},{2},{3}", model.hour.ToString("00"), model.minute.ToString("00"), model.sec.ToString("00"), model.times.ToString("00"));
        }
        public string GetLP(int xmno, string module)
        {
            ProcessDTUModuleLPModelBLL dtuLP = new ProcessDTUModuleLPModelBLL();
            var model = dtuLP.DTUModuleLPModelBLL(xmno, module, out mssg);
            if (model == null)
                return "";
            return model.LP == 1 ? "open lp" : "cancel lp";
        }

        public class DTUCommend
        {
            public string commendName { get; set; }
            public string delivery { get; set; }
            public string successWords { get; set; }
            public string failedWords { get; set; }
            public int sendTimeOut { get; set; }
            public int recTimeOut { get; set; }
            public int commendIndex { get; set; }
            //public int 
            public DTUCommend(string commendName, string successWords, string failedWords, int sendTimeOut, int recTimeOut, string delivery, int commendIndex)
            {
                this.commendName = commendName;
                this.successWords = successWords;
                this.failedWords = failedWords;
                this.sendTimeOut = sendTimeOut;
                this.recTimeOut = recTimeOut;
                this.delivery = delivery;
                this.commendIndex = commendIndex;
            }
            public DTUCommend()
            {

            }
        }


        public void CommendListTaskDelivery(int xmno, string module, Socket s)
        {
            int port = Convert.ToInt32(clientInfo(s).ToString().Split(':')[1]);
            List<DTUCommend> lscommend = new List<DTUCommend>();
            List<int> ls = GetCommendFromTaskQueue(xmno, module);
            if (ls.Count == 0) return;
            foreach (int commendIndx in ls)
            {
                lscommend.Add(CommendCreate(commendIndx, xmno, module));
            }
            List<string> resultList = new List<string>();
            foreach (DTUCommend commend in lscommend)
            {
                if (commendDelivery(xmno, module, s, commend))
                {
                    resultList.Add(string.Format("向项目{0}模块{1}发送{2}命令成功  ", xmno, module, commend.commendName, DateTime.Now));
                }
                else
                {
                    resultList.Add(string.Format("向项目{0}模块{1}发送{2}命令失败  ", xmno, module, commend.commendName, DateTime.Now));
                }

            }
            //FlushEvent += ClientDataFlush;
            ExceptionLog.NGNFixedInclinometerRecordWrite(string.Join("\n", resultList), port);
            //DTUDATAImport(datastr);
            //this.listBox1.EndInvoke(ar);
            //FlushEvent -= ClientDataFlush;

        }
        public ProcessDTUTimeIntervalBLL processDTUTimeIntervalBLL = new ProcessDTUTimeIntervalBLL();
        public ProcessDTUModuleLPBLL processDTUModuleLPBLL = new ProcessDTUModuleLPBLL();
        public bool commendDelivery(int xmno, string module, Socket s, DTUCommend commend)
        {
            int port = Convert.ToInt32(clientInfo(s).ToString().Split(':')[1]);
            int i = 0;
            for (i = 0; i < 3; i++)
            {
                try
                {
                    byte[] buff = new byte[1024];
                    s.SendTimeout = commend.sendTimeOut;
                    s.Send(Encoding.UTF8.GetBytes(commend.delivery));
                    s.ReceiveTimeout = -1;// 2 * commend.recTimeOut;
                    int cont = s.Receive(buff, SocketFlags.None);
                    if (cont < 1024)
                    {
                        string recresult = Encoding.UTF8.GetString(buff);
                        if (commend.successWords.IndexOf("success") != -1)
                        {

                            //processDTUTaskQueueFinishedSetBLL.DTUTaskQueueFinishedSet(xmno, module, commend.commendIndex, out mssg);
                            if (commend.delivery.IndexOf("jiange") != -1)
                            {
                                s.Send(Encoding.UTF8.GetBytes("cancel alarm"));
                                var timeintervalmodel = new DTU.Model.dtutimetask
                                {
                                    xmno = xmno,
                                    time = DateTime.Now,
                                    module = module,
                                     applicated = true
                                };
                                processDTUTimeIntervalBLL.ProcessDTUTimeIntervalUpdateFlagSet(timeintervalmodel, out mssg);
                                //processDTUTaskQueueFinishedSetBLL.DTUTaskQueueFinishedSet(xmno, module, commend.commendIndex, out mssg);
                                ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), "cancel alarm", DateTime.Now));
                            }
                            else if (commend.delivery.IndexOf("lp") != -1)
                            {
                                var lpmodel = new DTU.Model.dtulp
                                {
                                    xmno = xmno,
                                    module = module,
                                    applicated = true
                                };
                                processDTUModuleLPBLL.ProcessDTUModuleLPUpdate(lpmodel, out mssg);
                            };
                            processDTUTaskQueueFinishedSetBLL.DTUTaskQueueFinishedSet(xmno, module, commend.commendIndex, out mssg);
                            ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), commend.delivery, DateTime.Now));
                            
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        //发送返回的字节数太多
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("向项目编号{0}模块{1}发送命令{2}失败 第{3}次", xmno, module, commend.commendName, i), port);
                    return false;
                }
            }
            return false;
        }


        public ProcessDTUQueueLoad processDTUQueueLoad = new ProcessDTUQueueLoad();
        public List<int> GetCommendFromTaskQueue(int xmno, string pointname)
        {
            return processDTUQueueLoad.DTUQueueLoad(xmno, pointname, out mssg);
        }
        public ProcessChainDataMaxTime processChainDataMaxTime = new ProcessChainDataMaxTime();
        public ProcessDTUTimeIntervalLoad processDTUTimeIntervalLoad = new ProcessDTUTimeIntervalLoad();
        public bool DTUDataGet(Socket s, out double hcont, out string dtudata)
        {
            int port = Convert.ToInt32(clientInfo(s).ToString().Split(':')[1]);
            dtudata = "";
            hcont = 0;
            //要一条数据
            //if (times > 1) { hcont = 0; return true; };

            StringBuilder datastr = new StringBuilder(256);
            int count;
            byte[] b = new byte[4096];
            int xmno = 0;
            string pointname = "";
            string module = "";
            //设置20秒等待时间
            //bool state = s.Blocking;
            s.ReceiveTimeout = 600000;

            try
            {

                //s.Send(Encoding.UTF8.GetBytes("get rtc"));
                //ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), "get rtc", DateTime.Now));
                ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("等待{0}发回数据...  {1}", clientInfo(s), DateTime.Now), port);
                while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                {
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("等待DTU第一条数据发回获取从中获取DTU系统时间时发回来的数据【{0}】", Encoding.UTF8.GetString(b, 0, count).TrimEnd()), port);
                    var str = Encoding.UTF8.GetString(b, 0, count);
                    datastr.Append(str);
                    if (str.IndexOf("the end") != -1)
                    {
                        string recdatastr = datastr.ToString(); if (recdatastr.IndexOf("od") == -1)
                        {
                            ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("经解析【{0}】不是有效数据，现在返回，再次等待DTU主动发回数据...", recdatastr), port);
                            
                            datastr = new StringBuilder(256);
                            continue;
                        }
                        dtudata = datastr.ToString();
                    }
                    else
                        continue;

                    //int len = datastr.ToString().Split('\r').Length;
                    if ((datastr.ToString().IndexOf("2017/") != -1) || (datastr.ToString().IndexOf("2018/") != -1))
                    {

                        if (!GetDTUIDFromFirstData(s, datastr.ToString(), port, out module, out pointname, out xmno))
                        {

                            datastr = new StringBuilder(256);
                            //s.Send(Encoding.UTF8.GetBytes("get rtc"));
                            //ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), "get rtc", DateTime.Now));
                            ExceptionLog.NGNFixedInclinometerRecordWrite("校正DTU时间失败，现在返回，再次等待DTU主动发回数据...", port);
                            
                            
                            b = new byte[4096];
                            s.ReceiveTimeout = 300000;
                            continue;
                        }
                    }
                    else
                    {
                        string datastring = datastr.ToString();
                        if (datastring.IndexOf("the end") != -1)
                        {
                            datastr = new StringBuilder(256);
                            ExceptionLog.NGNFixedInclinometerRecordWrite("DTU发回的第一条数据格式有错，现在返回，再次等待DTU数据发回...", port);
                            continue;
                            //s.Send(Encoding.UTF8.GetBytes("get rtc"));
                            //ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), "get rtc", DateTime.Now));
                            //ExceptionLog.NGNFixedInclinometerRecordWrite("校正DTU时间失败，现在返回，再次等待DTU主动发回数据...");
                        }
                        b = new byte[4096];
                        s.ReceiveTimeout = 300000;
                        continue;
                    }

                    //UsingSockectRevData.WaitOne();
                    //UsingSockectDTUID

                    //System.Threading.Monitor.Enter(UsingHistoryContLocker);
                    int cnt = 0; int h = 0, m = 0;
                    s.ReceiveTimeout = 300000;
                    //检查电压
                    if (12 < DateTime.Now.Hour && DateTime.Now.Hour < 20)
                    {
                        ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("现在是{0}:{1}检查电池电压...", DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00")), port);
                        s.Send(Encoding.UTF8.GetBytes("check DY"));

                        ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), "check DY", DateTime.Now));

                        b = new byte[4096];
                        s.ReceiveTimeout = 60000;
                        string DY = "";
                        //调试注释
                        while ((cnt = s.Receive(b, SocketFlags.None)) > 0)
                        {
                            string dystr = Encoding.UTF8.GetString(b, 0, cnt);
                            if (dystr.IndexOf("DY") == -1) { s.Send(Encoding.UTF8.GetBytes("check DY")); b = new byte[4096]; continue; }
                            DY = dystr;
                            break;
                        }
                        ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("获取项目编号{0}模块{1}的电压【{2}】", xmno, module, DY), port);
                    }
                    //时间间隔

                    //s.Send(Encoding.UTF8.GetBytes("check fun"));

                    //ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), "check fun", DateTime.Now));
                    //cnt = 0;
                    //b = new byte[4096];
                    //s.ReceiveTimeout = 60000;
                    //while ((cnt = s.Receive(b, SocketFlags.None)) > 0)
                    //{
                    //    string dystr = Encoding.UTF8.GetString(b, 0, cnt);
                    //    if (dystr.IndexOf("hours") == -1) { s.Send(Encoding.UTF8.GetBytes("check fun")); b = new byte[4096]; continue; }
                    //    h = Convert.ToInt32(dystr.Substring(dystr.IndexOf("hours") - 3, 3));
                    //    m = Convert.ToInt32(dystr.Substring(dystr.IndexOf("mim") - 3, 3));
                    //    break;
                    //}
                    //ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("获取项目编号{0}模块{1}的时间间隔{2}:{3}", xmno, module, h,m));


                    /*调试注释*/
                    CommendListTaskDelivery(xmno, module, s);
                    
                    if (string.IsNullOrEmpty(pointname)) return true;
                    DateTime maxTime = processChainDataMaxTime.ChainDataMaxTime(xmno, pointname, out mssg);
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("获取项目编号{0}测斜链{1}的最大时间{2},{3}", xmno, point_name, maxTime, mssg), port);
                    DTU.Model.dtutimetask task = processDTUTimeIntervalLoad.DTUTimeIntervalLoad(xmno, module, out mssg);
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("获取项目编号{0}模块{1}的时间间隔{2}:{3},{4}", xmno, module, task.hour, task.minute, mssg), port);
                    h = task.hour; m = task.minute;
                    //if (task.hour == 0 && task.minute == 0) return 0;
                    double toBeDivMinute = (h * 60 + m) * 60;
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("获取项目编号{0}模块{1}的间隔秒数{2}", xmno, module, toBeDivMinute), port);
                    if (toBeDivMinute == 0) { hcont = 0; return true; }
                    double totalsec = (DateTime.Now - maxTime).TotalSeconds;
                    double totalcount = Math.Floor(totalsec / toBeDivMinute);
                    //System.Threading.Monitor.Exit(UsingHistoryContLocker);
                    //FlushEvent += ClientDataFlush;
                    totalcount = totalcount > 1000 ? 1000 : totalcount;
                    ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("{0}有{1}条数据未上传，现在下载...", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, totalcount), port);
                    //this.listBox1.EndInvoke(ar);
                    //FlushEvent -= ClientDataFlush;
                    
                    hcont = totalcount;
                    

                    return true;
                }
                //DataRec(s,port);
                //是否需要给设备发送命令，如果没有关闭连接
                //s.Shutdown(SocketShutdown.Both);
            }
            catch (Exception ex)
            {

                //FlushEvent += ClientDataFlush;
                //IAsyncResult ar = this.listBox1.BeginInvoke(FlushEvent, string.Format("接收{0}  从{1}发来的数据超时，现在向客户端发送数据获取请求", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port));
                ////DTUDATAImport(datastr);
                //this.listBox1.EndInvoke(ar);
                //FlushEvent -= ClientDataFlush;
                ////向客户端发送数据获取请求
                //DTUDATARequestSend(s, port);
                //string strdate = "2018/01/03 11:24:33 od";
                //int startIndex = strdate.IndexOf("2018/");
                //strdate = strdate.Substring(strdate.IndexOf("2018/"), 17);
                //if (times > 0) { hcont = 0; return true; };
                ////s.Send(Encoding.UTF8.GetBytes("get rtc"));
                ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("等待{0}发回数据超时...  {1}", clientInfo(s), DateTime.Now), port);
                ////ExceptionLog.NGNFixedInclinometerRecordWrite(string.Format("获取DTU{0}的历史条数出错,错误信息{1} {2}", clientInfo(s), ex.Message, DateTime.Now));
                //s.Send(Encoding.UTF8.GetBytes("get rtc"));
                //ExceptionLog.NGNFixedInclinometerCommendRecordWrite(string.Format("向{0}发送命令{1}  {2}", clientInfo(s), "get rtc", DateTime.Now));
                //DTUDataGet(s, 1,out hcont,out dtudata);
                //return 0;
                return true;
            }
            //hcont = 0;

            return true;

        }

        #endregion

    }
}
