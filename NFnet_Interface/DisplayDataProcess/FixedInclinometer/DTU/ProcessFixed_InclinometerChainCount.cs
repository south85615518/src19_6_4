﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer.DTU
{
    public class ProcessFixed_InclinometerChainCount
    {
        public ProcessFixed_Inclinomater_chainBLL processFixed_Inclinomater_chainBLL = new ProcessFixed_Inclinomater_chainBLL();
        public string Fixed_InclinometerChainCount(int xmno,out string mssg )
        {
            var model = new ProcessFixed_Inclinomater_chainBLL.Fixed_inclinometerPointChainTableCountLoadModel(xmno);
            if (processFixed_Inclinomater_chainBLL.PointTableCountLoad(model, out mssg))
            {
                return model.cnt;
            }
            return "";
        }
    }
}
