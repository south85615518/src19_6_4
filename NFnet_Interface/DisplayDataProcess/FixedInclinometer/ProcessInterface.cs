﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.UserProcess;
using NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.FixedInclinometer.NGN;

namespace NFnet_Interface.DisplayDataProcess.FixedInclinometer
{
    public class ProcessInterface
    {
        
        public static ProcessDTUPortXm processNGNPortXm = new ProcessDTUPortXm();

        public static int GetNGNXmnoFromPort(int port)
        {
            string mssg = "";
            return processNGNPortXm.DTUPortXm(port, out mssg).xmno;
        }
        public static ProcessDTUPortXmname processNGNPortXmname = new ProcessDTUPortXmname();
        public static string GetNGNXmnameFromPort(int port)
        {
            string mssg = "";
            return processNGNPortXmname.DTUPortXm(port, out mssg);
        }
        public static ProcessDeviceModelGet processDeviceModelGet = new ProcessDeviceModelGet();
        public static ProcessChainModelGet processChainModelGet = new ProcessChainModelGet();
        public static global::NGN.Model.fixed_inclinometer_device NGNDeviceModel(int xmno, int port, int od, int addressno, out string mssg)
        {
            return processDeviceModelGet.DeviceModelGet(xmno, Convert.ToInt32(od), Convert.ToInt32(addressno), port, out mssg);

            //return model.point_name;
        }
        public static ProcessNGNDeviceModelGet processNGNDeviceModelGet = new ProcessNGNDeviceModelGet();
        public static global::NGN.Model.fixed_inclinometer_device NGNFixedInclinometerDeviceModel(int xmno, int port, int od, int addressno, out string mssg)
        {
            return processNGNDeviceModelGet.NGNDeviceModelGet(xmno, Convert.ToInt32(od), Convert.ToInt32(addressno), port, out mssg);

            //return model.point_name;
        }



        public static global::NGN.Model.fixed_inclinometer_chain NGNChainModel(int xmno, int port, int od, out string mssg)
        {
            return processChainModelGet.ChainModelGet(xmno, port, Convert.ToInt32(od), out mssg);

            //return model.point_name;
        }



        //public static ProcessChainModelGet processChainModelGet = new ProcessChainModelGet();
        public static bool NGNModulePointName(int xmno, int port, string addressno, out string pointname)
        {
            pointname = "";
            //string mssg = "";
            //global::NGN.Model.fixed_inclinometer_device model = processPortModuleDeviceGet.PortModuleDeviceGet(xmno, port, addressno, out mssg);
            //pointname = model.point_name;
            return true;
        }

        public static bool NGNModuleChainName(int xmno, int addressno, int port, out string chainname)
        {
            string mssg = "";
            global::NGN.Model.fixed_inclinometer_chain model = processChainModelGet.ChainModelGet(xmno, addressno, port, out mssg);
            chainname = model.chain_name;
            return true;
        }

    }
}
