﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_Interface.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessDeviceDelete
    {
        public ProcessFixed_Inclinomater_deviceBLL processFixed_Inclinomater_deviceBLL = new ProcessFixed_Inclinomater_deviceBLL();
        public bool deviceDelete(int xmno, string devicename, out string mssg)
        {
            var model = new ProcessFixed_Inclinomater_deviceBLL.(xmno, devicename);
            return processFixed_Inclinomater_deviceBLL.Delete(model, out mssg);

        }
    }
}
