﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessLackPoints
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public string LackPoints(string xmname, int xmno, int cyc,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessLostPointsModel(xmname,xmno,cyc);
            if (processResultDataBLL.ProcessLostPoints(model, out mssg)) 
                return model.pointstr; 
                return "";
        }
    }
}
