﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
   public class ProcessPointResultDataMaxTime
    {

       public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public  DateTime PointResultDataMaxTime(string xmname,string pointname,out string mssg)
        {
            var pointNewestDateTimeCondition = new PointNewestDateTimeCondition(xmname,pointname,data.Model.gtsensortype._HorizontalDisplacement);

            if (processResultDataBLL.ProcessPointNewestDateTimeGet(pointNewestDateTimeCondition, out mssg))
            {
                return pointNewestDateTimeCondition.dt;
            }
            return new DateTime();
        }
    }
}
