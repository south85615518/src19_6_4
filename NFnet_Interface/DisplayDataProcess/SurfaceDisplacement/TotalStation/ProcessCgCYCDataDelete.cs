﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessCgCYCDataDelete
    {
        public ProcessTotalStationBLL totalstationBLL = new ProcessTotalStationBLL();
        public bool CgCYCDataDelete(int startcyc, int endcyc, string xmname, string pointname, out string mssg)
        {
            var model = new ProcessTotalStationBLL.ProcessCycdirnetCYCDataDeleteModel(startcyc, endcyc, xmname, pointname);
            return totalstationBLL.ProcessCgCycdirnetCYCDataDelete(model, out mssg);
        }
    }
}
