﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSurveyDataUpdate
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyDataUpdate(global::TotalStation.Model.fmos_obj.cycdirnet model,out string mssg)
        {
            //var model = new ProcessResultDataBLL.ProcessSurveyDataUpdataModel(xmname, pointname, vSet_name, vLink_name, cyc,srcval);
            return processResultDataBLL.ProcessSurveyDataUpdata(model,out mssg);
        }
    }
}
