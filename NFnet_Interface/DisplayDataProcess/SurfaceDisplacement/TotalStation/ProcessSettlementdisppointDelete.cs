﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSettlementdisppointDelete
    {
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public bool SettlementdisppointDelete(int xmno,string point_name, out string mssg)
        {
            var model = new ProcessPointAlarmBLL.ProcessSettlementDispPointReferenceDeleteModel(xmno,point_name);
            return processPointAlarmBLL.ProcessSettlementDispPointReferenceDelete(model, out mssg);
        }
    }
}
