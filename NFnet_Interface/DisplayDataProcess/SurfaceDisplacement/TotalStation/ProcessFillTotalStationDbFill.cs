﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessFillTotalStationDbFill
    {
        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public  ProcessResultDataCom.ProcessfillTotalStationDbFillModel   FillTotalStationDbFill(string pointstr,string sql,string xmname,Role role,bool tempRole)
        {

            var fillTotalStationDbFillCondition = new NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition(pointstr, sql, xmname);
            var processfillTotalStationDbFillModel = new ProcessResultDataCom.ProcessfillTotalStationDbFillModel(fillTotalStationDbFillCondition,role, tempRole);
            resultDataCom.ProcessfillTotalStationDbFill(processfillTotalStationDbFillModel);
            return processfillTotalStationDbFillModel;
        }
        public ProcessResultDataCom.ProcessfillTotalStationDbFillModel FillTotalStationDbFillNotExcute(string pointstr, string sql, string xmname, Role role, bool tempRole)
        {

            var fillTotalStationDbFillCondition = new NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition(pointstr, sql, xmname);
            var processfillTotalStationDbFillModel = new ProcessResultDataCom.ProcessfillTotalStationDbFillModel(fillTotalStationDbFillCondition, role, tempRole);
            resultDataCom.ProcessfillTotalStationDbFillNotExcute(processfillTotalStationDbFillModel);
            return processfillTotalStationDbFillModel;
        }
    }
}
