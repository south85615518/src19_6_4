﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
   public class ProcessResultDataMaxTime
    {

        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public  DateTime ResultDataMaxTime(string xmname,Role role,bool tempRole,out string mssg)
        {
            var processResultDataMaxTime = new TotalStationResultDataMaxTimeCondition(xmname);
            var processResultDataMaxTimeCom = new ProcessResultDataCom.ProcessResultDataMaxTimeModel(processResultDataMaxTime,role, tempRole);
            if (!resultDataCom.ProcessResultDataMaxTime(processResultDataMaxTimeCom, out mssg)) return new DateTime();
            return processResultDataMaxTimeCom.model.maxTime;
            

        }
    }
}
