﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessDeleteCgDataCycOnChain
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool DeleteCgDataCycOnChain(string xmname,int cyc,out string mssg)
        {
            var model = new NFnet_BLL.DataProcess.ProcessResultDataBLL.InsertCycStepModel(xmname,cyc);
            return processResultDataBLL.DeleteCgDataCycOnChain(model,out mssg);
        }
    }
}
