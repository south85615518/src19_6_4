﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSurveyDataCouldUnion
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyDataCouldUnion(string xmname,int cyc,int targetcyc,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessSurveyDataCouldUnionModel(xmname,cyc,targetcyc);
            return processResultDataBLL.ProcessSurveyDataCouldUnion(model,out mssg);
        }
    }
}
