﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.Strain.Sensor;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSurveyDataLackPointsModellistFromCgDataModelGet
    {
        public NFnet_BLL.DataProcess.ProcessResultDataBLL processResultDataBLL = new NFnet_BLL.DataProcess.ProcessResultDataBLL();
        public List<global::TotalStation.Model.fmos_obj.cycdirnet> SurveyDataLackPointsModellistFromCgDataModelGet(int xmno,List<string> pointnamelist,out string mssg)
        {
            var model = new NFnet_BLL.DataProcess.ProcessResultDataBLL.GetSurveyDataLackPointsModellistFromCgDataModel(xmno,pointnamelist);
            if (processResultDataBLL.GetSurveyDataLackPointsModellistFromCgData(model, out mssg))
                return model.modellist;
            return null;
        }
    }
}
