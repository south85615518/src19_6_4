﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessResultDataPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string ResultDataPlotLineAlarm(string pointnamestr, string xmname,int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<TotalStationPlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessResultDataAlarmBLL processResultDataAlarmBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
                List<TotalStationPlotlineAlarmModel> lmm = new List<TotalStationPlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    var pointvalue = processResultDataAlarmBLL.TestPointAlarmValue(pointname);
                    var alarmList = processResultDataAlarmBLL.TestAlarmValueList(pointvalue);
                    TotalStationPlotlineAlarmModel model = new TotalStationPlotlineAlarmModel
                    {
                        pointname = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                    break;
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移预警参数加载出错,错误信息:"+ex.Message);
                return null;
            }
        }
    }
}
