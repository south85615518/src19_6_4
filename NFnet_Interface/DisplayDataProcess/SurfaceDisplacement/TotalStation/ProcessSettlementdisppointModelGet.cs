﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSettlementdisppointModelGet
    {
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public PointAttribute.Model.fmos_settlementdisprelationship SettlementdisppointModelGet(int xmno,string pointname,out string mssg)
        {
            var model = new ProcessPointAlarmBLL.ProcessSettlementDispPointReferenceModelLoadModel(xmno,pointname);
            if (processPointAlarmBLL.ProcessSettlementDispPointReferenceModelLoad(model, out mssg))
                return model.model;
            return new PointAttribute.Model.fmos_settlementdisprelationship();
        }
    }
}
