﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessTotalStationPointLoad
    {
        public  ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public List<string> TotalStationPointLoad(int xmno,out string mssg)
        {
            var processTotalStationPointLoadModel = new TotalStationPointLoadCondition(xmno);
            var processTotalStationPointLoadModelCom = new ProcessResultDataCom.ProcessTotalStationPointLoadModel(processTotalStationPointLoadModel, Role.administratrorModel);
            if (resultDataCom.ProcessTotalStationPointLoad(processTotalStationPointLoadModelCom, out mssg))
            {
                return processTotalStationPointLoadModelCom.model.ls;
            }
            else
            {
                return null;
            }
        }
    }
}
