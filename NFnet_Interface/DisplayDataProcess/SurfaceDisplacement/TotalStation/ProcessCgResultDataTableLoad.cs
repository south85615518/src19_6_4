﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessCgResultDataTableLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public DataTable CgResultDataTableLoad(string xmname, string pointname, int pageIndex, int rows, int startCyc, int endCyc, string sord,out string mssg)
        {
            var model = new ResultDataLoadCondition(xmname,pointname,pageIndex,rows,startCyc,endCyc,sord);
            if (processResultDataBLL.ProcessCgResultDataLoad(model,out mssg))
            {
                return model.dt;
            }
            return null;
        }
    }
}
