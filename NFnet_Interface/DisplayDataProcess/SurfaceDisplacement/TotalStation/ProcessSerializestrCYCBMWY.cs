﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.LoginProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSerializestrCYCBMWY
    {
        public List<serie_cyc_null> SerializestrCYCBMWY(List<string> terminalCyc, object sql, object xmname, object pointname, object zus, Role role, bool tempRole, out string mssg)
        {

            var processSerializestrBMWY_cycModel = new SerializestrBMWYC_CYCondition(sql, xmname, pointname, zus, terminalCyc);
            var processSerializestrBMWY_cycModelCom = new ProcessTotalStationChartCom.ProcessSerializestrCYCBMWYModel(processSerializestrBMWY_cycModel, role, tempRole);
            if (ProcessTotalStationChartCom.ProcessSerializestrCYCBMWY(processSerializestrBMWY_cycModelCom, out mssg))
            {
                return processSerializestrBMWY_cycModel.serie_cycs;
            }
            return null;
        }
    }
}
