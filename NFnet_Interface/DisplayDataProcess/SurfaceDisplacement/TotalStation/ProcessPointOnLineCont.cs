﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess
{
    public class ProcessPointOnLineCont
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public string PointOnLineCont(string unitname,List<string> finishedxmnolist,out string mssg)
        {
            var pointOnLineContCondition = new PointOnLineContCondition(unitname,finishedxmnolist);
            if (processResultDataBLL.ProcessPointOnLineCont(pointOnLineContCondition, out mssg))
            {
                return pointOnLineContCondition.contpercent;
            }
            return "0/0";
        }
    }
}
