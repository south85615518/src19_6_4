﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.AchievementGeneration.TotalStationDAL;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
   public class ProcessPointResultDataTimeModelGet
    {

       public ProcessCycdirnetBLL processCycdirnetBLL = new ProcessCycdirnetBLL();
       public global::TotalStation.Model.fmos_obj.cycdirnet PointResultDataTimeModelGet(string xmname, string pointname,DateTime time, out string mssg)
        {
            var cycdirnetTimeModelGetCondition = new CycdirnetTimeModelGetCondition(xmname,pointname,time);
            if (processCycdirnetBLL.ProcessCycdirnetModel(cycdirnetTimeModelGetCondition,out mssg))
            {
                return cycdirnetTimeModelGetCondition.model;
            }
            return new global::TotalStation.Model.fmos_obj.cycdirnet();
        }
    }
}
