﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
   public class ProcessResultDataExtremely
    {
        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        /// <summary>
        /// 获取端点周期成功
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="aggregateFunction"></param>
        /// <param name="role"></param>
        /// <param name="tempRole"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public string ResultDataExtremely(string xmname, string aggregateFunction, Role role, bool tempRole, out string mssg)
        {

            var processResultDataExtremelyModel = new ResultDataExtremelyCondition(xmname, aggregateFunction);
            var processResultDataExtremelyModelCom = new ProcessResultDataCom.ProcessResultDataExtremelyModel(processResultDataExtremelyModel, role, tempRole);
            if (resultDataCom.ProcessResultDataExtremely(processResultDataExtremelyModelCom, out mssg))
            {
                return processResultDataExtremelyModelCom.model.extremelyCyc;

            }
            else
            {
                return "";
            }

        }
    }
}
