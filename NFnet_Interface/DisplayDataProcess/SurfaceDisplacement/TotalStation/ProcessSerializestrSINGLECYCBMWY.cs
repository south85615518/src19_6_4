﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSerializestrSINGLECYCBMWY
    {
        public List<serie_cyc_null> SerializestrSINGLECYCBMWY(object singlestartcyc,object singleendcyc , object xmname, object pointname, object zus, Role role, bool tempRole, out string mssg)
        {
            List<string> terminalCyc = new List<string>();
            
            var processSerializestrBMWY_Single_cycModel = new SerializestrBMWYC_SINGLE_CYCondition(xmname, pointname, singlestartcyc,singleendcyc,zus);
            var processSerializestrBMWY_cycModelCom = new ProcessTotalStationChartCom.ProcessSerializestrSINGLECYCBMWYModel(processSerializestrBMWY_Single_cycModel, role);
            if (ProcessTotalStationChartCom.ProcessSerializestrSINGLECYCBMWY(processSerializestrBMWY_cycModelCom, out mssg))
            {
                return processSerializestrBMWY_cycModelCom.model.serie_cycs;
            }
            return null;
        }
    }
}
