﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSettlementdisppointAdd
    {
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public bool SettlementdisppointAdd(PointAttribute.Model.fmos_settlementdisprelationship model, out string mssg)
        {
            return processPointAlarmBLL.ProcessSettlementDispPointReferenceAdd(model,out mssg);
        }
    }
}
