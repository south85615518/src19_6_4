﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.Strain.Sensor;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessCgTimeSortModellistGet
    {
        public NFnet_BLL.DataProcess.ProcessResultDataBLL processResultDataBLL = new NFnet_BLL.DataProcess.ProcessResultDataBLL();
        public List<global::TotalStation.Model.fmos_obj.cycdirnet> CgTimeSortModellistGet(int xmno,  out string mssg)
        {
            var model = new NFnet_BLL.DataProcess.ProcessResultDataBLL.GetCgTimeSortModellistModel(xmno);
            if (processResultDataBLL.GetCgTimeSortModellist(model, out mssg))
                return model.modellist;
            return null;
        }
    }
}
