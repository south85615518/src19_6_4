﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessResultSurveyDataImport
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool ResultSurveyDataImport(string xmname,int cyc,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessResultDataAddModel(xmname,cyc);
            return processResultDataBLL.ProcessResultDataAdd(model,out mssg);
        }
    }
}
