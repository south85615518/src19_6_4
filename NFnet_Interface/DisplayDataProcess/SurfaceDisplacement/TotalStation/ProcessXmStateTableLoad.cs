﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
   public class ProcessXmStateTableLoad
    {
       public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
       public DataTable XmStateTableLoad(int pageIndex,int pageSize,int xmno,string xmname,string unitname,string colName,string sord,out string mssg)
       {
           var model = new XmStateTableLoadCondition(pageIndex,pageSize,xmno,xmname,unitname,colName,sord);
           if (processResultDataBLL.ProcessXmStateTableLoad(model, out mssg))
           {
               return model.dt;
               //return true;
           }
           return new DataTable();
       }
    }
}
