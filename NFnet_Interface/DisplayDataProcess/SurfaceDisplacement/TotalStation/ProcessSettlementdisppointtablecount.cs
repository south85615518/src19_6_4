﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSettlementdisppointtablecount
    {
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public string Settlementdisppointtablecount(string searchstring,int xmno,out string mssg)
        {
            var model = new ProcessPointAlarmBLL.ProcessSettlementDispPointReferenceTableCountLoadModel(xmno,searchstring);
            if (processPointAlarmBLL.ProcessSettlementDispPointReferenceTableCountLoad(model, out mssg))
                return model.totalCont;
            return "";
        }
    }
}
