﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessResultDataTimeCycLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<string> ResultDataTimeCycLoad(string xmname,out string mssg)
        {
            var processResultDataTimeCycLoadModel = new NFnet_BLL.DataProcess.ProcessResultDataBLL.ProcessResultDataTimeCycLoadModel(xmname);
            if (processResultDataBLL.ProcessResultDataTimeCycLoad(processResultDataTimeCycLoadModel, out mssg))
            {
                return processResultDataTimeCycLoadModel.cyctimelist;
            }
            return new List<string>();
        }
    }
}
