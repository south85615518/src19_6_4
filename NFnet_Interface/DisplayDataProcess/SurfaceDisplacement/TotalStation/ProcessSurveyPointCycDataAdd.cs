﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSurveyPointCycDataAdd
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyPointCycDataAdd(string xmname, string pointname, int cyc, int importcyc,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessSurveyPointCycDataAddModel(xmname,pointname,cyc,importcyc);
            return processResultDataBLL.ProcessSurveyPointCycDataAdd(model,out mssg);
        }
    }
}
