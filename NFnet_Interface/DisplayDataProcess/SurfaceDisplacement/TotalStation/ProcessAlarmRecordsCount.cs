﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessAlarmRecordsCount
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public int AlarmRecordsCount(int xmno, string colName, int pageIndex, int rows, string sord, out string mssg)
        {
            var processAlarmRecordsCountModel = new ProcessAlarmBLL.ProcessAlarmRecordsCountModel(xmno, colName, pageIndex, rows, sord);



            if (alarmBLL.ProcessAlarmRecordsCount(processAlarmRecordsCountModel, out mssg))
            {
                return Convert.ToInt32(processAlarmRecordsCountModel.totalCont);

            }
            else
            {
                //计算结果数据数量出错信息反馈
                return 0;
            }
        }
    }
}
