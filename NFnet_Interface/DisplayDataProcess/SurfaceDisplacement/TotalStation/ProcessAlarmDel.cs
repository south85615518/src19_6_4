﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessAlarmDel
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmDel(NFnet_DAL.MODEL.alarmvalue model,out string mssg)
        {
            
            return alarmBLL.ProcessAlarmDel(model, out mssg);
        }
    }
}
