﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessPointAlarmValueMutilEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueMutilEdit(PointAttribute.Model.fmos_pointalarmvalue model,string pointstr ,out string mssg)
        {
            var processAlarmMultiUpdateModel = new ProcessPointAlarmBLL.ProcessAlarmMultiUpdateModel(pointstr, model);
            pointAlarmBLL.ProcessAlarmMultiUpdate(processAlarmMultiUpdateModel, out mssg);
        }
    }
}
