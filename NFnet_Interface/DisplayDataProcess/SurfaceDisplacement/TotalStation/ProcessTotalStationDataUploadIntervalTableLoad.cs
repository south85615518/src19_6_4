﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Strain.SurfaceDisplacement.totalstation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessTotalStationDataUploadIntervalTableLoad
    {
        public static ProcessTotalStationDataUploadInterval processTotalStationDataUploadInterval = new ProcessTotalStationDataUploadInterval();
        public DataTable TotalStationDataUploadIntervalTableLoad(string xmname,int xmno,out string mssg)
        {
            var model = new ProcessTotalStationDataUploadInterval.TableLoadModel(xmname,xmno);
            if (processTotalStationDataUploadInterval.TableLoad(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
