﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessAlarmValues
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public string AlarmValues(int xmno, string alarmvaluenamestr,out string mssg)
        {
            var alarmvaluename = new ProcessAlarmBLL.ProcessAlarmValueNameModel(xmno, alarmvaluenamestr);
            if (alarmBLL.ProcessAlarmValueName(alarmvaluename, out mssg))
            {
                return alarmvaluename.alarmValueNameStr;
            }
             return mssg;          
        }
    }
}
