﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessPointAlarmValueAdd
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueAdd(PointAttribute.Model.fmos_pointalarmvalue model,out string mssg)
        {
             pointAlarmBLL.ProcessAlarmAdd(model,out mssg);
        }
    }
}
