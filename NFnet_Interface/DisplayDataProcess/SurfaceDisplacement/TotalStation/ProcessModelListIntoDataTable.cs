﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessModelListIntoDataTable
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public DataTable ModelListIntoDataTable(string xmname,List<global::TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessModelListIntoDataTableModel(xmname,cycdirnetlist);
            if (processResultDataBLL.ProcessModelListIntoDataTable(model, out mssg))
            {
                return model.dt;
            }
            return null;
        }
    }
}
