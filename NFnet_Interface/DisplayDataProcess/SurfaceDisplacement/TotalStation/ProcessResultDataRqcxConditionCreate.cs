﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessResultDataRqcxConditionCreate
    {
        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public ResultDataRqcxConditionCreateModel ResultDataRqcxConditionCreate(string startCyc, string endCyc, QueryType QT,string range ,string xmname, DateTime maxTime)
        {
            List<string> ls = new List<string>();

            var processResultDataRqcxConditionCreateModel = new ResultDataRqcxConditionCreateCondition(startCyc, endCyc, QT, range, xmname, maxTime);
            resultDataBLL.ProcessResultDataRqcxConditionCreate(processResultDataRqcxConditionCreateModel);
            
            ls.Add(processResultDataRqcxConditionCreateModel.minCyc);
            ls.Add(processResultDataRqcxConditionCreateModel.maxCyc);
            string sql = processResultDataRqcxConditionCreateModel.sql;
            return new ResultDataRqcxConditionCreateModel(sql, ls, processResultDataRqcxConditionCreateModel.startTime, processResultDataRqcxConditionCreateModel.endTime);
        }
        public class ResultDataRqcxConditionCreateModel
        {
            public string sql {get;set;}
            public List<string> terminalCyc { get; set; }
            public string startTime { get; set; }
            public string endTime { get; set; }
            public ResultDataRqcxConditionCreateModel(string sql, List<string> terminalCyc, string startTime, string endTime)
            {
                this.sql = sql;
                this.terminalCyc = terminalCyc;
                this.startTime = startTime;
                this.endTime = endTime;
            }
        }
        
    }
}
