﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSettlementdisppointTableLoad
    {
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public DataTable SettlementdisppointTableLoad(int xmno, string colName, int pageIndex, int rows, string sord,out string mssg)
        {
            var model = new ProcessPointAlarmBLL.ProcessSettlementDispPointReferenceTableLoadModel(xmno, colName, pageIndex, rows, sord);
            if (processPointAlarmBLL.ProcessSettlementDispPointReferenceTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
}
