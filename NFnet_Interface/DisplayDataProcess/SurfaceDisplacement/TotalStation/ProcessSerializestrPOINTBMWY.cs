﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
   public class ProcessSerializestrPOINTBMWY
    {
      //public static 
       public List<serie_point> SerializestrPOINTBMWY(object sql,object xmname,object pointname,object zus,Role role,bool tempRole,int rows,int pageIndex,out string mssg)
       {
           var processSerializestrBMWYModel = new SerializestrBMWY_POINTCondition(sql,xmname,pointname, zus,rows,pageIndex);
           var processSerializestrBMWYModelCom = new ProcessTotalStationChartCom.ProcessSerializestrPOINTBMWYModel(processSerializestrBMWYModel, role, tempRole);
           if (ProcessTotalStationChartCom.ProcessSerializestrPOINTBMWY(processSerializestrBMWYModelCom, out mssg))
           {
               return processSerializestrBMWYModel.serie_points;
           }
           return null;
       }
       public List<serie_point> SerializestrPOINTBMWY(object sql, object xmname, object pointname, object zus, Role role, bool tempRole, out string mssg)
       {
           var processSerializestrBMWYModel = new SerializestrBMWY_POINTCondition(sql, xmname, pointname, zus);
           var processSerializestrBMWYModelCom = new ProcessTotalStationChartCom.ProcessSerializestrPOINTBMWYModel(processSerializestrBMWYModel, role, tempRole);
           if (ProcessTotalStationChartCom.ProcessSerializestrPOINTBMWY(processSerializestrBMWYModelCom, out mssg))
           {
               return processSerializestrBMWYModel.serie_points;
           }
           return null;
       }
    }
}
