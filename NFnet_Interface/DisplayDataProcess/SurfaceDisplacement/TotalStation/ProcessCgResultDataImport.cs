﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessCgResultDataImport
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool CgResultDataImport(string xmname,int cyc,int importcyc,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessCgResultDataAddModel(xmname,cyc,importcyc);
            return processResultDataBLL.ProcessCgResultDataAdd(model,out mssg);
        }
    }
}
