﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSurveyCYCDataDelete
    {
        public ProcessTotalStationBLL totalstationBLL = new ProcessTotalStationBLL();
        public bool SurveyCYCDataDelete(int startcyc, int endcyc, string xmname, string pointname, out string mssg)
        {
            var model = new ProcessTotalStationBLL.ProcessCycdirnetCYCDataDeleteModel(startcyc, endcyc, xmname, pointname);
            return totalstationBLL.ProcessSurveyCYCDataDelete(model, out mssg);
        }
    }
}
