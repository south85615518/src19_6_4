﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSurveyCycList
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<string> SurveyCycList(string xmname, out string mssg)
        {
            var model = new ProcessResultDataBLL.SurveyCycListModel(xmname);
            if (processResultDataBLL.SurveyCycList(model, out mssg))
                return model.cyclist;
            return null;
        }
    }
}
