﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessCgCycList
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<string> CgCycList(string xmname, out string mssg)
        {
            var model = new ProcessResultDataBLL.CgCycListModel(xmname);
            if (processResultDataBLL.CgCycList(model, out mssg))
                return model.cyclist;
            return null;
        }
    }
}
