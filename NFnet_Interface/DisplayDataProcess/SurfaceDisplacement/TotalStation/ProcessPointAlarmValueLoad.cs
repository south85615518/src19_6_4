﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessPointAlarmValueLoad
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public DataTable PointAlarmValueLoad(string xmname,int xmno,string searchstring,string colName,int pageIndex,int rows,string sord,out string mssg)
        {
            ExceptionLog.ExceptionWrite("现在加载预警点");
            try
            {
                var processPointAlarmLoadModel = new ProcessPointAlarmBLL.ProcessAlarmLoadModel(xmname, xmno, searchstring, colName, pageIndex, rows, sord);
                if (pointAlarmBLL.ProcessPointAlarmLoad(processPointAlarmLoadModel, out mssg))
                {
                    return processPointAlarmLoadModel.dt;
                }
                else
                {
                    //加载结果数据出错错误信息反馈
                    return new DataTable();
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取预警点出错,错误信息："+ex.Message);
                return null;
            }
        }
    }
}
