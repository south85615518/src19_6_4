﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessPointAlarmRecordsCount
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public int PointAlarmRecordsCount(string xmname, int xmno, string searchstring, int pageIndex, int rows, string sord, out string mssg)
        {
            var processPointAlarmRecordsCountModel = new ProcessPointAlarmBLL.ProcessAlarmRecordsCountModel(xmname, xmno, searchstring, pageIndex, rows, sord);
            if (pointAlarmBLL.ProcessPointAlarmRecordsCount(processPointAlarmRecordsCountModel, out mssg))
            {
                return Convert.ToInt32(processPointAlarmRecordsCountModel.totalCont);

            }
            else
            {
                //计算结果数据数量出错信息反馈
                return 0;
            }
        }
    }
}
