﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSurveyDataUnion
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyDataUnion(string xmname,int cyc,int targetcyc,int lastcyc,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessSurveyDataUnionModel(xmname,cyc,targetcyc,lastcyc);
            return processResultDataBLL.ProcessSurveyDataUnion(model,out mssg);
        }
    }
}
