﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
   public  class ProcessDateTimeToCyc
    {
       public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
       public bool DateTimeToCyc(string xmname, string startTime, string endTime, string pointnamestr,out List<string> cycList,Role role,bool tempRole,out string mssg)
       {
           cycList = new List<string>();
           var processDateTimeToCycModel = new DateTimeToCycCondition(xmname, startTime, endTime, pointnamestr);
           var processDateTimeToCycModelCom = new ProcessResultDataCom.ProcessDateTimeToCycModel(processDateTimeToCycModel, role, tempRole);
           if (!resultDataCom.ProcessDateTimeToCyc(processDateTimeToCycModelCom, out mssg)) return false;
           cycList = processDateTimeToCycModelCom.model.cycList;
           return true;
       }
    }
}
