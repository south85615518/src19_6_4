﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.LoginProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSerializestrBMWY
    {
        
        public List<serie> SerializestrBMWY(object sql,object xmname,object pointname,object zus,Role role,bool tempRole,out string mssg)
        {
            var processSerializestrBMWYModel = new SerializestrBMWYCondition(sql,xmname,pointname,zus);
            var processSerializestrBMWYModelCom = new ProcessTotalStationChartCom.ProcessSerializestrBMWYModel(processSerializestrBMWYModel, role, tempRole);
            if (ProcessTotalStationChartCom.ProcessSerializestrBMWY(processSerializestrBMWYModelCom, out mssg))
            {
               return processSerializestrBMWYModelCom.model.series;
            }
            return null;
        }
    }
}
