﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessDataTableIntoModelList
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<global::TotalStation.Model.fmos_obj.cycdirnet> DataTableIntoModelList(DataTable dt, out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessDataTableIntoModelListModel(dt);
            if (processResultDataBLL.ProcessDataTableIntoModelList(model, out mssg))
            {
                return model.cycdirnetlist;
            }
            return null;
        }
    }
}
