﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessSurveyDataGetTimeFromCyc
    {
        public NFnet_BLL.DataProcess.ProcessResultDataBLL processResultDataBLL = new NFnet_BLL.DataProcess.ProcessResultDataBLL();
        public DateTime SurveyDataGetTimeFromCyc(int xmno,int cyc,out string mssg)
        {
            var model = new NFnet_BLL.DataProcess.ProcessResultDataBLL.ProcessSurveyDataGetTimeFromCycModel(xmno,cyc);
            if (processResultDataBLL.ProcessSurveyDataGetTimeFromCyc(model, out mssg))
            {
                return model.time;
            }
            return new DateTime();
        }
    }
}
