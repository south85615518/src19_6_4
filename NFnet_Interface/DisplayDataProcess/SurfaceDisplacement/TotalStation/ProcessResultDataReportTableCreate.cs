﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessResultDataReportTableCreate
    {
        public ProcessResultDataCom resultDataCom = new ProcessResultDataCom();
        public ProcessResultDataCom.processResultDataReportTableCreateModel ResultDataReportTableCreate(string sql, string xmname, Role role, bool tempRole, out string mssg)
        {

            var resultDataReportTableCreateCondition = new ResultDataReportTableCreateCondition(sql,xmname);
            var processResultDataReportTableCreateModel = new ProcessResultDataCom.processResultDataReportTableCreateModel(resultDataReportTableCreateCondition, role, tempRole);
            if( resultDataCom.ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel, out mssg))
            {
                return processResultDataReportTableCreateModel;
            }
            return null;
        }
    }
}
