﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;

namespace NFnet_Interface.DisplayDataProcess.NGN_HXYL
{
    public class ProcessHXYLPreAcVal
    {
        public ProcessHXYLData processHXYLData = new ProcessHXYLData();
        public NGN.Model.hxyl HXYLPreAcVal(NGN.Model.hxyl model,out string mssg)
        {
            var preAcValModel = new ProcessHXYLData.PreAcValModel(model);
            if(processHXYLData.PreAcVal(preAcValModel,out  mssg))
            {
                return preAcValModel.pretimemodel;
            }
            return new NGN.Model.hxyl();
        }
    }
}
