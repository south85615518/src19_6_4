﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;

namespace NFnet_Interface.DisplayDataProcess.NGN_HXYL
{
    public class ProcessHXYLMaxTime
    {
        public ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public DateTime HXYLMaxTime(string pointnamenostr,int xmno, out string mssg)
        {
            var model = new ProcessHXYLBLL.ProcessHXYLMaxTimeModel(pointnamenostr,xmno);
            if (processHXYLBLL.ProcessHXYLMaxTime(model, out mssg))
            {
                return model.maxTime;
            }
            return new DateTime();
        }
    }
}
