﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;

namespace NFnet_Interface.DisplayDataProcess.NGN_HXYL
{
    public class ProcessHXYLIDPointName
    {
        public static ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public string HXYLPointname(int xmno, string id, out string mssg)
        {
            //ExceptionLog.ExceptionWrite(string.Format("获取项目编号{0}设备号为{1}的点名", xmno, id));
            var hXYLPointmodel = new NFnet_BLL.DisplayDataProcess.NGN_HXYL.ProcessHXYLBLL.HXYLPointmodel(xmno, id);
            if (processHXYLBLL.HXYLPointname(hXYLPointmodel, out mssg))
                return hXYLPointmodel.pointname;
            return "";
        }
    }
}
