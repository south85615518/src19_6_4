﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;

namespace NFnet_Interface.DisplayDataProcess.NGN_HXYL
{
   public class ProcessHXYLPointNewestDateTimeGet
    {
       public ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
       //public ProcessInclinometerCom inlcinometerCom = new ProcessInclinometerCom();
       public DateTime HXYLPointNewestDateTimeGet(int pointid,out string mssg)
       {
           var inclinometerPointNewestDateTimeCondition = new ProcessHXYLBLL.ProcessPointNewestDateTimeGetModel(pointid);
           if (!processHXYLBLL.ProcessPointNewestDateTimeGet(inclinometerPointNewestDateTimeCondition, out mssg))
               return new DateTime();
           return inclinometerPointNewestDateTimeCondition.dt;

       }
    }
}
