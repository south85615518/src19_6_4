﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAnglePlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string MCUAnglePlotLineAlarm(string pointnamestr, string xmname, int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    jss.Serialize(new List<MCUAnglePlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessMCUAngleDataAlarmBLL processMCUAngleDataAlarmBLL = new ProcessMCUAngleDataAlarmBLL(xmname, xmno, pointnamelist);
                List<MCUAnglePlotlineAlarmModel> lmm = new List<MCUAnglePlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    MDBDATA.Model.mcuanglepointalarm pointvalue = processMCUAngleDataAlarmBLL.TestPointAlarmValue(pointname);
                    List<MDBDATA.Model.mcuanglealarmvalue> alarmList = processMCUAngleDataAlarmBLL.TestAlarmValueList(pointvalue);
                    MCUAnglePlotlineAlarmModel model = new MCUAnglePlotlineAlarmModel
                    {
                        pointname = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("固定测斜预警参数加载出错,错误信息:" + ex.Message);
                return null;
            }
        }
    }
}
