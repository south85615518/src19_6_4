﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAlarmAdd
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmAdd(MDBDATA.Model.mcualarmvalue model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessAlarmBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.ProcessAlarmAdd(model, out mssg);
        }
    }
}
