﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUChart
    {
        public ProcessBKGMCUChartBLL bkgmuc = new ProcessBKGMCUChartBLL();
        public List<serie> SerializestrBKGMCU(object sql, object xmname, object pointname,out string mssg)
        {

            var serializestrBKGMCUModel = new SerializestrSWCondition(sql, xmname, pointname);

            if (ProcessBKGMCUChartBLL.ProcessSerializestrBKGMCU(serializestrBKGMCUModel, out mssg))
                return serializestrBKGMCUModel.series;
            return new List<serie>();


        }
    }
}
