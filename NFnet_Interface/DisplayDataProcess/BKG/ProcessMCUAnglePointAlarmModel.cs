﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAnglePointAlarmModel
    {
        public ProcessPointAlarmBLL processMCUAnglePointAlarmBLL = new ProcessPointAlarmBLL();
        public MDBDATA.Model.mcuanglepointalarm MCUAnglePointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processMCUAnglePointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processMCUAnglePointAlarmBLL.ProcessPointAlarmModelGet(processMCUAnglePointAlarmModel, out mssg))
            {
                return processMCUAnglePointAlarmModel.model;
            }
            return new MDBDATA.Model.mcuanglepointalarm();
        }
    }
}
