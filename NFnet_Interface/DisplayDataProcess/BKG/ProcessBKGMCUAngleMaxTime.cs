﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessBKGMCUAngleMaxTime
    {
        public ProcessMCUAngleBLL processMCUAngleBLL = new ProcessMCUAngleBLL();
        public DateTime BKGMCUAngleMaxTime(string xmname,out string mssg)
        {
            var model = new BKGMaxTimeCondition(xmname);
            if (processMCUAngleBLL.ProcessBKGMCUAngleMaxTime(model, out mssg))
            {
                return model.maxTime;
            }
            return new DateTime();
        }
    }
}
