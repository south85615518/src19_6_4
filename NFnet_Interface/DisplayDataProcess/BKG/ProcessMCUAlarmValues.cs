﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAlarmValues
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public string AlarmValues(int xmno,out string mssg)
        {
            var alarmvaluename = new ProcessAlarmBLL.ProcessAlarmValueNameModel(xmno);
            if (alarmBLL.ProcessAlarmValueName(alarmvaluename, out mssg))
            {
                return alarmvaluename.alarmValueNameStr;
            }
             return mssg;          
        }
    }
}
