﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.MCUBKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAlarmValueModel
    {
        public ProcessAlarmBLL processMCUAlarmValueBLL = new ProcessAlarmBLL();
        public MDBDATA.Model.mcualarmvalue MCUAlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var processMCUAlarmValueModelGetModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (processMCUAlarmValueBLL.ProcessAlarmModelGetByName(processMCUAlarmValueModelGetModel, out mssg))
            {
                return processMCUAlarmValueModelGetModel.model;
            }
            return new MDBDATA.Model.mcualarmvalue();
        }
    }
}
