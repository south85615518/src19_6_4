﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAlarmModelListGet
    {
        public ProcessBKGMCUData processMCUDATABLL = new ProcessBKGMCUData();
        public List<MDBDATA.Model.mcudata> MCUAlarmModelListGet(string xmname,List<string> pointnamelist,out string mssg)
        {
            var processMCUAlarmModelListGetModel = new ProcessBKGMCUData.ProcessMCUAlarmModelListGetModel(xmname, pointnamelist);
            if (processMCUDATABLL.ProcessMCUAlarmModelListGet(processMCUAlarmModelListGetModel, out mssg))
            {
                return processMCUAlarmModelListGetModel.model;
            }
            return new List<MDBDATA.Model.mcudata>();
        }
    }
}
