﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleDataAcc
    {
        public ProcessBKGMCUAngleData processBKGMCUAngleData = new ProcessBKGMCUAngleData();
        public bool MCUAngleDataAcc(int pid,DateTime time,double r2,out string mssg)
        {
            var model = new ProcessBKGMCUAngleData.ProcessMCUAngleDataAccModel(pid,time,r2);
            return processBKGMCUAngleData.ProcessMCUAngleDataAcc(model,out mssg);
        }
        
    }
}
