﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUTimeIntervalTableLoad
    {
        public ProcessMCUBLL processMCUBLL = new ProcessMCUBLL();
        public DataTable MCUTimeIntervalTableLoad(int xmno, string searchstring, int pageIndex, int rows, string sord, out string mssg)
        {
            var mcutimeintervalModel = new ProcessMCUBLL.mcutimeintervalModel(xmno,pageIndex,rows,sord);
            if (processMCUBLL.ProcessMCUTimeInterval(mcutimeintervalModel, out mssg))
            {
                return mcutimeintervalModel.dt;
            }
            return new DataTable();
        }
    }
}
