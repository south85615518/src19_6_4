﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleAlarmAdd
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmAdd(MDBDATA.Model.mcuanglealarmvalue model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessAlarmBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.ProcessAlarmAdd(model, out mssg);
        }
    }
}
