﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleAlarmValueModel
    {
        public ProcessAlarmBLL processMCUAngleAlarmValueBLL = new ProcessAlarmBLL();
        public MDBDATA.Model.mcuanglealarmvalue MCUAngleAlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var processMCUAngleAlarmValueModelGetModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (processMCUAngleAlarmValueBLL.ProcessAlarmModelGetByName(processMCUAngleAlarmValueModelGetModel, out mssg))
            {
                return processMCUAngleAlarmValueModelGetModel.model;
            }
            return new MDBDATA.Model.mcuanglealarmvalue();
        }
    }
}
