﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleDateTime
    {
        public ProcessMCUAngleBLL processMCUAngleBLL = new ProcessMCUAngleBLL();

        public MDBDATA.Model.mcuangledata MCUAngleDateTime(string xmname, string pointname, DateTime dt, out string mssg)
        {
            var mcuAngleDataTimeCondition = new ProcessMCUAngleBLL.McuAngleDataTimeCondition(xmname, pointname, dt);
            if (!processMCUAngleBLL.ProcessAngleMcuDataTime(mcuAngleDataTimeCondition, out mssg)) return new MDBDATA.Model.mcuangledata();
            return mcuAngleDataTimeCondition.model;

        }


    }
}
