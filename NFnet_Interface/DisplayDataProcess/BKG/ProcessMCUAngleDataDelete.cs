﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleDataDelete
    {
        public ProcessBKGMCUAngleData processBKGMCUAngleData = new ProcessBKGMCUAngleData();
        public bool MCUAngleDataDelete(string xmname, string point_name, DateTime dt,out string mssg)
        {
            var dataDeleteCondition = new DataDeleteCondition(xmname,point_name,dt);
            return processBKGMCUAngleData.ProcessMCUAngleDataDelete(dataDeleteCondition, out mssg);
        }
    }
}
