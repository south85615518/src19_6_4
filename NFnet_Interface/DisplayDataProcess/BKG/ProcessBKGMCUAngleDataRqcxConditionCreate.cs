﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
   
       public class ProcessBKGMCUAngleDataRqcxConditionCreate
       {
           public static ProcessBKGMCUAngleData processBKGMCUAngleData = new ProcessBKGMCUAngleData();
           public ResultDataRqcxConditionCreateCondition ResultDataRqcxConditionCreate(string startTime, string endTime, QueryType QT, string unit, string xmname, DateTime maxTime)
           {
               var resultDataRqcxConditionCreateCondition = new ResultDataRqcxConditionCreateCondition(startTime, endTime, QT, unit, xmname, maxTime);
               processBKGMCUAngleData.ProcessResultDataRqcxConditionCreate(resultDataRqcxConditionCreateCondition);
               return resultDataRqcxConditionCreateCondition;
           }
       }
    
}
