﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAnglePointAlarmValueEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueEdit(MDBDATA.Model.mcuanglepointalarm model,out string mssg)
        {
             pointAlarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
