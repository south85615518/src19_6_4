﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAlarmDataCount
    {
        public ProcessBKGMCUData processBKGMCUData = new ProcessBKGMCUData();
        //public string mssg = "";
        public int MCUAlarmDataCount(string xmname,List<string>pointnamelist,out string mssg)
        {
            var getAlarmTableContModel = new ProcessBKGMCUData.GetAlarmTableContModel(xmname, pointnamelist);
            if (processBKGMCUData.GetAlarmTableCont(getAlarmTableContModel, out mssg))
            {
                return getAlarmTableContModel.cont;
            }
            return 0;
        }
    }
}
