﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
   public class ProcessMCUAnglePointNewestDateTimeGet
    {
       public ProcessMCUAngleBLL processMCUBLL = new ProcessMCUAngleBLL();
        //public ProcessInclinometerCom inlcinometerCom = new ProcessInclinometerCom();
        public DateTime MCUAnglePointNewestDateTimeGet(string xmname, string pointname, out string mssg)
        {
            var mdbdataPointNewestDateTimeCondition = new MdbdataPointNewestDateTimeCondition(xmname, pointname);
            if (!processMCUBLL.ProcessPointNewestDateTimeGet(mdbdataPointNewestDateTimeCondition, out mssg))
                return new DateTime();
            return mdbdataPointNewestDateTimeCondition.dt;

        }
    }
}
