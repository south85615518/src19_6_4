﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessBKGMCUPointLoad
    {
        public static ProcessMCUBLL processMCUBLL = new ProcessMCUBLL();
        public List<string> BKGMCUPointLoad(string xmname, out string mssg)
        {
            BKGMcuPointLoadCondition model = new BKGMcuPointLoadCondition(xmname);
            if (processMCUBLL.ProcessBKGMCUPointLoad(model, out mssg))
                return model.pointnamelist;
            return new List<string>();
        }
    }
}
