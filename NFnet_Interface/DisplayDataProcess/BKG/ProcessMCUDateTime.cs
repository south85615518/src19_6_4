﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUDateTime
    {
        public ProcessMCUBLL processMCUBLL = new ProcessMCUBLL();

        public MDBDATA.Model.mcudata MCUDateTime( string xmname,string pointname,DateTime dt,out string mssg)
        {
            var mcuAngleDataTimeCondition = new ProcessMCUBLL.McuDataTimeCondition(xmname, pointname, dt);
            if (!processMCUBLL.ProcessMcuDataTime(mcuAngleDataTimeCondition, out mssg)) return new MDBDATA.Model.mcudata();
            return mcuAngleDataTimeCondition.model;

        }


    }
}
