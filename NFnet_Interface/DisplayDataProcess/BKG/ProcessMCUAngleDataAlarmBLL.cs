﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Tool;
using System.IO;
using InclimeterDAL.Model;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public List<string> pointnamelist { get; set; }
        //public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessMCUAngleDataAlarmBLL()
        {
        }
        public ProcessMCUAngleDataAlarmBLL(string xmname, int xmno, List<string> pointnamelist)
        {
            this.xmname = xmname;
            this.xmno = xmno;
            this.pointnamelist = pointnamelist;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static ProcessPointAlarmBLL processMCUAnglePointAlarmBLL = new ProcessPointAlarmBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";

        public bool main()
        {

            return TestMCUAngleModelList();

        }
        public ProcessMCUAngleAlarmModelListGet processMCUAngleAlarmModelListGet = new ProcessMCUAngleAlarmModelListGet();
        public ProcessMCUAnglePointAlarmModel processMCUAnglePointAlarmModel = new ProcessMCUAnglePointAlarmModel();
        public bool TestMCUAngleModelList()
        {


            List<MDBDATA.Model.mcuangledata> lc = (List<MDBDATA.Model.mcuangledata>)processMCUAngleAlarmModelListGet.MCUAngleAlarmModelListGet(xmname, pointnamelist, out mssg);
            //ProcessPrintMssg.Print(mssg);
            MCUAnglePointAlarm(lc);
            return true;
            //ProcessPrintMssg.Print(mssg);

        }
        public MDBDATA.Model.mcuanglepointalarm TestPointAlarmValue(string pointName)
        {
            return processMCUAnglePointAlarmModel.MCUAnglePointAlarmModel(xmno, pointName, out mssg);
        }



        public static ProcessMCUAngleAlarmValueModel processMCUAngleAlarmValueModel = new ProcessMCUAngleAlarmValueModel();
        public List<MDBDATA.Model.mcuanglealarmvalue> TestAlarmValueList(MDBDATA.Model.mcuanglepointalarm pointalarm)
        {
            List<MDBDATA.Model.mcuanglealarmvalue> alarmvalueList = new List<MDBDATA.Model.mcuanglealarmvalue>();

            //ProcessPrintMssg.Print("一级：" + mssg);
            alarmvalueList.Add(processMCUAngleAlarmValueModel.MCUAngleAlarmValueModel(pointalarm.firstalarm, xmno, out mssg));
            alarmvalueList.Add(processMCUAngleAlarmValueModel.MCUAngleAlarmValueModel(pointalarm.secalarm, xmno, out mssg));
            alarmvalueList.Add(processMCUAngleAlarmValueModel.MCUAngleAlarmValueModel(pointalarm.thirdalarm, xmno, out mssg));
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<MDBDATA.Model.mcuanglealarmvalue> levelalarmvalue, MDBDATA.Model.mcuangledata resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (processMCUAnglePointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                //processMCUAnglePointAlarmBLL.ProcessPointAlarmfilterInformation();
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public bool MCUAnglePointAlarm(List<MDBDATA.Model.mcuangledata> lc)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format(" {0} ", DateTime.Now));
            ls.Add(string.Format(" {0} ", xmname));
            ls.Add(string.Format(" {0} ", "深部位移--固定式测斜仪--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (MDBDATA.Model.mcuangledata cl in lc)
            {
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname))) return false;
                MDBDATA.Model.mcuanglepointalarm pointvalue = TestPointAlarmValue(cl.point_name);
                List<MDBDATA.Model.mcuanglealarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> MCUAngleDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        public void MCUAnglePointAlarm(MDBDATA.Model.mcuangledata data)
        {

            MDBDATA.Model.mcuanglepointalarm pointvalue = TestPointAlarmValue(data.point_name);
            if (pointvalue == null) return;
            List<MDBDATA.Model.mcuanglealarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestPointAlarmfilterInformation(alarmList, data);
        }

        //public Tool.DTUDataTableHelper ProcessPointAlarmValue(string pointname, int xmno)
        //{

        //    MDBDATA.Model.MCUAngle pointvalue = TestPointAlarmValue(pointname);
        //    List<MDBDATA.Model.MCUAnglealarmvalue> alarmList = TestAlarmValueList(pointvalue);
        //    var model = new Tool.MCUAngleReportHelper.MCUAnglealarm();
        //    model.pointname = pointname;
        //    if (alarmList[2] != null)
        //    {
        //        model.third_acdisp = alarmList[2].acdeep;
        //        model.third_thisdisp = alarmList[2].thisdeep;
        //        model.third_deep = alarmList[2].deep;
        //        model.third_rap = alarmList[2].rap;
        //    }
        //    if (alarmList[1] != null)
        //    {

        //        model.sec_acdisp = alarmList[1].acdeep;
        //        model.sec_thisdisp = alarmList[1].thisdeep;
        //        model.sec_deep = alarmList[1].deep;
        //        model.sec_rap = alarmList[1].rap;
        //    }
        //    if (alarmList[0] != null)
        //    {

        //        model.first_acdisp = alarmList[0].acdeep;
        //        model.first_thisdisp = alarmList[0].thisdeep;
        //        model.first_deep = alarmList[0].deep;
        //        model.first_rap = alarmList[0].rap;


        //    }


        //    return model;
        //}


    }
}