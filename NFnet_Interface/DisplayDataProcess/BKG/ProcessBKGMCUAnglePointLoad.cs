﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessBKGMCUAnglePointLoad
    {
        public static ProcessMCUAngleBLL processMCUAngleBLL = new ProcessMCUAngleBLL();
        public List<string> BKGMCUAnglePointLoad(string xmname, out string mssg)
        {
            BKGMcuPointLoadCondition model = new BKGMcuPointLoadCondition(xmname);
            if (processMCUAngleBLL.ProcessBKGMCUAnglePointLoad(model, out mssg))
                return model.pointnamelist;
            return new List<string>();
        }
    }
}
