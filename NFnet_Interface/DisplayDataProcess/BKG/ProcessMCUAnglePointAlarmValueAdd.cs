﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAnglePointAlarmValueAdd
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool PointAlarmValueAdd(MDBDATA.Model.mcuanglepointalarm model,out string mssg)
        {
             return pointAlarmBLL.ProcessAlarmAdd(model,out mssg);
        }
    }
}
