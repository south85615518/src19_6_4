﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleAlarmDataCount
    {
        public ProcessBKGMCUAngleData processBKGMCUAngleData = new ProcessBKGMCUAngleData();
        //public string mssg = "";
        public int MCUAngleAlarmDataCount(string xmname, List<string> pointnamelist, out string mssg)
        {
            var getAlarmTableContModel = new ProcessBKGMCUAngleData.GetAlarmTableContModel(xmname, pointnamelist);
            if (processBKGMCUAngleData.GetAlarmTableCont(getAlarmTableContModel, out mssg))
            {
                return getAlarmTableContModel.cont;
            }
            return 0;
        }
    }
}
