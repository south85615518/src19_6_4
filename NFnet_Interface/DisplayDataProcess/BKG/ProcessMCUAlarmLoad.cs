﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.MCUBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAlarmLoad
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public DataTable AlarmLoad(int  xmno, string colName, int pageIndex, int rows, string sord, out string mssg)
        {
            var processAlarmLoadModel = new ProcessAlarmBLL.ProcessAlarmLoadModel(xmno, colName, pageIndex, rows, sord);


            if (alarmBLL.ProcessAlarmLoad(processAlarmLoadModel, out mssg))
            {
                return processAlarmLoadModel.dt;
            }

            //加载结果数据出错错误信息反馈
            return new DataTable();

        }
    }
}
