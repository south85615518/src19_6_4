﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string MCUPlotLineAlarm(string pointnamestr, string xmname, int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<MCUPlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessMCUDataAlarmBLL processMCUDataAlarmBLL = new ProcessMCUDataAlarmBLL(xmname, xmno, pointnamelist);
                List<MCUPlotlineAlarmModel> lmm = new List<MCUPlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    MDBDATA.Model.mcupointalarmvalue pointvalue = processMCUDataAlarmBLL.TestPointAlarmValue(pointname);
                    List<MDBDATA.Model.mcualarmvalue> alarmList = processMCUDataAlarmBLL.TestAlarmValueList(pointvalue);
                    MCUPlotlineAlarmModel model = new MCUPlotlineAlarmModel
                    {
                        pointname = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("浸润线预警参数加载出错,错误信息:" + ex.Message);
                return null;
            }
        }
    }
}
