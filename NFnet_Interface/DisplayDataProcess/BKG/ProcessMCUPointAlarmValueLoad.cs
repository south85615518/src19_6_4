﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.MCUBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUPointAlarmValueLoad
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public DataTable PointAlarmValueLoad(string xmname,int xmno,string searchstring,string colName,int pageIndex,int rows,string sord,out string mssg)
        {
            var processPointAlarmLoadModel = new ProcessPointAlarmBLL.ProcessAlarmLoadModel(xmname, xmno, searchstring, colName, pageIndex, rows, sord);
            if (pointAlarmBLL.ProcessPointLoad(processPointAlarmLoadModel, out mssg))
            {
                return processPointAlarmLoadModel.dt;
            }
            else
            {
                //加载结果数据出错错误信息反馈
                return new DataTable();
            }
        }
    }
}
