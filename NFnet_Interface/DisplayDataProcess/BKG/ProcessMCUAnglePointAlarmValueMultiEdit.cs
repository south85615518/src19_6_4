﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAnglePointAlarmValueMutilEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueMutilEdit(MDBDATA.Model.mcuanglepointalarm model,string pointstr ,out string mssg)
        {
            var processAlarmMultiUpdateModel = new ProcessPointAlarmBLL.ProcessAlarmMultiUpdateModel(pointstr, model);
            pointAlarmBLL.ProcessAlarmMultiUpdate(processAlarmMultiUpdateModel, out mssg);
        }
    }
}
