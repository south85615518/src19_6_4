﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUBKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUPointAlarmModel
    {
        public ProcessPointAlarmBLL processMCUPointAlarmBLL = new ProcessPointAlarmBLL();
        public MDBDATA.Model.mcupointalarmvalue MCUPointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processMCUPointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processMCUPointAlarmBLL.ProcessPointAlarmModelGet(processMCUPointAlarmModel, out mssg))
            {
                return processMCUPointAlarmModel.model;
            }
            return new MDBDATA.Model.mcupointalarmvalue();
        }
    }
}
