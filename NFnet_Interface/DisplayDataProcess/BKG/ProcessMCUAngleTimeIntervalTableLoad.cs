﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleTimeIntervalTableLoad
    {
        public ProcessMCUAngleBLL processMCUAngleBLL = new ProcessMCUAngleBLL();
        public DataTable MCUAngleTimeIntervalTableLoad(int xmno, string searchstring, int pageIndex, int rows, string sord, out string mssg)
        {
            var mcutimeintervalModel = new ProcessMCUAngleBLL.mcuangletimeintervalModel(xmno,pageIndex,rows,sord);
            if (processMCUAngleBLL.ProcessMCUAngleTimeInterval(mcutimeintervalModel, out mssg))
            {
                return mcutimeintervalModel.dt;
            }
            return new DataTable();
        }
    }
}
