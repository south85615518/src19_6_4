﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleAlarmEdit
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmEdit(MDBDATA.Model.mcuanglealarmvalue model, out string mssg)
        {
            //var processAlarmEditModel = new ProcessAlarmBLL.(model, xmname);
            //return alarmBLL.ProcessAlarmEdit(processAlarmEditModel, out mssg);
            return alarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
