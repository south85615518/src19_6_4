﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleChart
    {
        public ProcessBKGMCUAngleChartBLL bkgmucangle = new ProcessBKGMCUAngleChartBLL();
        public List<serie> SerializestrBKGMCUAngle(object sql, object xmname, object pointname,out string mssg)
        {

            var serializestrBKGMCUAngleModel = new SerializestrSWCondition(sql, xmname, pointname);
           
            if (ProcessBKGMCUAngleChartBLL.ProcessSerializestrBKGAngle(serializestrBKGMCUAngleModel, out mssg))
                return serializestrBKGMCUAngleModel.series;
            return new List<serie>();


        }
    }
}
