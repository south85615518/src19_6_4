﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleAlarmModelListGet
    {
        public ProcessBKGMCUAngleData processMCUAngleDATABLL = new ProcessBKGMCUAngleData();
        public List<MDBDATA.Model.mcuangledata> MCUAngleAlarmModelListGet(string xmname, List<string> pointnamelist, out string mssg)
        {
            var processMCUAngleAlarmModelListGetModel = new ProcessBKGMCUAngleData.ProcessMCUAngleAlarmModelListGetModel(xmname, pointnamelist);
            if (processMCUAngleDATABLL.ProcessMCUAngleAlarmModelListGet(processMCUAngleAlarmModelListGetModel, out mssg))
            {
                return processMCUAngleAlarmModelListGetModel.model;
            }
            return new List<MDBDATA.Model.mcuangledata>();
        }
    }
}