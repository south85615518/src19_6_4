﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
   public class ProcessMCUPointNewestDateTimeGet
    {
       public ProcessMCUBLL processMCUBLL = new ProcessMCUBLL();
       //public ProcessInclinometerCom inlcinometerCom = new ProcessInclinometerCom();
       public DateTime MCUPointNewestDateTimeGet(string xmname,string pointname,out string mssg)
       {
           var mdbdataPointNewestDateTimeCondition = new MdbdataPointNewestDateTimeCondition(xmname, pointname);
           if (!processMCUBLL.ProcessPointNewestDateTimeGet(mdbdataPointNewestDateTimeCondition, out mssg))
               return new DateTime();
           return mdbdataPointNewestDateTimeCondition.dt;

       }
    }
}
