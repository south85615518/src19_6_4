﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessBKGMCUMaxTime
    {
        public ProcessMCUBLL processMCUBLL = new ProcessMCUBLL();
        public DateTime BKGMCUMaxTime(string xmname,out string mssg)
        {
            var model = new BKGMaxTimeCondition(xmname);
            if (processMCUBLL.ProcessBKGMCUMaxTime(model, out mssg))
            {
                return model.maxTime;
            }
            return new DateTime();
        }
    }
}
