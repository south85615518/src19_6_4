﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUDataAcc
    {
        public ProcessBKGMCUData processBKGMCUData = new ProcessBKGMCUData();
        public bool MCUDataAcc(int pid,DateTime time,double r2,out string mssg)
        {
            var model = new ProcessBKGMCUData.ProcessMCUDataAccModel(pid,time,r2);
            return processBKGMCUData.ProcessMCUDataAcc(model,out mssg);
        }
        
    }
}
