﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.MCUAngleBKG;
//using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleAlarmRecordsCount
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public int AlarmRecordsCount(int xmno, out string mssg)
        {
            var processAlarmRecordsCountModel = new ProcessAlarmBLL.ProcessAlarmRecordsCountModel(xmno);



            if (alarmBLL.ProcessAlarmRecordsCount(processAlarmRecordsCountModel, out mssg))
            {
                return Convert.ToInt32(processAlarmRecordsCountModel.totalCont);

            }
            else
            {
                //计算结果数据数量出错信息反馈
                return 0;
            }
        }
    }
}
