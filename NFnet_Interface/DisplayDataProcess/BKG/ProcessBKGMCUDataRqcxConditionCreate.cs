﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.BKG;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
   public class ProcessBKGMCUDataRqcxConditionCreate
    {
       public static ProcessBKGMCUData processBKGMCUData = new ProcessBKGMCUData();
       public ResultDataRqcxConditionCreateCondition ResultDataRqcxConditionCreate(string startTime, string endTime, QueryType QT, string unit, string xmname, DateTime maxTime)
       {
           var resultDataRqcxConditionCreateCondition = new ResultDataRqcxConditionCreateCondition(startTime, endTime, QT, unit, xmname, maxTime);
           processBKGMCUData.ProcessResultDataRqcxConditionCreate(resultDataRqcxConditionCreateCondition);
           return resultDataRqcxConditionCreateCondition;
       }
    }
}
