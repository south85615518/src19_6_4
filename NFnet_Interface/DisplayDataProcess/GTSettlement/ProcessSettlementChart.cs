﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
using NFnet_MODAL;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementChart
    {
        public ProcessGTSettlementChartBLL processSettlementChartBLL = new ProcessGTSettlementChartBLL();
        public List<serie> SerializestrSettlement(object sql, object xmno, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmno,pointname,0);

            if (ProcessGTSettlementChartBLL.ProcessSerializestrGTSettlementData(processChartCondition, out mssg))
                return processChartCondition.series;
            return new List<serie>();


        }
    }
}
