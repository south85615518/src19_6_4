﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTSettlementPointDataDateTimeStampLoad
    {
        public ProcessGTSettlemntDateTimeStampBLL processGTSettlemntDateTimeStampBLL = new ProcessGTSettlemntDateTimeStampBLL();
        public PointDataDateTimeStamp PointDataDateTimeStampLoad(int xmno, string pointname, PointDataDateTimeStamp timestamp, out string mssg)
        {
            var processPointDataDateTimeStampLoadModel = new NFnet_BLL.DisplayDataProcess.DateTimeStamp.ProcessGTSettlemntDateTimeStampBLL.ProcessPointDataDateTimeStampLoadModel(xmno,  pointname, timestamp);
            processGTSettlemntDateTimeStampBLL.ProcessPointDataDateTimeStampLoad(processPointDataDateTimeStampLoadModel, out mssg);
            return processPointDataDateTimeStampLoadModel.timestamp;
        }
    
    }
}
