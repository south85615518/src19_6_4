﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementSurveyTimeDataDelete
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyTimeDataDelete( int xmno, string pointname, DateTime starttime, DateTime endtime,out string mssg)
        {
            var model = new ProcessResultDataBLL.DeleteModel(xmno,pointname,starttime,endtime);
            return processResultDataBLL.DeleteSurveyData(model, out mssg);
        }
    }
}
