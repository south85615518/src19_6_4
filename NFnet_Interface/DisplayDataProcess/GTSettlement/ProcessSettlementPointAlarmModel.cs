﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementPointAlarmModel
    {
        public ProcessPointAlarmBLL processSettlementPointAlarmBLL = new ProcessPointAlarmBLL();
        public global::data.Model.gtsettlementpointalarmvalue SettlementPointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processSettlementPointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processSettlementPointAlarmBLL.ProcessPointAlarmModelGet(processSettlementPointAlarmModel, out mssg))
            {
                return processSettlementPointAlarmModel.model;
            }
            return new global::data.Model.gtsettlementpointalarmvalue();
        }
    }
}
