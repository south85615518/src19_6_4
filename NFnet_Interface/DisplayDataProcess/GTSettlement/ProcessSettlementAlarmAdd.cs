﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
//using NFnet_BLL.DisplayDataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementAlarmAdd
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public bool AlarmAdd(global::data.Model.gtsettlementalarmvalue model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessAlarmValueBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.ProcessgtsettlementalarmvalueAdd(model, out mssg);
        }
    }
}
