﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSurveySettlementDataTableCountLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public string SurveySettlementDataTableCountLoad(int xmno,string pointname ,DateTime starttime,DateTime endtime,out string mssg)
        {
            var gtResultDataCountLoadCondition = new GTResultDataCountLoadCondition(xmno,pointname,starttime,endtime);
            if (processResultDataBLL.ProcessSurveyResultDataRecordsCount(gtResultDataCountLoadCondition, out mssg))
                return gtResultDataCountLoadCondition.totalCont;
            return "";
        }
    }
}
