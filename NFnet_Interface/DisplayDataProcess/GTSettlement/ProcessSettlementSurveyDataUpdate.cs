﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.GTSettlement;


namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementSurveyDataUpdate
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyDataUpdate(int xmno, string pointname, DateTime dt,string vSet_name, string vLink_name, DateTime srcdatatime,out string mssg)
        {
            var model = new ProcessResultDataBLL.UpdataSurveyDataModel(xmno, pointname, dt,  vSet_name, vLink_name, srcdatatime);
            return processResultDataBLL.UpdataSurveyData(model,out mssg);
        }
    }
}
