﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
using NFnet_BLL.DisplayDataProcess;


namespace  NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessGTSettlementBasePointLoad
    {
        public ProcessPointAlarmBLL processSettlement = new ProcessPointAlarmBLL();
        public List<string> SettlementBasePointLoadBLL(int xmno,out string mssg)
        {
            var processSettlementPointLoadModel = new SensorPointLoadCondition(xmno,data.Model.gtsensortype._layeredSettlement);
            if (processSettlement.ProcessSettlementBasePointLoad(processSettlementPointLoadModel, out mssg))
            {
                return processSettlementPointLoadModel.ls;
            }
            return new List<string>();
        }
    }
}
