﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementCgTimeDataDelete
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool CgTimeDataDelete( int xmno, string pointname,DateTime starttime, DateTime endtime,out string mssg)
        {
            var model = new ProcessResultDataBLL.DeleteModel( xmno, pointname,starttime, endtime);
            return processResultDataBLL.DeleteCg(model, out mssg);
        }
    }
}
