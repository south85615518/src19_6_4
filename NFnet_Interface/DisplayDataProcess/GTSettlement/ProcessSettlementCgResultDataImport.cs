﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.GTSettlement;


namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementCgResultDataImport
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool CgResultDataImport(int xmno, string point_name,  DateTime starttime, DateTime endtime, out string mssg)
        {
            var model = new ProcessResultDataBLL.AddSurveyDataModel(xmno, point_name, starttime, endtime);
            return processResultDataBLL.ProcessCgDataImport(model,out mssg);
        }
    }
}
