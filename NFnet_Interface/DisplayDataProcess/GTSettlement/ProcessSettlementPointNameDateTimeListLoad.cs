﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public  class ProcessSettlementPointNameDateTimeListLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<string> GtPointNameDateTimeListLoad(int xmno,string point_name,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessResultDataTimeListLoadModel(xmno,point_name);
            if (processResultDataBLL.ProcessResultDataTimeListLoad(model, out mssg))
                return model.ls;
            return new List<string>();
        }
    }
}
