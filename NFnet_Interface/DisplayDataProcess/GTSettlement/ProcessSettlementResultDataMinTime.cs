﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
   public class ProcessSettlementResultDataMinTime
    {

       public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public  DateTime SettlementResultDataMinTime(int xmno,out string mssg)
        {
            var processSettlementResultDataMaxTime = new NFnet_BLL.DisplayDataProcess.SenorMaxTimeCondition(xmno);
            
            if (!resultDataBLL.ProcessdataResultDataMaxTime(processSettlementResultDataMaxTime, out mssg)) return new DateTime();
            return processSettlementResultDataMaxTime.dt;
            

        }
    }
}
