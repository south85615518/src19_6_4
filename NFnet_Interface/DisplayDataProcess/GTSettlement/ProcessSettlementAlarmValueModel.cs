﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
//using NFnet_BLL.DisplayDataProcess.GTSettlementBKG;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementAlarmValueModel
    {
        public ProcessAlarmValueBLL processSettlementAlarmValueBLL = new ProcessAlarmValueBLL();
        public global::data.Model.gtsettlementalarmvalue SettlementAlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var processSettlementAlarmValueModelGetModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (processSettlementAlarmValueBLL.ProcessAlarmModelGetByName(processSettlementAlarmValueModelGetModel, out mssg))
            {
                return processSettlementAlarmValueModelGetModel.model;
            }
            return new global::data.Model.gtsettlementalarmvalue();
        }
    }
}
