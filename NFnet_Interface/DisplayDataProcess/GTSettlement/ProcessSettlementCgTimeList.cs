﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.GTSettlement;


namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementCgTimeList
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public List<string> CgTimeList(int xmno, string point_name,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessResultDataTimeListLoadModel(xmno,point_name);
            if (processResultDataBLL.ProcessCgResultDataTimeListLoad(model, out mssg))
                return model.ls;
            return null;
        }
    }
}
