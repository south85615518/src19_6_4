﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess.GTSettlement;


namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementSurveyPointTimeDataAdd
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool SurveyPointTimeDataAdd(int xmno, string pointname,DateTime dt, DateTime importdt,out string mssg)
        {
            var model = new ProcessResultDataBLL.ProcessSurveyPointTimeDataAddModel(xmno, pointname, dt, importdt);
            return processResultDataBLL.ProcessSurveyPointTimeDataAdd(model,out mssg);
        }
    }
}
