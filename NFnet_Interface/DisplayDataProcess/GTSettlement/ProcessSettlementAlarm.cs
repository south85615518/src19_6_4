﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;


namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementAlarm
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool Alarm(global::data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            return pointAlarmBLL.ProcessAlarm(model,out mssg); 
        }
    }
}
