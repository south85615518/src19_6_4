﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementResultDataPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string SettlementResultDataPlotLineAlarm(string pointnamestr,int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<SettlementPlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessResultDataAlarmBLL processSettlementResultDataAlarmBLL = new ProcessResultDataAlarmBLL(xmno);
                List<GTSettlementPlotlineAlarmModel> lmm = new List<GTSettlementPlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    var pointvalue = processSettlementResultDataAlarmBLL.TestPointAlarmValue(pointname);
                    var alarmList = processSettlementResultDataAlarmBLL.TestAlarmValueList(pointvalue);
                    GTSettlementPlotlineAlarmModel model = new GTSettlementPlotlineAlarmModel
                    {
                         pointname = pointname,
                         firstalarm = alarmList[0],
                         secalarm = alarmList[1],
                         thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                    break;
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("沉降预警参数加载出错,错误信息:"+ex.Message);
                return null;
            }
        }
    }
}
