﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessgtSettlementBasePointBridgeGet
    {
        public NFnet_BLL.DisplayDataProcess.GTSettlement.ProcessPointAlarmBLL processPointAlarmBLL = new NFnet_BLL.DisplayDataProcess.GTSettlement.ProcessPointAlarmBLL();
        public List<List<global::data.Model.gtsettlementpointalarmvalue>> gtSettlementBasePointBridgeGet(int xmno,string rootpointname,out string mssg)
        {
            return processPointAlarmBLL.gtSettlementBasePointBridgeGet(xmno,rootpointname,out mssg);
        }
    }
}
