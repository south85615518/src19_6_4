﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementPointAlarmValueMutilEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueMutilEdit(global::data.Model.gtsettlementpointalarmvalue model,string pointstr ,out string mssg)
        {
            var processAlarmMultiUpdateModel = new ProcessPointAlarmBLL.ProcessAlarmMultiUpdateModel(pointstr, model);
            pointAlarmBLL.ProcessAlarmMultiUpdate(processAlarmMultiUpdateModel, out mssg);
        }
    }
}
