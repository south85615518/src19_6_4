﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL;
using NFnet_BLL.DataProcess.GTSettlement;


namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementCgResultDataTableLoad
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public DataTable CgScalarResultDataTableLoad(int xmno, string pointname, int pageIndex, int rows, string sord, DateTime starttime, DateTime endtime, out  string mssg)
        {
            var model = new GTResultDataLoadCondition(xmno, pointname, pageIndex, rows, sord, starttime, endtime);
            if (processResultDataBLL.CgResultdataTableLoad(model, out mssg))
                return model.dt;
            return new DataTable();
        }
    }
}
