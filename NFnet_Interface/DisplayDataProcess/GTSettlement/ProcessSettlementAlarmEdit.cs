﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
//using NFnet_BLL.DisplayDataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementAlarmEdit
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public bool AlarmEdit(global::data.Model.gtsettlementalarmvalue model, out string mssg)
        {
            //var processAlarmEditModel = new ProcessAlarmValueBLL.(model, xmname);
            //return alarmBLL.ProcessAlarmEdit(processAlarmEditModel, out mssg);
            return alarmBLL.ProcessgtsettlementalarmvalueUpdate(model,out mssg);
        }
    }
}
