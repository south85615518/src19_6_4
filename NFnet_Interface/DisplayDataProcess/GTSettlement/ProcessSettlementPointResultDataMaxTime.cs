﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
using NFnet_BLL.DataProcess.GTSettlement;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
   public class ProcessSettlementPointResultDataMaxTime
    {

       public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public  DateTime SettlementPointResultDataMaxTime(int xmno,string pointname,out string mssg)
        {
            var gTSensorMaxTimeCondition = new GTSettlementSensorTimeCondition(xmno, pointname);
            if (resultDataBLL.ProcesspointdataResultDataMaxTime(gTSensorMaxTimeCondition, out mssg))
            {
                return gTSensorMaxTimeCondition.dt;
            }
            return gTSensorMaxTimeCondition.dt;
            

        }
    }
}
