﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.DateTimeStamp;

namespace NFnet_Interface.DisplayDataProcess.GT
{
    public class ProcessGTSettlementDataLackDate
    {
        public ProcessGTSettlemntDateTimeStampBLL processDateTimeStampBLL = new ProcessGTSettlemntDateTimeStampBLL();
        public void Datalackdate(int xmno, string point_name, PointDataDateTimeStamp timestamp, out string mssg)
        {
            var processDatalackdateModel = new NFnet_BLL.DisplayDataProcess.DateTimeStamp.ProcessGTSettlemntDateTimeStampBLL.ProcessDatalackdateModel(xmno, point_name, timestamp);
            processDateTimeStampBLL.ProcessDatalackdate(processDatalackdateModel,out mssg);
        }
    }
}
