﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
//using NFnet_BLL.DisplayDataProcess.GTSettlement;

namespace NFnet_Interface.DisplayDataProcess.GTSettlement
{
    public class ProcessSettlementPointAlarmValueAdd
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool PointAlarmValueAdd(global::data.Model.gtsettlementpointalarmvalue model,out string mssg)
        {
             return pointAlarmBLL.ProcessAlarmAdd(model,out mssg);
        }
    }
}
