﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DataProcess.GTSettlement;


namespace NFnet_BLL_INTERFACE.DisplayDataProcess.GTSettlement
{
    public class ProcessPointResultDataTimeModel
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public data.Model.gtsettlementresultdata PointResultDataTimeModelGet(int xmno,string pointname,DateTime dt,out string mssg)
        {
            var processGetInitTimeModelGetModel = new NFnet_BLL.DataProcess.GTSettlement.ProcessResultDataBLL.ProcessResultDataLastTimeModelGetModel(pointname, xmno,dt);
            if (processResultDataBLL.ProcessResultDataLastTimeModelGet(processGetInitTimeModelGetModel, out mssg))
            {
                return processGetInitTimeModelGetModel.model;
            }
            return new data.Model.gtsettlementresultdata();
        }
    }
}