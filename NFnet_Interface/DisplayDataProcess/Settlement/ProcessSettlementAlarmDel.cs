﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Settlement;
//using NFnet_BLL.DisplayDataProcess.Settlement;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementAlarmDel
    {
        public ProcessSettlementAlarmValueBLL alarmBLL = new ProcessSettlementAlarmValueBLL();
        public bool AlarmDel(int xmno, string name,out string mssg)
        {
            var processAlarmDelModel = new ProcessSettlementAlarmValueBLL.ProcessAlarmDelModel( xmno,name);
            return alarmBLL.ProcessAlarmDel(xmno,name, out mssg);
        }
    }
}
