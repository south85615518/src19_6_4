﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Settlement;
//using NFnet_BLL.DisplayDataProcess.SettlementBKG;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementAlarmValueModel
    {
        public ProcessSettlementAlarmValueBLL processSettlementAlarmValueBLL = new ProcessSettlementAlarmValueBLL();
        public global::Settlement.Model.settlementalarmvalue SettlementAlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var processSettlementAlarmValueModelGetModel = new ProcessSettlementAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (processSettlementAlarmValueBLL.ProcessAlarmModelGetByName(processSettlementAlarmValueModelGetModel, out mssg))
            {
                return processSettlementAlarmValueModelGetModel.model;
            }
            return new global::Settlement.Model.settlementalarmvalue();
        }
    }
}
