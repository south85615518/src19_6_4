﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Settlement;
//using NFnet_BLL.DisplayDataProcess.Settlement;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementAlarmAdd
    {
        public ProcessSettlementAlarmValueBLL alarmBLL = new ProcessSettlementAlarmValueBLL();
        public bool AlarmAdd(global::Settlement.Model.settlementalarmvalue model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessSettlementAlarmValueBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.ProcessAlarmAdd(model, out mssg);
        }
    }
}
