﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.Settlement;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.Settlement
{
   public class ProcessSettlementResultDataMaxTime
    {

        public ProcessSettlementResultDataBLL resultDataBLL = new ProcessSettlementResultDataBLL();
        public  DateTime SettlementResultDataMaxTime(int xmno,out string mssg)
        {
            var processSettlementResultDataMaxTime = new SenorMaxTimeCondition(xmno);
            
            if (!resultDataBLL.ProcessSettlementResultDataMaxTime(processSettlementResultDataMaxTime, out mssg)) return new DateTime();
            return processSettlementResultDataMaxTime.dt;
            

        }
    }
}
