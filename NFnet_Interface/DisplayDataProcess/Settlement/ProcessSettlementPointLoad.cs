﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Settlement;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementPointLoad
    {
        public ProcessSettlementResultDataBLL processSettlementBLL = new ProcessSettlementResultDataBLL();
        public List<string> SettlementPointLoad(int xmno,out string mssg)
        {
            var processSettlementPointLoadModel = new SensorPointLoadCondition(xmno);
            if (processSettlementBLL.ProcessSettlementPointLoad(processSettlementPointLoadModel, out mssg))
            {
                return processSettlementPointLoadModel.ls;
            }
            return new List<string>();
        }
    }
}
