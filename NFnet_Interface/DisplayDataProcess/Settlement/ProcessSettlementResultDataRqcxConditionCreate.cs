﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.Settlement
{
    public class ProcessSettlementResultDataRqcxConditionCreate
    {
        public ProcessSettlementResultDataBLL resultDataBLL = new ProcessSettlementResultDataBLL();
        public ResultDataRqcxConditionCreateCondition SettlementResultDataRqcxConditionCreate( string startTime,string endTime,QueryType QT,string range ,int xmno, DateTime maxTime)
        {
            List<string> ls = new List<string>();

            var processSettlementResultDataRqcxConditionCreateModel = new ResultDataRqcxConditionCreateCondition(startTime,endTime,QT, range, xmno, maxTime);
            resultDataBLL.ProcessResultDataRqcxConditionCreate(processSettlementResultDataRqcxConditionCreateModel);
            return processSettlementResultDataRqcxConditionCreateModel;
        }
        
        
    }
}
