﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Settlement;
using NFnet_MODAL;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementChart
    {
        public ProcessSettlementChartBLL processSettlementChartBLL = new ProcessSettlementChartBLL();
        public List<serie> SerializestrSettlement(object sql, object xmno, object pointname,out string mssg)
        {

            var processChartCondition = new ProcessChartCondition(sql, xmno,pointname,0);

            if (ProcessSettlementChartBLL.ProcessSerializestrSettlement(processChartCondition, out mssg))
                return processChartCondition.series;
            return new List<serie>();


        }
    }
}
