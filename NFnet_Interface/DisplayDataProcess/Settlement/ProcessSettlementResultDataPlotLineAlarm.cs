﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.Settlement;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.Settlement
{
    public class ProcessSettlementResultDataPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string SettlementResultDataPlotLineAlarm(string pointnamestr, string xmname,int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<SettlementPlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessSettlementResultDataAlarmBLL processSettlementResultDataAlarmBLL = new ProcessSettlementResultDataAlarmBLL(xmname, xmno);
                List<SettlementPlotlineAlarmModel> lmm = new List<SettlementPlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    var pointvalue = processSettlementResultDataAlarmBLL.GetPointAlarmValue(pointname);
                    var alarmList = processSettlementResultDataAlarmBLL.GetAlarmValueList(pointvalue);
                    SettlementPlotlineAlarmModel model = new SettlementPlotlineAlarmModel
                    {
                        pointname = pointname,
                         firstalarm = alarmList[0],
                         secalarm = alarmList[1],
                         thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                    break;
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移预警参数加载出错,错误信息:"+ex.Message);
                return null;
            }
        }
    }
}
