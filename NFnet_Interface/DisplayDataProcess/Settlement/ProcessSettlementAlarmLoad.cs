﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Settlement;
//using NFnet_BLL.DisplayDataProcess.Settlement;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementAlarmLoad
    {
        public ProcessSettlementAlarmValueBLL alarmBLL = new ProcessSettlementAlarmValueBLL();
        public DataTable AlarmLoad(int  xmno, string colName, int pageIndex, int rows, string sord, out string mssg)
        {
            var processAlarmLoadModel = new ProcessSettlementAlarmValueBLL.ProcessAlarmLoadModel(xmno, colName, pageIndex, rows, sord);


            if (alarmBLL.ProcessAlarmLoad(processAlarmLoadModel, out mssg))
            {
                return processAlarmLoadModel.dt;
            }

            //加载结果数据出错错误信息反馈
            return new DataTable();

        }
    }
}
