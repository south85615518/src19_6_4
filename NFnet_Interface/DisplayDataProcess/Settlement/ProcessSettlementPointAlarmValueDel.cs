﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Settlement;
//using NFnet_BLL.DisplayDataProcess.Settlement;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementPointAlarmValueDel
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueDel( global::Settlement.Model.settlementpointalarmvalue model ,out string mssg)
        {
            
            pointAlarmBLL.ProcessAlarmDel(model, out mssg);
        }
    }
}
