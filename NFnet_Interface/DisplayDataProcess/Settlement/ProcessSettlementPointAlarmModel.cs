﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Settlement;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementPointAlarmModel
    {
        public ProcessPointAlarmBLL processSettlementPointAlarmBLL = new ProcessPointAlarmBLL();
        public global::Settlement.Model.settlementpointalarmvalue SettlementPointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processSettlementPointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processSettlementPointAlarmBLL.ProcessPointAlarmModelGet(processSettlementPointAlarmModel, out mssg))
            {
                return processSettlementPointAlarmModel.model;
            }
            return new global::Settlement.Model.settlementpointalarmvalue();
        }
    }
}
