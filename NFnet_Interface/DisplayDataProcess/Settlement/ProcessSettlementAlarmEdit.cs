﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Settlement;
//using NFnet_BLL.DisplayDataProcess.Settlement;

namespace NFnet_Interface.DisplayDataProcess.Settlement
{
    public class ProcessSettlementAlarmEdit
    {
        public ProcessSettlementAlarmValueBLL alarmBLL = new ProcessSettlementAlarmValueBLL();
        public bool AlarmEdit(global::Settlement.Model.settlementalarmvalue model, out string mssg)
        {
            //var processAlarmEditModel = new ProcessSettlementAlarmValueBLL.(model, xmname);
            //return alarmBLL.ProcessAlarmEdit(processAlarmEditModel, out mssg);
            return alarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
