﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;


namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessAlarm
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool Alarm(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return pointAlarmBLL.ProcessAlarm(model,out mssg); 
        }
    }
}
