﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessPointAlarmValueEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueEdit(PointAttribute.Model.fmos_pointalarmvalue model,out string mssg)
        {
             pointAlarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
