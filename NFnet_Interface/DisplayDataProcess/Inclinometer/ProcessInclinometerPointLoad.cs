﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    
    public class ProcessInclinometerPointLoad
    {
        public ProcessInclinometerBLL inclinometerBLL = new ProcessInclinometerBLL();
        public List<string> InclinometerPointLoad(int xmno,out string mssg)
        {
            
            var model = new ProcessInclinometerBLL.ProcessInclinometerPointLoadModel(xmno);
            if (inclinometerBLL.ProcessInclinometerPointLoad(model,out mssg))
            {
                return model.ls;
            }
            return new List<string>();
        }
    }
}
