﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessAlarmRecordsCount
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public int AlarmRecordsCount(string xmname, out string mssg)
        {
            var processAlarmRecordsCountModel = new ProcessAlarmBLL.ProcessAlarmRecordsCountModel(xmname);



            if (alarmBLL.ProcessAlarmRecordsCount(processAlarmRecordsCountModel, out mssg))
            {
                return Convert.ToInt32(processAlarmRecordsCountModel.totalCont);

            }
            else
            {
                //计算结果数据数量出错信息反馈
                return 0;
            }
        }
    }
}
