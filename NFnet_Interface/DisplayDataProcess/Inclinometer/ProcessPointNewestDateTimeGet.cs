﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
   public class ProcessPointNewestDateTimeGet
    {
       public ProcessInclinometerCom inlcinometerCom = new ProcessInclinometerCom();
       public DateTime PointNewestDateTimeGet(int xmno,string pointname,Role role,bool tmpRole,out string mssg)
       {
           var inclinometerPointNewestDateTimeCondition = new InclinometerPointNewestDateTimeCondition(xmno,pointname);
           var processPointNewestDateTimeGetModel = new ProcessInclinometerCom.ProcessPointNewestDateTimeGetModel(inclinometerPointNewestDateTimeCondition,role,tmpRole);
           if (!inlcinometerCom.ProcessPointNewestDateTimeGet(processPointNewestDateTimeGetModel, out mssg))
               return new DateTime();
           return processPointNewestDateTimeGetModel.model.dt;

       }
    }
}
