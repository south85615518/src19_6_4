﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessAlarmLoad
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public DataTable AlarmLoad(string xmname, string colName, int pageIndex, int rows, string sord, out string mssg)
        {
            var processAlarmLoadModel = new ProcessAlarmBLL.ProcessAlarmLoadModel(xmname, colName, pageIndex, rows, sord);


            if (alarmBLL.ProcessAlarmLoad(processAlarmLoadModel, out mssg))
            {
                return processAlarmLoadModel.dt;
            }

            //加载结果数据出错错误信息反馈
            return new DataTable();

        }
    }
}
