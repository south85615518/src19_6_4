﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessAlarmEdit
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmEdit(InclimeterDAL.Model.inclinometer_alarmvalue model, out string mssg)
        {
            //var processAlarmEditModel = new ProcessAlarmBLL.(model, xmname);
            //return alarmBLL.ProcessAlarmEdit(processAlarmEditModel, out mssg);
            return alarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
