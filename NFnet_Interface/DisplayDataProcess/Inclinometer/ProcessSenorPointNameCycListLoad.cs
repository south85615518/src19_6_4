﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessSenorPointNameCycListLoad
    {

        public ProcessInclinometerBLL inclinometerBLL = new ProcessInclinometerBLL();
        public ProcessInclinometerCgBLL inclinometerCgBLL = new ProcessInclinometerCgBLL();
        public   List<string>  SenorPointNameCycListLoad(int xmno, string pointname, Role role, bool tmpRole, out string mssg)
        {
           List<string> ls = new List<string>();
            var pointNameCycListCondition = new SenorPointNameCycListCondition(xmno, pointname);
            if (role == Role.superviseModel || tmpRole)
            {

                if (inclinometerCgBLL.ProcessSenorPointNameCycListLoad(pointNameCycListCondition, out mssg))
                {
                    ls = pointNameCycListCondition.ls;
                    return ls;
                }
                return ls;
            }

            if (inclinometerBLL.ProcessSenorPointNameCycListLoad(pointNameCycListCondition, out mssg))
            {
                ls = pointNameCycListCondition.ls;
                return ls;
            }
            return ls;
        }
    }
}
