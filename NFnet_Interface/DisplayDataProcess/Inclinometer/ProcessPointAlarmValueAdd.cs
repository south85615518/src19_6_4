﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessPointAlarmValueAdd
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueAdd(PointAttribute.Model.fmos_pointalarmvalue model,out string mssg)
        {
             pointAlarmBLL.ProcessAlarmAdd(model,out mssg);
        }
    }
}
