﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;



namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessPointAlarmExist
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool PointAlarmExist(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return pointAlarmBLL.ProcessAlarmExist(model,out mssg);
        }
    }
}
