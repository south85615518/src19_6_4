﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessAlarmAdd
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmAdd(InclimeterDAL.Model.inclinometer_alarmvalue model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessAlarmBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.ProcessAlarmAdd(model, out mssg);
        }
    }
}
