﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessSenorDataLoad
    {
        public ProcessInclinometerCom inclinometerCom = new ProcessInclinometerCom();
        public DataTable SenorDataLoad(int xmno, string pointname, DateTime starttime, DateTime endtime,  int startPageIndex, int pageSize, string sord ,Role role ,bool tmpRole,out string mssg)
        {
            var senorDataLoadCondition = new SenorDataLoadCondition(xmno, pointname, starttime, endtime, startPageIndex, pageSize, sord);
            var senordataTableLoadModel = new ProcessInclinometerCom.SenordataTableLoadModel(senorDataLoadCondition,tmpRole,role);
            if (inclinometerCom.SenordataTableLoad(senordataTableLoadModel, out mssg))
            {
                return senordataTableLoadModel.model.dt;
            }
            return new DataTable();
        }
    }
}
