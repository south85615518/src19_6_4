﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessSerializestrSBWY
    {
        public ProcessInclinometerChartCom inclinometerChartCom = new ProcessInclinometerChartCom();
        public List<seriesarrow> SerializestrSBWY(object sql,object xmname,object pointname,object zus,Role role,bool tmpRole,out string mssg)
        {

            var serializestrSBWYModel = new SerializestrSBWYCondition(sql, xmname, pointname,zus);
            var processSerializestrSBWYModel = new ProcessInclinometerChartCom.ProcessSerializestrSBWYModel(serializestrSBWYModel, role, tmpRole);
            if (inclinometerChartCom.ProcessSerializestrSBWY(processSerializestrSBWYModel, out mssg))
                return processSerializestrSBWYModel.model.sr;
            return new List<seriesarrow>();

            
        }
    }
}
