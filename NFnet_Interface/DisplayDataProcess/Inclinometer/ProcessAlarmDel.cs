﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessAlarmDel
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmDel(string xmname, string name,out string mssg)
        {
            var processAlarmDelModel = new ProcessAlarmBLL.ProcessAlarmDelModel( xmname,name);
            return alarmBLL.ProcessAlarmDel(xmname,name, out mssg);
        }
    }
}
