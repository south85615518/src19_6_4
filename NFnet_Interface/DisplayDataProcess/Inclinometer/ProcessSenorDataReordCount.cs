﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessSenorDataReordCount
    {
        public ProcessInclinometerCom inclinometerCom = new ProcessInclinometerCom();
        public int SenorDataReordCount(int xmno, string pointname, DateTime starttime, DateTime endtime, Role role, bool tmpRole, out string mssg)
        {
            var senorDataCountLoadCondition = new SenorDataCountLoadCondition(xmno, pointname, starttime, endtime);
            var processSenorDataRecordsCountModel = new ProcessInclinometerCom.ProcessSenorDataRecordsCountModel(senorDataCountLoadCondition, role, tmpRole);
            if (inclinometerCom.ProcessSenorDataRecordsCount(processSenorDataRecordsCountModel, out mssg))
            {
                return int.Parse(processSenorDataRecordsCountModel.model.totalCont);
            }
            return 0;



        }
    }
}
