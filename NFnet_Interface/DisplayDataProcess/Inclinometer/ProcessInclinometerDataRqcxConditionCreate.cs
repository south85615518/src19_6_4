﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
   public class ProcessInclinometerDataRqcxConditionCreate
    {
       public ProcessInclinometerCom inclinometerCom = new ProcessInclinometerCom();
       public ResultDataRqcxConditionCreateCondition ResultDataRqcxConditionCreate(string startTime, string endTime, QueryType QT, string unit, string xmname, DateTime maxTime)
       {
           var resultDataRqcxConditionCreateCondition = new ResultDataRqcxConditionCreateCondition(startTime, endTime, QT, unit, xmname, maxTime);
           inclinometerCom.ProcessResultDataRqcxConditionCreate(resultDataRqcxConditionCreateCondition);
           return resultDataRqcxConditionCreateCondition;
       }
    }
}
