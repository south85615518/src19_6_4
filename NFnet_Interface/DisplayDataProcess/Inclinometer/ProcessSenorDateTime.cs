﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessSenorDateTime
    {
        public ProcessInclinometerCom inclinometerCom = new ProcessInclinometerCom();

        public InclimeterDAL.Model.senor_data SenorDateTime( int xmno,string pointname,DateTime dt,Role role ,bool tmpRole,out string mssg)
        {
            var senorDataTimeCondition = new SenorDataTimeCondition(xmno,pointname,dt);
            var processSenorDataTimeModel = new ProcessInclinometerCom.ProcessSenorDataTimeModel(senorDataTimeCondition,role,tmpRole);
            if (!inclinometerCom.ProcessSenorDataTime(processSenorDataTimeModel, out mssg)) return new InclimeterDAL.Model.senor_data();
            return processSenorDataTimeModel.model.model;

        }


    }
}
