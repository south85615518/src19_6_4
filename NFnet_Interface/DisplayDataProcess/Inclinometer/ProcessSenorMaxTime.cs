﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessSenorMaxTime
    {
        public ProcessInclinometerCom inclinometerCom = new ProcessInclinometerCom();
        public DateTime SenorMaxTime(int xmno, Role role, bool tmpRole, out string mssg)
        {
            var senorMaxTimeCondition = new SenorMaxTimeCondition(xmno,data.Model.gtsensortype._inclinometer);
            var processSenorMaxTimeModel = new ProcessInclinometerCom.ProcessSenorMaxTimeModel(senorMaxTimeCondition,role,tmpRole);
            if (inclinometerCom.ProcessSenorMaxTime(processSenorMaxTimeModel, out mssg))
            {
                return processSenorMaxTimeModel.model.dt;
            }
            return Convert.ToDateTime("1970-01-01");
        } 
        
    }
}
