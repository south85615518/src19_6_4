﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
   public class ProcessInclinometerDbFill
    {
       public ProcessInclinometerCom inclinometerCom = new ProcessInclinometerCom();
       public FillInclinometerDbFillCondition InclinometerDbFill(string pointnamestr, string rqConditionStr, int xmno, Role role, bool tmpRole)
       {

           var model = new FillInclinometerDbFillCondition(pointnamestr,rqConditionStr,xmno);

           var ProcessInclinometerDbFillModel = new ProcessInclinometerCom.ProcessInclinometerDbFillModel(model,role,tmpRole);

           if (inclinometerCom.ProcessInclinometerDbFill(ProcessInclinometerDbFillModel))
           {
               return ProcessInclinometerDbFillModel.model;
           }

           return null;
       }
    }
}
