﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
    public class ProcessAlarmValues
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public string AlarmValues(string xmname, string alarmvaluenamestr,out string mssg)
        {
            var alarmvaluename = new ProcessAlarmBLL.ProcessAlarmValueNameModel(xmname, alarmvaluenamestr);
            if (alarmBLL.ProcessAlarmValueName(alarmvaluename, out mssg))
            {
                return alarmvaluename.alarmValueNameStr;
            }
             return mssg;          
        }
    }
}
