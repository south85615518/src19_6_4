﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GAUGE;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessGNSSTimeIntervalTableLoad
    {
        public ProcessGNSSBLL processGNSSBLL = new ProcessGNSSBLL();
        public DataTable GNSSTimeIntervalTableLoad(int xmno, string searchstring, int pageIndex, int rows, string sord, out string mssg)
        {
            var mcutimeintervalModel = new ProcessGNSSBLL.gnsstimeintervalModel(xmno,pageIndex,rows,sord);
            if (processGNSSBLL.ProcessGNSSTimeInterval(mcutimeintervalModel, out mssg))
            {
                return mcutimeintervalModel.dt;
            }
            return new DataTable();
        }
    }
}
