﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;
//using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSPointAlarmValueDel
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueDel( GPS.Model.gnsspointalarmvalue model ,out string mssg)
        {
            
            pointAlarmBLL.ProcessAlarmDel(model, out mssg);
        }
    }
}
