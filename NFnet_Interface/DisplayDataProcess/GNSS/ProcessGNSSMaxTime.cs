﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSMaxTime
    {
        public ProcessGNSSDATABLL processGNSSDATABLL = new ProcessGNSSDATABLL();
        public DateTime GNSSMaxTime(string pointnamestr, out string mssg)
        {
            var processGNSSDATAMaxTimeBLLModel = new ProcessGNSSDATABLL.ProcessGNSSDATAMaxTimeBLLModel(pointnamestr);
            //var processSenorMaxTimeModel = new ProcessDTUSenorCom.ProcessSenorMaxTimeModel(senorMaxTimeCondition, role, tmpRole);
            if (processGNSSDATABLL.ProcessGNSSDATAMaxTimeBLL(processGNSSDATAMaxTimeBLLModel, out mssg))
            {
                return processGNSSDATAMaxTimeBLLModel.dt;
            }
            return Convert.ToDateTime("1970-01-01");
        } 
        
    }
}
