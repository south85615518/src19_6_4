﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;
//using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSPointAlarmValueEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueEdit(GPS.Model.gnsspointalarmvalue model,out string mssg)
        {
             pointAlarmBLL.ProcessAlarmEdit(model,out mssg);
        }
    }
}
