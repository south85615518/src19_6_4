﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
   public class ProcessGNSSPointNewestDateTimeGet
    {
       public ProcessGNSSBLL processGNSSBLL = new ProcessGNSSBLL();
       //public ProcessInclinometerCom inlcinometerCom = new ProcessInclinometerCom();
       public DateTime GNSSPointNewestDateTimeGet(string pointname,out string mssg)
       {
           var inclinometerPointNewestDateTimeCondition = new InclinometerPointNewestDateTimeCondition(0, pointname);
           if (!processGNSSBLL.ProcessPointNewestDateTimeGet(inclinometerPointNewestDateTimeCondition, out mssg))
               return new DateTime();
           return inclinometerPointNewestDateTimeCondition.dt;

       }
    }
}
