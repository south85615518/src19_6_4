﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;


namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSAlarm
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool Alarm(GPS.Model.gnsspointalarmvalue model, out string mssg)
        {
            return pointAlarmBLL.ProcessAlarm(model,out mssg); 
        }
    }
}
