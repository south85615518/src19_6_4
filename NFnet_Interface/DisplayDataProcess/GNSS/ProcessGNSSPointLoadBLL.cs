﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSPointLoadBLL
    {
        public ProcessGNSSBLL processGNSSBLL = new ProcessGNSSBLL();
        public List<string> GNSSPointLoadBLL(int xmno,out string mssg)
        {
            var processGNSSPointLoadModel = new NFnet_BLL.DisplayDataProcess.GNSS.ProcessGNSSBLL.ProcessGNSSPointLoadModel(xmno);
            if (processGNSSBLL.ProcessGNSSPointLoad(processGNSSPointLoadModel, out mssg))
            {
                return processGNSSPointLoadModel.pointlist;
            }
            return new List<string>();
        }
    }
}
