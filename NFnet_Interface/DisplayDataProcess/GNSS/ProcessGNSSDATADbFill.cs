﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
   public class ProcessGNSSDATADbFill
    {
       public ProcessGNSSDATABLL processGNSSDATABLL = new ProcessGNSSDATABLL();
       public FillInclinometerDbFillCondition GNSSDbFill(string pointnamestr, string rqConditionStr,  int xmno)
       {
           //sql = "";
           var model = new FillInclinometerDbFillCondition(pointnamestr,rqConditionStr,xmno);


           if (processGNSSDATABLL.ProcessGNSSDATADbFill(model))
           {
               
               return model;
           }

           return null;
       }
    }
}
