﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSPointAlarmModel
    {
        public ProcessPointAlarmBLL processGNSSPointAlarmBLL = new ProcessPointAlarmBLL();
        public GPS.Model.gnsspointalarmvalue GNSSPointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processGNSSPointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processGNSSPointAlarmBLL.ProcessPointAlarmModelGet(processGNSSPointAlarmModel, out mssg))
            {
                return processGNSSPointAlarmModel.model;
            }
            return new GPS.Model.gnsspointalarmvalue();
        }
    }
}
