﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;
//using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSAlarmDel
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmDel(int xmno, string name,out string mssg)
        {
            var processAlarmDelModel = new ProcessAlarmBLL.ProcessAlarmDelModel( xmno,name);
            return alarmBLL.ProcessAlarmDel(xmno,name, out mssg);
        }
    }
}
