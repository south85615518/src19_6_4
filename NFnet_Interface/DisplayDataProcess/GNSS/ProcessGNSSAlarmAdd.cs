﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;
//using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSAlarmAdd
    {
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public bool AlarmAdd(GPS.Model.gnssalarmvalue model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessAlarmBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.ProcessAlarmAdd(model, out mssg);
        }
    }
}
