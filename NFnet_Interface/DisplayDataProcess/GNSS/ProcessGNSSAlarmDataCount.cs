﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSAlarmDataCount
    {
        public ProcessGNSSDATABLL processGNSSData = new ProcessGNSSDATABLL();
        //public string mssg = "";
        public int GNSSAlarmDataCount(List<string>pointnamelist,out string mssg)
        {
            var getAlarmTableContModel = new ProcessGNSSDATABLL.GetAlarmTableContModel(pointnamelist);
            if (processGNSSData.GetAlarmTableCont(getAlarmTableContModel, out mssg))
            {
                return getAlarmTableContModel.cont;
            }
            return 0;
        }
    }
}
