﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;
//using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSPointAlarmValueAdd
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool PointAlarmValueAdd(GPS.Model.gnsspointalarmvalue model,out string mssg)
        {
             return pointAlarmBLL.ProcessAlarmAdd(model,out mssg);
        }
    }
}
