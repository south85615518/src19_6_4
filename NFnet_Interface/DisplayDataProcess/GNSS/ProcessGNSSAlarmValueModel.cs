﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;
//using NFnet_BLL.DisplayDataProcess.GNSSBKG;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSAlarmValueModel
    {
        public ProcessAlarmBLL processGNSSAlarmValueBLL = new ProcessAlarmBLL();
        public GPS.Model.gnssalarmvalue GNSSAlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var processGNSSAlarmValueModelGetModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (processGNSSAlarmValueBLL.ProcessAlarmModelGetByName(processGNSSAlarmValueModelGetModel, out mssg))
            {
                return processGNSSAlarmValueModelGetModel.model;
            }
            return new GPS.Model.gnssalarmvalue();
        }
    }
}
