﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSDATADelete
    {
        public ProcessGNSSDATABLL processBKGGNSSDATABLL = new ProcessGNSSDATABLL();
        public bool GNSSDATABLLDelete(string xmname, string pointname, DateTime dt,out string mssg)
        {
            var dataDeleteCondition = new DataDeleteCondition(xmname, pointname, dt);
            return processBKGGNSSDATABLL.ProcessGNSSDataDelete(dataDeleteCondition,out mssg);
        }
    }
}
