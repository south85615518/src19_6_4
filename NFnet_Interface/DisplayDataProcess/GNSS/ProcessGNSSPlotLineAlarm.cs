﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string GNSSPlotLineAlarm(string pointnamestr, string xmname, int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<GNSSPlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessGNSSDataAlarmBLL processGNSSDataAlarmBLL = new ProcessGNSSDataAlarmBLL(xmname, xmno, pointnamelist);
                List<GNSSPlotlineAlarmModel> lmm = new List<GNSSPlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    GPS.Model.gnsspointalarmvalue pointvalue = processGNSSDataAlarmBLL.TestPointAlarmValue(pointname);
                    List<GPS.Model.gnssalarmvalue> alarmList = processGNSSDataAlarmBLL.TestAlarmValueList(pointvalue);
                    GNSSPlotlineAlarmModel model = new GNSSPlotlineAlarmModel
                    {
                        pointname = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("GNSS预警参数加载出错,错误信息:" + ex.Message);
                return null;
            }
        }
    }
}
