﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSDateTime
    {
        public ProcessGNSSBLL processGNSSBLL = new ProcessGNSSBLL();

        public GPS.Model.gnssdatareport GNSSDateTime(string pointname,DateTime dt,out string mssg)
        {
            var GNSSDataTimeCondition = new ProcessGNSSBLL.ProcessSenorDataTimeModel(pointname, dt);
            if (!processGNSSBLL.ProcessSenorDataTime(GNSSDataTimeCondition, out mssg)) return new GPS.Model.gnssdatareport();
            return GNSSDataTimeCondition.model;

        }


    }
}
