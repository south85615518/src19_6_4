﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.BKG;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.BKG
{
    public class ProcessGNSSChart
    {
        public ProcessGNSSChartBLL processGNSSChartBLL = new ProcessGNSSChartBLL();
        public List<serie> SerializestrGNSS(object sql, object xmname, object pointname,object zus,out string mssg)
        {

            var serializestrBMWYCondition = new SerializestrBMWYCondition(sql, xmname, pointname,zus);

            if (ProcessGNSSChartBLL.ProcessSerializestrGNSS(serializestrBMWYCondition, out mssg))
                return serializestrBMWYCondition.series;
            return new List<serie>();


        }
    }
}
