﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Tool;
using System.IO;
using InclimeterDAL.Model;
using NFnet_Interface.DisplayDataProcess.BKG;
using NFnet_BLL.DisplayDataProcess.GNSS;

namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public List<string> pointnamelist { get; set; }
        //public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessGNSSDataAlarmBLL()
        {
        }
        public ProcessGNSSDataAlarmBLL(string xmname, int xmno, List<string> pointnamelist)
        {
            this.xmname = xmname;
            this.xmno = xmno;
            this.pointnamelist = pointnamelist;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static ProcessPointAlarmBLL processGNSSPointAlarmBLL = new ProcessPointAlarmBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";

        public bool main()
        {

            return TestGNSSModelList();

        }
        public ProcessGNSSAlarmModelListGet processGNSSAlarmModelListGet = new ProcessGNSSAlarmModelListGet();
        public ProcessGNSSPointAlarmModel processGNSSPointAlarmModel = new ProcessGNSSPointAlarmModel();
        public bool TestGNSSModelList()
        {


            List<GPS.Model.gnssdatareport> lc = (List<GPS.Model.gnssdatareport>)processGNSSAlarmModelListGet.GNSSAlarmModelListGet(xmno,pointnamelist,out mssg);
            //ProcessPrintMssg.Print(mssg);
            GNSSPointAlarm(lc);
            return true;
            //ProcessPrintMssg.Print(mssg);

        }
        public GPS.Model.gnsspointalarmvalue TestPointAlarmValue(string pointName)
        {
            return processGNSSPointAlarmModel.GNSSPointAlarmModel(xmno, pointName, out mssg);
        }



        public static ProcessGNSSAlarmValueModel processGNSSAlarmValueModel = new ProcessGNSSAlarmValueModel();
        public List<GPS.Model.gnssalarmvalue> TestAlarmValueList(GPS.Model.gnsspointalarmvalue pointalarm)
        {
            List<GPS.Model.gnssalarmvalue> alarmvalueList = new List<GPS.Model.gnssalarmvalue>();

            //ProcessPrintMssg.Print("一级：" + mssg);
            alarmvalueList.Add(processGNSSAlarmValueModel.GNSSAlarmValueModel(pointalarm.firstalarm, xmno, out mssg));
            alarmvalueList.Add(processGNSSAlarmValueModel.GNSSAlarmValueModel(pointalarm.secalarm, xmno, out mssg));
            alarmvalueList.Add(processGNSSAlarmValueModel.GNSSAlarmValueModel(pointalarm.thirdalarm, xmno, out mssg));
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<GPS.Model.gnssalarmvalue> levelalarmvalue, GPS.Model.gnssdatareport resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (processGNSSPointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                //processGNSSPointAlarmBLL.ProcessPointAlarmfilterInformation();
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        //public ProcessGNSSIDBLL processGNSSIDBLL = new ProcessGNSSIDBLL();
        //public ProcessGNSSIDPointName processGNSSIDPointName = new ProcessGNSSIDPointName();
        public bool GNSSPointAlarm(List<GPS.Model.gnssdatareport> lc)
        {
            alarmInfoList = new List<string>();
            string pointname = "";
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format(" {0} ", DateTime.Now));
            ls.Add(string.Format(" {0} ", xmname));
            ls.Add(string.Format(" {0} ", "表面位移--GPS--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (GPS.Model.gnssdatareport cl in lc)
            {
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname))) return false;
                
                GPS.Model.gnsspointalarmvalue pointvalue = TestPointAlarmValue(cl.point_name);
                List<GPS.Model.gnssalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> GNSSDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        public void GNSSPointAlarm(GPS.Model.gnssdatareport data)
        {

            GPS.Model.gnsspointalarmvalue pointvalue = TestPointAlarmValue(data.point_name);
            if (pointvalue == null) return;
            List<GPS.Model.gnssalarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestPointAlarmfilterInformation(alarmList, data);
        }

        //public Tool.DTUDataTableHelper ProcessPointAlarmValue(string pointname, int xmno)
        //{

        //    MDBDATA.Model.GNSS pointvalue = TestPointAlarmValue(pointname);
        //    List<MDBDATA.Model.GNSSalarmvalue> alarmList = TestAlarmValueList(pointvalue);
        //    var model = new Tool.GNSSReportHelper.GNSSalarm();
        //    model.pointname = pointname;
        //    if (alarmList[2] != null)
        //    {
        //        model.third_acdisp = alarmList[2].acdeep;
        //        model.third_thisdisp = alarmList[2].thisdeep;
        //        model.third_deep = alarmList[2].deep;
        //        model.third_rap = alarmList[2].rap;
        //    }
        //    if (alarmList[1] != null)
        //    {

        //        model.sec_acdisp = alarmList[1].acdeep;
        //        model.sec_thisdisp = alarmList[1].thisdeep;
        //        model.sec_deep = alarmList[1].deep;
        //        model.sec_rap = alarmList[1].rap;
        //    }
        //    if (alarmList[0] != null)
        //    {

        //        model.first_acdisp = alarmList[0].acdeep;
        //        model.first_thisdisp = alarmList[0].thisdeep;
        //        model.first_deep = alarmList[0].deep;
        //        model.first_rap = alarmList[0].rap;


        //    }


        //    return model;
        //}


    }
}