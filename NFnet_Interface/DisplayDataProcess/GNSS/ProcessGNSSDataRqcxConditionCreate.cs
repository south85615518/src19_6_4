﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.Inclinometer
{
   public class ProcessGNSSDataRqcxConditionCreate
    {
       public ProcessGNSSCom gNSSCom = new ProcessGNSSCom();
       public ResultDataRqcxConditionCreateCondition ResultDataRqcxConditionCreate(string startTime, string endTime, QueryType QT, string unit, string xmname, DateTime maxTime)
       {
           var resultDataRqcxConditionCreateCondition = new ResultDataRqcxConditionCreateCondition(startTime, endTime, QT, unit, xmname, maxTime);
           gNSSCom.ProcessResultDataRqcxConditionCreate(resultDataRqcxConditionCreateCondition);
           return resultDataRqcxConditionCreateCondition;
       }
    }
}
