﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.GNSS;


namespace NFnet_Interface.DisplayDataProcess.GNSS
{
    public class ProcessGNSSAlarmModelListGet
    {
        public ProcessGNSSDATABLL processGNSSDATABLL = new ProcessGNSSDATABLL();
        public List<GPS.Model.gnssdatareport> GNSSAlarmModelListGet(int xmno, List<string> pointnamelist, out string mssg)
        {
            var processGNSSAlarmModelListGetModel = new ProcessGNSSDATABLL.ProcessGNSSAlarmModelListGetModel(xmno,pointnamelist);
            if (processGNSSDATABLL.ProcessGNSSAlarmModelListGet(processGNSSAlarmModelListGetModel, out mssg))
            {
                return processGNSSAlarmModelListGetModel.model;
            }
            return new List<GPS.Model.gnssdatareport>();
        }
    }
}