﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement;
using Tool;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_Interface.DisplayDataProcess.pub
{
    public class ProcessAspectXmDataImport
    {
        //public static ProcessFileBLL processFileBLL = new ProcessFileBLL();
        //public static ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public static ProcessTotalStationBLL processTotalStationBLL = new ProcessTotalStationBLL();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static void AspectXmDataImport()
        {
            ExceptionLog.XmRecordWrite("服务器重启检查未完成的解析任务...", "服务器重启未完任务检查");
            List<string> filenamelist = Tool.FileHelper.DirTravel(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表");
            
            string mssg = "";
            int j = 0;
            foreach (string filename in filenamelist)
            {
                List<string> ls = Tool.FileHelper.ProcessFileInfoList(filename);
                if (!ls.Exists(m => m.IndexOf("结果") != -1)) continue;
                int i = 0;
                foreach (var model in ls)
                {
                    
                    NFnet_BLL.DisplayDataProcess.DataFileDescodeTaskModel descodemodel = null;
                    try
                    {
                        ExceptionLog.XmRecordWrite(string.Format("************{0}/{1}/{2}/{3}************", i+1, ls.Count, j+1, filenamelist.Count),"服务器重启未完任务检查");
                        ExceptionLog.XmRecordWrite("现在导入" + model, "服务器重启未完任务检查");
                        descodemodel = jss.Deserialize<NFnet_BLL.DisplayDataProcess.DataFileDescodeTaskModel>(model);
                        ExceptionLog.XmRecordWrite(string.Format("现在向服务器发送项目{0}导入请求...", descodemodel.xmname),"服务器重启未完任务检查");

                        Tool.http.HttpHelper httpHelper = new Tool.http.HttpHelper();

                        string url = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\FileUploadSetting.txt");
                        httpHelper.PostMessageToWeb(string.Format("{0}/数据文件上传接口/ETS/FileUploadAPIDataProcess.ashx?xmname={1}&&path={2}&&fileType={3}", url, descodemodel.xmname, "", "结果数据"));
                        ExceptionLog.XmRecordWrite(string.Format("向服务器发送项目{0}导入请求成功", descodemodel.xmname), "服务器重启未完任务检查");
                        break;

                    }
                    catch (Exception ex)
                    {
                        ExceptionLog.XmRecordWrite(string.Format("向服务器发送项目{0}导入请求出错,错误信息:"+ex.Message, descodemodel.xmname), "服务器重启未完任务检查");

                    }
                    finally
                    {
                        i++;
                    }
                }
                j++;
            }
            ExceptionLog.ExceptionWrite("本次解析任务检查完成...");

        }
    }
}
