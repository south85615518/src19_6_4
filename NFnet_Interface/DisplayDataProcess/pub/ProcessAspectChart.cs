﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_Interface.AuthorityAlarmProcess;
using NFnet_MODAL;
using System.Data;
using Tool;
using NFnet_BLL.DisplayDataProcess.pub;


namespace NFnet_Interface.DisplayDataProcess.pub
{
    public class ProcessAspectChart
    {
        public string mssg = "";
        public AspectSeries AspectChart(int xmno, out string mssg)
        {
            
            ProcessAlarmPointLoad processAlarmPointLoad = new ProcessAlarmPointLoad();
            DataTable dt = processAlarmPointLoad.AlarmPointLoad(xmno, out mssg);
            List<string> datatypeList = Tool.DataTableHelper.ProcessDataTableFilter(dt, "type");
            AspectSeries aspectSeries = new AspectSeries { seriesarrowList = new List<seriesarrow>() };
            foreach (DataRowView dr in new DataView(dt))
            {
                ExceptionLog.ExceptionWrite("点名" + dr["point_name"] + "类型" + dr["type"]);
            }
            try
            {
                foreach (var datatype in datatypeList)
                {
                    //水平（竖直）位移
                    if (datatype == "表面位移")
                    {
                        //continue;
                        try
                        {
                            aspectSeries.seriesarrowList.Add(new seriesarrow { arrowname = "水平位移", lscyc = ProcessSerializestrBMWY(xmno, dt) });
                        }
                        catch (Exception ex)
                        {
                            ExceptionLog.ExceptionWrite(datatype + "曲线绘制出错,错误信息：" + ex.Message);
                        }

                    }
                    //静力水准-压力式
                    else if (datatype == "沉降")
                    {
                        //continue;
                        try
                        {
                            aspectSeries.seriesarrowList.Add(new seriesarrow { arrowname = "静力水准-压力式", lsseries = ProcessSerializestrGTSettlement(xmno, dt) });
                        }
                        catch (Exception ex)
                        {
                            ExceptionLog.ExceptionWrite(datatype + "曲线绘制出错,错误信息：" + ex.Message+" 位置:"+ex.StackTrace);
                        }
                    }
                    //混凝土支撑内力
                    else if (datatype == data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce))
                    {
                        //continue;
                        try
                        {
                            aspectSeries.seriesarrowList.Add(new seriesarrow { arrowname = "混凝土支撑内力",  ls_serie_cyc = ProcessSerializestrGTSurfacedata(xmno,data.Model.gtsensortype._supportAxialForce, dt) });
                        }
                        catch (Exception ex)
                        {
                            ExceptionLog.ExceptionWrite(datatype + "曲线绘制出错,错误信息：" + ex.Message);
                        }
                    }
                    //测斜
                    else if (datatype == "测斜")
                    {

                    }
                    //其他传感器
                    else
                    {
                        try
                        {
                            aspectSeries.seriesarrowList.Add(new seriesarrow { arrowname = datatype,  ls_serie_cyc = ProcessSerializestrGTSensordata(xmno, dt, data.DAL.gtsensortype.GTStringToSensorType(datatype)) });
                        }
                        catch (Exception ex)
                        {
                            ExceptionLog.ExceptionWrite(datatype + "曲线绘制出错,错误信息：" + ex.Message);
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("项目曲线加载出错,错误信息:" + ex.Message + "位置:" + ex.StackTrace);
            }
            return aspectSeries;
        }
        //水平（竖向）位移
        public List<serie_cyc_null> ProcessSerializestrBMWY(int xmno, DataTable dt)
        {

            NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessResultDataMaxTime resultDataMaxTime = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessResultDataMaxTime();
            NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessResultDataRqcxConditionCreate ResultDataRqcxConditionCreate = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessResultDataRqcxConditionCreate();
            NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessFillTotalStationDbFill fillTotalStationDbFill = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessFillTotalStationDbFill();
            NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessSerializestrCYCBMWY serializestrCYCBMWY = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessSerializestrCYCBMWY();
            NFnet_BLL.DataProcess.ProcessResultDataBLL processResultDataBLL = new NFnet_BLL.DataProcess.ProcessResultDataBLL();
            DataView dv = new DataView(dt);
            dv.RowFilter = " type ='表面位移' ";
            List<string> cycList = new List<string>();
            List<string> pointnamelist = new List<string>();
            foreach (DataRowView drv in dv)
            {
                pointnamelist.Add(drv["point_name"].ToString());
            }
            string pointstr = string.Join(",", pointnamelist);

            //生成表面位移查询SQL语句
            var maxTime = resultDataMaxTime.ResultDataMaxTime(xmno.ToString(), NFnet_BLL.LoginProcess.Role.administratrorModel, false, out mssg);
            var ResultDataRqcxConditionCreateModel = ResultDataRqcxConditionCreate.ResultDataRqcxConditionCreate("", "", NFnet_BLL.DisplayDataProcess.QueryType.QT, "day", xmno.ToString(), maxTime);
            var ResultDatacxConditionCreateModel = ResultDataRqcxConditionCreate.ResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateModel.startTime, ResultDataRqcxConditionCreateModel.endTime, NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", xmno.ToString(), maxTime);
            //获取表面位移时间对应的周期
            var model = new NFnet_BLL.DisplayDataProcess.DateTimeToCycCondition(xmno.ToString(), ResultDataRqcxConditionCreateModel.startTime, ResultDataRqcxConditionCreateModel.endTime, "'" + pointstr.Replace(",", "','") + "'");
            if (!processResultDataBLL.ProcessDateTimeToCyc(model, out mssg)) ;
            //查询表获取查询SQL语句
            var processfillTotalStationDbFillModel = fillTotalStationDbFill.FillTotalStationDbFill(pointstr, ResultDatacxConditionCreateModel.sql, xmno.ToString(), NFnet_BLL.LoginProcess.Role.administratrorModel, false);
            return serializestrCYCBMWY.SerializestrCYCBMWY(model.cycList, processfillTotalStationDbFillModel.model.sql, xmno.ToString(), "'" + pointstr.Replace(",", "','") + "'", processResultDataBLL.getfmoszu(), NFnet_BLL.LoginProcess.Role.administratrorModel, false, out mssg);

        }
        //静力水准（压力式）
        public List<serie> ProcessSerializestrGTSettlement(int xmno, DataTable dt)
        {
            NFnet_Interface.DisplayDataProcess.GTSettlement.ProcessSettlementResultDataMaxTime processSettlementResultDataMaxTime = new NFnet_Interface.DisplayDataProcess.GTSettlement.ProcessSettlementResultDataMaxTime();
            NFnet_Interface.DisplayDataProcess.GTSettlement.ProcessSettlementResultDataRqcxConditionCreate processSettlementResultDataRqcxConditionCreate = new 
            NFnet_Interface.DisplayDataProcess.GTSettlement.ProcessSettlementResultDataRqcxConditionCreate();
            NFnet_Interface.DisplayDataProcess.GTSettlement.ProcessSettlementChart processSettlementChart = new NFnet_Interface.DisplayDataProcess.GTSettlement.ProcessSettlementChart();
            NFnet_BLL.DataProcess.GTSettlement.ProcessResultDataBLL processResultDataBLL = new NFnet_BLL.DataProcess.GTSettlement.ProcessResultDataBLL();
            DataView dv = new DataView(dt);
            dv.RowFilter = " type ='沉降' ";
            List<string> cycList = new List<string>();
            List<string> pointnamelist = new List<string>();
            foreach (DataRowView drv in dv)
            {
                pointnamelist.Add(drv["point_name"].ToString());
            }
            string pointstr = string.Join(",", pointnamelist);
            //生成沉降查询SQL语句
            var maxTime = processSettlementResultDataMaxTime.SettlementResultDataMaxTime(xmno, out mssg);
            var ResultDataRqcxConditionCreateModel = processSettlementResultDataRqcxConditionCreate.SettlementResultDataRqcxConditionCreate("", "", NFnet_BLL.DisplayDataProcess.QueryType.QT, "day", xmno, maxTime);
            var ResultDatacxConditionCreateModel = processSettlementResultDataRqcxConditionCreate.SettlementResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateModel.startTime, ResultDataRqcxConditionCreateModel.endTime, NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", xmno, maxTime);

            //查询沉降表获取查询SQL语句
            var fillInclinometerDbFillCondition = new NFnet_BLL.DisplayDataProcess.FillTotalStationDbFillCondition(pointstr, ResultDatacxConditionCreateModel.sql, xmno);
            processResultDataBLL.ProcessfillTotalStationDbFill(fillInclinometerDbFillCondition);
            return processSettlementChart.SerializestrSettlement(fillInclinometerDbFillCondition.sql, xmno, "'" + pointstr.Replace(",", "','") + "'", out mssg);



        }

        //其他传感器
        public List<serie_cyc> ProcessSerializestrGTSensordata(int xmno, DataTable dt, data.Model.gtsensortype datatype)
        {
            NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT.ProcessResultDataMaxTime senorMaxTime = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT.ProcessResultDataMaxTime();
            NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT.ProcessResultDataRqcxConditionCreate processResultDataRqcxConditionCreate = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT.ProcessResultDataRqcxConditionCreate();
            NFnet_Interface.DisplayDataProcess.GT.ProcessFillgtsensordataDbFill processFillgtsensordataDbFill = new NFnet_Interface.DisplayDataProcess.GT.ProcessFillgtsensordataDbFill();
            NFnet_Interface.DisplayDataProcess.GT.ProcessgtsensordataChart processgtsensordataChart = new NFnet_Interface.DisplayDataProcess.GT.ProcessgtsensordataChart();
            DataView dv = new DataView(dt);
            dv.RowFilter = " type ='" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "' ";
            List<string> cycList = new List<string>();
            List<string> pointnamelist = new List<string>();
            foreach (DataRowView drv in dv)
            {
                pointnamelist.Add(drv["point_name"].ToString());
            }
            string pointstr = string.Join(",", pointnamelist);
            //生成沉降查询SQL语句
            var maxTime = senorMaxTime.ResultDataMaxTime(xmno.ToString(), datatype, out mssg);
            var ResultDataRqcxConditionCreateModel = processResultDataRqcxConditionCreate.ScalarResultDataRqcxConditionCreate("", "", NFnet_BLL.DisplayDataProcess.QueryType.QT, "day",ProcessAspectIndirectValue.GetXmnameFromXmno(xmno), maxTime);
            var ResultDatacxConditionCreateModel = processResultDataRqcxConditionCreate.ScalarResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateModel.startTime, ResultDataRqcxConditionCreateModel.endTime, NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", ProcessAspectIndirectValue.GetXmnameFromXmno(xmno), maxTime);

            //查询沉降表获取查询SQL语句
            var processdtuSenorDbFillModel = processFillgtsensordataDbFill.FillTotalStationDbFill(pointstr, ResultDatacxConditionCreateModel.sql, xmno.ToString());
            return processgtsensordataChart.Serializestrgtsensordata(processdtuSenorDbFillModel.sql,  xmno.ToString(), "'" + pointstr.Replace(",", "','") + "'", out mssg);

        }
        //混凝土支撑内力
        public List<serie_cyc> ProcessSerializestrGTSurfacedata(int xmno,data.Model.gtsensortype datatype ,DataTable dt)
        {
            NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT.ProcessSurfaceDataMaxTime surfaceMaxTime = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT.ProcessSurfaceDataMaxTime();
            NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT.ProcessSurfaceDataRqcxConditionCreate processResultDataRqcxConditionCreate = new NFnet_Interface.DisplayDataProcess.SurfaceDisplacement.GT.ProcessSurfaceDataRqcxConditionCreate();
            NFnet_Interface.DisplayDataProcess.GT.ProcessFillgtsurfaceataDbFill processFillgtsurfacedataDbFill = new NFnet_Interface.DisplayDataProcess.GT.ProcessFillgtsurfaceataDbFill();
            NFnet_Interface.DisplayDataProcess.GT.Processgtsurface_cycdataChart processgtsurfacedataChart = new NFnet_Interface.DisplayDataProcess.GT.Processgtsurface_cycdataChart();
            DataView dv = new DataView(dt);
            dv.RowFilter = " type ='" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce) + "' ";
            List<string> cycList = new List<string>();
            List<string> pointnamelist = new List<string>();
            foreach (DataRowView drv in dv)
            {
                pointnamelist.Add(drv["point_name"].ToString());
            }
            string pointstr = string.Join(",", pointnamelist);
            //生成沉降查询SQL语句
            var maxTime = surfaceMaxTime.ResultDataMaxTime(xmno, datatype, out mssg);
            var ResultDataRqcxConditionCreateModel = processResultDataRqcxConditionCreate.SurfaceDataRqcxConditionCreate("", "", NFnet_BLL.DisplayDataProcess.QueryType.QT, "day", xmno.ToString(), maxTime);
            var ResultDatacxConditionCreateModel = processResultDataRqcxConditionCreate.SurfaceDataRqcxConditionCreate(ResultDataRqcxConditionCreateModel.startTime, ResultDataRqcxConditionCreateModel.endTime, NFnet_BLL.DisplayDataProcess.QueryType.RQCX, "", xmno.ToString(), maxTime);

            //查询沉降表获取查询SQL语句
            var processdtuSenorDbFillModel = processFillgtsurfacedataDbFill.FillTotalStationDbFill(pointstr, ResultDatacxConditionCreateModel.sql, xmno, datatype);
            return processgtsurfacedataChart.Serializestrgtsurface_cycdata(processdtuSenorDbFillModel.sql, xmno.ToString(), "'" + pointstr.Replace(",", "','") + "'", out mssg);

        }



        public class AspectSeries
        {
            public List<seriesarrow> seriesarrowList { get; set; }

        }
    }
}
