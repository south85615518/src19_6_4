﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUSenorNoGet
    {
        public ProcessDTUSenorBLL processDTUSenorBLL = new ProcessDTUSenorBLL();
        public string DTUSenorNoGet(int xmno,out string mssg)
        {
            var processDTUSenorNoGetModel = new ProcessDTUSenorBLL.ProcessDTUSenorNoGetModel(xmno);
            if (processDTUSenorBLL.ProcessDTUSenorNoGet(processDTUSenorNoGetModel, out mssg))
            {
                return processDTUSenorNoGetModel.senornoStr;
            }
            return "";
        }
    }
}
