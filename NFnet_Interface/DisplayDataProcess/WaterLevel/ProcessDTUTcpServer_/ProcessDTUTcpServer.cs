﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Tool;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_Interface.UserProcess;

namespace NFnet_BLL.DisplayDataProcess
{
    public  class ProcessDTUTcpServer
    {
        public static TcpListener tcpListener = null;
        public delegate void ClientConnectEvent(IAsyncResult ar);
        private event ClientConnectEvent conEvent;
        public System.Timers.Timer timer = new System.Timers.Timer(10000);
        public static string ip;
        public static string port;
        public static Dictionary<TcpListener, Socket> tcpListenerdts = new Dictionary<TcpListener, Socket>();
        public static Dictionary<TcpListener, int> tcpListenerport = new Dictionary<TcpListener, int>();

        public  void DTUTcpServerStart(string dtuportpath)
        {
            try
            {
                //this.comboBox2.SelectedIndex = 1;
                TcpServerStopTotal();
                //EncodingSet();
                List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad(dtuportpath);
                //"D:\\华南水电\\DTUServer\\TcpSever_MYSQL+\\TcpSever\\bin\\debug\\端口配置文件\\setting.txt"
                TcpserverMutilStart(ltlif);
                TimerTick();



            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("服务器监听失败!");
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            TcpServerStopTotal();
        }
        #region 启动服务器监听
        public void TcpserverMutilStart(List<tcpclientIPInfo> ltlif)
        {
            try
            {

                //FlushEvent += ClientConnectFlush;
                foreach (tcpclientIPInfo tif in ltlif)
                {

                    TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(tif.ip), Convert.ToInt32(tif.port)));
                    tcpListenstart(tcpListener, tif.port);
                }

            }
            catch (Exception ex)
            {

            }


        }
        public string tcpListenstart(TcpListener tcpListener, int port)
        {
            string mss = "";
            try
            {
                conEvent += clientConnect;
                tcpListener.Start();
                ExceptionLog.DTURecordWrite(string.Format("端口{0}监听成功!{1}", port, DateTime.Now));
                tcpListenerport[tcpListener] = port;
                IAsyncResult ar;
                tcpListener.BeginAcceptSocket(new AsyncCallback(conEvent), tcpListener);
                conEvent -= clientConnect;


            }
            catch (Exception ex)
            {
                mss = ex.Message;
            }
            return mss;
        }
        public delegate void AcceptEvent(AsyncCallback ar, TcpListener tcpListener);
        public event AcceptEvent evt;
        private void clientConnect(IAsyncResult ar)
        {

            try
            {

                TcpListener listener = (TcpListener)ar.AsyncState;

                //接受客户的连接,得到连接的Socket

                Socket s = listener.EndAcceptSocket(ar);
                //receiveData(client);
                //if(tcp)
                tcpListenerdts[listener] = s;

                //FlushEvent += ClientConnectFlush;
                string mssg = string.Format("远程客户端{0}上线 {1}  {2}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, DateTime.Now, tcpListenerport[listener]);
                ExceptionLog.ExceptionWrite(mssg);
                //ar = this.listBox2.BeginInvoke(FlushEvent, mssg);
                //this.listBox2.EndInvoke(ar);
                //FlushEvent -= ClientConnectFlush;
                //FlushEvent -= ClientConnectFlush;


                ReceiveMess(s, tcpListenerport[listener]);
                ReStartListen(listener);
                ////conEvent += clientConnect;
                //tcpListener.BeginAcceptSocket(new AsyncCallback(conEvent), tcpListener);

            }

            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
            }

        }
        #endregion
        #region 心跳包
        public void TimerTick()
        {
            timer.Elapsed += new System.Timers.ElapsedEventHandler(HearBeat);

            timer.Enabled = true;

            timer.AutoReset = true;
        }
        private static object UsingPrinterLocker = new object();
        List<TcpListener> dtsDelete = new List<TcpListener>();
        public void HearBeat(object source, System.Timers.ElapsedEventArgs e)
        {
            //int i = 0;



            if (tcpListenerdts.Keys.Count == 0) return;
            System.Threading.Monitor.Enter(UsingPrinterLocker);

            try
            {
                foreach (TcpListener listener in tcpListenerdts.Keys)
                {
                    //测试连接在该端口上的客户端的连接状态
                    if (tcpListenerdts[listener] == null) continue;
                    //TcpClient tc = null;
                    Socket s = tcpListenerdts[listener] as Socket;


                    if (s == null) return;
                    if (!IsSocketAlive(s))
                    {
                        //if (s == null) return;

                        //FlushEvent += ClientConnectFlush;
                        string mssg = string.Format("远程客户端{0}已经断开连接 {1}  {2}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, DateTime.Now, tcpListenerport[listener]);
                        ExceptionLog.DTURecordWrite(mssg);

                        //IAsyncResult ar = this.listBox2.BeginInvoke(FlushEvent, mssg);
                        //this.listBox2.EndInvoke(ar);
                        //FlushEvent -= ClientConnectFlush;
                        ////conEvent += clientConnect;

                        conEvent += clientConnect;
                        listener.BeginAcceptSocket(new AsyncCallback(conEvent), listener);
                        conEvent -= clientConnect;
                        //tcpListenerdts[listener] = null;
                        dtsDelete.Add(listener);
                        //tcpListenerdts.Remove(listener);
                    }



                }
                if (dtsDelete.Count > 0)
                {
                    for (int i = 0; i < dtsDelete.Count; i++)
                    {
                        Socket stmp = tcpListenerdts[dtsDelete[i]];
                        stmp.Close();
                        stmp.Dispose();
                        tcpListenerdts[dtsDelete[i]] = null;


                    }
                    dtsDelete = new List<TcpListener>();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                System.Threading.Monitor.Exit(UsingPrinterLocker);
            }


        }

        public bool IsSocketAlive(Socket s)
        {
            //byte[] buff = new byte[100];

            if (s == null) return false;
            try
            {

                s.Send(Encoding.UTF8.GetBytes("1"));
                return true;

            }
            catch (Exception ex)
            {

                return false;
            }

        }

        #endregion
        //#region 窗体控件信息显示
        //public void ClientConnectFlush(string mssg)
        //{
        //    DateTime dt = DateTime.Now;

        //    ExceptionLog.DTURecordWrite(mssg);


        //}
        //public void ClientDataFlush(string mssg)
        //{

        //    DateTime dt = DateTime.Now;

        //    this.listBox1.Items.Add(mssg);


        //}
        

        //#endregion
        #region 数据通信
        //客户端上线从服务器等待客户端的数据回发
        public void ReceiveMess(Socket s, int port)
        {

            ExceptionLog.ExceptionWrite("开始接收数据");
            //对方发送文件
            DateTime dt = DateTime.Now;

            try
            {

                StringBuilder datastr = new StringBuilder(256);
                int count;
                byte[] b = new byte[4096];
                //设置20秒等待时间
                //bool state = s.Blocking;
                s.ReceiveTimeout = 1200000;

                try
                {
                    while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                    {
                        var str = Encoding.UTF8.GetString(b, 0, count);
                        datastr.Append(str);
                        if (IsThisSendFinish(s, datastr.ToString(), port))
                        {
                            datastr = new StringBuilder(256);
                        }
                        b = new byte[4096];
                        s.ReceiveTimeout = 1200000;
                    }
                    //DataRec(s,port);
                    //是否需要给设备发送命令，如果没有关闭连接
                    //s.Shutdown(SocketShutdown.Both);
                }
                catch (Exception ex)
                {

                    
                    ExceptionLog.DTURecordWrite(string.Format("接收{0}  从{1}发来的数据超时，现在向客户端发送数据获取请求", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port));
                    
                    //向客户端发送数据获取请求
                    DTUDATARequestSend(s, port);
                    return;
                }
                ExceptionLog.ExceptionWrite("数据接收完成");
               
               
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
            }


        }

        public bool IsThisSendFinish(Socket s, string str, int port)
        {
            if (str.IndexOf("the end") != -1)
            {
                DTUDATAImport(str, port, s);
                ExceptionLog.DTURecordWrite(string.Format("{2} 接收{0}  从{1}发来的数据【{3}】  ", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port, DateTime.Now, str));
                return true;
            }
            return false;
        }
        public bool DataRec(Socket s, int port)
        {
            int count = 0;
            byte[] b = new byte[1024];

            StringBuilder datastr = new StringBuilder(256);

            while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
            {
                var str = Encoding.UTF8.GetString(b, 0, count);
                datastr.Append(str);
                if (IsThisSendFinish(s, datastr.ToString(), port))
                {
                    datastr = new StringBuilder(256);
                }
                b = new byte[4096];

            }
            return true;

        }
        //服务器等待客户端的数据超时进行主动获取
        public void DTUDATARequestSend(Socket s, int port)
        {
            int i = 0;
            s.SendTimeout = 120000;
            IAsyncResult ar = null;
            //连续请求三次
            for (i = 0; i < 3; i++)
            {
                try
                {
                    //设置10秒等待时间

                    s.Send(Encoding.UTF8.GetBytes("set jiange:00,01,01"));

                    
                    ExceptionLog.DTURecordWrite(string.Format("向客户端{0}  由{1}发送数据获取请求成功,现在等待客户端数据回传...", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, port));

                    int count;
                    StringBuilder datastr = new StringBuilder(256);
                    byte[] b = new byte[4096];
                    //设置20秒等待时间
                    //bool state = s.Blocking;
                    s.ReceiveTimeout = 1200000;


                    //DataRec(s,port);
                    //int count = 0;

                    while ((count = s.Receive(b, 4096, SocketFlags.None)) != 0)
                    {
                        var str = Encoding.UTF8.GetString(b, 0, count);
                        datastr.Append(str);
                        if (IsThisSendFinish(s, datastr.ToString(), port))
                        {
                            datastr = new StringBuilder(256);
                        }
                        b = new byte[4096];

                    }

                    //ReceiveMess(s,port);

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(string.Format("向客户端{0}发送数据请求命令失败,失败原因{1} 请求次数{2}", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address, ex.Message, i));
                    continue;
                }

                SocketClose(s);
                ExceptionLog.DTURecordWrite( string.Format("向{0}  从{2}请求数据获取超时!设备采集时间间隔超过了规定时间或者设备已经损坏", ((System.Net.IPEndPoint)s.RemoteEndPoint).Address));
               
            }
        }

        #endregion
        #region 数据处理
        /// <summary>
        /// 数据导入
        /// </summary>
        /// <param name="str"></param>
        public void DTUDATAImport(string datastr, int port, Socket s)
        {

            DTUDATADescode(port, datastr, s);

        }
        /// <summary>
        /// 数据校验
        /// </summary>
        /// <returns></returns>
        public bool messvalidate()
        {
            return true;
        }
        public string point_name = "";
        public int xmno = 0;
        public static ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();
        public static ProcessDTUTaskQueueFinishedSet processDTUTaskQueueFinishedSetBLL = new ProcessDTUTaskQueueFinishedSet();
        public static ProcessDTUDataAlarmBLL processDTUDataAlarmBLL = new ProcessDTUDataAlarmBLL();
        public static string mssg = "";
        public static ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        //DTU数据解析入库
        public void DTUDATADescode(int port, string str, Socket s)
        {
            string addressno = str.Split(':')[0];
            string[] attrs = str.Split('\r');
            int i = 0;
            string pointname = "";
            double line = 0;
            double holedepth = 0;
            int xmno = GetDTUXmnoFromPort(port);
            string xmname = GetDTUXmnameFromPort(port);
            processDTUDATABLL.ProcessDATATmpDel(xmno, out mssg);
            for (i = 1; i < attrs.Length - 1; i++)
            {

                str = attrs[i];
                if (str.IndexOf("2017/") == -1) continue;
                str = str.Substring(str.IndexOf("\r\n") + 2);
                string datastr = str.Substring(str.IndexOf("2017/"));
                string[] dataary = datastr.Split(' ');


                DTUModulePointName(xmno, port, dataary[2], addressno, out pointname, out line, out holedepth);
                DTU.Model.dtudata model = new DTU.Model.dtudata
                {
                    point_name = pointname,
                    mkh = 0,
                    deep = Convert.ToDouble((line - Convert.ToDouble(dataary[3]) / 1000).ToString("0.000")),
                    time = DateTime.Now, //Convert.ToDateTime(dataary[0] + " " + dataary[1]),
                    xmno = xmno,
                    groundElevation = Convert.ToDouble((Convert.ToDouble(dataary[3]) / 1000).ToString("0.000"))
                };
                processDTUDATABLL.ProcessDTUDATAInsertBLL(model, out mssg);

                CommendListTaskDelivery(xmno, pointname, s);
            }

            List<string> alarmInfoList = processDTUDataAlarmBLL.DTUDataAlarm(xmname, xmno);
            emailSendBLL.ProcessEmailSend(alarmInfoList, xmname, xmno, out mssg);
        }
        #endregion
        #region 关闭/重启
        //重新开启监听
        public void ReStartListen(TcpListener tcpListener)
        {
            conEvent += clientConnect;
            IAsyncResult ar;
            tcpListener.BeginAcceptSocket(new AsyncCallback(conEvent), tcpListener);
            conEvent -= clientConnect;
        }
        
        //停止服务
        public void TcpServerStopTotal()
        {
            try
            {
                foreach (TcpListener tcpListener in tcpListenerdts.Keys)
                {
                    if (tcpListenerdts[tcpListener] != null)
                    {
                        //关闭连接
                        Socket s = tcpListenerdts[tcpListener];
                        s.Shutdown(SocketShutdown.Both);
                        s.Dispose();
                        s = null;
                    }

                    tcpListener.Server.Close();
                    tcpListener.Stop();



                }
                tcpListenerdts.Clear();
            }
            catch (Exception ex)
            {


            }
        }
        public void TcpServerStop(TcpListener tcpListener)
        {
            try
            {

                //关闭连接
                Socket s = tcpListenerdts[tcpListener];
                s.Shutdown(SocketShutdown.Both);
                s.Dispose();
                s = null;
                tcpListener.Server.Close();
                tcpListener.Stop();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("关闭服务器端口{0}监听出错,错误信息：" + ex.Message);

            }
        }
        private object useSocketLocker = new object();
        public void SocketClose(Socket s)
        {
            System.Threading.Monitor.Enter(useSocketLocker);
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("关闭远程客户端出错!出错原因:" + ex.Message);
            }
            finally
            {
                System.Threading.Monitor.Exit(useSocketLocker);
            }
        }
        /// <summary>
        /// 重启单个服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        public void TcpListenerRestart(TcpListener listenser)
        {
            List<tcpclientIPInfo> ltlif = ProcessInfoImportBLL.MachineSettingInfoLoad("端口配置文件\\setting.txt");
            int port = tcpListenerport[listenser];
            var ip = (from m in ltlif where m.port == port select m.ip).ToList()[0];
            TcpServerStop(listenser);
            try
            {
                TcpListener tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(port)));
                tcpListenstart(tcpListener, port);
                ExceptionLog.ExceptionWrite(string.Format("重启服务器端口{0}成功!", port));
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("重启服务器端口{0}出错，错误信息:" + ex.Message, port));
            }

        }
        /// <summary>
        /// 重启所有服务器端口监听
        /// </summary>
        /// <param name="listenser"></param>
        public void TcpListenerRestartTotal(TcpListener listenser)
        {

        }
        #endregion
        #region 处理类
        public static ProcessDTUPortXm processDTUPortXm = new ProcessDTUPortXm();
        public static int GetDTUXmnoFromPort(int port)
        {
            return processDTUPortXm.DTUPortXm(port, out mssg).xmno;
        }
        public static ProcessDTUPortXmname processDTUPortXmname = new ProcessDTUPortXmname();
        public static string GetDTUXmnameFromPort(int port)
        {
            return processDTUPortXmname.DTUPortXm(port, out mssg);
        }
        public static ProcessDTUPointModule processDTUPointModule = new ProcessDTUPointModule();
        public static bool DTUModulePointName(int xmno, int port, string od, string addressno, out string pointname, out double line, out double holedepth)
        {
            DTU.Model.dtu model = processDTUPointModule.DTUPointModule(xmno, port, od, addressno, out mssg);
            //ProcessPrintMssg.Print(mssg);
            pointname = model.point_name;
            line = model.line;
            holedepth = model.holedepth;
            return true;
            //return model.point_name;
        }
        #endregion
        #region 设置命令
        public DTUCommend CommendCreate(int commendIndex, int xmno, string point_name)
        {
            DTUCommend dtucommend = null;
            switch (commendIndex)
            {
                //设置时间间隔
                case 0:

                    string commendstr = GetTimeInterval(xmno, point_name);
                    dtucommend = new DTUCommend("设置时间间隔", "success", "failed", 60000, 60000, commendstr, 0);
                    return dtucommend;
                //break;
            }
            return new DTUCommend();
        }
        public enum CommendList
        {
            TIME_INTERVAL, TIME_TASK
        };
        public string GetTaskTime(int xmno, string point_name)
        {
            return "";
        }

        public string GetTimeInterval(int xmno, string point_name)
        {
            ProcessDTUTimeIntervalLoad dtuTimeIntervalLoad = new ProcessDTUTimeIntervalLoad();
            var model = dtuTimeIntervalLoad.DTUTimeIntervalLoad(xmno, point_name, out mssg);
            if (model == null)
                return "";
            return string.Format("set jiange:{0},{1},{2}", model.hour.ToString("00"), model.minute.ToString("00"), model.times.ToString("00"));
        }

        public class DTUCommend
        {
            public string commendName { get; set; }
            public string delivery { get; set; }
            public string successWords { get; set; }
            public string failedWords { get; set; }
            public int sendTimeOut { get; set; }
            public int recTimeOut { get; set; }
            public int commendIndex { get; set; }
            //public int 
            public DTUCommend(string commendName, string successWords, string failedWords, int sendTimeOut, int recTimeOut, string delivery, int commendIndex)
            {
                this.commendName = commendName;
                this.successWords = successWords;
                this.failedWords = failedWords;
                this.sendTimeOut = sendTimeOut;
                this.recTimeOut = recTimeOut;
                this.delivery = delivery;
                this.commendIndex = commendIndex;
            }
            public DTUCommend()
            {

            }
        }


        public void CommendListTaskDelivery(int xmno, string pointname, Socket s)
        {
            List<DTUCommend> lscommend = new List<DTUCommend>();
            List<int> ls = GetCommendFromTaskQueue(xmno, pointname);
            if (ls.Count == 0) return;
            foreach (int commendIndx in ls)
            {
                lscommend.Add(CommendCreate(commendIndx, xmno, pointname));
            }
            List<string> resultList = new List<string>();
            foreach (DTUCommend commend in lscommend)
            {
                if (commendDelivery(xmno, pointname, s, commend))
                {
                    resultList.Add(string.Format("向项目{0}点名{1}发送{2}命令成功  ", xmno, pointname, commend.commendName, DateTime.Now));
                }
                else
                {
                    resultList.Add(string.Format("向项目{0}点名{1}发送{2}命令失败  ", xmno, pointname, commend.commendName, DateTime.Now));
                }

            }
            ExceptionLog.DTURecordWrite( string.Join("\n", resultList));
            //DTUDATAImport(datastr);
            

        }
        public bool commendDelivery(int xmno, string point_name, Socket s, DTUCommend commend)
        {
            int i = 0;
            for (i = 0; i < 3; i++)
            {
                try
                {
                    byte[] buff = new byte[1024];
                    s.SendTimeout = commend.sendTimeOut;
                    s.Send(Encoding.UTF8.GetBytes(commend.delivery));
                    s.ReceiveTimeout = 2 * commend.recTimeOut;
                    int cont = s.Receive(buff, SocketFlags.None);
                    if (cont < 1024)
                    {
                        string recresult = Encoding.UTF8.GetString(buff);
                        if (commend.successWords.IndexOf("success") != -1)
                        {
                            processDTUTaskQueueFinishedSetBLL.DTUTaskQueueFinishedSet(xmno, point_name, commend.commendIndex, out mssg);
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        //发送返回的字节数太多
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(string.Format("向项目编号{0}点{1}发送命令{2}失败 第{3}次", xmno, point_name, commend.commendName, i));
                    return false;
                }
            }
            return false;
        }


        public ProcessDTUQueueLoad processDTUQueueLoad = new ProcessDTUQueueLoad();
        public List<int> GetCommendFromTaskQueue(int xmno, string pointname)
        {
            return processDTUQueueLoad.DTUQueueLoad(xmno, pointname, out mssg);
        }
        #endregion

    }
}
