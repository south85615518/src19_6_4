﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimeAlarmTable
    {
        public ProcessDTUTimeAlarmBLL processDTUTimeAlarmBLL = new ProcessDTUTimeAlarmBLL();
        public DataTable DTUTimeAlarmLoad(int pageIndex,int rows, int xmno, string colName, string sord, out string mssg)
        {
            var processDTUTimeAlarmLoadBLLModel = new ProcessDTUTimeAlarmBLL.ProcessDTUSenorLoadBLLModel(xmno,colName,pageIndex,rows,sord);
            if (processDTUTimeAlarmBLL.ProcessDTUTimeAlarmLoadBLL(processDTUTimeAlarmLoadBLLModel, out mssg))
            {
                return processDTUTimeAlarmLoadBLLModel.dt;
            }
            return new DataTable();
        }
    }
}
