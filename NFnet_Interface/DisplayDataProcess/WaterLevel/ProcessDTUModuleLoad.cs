﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUModuleLoad
    {
        public ProcessDTUModuleBLL processDTUModuleBLL = new ProcessDTUModuleBLL();
        public DataTable DTUModuleLoad(int pageIndex, int rows, int xmno, string colName, string sord, out string mssg)
        {
            var processDTUModuleLoadBLLModel = new ProcessDTUModuleBLL.ProcessDTUModuleLoadBLLModel(xmno, colName, pageIndex, rows, sord);
            if (processDTUModuleBLL.ProcessDTUModuleLoadBLL(processDTUModuleLoadBLLModel, out mssg))
            {
                return processDTUModuleLoadBLLModel.dt;
            }
            return new DataTable();
        }
    }
}
