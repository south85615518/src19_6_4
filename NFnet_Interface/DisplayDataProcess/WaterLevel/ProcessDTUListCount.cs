﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUListCount
    {
        public ProcessDTUPointAlarmBLL processDTUPointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public int DTUListCount(int xmno, string searchstring, out string mssg)
        {
            var processDTUListCountModel = new ProcessDTUPointAlarmBLL.ProcessDTUListCountModel(xmno, searchstring);
            if (processDTUPointAlarmBLL.ProcessDTUListCount(processDTUListCountModel, out mssg))
            {
                return processDTUListCountModel.totalCont;
            }
            return 0;
        } 
    }
}
