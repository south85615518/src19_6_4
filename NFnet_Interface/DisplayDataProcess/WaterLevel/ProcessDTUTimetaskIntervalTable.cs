﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimetaskIntervalTable
    {
        public ProcessDTUTimeIntervalBLL processDTUTimeIntervalBLL = new ProcessDTUTimeIntervalBLL();
        public DataTable DTUTimeIntervalLoad(int pageIndex,int rows, int xmno, string colName, string sord, out string mssg)
        {
            var processDTUTimeIntervalLoadBLLModel = new ProcessDTUTimeIntervalBLL.ProcessDTUSenorLoadBLLModel(xmno,colName,pageIndex,rows,sord);
            if (processDTUTimeIntervalBLL.ProcessDTUTimeIntervelLoadBLL(processDTUTimeIntervalLoadBLLModel, out mssg))
            {
                return processDTUTimeIntervalLoadBLLModel.dt;
            }
            return new DataTable();
        }
    }
}
