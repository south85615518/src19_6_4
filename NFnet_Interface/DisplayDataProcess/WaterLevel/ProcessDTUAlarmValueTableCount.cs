﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUAlarmValueTableCount
    {
        public ProcessDTUAlarmValueBLL processDTUAlarmValueBLL = new ProcessDTUAlarmValueBLL();
        public int DTUAlarmValueTableCount(int xmno,string searchstring,out string mssg)
        {
            var processDTUAlarmValueTableCountModel = new ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueTableCountModel(xmno,searchstring);
            if (processDTUAlarmValueBLL.ProcessDTUAlarmValueTableCount(processDTUAlarmValueTableCountModel, out mssg))
            {
                return processDTUAlarmValueTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
