﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUDATAList
    {
        public ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();
        public List<DTU.Model.dtudata> DTUDATAList(int xmno,string point_name,DateTime starttime,DateTime endtime,out string mssg )
        {
            var model = new ProcessDTUDATABLL.ProcessDTUDATAListGetModel(xmno,point_name,starttime,endtime);
            if (processDTUDATABLL.ProcessDTUDATAListGet(model, out mssg))
            {
                return model.dtudatalist;
            }
            return new List<DTU.Model.dtudata>();
        }
    }
}
