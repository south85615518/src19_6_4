﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimeAlarmTableCount
    {
        public ProcessDTUTimeAlarmBLL processDTUTimeAlarmBLL = new ProcessDTUTimeAlarmBLL();
        public int DTUTimeAlarmTableCount(int xmno,string searstring,out string mssg)
        {
            var processDTUTimeAlarmTableCountModel = new ProcessDTUTimeAlarmBLL.ProcessDTUSenorTableCountModel(xmno,searstring);
            if (processDTUTimeAlarmBLL.ProcessDTUTimeAlarmTableCount(processDTUTimeAlarmTableCountModel, out mssg))
            {
                return processDTUTimeAlarmTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
