﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointModuleWhithoutOd
    {
        public ProcessDTUPointBLL processDTUPointBLL = new ProcessDTUPointBLL();
        public DTU.Model.dtu DTUPointModule(int xmno, int port,string addressno ,out string mssg)
        {
            var processDTUPointModuleModel = new NFnet_BLL.DisplayDataProcess.WaterLevel.ProcessDTUPointBLL.ProcessDTUPointModuleWhithoutOdModel(xmno, port,addressno);
            if (processDTUPointBLL.ProcessDTUPointModule(processDTUPointModuleModel, out mssg))
            {
                return processDTUPointModuleModel.model;
            }
            return new DTU.Model.dtu();
        }
    }
}
