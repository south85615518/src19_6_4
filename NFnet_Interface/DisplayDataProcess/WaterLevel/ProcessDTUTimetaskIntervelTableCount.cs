﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimetaskIntervelTableCount
    {
        public ProcessDTUTimeIntervalBLL processDTUTimeIntervalBLL = new ProcessDTUTimeIntervalBLL();
        public int DTUTimeIntervalTableCount(int xmno,string searstring,out string mssg)
        {
            var processDTUTimeIntervalTableCountModel = new ProcessDTUTimeIntervalBLL.ProcessDTUSenorTableCountModel(xmno,searstring);
            if (processDTUTimeIntervalBLL.ProcessDTUTimeIntervelTableCount(processDTUTimeIntervalTableCountModel, out mssg))
            {
                return processDTUTimeIntervalTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
