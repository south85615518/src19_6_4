﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_MODAL;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessSerializestrSW
    {
        public ProcessWaterLevelChartCom senorChartCom = new ProcessWaterLevelChartCom();
        public List<serie> SerializestrSW(object sql,object xmno,object pointname,Role role,bool tmpRole,out string mssg)
        {

            var serializestrSWModel = new SerializestrSWCondition(sql,xmno, pointname);
            var processSerializestrSBWYModel = new ProcessWaterLevelChartCom.ProcessSerializestrSWModel(serializestrSWModel, role, tmpRole);
            if (senorChartCom.ProcessSerializestrSW(processSerializestrSBWYModel, out mssg))
                return processSerializestrSBWYModel.model.series;
            return new List<serie>();

            
        }
    }
}
