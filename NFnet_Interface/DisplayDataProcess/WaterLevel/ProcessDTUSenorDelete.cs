﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUSenorDelete
    {
        public ProcessDTUSenorBLL processDTUSenorBLL = new ProcessDTUSenorBLL();
        public bool DTUSenorDelete(int xmno,string senorno,out string mssg)
        {
            var processDTUSenorDelBLLModel = new ProcessDTUSenorBLL.ProcessDTUSenorDelBLLModel(xmno,senorno);
            return processDTUSenorBLL.ProcessDTUSenorDelBLL(processDTUSenorDelBLLModel,out mssg);
        }
    }
}
