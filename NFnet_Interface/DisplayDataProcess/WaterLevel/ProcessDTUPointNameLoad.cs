﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointNameLoad
    {
        public ProcessDTUPointBLL processDTUPointBLL = new ProcessDTUPointBLL();
        public List<string> DTUPointNameLoad(int xmno,out string mssg)
        {
            var processDTUPointNameLoadModel = new ProcessDTUPointBLL.ProcessDTUPointNameLoadModel(xmno);
            if (processDTUPointBLL.ProcessDTUPointNameLoad(processDTUPointNameLoadModel,out mssg))
            {
                return processDTUPointNameLoadModel.ls;
            }
            return new List<string>();
        }
    }
}
