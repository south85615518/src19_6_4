﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Tool;
using System.IO;
using InclimeterDAL.Model;
using NFnet_Interface.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUDataAlarmBLL
    {
        public  string xmname {get;set; }
        public int xmno { get; set; }
        //public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessDTUDataAlarmBLL() { 
        }
        public ProcessDTUDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = xmname;
            this.xmno = xmno;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static ProcessDTUPointAlarmBLL processDTUPointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public static string mssg = "";
       // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
       
        public bool main()
        {
            
           return TestDTUModelList();
            
        }
        public ProcessDTUAlarmModelListGet processDTUAlarmModelListGet = new ProcessDTUAlarmModelListGet();
        public ProcessDTUPointAlarmModel processDTUPointAlarmModel = new ProcessDTUPointAlarmModel();
        public bool TestDTUModelList()
        {
            
            
                List<DTU.Model.dtudatareport> lc = (List<DTU.Model.dtudatareport>)processDTUAlarmModelListGet.DTUAlarmModelListGet(xmno, out mssg);
                //ProcessPrintMssg.Print(mssg);
                DTUPointAlarm(lc);
                return true;
            //ProcessPrintMssg.Print(mssg);

        }
        public DTU.Model.dtu TestPointAlarmValue(string pointName )
        {
            return processDTUPointAlarmModel.DTUPointAlarmModel(xmno,pointName,out mssg);
        }

        

        public static ProcessDTUAlarmValueModel processDTUAlarmValueModel = new ProcessDTUAlarmValueModel();
        public List<DTU.Model.dtualarmvalue> TestAlarmValueList(DTU.Model.dtu pointalarm)
        {
            List<DTU.Model.dtualarmvalue> alarmvalueList = new List<DTU.Model.dtualarmvalue>();
            
                //ProcessPrintMssg.Print("一级：" + mssg);
            alarmvalueList.Add(processDTUAlarmValueModel.DTUAlarmValueModel(pointalarm.firstAlarm,xmno,out mssg));
            alarmvalueList.Add(processDTUAlarmValueModel.DTUAlarmValueModel(pointalarm.secalarm, xmno, out mssg));
            alarmvalueList.Add(processDTUAlarmValueModel.DTUAlarmValueModel(pointalarm.thirdalarm, xmno, out mssg));
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<DTU.Model.dtualarmvalue> levelalarmvalue, DTU.Model.dtudatareport resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessDTUPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (processDTUPointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                //processDTUPointAlarmBLL.ProcessPointAlarmfilterInformation();
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public bool DTUPointAlarm(List<DTU.Model.dtudatareport> lc)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format(" {0} ", DateTime.Now));
            ls.Add(string.Format(" {0} ", xmname));
            ls.Add(string.Format(" {0} ", "水位--水位传感器--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (DTU.Model.dtudatareport cl in lc)
            {
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname))) return false;
                DTU.Model.dtu pointvalue = TestPointAlarmValue(cl.point_name);
                List<DTU.Model.dtualarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> DTUDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        public void DTUPointAlarm(DTU.Model.dtudatareport data)
        {

            DTU.Model.dtu pointvalue = TestPointAlarmValue(data.point_name);
            if (pointvalue == null) return;
            List<DTU.Model.dtualarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestPointAlarmfilterInformation(alarmList, data);
        }

        public Tool.DTUReportHelper.dtualarm ProcessPointAlarmValue(string pointname, int xmno)
        {

            DTU.Model.dtu pointvalue = TestPointAlarmValue(pointname);
            List<DTU.Model.dtualarmvalue> alarmList = TestAlarmValueList(pointvalue);
            var model = new Tool.DTUReportHelper.dtualarm();
            model.pointname = pointname;
            if (alarmList[2] != null)
            {
                model.third_acdisp = alarmList[2].acdeep;
                model.third_thisdisp = alarmList[2].thisdeep;
                model.third_deep = alarmList[2].deep;
                model.third_rap = alarmList[2].rap;
            }
            if (alarmList[1] != null)
            {

                model.sec_acdisp = alarmList[1].acdeep;
                model.sec_thisdisp = alarmList[1].thisdeep;
                model.sec_deep = alarmList[1].deep;
                model.sec_rap = alarmList[1].rap;
            }
            if (alarmList[0] != null)
            {

                model.first_acdisp = alarmList[0].acdeep;
                model.first_thisdisp = alarmList[0].thisdeep;
                model.first_deep = alarmList[0].deep;
                model.first_rap = alarmList[0].rap;


            }


            return model;
        }


    }
}