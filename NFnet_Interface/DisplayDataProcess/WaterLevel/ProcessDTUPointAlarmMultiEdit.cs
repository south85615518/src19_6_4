﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointAlarmMutilEdit
    {
        public ProcessDTUPointAlarmBLL pointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public void PointAlarmValueMutilEdit(DTU.Model.dtu model,string pointstr ,out string mssg)
        {
            var processAlarmMultiUpdateModel = new ProcessDTUPointAlarmBLL.ProcessAlarmMultiUpdateModel(pointstr, model);
            pointAlarmBLL.ProcessAlarmMultiUpdate(processAlarmMultiUpdateModel, out mssg);
        }
    }
}
