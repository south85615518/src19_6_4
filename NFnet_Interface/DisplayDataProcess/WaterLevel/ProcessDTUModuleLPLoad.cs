﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUModuleLPLoad
    {
        public ProcessDTUModuleLPBLL processDTUModuleLPBLL = new ProcessDTUModuleLPBLL();
        public DataTable DTUModuleLPLoad(int pageIndex, int rows, int xmno, string colName, string sord, out string mssg)
        {
            var processDTUModuleLPLoadBLLModel = new ProcessDTUModuleLPBLL.ProcessDTUModuleLPLoadBLLModel(xmno, colName, pageIndex, rows, sord);
            if (processDTUModuleLPBLL.ProcessDTUModuleLPLoadBLL(processDTUModuleLPLoadBLLModel, out mssg))
            {
                return processDTUModuleLPLoadBLLModel.dt;
            }
            return new DataTable();
        }
    }
}
