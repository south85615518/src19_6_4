﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
   public class ProcessPointNewestDateTimeGet
    {
       public ProcessDTUSenorCom senorCom = new ProcessDTUSenorCom();
       public DateTime PointNewestDateTimeGet(int xmno,string pointname,Role role,bool tmpRole,out string mssg)
       {
           var inclinometerPointNewestDateTimeCondition = new InclinometerPointNewestDateTimeCondition(xmno,pointname);
           var processPointNewestDateTimeGetModel = new ProcessDTUSenorCom.ProcessPointNewestDateTimeGetModel(inclinometerPointNewestDateTimeCondition, role, tmpRole);
           if (!senorCom.ProcessPointNewestDateTimeGet(processPointNewestDateTimeGetModel, out mssg))
               return new DateTime();
           return processPointNewestDateTimeGetModel.model.dt;

       }
    }
}
