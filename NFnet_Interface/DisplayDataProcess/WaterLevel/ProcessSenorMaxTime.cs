﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessSenorMaxTime
    {
        public ProcessDTUSenorCom senorCom = new ProcessDTUSenorCom();
        public DateTime SenorMaxTime(int xmno, Role role, bool tmpRole, out string mssg)
        {
            var senorMaxTimeCondition = new SenorMaxTimeCondition(xmno, data.Model.gtsensortype._waterlevel);
            var processSenorMaxTimeModel = new ProcessDTUSenorCom.ProcessSenorMaxTimeModel(senorMaxTimeCondition, role, tmpRole);
            if (senorCom.ProcessSenorMaxTime(processSenorMaxTimeModel, out mssg))
            {
                return processSenorMaxTimeModel.model.dt;
            }
            return Convert.ToDateTime("1970-01-01");
        } 
        
    }
}
