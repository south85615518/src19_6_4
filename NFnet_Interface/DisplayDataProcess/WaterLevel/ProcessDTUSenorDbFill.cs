﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
   public class ProcessDTUSenorDbFill
    {
       public ProcessDTUSenorCom senorCom = new ProcessDTUSenorCom();
       public FillInclinometerDbFillCondition DTUSenorDbFill(string pointnamestr, string rqConditionStr, int xmno, Role role, bool tmpRole)
       {

           var model = new FillInclinometerDbFillCondition(pointnamestr,rqConditionStr,xmno);

           var ProcessDTUSenorDbFillModel = new ProcessDTUSenorCom.ProcessDTUSenorDbFillModel(model, role, tmpRole);

           if (senorCom.ProcessDTUSenorDbFill(ProcessDTUSenorDbFillModel))
           {
               return ProcessDTUSenorDbFillModel.model;
           }

           return null;
       }
    }
}
