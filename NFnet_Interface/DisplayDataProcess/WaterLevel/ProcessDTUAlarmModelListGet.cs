﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUAlarmModelListGet
    {
        public ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();
        public List<DTU.Model.dtudatareport> DTUAlarmModelListGet(int xmno,out string mssg)
        {
            var processDTUAlarmModelListGetModel = new ProcessDTUDATABLL.ProcessDTUAlarmModelListGetModel(xmno);
            if (processDTUDATABLL.ProcessDTUAlarmModelListGet(processDTUAlarmModelListGetModel, out mssg))
            {
                return processDTUAlarmModelListGetModel.model;
            }
            return new List<DTU.Model.dtudatareport>();
        }
    }
}
