﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUSenorTableCount
    {
        public ProcessDTUSenorBLL processDTUSenorBLL = new ProcessDTUSenorBLL();
        public int DTUSenorTableCount(int xmno,string searstring,out string mssg)
        {
            var processDTUSenorTableCountModel = new ProcessDTUSenorBLL.ProcessDTUSenorTableCountModel(xmno,searstring);
            if (processDTUSenorBLL.ProcessDTUSenorTableCount(processDTUSenorTableCountModel, out mssg))
            {
                return processDTUSenorTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
