﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.ProjectInfo;
using System.IO;
using Tool;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUDATAJson
    {
        public ProcessDTUDATAJsonBLL processDTUDATAJsonBLL = new ProcessDTUDATAJsonBLL();
        public ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public string tmpdatapath =  System.AppDomain.CurrentDomain.BaseDirectory + "\\"+"DTU数据\\";
        string ftppath = "";
        public void DTUDATAJsonSend(int xmno, List<DTU.Model.dtudata> dtudatalist,out string mssg)
        {
            var xmmodel = new ProcessXmBLL.ProcessXmIntInfoLoadModel(xmno);
            if (!processXmBLL.ProcessXmInfoLoad(xmmodel, out mssg)) return ;
            List<string> jsondata = new List<string>();
            int i = 0;
            foreach (var dtudata in dtudatalist)
            {
                string jsondatastr = string.Format("{0}|{1}|{2}|{3}|{4}", ++i, dtudata.point_name, dtudata.time, dtudata.deep, dtudata.groundElevation);
                jsondata.Add(jsondatastr);

            }
            List<string> json = new List<string>();
            string path = "";
            processDTUDATAJsonBLL.ProcessDTUDATAJsonHead(xmmodel.model,jsondata,out json,out path);
            tmpdatapath += path+".txt";
            ExceptionLog.ExceptionWrite(tmpdatapath);
            Tool.FileHelper.FileInfoListWrite(json,tmpdatapath);

            List<string> ftpinfo =  Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\"+"Setting\\ftpsetting.txt");
            Tool.FTP.ftpHelper ftphelper = new  Tool.FTP.ftpHelper{ ftpServerIP = ftpinfo[0], ftpUserID = ftpinfo[1], ftpPassword = ftpinfo[2] };
            if (!ftphelper.UpLoadFile(tmpdatapath, ftpinfo[3])) return ;
            string httpurl = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\SZDTHttpsetting.txt");
            httpurl = string.Format(httpurl,  Path.GetFileName(tmpdatapath));
            //Console.WriteLine(httpurl);
            Tool.http.HttpHelper httphelper = new Tool.http.HttpHelper();
            mssg = httphelper.SendMessageToWeb(httpurl);



        }

        public List<DTUJsonData> DTUDATASerializeJson(int xmno, List<DTU.Model.dtudata> dtudatalist, out string mssg)
        {
            mssg = "";
            List<DTUJsonData> dTUJsonDataList = new List<DTUJsonData>();
            var xmmodel = new ProcessXmBLL.ProcessXmIntInfoLoadModel(xmno);
            if (!processXmBLL.ProcessXmInfoLoad(xmmodel, out mssg)) return null;
            int i = 0;
            foreach (var dtudata in dtudatalist)
            {
                //string jsondatastr = string.Format("{0}|{1}|{2}|{3}|{4}", ++i, dtudata.point_name, dtudata.time, dtudata.deep, dtudata.groundElevation);
                //jsondata.Add(jsondatastr);
                DTUJsonData jsondata = new DTUJsonData { itemInfo = xmmodel.model.xmname, proInfo = xmmodel.model.xmaddress, pointNo = dtudata.point_name, monitorDate = dtudata.time.ToString(), monitorType = 0, initVal = dtudata.groundElevation, thisVal = dtudata.deep   };
                dTUJsonDataList.Add(jsondata);
            }
            return dTUJsonDataList;



        }



        public class DTUJsonData
        {
            public string itemInfo { get;set;}
            public string proInfo { get; set; }
            public int monitorType { get; set; }
            public string pointNo { get; set; }
            public string monitorDate { get; set; }
            public double initVal { get; set; }
            public double thisVal { get; set; }
        }
        


    }
}
