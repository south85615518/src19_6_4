﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPortXmname
    {
        public ProcessDTUXmPortBLL processDTUXmPortBLL = new ProcessDTUXmPortBLL();
        public string DTUPortXm(int port, out string mssg)
        {
            var processDTUPortXmnameBLLModel = new ProcessDTUXmPortBLL.ProcessDTUPortXmnameBLLModel(port);
            if (processDTUXmPortBLL.ProcessDTUPortXmBLL(processDTUPortXmnameBLLModel, out mssg))
            {
                return processDTUPortXmnameBLLModel.xmname;
            }
            return "";
        }
    }
}
