﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUModuleNoGet
    {
        public ProcessDTUModuleBLL processDTUModuleBLL = new ProcessDTUModuleBLL();
        public string DTUModuleNoGet(int xmno,out string mssg)
        {
            var processDTUModuleNoGetModel = new ProcessDTUModuleBLL.ProcessDTUModuleNoGetModel(xmno);
            if (processDTUModuleBLL.ProcessDTUModuleNoGet(processDTUModuleNoGetModel, out mssg))
            {
                return processDTUModuleNoGetModel.modulenoStr;
            }
            return "";
        }
    }
}
