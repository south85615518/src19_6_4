﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUQueueLoad
    {
        public ProcessDTUTaskQueueBLL processDTUTaskQueueBLL = new ProcessDTUTaskQueueBLL();
        public List<int> DTUQueueLoad(int xmno, string pointname, out string mssg)
        {
            var processDTUTaskQueueLoadModel = new ProcessDTUTaskQueueBLL.ProcessDTUTaskQueueLoadModel(xmno, pointname);
            if (processDTUTaskQueueBLL.ProcessDTUTaskQueueLoad(processDTUTaskQueueLoadModel, out mssg))
            {
                return (from m in processDTUTaskQueueLoadModel.commendList select Convert.ToInt32(m)).ToList();

            }
            return new List<int>();
        }
    }
}
