﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTaskQueueFinishedSet
    {
        public ProcessDTUTaskQueueBLL taskQueueBLL = new ProcessDTUTaskQueueBLL();
        public bool DTUTaskQueueFinishedSet(int xmno,string pointname,int commendIndex,out string mssg)
        {
            var processDTUTaskQueueLoadModel = new ProcessDTUTaskQueueBLL.ProcessDTUTaskQueueFinishedSetModel(xmno,pointname,commendIndex);
            return taskQueueBLL.ProcessDTUTaskQueueFinishedSet(processDTUTaskQueueLoadModel,out mssg);
        }
    }
}
