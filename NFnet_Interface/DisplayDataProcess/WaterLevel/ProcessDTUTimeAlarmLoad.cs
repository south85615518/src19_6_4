﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimeAlarmLoad
    {
        public ProcessDTUTimeAlarmBLL processDTUTimeAlarmBLL = new ProcessDTUTimeAlarmBLL();
        public DTU.Model.dtutimealarm DTUTimeAlarmLoad(int xmno,string module,out string mssg)
        {
            var processDTUTimeAlarmLoadModel = new ProcessDTUTimeAlarmBLL.ProcessDTUTimeAlarmLoadModel(xmno, module);
            if (processDTUTimeAlarmBLL.ProcessDTUTimeAlarmLoad(processDTUTimeAlarmLoadModel, out mssg))
            {
                return processDTUTimeAlarmLoadModel.model;
            }
            return new DTU.Model.dtutimealarm();
        }
    }
}
