﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUModuleLPTableCount
    {
        public ProcessDTUModuleLPBLL processDTUModuleLPBLL = new ProcessDTUModuleLPBLL();
        public int DTUModuleTableCount(int xmno, string searstring, out string mssg)
        {
            var processDTUModuleLPTableCountModel = new ProcessDTUModuleLPBLL.ProcessDTUModuleLPTableCountModel(xmno, searstring);
            if (processDTUModuleLPBLL.ProcessDTUModuleLPTableCount(processDTUModuleLPTableCountModel, out mssg))
            {
                return processDTUModuleLPTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
