﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessSenorPointNameTimesListLoad
    {
        public ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();
        public List<string> SenorPointNameTimesListLoad(int xmno, string pointname, Role role, bool tmpRole, out string mssg)
        {
            var pointNameCycListCondition = new SenorPointNameCycListCondition(xmno, pointname);
            if (processDTUDATABLL.ProcessSenorPointNameTimesListLoad(pointNameCycListCondition, out mssg))
                return pointNameCycListCondition.ls;
            return new List<string>();
        }
    }
}
