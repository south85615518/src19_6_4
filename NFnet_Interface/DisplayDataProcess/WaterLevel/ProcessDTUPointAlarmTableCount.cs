﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointAlarmTableCount
    {
        public ProcessDTUPointAlarmBLL processDTUPointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public int DTUPointAlarmTableCount(int xmno,string searchstring,out string mssg)
        {
            var processDTUPointAlarmTableCountModel = new ProcessDTUPointAlarmBLL.ProcessDTUPointAlarmTableCountModel(xmno,searchstring);
            if (processDTUPointAlarmBLL.ProcessDTUPointAlarmTableCount(processDTUPointAlarmTableCountModel, out mssg))
            {
                return processDTUPointAlarmTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
