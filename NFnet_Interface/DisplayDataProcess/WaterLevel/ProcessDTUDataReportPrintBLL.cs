﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.Data;
using NFnet_BLL.DataProcess;
using System.IO;
using NFnet_Interface.DisplayDataProcess.WaterLevel;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{

    /// <summary>
    /// 全站仪数据报表打印业务逻辑处理类
    /// </summary>
    public class ProcessDTUDataReportPrintBLL
    {
        public static SenorReportHelper reportHelper = new SenorReportHelper();
        public static ProcessDTUDataAlarmBLL dtuDataAlarmBLL = new ProcessDTUDataAlarmBLL();

        public static ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public static DateTime timefoot = new DateTime();
        //public static ProcessSenorDataReportPrintBLL totalStationReportPrintBLL = new ProcessSenorDataReportPrintBLL();
        /// <summary>
        /// 单点多周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessSenorDataSPMTReport(ProcessSenorDataSPMCReportModel model, out string mssg)
        {

          
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                DTUReportHelper dtuReportHelper = new DTUReportHelper();
                dtuReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                
                dtuReportHelper.Main(
                    model.senorReport,
                    model.xmname,
                    "时间,位移,标值",
                    model.dt,
                    model.exportpath
                    );
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("DTU报表输出出错，错误信息:" + ex.Message);
                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
            return false;
        }

       







        public class ProcessSenorDataSPMCReportModel
        {
            public Tool.SenorReport senorReport { get; set; }
            public string xlspath { get; set; }
            public DataTable dt { get; set; }
            public string exportpath { get; set; }
            public int xmno { get; set; }
            public string xmname { get; set; }
            public ProcessSenorDataSPMCReportModel(Tool.SenorReport senorReport, int xmno, string xmname, string xlspath, DataTable dt, string exportpath)
            {
                this.senorReport = senorReport;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
                this.xmno = xmno;
                this.xmname = xmname;
            }

        }





        /// <summary>
        /// 全站仪数据表打印类
        /// </summary>
        public class ProcessSenorDataReportModel : PrintCondition
        {
            public List<ChartCreateEnviroment> cce { get; set; }
            public ProcessSenorDataReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }

        public class ProcessSenorDataMPMCReportModel : PrintCondition
        {
            public ChartCreateEnviroment cce { get; set; }
            public List<string> pnames { get; set; }
            public ProcessSenorDataMPMCReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }
#region GT
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessWaterLevelSPMTReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                //var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                //ExceptionLog.ExceptionWrite("开始填充数据");
                ////model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}


                totalStationReportHelper.MainWaterLevel_GT_WithoutSettlement(
                    model.xmname,
                    "时间,本次(mm),累计(mm)",
                    model.dt,
                    model.exportpath
                    );
                mssg = string.Format(DateTime.Now + " {0}水位{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("水位单点报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }





        public class ProcessTotalStationSPMCReportModel
        {
            public string stCyc { get; set; }
            public string etCyc { get; set; }
            public string xmname { get; set; }
            public string xlspath { get; set; }
            public DataTable dt { get; set; }
            public string timeunitname { get; set; }
            public string exportpath { get; set; }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
            }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath, string timeunitname)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
                this.timeunitname = timeunitname;
            }
        }
#endregion


    }
}