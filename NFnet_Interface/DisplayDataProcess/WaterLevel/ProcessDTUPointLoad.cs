﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    
    public class ProcessDTUPointLoad
    {
        public ProcessDTUPointBLL dtuBLL = new ProcessDTUPointBLL();
        public List<string> DTUSenorPointLoad(int xmno,out string mssg)
        {

            var model = new ProcessDTUPointBLL.ProcessDTUPointNameLoadModel(xmno);
            if (dtuBLL.ProcessDTUPointNameLoad(model,out mssg))
            {
                return model.ls;
            }
            return new List<string>();
        }
    }
}
