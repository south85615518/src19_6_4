﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimeIntervalDelete
    {
        public ProcessDTUTimeIntervalBLL processDTUTimeIntervalBLL = new ProcessDTUTimeIntervalBLL();
        public bool DTUTimeIntervalDelete(int xmno, string module, out string mssg)
        {
            var processDTUTimeIntervalDeleteModel = new ProcessDTUTimeIntervalBLL.ProcessDTUTimeIntervalDeleteModel(xmno, module);
            return processDTUTimeIntervalBLL.ProcessDTUTimeIntervalDelete(processDTUTimeIntervalDeleteModel, out mssg);
        }
    }
}
