﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessSenorDataLoad
    {
        public ProcessDTUSenorCom senorCom = new ProcessDTUSenorCom();
        public DataTable SenorDataLoad(int xmno, string pointname, DateTime starttime, DateTime endtime,  int startPageIndex, int pageSize, string sord ,Role role ,bool tmpRole,out string mssg)
        {
            var senorDataLoadCondition = new SenorDataLoadCondition(xmno, pointname, starttime, endtime, startPageIndex, pageSize, sord);
            var senordataTableLoadModel = new ProcessDTUSenorCom.SenordataTableLoadModel(senorDataLoadCondition, tmpRole, role);
            if (senorCom.SenordataTableLoad(senordataTableLoadModel, out mssg))
            {
                return senordataTableLoadModel.model.dt;
            }
            return new DataTable();
        }
    }
}
