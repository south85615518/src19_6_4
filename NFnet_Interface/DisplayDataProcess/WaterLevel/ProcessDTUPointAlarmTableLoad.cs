﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointAlarmTableLoad
    {
        public ProcessDTUPointAlarmBLL processDTUPointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public DataTable DTUPointAlarm(int xmno,string colName,int pageIndex,int rows,string sord,out string mssg)
        {
            var processDTUPointAlarmTableLoadModel = new ProcessDTUPointAlarmBLL.ProcessDTUPointAlarmTableLoadModel(xmno,colName,pageIndex,rows,sord);
            if (processDTUPointAlarmBLL.ProcessDTUPointAlarmTableLoad(processDTUPointAlarmTableLoadModel, out mssg))
            {
                return processDTUPointAlarmTableLoadModel.dt;
            }
            return new DataTable();
        }
    }
}
