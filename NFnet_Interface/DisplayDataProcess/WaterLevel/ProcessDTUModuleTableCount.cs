﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUModuleTableCount
    {
        public ProcessDTUModuleBLL processDTUModuleBLL = new ProcessDTUModuleBLL();
        public int DTUModuleTableCount(int xmno,string searstring,out string mssg)
        {
            var processDTUModuleTableCountModel = new ProcessDTUModuleBLL.ProcessDTUModuleTableCountModel(xmno,searstring);
            if (processDTUModuleBLL.ProcessDTUModuleTableCount(processDTUModuleTableCountModel, out mssg))
            {
                return processDTUModuleTableCountModel.totalCont;
            }
            return 0;
        }
    }
}
