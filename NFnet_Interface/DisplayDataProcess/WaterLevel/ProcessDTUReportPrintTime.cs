﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUReportPrintTime
    {
        public string DTUReportPrintTime(int rows)
        {
            double pagecont = Math.Ceiling((double)(rows / 39));
            double sec = pagecont * (pagecont + 1) * 3.5 + 20;
            return sec.ToString();



        }
    }
}
