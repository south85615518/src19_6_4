﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointAlarmModel
    {
        public ProcessDTUPointAlarmBLL processDTUPointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public DTU.Model.dtu DTUPointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processDTUPointAlarmModel = new ProcessDTUPointAlarmBLL.ProcessDTUPointAlarmModel(xmno,pointname);
            if (processDTUPointAlarmBLL.ProcessDTUPointAlarmModelGet(processDTUPointAlarmModel, out mssg))
            {
                return processDTUPointAlarmModel.model;
            }
            return new DTU.Model.dtu();
        }
    }
}
