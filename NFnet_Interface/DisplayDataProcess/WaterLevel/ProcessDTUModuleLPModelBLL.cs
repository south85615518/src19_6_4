﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUModuleLPModelBLL
    {
        public ProcessDTUModuleLPBLL processDTUModuleLPBLL = new ProcessDTUModuleLPBLL();
        public DTU.Model.dtulp DTUModuleLPModelBLL(int xmno,string module,out string mssg)
        {
            var processDTUModuleLPModelBLLModel = new ProcessDTUModuleLPBLL.ProcessDTUModuleLPModelBLLModel(xmno,module);
            if (processDTUModuleLPBLL.ProcessDTUModuleLPModelBLL(processDTUModuleLPModelBLLModel, out mssg))
            {
                return processDTUModuleLPModelBLLModel.model;
            }
            return new DTU.Model.dtulp();
        }
    }
}
