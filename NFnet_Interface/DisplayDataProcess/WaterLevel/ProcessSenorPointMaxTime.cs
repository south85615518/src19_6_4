﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessSenorPointMaxTime
    {
        public ProcessDTUSenorCom senorCom = new ProcessDTUSenorCom();
        public DateTime SenorMaxTime(int xmno, string point_name,Role role, bool tmpRole, out string mssg)
        {
            var senorMaxTimeCondition = new ProcessDTUDATABLL.SenorPointMaxTimeModel(xmno, point_name);
            var processSenorMaxTimeModel = new ProcessDTUSenorCom.ProcessSenorPointMaxTimeModel(senorMaxTimeCondition, role, tmpRole);
            if (senorCom.ProcessSenorMaxTime(processSenorMaxTimeModel, out mssg))
            {
                return processSenorMaxTimeModel.model.dt;
            }
            return DateTime.Now.AddDays(-0.5);
        } 
        
    }
}
