﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.LoginProcess;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUDATAModelGet
    {
        public ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();
        public DTU.Model.dtudatareport DTUDATAModelGet(int xmno,string pointname,DateTime dt,Role role,bool tmpRole ,out string mssg)
        {
            var processDTUDATAModelGetModel = new ProcessDTUDATABLL.ProcessDTUDATAModelGetModel(xmno,pointname,dt);
            if (processDTUDATABLL.ProcessDTUDATAModelGet(processDTUDATAModelGetModel, out mssg))
            {
                return processDTUDATAModelGetModel.model;
            }
            return new DTU.Model.dtudatareport();
        }
    }
}
