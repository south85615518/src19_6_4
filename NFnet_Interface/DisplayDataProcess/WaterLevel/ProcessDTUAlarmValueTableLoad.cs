﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUAlarmValueTableLoad
    {
        public ProcessDTUAlarmValueBLL processDTUAlarmValueBLL = new ProcessDTUAlarmValueBLL();
        public DataTable DTUAlarmValue(int xmno,string colName,int pageIndex,int rows,string sord,out string mssg)
        {
            var processDTUAlarmValueTableLoadModel = new ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueTableLoadModel(xmno,colName,pageIndex,rows,sord);
            if (processDTUAlarmValueBLL.ProcessDTUAlarmValueTableLoad(processDTUAlarmValueTableLoadModel, out mssg))
            {
                return processDTUAlarmValueTableLoadModel.dt;
            }
            return new DataTable();
        }
    }
}
