﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUSenorLoad
    {
        public ProcessDTUSenorBLL processDTUSenorBLL = new ProcessDTUSenorBLL();
        public DataTable DTUSenorLoad(int pageIndex,int rows, int xmno, string colName, string sord, out string mssg)
        {
            var processDTUSenorLoadBLLModel = new ProcessDTUSenorBLL.ProcessDTUSenorLoadBLLModel(xmno,colName,pageIndex,rows,sord);
            if (processDTUSenorBLL.ProcessDTUSenorLoadBLL(processDTUSenorLoadBLLModel, out mssg))
            {
                return processDTUSenorLoadBLLModel.dt;
            }
            return new DataTable();
        }
    }
}
