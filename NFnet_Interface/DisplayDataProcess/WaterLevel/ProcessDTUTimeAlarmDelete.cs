﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimeAlarmDelete
    {
        public ProcessDTUTimeAlarmBLL processDTUTimeAlarmBLL = new ProcessDTUTimeAlarmBLL();
        public bool DTUTimeAlarmDelete(int xmno, string module, out string mssg)
        {
            var processDTUTimeAlarmDeleteModel = new ProcessDTUTimeAlarmBLL.ProcessDTUTimeAlarmDeleteModel(xmno, module);
            return processDTUTimeAlarmBLL.ProcessDTUTimeAlarmDelete(processDTUTimeAlarmDeleteModel, out mssg);
        }
    }
}
