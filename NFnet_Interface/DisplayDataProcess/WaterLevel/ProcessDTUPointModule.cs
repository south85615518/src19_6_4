﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointModule
    {
        public ProcessDTUPointBLL processDTUPointBLL = new ProcessDTUPointBLL();
        public DTU.Model.dtu DTUPointModule(int xmno, int port,string od ,string addressno ,out string mssg)
        {
            var processDTUPointModuleModel = new NFnet_BLL.DisplayDataProcess.WaterLevel.ProcessDTUPointBLL.ProcessDTUPointModuleModel(xmno, port, od, addressno);
            if (processDTUPointBLL.ProcessDTUPointModule(processDTUPointModuleModel, out mssg))
            {
                return processDTUPointModuleModel.model;
            }
            return new DTU.Model.dtu();
        }
    }
}
