﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessSenorDataReordCount
    {
        public ProcessDTUSenorCom senorCom = new ProcessDTUSenorCom();
        public int SenorDataReordCount(int xmno, string pointname, DateTime starttime, DateTime endtime, Role role, bool tmpRole, out string mssg)
        {
            var senorDataCountLoadCondition = new SenorDataCountLoadCondition(xmno, pointname, starttime, endtime);
            var processSenorDataRecordsCountModel = new ProcessDTUSenorCom.ProcessSenorDataRecordsCountModel(senorDataCountLoadCondition, role, tmpRole);
            if (senorCom.ProcessSenorDataRecordsCount(processSenorDataRecordsCountModel, out mssg))
            {
                return int.Parse(processSenorDataRecordsCountModel.model.totalCont);
            }
            return 0;



        }
    }
}
