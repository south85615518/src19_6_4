﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUAlarmNameStr
    {
        public ProcessDTUAlarmValueBLL processDTUAlarmValueBLL = new ProcessDTUAlarmValueBLL();
        public string DTUAlarmNameStr(int xmno,out string mssg)
        {
            var processDTUAlarmValueNameGetModel = new ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueNameGetModel(xmno);
            if (processDTUAlarmValueBLL.ProcessDTUAlarmValueNameGet(processDTUAlarmValueNameGetModel, out mssg))
            {
                return processDTUAlarmValueNameGetModel.alarmNameStr;
            }
            return "";
        }
    }
}
