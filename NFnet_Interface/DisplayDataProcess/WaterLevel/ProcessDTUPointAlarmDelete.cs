﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointAlarmDelete
    {
        public ProcessDTUPointAlarmBLL processDTUPointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public bool DTUPointAlarmDelete(int xmno,string pointname,string module,out string mssg)
        {
            var processDTUPointAlarmDeleteModel = new ProcessDTUPointAlarmBLL.ProcessDTUPointAlarmDeleteModel(xmno,pointname,module);
            return processDTUPointAlarmBLL.ProcessDTUPointAlarmDelete(processDTUPointAlarmDeleteModel,out mssg);
        }
    }
}