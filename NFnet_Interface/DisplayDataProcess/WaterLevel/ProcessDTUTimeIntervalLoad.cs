﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimeIntervalLoad
    {
        public ProcessDTUTimeIntervalBLL processDTUTimeIntervalBLL = new ProcessDTUTimeIntervalBLL();
        public DTU.Model.dtutimetask DTUTimeIntervalLoad(int xmno,string pointname,out string mssg)
        {
            var processDTUTimeIntervalLoadModel = new ProcessDTUTimeIntervalBLL.ProcessDTUTimeIntervalLoadModel(xmno,pointname);
            if (processDTUTimeIntervalBLL.ProcessDTUTimeIntervalLoad(processDTUTimeIntervalLoadModel, out mssg))
            {
                return processDTUTimeIntervalLoadModel.model;
            }
            return new DTU.Model.dtutimetask();
        }
    }
}
