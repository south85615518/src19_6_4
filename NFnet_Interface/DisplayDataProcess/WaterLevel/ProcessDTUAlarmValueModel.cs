﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUAlarmValueModel
    {
        public ProcessDTUAlarmValueBLL processDTUAlarmValueBLL = new ProcessDTUAlarmValueBLL();
        public DTU.Model.dtualarmvalue DTUAlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var processDTUAlarmValueModelGetModel = new ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueModelGetModel(xmno, alarmname);
            if (processDTUAlarmValueBLL.ProcessDTUAlarmValueModelGet(processDTUAlarmValueModelGetModel, out mssg))
            {
                return processDTUAlarmValueModelGetModel.model;
            }
            return new DTU.Model.dtualarmvalue();
        }
    }
}
