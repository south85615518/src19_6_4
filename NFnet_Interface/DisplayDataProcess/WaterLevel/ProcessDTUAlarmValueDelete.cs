﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUAlarmValueDelete
    {
        public ProcessDTUAlarmValueBLL processDTUAlarmValueBLL = new ProcessDTUAlarmValueBLL();
        public bool DTUAlarmValueDelete(int xmno,string alarmname,out string mssg)
        {
            var processDTUAlarmValueDeleteModel = new ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueDeleteModel(xmno,alarmname);
            return processDTUAlarmValueBLL.ProcessDTUAlarmValueDelete(processDTUAlarmValueDeleteModel,out mssg);
        }
    }
}