﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUModuleDelete
    {
        public ProcessDTUModuleBLL processDTUModuleBLL = new ProcessDTUModuleBLL();
        public bool DTUModuleDelete(int xmno,string addressno,string od,out string mssg)
        {
            var processDTUModuleDelBLLModel = new ProcessDTUModuleBLL.ProcessDTUModuleDeleteModel(xmno, addressno, od);
            return processDTUModuleBLL.ProcessDTUModuleDelete(processDTUModuleDelBLLModel,out mssg);
        }
        
    }
}
