﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using System.Data;

namespace NFnet_Interface.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUList
    {

        public ProcessDTUPointAlarmBLL processDTUPointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public DataTable DTUList(int xmno, string xmname,string colName, int pageIndex, int rows, string sord, out string mssg)
        {
            var processDTUListModel = new ProcessDTUPointAlarmBLL.ProcessDTUListModel(xmno, colName, pageIndex, rows, sord,xmname);
            if (processDTUPointAlarmBLL.ProcessDTUList(processDTUListModel, out mssg))
            {
                return processDTUListModel.dt;
            }
            return new DataTable();
        }


    }
}
