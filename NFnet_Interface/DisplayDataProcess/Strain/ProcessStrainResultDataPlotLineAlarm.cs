﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.Strain;
using Tool;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainResultDataPlotLineAlarm
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public string StrainResultDataPlotLineAlarm(string pointnamestr, string xmname, int xmno)
        {
            try
            {
                if (string.IsNullOrEmpty(pointnamestr))
                {
                    return jss.Serialize(new List<StrainPlotlineAlarmModel>());
                }
                List<string> pointnamelist = pointnamestr.Split(',').ToList();
                ProcessResultDataAlarmBLL processStrainResultDataAlarmBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
                List<StrainPlotlineAlarmModel> lmm = new List<StrainPlotlineAlarmModel>();
                foreach (string pointname in pointnamelist)
                {
                    var pointvalue = processStrainResultDataAlarmBLL.GetPointAlarmValue(pointname);
                    var alarmList = processStrainResultDataAlarmBLL.GetAlarmValueList(pointvalue);
                    StrainPlotlineAlarmModel model = new StrainPlotlineAlarmModel
                    {
                        pointname = pointname,
                        firstalarm = alarmList[0],
                        secalarm = alarmList[1],
                        thirdalarm = alarmList[2]
                    };
                    lmm.Add(model);
                    break;
                }
                return jss.Serialize(lmm);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移预警参数加载出错,错误信息:" + ex.Message);
                return null;
            }
        }
    }
}
