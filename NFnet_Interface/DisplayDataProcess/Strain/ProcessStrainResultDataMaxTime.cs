﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.Strain;
using NFnet_BLL.DataProcess.Strain.Sensor;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
   public class ProcessStrainResultDataMaxTime
    {

        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public  DateTime StrainResultDataMaxTime(int xmno,data.Model.gtsensortype datatype,out string mssg)
        {
            var processStrainResultDataMaxTime = new NFnet_BLL.DisplayDataProcess.SenorMaxTimeCondition(xmno,datatype);

            if (!resultDataBLL.ProcessResultDataMaxTime(processStrainResultDataMaxTime, out mssg)) return new DateTime();
            return processStrainResultDataMaxTime.dt;
            

        }
    }
}
