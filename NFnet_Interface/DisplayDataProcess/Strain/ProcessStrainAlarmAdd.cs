﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;
//using NFnet_BLL.DisplayDataProcess.Strain;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainAlarmAdd
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public bool AlarmAdd(global::Strain.Model.strainalarmvalue model,out string mssg)
        {
            //var processAlarmAddModel = new ProcessAlarmValueBLL.ProcessAlarmAddModel(model, xmname);
            return alarmBLL.ProcessstrainalarmvalueAdd(model, out mssg);
        }
    }
}
