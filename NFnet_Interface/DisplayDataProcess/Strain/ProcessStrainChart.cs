﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.LoginProcess;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainChart
    {
        public ProcessStrainChartBLL processStrainChartBLL = new ProcessStrainChartBLL();
        public List<serie> SerializestrStrain(object sql, object xmno, object pointname,out string mssg)
        {

            var processChartCondition = new NFnet_BLL.DisplayDataProcess.ProcessChartCondition(sql, xmno,pointname,0);

            if (ProcessStrainChartBLL.ProcessSerializestrStrain(processChartCondition, out mssg))
                return processChartCondition.series;
            return new List<serie>();


        }
    }
}
