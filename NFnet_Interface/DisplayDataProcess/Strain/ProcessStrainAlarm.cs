﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;


namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainAlarm
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool Alarm(global::Strain.Model.strainpointalarmvalue model, out string mssg)
        {
            return pointAlarmBLL.ProcessAlarm(model,out mssg); 
        }
    }
}
