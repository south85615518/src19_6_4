﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;
//using NFnet_BLL.DisplayDataProcess.Strain;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainPointAlarmValueDel
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueDel( global::Strain.Model.strainpointalarmvalue model ,out string mssg)
        {
            
            pointAlarmBLL.ProcessAlarmDel(model, out mssg);
        }
    }
}
