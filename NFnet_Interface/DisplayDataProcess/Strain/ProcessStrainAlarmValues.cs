﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;
//using NFnet_BLL.DisplayDataProcess.Strain;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainAlarmValues
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public string AlarmValues(int xmno, out string mssg)
        {
            var alarmvaluename = new ProcessAlarmValueBLL.ProcessAlarmValueNameModel(xmno, "");
            if (alarmBLL.ProcessAlarmValueName(alarmvaluename, out mssg))
            {
                return alarmvaluename.alarmValueNameStr;
            }
             return mssg;          
        }
    }
}
