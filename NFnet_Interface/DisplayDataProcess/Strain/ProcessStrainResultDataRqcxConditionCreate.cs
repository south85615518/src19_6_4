﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DataProcess.Strain.Sensor;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainResultDataRqcxConditionCreate
    {
        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public ResultDataRqcxConditionCreateCondition StrainResultDataRqcxConditionCreate(string startTime,string endTime,QueryType QT, string range, int xmno, DateTime maxTime)
        {
            List<string> ls = new List<string>();

            var processStrainResultDataRqcxConditionCreateModel = new ResultDataRqcxConditionCreateCondition(startTime,endTime,QT, range, xmno, maxTime);
            resultDataBLL.ProcessResultDataRqcxConditionCreate(processStrainResultDataRqcxConditionCreateModel);
            return processStrainResultDataRqcxConditionCreateModel;
        }
        
        
    }
}
