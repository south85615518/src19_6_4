﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainPointAlarmModel
    {
        public ProcessPointAlarmBLL processStrainPointAlarmBLL = new ProcessPointAlarmBLL();
        public global::Strain.Model.strainpointalarmvalue StrainPointAlarmModel(int xmno,string pointname,out string mssg)
        {
            var processStrainPointAlarmModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointname);
            if (processStrainPointAlarmBLL.ProcessPointAlarmModelGet(processStrainPointAlarmModel, out mssg))
            {
                return processStrainPointAlarmModel.model;
            }
            return new global::Strain.Model.strainpointalarmvalue();
        }
    }
}
