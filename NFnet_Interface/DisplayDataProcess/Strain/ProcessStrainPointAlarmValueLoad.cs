﻿
using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Strain;
//using NFnet_BLL.DisplayDataProcess.Strain;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainPointAlarmValueLoad
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public DataTable PointAlarmValueLoad(int xmno,string searchstring,string colName,int pageIndex,int rows,string sord,out string mssg)
        {
            var processPointAlarmLoadModel = new ProcessPointAlarmBLL.ProcessAlarmLoadModel( xmno, searchstring, colName, pageIndex, rows, sord);
            if (pointAlarmBLL.ProcessPointLoad(processPointAlarmLoadModel, out mssg))
            {
                return processPointAlarmLoadModel.dt;
            }
            else
            {
                //加载结果数据出错错误信息反馈
                return new DataTable();
            }
        }
    }
}
