﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Strain;
using NFnet_BLL.DataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainPointLoad
    {
        public ProcessResultDataBLL processStrainBLL = new ProcessResultDataBLL();
        public List<string> StrainPointLoad(int xmno,out string mssg)
        {
            var processStrainPointLoadModel = new SensorPointLoadCondition(xmno,data.Model.gtsensortype._strain);
            if (processStrainBLL.ProcessStrainPointLoad(processStrainPointLoadModel, out mssg))
            {
                return processStrainPointLoadModel.ls;
            }
            return new List<string>();
        }
    }
}
