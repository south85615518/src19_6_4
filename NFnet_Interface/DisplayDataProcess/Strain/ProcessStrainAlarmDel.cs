﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;
//using NFnet_BLL.DisplayDataProcess.Strain;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainAlarmDel
    {
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public bool AlarmDel(int xmno, string name,out string mssg)
        {
            var processAlarmDelModel = new ProcessAlarmValueBLL.ProcessstrainalarmvalueDeleteModel( xmno,name);
            return alarmBLL.ProcessstrainalarmvalueDelete(processAlarmDelModel, out mssg);
        }
    }
}
