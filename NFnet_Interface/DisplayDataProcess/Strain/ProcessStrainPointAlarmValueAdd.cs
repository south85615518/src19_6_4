﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;
//using NFnet_BLL.DisplayDataProcess.Strain;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainPointAlarmValueAdd
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public bool PointAlarmValueAdd(global::Strain.Model.strainpointalarmvalue model,out string mssg)
        {
             return pointAlarmBLL.ProcessAlarmAdd(model,out mssg);
        }
    }
}
