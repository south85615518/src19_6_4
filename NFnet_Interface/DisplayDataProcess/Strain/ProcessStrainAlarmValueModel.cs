﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;
//using NFnet_BLL.DisplayDataProcess.StrainBKG;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainAlarmValueModel
    {
        public ProcessAlarmValueBLL processStrainAlarmValueBLL = new ProcessAlarmValueBLL();
        public global::Strain.Model.strainalarmvalue StrainAlarmValueModel(string alarmname,int xmno,out string mssg)
        {
            var processStrainAlarmValueModelGetModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, alarmname);
            if (processStrainAlarmValueBLL.ProcessAlarmModelGetByName(processStrainAlarmValueModelGetModel, out mssg))
            {
                return processStrainAlarmValueModelGetModel.model;
            }
            return new global::Strain.Model.strainalarmvalue();
        }
    }
}
