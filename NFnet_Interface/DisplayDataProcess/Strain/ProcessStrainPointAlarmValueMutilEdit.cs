﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DisplayDataProcess.Strain;

namespace NFnet_Interface.DisplayDataProcess.Strain
{
    public class ProcessStrainPointAlarmValueMutilEdit
    {
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public void PointAlarmValueMutilEdit(global::Strain.Model.strainpointalarmvalue model,string pointstr ,out string mssg)
        {
            var processAlarmMultiUpdateModel = new ProcessPointAlarmBLL.ProcessAlarmMultiUpdateModel(pointstr, model);
            pointAlarmBLL.ProcessAlarmMultiUpdate(processAlarmMultiUpdateModel, out mssg);
        }
    }
}
