﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using System.IO;
using System.Data.Odbc;
using System.Data;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Collections;
using NPOI.SS.Util;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Record.Chart;
using NPOI.HSSF.Record;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Web.SessionState;
using NFnet_DAL;
using NFnet_MODAL;


namespace NFnet_BLL
{
    public class templat :IReadOnlySessionState
    {
        public static List<string> mapXm = new List<string>();
        public static database db = new database();
        public void setval(string sql, string mkh, string tppath, string ldpath)
        {
            InitializeWorkbook(tppath);
            DataTable dt = gettb(sql);
            DataView dv = new DataView(dt);
            ISheet sheet1 = hssfworkbook.GetSheet("Sheet1");
            //create cell on rows, since rows do already exist,it's not necessary to create rows again.
            int i = 3;
            foreach (DataRowView drv in dv)
            {

                IRow row = sheet1.CreateRow(i);
                row.CreateCell(2).SetCellValue(drv["sjbh"].ToString());
                row.CreateCell(3).SetCellValue(drv["mkh"].ToString());
                row.CreateCell(4).SetCellValue(drv["czlx"].ToString());
                row.CreateCell(5).SetCellValue(drv["gcz"].ToString());
                row.CreateCell(6).SetCellValue(drv["sj"].ToString());
                i++;
            }
            ISheet sheet4 = hssfworkbook.CreateSheet("Sheet4");
            HSSFSheet sheetclone = (HSSFSheet)sheet1;
            sheetclone.CloneSheet(hssfworkbook);
            hssfworkbook.RemoveSheetAt(3);
            getseriesdt(dt, mkh, tppath, ldpath);
            //Force excel to recalculate all the formula while open
            sheet1.ForceFormulaRecalculation = true;

            WriteToFile(ldpath);
        }
        //日降雨量报表
        public void dayrainval(string sql, string tppath, string ldpath)
        {
            //InitializeWorkbook(tppath);
            DataTable dt = raincross(sql);
            DataView dv = new DataView(dt);
            ISheet sheet1 = hssfworkbook.GetSheet("Sheet2");
            dv.Sort = "RID";
            //create cell on rows, since rows do already exist,it's not necessary to create rows again.
            int i = 3;
            string exdate = "";
            int sumrain = 0;
            IRow rowz = sheet1.CreateRow(i);
            rowz.CreateCell(2).SetCellValue("点号" + "(2)");
            rowz.CreateCell(3).SetCellValue("日降雨量");
            rowz.CreateCell(4).SetCellValue("地址");
            rowz.CreateCell(5).SetCellValue("初始值");
            rowz.CreateCell(6).SetCellValue("时间");
            i++;

            foreach (DataRowView drv in dv)
            {
                DateTime datetime = Convert.ToDateTime(drv["MonitorTime"].ToString());
                string datenow = datetime.ToShortDateString().ToString();
                if (exdate == "")
                {
                    exdate = datenow;
                }
                if (exdate != datenow)
                {
                    IRow row = sheet1.CreateRow(i);
                    row.CreateCell(2).SetCellValue(drv["RID"].ToString());
                    row.CreateCell(3).SetCellValue(sumrain);
                    row.CreateCell(4).SetCellValue(drv["Addr"].ToString());
                    row.CreateCell(5).SetCellValue(drv["initRainFall"].ToString());
                    row.CreateCell(6).SetCellValue(exdate);
                    sumrain = 0;//日降雨量置为
                    exdate = datenow;
                    i++;
                }
                //计算机降雨量
                sumrain += Convert.ToInt16(drv["Rainfall"].ToString());


            }
            ISheet sheet4 = hssfworkbook.CreateSheet("Sheet4");
            HSSFSheet sheetclone = (HSSFSheet)sheet1;
            sheetclone.CloneSheet(hssfworkbook);
            hssfworkbook.RemoveSheetAt(3);
            //getseriesdt(dt, mkh, tppath, ldpath);
            //Force excel to recalculate all the formula while open
            sheet1.ForceFormulaRecalculation = true;
            //WriteToFile(ldpath);
        }
        //雨量报表
        public void setrainval(string sql, string tppath, string ldpath)
        {
            InitializeWorkbook(tppath);
            DataTable dt = raincross(sql);
            DataView dv = new DataView(dt);
            ISheet sheet1 = hssfworkbook.GetSheet("Sheet1");
            dv.Sort = "RID";
            //create cell on rows, since rows do already exist,it's not necessary to create rows again.
            int i = 3;
            string exname = "";
            foreach (DataRowView drv in dv)
            {

                if (exname != "" && exname != drv["RID"].ToString())
                {

                    IRow rowz = sheet1.CreateRow(i);
                    rowz.CreateCell(2).SetCellValue("点号" + "(" + drv["RID"].ToString() + ")");
                    rowz.CreateCell(3).SetCellValue("降雨量");
                    rowz.CreateCell(4).SetCellValue("地址");
                    rowz.CreateCell(6).SetCellValue("初始值");
                    rowz.CreateCell(7).SetCellValue("时间");
                    i++;
                }


                IRow row = sheet1.CreateRow(i);
                exname = drv["RID"].ToString();
                row.CreateCell(3).SetCellValue(drv["Rainfall"].ToString());
                row.CreateCell(4).SetCellValue(drv["Addr"].ToString());
                row.CreateCell(5).SetCellValue(drv["initRainFall"].ToString());
                row.CreateCell(6).SetCellValue(drv["MonitorTime"].ToString());
                i++;

            }
            ISheet sheet4 = hssfworkbook.CreateSheet("Sheet4");
            HSSFSheet sheetclone = (HSSFSheet)sheet1;
            sheetclone.CloneSheet(hssfworkbook);
            hssfworkbook.RemoveSheetAt(3);
            //getseriesdt(dt, mkh, tppath, ldpath);
            //Force excel to recalculate all the formula while open
            sheet1.ForceFormulaRecalculation = true;
            dayrainval(sql, tppath, ldpath);
            WriteToFile(ldpath);
        }
        //计算日降雨量
        public int sumday(int sum/*这一天开始的雨量值*/ , int intervel/*该条记录记录时刻的降雨量*/)
        {
            return sum += intervel;
        }
        public void setcxval(string sql, string cxes, string tppath, string ldpath)
        {
            InitializeWorkbook(tppath);
            DataTable dt = cxcross(sql);
            DataView dv = new DataView(dt);
            ISheet sheet1 = hssfworkbook.GetSheet("Sheet1");
            dv.Sort = "Name";
            //create cell on rows, since rows do already exist,it's not necessary to create rows again.
            int i = 3;
            string exname = "";
            foreach (DataRowView drv in dv)
            {




                if (exname != "" && exname != drv["Name"].ToString())
                {

                    IRow rowz = sheet1.CreateRow(i);
                    rowz.CreateCell(2).SetCellValue("组号" + "(" + drv["GID"].ToString() + ")");
                    rowz.CreateCell(3).SetCellValue("设备号");
                    rowz.CreateCell(4).SetCellValue("形变量");
                    rowz.CreateCell(5).SetCellValue("本次形变");
                    rowz.CreateCell(6).SetCellValue("累计形变");
                    rowz.CreateCell(7).SetCellValue("电压");
                    rowz.CreateCell(8).SetCellValue("模数");
                    rowz.CreateCell(9).SetCellValue("时间");
                    i++;
                }


                IRow row = sheet1.CreateRow(i);

                exname = drv["Name"].ToString();
                row.CreateCell(3).SetCellValue(drv["Name"].ToString());
                row.CreateCell(4).SetCellValue(drv["s"].ToString());
                row.CreateCell(5).SetCellValue(drv["bcbh"].ToString());
                row.CreateCell(6).SetCellValue(drv["ljbh"].ToString());
                row.CreateCell(7).SetCellValue(drv["Voltage"].ToString());
                row.CreateCell(8).SetCellValue(drv["moshu"].ToString());
                row.CreateCell(9).SetCellValue(drv["MonitorTime"].ToString());
                i++;

            }
            ISheet sheet4 = hssfworkbook.CreateSheet("Sheet4");
            HSSFSheet sheetclone = (HSSFSheet)sheet1;
            sheetclone.CloneSheet(hssfworkbook);
            hssfworkbook.RemoveSheetAt(3);
            //getseriesdt(dt, mkh, tppath, ldpath);
            //Force excel to recalculate all the formula while open
            sheet1.ForceFormulaRecalculation = true;

            WriteToFile(ldpath);
        }
        public void setgpsval(string sql, string gpses, string tppath, string ldpath)
        {
            InitializeWorkbook(tppath);
            DataTable dt = cross(sql);
            DataView dv = new DataView(dt);
            ISheet sheet1 = hssfworkbook.GetSheet("Sheet1");
            HSSFSheet hs = new HSSFSheet(hssfworkbook);
            ISheet sht = hssfworkbook.CreateSheet("sheetz");
            ISheet sheet2 = hssfworkbook.GetSheet("BY1");
            IEnumerator enu = hssfworkbook.GetEnumerator();
            HSSFChart hcf = null;

            //将BY1中的曲线图在GPS5中创建
            while (enu.MoveNext())
            {
                HSSFSheet hst = (HSSFSheet)(enu.Current);
                if (hst.SheetName == "BY1")
                {
                    HSSFChart[] chart = HSSFChart.GetSheetCharts(hst);
                    hcf = chart[0];
                    //chart[0].CreateBarChart(hssfworkbook,hst);
                    break;
                }
            }
            HSSFSheet hsft = new HSSFSheet(hssfworkbook);
            ISheet sheetf = hssfworkbook.CreateSheet("GPS5");
            //用枚举找到这个hssfsheet
            IEnumerator enu1 = hssfworkbook.GetEnumerator();

            while (enu1.MoveNext())
            {
                HSSFSheet hst = (HSSFSheet)(enu1.Current);
                if (hst.SheetName == "BY1")
                {
                    HSSFChart[] chart = HSSFChart.GetSheetCharts(hst);
                    List<RecordBase> rb = null;
                    //hst.InsertChartRecords(rb);//插入元曲线图的记录？对象序列化？
                    ChartRecord chr = new ChartRecord();
                    chr.Serialize();
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    string rst = jss.Serialize(chart[0]);
                    int jh = chart[0].ChartWidth;
                    byte[] byteArray = System.Text.Encoding.Default.GetBytes(rst);

                    //StreamReader sr = new StreamReader();
                    HSSFChart.HSSFSeries[] hsf = chart[0].Series;
                    SeriesRecord srd = hsf[0].GetSeries();//获得了线的记录
                    string sriesstr = jss.Serialize(srd);
                    //为曲线图创建线
                    sriesstr = jss.Serialize(srd);
                    List<RecordBase> srdl = null;

                    // hst.InsertChartRecords(srdl);
                    // RecordInputStream ris = new RecordInputStream(sr.BaseStream);
                    break;
                }
            }
            //为Chart图关联曲线
            //IName range = hssfworkbook.CreateName();
            HSSFName hsf1 = (HSSFName)hssfworkbook.CreateName();
            SeriesRecord sr = new SeriesRecord();

            HSSFChart.HSSFSeries[] hseries = hcf.Series;

            CellRangeAddressBase crab = new CellRangeAddress(1, 2, 3, 4);

            ISheet shet = hssfworkbook.GetSheet("BY1");
            shet.CreateRow(0).CreateCell(1).SetCellValue(1);
            shet.GetRow(0).CreateCell(2).SetCellValue(2);
            shet.CreateRow(1).CreateCell(1).SetCellValue(3);
            shet.GetRow(1).CreateCell(2).SetCellValue(4);
            shet.CreateRow(2).CreateCell(1).SetCellValue(5);
            shet.GetRow(2).CreateCell(2).SetCellValue(6);
            shet.CreateRow(3).CreateCell(1).SetCellValue(7);
            shet.GetRow(3).CreateCell(2).SetCellValue(8);
            CellRangeAddress cra = CellRangeAddress.ValueOf("BY1!B1:C4");
            CellRangeAddress crag = CellRangeAddress.ValueOf("BY1!B1:C3");
            hseries[0].SetValuesCellRange(cra);
            hseries[0].SetCategoryLabelsCellRange(crag);
            //hseries.SetCategoryLabelsCellRange(cra);
            //hseries[0].SetValuesCellRange(crab);

            //用枚举找到这个hssfsheet
            //create cell on rows, since rows do already exist,it's not necessary to create rows again.
            int i = 3;
            foreach (DataRowView drv in dv)
            {

                IRow row = sheet1.CreateRow(i);
                row.CreateCell(3).SetCellValue(drv["GPSBaseName"].ToString());
                row.CreateCell(4).SetCellValue(drv["WGS84_X"].ToString());
                row.CreateCell(5).SetCellValue(drv["WGS84_Y"].ToString());
                row.CreateCell(6).SetCellValue(drv["WGS84_Z"].ToString());
                row.CreateCell(7).SetCellValue(drv["s_X"].ToString());
                row.CreateCell(8).SetCellValue(drv["s_Y"].ToString());
                row.CreateCell(9).SetCellValue(drv["s_Z"].ToString());
                row.CreateCell(10).SetCellValue(drv["l_X"].ToString());
                row.CreateCell(11).SetCellValue(drv["l_Y"].ToString());
                row.CreateCell(12).SetCellValue(drv["l_Z"].ToString());
                row.CreateCell(13).SetCellValue(drv["ljpy"].ToString());
                row.CreateCell(14).SetCellValue(drv["bcpy"].ToString());
                row.CreateCell(15).SetCellValue(drv["DateTime"].ToString());
                i++;
            }
            //Force excel to recalculate all the formula while open
            getgpsdt(dt, gpses, tppath, ldpath);
            sheet1.ForceFormulaRecalculation = true;
            WriteToFile(ldpath);
        }
        /// <summary>
        /// 根据GPS名字捡出绘图数据(查询条件为空时未处理)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="gpses"></param>
        /// <param name="tppath"></param>
        /// <param name="ldpath"></param>
        public void getgpsdt(DataTable dt, string gpses, string tppath, string ldpath)
        {
            //检出gps自身对比数据根据gps名字获得相应的sheet,取数据sheet的第一列为GPS名最后一列为时间
            string[] gpsarr = gpses.Split(',');
            for (int i = 0; i < gpsarr.Length; i++)
            {

                DataView dv = new DataView(dt);
                dv.RowFilter = "GPSBaseName = '" + gpsarr[i] + "'";
                int j = dv.Count;
                ISheet sheet = hssfworkbook.GetSheet(gpsarr[i].ToUpper());
                //r表示行游标 24~ j
                //c表示列游标 0~9
                int r = 24;
                foreach (DataRowView drv in dv)
                {
                    IRow row = sheet.CreateRow(r);
                    row.CreateCell(3).SetCellValue(drv["GPSBaseName"].ToString());
                    row.CreateCell(4).SetCellValue(Convert.ToDouble(drv["WGS84_X"].ToString()));
                    row.CreateCell(5).SetCellValue(Convert.ToDouble(drv["WGS84_Y"].ToString()));
                    row.CreateCell(6).SetCellValue(Convert.ToDouble(drv["WGS84_Z"].ToString()));
                    row.CreateCell(7).SetCellValue(Convert.ToDouble(drv["s_X"].ToString()));
                    row.CreateCell(8).SetCellValue(Convert.ToDouble(drv["s_Y"].ToString()));
                    row.CreateCell(9).SetCellValue(Convert.ToDouble(drv["s_Z"].ToString()));
                    row.CreateCell(10).SetCellValue(Convert.ToDouble(drv["l_X"].ToString()));
                    row.CreateCell(11).SetCellValue(Convert.ToDouble(drv["l_Y"].ToString()));
                    row.CreateCell(12).SetCellValue(Convert.ToDouble(drv["l_Z"].ToString()));
                    row.CreateCell(13).SetCellValue(Convert.ToDouble(drv["ljpy"].ToString()));
                    row.CreateCell(14).SetCellValue(Convert.ToDouble(drv["bcpy"].ToString()));
                    row.CreateCell(15).SetCellValue(drv["DateTime"].ToString());
                    r++;

                }
                sheet.ForceFormulaRecalculation = true;
            }
            WriteToFile(ldpath);
        }
        /// <summary>
        /// 根据模块号来捡出绘图数据，数据曲线分为水位和温度
        /// </summary>
        /// <param name="dt"></param>
        public void getseriesdt(DataTable dt, string mkhs, string tppath, string ldpath)
        {
            int j = 1;//单元格游标
            int r = 1;//单位行游标
            string[] mkharr = mkhs.Split(',');
            List<string> lssj = new List<string>();
            ISheet sheet3 = hssfworkbook.GetSheet("sheet3");
            ISheet sheet2 = hssfworkbook.GetSheet("sheet2");
            HSSFSheet sheet = (HSSFSheet)hssfworkbook.GetSheet("sheet2");
            sheet2.Workbook.CreateSheet("sheet4");
            //第一行写入日期
            DataView dvsj = new DataView(dt);
            int cont = dvsj.Count;
            IRow rowd = sheet2.CreateRow(0);
            IRow rosw = sheet3.CreateRow(0);
            foreach (DataRowView dwv in dvsj)
            {

                string dtm = dwv["sj"].ToString();
                DateTime dti = Convert.ToDateTime(dtm);
                if (lssj.Contains(dtm))
                {
                    continue;
                }
                rosw.CreateCell(j);
                rosw.CreateCell(j).SetCellValue(dtm);
                rowd.CreateCell(j);
                rowd.CreateCell(j).SetCellValue(dtm);
                lssj.Add(dwv["sj"].ToString());
                j++;
            }
            //水位
            foreach (string i in mkharr)
            {
                IRow row = sheet3.CreateRow(r);
                DataView dv = new DataView(dt);
                dv.RowFilter = " mkh = " + i + " and czlx = '水位'";
                int ij = dv.Count;
                j = 0;
                row.CreateCell(j).SetCellValue(i);
                foreach (DataRowView drv in dv)
                {
                    int cell = lssj.IndexOf(drv["sj"].ToString());
                    //如果该单元格存在
                    if (null == row.GetCell(cell + 1))
                    {

                        row.CreateCell(cell + 1).SetCellValue(Convert.ToDouble(drv["gcz"].ToString()));
                    }
                    else//覆盖单元格并且加上（覆盖）二字?加上次数?验证是否填满
                    {
                        row.GetCell(cell + 1).SetCellValue(Convert.ToDouble(drv["gcz"].ToString()));
                    }
                }
                r++;
            }//温度
            r = 1;
            foreach (string i in mkharr)
            {
                IRow row = sheet2.CreateRow(r);
                DataView dv = new DataView(dt);
                dv.RowFilter = " mkh = " + i + " and czlx = '温度'";
                int ij = dv.Count;
                j = 0;
                row.CreateCell(j).SetCellValue(i);
                foreach (DataRowView drv in dv)
                {
                    int cell = lssj.IndexOf(drv["sj"].ToString());
                    //如果该单元格存在
                    if (null == row.GetCell(cell + 1))
                    {

                        row.CreateCell(cell + 1).SetCellValue(Convert.ToDouble(drv["gcz"].ToString()));
                    }
                    else//覆盖单元格并且加上（覆盖）二字?加上次数?验证是否填满
                    {
                        row.GetCell(cell + 1).SetCellValue(Convert.ToDouble(drv["gcz"].ToString()));
                    }
                }
                r++;
            }
            sheet2.ForceFormulaRecalculation = true;
            sheet3.ForceFormulaRecalculation = true;
            WriteToFile(ldpath);
        }
        //用sql语句查询获得水位温度数据表
        public DataTable gettb(string sql)
        {
            OdbcConnection conn = db.getdbconn();
            DataTable dt = new DataTable();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;
        }
        /// <summary>
        /// 用sql语句查询获得gps数据表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable getgpstb(string sql, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter oda = new SqlDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;
        }
        public DataTable getcxdt(string sql, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter oda = new SqlDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;
        }
        public DataTable getraindt(string sql, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter oda = new SqlDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;
        }
        public DataTable cross(string gpssql)
        {

            //建立两个表 dt_2015,dt_2014
            //如果跨年将将两个表的数据进行合并
            DataTable dt_2015 = new DataTable();
            DataTable dt_2014 = new DataTable();
            DataTable dt_cross = new DataTable();
            //DropDownList DropDownList3 = (DropDownList)Master.FindControl("DropDownList3");
            //string sttm = this.TextBox1.Text; //((TextBox)Master.FindControl("TextBox1")).Text;
            //string edtm = this.TextBox2.Text;//((TextBox)Master.FindControl("TextBox2")).Text;
            ////获取时间范围，判断是否跨年，如果跨年则分成两个表后再将表数据整合成一个表
            //if (iscross(sttm, edtm))//如果跨年则将
            //{
            SqlConnection sqlconn_2014 = db.getsqlconnstr("2014");
            dt_2014 = getgpstb(gpssql, sqlconn_2014);
            SqlConnection sqlconn_2015 = db.getsqlconnstr("2015");
            dt_2015 = getgpstb(gpssql, sqlconn_2015);
            dt_cross = kisstable(dt_2014, dt_2015);
            DataView dv = new DataView(dt_cross);
            dv.Sort = "GPSBaseName";
            //dt_cross = dv;
            return viewtotable(dv);
        }
        public DataTable cxcross(string cxsql)
        {
            DataTable dt_2015 = new DataTable();
            DataTable dt_2014 = new DataTable();
            DataTable dt_cross = new DataTable();
            SqlConnection sqlconn_2014 = db.getsqlconnstr("2014");
            dt_2014 = getcxdt(cxsql, sqlconn_2014);
            SqlConnection sqlconn_2015 = db.getsqlconnstr("2015");
            dt_2015 = getcxdt(cxsql, sqlconn_2015);
            dt_cross = kisstable(dt_2014, dt_2015);
            DataView dv = new DataView(dt_cross);
            dv.Sort = "Name";
            //dt_cross = dv;
            return viewtotable(dv);
        }
        public DataTable raincross(string cxsql)
        {
            DataTable dt_2015 = new DataTable();
            DataTable dt_2014 = new DataTable();
            DataTable dt_cross = new DataTable();
            SqlConnection sqlconn_2014 = db.getsqlconnstr("2014");
            dt_2014 = getraindt(cxsql, sqlconn_2014);
            SqlConnection sqlconn_2015 = db.getsqlconnstr("2015");
            dt_2015 = getraindt(cxsql, sqlconn_2015);
            dt_cross = kisstable(dt_2014, dt_2015);
            DataView dv = new DataView(dt_cross);
            dv.Sort = "RID";
            //dt_cross = dv;
            return viewtotable(dv);
        }
        public DataTable viewtotable(DataView dv)
        {
            //获取字段
            DataTable dt = new DataTable();
            foreach (DataColumn col in dv.Table.Columns)
            {
                dt.Columns.Add(col.ColumnName);
            }
            int len = dv.Table.Columns.Count;
            //将视图中的行添加到表中
            foreach (DataRowView drv in dv)
            {
                DataRow dr = dt.NewRow();
                for (int i = 0; i < len; i++)
                {
                    dr[i] = drv[i];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable kisstable(DataTable dt_m, DataTable dt_w)//两个表的字段一模一样
        {
            //获取被衔接字段的长度
            int len = dt_w.Columns.Count;
            DataView dv = new DataView(dt_w);
            foreach (DataRowView drv in dv)
            {
                DataRow dr = dt_m.NewRow();
                for (int i = 0; i < len; i++)
                {
                    dr[i] = drv[i];
                }
                dt_m.Rows.Add(dr);
            }
            return dt_m;
        }
        static HSSFWorkbook hssfworkbook;
        
        static void WriteToFile(string ldpath)
        {
            //Write the stream data of workbook to the root directory
            FileStream file = new FileStream(ldpath, FileMode.Create, FileAccess.ReadWrite);
            hssfworkbook.Write(file);
            file.Close();
        }

        static void InitializeWorkbook(string tppath)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added. 
            FileStream file = new FileStream(tppath, FileMode.Open, FileAccess.Read);

            hssfworkbook = new HSSFWorkbook(file);
            //删除所有的工作簿
            int i = 0;
            //for (i = 0; i < hssfworkbook.NumberOfSheets;i++ )
            //{
            //    hssfworkbook.RemoveSheetAt(1);
            //}
            //create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfworkbook.DocumentSummaryInformation = dsi;

            //create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfworkbook.SummaryInformation = si;
        }
        public string SheetCreateByTable(HttpContext context,string st,string et)
        {

            InitializeWorkbook("D:/ReportExcel.xls");
            string tabHead = "";
            string title = "";
            string type = "";
            database db = new database();
            string xmname = "白云边坡监测";
            if (context.Session["jcxmname"] != null)
            {
                xmname = context.Session["jcxmname"].ToString();
            }
            if (context.Session["xmname"] != null)
            {
                xmname = context.Session["xmname"].ToString();
            }
            OdbcConnection conn = db.GetStanderConn(xmname);
            System.Data.DataTable data = new System.Data.DataTable();
            string[] sessionsqls = { "htusql", "pageBMWYsql", "rainsql", "cxsql", "settlementsql", "strainsql" };
            int index = 0;
            foreach (string sqlsession in sessionsqls)
            {
                if (context.Session[sqlsession] != "" && context.Session[sqlsession] != null)
                {
                    switch (sqlsession)
                    {
                        case "htusql": mapXm.Add("水位(mm)"); tabHead = "编号,模块号,点号,水位值,温度值,时间"; type="水位"; title = "水位监测数据报表"; break;
                        case "pageBMWYsql": mapXm.Add("表面位移(m)"); type = "表面位移"; tabHead = "点名, 周期, N(m), E(m), Z(m), ΔN(m), ΔE(m), ΔZ(m), ΣΔN(m), ΣΔE(m), ΣΔZ(m), 时间"; title = "表面位移监测数据报表"; break;
                        case "rainsql": mapXm.Add("雨量(cm)");type="雨量"; title = "雨量监测数据报表"; tabHead = "点号,降雨量,初始值,时间"; break;
                        case "cxsql": mapXm.Add("深部位移(m)"); type="深部位移";title = "测斜监测数据报表"; tabHead = "点号,形变值,时间"; break;
                        case "settlementsql": mapXm.Add("沉降(mm)");type="沉降"; title = "沉降监测数据报表"; tabHead = "点号,初始标高,当前标高,本次变化值,累计变化值,时间"; break;
                        case "strainsql": mapXm.Add("应力(N)");type="应力"; title = "应力监测数据报表"; tabHead = "点名,初始应力,当前应力,本次力变,累计力变,时间"; break;
                    }
                    string sql = context.Session[sqlsession].ToString();
                    data = querysql.querystanderdb(sql, conn);
                    SetTabVal(tabHead, data, title,type,st,et,index);
                    index++;
                }
            }
            string mappath = context.Server.MapPath("..\\数据报表");
            //SeriesCreate(context);
            WriteToFile(mappath + ("\\" + st + "-" + et + "数据.xls").Replace(":","时"));
            return mappath + ("\\" + st + "-" + et + "数据.xls").Replace(":","时");
        }
        /// <summary>
        /// 根据不同监测项目的表格填充数据
        /// </summary>
        /// <param name="tabHead"></param>
        /// <param name="dt"></param>
        /// <param name="tltle"></param>
        /// <param name="sheetName"></param>
        public void SetTabVal(string tabHead,DataTable dt,string title,string sheetName,string st,string et,int index)
        {
            string[] tabName = tabHead.Split(',');
            DataView dv = new DataView(dt);
            ISheet sheet1 = null;
            if(hssfworkbook.GetSheet(sheetName) ==null)
            sheet1 = hssfworkbook.CreateSheet(sheetName);
            else
                sheet1 = hssfworkbook.GetSheet(sheetName);
            //create cell on rows, since rows do already exist,it's not necessary to create rows again.
            if (dt.Columns.Count > 11) sheet1.PrintSetup.Landscape = true;
            sheet1.Header.Left = "南方测绘";
            sheet1.Header.Right = DateTime.Now.ToString();
            sheet1.Footer.Center = "第&p页";
            sheet1.PrintSetup.PaperSize = 9;
            sheet1.Workbook.SetRepeatingRowsAndColumns(index,0,dt.Columns.Count,0,1);
            //sheet1.PrintSetup.Scale = 400;
            sheet1.HorizontallyCenter = true;
            int k = 0;
            IRow row = sheet1.CreateRow(0);
            row = sheet1.CreateRow(0);
            ICellStyle styleCell = GetICellStyle();
            ICellStyle styleBorder = GetBorderSetStyle();
            //CellBorderSolid(row.CreateCell(1));
            foreach (string name in tabName)
            {

                row.CreateCell(k).SetCellValue(title);
                ICellStyle style = styleCell;
                k++;
            }
            sheet1.SetColumnWidth(dt.Columns.Count-1,27*256);
            //添加标题
            sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, dt.Columns.Count-2));
            CellFontSet(row.GetCell(0), "仿宋", 200, 16,true);
            CellBorderSet(row.GetCell(0),styleBorder,true);
            //stl.WrapText = true;
            CellFontSet(row.CreateCell(k - 1), "仿宋", 200, 5, true);
            row.GetCell(k - 1).SetCellValue(st + "至" + et
                );

            CellBorderSet(row.GetCell(k - 1),styleBorder, false);
            //创建表头
            row = sheet1.CreateRow(1);
            k = 0;
            foreach(string name in tabName)
            {

                row.CreateCell(k).SetCellValue(name);
                if (k == dt.Columns.Count - 1)
                    CellBorderSet(row.GetCell(k),styleBorder, false);
                else
                CellBorderSolid(row.GetCell(k),styleCell,true);
                k++;
            }
            //HSSFCellStyle style = (HSSFCellStyle)hssfworkbook.CreateCellStyle();
            //style.Alignment = HSSFCellStyle.ALIGN_CENTER;
            //HSSFFont font = hssfworkbook.CreateFont();
            //font.FontHeight = 20 * 20;
            //style.SetFont(font);
            //cell.CellStyle = style;
            //要产生图中的效果，即把A1:F1 这6 个单元格合并，然后添加合并区域：
            //sheet1.AddMergedRegion(new Region(0, 0, 0, 5));
            int i = 2;
            foreach (DataRowView drv in dv)
            {

                row = sheet1.CreateRow(i);
                int j = 0;
                for (j = 0; j < dt.Columns.Count;j++ )
                {
                    row.CreateCell(j).SetCellValue(dt.Rows[i-2].ItemArray[j].ToString());
                    if(j == dt.Columns.Count-1)
                        CellBorderSet(row.GetCell(j), styleBorder ,false);
                    else
                    CellBorderSolid(row.GetCell(j),styleCell,true);
                }
                i++;
            }
            //ISheet sheet4 = hssfworkbook.CreateSheet("Sheet4");
            //HSSFSheet sheetclone = (HSSFSheet)sheet1;
            //sheetclone.CloneSheet(hssfworkbook);
            //hssfworkbook.RemoveSheetAt(3);
            //getseriesdt(dt, mkh, tppath, ldpath);
            //Force excel to recalculate all the formula while open
            sheet1.ForceFormulaRecalculation = true;

           
        }
        //将单元格设置为有边框的
        public void CellBorderSolid(ICell cell, ICellStyle style,bool wrap)
        {
            //ICellStyle style = hssfworkbook.CreateCellStyle();
            style.WrapText = wrap;
            cell.CellStyle = style;
            
        }
        

        //创建表格样式
        public static ICellStyle GetICellStyle()
        {
            ICellStyle style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            style.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            return style;

        }
        //设置边框样式
        public static ICellStyle GetBorderSetStyle()
        {
            ICellStyle style = hssfworkbook.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;
            return style;
        }
        //设置单元格的边框
        public void CellBorderSet(ICell cell,ICellStyle style,bool wrap )
        {
            style.WrapText = wrap;
            cell.CellStyle = style;
        }
        //设置字体样式
        public void CellFontSet(ICell cell, string fontName,
            short fontWeight, short fontHeight,bool italic) 
        {
            ICellStyle style = hssfworkbook.CreateCellStyle();
            IFont font = hssfworkbook.CreateFont();
            font.FontName = fontName;
            font.Boldweight = fontWeight;
            font.FontHeightInPoints = fontHeight;
            font.IsItalic = italic;
            style.SetFont(font);
            cell.CellStyle = style;
        }
        //设置字体样式
        //public static ICellStyle GetICellStyle()
        //{
        //    ICellStyle style = hssfworkbook.CreateCellStyle();
        //    IFont font = hssfworkbook.CreateFont();
        //    font.FontName = fontName;
        //    font.Boldweight = fontWeight;
        //    font.FontHeightInPoints = fontHeight;
        //    font.IsItalic = italic;
        //    style.SetFont(font);
        //    cell.CellStyle = style;
        //}
        //日期格式转换
        public string DateFormatConverter(string dateStr)
        {
            if (dateStr == null || dateStr.Trim() == "") return "";
            char[] splits = {'-',' ','/',':' };
            string[] dateStrs = dateStr.Split(splits);
            return dateStrs[0].Substring(dateStrs[0].IndexOf("20")+2) + "年" + dateStrs[1] + "月" + dateStrs[2] + "日";
        }
        //填充曲线表
        /// <summary>
        /// 对不同监测项目的日期进行排序,生成X轴
        /// </summary>
        /// <param name="dt"></param>
        public List<DateTime> GetDateTimeFromSeriesInDiffJcxm(ISheet sheet,List<serie> series)
        {
            List<DateTime> ldt = new List<DateTime>();
            List<string> lType = new List<string>();
            int i = 0, j = 0;
            for (i = 0; i < series.Count; i++)
            {

                for (j = 0; j < series[i].Pts.Length; j++)
                {
                    ldt.Add(series[i].Pts[j].Dt);
                }

            }
            ldt = ldt.Distinct().ToList();
            ldt.Sort();
            IRow row  = sheet.CreateRow(0);
            for (i = 1; i < ldt.Count + 1; i++)
            {
                row.CreateCell(i).SetCellValue(ldt[i-1]);
            }
            return ldt;
        }
        /// <summary>
        /// 填充图例
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        /// <returns></returns>
        public List<string> LegendFill(ISheet sheet, List<serie> series)
        {
            List<string> legends = new List<string>();
            int i = 0;
            for (i = 1; i < series.Count + 1; i++)
            {
                IRow row = sheet.CreateRow(i);
                row.CreateCell(0).SetCellValue(series[i - 1].Name);
                legends.Add(series[i - 1].Name);
            }
            return legends;
        }
        /// <summary>
        /// 安装曲线数据点
        /// </summary>
        /// <param name="dataSheet"></param>
        /// <param name="series"></param>
        public void SerieDataPointFill(ISheet sheet, List<serie> series, List<DateTime> ldt, List<string> legends)
        {

            int y = 0, t = 0, posy = 0, posx = 0;
            for (y = 0; y < series.Count; y++)
            {
                IRow row = sheet.GetRow(y);
                posy = 1 + legends.IndexOf(series[y].Name);
                for (t = 0; t < series[y].Pts.Length; t++)
                {
                    posx = 1 + ldt.IndexOf(series[y].Pts[t].Dt);
                    row.CreateCell(posx).SetCellValue(series[y].Pts[t].Yvalue1);
                }
            }
        }
        //调用曲线填充函数
        public void SeriesCreate(HttpContext context)
        {
            //InitializeWorkbook("D:\\ReportExcel.xls");
            List<serie> ls = (List<serie>)context.Session["series"];
            //分拣出本次和累计
            List<serie> lsThis = new List<serie>();
            List<serie> lsAc = new List<serie>();
            foreach (serie s in ls)
            {
                if (s.Stype.IndexOf("累计") != -1)
                {
                    lsAc.Add(s);
                }
                else
                {
                    lsThis.Add(s);
                }

            }
            if (lsThis.Count > 0)
            {
              //本次曲线
                ISheet sheet = hssfworkbook.CreateSheet("本次变化曲线生成数据");
                List<DateTime> ldt =  GetDateTimeFromSeriesInDiffJcxm(sheet,lsThis);
                List<string> legends = LegendFill(sheet,lsThis);
                SerieDataPointFill(sheet,lsThis,ldt,legends);
            }
            if (lsAc.Count > 0)
            {
               //累计曲线
                ISheet sheet = hssfworkbook.CreateSheet("累计变化曲线生成数据");
                List<DateTime> ldt = GetDateTimeFromSeriesInDiffJcxm(sheet, lsAc);
                List<string> legends = LegendFill(sheet, lsAc);
                SerieDataPointFill(sheet, lsAc, ldt, legends);
            }
        }
        ////添加曲线图表
        //public void ChartPlus()
        //{
        //    Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
        //    Microsoft.Office.Interop.Excel.Workbook wkb = app.Workbooks.Add(@"D:\\ReportExcel.xls");
        //    Microsoft.Office.Interop.Excel.Worksheet sheet = (Microsoft.Office.Interop.Excel.Worksheet)wkb.Worksheets[1];
        //    Microsoft.Office.Interop.Excel.Chart chart = (Microsoft.Office.Interop.Excel.Chart)wkb.Charts.Add();
        //    Microsoft.Office.Interop.Excel.Range range = sheet.get_Range("B2", "C4");
        //    chart.SetSourceData(range);
        //    wkb.Save();
        //    app.Quit();
        //}
    }

}