﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;
using System.Web.Security;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;
using System.Diagnostics;
using System.Web.Script.Serialization;
using NFnet_DAL;
namespace NFnet_BLL
{
    public class GetStanderTab
    {
        public static string GetSqlStr(string path/*配置文件路径*/)
        {
            //string pth = Path.GetFullPath("../专库配置文件/水位转换文件");
            //读取配置文件
            FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read);
            byte[] buff = new byte[100];
            List<string> ls = new List<string>();
            string str = "";
            while ( fs.Read(buff, 0, 100)>0)
            {
               
                str += System.Text.Encoding.Default.GetString(buff);
                buff = new byte[100];
            }
            fs.Close();
            return str;

        }
        /// <summary>
        /// 根据分页的起始位置和行数查询表记录
        /// </summary>
        public static DataTable GetMySqlTab(string sql, int offset, int rows, OdbcConnection conn)
        {
            DataTable dt = new DataTable();
            string limit = "limit " + offset + "," + rows;
            //List<string> ls = new List<string>();
            //ls.Add(sql);
            //ls.Add(limit);
            sql = sql.Replace("#_1",offset.ToString());
            sql = sql.Replace("#_2", (rows-offset).ToString());
            dt = querysql.querystanderdb(sql, conn);
            return dt;
        }
        /// <summary>
        /// 根据分页的起始位置和行数查询Access表记录
        /// </summary>
        public static DataTable GetAccessTab(string sql, int offset, int rows, OleDbConnection  conn,DataTable dtSatander,TranstInfo tif)
        {
            //读取转库映射方案生成标准表
            if (sql == "select * from []")
            {
                return null;
            }
            //将标准表的列的类型全部转换成字符型
            DataTable dt = new DataTable();
            for (int j = 0; j < dtSatander.Columns.Count; j++)
            {
                dt.Columns.Add(dtSatander.Columns[j].ColumnName);
            }
            int tabColumnLen = dt.Columns.Count;
            int cont = 0;
            string limit = "limit " + offset + "," + rows;
            if (conn.State != ConnectionState.Open) conn.Open();
            OleDbCommand cmd = new OleDbCommand(sql,conn);
            OleDbDataReader reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                
                if (cont < offset) 
                { 
                    cont++; continue; 
                }
                else
                {
                    DataRow dr = dt.NewRow();
                    int len = 0;
                    for (len = 0; len < tabColumnLen; len++)
                    {
                        dr[len] = reader[len];
                    }
                    dt.Rows.Add(dr);
                    cont++;
                }
                if (cont > (offset + rows))
                {
                    break;
                }
                
            }
           
            return dt;
        }
        /// <summary>
        /// 查询mysql表的记录数
        /// </summary>
        public static int GetMySqlTabCount(string sql, OdbcConnection conn)
        {
            int cont = 0;
            cont = Int32.Parse(querysql.querystanderstr(sql, conn));
            return cont;
        }
        /// <summary>
        /// 根据分页的起始位置和行数查询表记录
        /// </summary>
        public static DataTable GetSqlServerTab(string sql, int offset, int rows, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            sql = sql.Replace("#_1", offset.ToString());
            sql = sql.Replace("#_2", (rows-1).ToString());
            dt = querysql.QuerySSTargetDB(sql, conn);
            return dt;
        }
        /// <summary>
        /// 根据分页的起始位置和行数查询表记录
        /// </summary>
        public static int GetSqlServerTabCount(string sql, SqlConnection conn)
        {
            int cont = Int32.Parse(querysql.QuerySSTargetDBStr(sql, conn));
            return cont;
        }
       public static DataTable getTab(string sql, int offset, int rows, object conn)
       {
           OleDbConnection oledbconn = conn as OleDbConnection;
           SqlConnection sqlconn = conn as SqlConnection;
           OdbcConnection odbcconn = conn as OdbcConnection;
           if (sqlconn != null)
           {
             return  GetSqlServerTab(sql,offset,rows,sqlconn);
           }
           else if(odbcconn!=null)
           {
             return  GetMySqlTab(sql,offset,rows,odbcconn);
           }
           return null;
       }
       public static int getTabCount(string sql, object conn)
       {
           OleDbConnection oledbconn = conn as OleDbConnection;
           SqlConnection sqlconn = conn as SqlConnection;
           OdbcConnection odbcconn = conn as OdbcConnection;
           if (sqlconn != null)
           {
               return GetSqlServerTabCount(sql,sqlconn);
           }
           else if (odbcconn != null)
           {
               return GetMySqlTabCount(sql,odbcconn);
           }
           else if (oledbconn != null)
           {
               return GetAccessTabCount(sql,oledbconn);
           }
           return 0;
       }
       /// <summary>
       /// 根据分页的起始位置和行数查询表记录
       /// </summary>
       public static int GetAccessTabCount(string sql, OleDbConnection conn)
       {
           int cont = Int32.Parse(querysql.queryaccessdbstring(sql, conn));
           return cont;
       }
     

    }
}