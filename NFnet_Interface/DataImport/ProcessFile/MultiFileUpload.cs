﻿using System;
using System.IO;
using NFnet_MODAL;
namespace NFnet_BLL
{
    public class MultiFileUpload
    {
        private Stream fs = null;//接收客户端传过来的文件集
        private string filename = "";//文件名
        private string fpath = "";//文件写入的路径 
        public string Fpath
        {
            get { return fpath; }
            set { fpath = value; }
        }
        public Stream Fs
        {
            get { return fs; }
            set { fs = value; }
        }
        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }
       
        public  MultiFileUpload(Stream stream,string filename,string fpath)
        {
            this.fs = stream;
            if (filename.LastIndexOf("\\") != -1)
            {
                this.filename = filename.Substring(filename.LastIndexOf("\\")+1);
            }
            if (filename.LastIndexOf("/") != -1)
            {
                this.filename = filename.Substring(filename.LastIndexOf("/")+1);
            }
            else
            {
                this.filename = filename;
            }
            this.fpath = fpath;
        }
        /// <summary>
        /// 上传多文件
        /// </summary>
        /// <summary>
        /// 上传单个文件//更改文件名
        /// </summary>
        /// <param name="fs"></param>
        public string UploadFile()
        {
          //string filename = 
            string newname = DateTime.Now.ToFileTime() + filename;
            //如果目录不存在则创建
            if (!Directory.Exists(fpath))
            {
                Directory.CreateDirectory(fpath);
            }
            else
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(fpath);
                directoryInfo.Create();
            }
            FileStream fwrite = File.Open(fpath+"/"+newname,FileMode.OpenOrCreate,FileAccess.ReadWrite);
            byte[] buff = new byte[100];
            int i = 0;
            while((i = fs.Read(buff,0,100))>0)
            {
                fwrite.Write(buff,0,i);
                buff = new byte[100];
            }
            fwrite.Close();
            return fpath +"/"+ newname;
        }

        /// <summary>
        /// 上传多文件
        /// </summary>
        /// <summary>
        /// 上传单个文件//更改文件名
        /// </summary>
        /// <param name="fs"></param>
        public void UploadFile(string notReName)
        {
            //string filename = 
            string newname = filename;//日期+文件名
            //如果目录不存在则创建
            FileStream fwrite = null;
            try
            {

                //如果文件为空则直接返回
                if (this.fs == null || this.fs.Length == 0)
                {
                    Err.ErrMessgeList.Add(filename + "文件上传失败！原因:文件上传结果为0个字节!");
                    return;
                }
                //对于监测平面图覆盖处理
                if (this.Fpath.IndexOf("方案编辑/监测平面图") != -1)
                {
                    Directory.Delete(this.Fpath);
                }
                if (!Directory.Exists(this.fpath))
                {
                    Directory.CreateDirectory(this.fpath);
                }
                
                //建立一个时间脚注文件记录对应文件的上传日期
                string datescript = fpath + "/" + "date_" + newname.Substring(0, newname.LastIndexOf(".")) + ".txt";
                fwrite = File.Open(datescript, FileMode.CreateNew, FileAccess.ReadWrite);
                string writeDate = DateTime.Now.ToString();
                byte[] buf = System.Text.Encoding.UTF8.GetBytes(writeDate);
                fwrite.Write(buf, 0, buf.Length);
                fwrite.Close();
                fwrite = File.Open(fpath + "/" + newname, FileMode.CreateNew, FileAccess.ReadWrite);
                byte[] buff = new byte[100];
                int i = 0;

                while ((i = this.Fs.Read(buff, 0, 100)) > 0)
                {
                    fwrite.Write(buff, 0, i);
                    buff = new byte[100];
                }
                fwrite.Close();
                Err.ErrMessgeList.Add("上传成功!");


            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, "文件错误:文件大小" + this.Fs.Length);
                Err.ErrMessgeList.Add(filename + "文件上传失败！原因:存在相同的文件名!");
                //return "-1";

            }
            finally
            {
                this.Fs.Close();
                if (fwrite != null) fwrite.Close();
            }
           
        }

        public string GetFileName(string dpath)
        {
            if (dpath.LastIndexOf("\\") != -1)
            {
                dpath = filename.Substring(filename.LastIndexOf("\\") + 1);
            }
            if (dpath.LastIndexOf("/") != -1)
            {
                dpath = filename.Substring(filename.LastIndexOf("/") + 1);
            }
            return fpath + "/" + dpath;
            
        }

    }
}