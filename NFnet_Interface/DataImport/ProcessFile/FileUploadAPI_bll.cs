﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using NFnet_MODAL;
using System.Data.Odbc;
using NFnet_DAL;

namespace NFnet_BLL
{
    public class FileUploadAPI_bll
    {
        public static database db = new database();
        /// <summary>
        /// 文件上传接口
        /// </summary>
        /// <returns></returns>
        public string FileUploadServer(HttpContext context,string name ,string pass,string xmname)
        {
;
            string unitName = LoginServer(name, pass);
            ExceptionLog.ExceptionWrite("name:"+name+"  pass:"+pass+" 项目:"+xmname);
            if (unitName == "")
            {
                context.Response.Write("-1");
            }
            else
            {
                ExceptionLog.ExceptionWrite("账号密码验证通过!");
                string path = DataFileUpload(unitName, xmname, context);
                ExceptionLog.ExceptionWrite("文件上传成功!");
                string result = FileDecodeServer(path, xmname);
                if (result == "-3") context.Response.Write(-3);
                ExceptionLog.ExceptionWrite("文件解析入库成功!");
                context.Response.Write(0);
            }
            return null;
        }
        /// <summary>
        /// 用户名密码登陆验证
        /// </summary>
        /// <param name="name">用户名</param>
        /// <param name="pass">密码</param>
        /// <returns></returns>
        public string LoginServer(string name, string pass)
        {
            member member = new member { UserId = name, Password = pass };
            member = member.LoginMember();
            if (member != null)
                return member.UnitName;
            else
                return "";

        }
        /// <summary>
        /// 全站仪数据文件上传到相应的文件夹中
        /// </summary>
        /// <param name="unitName">单位名</param>
        /// <param name="xmname">项目名称</param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string DataFileUpload(string unitName, string xmname, HttpContext context)
        {
            string filename = context.Request.Files[0].FileName;
            DateTime dt = DateTime.Now;
            string fileName = string.Format("{0}_{1}月_{2}日_{3}时_{4}分{5}", Path.GetFileNameWithoutExtension(filename), dt.Month, dt.Day, dt.Hour, dt.Minute, Path.GetExtension(filename));
            string path = context.Server.MapPath("../文件管理/全部文件/" + unitName + "/" + xmname + "/全站仪/" + fileName);
            try
            {
                if (!Directory.Exists("../文件管理/全部文件/" + unitName + "/" + xmname + "/全站仪"))
                {
                    Directory.CreateDirectory("../文件管理/全部文件/" + unitName + "/" + xmname + "/全站仪");
                }
                FileUploadProcess(context.Request.Files[0].InputStream, path);
                return path;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("FileUploadAPI_bll" + ex.Message);
                return "";
            }

        }
        /// <summary>
        /// 文件解析入库
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string FileToDataBase(string filePath)
        {

            return null;
        }
        public bool FileUploadProcess(Stream stream, string path)
        {
            byte[] buff = new byte[1024];
            FileStream fs = null;
            try
            {
                fs = File.Open(path, FileMode.Create, FileAccess.Write);
                while (stream.Read(buff, 0, buff.Length) > 0)
                {
                    fs.Write(buff, 0, buff.Length);
                    buff = new byte[1024];
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message + "出错文件_FileUploadAPI_bll");
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
            return false;
        }
        public string FileDecodeServer(string path, string xmname)
        {
            if (!FileInvalidate(path)) return "数据文件格式错误！";
            OdbcConnection conn = db.GetStanderConn(xmname);
            string result = "";//FileDecode.FileDecodeTxt(path, conn,xmname);
            return result;
        }
        /// <summary>
        /// 文件格式检查
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool FileInvalidate(string path)
        {
            StreamReader sr = null;
            try
            {
                sr = File.OpenText(path);
                string lineTxt = "";
                string endfile = "ThisIsFileEnd";
                int i = 0;
                while ((lineTxt = sr.ReadLine()) != "")
                {

                    if (lineTxt == null) break;
                    if (lineTxt.Trim() == endfile) return true;
                }
                sr.Close();
                return false;
            }
            catch (Exception ex)
            {
                sr.Close();
                ExceptionLog.ExceptionWrite(ex, "文件校验出错!");
                return false;
            }

        }
    }
}