﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Report;

namespace NFnet_Interface.Report
{
    public class ProcessXmWeekReport
    {
        public ProcessWeekReportBLL processWeekReportBLL = new ProcessWeekReportBLL();
        public AutoReport.Model.autoweekreport XmWeekReport(int xmno,out string mssg)
        {
            mssg = "";
            var weekreportmodel = new ProcessWeekReportBLL.ProcessXmWeekReportModelGetModel(xmno);
            if (processWeekReportBLL.ProcessXmWeekReportModelGet(weekreportmodel, out mssg))
                return weekreportmodel.model;
            return null;
        }
    }
}
