﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Report;

namespace NFnet_Interface.Report
{
    public class ProcessMonthReport
    {
        public ProcessMonthReportBLL processMonthReportBLL = new ProcessMonthReportBLL();
        public AutoReport.Model.automonthreport MonthReport(int xmno,int Month,int hour,out string mssg)
        {
            mssg = "";
            var Monthreportmodel = new ProcessMonthReportBLL.ProcessmonthReportModelGetModel(xmno, Month, hour);
            if (processMonthReportBLL.ProcessmonthReportModelGet(Monthreportmodel, out mssg))
                return Monthreportmodel.model;
            return null;
        }
    }
}
