﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Report;

namespace NFnet_Interface.Report
{
    public class ProcessXmMonthReport
    {
        public ProcessMonthReportBLL processMonthReportBLL = new ProcessMonthReportBLL();
        public AutoReport.Model.automonthreport XmMonthReport(int xmno,out string mssg)
        {
            mssg = "";
            var Monthreportmodel = new ProcessMonthReportBLL.ProcessXmmonthReportModelGetModel(xmno);
            if (processMonthReportBLL.ProcessXmmonthReportModelGet(Monthreportmodel, out mssg))
                return Monthreportmodel.model;
            return null;
        }
    }
}
