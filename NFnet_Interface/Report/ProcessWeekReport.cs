﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Report;

namespace NFnet_Interface.Report
{
    public class ProcessWeekReport
    {
        public ProcessWeekReportBLL processWeekReportBLL = new ProcessWeekReportBLL();
        public AutoReport.Model.autoweekreport WeekReport(int xmno,int week,int hour,out string mssg)
        {
            mssg = "";
            var weekreportmodel = new ProcessWeekReportBLL.ProcessWeekReportModelGetModel(xmno, week, hour);
            if (processWeekReportBLL.ProcessWeekReportModelGet(weekreportmodel, out mssg))
                return weekreportmodel.model;
            return null;
        }
    }
}
