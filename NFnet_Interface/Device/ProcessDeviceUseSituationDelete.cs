﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;
using NFnet_BLL.deviceusesituation;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceUseSituationDelete
    {
        public ProcessdeviceusesituationUseSituation deviceusesituationUse = new ProcessdeviceusesituationUseSituation();
        public bool DeviceDelete(string unitname,int id,out string mssg)
        {
            var model = new ProcessdeviceusesituationUseSituation.ProcessdeviceusesituationDeleteModel(unitname,id);
            return deviceusesituationUse.ProcessdeviceusesituationDelete(model, out mssg);
        }
    }
}
