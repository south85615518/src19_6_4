﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;
using NFnet_BLL.deviceusesituation;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceUseSituationTableCountLoad
    {
        public ProcessdeviceusesituationUseSituation deviceusesituationUseBLL = new ProcessdeviceusesituationUseSituation();
        public string XmDeviceTableCountLoad(string unitname,List<string>searchList,out string mssg)
        {
            var model = new ProcessdeviceusesituationUseSituation.ProcessXmdeviceusesituationTableCountLoadModel(unitname,searchList);
            if (deviceusesituationUseBLL.ProcessXmdeviceusesituationTableCountLoad(model, out mssg))
                return model.totalCont;
            return null;
        }
    }
}
