﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceDelete
    {
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public bool DeviceDelete(string unitname,int id,out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessDeviceDeleteModel(unitname,id);
            return processDeviceBLL.ProcessDeviceDelete(model,out mssg);
        }
    }
}
