﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessMonitorDeviceTableCountLoad
    {
        public ProcessDeviceApplyBLL processDeviceApplyBLL = new ProcessDeviceApplyBLL();
        public string MonitorDeviceTableCountLoad(string unitname,string monitorID,out string mssg)
        {
            var model = new ProcessDeviceApplyBLL.ProcessMonitordeviceuseapplyTableCountLoadModel(unitname,monitorID);
            if (processDeviceApplyBLL.ProcessMonitordeviceuseapplyTableCountLoad(model, out mssg))
                return model.totalCont;
            return null;
        }
    }
}
