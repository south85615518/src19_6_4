﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceStateSet
    {
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public bool DeviceStateSet(string unitname,int id,string state,out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessDeviceStateSetModel(unitname,id,state);
            return processDeviceBLL.ProcessDeviceStateSet(model,out mssg);
        }
    }
}
