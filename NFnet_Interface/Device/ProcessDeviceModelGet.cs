﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceModelGet
    {
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public global::device.Model.xmdevice DeviceModelGet(string unitname,int id,out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessDeviceModelGetModel(unitname,id);
            if (processDeviceBLL.ProcessDeviceModelGet(model, out mssg))
                return model.xmdevicemodel;
            return new device.Model.xmdevice();
        }
    }
}
