﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessXmDeviceAlarm
    {
        
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public List<global::device.Model.xmdevice> XmDeviceAlarm(string unitname,out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessXmDeviceAlarmModel(unitname);
            if (processDeviceBLL.ProcessXmDeviceAlarm(model, out mssg))
                return model.modellist;
            return null ;
        }
    }
}
