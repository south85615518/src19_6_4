﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessXmDeviceApplyTableLoad
    {
        public ProcessDeviceApplyBLL processDeviceApplyBLL = new ProcessDeviceApplyBLL();
        public DataTable XmDeviceTableLoad(string unitname,int pageIndex,int rows,  string sord, out string mssg)
        {
            var model = new ProcessDeviceApplyBLL.ProcessdeviceuseapplyTableLoadModel(pageIndex, rows, unitname, sord);
            if (processDeviceApplyBLL.ProcessdeviceuseapplyTableLoad(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
