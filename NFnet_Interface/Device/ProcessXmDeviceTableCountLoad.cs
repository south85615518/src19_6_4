﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessXmDeviceTableCountLoad
    {
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public string XmDeviceTableCountLoad(string unitname,List<string>searchList,out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessXmDeviceTableCountLoadModel(unitname,searchList);
            if (processDeviceBLL.ProcessXmDeviceTableCountLoad(model, out mssg))
                return model.totalCont;
            return null;
        }
    }
}
