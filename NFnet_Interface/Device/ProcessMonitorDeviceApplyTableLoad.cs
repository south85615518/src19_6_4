﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessMonitorDeviceApplyTableLoad
    {
        public ProcessDeviceApplyBLL processDeviceApplyBLL = new ProcessDeviceApplyBLL();
        public DataTable MonitorDeviceApplyTableLoad(string unitname,string monitorID,int pageIndex,int rows, string sord, out string mssg)
        {
            var model = new ProcessDeviceApplyBLL.ProcessMonitordeviceuseapplyTableLoadModel(unitname,monitorID, pageIndex, rows, sord);
            if (processDeviceApplyBLL.ProcessMonitordeviceuseapplyTableLoad(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
