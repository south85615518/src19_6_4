﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessXmDeviceApplyTableCountLoad
    {
        public ProcessDeviceApplyBLL processDeviceApplyBLL = new ProcessDeviceApplyBLL();
        public string XmDeviceApplyTableCountLoad(string unitname, out string mssg)
        {
            var model = new ProcessDeviceApplyBLL.ProcessdeviceuseapplyTableCountLoadModel(unitname);
            if (processDeviceApplyBLL.ProcessdeviceuseapplyTableCountLoad(model, out mssg))
                return model.totalCont;
            return null;
        }
    }
}
