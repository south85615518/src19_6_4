﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceUseStateSet
    {
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public bool DeviceUseStateSet(string unitname,int id,bool state,out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessDeviceUseStateSetModel(unitname,id,state);
            return processDeviceBLL.ProcessDeviceUseStateSet(model,out mssg);
        }
    }
}
