﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;
using NFnet_BLL.deviceusesituation;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceUseSituationModelGet
    {
        public ProcessdeviceusesituationUseSituation deviceusesituationUseBLL = new ProcessdeviceusesituationUseSituation();
        public global::device.Model.deviceusesituation DeviceModelGet(string unitname,int id,out string mssg)
        {
            var model = new ProcessdeviceusesituationUseSituation.ProcessdeviceusesituationModelGetModel(unitname,id);
            if (deviceusesituationUseBLL.ProcessdeviceusesituationModelGet(model, out mssg))
                return model.xmdevicemodel;
            return new device.Model.deviceusesituation();
        }
    }
}
