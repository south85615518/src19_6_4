﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessUnitDeviceTableLoad
    {
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public DataTable UnitDeviceTableLoad(string unitname,int pageIndex,int rows, int xmno, List<string> searchList, string sord, out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessUnitDeviceTableLoadModel(pageIndex,rows,unitname,searchList,sord);
            if (processDeviceBLL.ProcessUnitDeviceTableLoad(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
