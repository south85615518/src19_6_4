﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceApplyModelGet
    {
        public ProcessDeviceApplyBLL processDeviceApplyBLL = new ProcessDeviceApplyBLL();
        public global::device.Model.deviceuseapply DeviceModelGet(string unitname,int id,out string mssg)
        {
            var model = new ProcessDeviceApplyBLL.ProcessDeviceModelGetModel(unitname, id);
            if (processDeviceApplyBLL.ProcessDeviceModelGet(model, out mssg))
                return model.deviceuseapplymodel;
            return new device.Model.deviceuseapply();
        }
    }
}
