﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceApplyDelete
    {
        public ProcessDeviceApplyBLL processDeviceApplyBLL = new ProcessDeviceApplyBLL();
        public bool DeviceApplyDelete(string unitname, int id, out string mssg)
        {
            var model = new ProcessDeviceApplyBLL.ProcessDeviceDeleteModel(unitname,id);
            return processDeviceApplyBLL.ProcessDeviceDelete(model, out mssg);
        }
    }
}
