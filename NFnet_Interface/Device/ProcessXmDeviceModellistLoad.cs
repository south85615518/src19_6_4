﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.Device;

namespace NFnet_Interface.Device
{
    public class ProcessXmDeviceModellistLoad
    {
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public List<device.Model.xmdevice> XmDeviceModellistLoad(int xmno, out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessXmDeviceModellistLoadModel(xmno);
            if (processDeviceBLL.ProcessXmDeviceModellistLoad(model, out mssg))
            {
                return model.xmdevicelist;
            }
            return new List<device.Model.xmdevice>();
        }
    }
}
