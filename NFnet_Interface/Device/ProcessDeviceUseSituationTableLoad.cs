﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.Device;
using NFnet_BLL.deviceusesituation;

namespace NFnet_Interface.Device
{
    public class ProcessDeviceUseSituationTableLoad
    {
        public ProcessdeviceusesituationUseSituation processdeviceusesituationUseBLL = new ProcessdeviceusesituationUseSituation();
        public DataTable XmDeviceTableLoad(string unitname,int pageIndex,int rows, List<string> searchList, string sord, out string mssg)
        {
            var model = new ProcessdeviceusesituationUseSituation.ProcessXmdeviceusesituationTableLoadModel(pageIndex, rows, unitname, searchList, sord);
            if (processdeviceusesituationUseBLL.ProcessXmdeviceusesituationTableLoad(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
