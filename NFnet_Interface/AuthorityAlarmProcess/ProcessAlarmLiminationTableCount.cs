﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthorityAlarm.BLL;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;
using System.Data;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmLiminationTableCount
    {
        public ProcessAlarmLiminationBLL processAlarmLiminationBLLbll = new ProcessAlarmLiminationBLL();
        public int AlarmLiminationTableCountLoad(string unitname, string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword, out string mssg)
        {
            var model = new ProcessAlarmLiminationBLL.AlarmEliminationTableCountLoadModel( unitname,  monitorID, xmno,  jclx,  pointname,   starttime,  endtime, keyword);
            if (processAlarmLiminationBLLbll.AlarmEliminationTableCountLoad(model, out mssg))
            {
                return model.cont;
            }
            return 0;
        }
    }
}
