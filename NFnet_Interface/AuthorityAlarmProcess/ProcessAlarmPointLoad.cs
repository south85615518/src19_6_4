﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;
using System.Data;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmPointLoad
    {
        public ProcessPointCheckBLL processPointCheckBLL = new ProcessPointCheckBLL();
        public DataTable AlarmPointLoad(int xmno, out string mssg)
        {
            var processXmAlarmCountModel = new ProcessPointCheckBLL.ProcessAlarmPointLoadModel(xmno);
            if (processPointCheckBLL.ProcessAlarmPointLoad(processXmAlarmCountModel, out mssg))
                return processXmAlarmCountModel.dt;
            return new DataTable();
        }
    }
}
