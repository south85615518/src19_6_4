﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
   public  class ProcessAlarmSplitOnDateModelGet
    {
       ProcessalarmsplitondateBLL processalarmsplitondateBLL = new ProcessalarmsplitondateBLL();
       public AuthorityAlarm.Model.alarmsplitondate AlarmSplitOnDateModelGet(int xmno,string dno,out string mssg)
       {
           var processalarmcgsplitondateModelGetModel = new NFnet_BLL.AuthorityAlarmProcess.ProcessalarmsplitondateBLL.ProcessalarmcgsplitondateModelGetModel(xmno,dno);
           processalarmsplitondateBLL.ProcessalarmcgsplitondateModelGet(processalarmcgsplitondateModelGetModel,out mssg);
           return processalarmcgsplitondateModelGetModel.model;
       }
    }
}
