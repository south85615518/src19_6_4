﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessXmAlarmCount
    {
        public ProcessPointCheckBLL processPointCheckBLL = new ProcessPointCheckBLL();
        public int XmAlarmCount(int xmno,string type,out string mssg)
        {
            var processXmAlarmCountModel = new ProcessPointCheckBLL.ProcessXmAlarmCountModel(xmno,type);
            if (processPointCheckBLL.ProcessXmAlarmCount(processXmAlarmCountModel, out mssg))
                return processXmAlarmCountModel.cont;
            return 0;
        }
    }
}
