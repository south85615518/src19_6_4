﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmSplitOnDateDeleteByAlarmNo
    {
        public bool AlarmSplitOnDateDeleteByAlarmNo(int xmno,string alarmno,out string mssg)
        {
            ProcessalarmsplitondateBLL processalarmsplitondateBLL = new ProcessalarmsplitondateBLL();
            var model = new ProcessalarmsplitondateBLL.ProcessalarmcgsplitondatedeletebyalarmidModel(xmno,alarmno);
            return processalarmsplitondateBLL.Processalarmcgsplitondatedeletebyalarmid(model,out mssg);
        }
    }
}
