﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmContLayout
    {
        public ProcessPointCheckBLL processPointCheckBLL = new ProcessPointCheckBLL();
        public string AlarmContLayout(string unitname,out string mssg)
        {
            var model = new NFnet_BLL.AuthorityAlarmProcess.ProcessPointCheckBLL.ProcessAlarmContLayoutModel(unitname);
            if (processPointCheckBLL.ProcessAlarmContLayout(model, out mssg))
            {
                return model.alarmcontlayout;
            }
            return "0/0/0/0";
        }
    }
}
