﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
 
    public class ProcessMaxAlarm
    {
        public ProcessPointCheckBLL processPointCheckBLL = new ProcessPointCheckBLL();
        public int MaxAlarm(int xmno, string type, out string mssg)
        {
            var processMaxAlarmModel = new ProcessPointCheckBLL.ProcessMaxAlarmModel(xmno, type);
            if (processPointCheckBLL.ProcessMaxAlarm(processMaxAlarmModel, out mssg))
                return processMaxAlarmModel.alarm;
            return 0;
        }
    }
}
