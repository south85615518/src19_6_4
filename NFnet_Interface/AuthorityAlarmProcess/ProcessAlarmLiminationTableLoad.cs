﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthorityAlarm.BLL;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;
using System.Data;
using Tool;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmLiminationTableLoad
    {
        public ProcessAlarmLiminationBLL processAlarmLiminationBLLbll = new ProcessAlarmLiminationBLL();
        public  DataTable  AlarmLiminationTableLoad(string unitname, string monitorID,int xmno, string jclx, string pointname, DateTime  starttime, DateTime endtime,string keyword, int startPageIndex,int pageSize, string sordname, string sord,out string mssg)
        {
            ExceptionLog.ExceptionWrite("起始页:"+startPageIndex+"行:"+pageSize);
            var model = new ProcessAlarmLiminationBLL.AlarmEliminationTableLoadModel( unitname,  monitorID, xmno,  jclx,  pointname,   starttime,  endtime, keyword,  startPageIndex, pageSize,  sordname,  sord);
            if (processAlarmLiminationBLLbll.AlarmEliminationTableLoad(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
