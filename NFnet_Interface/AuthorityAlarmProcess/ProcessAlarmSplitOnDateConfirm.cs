﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmSplitOnDateConfirm
    {

        public bool AlarmSplitOnDateConfirm(int xmno,string alarmid,out string mssg)
        {
            ProcessalarmsplitondateBLL processalarmsplitondateBLL = new ProcessalarmsplitondateBLL();
            var model =new ProcessalarmsplitondateBLL.ProcessalarmcgsplitondateconfirmModel(xmno, alarmid);
            return processalarmsplitondateBLL.Processalarmcgsplitondateconfirm(model,out mssg);

        }
    }
}
