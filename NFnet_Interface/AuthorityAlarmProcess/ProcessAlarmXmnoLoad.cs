﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;
using System.Data;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmXmnoLoad
    {
        public ProcessPointCheckBLL processPointCheckBLL = new ProcessPointCheckBLL();
        public List<string> AlarmXmnoLoad(string unitname, out string mssg)
        {
            var processXmAlarmCountModel = new ProcessPointCheckBLL.ProcessUnitAlarmXmnoLoadModel(unitname);
            if (processPointCheckBLL.ProcessUnitAlarmXmnoLoad(processXmAlarmCountModel, out mssg))
                return processXmAlarmCountModel.xmnolist;
            return new List<string>();
        }
    }
}
