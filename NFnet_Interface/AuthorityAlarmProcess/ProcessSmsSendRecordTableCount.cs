﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthorityAlarm.BLL;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;
using System.Data;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessSmsSendRecordTableCount
    {
        public ProcessSmsSendRecordBLL processSmsSendRecordBLLbll = new ProcessSmsSendRecordBLL();
        public int SmsSendRecordTableCountLoad(string unitname,int xmno,DateTime starttime, DateTime endtime, string keyword, out string mssg)
        {
            var model = new ProcessSmsSendRecordBLL.smssendrecordTableCountLoadModel( unitname, xmno,   starttime,  endtime, keyword);
            if (processSmsSendRecordBLLbll.smssendrecordTableCountLoad(model, out mssg))
            {
                return model.cont;
            }
            return 0;
        }
    }
}
