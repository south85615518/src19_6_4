﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessUnitAlarmSplitModelList
    {
       ProcessalarmsplitondateBLL processalarmsplitondateBLL = new ProcessalarmsplitondateBLL();
       public List<AuthorityAlarm.Model.alarmsplitondate> UnitAlarmSplitModelList(string unitname , DateTime lasttime,int timeunit ,out string mssg)
       {
           var processUnitAlarmSplitOnDateModelListModel = new NFnet_BLL.AuthorityAlarmProcess.ProcessalarmsplitondateBLL.ProcessUnitAlarmSplitOnDateModelListModel(unitname, lasttime,timeunit);
           processalarmsplitondateBLL.ProcessUnitAlarmSplitOnDateModelList(processUnitAlarmSplitOnDateModelListModel, out mssg);
           return processUnitAlarmSplitOnDateModelListModel.ls;
       }
    }
}
