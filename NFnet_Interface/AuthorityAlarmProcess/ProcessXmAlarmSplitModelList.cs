﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessXmAlarmSplitModelList
    {
       ProcessalarmsplitondateBLL processalarmsplitondateBLL = new ProcessalarmsplitondateBLL();
       public List<AuthorityAlarm.Model.alarmsplitondate> XmAlarmSplitModelList(int xmno , DateTime lasttime,int timeunit ,out string mssg)
       {
           var processXmAlarmSplitOnDateModelListModel = new NFnet_BLL.AuthorityAlarmProcess.ProcessalarmsplitondateBLL.ProcessXmAlarmSplitOnDateModelListModel(xmno, lasttime,timeunit);
           processalarmsplitondateBLL.ProcessXmAlarmSplitOnDateModelList(processXmAlarmSplitOnDateModelListModel, out mssg);
           return processXmAlarmSplitOnDateModelListModel.ls;
       }
    }
}
