﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthorityAlarm.BLL;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;
using System.Data;
using Tool;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessSmsSendRecordTableLoad
    {
        public ProcessSmsSendRecordBLL processSmsSendRecordBLLbll = new ProcessSmsSendRecordBLL();
        public  DataTable  SmsSendRecordTableLoad(string unitname, int xmno,  DateTime  starttime, DateTime endtime,string keyword, int startPageIndex,int pageSize, string sordname, string sord,out string mssg)
        {
            ExceptionLog.ExceptionWrite("起始页:"+startPageIndex+"行:"+pageSize);
            var model = new ProcessSmsSendRecordBLL.smssendrecordTableLoadModel( unitname,  xmno,   starttime,  endtime, keyword,  startPageIndex, pageSize,  sordname,  sord);
            if (processSmsSendRecordBLLbll.smssendrecordTableLoad(model, out mssg))
            {
                return model.dt;
            }
            return new DataTable();
        }
    }
}
