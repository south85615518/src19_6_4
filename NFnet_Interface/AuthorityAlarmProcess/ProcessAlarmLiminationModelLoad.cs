﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthorityAlarm.BLL;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;
using System.Data;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmLiminationModel
    {
        public ProcessAlarmLiminationBLL processAlarmLiminationBLLbll = new ProcessAlarmLiminationBLL();
        public AuthorityAlarm.Model.alarmelimination AlarmLiminationModel(int id, string unitname, out string mssg)
        {
            var model = new ProcessAlarmLiminationBLL.AlarmLiminationModelGetModel(id, unitname);
            if (processAlarmLiminationBLLbll.GetModel(model, out mssg))
            {
                return model.model;
            }
            return new AuthorityAlarm.Model.alarmelimination();
        }
    }
}
