﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthorityAlarm.BLL;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;
using System.Data;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessSmsSendRecordModel
    {
        public ProcessSmsSendRecordBLL processSmsSendRecordBLLbll = new ProcessSmsSendRecordBLL();
        public AuthorityAlarm.Model.smssendrecord SmsSendRecordModel(int id, string unitname, out string mssg)
        {
            var model = new ProcessSmsSendRecordBLL.SmsSendRecordModelGetModel(id, unitname);
            if (processSmsSendRecordBLLbll.GetModel(model, out mssg))
            {
                return model.model;
            }
            return new AuthorityAlarm.Model.smssendrecord();
        }
    }
}
