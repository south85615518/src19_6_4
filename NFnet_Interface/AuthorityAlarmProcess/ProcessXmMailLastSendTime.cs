﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessXmMailLastSendTime
    {
        public ProcessMonitorAlarmBLL processMonitorAlarmBLL = new ProcessMonitorAlarmBLL();
        public DateTime XmMailLastSendTime(int xmno,out string mssg)
        {
            var model = new ProcessMonitorAlarmBLL.ProcessMonitorAlarmLastSendTimeModel(xmno);
            if (processMonitorAlarmBLL.ProcessMonitorAlarmLastSendTime(model, out mssg))
            {
                return model.dt;
            }
            return new DateTime();
        }
    }
}
