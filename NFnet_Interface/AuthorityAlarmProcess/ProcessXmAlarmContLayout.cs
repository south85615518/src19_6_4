﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessXmAlarmContLayout
    {
        public ProcessPointCheckBLL processPointCheckBLL = new ProcessPointCheckBLL();
        public string XmAlarmContLayout(int xmno,out string mssg)
        {
            var model = new NFnet_BLL.AuthorityAlarmProcess.ProcessPointCheckBLL.ProcessXmAlarmContLayoutModel(xmno);
            if (processPointCheckBLL.ProcessXmAlarmContLayout(model, out mssg))
            {
                return model.alarmcontlayout;
            }
            return "0/0/0/0";
        }
    }
}
