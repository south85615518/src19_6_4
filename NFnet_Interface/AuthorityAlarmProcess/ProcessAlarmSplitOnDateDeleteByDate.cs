﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_Interface.AuthorityAlarmProcess
{
    public class ProcessAlarmSplitOnDateDeleteByDate
    {
        public bool AlarmSplitOnDateDeleteByDate(int xmno,DateTime date,out string mssg)
        {
            ProcessalarmsplitondateBLL processalarmsplitondateBLL = new ProcessalarmsplitondateBLL();
            var model = new ProcessalarmsplitondateBLL.ProcessalarmcgsplitondatedeletebydateModel(xmno,date);
            return processalarmsplitondateBLL.Processalarmcgsplitondatedeletebydate(model, out mssg);
        }
    }
}
