﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class TranstCase
    {
        private string tabname;//表名

        public string Tabname
        {
            get { return tabname; }
            set { tabname = value; }
        }
        List<Dictionary<string, string>> mapcase;  //对应方案

        public List<Dictionary<string, string>> Mapcase
        {
            get { return mapcase; }
            set { mapcase = value; }
        }

        public TranstCase(string tabname, List<Dictionary<string, string>> mapcase)
        {
            this.tabname = tabname;
            this.mapcase = mapcase;
        }
    }
}