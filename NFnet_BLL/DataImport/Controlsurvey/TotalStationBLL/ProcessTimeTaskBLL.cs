﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.Model.fmos_obj;
using System.Web.Script.Serialization;
using System.Threading;
using Tool;

namespace NFnet_BLL.Controlsurvey.TotalStationBLL
{
    public class ProcessTimeTaskBLL
    {
        
        public  JavaScriptSerializer jss = new JavaScriptSerializer();
        //public ProcessTimeTaskBLL timetaskServer = new ProcessTimeTaskBLL();
        public static string timeJson = "";
        public static TotalStation.BLL.fmos_obj.timetask timetaskBLL = new TotalStation.BLL.fmos_obj.timetask();

        public bool ProcessTimeTaskFormat(ProcessTimeTaskFormatModel model, out string mssg)
        {
            List<timetask> timetask = null;
            if (timetaskBLL.GetModelList(model.taskName, out timetask, out mssg))
            {
                model.timetask = timetask;
                return true;
            }
            return false;
        }

        public bool ProcessTimeTaskAllSort(ProcessTimeTaskFormatModel model, out string mssg)
        {
            List<timetask> timetask = null;
            if (timetaskBLL.GetModelList(model.taskName, out timetask, out mssg))
            {
                model.timetask = timetask;
                List<timetask> taskList = timetask.OrderBy(s => s.TimingHour).ThenBy(s => s.TimingMinute).ToList<timetask>();
                int i = 0;
                foreach (timetask task in taskList)
                {
                    task.Indx = i;
                    ProcessTimeTaskAdd(task,out mssg);
                    i++;
                }
                return true;
            }
            return false;
        }
        public class ProcessTimeTaskFormatModel
        {
            public string taskName { get; set; }
            public List<TotalStation.Model.fmos_obj.timetask> timetask { get; set; }
            public ProcessTimeTaskFormatModel(string taskName)
            {
                this.taskName = taskName;
            }
        }
        public bool ProcessTimeTaskAdd(TotalStation.Model.fmos_obj.timetask time, out string mssg)
        {

            return  timetaskBLL.Add(time, out mssg);

            //ProcessTimeTaskAllSort
        }

        public bool ProcessTimeTaskEdit(TotalStation.Model.fmos_obj.timetask time, out string mssg)
        {
            return timetaskBLL.Update(time, out mssg);
        }


        public bool ProcessTimeTaskDel(TotalStation.Model.fmos_obj.timetask time, out string mssg)
        {
            return timetaskBLL.Delete(time.DateTaskName, time.taskName, out mssg);

        }
        /// <summary>
        /// 全站仪采集任务设置
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessTimeSetControlSurvey(string xmname, out string mssg)
        {
            mssg = "";
            var ProcessTimeTaskFormatModel = new ProcessTimeTaskBLL.ProcessTimeTaskFormatModel(xmname);

            if (ProcessTimeTaskFormat(ProcessTimeTaskFormatModel, out mssg))
            {
              
                
                WebServerHelper.MySetTime(xmname, ConvertToString(ProcessTimeTaskFormatModel.timetask), out mssg);
                

            }
            else
            {
                //ProcessPrintMssg.Print(mssg);
            }



            return false;
        }
        public List<string> ConvertToString(List<timetask> timetasks)
        {
            List<string> ls = new List<string>();
            foreach (timetask e in timetasks)
            {
                ls.Add(jss.Serialize(e));
            }
            return ls;
        }
    }
}