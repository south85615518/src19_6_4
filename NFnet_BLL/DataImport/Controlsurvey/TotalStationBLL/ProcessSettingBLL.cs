﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.Model.fmos_obj;
using System.Web.Script.Serialization;
using Tool;
namespace NFnet_BLL.Controlsurvey.TotalStationBLL
{
    public class ProcessSettingBLL
    {
        public static TotalStation.BLL.fmos_obj.setting settingBLL = new TotalStation.BLL.fmos_obj.setting();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public bool ProcessSettingFormat(ProcessSettingFormatModel model, out string mssg)
        {
            mssg = "";
            setting st = null;
            if (settingBLL.GetModel(model.xmname, out st, out mssg))
            {
                model.st = st;
                return true;
            }
            return false;
        }
        public class ProcessSettingFormatModel
        {
            public string xmname { get; set; }
            public setting st { get; set; }
            public ProcessSettingFormatModel(string xmname)
            {
                this.xmname = xmname;
            }
        }

        public bool ProcessSettingName(ProcessSettingNameModel model, out string mssg)
        {
            List<string> settingname;
            if (settingBLL.SettingNameLoad(model.xmname, out settingname, out mssg))
            {
                model.settigname = settingname;
                return true;
            }
            return false;
        }
        public class ProcessSettingNameModel
        {
            public List<string> settigname { get; set; }
            public string xmname { get; set; }
            public ProcessSettingNameModel(string xmname)
            {
                this.xmname = xmname;

            }
        }

        public bool ProcessSettingUpdate(TotalStation.Model.fmos_obj.setting setting, out string mssg)
        {
            return settingBLL.Add(setting, out mssg);
            
        }

        public bool ProcessSettingSetControlSurvey(string xmname,out string mssg)
        {
            var processSettingFormatModel = new ProcessSettingBLL.ProcessSettingFormatModel(xmname);
            if (ProcessSettingFormat(processSettingFormatModel, out mssg))
            {
                string settingJson = jss.Serialize(processSettingFormatModel.st);
                WebServerHelper.MySetSetting(xmname, settingJson, out mssg);
            }
            else
            {
               
            }
            return false;
        }
    }
}