﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DataImport.ProcessFileSenmos;

namespace NFnet_BLL.DataImport.ProcessFile.SenmosServer
{
    public class ProcessDTUBLL
    {
        public ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();

        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataBLL(sensordata sensordata, int xmno, out string mssg)
        {
            mssg = "";
            if (sensordata.SensorType != "1") return false;

            global::DTU.Model.dtudata dtudata = new global::DTU.Model.dtudata
            {
                xmno = xmno,
                point_name = sensordata.PointName,
                deep = sensordata.MonitorValue,
                dtutime = sensordata.CollectionTime,
                time = sensordata.CollectionTime
            };
            return processDTUDATABLL.ProcessDTUDATAInsertBLL(dtudata, out mssg);
        }
    }
}