﻿using System;
using System.Collections.Generic;
using Tool;
using System.IO;
using System.Web.Script.Serialization;
using NFnet_BLL.UserProcess;
using System.Linq;
using NFnet_BLL.AuthorityAlarmProcess;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.DataImport.ProcessFileSenmos;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DataProcess.Strain.Sensor;
namespace NFnet_BLL.DataImport.ProcessFile.Strain
{
    /// <summary>
    /// 全站仪数据导入处理类
    /// </summary>
    public class ProcessStrainBLL
    {
        public ProcessResultDataBLL processStrainDataBLL = new ProcessResultDataBLL();

        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataBLL(sensordata sensordata, int xmno, out string mssg)
        {
            mssg = "";
            if (sensordata.SensorType != "13" && sensordata.SensorType != "14" && sensordata.SensorType != "15") return false;

            global::Strain.Model.strainresultdata strainresultdata = new global::Strain.Model.strainresultdata
            {
                xmno = xmno,
                this_val = sensordata.CurrentVariation,
                ac_val = sensordata.AccumulateVariation,
                strengthVal = sensordata.MonitorValue,
                monitorTime = sensordata.CollectionTime,
                cyc = sensordata.Cycle,
                pointname = sensordata.PointName

            };


           return  processStrainDataBLL.ProcessResultDataAdd(strainresultdata,out mssg);
        }






    }

}