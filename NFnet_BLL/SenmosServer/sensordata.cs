﻿/**  版本信息模板在安装目录下，可自行修改。
* sensordata.cs
*
* 功 能： N/A
* 类 名： sensordata
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/7/23 16:32:37   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace NFnet_BLL.DataImport.ProcessFileSenmos
{
	/// <summary>
	/// sensordata:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class sensordata
	{
		public sensordata()
		{}
		#region Model
		private int _id;
		private int _cycle;
		private string _pointname;
		private string _pointpattern;
		private string _datatype;
		private string _sensortype;
		private double _monitorvalue;
		private double _currentvariation;
		private double _accumulatevariation;
		private DateTime _collectiontime;
		private DateTime _sensortime;
        /// <summary>
		/// auto_increment
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int Cycle
		{
			set{ _cycle=value;}
			get{return _cycle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PointName
		{
			set{ _pointname=value;}
			get{return _pointname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PointPattern
		{
			set{ _pointpattern=value;}
			get{return _pointpattern;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DataType
		{
			set{ _datatype=value;}
			get{return _datatype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SensorType
		{
			set{ _sensortype=value;}
			get{return _sensortype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double MonitorValue
		{
			set{ _monitorvalue=value;}
			get{return _monitorvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double CurrentVariation
		{
			set{ _currentvariation=value;}
			get{return _currentvariation;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double AccumulateVariation
		{
			set{ _accumulatevariation=value;}
			get{return _accumulatevariation;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CollectionTime
		{
			set{ _collectiontime=value;}
			get{return _collectiontime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime sensorTime
		{
			set{ _sensortime=value;}
			get{return _sensortime;}
		}
		#endregion Model

	}
}

