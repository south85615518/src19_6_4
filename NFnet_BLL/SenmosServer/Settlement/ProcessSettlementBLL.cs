﻿using System;
using System.Collections.Generic;
using Tool;
using System.IO;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.UserProcess;
using System.Linq;
using NFnet_BLL.DataProcess;
using NFnet_BLL.AuthorityAlarmProcess;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DataImport.ProcessFileSenmos;
using NFnet_BLL.DisplayDataProcess.Settlement.Senor;
namespace NFnet_BLL.DataImport.ProcessFile.Settlement
{
    /// <summary>
    /// 全站仪数据导入处理类
    /// </summary>
    public class ProcessSettlementBLL
    {
        public ProcessSettlementDataBLL processSettlementDataBLL = new ProcessSettlementDataBLL();

        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataBLL(sensordata sensordata, int xmno, out string mssg)
        {
            mssg = "";
            if (sensordata.DataType != "沉降") return false;

            global::Settlement.Model.Settlementresultdata Settlementresultdata = new global::Settlement.Model.Settlementresultdata
            {
                xmno = xmno,
                this_val = sensordata.CurrentVariation,
                ac_val = sensordata.AccumulateVariation,
                elevation  = sensordata.MonitorValue,
                monitorTime = sensordata.CollectionTime,
                cyc = sensordata.Cycle,
                pointname = sensordata.PointName

            };


           return  processSettlementDataBLL.Add(Settlementresultdata,out mssg);
        }






    }

}