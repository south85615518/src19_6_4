﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.IO;
using NFnet_BLL.DataImport.ProcessFileSenmos;
using System.Web.Script.Serialization;
using System.Threading;

namespace NFnet_BLL.DataImport.ProcessFile.SenmosServer
{
    public class ProcessSensorBLL
    {

        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public NFnet_BLL.DataImport.ProcessFile.Strain.ProcessStrainBLL processStrainBLL = new NFnet_BLL.DataImport.ProcessFile.Strain.ProcessStrainBLL();
        public NFnet_BLL.DataImport.ProcessFile.Settlement.ProcessSettlementBLL processSettlementBLL = new NFnet_BLL.DataImport.ProcessFile.Settlement.ProcessSettlementBLL();
        public NFnet_BLL.DataImport.ProcessFile.SenmosServer.ProcessDTUBLL processDTUBLL = new NFnet_BLL.DataImport.ProcessFile.SenmosServer.ProcessDTUBLL();
        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">传感器文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataBLL(SensorFileUploadConditionModel model, out string mssg)
        {
            int i = 0, t = 0;
            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                //记录下当前项目正在入库操作的周期
                while ( strTemp != null && strTemp != "" && strTemp.Trim() != "" && strTemp != "ThisIsFileEnd")
                {
                    try
                    {
                        sensordata sensordata = (sensordata)jss.Deserialize(strTemp, typeof(sensordata));
                        strTemp = sr.ReadLine();
                        i++;
                        processStrainBLL.ProcessResultDataBLL(sensordata, model.xmno, out mssg);
                        processSettlementBLL.ProcessResultDataBLL(sensordata,model.xmno ,out mssg);
                        processDTUBLL.ProcessResultDataBLL(sensordata,model.xmno,out mssg);
                    }
                    catch (Exception ex)
                    {
                        mssg += string.Format("{1}文件第{0}行存在错误", i, "#");
                        strTemp = sr.ReadLine();
                        i++;
                    }

                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();






            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }


        /// <summary>
        /// 传感器数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileDecodeTxt(ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            switch (model.fileType)
            {
                case "结果数据":
                    bool ispass = Tool.ObjectHelper.ProcessTextFileCheck<sensordata>(model.path, out mssg);
                    var sensorDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);
                    if (!ThreadProcess.ThreadExist(string.Format("{0}sensor", model.xmname)))
                    {

                        Thread t = new Thread(new ParameterizedThreadStart(SenosorDataImport));
                        t.Name = string.Format("{0}resultdataimport", model.xmname);
                        t.Start(sensorDataImportModel);
                        model.thread = t;
                    }
                    return ispass;

                default: return false;

            }

        }





        public void SenosorDataImport(object obj)
        {
            //NFnet_BLL.DisplayDataProcess.pub.ProcessEmailSendBLL emailSendBLL = new NFnet_BLL.DisplayDataProcess.pub.ProcessEmailSendBLL();
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL();
            DataImportModel model = obj as DataImportModel;
            Tool.ThreadProcess.Threads.Add(string.Format("{0}sensor", model.xmname));
            string mssg = "";
            var sensorFileUploadConditionModel = new SensorFileUploadConditionModel(model.xmname,model.xmno ,model.path);
            if (!ProcessResultDataBLL(sensorFileUploadConditionModel, out mssg))
            {
                return;
            }
            else
            {

                //List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
                //emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
            }
            Tool.ThreadProcess.Threads.Remove(string.Format("{0}sensor", model.xmname));


        }
        public class DataImportModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string path { get; set; }
            public DataImportModel(string xmname, int xmno, string path)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
            }
        }



        /// <summary>
        /// 传感器数据导入处理主程序处理参数实体类
        /// </summary>
        public class ProcessFileDecodeTxtModel : SensorFileUploadConditionModel
        {
            /// <summary>
            /// 文件类型
            /// </summary>
            public string fileType { get; set; }
            public Thread thread { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessFileDecodeTxtModel(string xmname, int xmno, string path, string fileType)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
                this.fileType = fileType;
            }

        }


    }
}