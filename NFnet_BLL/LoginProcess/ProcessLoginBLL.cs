﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.UserProcess;

namespace NFnet_BLL.LoginProcess
{
    public class ProcessLoginBLL
    {
        /// <summary>
        /// 登陆验证
        /// </summary>
        /// <param name="model">登录参数对象</param>
        /// <param name="mssg">结果信息/错误信息</param>
        /// <returns>返回登录用户的个人信息currentUseInfo、用户的角色Role</returns>
        public static bool ProcessLogin(ProcessLoginNGNModel model, out string mssg)
        {
            
            model.currentUseInfo = null;
            model.Role = Role.unknown;
            var administratrorModel = new ProcessAdministratrorBLL.ProcessAdministratorModel(model.name, model.password);
            var monitorModel = new ProcessMonitorBLL.ProcessMonitorModel(model.name, model.password);
            var superviseModel = new ProcessSuperviseBLL.ProcessSuperviseModel(model.name, model.password);


            if (ProcessAdministratrorBLL.AdministratrorLoginProcess(administratrorModel, out mssg))
            {
                model.currentUseInfo = administratrorModel.member;
                if (administratrorModel.member.role != "系统管理员")
                model.Role = Role.administratrorModel;
                else
                model.Role = Role.superAdministratrorModel;
                model.surveybase = administratrorModel.member.surveybase;
                return true;
                //return administratrorModel.member.role;

            }
            else if (ProcessMonitorBLL.MonitorLoginProcess(monitorModel, out mssg))
            {
                model.currentUseInfo = monitorModel.member;
                //return monitorModel.member.role;
                model.surveybase = monitorModel.member.surveybase;
                model.Role = Role.monitorModel;
                return true;
            }
            else if (ProcessSuperviseBLL.SuperviseLoginProcess(superviseModel, out mssg))
            {
                model.currentUseInfo = superviseModel.member;
                //return superviseModel.member.role;
                model.surveybase = superviseModel.member.surveybase;
                model.Role = Role.superviseModel;
                return true;
            }
            else
            {
                //return "false";
                return false;
            }
        }

        /// <summary>
        /// 登陆验证
        /// </summary>
        /// <param name="model">登录参数对象</param>
        /// <param name="mssg">结果信息/错误信息</param>
        /// <returns>返回登录用户的个人信息currentUseInfo、用户的角色Role</returns>
        public static bool ProcessAdministratorLogin(ProcessLoginNGNModel model, out string mssg)
        {

            model.currentUseInfo = null;
            model.Role = Role.unknown;
            var administratrorModel = new ProcessAdministratrorBLL.ProcessAdministratorModel(model.name, model.password);
            var monitorModel = new ProcessMonitorBLL.ProcessMonitorModel(model.name, model.password);
            var superviseModel = new ProcessSuperviseBLL.ProcessSuperviseModel(model.name, model.password);


            if (ProcessAdministratrorBLL.AdministratrorLoginProcess(administratrorModel, out mssg))
            {
                model.currentUseInfo = administratrorModel.member;
                if (administratrorModel.member.role != "系统管理员")
                    model.Role = Role.administratrorModel;
                else
                    model.Role = Role.superAdministratrorModel;
                model.surveybase = administratrorModel.member.surveybase;
                return true;
                //return administratrorModel.member.role;

            }
           
            else
            {
                //return "false";
                return false;
            }
        }


        /// <summary>
        /// 登录实体类
        /// </summary>
        public class ProcessLoginNGNModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 密码
            /// </summary>
            public string password { get; set; }
            /// <summary>
            /// 用户信息对象
            /// </summary>
            public object currentUseInfo { get; set; }
            /// <summary>
            /// 用户角色
            /// </summary>
            public Role Role { get; set; }
            public bool surveybase { get; set; }
            public ProcessLoginNGNModel(string name, string password)
            {
                this.name = name;
                this.password = password;

            }
        }
        /// <summary>
        /// 获取当前用户的最新项目名称
        /// </summary>
        /// <param name="model">获取最新项目的参数类</param>
        /// <param name="mssg"></param>
        /// <returns>最新项目的名称</returns>
        public static bool ProcessRoleNearestXm(ProcessRoleNearestXmModel model, out string mssg)
        {
            var adminstratorXmProcessModel = new ProcessAdministratrorBLL.ProcessXmModel(model.name);
            var monitorXmProcessModel = new ProcessMonitorBLL.ProcessXmModel(model.name);
            var superviseXmProcess = new ProcessSuperviseBLL.ProcessXmModel(model.name);
            string xmname = "";
            mssg = "";
            switch (model.role)
            {

                case Role.administratrorModel:
                    if (ProcessAdministratrorBLL.GetAdminstratorXmProcess(adminstratorXmProcessModel, out mssg))
                    {
                        model.xmname = adminstratorXmProcessModel.xmname;
                        return true;
                    }
                    break;
                case Role.monitorModel:
                    if (ProcessMonitorBLL.GetMonitorXmProcess(monitorXmProcessModel, out mssg))
                    {
                        model.xmname = monitorXmProcessModel.xmname;
                        return true;
                    }
                    break;
                case Role.superviseModel:
                    if (ProcessSuperviseBLL.GetSuperviseXmProcess(superviseXmProcess, out mssg))
                    {
                        model.xmname = superviseXmProcess.xmname;
                        return true;
                    }
                    break;


            }
            return false;
        }
        /// <summary>
        /// 获取角色最近项目的参数类
        /// </summary>
        public class ProcessRoleNearestXmModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 角色
            /// </summary>
            public Role role { get; set; }
            public string xmname { get; set; }
            public ProcessRoleNearestXmModel(string name, Role role)
            {
                this.name = name;
                this.role = role;
            }
        }




    }
}