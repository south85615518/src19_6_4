﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace NFnet_BLL.LoginProcess
{
    /// <summary>
    /// 登录票据业务逻辑处理类
    /// </summary>
    public class ProcessAuthTicketBLL
    {
        /// <summary>
        /// 票据生成
        /// </summary>
        /// <param name="model"></param>
        public static void scurity(ProcessAuthTicketModel model)
        {
            //ActiveDirectoryMembershipProvider
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
            1, model.name, DateTime.Now, DateTime.Now.AddMinutes(30),
            false, model.userHostAddress);
            //将票据加密 
            string authTicket = FormsAuthentication.Encrypt(ticket);
            //将加密后的票据保存为cookie 
            HttpCookie coo = new HttpCookie(FormsAuthentication.FormsCookieName, authTicket);
            model.coo = coo;
            
        }
        /// <summary>
        /// 票据生成类
        /// </summary>
        public class ProcessAuthTicketModel {
            public string name{get;set;}
            public string userHostAddress {get;set; }
            public HttpCookie coo { get; set; }
            public ProcessAuthTicketModel(string name,string hostAddress)
            {
                this.name = name;
                this.userHostAddress = hostAddress;
            }
        }
    }
}