﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.LoginProcess
{
    /// <summary>
    /// 角色枚举
    /// </summary>
    public enum Role
    {
        superAdministratrorModel = 0,
        administratrorModel =1,
        monitorModel = 2,
        superviseModel = 3,
        unknown = 99
    }//

}