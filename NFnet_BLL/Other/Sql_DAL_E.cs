﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;
using System.Data.Odbc;
using NFnet_DAL;
using NFnet_MODAL;
using System.IO;
using MySql.Data.MySqlClient;
namespace NFnet_BLL
{
    public class Sql_DAL_E : Sql_Bll_E
    {
        public static accessdbse adb = new accessdbse();
        public static database db = new database();
        /// <summary>
        /// 单位机构信息添加
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public string UnitInsertExcute(Unit unit)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_i();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            cmd.CommandText = sql;
            cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            cmd.Parameters.Add("@pro", OleDbType.VarChar).Value = unit.Pro;
            cmd.Parameters.Add("@city", OleDbType.VarChar).Value = unit.City;
            cmd.Parameters.Add("@country", OleDbType.VarChar).Value = unit.Country;
            cmd.Parameters.Add("@adr", OleDbType.VarChar).Value = unit.Addr;
            cmd.Parameters.Add("@tel", OleDbType.VarChar).Value = unit.Tel;
            cmd.Parameters.Add("@email", OleDbType.VarChar).Value = unit.Email;
            cmd.Parameters.Add("@nature", OleDbType.VarChar).Value = unit.Natrue;
            cmd.Parameters.Add("@linkman", OleDbType.VarChar).Value = unit.Linkman;
            cmd.Parameters.Add("@aptitude", OleDbType.VarChar).Value = unit.Aptitude;
            cmd.Parameters.Add("@police", OleDbType.VarChar).Value = unit.Police;
            cmd.Parameters.Add("@taxproof", OleDbType.VarChar).Value = unit.Taxproof;
            cmd.Parameters.Add("@state", OleDbType.VarChar).Value = "未通过审核";
            cmd.Parameters.Add("@createTime", OleDbType.Date).Value = DateTime.Now.Date;
            cmd.Parameters.Add("@dbname", OleDbType.VarChar).Value = unit.DbName;
            updatedb udb = new updatedb();
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }
        /// <summary>
        /// 单位机构信息修改
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public string UnitUpdateExcute(Unit unit)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_u();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand cmd = new OleDbCommand(sql, conn);

            sql = sql.Replace("@unitName", "'" + unit.UnitName + "'");
            sql = sql.Replace("@pro", "'" + unit.Pro + "'");
            sql = sql.Replace("@city", "'" + unit.City + "'");
            sql = sql.Replace("@country", "'" + unit.Country + "'");
            sql = sql.Replace("@addr", "'" + unit.Addr + "'");
            sql = sql.Replace("@tel", "'" + unit.Tel + "'");
            sql = sql.Replace("@email", "'" + unit.Email + "'");
            sql = sql.Replace("@natrue", "'" + unit.Natrue + "'");
            sql = sql.Replace("@linkman", "'" + unit.Linkman + "'");
            sql = sql.Replace("@aptitude", "'" + unit.Aptitude + "'");
            sql = sql.Replace("@police", "'" + unit.Police + "'");
            sql = sql.Replace("@taxproof", "'" + unit.Taxproof + "'");
            cmd.CommandText = sql;
            updatedb udb = new updatedb();
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }


        public Unit UnitSelectExcute(Unit unit)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_s();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@unitName", "'" + unit.UnitName + "'");
            //OleDbCommand cmd = new OleDbCommand(sql,conn);
            //cmd.CommandText = sql;
            //cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            List<string> ls = querysql.queryaccesslist(sql);

            foreach (string str in ls)
            {
                string[] vals = str.Split(',');
                Unit unitEntrl = new Unit
                {
                    UnitName = unit.UnitName,
                    Pro = vals[0],
                    City = vals[1],
                    Country = vals[2],
                    Addr = vals[3],
                    Tel = vals[4],
                    Email = vals[5],
                    Natrue = vals[6],
                    Linkman = vals[7],
                    Aptitude = vals[8],
                    Police = vals[9],
                    Taxproof = vals[10],
                    State = vals[11],
                    DbName = vals[12]

                };
                return unitEntrl;
            }
            return null;
        }
        /// <summary>
        /// 机构信息列表
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        public List<Unit> UnitSelectListExcute(Unit unit)
        {
            Sql_Bll bll = new Sql_Bll();
            List<Unit> lu = new List<Unit>();
            string sql = bll.GetUnitSql_ls();
            OleDbConnection conn = adb.getconn();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add("@unitName", OleDbType.VarChar).Value = unit.UnitName;
            //DataTable dt = 
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = {"unitName",
            "pro ", 
            "addr", 
            "aptitude",
            "city", 
            "country", 
            "email", 
            "linkman",
            "natrue", 
            " police",
            " taxproof",
            "tel", 
            };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);

            foreach (List<string> ls in lls)
            {
                Unit unitEntrl = new Unit
                {
                    UnitName = ls[0],
                    Pro = ls[1],
                    Addr = ls[2],
                    Aptitude = ls[3],
                    City = ls[4],
                    Country = ls[5],
                    Email = ls[6],
                    Linkman = ls[7],
                    Natrue = ls[8],
                    Police = ls[9],
                    Taxproof = ls[10],
                    Tel = ls[11]
                };
                lu.Add(unitEntrl);
            }
            return lu;
        }

        /// <summary>
        /// 人员信息添加
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public string MemberInsertExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_i();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@userId", "'" + member.UserId + "'");
            sql = sql.Replace("@pass", "'" + member.Password + "'");
            sql = sql.Replace("@role", "'" + member.Role + "'");
            sql = sql.Replace("@userGroup", "'" + member.UserGroup + "'");
            sql = sql.Replace("@userName", "'" + member.UserName + "'");
            sql = sql.Replace("@workNo", "'" + member.WorkNo + "'");
            sql = sql.Replace("@position", "'" + member.Position + "'");
            sql = sql.Replace("@tel", "'" + member.Tel + "'");
            sql = sql.Replace("@email", "'" + member.Email + "'");
            sql = sql.Replace("@zczsmc", "'" + member.Zczsmc + "'");
            sql = sql.Replace("@zczsbh", "'" + member.Zczsbh + "'");
            sql = sql.Replace("@zczs", "'" + member.Zczs + "'");
            sql = sql.Replace("@sgzsmc", "'" + member.Sgzsmc + "'");
            sql = sql.Replace("@sgzsbh", "'" + member.Sgzsbh + "'");
            sql = sql.Replace("@sgzs", "'" + member.Sgzs + "'");
            sql = sql.Replace("@unitName", "'" + member.UnitName + "'");
            sql = sql.Replace("@createTime", "'" + DateTime.Now.Date + "'");
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            //cmd.CommandText = sql;
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }
        /// <summary>
        /// 人员信息更新
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public string MemberUpdateExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_u();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@userId", "'" + member.UserId + "'");
            sql = sql.Replace("@pass", "'" + member.Password + "'");
            sql = sql.Replace("@role", "'" + member.Role + "'");
            sql = sql.Replace("@userGroup", "'" + member.UserGroup + "'");
            sql = sql.Replace("@userName", "'" + member.UserName + "'");
            sql = sql.Replace("@workNo", "'" + member.WorkNo + "'");
            sql = sql.Replace("@position", "'" + member.Position + "'");
            sql = sql.Replace("@tel", "'" + member.Tel + "'");
            sql = sql.Replace("@email", "'" + member.Email + "'");
            sql = sql.Replace("@zczsmc", "'" + member.Zczsmc + "'");
            sql = sql.Replace("@zczsbh", "'" + member.Zczsbh + "'");
            sql = sql.Replace("@zczs", "'" + member.Zczs + "'");
            sql = sql.Replace("@sgzsmc", "'" + member.Sgzsmc + "'");
            sql = sql.Replace("@sgzsbh", "'" + member.Sgzsbh + "'");
            sql = sql.Replace("@sgzs", "'" + member.Sgzs + "'");
            sql = sql.Replace("@unitName", "'" + member.UnitName + "'");
            sql = sql.Replace("@createTime", "'" + DateTime.Now.Date + "'");
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }
        /// <summary>
        /// 人员信息获取
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public member MemberSelectExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_s();
            OleDbConnection conn = adb.getconn();
            sql = sql.Replace("@userId", "'" + member.UserId + "'");
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "userId", 
            "pass", 
            "role", 
            "userGroup", 
            "userName", 
            "workNo", 
            "position", 
            "tel", 
            "zczsmc", 
            "zczsbh", 
            "zczs", 
            "sgzsmc",
            "sgzsbh",
            "sgzs",
            "unitName",
             "email"};
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<member> lds = new List<member>();
            foreach (List<string> ls in lls)
            {
                member memberEntrl = new member
                {
                    UserId = ls[0],
                    Password = ls[1],
                    Role = ls[2],
                    UserGroup = ls[3],
                    UserName = ls[4],
                    WorkNo = ls[5],
                    Position = ls[6],
                    Tel = ls[7],
                    Zczsmc = ls[8],
                    Zczsbh = ls[9],
                    Zczs = ls[10],
                    Sgzsmc = ls[11],
                    Sgzsbh = ls[12],
                    Sgzs = ls[13],
                    UnitName = ls[14],
                    Email = ls[15]

                };
                return memberEntrl;
            }
            return null;
        }
        /// <summary>
        /// 人员信息列表获取
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public List<member> MemberSelectListExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_s();
            List<member> lm = new List<member>();
            OleDbConnection conn = adb.getconn();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add("@userId", OleDbType.VarChar).Value = member.UserId;
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "userId", 
            "password", 
            "role", 
            "userGroup", 
            "userName", 
            "workNo", 
            "position", 
            "tel", 
            "zczsmc", 
            "zczsbh", 
            "zczs", 
            "sgzsmc",
            "sgzsbh",
            "sgzs",
            "unitName" };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<member> lds = new List<member>();
            foreach (List<string> ls in lls)
            {
                member memberEntrl = new member
                {
                    UserId = ls[0],
                    Password = ls[1],
                    Role = ls[2],
                    UserGroup = ls[3],
                    UserName = ls[4],
                    WorkNo = ls[5],
                    Position = ls[6],
                    Tel = ls[7],
                    Zczsmc = ls[8],
                    Zczsbh = ls[9],
                    Zczs = ls[10],
                    Sgzsmc = ls[11],
                    Sgzsbh = ls[12],
                    Sgzs = ls[13],
                    UnitName = ls[14]

                };
                lds.Add(memberEntrl);
            }
            return lds;

        }


        public DataTable MemberTableExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_t();
            List<member> lm = new List<member>();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@unitName", "'" + member.UnitName + "'");
            DataTable dt = new DataTable();
            OleDbDataAdapter adpter = new OleDbDataAdapter(sql, conn);
            try
            {
                adpter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, "人员信表获取出错");
            }
            finally
            {
                conn.Close();
            }
            return null;
        }


        public member MemberLoginExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.MemberLoginSql_s();
            OleDbConnection conn = adb.getconn();

            sql = sql.Replace("@userId", "'" + member.UserId + "'");
            sql = sql.Replace("@pass", "'" + member.Password + "'");
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "userId", 
            "pass", 
            "role", 
            "userGroup", 
            "userName", 
            "workNo", 
            "position", 
            "tel", 
            "zczsmc", 
            "zczsbh", 
            "zczs", 
            "sgzsmc",
            "sgzsbh",
            "sgzs",
            "unitName" };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<member> lds = new List<member>();
            foreach (List<string> ls in lls)
            {
                member memberEntrl = new member
                {
                    UserId = ls[0],
                    Password = ls[1],
                    Role = ls[2],
                    UserGroup = ls[3],
                    UserName = ls[4],
                    WorkNo = ls[5],
                    Position = ls[6],
                    Tel = ls[7],
                    Zczsmc = ls[8],
                    Zczsbh = ls[9],
                    Zczs = ls[10],
                    Sgzsmc = ls[11],
                    Sgzsbh = ls[12],
                    Sgzs = ls[13],
                    UnitName = ls[14]

                };
                return memberEntrl;
            }
            return null;
        }


        public DataTable UnitTableExcute(Unit member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitSql_t();
            List<member> lm = new List<member>();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            DataTable dt = new DataTable();
            OleDbDataAdapter adpter = new OleDbDataAdapter(sql, conn);
            try
            {
                adpter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, "机构信息表获取出错");
            }
            finally
            {
                conn.Close();
            }
            return null;
        }


        public string InstrumentInsertExcute(Instrument instrument)
        {

            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetMemberSql_i();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            sql = sql.Replace("@instrumentName", "'" + instrument.InstrumentName + "'");
            sql = sql.Replace("@vender", "'" + instrument.Vender + "'");
            sql = sql.Replace("@instrumentNo ", "'" + instrument.InstrumentNo + "'");
            sql = sql.Replace("@instrumentType ", "'" + instrument.InstrumentType + "'");
            sql = sql.Replace("@verificationDate ", "'" + instrument.VerificationDate + "'");
            sql = sql.Replace("@validityPeriod ", "'" + instrument.ValidityPeriod + "'");
            sql = sql.Replace("@verificationProof ", "'" + instrument.VerificationProof + "'");
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            //cmd.CommandText = sql;

            updatedb udb = new updatedb();
            try
            {
                cmd.ExecuteNonQuery();
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }


            return "";
        }

        public string InstrumentUpdateExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        public DataTable InstrumentTableExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        public Instrument InstrumentSelectExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        public List<Instrument> InstrumentSelectListExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        public Instrument InstrumentLoginExcute(Instrument Instrument)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 数据库列表加载
        /// </summary>
        /// <returns></returns>
        public List<string> DbListExcute()
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.dbNameSql_s();
            OdbcConnection conn = db.GetXmConn("information_schema");
            List<string> ls = querysql.querystanderlist(sql, conn);
            return ls;
        }



        public List<string> ProjectNameExcute(Unit unit)
        {

            Sql_Bll bll = new Sql_Bll();
            //unit = UnitSelectExcute(unit);
            string sql = bll.taskSql_s();

            OdbcConnection conn = null;
            try
            {
                conn = db.GetXmConn(unit.DbName);
                List<string> ls = querysql.querystanderlist(sql, conn);
                return ls;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return null;
            }
        }

        public List<string> ProjectNameExcute()
        {
            throw new NotImplementedException();
        }

        public string projectInfoInsertExcute(xm xminfo)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.projectInfoSql_u();
            sql = sql.Replace("@xmname", "'" + xminfo.Xmname + "'");
            sql = sql.Replace("@jcdw", "'" + xminfo.Jcdw + "'");
            sql = sql.Replace("@dbase", "'" + xminfo.Dbase + "'");
            updatedb udb = new updatedb();
            try
            {
                udb.updateaccess(sql);
                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return ex.Message;
            }
        }


        void Sql_Bll_E.projectInfoInsertExcute(xm xminfo)
        {
            throw new NotImplementedException();
        }


        public List<string> StudyPointExcute(jcxmAndjcd jclxandjcd, string sql)
        {
            Sql_Bll bll = new Sql_Bll();
            OdbcConnection conn = db.GetStanderConn(jclxandjcd.Xmname);
            List<string> ls = querysql.querystanderlist(sql, conn);
            return ls;
        }


        public List<string> StudyPointExcute(jcxmAndjcd xminfo)
        {
            throw new NotImplementedException();
        }


        public xm XmInfoExcute(xm xminfo)
        {
            return null;
        }


        public string NearestXmnameExcute(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = "";
            member = MemberSelectExcute(member);
            if (member.Position == "系统管理员")
            {
                sql = bll.CGNearest_s();

            }
            else
            {
                sql = bll.NearestSql_s();
                sql = sql.Replace("@userId", "'" + member.UserId + "'");
            }
            List<string> xmnameList = querysql.queryaccesslist(sql);
            if (xmnameList != null)
                return xmnameList[0];
            return "";
        }


        public List<string> GMXmnameList(member member)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GMXmnamesql_s();
            sql = sql.Replace("@jcdw", "'" + member.UnitName + "'");
            List<string> ls = querysql.queryaccesslist(sql);
            return ls;
        }

        public List<member> UnitMemberExcute(string unitName)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.GetUnitMemberSql_ls();
            List<member> lm = new List<member>();
            sql = sql.Replace("@unitName", "'" + unitName + "'");
            querysql qs = new querysql();
            DataTable dt = qs.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "userId", 
            "pass", 
            "role", 
            "userGroup", 
            "userName", 
            "workNo", 
            "position", 
            "tel", 
            "zczsmc", 
            "zczsbh", 
            "zczs", 
            "sgzsmc",
            "sgzsbh",
            "sgzs",
            "unitName" };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<member> lds = new List<member>();
            foreach (List<string> ls in lls)
            {
                member memberEntrl = new member
                {
                    UserId = ls[0],
                    Password = ls[1],
                    Role = ls[2],
                    UserGroup = ls[3],
                    UserName = ls[4],
                    WorkNo = ls[5],
                    Position = ls[6],
                    Tel = ls[7],
                    Zczsmc = ls[8],
                    Zczsbh = ls[9],
                    Zczs = ls[10],
                    Sgzsmc = ls[11],
                    Sgzsbh = ls[12],
                    Sgzs = ls[13],
                    UnitName = ls[14]

                };
                lds.Add(memberEntrl);
            }
            return lds;
        }

        /// <summary>
        /// 
        /// </summary>
        public string UnitDbCreate(string dbname,string path)
        {
            try
            {


                dbcreate dbc = new dbcreate();
                dbc.createdb(dbname);
                //string sql = " SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'fmosdata'";
                MySqlConnection conn = TargetBaseLink.GetMysqlConnect(dbname);
                FileInfo file = new FileInfo(path);  //filename是sql脚本文件路径。  
                string sql = file.OpenText().ReadToEnd();
                MySqlScript script = new MySqlScript(conn);
                script.Query = sql;
                int count = script.Execute();
                //textbox_log.Text += "Executed " + count + " statement(s)\r\n";
                //textbox_log.Text += "Delimiter: " + script.Delimiter + "\r\n"; 
                //List<string> tabNames = querysql.querystanderlist(sql, conn);
                //updatedb update = new updatedb();
                //foreach (string tabname in tabNames)
                //{
                //    sql = "CREATE TABLE " + dbname + "." + tabname + " SELECT * FROM fmosdata." + tabname + "  WHERE 1=2";
                //    update.UpdateStanderDB(sql, conn);

                //}
                ////增加字段属性
                //List<string> attrAddSql = new List<string>();
                //PrimaryKeyAdd(dbname);
                //foreach (string attrsql in attrAddSql)
                //{

                //    update.UpdateStanderDB(attrsql, conn);

                //}
                return "1";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return "-1";
            }

        }
        /// <summary>
        /// 为数据库表增加主键
        /// </summary>
        public void PrimaryKeyAdd(string dbname)
        {
            List<string> cmds = new List<string>();
            //预警值表
            cmds.Add("alter table fmos_alarmvalue add primary key(name)");
            //结果数据表
            cmds.Add("alter table fmos_cycdirnet add primary key(point_name,taskName,time)");
            //原始数据表
            cmds.Add("alter table fmos_orgldata add primary key(point_name,taskName,search_time)");
            //学习点表
            cmds.Add("alter table fmos_studypoint add primary key(point_name,taskName)");
            //测站
            cmds.Add("alter table fmos_stationinfo add primary key(station_name,taskName)");
            //时间
            cmds.Add("alter table fmos_timetask add primary key(DateTaskName,taskName)");
            //设置
            cmds.Add("alter table fmos_setting add primary key(name,taskName)");
            //点组
            cmds.Add("alter table fmos_pointsinteam add primary key(team_Name,point_name,xmno)");
            //仪器
            cmds.Add("alter table fmos_instrumentparam add primary key(name,xmno)");
            //
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            foreach (string cmdsql in cmds)
            {
                updatedb udb = new updatedb();
                udb.UpdateStanderDB(cmdsql,conn);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns>1表示插入成功,0表示该预警值名称已经存在,-1表示程序出错</returns>
        public int AlarmValueInsert(alarmvalue alarm, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.AlarmValueSql_i();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@name", "'" + alarm.Name + "'");
            sql = sql.Replace("@this_rn", "'" + alarm.This_rn + "'");
            sql = sql.Replace("@this_re", "'" + alarm.This_re + "'");
            sql = sql.Replace("@this_rz", "'" + alarm.This_rz + "'");
            sql = sql.Replace("@ac_rn", "'" + alarm.Ac_rn + "'");
            sql = sql.Replace("@ac_re", "'" + alarm.Ac_re + "'");
            sql = sql.Replace("@ac_rz", "'" + alarm.Ac_rz + "'");
            //判断
            string sqlExist = "select count(*) from fmos_alarmvalue where name='"+alarm.Name+"'";
            try
            {
                string cont = querysql.querystanderstr(sqlExist, conn);
                if (cont == "0")
                {
                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(sql, conn);
                    return 1;
                }
                return 0;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("添加预警值出错");
                return -1;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns>1表示更新成功,-1表示程序出错</returns>
        public int AlarmValueUpdate(alarmvalue alarm, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.AlarmValueSql_u();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@name", "'" + alarm.Name + "'");
            sql = sql.Replace("@this_rn", "'" + alarm.This_rn + "'");
            sql = sql.Replace("@this_re", "'" + alarm.This_re + "'");
            sql = sql.Replace("@this_rz", "'" + alarm.This_rz + "'");
            sql = sql.Replace("@ac_rn", "'" + alarm.Ac_rn + "'");
            sql = sql.Replace("@ac_re", "'" + alarm.Ac_re + "'");
            sql = sql.Replace("@ac_rz", "'" + alarm.Ac_rz + "'");
            try
            {

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(sql, conn);
                return 1;

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("更新预警值出错");
                return -1;
            }
        }


        public DataTable AlarmValueSelect(string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.AlarmValueSql_s();
            OdbcConnection conn = db.GetStanderConn(xmname);
            try
            {
                DataTable dt = querysql.querystanderdb(sql, conn);
                return dt;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("显示所有的预警值出错");
                return null;
            }
        }


       public  List<string> AlarmValueListSelect(string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.AlarmValueSql_s();
            OdbcConnection conn = db.GetStanderConn(xmname);
            try
            {
                List<string> ls = querysql.querystanderlist(sql, conn);
                return ls;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("显示所有的预警名称出错");
                return null;
            }
        }


        List<string> Sql_Bll_E.AlarmValueListSelect(string xmname)
        {
            throw new NotImplementedException();
        }


        public int PointAlarmValueInsert(pointalarmvalue alarm, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.PointAlarmValueSql_i();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + alarm.TaskName + "'");
            sql = sql.Replace("@pointtype", "'" + alarm.Pointtype + "'");
            sql = sql.Replace("@point_name", "'" + alarm.PointName + "'");
            sql = sql.Replace("@firstAlarmName", "'" + alarm.FirstAlarmName + "'");
            sql = sql.Replace("@secondAlarmName", "'" + alarm.SecondAlarmName + "'");
            sql = sql.Replace("@thirdAlarmName", "'" + alarm.ThirdAlarmName + "'");
            sql = sql.Replace("@remark", "'" + alarm.Remark + "'");
            //判断
            string sqlExist = "select count(*) from fmos_pointalarmvalue where taskName='"+alarm.TaskName+"' and point_name = '"+alarm.PointName+"' ";
            try
            {
                string cont = querysql.querystanderstr(sqlExist, conn);
                if (cont == "0")
                {
                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(sql, conn);
                    return 1;
                }
                return 0;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("添加预警值出错");
                return -1;
            }
        }

        public int PointAlarmValueUpdate(pointalarmvalue alarm, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.PointAlarmValueSql_u();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + alarm.TaskName + "'");
            sql = sql.Replace("@point_name", "'" + alarm.PointName + "'");
            sql = sql.Replace("@firstAlarmName", "'" + alarm.FirstAlarmName + "'");
            sql = sql.Replace("@secondAlarmName", "'" + alarm.SecondAlarmName + "'");
            sql = sql.Replace("@thirdAlarmName", "'" + alarm.ThirdAlarmName + "'");
            sql = sql.Replace("@remark", "'" + alarm.Remark + "'");
            try
            {

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(sql, conn);
                return 1;

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("更新点号预警值出错");
                return -1;
            }
        }


        public int PointAlarmValueEmptyInsert(string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.PointAlarmValueSqlEmpty_i();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + xmname + "'");
            try
            {

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(sql, conn);
                return 1;

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("执行点号预警的空插入语句出错");
                return -1;
            }
        }


        public int PointAlarmValueIncrement(string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.PointAlarmValueSqlIncreament_i();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + xmname + "'");
            try
            {

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(sql, conn);
                return 1;

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("执行点号预警的新增插入语句出错");
                return -1;
            }
            return 0;
        }


        public int AlarmValueDel(alarmvalue alarm, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.AlarmValueSql_del();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@name", "'" + alarm.Name + "'");
            try
            {

                updatedb udb = new updatedb();

                int result = udb.UpdateStanderDB(sql, conn);
                 PointAlarmValueDelCasc(alarm, xmname);
                 return result;
                

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("执行点号预警删除语句出错");
                return -1;
            }
           
        }


        public int PointAlarmValueDel(pointalarmvalue alarm, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.PointAlarmValueSql_del();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@point_name", "'" + alarm.PointName + "'");
            sql = sql.Replace("@taskName", "'" + alarm.TaskName + "'");
            try
            {

                updatedb udb = new updatedb();
                return udb.UpdateStanderDB(sql, conn);


            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("执行点号预警删除语句出错");
                return -1;
            }
           
        }

        public int PointAlarmValueDelCasc(alarmvalue alarm, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            List<string> cascSql = bll.PointAlarmDelCasc();
            OdbcConnection conn = db.GetStanderConn(xmname);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@taskName", "'" + xmname + "'");
                temp = temp.Replace("@firstAlarmName", "'" + alarm.Name + "'");
                temp = temp.Replace("@secondAlarmName", "'" + alarm.Name + "'");
                temp = temp.Replace("@thirdAlarmName", "'" + alarm.Name + "'");
                try
                {

                    ExceptionLog.ExceptionWrite("级联删除点号预警"+temp);
                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(temp, conn);


                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("执行点号预警删除语句出错");
                    return -1;
                }
            }
            return 0;
        }


        public int PointAlarmValueMultilUpdate(pointalarmvalue alarm, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.PointAlarmValueSql_Multiu();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@id", "'" + alarm.Id + "'");
            sql = sql.Replace("@pointname", "'" + alarm.PointName + "'");
            sql = sql.Replace("@taskName", "'" + alarm.TaskName + "'");
            sql = sql.Replace("@firstAlarmName", "'" + alarm.FirstAlarmName + "'");
            sql = sql.Replace("@secondAlarmName", "'" + alarm.SecondAlarmName + "'");
            sql = sql.Replace("@thirdAlarmName", "'" + alarm.ThirdAlarmName + "'");
            sql = sql.Replace("@remark", "'" + alarm.Remark + "'");
            try
            {

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(sql, conn);
                return 1;

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("批量更新点号预警值出错");
                return -1;
            }
        }

        //学习点添加
        public int StudyPointInsert(studypoint point, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.StudyPointAdd_i();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@point_name","'"+point.Point_name+"'");
            sql = sql.Replace("@n", "'" + point.N + "'");
            sql = sql.Replace("@e", "'" + point.E + "'");
            sql = sql.Replace("@z", "'" + point.Z + "'");
            sql = sql.Replace("@orglHar", "'" + point.OrglHar+ "'");
            sql = sql.Replace("@orglVar", "'" + point.OrglVar + "'");
            sql = sql.Replace("@orglSd", "'" + point.OrglSd + "'");
            sql = sql.Replace("@stationName", "'" + point.StationName + "'");
            sql = sql.Replace("@onlyAngleUseful", "'" + point.OnlyAngleUseful + "'");
            sql = sql.Replace("@mileage", "'" + point.Mileage + "'");
            sql = sql.Replace("@iscontrolpoint", "'" + point.Iscontrolpoint + "'");
            sql = sql.Replace("@targetheight", "'" + point.Targethight + "'");
            sql = sql.Replace("@reflectorName", "'" + point.ReflectorName+ "'");
            sql = sql.Replace("@taskName", "'" + point.TaskName + "'");
            sql = sql.Replace("@search_time", "'" + point.Search_time + "'");
            sql = sql.Replace("@remark", "'" + point.Remark + "'");
            /*
             * insert into fmos_studypoint(id,point_name ,n,e,z,orglHar,orglVar,orglSd,stationName,search_time,onlyAngleUseful,mileage,iscontrolpoint,targethight,reflectorName,remark)values(@id,'ZX-01' ,@n,@e,@z,'3','3','3',@stationName,@search_time,@onlyAngleUseful,'2',@iscontrolpoint,@targethight,'2','3')
             */
            try
            {
                string sqlExit = "select count(*) from fmos_studypoint where point_name= '"+point.Point_name+"' and taskName = '"+point.TaskName+"'";
                string cont = querysql.querystanderstr(sqlExit,conn);
                if (cont == "0")
                {
                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(sql, conn);
                    return 1;
                }
                return 0;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("批量更新点号预警值出错");
                return -1;
            }
            return 0;
        }



        public int StudyPointUpdate(studypoint point, string xmname)
        {

            Sql_Bll bll = new Sql_Bll();
            string sql = bll.StudyPointSql_u();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@point_name", "'" + point.Point_name + "'");
            sql = sql.Replace("@n", "'" + point.N + "'");
            sql = sql.Replace("@e", "'" + point.E + "'");
            sql = sql.Replace("@z", "'" + point.Z + "'");
            sql = sql.Replace("@orglHar", "'" + point.OrglHar + "'");
            sql = sql.Replace("@orglVar", "'" + point.OrglVar + "'");
            sql = sql.Replace("@orglSd", "'" + point.OrglSd + "'");
            sql = sql.Replace("@stationName", "'" + point.StationName + "'");
            sql = sql.Replace("@onlyAngleUseful", "'" + point.OnlyAngleUseful + "'");
            sql = sql.Replace("@mileage", "'" + point.Mileage + "'");
            sql = sql.Replace("@iscontrolpoint", "'" + point.Iscontrolpoint + "'");
            sql = sql.Replace("@targetheight", "'" + point.Targethight + "'");
            sql = sql.Replace("@reflectorName", "'" + point.ReflectorName + "'");
            sql = sql.Replace("@taskName", "'" + point.TaskName + "'");
            sql = sql.Replace("@search_time", "'" + point.Search_time + "'");
            sql = sql.Replace("@remark", "'" + point.Remark + "'");
            try
            {
                
                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(sql, conn);
                    return 1;
              
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("批量更新点号预警值出错");
                return -1;
            }
            return 0;
        }


        public int StudyPointDelete(studypoint point, string xmname)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.StudyPointSql_del();
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@taskName", "'" + point.TaskName + "'");
            sql = sql.Replace("@point_name", "'" + point.Point_name + "'");
            try
            {

                updatedb udb = new updatedb();
                return udb.UpdateStanderDB(sql, conn);


            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("执行学习点删除语句出错");
                return -1;
            }
           
        }


        public int PointMutilInsert(string pointnamestr, string tabname, string columnname, string xmname,string columnxmname)
        {
            Sql_Bll bll = new Sql_Bll();
            
            OdbcConnection conn = db.GetStanderConn(xmname);
            string[] pointnames = pointnamestr.Split(',');
            string sql = "";
            int i = 0;
            foreach (string point in pointnames)
            {




                sql = bll.PointMultiSql_i();
                sql = sql.Replace("@columnname", columnname );
                sql = sql.Replace("@tbname", tabname );
                sql = sql.Replace("@pointname", "'"+point+"'");
                sql = sql.Replace("@columnxmname",  columnxmname);
                sql = sql.Replace("@xmname", "'" + xmname + "'");
                try
                {

                    string sqlcont = bll.PointRepeat_s();
                    sqlcont = sqlcont.Replace("@columnname",columnname );
                    sqlcont = sqlcont.Replace("@tbname", tabname);
                    sqlcont = sqlcont.Replace("@pointname", "'" + point + "'");
                    sqlcont = sqlcont.Replace("@columnxmname", columnxmname);
                    sqlcont = sqlcont.Replace("@xmname", "'" + xmname + "'");
                    string cont = querysql.querystanderstr(sqlcont, conn);
                    if (cont == "0")
                    {
                        updatedb udb = new updatedb();
                        udb.UpdateStanderDB(sql, conn);
                        //return 1;
                        i++;
                    }
                    //return 0;

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("执行点名插入语句出错");
                    return -1;
                }
            }
            return i;
        }


        public int JcmapSpotInsert(jcmap mp)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.jcmapspot_i();
            OdbcConnection conn = db.GetStanderConn(mp.TaskName);
            sql = sql.Replace("@taskName", "'" + mp.TaskName + "'");
            sql = sql.Replace("@pointName", "'" + mp.PointName + "'");
            sql = sql.Replace("@jclx", "'" + mp.Jclx + "'");
            sql = sql.Replace("@AbsX", "'" + mp.AbsX + "'");
            sql = sql.Replace("@AbsY", "'" + mp.AbsY + "'");
            //判断
            string sqlExist = "select count(*) from jcmapset where taskName='" + mp.TaskName + "' and pointName = '" + mp.PointName + "' ";
            try
            {
                string cont = querysql.querystanderstr(sqlExist, conn);
                if (cont == "0")
                {

                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(sql, conn);
                    return 1;
                }
                else
                {
                    string sqlupdate = bll.jcmapspot_u();
                    sqlupdate = sqlupdate.Replace("@taskName", "'" + mp.TaskName + "'");
                    sqlupdate = sqlupdate.Replace("@pointName", "'" + mp.PointName + "'");
                    sqlupdate = sqlupdate.Replace("@jclx", "'" + mp.Jclx + "'");
                    sqlupdate = sqlupdate.Replace("@AbsX", "'" + mp.AbsX + "'");
                    sqlupdate = sqlupdate.Replace("@AbsY", "'" + mp.AbsY + "'");
                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(sqlupdate, conn);
                    return 1;
                }
                //return 0;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("添加监测平面热点失败");
                return -1;
            }
        }


        public List<jcmap> JcmapSpotLoad(jcmap mp)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.jcmapspot_s();
            OdbcConnection conn = db.GetStanderConn(mp.TaskName);
            sql = sql.Replace("@taskName", "'" + mp.TaskName + "'");
            sql = sql.Replace("@jclx", "'" + mp.Jclx + "'");
            DataTable dt = querysql.querystanderdb(sql,conn);
            DataView dv = new DataView(dt);
            List<jcmap> ljp = new List<jcmap>();
            foreach(DataRowView drv in dv)
            {
                jcmap jcmap = new jcmap { 
                    PointName = drv["pointName"].ToString(), 
                    TaskName = mp.TaskName,
                    Jclx = mp.Jclx,
                    AbsX = drv["AbsX"].ToString(),
                    AbsY = drv["AbsY"].ToString()
                      
                };
                ljp.Add(jcmap);
            }
            return ljp;
        }



        public int JcmapSpotDelete(jcmap mp)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.jcmapspot_del();
            OdbcConnection conn = db.GetStanderConn(mp.TaskName);
            sql = sql.Replace("@pointName", "'" + mp.PointName + "'");
            sql = sql.Replace("@taskName", "'" + mp.TaskName + "'");
            sql = sql.Replace("@jclx", "'" + mp.Jclx + "'");
            try
            {

                updatedb udb = new updatedb();
                return udb.UpdateStanderDB(sql, conn);


            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("执行监测平面热点删除失败");
                return -1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabname">表名</param>
        /// <param name="attrs">属性名</param>
        /// <param name="values">属性值</param>
        /// <param name="repeatkey">区分字段组合</param>
        /// <param name="keyVals">区分字段组合值</param>
        /// <returns></returns>
        public List<string> dataImportInsert(string tabname, List<string> attrs, List<string> repeatkeys)
        {
            List<string> tempStrList = new List<string>();
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.dataimport_i();
            //OdbcConnection conn = db.GetStanderConn(xmname);
            //构建语句模板
            List<string> iAttrs = new List<string>();
            iAttrs.AddRange(attrs);
            iAttrs.AddRange(repeatkeys);

            string iAttrStr = string.Join(",", iAttrs);
            string iValueStr = "@" + string.Join(",@", iAttrs);
            List<string> insertList = new List<string>();
            insertList.Add("insert  into  ");
            insertList.Add(tabname);
            insertList.Add("(");
            insertList.Add(iAttrStr);
            insertList.Add(")");
            insertList.Add("values");
            insertList.Add("(");
            insertList.Add(iValueStr);
            insertList.Add(")");
            string insertTemp = string.Join("   ",insertList);
            tempStrList.Add(insertTemp);
            //构建更新语句
            List<string> uAttr = new List<string>();
            foreach(string attr in attrs)
            {
                uAttr.Add(attr+"=@"+attr);
            }
            string uAttrStr = string.Join(",",uAttr);
            List<string> uValue = new List<string>();
            foreach (string value in repeatkeys)
            {
                uValue.Add(value + "=@" + value);
            }
            string uValueStr = string.Join(" and ",uValue);
            List<string> updateList = new List<string>();
            updateList.Add("update  ");
            updateList.Add(tabname);
            updateList.Add("  set  ");
            updateList.Add(uAttrStr);
            updateList.Add("where");
            updateList.Add(uValueStr);
            string updateTemp = string.Join("   ", updateList);
            tempStrList.Add(updateTemp);
            return tempStrList;
            /*                tabname
            * attrFir(主键)   attrSec     attrThird 
            *      a            1        true/false
            *      
            * 插入语句
            * insert into tabname (attrFir,attrSec,attrThird) values(a,1,true);
            * 更新语句
            * update  tabname set attrSec = '1',attrThird = 'true' where attrFirst = 'a';
            * 
            * 参数
            * tabname           ----表名
            * attrSec,attrThird ----属性名
            * @attrSec,@attrThird            ----属性值
            * attrFir           ----主键名
            * @attrFir                 ----主键值
            * xmname            ----项目名称
            * 
            */




            //构建更新语句模板
        }


        public int ResultDataEditExcute(result result, string reportCyc)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.resultdataedit();
            OdbcConnection conn = db.GetStanderConn(result.TaskName);
            sql = sql.Replace("@pointname", "'" + result.PointName + "'");
            sql = sql.Replace("@taskName", "'" + result.TaskName + "'");
            sql = sql.Replace("@cyc", "'" + result.Cyc + "'");
            sql = sql.Replace("@reportCyc", "'" + reportCyc + "'");
            try
            {
                if (!IsResultRecord(result,reportCyc))
                {
                    updatedb udb = new updatedb();
                    return udb.UpdateStanderDB(sql, conn);
                }
                else {
                    return ResultDataRecordUpdate(result,reportCyc);
                }


            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("执行结果数据编辑出错");
                return -1;
            }
        }


        public bool IsResultRecord(result result,string reportCyc)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.IsResultRecordExits();
            OdbcConnection conn = db.GetStanderConn(result.TaskName);
            sql = sql.Replace("@pointname", "'" + result.PointName + "'");
            sql = sql.Replace("@taskName", "'" + result.TaskName + "'");
            sql = sql.Replace("@cyc", "'" + reportCyc + "'");
            try
            {

                string cont = querysql.querystanderstr(sql,conn);
                return cont == "0" ? false : true;

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("结果数据记录存在查询出错");
                return true;
            }
        }


        public int ResultDataRecordUpdate(result result,string reportCyc)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.ResultRecordUpdate();
            OdbcConnection conn = db.GetStanderConn(result.TaskName);
            sql = sql.Replace("@pointname", "'" + result.PointName + "'");
            sql = sql.Replace("@taskName", "'" + result.TaskName + "'");
            sql = sql.Replace("@cyc", "'" + result.Cyc + "'");
            sql = sql.Replace("@reportCyc", "'" + reportCyc + "'");
            try
            {

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(sql, conn);
                return 1;

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("结果数据记录更新出错");
                return -1;
            }
        }


        public bool IsResultRecord(result result)
        {
            throw new NotImplementedException();
        }


        public string ResultDataLowCheck(result result, string reportCyc, string time)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.ResultLawCheck();
            OdbcConnection conn = db.GetStanderConn(result.TaskName);
            sql = sql.Replace("@pointName", "'" + result.PointName + "'");
            sql = sql.Replace("@taskName", "'" + result.TaskName + "'");
            sql = sql.Replace("@cyc", "'" + result.Cyc + "'");
            sql = sql.Replace("@reportCyc", "'" + reportCyc + "'");
            sql = sql.Replace("@time", "'" + time + "'");
            try
            {

                DataTable dt = querysql.querystanderdb(sql,conn);
                if (dt.Rows.Count == 0)
                {
                    if (IsResultRecord(result, reportCyc))
                    {
                        return string.Format("点名{0}第{1}周期已经存在相同记录!",result.PointName,reportCyc);
                    }
                    
                    return "0";
                    
                }
                return string.Format("点名{0}第{1}周期的时间为{2}而您选择的第{3}周期的时间为{4}！", result.PointName, dt.Rows[0].ItemArray[0], dt.Rows[0].ItemArray[1],reportCyc,time);

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("结果数据记录更新出错");
                return "-1";
            }
        }


        public int ResultDataDel(result result)
        {
            Sql_Bll bll = new Sql_Bll();
            string sql = bll.ResultDataDel();
            OdbcConnection conn = db.GetStanderConn(result.TaskName);
            sql = sql.Replace("@pointName", "'" + result.PointName + "'");
            sql = sql.Replace("@taskName", "'" + result.TaskName + "'");
            sql = sql.Replace("@cyc", "'" + result.Cyc + "'");
            try
            {

                updatedb udb = new updatedb();
                return udb.UpdateStanderDB(sql, conn);


            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("执行成果数据删除出错");
                return -1;
            }
        }


        public string UnitDbCreate(string dbname)
        {
            throw new NotImplementedException();
        }
    }
}
