﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.Other
{
    public class QuerystanderdbModel
    {
        /// <summary>
        /// 查询SQL语句
        /// </summary>
        public string sql { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 查询结果
        /// </summary>
        public DataTable dt { get; set; }
        public List<string> columnnames;
        public QuerystanderdbModel(string sql, string xmname)
        {
            this.sql = sql;
            this.xmname = xmname;
        }
        public QuerystanderdbModel(string sql, string xmname, List<string> columnnames)
        {
            this.sql = sql;
            this.xmname = xmname;
            this.columnnames = columnnames;
        }
        public QuerystanderdbModel(string sql, int xmno)
        {
            this.sql = sql;
            this.xmno = xmno;
        }
    }
}