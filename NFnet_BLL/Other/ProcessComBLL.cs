﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SqlHelpers;
using Tool;

namespace NFnet_BLL.Other
{
    /// <summary>
    /// 公共业务逻辑处理类
    /// </summary>
    public class ProcessComBLL
    {
        /// <summary>
        /// 可空条件查询语句生成
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool Processquerynvl(Processquerynvlmodel model, out string mssg)
        {
            string str = "";
            mssg = "";

            if (SqlHelperBLL.querynvl(model.queryname, model.queryword, model.op, model.inserth, model.insertt, out str, out mssg))
            {
                model.str = str;
                return true;
            }
            else
            {
                return false;
            }



        }
        /// <summary>
        /// 可空条件查询语句生成类
        /// </summary>
        public class Processquerynvlmodel
        {

            public string queryname { get; set; }
            public string queryword/*查询的字段名*/{ get; set; }
            public string op { get; set; }
            public string inserth { get; set; }
            public string insertt { get; set; }
            public string str { get; set; }
            public Processquerynvlmodel(string queryname, string queryword, string op, string inserth, string insertt)
            {
                this.queryname = queryname;
                this.queryword = queryword;
                this.op = op;
                this.inserth = inserth;
                this.insertt = insertt;

            }
        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderstr(QuerystanderstrModel model, out string mssg)
        {
            string str = "";
            mssg = "";

            if (SqlHelperBLL.querystanderstr(model.sql, model.xmname, out str, out mssg))
            {
                model.str = str;
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 使用mysql远程连接查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processqueryremotedb(QueryremoteModel model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";

            if (SqlHelperBLL.queryremotedb(model.sql, model.dbname, out dt,out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 使用查询BKG转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderdb(QueryBKGModel model, out string mssg)
        {
            DataTable dt = null;
            querybkgsql bkgsql = new querybkgsql();
            mssg = "";
            model.dt = bkgsql.querytaccesstdb(model.sql);
            return true;
           
        }
        /// <summary>
        /// 使用查询BKG转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderdb(QueryReadBKGModel model, out string mssg)
        {
            DataTable dt = null;
            querybkgsql bkgsql = new querybkgsql();
            mssg = "";
            model.dt = bkgsql.querytbkgtdb(model.sql,model.lsname);
            return true;

        }
        /// <summary>
        /// 使用查询BKG转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderdb(QueryHXYLModel model, out string mssg)
        {
            DataTable dt = null;
            queryylsql hxylsql = new queryylsql();
            mssg = "";
            model.dt = hxylsql.querytaccesstdb(model.sql);
            return true;

        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderdb(QuerystanderdbIntModel model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";

            if (SqlHelperBLL.querystanderdb(model.sql, model.xmno, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 使用mysql远程连接查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderdb(QuerystanderdbModel model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";

            if (SqlHelperBLL.querystanderdb(model.sql, model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 使用mysql远程连接查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool ProcessquerystanderdbInreader(QuerystanderdbModel model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";

            if (SqlHelperBLL.querystanderdb(model.sql, model.xmname,model.columnnames ,out dt, out mssg))
            {
                ExceptionLog.ExceptionWrite(mssg);
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 处理快捷查询的值，生成sql语句
        /// </summary>
        /// <returns></returns>
        public static bool Processdateswdl(ProcessdateswdlModel model,out string mssg)
        {
            model.sttm = "";
            model.edtm = "";
            model.sql = "";
            mssg = "";
            switch (model.unit)
            {
                case "week":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddDays(-7).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        model.sql = " date_sub('"+model.maxTime+"',INTERVAL 1 week) <=" + model.dateword + " and '"+model.maxTime+"'>= " + model.dateword + "   order by #_point,#_cyc,#_date asc";
                        break;

                    }
                case "day":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddDays(-1).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        model.sql = "  date_sub('"+model.maxTime+"',INTERVAL 1 day) <=" + model.dateword + " and '"+model.maxTime+"'>= " + model.dateword + "  order by #_point,#_cyc,#_date asc";
                        break;
                    }
                case "mon":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddMonths(-1).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        model.sql = "  DATE_SUB('"+model.maxTime+"',INTERVAL 1 month) <=" + model.dateword + " and '"+model.maxTime+"'>= " + model.dateword + " order by #_point,#_cyc,#_date asc";
                        break;
                    }
                case "threemon":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddMonths(-3).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        model.sql = " DATE_SUB('"+model.maxTime+"',INTERVAL 3 month) <=" + model.dateword + " and '"+model.maxTime+"'>= " + model.dateword + " order by #_point,#_cyc,#_date asc";
                        break;
                    }
                case "year":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddYears(-1).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        model.sql = "YEAR('"+model.maxTime+"') - yEAR(" + model.dateword + ")<=1 and '"+model.maxTime+"' >= " + model.dateword + " order by #_point,#_cyc,#_date asc ";
                        break;
                    }
                default: mssg = "没有该日期请求的处理条件";
                    break;
            }
            return true;
            //CycTeriminalSet(sttm, edtm, xmname);

        }
        /// <summary>
        /// 处理快捷查询的值，生成sql语句
        /// </summary>
        public class ProcessdateswdlModel
        {
            /// <summary>
            /// 起始日期
            /// </summary>
            public string sttm { get; set; }
            /// <summary>
            /// 结束日期
            /// </summary>
            public string edtm { get; set; }
            public string dateword/*表中的时间字段*/{ get; set; }
            /// <summary>
            /// 查询SQL语句
            /// </summary>
            public string sql { get; set; }
            /// <summary>
            /// 时间单位
            /// </summary>
            public string unit { get; set; }

            public DateTime maxTime { get; set; }

            public ProcessdateswdlModel(string dateword/*表中的时间字段*/, string unit, DateTime maxTime)
            {
                this.maxTime = maxTime;
                this.dateword = dateword;
                this.unit = unit;
            }
        }
        /// <summary>
        /// 处理快捷查询的值，生成sql语句
        /// </summary>
        /// <returns></returns>
        public static bool Processdatemdb(ProcessdatemdbModel model, out string mssg)
        {
            model.sttm = "";
            model.edtm = "";
            mssg = "";
            switch (model.unit)
            {
                case "week":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddDays(-7).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        //model.sql = " date_sub('" + model.maxTime + "',INTERVAL 1 week) <=" + model.dateword + " and '" + model.maxTime + "'>= " + model.dateword + "   order by #_point,#_cyc,#_date asc";
                        break;

                    }
                case "day":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddDays(-1).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        //model.sql = "  date_sub('" + model.maxTime + "',INTERVAL 1 day) <=" + model.dateword + " and '" + model.maxTime + "'>= " + model.dateword + "  order by #_point,#_cyc,#_date asc";
                        break;
                    }
                case "mon":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddMonths(-1).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        //model.sql = "  DATE_SUB('" + model.maxTime + "',INTERVAL 1 month) <=" + model.dateword + " and '" + model.maxTime + "'>= " + model.dateword + " order by #_point,#_cyc,#_date asc";
                        break;
                    }
                case "threemon":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddMonths(-3).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        //model.sql = " DATE_SUB('" + model.maxTime + "',INTERVAL 3 month) <=" + model.dateword + " and '" + model.maxTime + "'>= " + model.dateword + " order by #_point,#_cyc,#_date asc";
                        break;
                    }
                case "year":
                    {
                        model.sttm = Convert.ToDateTime(model.maxTime).AddYears(-1).ToString().Replace("/", "-");
                        model.edtm = Convert.ToDateTime(model.maxTime).ToString().Replace("/", "-");
                        //model.sql = "YEAR('" + model.maxTime + "') - yEAR(" + model.dateword + ")<=1 and '" + model.maxTime + "' >= " + model.dateword + " order by #_point,#_cyc,#_date asc ";
                        break;
                    }
                default: mssg = "没有该日期请求的处理条件";
                    break;
            }
            return true;
            //CycTeriminalSet(sttm, edtm, xmname);

        }

        /// <summary>
        /// 处理快捷查询的值，生成sql语句
        /// </summary>
        public class ProcessdatemdbModel
        {
            /// <summary>
            /// 起始日期
            /// </summary>
            public string sttm { get; set; }
            /// <summary>
            /// 结束日期
            /// </summary>
            public string edtm { get; set; }
            /// <summary>
            /// 时间单位
            /// </summary>
            public string unit { get; set; }

            public DateTime maxTime { get; set; }

            public ProcessdatemdbModel( string unit, DateTime maxTime)
            {
                this.maxTime = maxTime;
                this.unit = unit;
            }
        }


        /// <summary>
        /// 使用mysql远程连接查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool queryaccessdb(queryaccessdbModel model, out string mssg)
        {
            DataTable dt = null;
            if (SqlHelperBLL.queryaccessdb(model.sql, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class queryaccessdbModel
        {
            public string sql { get; set; }
            public DataTable dt { get; set; }
            public queryaccessdbModel(string sql)
            {
                this.sql = sql;
            }
        }




    }
}