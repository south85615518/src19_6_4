﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.Other
{
    public class QueryReadBKGModel
    {
        /// <summary>
        /// 查询SQL语句
        /// </summary>
        public string sql { get; set; }
        /// <summary>
        /// 表头
        /// </summary>
        public List<string> lsname { get; set; }
        /// <summary>
        /// 查询结果
        /// </summary>
        public DataTable dt { get; set; }
        public QueryReadBKGModel(string sql)
        {
            this.sql = sql;
        }
        public QueryReadBKGModel()
        {
            
        }
    }
}