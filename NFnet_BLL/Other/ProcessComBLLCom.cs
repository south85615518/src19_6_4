﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;

namespace NFnet_BLL.Other
{
    public class ProcessComBLLCom
    {

        
        public bool Processquerystanderdb(StanderDBLoadModel model,out string mssg)
        {
            if (model.role == Role.superviseModel || model.tmpRole)
            {
                return ProcessCgComBLL.Processquerystanderdb(model.model,out mssg);
            }
            return ProcessComBLL.Processquerystanderdb(model.model, out mssg);
        }


        public class StanderDBLoadModel
        {
            public QuerystanderdbModel model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public StanderDBLoadModel(QuerystanderdbModel model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }

         






    }
}