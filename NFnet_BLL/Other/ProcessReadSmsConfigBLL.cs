﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool.sms;
using Tool;

namespace NFnet_BLL.Other
{
    public class ProcessReadSmsConfigBLL
    {

        /// <summary>
        /// 返回平台发送短信的对象
        /// </summary>
        /// <returns></returns>
        public static Isendsms GetSmsSender(string path)
        {
            List<string> comList = FileHelper.ProcessFileInfoList(path);
            if (comList[0] == "0")
            return new comsms(comList[1]);
            return new websms(comList[2]);

        }




    }
}