﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SqlHelpers;
using System.Data;

namespace NFnet_BLL.Other
{
    /// <summary>
    /// SQLhelper处理类
    /// </summary>
    public class SqlHelperBLL
    {
        public static querysql query = new querysql();
        public static bool querynvl(string queryname, string queryword, string op, string inserth, string insertt, out string str,out string mssg)
        {
            str = "";
            try
            {
                querystring.querynvl(queryname, queryword, op, inserth, insertt, out str);
                mssg = "Sql语句拼接成功";
                return true;
            }
            catch (Exception ex)
            {
                mssg = "Sql语句拼接出错，错误信息:"+ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool querystanderstr(string sql, string xmname, out string str,out string mssg)
        {
            str = "";
            try
            {
                if (querysql.querystanderstr(sql, xmname, out str))
                {
                    mssg = "查询标准表数据库成功";
                    return true;
                }
                else
                {
                    mssg = "查询标准表数据库失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询标准表数据库出错,错误信息:"+ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool querystanderdb(string sql, string xmname, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (querysql.querystanderdb(sql, xmname, out dt))
                {
                    mssg = "查询标准数据表成功";
                    return true;
                }
                else
                {
                    mssg = "查询标准数据表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询标准数据表出错,错误信息:" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool querystanderdb(string sql, string xmname, List<string> columnnames,out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if ((dt = querysql.querystanderdb(sql, xmname, columnnames)) != null)
                {
                    mssg = "查询标准数据表成功";
                    return true;
                }
                else
                {
                    mssg = "查询标准数据表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询标准数据表出错,错误信息:" + ex.Message;
                return false;
            }
        }


        /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool querystanderdb(string sql, int xmno, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (querysql.querystanderdb(sql, xmno, out dt))
                {
                    mssg = "查询标准数据表成功";
                    return true;
                }
                else
                {
                    mssg = "查询标准数据表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询标准数据表出错,错误信息:" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 使用mysql远程连接查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool queryremotedb(string sql, string remotedbname , out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (querysql.queryremotedb(sql, remotedbname, out dt))
                {
                    mssg = "查询远程数据表成功";
                    return true;
                }
                else
                {
                    mssg = "查询远程数据表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询远程数据表出错,错误信息:" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 使用mysql远程连接查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool queryaccessdb(string sql, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if ((dt = query.querytaccesstdb(sql)) != null)
                {
                    mssg = "查询权限库数据表成功";
                    return true;
                }
                else
                {
                    mssg = "查询权限库数据表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询权限库数据表出错,错误信息:" + ex.Message;
                return false;
            }
        }
    }
}