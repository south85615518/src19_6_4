﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.Other
{
    public class QueryremoteModel
    {
        /// <summary>
        /// 查询SQL语句
        /// </summary>
        public string sql { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string dbname { get; set; }
        /// <summary>
        /// 查询结果
        /// </summary>
        public DataTable dt { get; set; }
        public QueryremoteModel(string sql, string dbname)
        {
            this.sql = sql;
            this.dbname = dbname;
        }
    }
}