﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.Other
{
    /// <summary>
    /// 公共业务逻辑处理类
    /// </summary>
    public class ProcessCgComBLL
    {
        /// <summary>
        /// 可空条件查询语句生成
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool Processquerynvl(Processquerynvlmodel model, out string mssg)
        {
            string str = "";
            mssg = "";

            if (SqlHelperBLL.querynvl(model.queryname, model.queryword, model.op, model.inserth, model.insertt, out str, out mssg))
            {
                model.str = str;
                return true;
            }
            else
            {
                return false;
            }



        }
        /// <summary>
        /// 可空条件查询语句生成类
        /// </summary>
        public class Processquerynvlmodel
        {

            public string queryname { get; set; }
            public string queryword/*查询的字段名*/{ get; set; }
            public string op { get; set; }
            public string inserth { get; set; }
            public string insertt { get; set; }
            public string str { get; set; }
            public Processquerynvlmodel(string queryname, string queryword, string op, string inserth, string insertt)
            {
                this.queryname = queryname;
                this.queryword = queryword;
                this.op = op;
                this.inserth = inserth;
                this.insertt = insertt;

            }
        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderstr(QuerystanderstrModel model, out string mssg)
        {
            string str = "";
            mssg = "";

            if (SqlHelperCgBLL.querystanderstr(model.sql, model.xmname, out str, out mssg))
            {
                model.str = str;
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderdb(QuerystanderdbModel model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";

            if (SqlHelperCgBLL.querystanderdb(model.sql, model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static bool Processquerystanderdb(QuerystanderdbIntModel model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";

            if (SqlHelperCgBLL.querystanderdb(model.sql, model.xmno, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 处理快捷查询的值，生成sql语句
        /// </summary>
        /// <returns></returns>
        public static bool Processdateswdl(ProcessdateswdlModel model, out string mssg)
        {
            model.sttm = "";
            model.edtm = "";
            model.sql = "";
            mssg = "";
            switch (model.unit)
            {
                case "week":
                    {
                        model.sttm = DateTime.Now.AddDays(-7).ToString().Replace("/", "-");
                        model.edtm = DateTime.Now.ToString().Replace("/", "-");
                        model.sql = " date_sub(SYSDATE(),INTERVAL 1 week) <=" + model.dateword + " and sysdate()>= " + model.dateword + "   order by #_point,cyc,#_date asc";
                        break;

                    }
                case "day":
                    {
                        model.sttm = DateTime.Now.AddDays(-1).ToString().Replace("/", "-");
                        model.edtm = DateTime.Now.ToString().Replace("/", "-");
                        model.sql = "  date_sub(SYSDATE(),INTERVAL 1 day) <=" + model.dateword + " and sysdate()>= " + model.dateword + "  order by #_point,cyc,#_date asc";
                        break;
                    }
                case "mon":
                    {
                        model.sttm = DateTime.Now.AddMonths(-1).ToString().Replace("/", "-");
                        model.edtm = DateTime.Now.ToString().Replace("/", "-");
                        model.sql = "  DATE_SUB(SYSDATE(),INTERVAL 1 month) <=" + model.dateword + " and sysdate()>= " + model.dateword + " order by #_point,cyc,#_date asc";
                        break;
                    }
                case "threemon":
                    {
                        model.sttm = DateTime.Now.AddMonths(-3).ToString().Replace("/", "-");
                        model.edtm = DateTime.Now.ToString().Replace("/", "-");
                        model.sql = " DATE_SUB(SYSDATE(),INTERVAL 3 month) <=" + model.dateword + " and sysdate()>= " + model.dateword + " order by #_point,cyc,#_date asc";
                        break;
                    }
                case "year":
                    {
                        model.sttm = DateTime.Now.AddYears(-1).ToString().Replace("/", "-");
                        model.edtm = DateTime.Now.ToString().Replace("/", "-");
                        model.sql = "YEAR(SYSDATE()) - yEAR(" + model.dateword + ")<=1 and sysdate() >= " + model.dateword + " order by #_point,cyc,#_date asc ";
                        break;
                    }
                default: mssg = "没有该日期请求的处理条件";
                    break;
            }
            return true;
            //CycTeriminalSet(sttm, edtm, xmname);

        }
        /// <summary>
        /// 处理快捷查询的值，生成sql语句
        /// </summary>
        public class ProcessdateswdlModel
        {
            /// <summary>
            /// 起始日期
            /// </summary>
            public string sttm { get; set; }
            /// <summary>
            /// 结束日期
            /// </summary>
            public string edtm { get; set; }
            public string dateword/*表中的时间字段*/{ get; set; }
            /// <summary>
            /// 查询SQL语句
            /// </summary>
            public string sql { get; set; }
            /// <summary>
            /// 时间单位
            /// </summary>
            public string unit { get; set; }
            public ProcessdateswdlModel(string dateword/*表中的时间字段*/, string unit)
            {

                this.dateword = dateword;
                this.unit = unit;
            }
        }





    }
}