﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.Other
{
    public class QueryBKGModel
    {
        /// <summary>
        /// 查询SQL语句
        /// </summary>
        public string sql { get; set; }
        /// <summary>
        /// 查询结果
        /// </summary>
        public DataTable dt { get; set; }
        public QueryBKGModel(string sql)
        {
            this.sql = sql;
        }
        public QueryBKGModel()
        {
            
        }
    }
}