﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.Other
{
    public class QuerystanderdbIntModel
    {
        /// <summary>
        /// 查询SQL语句
        /// </summary>
        public string sql { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 查询结果
        /// </summary>
        public DataTable dt { get; set; }
        public QuerystanderdbIntModel(string sql, int xmno)
        {
            this.sql = sql;
            this.xmno = xmno;
        }
    }
}