﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.Other
{
    public class QuerystanderstrModel
    {
        /// <summary>
        /// 查询SQL语句
        /// </summary>
        public string sql { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 查询结果
        /// </summary>
        public string str { get; set; }
        public QuerystanderstrModel(string sql, string xmname)
        {
            this.sql = sql;
            this.xmname = xmname;
            
        }
        public QuerystanderstrModel(string sql, int xmno)
        {
            this.sql = sql;
            this.xmno = xmno;
        }
    }
}