﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data;
using MySql.Data.Types;
using MySql.Data.MySqlClient;
using NFnet_DAL;
using NFnet_MODAL;
namespace NFnet_BLL
{
    public class TargetBaseLink
    {
        /// <summary>
        /// sql server 目标数据库连接
        /// </summary>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public static SqlConnection GetSqlConnect(string DBName)
        {
            string sql = "select * from LinkAttr where LinkVia = '是' and engine = 'sqlserver'";
            DataTable dt = new DataTable();
            querysql query = new querysql();
            dt = query.querytaccesstdb(sql);
            DataView dv = new DataView(dt);
            SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            foreach (DataRowView drv in dv)
            {
                scsb.DataSource = @"pc-PC\SQLEXPRESS";
                scsb.IntegratedSecurity = true;
                scsb.InitialCatalog = DBName;
                scsb.UserID = drv["uname"].ToString();
                scsb.Password = drv["pass"].ToString();
                break;
                //scsb["database"] = dbname;
            }
            
            //获取到系统的默认数据库连接属性
            if (scsb.UserID == "")
            {
                scsb.IntegratedSecurity = true;
            }
            else
            {
                scsb.IntegratedSecurity = false;
            }
            SqlConnection myConnection = new SqlConnection(scsb.ConnectionString);
            if (myConnection.State != ConnectionState.Open)
                myConnection.Open();
            return myConnection;
        }

        public static OdbcConnection GetOdbcConnect(string DBName)
        {
            string constr = ""; 
            constr = constr.Replace("#", DBName);
            string sql = "select * from LinkAttr where LinkVia = '是' and engine = 'mysql'";
            DataTable dt = new DataTable();
            querysql query = new querysql();
            dt = query.querytaccesstdb(sql);
            DataView dv = new DataView(dt);
            foreach (DataRowView drv in dv)
            {
            constr = "Driver={MySQL ODBC 5.3 ANSI Driver};database=#_1;uid=#_2;password=#_3;server=localhost;port=#_4";//发布开发版
            constr = constr.Replace("#_1",DBName);
            constr = constr.Replace("#_2", drv["uname"].ToString());
            constr = constr.Replace("#_3", drv["pass"].ToString());
            constr = constr.Replace("#_4", drv["Port"].ToString());
            }
            //constr = "Driver={MySQL ODBC 5.3 ANSI Driver};database="+DBName+";uid=root;password=;server=localhost;port=3308";
            OdbcConnection conn = new OdbcConnection(constr);
            if (conn.State != ConnectionState.Open)
            {
                try
                {
                    conn.Open();
                }
                catch(Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex,sql);
                    return null;
                }
            }
            System.Console.WriteLine("数据库连接成功!\n");
            return conn;
        }
        public static MySqlConnection GetMysqlConnect(string DBName)
        {
            string constr = "";
            constr = constr.Replace("#", DBName);
            string sql = "select * from LinkAttr where LinkVia = '是' and engine = 'mysql'";
            DataTable dt = new DataTable();
            querysql query = new querysql();
            dt = query.querytaccesstdb(sql);
            DataView dv = new DataView(dt);
            foreach (DataRowView drv in dv)
            {
                constr = "database=#_1;uid=#_2;password=#_3;server=localhost;port=#_4";//发布开发版
                constr = constr.Replace("#_1", DBName);
                constr = constr.Replace("#_2", drv["uname"].ToString());
                constr = constr.Replace("#_3", drv["pass"].ToString());
                constr = constr.Replace("#_4", drv["Port"].ToString());
            }
            //constr = "Driver={MySQL ODBC 5.3 ANSI Driver};database="+DBName+";uid=root;password=;server=localhost;port=3308";
            MySqlConnection conn = new MySqlConnection(constr);
            if (conn.State != ConnectionState.Open)
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex, sql);
                    return null;
                }
            }
            System.Console.WriteLine("数据库连接成功!\n");
            return conn;
        }


        public static object GetConnect(string dbbz)
        {
            string engine = querysql.queryaccessdbstring("select engine from DataBaseBz where bz ='" + dbbz + "'");
            string dbname = querysql.queryaccessdbstring("select name from DataBaseBz where bz ='" + dbbz + "'");
            switch (engine)
            {
                case "sqlserver":
                    return TargetBaseLink.GetSqlConnect(dbname);

                case "mysql":
                    return TargetBaseLink.GetOdbcConnect(dbname);

            }
            return null;
        }
    }
}