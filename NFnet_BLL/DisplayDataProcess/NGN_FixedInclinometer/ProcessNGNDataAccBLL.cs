﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessNGNDataAccBLL
    {
        /// <summary>
        /// 计算单个测斜点的位移
        /// </summary>
        public double FixInclinometerS(double a, double b, double c, double d, double L, double f)
        {
            /*
             * 
             */
            double s = L * Math.Sin(a + b * f + c * Math.Pow(f, 2) + d * Math.Pow(f, 3));
            return s;

        }
    }
}