﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public partial class ProcessDTUModuleLPBLL
    {
        

        public bool ProcessFixedInclinometerModuleLPLoadBLL(ProcessDTUModuleLPLoadBLLModel model, out string mssg)
        {

            DataTable dt = new DataTable();
            if (dtulpBLL.FixedInclinometerTableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }


        public bool ProcessFixedInclinometerXmPortModuleLPLoadBLL(ProcessDTUModuleLPLoadBLLModel model, out string mssg)
        {

            DataTable dt = new DataTable();
            if (dtulpBLL.FixedInclinometerXmPortTableLoad(model.pageIndex, model.rows, model.xmno, model.xmname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }


        public bool ProcessFixedInclinometerModuleLPTableCount(ProcessDTUModuleLPTableCountModel model, out string mssg)
        {
            int totalCont = 0;
            if (dtulpBLL.FixedInclinometerTableRowsCount(model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

       

    }
}