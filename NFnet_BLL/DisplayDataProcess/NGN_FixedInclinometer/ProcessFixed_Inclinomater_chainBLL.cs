﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NGN.BLL;
using System.Data;
using NFnet_BLL.DataProcess;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;
using NFnet_BLL.XmInfo.pub;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixed_Inclinomater_chainBLL
    {
        public fixed_inclinometer_chain bll = new fixed_inclinometer_chain();
        public static ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public static ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public static ProcessalarmsplitondateBLL splitOnDateBLL = new ProcessalarmsplitondateBLL();
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_chain model, out string mssg)
        {
            return bll.Add(model, out mssg);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(fixed_inclinometer_chaindeleteModel model, out string mssg)
        {
            return bll.Delete(model.xmno,model.chain_name, out mssg);
        }

        public class fixed_inclinometer_chaindeleteModel
        {
            public int xmno { get;set; }
            public string chain_name { get; set; }
            public fixed_inclinometer_chaindeleteModel(int xmno,string chain_name)
            {
                this.xmno = xmno;
                this.chain_name = chain_name;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(fixed_inclinometer_chainModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_chain fixed_inclinometer_chainmodel = null;
            if (bll.GetModel(model.xmno, model.deviceno, model.port, out fixed_inclinometer_chainmodel, out mssg))
            {
                model.model = fixed_inclinometer_chainmodel;
                return true;
            }
            return false;
        }
        public class fixed_inclinometer_chainModelGetModel
        {
            public int xmno { get; set; }
            public int deviceno { get; set; }
            public int port { get; set; }
            public NGN.Model.fixed_inclinometer_chain model { get; set; }
            public fixed_inclinometer_chainModelGetModel(int xmno, int deviceno, int port)
            {
                this.xmno = xmno;
                this.deviceno = deviceno;
                this.port = port;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(fixed_inclinometer_chainModelGetByNameModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_chain fixed_inclinometer_chainmodel = null;
            if (bll.GetModel(model.xmno, model.pointname, out fixed_inclinometer_chainmodel, out mssg))
            {
                model.model = fixed_inclinometer_chainmodel;
                return true;
            }
            return false;
        }
        public class fixed_inclinometer_chainModelGetByNameModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public NGN.Model.fixed_inclinometer_chain model { get; set; }
            public fixed_inclinometer_chainModelGetByNameModel(int xmno, string pointname)
            {
               this.xmno = xmno;
               this.pointname = pointname;
            }
        }
       

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateAlarm(NGN.Model.fixed_inclinometer_chain model, out string mssg)
        {
            return bll.UpdateAlarm(model, out mssg);
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdateAlarm(MultiUpdateAlarmModel model, out string mssg)
        {
            return bll.MultiUpdateAlarm(model.pointNameStr, model.model, out mssg);

        }
        public class MultiUpdateAlarmModel
        {
            public string pointNameStr { get; set; }
            public NGN.Model.fixed_inclinometer_chain model { get; set; }
            public MultiUpdateAlarmModel(string pointNameStr, NGN.Model.fixed_inclinometer_chain model)
            {
                this.pointNameStr = pointNameStr;
                this.model = model;
            }
        }

        public bool ChainNameLoad(ChainNameLoadModel model, out string mssg)
        {
            List<string> chainnamelsit = null;
            if (bll.ChainNameLoad(model.xmno, out chainnamelsit, out mssg))
            {
                model.chainnamelsit = chainnamelsit;
                return true;
            }
            return false;
        }

        public class ChainNameLoadModel
        {
            public int xmno { get; set; }
            public List<string> chainnamelsit { get; set; }
            public ChainNameLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
      



        /// <summary>
        /// 加载固定测斜链表
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableLoad(Fixed_inclinometerPointChainTableLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.PointTableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class Fixed_inclinometerPointChainTableLoadModel:SearchCondition
        {
            public int xmno { get; set; }
            public DataTable dt { get; set; }
            public Fixed_inclinometerPointChainTableLoadModel(int xmno, int pageIndex, int rows, string colName,string sord)
            {
                this.xmno = xmno;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.colName = colName;
                this.sord = sord;
            }
        }

        /// <summary>
        /// 加载固定测斜链表
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableCountLoad(Fixed_inclinometerPointChainTableCountLoadModel model, out string mssg)
        {
            string cnt = "";
            if (bll.PointTableCountLoad(model.xmno,out cnt ,out mssg))
            {
                model.cnt = cnt;
                return true;
            }
            return false;
        }


        public class Fixed_inclinometerPointChainTableCountLoadModel
        {
            public int xmno { get; set; }
            public string cnt { get; set; }
            public Fixed_inclinometerPointChainTableCountLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        /// <summary>
        /// 结果数据对象预警处理
        /// </summary>
        /// <param name="alarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmIntoInformation(NGN.Model.fixed_inclinometer_chainalarmvalue alarmvalue, NGN.Model.fixed_inclinometer_chaindata resultModel, out List<string> lscontext)
        {
            lscontext = new List<string>();
            bool result = false;
            List<string> listspace = new List<string>();
            listspace.Add(string.Format("{0},{1}, 位移:{2}mm", resultModel.point_name, resultModel.time.ToString(), resultModel.disp));
            listspace.Add(DataProcessHelper.RangeCompareU("", "位移上限", resultModel.disp, alarmvalue.dispU, out result, result));
            listspace.Add(DataProcessHelper.RangeCompareL("", "位移下限", resultModel.disp, alarmvalue.dispL, out result, result));
            //listspace.Add(DataProcessHelper.RangeCompare("", "温度上限", resultModel.temperlate, alarmvalue.temperlate, out result, result));
            //listspace.Add(DataProcessHelper.RangeCompare("", "温度下限", resultModel.temperlate, alarmvalue.temperlateL, out result, result));
            lscontext.Add(string.Join("  ", listspace));
            //lscontext.Add(string.Join(" ",ls));
            return result;
        }
        public static int sec = 0;
        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            List<string> ls = new List<string>();
            string mssg = "";

            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.point_name,
                time = model.resultModel.time,
                type = "固定测斜链",
                atime = DateTime.Now,
                pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processCgAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, "固定测斜链", model.resultModel.point_name, model.resultModel.time);

            splitOnDateBLL.ProcessalarmsplitondateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);

            AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = "固定测斜链",
                pointName = model.resultModel.point_name,
                xmno = model.xmno,
                time = model.resultModel.time,
                adate = DateTime.Now

            };
            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add("三级预警");
                    pointCheck.alarm = 3;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "固定测斜链", "", model.resultModel.point_name, 3, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add("二级预警");
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "固定测斜链", "", model.resultModel.point_name, 2, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add("一级预警");
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "固定测斜链", "", model.resultModel.point_name, 1, out mssg);
                    return true;
                }


            }
            ls.Add("========预警解除==========");
            pointCheck.alarm = 0;
            pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
            ProcessHotPotColorUpdate(model.xmno, "固定测斜链", "", model.resultModel.point_name, 0, out mssg);
            return false;

        }
        public class ProcessPointAlarmfilterInformationModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public List<NGN.Model.fixed_inclinometer_chainalarmvalue> levelalarmvalue { get; set; }
            public NGN.Model.fixed_inclinometer_chaindata resultModel { get; set; }
            public List<string> ls { get; set; }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<NGN.Model.fixed_inclinometer_chainalarmvalue> levelalarmvalue, NGN.Model.fixed_inclinometer_chaindata resultModel, int xmno)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
                this.xmno = xmno;
            }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<NGN.Model.fixed_inclinometer_chainalarmvalue> levelalarmvalue, NGN.Model.fixed_inclinometer_chaindata resultModel)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
            }
            public ProcessPointAlarmfilterInformationModel()
            {

            }
        }
        public class AlarmRecord
        {
            public string pointName { get; set; }
            public string Time { get; set; }
            public string alarm { get; set; }
            public string RecordTime { get; set; }

        }

        public bool ProcessPointAlarmModelGet(ProcessPointAlarmModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_chain pointalarm = new NGN.Model.fixed_inclinometer_chain();
            if (bll.GetModel(model.xmno, model.pointName, out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }
        public class ProcessPointAlarmModelGetModel
        {
            public int xmno { get; set; }
            public string pointName { get; set; }
            public NGN.Model.fixed_inclinometer_chain model { get; set; } 
            public ProcessPointAlarmModelGetModel(int xmno, string pointName)
            {
                this.xmno = xmno;
                this.pointName = pointName;
            }
        }

        public bool ProcessHotPotColorUpdate(int xmno, string jclx, string jcoption, string pointName, int color, out string mssg)
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(xmno, jclx, jcoption, pointName, color);
            return layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);

        }

    }
}