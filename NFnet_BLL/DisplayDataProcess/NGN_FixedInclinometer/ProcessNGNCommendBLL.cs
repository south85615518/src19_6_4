﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NGN.Model;
using Tool.com;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessNGNCommendBLL
    {
        public IEEE754Helper iEEE754Helper = new IEEE754Helper();
        public bool ProcessNGNCommendCheck(byte[] bytes,out string mssg)
        {

            //固定测斜的命令组成 
            /*
             * 包首（1+1+2+2+1）
             * 时间戳(1+1+4)
             * 电池电压(2+4)
             * 测斜仪1数据(2+4)
             * 测斜仪2数据(2+4)
             * .
             * .
             * .
             * 测斜仪n数据(2+4)
             * CRC校验(2)
             */
            List<byte[]> bytearrarylist = new List<byte[]>();

            bytearrarylist.Add(new byte[2] { 0x30, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x31, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x32, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x33, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x34, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x35, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x36, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x37, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x38, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x39, 0x64 });
            foreach (byte[] bytearray in bytearrarylist)
            {
                if (Tool.com.HexHelper.ByteIndexByteArray(bytes, bytearray) != -1) {
                    mssg = string.Format("从数据位数特性初步判断{0}是固定测斜自报数据", Tool.com.HexHelper.ByteToHexString(bytes));
                    return true;
                }
                

            }
            mssg = string.Format("从数据位数特性判断{0}不是固定测斜自报数据", Tool.com.HexHelper.ByteToHexString(bytes));
            return false;
        }
        #region 时间
        //获取设备时间
        public bool ProcessNGNTime(byte[] bytes,out DateTime dt,out string  mssg)
        {
            mssg = "";
            dt = new DateTime();
            return false;
        }
        //生成设备时间设置命令
        public bool PrcoessNGNTimeSetCommendCreate(int deviceno,Dictionary<string,int> FCDictionary,out byte[] commend,out string mssg  )
        {
            commend = null;
            mssg = "";
            return false;

        }
        //设备时间设置成功返回指令确认
        public bool ProcessNGNTimeSetSuccessComfire(int deviceno, Dictionary<string, int> FCDictionary, out string mssg)
        {
            mssg = "";
            return false;
        }

        #endregion
        #region 采集间隔

        //生成设备采集间隔设置命令
        public bool PrcoessNGNIntervalSetCommendCreate(int deviceno, Dictionary<string, int> FCDictionary, out byte[] commend, out string mssg)
        {
            commend = null;
            mssg = "";
            return false;

        }

        //设备时间设置成功返回指令确认
        public bool ProcessNGNIntervalSetSuccessComfire(int deviceno, Dictionary<string, int> FCDictionary, out string mssg)
        {
            mssg = "";
            return false;
        }
        #endregion

        #region 数据解析

        //解析数据获取单个测斜仪的数据
        //解析数据获取水位原始值和温度原始值
        public void PrcoessNGNDataDescode(byte[] data,out List<fixedinclinometer> fixedinclinometerlist)
        {

            //FF 60 00 A9 00 27 10 01 44 57 67 F7 0A 02 44 56 86 41 3C 03 64 07 81 85 40 11 64 00 00 00 00 12 64 9A 99 88 C3 B5 24
            //ngnorgldata = new DTU.Model.dtudata();
            int timeindex = 0, voltageindex = 0, indexf = 0;
            fixedinclinometerlist = new List<fixedinclinometer>();
            string taskid = DateTime.Now.ToFileTime().ToString();
            fixedinclinometer tmp = new fixedinclinometer();
            tmp.deviceno = Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 2, 2), 16);
            tmp.taskid = taskid;
            timeindex = Tool.com.HexHelper.ByteIndexByteArray(data, new byte[2] { 0x01, 0x44 });
            if (timeindex != -1)
                tmp.time = Tool.DateHelper.Unixtimestamp(Convert.ToInt32(Tool.com.HexHelper.oxdata(data, timeindex + 2, 4), 16));
             voltageindex = Tool.com.HexHelper.ByteIndexByteArray(data, new byte[2] { 0x03, 0x64 });
            if (voltageindex != -1)
                tmp.voltage = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, voltageindex + 2, 4));


           


            List<byte[]> bytearrarylist = new List<byte[]>();

            bytearrarylist.Add(new byte[2] { 0x30, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x31, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x32, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x33, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x34, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x35, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x36, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x37, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x38, 0x64 });
            bytearrarylist.Add(new byte[2] { 0x39, 0x64 });

            foreach (byte[] bytearray in bytearrarylist)
            {
                if (Tool.com.HexHelper.ByteIndexByteArray(data, bytearray) == -1) continue;
                
                    fixedinclinometer model = tmp.clone();
                    indexf = Tool.com.HexHelper.ByteIndexByteArray(data, bytearray);
                    model.f = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, indexf + 2, 4));
                    model.address = Convert.ToInt32(bytearray[0].ToString());
                    fixedinclinometerlist.Add(model);
                
            }
            

        }
        //public FixInclinometer FixInclinometerDescode(byte[] data)
        //{
        //    //FF 1C 00 06 00 2D 11 01 44 57 68 02 8E 03 64 4E 62 80 40 30 64 00 00 00 00 31 64 00 00 00 00 32 64 00 00 00 00 33 64 00 00 00 00 4B 8C
        //    FixInclinometer FixInclinometer = new FixInclinometer();
        //    FixInclinometer.id = Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 2, 2));
        //    FixInclinometer.dt = Tool.DateHelper.Unixtimestamp(Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 9, 4), 16));
        //    FixInclinometer.fOne = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 21, 4));
        //    FixInclinometer.fTwo = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 25, 4));
        //    FixInclinometer.fThree = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 29, 4));
        //    FixInclinometer.fFour = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, 28, 4));
        //    return new FixInclinometer();
        //}

        public double FixInclinometerS(double a, double b, double c, double d, double L, double f)
        {
            /*
             * 
             */
            double s = L * Math.Sin(a + b * f + c * Math.Pow(f, 2) + d * Math.Pow(f, 3));
            return s;

        }
        public class fixedinclinometer
        {
            //设备号
            public int deviceno { get; set; }
            //端口
            public int port { get; set; }
            //地址码
            public int address { get; set; }
            //模数值
            public double f { get; set; }
            //时间戳
            public DateTime time { get; set; }
            //周期
            public string taskid { get; set; }
            //电压
            public double voltage { get; set; }
            public fixedinclinometer clone()
            {
                fixedinclinometer model = new fixedinclinometer();
                model.deviceno = this.deviceno;
                model.port = this.port;
                model.address = this.address;
                model.time = this.time;
                model.taskid = this.taskid;
                model.voltage = this.voltage;
                return model;
            }


        }
       



        #endregion

    }
}