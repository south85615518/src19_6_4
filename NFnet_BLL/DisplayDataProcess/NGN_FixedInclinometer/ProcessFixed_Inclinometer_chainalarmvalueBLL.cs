﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NGN.BLL;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixed_Inclinometer_chainalarmvalueBLL
    {
        public fixed_inclinometer_chainalarmvalue bll = new fixed_inclinometer_chainalarmvalue();

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_chainalarmvalue model, out string mssg)
        {
            return bll.Add(model, out mssg);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(NGN.Model.fixed_inclinometer_chainalarmvalue model, out string mssg)
        {
            return bll.Update(model, out mssg);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DeleteModel model, out string mssg)
        {
            return bll.Delete(model.xmno, model.alarmName, out mssg);

        }

        public class DeleteModel
        {
            public int xmno { get; set; }
            public string alarmName { get; set; }
            public DeleteModel(int xmno, string alarmName)
            {
                this.xmno = xmno;
                this.alarmName = alarmName;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(Fixed_Inclinometer_chainModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_chainalarmvalue fixed_inclinometer_chainalarmvaluemodel = new NGN.Model.fixed_inclinometer_chainalarmvalue();
            if (bll.GetModel(model.xmno, model.alarmName, out fixed_inclinometer_chainalarmvaluemodel, out mssg))
            {
                model.model = fixed_inclinometer_chainalarmvaluemodel;
                return true;
            }
            return false;

        }
        public class Fixed_Inclinometer_chainModelGetModel
        {
            public int xmno { get; set; }
            public string alarmName { get; set; }
            public NGN.Model.fixed_inclinometer_chainalarmvalue model { get; set; }
            public Fixed_Inclinometer_chainModelGetModel(int xmno, string alarmName)
            {
                this.xmno = xmno;
                this.alarmName = alarmName;
            }
        }

        /// <summary>
        /// 测斜点预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(Fixed_Inclinometer_chainTableLoadModel model, out string mssg)
        {
            DataTable dt = new DataTable();
            if (bll.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class Fixed_Inclinometer_chainTableLoadModel : SearchCondition
        {
            public int xmno { get; set; }
            public DataTable dt { get; set; }
            public Fixed_Inclinometer_chainTableLoadModel(int startPageIndex, int pageSize, int xmno, string colName, string sord)
            {
                this.pageIndex = startPageIndex;
                this.rows = pageSize;
                this.xmno = xmno;
                this.colName = colName;
                this.sord = sord;
            }
        }


        /// <summary>
        /// 获取测斜点预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(AlarmValueNameGetModel model, out string mssg)
        {
            string alarmValueNameStr = "";
            if (bll.AlarmValueNameGet(model.xmno, out alarmValueNameStr, out mssg))
            {
                model.alarmValueNameStr = alarmValueNameStr;
                return true;
            }
            return false;
        }
        public class AlarmValueNameGetModel
        {
            public int xmno { get; set; }
            public string alarmValueNameStr { get; set; }
            public AlarmValueNameGetModel(int xmno)
            {
                this.xmno = xmno;
                this.alarmValueNameStr = alarmValueNameStr;
            }
        }
    }
}