﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.NGN_FixedInclinometer
{
    public partial class ProcessOrglDataAlarmBLL
    {
       
        public bool cgmain()
        {

            return TestFixedInclinometerModelList();

        }
        public bool TestCgFixedInclinometerModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessFixed_Inclinometer_orglDataBLL.ProcessResultDataAlarmModelListModel(xmno);
            
            if (deviceBLL.ProcessCgResultDataAlarmModelList(processResultDataAlarmModelListModel, out mssg))
            {
                List<global::NGN.Model.fixed_inclinometer_orgldata> devicedatalist = processResultDataAlarmModelListModel.modellist;
                //ProcessPrintMssg.Print(mssg);
                FixedInclinometerPointAlarm(devicedatalist);
                return true;
            }
            return false;

            //ProcessPrintMssg.Print(mssg);

        }
        public data.Model.gtpointalarmvalue TestPointAlarmValue(string pointName)
        {
            var processPointAlarmModelGetModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointName,data.Model.gtsensortype._inclinometer);
            NGN.Model.fixed_inclinometer_alarmvalue model = null;
            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                return processPointAlarmModelGetModel.model;
            }
            return null;
        }
        public List<data.Model.gtalarmvalue> TestAlarmValueList(data.Model.gtpointalarmvalue pointalarm)
        {
            List<data.Model.gtalarmvalue> alarmvalueList = new List<data.Model.gtalarmvalue>();
            //一级
            var processAlarmModelGetByNameModel = new NFnet_gtalarmvaluebll.DisplayDataProcess.GT.ProcessAlarmValueBLL.ScalarvalueModelGetModel(xmno, pointalarm.firstalarmname,data.Model.gtsensortype._inclinometer);
            if (alarmBLL.ProcessSingleScalarvalueModelGet(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("一级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            else
                alarmvalueList.Add(null);
            processAlarmModelGetByNameModel = new NFnet_gtalarmvaluebll.DisplayDataProcess.GT.ProcessAlarmValueBLL.ScalarvalueModelGetModel(xmno, pointalarm.secondalarmname, data.Model.gtsensortype._inclinometer);
            if (alarmBLL.ProcessSingleScalarvalueModelGet(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("二级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            else
                alarmvalueList.Add(null);
            processAlarmModelGetByNameModel = new NFnet_gtalarmvaluebll.DisplayDataProcess.GT.ProcessAlarmValueBLL.ScalarvalueModelGetModel(xmno, pointalarm.thirdalarmname, data.Model.gtsensortype._inclinometer);
            if (alarmBLL.ProcessSingleScalarvalueModelGet(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("三级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            else
                alarmvalueList.Add(null);
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<data.Model.gtalarmvalue> levelalarmvalue, NGN.Model.fixed_inclinometer_orgldata resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessFixed_Inclinomater_deviceBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (inclinometerdeviceBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));

                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public bool FixedInclinometerPointAlarm(List<global::NGN.Model.fixed_inclinometer_orgldata> lc)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format("===================================={0}=================================", DateTime.Now));
            ls.Add(string.Format("===================================={0}=================================", xmname));
            ls.Add(string.Format("===================================={0}=================================", "深部位移--测斜--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (global::NGN.Model.fixed_inclinometer_orgldata cl in lc)
            {
               
                data.Model.gtpointalarmvalue pointvalue = TestPointAlarmValue(cl.holename); 
                List<data.Model.gtalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public bool FixedInclinometerPointCurrentAlarmCreate(List<global::NGN.Model.fixed_inclinometer_orgldata> lc)
        {
            alarmInfoList = new List<string>();
            foreach (global::NGN.Model.fixed_inclinometer_orgldata cl in lc)
            {

                data.Model.gtpointalarmvalue pointvalue = TestPointAlarmValue(cl.holename);
                List<data.Model.gtalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> ResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        //public void FixedInclinometerPointAlarm(NGN.Model.fixed_inclinometer_orgldata dirnet)
        //{

        //    NGN.Model.fixed_inclinometer_device pointvalue = TestPointAlarmValue(dirnet.point_name);
        //    if (pointvalue == null) return;
        //    List<NFnet_DAL.MODEL.alarmvalue> alarmList = TestAlarmValueList(pointvalue);
        //    TestPointAlarmfilterInformation(alarmList, dirnet);
        //}




    }
}