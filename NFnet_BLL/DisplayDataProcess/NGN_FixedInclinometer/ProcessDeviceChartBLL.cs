﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using NFnet_MODAL;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessdeviceChartBLL
    {
        #region 测斜孔深度位移曲线
        #region FaceA


        /// <summary>
        /// 位移/角度曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessFaceASerializestrdevice(SerializestrSBWY_POINTCondition model, out string mssg)
        {

            mssg = "";
            //string xmname = model.xmname;
            DataTable dt = new DataTable();
            string mkhs = model.pointname;
            string sql = model.sql;
            //分模块号显示
            if (sql == "") return false;
            var devicemodel = new QuerystanderdbIntModel(model.sql, model.xmno);
            if (!ProcessComBLL.Processquerystanderdb(devicemodel, out mssg))
            {
                //错误信息反馈
            }
            dt = devicemodel.dt;
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_point> lst = Createserie_pointsFaceAFromData(dt, mkhs);
                model.serie_points = lst;
            }//如果mkh为空默认只显示过程
            return true;
        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">位移/角度点名</param>
        /// <returns></returns>
        public static List<serie_point> Createserie_pointsFaceAFromData(DataTable dt, string mkhs)
        {
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_point> lst = new List<serie_point>();

                omkhs[] omkharr = new omkhs[mkharr.Length];
                for (int t = 0; t < mkharr.Length; t++)
                {

                    serie_point srthis = new serie_point();
                    serie_point srac = new serie_point();
                    
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "holename='" + mkharr[t] + "'";
                    dv.Sort = "holename,cyc,deepth asc";
                    DataTable dtsrc = dv.ToTable(dt.TableName);
                    List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(dtsrc, "cyc");
                    foreach (var cyc in cyclist)
                    {
                        serie_point sr = new serie_point();
                        serie_point srrap = new serie_point();
                        DataView dvtask = new DataView(dtsrc);
                        dvtask.RowFilter = "cyc = '" + cyc + "'";
                        //DateTime time = Convert.ToDateTime(dvtask[0]["time"]);
                        sr.Name = string.Format("{0}_{1}_{2}", mkharr[t], cyc, dvtask[0]["time"]);
                        sr.Stype = "测斜累计";
                        //加载点
                        formatdat(dvtask, sr, "a_ac_diff");
                        lst.Add(sr);
                        srrap.Name = string.Format("{0}_{1}_{2}", mkharr[t], cyc, dvtask[0]["time"]);
                        srrap.Stype = "测斜日变化";
                        //加载点
                        formatdat(dvtask, srrap, "a_this_rap");
                        lst.Add(srrap);


                    }


                    //int nu = dv.Count;
                    //string rri = mkharr[t];

                    //sr.Stype = "温度";
                    //sr.Name = rri.Replace("'", "");  //InstandMkhByPotName(mkharr[t], conn);
                    ////加载点
                    //formatdat(dv, sr, "wdz");
                    //lst.Add(sr);
                    //sr = new serie_point();



                }


                return lst;
            }
            return null;
        }
        /// <summary>
        #endregion
        #region FaceB


        /// <summary>
        /// 位移/角度曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessFaceBSerializestrdevice(SerializestrSBWY_POINTCondition model, out string mssg)
        {

            mssg = "";
            //string xmname = model.xmname;
            DataTable dt = new DataTable();
            string mkhs = model.pointname;
            string sql = model.sql;
            //分模块号显示
            if (sql == "") return false;
            var devicemodel = new QuerystanderdbIntModel(model.sql, model.xmno);
            if (!ProcessComBLL.Processquerystanderdb(devicemodel, out mssg))
            {
                //错误信息反馈
            }
            dt = devicemodel.dt;
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_point> lst = Createserie_pointsFaceAFromData(dt, mkhs);
                model.serie_points = lst;
            }//如果mkh为空默认只显示过程
            return true;
        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">位移/角度点名</param>
        /// <returns></returns>
        public static List<serie_point> Createserie_pointsFaceBFromData(DataTable dt, string mkhs)
        {
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_point> lst = new List<serie_point>();

                omkhs[] omkharr = new omkhs[mkharr.Length];
                for (int t = 0; t < mkharr.Length; t++)
                {

                    serie_point srthis = new serie_point();
                    serie_point srac = new serie_point();
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "holename='" + mkharr[t] + "'";
                    dv.Sort = "holename,cyc,deepth asc";
                    DataTable dtsrc = dv.ToTable(dt.TableName);
                    List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(dtsrc, "cyc");
                    foreach (var cyc in cyclist)
                    {
                        //serie_point sr = new serie_point();
                        //DataView dvtask = new DataView(dtsrc);
                        //dvtask.RowFilter = "cyc = '" + cycstr + "'";
                        ////DateTime time = Convert.ToDateTime(dvtask[0]["time"]);
                        //sr.Name = string.Format("{0}_{1}", mkharr[t], cycstr);
                        //sr.Stype = "测斜";
                        ////加载点
                        //formatdat(dvtask, sr, "b_ac_diff");
                        //lst.Add(sr);

                        serie_point sr = new serie_point();
                        serie_point srrap = new serie_point();
                        DataView dvtask = new DataView(dtsrc);
                        dvtask.RowFilter = "cyc = '" + cyc + "'";
                        //DateTime time = Convert.ToDateTime(dvtask[0]["time"]);
                        sr.Name = string.Format("{0}_{1}_{2}", mkharr[t], cyc, dvtask[0]["time"]);
                        sr.Stype = "测斜累计";
                        //加载点
                        formatdat(dvtask, sr, "b_ac_diff");
                        lst.Add(sr);
                        srrap.Name = string.Format("{0}_{1}_{2}", mkharr[t], cyc, dvtask[0]["time"]);
                        srrap.Stype = "测斜日变化";
                        //加载点
                        formatdat(dvtask, srrap, "b_this_rap");
                        lst.Add(srrap);


                    }


                    //int nu = dv.Count;
                    //string rri = mkharr[t];

                    //sr.Stype = "温度";
                    //sr.Name = rri.Replace("'", "");  //InstandMkhByPotName(mkharr[t], conn);
                    ////加载点
                    //formatdat(dv, sr, "wdz");
                    //lst.Add(sr);
                    //sr = new serie_point();



                }


                return lst;
            }
            return null;
        }
        /// <summary>
        #endregion



        /// 位移/角度数据生成曲线
        /// </summary>
        /// <param name="dv">数据表视图</param>
        /// <param name="sr">曲线</param>
        /// <param name="valstr">值字段名称</param>
        public static void formatdat(DataView dv, serie_point sr, string valstr)
        {

            sr.Pts = new pt_point[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string deepth = "", v = "";
                DateTime d = DateTime.Now;
                try
                {
                    deepth = drv["deepth"].ToString();
                    v = drv[valstr].ToString();
                    int year = d.Year;
                    int mon = d.Month;
                    int day = d.Day;
                    int hour = d.Hour;
                    int minute = d.Minute;
                    int second = d.Second;
                    sr.Pts[i] = new pt_point {  PointName = drv["deepth"].ToString(), Yvalue1 = (float)Convert.ToDouble(v) };
                    i++;
                }
                catch (Exception ex)
                {
                    sr.Pts[i] = new pt_point {  PointName  = deepth, Yvalue1 = 0 };
                    i++;
                    ExceptionLog.ExceptionWrite("测斜曲线数据点生成出错，错误信息" + ex.Message + "深度:" + deepth + "值:" + v);
                }
            }

        }
        #endregion
        #region 测斜孔深度传感器位移-时间曲线
        #region FaceA
        /// <summary>
        /// 位移/角度曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrFaceASingledevice(SerializestrSBWY_POINTCondition model, out string mssg)
        {

            mssg = "";
            //string xmname = model.xmname;
            DataTable dt = new DataTable();
            string mkhs = model.pointname;
            string sql = model.sql;
            //分模块号显示
            if (sql == "") return false;
            var devicemodel = new QuerystanderdbIntModel(model.sql, model.xmno);
            if (!ProcessComBLL.Processquerystanderdb(devicemodel, out mssg))
            {
                //错误信息反馈
            }
            dt = devicemodel.dt;
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_cyc> lst = CreateserieFaceAFromData(dt, mkhs);
                model.serie_cyc = lst;
            }//如果mkh为空默认只显示过程
            return true;
        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">位移/角度点名</param>
        /// <returns></returns>
        public static List<serie_cyc> CreateserieFaceAFromData(DataTable dt, string mkhs)
        {
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_cyc> lst = new List<serie_cyc>();

                omkhs[] omkharr = new omkhs[mkharr.Length];
                for (int t = 0; t < mkharr.Length; t++)
                {

                    
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "holename='" + mkharr[t] + "'";
                    dv.Sort = "holename,deepth,cyc asc";
                    DataTable dtsrc = dv.ToTable(dt.TableName);
                    List<string> deepthlist = Tool.DataTableHelper.ProcessDataTableFilter(dtsrc, "deepth");
                    foreach (var deepth in deepthlist)
                    {
                        serie_cyc sr = new serie_cyc();
                        DataView dvtask = new DataView(dtsrc);
                        dvtask.RowFilter = "deepth = " + deepth + "";
                        sr.Name = string.Format("{0}_{1}", mkharr[t], deepth);
                        sr.Stype = "测斜累计";
                        //加载点
                        formatdat(dvtask, sr, "a_ac_diff");
                        lst.Add(sr);

                        serie_cyc srrap = new serie_cyc();
                        dvtask.RowFilter = "deepth = " + deepth + "";
                        srrap.Name = string.Format("{0}_{1}", mkharr[t], deepth);
                        srrap.Stype = "测斜日变化";
                        //加载点
                        formatdat(dvtask, srrap, "a_this_rap");
                        lst.Add(srrap);

                    }


                    //int nu = dv.Count;
                    //string rri = mkharr[t];

                    //sr.Stype = "温度";
                    //sr.Name = rri.Replace("'", "");  //InstandMkhByPotName(mkharr[t], conn);
                    ////加载点
                    //formatdat(dv, sr, "wdz");
                    //lst.Add(sr);
                    //sr = new serie();



                }


                return lst;
            }
            return null;
        }
        #endregion

        #region FaceB
        /// <summary>
        /// 位移/角度曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrFaceBSingledevice(SerializestrSBWY_POINTCondition model, out string mssg)
        {

            mssg = "";
            //string xmname = model.xmname;
            DataTable dt = new DataTable();
            string mkhs = model.pointname;
            string sql = model.sql;
            //分模块号显示
            if (sql == "") return false;
            var devicemodel = new QuerystanderdbIntModel(model.sql, model.xmno);
            if (!ProcessComBLL.Processquerystanderdb(devicemodel, out mssg))
            {
                //错误信息反馈
            }
            dt = devicemodel.dt;
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_cyc> lst = CreateserieFaceBFromData(dt, mkhs);
                model.serie_cyc = lst;
            }//如果mkh为空默认只显示过程
            return true;
        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">位移/角度点名</param>
        /// <returns></returns>
        public static List<serie_cyc> CreateserieFaceBFromData(DataTable dt, string mkhs)
        {
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_cyc> lst = new List<serie_cyc>();

                omkhs[] omkharr = new omkhs[mkharr.Length];
                for (int t = 0; t < mkharr.Length; t++)
                {

                    
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "holename='" + mkharr[t] + "'";
                    dv.Sort = "holename,deepth,cyc asc";
                    DataTable dtsrc = dv.ToTable(dt.TableName);
                    List<string> deepthlist = Tool.DataTableHelper.ProcessDataTableFilter(dtsrc, "deepth");
                    foreach (var deepth in deepthlist)
                    {
                        //serie_cyc sr = new serie_cyc();
                        //DataView dvtask = new DataView(dtsrc);
                        //dvtask.RowFilter = "deepth = " + deepth + "";
                        //sr.Name = string.Format("{0}_{1}", mkharr[t], deepth);
                        //sr.Stype = "测斜";
                        ////加载点
                        //formatdat(dvtask, sr, "b_ac_diff");
                        //lst.Add(sr);

                        serie_cyc sr = new serie_cyc();
                        DataView dvtask = new DataView(dtsrc);
                        dvtask.RowFilter = "deepth = " + deepth + "";
                        sr.Name = string.Format("{0}_{1}", mkharr[t], deepth);
                        sr.Stype = "测斜累计";
                        //加载点
                        formatdat(dvtask, sr, "b_ac_diff");
                        lst.Add(sr);

                        serie_cyc srrap = new serie_cyc();
                        dvtask.RowFilter = "deepth = " + deepth + "";
                        srrap.Name = string.Format("{0}_{1}", mkharr[t], deepth);
                        srrap.Stype = "测斜日变化";
                        //加载点
                        formatdat(dvtask, srrap, "b_this_rap");
                        lst.Add(srrap);

                    }


                    //int nu = dv.Count;
                    //string rri = mkharr[t];

                    //sr.Stype = "温度";
                    //sr.Name = rri.Replace("'", "");  //InstandMkhByPotName(mkharr[t], conn);
                    ////加载点
                    //formatdat(dv, sr, "wdz");
                    //lst.Add(sr);
                    //sr = new serie();



                }


                return lst;
            }
            return null;
        }
        #endregion
        
        /// <summary>
        /// 位移/角度数据生成曲线
        /// </summary>
        /// <param name="dv">数据表视图</param>
        /// <param name="sr">曲线</param>
        /// <param name="valstr">值字段名称</param>
        public static void formatdat(DataView dv, serie_cyc sr, string valstr)
        {

            sr.Pts = new pt_cyc[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string cyc= drv["cyc"].ToString();
                string v = "";
                try
                {
                    v = drv[valstr].ToString();
                    sr.Pts[i] = new pt_cyc { Pt_x = string.Format("第{0}周期 {1}", cyc, drv["time"].ToString()), Yvalue1 = (float)Convert.ToDouble(v) };
                    i++;
                }
                catch (Exception ex)
                {
                    sr.Pts[i] = new pt_cyc { Pt_x = string.Format("第{0}周期 {1}", cyc, drv["time"].ToString()), Yvalue1 = 0 };
                    i++;
                    ExceptionLog.ExceptionWrite("测斜曲线数据点生成出错，错误信息" + ex.Message + "日期:" + drv["time"].ToString() + "值:" + v);
                }
            }

        }
        #endregion

    }
}