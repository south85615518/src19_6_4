﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NGN.Model;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessNGNDataImportBLL
    {
       
        /// <summary>
        /// 测斜链数据录入
        /// </summary>
        /// <param name="dtudata"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessNGNChainDataImport(fixed_inclinometer_orgldata dtudata, out string mssg)
        {
            mssg = "";
            return false;
        }
        /// <summary>
        /// 单个测斜段数据录入
        /// </summary>
        /// <param name="dtudata"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessNGNOrglDataImport(DTU.Model.dtudata dtudata, out string mssg)
        {
            mssg = "";
            return false;
        }
        



    }
}