﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NGN.BLL;
using System.Data;
using NFnet_BLL.DataProcess;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;
using NFnet_BLL.XmInfo.pub;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixed_Inclinomater_deviceBLL
    {

        public fixed_inclinometer_device bll = new fixed_inclinometer_device();
        public static ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public static ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public static ProcessalarmsplitondateBLL splitOnDateBLL = new ProcessalarmsplitondateBLL();
        public bool Exist(IsFixed_Inclinomater_deviceNameExistModel model, out string mssg)
        {

            return bll.Exist(model.point_name,model.xmno,out mssg);

        }
        public class IsFixed_Inclinomater_deviceNameExistModel
        {
            public string point_name { get; set; }
            public int xmno { get; set; }
            public IsFixed_Inclinomater_deviceNameExistModel(string point_name,int xmno)
            {
                this.point_name = point_name;
                this.xmno = xmno;
            }
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_device model, out string mssg)
        {
            return bll.Add(model,out mssg);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(NGN.Model.fixed_inclinometer_device model, out string mssg)
        {
            return bll.Update(model,out mssg);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(NGN.Model.fixed_inclinometer_device model, out string mssg)
        {
            return bll.Delete(model,out mssg);
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(fixed_inclinometer_deviceModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_device fixed_inclinometer_devicemodel = null;
            if (bll.GetModel(model.xmno, model.deviceno, model.address, model.port, out fixed_inclinometer_devicemodel, out mssg))
            {
                model.model = fixed_inclinometer_devicemodel;
                return true;
            }
            return false;
        }
        public class fixed_inclinometer_deviceModelGetModel
        {
            public int xmno { get; set; }
            public int deviceno { get; set; }
            public int address { get; set; }
            public int port { get; set; }
            public NGN.Model.fixed_inclinometer_device model { get; set; }
            public fixed_inclinometer_deviceModelGetModel(int xmno,int deviceno,int address,int port)
            {
                this.xmno = xmno;
                this.deviceno = deviceno;
                this.address = address;
                this.port = port;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetNGN_FixedInclinometerdeviceModel(NGN_FixedInclinometerdeviceModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_device fixed_inclinometer_devicemodel = null;
            if (bll.GetNGN_FixedInclinometerModel(model.xmno, model.deviceno, model.address, model.port, out fixed_inclinometer_devicemodel, out mssg))
            {
                model.model = fixed_inclinometer_devicemodel;
                return true;
            }
            return false;
        }
        public class NGN_FixedInclinometerdeviceModelGetModel
        {
            public int xmno { get; set; }
            public int deviceno { get; set; }
            public int address { get; set; }
            public int port { get; set; }
            public NGN.Model.fixed_inclinometer_device model { get; set; }
            public NGN_FixedInclinometerdeviceModelGetModel(int xmno, int deviceno, int address, int port)
            {
                this.xmno = xmno;
                this.deviceno = deviceno;
                this.address = address;
                this.port = port;
            }
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(fixed_inclinometer_deviceModelGetByNameModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_device fixed_inclinometer_devicemodel = null;
            if (bll.GetModel(model.xmno, model.pointname, out fixed_inclinometer_devicemodel, out mssg))
            {
                model.model = fixed_inclinometer_devicemodel;
                return true;
            }
            return false;
        }
        public class fixed_inclinometer_deviceModelGetByNameModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public NGN.Model.fixed_inclinometer_device model { get; set; }
            public fixed_inclinometer_deviceModelGetByNameModel(int xmno, string pointname)
            {
                this.xmno = xmno;
                this.pointname = pointname;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetChainDeviceModelList(fixed_inclinometer_chainDeviceModelListGetModel model, out string mssg)
        {
            List<NGN.Model.fixed_inclinometer_device> modellist = null;
            if (bll.GetModelList(model.xmno, model.chainname, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class fixed_inclinometer_chainDeviceModelListGetModel
        {
            public int xmno { get; set; }
            public string chainname { get; set; }
            public List<NGN.Model.fixed_inclinometer_device> modellist { get; set; }
            public fixed_inclinometer_chainDeviceModelListGetModel(int xmno, string chainname)
            {
                this.xmno = xmno;
                this.chainname = chainname;
            }
        }



     
        /// <summary>
        /// 关联多个测斜段
        /// </summary>
        public bool ChainMultiUpdate(ChainMultiUpdateModel model, out string mssg)
        {
            
            if (bll.ChainMultiUpdate(model.pointNameStr, model.model, out mssg))
            {
                return true;
            }
            return false;
        }
        public class ChainMultiUpdateModel
        {
            public string pointNameStr { get; set; }
            public NGN.Model.fixed_inclinometer_chain model { get; set; }
            public ChainMultiUpdateModel(string pointNameStr, NGN.Model.fixed_inclinometer_chain model)
            {
                this.pointNameStr = pointNameStr;
                this.model = model;
            }
        }
         
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateAlarm(NGN.Model.fixed_inclinometer_device model, out string mssg)
        {
            return bll.UpdateAlarm(model,out mssg);
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdateAlarm(MultiUpdateAlarmModel model, out string mssg)
        {
            return bll.MultiUpdateAlarm(model.pointNameStr, model.model, out mssg);
              
        }
        public class MultiUpdateAlarmModel
        {
            public string pointNameStr { get; set; }
            public NGN.Model.fixed_inclinometer_device model { get; set; }
            public MultiUpdateAlarmModel(string pointNameStr, NGN.Model.fixed_inclinometer_device model)
            {
                this.pointNameStr = pointNameStr;
                this.model = model;
            }
        }




        /// <summary>
        /// 加载测斜链的所有测斜段
        /// </summary>
        /// <returns></returns>
        public bool ChainFixed_inclinometerNameLoad(ChainFixed_inclinometerNameLoadModel model, out string mssg)
        {

            List<string> devicenamelist = null;
            if (bll.ChainFixed_inclinometerLoad(model.chain_name, model.xmno, out devicenamelist, out mssg))
            {
                model.devicenamelist = devicenamelist;
                return true;
            }
            return false;
        }

        public class ChainFixed_inclinometerNameLoadModel
        {
            public string chain_name { get; set; }
            public int xmno { get; set; }
            public List<string> devicenamelist { get; set; }

            public ChainFixed_inclinometerNameLoadModel(string chain_name,int xmno)
            {
                this.chain_name = chain_name;
                this.xmno = xmno;
            }

        }

        /// <summary>
        /// 清空关联的测斜链
        /// </summary>
        public bool DeleteChain(DeleteChainModel model, out string mssg)
        {
            return bll.DeleteChain(model.xmno,model.chainname,out mssg);
        }
        public class DeleteChainModel
        {
            public int xmno { get; set; }
            public string chainname { get; set; }
            public DeleteChainModel(int xmno,string chainname)
            {
                this.xmno = xmno;
                this.chainname = chainname;
            }
        }

        /// <summary>
        /// 加载测斜链表
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmno"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool PointTableLoad(Fixed_inclinometerPointDeviceTableLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if(bll.PointTableLoad(model.pageIndex,model.rows,model.xmno,model.colName,model.sord,out dt,out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }

        public class Fixed_inclinometerPointDeviceTableLoadModel: SearchCondition
        {
            public int xmno { get; set; }
            public DataTable dt { get; set; }
            public Fixed_inclinometerPointDeviceTableLoadModel(int xmno, int pageIndex, int pagesize, string colName, string sord)
            {
                this.xmno = xmno;
                this.pageIndex = pageIndex;
                this.rows = pagesize;
                this.colName = colName;
                this.sord = sord;
            }
        }


        /// <summary>
        /// 从测斜仪的端口和设备号设置获取点名
        /// </summary>
        /// <returns></returns>
        public bool ProcessFixedInclinometerTOPointModule(ProcessFixedInclinometerTOPointModuleGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_device devicemodel = null;
            if (bll.FixedInclinometerTOPointModule(model.xmno, model.port, model.addressno, out devicemodel, out mssg))
            {
                model.devicemodel = devicemodel;
                return true;
            }
            return false;

        }
        public class ProcessFixedInclinometerTOPointModuleGetModel {
            public int xmno { get; set; }
            public int port { get; set; }
            public string addressno { get; set; }
            public NGN.Model.fixed_inclinometer_device devicemodel { get; set; }
            public ProcessFixedInclinometerTOPointModuleGetModel(int xmno,int port,string addressno)
            {
                this.xmno = xmno;
                this.port = port;
                this.addressno = addressno;
            }
        }

        /// <summary>
        /// 结果数据对象预警处理
        /// </summary>
        /// <param name="alarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmIntoInformation(data.Model.gtalarmvalue alarmvalue, NGN.Model.fixed_inclinometer_orgldata resultModel, out List<string> lscontext)
        {
            lscontext = new List<string>();
            bool result = false;
            bool hasalarm = false;
            List<string> listspace = new List<string>();
            listspace.Add(string.Format("{0}孔深{1}m,{2}, 位移:{3}mm,第{4}次", resultModel.holename, resultModel.deepth, resultModel.time.ToString(), resultModel.a_planediff, resultModel.cyc));
            DataProcessHelper.RangeCompareU("", "" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer) + "", resultModel.a_this_rap, alarmvalue.single_this_scalarvalue_u, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}A日变化{1}", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer), resultModel.a_this_rap));

            DataProcessHelper.RangeCompareL("", "" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer) + "", resultModel.a_this_rap, alarmvalue.single_this_scalarvalue_l, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}A日变化{1}", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer), resultModel.a_this_rap));

            DataProcessHelper.RangeCompareU("", "" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer) + "", resultModel.b_this_rap, alarmvalue.single_this_scalarvalue_u, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}B日变化{1}", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer), resultModel.b_this_rap));

            DataProcessHelper.RangeCompareL("", "" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer) + "", resultModel.b_this_rap, alarmvalue.single_this_scalarvalue_l, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}B日变化{1}", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer), resultModel.b_this_rap));

            DataProcessHelper.RangeCompareU("", "" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer) + "", resultModel.a_ac_diff, alarmvalue.single_ac_scalarvalue_u, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}A累计{1}", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer), resultModel.a_ac_diff));

            DataProcessHelper.RangeCompareL("", "" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer) + "", resultModel.a_ac_diff, alarmvalue.single_ac_scalarvalue_l, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}A累计{1}", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer), resultModel.a_ac_diff));

            DataProcessHelper.RangeCompareU("", "" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer) + "", resultModel.b_ac_diff, alarmvalue.single_ac_scalarvalue_u, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}B累计{1}", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer), resultModel.b_ac_diff));

            DataProcessHelper.RangeCompareL("", "" + data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer) + "", resultModel.b_ac_diff, alarmvalue.single_ac_scalarvalue_l, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}B累计{1}", data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._inclinometer), resultModel.b_ac_diff));

            lscontext.Add(string.Join(" ", listspace));
            return hasalarm;



            //listspace.Add(DataProcessHelper.RangeCompare("", "温度上限", resultModel.temperlate, alarmvalue.temperlate, out result, result));
            //listspace.Add(DataProcessHelper.RangeCompare("", "温度下限", resultModel.temperlate, alarmvalue.temperlateL, out result, result));
            //lscontext.Add(string.Join("  ", listspace));
            //lscontext.Add(string.Join(" ",ls));
            //return result;
        }
        public static int sec = 0;
        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            List<string> ls = new List<string>();
            string mssg = "";

            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.holename,
                time = model.resultModel.time,
                type = "测斜",
                atime = DateTime.Now,
                pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processCgAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, "测斜", model.resultModel.holename, model.resultModel.time);

            splitOnDateBLL.ProcessalarmsplitondateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);

            AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = "测斜",
                pointName = model.resultModel.holename,
                xmno = model.xmno,
                time = model.resultModel.time,
                adate = DateTime.Now,
                cyc = model.resultModel.cyc

            };
            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add("三级预警");
                    pointCheck.alarm = 3;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "测斜", "", model.resultModel.holename, 3, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add("二级预警");
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "测斜", "", model.resultModel.holename, 2, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add("一级预警");
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "测斜", "", model.resultModel.holename, 1, out mssg);
                    return true;
                }


            }
            ls.Add("========预警解除==========");
            pointCheck.alarm = 0;
            pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
            ProcessHotPotColorUpdate(model.xmno, "测斜", "", model.resultModel.holename, 0, out mssg);
            return false;

        }
        public class ProcessPointAlarmfilterInformationModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public List<data.Model.gtalarmvalue> levelalarmvalue { get; set; }
            public NGN.Model.fixed_inclinometer_orgldata resultModel { get; set; }
            public List<string> ls { get; set; }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<data.Model.gtalarmvalue> levelalarmvalue, NGN.Model.fixed_inclinometer_orgldata resultModel, int xmno)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
                this.xmno = xmno;
            }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<data.Model.gtalarmvalue> levelalarmvalue, NGN.Model.fixed_inclinometer_orgldata resultModel)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
            }
            public ProcessPointAlarmfilterInformationModel()
            {

            }
        }
        public class AlarmRecord
        {
            public string pointName { get; set; }
            public string Time { get; set; }
            public string alarm { get; set; }
            public string RecordTime { get; set; }

        }

        public bool ProcessPointAlarmModelGet(ProcessPointAlarmModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_device pointalarm = new NGN.Model.fixed_inclinometer_device();
            if (bll.GetModel(model.xmno, model.pointName, out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }
        public class ProcessPointAlarmModelGetModel
        {
            public int xmno { get; set; }
            public string pointName { get; set; }
            public NGN.Model.fixed_inclinometer_device model { get; set; }
            public ProcessPointAlarmModelGetModel(int xmno, string pointName)
            {
                this.xmno = xmno;
                this.pointName = pointName;
            }
        }

        public bool ProcessHotPotColorUpdate(int xmno, string jclx, string jcoption, string pointName, int color, out string mssg)
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(xmno, jclx, jcoption, pointName, color);
            return layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);

        }

        public bool ProcessXmStateTableLoad(XmStateTableLoadCondition model, out string mssg)
        {
            DataTable dt = new DataTable();
            if (bll.FixedInclinometerStateTabLoad(model.pageIndex, model.rows, model.xmno, model.xmname, model.unitname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }



    }
}