﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessFixed_Inclinometer_chainDataBLL
    {

        public NGN.BLL.fixed_inclinometer_chaindata bll = new NGN.BLL.fixed_inclinometer_chaindata();
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_chaindata model, out string mssg)
        {
            return bll.Add(model,out mssg);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(Fixed_Inclinometer_chainDataModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_chaindata fixed_inclinometer_chaindatamodel = null;
            if (bll.GetModel(model.xmno, model.point_name, model.time, out fixed_inclinometer_chaindatamodel, out mssg))
            {
                model.model = fixed_inclinometer_chaindatamodel;
                return true;
            }
            return false;
        }
        public class Fixed_Inclinometer_chainDataModelGetModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public DateTime time { get; set; }
            public NGN.Model.fixed_inclinometer_chaindata model { get; set; }
            public Fixed_Inclinometer_chainDataModelGetModel(int xmno,string point_name,DateTime time)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.time = time;
            }
        }

        public bool ProcessFixedInclinometerMaxTime(ProcessFixedInclinometerMaxTimeModel model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(model.xmno, model.chainname, out maxTime, out mssg))
            {
                model.maxTime = maxTime;
                return true;
            }
            return false;
        }
        public class ProcessFixedInclinometerMaxTimeModel
        {
            public int xmno { get; set; }
            public string chainname { get; set; }
            public DateTime maxTime { get; set; }
            public ProcessFixedInclinometerMaxTimeModel(int xmno,string chainname)
            {
                this.xmno = xmno;
                this.chainname = chainname; 
            }
        }

        public bool delete_tmp(deletetmpmodel model, out string mssg)
        {
            int cont = 0;
            if (bll.Delete_tmp(model.xmno, out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            return false;
        }

        public class deletetmpmodel
        {
            public int xmno { get; set; }
            public int cont { get; set; }
            public deletetmpmodel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        public bool ProcessResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out string mssg)
        {
            List<global::NGN.Model.fixed_inclinometer_chaindata> modellist = null;
            mssg = "";
            if (bll.GetModelList(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class ProcessResultDataAlarmModelListModel
        {
            public int  xmno { get; set; }
            public List<global::NGN.Model.fixed_inclinometer_chaindata> modellist { set; get; }
            public ProcessResultDataAlarmModelListModel(int  xmno)
            {
                this.xmno = xmno;
            }
        }

       



    }
}