﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NGN.BLL;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public partial class ProcessFixed_Inclinometer_orglDataBLL
    {
        public bool ProcesspointcgFixedInclinometerResultDataMaxTime(GTSurfaceTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.CgMaxTime(model.xmno,  model.pointname, out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }

        public bool ProcesspointcgFixedInclinometerResultDataMinTime(GTSurfaceTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.CgMinTime(model.xmno,  model.pointname, out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            return false;


        }
        public bool ProcessCgResultDataTimeListLoad(NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.CgPointNameCYCDateTimeListGet(model.xmno, model.pointname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessCgDataImport(SurveyDataAddModel model, out string mssg)
        {
            return bll.AddCgData(model.xmno, model.point_name,  model.startcyc, model.importcyc, out mssg);
        }
        public class SurveyDataAddModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public int startcyc { get; set; }
            public int importcyc { get; set; }
            public int endcyc { get; set; }
            public SurveyDataAddModel(int xmno, string point_name, int startcyc, int importcyc)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.startcyc = startcyc;
                this.importcyc = importcyc;
                this.endcyc = importcyc;
            }
            
        }
        //<summary>
        //删除成果数据
        //</summary>
        public bool DeleteCg(SurveyDataAddModel model, out string mssg)
        {
            return bll.DeleteCg(model.xmno, model.point_name, model.startcyc, model.endcyc, out mssg);

        }
        public bool DeleteCgTmp(NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.DeleteTmpModel model, out string mssg)
        {
            return bll.DeleteCgTmp(model.xmno,  out mssg);

        }
        public bool CgResultFixedInclinometerTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.CgResultdataTableLoad(model.pageIndex, model.rows, model.xmno, model.pointname, model.sord, model.startcyc, model.endcyc, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
        }

        public bool CgResultFixedInclinometerTableRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.CgResultTableRowsCount(model.xmno, model.pointname, model.sord, model.startcyc, model.endcyc, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }

        public bool ProcessCgResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model,  out string mssg)
        {
            var modellist = new List<global::NGN.Model.fixed_inclinometer_orgldata>();
            if (bll.GetModelList(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
    }
}