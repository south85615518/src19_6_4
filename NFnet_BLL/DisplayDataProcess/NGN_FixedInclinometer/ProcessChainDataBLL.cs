﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using Tool;
using System.Data.OleDb;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ProcessChainDataBLL
    {
        public static string mssg = "";
        public static NGN.BLL.fixed_inclinometer_chaindata bll = new NGN.BLL.fixed_inclinometer_chaindata();
        /// <summary>
        /// 数据展示日期查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {
            if (model.type == QueryType.QT)
            {
                var processdatemdbModel = new ProcessComBLL.ProcessdatemdbModel(model.unit, model.maxTime);
                ProcessComBLL.Processdatemdb(processdatemdbModel, out mssg);
                model.startTime = processdatemdbModel.sttm;
                model.endTime = processdatemdbModel.edtm;
            }

            ExceptionLog.ExceptionWrite("起始日期:"+model.startTime+"结束日期:"+model.endTime);
            DateTime dtstart = Convert.ToDateTime(model.startTime);

            DateTime dtend = Convert.ToDateTime(model.endTime);

            model.sql = "  select point_name,disp,time from fixed_inclinometer_chaindata where  xmno = '" + model.xmno + "' and time>'"+model.startTime+"' and time <= '"+model.endTime+"'   ";
            ExceptionLog.ExceptionWrite(model.sql);
            ExceptionLog.ExceptionWrite("测斜链查询语句:" + model.sql);
            

        }
        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessInclinometerDbFill(FillInclinometerDbFillCondition model)
        {

            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" point_name ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = model.rqConditionStr + " and  " + Processquerynvlmodel.str + " order by point_name,time asc ";//表名由项目任务决定
                //sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            
            //dt = new DataTable();
            //dt.Columns.Add("sname", Type.GetType("System.String"));
            //dt.Columns.Add("r_1", Type.GetType("System.Double"));
            //dt.Columns.Add("r2", Type.GetType("System.Double"));
            //dt.Columns.Add("dt", Type.GetType("System.DateTime"));

            var chainModel = new QuerystanderdbIntModel(sql,model.xmno);
            if (ProcessComBLL.Processquerystanderdb(chainModel, out mssg))
            {

                model.dt = chainModel.dt;
                return true;
            }
            else
            {
                return false;
            }


        }
       


    }
    
}