﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.NGN_FixedInclinometer
{
    public class ProcessChainDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessChainDataAlarmBLL()
        {
        }
        public ProcessChainDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = xmname;
            this.xmno = xmno;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessFixed_Inclinomater_chainBLL pointAlarmBLL = new ProcessFixed_Inclinomater_chainBLL();
        public ProcessFixed_Inclinometer_chainalarmvalueBLL alarmBLL = new ProcessFixed_Inclinometer_chainalarmvalueBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
        public static ProcessFixed_Inclinometer_chainDataBLL chainBLL = new ProcessFixed_Inclinometer_chainDataBLL();
        public bool main()
        {

            return TestFixedInclinometerModelList();

        }
        public bool TestFixedInclinometerModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessFixed_Inclinometer_chainDataBLL.ProcessResultDataAlarmModelListModel(xmno);
            
            if (chainBLL.ProcessResultDataAlarmModelList(processResultDataAlarmModelListModel, out mssg))
            {
                List<global::NGN.Model.fixed_inclinometer_chaindata> chaindatalist = processResultDataAlarmModelListModel.modellist;
                //ProcessPrintMssg.Print(mssg);
                FixedInclinometerPointAlarm(chaindatalist);
                return true;
            }
            return false;

            //ProcessPrintMssg.Print(mssg);

        }
        public NGN.Model.fixed_inclinometer_chain TestPointAlarmValue(string pointName)
        {
            var processPointAlarmModelGetModel = new ProcessFixed_Inclinomater_chainBLL.fixed_inclinometer_chainModelGetByNameModel(xmno, pointName);
            NGN.Model.fixed_inclinometer_chainalarmvalue model = null;
            if (pointAlarmBLL.GetModel(processPointAlarmModelGetModel, out mssg))
            {
                return processPointAlarmModelGetModel.model;
            }
            return null;
        }
        public List<NGN.Model.fixed_inclinometer_chainalarmvalue> TestAlarmValueList(NGN.Model.fixed_inclinometer_chain pointalarm)
        {
            List<NGN.Model.fixed_inclinometer_chainalarmvalue> alarmvalueList = new List<NGN.Model.fixed_inclinometer_chainalarmvalue>();
            //一级
            var processAlarmModelGetByNameModel = new ProcessFixed_Inclinometer_chainalarmvalueBLL.Fixed_Inclinometer_chainModelGetModel( xmno, pointalarm.firstAlarmName);
            if (alarmBLL.GetModel(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("一级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessFixed_Inclinometer_chainalarmvalueBLL.Fixed_Inclinometer_chainModelGetModel(xmno, pointalarm.secondAlarmName);
            if (alarmBLL.GetModel(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("二级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessFixed_Inclinometer_chainalarmvalueBLL.Fixed_Inclinometer_chainModelGetModel(xmno, pointalarm.thirdAlarmName);
            if (alarmBLL.GetModel(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("三级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<NGN.Model.fixed_inclinometer_chainalarmvalue> levelalarmvalue, NGN.Model.fixed_inclinometer_chaindata resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessFixed_Inclinomater_chainBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (pointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));

                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public bool FixedInclinometerPointAlarm(List<global::NGN.Model.fixed_inclinometer_chaindata> lc)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format("===================================={0}=================================", DateTime.Now));
            ls.Add(string.Format("===================================={0}=================================", xmname));
            ls.Add(string.Format("===================================={0}=================================", "深部位移--测斜链--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (global::NGN.Model.fixed_inclinometer_chaindata cl in lc)
            {
               
                NGN.Model.fixed_inclinometer_chain pointvalue = TestPointAlarmValue(cl.point_name); 
                List<NGN.Model.fixed_inclinometer_chainalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> ResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        //public void FixedInclinometerPointAlarm(NGN.Model.fixed_inclinometer_chaindata dirnet)
        //{

        //    NGN.Model.fixed_inclinometer_chain pointvalue = TestPointAlarmValue(dirnet.point_name);
        //    if (pointvalue == null) return;
        //    List<NFnet_DAL.MODEL.alarmvalue> alarmList = TestAlarmValueList(pointvalue);
        //    TestPointAlarmfilterInformation(alarmList, dirnet);
        //}




    }
}