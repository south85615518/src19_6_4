﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NGN.BLL;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public partial class ProcessFixed_Inclinometer_orglDataBLL
    {
        public fixed_inclinometer_orgldata bll = new fixed_inclinometer_orgldata();
       

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(NGN.Model.fixed_inclinometer_orgldata model, out string mssg)
        {
            return bll.Add(model,out mssg);
        }
        public bool Delete(NGN.Model.fixed_inclinometer_orgldata model, out string mssg)
        {
            return bll.Delete(model, out mssg);
        }
        public bool delete_tmp(deletetmpmodel model,out string mssg)
        {
            int cont = 0;
            if(bll.Delete_tmp(model.xmno,out cont,out mssg))
            {
                model.cont = cont;
                return true;
            }
            return false;
        }

        public class deletetmpmodel
        {
            public int xmno { get; set; }
            public int cont { get; set; }
            public deletetmpmodel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        public bool ProcesspointFixedInclinometerResultDataMaxTime(GTSurfaceTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(model.xmno, model.pointname, out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }

        public bool ProcesspointFixedInclinometerResultDataMinTime(GTSurfaceTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.MinTime(model.xmno, model.pointname, out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            return false;


        }
        public bool ProcessResultDataTimeListLoad(NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.PointNameDateTimeListGet(model.xmno, model.pointname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }


        public bool ResultFixedInclinometerTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ResultdataTableLoad(model.pageIndex, model.rows, model.xmno, model.pointname, model.sord, model.startcyc, model.endcyc, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
        }

        public bool ResultFixedInclinometerTableRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.ResultTableRowsCount(model.xmno.ToString(), model.pointname, model.sord, model.startcyc, model.endcyc, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(fixed_inclinometer_orgldataModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_orgldata fixed_inclinometer_orgldatamodel = null;
            if (bll.GetModel(model.xmno, model.point_name,model.deepth ,model.time, out fixed_inclinometer_orgldatamodel, out mssg))
            {
                model.model = fixed_inclinometer_orgldatamodel;
                return true;
            }
            return false;
        }

        public class fixed_inclinometer_orgldataModelGetModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public DateTime time { get; set; }
            public double deepth { get; set; }
            public NGN.Model.fixed_inclinometer_orgldata model { get; set; }
            public fixed_inclinometer_orgldataModelGetModel(int xmno,string point_name,double deepth,DateTime time)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.time = time;
                this.deepth = deepth;
            }
        }

        public bool ProcessResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out string mssg)
        {
            List<global::NGN.Model.fixed_inclinometer_orgldata> modellist = null;
            mssg = "";
            if (bll.GetModelList(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class ProcessResultDataAlarmModelListModel
        {
            public int xmno { get; set; }
            public List<global::NGN.Model.fixed_inclinometer_orgldata> modellist { set; get; }
            public ProcessResultDataAlarmModelListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        public bool ProcessLastDateTimeModelList(ProcessLastDateTimeModelListModel model,out string mssg)
        {
            List<global::NGN.Model.fixed_inclinometer_orgldata> modellist = new List<NGN.Model.fixed_inclinometer_orgldata>();
            if (bll.GetModelList(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class ProcessLastDateTimeModelListModel
        {
            public int xmno { get; set; }
            public string chain_name { get; set; }
            public DateTime lasttime { get; set; }
            public List<global::NGN.Model.fixed_inclinometer_orgldata> modellist { set; get; }
            public ProcessLastDateTimeModelListModel(int xmno,string chain_name,DateTime lasttime)
            {
                this.xmno = xmno;
                this.chain_name = chain_name;
                this.lasttime = lasttime;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetLastTimeModel(fixed_inclinometer_orgldataModelGetModel model, out string mssg)
        {
            NGN.Model.fixed_inclinometer_orgldata fixed_inclinometer_orgldatamodel = null;
            if (bll.GetLastTimeModel(model.xmno, model.point_name, model.deepth, model.time, out fixed_inclinometer_orgldatamodel, out mssg))
            {
                model.model = fixed_inclinometer_orgldatamodel;
                return true;
            }
            return false;
        }


        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool ProcessInsertCycStep(InsertCycStepModel model, out string mssg)
        {
            return bll.InsertCycStep(model.xmno, model.holename, model.startcyc, out mssg);
        }

        public class InsertCycStepModel
        {
            public int xmno { get; set; }
            public int startcyc { get; set; }
            public string holename { get; set; }
            public InsertCycStepModel(int xmno, int startcyc,string holename)
            {
                this.xmno = xmno;
                this.startcyc = startcyc;
                this.holename = holename;
            }
        }

        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(IsTypeDataModel model, out string mssg)
        {
            return bll.IsInsertData(model.xmno, model.holename, model.dt, out mssg);

        }
        public class IsTypeDataModel
        {
            public int xmno { get; set; }
            public DateTime dt { get; set; }
            public string holename { get; set; }
            public IsTypeDataModel(int xmno, DateTime dt, string holename)
            {
                this.xmno = xmno;
                this.dt = dt;
                this.holename = holename;
            }
        }
        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(IsInsertCycExistModel model, out string mssg)
        {
            return bll.IsInsertCycExist(model.xmno,model.holename ,model.cyc, out mssg);
        }
        public class IsInsertCycExistModel
        {
            public int xmno { get; set; }
            public int cyc { get; set; }
            public string holename { get; set; }
            public IsInsertCycExistModel(int xmno, int cyc,string holename)
            {
                this.xmno = xmno;
                this.cyc = cyc;
                this.holename = holename;
            }
        }

        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(InsertDataCycGetModel model, out string mssg)
        {
            int cyc = -1;
            if (bll.InsertDataCycGet(model.xmno, model.holename, model.dt, out cyc, out mssg))
            {
                model.cyc = cyc;
                return true;
            }
            return false;
        }
        public class InsertDataCycGetModel
        {
            public int xmno { get; set; }
            public DateTime dt { get; set; }
            public int cyc { get; set; }
            public string holename { get; set; }
            public InsertDataCycGetModel(int xmno, DateTime dt,string holename)
            {
                this.xmno = xmno;
                this.dt = dt;
                this.holename = holename;
            }
        }


        public bool PointMaxDateTimeGet(PointMaxDateTimeGetModel model, out string mssg)
        {
            DateTime dt = new DateTime();
            if(bll.PointMaxDateTimeGet(model.xmno,out dt,out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;

        }
        public class PointMaxDateTimeGetModel
        {
            public int xmno { get; set; }
            public DateTime dt { get; set; }
            public PointMaxDateTimeGetModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        /// <summary>
        /// 结果数据端点周期获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataExtremely(ResultDataExtremelyCondition model, out string mssg)
        {
            string extremelyCyc = "";
            if (bll.ExtremelyCycGet(model.xmno, model.holename, model.dateFunction, out mssg, out extremelyCyc))
            {
                model.extremelyCyc = extremelyCyc;
                return true;
            }
            else
            {
                return false;
            }
        }
        public class ResultDataExtremelyCondition
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public int xmno { get; set; }
            public string holename { get; set; }
            /// <summary>
            /// 日期函数
            /// </summary>
            public string dateFunction { get; set; }
            /// <summary>
            /// 周期
            /// </summary>
            public string extremelyCyc { get; set; }

            public ResultDataExtremelyCondition(int xmno,string holename ,string dateFunction)
            {
                this.xmno = xmno;
                this.holename = holename;
                this.dateFunction = dateFunction;
                
            }
        }

        /// <summary>
        /// 判断记录是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool IsInclinometerDataExist(IsInclinometerDataExistModel model, out string mssg)
        {
            return bll.IsInclinometerDataExist(model.xmno, model.pointname, model.dt, out mssg);

        }
        public class IsInclinometerDataExistModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public DateTime dt { get; set; }
            public IsInclinometerDataExistModel(int xmno, string pointname, DateTime dt)
            {
                this.xmno = xmno;
                this.pointname = pointname;
                this.dt = dt;
            }
            public IsInclinometerDataExistModel()
            {

            }
        }
        public bool ProcessReportDataView(ProcessReportDataViewModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ReportDataView(model.cyclist, model.pageIndex, model.rows, model.xmno, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessReportDataViewModel : ResultDataLoadCondition
        {
            public List<string> cyclist { get; set; }
            public ProcessReportDataViewModel(List<string> cyclist, int pageIndex, int pageSize, int xmno, string sord)
            {
                this.cyclist = cyclist;
                this.pageIndex = pageIndex;
                this.rows = pageSize;
                this.xmno = xmno;
                this.sord = sord;
          
            }
        }


    }
}