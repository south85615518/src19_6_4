﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.NGN_FixedInclinometer
{
    public partial class ProcessOrglDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessOrglDataAlarmBLL()
        {
        }
        public ProcessOrglDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = xmname;
            this.xmno = xmno;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessFixed_Inclinomater_deviceBLL inclinometerdeviceBLL = new ProcessFixed_Inclinomater_deviceBLL();

        public NFnet_BLL.DisplayDataProcess.GT.ProcessPointAlarmBLL pointAlarmBLL = new NFnet_BLL.DisplayDataProcess.GT.ProcessPointAlarmBLL();

        public NFnet_gtalarmvaluebll.DisplayDataProcess.GT.ProcessAlarmValueBLL alarmBLL = new NFnet_gtalarmvaluebll.DisplayDataProcess.GT.ProcessAlarmValueBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
        public static ProcessFixed_Inclinometer_orglDataBLL deviceBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public bool main()
        {

            return TestFixedInclinometerModelList();

        }
        public bool TestFixedInclinometerModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessFixed_Inclinometer_orglDataBLL.ProcessResultDataAlarmModelListModel(xmno);
            
            if (deviceBLL.ProcessResultDataAlarmModelList(processResultDataAlarmModelListModel, out mssg))
            {
                List<global::NGN.Model.fixed_inclinometer_orgldata> devicedatalist = processResultDataAlarmModelListModel.modellist;
                //ProcessPrintMssg.Print(mssg);
                FixedInclinometerPointAlarm(devicedatalist);
                return true;
            }
            return false;

            //ProcessPrintMssg.Print(mssg);

        }
     
     
     
        public bool CgFixedInclinometerPointCurrentAlarmCreate(List<global::NGN.Model.fixed_inclinometer_orgldata> lc)
        {
            alarmInfoList = new List<string>();
            foreach (global::NGN.Model.fixed_inclinometer_orgldata cl in lc)
            {

                data.Model.gtpointalarmvalue pointvalue = TestPointAlarmValue(cl.holename);
                List<data.Model.gtalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> CgResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            cgmain();
            return this.alarmInfoList;
        }
        //public void FixedInclinometerPointAlarm(NGN.Model.fixed_inclinometer_orgldata dirnet)
        //{

        //    NGN.Model.fixed_inclinometer_device pointvalue = TestPointAlarmValue(dirnet.point_name);
        //    if (pointvalue == null) return;
        //    List<NFnet_DAL.MODEL.alarmvalue> alarmList = TestAlarmValueList(pointvalue);
        //    TestPointAlarmfilterInformation(alarmList, dirnet);
        //}




    }
}