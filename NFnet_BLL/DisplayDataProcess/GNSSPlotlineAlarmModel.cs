﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GNSSPlotlineAlarmModel
    {
        public string pointname { get; set; }
        public GPS.Model.gnssalarmvalue firstalarm { get; set; }
        public GPS.Model.gnssalarmvalue secalarm { get; set; }
        public GPS.Model.gnssalarmvalue thirdalarm { get; set; }

    }
}