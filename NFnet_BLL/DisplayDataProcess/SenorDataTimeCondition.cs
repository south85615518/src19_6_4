﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SenorDataTimeCondition
    {
        public int xmno { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public InclimeterDAL.Model.senor_data model { get; set; }
        public SenorDataTimeCondition(int xmno, string pointname, DateTime dt)
        {
            this.xmno = xmno;
            this.pointname = pointname;
            this.dt = dt;
        }
    }
}