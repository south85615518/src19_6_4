﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Tool;
using System.IO;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.Settlement
{
    public class ProcessSettlementResultDataAlarmBLL
    {

        #region  变量声明
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessSettlementAlarmValueBLL alarmBLL = new ProcessSettlementAlarmValueBLL();
        public static string mssg = "";
        public static string timeJson = "";
        public static ProcessSettlementResultDataBLL resultBLL = new ProcessSettlementResultDataBLL();
        #endregion
        #region 成员变量
        public string xmname{ get; set; }
        public int xmno{ get; set; }
        public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        #endregion
        
        public ProcessSettlementResultDataAlarmBLL()
        {
        }
        public ProcessSettlementResultDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = xmname;
            this.xmno = xmno;
        }
        
        public bool main()
        {

            return GetSettlementResultDataModelList();

        }
        public bool GetSettlementResultDataModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessSettlementResultDataBLL.ProcessResultDataAlarmModelListModel(xmno);
            List<global::Settlement.Model.Settlementresultdata> modellist = new List<global::Settlement.Model.Settlementresultdata>();
            if (resultBLL.ProcessResultDataAlarmModelList(processResultDataAlarmModelListModel, out modellist, out mssg))
            {
                SettlementResultDataPointAlarm(modellist);
                return true;
            }
            return false;

        }
        public global::Settlement.Model.settlementpointalarmvalue     GetPointAlarmValue(string pointName)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointName);
            global::Settlement.Model.settlementpointalarmvalue model = null;
            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                return processPointAlarmModelGetModel.model;

            }
            return null;
        }
        public List<global::Settlement.Model.settlementalarmvalue> GetAlarmValueList(global::Settlement.Model.settlementpointalarmvalue pointalarm)
        {
            List<global::Settlement.Model.settlementalarmvalue> alarmvalueList = new List<global::Settlement.Model.settlementalarmvalue>();
            //一级
            var processAlarmModelGetByNameModel = new ProcessSettlementAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.firstAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("一级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessSettlementAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.secondAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("二级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessSettlementAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.thirdAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("三级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            return alarmvalueList;
        }
        public void GetPointAlarmfilterInformation(List<global::Settlement.Model.settlementalarmvalue> levelalarmvalue, global::Settlement.Model.Settlementresultdata resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (pointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));

                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public bool SettlementResultDataPointAlarm(List<global::Settlement.Model.Settlementresultdata> lc)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format("===================================={0}=================================", DateTime.Now));
            ls.Add(string.Format("===================================={0}=================================", xmname));
            ls.Add(string.Format("===================================={0}=================================", "--分层沉降--沉降仪--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (global::Settlement.Model.Settlementresultdata cl in lc)
            {
                if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}Settlement", xmname))) return false;
                global::Settlement.Model.settlementpointalarmvalue pointvalue = GetPointAlarmValue(cl.pointname);
                List<global::Settlement.Model.settlementalarmvalue> alarmList = GetAlarmValueList(pointvalue);
                GetPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> ResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        public void SettlementResultDataPointAlarm(global::Settlement.Model.Settlementresultdata resultmodel)
        {

            global::Settlement.Model.settlementpointalarmvalue pointvalue = GetPointAlarmValue(resultmodel.pointname);
            if (pointvalue == null) return;
            List<global::Settlement.Model.settlementalarmvalue> alarmList = GetAlarmValueList(pointvalue);
            GetPointAlarmfilterInformation(alarmList, resultmodel);
        }




    }
}