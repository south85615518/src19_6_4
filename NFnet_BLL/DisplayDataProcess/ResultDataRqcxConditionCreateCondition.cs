﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DisplayDataProcess
{
    public class ResultDataRqcxConditionCreateCondition
    {
        
            /// <summary>
            /// 起始日期
            /// </summary>
            public string startTime { get; set; }
            /// <summary>
            /// 结束日期
            /// </summary>
            public string endTime { get; set; }
            /// <summary>
            /// 快捷查询日期条件
            /// </summary>
            public string unit { get; set; }
            /// <summary>
            /// 生成的SQL语句
            /// </summary>
            public string sql { get; set; }
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 起始周期
            /// </summary>
            public string minCyc { get; set; }
            /// <summary>
            /// 结束周期
            /// </summary>
            public string maxCyc { get; set; }
            /// <summary>
            /// 查询类型
            /// </summary>
            public DateTime maxTime { get; set; }

            public data.Model.gtsensortype datatype { get; set; }
            
            public QueryType type { get; set; }
            public ResultDataRqcxConditionCreateCondition( string startTime, string endTime, QueryType type, string unit, string xmname, DateTime maxTime)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.type = type;
                this.unit = unit;
                this.xmname = ProcessAspectIndirectValue.GetXmnoFromXmnameStr(xmname);
                this.maxTime = maxTime;
                this.datatype = datatype;
                //this.xmno = ProcessAspectIndirectValue.GetXmnoFromXmname(xmname);
            }


            public ResultDataRqcxConditionCreateCondition(data.Model.gtsensortype datatype, string startTime, string endTime, QueryType type, string unit, int xmno, DateTime maxTime)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.type = type;
                this.unit = unit;
                this.xmno = xmno;
                this.maxTime = maxTime;
                this.datatype = datatype;
            }
            public ResultDataRqcxConditionCreateCondition( string startTime, string endTime, QueryType type, string unit, int xmno, DateTime maxTime)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.type = type;
                this.unit = unit;
                this.xmno = xmno;
                this.maxTime = maxTime;
    
            }
    }
}