﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeBLL
    {
        public Gauge.BLL.smos_module_water_gauge_data bll = new Gauge.BLL.smos_module_water_gauge_data();
        public Gauge.BLL.gaugedatareport gaugebll= new Gauge.BLL.gaugedatareport();
        public static Gauge.BLL.gaugetimeinterval gaugetimeintervalBLL = new Gauge.BLL.gaugetimeinterval();
        public bool ProcessGaugePointLoad(ProcessGaugePointLoadModel model, out string mssg)
        {
            List<string> pointlist = new List<string>();
            if (bll.PointLoad(model.xmno, out pointlist, out mssg))
            {
                model.pointlist = pointlist;
                return true;
            }
            return false;
        }
        public class ProcessGaugePointLoadModel
        {
            public int xmno { get; set; }
            public List<string> pointlist { get; set; }
            public ProcessGaugePointLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        public bool ProcessPointNewestDateTimeGet(InclinometerPointNewestDateTimeCondition model, out string mssg)
        {

            DateTime dt = new DateTime();
            if (gaugebll.PointNewestDateTimeGet(model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;


        }


        public bool ProcessSenorDataTime(ProcessSenorDataTimeModel model, out string mssg)
        {

            Gauge.Model.gaugedatareport senorDataModel = new Gauge.Model.gaugedatareport();
            if (gaugebll.GetModel(model.pointname, model.dt, out senorDataModel, out mssg))
            {
                model.model = senorDataModel;
                return true;

            }
            return false;
        }
        public class ProcessSenorDataTimeModel
        {
            public string pointname { get; set; }
            public DateTime dt { get; set; }
            public Gauge.Model.gaugedatareport model { get; set; }
            public ProcessSenorDataTimeModel(string pointname, DateTime dt)
            {
                this.pointname = pointname;
                this.dt = dt;
            }
        }

        /// <summary>
        /// 时间间隔获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessGaugeTimeInterval(gaugetimeintervalModel model, out string mssg)
        {
            DataTable dt = null;
            if (gaugetimeintervalBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 时间间隔表获取
        /// </summary>
        public class gaugetimeintervalModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public gaugetimeintervalModel(int xmno, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessGaugeTimeIntevalAdd(Gauge.Model.gaugetimeinterval model, out string mssg)
        {
            return gaugetimeintervalBLL.Add(model, out mssg);
        }



    }
}