﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.Other;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.GAUGE
{
    public class ProcessGaugeDATABLL
    {
        public Gauge.BLL.smos_module_water_gauge_data bll = new Gauge.BLL.smos_module_water_gauge_data();
        public Gauge.BLL.gaugedatareport gaugebll = new Gauge.BLL.gaugedatareport();
        //public static DTU.BLL.dtudata dtudataBLL = new DTU.BLL.dtudata();
        //public bool ProcessDTUDATAInsertBLL(DTU.Model.dtudata model,out string mssg)
        //{
        //    return dtudataBLL.Add(model,out mssg);
        //}
        public bool ProcessGaugeDATAMaxTimeBLL(ProcessGaugeDATAMaxTimeBLLModel model, out string mssg)
        {
            DateTime dt = new DateTime();
            if (bll.MaxTime(model.pointnamestr, out dt, out  mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessGaugeDATAMaxTimeBLLModel
        {
            public string pointnamestr { get; set; }
            public DateTime dt { get; set; }
            public ProcessGaugeDATAMaxTimeBLLModel(string pointnamestr)
            {
                this.pointnamestr = pointnamestr;
                dt = new DateTime();
            }
        }
        //public bool ProcessDTUDATAModelGet(ProcessDTUDATAModelGetModel model,out string mssg)
        //{
        //    DTU.Model.dtudatareport dtumodel = new DTU.Model.dtudatareport();
        //    if (dtudataBLL.GetModel(model.xmno, model.pointname, model.dt, out dtumodel, out mssg))
        //    {
        //        model.model = dtumodel;
        //        return true;
        //    }
        //    return false;
        //}
        //public class ProcessDTUDATAModelGetModel
        //{
        //    public int xmno { get; set; }
        //    public string pointname { get; set; }
        //    public DateTime dt { get; set; }
        //    public DTU.Model.dtudatareport model { get; set; }
        //    public ProcessDTUDATAModelGetModel(int xmno,string pointname,DateTime dt)
        //    {
                
        //    }
        //}
        //public bool ProcessDeleteTmp(int xmno, out string mssg)
        //{
        //    return dtudataBLL.DeleteTmp(xmno, out mssg);
        //}
        //public bool ProcessPointNewestDateTimeGet(InclinometerPointNewestDateTimeCondition model, out string mssg)
        //{

        //    DateTime dt = new DateTime();
        //    if (dtudataBLL.PointNewestDateTimeGet(model.xmno, model.pointname, out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    return false;


        //}
        //public bool ProcessSenorMaxTime(SenorMaxTimeCondition model, out string mssg)
        //{
        //    DateTime dt = new DateTime();
        //    if (dtudataBLL.MaxTime(model.xmno, out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    return false;

        //}
        //public bool ProcessSenorMaxTime(SenorPointMaxTimeModel model, out string mssg)
        //{
        //    DateTime dt = new DateTime();
        //    if (dtudataBLL.MaxTime(model.xmno,model.point_name ,out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    return false;

        //}
        //public class SenorPointMaxTimeModel : SenorMaxTimeCondition
        //{
        //    public int xmno { get; set; }
        //    public string point_name { get; set; }
        //    public DateTime dt { get; set; }
        //    public SenorPointMaxTimeModel(int xmno,string point_name)
        //    {
        //        this.xmno = xmno;
        //        this.point_name = point_name;
        //    }
        //}
        ///// <summary>
        ///// 结果数据报表数据表获取
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="mssg"></param>
        ///// <returns></returns>
        //public bool ProcessResultDataReportTableCreate(SenorDataReportTableCreateCondition model, out string mssg)
        //{
        //    DataTable dt = null;
        //    if (dtudataBLL.ResultDataReportPrint(model.sql, model.xmno, out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        //public bool ProcessSenorDataLoad(SenorDataLoadCondition model, out string mssg)
        //{
        //    mssg = "";
        //    DataTable dt = new DataTable();
        //    if (dtudataBLL.DTUDATATableLoad(model.starttime, model.endtime, model.startPageIndex, model.endPageIndex, model.xmno, model.pointname, model.sord, out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    return false;
        //}
        //public bool ProcessSenorDataRecordsCount(SenorDataCountLoadCondition model, out string mssg)
        //{
        //    string totalCont = "0";
        //    if (dtudataBLL.DTUTableRowsCount(model.starttime, model.endtime, model.xmno, model.pointname, out totalCont, out mssg))
        //    {
        //        model.totalCont = totalCont;
        //        return true;
        //    }
        //    else
        //    {

        //        return false;
        //    }

        //}
        ///// <summary>
        ///// 结果数据报表数据表获取
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="mssg"></param>
        ///// <returns></returns>
        ////public bool ProcessResultDataReportTableCreate(SenorDataReportTableCreateCondition model, out string mssg)
        ////{
        ////    DataTable dt = null;
        ////    if (dtudataBLL.ResultDataReportPrint(model.sql, model.xmname, out dt, out mssg))
        ////    {
        ////        model.dt = dt;
        ////        return true;
        ////    }
        ////    else
        ////    {
        ////        return false;
        ////    }
        ////}
        //public bool ProcessDATATmpDel(int xmno,out string mssg)
        //{
        //    return dtudataBLL.DTUDATATempDel(xmno, out mssg);
        //}
        //public bool ProcessDTUAlarmModelListGet(ProcessDTUAlarmModelListGetModel model,out string mssg)
        //{
        //    List<DTU.Model.dtudatareport> dtumodel = null;
        //    if (dtudataBLL.GetList(model.xmno, out dtumodel, out mssg))
        //    {
        //        model.model = dtumodel;
        //        return true;
        //    }
        //    return false;

        //}

        //public class ProcessDTUAlarmModelListGetModel
        //{
        //    public int xmno { get; set; }
        //    public List<DTU.Model.dtudatareport> model { get; set; }
        //    public ProcessDTUAlarmModelListGetModel(int xmno)
        //    {
        //        this.xmno = xmno;
        //    }
        //}

        //public bool ProcessSenorPointNameTimesListLoad(SenorPointNameCycListCondition model, out string mssg)
        //{
        //    List<string> ls = new List<string>();
        //    if (dtudataBLL.PointNameTimesListGet(model.xmno, model.pointname, out ls, out mssg))
        //    {
        //        model.ls = ls;
        //        return true;
        //    }
        //    return false;
        //}
        //public class ProcessDTUDATATimesListLoadModel
        //{
        //    public int xmno
        //}



        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessGaugeDbFill(FillInclinometerDbFillCondition model)
        {
            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "time");
            model.rqConditionStr = model.rqConditionStr.Replace("#_cyc", "deep");
            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "point_name");
            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" point_name ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = "select point_name,deep,predeep,thisdeep,acdeep,rap,time,holedepth,times  from  gaugedatareport  where  " + Processquerynvlmodel.str +"  and hour(time)in (2,6,10,14,18,22) and minute(time) < 30  "+ "  ";//表名由项目任务决定
                sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            var queryremoteModel = new QueryremoteModel(sql, "smos_client");
            if (ProcessComBLL.Processqueryremotedb(queryremoteModel, out mssg))
            {
                model.dt = queryremoteModel.dt;
                return true;
            }
            else
            {
                return false;
            }


        }
        /// <summary>
        /// 根据表的分页单位获取当前记录在表中的页数
        /// </summary>
        /// <param name="pointname">点名</param>
        /// <param name="date">时间</param>
        /// <param name="deep">深度</param>
        /// <param name="dt">数据表</param>
        /// <param name="pointnameStr">点名字段名</param>
        /// <param name="deepstr">深度字段名</param>
        /// <param name="dateStr">日期字段名</param>
        /// <param name="pageSize">页数</param>
        /// <returns></returns>
        public int PageIndexFromTabCYC(string pointname, string date, DataTable dt, string pointnameStr,  string dateStr, int pageSize)
        {

            DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr] ;
                ExceptionLog.ExceptionWrite(a);
                if (drv[pointnameStr].ToString() == pointname && Convert.ToDateTime(date.Trim()) == Convert.ToDateTime(drv[dateStr].ToString()) )
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }
        public bool ProcessgaugeAlarmModelListGet(ProcessgaugeAlarmModelListGetModel model, out string mssg)
        {
            List<Gauge.Model.gaugedatareport> gaugemodel = null;
            if (gaugebll.GetList(model.pointnamelist, out gaugemodel, out mssg))
            {
                model.model = gaugemodel;
                return true;
            }
            return false;

        }
        public class ProcessgaugeAlarmModelListGetModel
        {
            
            public List<Gauge.Model.gaugedatareport> model { get; set; }
            public List<string> pointnamelist { get; set; }
            public ProcessgaugeAlarmModelListGetModel(List<string> pointnamelist)
            {
                
                this.pointnamelist = pointnamelist;
            }
        }
        public bool GetAlarmTableCont(GetAlarmTableContModel model, out string mssg)
        {
            int cont = 0;
            if (gaugebll.GetAlarmTableCont(model.pointnamelist, out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            return false;
        }
        public class GetAlarmTableContModel
        {
            public List<string> pointnamelist { get; set; }
            public int cont { get; set; }

            public GetAlarmTableContModel( List<string> pointnamelist)
            {
                this.pointnamelist = pointnamelist;

            }
        }
        public bool ProcessGaugeDataDelete(DataDeleteCondition model, out string mssg)
        {
            return gaugebll.Delete(model.point_name, model.dt, out mssg);
        }
    }
}