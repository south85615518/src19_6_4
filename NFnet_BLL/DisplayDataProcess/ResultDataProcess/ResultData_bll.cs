﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL.DataProcess.ResultDataProcess;
using NFnet_DAL.ResultDataProcess.DataProcess;
using NFnet_MODAL;

namespace NFnet_BLL.DataProcess.ResultDataProcess
{
    
    public class ResultData_bll
    {
        private ResultDataSearch dal = new ResultDataSearch();
        public bool List(ResultDataSearchCondition model, out string jsondata)
        {
            try
            {
                return dal.List(model,out jsondata);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex,"结果数据获取出错:");
                jsondata = "";
                return false;
            }
        }
    }
}