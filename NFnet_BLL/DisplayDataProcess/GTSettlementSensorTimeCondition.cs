﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GTSettlementSensorTimeCondition
    {
        public int xmno { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public GTSettlementSensorTimeCondition(string xmname)
        {
            this.xmno = xmno;
        }
        public GTSettlementSensorTimeCondition(int xmno,string point_name)
        {
            this.xmno = xmno;
            this.pointname = point_name;
        }
        public GTSettlementSensorTimeCondition()
        {
            
        }
    }
}