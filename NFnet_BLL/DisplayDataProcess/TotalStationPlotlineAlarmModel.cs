﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class TotalStationPlotlineAlarmModel
    {
        public string pointname { get; set; }
        public NFnet_DAL.MODEL.alarmvalue firstalarm { get; set; }
        public NFnet_DAL.MODEL.alarmvalue secalarm { get; set; }
        public NFnet_DAL.MODEL.alarmvalue thirdalarm { get; set; }

    }
}