﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class DataFileDescodeTaskModel
    {
        public string xmname { get; set; }
        public string path { get; set; }
        public string filetype { get; set; }
        public DateTime lastdescodetime { get; set; }
        public bool descoding { get; set; }
        public DataFileDescodeTaskModel(string xmname, string path, string filetype, DateTime lastdescodetime,bool descoding)
        {
            this.xmname = xmname;
            this.path = path;
            this.filetype = filetype;
            this.lastdescodetime = lastdescodetime;
            this.descoding = descoding;
        }
        public DataFileDescodeTaskModel()
        {
          
        }
    }
}