﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GaugePlotlineAlarmModel
    {
        public string pointname { get; set; }
        public Gauge.Model.reservoirwaterlevel firstalarm { get; set; }
        public Gauge.Model.reservoirwaterlevel secalarm { get; set; }
        public Gauge.Model.reservoirwaterlevel thirdalarm { get; set; }

    }
}