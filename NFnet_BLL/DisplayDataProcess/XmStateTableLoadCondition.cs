﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    public class XmStateTableLoadCondition:SearchCondition
    {
        /// <summary>
        /// 预警参数表
        /// </summary>
        public DataTable dt { get; set; }
        public int xmno { get; set; }
        public string unitname { get; set; }
        public XmStateTableLoadCondition()
        {
 
        }
        public XmStateTableLoadCondition(int pageIndex, int pageSize, int xmno, string xmname, string unitname, string colName, string sord)
        {
            this.pageIndex = pageIndex;
            this.rows = pageSize;
            this.xmno = xmno;
            this.xmname = xmname;
            this.colName = colName;
            this.sord = sord;
            this.unitname = unitname;
        }
    }
}