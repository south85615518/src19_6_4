﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTimeAlarmBLL
    {
        public DTU.BLL.dtutimealarm taskBLL = new DTU.BLL.dtutimealarm();
        public bool ProcessDTUTimeAlarmLoad(ProcessDTUTimeAlarmLoadModel model, out string mssg)
        {
            DTU.Model.dtutimealarm dtutaskmodel = new DTU.Model.dtutimealarm();
            if (taskBLL.GetModel(model.xmno, model.point_name, out dtutaskmodel, out mssg))
            {
                model.model = dtutaskmodel;
                return true;
            }
            return false;
        }
        public class ProcessDTUTimeAlarmLoadModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public DTU.Model.dtutimealarm model { get; set; }
            public ProcessDTUTimeAlarmLoadModel(int xmno, string point_name)
            {
                this.xmno = xmno;
                this.point_name = point_name;
            }
        }
        public bool ProcessDTUTimeAlarmAdd(DTU.Model.dtutimealarm model, out string mssg)
        {
            if (!taskBLL.Exists(model.module, model.xmno, out mssg))
                return taskBLL.Add(model, out mssg);
            return false;
        }
        public bool ProcessDTUTimeAlarmUpdate(DTU.Model.dtutimealarm model, out string mssg)
        {
            return taskBLL.Update(model, out mssg);
        }
        public bool ProcessDTUTimeAlarmDelete(ProcessDTUTimeAlarmDeleteModel model, out string mssg)
        {
            return taskBLL.Delete(model.module, model.xmno, out mssg);
        }
        public class ProcessDTUTimeAlarmDeleteModel
        {
            public int xmno{get;set;}
            public string module{get;set;}
            public ProcessDTUTimeAlarmDeleteModel(int xmno,string module)
            {
                this.xmno = xmno;
                this.module = module;
            }
        }


        public bool ProcessDTUTimeAlarmLoadBLL(ProcessDTUSenorLoadBLLModel model, out string mssg)
        {

            DataTable dt = new DataTable();
            if (taskBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessDTUSenorLoadBLLModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessDTUSenorLoadBLLModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessDTUTimeAlarmTableCount(ProcessDTUSenorTableCountModel model, out string mssg)
        {
            int totalCont = 0;
            if (taskBLL.TableRowsCount(model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessDTUSenorTableCountModel : SearchCondition
        {
            public int totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessDTUSenorTableCountModel(int xmno, string searchString)
            {

                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
    }
}