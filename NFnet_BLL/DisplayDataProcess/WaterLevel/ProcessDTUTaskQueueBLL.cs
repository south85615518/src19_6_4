﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUTaskQueueBLL
    {
        public DTU.BLL.dtutaskqueue queueBLL = new DTU.BLL.dtutaskqueue();
        public bool ProcessDTUTaskQueueLoad(ProcessDTUTaskQueueLoadModel model,out string mssg)
        {
            mssg = "";
            List<string> commendList = new List<string>();
            if (queueBLL.GetDTUQueue(model.xmno, model.point_name, out commendList, out mssg))
            {
                model.commendList = commendList;
                return true;
            }
            return false;
        }
        public class ProcessDTUTaskQueueLoadModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public List<string> commendList { get; set; }
            public ProcessDTUTaskQueueLoadModel(int xmno, string point_name)
            {
                this.xmno = xmno;
                this.point_name = point_name;
            }
        }
        public bool ProcessDTUTaskQueueFinishedSet(ProcessDTUTaskQueueFinishedSetModel model,out string mssg)
        {
            return queueBLL.DTUQueueTaskSetActived(model.xmno,model.pointname,model.commendIndex,out mssg);
        }
        public class ProcessDTUTaskQueueFinishedSetModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public int commendIndex { get; set; }
            public ProcessDTUTaskQueueFinishedSetModel(int xmno, string pointname,int commendIndex)
            {
                this.xmno = xmno;
                this.pointname = pointname;
                this.commendIndex = commendIndex;
            }
        }
    }
}