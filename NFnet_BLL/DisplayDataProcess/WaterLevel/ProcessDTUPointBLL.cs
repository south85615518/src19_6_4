﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointBLL
    {
        public DTU.BLL.dtu dtuBLL = new DTU.BLL.dtu();
        public bool ProcessDTUPointAddBLL(DTU.Model.dtu model,out string mssg)
        {
            return dtuBLL.Add(model,out mssg);
        }
        public bool ProcessDTUPointUpdateBLL(DTU.Model.dtu model, out string mssg)
        {
            return dtuBLL.Update(model, out mssg);
        }
        public bool ProcessDTUPointMultiUpdateBLL(string pointname, DTU.Model.dtu model,out string mssg)
        {
            return dtuBLL.MultiUpdate(pointname,model,out  mssg);
        }
        public bool ProcessDTUPointNameLoad(ProcessDTUPointNameLoadModel model,out string mssg)
        {
            List<string> ls = new List<string>();
            if (dtuBLL.DTUPointLoadDAL(model.xmno, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;

        }
        public class ProcessDTUPointNameLoadModel
        {
            public int xmno { get; set; }
            public List<string> ls { get; set; }
            public ProcessDTUPointNameLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool ProcessDTUPointModule(ProcessDTUPointModuleWhithoutOdModel model, out string mssg)
        {
            DTU.Model.dtu dtumodel = new DTU.Model.dtu();
            if (dtuBLL.DTUTOPointModule(model.xmno, model.port,  model.addressno, out dtumodel, out mssg))
            {
                model.model = dtumodel;
                return true;
            }
            return false;
        }
        public class ProcessDTUPointModuleWhithoutOdModel
        {
            public int xmno { get; set; }
            public int port { get; set; }
            public string addressno { get; set; }
            public DTU.Model.dtu model = new DTU.Model.dtu();
            public ProcessDTUPointModuleWhithoutOdModel(int xmno, int port,  string addressno)
            {
                this.xmno = xmno;
                this.port = port;
                this.addressno = addressno;
            }
        }
        public bool ProcessDTUPointModule(ProcessDTUPointModuleModel model,out string mssg)
        {
            DTU.Model.dtu dtumodel = new DTU.Model.dtu();
            if (dtuBLL.DTUTOPointModule(model.xmno, model.port,model.od ,model.addressno,out dtumodel,out mssg))
            {
                model.model = dtumodel;
                return true;
            }
            return false;
        }
        public class ProcessDTUPointModuleModel
        {
            public int xmno { get; set; }
            public int port { get; set; }
            public string od { get; set; }
            public string addressno { get; set; }
            public DTU.Model.dtu model = new DTU.Model.dtu();
            public ProcessDTUPointModuleModel(int xmno,int port,string od,string addressno)
            {
                this.xmno = xmno;
                this.port = port;
                this.od = od;
                this.addressno = addressno;
            }
        }


        public bool ProcessNGNPointModule(ProcessNGNPointModuleModel model, out string mssg)
        {
            DTU.Model.dtu dtumodel = new DTU.Model.dtu();
            if (dtuBLL.NGNTOPointModule(model.xmno, model.port,  model.addressno, out dtumodel, out mssg))
            {
                model.model = dtumodel;
                return true;
            }
            return false;
        }
        public class ProcessNGNPointModuleModel
        {
            public int xmno { get; set; }
            public int port { get; set; }
            public string addressno { get; set; }
            public DTU.Model.dtu model = new DTU.Model.dtu();
            public ProcessNGNPointModuleModel(int xmno, int port,  string addressno)
            {
                this.xmno = xmno;
                this.port = port;
                this.addressno = addressno;
            }
        }

    }
}