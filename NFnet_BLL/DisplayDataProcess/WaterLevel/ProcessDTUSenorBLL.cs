﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DTU.BLL;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUSenorBLL
    {
        public dtusenor senorBLL = new dtusenor();
        public bool ProcessDTUSenorAddBLL(DTU.Model.dtusenor model,out string mssg)
        {
            return senorBLL.Add(model,out mssg);
        }
        public bool ProcessDTUSenorNoGet(ProcessDTUSenorNoGetModel model, out string mssg)
        {
            string alarmNameStr = "";
            if (senorBLL.SenorNoGet(model.xmno, out alarmNameStr, out mssg))
            {
                model.senornoStr = alarmNameStr;
                return true;
            }
            return false;

        }
        public class ProcessDTUSenorNoGetModel
        {
            public int xmno { get; set; }
            public string senornoStr { get; set; }
            public ProcessDTUSenorNoGetModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool ProcessDTUSenorUpdateBLL(DTU.Model.dtusenor model, out string mssg)
        {
            return senorBLL.Update(model, out mssg);
        }
        public bool ProcessDTUSenorLoadBLL(ProcessDTUSenorLoadBLLModel model,out string mssg)
        {

            DataTable dt = new DataTable();
            if (senorBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessDTUSenorLoadBLLModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessDTUSenorLoadBLLModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessDTUSenorTableCount(ProcessDTUSenorTableCountModel model,out string mssg)
        {
            int totalCont = 0;
            if (senorBLL.TableRowsCount(model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }
       
        public class ProcessDTUSenorTableCountModel : SearchCondition
        {
            public int totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessDTUSenorTableCountModel(int xmno, string searchString)
            {
                
                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        public bool ProcessDTUSenorDelBLL(ProcessDTUSenorDelBLLModel model,out string mssg)
        {
            return senorBLL.Delete(model.xmno,model.senorno,out mssg);
        }
        public class ProcessDTUSenorDelBLLModel
        {
            public int xmno { get; set; }
            public string senorno { get; set; }
            public ProcessDTUSenorDelBLLModel(int xmno, string senorno)
            {
                this.xmno = xmno;
                this.senorno = senorno;
            }
        }

    }
}