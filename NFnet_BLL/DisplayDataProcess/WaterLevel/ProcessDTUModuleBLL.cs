﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DTU.BLL;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUModuleBLL
    {
        public dtumodule moduleBLL = new dtumodule();
        public bool ProcessDTUModuleAddBLL(DTU.Model.dtumodule model, out string mssg)
        {

            if (!moduleBLL.ExistSenorNo(model.xmno,model.id,model.senorno, out mssg))
                moduleBLL.Add(model, out mssg);
            return false;
        }
        public bool ProcessDTUModuleNoGet(ProcessDTUModuleNoGetModel model, out string mssg)
        {
            string alarmNameStr = "";
            if (moduleBLL.ModuleNoGet(model.xmno, out alarmNameStr, out mssg))
            {
                model.modulenoStr = alarmNameStr;
                return true;
            }
            return false;

        }
        public class ProcessDTUModuleNoGetModel
        {
            public int xmno { get; set; }
            public string modulenoStr { get; set; }
            public ProcessDTUModuleNoGetModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool ProcessDTUModuleUpdateBLL(DTU.Model.dtumodule model, out string mssg)
        {
            if (!moduleBLL.ExistSenorNo(model.xmno,model.id ,model.senorno, out mssg))
                return moduleBLL.Update(model, out mssg);
            return false;
        }
        public bool ProcessDTUModuleLoadBLL(ProcessDTUModuleLoadBLLModel model, out string mssg)
        {

            DataTable dt = new DataTable();
            if (moduleBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessDTUModuleLoadBLLModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessDTUModuleLoadBLLModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessDTUModuleTableCount(ProcessDTUModuleTableCountModel model, out string mssg)
        {
            int totalCont = 0;
            if (moduleBLL.TableRowsCount(model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessDTUModuleTableCountModel : SearchCondition
        {
            public int totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessDTUModuleTableCountModel(int xmno, string searchString)
            {

                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        public bool ProcessDTUModuleDelete(ProcessDTUModuleDeleteModel model, out string mssg)
        {
            return moduleBLL.Delete(model.xmno, model.addressno, model.od, out mssg);
        }
        public class ProcessDTUModuleDeleteModel
        {
            public int xmno { get; set; }
            public string addressno { get; set; }
            public string od { get; set; }
            public ProcessDTUModuleDeleteModel(int xmno, string addressno, string od)
            {
                this.xmno = xmno;
                this.addressno = addressno;
                this.od = od;
            }
        }

    }
}