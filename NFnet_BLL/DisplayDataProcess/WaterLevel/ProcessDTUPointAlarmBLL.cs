﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.XmInfo;
using NFnet_BLL.AuthorityAlarmProcess;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUPointAlarmBLL
    {
        public DTU.BLL.dtu pointalarmBLL = new DTU.BLL.dtu();
        public static ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public static ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public static ProcessalarmsplitondateBLL splitOnDateBLL = new ProcessalarmsplitondateBLL();
        public bool ProcessDTUPointAlarmAdd(DTU.Model.dtu model, out string mssg)
        {
            if (!pointalarmBLL.Exists(model.point_name, model.xmno, out mssg)&&!pointalarmBLL.ExistModuleNo(model.xmno,model.point_name,model.module,out mssg))
                return pointalarmBLL.Add(model, out mssg);
            return false;
        }
        public bool ProcessDTUPointAlarmUpdate(DTU.Model.dtu model, out string mssg)
        {
            if (!pointalarmBLL.ExistModuleNo(model.xmno, model.point_name, model.module, out mssg))
            return pointalarmBLL.Update(model, out mssg);
            return false;
        }
        /// <summary>
        /// 点号预警批量更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmMultiUpdate(ProcessAlarmMultiUpdateModel model, out string mssg)
        {
            return pointalarmBLL.MultiUpdate(model.pointstr, model.model, out mssg);
        }

        /// <summary>
        /// 点号预警批量更新类
        /// </summary>
        public class ProcessAlarmMultiUpdateModel
        {
            /// <summary>
            /// 点组
            /// </summary>
            public string pointstr { get; set; }
            /// <summary>
            /// 点号预警类
            /// </summary>
            public DTU.Model.dtu model { get; set; }
            public ProcessAlarmMultiUpdateModel(string pointstr, DTU.Model.dtu model)
            {
                this.pointstr = pointstr;
                this.model = model;
            }
        }
        public bool ProcessDTUPointAlarmModelGet(ProcessDTUPointAlarmModel model,out string mssg)
        {
            DTU.Model.dtu pointalarmmodel = null;
            if (pointalarmBLL.GetModel(model.xmno, model.pointname, out pointalarmmodel, out mssg))
            {
                model.model = pointalarmmodel;
                return true;
            }
            return false;
        }
        public class ProcessDTUPointAlarmModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public DTU.Model.dtu model { get; set; }
            public ProcessDTUPointAlarmModel(int xmno, string pointname)
            {
                this.xmno = xmno;
                this.pointname = pointname;
            }
        }


        public bool ProcessDTUPointAlarmTableLoad(ProcessDTUPointAlarmTableLoadModel model, out string mssg)
        {
            DataTable dt = new DataTable();
            if (pointalarmBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        

        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessDTUPointAlarmTableLoadModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessDTUPointAlarmTableLoadModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessDTUPointAlarmTableCount(ProcessDTUPointAlarmTableCountModel model, out string mssg)
        {
            int totalCont = 0;
            if (pointalarmBLL.TableRowsCount(model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessDTUPointAlarmTableCountModel : SearchCondition
        {
            public int totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessDTUPointAlarmTableCountModel(int xmno, string searchString)
            {

                this.xmno = xmno;
                this.searchString = searchString;
            }
        }

        public bool ProcessDTUPointAlarmDelete(ProcessDTUPointAlarmDeleteModel model, out string mssg)
        {
            return pointalarmBLL.Delete(model.point_name, model.module,model.xmno, out mssg);
        }
        public class ProcessDTUPointAlarmDeleteModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public string module { get; set; }
            public ProcessDTUPointAlarmDeleteModel(int xmno, string point_name,string module)
            {
                this.xmno = xmno;
                this.module = module;
                this.point_name = point_name;

            }
        }
        public bool ProcessDTUList(ProcessDTUListModel model,out string mssg)
        {
            DataTable dt = new DataTable();
            if (pointalarmBLL.DTUList(model.pageIndex, model.rows, model.xmno, model.xmname,model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessDTUListModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            
            public ProcessDTUListModel(int xmno, string colName, int pageIndex, int rows, string sord,string xmname)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.xmname = xmname;

            }
        }
        public bool ProcessDTUListCount(ProcessDTUListCountModel model, out string mssg)
        {
            int totalCont = 0;
            if (pointalarmBLL.TableRowsCount(model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }
        public class ProcessDTUListCountModel : SearchCondition
        {
            public int totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessDTUListCountModel(int xmno, string searchString)
            {

                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        /// <summary>
        /// 结果数据对象预警处理
        /// </summary>
        /// <param name="alarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmIntoInformation(DTU.Model.dtualarmvalue alarmvalue, DTU.Model.dtudatareport resultModel, out List<string> lscontext)
        {
            lscontext = new List<string>();
            List<string> ls = new List<string>();
            bool result = false;
            lscontext.Add(string.Format("{0},{1}, 本次变化:{2}mm 累计变化:{3}mm 变化速率:{4}mm/d", resultModel.point_name, resultModel.time.ToString(),  resultModel.thisdeep,resultModel.acdeep,resultModel.rap));
            //ls.Add(DataProcessHelper.Compare("", "水位超限值:", resultModel.deep, alarmvalue.deep, out result, result));
            ls.Add(DataProcessHelper.Compare("", "本次超限值:", resultModel.thisdeep, alarmvalue.thisdeep, out result, result));
            ls.Add(DataProcessHelper.Compare("", "累计超限值:", resultModel.acdeep, alarmvalue.acdeep, out result, result));
            ls.Add(DataProcessHelper.Compare("", "变化速率超限值:", resultModel.rap, alarmvalue.rap, out result, result));
            //ls.Add(DataProcessHelper.Compare("", "速率", resultModel.this_rap, alarmvalue.this_rap, out result, result));
            lscontext.Add(string.Join(" ", ls));
            return result;
        }
        public static int sec = 0;
        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            List<string> ls = new List<string>();
            string mssg = "";
            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.point_name,
                time = model.resultModel.time,
                type = "水位",
                atime = DateTime.Now,
                pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processCgAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, "水位", model.resultModel.point_name, model.resultModel.time);
            splitOnDateBLL.ProcessalarmsplitondateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);

            AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = "水位",
                pointName = model.resultModel.point_name,
                xmno = model.xmno,
                time = model.resultModel.time,
                adate = DateTime.Now

            };
            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add("三级预警");
                    pointCheck.alarm = 3;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "水位", "", model.resultModel.point_name, 3, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add("二级预警");
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "水位", "", model.resultModel.point_name, 2, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add("一级预警");
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "水位", "", model.resultModel.point_name, 1, out mssg);
                    return true;
                }


            }
            ls.Add("========预警解除==========");
            pointCheck.alarm = 0;
            pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
            ProcessHotPotColorUpdate(model.xmno, "水位", "", model.resultModel.point_name, 0, out mssg);
            return false;

        }
        public class ProcessPointAlarmfilterInformationModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public List<DTU.Model.dtualarmvalue> levelalarmvalue { get; set; }
            public DTU.Model.dtudatareport resultModel { get; set; }
            public List<string> ls { get; set; }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<DTU.Model.dtualarmvalue> levelalarmvalue, DTU.Model.dtudatareport resultModel, int xmno)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
                this.xmno = xmno;
            }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<DTU.Model.dtualarmvalue> levelalarmvalue, DTU.Model.dtudatareport resultModel)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
            }
            public ProcessPointAlarmfilterInformationModel()
            {

            }
        }
        public class AlarmRecord
        {
            public string pointName { get; set; }
            public string Time { get; set; }
            public string alarm { get; set; }
            public string RecordTime { get; set; }

        }

      
        public class ProcessPointAlarmModelGetModel
        {
            public int xmno { get; set; }
            public string pointName { get; set; }
            public PointAttribute.Model.fmos_pointalarmvalue model { get; set; }
            public ProcessPointAlarmModelGetModel(int xmno, string pointName)
            {
                this.xmno = xmno;
                this.pointName = pointName;
            }
        }

        public bool ProcessHotPotColorUpdate(int xmno, string jclx, string jcoption, string pointName, int color, out string mssg)
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(xmno, jclx, jcoption, pointName, color);
            return layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);

        }

    }
}