﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUAlarmValueBLL
    {
        public DTU.BLL.dtualarmvalue alarmBLL = new DTU.BLL.dtualarmvalue();
        public bool ProcessDTUAlarmValueAdd(DTU.Model.dtualarmvalue model,out string mssg )
        {
            return alarmBLL.Add(model,out mssg);
        }
        public bool ProcessDTUAlarmValueUpdate(DTU.Model.dtualarmvalue model, out string mssg)
        {
            return alarmBLL.Update(model, out mssg);
        }
        public bool ProcessDTUAlarmValueNameGet(ProcessDTUAlarmValueNameGetModel model,out string mssg)
        {
            string alarmNameStr = "";
            if (alarmBLL.AlarmValueNameGet(model.xmno, out alarmNameStr, out mssg))
            {
                model.alarmNameStr = alarmNameStr;
                return true;
            }
            return false;

        }
        public class ProcessDTUAlarmValueNameGetModel
        {
            public int xmno { get; set; }
            public string alarmNameStr { get; set; }
            public ProcessDTUAlarmValueNameGetModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool ProcessDTUAlarmValueModelGet(ProcessDTUAlarmValueModelGetModel model,out string mssg)
        {
            DTU.Model.dtualarmvalue dtualarmvaluemodel = null;
            if (alarmBLL.GetModel(model.name, model.xmno,out dtualarmvaluemodel,out mssg))
            {
                model.model = dtualarmvaluemodel;
                return true;
            }
            return false;
        }
        public class ProcessDTUAlarmValueModelGetModel
        {
            public int xmno { get; set; }
            public string name { get; set; }
            public DTU.Model.dtualarmvalue model { get; set; }
            public ProcessDTUAlarmValueModelGetModel(int xmno,string name)
            {
                this.xmno = xmno;
                this.name = name;
            }
        }

        public bool ProcessDTUAlarmValueTableLoad(ProcessDTUAlarmValueTableLoadModel model,out string mssg)
        {
            DataTable dt = new DataTable();
            if (alarmBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessDTUAlarmValueTableLoadModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessDTUAlarmValueTableLoadModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessDTUAlarmValueTableCount(ProcessDTUAlarmValueTableCountModel model, out string mssg)
        {
            int totalCont = 0;
            if (alarmBLL.TableRowsCount(model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessDTUAlarmValueTableCountModel : SearchCondition
        {
            public int totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessDTUAlarmValueTableCountModel(int xmno, string searchString)
            {

                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        public bool ProcessDTUAlarmValueDelete(ProcessDTUAlarmValueDeleteModel model, out string mssg)
        {
            return alarmBLL.Delete(model.alarmname,model.xmno, out mssg);
        }
        public class ProcessDTUAlarmValueDeleteModel
        {
            public int xmno { get; set; }
            public string alarmname { get; set; }
            public ProcessDTUAlarmValueDeleteModel(int xmno, string alarmname)
            {
                this.xmno = xmno;
                this.alarmname = alarmname;
                
            }
        }

    }
}