﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessInfoImportBLL
    {
        //获取最后一次设置的端口号
        public static List<tcpclientIPInfo> MachineSettingInfoLoad(string path)
        {
            List<string> MachineSetting = new List<string>();
            List<tcpclientIPInfo> ltif = new List<tcpclientIPInfo>();
            StreamReader sr = null;
            int i = 0;
            try
            {
                sr = File.OpenText(path);

                string txt = "";
                tcpclientIPInfo tif = new tcpclientIPInfo();
                while ((txt = sr.ReadLine()) != null)
                {
                    if (i % 2 == 0)
                    {
                        tif.ip = txt;
                    }
                    else
                    {
                        tif.port = Convert.ToInt32(txt);
                        ltif.Add(tif);
                        tif = new tcpclientIPInfo();
                    }
                    i++;

                }
                return ltif;
                //this.textBox1.Text = MachineSetting[0];
                //this.textBox2.Text = MachineSetting[1];



            }
            catch (Exception ex)
            {
                
                ExceptionLog.ExceptionWrite("读取机器配置文件出错,错误信息："+ex.Message);
                throw (ex);
                //return null;
            }
            finally
            {

                if (sr != null)
                    sr.Close();

            }
        }
    }
}
