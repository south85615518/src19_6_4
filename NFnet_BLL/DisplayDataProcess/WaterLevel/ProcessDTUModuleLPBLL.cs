﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public  partial class ProcessDTUModuleLPBLL
    {
        public DTU.BLL.dtulp dtulpBLL = new DTU.BLL.dtulp();
        public bool ProcessDTUModuleLPAdd(DTU.Model.dtulp model, out string mssg)
        {
            return dtulpBLL.Add(model, out mssg);
        }
       
        public bool ProcessDTUModuleLPUpdate(DTU.Model.dtulp model, out string mssg)
        {
            if(dtulpBLL.Exists(model.xmno,model.module,out mssg))
            return dtulpBLL.Update(model, out mssg);
            else
            return dtulpBLL.Add(model, out mssg);
        }

        public bool ProcessDTUModuleLPLoadBLL(ProcessDTUModuleLPLoadBLLModel model, out string mssg)
        {

            DataTable dt = new DataTable();
            if (dtulpBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public bool ProcessDTUXmPortModuleLPLoadBLL(ProcessDTUModuleLPLoadBLLModel model, out string mssg)
        {

            DataTable dt = new DataTable();
            if (dtulpBLL.XmPortTableLoad(model.pageIndex, model.rows, model.xmno,model.xmname ,model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessDTUModuleLPLoadBLLModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessDTUModuleLPLoadBLLModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
            public ProcessDTUModuleLPLoadBLLModel(int xmno,string xmname ,string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.xmname = xmname;

            }
        }
       

        public bool ProcessDTUModuleLPTableCount(ProcessDTUModuleLPTableCountModel model, out string mssg)
        {
            int totalCont = 0;
            if (dtulpBLL.TableRowsCount(model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessDTUModuleLPTableCountModel : SearchCondition
        {
            public int totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessDTUModuleLPTableCountModel(int xmno, string searchString)
            {

                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        public bool ProcessDTUModuleLPModelBLL(ProcessDTUModuleLPModelBLLModel model, out string mssg)
        {
            DTU.Model.dtulp dtulpmodel = new DTU.Model.dtulp();
            if (dtulpBLL.GetModel(model.xmno, model.module, out dtulpmodel, out mssg))
            {
                model.model = dtulpmodel;
                return true;
            }
            return false;
        }
        public class ProcessDTUModuleLPModelBLLModel
        {
            public int xmno { get; set; }
            public string module { get; set; }
            public DTU.Model.dtulp model { get; set; }
            public ProcessDTUModuleLPModelBLLModel(int xmno, string module)
            {
                this.xmno = xmno;
                this.module = module;
            }
        }

    }
}