﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessDTUDATAJsonBLL
    {
        
        public bool ProcessDTUDATAJsonHead(Authority.Model.xmconnect xmconnect, List<string> jsondata,out List<string> json,out string path)
        {
            json = null;
            path = "";
            if (jsondata.Count == 0) return false; 
            json = new List<string>();
            string jsonhead = string.Format("项目名称|{0}|工点名称|{1}|监测类型|施工监测|数据条数|{2}",xmconnect.xmname,xmconnect.xmaddress,jsondata.Count);
            json.Add(jsonhead);
            json.Add("序号|测点号|监测时间|当前初始值|本次测值");
            json.AddRange(jsondata);
            path = string.Format("{0}_{1}_{2}_{3}", xmconnect.xmname, xmconnect.xmaddress, jsondata.Count,Tool.DateHelper.DateTimeToStringCHN(DateTime.Now));
            return true;
            
        }
    }
}