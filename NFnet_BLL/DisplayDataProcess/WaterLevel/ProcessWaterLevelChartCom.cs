﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.Settlement;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessWaterLevelChartCom
    {
        public ProcessWaterLevelChartBLL dtuSenorChartBLL = new ProcessWaterLevelChartBLL();
        public ProcessWaterLevelChartBLL dtuSenorChartCgBLL = new ProcessWaterLevelChartBLL();
        public bool ProcessSerializestrSW(ProcessSerializestrSWModel model,out string mssg)
        {
            if (model.role == Role.superviseModel || model.tmpRole)
            {
                return ProcessWaterLevelChartBLL.ProcessSerializestrSW(model.model, out mssg);
            }
            return ProcessWaterLevelChartBLL.ProcessSerializestrSW(model.model, out mssg);
        }
        public class ProcessSerializestrSWModel
        {
            public SerializestrSWCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessSerializestrSWModel(SerializestrSWCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }
    }
}