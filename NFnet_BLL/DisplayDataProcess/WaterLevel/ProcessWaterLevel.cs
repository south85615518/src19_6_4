﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    /// <summary>
    /// 水位数据业务逻辑处理类
    /// </summary>
    public class ProcessWaterLevel
    {
        public static sensorDAL.BLL.SwBLL ylBLL = new sensorDAL.BLL.SwBLL();
        /// <summary>
        /// 水位点名获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSwPointLoad(ProcessSwPointLoadModel model, out string mssg)
        {
            List<string> ls = null;
            if (ylBLL.SwPointLoadBLL(model.xmname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 水位点名获取类
        /// </summary>
        public class ProcessSwPointLoadModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 点名列表
            /// </summary>
            public List<string> ls { get; set; }
            public ProcessSwPointLoadModel(string xmname, List<string> ls)
            {
                this.xmname = xmname;
                this.ls = ls;
            }
        }
    }
}