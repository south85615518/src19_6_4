﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DataProcess
{
    public abstract class SearchCondition
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 项目名
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 点名
        /// </summary>
        public string pointname { get; set; }
        /// <summary>
        /// 排序列名
        /// </summary>
        public string colName { get; set; }

        /// <summary>
        /// 起始页号
        /// </summary>
        public int pageIndex { get; set; }
        /// <summary>
        /// 每页行数
        /// </summary>
        public int rows { get; set; }

        /// <summary>
        /// 排序列名
        /// </summary>
        public string sord { get; set; }
       
        
        

    }
}