﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class DataDeleteCondition
    {
        public string xmname { get; set; }
        public string point_name { get; set; }
        public DateTime dt { get; set; }
        public DataDeleteCondition(string xmname,string point_name,DateTime dt)
        {
            this.xmname = xmname;
            this.point_name = point_name;
            this.dt = dt;
        }
        
    }
}