﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SerializestrSBWY_POINTCondition : SerializestrBMWYCondition
    {
        public List<serie_point> serie_points { get; set; }
        public SerializestrSBWY_POINTCondition(object sql, object xmno, object pointname)
        {
            this.sql = sql == null ? "" : sql.ToString();
            this.xmno = xmno == null ? 0 : Convert.ToInt32(xmno);
            this.pointname = pointname == null ? "" : pointname.ToString();
        }
    }
}