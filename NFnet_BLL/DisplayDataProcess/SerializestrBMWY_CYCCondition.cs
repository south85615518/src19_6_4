﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SerializestrBMWYC_CYCondition : SerializestrBMWYCondition
    {
        /// <summary>
        /// 端点周期
        /// </summary>
        public List<string> terminalCyc { get; set; }
        /// <summary>
        /// 周期曲线
        /// </summary>
        public List<serie_cyc_null> serie_cycs { get; set; }
        public SerializestrBMWYC_CYCondition(object sql, object xmname, object pointname, object zus, object terminalCyc)
        {
            this.sql = sql == null ? "" : sql.ToString();
            this.xmname = xmname == null ? "" : xmname.ToString();
            this.pointname = pointname == null ? "" : pointname.ToString();
            this.zus = zus == null ? null : (zuxyz[])zus;
            this.terminalCyc = terminalCyc == null ? null : (List<string>)terminalCyc;
        }
    }
}