﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.ngn
{
    public class ProcessNGNDataBLL
    {
        public DTU.BLL.dtudata bll = new DTU.BLL.dtudata();
        public bool ProcessNGNDataDegreeUpdate(DTU.Model.dtudata model,out string mssg)
        {
            return bll.UpdateDegree(model,out mssg);
        }
        
    }
}