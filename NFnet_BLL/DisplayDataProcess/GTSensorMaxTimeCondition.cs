﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GTSensorMaxTimeCondition
    {
        public string xmname { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public data.Model.gtsensortype datatype { get; set; }
        public GTSensorMaxTimeCondition(string xmname, string pointname, data.Model.gtsensortype datatype)
        {
            this.xmname = xmname;
            this.pointname = pointname;
            this.datatype = datatype;
        }
        public GTSensorMaxTimeCondition(string xmname, data.Model.gtsensortype datatype)
        {
            this.xmname = xmname;
            this.datatype = datatype;
        }
        public GTSensorMaxTimeCondition()
        {
            
        }
    }
}