﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SensorPointLoadCondition
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public int xmno { get; set; }
        public data.Model.gtsensortype datatype { get; set; }
        /// <summary>
        /// 学习点名列表
        /// </summary>
        public List<string> ls { get; set; }
        public SensorPointLoadCondition(int xmno, data.Model.gtsensortype datatype)
        {
            this.xmno = xmno;
            this.datatype = datatype;
        }
        public SensorPointLoadCondition(int xmno)
        {
            this.xmno = xmno;
       
        }
    }
}