﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    public class PrintTableCondition
    {
        public string pointname { get; set; }
        public DataTable dt = new DataTable();
        public PrintTableCondition(string pointname, DataTable dt)
        {
            this.pointname = pointname;
            this.dt = dt;
        }
    }
}