﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GTSensorMinTimeCondition
    {
        public string xmname { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public data.Model.gtsensortype datatype { get; set; }
        public GTSensorMinTimeCondition(string xmname, data.Model.gtsensortype datatype)
        {
            this.xmname = xmname;
            this.datatype = datatype;
        }
        public GTSensorMinTimeCondition(string xmname,string point_name ,data.Model.gtsensortype datatype)
        {
            this.xmname = xmname;
            this.pointname = point_name;
            this.datatype = datatype;
        }
        public GTSensorMinTimeCondition()
        {
            
        }
    }
}