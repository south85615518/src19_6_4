﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using Tool;
using NFnet_BLL.ProjectInfo;
using NFnet_BLL.GTSensorServer;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    public partial class ProcessResultDataBLL
    {
       
        public static ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public static ProcessSurfacePointAlarmBLL processSurfacePointAlarmBLL = new ProcessSurfacePointAlarmBLL();
        public static ProcessSurfaceDataBLL processSurfaceDataBLL = new ProcessSurfaceDataBLL();
        public static data.BLL.gtsensordata bll = new data.BLL.gtsensordata();
        public ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public string mssg = "";

        public bool ProcessResultDataExists(data.Model.gtsensordata model,out string mssg)
        {
            return bll.Exists(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.project_name), model.senorno, model.datatype, model.time, out mssg);
        }
       
        public bool ProcessResultDataAdd(global::data.Model.gtsensordata model, out string mssg)
        {
            mssg = "";
            if(model.datatype == data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._enclosureWallReinforcementStress)&& model.sec_oregion_scalarvalue == -999999 )
                return false;

            if ( ((model.datatype != data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce)) && (model.datatype != data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._SteelSupport)) && (model.datatype != data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._StructuralStress)) && model.first_oregion_scalarvalue == -999999 ))
                return false;
            return bll.Add(model, out mssg);
        }
        public bool ProcessGetInitTimeModel(ProcessGetInitTimeModelGetModel model, out string mssg)
        {
            var gtsensordatamodel = new data.Model.gtsensordata();
            if (bll.GetInitTimeModel(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.datatype,  out gtsensordatamodel, out mssg))
            {
                model.model = gtsensordatamodel;
                return true;
            }
            return false;
        }
        public class ProcessGetInitTimeModelGetModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public data.Model.gtsensordata model { get; set; }
            public ProcessGetInitTimeModelGetModel(string xmname, string pointname, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.datatype = datatype;
            }
        }

        public bool ProcessGetLastTimeModel(ProcessGetLastTimeModelGetModel model, out string mssg)
        {
            var gtsensordatamodel = new data.Model.gtsensordata();
            if (bll.GetLastTimeModel(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.datatype, model.dt, out gtsensordatamodel, out mssg))
            {
                model.model = gtsensordatamodel;
                return true;
            }
            return false;
        }

        public class ProcessGetLastTimeModelGetModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public DateTime dt { get; set; }
            public data.Model.gtsensordata model { get; set; }
            public ProcessGetLastTimeModelGetModel(string xmname, string pointname, data.Model.gtsensortype datatype, DateTime dt)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.datatype = datatype;
                this.dt = dt;
            }
        }
        string existmssg = "";
        public bool ProcessResultDataCalculate(global::data.Model.gtsensordata model,out string mssg)
        {
            try
            {
                model.datatype = data.DAL.gtsensortype.GTSensorTypeToString(data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
                Console.WriteLine("获取到类型为:" + model.datatype+"  "+model.time);
                var processXmInfoLoadModel = new ProcessXmBLL.ProcessXmIntInfoLoadModel(Convert.ToInt32(model.project_name));
                processXmBLL.ProcessXmInfoLoad(processXmInfoLoadModel, out mssg);
                data.Model.gtsensortype datatype = data.Model.gtsensortype._other;
                //获取点名
                var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(processXmInfoLoadModel.model.xmno, model.senorno);
                int i = 0;
                for (i = 0; i < 3; i++)
                {
                    if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
                    {
                        //model.point_name = "";
                        Console.WriteLine(mssg);
                        System.Threading.Thread.Sleep(1000);
                        if (i == 2)
                            return false;
                        continue;

                    }
                    else
                        break;
                }
                model.point_name = processPointAlarmModelGetModel.model.pointname;
                datatype = processPointAlarmModelGetModel.model.datatype;
                //if (ProcessResultDataExists(model, out existmssg)) return false;
                processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(processXmInfoLoadModel.model.xmno, model.point_name, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
                switch (data.DAL.gtsensortype.GTStringToSensorType(model.datatype))
                {
                    case data.Model.gtsensortype._surfaceSurpport:
                        SurfaceDataCalculate(model, datatype, processXmInfoLoadModel.model.xmno);
                        break;
                    case data.Model.gtsensortype._soilPressure:
                    case data.Model.gtsensortype._fractureMeter:
                        FractureMeterCalculate(model, processXmInfoLoadModel.model.xmno);
                        break;
                    case data.Model.gtsensortype._staticLevel:

                    case data.Model.gtsensortype._enclosureWallReinforcementStress:
                    case data.Model.gtsensortype._SteelSupport:

                        var processGetLastTimeModelGetModel = new ProcessGetLastTimeModelGetModel(model.project_name, model.point_name, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), model.time);
                        if (!ProcessGetLastTimeModel(processGetLastTimeModelGetModel, out mssg)) processGetLastTimeModelGetModel.model = model;
                        model.single_ac_scalarvalue = model.first_oregion_scalarvalue;
                        model.single_this_scalarvalue = model.single_ac_scalarvalue - processGetLastTimeModelGetModel.model.single_ac_scalarvalue;
                        return true;
                        break;
                    case data.Model.gtsensortype._waterlevel:
                        WaterLevelCalculate(model, processXmInfoLoadModel.model.xmno);
                        return true;

                    case data.Model.gtsensortype._layeredSettlement:

                        processPointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg);
                        processGetLastTimeModelGetModel = new ProcessGetLastTimeModelGetModel(model.project_name, model.point_name, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), model.time);
                        if (!ProcessGetLastTimeModel(processGetLastTimeModelGetModel, out mssg)) processGetLastTimeModelGetModel.model = model;
                        model.single_ac_scalarvalue = processPointAlarmModelGetModel.model.orificeHeight - model.first_oregion_scalarvalue;
                        model.single_this_scalarvalue = model.single_ac_scalarvalue - processGetLastTimeModelGetModel.model.single_ac_scalarvalue;
                        return true;
                    default: return true;

                }
                return true;
            }
            catch (Exception ex)
            {
                mssg = "";
                return false;
            }
           
        }
        public void SurfaceDataCalculate(data.Model.gtsensordata model,data.Model.gtsensortype datatype, int xmno)
        {
            if (datatype == data.Model.gtsensortype._supportAxialForce)
            {
                model.datatype = data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._supportAxialForce);
                SupportAxialForceCalculate(model, xmno);
            }
            else if (datatype == data.Model.gtsensortype._SteelSupport)
            {
                model.datatype = data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._SteelSupport);
                SteelSupportCalculate(model, xmno);
            }
            else if (datatype == data.Model.gtsensortype._StructuralStress)
            {
                model.datatype = data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._StructuralStress);
                StructuralStressCalculate(model, xmno);
            }
        }


        #region 混凝土支撑轴力
        public void SupportAxialForceCalculate(data.Model.gtsensordata model,int xmno)
        {

            IsCycdirnetDataExistModel tsgtsensordataDataExistModel = new IsCycdirnetDataExistModel(model.project_name, model.point_name, model.time, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
            //if(checkexist)
            //{
                if (IsCycdirnetDataExist(tsgtsensordataDataExistModel, out mssg)) { Console.WriteLine(mssg + "不加入结算点"); /*return;*/ }
            //}
            //获取点
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, model.senorno);
            int i = 0;
            for (i = 0; i < 3; i++)
            {
                if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    Console.WriteLine("暂停一秒...");
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                else
                {
                    Console.WriteLine(mssg);
                    break;
                }

            }
            Console.WriteLine(mssg);
            if (!processPointAlarmModelGetModel.model.isInCalculate) return;
            //获取面
            var ProcessSurfaceAlarmModelGetModel = new ProcessSurfacePointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, processPointAlarmModelGetModel.model.surfaceName, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

           
             for (i = 0; i < 3; i++)
             {

                 if (!processSurfacePointAlarmBLL.ProcessPointAlarmModelGet(ProcessSurfaceAlarmModelGetModel, out mssg))
                 {
                     Console.WriteLine(mssg);
                     Console.WriteLine("暂停一秒...");
                     System.Threading.Thread.Sleep(1000);
                     continue;
                 }
                 else
                 {
                     Console.WriteLine(mssg);
                     break;
                 }
             }
            Console.WriteLine(mssg);
            var processSameTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno,data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            string samemssg = "";
            if (!processSurfaceDataBLL.SurfaceDataSameTimeModel(processSameTimeModelGetModel, out samemssg))
            {
                Console.WriteLine(samemssg);
                processSameTimeModelGetModel.model = new data.Model.gtsurfacedata { xmno = xmno, calculatepointsCont = 0, time = model.time, surfacename = processPointAlarmModelGetModel.model.surfaceName };
            }
            Console.WriteLine(samemssg);
            double modules = (ProcessSurfaceAlarmModelGetModel.model.concreteElasticityModulus * ProcessSurfaceAlarmModelGetModel.model.concreteConversionArea + ProcessSurfaceAlarmModelGetModel.model.steelElasticityModulus * ProcessSurfaceAlarmModelGetModel.model.steelConversionArea) / 1000000;

            //Console.WriteLine(string.Format("混凝土{0}应变系数 {1} 混凝土计{2}初始温度为", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));


            //获取初始温度
            var processGetInitTimeModelGetModel = new ProcessGetInitTimeModelGetModel(model.project_name, processPointAlarmModelGetModel.model.pointname, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
            if (!ProcessGetInitTimeModel(processGetInitTimeModelGetModel, out mssg))
            {
                Console.WriteLine(mssg);
                processGetInitTimeModelGetModel.model = model;
            }
            Console.WriteLine(mssg);
            //设置初始温度
            if (string.IsNullOrEmpty(processPointAlarmModelGetModel.model.remark))
            {

                processPointAlarmModelGetModel.model.remark = processGetInitTimeModelGetModel.model.sec_oregion_scalarvalue.ToString();
                processPointAlarmBLL.ProcessUpdateInitDegree(processPointAlarmModelGetModel.model, out mssg);
                Console.WriteLine(mssg);
            }
            Console.WriteLine(mssg);

            Console.WriteLine(string.Format("混凝土{0}应变系数 {1} 混凝土计{2}初始温度为{3}", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));
            double degreemodules = model.sec_oregion_scalarvalue == -999999 ? 0 : 1;
            //计算应力
            double s = model.first_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.ac_val : (
                (processSameTimeModelGetModel.model.ac_val / modules) * processSameTimeModelGetModel.model.calculatepointsCont
                + model.first_oregion_scalarvalue +degreemodules * 1.8 * (model.sec_oregion_scalarvalue - Convert.ToDouble(processPointAlarmModelGetModel.model.remark))) * modules / (processSameTimeModelGetModel.model.calculatepointsCont + 1);
            //Console.WriteLine(string.Format("第{0}次计算:" + processSameTimeModelGetModel.model.calculatepointsCont + 1));
            //Console.WriteLine(string.Format("([混凝土当前的累积变化量]" + processSameTimeModelGetModel.model.ac_val + "/" + modules + "[应变系数])*" + processSameTimeModelGetModel.model.calculatepointsCont + "[参与计算的混凝土计个数]+" + model.first_oregion_scalarvalue + "[正在计算的混凝土计的数据2]+" + "1.8*(" + model.sec_oregion_scalarvalue + "[正在计算的混凝土计的数据3]-"));
            processSameTimeModelGetModel.model.ac_val = s;


            //计算本次变化量
            var processsurfacedataGetLastTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            if (!processSurfaceDataBLL.SurfaceDataLastTimeModel(processsurfacedataGetLastTimeModelGetModel, out mssg))
            {
                processsurfacedataGetLastTimeModelGetModel.model = processSameTimeModelGetModel.model;
            }
            processSameTimeModelGetModel.model.this_val = processSameTimeModelGetModel.model.ac_val - processsurfacedataGetLastTimeModelGetModel.model.ac_val;
            processSameTimeModelGetModel.model.calculatepointsCont += 1;
            processSameTimeModelGetModel.model.SteelChordVal = model.sec_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.SteelChordVal : (processSameTimeModelGetModel.model.SteelChordVal * (processSameTimeModelGetModel.model.calculatepointsCont - 1) + model.sec_oregion_scalarvalue) / processSameTimeModelGetModel.model.calculatepointsCont;

            if (processSameTimeModelGetModel.model.calculatepointsCont > 4)
            { string a = ""; }
            processSurfaceDataBLL.ProcessResultDataAdd(processSameTimeModelGetModel.model, out mssg);
            Console.WriteLine(mssg);

            if (/*DateTime.Now.Hour == 10  || DateTime.Now.Hour == 18 || */DateTime.Now.Hour == 23)
            {
                SupportAxialForceCalculate(xmno);
                
            }
            //var processgtSurfacedataCYCUpdateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessgtSurfacedataCYCUpdateModel(xmno, processSameTimeModelGetModel.model.surfacename, processSameTimeModelGetModel.model.time);
            //processSurfaceDataBLL.ProcessgtSurfacedataCYCUpdate(processgtSurfacedataCYCUpdateModel, out mssg);
            //Console.WriteLine(mssg);
            return;
        }
        public void SupportAxialForceCalculate(data.Model.gtsensordata model, int xmno, List<data.Model.gtpointalarmvalue> gtpointalarmvaluemodellist)
        {

            IsCycdirnetDataExistModel tsgtsensordataDataExistModel = new IsCycdirnetDataExistModel(model.project_name, model.point_name, model.time, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
            //if (checkexist)
            //{
            //    if (IsCycdirnetDataExist(tsgtsensordataDataExistModel, out mssg)) { Console.WriteLine(mssg + "不加入结算点"); return; }
            //}
            //获取点
            var pointmodellist = (from m in gtpointalarmvaluemodellist where m.sensorno == model.senorno select m).ToList();
            if (pointmodellist.Count == 0) { Console.WriteLine( string.Format("未查询到传感器编号为{0}测点信息",model.senorno)) ;return; }
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, model.senorno);
            //if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            //{
            //    Console.WriteLine(mssg);
            //    return;
            //}
            processPointAlarmModelGetModel.model = pointmodellist[0];
            Console.WriteLine(string.Format("获取到到传感器编号为{0}测点名为{1}支撑面名为{2}初始温度为{3}是否加入计算:{4}", model.senorno, processPointAlarmModelGetModel.model.pointname, processPointAlarmModelGetModel.model.surfaceName, processPointAlarmModelGetModel.model.remark, processPointAlarmModelGetModel.model.isInCalculate));
            if (!processPointAlarmModelGetModel.model.isInCalculate) return;
            //获取面
            var ProcessSurfaceAlarmModelGetModel = new ProcessSurfacePointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, processPointAlarmModelGetModel.model.surfaceName, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

            int i = 0;
            for (i = 0; i < 3;i++ )
            {
                if (!processSurfacePointAlarmBLL.ProcessPointAlarmModelGet(ProcessSurfaceAlarmModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    Console.WriteLine("暂停1秒...");
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                else { Console.WriteLine(mssg);  break; }
            }

            
            Console.WriteLine(mssg);
            var processSameTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno,data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            string samemssg = "";
            if (!processSurfaceDataBLL.SurfaceDataSameTimeModel(processSameTimeModelGetModel, out samemssg))
            {
                Console.WriteLine(samemssg);
                processSameTimeModelGetModel.model = new data.Model.gtsurfacedata { xmno = xmno, calculatepointsCont = 0, time = model.time, surfacename = processPointAlarmModelGetModel.model.surfaceName };
            }
            Console.WriteLine(samemssg);
            double modules = (ProcessSurfaceAlarmModelGetModel.model.concreteElasticityModulus * ProcessSurfaceAlarmModelGetModel.model.concreteConversionArea + ProcessSurfaceAlarmModelGetModel.model.steelElasticityModulus * ProcessSurfaceAlarmModelGetModel.model.steelConversionArea) / 1000000;

            //Console.WriteLine(string.Format("混凝土{0}应变系数 {1} 混凝土计{2}初始温度为", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));


            //获取初始温度
            var processGetInitTimeModelGetModel = new ProcessGetInitTimeModelGetModel(model.project_name, processPointAlarmModelGetModel.model.pointname, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
           
            //设置初始温度
            if (string.IsNullOrEmpty(processPointAlarmModelGetModel.model.remark))
            {
                if (!ProcessGetInitTimeModel(processGetInitTimeModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    processGetInitTimeModelGetModel.model = model;
                }
                Console.WriteLine(mssg);
                processPointAlarmModelGetModel.model.remark = processGetInitTimeModelGetModel.model.sec_oregion_scalarvalue.ToString();
                processPointAlarmBLL.ProcessUpdateInitDegree(processPointAlarmModelGetModel.model, out mssg);
                Console.WriteLine(mssg);
            }
            Console.WriteLine(mssg);

            Console.WriteLine(string.Format("混凝土{0}应变系数 {1} 混凝土计{2}初始温度为{3}", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));

            double degreemodules = model.sec_oregion_scalarvalue == -999999 ? 0 : 1;
            //计算应力

            double s = model.first_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.ac_val : (
                (processSameTimeModelGetModel.model.ac_val / modules) * processSameTimeModelGetModel.model.calculatepointsCont
                + model.first_oregion_scalarvalue + degreemodules*1.8 * (model.sec_oregion_scalarvalue - Convert.ToDouble(processPointAlarmModelGetModel.model.remark))) * modules / (processSameTimeModelGetModel.model.calculatepointsCont + 1);
            //Console.WriteLine(string.Format("第{0}次计算:" + processSameTimeModelGetModel.model.calculatepointsCont + 1));
            //Console.WriteLine(string.Format("([混凝土当前的累积变化量]" + processSameTimeModelGetModel.model.ac_val + "/" + modules + "[应变系数])*" + processSameTimeModelGetModel.model.calculatepointsCont + "[参与计算的混凝土计个数]+" + model.first_oregion_scalarvalue + "[正在计算的混凝土计的数据2]+" + "1.8*(" + model.sec_oregion_scalarvalue + "[正在计算的混凝土计的数据3]-"));
            processSameTimeModelGetModel.model.ac_val = s;


            //计算本次变化量
            var processsurfacedataGetLastTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno,data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            if (!processSurfaceDataBLL.SurfaceDataLastTimeModel(processsurfacedataGetLastTimeModelGetModel, out mssg))
            {
                processsurfacedataGetLastTimeModelGetModel.model = processSameTimeModelGetModel.model;
            }
            processSameTimeModelGetModel.model.this_val = processSameTimeModelGetModel.model.ac_val - processsurfacedataGetLastTimeModelGetModel.model.ac_val;
            processSameTimeModelGetModel.model.calculatepointsCont += 1;

            processSameTimeModelGetModel.model.SteelChordVal = model.sec_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.SteelChordVal : (processSameTimeModelGetModel.model.SteelChordVal * (processSameTimeModelGetModel.model.calculatepointsCont - 1) + model.sec_oregion_scalarvalue) / processSameTimeModelGetModel.model.calculatepointsCont;

            if (processSameTimeModelGetModel.model.calculatepointsCont > 4)
            { string a = ""; }
            processSurfaceDataBLL.ProcessResultDataAdd(processSameTimeModelGetModel.model, out mssg);
            Console.WriteLine(mssg);
            //var processgtSurfacedataCYCUpdateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessgtSurfacedataCYCUpdateModel(xmno, processSameTimeModelGetModel.model.surfacename, processSameTimeModelGetModel.model.time);
            //processSurfaceDataBLL.ProcessgtSurfacedataCYCUpdate(processgtSurfacedataCYCUpdateModel, out mssg);
            //Console.WriteLine(mssg);
            return;
        }

        public void SupportAxialForceCalculate(int xmno)
        {
            string mssg = "";
            ProcessSurfaceDataBLL processSurfaceDataBLL = new ProcessSurfaceDataBLL();
            var processLackPointCalculateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessLackPointCalculateModel(xmno,data.Model.gtsensortype._supportAxialForce);
            if (!processSurfaceDataBLL.ProcessLackPointCalculate(processLackPointCalculateModel, out mssg)) {Console.WriteLine(mssg) ;return; }
            DataView dv = new DataView(processLackPointCalculateModel.dt);
            ProcessSurfacedataSameTimeModelListGetModel processSurfacedataSameTimeModelListGet = null;
            data.Model.gtsurfacedata gtsurfacedata = null;
            var processPointAlarmModelListGetModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessPointAlarmBLL.ProcessPointAlarmModelListGetModel(xmno,data.Model.gtsensortype._supportAxialForce);
            processPointAlarmBLL.ProcessPointAlarmModelListGet(processPointAlarmModelListGetModel,out mssg);
            Console.WriteLine(mssg);
            foreach(DataRowView drv in dv)
            {
                processSurfacedataSameTimeModelListGet = new ProcessSurfacedataSameTimeModelListGetModel(xmno.ToString(),drv["surfacename"].ToString(),Convert.ToDateTime(drv["time"]));
                if (!ProcessSurfacedataSameTimeModelListGet(processSurfacedataSameTimeModelListGet, out mssg)) { Console.WriteLine(mssg); continue; }
                gtsurfacedata = new data.Model.gtsurfacedata{ surfacename = processSurfacedataSameTimeModelListGet.surfacename,  cyc = Convert.ToInt32(drv["cyc"]), xmno = xmno, time = processSurfacedataSameTimeModelListGet.dt   };
                
                if(!processSurfaceDataBLL.ProcessResultDataUpdate(gtsurfacedata,out mssg)){ Console.WriteLine(mssg);  }
                string tmpsensorno = "";//processSurfacedataSameTimeModelListGet.modellist[0].senorno;
                foreach (var model in processSurfacedataSameTimeModelListGet.modellist)
                {
                    if (model.senorno == tmpsensorno) {  continue; }
                    tmpsensorno = model.senorno;
                    SupportAxialForceCalculate(model, xmno, processPointAlarmModelListGetModel.modellist);
                }

            }
            var cycSortModel = new ProcessSurfaceDataBLL.CycSortModel(xmno,data.Model.gtsensortype._supportAxialForce);
            processSurfaceDataBLL.CycSort(cycSortModel, out mssg);
            Console.WriteLine(mssg);
        }

        #endregion
        #region 钢支撑
        public void SteelSupportCalculate(data.Model.gtsensordata model, int xmno)
        {

            IsCycdirnetDataExistModel tsgtsensordataDataExistModel = new IsCycdirnetDataExistModel(model.project_name, model.point_name, model.time, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
            //if (checkexist)
            //{
                if (IsCycdirnetDataExist(tsgtsensordataDataExistModel, out mssg)) { Console.WriteLine(mssg + "不加入结算点"); return; }
            //}
            //获取点
                var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, model.senorno);
                int i = 0;
                for (i = 0; i < 3; i++)
                {
                    if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
                    {
                        Console.WriteLine(mssg);
                        Console.WriteLine("暂停一秒...");
                        System.Threading.Thread.Sleep(1000);
                        continue;
                    }
                    else
                    {
                        Console.WriteLine(mssg);
                        break;
                    }

                }
                Console.WriteLine(mssg);
                if (!processPointAlarmModelGetModel.model.isInCalculate) return;
            
            Console.WriteLine(string.Format("获取到到传感器编号为{0}测点名为{1}支撑面名为{2}初始温度为{3}是否加入计算:{4}", model.senorno, processPointAlarmModelGetModel.model.pointname, processPointAlarmModelGetModel.model.surfaceName, processPointAlarmModelGetModel.model.remark, processPointAlarmModelGetModel.model.isInCalculate));
            if (!processPointAlarmModelGetModel.model.isInCalculate) return;
            //获取面
            var ProcessSurfaceAlarmModelGetModel = new ProcessSurfacePointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, processPointAlarmModelGetModel.model.surfaceName, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

             i = 0;
            for (i = 0; i < 3; i++)
            {
                if (!processSurfacePointAlarmBLL.ProcessPointAlarmModelGet(ProcessSurfaceAlarmModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    Console.WriteLine("暂停1秒...");
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                else { Console.WriteLine(mssg); break; }
            }


            Console.WriteLine(mssg);
            var processSameTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            string samemssg = "";
            if (!processSurfaceDataBLL.SurfaceDataSameTimeModel(processSameTimeModelGetModel, out samemssg))
            {
                Console.WriteLine(samemssg);
                processSameTimeModelGetModel.model = new data.Model.gtsurfacedata { xmno = xmno, calculatepointsCont = 0, time = model.time, surfacename = processPointAlarmModelGetModel.model.surfaceName };
            }
            Console.WriteLine(samemssg);

            double modules = Math.PI * (Math.Pow(ProcessSurfaceAlarmModelGetModel.model.externalDiameter, 2) - Math.Pow((ProcessSurfaceAlarmModelGetModel.model.externalDiameter - ProcessSurfaceAlarmModelGetModel.model.thick), 2)) / 1000000;

            //Console.WriteLine(string.Format("混凝土{0}应变系数 {1} 混凝土计{2}初始温度为", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));


            //获取初始温度
            //var processGetInitTimeModelGetModel = new ProcessGetInitTimeModelGetModel(model.project_name, processPointAlarmModelGetModel.model.pointname, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

            ////设置初始温度
            //if (string.IsNullOrEmpty(processPointAlarmModelGetModel.model.remark))
            //{
            //    if (!ProcessGetInitTimeModel(processGetInitTimeModelGetModel, out mssg))
            //    {
            //        Console.WriteLine(mssg);
            //        processGetInitTimeModelGetModel.model = model;
            //    }
            //    Console.WriteLine(mssg);
            //    processPointAlarmModelGetModel.model.remark = processGetInitTimeModelGetModel.model.sec_oregion_scalarvalue.ToString();
            //    processPointAlarmBLL.ProcessUpdateInitDegree(processPointAlarmModelGetModel.model, out mssg);
            //    Console.WriteLine(mssg);
            //}
            //Console.WriteLine(mssg);

            Console.WriteLine(string.Format("钢支撑{0}应变系数 {1} 应变计{2}", processSameTimeModelGetModel.model.surfacename, modules, model.point_name));

            //计算应力

            double s = model.first_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.ac_val : (
                (processSameTimeModelGetModel.model.ac_val / modules) * processSameTimeModelGetModel.model.calculatepointsCont
                + model.first_oregion_scalarvalue) * modules / (processSameTimeModelGetModel.model.calculatepointsCont + 1);
            //Console.WriteLine(string.Format("第{0}次计算:" + processSameTimeModelGetModel.model.calculatepointsCont + 1));
            //Console.WriteLine(string.Format("([混凝土当前的累积变化量]" + processSameTimeModelGetModel.model.ac_val + "/" + modules + "[应变系数])*" + processSameTimeModelGetModel.model.calculatepointsCont + "[参与计算的混凝土计个数]+" + model.first_oregion_scalarvalue + "[正在计算的混凝土计的数据2]+" + "1.8*(" + model.sec_oregion_scalarvalue + "[正在计算的混凝土计的数据3]-"));
            processSameTimeModelGetModel.model.ac_val = s;


            //计算本次变化量
            var processsurfacedataGetLastTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            if (!processSurfaceDataBLL.SurfaceDataLastTimeModel(processsurfacedataGetLastTimeModelGetModel, out mssg))
            {
                processsurfacedataGetLastTimeModelGetModel.model = processSameTimeModelGetModel.model;
            }
            processSameTimeModelGetModel.model.this_val = processSameTimeModelGetModel.model.ac_val - processsurfacedataGetLastTimeModelGetModel.model.ac_val;
            processSameTimeModelGetModel.model.calculatepointsCont += 1;

            processSameTimeModelGetModel.model.SteelChordVal = model.sec_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.SteelChordVal : (processSameTimeModelGetModel.model.SteelChordVal * (processSameTimeModelGetModel.model.calculatepointsCont - 1) + model.sec_oregion_scalarvalue) / processSameTimeModelGetModel.model.calculatepointsCont;

            if (processSameTimeModelGetModel.model.calculatepointsCont > 4)
            { string a = ""; }
            processSurfaceDataBLL.ProcessResultDataAdd(processSameTimeModelGetModel.model, out mssg);
            Console.WriteLine(mssg);
            //var processgtSurfacedataCYCUpdateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessgtSurfacedataCYCUpdateModel(xmno, processSameTimeModelGetModel.model.surfacename, processSameTimeModelGetModel.model.time);
            //processSurfaceDataBLL.ProcessgtSurfacedataCYCUpdate(processgtSurfacedataCYCUpdateModel, out mssg);
            //Console.WriteLine(mssg);
            return;
        }



        public void SteelSupportCalculate(data.Model.gtsensordata model, int xmno, List<data.Model.gtpointalarmvalue> gtpointalarmvaluemodellist)
        {

            IsCycdirnetDataExistModel tsgtsensordataDataExistModel = new IsCycdirnetDataExistModel(model.project_name, model.point_name, model.time, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
            //if (checkexist)
            //{
            //    if (IsCycdirnetDataExist(tsgtsensordataDataExistModel, out mssg)) { Console.WriteLine(mssg + "不加入结算点"); return; }
            //}
            //获取点
            var pointmodellist = (from m in gtpointalarmvaluemodellist where m.sensorno == model.senorno select m).ToList();
            if (pointmodellist.Count == 0) { Console.WriteLine(string.Format("未查询到传感器编号为{0}测点信息", model.senorno)); return; }
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, model.senorno);
            //if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            //{
            //    Console.WriteLine(mssg);
            //    return;
            //}
            processPointAlarmModelGetModel.model = pointmodellist[0];
            Console.WriteLine(string.Format("获取到到传感器编号为{0}测点名为{1}支撑面名为{2}初始温度为{3}是否加入计算:{4}", model.senorno, processPointAlarmModelGetModel.model.pointname, processPointAlarmModelGetModel.model.surfaceName, processPointAlarmModelGetModel.model.remark, processPointAlarmModelGetModel.model.isInCalculate));
            if (!processPointAlarmModelGetModel.model.isInCalculate) return;
            //获取面
            var ProcessSurfaceAlarmModelGetModel = new ProcessSurfacePointAlarmBLL.ProcessPointAlarmModelGetModel(xmno,processPointAlarmModelGetModel.model.surfaceName,data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

            int i = 0;
            for (i = 0; i < 3; i++)
            {
                if (!processSurfacePointAlarmBLL.ProcessPointAlarmModelGet(ProcessSurfaceAlarmModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    Console.WriteLine("暂停1秒...");
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                else { Console.WriteLine(mssg); break; }
            }


            Console.WriteLine(mssg);
            var processSameTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            string samemssg = "";
            if (!processSurfaceDataBLL.SurfaceDataSameTimeModel(processSameTimeModelGetModel, out samemssg))
            {
                Console.WriteLine(samemssg);
                processSameTimeModelGetModel.model = new data.Model.gtsurfacedata { xmno = xmno, calculatepointsCont = 0, time = model.time, surfacename = processPointAlarmModelGetModel.model.surfaceName };
            }
            Console.WriteLine(samemssg);

            double modules = Math.PI * (Math.Pow(ProcessSurfaceAlarmModelGetModel.model.externalDiameter,2)  - Math.Pow((ProcessSurfaceAlarmModelGetModel.model.externalDiameter - ProcessSurfaceAlarmModelGetModel.model.thick),2)) / 1000000;

            //Console.WriteLine(string.Format("混凝土{0}应变系数 {1} 混凝土计{2}初始温度为", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));


            //获取初始温度
            //var processGetInitTimeModelGetModel = new ProcessGetInitTimeModelGetModel(model.project_name, processPointAlarmModelGetModel.model.pointname, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

            ////设置初始温度
            //if (string.IsNullOrEmpty(processPointAlarmModelGetModel.model.remark))
            //{
            //    if (!ProcessGetInitTimeModel(processGetInitTimeModelGetModel, out mssg))
            //    {
            //        Console.WriteLine(mssg);
            //        processGetInitTimeModelGetModel.model = model;
            //    }
            //    Console.WriteLine(mssg);
            //    processPointAlarmModelGetModel.model.remark = processGetInitTimeModelGetModel.model.sec_oregion_scalarvalue.ToString();
            //    processPointAlarmBLL.ProcessUpdateInitDegree(processPointAlarmModelGetModel.model, out mssg);
            //    Console.WriteLine(mssg);
            //}
            //Console.WriteLine(mssg);

            Console.WriteLine(string.Format("钢支撑{0}应变系数 {1} 应变计{2}", processSameTimeModelGetModel.model.surfacename, modules, model.point_name));

            //计算应力

            double s = model.first_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.ac_val : (
                (processSameTimeModelGetModel.model.ac_val / modules) * processSameTimeModelGetModel.model.calculatepointsCont
                + model.first_oregion_scalarvalue) * modules / (processSameTimeModelGetModel.model.calculatepointsCont + 1);
            //Console.WriteLine(string.Format("第{0}次计算:" + processSameTimeModelGetModel.model.calculatepointsCont + 1));
            //Console.WriteLine(string.Format("([混凝土当前的累积变化量]" + processSameTimeModelGetModel.model.ac_val + "/" + modules + "[应变系数])*" + processSameTimeModelGetModel.model.calculatepointsCont + "[参与计算的混凝土计个数]+" + model.first_oregion_scalarvalue + "[正在计算的混凝土计的数据2]+" + "1.8*(" + model.sec_oregion_scalarvalue + "[正在计算的混凝土计的数据3]-"));
            processSameTimeModelGetModel.model.ac_val = s;


            //计算本次变化量
            var processsurfacedataGetLastTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            if (!processSurfaceDataBLL.SurfaceDataLastTimeModel(processsurfacedataGetLastTimeModelGetModel, out mssg))
            {
                processsurfacedataGetLastTimeModelGetModel.model = processSameTimeModelGetModel.model;
            }
            processSameTimeModelGetModel.model.this_val = processSameTimeModelGetModel.model.ac_val - processsurfacedataGetLastTimeModelGetModel.model.ac_val;
            processSameTimeModelGetModel.model.calculatepointsCont += 1;

            processSameTimeModelGetModel.model.SteelChordVal = model.sec_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.SteelChordVal : (processSameTimeModelGetModel.model.SteelChordVal * (processSameTimeModelGetModel.model.calculatepointsCont - 1) + model.sec_oregion_scalarvalue) / processSameTimeModelGetModel.model.calculatepointsCont;

            if (processSameTimeModelGetModel.model.calculatepointsCont > 4)
            { string a = ""; }
            processSurfaceDataBLL.ProcessResultDataAdd(processSameTimeModelGetModel.model, out mssg);
            Console.WriteLine(mssg);
            //var processgtSurfacedataCYCUpdateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessgtSurfacedataCYCUpdateModel(xmno, processSameTimeModelGetModel.model.surfacename, processSameTimeModelGetModel.model.time);
            //processSurfaceDataBLL.ProcessgtSurfacedataCYCUpdate(processgtSurfacedataCYCUpdateModel, out mssg);
            //Console.WriteLine(mssg);
            return;
        }

        public void SteelSupportCalculate(int xmno)
        {
            string mssg = "";
            ProcessSurfaceDataBLL processSurfaceDataBLL = new ProcessSurfaceDataBLL();
            var processLackPointCalculateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessLackPointCalculateModel(xmno,data.Model.gtsensortype._SteelSupport);
            if (!processSurfaceDataBLL.ProcessLackPointCalculate(processLackPointCalculateModel, out mssg)) { Console.WriteLine(mssg); return; }
            DataView dv = new DataView(processLackPointCalculateModel.dt);
            ProcessSurfacedataSameTimeModelListGetModel processSurfacedataSameTimeModelListGet = null;
            data.Model.gtsurfacedata gtsurfacedata = null;
            var processPointAlarmModelListGetModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessPointAlarmBLL.ProcessPointAlarmModelListGetModel(xmno, data.Model.gtsensortype._SteelSupport);
            processPointAlarmBLL.ProcessPointAlarmModelListGet(processPointAlarmModelListGetModel, out mssg);
            Console.WriteLine(mssg);
            foreach (DataRowView drv in dv)
            {
                processSurfacedataSameTimeModelListGet = new ProcessSurfacedataSameTimeModelListGetModel(xmno.ToString(), drv["surfacename"].ToString(), Convert.ToDateTime(drv["time"]));
                if (!ProcessSurfacedataSameTimeModelListGet(processSurfacedataSameTimeModelListGet, out mssg)) { Console.WriteLine(mssg); continue; }
                gtsurfacedata = new data.Model.gtsurfacedata { surfacename = processSurfacedataSameTimeModelListGet.surfacename, cyc = Convert.ToInt32(drv["cyc"]), xmno = xmno, time = processSurfacedataSameTimeModelListGet.dt };

                if (!processSurfaceDataBLL.ProcessResultDataUpdate(gtsurfacedata, out mssg)) { Console.WriteLine(mssg); }
                string tmpsensorno = "";//processSurfacedataSameTimeModelListGet.modellist[0].senorno;
                foreach (var model in processSurfacedataSameTimeModelListGet.modellist)
                {
                    if (model.senorno == tmpsensorno) { continue; }
                    tmpsensorno = model.senorno;
                    SteelSupportCalculate(model, xmno, processPointAlarmModelListGetModel.modellist);
                }

            }
            var cycSortModel = new ProcessSurfaceDataBLL.CycSortModel(xmno, data.Model.gtsensortype._SteelSupport);
            processSurfaceDataBLL.CycSort(cycSortModel, out mssg);
            Console.WriteLine(mssg);
        }

        #endregion
        #region 结构应力
        public void StructuralStressCalculate(data.Model.gtsensordata model, int xmno)
        {

            IsCycdirnetDataExistModel tsgtsensordataDataExistModel = new IsCycdirnetDataExistModel(model.project_name, model.point_name, model.time, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
            //if (checkexist)
            //{
            if (IsCycdirnetDataExist(tsgtsensordataDataExistModel, out mssg)) { Console.WriteLine(mssg + "不加入结算点"); return; }
            //}
            //获取点
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, model.senorno);
            int i = 0;
            for (i = 0; i < 3; i++)
            {
                if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    Console.WriteLine("暂停一秒...");
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                else
                {
                    Console.WriteLine(mssg);
                    break;
                }

            }
            Console.WriteLine(mssg);
            if (!processPointAlarmModelGetModel.model.isInCalculate) return;
            Console.WriteLine(string.Format("获取到到传感器编号为{0}测点名为{1}支撑面名为{2}初始温度为{3}是否加入计算:{4}", model.senorno, processPointAlarmModelGetModel.model.pointname, processPointAlarmModelGetModel.model.surfaceName, processPointAlarmModelGetModel.model.remark, processPointAlarmModelGetModel.model.isInCalculate));
            if (!processPointAlarmModelGetModel.model.isInCalculate) return;
            //获取面
            var ProcessSurfaceAlarmModelGetModel = new ProcessSurfacePointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, processPointAlarmModelGetModel.model.surfaceName, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

            i = 0;
            for (i = 0; i < 3; i++)
            {
                if (!processSurfacePointAlarmBLL.ProcessPointAlarmModelGet(ProcessSurfaceAlarmModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    Console.WriteLine("暂停1秒...");
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                else { Console.WriteLine(mssg); break; }
            }


            Console.WriteLine(mssg);
            var processSameTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            string samemssg = "";
            if (!processSurfaceDataBLL.SurfaceDataSameTimeModel(processSameTimeModelGetModel, out samemssg))
            {
                Console.WriteLine(samemssg);
                processSameTimeModelGetModel.model = new data.Model.gtsurfacedata { xmno = xmno, calculatepointsCont = 0, time = model.time, surfacename = processPointAlarmModelGetModel.model.surfaceName, datatype=data.DAL.gtsensortype.GTSensorTypeToString(data.Model.gtsensortype._StructuralStress) };
            }
            Console.WriteLine(samemssg);
            double modules = ProcessSurfaceAlarmModelGetModel.model.concreteElasticityModulus / 1000000;

            //Console.WriteLine(string.Format("混凝土{0}应变系数 {1} 混凝土计{2}初始温度为", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));


            //获取初始温度
            var processGetInitTimeModelGetModel = new ProcessGetInitTimeModelGetModel(model.project_name, processPointAlarmModelGetModel.model.pointname, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

            //设置初始温度
            if (string.IsNullOrEmpty(processPointAlarmModelGetModel.model.remark))
            {
                if (!ProcessGetInitTimeModel(processGetInitTimeModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    processGetInitTimeModelGetModel.model = model;
                }
                Console.WriteLine(mssg);
                processPointAlarmModelGetModel.model.remark = processGetInitTimeModelGetModel.model.sec_oregion_scalarvalue.ToString();
                processPointAlarmBLL.ProcessUpdateInitDegree(processPointAlarmModelGetModel.model, out mssg);
                Console.WriteLine(mssg);
            }
            Console.WriteLine(mssg);

            Console.WriteLine(string.Format("结构应力{0}应变系数 {1} 应变计{2}初始温度为{3}", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));

            //计算应力

            double s = model.first_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.ac_val : (
                (processSameTimeModelGetModel.model.ac_val / modules) * processSameTimeModelGetModel.model.calculatepointsCont
                + model.first_oregion_scalarvalue + 1.8 * (model.sec_oregion_scalarvalue - Convert.ToDouble(processPointAlarmModelGetModel.model.remark))) * modules / (processSameTimeModelGetModel.model.calculatepointsCont + 1);
            //Console.WriteLine(string.Format("第{0}次计算:" + processSameTimeModelGetModel.model.calculatepointsCont + 1));
            //Console.WriteLine(string.Format("([混凝土当前的累积变化量]" + processSameTimeModelGetModel.model.ac_val + "/" + modules + "[应变系数])*" + processSameTimeModelGetModel.model.calculatepointsCont + "[参与计算的混凝土计个数]+" + model.first_oregion_scalarvalue + "[正在计算的混凝土计的数据2]+" + "1.8*(" + model.sec_oregion_scalarvalue + "[正在计算的混凝土计的数据3]-"));
            processSameTimeModelGetModel.model.ac_val = s;


            //计算本次变化量
            var processsurfacedataGetLastTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            if (!processSurfaceDataBLL.SurfaceDataLastTimeModel(processsurfacedataGetLastTimeModelGetModel, out mssg))
            {
                processsurfacedataGetLastTimeModelGetModel.model = processSameTimeModelGetModel.model;
            }
            processSameTimeModelGetModel.model.this_val = processSameTimeModelGetModel.model.ac_val - processsurfacedataGetLastTimeModelGetModel.model.ac_val;
            processSameTimeModelGetModel.model.calculatepointsCont += 1;

            processSameTimeModelGetModel.model.SteelChordVal = model.sec_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.SteelChordVal : (processSameTimeModelGetModel.model.SteelChordVal * (processSameTimeModelGetModel.model.calculatepointsCont - 1) + model.sec_oregion_scalarvalue) / processSameTimeModelGetModel.model.calculatepointsCont;

            if (processSameTimeModelGetModel.model.calculatepointsCont > 4)
            { string a = ""; }
            processSurfaceDataBLL.ProcessResultDataAdd(processSameTimeModelGetModel.model, out mssg);
            Console.WriteLine(mssg);
            //var processgtSurfacedataCYCUpdateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessgtSurfacedataCYCUpdateModel(xmno, processSameTimeModelGetModel.model.surfacename, processSameTimeModelGetModel.model.time);
            //processSurfaceDataBLL.ProcessgtSurfacedataCYCUpdate(processgtSurfacedataCYCUpdateModel, out mssg);
            //Console.WriteLine(mssg);
            if (/*DateTime.Now.Hour == 10  || DateTime.Now.Hour == 18 || */DateTime.Now.Hour ==10)
            {
                StructuralStressCalculate(xmno);

            }
     
            return;
        }


        public void StructuralStressCalculate(data.Model.gtsensordata model, int xmno, List<data.Model.gtpointalarmvalue> gtpointalarmvaluemodellist)
        {

            IsCycdirnetDataExistModel tsgtsensordataDataExistModel = new IsCycdirnetDataExistModel(model.project_name, model.point_name, model.time, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));
            //if (checkexist)
            //{
            //    if (IsCycdirnetDataExist(tsgtsensordataDataExistModel, out mssg)) { Console.WriteLine(mssg + "不加入结算点"); return; }
            //}
            //获取点
            var pointmodellist = (from m in gtpointalarmvaluemodellist where m.sensorno == model.senorno select m).ToList();
            if (pointmodellist.Count == 0) { Console.WriteLine(string.Format("未查询到传感器编号为{0}测点信息", model.senorno)); return; }
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, model.senorno);
            //if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            //{
            //    Console.WriteLine(mssg);
            //    return;
            //}
            processPointAlarmModelGetModel.model = pointmodellist[0];
            Console.WriteLine(string.Format("获取到到传感器编号为{0}测点名为{1}支撑面名为{2}初始温度为{3}是否加入计算:{4}", model.senorno, processPointAlarmModelGetModel.model.pointname, processPointAlarmModelGetModel.model.surfaceName, processPointAlarmModelGetModel.model.remark, processPointAlarmModelGetModel.model.isInCalculate));
            if (!processPointAlarmModelGetModel.model.isInCalculate) return;
            //获取面
            var ProcessSurfaceAlarmModelGetModel = new ProcessSurfacePointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, processPointAlarmModelGetModel.model.surfaceName, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

            int i = 0;
            for (i = 0; i < 3; i++)
            {
                if (!processSurfacePointAlarmBLL.ProcessPointAlarmModelGet(ProcessSurfaceAlarmModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    Console.WriteLine("暂停1秒...");
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                else { Console.WriteLine(mssg); break; }
            }


            Console.WriteLine(mssg);
            var processSameTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            string samemssg = "";
            if (!processSurfaceDataBLL.SurfaceDataSameTimeModel(processSameTimeModelGetModel, out samemssg))
            {
                Console.WriteLine(samemssg);
                processSameTimeModelGetModel.model = new data.Model.gtsurfacedata { xmno = xmno, calculatepointsCont = 0, time = model.time, surfacename = processPointAlarmModelGetModel.model.surfaceName };
            }
            Console.WriteLine(samemssg);
            double modules = ProcessSurfaceAlarmModelGetModel.model.concreteElasticityModulus/ 1000000;

            //Console.WriteLine(string.Format("混凝土{0}应变系数 {1} 混凝土计{2}初始温度为", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));


            //获取初始温度
            var processGetInitTimeModelGetModel = new ProcessGetInitTimeModelGetModel(model.project_name, processPointAlarmModelGetModel.model.pointname, data.DAL.gtsensortype.GTStringToSensorType(model.datatype));

            //设置初始温度
            if (string.IsNullOrEmpty(processPointAlarmModelGetModel.model.remark))
            {
                if (!ProcessGetInitTimeModel(processGetInitTimeModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    processGetInitTimeModelGetModel.model = model;
                }
                Console.WriteLine(mssg);
                processPointAlarmModelGetModel.model.remark = processGetInitTimeModelGetModel.model.sec_oregion_scalarvalue.ToString();
                processPointAlarmBLL.ProcessUpdateInitDegree(processPointAlarmModelGetModel.model, out mssg);
                Console.WriteLine(mssg);
            }
            Console.WriteLine(mssg);

            Console.WriteLine(string.Format("结构应力{0}应变系数 {1} 应变计{2}初始温度为{3}", processSameTimeModelGetModel.model.surfacename, modules, model.point_name, processPointAlarmModelGetModel.model.remark));

            //计算应力

            double s = model.first_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.ac_val : (
                (processSameTimeModelGetModel.model.ac_val / modules) * processSameTimeModelGetModel.model.calculatepointsCont
                + model.first_oregion_scalarvalue + 1.8 * (model.sec_oregion_scalarvalue - Convert.ToDouble(processPointAlarmModelGetModel.model.remark))) * modules / (processSameTimeModelGetModel.model.calculatepointsCont + 1);
            //Console.WriteLine(string.Format("第{0}次计算:" + processSameTimeModelGetModel.model.calculatepointsCont + 1));
            //Console.WriteLine(string.Format("([混凝土当前的累积变化量]" + processSameTimeModelGetModel.model.ac_val + "/" + modules + "[应变系数])*" + processSameTimeModelGetModel.model.calculatepointsCont + "[参与计算的混凝土计个数]+" + model.first_oregion_scalarvalue + "[正在计算的混凝土计的数据2]+" + "1.8*(" + model.sec_oregion_scalarvalue + "[正在计算的混凝土计的数据3]-"));
            processSameTimeModelGetModel.model.ac_val = s;


            //计算本次变化量
            var processsurfacedataGetLastTimeModelGetModel = new ProcessSurfaceDataBLL.ProcessGetLastTimeModelGetModel(xmno, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), processPointAlarmModelGetModel.model.surfaceName, model.time);
            if (!processSurfaceDataBLL.SurfaceDataLastTimeModel(processsurfacedataGetLastTimeModelGetModel, out mssg))
            {
                processsurfacedataGetLastTimeModelGetModel.model = processSameTimeModelGetModel.model;
            }
            processSameTimeModelGetModel.model.this_val = processSameTimeModelGetModel.model.ac_val - processsurfacedataGetLastTimeModelGetModel.model.ac_val;
            processSameTimeModelGetModel.model.calculatepointsCont += 1;

            processSameTimeModelGetModel.model.SteelChordVal = model.sec_oregion_scalarvalue == -999999 ? processSameTimeModelGetModel.model.SteelChordVal : (processSameTimeModelGetModel.model.SteelChordVal * (processSameTimeModelGetModel.model.calculatepointsCont - 1) + model.sec_oregion_scalarvalue) / processSameTimeModelGetModel.model.calculatepointsCont;

            if (processSameTimeModelGetModel.model.calculatepointsCont > 4)
            { string a = ""; }
            processSurfaceDataBLL.ProcessResultDataAdd(processSameTimeModelGetModel.model, out mssg);
            Console.WriteLine(mssg);
            //var processgtSurfacedataCYCUpdateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessgtSurfacedataCYCUpdateModel(xmno, processSameTimeModelGetModel.model.surfacename, processSameTimeModelGetModel.model.time);
            //processSurfaceDataBLL.ProcessgtSurfacedataCYCUpdate(processgtSurfacedataCYCUpdateModel, out mssg);
            //Console.WriteLine(mssg);
            return;
        }

        public void StructuralStressCalculate(int xmno)
        {
            string mssg = "";
            ProcessSurfaceDataBLL processSurfaceDataBLL = new ProcessSurfaceDataBLL();
            var processLackPointCalculateModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessLackPointCalculateModel(xmno, data.Model.gtsensortype._StructuralStress);
            /*if (!processSurfaceDataBLL.ProcessLackPointCalculate(processLackPointCalculateModel, out mssg)) { Console.WriteLine(mssg); return; }
            DataView dv = new DataView(processLackPointCalculateModel.dt);
            ProcessSurfacedataSameTimeModelListGetModel processSurfacedataSameTimeModelListGet = null;
            data.Model.gtsurfacedata gtsurfacedata = null;
            var processPointAlarmModelListGetModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessPointAlarmBLL.ProcessPointAlarmModelListGetModel(xmno, data.Model.gtsensortype._StructuralStress);
            processPointAlarmBLL.ProcessPointAlarmModelListGet(processPointAlarmModelListGetModel, out mssg);
            Console.WriteLine(mssg);
            foreach (DataRowView drv in dv)
            {
                processSurfacedataSameTimeModelListGet = new ProcessSurfacedataSameTimeModelListGetModel(xmno.ToString(), drv["surfacename"].ToString(), Convert.ToDateTime(drv["time"]));
                if (!ProcessSurfacedataSameTimeModelListGet(processSurfacedataSameTimeModelListGet, out mssg)) { Console.WriteLine(mssg); continue; }
                gtsurfacedata = new data.Model.gtsurfacedata { surfacename = processSurfacedataSameTimeModelListGet.surfacename, cyc = Convert.ToInt32(drv["cyc"]), xmno = xmno, time = processSurfacedataSameTimeModelListGet.dt };

                if (!processSurfaceDataBLL.ProcessResultDataUpdate(gtsurfacedata, out mssg)) { Console.WriteLine(mssg); }
                string tmpsensorno = "";//processSurfacedataSameTimeModelListGet.modellist[0].senorno;
                foreach (var model in processSurfacedataSameTimeModelListGet.modellist)
                {
                    if (model.senorno == tmpsensorno) { continue; }
                    tmpsensorno = model.senorno;
                    StructuralStressCalculate(model, xmno, processPointAlarmModelListGetModel.modellist);
                }

            }*/
            var cycSortModel = new ProcessSurfaceDataBLL.CycSortModel(xmno, data.Model.gtsensortype._StructuralStress);
            processSurfaceDataBLL.CycSort(cycSortModel, out mssg);
            Console.WriteLine(mssg);
        }

        #endregion


        public void WaterLevelCalculate(data.Model.gtsensordata model, int xmno)
        {
            
            //获取点名
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, model.senorno);
            string mssg = "";
            if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                Console.WriteLine(mssg);
                return;
            }
            //本次水深
            double L = processPointAlarmModelGetModel.model.line - model.first_oregion_scalarvalue;
            //计算累计变化量
            model.single_ac_scalarvalue = processPointAlarmModelGetModel.model.thisOrificeHeight - processPointAlarmModelGetModel.model.orificeHeight + L;
            //获取上次变化
            var processGetLastTimeModelGetModel = new ProcessGetLastTimeModelGetModel(model.project_name,processPointAlarmModelGetModel.model.pointname,data.DAL.gtsensortype.GTStringToSensorType(model.datatype),model.time);
             if(!ProcessGetLastTimeModel(processGetLastTimeModelGetModel,out mssg))
            {
                Console.WriteLine(mssg);
                processGetLastTimeModelGetModel.model = model;
            }
             model.single_this_scalarvalue = model.single_ac_scalarvalue - processGetLastTimeModelGetModel.model.single_ac_scalarvalue;
        }
        public void FractureMeterCalculate(data.Model.gtsensordata model, int xmno)
        {

            //获取点名
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, model.senorno);
            string mssg = "";
            if (!processPointAlarmBLL.ProcessSensornoAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                Console.WriteLine(mssg);
                return;
            }
           
            //计算累计变化量
            model.single_ac_scalarvalue = processPointAlarmModelGetModel.model.orificeHeight + model.first_oregion_scalarvalue;
            //获取上次变化
            var processGetLastTimeModelGetModel = new ProcessGetLastTimeModelGetModel(model.project_name, processPointAlarmModelGetModel.model.pointname, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), model.time);
            if (!ProcessGetLastTimeModel(processGetLastTimeModelGetModel, out mssg))
            {
                Console.WriteLine(mssg);
                processGetLastTimeModelGetModel.model = model;
            }
            model.single_this_scalarvalue = model.single_ac_scalarvalue - processGetLastTimeModelGetModel.model.single_ac_scalarvalue;
            model.first_ac_scalarvalue = model.single_ac_scalarvalue + processPointAlarmModelGetModel.model.orificeHeight;
            
            //model.single_this_scalarvalue = model.single_ac_scalarvalue - processGetLastTimeModelGetModel.model.single_ac_scalarvalue;
        }

        public void StaticLevelCalculate(int xmno,List<data.Model.gtsensordata> gtsensordatalist)
        {
             NFnet_BLL.DisplayDataProcess.GTSettlement.ProcessPointAlarmBLL gtsettlementProcessPointAlarmBLL = new GTSettlement.ProcessPointAlarmBLL();
            Dictionary<DateTime, List<data.Model.gtsensordata>> dtl = StaticLevelTimeSplit(gtsensordatalist);
            //获取基准点
            var sensorPointLoadCondition = new SensorPointLoadCondition(xmno,data.Model.gtsensortype._staticLevel);
            gtsettlementProcessPointAlarmBLL.ProcessSettlementBasePointLoad(sensorPointLoadCondition,out mssg);
            foreach (string basepointname in sensorPointLoadCondition.ls)
            {
                var settlementBasePointBridgeGetModel = new NFnet_BLL.DisplayDataProcess.GTSettlement.ProcessPointAlarmBLL.gtSettlementBasePointBridgeGetModel(xmno,basepointname);
                
                gtsettlementProcessPointAlarmBLL.gtSettlementBasePointBridgeGet(settlementBasePointBridgeGetModel, out mssg);
                List<List<global::data.Model.gtsettlementpointalarmvalue>> gtsettlementpointalarmvaluelist = settlementBasePointBridgeGetModel.gtsettlementpointalarmvaluebridge;
                foreach (var key in dtl.Keys)
                {
                    List<data.Model.gtsensordata> gtsensordatabridge = dtl[key];
                    //将数据压入桥栈

                    foreach (var gtsettlementbridge in gtsettlementpointalarmvaluelist)
                    {
                        int i = 0;
                        List<data.Model.gtsensordata> bridgestack = new List<data.Model.gtsensordata>();
                        foreach(var gtsettlement in gtsettlementbridge)
                        {
                        var gtsensordata = (from m in gtsensordatabridge where m.point_name == gtsettlement.point_name select m).ToList();

                        if (gtsensordata.Count == 0) {break; }
                        bridgestack.Add(gtsensordata[0]);
                        i++;
                        }
                        if (i == gtsettlementbridge.Count)
                        {
                            Console.WriteLine(string.Format("成功生成桥栈 {0}现在计算沉降桥的累计值", string.Join(",", (from m in bridgestack select m.point_name).ToList())));
                            Console.WriteLine(string.Format("成功生成桥栈 {0}现在计算沉降桥的累计值", string.Join(",", (from m in bridgestack select m.point_name).ToList())));
                        }
                        else
                        { Console.WriteLine(string.Format("生成桥栈 {0}失败", string.Join(",", (from m in gtsettlementbridge select m.point_name).ToList())));
                        Console.WriteLine(string.Format("生成桥栈 {0}失败", string.Join(",", (from m in gtsettlementbridge select m.point_name).ToList())));
                            continue; }
                        Gtsettlementbridgecalculate(xmno, bridgestack,out mssg);
                   
                    }
                }
            }

        }
        //public List<>

        public void Gtsettlementbridgecalculate(int xmno,List<data.Model.gtsensordata> gtsensordatabridge,out string mssg)
        {
            mssg = "";
            double sumbasesettlevalue = 0;
            NFnet_BLL.DisplayDataProcess.GTSettlement.ProcessPointAlarmBLL gtsettlementProcessPointAlarmBLL = new GTSettlement.ProcessPointAlarmBLL();
 
            foreach (data.Model.gtsensordata model in gtsensordatabridge)
            {
                var processPointAlarmModelGetModel = new NFnet_BLL.DisplayDataProcess.GTSettlement.ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno,model.point_name);
                gtsettlementProcessPointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel,out mssg);
                model.single_ac_scalarvalue = model.first_oregion_scalarvalue - processPointAlarmModelGetModel.model.initsettlementval - sumbasesettlevalue;
                var processGetLastTimeModelGetModel = new ProcessGetLastTimeModelGetModel(model.project_name, processPointAlarmModelGetModel.model.point_name, data.DAL.gtsensortype.GTStringToSensorType(model.datatype), model.time);
                if (!ProcessGetLastTimeModel(processGetLastTimeModelGetModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    processGetLastTimeModelGetModel.model = model;
                }
                model.single_this_scalarvalue = model.single_ac_scalarvalue - processGetLastTimeModelGetModel.model.single_ac_scalarvalue;
                sumbasesettlevalue += model.single_ac_scalarvalue;
            }
        }

        public Dictionary<DateTime, List<data.Model.gtsensordata>> StaticLevelTimeSplit(List<data.Model.gtsensordata> gtsensordatalist)
        {
            var modellist = (from m in gtsensordatalist  orderby m.time,m.point_name select m).ToList();
            DateTime dttmp = modellist[0].time;
            List<data.Model.gtsensordata> sensordatalist = new List<data.Model.gtsensordata>();
            Dictionary<DateTime, List<data.Model.gtsensordata>> dtl = new Dictionary<DateTime, List<data.Model.gtsensordata>>();
            foreach (var model in gtsensordatalist)
            {
                if (dttmp != model.time)
                {
                    dtl.Add(dttmp, sensordatalist);
                    dttmp = model.time;
                    sensordatalist = new List<data.Model.gtsensordata>();
                }
                sensordatalist.Add(model);
            }
            dtl.Add(dttmp, sensordatalist);
            return dtl;
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DeleteModel model,out string mssg)
        {
            return bll.Delete(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname),model.datatype,model.startcyc,model.endcyc,out mssg);
           
        }
        public class DeleteModel
        {
            public string xmname { get; set; }
            public string point_name { get; set; }
            public int startcyc { get; set; }
            public int endcyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public DeleteModel(string xmname,  int startcyc, int endcyc, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.point_name = point_name;
                this.startcyc = startcyc;
                this.endcyc = endcyc;
                this.datatype = datatype;
            }
        }


        public bool DeleteTmp(DeleteTmpModel model,out string mssg)
        {

            return bll.DeleteTmp(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, out mssg);
                
           
        }
        public class DeleteTmpModel
        {
            public string xmname{ get; set;}
            public data.Model.gtsensortype datatype{ get; set;}
            public DeleteTmpModel(string xmname, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.datatype = datatype;
            }
        }

       

        public bool ProcessResultDataUpdate(global::data.Model.gtsensordata model, out string mssg)
        {
            return bll.Update(model, out mssg);
        }



        /// <summary>
        /// 结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ScalarResultdataTableRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            int totalCont = 0;
            if (bll.ScalarResultTableRowsCount(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.sord, model.datatype, model.startcyc, model.endcyc, out totalCont, out mssg))
            {
                model.totalCont = totalCont.ToString();
                return true;
            }
            else
            {

                return false;
            }

        }

        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool SingleScalarResultdataTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.SingleScalarResultdataTableLoad(model.pageIndex, model.rows, ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.sord, model.datatype, model.startcyc, model.endcyc, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool SingleScalarOrgldataTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.SingleScalarOrgldataTableLoad(model.pageIndex, model.rows, ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.sord, model.datatype, model.startcyc, model.endcyc, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }


        public bool ProcessXmStateTableLoad(XmStateTableLoadCondition model, out string mssg)
        {
            DataTable dt = new DataTable();
            mssg = "";
            //if (bll.XmStateTable(model.pageIndex, model.rows, model.xmno,model.xmname, model.unitname, model.colName, model.sord, out dt, out mssg))
            //{
            //    model.dt = dt;
            //    return true;
            //}
            return false;
        }
        /// <summary>
        /// 结果数据报表数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataReportTableCreate(ResultDataReportTableCreateCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ResultDataReportPrint(model.sql, ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model,out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.PointNameDateTimeListGet(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.datatype, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
       
        public class ProcessResultDataTimeListLoadModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public List<string> ls { get; set; }
            public ProcessResultDataTimeListLoadModel(string xmname, string pointname, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.datatype = datatype;
            }
        }


        /// <summary>
        /// 数据展示日期查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {

            switch (model.type)
            {
                case QueryType.RQCX:
                    string sqlsttm = "";
                    string sqledtm = "";
                    string mssg = "";
                    string cycmin = "";
                    string cycmax = "";
                    var querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.startTime, ">=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqlsttm = querynvlmodel.str;
                    }
                    querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.endTime, "<=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqledtm = querynvlmodel.str;
                    }
                    string rqConditionStr = sqlsttm + " and " + sqledtm + " order by #_point,#_date asc ";
                    string sqlmin = "select min(time) as mincyc from gtsensordata where project_name = '" + model.xmname + "'";
                    string sqlmax = "select max(time) as maxcyc from gtsensordata where project_name = '" + model.xmname + "'";
                    var processquerystanderstrModel = new QuerystanderstrModel(sqlmin, model.xmno);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                        model.startTime = processquerystanderstrModel.str;
                    }
                    processquerystanderstrModel = new QuerystanderstrModel(sqlmax, model.xmno);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                        model.endTime = processquerystanderstrModel.str;
                    }
                    model.sql = rqConditionStr;
                    break;
                case QueryType.ZQCX:
                    rqConditionStr = "#_cyc >= " + model.startTime + "   and  #_cyc <= " + model.endTime + " order by #_point,cyc,#_date asc ";
                    model.minCyc = model.startTime;
                    model.maxCyc = model.endTime;

                    model.sql = rqConditionStr;
                    break;
                case QueryType.QT:
                    var processdateswdlModel = new ProcessComBLL.ProcessdateswdlModel("#_date", model.unit, model.maxTime);
                    if (ProcessComBLL.Processdateswdl(processdateswdlModel, out mssg))
                    {
                        model.startTime = processdateswdlModel.sttm;
                        model.endTime = processdateswdlModel.edtm;
                        //model.sql = processdateswdlModel.sql;
                    }
                    break;


            }
        }

        public bool ProcessResultDataTimeCycLoad(ProcessResultDataTimeCycLoadModel model, out string mssg)
        {
            List<string> cyctimelist = null;
            mssg = "";
            if (bll.CycTimeList(model.xmname,model.datatype ,out cyctimelist, out mssg))
            {
                model.cyctimelist = cyctimelist;
                return true;
            }
            return false;
        }
        public class ProcessResultDataTimeCycLoadModel
        {
            public List<string> cyctimelist { get; set; }
            public string xmname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public ProcessResultDataTimeCycLoadModel(string xmname, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.datatype = datatype;
            }

        }

        public bool ProcessdataResultDataMaxTime(GTSensorMaxTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }
        public bool ProcesspointdataResultDataMaxTime(GTSensorMaxTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, model.pointname, out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }
        public bool ProcesspointdataResultDataMinTime(GTSensorMinTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.MinTime(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, model.pointname, out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            return false;


        }



        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessSingleScalarfillTotalStationDbFill(FillTotalStationDbFillCondition model)
        {
            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "time");
            model.rqConditionStr = model.rqConditionStr.Replace("#_cyc", "cyc");
            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "point_name");
            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel("  point_name  ", model.pointname, " in ", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = "select  project_name,point_name,senorno,datatype,valuetype,single_oregion_scalarvalue,first_oregion_scalarvalue,sec_oregion_scalarvalue ,single_this_scalarvalue,single_ac_scalarvalue ,cyc,time     from  gtsensordata where  project_name = '" + model.xmname + "' and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
                sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            Console.WriteLine(sql);
            DataTable dt = new DataTable();
            Console.WriteLine(mssg);
            if (GetResultDataTable(sql, model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                Console.WriteLine(mssg);
                return true;
            }
            
            return false;

        }

        public bool GetResultDataTable(string sql, string xmname, out DataTable dt, out string mssg)
        {
            mssg = "";
            dt = null;
            var processquerystanderdbModel = new QuerystanderdbModel(sql, ProcessAspectIndirectValue.GetXmnoFromXmnameStr(xmname));
            if (ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
            {
                dt = processquerystanderdbModel.dt;
                return true;
            }
            return false;
        }



        //根据表的分页单位获取当前记录在表中的页数
        public int PageIndexFromTab(string pointname, string date, DataView dv, string pointnameStr, string dateStr, int pageSize)
        {

            //DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr];
                if (drv[pointnameStr].ToString() == pointname && date.Trim() == drv[dateStr].ToString())
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }

        public bool ProcessScalarResultDataAlarmModelList(ProcessScalarResultDataAlarmModelListModel model, out List<global::data.Model.gtsensordata> modellist, out string mssg)
        {
            modellist = new List<global::data.Model.gtsensordata>();
            if (bll.ScalarGetModelList(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }

        public class ProcessScalarResultDataAlarmModelListModel
        {
            public string xmname { get; set; }
            public List<global::data.Model.gtsensordata> modellist { set; get; }
            public ProcessScalarResultDataAlarmModelListModel(string xmname)
            {
                this.xmname = xmname;
            }
        }



        public bool ProcessSingleResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out List<global::data.Model.gtsensordata> modellist, out string mssg)
        {
            modellist = new List<global::data.Model.gtsensordata>();
            if (bll.SingleScalarGetModelList(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }

        public class ProcessResultDataAlarmModelListModel
        {
            public string xmname { get; set; }
            public List<global::data.Model.gtsensordata> modellist { set; get; }
            public data.Model.gtsensortype datatype { get; set; }
            public ProcessResultDataAlarmModelListModel(string xmname, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.datatype = datatype;
            }
        }

        public bool ProcessSurfacedataSameTimeModelListGet(ProcessSurfacedataSameTimeModelListGetModel model, out string mssg)
        {
            var modellist = new List<global::data.Model.gtsensordata>();
            if (bll.SurfacedataSameTimeModelListGet(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.surfacename,model.dt ,out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class ProcessSurfacedataSameTimeModelListGetModel
        {
            public string xmname { get; set; }
            public string surfacename { get; set; }
            public DateTime dt { get; set; }
            public List<global::data.Model.gtsensordata> modellist { get; set; }
            public ProcessSurfacedataSameTimeModelListGetModel(string xmname, string surfacename,DateTime dt)
            {
                this.xmname = xmname;
                this.surfacename = surfacename;
                this.dt = dt;
            }
        }


        /// <summary>
        /// 点名列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessdataPointLoad(GTSensorPointLoadCondition model, out string mssg)
        {
            List<string> ls = null;
            if (bll.dataPointLoadBLL(model.xmno, model.datatype, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ProcessPointNewestDateTimeGet(GTPointNewestDateTimeCondition model, out string mssg)
        {
            DateTime dt = new DateTime();
            if (bll.PointNewestDateTimeGet(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.datatype, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }

        public bool ProcessPointOnLineCont(PointOnLineContCondition model, out string mssg)
        {
            string contpercent = "";
            if (bll.OnLinePointCont(model.unitname,model.finishedxmnolist ,out contpercent, out mssg))
            {
                model.contpercent = contpercent;
                return true;
            }
            return false;
        }

        //public bool Processdatacyccreate(ProcessdatacyccreateModel model,out string mssg)
        //{
        //    int cyc = -1;
        //    if(bll.cyc)
        //}
        public class ProcessdatacyccreateModel
        {
            public string xmname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string point_name { get; set; }
            public int cyc { get; set; }

            public ProcessdatacyccreateModel(string xmname, data.Model.gtsensortype datatype,string point_name)
            {
                this.xmname = xmname;
                this.datatype = datatype;
                this.point_name = point_name;
            }
        }
        public class IsTypeDataModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public IsTypeDataModel(string xmname, DateTime dt, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.dt = dt;
                this.datatype = datatype;
            }
        }

        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(IsTypeDataModel model, out string mssg)
        {
            return bll.IsInsertData(model.xmname, model.dt,model.datatype ,out mssg);

        }
        public class IsInsertDataModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public IsInsertDataModel(string xmname, DateTime dt, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.dt = dt;
                this.datatype = datatype;
            }
        }
        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(InsertDataCycGetModel model, out string mssg)
        {
            int cyc = -1;
            if (bll.InsertDataCycGet(model.xmname, model.dt,model.datatype ,out cyc, out mssg))
            {
                model.cyc = cyc;
                return true;
            }
            return false;
        }
        public class InsertDataCycGetModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public int cyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public InsertDataCycGetModel(string xmname, DateTime dt, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.dt = dt;
                this.datatype = datatype;
            }
        }

        /// <summary>
        /// 判断记录是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool IsCycdirnetDataExist(IsCycdirnetDataExistModel model, out string mssg)
        {
            return bll.IsCycdirnetDataExist(model.xmname, model.pointname, model.dt,model.datatype ,out mssg);

        }
        public class IsCycdirnetDataExistModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public DateTime dt { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public IsCycdirnetDataExistModel(string xmname, string pointname, DateTime dt, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.dt = dt;
                this.datatype = datatype;
            }
            public IsCycdirnetDataExistModel()
            {
                
            }
        }

        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(IsInsertCycExistModel model, out string mssg)
        {
            return bll.IsInsertCycExist(model.xmname, model.cyc,model.datatype ,out mssg);
        }
        public class IsInsertCycExistModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public IsInsertCycExistModel(string xmname, int cyc, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.cyc = cyc;
                this.datatype = datatype;
            }
        }

        public bool ProcessReportDataView(ProcessReportDataViewModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ReportDataView(model.cyclist, model.pageIndex, model.rows, model.xmname,model.datatype ,model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessReportDataViewModel : ResultDataLoadCondition
        {
            public List<string> cyclist { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public ProcessReportDataViewModel(List<string> cyclist, int pageIndex, int pageSize, string xmname,data.Model.gtsensortype datatype ,string sord)
            {
                this.cyclist = cyclist;
                this.pageIndex = pageIndex;
                this.rows = pageSize;
                this.xmname = xmname;
                this.sord = sord;
                this.datatype = datatype;
            }
        }


        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        //public bool IsInsertCycExist(IsInsertCycExistModel model, out string mssg)
        //{
        //    return bll.IsInsertCycExist(model.xmname, model.cyc, out mssg);
        //}
        //public class IsInsertCycExistModel
        //{
        //    public string xmname { get; set; }
        //    public int cyc { get; set; }
        //    public IsInsertCycExistModel(string xmname, int cyc)
        //    {
        //        this.xmname = xmname;
        //        this.cyc = cyc;
        //    }
        //}


        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool InsertCycStep(InsertCycStepModel model, out string mssg)
        {
            return bll.InsertCycStep(model.xmname, model.startcyc,model.datatype ,out mssg);
        }

        public class InsertCycStepModel
        {
            public string xmname { get; set; }
            public int startcyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public InsertCycStepModel(string xmname, int startcyc,data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.startcyc = startcyc;
                this.datatype = datatype;
            }
        }
        public bool SiblingDataCycGet(DataCycGetModel model, out string mssg)
        {
            int cyc = -1;
            if (bll.SiblingDataCycGet(model.xmname, model.dt,model.datatype ,model.points, model.mobileStationInteval, out cyc, out mssg))
            {
                model.cyc = cyc;
                return true;
            }
            return false;
        }
        public class DataCycGetModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public List<string> points { get; set; }
            public double mobileStationInteval { get; set; }
            public int cyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public DataCycGetModel(string xmname, DateTime dt, data.Model.gtsensortype datatype, List<string> points, double mobileStationInteval)
            {
                this.xmname = xmname;
                this.dt = dt;
                this.points = points;
                this.mobileStationInteval = mobileStationInteval;
                this.datatype = datatype;
            }
        }
        /// <summary>
        /// 结果数据端点周期获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataExtremely(ResultDataExtremelyCondition model, out string mssg)
        {
            string extremelyCyc = "";
            if (bll.ExtremelyCycGet(model.xmname,model.datatype,model.dateFunction, out mssg, out extremelyCyc))
            {
                model.extremelyCyc = extremelyCyc;
                return true;
            }
            else
            {
                return false;
            }
        }
        public class ResultDataExtremelyCondition
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 日期函数
            /// </summary>
            public string dateFunction { get; set; }
            /// <summary>
            /// 周期
            /// </summary>
            public string extremelyCyc { get; set; }

            public data.Model.gtsensortype datatype { get; set; }
            public ResultDataExtremelyCondition(string xmname, string dateFunction, data.Model.gtsensortype datatype)
            {
                this.xmname = ProcessAspectIndirectValue.GetXmnoFromXmnameStr(xmname);
                this.dateFunction = dateFunction;
                this.datatype = datatype;
            }
        }

        public bool ProcessLostPoints(ProcessLostPointsModel model, out string mssg)
        {
            mssg = "";
            string pointstr = "";
            if (bll.LostPoints(model.xmno, model.xmname,model.datatype ,model.cyc, out pointstr, out mssg))
            {
                model.pointstr = pointstr;
                return true;
            }
            return false;

        }
        public class ProcessLostPointsModel
        {
            public int xmno { get; set; }
            public string xmname { get; set; }
            public int cyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string pointstr { get; set; }
            public ProcessLostPointsModel(string xmname, int xmno, data.Model.gtsensortype datatype, int cyc)
            {
                this.cyc = cyc;
                this.xmno = xmno;
                this.xmname = xmname;
                this.pointstr = pointstr;
                this.datatype = datatype;
            }
        }

    }
}