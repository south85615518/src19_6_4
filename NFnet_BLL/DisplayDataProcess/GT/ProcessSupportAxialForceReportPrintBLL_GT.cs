﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.Data;
using NFnet_BLL.DataProcess;
using System.IO;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.gtsensordata
{
    /// <summary>
    /// 全站仪数据报表打印业务逻辑处理类
    /// </summary>
    public partial class ProcessReportPrintBLL
    {
        
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessSupportAxialForceStressSPMTReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                //var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                //ExceptionLog.ExceptionWrite("开始填充数据");
                ////model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}


                //totalStationReportHelper.MainSupportAxialForceStress_GT_WithoutSettlement(
                //    model.xmname,
                //    "时间,本次(mm),累计(mm)",
                //    model.dt,
                //    model.exportpath
                //    );
                //mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                mssg = "";
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("地下围护墙钢筋应力单点报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }

        



 




     
        #region GT
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessSupportAxialForceMPSCReport_GT(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                var processTotalStationReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}




                //model.reportparams = new Tool.DATAREPORT.TotalStation.MPSC.GT.reportparams { alarmmess = new List<string>(),  firstalrmacdisp = "5/-5",  firstalrmthisdisp = "5/-5",  secalrmacdisp = "2/-2",  secalrmthisdisp = "10/-10", xmaddress = "工程地址" };
                //model.reportparams.alarmmess = new string[] { "SDYBQ687累计东方向4.5，级别:预警" }.ToList();

                List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(model.dt, "cyc");

                model.reportparams.alarmmess = NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess.ProcessReportAlarmSplitModelList.ReportAlarmSplitMssgLoad(NFnet_BLL.DisplayDataProcess.pub.ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname), cyclist, data.Model.gtsensortype._supportAxialForce, out mssg);


                totalStationReportHelper.gtSupportAxialForceMainMPSC(
                    model.xmname,
                    "点名,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath,
                    model.timeunitname,
                    model.reportparams
                    );
                mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点单周期报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessSupportAxialForce_timespanMPSCReport_GT(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                var processTotalStationReportModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}




                //model.reportparams = new Tool.DATAREPORT.TotalStation.MPSC.GT.reportparams { alarmmess = new List<string>(), firstalrmacdisp = "5/-5", firstalrmthisdisp = "5/-5", secalrmacdisp = "2/-2", secalrmthisdisp = "10/-10", xmaddress = "工程地址" };
                //model.reportparams.alarmmess = new string[] { "SDYBQ687累计东方向4.5，级别:预警" }.ToList();

                List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(model.dt, "cyc");

                model.reportparams.alarmmess = NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess.ProcessReportAlarmSplitModelList.ReportAlarmSplitMssgLoad(NFnet_BLL.DisplayDataProcess.pub.ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname), cyclist, data.Model.gtsensortype._supportAxialForce, out mssg);


                totalStationReportHelper.SupportAxialForce_timespanMainMPSC(
                    model.xmname,
                    "点名,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath,
                    model.timeunitname,
                    model.reportparams
                    );
                mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点单周期报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }
       

        #endregion


    }
}