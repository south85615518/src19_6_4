﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DataProcess
{
    public partial class ProcessSurfaceDataBLL
    {

        public bool AddSurveyData(AddSurveyDataModel model, out string mssg)
        {
            return bll.AddSurveyData(model.xmno,model.datatype ,model.cyc,  out mssg);
        }
        public class AddSurveyDataModel
        {
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public int cyc { get; set; }
            public int importcyc { get; set; }
            
            public AddSurveyDataModel(int xmno,data.Model.gtsensortype datatype, int cyc, int importcyc)
            {
                this.xmno = xmno;
                this.cyc = cyc;
                this.importcyc = importcyc;
                this.datatype = datatype;
                
            }
            public AddSurveyDataModel(int xmno,data.Model.gtsensortype datatype, int cyc)
            {
                this.xmno = xmno;this.datatype = datatype;
                this.cyc = cyc;

            }
        }

        public bool UpdataSurveyData(UpdataSurveyDataModel model, out string mssg)
        {
            mssg = "";
            return false;//bll.UpdataSurveyData(model.xmno,model.datatype , model.point_name, model.dt,  model.vSet_name, model.vLink_name, model.srcdatetime, out mssg);
        }
        public class UpdataSurveyDataModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public DateTime dt { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string vSet_name { get; set; }
            public string vLink_name { get; set; }
            public DateTime srcdatetime { get; set; }
            public UpdataSurveyDataModel(int xmno,data.Model.gtsensortype datatype, string point_name, DateTime dt, string vSet_name, string vLink_name, DateTime srcdatetime)
            {
                this.xmno = xmno;
                this.datatype = datatype;
                this.point_name = point_name;
                this.dt = dt;
                
                this.vSet_name = vSet_name;
                this.vLink_name = vLink_name;
                this.srcdatetime = srcdatetime;
            }
        }
        //public bool ProcessSurveyPointTimeDataAdd(ProcessSurveyPointTimeDataAddModel model, out string mssg)
        //{
        //    return bll.SurveyPointTimeDataAdd(model.xmno,model.datatype ,  model.pointname, model.time, model.importtime, out mssg);
        //}
        public class ProcessSurveyPointTimeDataAddModel
        {
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string pointname { get; set; }
            public DateTime time { get; set; }
            public DateTime importtime { get; set; }
            public ProcessSurveyPointTimeDataAddModel(int xmno, data.Model.gtsensortype datatype, string pointname, DateTime time, DateTime importtime)
            {
                this.xmno = xmno;
                this.datatype = datatype;
                this.pointname = pointname;
                this.time = time;
                this.importtime = importtime;
            }

        }
        public bool DeleteSurveyData(DeleteModel model, out string mssg)
        {
            return bll.DeleteSurveyData(model.xmno,model.datatype , model.startcyc,  model.endcyc, out mssg);

        }
        public class DeleteModel
        {
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public int startcyc { get; set; }
            public int endcyc { get; set; }
            public DeleteModel(int xmno,data.Model.gtsensortype datatype, int startcyc,int endcyc)
            {
                this.xmno = xmno;
                this.datatype = datatype;
                this.startcyc = startcyc;
                this.endcyc = endcyc;
                this.datatype = datatype;
            }
        }
        public bool ProcessSurveyResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.PointNameSurveyDateTimeListGet(model.xmno,model.datatype , model.pointname,  out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessSurveySurfaceDataTimeCycLoad(ProcessSurfaceDataTimeCycLoadModel model, out string mssg)
        {
            List<string> cyctimelist = null;
            mssg = "";
            if (bll.SurveyCYCDateTimeListGet(model.xmno,model.datatype , out cyctimelist, out mssg))
            {
                model.cyctimelist = cyctimelist;
                return true;
            }
            return false;
        }
       

        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool SurveyResultdataTableLoad(GTSurfaceDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.SurveyResultdataTableLoad(model.pageIndex, model.rows, model.xmno,model.datatype , model.pointname, model.sord,  model.startcyc, model.endcyc, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 编辑库结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool SurveyResultdataTableRecordsCount(GTSurfaceDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.SurveyResultTableRowsCount(model.xmno,model.datatype , model.pointname, model.startcyc, model.endcyc, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }

        public class GTSurfaceDataLoadCondition
        {
    
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 起始页号
            /// </summary>
            public int pageIndex { get; set; }
            /// <summary>
            /// 每页行数
            /// </summary>
            public int rows { get; set; }

            public int startcyc  { get; set; }

            public int endcyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string pointname { get; set; }
            /// <summary>
            /// 结果数据表
            /// </summary>
            public DataTable dt { get; set; }
            /// <summary>
            /// 排序字段
            /// </summary>
            public string sord { get; set; }
            public GTSurfaceDataLoadCondition(int xmno,data.Model.gtsensortype datatype,string pointname,int pageIndex, int rows, string sord, int startcyc, int endcyc)
            {
                this.xmno = xmno;this.datatype = datatype; 
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.startcyc = startcyc;
                this.endcyc = endcyc;
                this.pointname = pointname;

            }
            public GTSurfaceDataLoadCondition()
            {

            }
        }

        public class GTSurfaceDataCountLoadCondition
        {

            public string sord { get; set; }
            public int xmno { get; set; }
            public string pointname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public int startcyc { get; set; }
            public int endcyc { get; set; }
            public string totalCont { get; set; }
            public GTSurfaceDataCountLoadCondition(int xmno,data.Model.gtsensortype datatype, string pointname,  int startcyc, int endcyc)
            {
                this.xmno = xmno;this.datatype = datatype;
                this.pointname = pointname;
                this.startcyc = startcyc;
                this.endcyc = endcyc;
            }
            public GTSurfaceDataCountLoadCondition( )
            {
                
            }
        }
    

    }
}