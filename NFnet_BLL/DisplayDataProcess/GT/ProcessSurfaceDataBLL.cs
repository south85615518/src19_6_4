﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation;
using System.Data;
using TotalStation.BLL.fmos_obj;
using SqlHelpers;
using NFnet_BLL.Other;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using Tool;


namespace NFnet_BLL.DataProcess
{

    /// <summary>
    /// 结果数据业务逻辑处理类
    /// </summary>
    public partial class ProcessSurfaceDataBLL
    {

        public static data.BLL.gtsurfacedata bll = new data.BLL.gtsurfacedata();
        
        public bool ProcessResultDataAdd(global::data.Model.gtsurfacedata model,out string mssg)
        {
            return bll.Add(model,out mssg);
            
        }



        public bool ProcessResultDataUpdate(global::data.Model.gtsurfacedata model,out string mssg)
        {
            return bll.Update(model,out mssg);
        }

        public bool ProcessgtSurfacedataCYCUpdate(ProcessgtSurfacedataCYCUpdateModel model,out string mssg)
        {
            return bll.gtsurfacedatacycupdate(model.xmno,model.datatype,model.surfacename,model.dt,out mssg);
        }
        public class ProcessgtSurfacedataCYCUpdateModel
        {
            public int xmno { get; set; }
            public string surfacename{get;set;}
            public DateTime dt { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public ProcessgtSurfacedataCYCUpdateModel(int xmno, string surfacename, data.Model.gtsensortype datatype, DateTime dt)
            {
                this.xmno = xmno;
                this.surfacename = surfacename;
                this.dt = dt;
                this.datatype = datatype;
            }
        }

        public bool ProcessSurveyDataUpdata(global::data.Model.gtsurfacedata model, out string mssg)
        {
            return bll.UpdataSurveySurfaceData(model, out mssg);
        }
        public bool ProcessSurveyDataNextCYCThisUpdate(global::data.Model.gtsurfacedata model, out string mssg)
        {
            return bll.UpdataSurveySurfaceDataNextCYCThis(model, out mssg);
        }

        public bool ProcessLostPoints(ProcessLostPointsModel model, out string mssg)
        {
            mssg = "";
            string pointstr = "";
            if (bll.LostPoints(model.xmno,model.datatype  ,model.cyc, out pointstr, out mssg))
            {
                model.pointstr = pointstr;
                return true;
            }
            return false;

        }
        public class ProcessLostPointsModel
        {
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public int cyc { get; set; }
            public string pointstr { get; set; }
            public ProcessLostPointsModel( int xmno,data.Model.gtsensortype datatype, int cyc)
            {
                this.cyc = cyc;
                this.xmno = xmno;
                this.pointstr = pointstr;
                this.datatype = datatype;
            }
        }

        public bool DeleteData(DeleteModel model, out string mssg)
        {
            return bll.DeleteData(model.xmno,model.datatype ,model.startcyc, model.endcyc, out mssg);

        }
        /// <summary>
        /// 结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.ResultTableRowsCount(model.xmno,model.datatype,model.pointname ,model.startTime,model.endTime,out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
       
        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataLoad(GTSurfaceDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ResultdataTableLoad(model.pageIndex, model.rows, model.xmno,model.datatype ,model.pointname,model.sord ,model.startcyc,model.endcyc,out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool ProcessReportDataView(ProcessReportDataViewModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ReportDataView(model.cyclist, model.pageIndex, model.rows, model.xmno,model.datatype,model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessReportDataViewModel : GTSurfaceDataLoadCondition
        {
            public List<string> cyclist { get; set; }
            public ProcessReportDataViewModel(List<string> cyclist,data.Model.gtsensortype datatype ,int pageIndex, int pageSize, int xmno,  string sord)
            {
                this.cyclist = cyclist;
                this.pageIndex = pageIndex;
                this.rows = pageSize;
                this.xmno = xmno;
                this.sord = sord;
                this.datatype = datatype;
            }
        }



        public bool ProcessSurfaceDataTimeCycLoad(ProcessSurfaceDataTimeCycLoadModel model, out string mssg)
        {
            List<string> cyctimelist = null;
            mssg = "";
            if (bll.CYCDateTimeListGet(model.xmno,model.datatype , out cyctimelist, out mssg))
            {
                model.cyctimelist = cyctimelist;
                return true;
            }
            return false;
        }
        public class ProcessSurfaceDataTimeCycLoadModel
        {
            public List<string> cyctimelist { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public int xmno { get; set; }
            public ProcessSurfaceDataTimeCycLoadModel(int xmno, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;
            }

        }

        public bool ProcessResultDataTimeListLoad(ProcessSurfaceDataBLL.ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.PointNameDateTimeListGet(model.xmno,model.datatype, model.pointname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public class ProcessResultDataTimeListLoadModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public List<string> ls { get; set; }
            public ProcessResultDataTimeListLoadModel(int xmno, string pointname, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.pointname = pointname;
                this.datatype = datatype;
            }
           
        }
        public bool ProcessSurfaceDataMaxTime(ProcessSurfaceDataMaxTimeModel model,out string mssg)
        {
            DateTime dt = new DateTime();
            if (bll.MaxTime(model.xmno,model.datatype ,out dt,out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessSurfaceDataMaxTimeModel
        {
            public int xmno { get; set; }
            public DateTime dt { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public ProcessSurfaceDataMaxTimeModel(int xmno, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;
            }
        }
        //public bool ProcessXmStateTableLoad(XmStateTableLoadCondition model,out string mssg)
        //{
        //    DataTable dt = new DataTable();
        //    if (bll.XmStateTable(model.pageIndex, model.rows, model.xmno,model.xmname, model.unitname, model.colName, model.sord, out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    return false;
        //}
        /// <summary>
        /// 结果数据报表数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        //public bool ProcessResultDataReportTableCreate(ResultDataReportTableCreateCondition model, out string mssg)
        //{
        //    DataTable dt = null;
        //    if (bll.ResultDataReportPrint(model.sql, model.xmno, out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        
      
        /// <summary>
        /// 数据展示日期查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {

            switch (model.type)
            {
                case QueryType.RQCX:
                    string sqlsttm = "";
                    string sqledtm = "";
                    string mssg = "";
                    string cycmin = "";
                    string cycmax = "";
                    var querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.startTime, ">=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqlsttm = querynvlmodel.str;
                    }
                    querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.endTime, "<=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqledtm = querynvlmodel.str;
                    }
                    string rqConditionStr = sqlsttm + " and " + sqledtm + " order by #_point,#_date asc ";
                    string sqlmin = "select min(time) as mincyc from gtsurfacedata where xmno = '" + model.xmno + "' and  datatype = "+Convert.ToInt32(model.datatype);
                    string sqlmax = "select max(time) as maxcyc from gtsurfacedata where xmno = '" + model.xmno + "' datatype = "+Convert.ToInt32(model.datatype);
                    var processquerystanderstrModel = new QuerystanderstrModel(sqlmin, model.xmno);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                       model.startTime = processquerystanderstrModel.str;
                    }
                    processquerystanderstrModel = new QuerystanderstrModel(sqlmax, model.xmno);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                       model.endTime = processquerystanderstrModel.str;
                    }
                    //model.minCyc = cycmin;
                    //model.maxCyc = cycmax;
                    model.sql = rqConditionStr;
                    break;
                case QueryType.ZQCX:
                    rqConditionStr = "#_cyc >= " + model.startTime + "   and  #_cyc <= " + model.endTime + " order by #_point,cyc,#_date asc ";
                    model.minCyc = model.startTime;
                    model.maxCyc = model.endTime;

                    model.sql = rqConditionStr;
                    break;
                case QueryType.QT:
                    var processdateswdlModel = new ProcessComBLL.ProcessdateswdlModel("#_date", model.unit,model.maxTime);
                    if (ProcessComBLL.Processdateswdl(processdateswdlModel, out mssg))
                    {
                        model.startTime = processdateswdlModel.sttm;
                        model.endTime = processdateswdlModel.edtm;
                        //model.sql = processdateswdlModel.sql;
                    }
                    break;


            }
        }
       


        public bool ProcessgtsurfacedataMaxTime(SenorMaxTimeCondition model,out string mssg )
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(model.xmno,model.datatype ,out maxTime,out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;

                
        }

        public bool SurfaceDataSameTimeModel(ProcessGetLastTimeModelGetModel model, out string mssg)
        {
            var gtsensordatamodel = new data.Model.gtsurfacedata();
            if (bll.SurfaceDataSameTimeModel(model.xmno,model.datatype ,model.pointname, model.dt, out gtsensordatamodel, out mssg))
            {
                model.model = gtsensordatamodel;
                return true;
            }
            return false;
        }

        public bool SurfaceDataLastTimeModel(ProcessGetLastTimeModelGetModel model, out string mssg)
        {
            var gtsensordatamodel = new data.Model.gtsurfacedata();
            if (bll.GetLastTimeModel(model.xmno,model.datatype ,model.pointname, model.dt, out gtsensordatamodel, out mssg))
            {
                model.model = gtsensordatamodel;
                return true;
            }
            return false;
        }

        public class ProcessGetLastTimeModelGetModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public DateTime dt { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public data.Model.gtsurfacedata model { get; set; }
            public ProcessGetLastTimeModelGetModel(int xmno,data.Model.gtsensortype datatype, string pointname, DateTime dt)
            {
                this.xmno = xmno;
                this.datatype = datatype;
                this.pointname = pointname;
                this.dt = dt;
            }
          
        }


        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessfillTotalStationDbFill(FillTotalStationDbFillCondition model)
        {
            try
            {
                model.rqConditionStr = model.rqConditionStr.Replace("#_date", "time");
                model.rqConditionStr = model.rqConditionStr.Replace("#_point", "surfacename");
                model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
                string mssg = "";
                DataTable dt = null;
                var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel("  surfacename   ", model.pointname, "   in   ", "(", ")");
                string sql = "";
                if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
                {
                    sql = "select  surfacename,cyc,SteelChordVal,round(this_val,0) as this_val,round(ac_val,0) as ac_val,calculatepointsCont,time    from  gtsurfacedata where  xmno = '" + model.xmno + "'  and  datatype = "+Convert.ToInt32(model.datatype)+"  and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
                    sql += " and " + model.rqConditionStr;
                }
                model.sql = sql;
                dt = new DataTable();
                if (GetResultDataTable(sql, model.xmno, out dt, out mssg))
                {
                    model.dt = dt;
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("混凝土支撑内力数据加载出错，错误信息:"+ex.Message);
                return false;
            }



          


        }
        
        public   bool   GetResultDataTable(string sql,int xmno,out DataTable dt,out string mssg)
        {
            dt = null;
            mssg = "";
            var processquerystanderdbModel = new QuerystanderdbIntModel(sql, xmno);
            if (ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
            {
                dt = processquerystanderdbModel.dt;
                return true;
            }
            return false;
        }



        //根据表的分页单位获取当前记录在表中的页数
        public int PageIndexFromTab(string pointname, string date, DataView dv, string pointnameStr, string dateStr, int pageSize)
        {

            //DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr];
                if (drv[pointnameStr].ToString() == pointname && date.Trim() == drv[dateStr].ToString())
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }

        //public bool ProcessResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out         List<global::data.Model.gtsurfacedata> modellist, out string mssg)
        //{
        //    modellist = new List<global::data.Model.gtsurfacedata>();
        //    if (bll.(model.xmno, out modellist, out mssg))
        //    {
        //        model.modellist = modellist;
        //        return true;
        //    }
        //    return false;
        //}

        public class ProcessResultDataAlarmModelListModel
        {
            public int xmno { get; set; }
            public List<global::data.Model.gtsurfacedata> modellist { set; get; }
            public ProcessResultDataAlarmModelListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }


        

       
        /// <summary>
        /// 点名列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcesssurfacenamePointLoad(SensorPointLoadCondition model, out string mssg)
        {
            List<string> ls = null;
            if (bll.dataPointLoadBLL(model.xmno,model.datatype ,out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }


       //public bool GetAlarmTableCont(GetAlarmTableContModel model, out string mssg)
       // {
       //     int cont = 0;
       //     if (bll.GetAlarmTableCont(model.pointnamelist, out cont, out mssg))
       //     {
       //         model.cont = cont;
       //         return true;
       //     }
       //     return false;
       // }
       // public class GetAlarmTableContModel
       // {
       //     public List<string> pointnamelist { get; set; }
       //     public int cont { get; set; }

       //     public GetAlarmTableContModel( List<string> pointnamelist)
       //     {
       //         this.pointnamelist = pointnamelist;

       //     }
       // }
        public bool ProcessPointNewestDateTimeGet(PointNewestDateTimeCondition model,out string mssg)
        {
            DateTime dt = new DateTime();
            if (bll.PointNewestDateTimeGet(model.xmno,model.datatype ,model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }

        public bool ProcesssurfacedataResultDataMaxTime(GTSurfaceTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(model.xmno,model.datatype ,model.pointname, out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            mssg = "";
            return false;


        }
        public bool ProcesssurfacedataResultDataMinTime(GTSurfaceTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.MinTime(model.xmno,model.datatype ,model.pointname, out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            mssg = "";
            return false;


        }
        public bool ProcesssurfacedataModelListLoad(ProcesssurfacedataModelListLoadModel model,out string mssg)
        {
            List<data.Model.gtsurfacedata> modellist = new List<data.Model.gtsurfacedata>();
            if (bll.GetModelList(model.xmno,model.datatype ,out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class ProcesssurfacedataModelListLoadModel
        {
            public int xmno{ get; set;}
            public List<data.Model.gtsurfacedata> modellist{get;set;}
            public data.Model.gtsensortype datatype { get; set; }
            public ProcesssurfacedataModelListLoadModel(int xmno, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;
            }
          
        }


        public bool ProcessLackPointCalculate(ProcessLackPointCalculateModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.LackPointCalculate(model.xmno,model.datatype ,out  dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessLackPointCalculateModel
        {
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public DataTable dt { get; set; }
            public ProcessLackPointCalculateModel(int xmno, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;
            }
        }

        public bool CycSort(CycSortModel model, out string mssg)
        {
            return bll.CycSort(model.xmno,model.datatype,out mssg);
        }
        public class CycSortModel
        {
 public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public CycSortModel(int xmno, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;
            }
        }

    }
}