﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.XmInfo.pub;
using NFnet_BLL.AuthorityAlarmProcess;
using NFnet_BLL.DataProcess;
using System.Data;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    public partial class ProcessPointAlarmBLL
    {
        public global::data.BLL.gtpointalarmvalue bll = new global::data.BLL.gtpointalarmvalue();
        public static ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public static ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public static ProcessalarmsplitondateBLL splitOnDateBLL = new ProcessalarmsplitondateBLL();
        public static int sec = 0;
        /// <summary>
        /// 点号预警表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointAlarmRecordsCount(ProcessAlarmRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (bll.PointAlarmValueTableRowsCount(model.searchString, model.xmno, model.datatype,out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点号预警记录数获取类
        /// </summary>
        public class ProcessAlarmRecordsCountModel : SearchCondition
        {
            public string totalCont { get; set; }
            public int xmno { get; set; }
            public global::data.Model.gtsensortype datatype{ get; set;}
            public string searchString { get; set; }
            public ProcessAlarmRecordsCountModel(string xmname, int xmno, string searchString)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.searchString = searchString;
            }
            public ProcessAlarmRecordsCountModel(string xmname, int xmno, data.Model.gtsensortype datatype, string searchString)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.searchString = searchString;
                this.datatype = datatype;
            }
            public ProcessAlarmRecordsCountModel(int xmno, data.Model.gtsensortype datatype, string searchString)
            {
                this.xmno = xmno;
                this.searchString = searchString;
                this.datatype = datatype;
            }
        }
        /// <summary>
        /// #传感器名称#点号表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.PointTableLoad(model.searchString, model.pageIndex, model.rows, model.xmno,model.colName, model.sord , model.datatype ,out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 水位点号表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessGTWaterLinePointLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.PointGTWaterLineTableLoad(model.searchString, model.pageIndex, model.rows, model.xmno, model.colName, model.sord, model.datatype, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 支撑轴力点号表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessGTSupportingAxialForcePointLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.PointGTSupportingAxialForceLoad(model.searchString, model.pageIndex, model.rows, model.xmno, model.colName, model.sord, model.datatype, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }

        /// <summary>
        /// 点号预警表获取类
        /// </summary>
        public class ProcessAlarmLoadModel : SearchCondition
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 搜索条件
            /// </summary>
            public string searchString { get; set; }
            /// <summary>
            /// 点号预警表
            /// </summary>
            public DataTable dt { get; set; }
            public global::data.Model.gtsensortype datatype{ get; set;}
            public ProcessAlarmLoadModel( int xmno, string searchString, string colName, int pageIndex, int rows, string sord, global::data.Model.gtsensortype datatype)
            {
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.searchString = searchString;
                this.xmno = xmno;
                this.datatype = datatype;

            }
        }
        /// <summary>
        /// 点号是否存在
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmExist(global::data.Model.gtpointalarmvalue model, out string mssg)
        {
            return bll.Exist(model, out mssg);
        }
        /// <summary>
        /// 点号预警编辑
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmEdit(global::data.Model.gtpointalarmvalue model, out string mssg)
        {
            return bll.Update(model, out mssg);
        }
        /// <summary>
        /// 点号预警编辑
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessUpdateInitDegree(global::data.Model.gtpointalarmvalue model, out string mssg)
        {
            return bll.UpdateInitDegree(model, out mssg);
        }
        /// <summary>
        /// 点号预警添加
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmAdd(global::data.Model.gtpointalarmvalue model, out string mssg)
        {
            if (!ProcessAlarmExist(model,out mssg))
            return bll.Add(model, out mssg);
            return true;
        }

        public bool  ProcessAlarm(global::data.Model.gtpointalarmvalue model, out string mssg)
        {
            mssg = "";
            if (!ProcessAlarmExist(model, out mssg))
            {
               return  ProcessAlarmAdd(model, out mssg);
            }
            else
            {
              return  ProcessAlarmEdit(model, out mssg);
            }
        }
        /// <summary>
        /// 点号预警批量更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmMultiUpdate(ProcessAlarmMultiUpdateModel model, out string mssg)
        {
            return bll.MultiUpdate(model.pointstr, model.model, out mssg);
        }


        public bool ProcessSurfaceMultiUpdate(ProcessAlarmMultiUpdateModel model, out string mssg)
        {
            return bll.SurfaceMultiUpdate(model.pointstr, model.model, out mssg);
        }
        /// <summary>
        /// 点号预警删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmDel(global::data.Model.gtpointalarmvalue model, out string mssg)
        {
            return bll.Delete(model, out mssg);
        }
        public class ProcessAlarmDelModel
        {
            public string xmname { get; set; }
            public string name { get; set; }
            public ProcessAlarmDelModel(string xmname,string name)
            {
                this.xmname = xmname;
                this.name = name;
            }
        }
        public bool GTSensorDataPointLoad(GTSensorDataPointLoadModel model,out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.GTSensorDataPointLoadDAL(model.xmno, model.datatype, out ls, out mssg))
            { 
                model.ls = ls;
                return true;
            }
            return false;

        }
        public class GTSensorDataPointLoadModel
        {
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public List<string> ls { get; set; }
            public GTSensorDataPointLoadModel(int xmno, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;
            }
        }
        /// <summary>
        /// 点号预警批量更新类
        /// </summary>
        public class ProcessAlarmMultiUpdateModel
        {
            /// <summary>
            /// 点组
            /// </summary>
            public string pointstr { get; set; }
            /// <summary>
            /// 点号预警类
            /// </summary>
            public global::data.Model.gtpointalarmvalue model { get; set; }
            public ProcessAlarmMultiUpdateModel(string pointstr, global::data.Model.gtpointalarmvalue model)
            {
                this.pointstr = pointstr;
                this.model = model;
            }
        }

        /// <summary>
        /// 结果数据对象预警处理
        /// </summary>
        /// <param name="alarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessSinglePointAlarmIntoInformation(global::data.Model.gtalarmvalue alarmvalue, global::data.Model.gtsensordata resultModel, out List<string> lscontext)
        {
            lscontext = new List<string>();
            //List<string>  ls = new List<string>();
            List<string> listspace = new List<string>();
            bool result = false;
            bool hasalarm = false;
            listspace.Add(string.Format("{0},{1}," + resultModel.datatype + " :{2},第{3}周期", resultModel.point_name, resultModel.time.ToString(), resultModel.sec_ac_scalarvalue, resultModel.cyc));

            DataProcessHelper.RangeCompareU("", "" + resultModel.datatype + "上限", resultModel.single_this_scalarvalue, alarmvalue.single_this_scalarvalue_u, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}本次{1}", resultModel.datatype, resultModel.single_this_scalarvalue));

            DataProcessHelper.RangeCompareL("", "" + resultModel.datatype + "下限", resultModel.single_this_scalarvalue, alarmvalue.single_this_scalarvalue_l, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}本次{1}", resultModel.datatype, resultModel.single_this_scalarvalue));

            DataProcessHelper.RangeCompareU("", "" + resultModel.datatype + "上限", resultModel.single_ac_scalarvalue, alarmvalue.single_ac_scalarvalue_u, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}累计{1}", resultModel.datatype, resultModel.single_ac_scalarvalue));

            DataProcessHelper.RangeCompareL("", "" + resultModel.datatype + "下限", resultModel.single_ac_scalarvalue, alarmvalue.single_ac_scalarvalue_l, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
                listspace.Add(string.Format("{0}累计{1}", resultModel.datatype, resultModel.single_ac_scalarvalue));

            lscontext.Add(string.Join(" ", listspace));
            return hasalarm;
        }

        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessSinglePointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            List<string> ls = new List<string>();
            string mssg = "";
            if (model.resultModel.point_name == "GY-YB-3")
            { 
                string a = "";
            }
            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.point_name,
                time = model.resultModel.time,
                type = model.resultModel.datatype,
                atime = DateTime.Now,
                pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processCgAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, model.resultModel.datatype, model.resultModel.point_name, model.resultModel.time);
            splitOnDateBLL.ProcessalarmsplitondateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);

            AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = model.resultModel.datatype,
                pointName = model.resultModel.point_name,
                xmno = model.xmno,
                time = model.resultModel.time,
                adate = DateTime.Now,
                cyc = model.resultModel.cyc

            };
            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessSinglePointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add("三级预警");
                    pointCheck.alarm = 3;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, model.resultModel.datatype, "", model.resultModel.point_name, 3, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessSinglePointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add("二级预警");
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, model.resultModel.datatype, "", model.resultModel.point_name, 2, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessSinglePointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add("一级预警");
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, model.resultModel.datatype, "", model.resultModel.point_name, 1, out mssg);
                    return true;
                }


            }
            ls.Add("========预警解除==========");
            pointCheck.alarm = 0;
            pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
            ProcessHotPotColorUpdate(model.xmno, model.resultModel.datatype, "", model.resultModel.point_name, 0, out mssg);
            return false;

        }
        public class ProcessPointAlarmfilterInformationModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public List<global::data.Model.gtalarmvalue> levelalarmvalue { get; set; }
            public global::data.Model.gtsensordata resultModel { get; set; }
            public List<string> ls { get; set; }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<global::data.Model.gtalarmvalue> levelalarmvalue, global::data.Model.gtsensordata resultModel, int xmno)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
                this.xmno = xmno;
            }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<global::data.Model.gtalarmvalue> levelalarmvalue, global::data.Model.gtsensordata resultModel)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
            }
            public ProcessPointAlarmfilterInformationModel()
            {
               
            }
        }
        public class AlarmRecord
        {
            public string pointName { get; set; }
            public string Time { get; set; }
            public string alarm { get; set; }
            public string RecordTime { get; set; }

        }

        public bool ProcessPointAlarmModelGet(ProcessPointAlarmModelGetModel model, out string mssg)
        {
            global::data.Model.gtpointalarmvalue pointalarm = new global::data.Model.gtpointalarmvalue();
            if (bll.GetModel(model.xmno, model.pointName,model.datatype ,out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }

        public bool ProcessPointAlarmModelListGet(ProcessPointAlarmModelListGetModel model, out string mssg)
        {
            List<global::data.Model.gtpointalarmvalue> pointalarmlist = new List<global::data.Model.gtpointalarmvalue>();
            if (bll.GetModelList(model.xmno, model.datatype, out pointalarmlist, out mssg))
            {
                model.modellist = pointalarmlist;
                return true;
            }
            return false;
        }
        public class ProcessPointAlarmModelListGetModel
        {
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public List<global::data.Model.gtpointalarmvalue> modellist { get; set; }
            public ProcessPointAlarmModelListGetModel(int xmno,  data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;
            }
          
        }


        public bool ProcessSensornoAlarmModelGet(ProcessPointAlarmModelGetModel model, out string mssg)
        {
            global::data.Model.gtpointalarmvalue pointalarm = new global::data.Model.gtpointalarmvalue();
            if (bll.GetSensornoModel(model.xmno, model.pointName, out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }
        public class ProcessPointAlarmModelGetModel
        {
            public int xmno { get; set; }
            public string pointName { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public global::data.Model.gtpointalarmvalue model { get; set; }
            public ProcessPointAlarmModelGetModel(int xmno, string pointName, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.pointName = pointName;
                this.datatype = datatype;
            }
            public ProcessPointAlarmModelGetModel(int xmno, string pointName)
            {
                this.xmno = xmno;
                this.pointName = pointName;
                this.datatype = datatype;
            }
        }
        
        public bool ProcessHotPotColorUpdate(int xmno,string jclx,string jcoption,string pointName,int color,out string mssg)
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(xmno, jclx, jcoption, pointName, color);
            return layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);
            
        }

    }
}