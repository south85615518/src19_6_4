﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.GT
{

    /// <summary>
    /// 结果数据业务逻辑处理类
    /// </summary>
    public partial class ProcessResultDataBLL
    {

       

        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ThirdScalarResultdataTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ThirdScalarResultdataTableLoad(model.pageIndex, model.rows, model.xmname, model.pointname, model.sord, model.datatype, model.starttime, model.endtime, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
           

        }

     
       


       
    }
}
