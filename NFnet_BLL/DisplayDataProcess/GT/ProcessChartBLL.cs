﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;
using NFnet_BLL.Other;
using System.Data;
using NFnet_BLL.DataProcess;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    /// <summary>
    /// 水位曲线业务逻辑处理类
    /// </summary>
    public partial class ProcessGTSensorDataChartBLL
    {
        /// <summary>
        /// 水位曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrGTSensorData_cyc(ProcessChartCondition model, out string mssg)
        {

            mssg = "";
            string xmname = model.xmname;
            DataTable dt = new DataTable();
            string mkhs = model.pointname;
            string sql = model.sql;

            ProcessResultDataBLL processGTResultDataBLL = new ProcessResultDataBLL();


            //分模块号显示
            if (sql == "") return false;

            if (!processGTResultDataBLL.GetResultDataTable(sql, model.xmname, out dt, out mssg)) return false;
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_cyc> lst = CreateSeriesFromData_cyc(dt, mkhs);
                model.serie_cyc = lst;
            }//如果mkh为空默认只显示过程
            return true;
        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">水位点名</param>
        /// <returns></returns>
        public static List<serie_cyc> CreateSeriesFromData_cyc(DataTable dt, string mkhs)
        {
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_cyc> lst = new List<serie_cyc>();

                omkhs[] omkharr = new omkhs[mkharr.Length];
                for (int t = 0; t < mkharr.Length; t++)
                {
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "point_name=" + mkharr[t];
                    int nu = dv.Count;
                    string rri = mkharr[t];


                    //sr = new serie();
                    serie_cyc st_scalarvalue = new serie_cyc();//创建曲线
                    st_scalarvalue.Stype = "测量值";//曲线类别
                    st_scalarvalue.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_scalarvalue, "first_ac_scalarvalue");
                    lst.Add(st_scalarvalue);
                    serie_cyc st_scalarvalue_degree = new serie_cyc();//创建曲线
                    st_scalarvalue_degree.Stype = "温度";//曲线类别
                    st_scalarvalue_degree.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_scalarvalue_degree, "sec_oregion_scalarvalue");
                    lst.Add(st_scalarvalue_degree);
                    serie_cyc st_this = new serie_cyc();//创建曲线
                    st_this.Stype = "本次变化量";//曲线类别
                    st_this.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_this, "single_this_scalarvalue");
                    lst.Add(st_this);
                    serie_cyc st_this_degree = new serie_cyc();//创建曲线
                    st_this_degree.Stype = "本次温度";//曲线类别
                    st_this_degree.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_this_degree, "sec_oregion_scalarvalue");
                    lst.Add(st_this_degree);
                    serie_cyc st_ac = new serie_cyc();//创建曲线
                    st_ac.Stype = "累计变化量";//曲线类别
                    st_ac.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_ac, "single_ac_scalarvalue");
                    lst.Add(st_ac);
                    serie_cyc st_ac_degree = new serie_cyc();//创建曲线
                    st_ac_degree.Stype = "累计温度";//曲线类别
                    st_ac_degree.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_ac_degree, "sec_oregion_scalarvalue");
                    lst.Add(st_ac_degree);

                }


                return lst;
            }
            return null;
        }
        /// <summary>
        /// 水位数据生成曲线
        /// </summary>
        /// <param name="dv">数据表视图</param>
        /// <param name="sr">曲线</param>
        /// <param name="valstr">值字段名称</param>
        public static void formatdat(DataView dv, serie_cyc sr, string valstr)
        {

            sr.Pts = new pt_cyc[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string cyc = drv["cyc"].ToString();
                string v = drv[valstr].ToString();
                //DateTime d = Convert.ToDateTime(sj);
                //int year = d.Year;
                //int mon = d.Month;
                //int day = d.Day;
                //int hour = d.Hour;
                //int minute = d.Minute;
                //int second = d.Second;

                sr.Pts[i] = new pt_cyc { Pt_x = string.Format("第{0}周期 {1}", cyc, drv["time"].ToString()), Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
        public static void formatdat(DataView dv, serie sr, string valstr)
        {

            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["time"].ToString();
                string v = drv[valstr].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt {  Dt  =  d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
        #region 面

        /// <summary>
        /// 水位曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        //public static bool ProcessSerializestrGTSurfaceData(ProcessChartCondition model, out string mssg)
        //{

        //    mssg = "";
        //    string xmname = model.xmname;
        //    DataTable dt = new DataTable();
        //    string mkhs = model.pointname;
        //    string sql = model.sql;

        //    ProcessSurfaceDataBLL processGTResultDataBLL = new ProcessSurfaceDataBLL();


        //    //分模块号显示
        //    if (sql == "") return false;
        //    Tool.ExceptionLog.ExceptionWrite(sql + "," + model.xmno);
        //    if (!processGTResultDataBLL.GetResultDataTable(sql, model.xmno, out dt, out mssg)) return false;
        //    string[] mkharr = mkhs.Split(',');

        //    if (mkhs != "" && mkhs != null)
        //    {
        //        List<serie> lst = CreateSurfaceSeriesFromData(dt, mkhs);
        //        model.series = lst;
        //    }//如果mkh为空默认只显示过程
        //    return true;
        //}
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">水位点名</param>
        /// <returns></returns>
        //public static List<serie> CreateSurfaceSeriesFromData(DataTable dt, string mkhs)
        //{
        //    string[] mkharr = mkhs.Split(',');

        //    if (mkhs != "" && mkhs != null)
        //    {
        //        List<serie> lst = new List<serie>();

        //        omkhs[] omkharr = new omkhs[mkharr.Length];
        //        for (int t = 0; t < mkharr.Length; t++)
        //        {
        //            serie sr = new serie();
        //            serie sr_degree = new serie();
        //            serie srthis = new serie();
        //            serie srthis_degree = new serie();
        //            serie srac = new serie();
        //            serie srac_srdegree = new serie();
        //            DataView dv = new DataView(dt);
        //            dv.RowFilter = "surfacename=" + mkharr[t];
        //            int nu = dv.Count;
        //            string rri = mkharr[t];

        //            //sr.Stype = "标量值";
        //            //sr.Name = rri.Replace("'", "");
        //            ////加载点
        //            //formatdat(dv, sr, "first_oregion_scalarvalue");
        //            //lst.Add(sr);
        //            //sr = new serie();
        //            srthis.Stype = "本次变化量";
        //            srthis.Name = rri.Replace("'", "");
        //            //加载点
        //            formatdat(dv, srthis, "this_val");
        //            lst.Add(srthis);
        //            srac.Stype = "累计变化量";
        //            srac.Name = rri.Replace("'", "");
        //            //加载点
        //            formatdat(dv, srac, "ac_val");
        //            lst.Add(srac);
        //            srdegree.Stype = "温度";
        //            srdegree.Name = rri.Replace("'", "");
        //            //加载点
        //            formatdat(dv, srdegree, "SteelChordVal");
        //            lst.Add(srdegree);

        //        }


        //        return lst;
        //    }
        //    return null;
        //}

        public static bool ProcessSerializestrGTSurfaceSensorData_cyc(ProcessChartCondition model, out string mssg)
        {

            mssg = "";
            string xmname = model.xmname;
            DataTable dt = new DataTable();
            string mkhs = model.pointname;
            string sql = model.sql;

            ProcessSurfaceDataBLL processSurfaceDataBLL = new ProcessSurfaceDataBLL();


            //分模块号显示
            if (sql == "") return false;

            if (!processSurfaceDataBLL.GetResultDataTable(sql, model.xmno, out dt, out mssg)) return false;
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_cyc> lst = CreategtSurfaceSeriesFromData_cyc(dt, mkhs);
                model.serie_cyc = lst;
            }//如果mkh为空默认只显示过程
            return true;
        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">水位点名</param>
        /// <returns></returns>
        public static List<serie_cyc> CreategtSurfaceSeriesFromData_cyc(DataTable dt, string mkhs)
        {
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie_cyc> lst = new List<serie_cyc>();

                omkhs[] omkharr = new omkhs[mkharr.Length];
                for (int t = 0; t < mkharr.Length; t++)
                {
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "surfacename=" + mkharr[t];
                    int nu = dv.Count;
                    string rri = mkharr[t];


                    //sr = new serie();
                    serie_cyc st_this = new serie_cyc();//创建曲线
                    st_this.Stype = "本次变化量";//曲线类别
                    st_this.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_this, "this_val");
                    lst.Add(st_this);
                    serie_cyc st_this_degree = new serie_cyc();//创建曲线
                    st_this_degree.Stype = "本次温度";//曲线类别
                    st_this_degree.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_this_degree, "SteelChordVal");
                    lst.Add(st_this_degree);
                    serie_cyc st_ac = new serie_cyc();//创建曲线
                    st_ac.Stype = "累计变化量";//曲线类别
                    st_ac.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_ac, "ac_val");
                    lst.Add(st_ac);
                    serie_cyc st_ac_degree = new serie_cyc();//创建曲线
                    st_ac_degree.Stype = "累计温度";//曲线类别
                    st_ac_degree.Name = mkharr[t].Replace("'", "");
                    formatdat(dv, st_ac_degree, "SteelChordVal");
                    lst.Add(st_ac_degree);

                }


                return lst;
            }
            return null;
        }


        /// <summary>
        /// 以点名作为横轴的曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrgtsurfacedata_point(ProcessChartCondition model, out string mssg)
        {
            int seriescont = 0;
            mssg = "";
            try
            {

                DataTable dt = new DataTable();
                DataTable dtt = new DataTable();
                string sql = model.sql;
                string pointname = model.pointname;
                string xmname = model.xmname;
                if (pointname != "" && pointname != null)
                {

                    var processquerystanderdbModel = new QuerystanderdbIntModel(sql, model.xmno);
                    if (!ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
                    {
                        //错误信息反馈
                    }
                    dt = processquerystanderdbModel.dt;
                    DataView dvdefault = new DataView(dt);
                    DataTable dtdistinct = dvdefault.ToTable(true, "cyc");
                    //从表中筛选出周期
                    List<string> lscyc = CycListGet(dtdistinct);

                    //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                    //string[] pointnamezu = pointname.Split(',');
                    DataView[] dvzu = new DataView[lscyc.Count];

                    Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();

                    for (int k = 0; k < lscyc.Count; k++)
                    {
                        dvzu[k] = new DataView(dt);
                        ddzu.Add(lscyc[k], dvzu[k]);
                    }
                    List<serie_point> lst = new List<serie_point>();
                    //for (int z = 0; z < 2; z++)
                    //{

                    for (int d = 0; d < lscyc.Count; d++)
                    {

                        if (seriescont > model.rows * model.pageIndex - 1) { ExceptionLog.ExceptionWrite(seriescont + ">" + model.rows * model.pageIndex); break; }
                        else if (seriescont < model.rows * (model.pageIndex - 1)) { seriescont++; continue; }

                        string filter =
                        dvzu[d].RowFilter = "cyc= " + lscyc[d];

                       
                        serie_point st_this = new serie_point();//创建曲线
                        st_this.Stype = "本次变化量";//曲线类别
                        st_this.Name = lscyc[d].Replace("'", "");
                        switch_point(dvzu[d], st_this, "this_val");
                        lst.Add(st_this);
                        serie_point st_this_degree = new serie_point();//创建曲线
                        st_this_degree.Stype = "本次温度";//曲线类别
                        st_this_degree.Name = lscyc[d].Replace("'", "");
                        switch_point(dvzu[d], st_this_degree, "SteelChordVal");
                        lst.Add(st_this_degree);
                        serie_point st_ac = new serie_point();//创建曲线
                        st_ac.Stype = "累计变化量";//曲线类别
                        st_ac.Name = lscyc[d].Replace("'", "");
                        switch_point(dvzu[d], st_ac, "ac_val");
                        lst.Add(st_ac);
                        serie_point st_ac_degree = new serie_point();//创建曲线
                        st_ac_degree.Stype = "累计温度";//曲线类别
                        st_ac_degree.Name = lscyc[d].Replace("'", "");
                        switch_point(dvzu[d], st_ac_degree, "SteelChordVal");
                        lst.Add(st_ac_degree);
                        seriescont++;
                    }



                    //}
                    model.serie_points = lst;

                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("曲线绘制出错，错误信息：" + ex.Message + "位置:" + ex.StackTrace);
                return false;
            }

        }





        /// <summary>
        /// 有数据表生成曲线组
        /// </summary>
        /// <param name="dv">数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量名称</param>
        public static void switch_point(DataView dv, serie_point st, string valstr)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_point[len];
            foreach (DataRowView drv in dv)
            {
                st.Pts[i] = new pt_point();
                st.Pts[i] = pointgtsurfaceformat(drv, valstr);
                i++;
            }


        }
        /// <summary>
        /// 数据行生成
        /// </summary>
        /// <param name="drv"></param>
        /// <param name="sxtj"></param>
        /// <returns></returns>
        public static pt_point pointgtsurfaceformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = drv["cyc"].ToString();

            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt_point { PointName = string.Format("{0} {1}", drv["surfacename"].ToString(), drv["time"].ToString()), Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
       



        #endregion
        #region 多点单周期
        /// <summary>
        /// 以点名作为横轴的曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrgtsensordata_point(ProcessChartCondition model, out string mssg)
        {

            int seriescont = 0;

            mssg = "";
            try
            {

                DataTable dt = new DataTable();
                DataTable dtt = new DataTable();
                string sql = model.sql;
                string pointname = model.pointname;
                string xmname = model.xmname;
                if (pointname != "" && pointname != null)
                {

                    var processquerystanderdbModel = new QuerystanderdbModel(sql, model.xmname);
                    if (!ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
                    {
                        //错误信息反馈
                    }
                    dt = processquerystanderdbModel.dt;
                    DataView dvdefault = new DataView(dt);
                    DataTable dtdistinct = dvdefault.ToTable(true, "cyc");
                    //从表中筛选出周期
                    List<string> lscyc = CycListGet(dtdistinct);

                    //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                    //string[] pointnamezu = pointname.Split(',');
                    DataView[] dvzu = new DataView[lscyc.Count];

                    Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();

                    for (int k = 0; k < lscyc.Count; k++)
                    {
                        dvzu[k] = new DataView(dt);
                        ddzu.Add(lscyc[k], dvzu[k]);
                    }
                    List<serie_point> lst = new List<serie_point>();
                    //for (int z = 0; z < 2; z++)
                    //{

                    for (int d = 0; d < lscyc.Count; d++)
                    {
                        if (seriescont > model.rows * model.pageIndex - 1) { ExceptionLog.ExceptionWrite(seriescont + ">" + model.rows * model.pageIndex); break; }
                        else if (seriescont < model.rows * (model.pageIndex - 1)) { seriescont++; continue; }
                        string filter =
                        dvzu[d].RowFilter = "cyc= " + lscyc[d];

                        serie_point st_scalarvalue = new serie_point();//创建曲线
                        st_scalarvalue.Stype = "测量值";//曲线类别
                        st_scalarvalue.Name = lscyc[d].Replace("'", "");
                        switchnez_point(dvzu[d], st_scalarvalue, "first_ac_scalarvalue");
                        lst.Add(st_scalarvalue);
                        serie_point st_scalarvalue_degree = new serie_point();//创建曲线
                        st_scalarvalue_degree.Stype = "温度";//曲线类别
                        st_scalarvalue_degree.Name = lscyc[d].Replace("'", "");
                        switchnez_point(dvzu[d], st_scalarvalue_degree, "sec_oregion_scalarvalue");
                        lst.Add(st_scalarvalue_degree);
                        serie_point st_this = new serie_point();//创建曲线
                        st_this.Stype = "本次变化量";//曲线类别
                        st_this.Name = lscyc[d].Replace("'", "");
                        switchnez_point(dvzu[d], st_this, "single_this_scalarvalue");
                        lst.Add(st_this);
                        serie_point st_this_degree = new serie_point();//创建曲线
                        st_this_degree.Stype = "本次温度";//曲线类别
                        st_this_degree.Name = lscyc[d].Replace("'", "");
                        switchnez_point(dvzu[d], st_this_degree, "sec_oregion_scalarvalue");
                        lst.Add(st_this_degree);
                        serie_point st_ac = new serie_point();//创建曲线
                        st_ac.Stype = "累计变化量";//曲线类别
                        st_ac.Name = lscyc[d].Replace("'", "");
                        switchnez_point(dvzu[d], st_ac, "single_ac_scalarvalue");
                        lst.Add(st_ac);
                        serie_point st_ac_degree = new serie_point();//创建曲线
                        st_ac_degree.Stype = "累计温度";//曲线类别
                        st_ac_degree.Name = lscyc[d].Replace("'", "");
                        switchnez_point(dvzu[d], st_ac_degree, "sec_oregion_scalarvalue");
                        lst.Add(st_ac_degree);
                        seriescont++;
                    }



                    //}
                    model.serie_points = lst;

                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("曲线绘制出错，错误信息：" + ex.Message + "位置:" + ex.StackTrace);
                return false;
            }

        }





        /// <summary>
        /// 周期列表初始化
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<string> CycListGet(DataTable dt)
        {
            DataView dv = new DataView(dt);
            dv.Sort = " cyc asc ";
            List<string> ls = new List<string>();
            foreach (DataRowView drv in dv)
            {
                ls.Add(drv["cyc"].ToString());
            }
            ExceptionLog.ExceptionWrite(string.Join(",", ls));
            return ls;
        }
        /// <summary>
        /// 有数据表生成曲线组
        /// </summary>
        /// <param name="dv">数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量名称</param>
        public static void switchnez_point(DataView dv, serie_point st, string valstr)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_point[len];
            foreach (DataRowView drv in dv)
            {
                st.Pts[i] = new pt_point();
                st.Pts[i] = pointwgformat(drv, valstr);
                i++;
            }


        }
        /// <summary>
        /// 数据行生成
        /// </summary>
        /// <param name="drv"></param>
        /// <param name="sxtj"></param>
        /// <returns></returns>
        public static pt_point pointwgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = drv["cyc"].ToString();

            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt_point { PointName = string.Format("{0} {1}", drv["point_name"].ToString(), drv["time"].ToString()), Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        #endregion
    }
}