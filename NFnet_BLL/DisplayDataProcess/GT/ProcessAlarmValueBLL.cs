﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_gtalarmvaluebll.DisplayDataProcess.GT
{
    public partial class ProcessAlarmValueBLL
    {
        public global::data.BLL.gtalarmvalue gtalarmvaluebll = new data.BLL.gtalarmvalue();
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtalarmvalue model, out string mssg)
        {
            return gtalarmvaluebll.Add(model,out mssg);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(data.Model.gtalarmvalue model, out string mssg)
        {
            return gtalarmvaluebll.Update(model, out mssg);
        }

      /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool ProcessgtalarmvalueDelete(ProcessgtalarmvalueDeleteModel model,out string mssg)
        {

            return gtalarmvaluebll.Delete(model.xmno, model.alarmname, out mssg);
        }
         public class ProcessgtalarmvalueDeleteModel
        {
            public int xmno{get;set;}
            public string alarmname{get;set;}
            public ProcessgtalarmvalueDeleteModel(int xmno,string alarmname)
            {
            this.xmno = xmno;
            this.alarmname = alarmname;
            }
        }


        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool ProcessgtpointalarmvalueDelCasc(ProcessgtpointalarmvalueDelCascModel model,out string mssg)
        {
             return gtalarmvaluebll.PointAlarmValueDelCasc(model.alarmname, model.xmno,out mssg);
        }

        public class ProcessgtpointalarmvalueDelCascModel
        {
            public string alarmname{get;set;}
            public int xmno{get;set;}
            ProcessgtpointalarmvalueDelCascModel()
            {
            this.alarmname = alarmname;
            this.xmno = xmno;
            }
        }

        //--------------------------------------------------------------

          /// <summary>
        /// 预警参数表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool SingleScalarTableRowsCount(ProcessAlarmRecordsCountModel model, out string mssg)
        {
            mssg = "";
            int totalCont = 0;
            if (gtalarmvaluebll.SingleScalarTableRowsCount( model.seachstring,model.xmno,model.datatype ,out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 预警参数表记录数获取实体
        /// </summary>
        public class ProcessAlarmRecordsCountModel : SearchCondition
        {
            /// <summary>
            /// 记录数
            /// </summary>
            public int totalCont { get; set; }
            public string seachstring { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessAlarmRecordsCountModel(int xmno, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;

            }
        }
        /// <summary>
        /// 预警参数表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool SingleScalarTableLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            mssg = "";
            DataTable dt = null;
            if (gtalarmvaluebll.SingleScalarTableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord,model.datatype ,out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessAlarmLoadModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public ProcessAlarmLoadModel(int xmno, string colName, int pageIndex, int rows, string sord, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.datatype = datatype;

            }
        }


        /// <summary>
        /// 预警名称获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmValueName(ProcessAlarmValueNameModel model,out string mssg)
        {
             string alarmValueNameStr ="";
             mssg = "";
             if (gtalarmvaluebll.AlarmValueNameGet(model.xmno, model.datatype, out alarmValueNameStr, out mssg))
             {
                 model.alarmValueNameStr = alarmValueNameStr;
                 return true;
             }
             else
             {
                 return false;
             }
        }
        /// <summary>
        ///预警名称获取类
        /// </summary>
        public class ProcessAlarmValueNameModel
        {
            public int xmno { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string alarmValueNameStr { get; set; }
            public ProcessAlarmValueNameModel(int xmno,  data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.datatype = datatype;
            }
        }


        public bool ProcessSingleScalarvalueModelGet(ScalarvalueModelGetModel model, out string mssg)
        {
            mssg = "";
            global::data.Model.gtalarmvalue alarm = new global::data.Model.gtalarmvalue();
            if (gtalarmvaluebll.GetSingleScalarvalueModel(model.name, model.xmno,model.datatype ,out alarm, out mssg))
            {
                model.model = alarm;
                return true;
            }
            return false;
        }
        public class ScalarvalueModelGetModel
        {
            public int xmno { get; set; }
            public  global::data.Model.gtalarmvalue  model { get; set; }
            public string name { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public ScalarvalueModelGetModel(int xmno, string name, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.name = name;
                this.datatype = datatype;
            }
        }
    }
}