﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using System.Threading;
using NFnet_gtalarmvaluebll.DisplayDataProcess.GT;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    public partial class ProcessResultDataAlarmBLL
    {
       
        
        public bool twoscalarmain()
        {
            alarmcont = 0;
            return TestTwoScalarCycdirnetModelList();

        }
        public bool TestTwoScalarCycdirnetModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessResultDataBLL.ProcessScalarResultDataAlarmModelListModel(xmname);
            List<global::data.Model.gtsensordata> modellist;
            if (resultBLL.ProcessScalarResultDataAlarmModelList(processResultDataAlarmModelListModel, out modellist, out mssg))
            {

                CycdirnetPointAlarm(modellist);
                return true;
            }
            return false;
        }
        public void TestTwoPointAlarmfilterInformation(List<data.Model.gtalarmvalue> levelalarmvalue, data.Model.gtsensordata resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (pointAlarmBLL.ProcessTwoScalarPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                alarmcont++;
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.Add(new alarmInfo { datatype = resultModel.datatype, alarmcontext = string.Join("\r\n", processPointAlarmfilterInformationModel.ls) });
            }
        }
        public bool TwoScalarCycdirnetPointAlarm(List<data.Model.gtsensordata> lc, string title)
        {
            alarmInfoList = new List<alarmInfo>();
            //List<string> ls = new List<string>();
            //ls.Add("\n");
            //ls.Add(string.Format("==========={0}===========", DateTime.Now));
            //ls.Add(string.Format("==========={0}===========", xmname));
            //ls.Add(string.Format("==========={0}===========",title));
            //ls.Add("\n");
            //alarmInfoList.AddRange(ls);
            //ExceptionLog.ExceptionWriteCheck(ls);
            ExceptionLog.TotalSationPointCheckVedioWrite("获取到项目"+xmname+"自检临时表中记录数" + lc.Count + "条");

            foreach (data.Model.gtsensordata cl in lc)
            {
                //string threadname = Thread.CurrentThread.Name;
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname)) && !Tool.ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", xmname))) return false;
                data.Model.gtpointalarmvalue pointvalue = TestPointAlarmValue(cl.point_name, data.DAL.gtsensortype.GTStringToSensorType(cl.datatype));
                List<data.Model.gtalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestSinglePointAlarmfilterInformation(alarmList, cl);

            }
            ExceptionLog.TotalSationPointCheckVedioWrite("项目" + xmname  + "本次预警共产生新预警记录" + alarmcont + "条");
            ExceptionLog.TotalSationPointCheckVedioWrite("************预警结束**********");
            return true;
            //Console.ReadLine();
        }
        public List<string> TwoScalarResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            ExceptionLog.TotalSationPointCheckVedioWrite("************开始预警**********");
            main();
            var infolist = (from m in alarmInfoList orderby m.datatype select m.alarmcontext).ToList();
            return infolist;
        }
        public void TwoScalarCycdirnetPointAlarm(data.Model.gtsensordata dirnet)
        {

            data.Model.gtpointalarmvalue pointvalue = TestPointAlarmValue(dirnet.point_name, data.DAL.gtsensortype.GTStringToSensorType(dirnet.datatype));
            if (pointvalue == null) return;
            List<data.Model.gtalarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestTwoPointAlarmfilterInformation(alarmList, dirnet);
            
        }




    }
}