﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_gtalarmvaluebll.DisplayDataProcess.GT
{
    public partial class ProcessAlarmValueBLL
    {
        /// <summary>
        /// 双量预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TwoScalarTableLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (gtalarmvaluebll.TwoScalarTableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord,model.datatype, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
        }

        /// <summary>
        /// 预警参数表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool TwoScalarTableRowsCount(ProcessAlarmRecordsCountModel model, out string mssg)
        {
            mssg = "";
            int totalCont = 0;
            if (gtalarmvaluebll.TwoScalarTableRowsCount(model.seachstring, model.xmno, model.datatype, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        public bool ProcessTwoScalarvalueModelGet(ScalarvalueModelGetModel model, out string mssg)
        {
            mssg = "";
            global::data.Model.gtalarmvalue alarm = new global::data.Model.gtalarmvalue();
            if (gtalarmvaluebll.GetTwoScalarvalueModel(model.name, model.xmno, model.datatype, out alarm, out mssg))
            {
                model.model = alarm;
                return true;
            }
            return false;
        }
    }
}