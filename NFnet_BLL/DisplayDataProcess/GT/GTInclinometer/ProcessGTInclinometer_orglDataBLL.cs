﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NGN.BLL;

namespace NFnet_BLL.DisplayDataProcess.GTInclinometer
{
    public class ProcessGTInclinometer_orglDataBLL
    {
        public data.BLL.gtinclinometer bll = new data.BLL.gtinclinometer();
       

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(data.Model.gtinclinometer model, out string mssg)
        {
            return bll.Add(model,out mssg);
        }
        public bool Delete(data.Model.gtinclinometer model, out string mssg)
        {
            return bll.Delete(model, out mssg);
        }
      
        public bool delete_tmp(deletetmpmodel model,out string mssg)
        {
            int cont = 0;
            if(bll.Delete_tmp(model.xmno,out cont,out mssg))
            {
                model.cont = cont;
                return true;
            }
            return false;
        }

        public class deletetmpmodel
        {
            public int xmno { get; set; }
            public int cont { get; set; }
            public deletetmpmodel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(fixed_inclinometer_orgldataModelGetModel model, out string mssg)
        {
            data.Model.gtinclinometer fixed_inclinometer_orgldatamodel = null;
            if (bll.GetModel(model.xmno, model.point_name,model.deepth ,model.time, out fixed_inclinometer_orgldatamodel, out mssg))
            {
                model.model = fixed_inclinometer_orgldatamodel;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetLastTimeModel(fixed_inclinometer_orgldataModelGetModel model, out string mssg)
        {
            data.Model.gtinclinometer fixed_inclinometer_orgldatamodel = null;
            if (bll.GetLastTimeModel(model.xmno, model.point_name, model.deepth, model.time, out fixed_inclinometer_orgldatamodel, out mssg))
            {
                model.model = fixed_inclinometer_orgldatamodel;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetSumAcDiff(fixed_inclinometer_orgldataModelGetModel model, out string mssg)
        {
            data.Model.gtinclinometer fixed_inclinometer_orgldatamodel = null;
            if (bll.GetSumAcDiff(model.xmno, model.point_name, model.deepth, model.time, out fixed_inclinometer_orgldatamodel, out mssg))
            {
                model.model = fixed_inclinometer_orgldatamodel;
                return true;
            }
            return false;
        }
        public class fixed_inclinometer_orgldataModelGetModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public double deepth { get; set; }
            public DateTime time { get; set; }
            public data.Model.gtinclinometer model { get; set; }
            public fixed_inclinometer_orgldataModelGetModel(int xmno,string point_name,DateTime time)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.time = time;
            }
            public fixed_inclinometer_orgldataModelGetModel(int xmno, string point_name,double deepth ,DateTime time)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.deepth = deepth;
                this.time = time;
            }
        }

        public bool ProcessResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out string mssg)
        {
            List<global::data.Model.gtinclinometer> modellist = null;
            mssg = "";
            if (bll.GetModelList(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class ProcessResultDataAlarmModelListModel
        {
            public int xmno { get; set; }
            public List<global::data.Model.gtinclinometer> modellist { set; get; }
            public ProcessResultDataAlarmModelListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        public bool PointMaxDateTimeGet(PointMaxDateTimeGetModel model, out string mssg)
        {
            DateTime dt = new DateTime();
            if(bll.PointMaxDateTimeGet(model.xmno,out dt,out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;

        }
        public class PointMaxDateTimeGetModel
        {
            public int xmno { get; set; }
            public DateTime dt { get; set; }
            public PointMaxDateTimeGetModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
    }
}