﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.GT
{

    /// <summary>
    /// 结果数据业务逻辑处理类
    /// </summary>
    public partial class ProcessResultDataBLL
    {

       

        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool TwoScalarResultdataTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.TwoScalarResultdataTableLoad(model.pageIndex, model.rows, model.xmname, model.pointname, model.sord, model.datatype, model.starttime, model.endtime, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
           

        }

        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessTwoScalarfillTotalStationDbFill(FillTotalStationDbFillCondition model)
        {
            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "time");
            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "point_name");
            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel("   point_name   ", model.pointname, "  in  ", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = "select  project_name,point_name,senorno,datatype,valuetype,first_oregion_scalarvalue,first_this_scalarvalue,first_ac_scalarvalue,sec_oregion_scalarvalue,sec_this_scalarvalue,sec_ac_scalarvalue,cyc,time     from  gtsensordata where  project_name = '" + model.xmname + "' and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
                sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            DataTable dt = new DataTable();
            if (GetResultDataTable(sql, model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                ExceptionLog.ExceptionWrite(mssg);
                return true;
            }
            return false;

        }

        public bool ProcessTwoResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out List<global::data.Model.gtsensordata> modellist, out string mssg)
        {
            modellist = new List<global::data.Model.gtsensordata>();
            if (bll.TwoScalarGetModelList(model.xmname, model.datatype, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }

    
       


       
    }
}
