﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    public partial class ProcessResultDataBLL
    {

        public bool AddSurveyData(AddSurveyDataModel model, out string mssg)
        {
            return bll.AddSurveyData(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.cyc, model.datatype, out mssg);
        }
        public class AddSurveyDataModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public AddSurveyDataModel(string xmname, int cyc, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.cyc = cyc;
                this.datatype = datatype;
            }
        }

        public bool ProcessSurveyDataUpdata(global::data.Model.gtsensordata model, out string mssg)
        {
            return bll.UpdataSurveyData(model, out mssg);
        }
        public bool ProcessSurveyDataNextCYCThisUpdate(global::data.Model.gtsensordata model, out string mssg)
        {
            return bll.UpdataSurveyDataNextThis(model, out mssg);
        }
       
        public bool ProcessSurveyPointTimeDataAdd(ProcessSurveyPointTimeDataAddModel model, out string mssg)
        {
            return bll.SurveyPointTimeDataAdd(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, model.pointname, model.time, model.importtime, out mssg);
        }
        public class ProcessSurveyPointTimeDataAddModel
        {
            public string xmname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public string pointname { get; set; }
            public DateTime time { get; set; }
            public DateTime importtime { get; set; }
            public ProcessSurveyPointTimeDataAddModel(string xmname, data.Model.gtsensortype datatype, string pointname, DateTime time, DateTime importtime)
            {
                this.xmname = xmname;
                this.datatype = datatype;
                this.pointname = pointname;
                this.time = time;
                this.importtime = importtime;
            }

        }
        /// 删除一条数据
        /// </summary>
        public bool DeleteSurveyData(DeleteModel model, out string mssg)
        {
            return bll.DeleteSurveyData(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, model.startcyc, model.endcyc, out mssg);

        }
       
        public bool ProcessSurveyResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.PointNameSurveyDateTimeListGet(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.datatype, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessSurveyResultDataTimeCycLoad(ProcessResultDataTimeCycLoadModel model, out string mssg)
        {
            List<string> cyctimelist = null;
            mssg = "";
            if (bll.SurveyCycTimeList(model.xmname, model.datatype, out cyctimelist, out mssg))
            {
                model.cyctimelist = cyctimelist;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool SurveyResultdataTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.SurveyResultdataTableLoad(model.pageIndex, model.rows, ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.sord, model.datatype, model.starttime, model.endtime, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 编辑库结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool SurveyResultdataTableRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            int totalCont = 0;
            if (bll.SurveyResultTableRowsCount(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.sord, model.datatype, model.startcyc, model.endcyc, out totalCont, out mssg))
            {
                model.totalCont = totalCont.ToString();
                return true;
            }
            else
            {

                return false;
            }

        }
    }
}