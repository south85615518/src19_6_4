﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    public partial class ProcessResultDataBLL
    {
        
        public bool ProcesspointcgdataResultDataMaxTime(GTSensorMaxTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.CgMaxTime(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, model.pointname, out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }

        public bool ProcesspointcgdataResultDataMinTime(GTSensorMinTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.CgMinTime(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, model.pointname, out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            return false;


        }
        public bool ProcessCgResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.CgPointNameDateTimeListGet(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.datatype, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessCgDataImport(ProcessCgDataImportModel model, out string mssg)
        {
            
             return bll.AddCgData(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname),  model.datatype, model.cyc, model.importcyc, out mssg); 
        }
        public class ProcessCgDataImportModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public int importcyc { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public int rows { get; set; }
            public ProcessCgDataImportModel(string xmname, int cyc, int importcyc, data.Model.gtsensortype datatype)
            {
                this.xmname = xmname;
                this.cyc = cyc;
                this.importcyc = importcyc;
                this.datatype = datatype;
            }
        }
        public bool ProcessCgResultDataTimeCycLoad(ProcessResultDataTimeCycLoadModel model, out string mssg)
        {
            List<string> cyctimelist = null;
            mssg = "";
            if (bll.CgCycTimeList(model.xmname, model.datatype, out cyctimelist, out mssg))
            {
                model.cyctimelist = cyctimelist;
                return true;
            }
            return false;
        }
         //<summary>
         //删除成果数据
         //</summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteCg(DeleteModel model, out string mssg)
        {
            return bll.DeleteCgData(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, model.startcyc, model.endcyc, out mssg);

        }
        public bool DeleteCgTmp(DeleteTmpModel model, out string mssg)
        {
            return bll.DeleteCgTmp(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.datatype, out mssg);

        }
        public bool CgSingleScalarResultdataTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.CgSingleScalarResultdataTableLoad(model.pageIndex, model.rows, ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.sord, model.datatype, model.starttime, model.endtime, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
        }

        public bool CgScalarResultdataTableRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.CgScalarResultTableRowsCount(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.pointname, model.sord, model.datatype, model.startTime, model.endTime, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }

        public bool ProcessCgScalarResultDataAlarmModelList(ProcessScalarResultDataAlarmModelListModel model, out List<global::data.Model.gtsensordata> modellist, out string mssg)
        {
            modellist = new List<global::data.Model.gtsensordata>();
            if (bll.CgScalarGetModelList(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        


    }
}