﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using System.Threading;
using NFnet_gtalarmvaluebll.DisplayDataProcess.GT;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    public partial class ProcessResultDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public string dateTime { get; set; }
        public int alarmcont { get; set; }
        //保存预警信息
        public List<alarmInfo> alarmInfoList { get; set; }

        public class alarmInfo
        {
            public string datatype { get; set; }
            public string alarmcontext { get; set; }
            public alarmInfo()
            {
 
            }
        }


        public ProcessResultDataAlarmBLL()
        {
        }
        public ProcessResultDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = ProcessAspectIndirectValue.GetXmnoFromXmnameStr(xmname);
            this.xmno = xmno;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
        public static ProcessResultDataBLL resultBLL = new ProcessResultDataBLL();
        public bool main()
        {
            alarmcont = 0;
            return TestCycdirnetModelList();

        }
        public bool TestCycdirnetModelList()
        {
            try
            {
                if (dateTime == "") dateTime = DateTime.Now.ToString();
                var processResultDataAlarmModelListModel = new ProcessResultDataBLL.ProcessScalarResultDataAlarmModelListModel(xmname);
                List<global::data.Model.gtsensordata> modellist;
                if (resultBLL.ProcessScalarResultDataAlarmModelList(processResultDataAlarmModelListModel, out modellist, out mssg))
                {

                    CycdirnetPointAlarm(modellist);
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex);
                return false;
            }
        }
        public data.Model.gtpointalarmvalue TestPointAlarmValue(string pointName,data.Model.gtsensortype datatype)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointName,datatype);
            data.Model.gtpointalarmvalue model = null;
            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print(mssg);
                //TestAlarmValueList(processPointAlarmModelGetModel.model);
                return processPointAlarmModelGetModel.model;

            }
            return null;
        }
        public List<data.Model.gtalarmvalue> TestAlarmValueList(data.Model.gtpointalarmvalue pointalarm)
        {
            List<data.Model.gtalarmvalue> alarmvalueList = new List<data.Model.gtalarmvalue>();
            //一级
            var ProcessSingleScalarvalueModelGetModel = new ProcessAlarmValueBLL.ScalarvalueModelGetModel(xmno, pointalarm.firstalarmname, pointalarm.datatype);
            if (alarmBLL.ProcessSingleScalarvalueModelGet(ProcessSingleScalarvalueModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print("一级：" + mssg);
                alarmvalueList.Add(ProcessSingleScalarvalueModelGetModel.model);
            }
            else
                alarmvalueList.Add(null);
            ProcessSingleScalarvalueModelGetModel = new ProcessAlarmValueBLL.ScalarvalueModelGetModel(xmno, pointalarm.secondalarmname, pointalarm.datatype);
            if (alarmBLL.ProcessSingleScalarvalueModelGet(ProcessSingleScalarvalueModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print("二级：" + mssg);
                alarmvalueList.Add(ProcessSingleScalarvalueModelGetModel.model);
            }
            else
                alarmvalueList.Add(null);
            ProcessSingleScalarvalueModelGetModel = new ProcessAlarmValueBLL.ScalarvalueModelGetModel(xmno, pointalarm.thirdalarmname, pointalarm.datatype);
            if (alarmBLL.ProcessSingleScalarvalueModelGet(ProcessSingleScalarvalueModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print("三级：" + mssg);
                alarmvalueList.Add(ProcessSingleScalarvalueModelGetModel.model);
            }
            else
                alarmvalueList.Add(null);
            return alarmvalueList;
        }
        public void TestSinglePointAlarmfilterInformation(List<data.Model.gtalarmvalue> levelalarmvalue, data.Model.gtsensordata resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (pointAlarmBLL.ProcessSinglePointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                alarmcont++;
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.Add(new alarmInfo { datatype = resultModel.datatype, alarmcontext = string.Join("\r\n",processPointAlarmfilterInformationModel.ls) });
                Console.WriteLine(string.Format("{0}   {1}   {2}\n", resultModel.datatype, resultModel.point_name, string.Join("\r\n", processPointAlarmfilterInformationModel.ls)));
            }
        }
        public bool CycdirnetPointAlarm(List<data.Model.gtsensordata> lc)
        {
            alarmInfoList = new List<alarmInfo>();
            //List<string> ls = new List<string>();
            //ls.Add("\n");
            //ls.Add(string.Format("==========={0}===========", DateTime.Now));
            //ls.Add(string.Format("==========={0}===========", xmname));
            //ls.Add(string.Format("==========={0}===========","超限自检"));
            //ls.Add("\n");
            //alarmInfoList.AddRange(ls);
            //ExceptionLog.ExceptionWriteCheck(ls);
            ExceptionLog.TotalSationPointCheckVedioWrite("获取到项目"+xmname+"自检临时表中记录数" + lc.Count + "条");
            int i = 0;
            foreach (data.Model.gtsensordata cl in lc)
            {
                
                
                //string threadname = Thread.CurrentThread.Name;
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname)) && !Tool.ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", xmname))) return false;
                data.Model.gtpointalarmvalue pointvalue = TestPointAlarmValue(cl.point_name,data.DAL.gtsensortype.GTStringToSensorType(cl.datatype));
                if (pointvalue == null) continue;
                List<data.Model.gtalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                if( data.DAL.gtsensortype.GTStringToSensorType(cl.datatype) ==data.Model.gtsensortype._windspeedanddirection )
                TestTwoPointAlarmfilterInformation(alarmList, cl);
                else
                TestSinglePointAlarmfilterInformation(alarmList, cl);
                

            }
            ExceptionLog.TotalSationPointCheckVedioWrite("项目" + xmname + "本次预警共产生新预警记录" + alarmcont + "条");
            ExceptionLog.TotalSationPointCheckVedioWrite("************预警结束**********");
            return true;
        }
        public List<string> ResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            ExceptionLog.TotalSationPointCheckVedioWrite("************开始预警**********");
            main();
            var infolist = (from m in alarmInfoList orderby m.datatype select m.alarmcontext).ToList();
            return infolist;
        }
        public void CycdirnetPointAlarm(data.Model.gtsensordata dirnet)
        {
            alarmInfoList = new List<alarmInfo>();
            data.Model.gtpointalarmvalue pointvalue = TestPointAlarmValue(dirnet.point_name, data.DAL.gtsensortype.GTStringToSensorType(dirnet.datatype));
            if (pointvalue == null) return;
            List<data.Model.gtalarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestSinglePointAlarmfilterInformation(alarmList, dirnet);
            
        }
        public bool GTPointResultDataCurrentAlarmCreate(List<global::data.Model.gtsensordata> lc)
        {
            alarmInfoList = new List<alarmInfo>();
            foreach (global::data.Model.gtsensordata cl in lc)
            {

                data.Model.gtpointalarmvalue pointvalue = TestPointAlarmValue(cl.point_name, data.DAL.gtsensortype.GTStringToSensorType(cl.datatype));
                List<data.Model.gtalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestSinglePointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<alarmInfo> GTSettlementResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }



    }
}