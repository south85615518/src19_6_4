﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;
using NFnet_BLL.Other;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    /// <summary>
    /// 水位曲线业务逻辑处理类
    /// </summary>
    public partial class ProcessGTSensorDataChartBLL
    {
        /// <summary>
        /// 水位曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessTwoScalarSerializestrGTSensorData(ProcessChartCondition model, out string mssg)
        {

                mssg = "";
                string xmname = model.xmname;
                DataTable dt = new DataTable();
                string mkhs = model.pointname;
                string sql = model.sql;

                 ProcessResultDataBLL processGTSensorDataResultDataBLL = new ProcessResultDataBLL();
                

                //分模块号显示
                if (sql == "") return false;

                if(!processGTSensorDataResultDataBLL.GetResultDataTable(sql,model.xmname,out dt,out mssg)) return false;
                string[] mkharr = mkhs.Split(',');

                if (mkhs != "" && mkhs != null)
                {
                    List<serie> lst = TwoScalarCreateSeriesFromData(dt, mkhs);
                    model.series = lst;
                }//如果mkh为空默认只显示过程
            return true;
        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">水位点名</param>
        /// <returns></returns>
        public static List<serie> TwoScalarCreateSeriesFromData(DataTable dt, string mkhs)
        {
            //string[] mkharr = mkhs.Split(',');

            //if (mkhs != "" && mkhs != null)
            //{
            //    List<serie> lst = new List<serie>();

            //    omkhs[] omkharr = new omkhs[mkharr.Length];
            //    for (int t = 0; t < mkharr.Length; t++)
            //    {
            //        serie first_sr = new serie();
            //        serie first_srthis = new serie();
            //        serie first_srac = new serie();
            //        serie sec_sr = new serie();
            //        serie sec_srthis = new serie();
            //        serie sec_srac = new serie();
            //        DataView dv = new DataView(dt);
            //        dv.RowFilter = "point_name=" + mkharr[t];
            //        int nu = dv.Count;
            //        string rri = mkharr[t];
            //        first_sr.Stype = "第一个观测量的值";
            //        first_sr.Name = rri.Replace("'", "");
            //        //加载点
            //        formatdat(dv, first_sr, "first_oregion_scalarvalue");
            //        lst.Add(first_sr);
            //        //sr = new serie();
            //        first_srthis.Stype = "第一个标量值本次变化量";
            //        first_srthis.Name = rri.Replace("'", "");
            //        //加载点
            //        formatdat(dv, first_srthis, "first_this_scalarvalue");
            //        lst.Add(first_srthis);
            //        first_srac.Stype = "第一个标量值累计变化量";
            //        first_srac.Name = rri.Replace("'", "");
            //        //加载点
            //        formatdat(dv, first_srac, "first_ac_scalarvalue");
            //        lst.Add(first_srac);

            //        sec_sr.Stype = "第二个观测量的值";
            //        sec_sr.Name = rri.Replace("'", "");
            //        //加载点
            //        formatdat(dv, sec_sr, "sec_oregion_scalarvalue");
            //        lst.Add(sec_sr);
            //        //sr = new serie();
            //        sec_srthis.Stype = "第二个标量值本次变化量";
            //        sec_srthis.Name = rri.Replace("'", "");
            //        //加载点
            //        formatdat(dv, sec_srthis, "sec_this_scalarvalue");
            //        lst.Add(sec_srthis);
            //        sec_srac.Stype = "第二个标量值累计变化量";
            //        sec_srac.Name = rri.Replace("'", "");
            //        //加载点
            //        formatdat(dv, sec_srac, "sec_ac_scalarvalue");
            //        lst.Add(sec_srac);

            //    }


            //    return lst;
            //}
            return null;
        }
        
    }
}