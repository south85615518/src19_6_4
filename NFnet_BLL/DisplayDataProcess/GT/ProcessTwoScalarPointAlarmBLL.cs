﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.XmInfo.pub;
using NFnet_BLL.AuthorityAlarmProcess;
using NFnet_BLL.DataProcess;
using System.Data;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.GT
{
    public partial class ProcessPointAlarmBLL
    {
        

        /// <summary>
        /// 结果数据对象预警处理
        /// </summary>
        /// <param name="alarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessTwoScalarPointAlarmIntoInformation(global::data.Model.gtalarmvalue alarmvalue, global::data.Model.gtsensordata resultModel, out List<string> lscontext)
        {
            lscontext = new List<string>();
            //List<string>  ls = new List<string>();
            List<string> listspace = new List<string>();
            bool result = false;
            listspace.Add(string.Format("{0},{1},"+resultModel.datatype+":{2},", resultModel.point_name, resultModel.time.ToString(), resultModel.sec_ac_scalarvalue));
            listspace.Add(DataProcessHelper.RangeCompareU("", ""+resultModel.datatype+"上限", resultModel.first_ac_scalarvalue, alarmvalue.first_ac_scalarvalue_u, out result, result));
            listspace.Add(DataProcessHelper.RangeCompareL("", ""+resultModel.datatype+"下限", resultModel.first_ac_scalarvalue, alarmvalue.first_ac_scalarvalue_l, out result, result));
            lscontext.Add(string.Join(" ", listspace));
            return result;
        }
       
        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessTwoScalarPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            List<string> ls = new List<string>();
            string mssg = "";
            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.point_name,
                time = model.resultModel.time,
                type = model.resultModel.datatype,
                atime = DateTime.Now,
                pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processCgAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, model.resultModel.datatype, model.resultModel.point_name, model.resultModel.time);
            splitOnDateBLL.ProcessalarmsplitondateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);

            AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = model.resultModel.datatype,
                pointName = model.resultModel.point_name,
                xmno = model.xmno,
                time = model.resultModel.time,
                adate = DateTime.Now

            };
            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessTwoScalarPointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add("三级预警");
                    pointCheck.alarm = 3;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck,out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno,model.resultModel.datatype,"",model.resultModel.point_name,3,out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessTwoScalarPointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add("二级预警");
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, model.resultModel.datatype, "", model.resultModel.point_name, 2, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessTwoScalarPointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add("一级预警");
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, model.resultModel.datatype, "", model.resultModel.point_name, 1, out mssg);
                    return true;
                }


            }
                ls.Add("========预警解除==========");
                pointCheck.alarm = 0;
                pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
                ProcessHotPotColorUpdate(model.xmno, model.resultModel.datatype, "", model.resultModel.point_name, 0, out mssg);
                return false;

        }
        
        

    }
}