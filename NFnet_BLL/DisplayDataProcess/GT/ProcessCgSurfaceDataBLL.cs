﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation;
using System.Data;
using TotalStation.BLL.fmos_obj;
using SqlHelpers;
using NFnet_BLL.Other;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.GT;
using Tool;


namespace NFnet_BLL.DataProcess
{

    /// <summary>
    /// 结果数据业务逻辑处理类
    /// </summary>
    public partial class  ProcessSurfaceDataBLL
    {



        public bool ProcesspointcgdataResultDataMaxTime(GTSurfaceTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.CgMaxTime(model.xmno,model.datatype ,  model.pointname, out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }

        public bool ProcesspointcgdataResultDataMinTime(GTSurfaceTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.CgMinTime(model.xmno,model.datatype ,  model.pointname, out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            return false;


        }
        public bool ProcessCgResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.CgPointNameDateTimeListGet(model.xmno,model.datatype , model.pointname,  out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessCgDataImport(AddSurveyDataModel model, out string mssg)
        {
            return bll.AddCgData(model.xmno,model.datatype , model.cyc,model.importcyc, out mssg);
        }

        //<summary>
        //删除成果数据
        //</summary>
        public bool DeleteCg(DeleteModel model, out string mssg)
        {
            return bll.DeleteCg(model.xmno,model.datatype , model.startcyc,  model.endcyc, out mssg);

        }
        public bool DeleteCgTmp(DeleteTmpModel model, out string mssg)
        {
            return bll.DeleteCgTmp(model.xmno,model.datatype , out mssg);

        }
        public class DeleteTmpModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public DeleteTmpModel(int xmno,data.Model.gtsensortype datatype, string point_name)
            {
                this.xmno = xmno;this.datatype = datatype;
                this.pointname = pointname;
            }
        }

        public bool CgSurfaceResultdataTableLoad(GTSurfaceDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.CgSurfaceDataResultdataTableLoad(model.pageIndex, model.rows, model.xmno,model.datatype , model.pointname, model.sord, model.startcyc, model.endcyc, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
        }

        public bool CgSurfaceResultdataTableRecordsCount(GTSurfaceDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.CgScalarResultTableRowsCount(model.xmno,model.datatype , model.pointname, model.sord,  model.startcyc, model.endcyc, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }

        public bool ProcessCgSurfaceDataTimeCycLoad(ProcessSurfaceDataTimeCycLoadModel model, out string mssg)
        {
            List<string> cyctimelist = null;
            mssg = "";
            if (bll.CgCYCDateTimeListGet(model.xmno,model.datatype , out cyctimelist, out mssg))
            {
                model.cyctimelist = cyctimelist;
                return true;
            }
            return false;
        }

        public bool ProcesscgsurfacedataModelListLoad(ProcesssurfacedataModelListLoadModel model, out string mssg)
        {
            List<data.Model.gtsurfacedata> modellist = new List<data.Model.gtsurfacedata>();
            if (bll.CgSurfaceDataGetModelList(model.xmno,model.datatype , out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }

    }
}