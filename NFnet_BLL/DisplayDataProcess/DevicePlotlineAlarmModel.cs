﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class DevicePlotlineAlarmModel
    {
        public string pointname { get; set; }
        public NGN.Model.fixed_inclinometer_alarmvalue firstalarm { get; set; }
        public NGN.Model.fixed_inclinometer_alarmvalue secalarm { get; set; }
        public NGN.Model.fixed_inclinometer_alarmvalue thirdalarm { get; set; }

    }
}