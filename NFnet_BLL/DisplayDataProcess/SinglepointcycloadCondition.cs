﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SinglepointcycloadCondition
    {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public DataTable dt { get; set; }
            public SinglepointcycloadCondition(string xmname, string pointname)
            {
                this.xmname = xmname;
                this.pointname = pointname;
            }
    }
}