﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL
{
    public class GTResultDataLoadCondition
    {
        /// <summary>
        /// 项目名
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        public global::data.Model.gtsensortype datatype { get; set; }
        /// 点名
        /// <summary>
        /// 起始页号
        /// </summary>
        public int pageIndex { get; set; }
        /// <summary>
        /// 每页行数
        /// </summary>
        public int rows { get; set; }
        
        public int startcyc { get; set; }

        public int endcyc { get; set; }


        public DateTime starttime { get; set; }

        public DateTime endtime { get; set; }
        /// <summary>
        /// 点名
        /// </summary>
        public string pointname { get; set; }
        /// <summary>
        /// 结果数据表
        /// </summary>
        public DataTable dt { get; set; }
        /// <summary>
        /// 排序字段
        /// </summary>
        public string sord { get; set; }
        public GTResultDataLoadCondition(string xmname, string pointname, int pageIndex, int rows, string sord,data.Model.gtsensortype datatype,int startcyc,int endcyc)
        {
            this.xmname = xmname;
            this.pageIndex = pageIndex;
            this.rows = rows;
            this.pointname = pointname;
            this.sord = sord;
            this.datatype = datatype;
            this.startcyc = startcyc;
            this.endcyc = endcyc;
        }

        public GTResultDataLoadCondition(int xmno, string pointname, int pageIndex, int rows, string sord, int startcyc, int endcyc)
        {
            this.xmno = xmno;
            this.pageIndex = pageIndex;
            this.rows = rows;
            this.pointname = pointname;
            this.sord = sord;
            this.startcyc = startcyc;
            this.endcyc = endcyc;
        }
        public GTResultDataLoadCondition(string xmname, string pointname, int pageIndex, int rows, string sord, data.Model.gtsensortype datatype, DateTime starttime, DateTime endtime)
        {
            this.xmname = xmname;
            this.pageIndex = pageIndex;
            this.rows = rows;
            this.pointname = pointname;
            this.sord = sord;
            this.datatype = datatype;
            this.starttime = starttime;
            this.endtime = endtime;
        }

        public GTResultDataLoadCondition(int xmno, string pointname, int pageIndex, int rows, string sord, DateTime starttime, DateTime endtime)
        {
            this.xmno = xmno;
            this.pageIndex = pageIndex;
            this.rows = rows;
            this.pointname = pointname;
            this.sord = sord;
            this.starttime = starttime;
            this.endtime = endtime;
        }


        public GTResultDataLoadCondition()
        {
           
        }
    }
}