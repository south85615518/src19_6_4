﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class MCUAnglePlotlineAlarmModel
    {
        public string pointname { get; set; }
        public MDBDATA.Model.mcuanglealarmvalue firstalarm { get; set; }
        public MDBDATA.Model.mcuanglealarmvalue secalarm { get; set; }
        public MDBDATA.Model.mcuanglealarmvalue thirdalarm { get; set; }

    }
}