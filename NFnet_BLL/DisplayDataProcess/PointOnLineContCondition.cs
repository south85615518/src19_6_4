﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class PointOnLineContCondition
    {
        public string unitname { get; set; }
        public List<string> finishedxmnolist { get; set; }
        public string contpercent { get; set; }
        public PointOnLineContCondition(string unitname, List<string> finishedxmnolist)
        {
            this.unitname = unitname;
            this.finishedxmnolist = finishedxmnolist;
        }
    }
}