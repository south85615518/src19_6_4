﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.InteropServices;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessWinLaunchIfNotRunning
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public struct CopyDataStruct
        {
            public IntPtr dwData;
            public int cbData;

            [MarshalAs(UnmanagedType.LPStr)]

            public string lpData;
        }

        public const int WM_COPYDATA = 0x004A;
        //当一个应用程序传递数据给另一个应用程序时发送此消息指令

        //通过窗口的标题来查找窗口的句柄 
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        private static extern int FindWindow(string lpClassName, string lpWindowName);

        //在DLL库中的发送消息函数
        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage
            (
            int hWnd,                         // 目标窗口的句柄  
            int Msg,                          // 在这里是WM_COPYDATA
            int wParam,                       // 第一个消息参数
            ref CopyDataStruct lParam        // 第二个消息参数
           );



        public static void LaunchWin()
        {
            List<string> pathlist = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "Setting\\exepathlist.txt");
            var pathdictionary = pathlist.Where(m=>m.IndexOf('|') != -1).Select(m => new { path = m.Split('|')[0], winname = m.Split('|')[1] });
            foreach (var model in pathdictionary)
            {
                int hWnd = FindWindow(null, model.winname);
            if (hWnd == 0)
            {
                //启动窗体

                Tool.com.WinHelper.ExeRestart(model.path);
            }
            }
            //将文本框中的值， 发送给接收端          
            //CopyDataStruct cds;
            //cds.dwData = (IntPtr)1; //这里可以传入一些自定义的数据，但只能是4字节整数      
            //cds.lpData = "1";    //消息字符串
            //cds.cbData = System.Text.Encoding.Default.GetBytes("1").Length + 1;
            ////注意，这里的长度是按字节来算的
            //int ret = SendMessage(hWnd, WM_COPYDATA, 0, ref cds);
            // 这里要修改成接收窗口的标题“接收端”
        }
    }
}