﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessXmReportPrintBLL
    {
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessXmReport( ProcessXmReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
             
                totalStationReportHelper.MainXm_GT_Report(
                    model.unitname,
                    model.dt,
                    model.exportpath
                    );
                mssg = string.Format(DateTime.Now + " {0}项目报表生成成功", model.unitname);
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format(DateTime.Now + " {0}项目报表输出出错，错误信息:" + ex.Message,model.unitname));

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }
        public class ProcessXmReportModel
        {
            public string unitname{get;set;}
            public string xlspath { get; set; }
            public DataTable dt { get; set; }
            public string exportpath { get; set; }
            public ProcessXmReportModel(string unitname,string xlspath,DataTable dt,string exportpath)
            {
                this.unitname = unitname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
            }

        }
    }
}