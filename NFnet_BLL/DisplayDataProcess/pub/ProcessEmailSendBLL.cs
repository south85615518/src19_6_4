﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;
using System.IO;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessEmailSendBLL
    {
        public ProcessMonitorBLL processMonitorBLL = new ProcessMonitorBLL();
        public ProcessMonitorAlarmBLL monitorAlarmBLL = new ProcessMonitorAlarmBLL();
        public ProcessSmsSendBLL processSmsSendBLL = new ProcessSmsSendBLL();
        public MailHelper mail = new MailHelper();
        public void ProcessEmailSend(List<string> alarmInfoList, string xmname,int xmno,out string mssg)
        {
            mssg = "";

            if (alarmInfoList == null|| alarmInfoList.Count < 6) return;
            //邮件群发
            var mailsendmodel = new ProcessMonitorAlarmBLL.ProcessMonitorAlarmLastSendTimeModel(xmno);
            if (monitorAlarmBLL.ProcessMonitorAlarmLastSendTime(mailsendmodel, out mssg))
            {
                DateTime lastsendtime = mailsendmodel.dt; 
                if (DateTime.Now.AddHours(-3) < lastsendtime)
                {
                    ExceptionLog.ExceptionWrite(string.Format("现在是{0}项目{1}的最后预警时间是{2}与当前时间间隔少于3小时，取消本次邮件发送!", DateTime.Now, xmname, lastsendtime));
                    return;
                }
            }
            var processXmMonitorListModel = new ProcessMonitorBLL.ProcessXmMonitorListModel(xmno);
            processMonitorBLL.ProcessXmMonitorList(processXmMonitorListModel, out mssg);
            List<Authority.Model.smssendmember> smssendmemberlist = new List<Authority.Model.smssendmember>();

            DateTime dt = DateTime.Now;
            int sec = 0;
            List<string> recmails = new List<string>();
            List<AuthorityAlarm.Model.monitoralarm> lamm = new List<AuthorityAlarm.Model.monitoralarm>();
            ProcessMailDelegateSend pms = new ProcessMailDelegateSend();
            foreach (Authority.Model.UnitMember unitMemberModel in processXmMonitorListModel.lum)
            {
                //监测员预警通知存库
                AuthorityAlarm.Model.monitoralarm monitorAlarm = new AuthorityAlarm.Model.monitoralarm
                {
                    xmno = xmno,
                    context = string.Join(",", alarmInfoList.Where(s => (s != "")).ToList()),
                    time = DateTime.Now,
                    confirm = false,
                    mail = false,
                    unitmember = unitMemberModel.memberno,
                    dataType = 1,
                    ForwardTime = 0,
                    mess = false,
                    mid = string.Format("M{0}{1}", unitMemberModel.memberno, DateHelper.DateTimeToString(dt.AddSeconds(++sec)))
                };
                smssendmemberlist.Add(new Authority.Model.smssendmember { name = unitMemberModel._call, tel = unitMemberModel.tel });
                if (!monitorAlarmBLL.ProcessMonitorAlarmAdd(monitorAlarm, out mssg))
                {
                    ExceptionLog.ExceptionWrite(mssg);
                    break;
                }
                recmails.Add(unitMemberModel.email);
                lamm.Add(monitorAlarm);
            }

            

            //监测员预警通知发送邮件
            //int t = 0;
            //while (t < 3)
            //{
            var messcontext =string.Join("<p>", alarmInfoList.Where(s => (s != "")).ToList());

            //发送预警短信

            string smssend = FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "短信发送切换\\"+xmname+"\\setting.txt");

            if (smssend == "1")
            {
                List<string> mssglist = new List<string>();
                processSmsSendBLL.ProcessSmsSend(xmno, "自检短信", smssendmemberlist, smsalarmsplit(messcontext), out mssglist);
                
            }



                //重发三次
            if (mail.Small(messcontext, recmails, string.Format("{0}预警信息通知", xmname), out mssg))
            {

                foreach (var monitoralarm in lamm)
                {
                    monitoralarm.mail = true;
                    monitorAlarmBLL.ProcessMonitorAlarmUpdate(monitoralarm, out mssg);
                }
            }
            else
            {
                ExceptionLog.ExceptionWrite("启用邮件代发功能");
                MailHelper.messprotocol protocol = new MailHelper.messprotocol { context = messcontext, lsrecmail = recmails, title = string.Format("{0}预警信息通知", xmname) };
                pms.testmaildelegate(protocol);

            }
            ExceptionLog.ExceptionWrite(mssg);

            //foreach (Authority.Model.UnitMember unitMemberModel in processXmMonitorListModel.lum)
            //{
            //    //监测员预警通知存库
            //    AuthorityAlarm.Model.monitoralarm monitorAlarm = new AuthorityAlarm.Model.monitoralarm
            //    {
            //        xmno = xmno,
            //        context = string.Join(",", alarmInfoList.Where(s => (s != "")).ToList()),
            //        time = DateTime.Now,
            //        confirm = false,
            //        mail = true,
            //        unitmember = unitMemberModel.memberno,
            //        dataType = 1,
            //        ForwardTime = 0,
            //        mess = false,
            //        mid = string.Format("M{0}{1}", unitMemberModel.memberno, DateHelper.DateTimeToString(dt.AddSeconds(++sec)))
            //    };
            //    //更新短信标志
            //    //monitorAlarm.mess = true;
                
            //    if (!monitorAlarmBLL.ProcessMonitorAlarmUpdate(monitorAlarm, out mssg))
            //    {
            //        return;
            //    }
            //    //break;
               
            //}



                   
                
            //}
            

        }

        public List<string> smsalarmsplit(string mssgsrc)
        {
            mssgsrc.Replace("<p>", "\r\t");
            int smscont = mssgsrc.Length / 280;
            List<string> smslist = new List<string>();
            int i = 0;
            for (i = 0; i < smscont; i++)
            {
                smslist.Add(mssgsrc.Substring(i * 280, 280));
            }
            smslist.Add(mssgsrc.Substring(280 * i));
            return smslist;
        }

        public void ProcessEmailSend(List<string> pathlist, string mailcontext,string xmname, int xmno, out string mssg)
        {

            //邮件群发
            //var mailsendmodel = new ProcessMonitorAlarmBLL.ProcessMonitorAlarmLastSendTimeModel(xmno);
            //if (monitorAlarmBLL.ProcessMonitorAlarmLastSendTime(mailsendmodel, out mssg))
            //{
            //    DateTime lastsendtime = mailsendmodel.dt;
            //    if (DateTime.Now.AddHours(-3) < lastsendtime)
            //    {
            //        ExceptionLog.ExceptionWrite(string.Format("现在是{0}项目{1}的最后预警时间是{2}与当前时间间隔少于3小时，取消本次邮件发送!", DateTime.Now, xmname, lastsendtime));
            //        return;
            //    }
            //}
            var processXmMonitorListModel = new ProcessMonitorBLL.ProcessXmMonitorListModel(xmno);
            processMonitorBLL.ProcessXmMonitorList(processXmMonitorListModel, out mssg);


            DateTime dt = DateTime.Now;
            int sec = 0;
            List<string> recmails = new List<string>();
            List<AuthorityAlarm.Model.monitoralarm> lamm = new List<AuthorityAlarm.Model.monitoralarm>();
            ProcessMailDelegateSend pms = new ProcessMailDelegateSend();
            foreach (Authority.Model.UnitMember unitMemberModel in processXmMonitorListModel.lum)
            {
                recmails.Add(unitMemberModel.email);
            }
            List<System.Net.Mail.Attachment> attachmentlist = new List<System.Net.Mail.Attachment>();
            List<FileStream> fslist = new List<FileStream>();
            foreach (var path in pathlist)
            {
                FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read);

                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(
                                 fs, Path.GetFileName(path), System.Net.Mime.MediaTypeNames.Application.Zip);
                //fs.Close();
                attachmentlist.Add(attachment);
                fslist.Add(fs);
            }
            //重发三次
            mail.Small(mailcontext, recmails, mailcontext, attachmentlist, out mssg);

            foreach (var fs in fslist)
            {
                if(fs!=null)
                fs.Close();
            }
            ExceptionLog.ExceptionWrite(mssg);



        }
    }
}
