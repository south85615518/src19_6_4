﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessFileQueryBLL
    {

        //查询报表
        public static List<string> FileQuery(string dirpath,ReportType Report,TimeUnit timeunit ,data.Model.gtsensortype datatype,int startcyc,int endcyc)
        { 
            List<report> reportlist = new List<report>();
            //报表格式 项目名_监测类型_报表类型_第【起始周期】-第【终止周期】_时间单位_编号
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            ExceptionLog.ExceptionWrite("找到文件个数" + filenamelist.Count);
            string[] filenameattr = null;
            string filenametmp = "";
            report report = new report();
            foreach (var filenamestr in filenamelist)
            {
                try{
                if (Path.GetExtension(filenamestr) != ".xls" && Path.GetExtension(filenamestr) != ".xlsx")
                    continue;
                filenametmp = Path.GetFileNameWithoutExtension(filenamestr);
                filenameattr = filenamestr.Split('_');
                string[] cycstr = filenameattr[3].Split('【','】');


                report = new report(filenamestr, filenameattr[0], filenameattr[1], filenameattr[2], filenameattr[4],Convert.ToInt32(cycstr[1]),Convert.ToInt32(cycstr[3]));
                reportlist.Add(report);
                }
                catch(Exception ex)
                {
                    ExceptionLog.ExceptionWrite("文件查询出错"+ex.Message);
                }
            }
            ExceptionLog.ExceptionWrite(string.Format("报表个数{0}", reportlist.Count));
            //string datatypesearchstr = datatype == data.Model.gtsensortype._all? "true" : " {0} == " + data.DAL.gtsensortype.GTSensorTypeToString(datatype);
            //bool reportsearchstr = Report == ReportType.ALL ? true : "{0}" ==  ReportTypeToString(Report);
            //bool timeunitstr = timeunit == TimeUnit .ALL? true : "{0} == " + TimeUnitToString(timeunit);
            //bool startcycstr = startcyc == 0 ? "true":"{0} >= " +startcyc.ToString();
            //bool endcycstr = endcyc == 0 ? "true":"{0} <= " + endcyc.ToString();
            List<string> modellist = new List<string>();
            foreach (var reportmodel in reportlist)
            {
                bool result = true;
                ExceptionLog.ExceptionWrite(datatype + " : " + reportmodel.datatype + ":" + data.DAL.gtsensortype.GTSensorTypeToReportString(datatype));
                result = result && (datatype == data.Model.gtsensortype._all ? true : reportmodel.datatype == data.DAL.gtsensortype.GTSensorTypeToReportString(datatype));
                result = result &&( Report == ReportType.ALL ? true : reportmodel.reportType == ReportTypeToString(Report));
                result = result &&( timeunit == TimeUnit.ALL ? true : reportmodel.timeunit == TimeUnitToString(timeunit));
                result = result &&( startcyc == 0 ? true : reportmodel.startcyc >= startcyc);
                result = result &&( endcyc == 0 ? true : reportmodel.endcyc <= endcyc);
                if (result) modellist.Add(reportmodel.filename); 
            }
           
            return modellist;

        }







        public class report
        {
            public string filename { get; set; }
            public string xmname { get; set; }
            public string datatype { get; set; }
            public string reportType { get; set; }
            public string timeunit { get; set; }
            public int startcyc { get; set; }
            public int endcyc { get; set; }
            public report(string filename, string xmname, string datatype, string reportType, string timeunit, int startcyc,int endcyc)
            {
                this.filename = filename;
                this.xmname = xmname;
                this.datatype = datatype;
                this.reportType = reportType;
                this.timeunit = timeunit;
                this.startcyc = startcyc;
                this.endcyc = endcyc;
            }
            public report()
            {
            
            }
        }


        public enum ReportType
        {
            MPSC,SPMC,ALL
        }
        public enum TimeUnit{
            CYC,Day,Week,Month,ALL
        }
        public static string ReportTypeToString(ReportType reportType)
        {
            switch (reportType)
            {
                case ReportType.MPSC: return "多点单周期";
                case ReportType.SPMC: return "单点多周期";
                default:  return "";
            }
        }
        public static string TimeUnitToString(TimeUnit timeUnit)
        {
            switch (timeUnit)
            {
                case TimeUnit.CYC: return "周期";
                case TimeUnit.Day: return "日报";
                case TimeUnit.Week: return "周报";
                case TimeUnit.Month: return "月报";
                default: return "";
            }
        }
        public static ReportType StringToReportType(string reportTypestr)
        {
            switch (reportTypestr)
            {
                case "多点单周期": return ReportType.MPSC;
                case "单点多周期": return ReportType.SPMC;
                default: return ReportType.ALL;
            }
        }
        public static TimeUnit StringToTimeUnit(string timeUnitstr)
        {
            switch (timeUnitstr)
            {
                case "周期": return TimeUnit.CYC;
                case "日报": return TimeUnit.Day;
                case "周报": return TimeUnit.Week;
                case "月报": return TimeUnit.Month;
                default: return TimeUnit.ALL;
            }
        }
    }
}