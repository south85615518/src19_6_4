﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using Tool;


namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessAlarmThread
    {
        public void TotalstationCgalarmthread(int xmno)
        {
            //ExceptionLog.ExceptionWrite("现在执行线程...");
            //Thread thread = new Thread(new ParameterizedThreadStart(TotalstationCgAlarm));
            //thread.Start(xmno);
            //ExceptionLog.ExceptionWrite("线程执行完成...");
            TotalstationCgAlarm(xmno);
        }
        public void TotalstationCgAlarm(object obj)
        {
            ExceptionLog.ExceptionWrite("现在执行成果预警线程");
            int xmno = Convert.ToInt32(obj);
            string mssg = "";
            NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessResultDataAlarmBLL processCgResultDataAlarmBLL =
             new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessResultDataAlarmBLL(xmno.ToString(), xmno);
            ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
            processCgResultDataAlarmBLL =
           new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessResultDataAlarmBLL(xmno.ToString(), xmno);
           List<string> alarmlist = processCgResultDataAlarmBLL.CgResultDataAlarm(xmno.ToString(), xmno);
           emailSendBLL.ProcessEmailSend(alarmlist, xmno.ToString(),xmno, out mssg);
        }
        public void WriteThread(object obj)
        {
            ExceptionLog.ExceptionWrite("我是第" + Convert.ToInt32(obj) + "个线程我现在睡5秒");
        }
    }
}