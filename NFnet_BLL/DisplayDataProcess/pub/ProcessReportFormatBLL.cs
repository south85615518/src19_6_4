﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.Model.fmos_obj;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessReportFormatBLL
    {


        public  void ProcessReportFormatWrite( string foramtsetting,string path)
        {
            Tool.FileHelper.FileStringWrite(foramtsetting,path);
        }
        public int ProcessReportFormatRead( string path)
        {
            string settingstr = Tool.FileHelper.ProcessSettingString(path);
            return settingstr == "" ? 0 : Convert.ToInt32(settingstr);
           
            
        }
    }
}