﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessPointAlarmValue
    {
        public static NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessPointAlarmBLL processPointAlarmBLL = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessPointAlarmBLL();
        public static PointAttribute.Model.fmos_pointalarmvalue XmPointAlarmValue(int xmno,out string mssg)
        {
            var processPointAlarmModelGetModel = new NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation.ProcessPointAlarmBLL.ProcessXmAlarmModelGetModel(xmno);
            if (processPointAlarmBLL.ProcessXmAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                return processPointAlarmModelGetModel.model;
            }
            return new PointAttribute.Model.fmos_pointalarmvalue();
        }
    }
}