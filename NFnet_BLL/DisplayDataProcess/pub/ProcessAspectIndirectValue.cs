﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessAspectIndirectValue
    {
        public static Authority.BLL.xmconnect bll = new Authority.BLL.xmconnect();
        public static int GetXmnoFromXmname(string xmname)
        {
            
            string mssg = "";
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();
            try
            {

                if (bll.GetModel(xmname, out model, out mssg))
                {
                    ExceptionLog.ExceptionWrite(string.Format("获取项目名{0}的项目编号{1}", xmname, model.xmno));
       
                    return model.xmno;
                }
                else
                {
                    mssg = "不存在该项目名称的项目编号";
                    return Convert.ToInt32(xmname);
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("获取项目名{0}的项目编号出错，错误信息:"+ex.Message,xmname));
                return -1;
            }
        }
        public static string GetXmnoFromXmnameStr(string xmname)
        {

            string mssg = "";
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();
            try
            {

                if (bll.GetModel(xmname, out model, out mssg))
                {
                    ExceptionLog.ExceptionWrite(string.Format("获取项目名{0}的项目编号{1}", xmname,model.xmno));
       
                    return model.xmno.ToString();
                }
                else
                {
                    mssg = "不存在该项目名称的项目编号";
                    return xmname;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("获取项目名{0}的项目编号出错，错误信息:" + ex.Message, xmname));
                return "-1";
            }
        }
        public static string GetXmnameFromXmno(int xmno)
        {

            string mssg = "";
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();
            try
            {

                if (bll.GetModel(xmno, out model, out mssg))
                {
                    ExceptionLog.ExceptionWrite(string.Format("获取项目编号{0}的项目名{1}", xmno, model.xmname));

                    return model.xmname.ToString();
                }
                else
                {
                    mssg = "不存在该项目编号的项目名";
                    return xmno.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("获取项目编号{0}的项目编号出错，错误信息:" + ex.Message, xmno));
                return "-1";
            }
        }
        /// <summary>
        /// 获取监测点的当前预警级别
        /// </summary>
        /// <returns></returns>
        public static int PointAlarmLevelGet(int xmno, string jclx, string pointname, out string mssg)
        {
            ProcessPointCheckBLL processPointCheckBLL = new ProcessPointCheckBLL();
            var model = new RolePointAlarmCondition(xmno, jclx, pointname);
            processPointCheckBLL.ProcessPointCheckModelGet(model, out mssg);
            return model.alarm;
        }
    }
}