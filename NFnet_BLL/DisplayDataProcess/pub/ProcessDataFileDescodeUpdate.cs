﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessDataFileDescodeUpdate
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static void DataFileDescodeTaskUpdate(NFnet_BLL.DisplayDataProcess.DataFileDescodeTaskModel model)
        {
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname) + ".txt");
            List<string> tmplist = new List<string>();
            ls.ForEach(m =>
            {
                var descodemodel = jss.Deserialize<NFnet_BLL.DisplayDataProcess.DataFileDescodeTaskModel>(m);
                if (descodemodel.path == model.path)
                {
                    descodemodel.descoding = true;
                    tmplist.Add(jss.Serialize(descodemodel));
                }
                else
                    tmplist.Add(m);

            });
            Tool.FileHelper.FileInfoListWrite(tmplist, System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname) + ".txt");
        }
    }
}