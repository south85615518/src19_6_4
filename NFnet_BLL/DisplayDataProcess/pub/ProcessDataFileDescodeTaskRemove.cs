﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Tool;
namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessDataFileDescodeTaskRemove
    {
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static void  DataFileDescodeTaskRemove(string xmname,string path)
        {
            //DataFileDescodeTaskMatchedRemove(xmname,path);
            //List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(xmname) + ".txt");
            //List<string> tmplist = new List<string>();
            //ls.ForEach(m => {
            //    var descodemodel = jss.Deserialize<NFnet_BLL.DisplayDataProcess.DataFileDescodeTaskModel>(m);
            //    if (descodemodel.path != path)
            //        tmplist.Add(m);
            //});
            //Tool.FileHelper.FileInfoListWrite(tmplist,System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(xmname) + ".txt");

            string uploadfilename = "";
            if (path.IndexOf("原始数据") != -1)
                uploadfilename = path.Substring(0, path.IndexOf("原始数据_") + 5);
            if (path.IndexOf("结果数据") != -1)
                uploadfilename = path.Substring(0, path.IndexOf("结果数据_") + 5);
            ExceptionLog.XmRecordWrite("清除上传文件名"+uploadfilename,xmname);
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(xmname) + ".txt");
            List<string> tmplist = new List<string>();
            ls.ForEach(m =>
            {
                var descodemodel = jss.Deserialize<NFnet_BLL.DisplayDataProcess.DataFileDescodeTaskModel>(m);
                if (descodemodel.path.IndexOf(uploadfilename) == -1)
                    tmplist.Add(m);
                else
                    ExceptionLog.XmRecordWrite("清除文件" + m, xmname);
            });
            Tool.FileHelper.FileInfoListWrite(tmplist, System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(xmname) + ".txt");
        }
        public static void DataFileDescodeTaskMatchedRemove(string xmname, string path)
        {
            string uploadfilename = "";
            if(path.IndexOf("原始数据") != -1)
            uploadfilename = path.Substring(0,path.IndexOf("原始数据_")+5);
            if (path.IndexOf("结果数据") != -1)
            uploadfilename = path.Substring(0,path.IndexOf("结果数据_")+5);
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(xmname) + ".txt");
            List<string> tmplist = new List<string>();
            ls.ForEach(m =>
            {
                var descodemodel = jss.Deserialize<NFnet_BLL.DisplayDataProcess.DataFileDescodeTaskModel>(m);
                if (descodemodel.path.IndexOf(uploadfilename) == -1)
                    tmplist.Add(m);
            });
            Tool.FileHelper.FileInfoListWrite(tmplist, System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(xmname) + ".txt");
        }
    }
}