﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.UserProcess;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;
using Tool.sms;
using NFnet_BLL.Other;
using NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessSmsSendBLL
    {
        public ProcessMonitorBLL processMonitorBLL = new ProcessMonitorBLL();
        public ProcessMonitorAlarmBLL monitorAlarmBLL = new ProcessMonitorAlarmBLL();
        public ProcessSmsSendRecordBLL processSmsSendRecordBLL = new ProcessSmsSendRecordBLL();
        public Isendsms send = ProcessReadSmsConfigBLL.GetSmsSender(System.AppDomain.CurrentDomain.BaseDirectory+ "smssetting/sms.txt");

        public bool ProcessSmsSend(List<string> alarmInfoList, string xmname,int xmno,out string mssg)
        {

            string alarmSendOrNot = FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "项目预警切换\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(xmname)+".txt");
            if (alarmSendOrNot == "0") { mssg = string.Format("{0}未开启预警邮件",xmname); return false; }

            ExceptionLog.ExceptionWrite("现在发送预警短信:"+string.Join(",",alarmInfoList));
            mssg = "";
            if (alarmInfoList.Count < 6) return false;
            //邮件群发

            var processXmMonitorListModel = new ProcessMonitorBLL.ProcessXmMonitorListModel(xmno);
            processMonitorBLL.ProcessXmMonitorList(processXmMonitorListModel, out mssg);


            DateTime dt = DateTime.Now;
            int sec = 0;
            foreach (Authority.Model.UnitMember unitMemberModel in processXmMonitorListModel.lum)
            {
                //监测员预警通知存库
                AuthorityAlarm.Model.monitoralarm monitorAlarm = new AuthorityAlarm.Model.monitoralarm
                {
                    xmno = xmno,
                    context = string.Join(",", alarmInfoList.Where(s => (s != "")).ToList()),
                    time = DateTime.Now,
                    confirm = false,
                    mail = false,
                    unitmember = unitMemberModel.memberno,
                    dataType = 1,
                    ForwardTime = 0,
                    mess = false,
                    mid = string.Format("M{0}{1}", unitMemberModel.memberno, DateHelper.DateTimeToString(dt.AddSeconds(++sec)))
                };
                if (!monitorAlarmBLL.ProcessMonitorAlarmAdd(monitorAlarm, out mssg))
                {
                    ExceptionLog.ExceptionWrite(mssg);
                    //return false ;
                }
                //监测员预警通知发送邮件
                int t = 0;
                while (t < 3)
                {
                    //重发三次
                    if (!send.smssend(unitMemberModel.tel, string.Join("\r\t", alarmInfoList.Where(s => (s != "")).ToList()), /*string.Format("{0}预警信息通知", xmname)*/ out mssg))
                    {
                        ExceptionLog.ExceptionWrite(mssg);
                        t++;
                    }
                    else
                    {
                        ExceptionLog.ExceptionWrite(mssg);
                        //更新短信标志
                        monitorAlarm.mess = true;
                        if (!monitorAlarmBLL.ProcessMonitorAlarmUpdate(monitorAlarm, out mssg))
                        {
                            return false ;
                        }
                        //break;
                        return true;
                    }
                }

            }
            return false;
        }

        public bool ProcessSmsSend(List<string> alarmInfoList,int xmno ,List<Authority.Model.MonitorMember> monitorMemberList   , out string mssg)
        {
            ExceptionLog.ExceptionWrite("现在发送预警短信:" + string.Join(",", alarmInfoList));
            mssg = "";
            //if (alarmInfoList.Count < 6) return false;
            //邮件群发
            try
            {
                DateTime dt = DateTime.Now;
                int sec = 0;
                foreach (Authority.Model.MonitorMember monitorModel in monitorMemberList)
                {
                    //监测员预警通知存库
                    AuthorityAlarm.Model.monitoralarm monitorAlarm = new AuthorityAlarm.Model.monitoralarm
                    {
                        xmno = xmno,
                        context = string.Join(",", alarmInfoList.Where(s => (s != "")).ToList()),
                        time = DateTime.Now,
                        confirm = false,
                        mail = false,
                        unitmember = monitorModel.userNO,
                        dataType = 1,
                        ForwardTime = 0,
                        mess = false,
                        mid = string.Format("M{0}{1}", monitorModel.userNO, DateHelper.DateTimeToString(dt.AddSeconds(++sec)))
                    };
                    if (!monitorAlarmBLL.ProcessMonitorAlarmAdd(monitorAlarm, out mssg))
                    {
                        ExceptionLog.ExceptionWrite(mssg);
                        //return false ;
                    }
                    //监测员预警通知发送邮件
                    int t = 0;
                    while (t < 3)
                    {
                        //重发三次
                        ExceptionLog.ExceptionWrite("现在开始发送短信...");
                        if (!send.smssend(monitorModel.tel, string.Join("\r\t", alarmInfoList.Where(s => (s != "")).ToList()), /*string.Format("{0}预警信息通知", xmname)*/ out mssg))
                        {
                            ExceptionLog.ExceptionWrite(mssg);
                            t++;
                        }
                        else
                        {
                            ExceptionLog.ExceptionWrite(mssg);
                            //更新短信标志
                            monitorAlarm.mess = true;
                            if (!monitorAlarmBLL.ProcessMonitorAlarmUpdate(monitorAlarm, out mssg))
                            {
                                return false;
                            }
                            //break;
                            return true;
                        }
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("短信预警发送出错,错误信息:"+ex.Message);
                return false;
            }
        }
        
        public bool ProcessSmsSend(int xmno,string messtype ,List<Authority.Model.smssendmember> smssendmemberlist, List<string> messlist,out List<string> mssglist)
        {
            string mssgstr = "";
            mssglist = new List<string>();
            string membermssg = "{0}";
            string msmsmssg = "";
            int i = 0;
            foreach (Authority.Model.smssendmember smssendmember in smssendmemberlist)
            {
                membermssg = "{0}";
                i = 0;
                foreach(string mess in messlist)
                {
                    send.smssend(smssendmember.tel, ProcessAspectIndirectValue.GetXmnameFromXmno(xmno)+" "+mess + "(" + (i + 1) + "/" + messlist.Count + ")", out mssgstr);
                    membermssg = string.Format(membermssg, mssgstr + "(" + (i + 1) + "/" + messlist.Count + ")" + "\r\n{0}");
                    var model = new AuthorityAlarm.Model.smssendrecord { xmno = xmno, messtype = messtype, sendcontext = mess, sendtime = DateTime.Now, sendto = smssendmember .name+ "[" + smssendmember.tel + "]", result = mssgstr };
                        i++;
                        processSmsSendRecordBLL.ProcessSmsSendRecordAdd(model, out msmsmssg);
                }
                mssglist.Add(membermssg.Replace("{0}",""));
            }
            return true;
        }

        public bool ProcessSmsSend( List<Authority.Model.smssendmember> smssendmemberlist, List<string> messlist, out List<string> mssglist)
        {
            string mssgstr = "";
            mssglist = new List<string>();
            string membermssg = "{0}";
            string msmsmssg = "";
            int i = 0;
            foreach (Authority.Model.smssendmember smssendmember in smssendmemberlist)
            {
                membermssg = "{0}";
                i = 0;
                foreach (string mess in messlist)
                {
                    send.smssend(smssendmember.tel, "仪器检定周期预警 " + mess + "(" + (i + 1) + "/" + messlist.Count + ")", out mssgstr);
                    membermssg = string.Format(membermssg, mssgstr + "(" + (i + 1) + "/" + messlist.Count + ")" + "\r\n{0}");
                    i++;
                    
                }
                mssglist.Add(membermssg.Replace("{0}", ""));
            }
            return true;
        }


        public List<string> smsalarmsplit(string mssgsrc)
        {
            mssgsrc.Replace("<p>", "\r\t");
            int smscont = mssgsrc.Length / 350;
            List<string> smslist = new List<string>();
            int i = 0;
            for (i = 0; i < smscont; i++)
            {
                smslist.Add(mssgsrc.Substring(i * 350, 350));
            }
            smslist.Add(mssgsrc.Substring(350 * i));
            return smslist;
        }
       


    }
}
