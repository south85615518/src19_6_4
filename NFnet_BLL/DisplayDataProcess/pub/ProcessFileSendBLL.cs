﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessFileSendBLL
    {
        public void ProcessFileSend(string filename)
        {
            List<string> ipport = FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\报表输出服务托管报表接收设置\\setting.txt");
           
            string connectmssg = "";
            IAsyncResult ar = null;
            if (TcpServer.client == null || TcpServer.client.Connected == false)
            {
                TcpServer.client = new System.Net.Sockets.TcpClient();

                connectmssg = TcpServer.ServerConncet(ipport[0], ipport[1]);
                Console.WriteLine(connectmssg);
            }
            TcpServer server = new TcpServer { StrFile = filename };
            string result = server.SendFile(ipport[0], ipport[1]);
            //ProcessPrintMssg.Print(result);
            ExceptionLog.ExceptionWrite(result);
        }
    }
}