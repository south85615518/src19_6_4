﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.pub
{
    public class ProcessFileRemove
    {
        public static void FileInfoListRemove(string settingpath, string str)
        {
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(settingpath);
            ls.Remove(str);
            Tool.FileHelper.FileInfoListWrite(ls,settingpath);
        }
    }
}