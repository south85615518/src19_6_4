﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.NGN_HXYL
{
    public class ProcessHXYLData
    {
        public static NGN.BLL.HXYL hxylbll = new NGN.BLL.HXYL();
        /// <summary>
        /// 数据展示时查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {




            if (model.type == QueryType.QT)
            {
                var processdatemdbModel = new ProcessComBLL.ProcessdatemdbModel(model.unit, model.maxTime);
                ProcessComBLL.Processdatemdb(processdatemdbModel, out mssg);
                model.startTime = processdatemdbModel.sttm;
                model.endTime = processdatemdbModel.edtm;
            }
            

                DateTime dtstart = Convert.ToDateTime(model.startTime);

                DateTime dtend = Convert.ToDateTime(model.endTime);

                model.sql = string.Format(@"select sum(val) as val,id  ,DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':00'),'%Y/%m/%d %H:%i')  as dt from hxyldata where DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i')>=DATE_FORMAT('{0}','%Y/%m/%d %H:%i') and DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i')<=date_format('{1}','%Y/%m/%d %H:%i')  
 ", dtstart, dtend);




        }
        /// <summary>
        /// 数据展示日查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreateDay(ResultDataRqcxConditionCreateCondition model)
        {




            if (model.type == QueryType.QT)
            {
                var processdatemdbModel = new ProcessComBLL.ProcessdatemdbModel(model.unit, model.maxTime);
                ProcessComBLL.Processdatemdb(processdatemdbModel, out mssg);
                model.startTime = processdatemdbModel.sttm;
                model.endTime = processdatemdbModel.edtm;
            }


            DateTime dtstart = Convert.ToDateTime(model.startTime);

            DateTime dtend = Convert.ToDateTime(model.endTime);

            model.sql = string.Format(@"select sum(val) as val,id  ,DATE_FORMAT(concat(year,'/',month,'/',day),'%Y/%m/%d')  as dt from hxyldata where DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i')>=DATE_FORMAT('{0}','%Y/%m/%d %H:%i') and DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i')<=date_format('{1}','%Y/%m/%d %H:%i')  
 ", dtstart, dtend);




        }
        /// <summary>
        /// 数据展示月查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreateMon(ResultDataRqcxConditionCreateCondition model)
        {




            if (model.type == QueryType.QT)
            {
                var processdatemdbModel = new ProcessComBLL.ProcessdatemdbModel(model.unit, model.maxTime);
                ProcessComBLL.Processdatemdb(processdatemdbModel, out mssg);
                model.startTime = processdatemdbModel.sttm;
                model.endTime = processdatemdbModel.edtm;
            }


            DateTime dtstart = Convert.ToDateTime(model.startTime);

            DateTime dtend = Convert.ToDateTime(model.endTime);

            model.sql = string.Format(@"select sum(val) as val,id ,DATE_FORMAT(concat(year,'/',month,'/',day),'%Y/%m')  as dt from hxyldata where DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i')>=DATE_FORMAT('{0}','%Y/%m/%d %H:%i') and DATE_FORMAT(concat(year,'/',month,'/',day,' ',hour,':',minute),'%Y/%m/%d %H:%i')<=date_format('{1}','%Y/%m/%d %H:%i')  
 ", dtstart, dtend);




        }
        //public ProcessHXYLIDBLL
        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessInclinometerDbFill(FillInclinometerDbFillCondition model)
        {




            ExceptionLog.ExceptionWrite("现在获取项目编号" + model.xmno + "雨量数据");
            //model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" id ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                //sql = model.rqConditionStr + "' and  " + Processquerynvlmodel.str + "  order by '年份','月份','日','时','分' ";//表名由项目任务决定
                //sql += " and " + model.rqConditionStr;
                sql = model.rqConditionStr + " and  " + Processquerynvlmodel.str + "  group by id,year,month,day,hour  order by id,year,month,day,hour ";
            }
            model.sql = sql;
            var queryylModel = new QuerystanderdbIntModel(sql,model.xmno);
            if (ProcessComBLL.Processquerystanderdb(queryylModel, out mssg))
            {

                model.dt = HXYLTableFormat(model.xmno, queryylModel.dt);
                ExceptionLog.ExceptionWrite("获取雨量数据完成");
                return true;
            }
            else
            {
                return false;
            }


        }

        public bool ProcessInclinometerDbFillDay(FillInclinometerDbFillCondition model)
        {




            ExceptionLog.ExceptionWrite("现在获取项目编号" + model.xmno + "雨量数据");
            //model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" id ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                //sql = model.rqConditionStr + "' and  " + Processquerynvlmodel.str + "  order by '年份','月份','日','时','分' ";//表名由项目任务决定
                //sql += " and " + model.rqConditionStr;
                sql = model.rqConditionStr + " and  " + Processquerynvlmodel.str + "  group by id,year,month,day  order by id,year,month,day ";
            }
            model.sql = sql;
            var queryylModel = new QuerystanderdbIntModel(sql,model.xmno);
            if (ProcessComBLL.Processquerystanderdb(queryylModel, out mssg))
            {

                model.dt = HXYLTableFormat(model.xmno, queryylModel.dt);
                ExceptionLog.ExceptionWrite("获取雨量数据完成");
                return true;
            }
            else
            {
                return false;
            }


        }

        public bool ProcessInclinometerDbFillMon(FillInclinometerDbFillCondition model)
        {




            ExceptionLog.ExceptionWrite("现在获取项目编号" + model.xmno + "雨量数据");
            //model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" id ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                //sql = model.rqConditionStr + "' and  " + Processquerynvlmodel.str + "  order by '年份','月份','日','时','分' ";//表名由项目任务决定
                //sql += " and " + model.rqConditionStr;
                sql = model.rqConditionStr + " and  " + Processquerynvlmodel.str + "  group by id,year,month  order by id,year,month ";
            }
            model.sql = sql;
            var queryylModel = new QuerystanderdbIntModel(sql,model.xmno);
            if (ProcessComBLL.Processquerystanderdb(queryylModel, out mssg))
            {

                model.dt = HXYLTableFormat(model.xmno, queryylModel.dt);
                ExceptionLog.ExceptionWrite("获取雨量数据完成");
                return true;
            }
            else
            {
                return false;
            }


        }
        public static string mssg = "";
        public DataTable HXYLTableFormat(int xmno, DataTable dt)
        {
            if (dt.Rows.Count == 0) return new DataTable();
            ExceptionLog.ExceptionWrite("雨量数据表格式转换");
            DataTable tb = new DataTable();
            tb.Columns.Add("point_name");
            tb.Columns.Add("rvl");
            tb.Columns.Add("time");
            DataView dv = new DataView(dt);
            string tmp = dv[0]["id"].ToString();
            string tmpno = HXYLPointname(xmno, tmp, out mssg);
            foreach (DataRowView drv in dv)
            {
                DataRow dr = tb.NewRow();
                if (drv["id"].ToString() != tmp)
                {
                    tmpno = HXYLPointname(xmno, drv["id"].ToString(), out mssg);
                    tmp = drv["id"].ToString();
                }
                dr[0] = tmpno;
                dr[1] = drv["val"].ToString();
                dr[2] = drv["dt"].ToString();
                tb.Rows.Add(dr);
            }
            ExceptionLog.ExceptionWrite("雨量数据表格式转换完成");
            return tb;
        }
        public bool PreAcVal(PreAcValModel model,out string mssg)
        {
            NGN.Model.hxyl pretimemodel = new NGN.Model.hxyl();
            if (hxylbll.PreAcVal(model.model, out pretimemodel, out mssg))
            {
                model.pretimemodel = pretimemodel;
                return true;
            }
            return false;
        }
        public class PreAcValModel
        {
            public NGN.Model.hxyl model { get; set; }
            public NGN.Model.hxyl pretimemodel { get; set; }
            public PreAcValModel(NGN.Model.hxyl model)
            {
                this.model = model;
            }
        }

        public static ProcessHXYLBLL processHXYLBLL = new ProcessHXYLBLL();
        public string HXYLPointname(int xmno, string id, out string mssg)
        {
            ExceptionLog.ExceptionWrite(string.Format("获取项目编号{0}设备号为{1}的点名", xmno, id));
            var hXYLPointmodel = new NFnet_BLL.DisplayDataProcess.NGN_HXYL.ProcessHXYLBLL.HXYLPointmodel(xmno, id);
            if (processHXYLBLL.HXYLPointname(hXYLPointmodel, out mssg))
                return hXYLPointmodel.pointname;
            return "";
        }

        //public bool ProcesshxylAlarmModelListGet(ProcesshxylAlarmModelListGetModel model, out string mssg)
        //{
        //    List<MDBDATA.Model.hxyl> hxylmodel = null;
        //    if (hxylbll.GetList(model.pointnamelist, out hxylmodel, out mssg))
        //    {
        //        model.model = hxylmodel;
        //        return true;
        //    }
        //    return false;

        //}
        //public class ProcesshxylAlarmModelListGetModel
        //{
           
        //    public List<MDBDATA.Model.hxyl> model { get; set; }
        //    public List<string> pointnamelist { get; set; }
        //    public ProcesshxylAlarmModelListGetModel( List<string> pointnamelist)
        //    {
               
        //        this.pointnamelist = pointnamelist;
        //    }
        //}
        //public bool GetAlarmTableCont(GetAlarmTableContModel model, out string mssg)
        //{
        //    int cont = 0;
        //    if (hxylbll.GetAlarmTableCont(model.idlist, out cont, out mssg))
        //    {
        //        model.cont = cont;
        //        return true;
        //    }
        //    return false;
        //}
        //public class GetAlarmTableContModel
        //{
        //    public List<string> idlist { get; set; }
        //    public int cont { get; set; }
        //    public GetAlarmTableContModel(List<string> idlist)
        //    {
        //        this.idlist = idlist;
        //    }
        //}

        //public bool ProcessHXYLDataDelete(DataDeleteCondition model, out string mssg)
        //{
        //    return hxylbll.Delete(model.point_name, model.dt, out mssg);
        //}


    }


}