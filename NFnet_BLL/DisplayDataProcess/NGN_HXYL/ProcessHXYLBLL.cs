﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.NGN_HXYL
{
    public class ProcessHXYLBLL
    {
        
        public static NGN.BLL.HXYL hxylbll = new NGN.BLL.HXYL();

        public bool ProcessHXYLAdd(NGN.Model.hxyl model, out string mssg)
        {
            return hxylbll.Addhxyl(model, out mssg);
        }
        public bool HXYLPointLoad(HXYLPointLoadModel model,out string mssg)
        {
            List<string> ls = new List<string>();
            if (hxylbll.HXYLPointLoad(model.xmno, out ls, out mssg))
            {
                model.pointnamelist = ls;
                return true;
            }
            return false;
        }
         public class HXYLPointLoadModel
        {
            public int xmno { get; set; }
            public List<string> pointnamelist { get;set;}
            public HXYLPointLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
         public bool HXYLID(HXYLIDmodel model, out string mssg)
        {
            string id = "";
            if (hxylbll.HXYLID(model.xmno, model.pointname,out id, out mssg))
            {
                model.id = id;
                return true;
            }
            return false;
        }
        public class HXYLIDmodel {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public string id { get; set; }
            public HXYLIDmodel(int xmno, string pointname)
            {
                this.xmno = xmno;
                this.pointname = pointname;
            }
        }
        public bool HXYLPointname(HXYLPointmodel model, out string mssg)
        {
            string pointname = "";
            if (hxylbll.HXYLPointName(model.xmno, model.id, out pointname, out mssg))
            {
                model.pointname = pointname;
                return true;
            }
            return false;
        }
        public class HXYLPointmodel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public string id { get; set; }
            public HXYLPointmodel(int xmno, string id)
            {
                this.xmno = xmno;
                this.id = id;
            }
        }
        public bool ProcessHXYLMaxTime(ProcessHXYLMaxTimeModel model,out string mssg)
        {
            DateTime dt = new DateTime();
            if (hxylbll.HXYLMaxTime(model.pointnostr,model.xmno, out dt, out mssg))
            {
                model.maxTime = dt;
                return true;
            }
            return false;
        }
        public class ProcessHXYLMaxTimeModel
        {
            public string pointnostr { get; set; }
            public int xmno { get; set; }
            public DateTime maxTime { get; set; }
            public ProcessHXYLMaxTimeModel(string pointnostr,int xmno)
            {
                this.pointnostr = pointnostr;
                this.xmno = xmno;
            }
        }

        
        

        public bool ProcessHXYLDataTime(HXYLDataTimeCondition model, out string mssg)
        {

            NGN.Model.hxyl hxylDataModel = new NGN.Model.hxyl();
            if (hxylbll.GetModel( model.pointid, model.dt, out hxylDataModel, out mssg))
            {
                model.model = hxylDataModel;
                return true;

            }
            return false;
        }
        public class HXYLDataTimeCondition
        {
            public int pointid { get; set; }
            public DateTime dt { get; set; }
            public NGN.Model.hxyl model { get; set; }
            public HXYLDataTimeCondition(int pointid, DateTime dt)
            {
                this.pointid = pointid;
                this.dt = dt;
            }
        }
        public bool ProcessPointNewestDateTimeGet(ProcessPointNewestDateTimeGetModel model, out string mssg)
        {

            DateTime dt = new DateTime();
            if (hxylbll.PointNewestDateTimeGet(model.pointid, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessPointNewestDateTimeGetModel
        {
            public int pointid { get; set; }
            public DateTime dt { get; set; }
            public ProcessPointNewestDateTimeGetModel(int pointid)
            {
                this.pointid = pointid;
            }
        }
    }
}