﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool.com;

namespace NFnet_BLL.DisplayDataProcess.NGN_HXYL
{
    public class ProcessNGNCommendBLL
    {
        public IEEE754Helper iEEE754Helper = new IEEE754Helper();
        public Tool.com.HexHelper hexHelper = new HexHelper();
        public bool ProcessNGNCommendCheck(byte[] bytes, out string mssg)
        {
            //雨量自报的命令组成 
            /*
             * 包首（1+1+2+2+1）FF 1C 设备号*2 接收字节长度*2 
             * 时间戳(1+1+4)
             * 电池电压(2+4)
             * 实时模数(2+4)
             * 实时温度(2+4)
             * CRC校验(2)
             */
            if (Tool.com.HexHelper.IsByteContainByteArray(bytes, new byte[2] { 0x14, 0x64 }))
            {
                
                    mssg = string.Format("经位数校验{0}数据是雨量自报数据", Tool.com.HexHelper.ByteToHexString(bytes));
                    return true;
               
            }
           
            mssg = string.Format("经位数校验{0}该数据不是雨量自报数据", Tool.com.HexHelper.ByteToHexString(bytes));
            return false;
        }
        #region 时间
        //获取设备时间
        public bool ProcessNGNTime(byte[] bytes, out DateTime dt, out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            return false;
        }
        //生成设备时间设置命令
        public bool PrcoessNGNTimeSetCommendCreate(int deviceno, Dictionary<string, int> FCDictionary, out byte[] commend, out string mssg)
        {
            commend = null;
            mssg = "";
            return false;

        }
        //设备时间设置成功返回指令确认
        public bool ProcessNGNTimeSetSuccessComfire(int deviceno, Dictionary<string, int> FCDictionary, out string mssg)
        {
            mssg = "";
            return false;
        }

        #endregion
        #region 采集间隔

        //生成设备采集间隔设置命令
        public bool PrcoessNGNIntervalSetCommendCreate(int deviceno, Dictionary<string, int> FCDictionary, out byte[] commend, out string mssg)
        {
            commend = null;
            mssg = "";
            return false;

        }

        //设备时间设置成功返回指令确认
        public bool ProcessNGNIntervalSetSuccessComfire(int deviceno, Dictionary<string, int> FCDictionary, out string mssg)
        {
            mssg = "";
            return false;
        }
        #endregion

        #region 数据解析

        //解析数据获取水位原始值和温度原始值
        public void PrcoessNGNDataDescode(byte[] data, Dictionary<string, int> FCDictionary, out hxyl hxyl)
        {

            //FF 60 00 A9 00 27 10 01 44 57 67 F7 0A 02 44 56 86 41 3C 03 64 07 81 85 40 11 64 00 00 00 00 12 64 9A 99 88 C3 B5 24
            //ngnorgldata = new DTU.Model.dtudata();
            int timeindex = 0,voltageindex = 0,ylindex = 0;
            hxyl = new hxyl();
            hxyl.id = Convert.ToInt32(Tool.com.HexHelper.oxdata(data, 2, 2), 16);
            timeindex = Tool.com.HexHelper.ByteIndexByteArray(data, new byte[2] { 0x01, 0x44 });
            if(timeindex != -1)
                hxyl.dt = Tool.DateHelper.Unixtimestamp(Convert.ToInt32(Tool.com.HexHelper.oxdata(data, timeindex + 2, 4), 16));

            voltageindex  = Tool.com.HexHelper.ByteIndexByteArray(data, new byte[2] { 0x03, 0x64 });
            if (voltageindex != -1)
                hxyl.voltage = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, voltageindex + 2, 4));

            ylindex = Tool.com.HexHelper.ByteIndexByteArray(data, new byte[2] { 0x14, 0x64 });
            if (ylindex != -1)
                hxyl.ylval = iEEE754Helper.INT0XToIEEE754(Tool.com.HexHelper.oxdata(data, ylindex + 2, 4));

           

        }
        public class hxyl
        {
            public int id { get; set; }
            public DateTime dt { get; set; }
            public double voltage { get; set; }
            public double ylval { get; set; }
           

        }




        #endregion
    }
}