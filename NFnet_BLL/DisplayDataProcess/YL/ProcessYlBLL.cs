﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.YL
{
    /// <summary>
    /// 雨量数据业务逻辑处理
    /// </summary>
    public class ProcessYlBLL
    {
        public static sensorDAL.BLL.YlBLL ylBLL = new sensorDAL.BLL.YlBLL();
        /// <summary>
        /// 雨量点名获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessYlPointLoad(ProcessYlPointLoadModel model,out string mssg)
        {
            List<string> ls = null;
            if (ylBLL.YlPointLoadBLL(model.xmname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 雨量点名获取类
        /// </summary>
        public class ProcessYlPointLoadModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 雨量点名列表
            /// </summary>
            public List<string> ls { get; set; }
            public ProcessYlPointLoadModel(string xmname, List<string> ls)
            {
                this.xmname = xmname;
                this.ls = ls;
            }
        }
    }
}