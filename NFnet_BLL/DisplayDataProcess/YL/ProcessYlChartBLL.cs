﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using NFnet_MODAL;

namespace NFnet_BLL.DisplayDataProcess.YL
{
    /// <summary>
    /// 雨量曲线业务逻辑处理
    /// </summary>
    public class ProcessYlChartBLL
    {
        /// <summary>
        /// 雨量曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrRAIN(ProcessChartCondition model, out string mssg)
        {
            DataTable dt = new DataTable();
            string raines = model.pointname;
            string sql = model.sql;
            mssg = "";
            //分模块号显示
            raines = raines.Replace("(", "");
            raines = raines.Replace(")", "");
            string[] rainarr = raines.Split(',');
            if (raines != "" && raines != null)
            {
                var processquerystanderdbModel = new QuerystanderdbModel(sql, model.xmname);
                if (!ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
                {
                    //错误信息反馈
                }
                dt = processquerystanderdbModel.dt;
                List<serie> lst = new List<serie>();
                for (int i = 0; i < rainarr.Length; i++)
                {
                    serie sr = new serie();
                    //sr.Stype = "雨量";
                    DataView filter = new DataView(dt);
                    string filterstr = "RName in (" + rainarr[i] + ")";
                    filter.RowFilter = filterstr;
                    sr.Name = rainarr[i].Replace("'", "");
                    //加载点
                    formatdat(filter, sr);
                    lst.Add(sr);


                }
                model.series = lst;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 有数据表生成雨量曲线
        /// </summary>
        /// <param name="dv">数据表视图</param>
        /// <param name="sr">曲线</param>
        public static void formatdat(DataView dv, serie sr)
        {
            sr.Stype = "雨量";
            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["MonitorTime"].ToString();
                string v = drv["Rainfall"].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }




    }
}