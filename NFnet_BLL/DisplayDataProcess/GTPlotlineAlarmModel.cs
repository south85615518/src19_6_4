﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GTPlotlineAlarmModel
    {
        public string pointname { get; set; }
        public global::data.Model.gtalarmvalue firstalarm { get; set; }
        public global::data.Model.gtalarmvalue secalarm { get; set; }
        public global::data.Model.gtalarmvalue thirdalarm { get; set; }

    }
}