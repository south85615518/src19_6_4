﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SenorDataReportTableCreateCondition
    {
        public string sql { get; set; }
        public string xmname { get; set; }
        public int xmno { get; set; }
        public DataTable dt { get; set; }
        public SenorDataReportTableCreateCondition(string sql, string xmname, int xmno)
        {
            this.sql = sql;
            this.xmname = xmname;
            this.xmno = xmno;
        }
    }
}