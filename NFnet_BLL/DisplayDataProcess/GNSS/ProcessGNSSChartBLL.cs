﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;
using NFnet_BLL.Other;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    /// <summary>
    /// 水位曲线业务逻辑处理类
    /// </summary>
    public class ProcessGNSSChartBLL
    {
        #region GNSS
        /// <summary>
        /// 表面位移曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrGNSS(SerializestrBMWYCondition model, out string mssg)
        {
            mssg = "";
            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            string sql = model.sql;
            string pointname = model.pointname;
            zuxyz[] zus = model.zus;
            string xmname = model.xmname;
            if (pointname != "" && pointname != null && zus != null)
            {

                var queryremoteModel = new QueryremoteModel(sql, "smos_client");
                if (!ProcessComBLL.Processqueryremotedb(queryremoteModel, out mssg))
                {
                   
                }

                dt = queryremoteModel.dt;
                //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[pointnamezu.Length];
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
                for (int k = 0; k < pointnamezu.Length; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(pointnamezu[k], dvzu[k]);
                }
                List<serie> lst = new List<serie>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < pointnamezu.Length; d++)
                    {
                        string filter =
                        dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d];
                        int cont = dvzu[d].Count;
                        //if (cont != 0)
                        //{

                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie st = new serie();//创建曲线
                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = pointnamezu[d].Replace("'", "") + "_" + rplname(zus[z].Bls[u]);
                            switchnez(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                        //}

                    }



                }
                model.series = lst;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 重命名分量标签
        /// </summary>
        /// <param name="blm"></param>
        /// <returns></returns>
        public static string rplname(string blm)
        {
            if (blm.IndexOf("This_dE") != -1)
            {
                return "△E";
            }
            else if (blm.IndexOf("This_dN") != -1)
            {
                return "△N";
            }
            else if (blm.IndexOf("This_dZ") != -1)
            {
                return "△Z";
            }
            else if (blm.IndexOf("Ac_dE") != -1)
            {
                return "∑△E";
            }
            else if (blm.IndexOf("Ac_dN") != -1)
            {
                return "∑△N";
            }
            else if (blm.IndexOf("Ac_dZ") != -1)
            {
                return "∑△Z";
            }
            return blm;

        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dv">结果数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量名称</param>
        public static void switchnez(DataView dv, serie st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt[len];
            if (len != 0)
            {

                if (xyzes == "this_x")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "this_x");
                        i++;
                    }
                }
                else if (xyzes == "this_y")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "this_y");
                        i++;
                    }
                }
                else if (xyzes == "this_z")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "this_z");
                        i++;
                    }
                }
                else if (xyzes == "l_x")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_x");
                        i++;
                    }
                }
                else if (xyzes == "l_y")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_y");
                        i++;
                    }
                }
                else if (xyzes == "l_z")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_z");
                        i++;
                    }
                }
                


            }

        }
        /// <summary>
        /// 数据行生成曲线数据点
        /// </summary>
        /// <param name="drv"></param>
        /// <param name="sxtj"></param>
        /// <returns></returns>
        public static pt wgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string dtime = drv["time"].ToString();
            DateTime d = Convert.ToDateTime(dtime);
            int year = d.Year;
            int mon = d.Month;
            int day = d.Day;
            int hour = d.Hour;
            int minute = d.Minute;
            int second = d.Second;
            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt { Dt = d, Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        #endregion
    }
}