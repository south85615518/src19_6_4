﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.GNSS
{
    public class ProcessGNSSBLL
    {
        public GPS.BLL.smos_module_gnss_device bll = new GPS.BLL.smos_module_gnss_device();
        public GPS.BLL.gnssdatareport gnssdatabll = new GPS.BLL.gnssdatareport();
        public static GPS.BLL.gnsstimeinterval gnsstimeintervalBLL = new GPS.BLL.gnsstimeinterval();
        public bool ProcessGNSSPointLoad(ProcessGNSSPointLoadModel model,out string mssg)
        {
            List<string> pointlist = new List<string>();
            if (bll.gnsspointnameload(model.xmno, out pointlist, out mssg))
            {
                model.pointlist = pointlist;
                return true;
            }
            return false;
        }
        public class ProcessGNSSPointLoadModel
        {
            public int xmno { get; set; }
            public List<string> pointlist { get; set; }
            public ProcessGNSSPointLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool ProcessPointNewestDateTimeGet(InclinometerPointNewestDateTimeCondition model, out string mssg)
        {

            DateTime dt = new DateTime();
            if (gnssdatabll.PointNewestDateTimeGet(model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;


        }


        public bool ProcessSenorDataTime(ProcessSenorDataTimeModel model, out string mssg)
        {

            GPS.Model.gnssdatareport senorDataModel = new GPS.Model.gnssdatareport();
            if (gnssdatabll.GetModel(model.pointname, model.dt, out senorDataModel, out mssg))
            {
                model.model = senorDataModel;
                return true;

            }
            return false;
        }
        public class ProcessSenorDataTimeModel
        {
            public string pointname { get; set; }
            public DateTime dt { get; set; }
            public GPS.Model.gnssdatareport model { get; set; }
            public ProcessSenorDataTimeModel(string pointname, DateTime dt)
            {
                this.pointname = pointname;
                this.dt = dt;
            }
        }

        /// <summary>
        /// 时间间隔获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessGNSSTimeInterval(gnsstimeintervalModel model, out string mssg)
        {
            DataTable dt = null;
            if (gnsstimeintervalBLL.TableLoad(model.pageIndex, model.rows, model.xmno,  model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 时间间隔表获取
        /// </summary>
        public class gnsstimeintervalModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public gnsstimeintervalModel(int xmno,  int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessGNSSTimeIntevalAdd(GPS.Model.gnsstimeinterval model, out string mssg)
        {
            return gnsstimeintervalBLL.Add(model, out mssg);
        }


        
    }
}