﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.Other;
using NFnet_MODAL;
namespace NFnet_BLL.DisplayDataProcess.WaterLevel
{
    public class ProcessGNSSCom
    {

       
        //<summary>
        //数据展示日期查询条件生成
        //</summary>
        //<param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {

            switch (model.type)
            {
                case QueryType.RQCX:
                    string sqlsttm = "";
                    string sqledtm = "";
                    string mssg = "";
                    string cycmin = "";
                    string cycmax = "";
                    var querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.startTime, ">=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqlsttm = querynvlmodel.str;
                    }
                    querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.endTime, "<=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqledtm = querynvlmodel.str;
                    }
                    string rqConditionStr = sqlsttm + " and " + sqledtm + "     order by #_point,#_date asc ";


                    model.sql = rqConditionStr;
                    break;

                case QueryType.QT:
                    var processdateswdlModel = new ProcessComBLL.ProcessdateswdlModel("#_date", model.unit, model.maxTime);
                    if (ProcessComBLL.Processdateswdl(processdateswdlModel, out mssg))
                    {
                        model.startTime = processdateswdlModel.sttm;
                        model.endTime = processdateswdlModel.edtm;
                        //model.sql = processdateswdlModel.sql;
                    }
                    break;


            }
        }

        //public bool SenordataTableLoad(SenordataTableLoadModel model, out string mssg)
        //{

        //    if (model.role == Role.superviseModel || model.tmpRole)
        //    {
        //        return processCgDTUDATABLL.ProcessSenorDataLoad(model.model, out mssg);
        //    }
        //    return processDTUDATABLL.ProcessSenorDataLoad(model.model, out mssg);
        //}
        //public class SenordataTableLoadModel
        //{
        //    public SenorDataLoadCondition model { get; set; }
        //    public bool tmpRole { get; set; }
        //    public Role role { get; set; }
        //    public SenordataTableLoadModel(SenorDataLoadCondition model, bool tmpRole, Role role)
        //    {
        //        this.model = model;
        //        this.tmpRole = tmpRole;
        //        this.role = role;
        //    }
        //}

        //public bool ProcessSenorDataRecordsCount(ProcessSenorDataRecordsCountModel model, out string mssg)
        //{
        //    if (model.role == Role.superviseModel || model.tmpRole)
        //    {
        //        return processCgDTUDATABLL.ProcessSenorDataRecordsCount(model.model, out mssg);
        //    }
        //    return processDTUDATABLL.ProcessSenorDataRecordsCount(model.model, out mssg);
        //}
        public class ProcessSenorDataRecordsCountModel
        {
            public SenorDataCountLoadCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessSenorDataRecordsCountModel(SenorDataCountLoadCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }


        //public bool ProcessDTUSenorDbFill(ProcessDTUSenorDbFillModel model)
        //{

        //    if (model.role == Role.superviseModel || model.tmpRole)
        //    {
        //        return processCgDTUDATABLL.ProcessInclinometerDbFill(model.model);
        //    }
        //    else
        //        return processDTUDATABLL.ProcessInclinometerDbFill(model.model);
        //}
        public class ProcessDTUSenorDbFillModel
        {
            public FillInclinometerDbFillCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessDTUSenorDbFillModel(FillInclinometerDbFillCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }



        public class ProcessSenorPointNameCycListLoadModel
        {
            public SenorPointNameCycListCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessSenorPointNameCycListLoadModel(SenorPointNameCycListCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }


        //public class ProcessDTUSenorDbFillModel
        //{
        //    public FillInclinometerDbFillCondition model { get; set; }
        //    public Role role { get; set; }
        //    public bool tmpRole { get; set; }
        //    public ProcessDTUSenorDbFillModel(FillInclinometerDbFillCondition model, Role role, bool tmpRole)
        //    {
        //        this.model = model;
        //        this.role = role;
        //        this.tmpRole = tmpRole;
        //    }
        //}


        /// <summary>
        /// 获取fmos组号
        /// </summary>
        /// <returns></returns>
        public zuxyz[] getGNSSzu()
        {

            string[] XzBlAry = { "this_x", "this_y", "this_z", "l_x","l_y","l_z" };
            List<zuxyz> zusls = new List<zuxyz>();
            List<string> bc = new List<string>();
            List<string> lj = new List<string>();
            for (int i = 0; i < XzBlAry.Length; i++)
            {
                if ((XzBlAry[i] == "this_x") || (XzBlAry[i] == "this_y") || (XzBlAry[i] == "this_z"))
                {
                    bc.Add(XzBlAry[i]);
                }

                if ((XzBlAry[i] == "l_x") || (XzBlAry[i] == "l_y") || (XzBlAry[i] == "l_z"))
                {
                    lj.Add(XzBlAry[i]);
                }
               
            }
            if (bc.Count != 0)
            {
                zuxyz zu = new zuxyz { Bls = bc.ToArray<string>(), Name = "本次变化量" };
                zusls.Add(zu);
            }
            if (lj.Count != 0)
            {
                zuxyz zu = new zuxyz { Bls = lj.ToArray<string>(), Name = "累计变化" };
                zusls.Add(zu);
            }
            
            return zusls.ToArray<zuxyz>();


        }
        //public bool ProcessPointNewestDateTimeGet(ProcessPointNewestDateTimeGetModel model, out string mssg)
        //{
        //    if (model.role == Role.superviseModel || model.tmpRole == false)
        //    {
        //        return processCgDTUDATABLL.ProcessPointNewestDateTimeGet(model.model, out mssg);
        //    }
        //    return processDTUDATABLL.ProcessPointNewestDateTimeGet(model.model, out mssg);
        //}
        public class ProcessPointNewestDateTimeGetModel
        {
            public InclinometerPointNewestDateTimeCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessPointNewestDateTimeGetModel(InclinometerPointNewestDateTimeCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }
        public bool ProcessSenorDataTime(ProcessSenorDataTimeModel model, out string mssg)
        {
            mssg = "";
            //if (model.role == Role.superviseModel || model.tmpRole == false)
            //{
            //    return processDTUDATABLL.(model.model,out mssg);
            //}
            //return processCgDTUDATABLL.ProcessSenorDataTime(model.model, out mssg);
            return false;
        }
        public class ProcessSenorDataTimeModel
        {
            public SenorDataTimeCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessSenorDataTimeModel(SenorDataTimeCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }
        //public bool ProcessSenorMaxTime(ProcessSenorMaxTimeModel model, out string mssg)
        //{
        //    mssg = "";

        //    if (model.role == Role.superviseModel || model.tmpRole)
        //    {
        //        return processCgDTUDATABLL.ProcessSenorMaxTime(model.model, out mssg);
        //    }
        //    return processDTUDATABLL.ProcessSenorMaxTime(model.model, out mssg);

        //}
        public class ProcessSenorMaxTimeModel
        {
            public SenorMaxTimeCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessSenorMaxTimeModel(SenorMaxTimeCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }

        //public bool ProcessSenorMaxTime(ProcessSenorPointMaxTimeModel model, out string mssg)
        //{
        //    mssg = "";

        //    if (model.role == Role.superviseModel || model.tmpRole)
        //    {
        //        return processCgDTUDATABLL.ProcessSenorMaxTime(model.model, out mssg);
        //    }
        //    return processDTUDATABLL.ProcessSenorMaxTime(model.model, out mssg);

        //}
        public class ProcessSenorPointMaxTimeModel
        {
            public ProcessDTUDATABLL.SenorPointMaxTimeModel model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessSenorPointMaxTimeModel(ProcessDTUDATABLL.SenorPointMaxTimeModel model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }


        //public bool ProcessSenorDataReportTableCreate(processResultDataReportTableCreateModel model, out string mssg)
        //{
        //    if (model.role == Role.superviseModel || model.tmpRole)
        //    {
        //        return processCgDTUDATABLL.ProcessResultDataReportTableCreate(model.model, out mssg);

        //    }
        //    return processDTUDATABLL.ProcessResultDataReportTableCreate(model.model, out mssg);

        //}
        public class processResultDataReportTableCreateModel
        {
            public SenorDataReportTableCreateCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public processResultDataReportTableCreateModel(SenorDataReportTableCreateCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }

    }
}