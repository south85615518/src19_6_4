﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Tool;

namespace NFnet_BLL.DisplayDataProcess
{
    /// <summary>
    /// 打印条件类
    /// </summary>
    public abstract class PrintCondition
    {
        /// <summary>
        /// 起始日期
        /// </summary>
        public string startTime { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public string endTime { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 表头
        /// </summary>
        public string tabHead { get; set; }
        /// <summary>
        /// 监测类型
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 生成数据表的SQL语句
        /// </summary>
        public string sql { get; set; }
        /// <summary>
        /// 生成的数据表
        /// </summary>
        public DataTable dt { get; set; }
        /// <summary>
        /// 报表标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// EXCEL工作簿名
        /// </summary>
        public string sheetName { get; set; }
        /// <summary>
        /// 工作簿的序号
        /// </summary>
        public int index { get; set; }
        /// <summary>
        /// 分表字段
        /// </summary>
        public string splitname { get; set; }
        /// <summary>
        /// EXCEL路径
        /// </summary>
        public string xlspath { get; set; }
        
    }
}