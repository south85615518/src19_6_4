﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GTSettlementPlotlineAlarmModel
    {
        public string pointname { get; set; }
        public global::data.Model.gtsettlementalarmvalue firstalarm { get; set; }
        public global::data.Model.gtsettlementalarmvalue secalarm { get; set; }
        public global::data.Model.gtsettlementalarmvalue thirdalarm { get; set; }

    }
}