﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    public class FillBKGDbFillCondition
    {
            public string pointname { get; set; }
            public string rqConditionStr { get; set; }
            public string xmname { get; set; }
            public string sql { get; set; }
            public DataTable dt { get; set; }
            public FillBKGDbFillCondition(string pointname, string rqConditionStr, string xmname)
            {
                this.pointname = pointname;
                this.rqConditionStr = rqConditionStr;
                this.xmname = xmname;
            }
    }
}