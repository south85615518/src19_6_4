﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    /// <summary>
    /// 数据表记录查询类
    /// </summary>
    public abstract class TabCountSearchCondition
    {
        public string xmname { get; set; }
        public string tabCount { get; set; }
    }
}