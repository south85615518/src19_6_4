﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class DateTimeToCycCondition
    {
        public string xmname { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string pointstr { get; set; }
        public List<string> cycList { get; set; }
        public DateTimeToCycCondition(string xmname, string startTime, string endTime, string pointstr)
        {
            this.xmname = xmname;
            this.startTime = startTime;
            this.endTime = endTime;
            this.pointstr = pointstr;
        }
    }
}