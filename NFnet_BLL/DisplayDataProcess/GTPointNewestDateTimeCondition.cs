﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GTPointNewestDateTimeCondition
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public data.Model.gtsensortype datatype { get; set; }
        public GTPointNewestDateTimeCondition(string xmname, string pointname, data.Model.gtsensortype datatype)
        {
            this.xmname = xmname;
            this.pointname = pointname;
            this.datatype = datatype;
        }
        public GTPointNewestDateTimeCondition(int xmno, string pointname)
        {
            this.xmno = xmno;
            this.pointname = pointname;
        }
    }
}