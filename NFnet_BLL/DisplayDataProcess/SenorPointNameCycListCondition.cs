﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SenorPointNameCycListCondition
    {
        public int xmno { get; set; }
        public string pointname { get; set; }
        public List<string> ls { get; set; }
        public SenorPointNameCycListCondition(int xmno, string pointname)
        {
            this.xmno = xmno;
            this.pointname = pointname;
        }
    }
}