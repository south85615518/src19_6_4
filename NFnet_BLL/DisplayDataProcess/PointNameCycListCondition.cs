﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class PointNameCycListCondition
    {
        public string xmname { get; set; }
        public string pointname { get; set; }
        public List<string> ls { get; set; }
        public PointNameCycListCondition(string xmname,string pointname)
        {
            this.xmname = xmname;
            this.pointname = pointname;
        }
    }
}