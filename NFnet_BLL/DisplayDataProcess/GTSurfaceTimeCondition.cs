﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GTSurfaceTimeCondition
    {
        public int xmno { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public data.Model.gtsensortype datatype{ get; set;}
        public GTSurfaceTimeCondition(int xmno, string pointname, data.Model.gtsensortype datatype)
        {
            this.xmno = xmno;
            this.pointname = pointname;
            this.datatype = datatype;
        }
        public GTSurfaceTimeCondition(int xmno, data.Model.gtsensortype datatype)
        {
            this.xmno = xmno;
            this.datatype = datatype;
        }
        public GTSurfaceTimeCondition()
        {
            
        }
    }
}