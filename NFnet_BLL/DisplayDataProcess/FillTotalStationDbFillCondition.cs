﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DisplayDataProcess
{
    public class FillTotalStationDbFillCondition
    {
            public string pointname { get; set; }
            public string rqConditionStr { get; set; }
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string sql { get; set; }
            //public string sql_series { get; set; }
            //public string sql_settlement { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public DataTable dt { get; set; }
            public FillTotalStationDbFillCondition(string pointname, string rqConditionStr, string xmname)
            {
                this.pointname = pointname;
                this.rqConditionStr = rqConditionStr;
                this.xmname = ProcessAspectIndirectValue.GetXmnoFromXmnameStr(xmname);
            }
            public FillTotalStationDbFillCondition(string pointname, string rqConditionStr, int xmno)
            {
                this.pointname = pointname;
                this.rqConditionStr = rqConditionStr;
                this.xmno = xmno;
            }
            public FillTotalStationDbFillCondition(string pointname, string rqConditionStr, int xmno, data.Model.gtsensortype datatype)
            {
                this.pointname = pointname;
                this.rqConditionStr = rqConditionStr;
                this.xmno = xmno;
                this.datatype = datatype;
            }
    }
}