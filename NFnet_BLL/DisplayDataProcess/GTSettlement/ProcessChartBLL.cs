﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;
using NFnet_BLL.Other;
using System.Data;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_BLL.DisplayDataProcess.GTSettlement
{
    /// <summary>
    /// 水位曲线业务逻辑处理类
    /// </summary>
    public partial class ProcessGTSettlementChartBLL
    {
        /// <summary>
        /// 水位曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrGTSettlementData(ProcessChartCondition model, out string mssg)
        {

                mssg = "";
                string xmname = model.xmname;
                DataTable dt = new DataTable();
                string mkhs = model.pointname;
                string sql = model.sql;

                 ProcessResultDataBLL processGTResultDataBLL = new ProcessResultDataBLL();
                

                //分模块号显示
                if (sql == "") return false;

                if(!processGTResultDataBLL.GetResultDataTable(sql,model.xmno,out dt,out mssg)) return false;
                string[] mkharr = mkhs.Split(',');

                if (mkhs != "" && mkhs != null)
                {
                    List<serie> lst = CreateSeriesFromData(dt, mkhs);
                    model.series = lst;
                }//如果mkh为空默认只显示过程
            return true;
        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dt">数据表</param>
        /// <param name="mkhs">水位点名</param>
        /// <returns></returns>
        public static List<serie> CreateSeriesFromData(DataTable dt, string mkhs)
        {
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie> lst = new List<serie>();

                omkhs[] omkharr = new omkhs[mkharr.Length];
                for (int t = 0; t < mkharr.Length; t++)
                {
                    serie sr = new serie();
                    serie srdegree = new serie();
                    serie srthis = new serie();
                    serie thisdegree = new serie();
                    serie srac = new serie();
                    serie acdegree = new serie();
                    serie srrap = new serie();
                    serie rapdegree = new serie();
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "point_name=" + mkharr[t];
                    int nu = dv.Count;
                    string rri = mkharr[t];

                    //sr.Stype = "沉降值";
                    //sr.Name = rri.Replace("'", ""); 
                    ////加载点
                    //formatdat(dv, sr, "settlementvalue");
                    //lst.Add(sr);
                    //srdegree.Stype = "沉降温度值";
                    //srdegree.Name = rri.Replace("'", "");
                    ////加载点
                    //formatdat(dv, srdegree, "degree");
                    //lst.Add(srdegree);

                    //sr = new serie();
                    srthis.Stype = "应力";
                    srthis.Name = rri.Replace("'", ""); 
                    //加载点
                    formatdat(dv, srthis, "this_val");
                    lst.Add(srthis);

                    thisdegree.Stype = "预警值";
                    thisdegree.Name = rri.Replace("'", "");
                    //加载点
                    formatdat(dv, thisdegree, "degree");
                    lst.Add(thisdegree);

                    srac.Stype = "累计变化量";
                    srac.Name = rri.Replace("'", "");
                    //加载点
                    formatdat(dv, srac, "ac_val");
                    lst.Add(srac);

                    acdegree.Stype = "累计温度值";
                    acdegree.Name = rri.Replace("'", "");
                    //加载点
                    formatdat(dv, acdegree, "degree");
                    lst.Add(acdegree);


                    //srrap.Stype = "日变化量";
                    //srrap.Name = rri.Replace("'", "");
                    ////加载点
                    //formatdat(dv, srrap, "d_val");
                    //lst.Add(srrap);
                    //rapdegree.Stype = "日变化温度值";
                    //rapdegree.Name = rri.Replace("'", "");
                    ////加载点
                    //formatdat(dv, rapdegree, "degree");
                    //lst.Add(rapdegree);
                   
                }


                return lst;
            }
            return null;
        }
        /// <summary>
        /// 水位数据生成曲线
        /// </summary>
        /// <param name="dv">数据表视图</param>
        /// <param name="sr">曲线</param>
        /// <param name="valstr">值字段名称</param>
        public static void formatdat(DataView dv, serie sr, string valstr)
        {

            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["time"].ToString();
                string v = drv[valstr].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
    }
}