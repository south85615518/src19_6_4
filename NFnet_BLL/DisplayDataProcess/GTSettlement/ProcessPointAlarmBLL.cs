﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.BLL;
using NFnet_BLL.DataProcess;
using System.Data;
using TotalStation.Model.fmos_obj;
using Tool;
using NFnet_BLL.XmInfo;
using NFnet_BLL.AuthorityAlarmProcess;
using InclimeterDAL.Model;

namespace NFnet_BLL.DisplayDataProcess.GTSettlement
{
    /// <summary>
    /// 点号预警业务逻辑处理类
    /// </summary>
    public class ProcessPointAlarmBLL
    {
        
        public global::data.BLL.gtsettlementpointalarmvalue bll = new global::data.BLL.gtsettlementpointalarmvalue();
        public static ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public static ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public static ProcessalarmsplitondateBLL splitOnDateBLL = new ProcessalarmsplitondateBLL();
        public static int sec = 0;
        /// <summary>
        /// 点号预警表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointAlarmRecordsCount(ProcessAlarmRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (bll.PointTableCountLoad( model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点号预警记录数获取类
        /// </summary>
        public class ProcessAlarmRecordsCountModel : SearchCondition
        {
            public string totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessAlarmRecordsCountModel(string xmname, int xmno, string searchString)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        /// <summary>
        /// 沉降点号表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.PointTableLoad( model.pageIndex, model.rows, model.xmno, model.xmname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点名列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSettlementPointLoad(SensorPointLoadCondition model, out string mssg)
        {
            List<string> ls = null;
            if (bll.SettlementPointLoadDAL(model.xmno, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 沉降基准点名列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSettlementBasePointLoad(SensorPointLoadCondition model, out string mssg)
        {
            List<string> ls = null;
            if (bll.SettlementBasePointLoad(model.xmno, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 点号预警表获取类
        /// </summary>
        public class ProcessAlarmLoadModel : SearchCondition
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 搜索条件
            /// </summary>
            public string searchString { get; set; }
            /// <summary>
            /// 点号预警表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessAlarmLoadModel( int xmno, string searchString, string colName, int pageIndex, int rows, string sord)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.searchString = searchString;
                this.xmno = xmno;

            }
        }
        /// <summary>
        /// 点号是否存在
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmExist(global::data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            return bll.Exist(model.point_name,model.xmno, out mssg);
        }
        /// <summary>
        /// 点号预警编辑
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmEdit(global::data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            return bll.Update(model, out mssg);
        }
        /// <summary>
        /// 点号预警添加
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmAdd(global::data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            if (!ProcessAlarmExist(model,out mssg))
            return bll.Add(model, out mssg);
            return true;
        }

        public bool  ProcessAlarm(global::data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            mssg = "";
            if (!ProcessAlarmExist(model, out mssg))
            {
               return  ProcessAlarmAdd(model, out mssg);
            }
            else
            {
              return  ProcessAlarmEdit(model, out mssg);
            }
        }
        /// <summary>
        /// 点号预警批量更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmMultiUpdate(ProcessAlarmMultiUpdateModel model, out string mssg)
        {
            return bll.MultiUpdate(model.pointstr, model.model, out mssg);
        }


        /// <summary>
        /// 从测斜仪的端口和设备号设置获取点名
        /// </summary>
        /// <returns></returns>
        public bool ProcessSettlementTOPointModule(ProcessSettlementTOPointModuleGetModel model, out string mssg)
        {
            data.Model.gtsettlementpointalarmvalue devicemodel = null;
            if (bll.SettlementTOPointModule(model.xmno, model.deviceno, model.addressno, out devicemodel, out mssg))
            {
                model.devicemodel = devicemodel;
                return true;
            }
            return false;

        }
        public class ProcessSettlementTOPointModuleGetModel
        {
            public int xmno { get; set; }
            public string deviceno { get; set; }
            public string addressno { get; set; }
            public data.Model.gtsettlementpointalarmvalue devicemodel { get; set; }
            public ProcessSettlementTOPointModuleGetModel(int xmno, string deviceno, string addressno)
            {
                this.xmno = xmno;
                this.deviceno = deviceno;
                this.addressno = addressno;
            }
        }



        /// <summary>
        /// 点号预警删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmDel(global::data.Model.gtsettlementpointalarmvalue model, out string mssg)
        {
            return bll.Delete(model.point_name,model.xmno,out mssg);
        }
        public class ProcessAlarmDelModel
        {
            public string xmname { get; set; }
            public string name { get; set; }
            public ProcessAlarmDelModel(string xmname,string name)
            {
                this.xmname = xmname;
                this.name = name;
            }
        }
        /// <summary>
        /// 点号预警批量更新类
        /// </summary>
        public class ProcessAlarmMultiUpdateModel
        {
            /// <summary>
            /// 点组
            /// </summary>
            public string pointstr { get; set; }
            /// <summary>
            /// 点号预警类
            /// </summary>
            public global::data.Model.gtsettlementpointalarmvalue model { get; set; }
            public ProcessAlarmMultiUpdateModel(string pointstr, global::data.Model.gtsettlementpointalarmvalue model)
            {
                this.pointstr = pointstr;
                this.model = model;
            }
        }

        /// <summary>
        /// 结果数据对象预警处理
        /// </summary>
        /// <param name="alarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmIntoInformation(global::data.Model.gtsettlementalarmvalue alarmvalue, global::data.Model.gtsettlementresultdata resultModel, out List<string> lscontext)
        {
            lscontext = new List<string>();
            //List<string>  ls = new List<string>();
            List<string> listspace = new List<string>();
            bool result = false;
            listspace.Add(string.Format("{0},{1},沉降值:{2},温度{3}", resultModel.point_name, resultModel.time.ToString(), resultModel.settlementvalue, resultModel.degree));
            //listspace.Add(DataProcessHelper.("", "沉降上限", resultModel.deep, alarmvalue.deep, out result, result));
            listspace.Add(DataProcessHelper.Compare("", "本次", resultModel.this_val, alarmvalue.this_val, out result, result));
            listspace.Add(DataProcessHelper.Compare("", "累计", resultModel.ac_val, alarmvalue.ac_val, out result, result));
            listspace.Add(DataProcessHelper.Compare("", "日变化量", resultModel.d_val, alarmvalue.rap, out result, result));
            listspace.Add(DataProcessHelper.Compare("", "沉降差", resultModel.settlementdiff, alarmvalue.settlementdiff, out result, result));
            lscontext.Add(string.Join(" ", listspace));
            return result;
        }
       
        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            List<string> ls = new List<string>();
            string mssg = "";
            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.point_name,
                time = model.resultModel.time,
                type = "沉降",
                atime = DateTime.Now,
                pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processCgAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, "沉降", model.resultModel.point_name, model.resultModel.time);
            splitOnDateBLL.ProcessalarmsplitondateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);

            AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = "沉降",
                pointName = model.resultModel.point_name,
                xmno = model.xmno,
                time = model.resultModel.time,
                adate = DateTime.Now

            };
            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add("三级预警");
                    pointCheck.alarm = 3;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck,out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno,"沉降","",model.resultModel.point_name,3,out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add("二级预警");
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "沉降", "", model.resultModel.point_name, 2, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add("一级预警");
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "沉降", "", model.resultModel.point_name, 1, out mssg);
                    return true;
                }


            }
                ls.Add("========预警解除==========");
                pointCheck.alarm = 0;
                pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
                ProcessHotPotColorUpdate(model.xmno, "沉降", "", model.resultModel.point_name, 0, out mssg);
                return false;

        }
        public class ProcessPointAlarmfilterInformationModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public List<global::data.Model.gtsettlementalarmvalue> levelalarmvalue { get; set; }
            public global::data.Model.gtsettlementresultdata resultModel { get; set; }
            public List<string> ls { get; set; }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<global::data.Model.gtsettlementalarmvalue> levelalarmvalue, global::data.Model.gtsettlementresultdata resultModel, int xmno)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
                this.xmno = xmno;
            }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<global::data.Model.gtsettlementalarmvalue> levelalarmvalue, global::data.Model.gtsettlementresultdata resultModel)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
            }
            public ProcessPointAlarmfilterInformationModel()
            {
               
            }
        }
        public class AlarmRecord
        {
            public string pointName { get; set; }
            public string Time { get; set; }
            public string alarm { get; set; }
            public string RecordTime { get; set; }

        }

        public bool ProcessBasePointBridgeModellistGet(ProcessBasePointBridgeModellistGetModel model, out string mssg)
        {
            List<global::data.Model.gtsettlementpointalarmvalue> modellist = null;
            if (bll.GetBasePointBridgeModellist(model.xmno, model.basepointName, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class ProcessBasePointBridgeModellistGetModel
        {
            public int xmno { get; set; }
            public string basepointName { get; set; }
            public List<global::data.Model.gtsettlementpointalarmvalue> modellist { get; set; }
            public ProcessBasePointBridgeModellistGetModel(int xmno, string basepointName)
            {
                this.xmno = xmno;
                this.basepointName = basepointName;
            }
            public ProcessBasePointBridgeModellistGetModel()
            {
               
            }
        }

        public bool ProcessPointAlarmModelGet(ProcessPointAlarmModelGetModel model, out string mssg)
        {
            global::data.Model.gtsettlementpointalarmvalue pointalarm = new global::data.Model.gtsettlementpointalarmvalue();
            if (bll.GetModel(model.xmno, model.pointName, out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }
        public class ProcessPointAlarmModelGetModel
        {
            public int xmno { get; set; }
            public string pointName { get; set; }
            public global::data.Model.gtsettlementpointalarmvalue model { get; set; }
            public ProcessPointAlarmModelGetModel(int xmno, string pointName)
            {
                this.xmno = xmno;
                this.pointName = pointName;
            }
        }
        
        public bool ProcessHotPotColorUpdate(int xmno,string jclx,string jcoption,string pointName,int color,out string mssg)
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(xmno, jclx, jcoption, pointName, color);
            return layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);
            
        }

        public List<List<global::data.Model.gtsettlementpointalarmvalue>>  GtSettlementBasePointBridgeLoad(int xmno, List<List<global::data.Model.gtsettlementpointalarmvalue>> pointnamebridgeList,int n ,out string mssg)
        {
           
            List<List<global::data.Model.gtsettlementpointalarmvalue>> pointnamebridgeListdev = new List<List<data.Model.gtsettlementpointalarmvalue>>();  
            mssg = "";
            ProcessBasePointBridgeModellistGetModel processBasePointBridgeModellistGetModel = new ProcessBasePointBridgeModellistGetModel();
            bool leafall = true;
            foreach (List<global::data.Model.gtsettlementpointalarmvalue> model in pointnamebridgeList)
            {
                processBasePointBridgeModellistGetModel = new ProcessBasePointBridgeModellistGetModel(xmno,model[model.Count-1].point_name);
                if (ProcessBasePointBridgeModellistGet(processBasePointBridgeModellistGetModel, out mssg))
                {
                    leafall = false;
                    foreach (global::data.Model.gtsettlementpointalarmvalue gtsettlementpointalarmvaluemodel in processBasePointBridgeModellistGetModel.modellist)
                    {
                        List<global::data.Model.gtsettlementpointalarmvalue> pointnamebridgeListtmp = new List<data.Model.gtsettlementpointalarmvalue>();

                        pointnamebridgeListtmp.AddRange(model);
                        pointnamebridgeListtmp.Add(gtsettlementpointalarmvaluemodel);
                        pointnamebridgeListdev.Add(pointnamebridgeListtmp);
                    }
                }
                else
                {
                    
                    pointnamebridgeListdev.Add(model);
                }
               
            }
            if (leafall)
            {
                int i = 0;
                foreach (List<global::data.Model.gtsettlementpointalarmvalue> pointnamebridgeListdevmodel in pointnamebridgeList)
                {
                    var gtsettlementbridge = (from m in pointnamebridgeListdevmodel select m.point_name).ToList();
                    ExceptionLog.ExceptionWrite(string.Format("第{0}条沉降点连接桥点名{1}", i, string.Join(",", gtsettlementbridge)));
                    Console.WriteLine(string.Format("第{0}条沉降点连接桥点名{1}", i, string.Join(",", gtsettlementbridge)));
                    i++;
                }
                return pointnamebridgeList;
            }
            else
            {
                ExceptionLog.ExceptionWrite(string.Format("遍历第{0}层的沉降点连接桥叶有{1}", n, string.Join(",", GtSettlementBasePointBridgeLeaf(pointnamebridgeListdev))));
                Console.WriteLine(string.Format("遍历第{0}层的沉降点连接桥叶有{1}", n, string.Join(",", GtSettlementBasePointBridgeLeaf(pointnamebridgeListdev))));
                return GtSettlementBasePointBridgeLoad(xmno, pointnamebridgeListdev, n + 1, out mssg);
            }

            
           
            
           
           
        }
        public List<string> GtSettlementBasePointBridgeLeaf(List<List<global::data.Model.gtsettlementpointalarmvalue>> pointnamebridgeList)
        {
            List<string> ls = new List<string>();
            foreach (var model in pointnamebridgeList)
            {
                ls.Add(model[model.Count - 1].point_name);
            }
            return ls;
        }


        public bool GtSettlementPointBridgeCheck(GtSettlementPointBridgeCheckModel model,out string mssg)
        {
            mssg = "";
            if (model.gtsettlementpointalarmvaluebridge.Count == 0) return false;
            foreach (List<global::data.Model.gtsettlementpointalarmvalue> pointnamebridgeListdevmodel in model.gtsettlementpointalarmvaluebridge)
            {
                var gtsettlementbridge = (from m in pointnamebridgeListdevmodel where m.point_name == model.basepointname select m.point_name).ToList();
                if (gtsettlementbridge == null || gtsettlementbridge.Count == 0) continue;
                return false;
            }
            return true;
        }
        public class GtSettlementPointBridgeCheckModel
        {
            public List<List<global::data.Model.gtsettlementpointalarmvalue>> gtsettlementpointalarmvaluebridge { get; set; }
            public string basepointname { get; set; }
            public GtSettlementPointBridgeCheckModel(string basepointname, List<List<global::data.Model.gtsettlementpointalarmvalue>> gtsettlementpointalarmvaluebridge)
            {
                this.basepointname = basepointname;
                this.gtsettlementpointalarmvaluebridge = gtsettlementpointalarmvaluebridge;
            }
        }




        public bool gtSettlementBasePointBridgeGet(gtSettlementBasePointBridgeGetModel model, out string mssg)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmModelGetModel(model.xmno,model.rootpointname);
            if (!ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg)) return false;
            ProcessBasePointBridgeModellistGetModel processBasePointBridgeModellistGetModel = new ProcessBasePointBridgeModellistGetModel(model.xmno,model.rootpointname);
            List<List<global::data.Model.gtsettlementpointalarmvalue>> gtsettlementpointalarmvaluebridge = new List<List<data.Model.gtsettlementpointalarmvalue>>();
            if (ProcessBasePointBridgeModellistGet(processBasePointBridgeModellistGetModel, out mssg))
            {
                foreach (global::data.Model.gtsettlementpointalarmvalue gtsettlementpointalarmvaluemodel in processBasePointBridgeModellistGetModel.modellist)
                {
                    List<global::data.Model.gtsettlementpointalarmvalue> gtsettlementpointalarmvaluetmplist = new List<data.Model.gtsettlementpointalarmvalue>();
                    gtsettlementpointalarmvaluetmplist.Add(processPointAlarmModelGetModel.model);
                    gtsettlementpointalarmvaluetmplist.Add(gtsettlementpointalarmvaluemodel);
                    gtsettlementpointalarmvaluebridge.Add(gtsettlementpointalarmvaluetmplist);
                }
            }
            var gtSettlementBasePointBridge =  GtSettlementBasePointBridgeLoad(model.xmno,gtsettlementpointalarmvaluebridge,0,out mssg);
            model.gtsettlementpointalarmvaluebridge = gtSettlementBasePointBridge;
            return true;
        }
        public class  gtSettlementBasePointBridgeGetModel
        {
            public int xmno { get; set; }
            public string rootpointname { get; set; }
            public List<List<global::data.Model.gtsettlementpointalarmvalue>> gtsettlementpointalarmvaluebridge { get; set; }
            public gtSettlementBasePointBridgeGetModel(int xmno, string rootpointname)
            {
                this.xmno = xmno;
                this.rootpointname = rootpointname;
            }
        
        
        }

        //public void GtSettlementDataBridgeCalculate(string pointname,int xmno,DateTime time,out string mssg)
        //{



        //    List<string> gtsettlementpointbridge = GtSettlementBasePointBridgeLoad(pointname,xmno,out mssg);

        //    foreach()
        //    {

        //    }

        //}



    }
}