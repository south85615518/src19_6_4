﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation;
using System.Data;
using TotalStation.BLL.fmos_obj;
using SqlHelpers;
using NFnet_BLL.Other;
using NFnet_MODAL;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DataProcess.GTSettlement;
using Tool;
using NFnet_BLL.DisplayDataProcess;


namespace NFnet_BLL.DataProcess.GTSettlement
{

    /// <summary>
    /// 结果数据业务逻辑处理类
    /// </summary>
    public partial class ProcessResultDataBLL
    {

        public static data.BLL.gtsettlementresultdata bll = new data.BLL.gtsettlementresultdata();

        public bool ProcessResultDataAdd(global::data.Model.gtsettlementresultdata model,out string mssg)
        {
            return bll.Add(model,out mssg);
        }



        public bool ProcessResultDataUpdate(global::data.Model.gtsettlementresultdata model,out string mssg)
        {
            return bll.Update(model,out mssg);
        }

        public bool ProcessResultDataLastTimeModelGet(ProcessResultDataLastTimeModelGetModel model,out string mssg)
        {
            global::data.Model.gtsettlementresultdata resultdatamodel  = new data.Model.gtsettlementresultdata();
            if (bll.GetLastTimeModel(model.xmno, model.point_name, model.dt, out resultdatamodel, out mssg))
            {
                model.model = resultdatamodel;
                return true;
            }
            return false;
        }
        public class ProcessResultDataLastTimeModelGetModel
        {
            public string point_name { get; set; }
            public int xmno { get; set; }
            public DateTime dt { get; set; }
            public global::data.Model.gtsettlementresultdata model { get; set; }
            public ProcessResultDataLastTimeModelGetModel(string point_name, int xmno,DateTime dt)
            {
                this.point_name = point_name;
                this.xmno = xmno;
                this.dt = dt;
            }
        }
       
        


        /// <summary>
        /// 结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.SettlementDATATableRowsCount(model.startTime,model.endTime,model.xmno,model.pointname ,out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
       
        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.SettlementDATATableLoad(model.starttime,model.endtime,model.pageIndex, model.rows, model.xmno, model.pointname,model.sord ,out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }

        //public bool ProcessXmStateTableLoad(XmStateTableLoadCondition model,out string mssg)
        //{
        //    DataTable dt = new DataTable();
        //    if (bll.XmStateTable(model.pageIndex, model.rows, model.xmno,model.xmname, model.unitname, model.colName, model.sord, out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    return false;
        //}
        /// <summary>
        /// 结果数据报表数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataReportTableCreate(ResultDataReportTableCreateCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ResultDataReportPrint(model.sql, model.xmno, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        
      
        /// <summary>
        /// 数据展示日期查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {

            switch (model.type)
            {
                case QueryType.RQCX:
                    string sqlsttm = "";
                    string sqledtm = "";
                    string mssg = "";
                    string cycmin = "";
                    string cycmax = "";
                    var querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.startTime, ">=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqlsttm = querynvlmodel.str;
                    }
                    querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.endTime, "<=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqledtm = querynvlmodel.str;
                    }
                    string rqConditionStr = sqlsttm + " and " + sqledtm + " order by #_point,#_date asc ";
                    string sqlmin = "select min(time) as mincyc from gtsettlementresultdata where xmno = '" + model.xmno + "'";
                    string sqlmax = "select max(time) as maxcyc from gtsettlementresultdata where xmno = '" + model.xmno + "'";
                    var processquerystanderstrModel = new QuerystanderstrModel(sqlmin, model.xmno);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                       model.startTime = processquerystanderstrModel.str;
                    }
                    processquerystanderstrModel = new QuerystanderstrModel(sqlmax, model.xmno);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                       model.endTime = processquerystanderstrModel.str;
                    }
                    //model.minCyc = cycmin;
                    //model.maxCyc = cycmax;
                    model.sql = rqConditionStr;
                    break;
                case QueryType.ZQCX:
                    rqConditionStr = "#_cyc >= " + model.startTime + "   and  #_cyc <= " + model.endTime + " order by #_point,cyc,#_date asc ";
                    model.minCyc = model.startTime;
                    model.maxCyc = model.endTime;

                    model.sql = rqConditionStr;
                    break;
                case QueryType.QT:
                    var processdateswdlModel = new ProcessComBLL.ProcessdateswdlModel("#_date", model.unit,model.maxTime);
                    if (ProcessComBLL.Processdateswdl(processdateswdlModel, out mssg))
                    {
                        model.startTime = processdateswdlModel.sttm;
                        model.endTime = processdateswdlModel.edtm;
                        //model.sql = processdateswdlModel.sql;
                    }
                    break;


            }
        }
       


        public bool ProcessdataResultDataMaxTime(SenorMaxTimeCondition model,out string mssg )
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(model.xmno, out maxTime,out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;

                
        }

        public bool ProcesspointdataResultDataMaxTime(GTSettlementSensorTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(model.xmno, model.pointname, out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }

        public bool ProcesspointdataResultDataMinTime(GTSettlementSensorTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.MinTime(model.xmno, model.pointname, out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            return false;


        }


        public bool ProcessResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.PointNameDateTimeListGet(model.xmno, model.pointname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
        public class ProcessResultDataTimeListLoadModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public List<string> ls { get; set; }
            public ProcessResultDataTimeListLoadModel(int xmno, string pointname)
            {
                this.xmno = xmno;
                this.pointname = pointname;
            }
        }

        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessfillTotalStationDbFill(FillTotalStationDbFillCondition model)
        {
            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "time");
            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "point_name");
            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(  "  point_name  ", model.pointname, "   in  ", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = "select  xmno,point_name,settlementvalue,degree,settlementdiff,round(this_val,2) as this_val,round(ac_val,2) as ac_val,round(d_val,4) as d_val,time,initsettlementval    from  gtsettlementresultdata  where  xmno = '" + model.xmno + "' and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
                sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            dt = new DataTable();
            if(GetResultDataTable(sql,model.xmno,out dt,out mssg))
            {
                model.dt = dt;
                ExceptionLog.ExceptionWrite(mssg);
                return true;
            }
            return false;



          


        }
        
        public   bool   GetResultDataTable(string sql,int xmno,out DataTable dt,out string mssg)
        {
            mssg = "";
            dt = null;
            var processquerystanderdbModel = new QuerystanderdbIntModel(sql, xmno);
            if (ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
            {
                dt = processquerystanderdbModel.dt;
                return true;
            }
            return false;
        }



        //根据表的分页单位获取当前记录在表中的页数
        public int PageIndexFromTab(string pointname, string date, DataView dv, string pointnameStr, string dateStr, int pageSize)
        {

            //DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr];
                if (drv[pointnameStr].ToString() == pointname && date.Trim() == drv[dateStr].ToString())
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }

        public bool ProcessResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out List<global::data.Model.gtsettlementresultdata> modellist, out string mssg)
        {
            modellist = new List<global::data.Model.gtsettlementresultdata>();
            if (bll.GetModelList(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }

        public class ProcessResultDataAlarmModelListModel
        {
            public int xmno { get; set; }
            public List<global::data.Model.gtsettlementresultdata> modellist { set; get; }
            public ProcessResultDataAlarmModelListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }


        

        //public bool ProcessResultDataSearch(ProcessResultDataTimeSearchModel model, out string mssg)
        //{
        //    DataTable dt = null;
        //    if (bll(model.xmno, model.pointName, model.timestart, model.timeend,out dt, out mssg))
        //    {
        //        model.dt = dt;
        //        return true;
        //    }
        //    return false;
        //}

        public class ProcessResultDataTimeSearchModel 
        {

            public string xmname { get; set; }
            public string pointName { get; set; }
            public DateTime timestart { get; set; }
            public DataTable dt { get; set; }
            public DateTime timeend { get; set; }
            public ProcessResultDataTimeSearchModel(string xmname, string pointName, DateTime timestart, DateTime timeend)
            {
                this.xmname = xmname;
                this.pointName = pointName;
                this.timestart = timestart;
                this.timeend = timeend;
            }
        }
        

       //public bool GetAlarmTableCont(GetAlarmTableContModel model, out string mssg)
       // {
       //     int cont = 0;
       //     if (bll.(model.pointnamelist, out cont, out mssg))
       //     {
       //         model.cont = cont;
       //         return true;
       //     }
       //     return false;
       // }
        public class GetAlarmTableContModel
        {
            public List<string> pointnamelist { get; set; }
            public int cont { get; set; }

            public GetAlarmTableContModel( List<string> pointnamelist)
            {
                this.pointnamelist = pointnamelist;

            }
        }



        public bool ProcessPointNewestDateTimeGet(PointNewestDateTimeCondition model,out string mssg)
        {
            DateTime dt = new DateTime();
            if (bll.PointNewestDateTimeGet(model.xmno, model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }

        public bool ProcessPointOnLineCont(PointOnLineContCondition model, out string mssg)
        {
            string contpercent = "";
            if (bll.OnLinePointCont(model.unitname, out contpercent, out mssg))
            {
                model.contpercent = contpercent;
                return true;
            }
            return false;
        }



    }
}