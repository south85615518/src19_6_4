﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_BLL.DisplayDataProcess.GTSettlement
{
    public partial class ProcessResultDataAlarmBLL
    {
      
        //保存预警信息
        public bool cgmain()
        {
            alarmcont = 0;
            return TestGTSettlementModelList();

        }
        public bool TestCgGTSettlementModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessResultDataBLL.ProcessResultDataAlarmModelListModel(xmno);
            List<global::data.Model.gtsettlementresultdata> modellist;
            if (resultBLL.ProcessCgScalarResultDataAlarmModelList(processResultDataAlarmModelListModel, out modellist, out mssg))
            {

                GTSettlementPointAlarm(modellist);
                return true;
            }
            return false;
        }
       
       
        public List<string> CgResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            ExceptionLog.TotalSationPointAlarmVedioWrite("************开始预警**********");
            cgmain();
            var infolist = (from m in alarmInfoList orderby m.datatype select m.alarmcontext).ToList();
            return infolist;
        }
        




    }
}