﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DataProcess.GTSettlement
{
    public partial class ProcessResultDataBLL
    {

        public bool AddSurveyData(AddSurveyDataModel model, out string mssg)
        {
            return bll.AddSurveyData(model.xmno, model.point_name, model.starttime, model.endtime,  out mssg);
        }
        public class AddSurveyDataModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public DateTime starttime { get; set; }
            public DateTime endtime { get; set; }
            
            public AddSurveyDataModel(int xmno, string point_name, DateTime starttime, DateTime endtime)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.starttime = starttime;
                this.endtime = endtime;
                
            }
        }

        public bool UpdataSurveyData(UpdataSurveyDataModel model, out string mssg)
        {
            return bll.UpdataSurveyData(model.xmno, model.point_name, model.dt,  model.vSet_name, model.vLink_name, model.srcdatetime, out mssg);
        }
        public class UpdataSurveyDataModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public DateTime dt { get; set; }
            public string vSet_name { get; set; }
            public string vLink_name { get; set; }
            public DateTime srcdatetime { get; set; }
            public UpdataSurveyDataModel(int xmno, string point_name, DateTime dt,  string vSet_name, string vLink_name, DateTime srcdatetime)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.dt = dt;
                this.vSet_name = vSet_name;
                this.vLink_name = vLink_name;
                this.srcdatetime = srcdatetime;
            }
        }
        public bool ProcessSurveyPointTimeDataAdd(ProcessSurveyPointTimeDataAddModel model, out string mssg)
        {
            return bll.AddSurveyData(model.xmno,  model.pointname, model.time, model.importtime, out mssg);
        }
        public class ProcessSurveyPointTimeDataAddModel
        {
            public int xmno { get; set; }
            
            public string pointname { get; set; }
            public DateTime time { get; set; }
            public DateTime importtime { get; set; }
            public ProcessSurveyPointTimeDataAddModel(int xmno, string pointname, DateTime time, DateTime importtime)
            {
                this.xmno = xmno;
                this.pointname = pointname;
                this.time = time;
                this.importtime = importtime;
            }

        }
        public bool DeleteSurveyData(DeleteModel model, out string mssg)
        {
            return bll.DeleteSurveyData(model.xmno, model.point_name,  model.starttime, model.endtime, out mssg);

        }
        public class DeleteModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public DateTime starttime { get; set; }
            public DateTime endtime { get; set; }
            public DeleteModel(int xmno, string point_name, DateTime starttime, DateTime endtime)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.starttime = starttime;
                this.endtime = endtime;
            }
        }
        public bool ProcessSurveyResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.PointNameSurveyDateTimeListGet(model.xmno, model.pointname,  out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSurveyResultDataRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.SettlementSurveyDATATableRowsCount(model.startTime, model.endTime, model.xmno, model.pointname, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }

        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSurveyResultDataLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.SettlementSurveyDATATableLoad(model.starttime, model.endtime, model.pageIndex, model.rows, model.xmno, model.pointname, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
    }
}