﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DisplayDataProcess;

namespace NFnet_BLL.DataProcess.GTSettlement
{
    public partial class ProcessResultDataBLL
    {
        
        public bool ProcesspointcgdataResultDataMaxTime(GTSettlementSensorTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.CgMaxTime(model.xmno, model.pointname ,out maxTime, out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;


        }

        public bool ProcesspointcgdataResultDataMinTime(GTSettlementSensorTimeCondition model, out string mssg)
        {
            DateTime minTime = new DateTime();
            if (bll.CgMinTime(model.xmno,  model.pointname,out minTime, out mssg))
            {
                model.dt = minTime;
                return true;
            }
            return false;


        }
        public bool ProcessCgResultDataTimeListLoad(ProcessResultDataTimeListLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (bll.CgPointNameDateTimeListGet(model.xmno, model.pointname,  out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessCgDataImport(AddSurveyDataModel model, out string mssg)
        {
            return bll.AddCgData(model.xmno, model.point_name,  model.starttime, model.endtime,out mssg); 
        }
        
         //<summary>
         //删除成果数据
         //</summary>
        public bool DeleteCg(DeleteModel model, out string mssg)
        {
            return bll.DeleteCg(model.xmno, model.point_name,  model.starttime, model.endtime, out mssg);

        }
        public bool DeleteCgTmp(DeleteModel model, out string mssg)
        {
            return bll.DeleteCgTmp(model.xmno,  out mssg);

        }
        public bool CgResultdataTableLoad(GTResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.CgResultdataTableLoad(model.pageIndex, model.rows, model.xmno, model.pointname, model.sord,  model.starttime, model.endtime, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }
        }

        public bool CgResultdataTableRecordsCount(GTResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.CgResultTableRowsCount(model.xmno, model.pointname, model.sord,  model.startTime, model.endTime, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }

        public bool ProcessCgScalarResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out List<global::data.Model.gtsettlementresultdata> modellist, out string mssg)
        {
            modellist = new List<global::data.Model.gtsettlementresultdata>();
            if (bll.GetModelList(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        


    }
}