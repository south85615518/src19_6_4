﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_BLL.DisplayDataProcess.GTSettlement
{
    public partial class ProcessResultDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public string dateTime { get; set; }
        public int alarmcont { get; set; }
        //保存预警信息
        public List<alarmInfo> alarmInfoList { get; set; }

        public class alarmInfo
        {
            public string datatype { get; set; }
            public string alarmcontext { get; set; }
            public alarmInfo()
            {
 
            }
        }


        public ProcessResultDataAlarmBLL()
        {
        }
        public ProcessResultDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = xmname;
            this.xmno = xmno;
        }
        public ProcessResultDataAlarmBLL( int xmno)
        {
            this.xmno = xmno;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmValueBLL alarmBLL = new ProcessAlarmValueBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
        public static ProcessResultDataBLL resultBLL = new ProcessResultDataBLL();
        public bool main()
        {
            alarmcont = 0;
            return TestGTSettlementModelList();

        }
        public bool TestGTSettlementModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessResultDataBLL.ProcessResultDataAlarmModelListModel(xmno);
            List<global::data.Model.gtsettlementresultdata> modellist;
            if (resultBLL.ProcessResultDataAlarmModelList(processResultDataAlarmModelListModel, out modellist, out mssg))
            {

                GTSettlementPointAlarm(modellist);
                return true;
            }
            return false;
        }
        public data.Model.gtsettlementpointalarmvalue TestPointAlarmValue(string pointName)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointName);
            data.Model.gtsettlementpointalarmvalue model = null;
            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print(mssg);
                //TestAlarmValueList(processPointAlarmModelGetModel.model);
                return processPointAlarmModelGetModel.model;

            }
            return null;
        }
        public List<data.Model.gtsettlementalarmvalue> TestAlarmValueList(data.Model.gtsettlementpointalarmvalue pointalarm)
        {
            List<data.Model.gtsettlementalarmvalue> alarmvalueList = new List<data.Model.gtsettlementalarmvalue>();
            //一级
            var ProcessSingleScalarvalueModelGetModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.firstalarmname);
            if (alarmBLL.ProcessAlarmModelGetByName(ProcessSingleScalarvalueModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print("一级：" + mssg);
                alarmvalueList.Add(ProcessSingleScalarvalueModelGetModel.model);
            }
            else
                alarmvalueList.Add(null);
            ProcessSingleScalarvalueModelGetModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.secalarmname);
            if (alarmBLL.ProcessAlarmModelGetByName(ProcessSingleScalarvalueModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print("二级：" + mssg);
                alarmvalueList.Add(ProcessSingleScalarvalueModelGetModel.model);
            }
            else
                alarmvalueList.Add(null);
            ProcessSingleScalarvalueModelGetModel = new ProcessAlarmValueBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.thirdalarmname);
            if (alarmBLL.ProcessAlarmModelGetByName(ProcessSingleScalarvalueModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print("三级：" + mssg);
                alarmvalueList.Add(ProcessSingleScalarvalueModelGetModel.model);
            }
            else
                alarmvalueList.Add(null);
            return alarmvalueList;
        }
        public void TestGTSettlementAlarmfilterInformation(List<data.Model.gtsettlementalarmvalue> levelalarmvalue, data.Model.gtsettlementresultdata resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (pointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                alarmcont++;
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.Add(new alarmInfo {  alarmcontext = string.Join("\r\n",processPointAlarmfilterInformationModel.ls) });
                Console.WriteLine(string.Format("{0}   {1}   {2}\n", "沉降", resultModel.point_name, string.Join("\r\n", processPointAlarmfilterInformationModel.ls)));
            }
        }
        public bool GTSettlementPointAlarm(List<data.Model.gtsettlementresultdata> lc)
        {
            alarmInfoList = new List<alarmInfo>();
            //List<string> ls = new List<string>();
            //ls.Add("\n");
            //ls.Add(string.Format("==========={0}===========", DateTime.Now));
            //ls.Add(string.Format("==========={0}===========", xmname));
            //ls.Add(string.Format("==========={0}===========","超限自检"));
            //ls.Add("\n");
            //alarmInfoList.AddRange(ls);
            //ExceptionLog.ExceptionWriteCheck(ls);
            ExceptionLog.TotalSationPointCheckVedioWrite("获取到项目"+xmname+"自检临时表中记录数" + lc.Count + "条");
            int i = 0;
            foreach (data.Model.gtsettlementresultdata cl in lc)
            {
                //if (i > 20) break;
                
                //string threadname = Thread.CurrentThread.Name;
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname)) && !Tool.ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", xmname))) return false;
                data.Model.gtsettlementpointalarmvalue pointvalue = TestPointAlarmValue(cl.point_name);
                if (pointvalue == null) continue;
                List<data.Model.gtsettlementalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                
                

            }
            ExceptionLog.TotalSationPointCheckVedioWrite("项目" + xmname + "本次预警共产生新预警记录" + alarmcont + "条");
            ExceptionLog.TotalSationPointCheckVedioWrite("************预警结束**********");
            return true;
        }
        public List<string> ResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            ExceptionLog.TotalSationPointCheckVedioWrite("************开始预警**********");
            main();
            var infolist = (from m in alarmInfoList orderby m.datatype select m.alarmcontext).ToList();
            return infolist;
        }
        public void GTSettlementPointAlarm(data.Model.gtsettlementresultdata dirnet)
        {
            alarmInfoList = new List<alarmInfo>();
            data.Model.gtsettlementpointalarmvalue pointvalue = TestPointAlarmValue(dirnet.point_name);
            if (pointvalue == null) return;
            List<data.Model.gtsettlementalarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestGTSettlementAlarmfilterInformation(alarmList, dirnet);
            
        }

        public bool GTPointSurfaceDataCurrentAlarmCreate(List<global::data.Model.gtsettlementresultdata> lc)
        {
            alarmInfoList = new List<alarmInfo>();
            foreach (global::data.Model.gtsettlementresultdata cl in lc)
            {

                data.Model.gtsettlementpointalarmvalue pointvalue = TestPointAlarmValue(cl.point_name);
                List<data.Model.gtsettlementalarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestGTSettlementAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<alarmInfo> SurfaceDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }



    }
}