﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class MdbdataPointNewestDateTimeCondition
    {
        public string xmname { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public MdbdataPointNewestDateTimeCondition(string xmname, string pointname)
        {
            this.xmname = xmname;
            this.pointname = pointname;
        }
    }
}