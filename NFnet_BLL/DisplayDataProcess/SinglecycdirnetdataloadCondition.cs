﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SinglecycdirnetdataloadCondition
    {
          public string xmname { get; set; }
            public string pointname { get; set; }
            public int startcyc { get; set; }
            public int endcyc { get; set; }
            public DataTable dt { get; set; }
            public SinglecycdirnetdataloadCondition(string xmname, string pointname, int startcyc, int endcyc)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.startcyc = startcyc;
                this.endcyc = endcyc;
            }
    }
}