﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;

namespace NFnet_BLL.DisplayDataProcess
{
    /// <summary>
    /// 曲线图序列化类
    /// </summary>
    public class ProcessGTChartCondition
    {
        /// <summary>
        /// 数据表获取SQL语句
        /// </summary>
        public string sql { get; set; }
        /// <summary>
        /// 生成的曲线列表
        /// </summary>
        public List<serie> series { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 点名
        /// </summary>
        public string pointname { get; set; }
        public global::data.Model.gtsensortype datatype{get;set;}

        public ProcessGTChartCondition()
        {
        }
        public ProcessGTChartCondition(object sql, object xmname, object pointname, object datatype)
        {
            this.sql = sql == null ? "" : sql.ToString();
            this.xmname = xmname == null ? "" : xmname.ToString();
            this.pointname = pointname == null ? "" : pointname.ToString();
            this.datatype = datatype == null ? data.Model.gtsensortype._other: (data.Model.gtsensortype)datatype;
        }
        public ProcessGTChartCondition(object sql, object xmno, object pointname, int xm)
        {
            this.sql = sql == null ? "" : sql.ToString();
            this.xmno = xmno == null ? -1 : Convert.ToInt32(xmno);
            this.pointname = pointname == null ? "" : pointname.ToString();
        }
    }
}