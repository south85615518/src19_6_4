﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL
{
    public class ResultDataLoadCondition
    {
        /// <summary>
        /// 项目名
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 点名
        /// <summary>
        /// 起始页号
        /// </summary>
        public int pageIndex { get; set; }
        /// <summary>
        /// 每页行数
        /// </summary>
        public int rows { get; set; }

        /// 起始周期
        /// </summary>
        public int startCyc { get; set; }
        /// <summary>
        /// 结束周期
        /// </summary>
        public int endCyc { get; set; }
        /// <summary>
        /// 点名
        /// </summary>
        public string pointname { get; set; }
        /// <summary>
        /// 结果数据表
        /// </summary>
        public DataTable dt { get; set; }
        /// <summary>
        /// 排序字段
        /// </summary>
        public string sord { get; set; }
        public ResultDataLoadCondition(string xmname, string pointname, int pageIndex, int rows, int startCyc, int endCyc,string sord)
        {
            this.xmname = xmname;
            this.pageIndex = pageIndex;
            this.rows = rows;
            this.startCyc = startCyc;
            this.endCyc = endCyc;
            this.pointname = pointname;
            this.sord = sord;
        }
       
        public ResultDataLoadCondition(int xmno, string pointname, int pageIndex, int rows, int startCyc, int endCyc, string sord)
        {
            this.xmno = xmno;
            this.pageIndex = pageIndex;
            this.rows = rows;
            this.startCyc = startCyc;
            this.endCyc = endCyc;
            this.pointname = pointname;
            this.sord = sord;
        }

      
        public ResultDataLoadCondition()
        {
           
        }
    }
}