﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class ResultDataCountLoadCondition
    {
       
        public string sord { get; set; }
        public string xmname { get; set; }
        public int xmno { get; set; }
        public string pointname { get; set; }
        public int startCyc { get; set; }
        public int endCyc { get; set; }
        public string totalCont { get; set; }
        public ResultDataCountLoadCondition(string xmname, string pointname, int startCyc, int endCyc)
        {
            this.xmname = xmname;
            this.pointname = pointname;
            this.startCyc = startCyc;
            this.endCyc = endCyc;
        }
        public ResultDataCountLoadCondition(int xmno, string pointname, int startCyc, int endCyc)
        {
            this.xmno = xmno;
            this.pointname = pointname;
        }
    }
    
}