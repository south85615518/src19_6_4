﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;

namespace NFnet_BLL.DisplayDataProcess.DateTimeStamp
{
    public class ProcessGTinclinometerDateTimeStampBLL
    {
        public ProcessFixed_Inclinometer_orglDataBLL ProcessInclinometerDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public bool ProcessPointDataDateTimeStampLoad(ProcessPointDataDateTimeStampLoadModel model, out string mssg)
        {
            //DateTime maxTime = new DateTime();
            //model.timestamp.point_name = model.point_name;
            //var gTSensorMinTimeCondition = new GTSurfaceTimeCondition(model.xmno, model.point_name,data.DAL.gtsensortype.GTStringToSensorType(model.datatype),);
            //if (ProcessInclinometerDataBLL.ProcesspointFixedInclinometerResultDataMinTime(gTSensorMinTimeCondition, out mssg))
            //{
            //    model.timestamp.surveystarttime = gTSensorMinTimeCondition.dt.ToString();
            //}
            //if (ProcessInclinometerDataBLL.ProcesspointcgFixedInclinometerResultDataMinTime(gTSensorMinTimeCondition, out mssg))
            //{
            //    model.timestamp.cgstarttime = gTSensorMinTimeCondition.dt.ToString();
            //}
            //var gTSensorMaxTimeCondition = new GTSurfaceTimeCondition(model.xmno, model.point_name);
            //if (ProcessInclinometerDataBLL.ProcesspointFixedInclinometerResultDataMaxTime(gTSensorMaxTimeCondition, out mssg))
            //{
            //    model.timestamp.surveyendtime = gTSensorMaxTimeCondition.dt.ToString();
            //}
            //if (ProcessInclinometerDataBLL.ProcesspointcgFixedInclinometerResultDataMaxTime(gTSensorMaxTimeCondition, out mssg))
            //{
            //    model.timestamp.cgendtime = gTSensorMaxTimeCondition.dt.ToString();
            //}
            mssg = "";
            return true;

        }




        public class ProcessPointDataDateTimeStampLoadModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string point_name { get; set; }
            public PointDataDateTimeStamp timestamp { get; set; }
            public ProcessPointDataDateTimeStampLoadModel(string xmname, string point_name, PointDataDateTimeStamp timestamp)
            {
                this.xmname = xmname;
                this.timestamp = timestamp;
                this.point_name = point_name;
            }

            public ProcessPointDataDateTimeStampLoadModel(int xmno, string point_name, PointDataDateTimeStamp timestamp)
            {
                this.xmno = xmno;
                this.timestamp = timestamp;
                this.point_name = point_name;
            }

        }


        public void ProcessDatalackdate(ProcessDatalackdateModel model, out string mssg)
        {

            //var processResultDataTimeListLoadModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessResultDataTimeListLoadModel(model.xmno, model.point_name,data.Model.gtsensortype._inclinometer);
            //if (!ProcessInclinometerDataBLL.ProcessResultDataTimeListLoad(processResultDataTimeListLoadModel, out mssg)) return;
            //DateTime dttmp = DateTime.Now;
            //string time = "";
            //int i = 0;
            //if (processResultDataTimeListLoadModel.ls.Count == 0) return;
            //model.timestamp.surveylackdatetime = new List<string>();
            //foreach (var datetime in processResultDataTimeListLoadModel.ls)
            //{
            //    time = datetime.Substring(1, datetime.Length - 2);
            //    //if (dttmp != new DateTime())
            //    //int hous = (Convert.ToDateTime(time) - dttmp).TotalHours;
            //    if ((Convert.ToDateTime(time) - dttmp).TotalHours > model.timestamp.intervalhour)
            //        model.timestamp.surveylackdatetime.Add(string.Format("{0}-{1}", dttmp, time));
            //    dttmp = Convert.ToDateTime(time);
            //    i++;
            //}
            //model.timestamp.surveystarttime = Convert.ToDateTime(processResultDataTimeListLoadModel.ls[0].Substring(1, processResultDataTimeListLoadModel.ls[0].Length - 2)).ToString();
            //model.timestamp.surveyendtime = Convert.ToDateTime(processResultDataTimeListLoadModel.ls[processResultDataTimeListLoadModel.ls.Count - 1].Substring(1, processResultDataTimeListLoadModel.ls[processResultDataTimeListLoadModel.ls.Count - 1].Length - 2)).ToString();
            //processResultDataTimeListLoadModel = new NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessResultDataTimeListLoadModel(model.xmno, model.point_name);
            //if (!ProcessInclinometerDataBLL.ProcessCgResultDataTimeListLoad(processResultDataTimeListLoadModel, out mssg)) return;
            //dttmp = DateTime.Now;
            //if (processResultDataTimeListLoadModel.ls.Count == 0) return;
            //processResultDataTimeListLoadModel.ls.Insert(0, "[" + model.timestamp.surveystarttime + "]");

            //processResultDataTimeListLoadModel.ls.Add("[" + model.timestamp.surveyendtime + "]");


            //model.timestamp.cglackdatetime = new List<string>();
            //foreach (var datetime in processResultDataTimeListLoadModel.ls)
            //{
            //    time = datetime.Substring(1, datetime.Length - 2);
            //    if ((Convert.ToDateTime(time) - dttmp).TotalHours > model.timestamp.intervalhour)
            //        model.timestamp.cglackdatetime.Add(string.Format("{0}-{1}", dttmp, time));
            //    dttmp = Convert.ToDateTime(time);
            //    i++;
            //}

            //model.timestamp.cgstarttime = Convert.ToDateTime(processResultDataTimeListLoadModel.ls[1].Substring(1, processResultDataTimeListLoadModel.ls[1].Length - 2)).ToString();
            //model.timestamp.cgendtime = Convert.ToDateTime(processResultDataTimeListLoadModel.ls[processResultDataTimeListLoadModel.ls.Count - 2].Substring(1, processResultDataTimeListLoadModel.ls[processResultDataTimeListLoadModel.ls.Count - 2].Length - 2)).ToString();
            mssg = "";
        }
        public class ProcessDatalackdateModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public PointDataDateTimeStamp timestamp { get; set; }
            public ProcessDatalackdateModel(int xmno, string point_name, PointDataDateTimeStamp timestamp)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.timestamp = timestamp;
            }
        }


    }
}