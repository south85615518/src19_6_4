﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.GT;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.DateTimeStamp
{
    public class ProcessDateTimeStampBLL
    {
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public bool ProcessPointDataDateTimeStampLoad(ProcessPointDataDateTimeStampLoadModel model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            model.timestamp.point_name = model.point_name;
            var gTSensorMinTimeCondition = new GTSensorMinTimeCondition(model.xmname,model.point_name ,model.datatype);
            if (processResultDataBLL.ProcesspointdataResultDataMinTime(gTSensorMinTimeCondition, out mssg))
            {
                model.timestamp.surveystarttime =  gTSensorMinTimeCondition.dt.ToString();
            } 
            if (processResultDataBLL.ProcesspointcgdataResultDataMinTime(gTSensorMinTimeCondition, out mssg))
            {
                model.timestamp.cgstarttime = gTSensorMinTimeCondition.dt.ToString();
            }
            var gTSensorMaxTimeCondition = new GTSensorMaxTimeCondition(model.xmname,model.point_name ,model.datatype);
            if (processResultDataBLL.ProcesspointdataResultDataMaxTime(gTSensorMaxTimeCondition, out mssg))
            {
                model.timestamp.surveyendtime = gTSensorMaxTimeCondition.dt.ToString();
            }
            if (processResultDataBLL.ProcesspointcgdataResultDataMaxTime(gTSensorMaxTimeCondition, out mssg))
            {
                model.timestamp.cgendtime = gTSensorMaxTimeCondition.dt.ToString();
            }
            return true;

        }




        public class ProcessPointDataDateTimeStampLoadModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string point_name { get; set; }
            public PointDataDateTimeStamp timestamp { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public ProcessPointDataDateTimeStampLoadModel(string xmname, data.Model.gtsensortype datatype, string point_name,PointDataDateTimeStamp timestamp)
            {
                this.xmname = xmname;
                this.timestamp = timestamp;
                this.datatype = datatype;
                this.point_name = point_name;
            }

            public ProcessPointDataDateTimeStampLoadModel(int xmno, data.Model.gtsensortype datatype, string point_name, PointDataDateTimeStamp timestamp)
            {
                this.xmno = xmno;
                this.timestamp = timestamp;
                this.datatype = datatype;
                this.point_name = point_name;
            }

        }


        public void ProcessDatalackdate(ProcessDatalackdateModel model, out string mssg)
        {

            var processResultDataTimeListLoadModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessResultDataBLL.ProcessResultDataTimeListLoadModel(model.xmname, model.point_name, model.datatype);
            if (!processResultDataBLL.ProcessResultDataTimeListLoad(processResultDataTimeListLoadModel, out mssg)) return;
            DateTime dttmp = DateTime.Now;
            string time = "";
            int i = 0;
            if (processResultDataTimeListLoadModel.ls.Count == 0) return;
            model.timestamp.surveylackdatetime = new List<string>();
            foreach (var datetime in processResultDataTimeListLoadModel.ls)
            {
                time = datetime.Substring(1, datetime.Length - 2);
                //if (dttmp != new DateTime())
                //int hous = (Convert.ToDateTime(time) - dttmp).TotalHours;
                if ((Convert.ToDateTime(time) - dttmp).TotalHours > model.timestamp.intervalhour)
                    model.timestamp.surveylackdatetime.Add(string.Format("{0}-{1}", dttmp, time));
                dttmp = Convert.ToDateTime(time);
                i++;
            }
            model.timestamp.surveystarttime = Convert.ToDateTime(processResultDataTimeListLoadModel.ls[0].Substring(1, processResultDataTimeListLoadModel.ls[0].Length - 2)).ToString();
            model.timestamp.surveyendtime = Convert.ToDateTime(processResultDataTimeListLoadModel.ls[processResultDataTimeListLoadModel.ls.Count - 1].Substring(1, processResultDataTimeListLoadModel.ls[processResultDataTimeListLoadModel.ls.Count - 1].Length - 2)).ToString();
             processResultDataTimeListLoadModel = new NFnet_BLL.DisplayDataProcess.GT.ProcessResultDataBLL.ProcessResultDataTimeListLoadModel(model.xmname, model.point_name, model.datatype);
            if (!processResultDataBLL.ProcessCgResultDataTimeListLoad(processResultDataTimeListLoadModel, out mssg)) return;
            dttmp = DateTime.Now;
            if (processResultDataTimeListLoadModel.ls.Count == 0) return;
            processResultDataTimeListLoadModel.ls.Insert(0,"["+model.timestamp.surveystarttime+"]");

            processResultDataTimeListLoadModel.ls.Add("["+model.timestamp.surveyendtime+"]");

            
            model.timestamp.cglackdatetime = new List<string>();
            foreach (var datetime in processResultDataTimeListLoadModel.ls)
            {
                ExceptionLog.ExceptionWrite(string.Format("{0}:datetime {1}",model.point_name,datetime));
                time = datetime.Substring(1, datetime.Length - 2);
                ExceptionLog.ExceptionWrite(string.Format("{0}:time {1}", model.point_name, time));
                if ((Convert.ToDateTime(time) - dttmp).TotalHours > model.timestamp.intervalhour)
                    model.timestamp.cglackdatetime.Add(string.Format("{0}-{1}", dttmp, time));
                dttmp = Convert.ToDateTime(time);
                i++;
            }

            model.timestamp.cgstarttime = Convert.ToDateTime(processResultDataTimeListLoadModel.ls[1].Substring(1, processResultDataTimeListLoadModel.ls[1].Length - 2)).ToString();
            model.timestamp.cgendtime = Convert.ToDateTime(processResultDataTimeListLoadModel.ls[processResultDataTimeListLoadModel.ls.Count - 2].Substring(1, processResultDataTimeListLoadModel.ls[processResultDataTimeListLoadModel.ls.Count - 2].Length - 2)).ToString();
        }
        public class ProcessDatalackdateModel
        {
            public string xmname { get; set; }
            public string point_name { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public PointDataDateTimeStamp timestamp { get; set; }
            public ProcessDatalackdateModel(string xmname, string point_name, data.Model.gtsensortype datatype, PointDataDateTimeStamp timestamp)
            {
                this.xmname = xmname;
                this.point_name = point_name;
                this.datatype = datatype;
                this.timestamp = timestamp;
            }
        }


    }
}