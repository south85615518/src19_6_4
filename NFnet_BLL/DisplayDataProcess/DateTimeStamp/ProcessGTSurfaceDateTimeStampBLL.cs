﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.GT;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.DateTimeStamp
{
    public class ProcessGTSurfaceDataDateTimeStampBLL
    {
        public ProcessSurfaceDataBLL processSurfaceDataBLL = new ProcessSurfaceDataBLL();
        public bool ProcessPointDataDateTimeStampLoad(ProcessPointDataDateTimeStampLoadModel model, out string mssg)
        {
            //DateTime maxTime = new DateTime();
            //model.timestamp.point_name = model.point_name;
            //var gTSensorMinTimeCondition = new GTSurfaceTimeCondition(model.xmno,model.datatype , model.point_name);
            //if (processSurfaceDataBLL.ProcesssurfacedataResultDataMinTime(gTSensorMinTimeCondition, out mssg))
            //{
            //    model.timestamp.surveystarttime = gTSensorMinTimeCondition.dt.ToString();
            //}
            //if (processSurfaceDataBLL.ProcesspointcgdataResultDataMinTime(gTSensorMinTimeCondition, out mssg))
            //{
            //    model.timestamp.cgstarttime = gTSensorMinTimeCondition.dt.ToString();
            //}
            //var gTSensorMaxTimeCondition = new GTSurfaceTimeCondition(model.xmno,model.datatype ,model.point_name);
            //if (processSurfaceDataBLL.ProcesssurfacedataResultDataMaxTime(gTSensorMaxTimeCondition, out mssg))
            //{
            //    model.timestamp.surveyendtime = gTSensorMaxTimeCondition.dt.ToString();
            //}
            //if (processSurfaceDataBLL.ProcesspointcgdataResultDataMaxTime(gTSensorMaxTimeCondition, out mssg))
            //{
            //    model.timestamp.cgendtime = gTSensorMaxTimeCondition.dt.ToString();
            //}
            
            mssg = "";return true;

        }




        public class ProcessPointDataDateTimeStampLoadModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string point_name { get; set; }
            public PointDataDateTimeStamp timestamp { get; set; }
            public ProcessPointDataDateTimeStampLoadModel(string xmname, string point_name, PointDataDateTimeStamp timestamp)
            {
                this.xmname = xmname;
                this.timestamp = timestamp;
                this.point_name = point_name;
            }

            public ProcessPointDataDateTimeStampLoadModel(int xmno,  string point_name, PointDataDateTimeStamp timestamp)
            {
                this.xmno = xmno;
                this.timestamp = timestamp;
                this.point_name = point_name;
            }

        }


        public void ProcessDatalackdate(ProcessDatalackdateModel model, out string mssg)
        {


            mssg = "";
        }
        public class ProcessDatalackdateModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public PointDataDateTimeStamp timestamp { get; set; }
            public ProcessDatalackdateModel(int xmno, string point_name, PointDataDateTimeStamp timestamp)
            {
                this.xmno = xmno;
                this.point_name = point_name;
                this.timestamp = timestamp;
            }
        }


    }
}