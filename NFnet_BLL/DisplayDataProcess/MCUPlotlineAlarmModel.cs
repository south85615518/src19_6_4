﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class MCUPlotlineAlarmModel
    {
        public string pointname { get; set; }
        public MDBDATA.Model.mcualarmvalue firstalarm { get; set; }
        public MDBDATA.Model.mcualarmvalue secalarm { get; set; }
        public MDBDATA.Model.mcualarmvalue thirdalarm { get; set; }

    }
}