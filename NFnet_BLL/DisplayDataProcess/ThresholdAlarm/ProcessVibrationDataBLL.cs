﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//using vibration2.Model;


////测振仪数据
//namespace NFnet_BLL.Vibration
//{

//    public class ProcessVibrationBLL
//    {
//        private readonly vibration2.BLL.vibrationdata bll = new vibration2.BLL.vibrationdata();

//        //添加数据
//        public bool ProcessAddVibration(vibration model, out string mssg)
//        {
//            return bll.AddVibration(model, out mssg);
//        }

//        //更新数据
//        public bool ProcessUpdateVibration(vibration model, out string mssg)
//        {
//            return bll.UpdateVibration(model, out mssg);
//        }

//        //分页条件
//        public class ListeQueryCondition
//        {
//            public int pageIndex { get; set; }
//            public int pageSize { get; set; }
//            public string orderName { get; set; }
//            public string sord { get; set; }
//            public List<vibration> modelList { get; set; }
//            public int count;
//        }

//        public class ProcessGetVIbrationModel : ListeQueryCondition
//        {
//            public int xmno { get; set; }
//            public string point_name { get; set; }
//            public DateTime beginTime { get; set; }
//            public DateTime endTime { get; set; }
//            public ProcessGetVIbrationModel() { }
//            public ProcessGetVIbrationModel(int xmno, string point_name, DateTime beginTime, DateTime endTime, string orderName, string sord, int pageIndex, int pageSize)
//            {
//                //查询条件
//                this.xmno = xmno;
//                this.point_name = point_name;
//                this.beginTime = beginTime;
//                this.endTime = endTime;

//                //分页条件
//                this.orderName = orderName;
//                this.sord = sord;
//                this.pageIndex = pageIndex;
//                this.pageSize = pageSize;
//            }
//        }

//        //根据项目编号 点名 时间 分页获取测振仪数据List
//        public bool ProcessGetVibrationList(ProcessGetVIbrationModel model, out string mssg)
//        {
//            List<vibration> modelList = null;
//            if (bll.GetVibrationList(model.xmno, model.point_name, model.beginTime.ToString(), model.endTime.ToString(), model.pageIndex, model.pageSize, model.orderName, model.sord, out modelList, out mssg))
//            {
//                model.modelList = modelList;
//                return true;
//            }
//            else
//                return false;
//        }

//        //根据项目编号 点名 时间 分页获取测振仪数据List总行数
//        public bool GetVibrationListCount(ProcessGetVIbrationModel model, out string mssg)
//        {
//            int count = 0;
//            if (bll.GetVibrationListCount(model.xmno, model.point_name, model.beginTime.ToString(), model.endTime.ToString(), out count, out mssg))
//            {
//                model.count = count;
//                return true;
//            }
//            else
//                return false;
//        }

//        /// <summary>
//        /// 数据展示日期查询条件生成
//        /// 表名
//        /// 时间字段名
//        /// 周期字段名
//        /// 点名字段名称
//        /// 项目名/项目编号
//        /// 
//        /// </summary>
//        /// <param name="model"></param>
//        public void ProcessQueryConditionCreate(ResultDataRqcxConditionCreateCondition model)
//        {

//            switch (model.type)
//            {
//                case QueryType.RQCX:
//                    string sqlsttm = "";
//                    string sqledtm = "";
//                    string mssg = "";
//                    string cycmin = "";
//                    string cycmax = "";
//                    var querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.startTime, ">=", "date_format('", "','%y-%m-%d %H:%i:%s')");
//                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
//                    {
//                        sqlsttm = querynvlmodel.str;
//                    }
//                    querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.endTime, "<=", "date_format('", "','%y-%m-%d %H:%i:%s')");
//                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
//                    {
//                        sqledtm = querynvlmodel.str;
//                    }
//                    string rqConditionStr = sqlsttm + " and " + sqledtm + " order by #_point,#_date asc ";
//                    string sqlmin = "select min(time) as mincyc from gtsensordata where project_name = '" + model.xmname + "'";
//                    string sqlmax = "select max(time) as maxcyc from gtsensordata where project_name = '" + model.xmname + "'";
//                    var processquerystanderstrModel = new QuerystanderstrModel(sqlmin, model.xmno);
//                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
//                    {
//                        model.startTime = processquerystanderstrModel.str;
//                    }
//                    processquerystanderstrModel = new QuerystanderstrModel(sqlmax, model.xmno);
//                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
//                    {
//                        model.endTime = processquerystanderstrModel.str;
//                    }
//                    model.sql = rqConditionStr;
//                    break;
//                case QueryType.ZQCX:
//                    rqConditionStr = "#_cyc >= " + model.startTime + "   and  #_cyc <= " + model.endTime + " order by #_point,#_cyc,#_date asc ";
//                    model.minCyc = model.startTime;
//                    model.maxCyc = model.endTime;

//                    model.sql = rqConditionStr;
//                    break;
//                case QueryType.QT:
//                    var processdateswdlModel = new ProcessComBLL.ProcessdateswdlModel("#_date", model.unit, model.maxTime);
//                    if (ProcessComBLL.Processdateswdl(processdateswdlModel, out mssg))
//                    {
//                        model.startTime = processdateswdlModel.sttm;
//                        model.endTime = processdateswdlModel.edtm;
//                        //model.sql = processdateswdlModel.sql;
//                    }
//                    break;


//            }
//        }
//        /// <summary>
//        /// 需要查询的字段/*
//        /// 函数名/*DBFill*/
//        /// </summary>
//        /// <param name="model"></param>
//        /// <returns></returns>

//        public bool ProcessDBFill(FillTotalStationDbFillCondition model)
//        {
//            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "time");
//            model.rqConditionStr = model.rqConditionStr.Replace("#_cyc", "cyc");
//            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "point_name");
//            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
//            string mssg = "";
//            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel("  point_name  ", model.pointname, " in ", "(", ")");
//            string sql = "";
//            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
//            {
//                sql = "select  project_name,point_name,senorno,datatype,valuetype,single_oregion_scalarvalue,first_oregion_scalarvalue,sec_oregion_scalarvalue ,single_this_scalarvalue,single_ac_scalarvalue ,cyc,time     from  gtsensordata where  project_name = '" + model.xmname + "' and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
//                sql += " and " + model.rqConditionStr;
//            }
//            model.sql = sql;
//            Console.WriteLine(sql);
//            DataTable dt = new DataTable();
//            Console.WriteLine(mssg);
//            if (GetResultDataTable(sql, model.xmname, out dt, out mssg))
//            {
//                model.dt = dt;
//                Console.WriteLine(mssg);
//                return true;
//            }

//            return false;

//        }





//    }
//}