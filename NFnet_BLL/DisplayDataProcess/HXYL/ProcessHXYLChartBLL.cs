﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess.NGN_HXYL;

namespace NFnet_BLL.DisplayDataProcess.BKG
{
    public class ProcessHXYLChartBLL
    {
        public static ProcessHXYLData hxyldata = new ProcessHXYLData();
        public static string mssg = "";
            /// <summary>
            /// 位移/角度曲线序列化
            /// </summary>
            /// <param name="model"></param>
            /// <param name="mssg"></param>
            /// <returns></returns>
            public static bool ProcessSerializestrHXYL(ProcessChartCondition model, out string mssg)
            {

                mssg = "";
                string xmname = model.xmname;
                DataTable dt = new DataTable();
                string mkhs = model.pointname;
                string sql = model.sql;
                //分模块号显示
                if (sql == "") return false;
                var hxylmodel = new QuerystanderdbIntModel(sql,model.xmno);
                if (!ProcessComBLL.Processquerystanderdb(hxylmodel, out mssg))
                {
                    //错误信息反馈
                }
                dt = hxyldata.HXYLTableFormat(model.xmno,hxylmodel.dt);

                string[] mkharr = mkhs.Split(',');

                if (mkhs != "" && mkhs != null)
                {
                    List<serie> lst = CreateSeriesFromData(model.xmno,dt, mkhs);
                    model.series = lst;
                }//如果mkh为空默认只显示过程
                return true;
            }
            /// <summary>
            /// 由数据表生成曲线组
            /// </summary>
            /// <param name="dt">数据表</param>
            /// <param name="mkhs">位移/角度点名</param>
            /// <returns></returns>
            public static List<serie> CreateSeriesFromData(int xmno,DataTable dt, string mkhs)
            {
                string[] mkharr = mkhs.Split(',');

                if (mkhs != "" && mkhs != null)
                {
                    List<serie> lst = new List<serie>();

                    omkhs[] omkharr = new omkhs[mkharr.Length];
                    for (int t = 0; t < mkharr.Length; t++)
                    {
                        serie sr = new serie();
                        serie srthis = new serie();
                        serie srac = new serie();
                        serie srrap = new serie();
                        DataView dv = new DataView(dt);
                        dv.RowFilter = "point_name='" + mkharr[t]+"'";
                        int nu = dv.Count;
                        string rri = mkharr[t];

                        //sr.Stype = "温度";
                        //sr.Name = rri.Replace("'", "");  //InstandMkhByPotName(mkharr[t], conn);
                        ////加载点
                        //formatdat(dv, sr, "wdz");
                        //lst.Add(sr);
                        //sr = new serie();
                        sr.Stype = "雨量";
                        sr.Name = rri.Replace("'", ""); //InstandMkhByPotName(mkharr[t], conn);
                        //加载点
                        formatdat(dv, sr, "rvl");
                        lst.Add(sr);
                       

                    }


                    return lst;
                }
                return null;
            }
            /// <summary>
            /// 位移/角度数据生成曲线
            /// </summary>
            /// <param name="dv">数据表视图</param>
            /// <param name="sr">曲线</param>
            /// <param name="valstr">值字段名称</param>
            public static void formatdat(DataView dv, serie sr, string valstr)
            {

                sr.Pts = new pt[dv.Count];
                int i = 0;
                foreach (DataRowView drv in dv)
                {
                    string sj = drv["time"].ToString();
                    string v = drv[valstr].ToString();
                    DateTime d = Convert.ToDateTime(sj);
                    int year = d.Year;
                    int mon = d.Month;
                    int day = d.Day;
                    int hour = d.Hour;
                    int minute = d.Minute;
                    int second = d.Second;
                    sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                    i++;
                }

            }
        
    }
}