﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.Data;
using NFnet_BLL.DataProcess;
using System.IO;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.BKG
{
    /// <summary>
    /// 全站仪数据报表打印业务逻辑处理类
    /// </summary>
    public class ProcessHXYLReportPrintBLL
    {
        public static HXYLReportHelper reportHelper = new HXYLReportHelper();
        /// <summary>
        /// 全站仪单点多周期数据报表打印
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMcuReportPrintSPMC(PrintCondition pcmodel, out string mssg)
        {
            var model = pcmodel as ProcessTotalStationReportModel;
            List<ChartCreateEnviroment> lce = new List<ChartCreateEnviroment>();
            HXYLReportHelper.SetTabValSPMC(string.Format("{0}数据报表", model.xmname), model.tabHead, model.dt, model.title, model.sheetName, model.startTime, model.endTime, model.index, model.xlspath, out lce);
            model.cce = lce;
            mssg = "";
            return true;

        }
        /// <summary>
        /// 全站仪多点多周期数据报表打印
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        //public bool ProcessTotalStationReportPrintMPMC(PrintCondition pcmodel, out string mssg)
        //{
        //    var model = pcmodel as ProcessTotalStationMPMCReportModel ;
        //    ChartCreateEnviroment lce = new ChartCreateEnviroment();
        //    ReportHelper.SetTabValMPMC(string.Format("{0}数据报表", model.xmname), model.tabHead, model.dt, model.title, model.sheetName, model.startTime, model.endTime, model.index, model.xlspath, out lce);
        //    mssg = "";
        //    return true;

        //}
        ///// <summary>
        ///// 全站仪多点单周期数据报表打印
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="mssg"></param>
        ///// <returns></returns>
        //public bool ProcessTotalStationReportPrintMPSC(PrintCondition pcmodel, out string mssg)
        //{
        //    var model = pcmodel as ProcessTotalStationReportModel;
        //    List<ChartCreateEnviroment> lce = new List<ChartCreateEnviroment>();
        //    ReportHelper.SetTabValMPSC(string.Format("{0}数据报表", model.xmname), model.tabHead, model.dt, model.title, model.sheetName, model.startTime, model.endTime, model.index, model.xlspath, out lce);
        //    model.cce = lce;
        //    mssg = "";
        //    //ReportHelper.SetTabValMPSC(string.Format("{0}数据报表", model.xmname), model.tabHead, model.dt, model.title, model.sheetName, model.startTime, model.index);
        //    //mssg = "";
        //    return true;

        //}
        //public static ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        //public static ProcessGnssReportPrintBLL gnssReportPrintBLL = new ProcessGnssReportPrintBLL();
        /// <summary>
        /// 单点多周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationSPMCReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                HXYLReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成");
                var processTotalStationReportModel = new ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "点名,分钟雨量(m), 时间", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                if (ProcessMcuReportPrintSPMC(processTotalStationReportModel, out mssg))
                {
                    ExceptionLog.ExceptionWrite("填充数据完成");
                    string mappath = model.xlspath;
                    //SeriesCreate(context);
                    ExceptionLog.ExceptionWrite(model.exportpath);
                    HXYLReportHelper.WriteToFile(model.exportpath);
                    //model.exportpath = model.exportpath; //Path.GetDirectoryName(mappath) + string.Format("\\{0}_表面位移_第" + model.stCyc + "周期-第" + model.etCyc + "周期数据.xls", model.xmname);
                    HXYLChartHelper chartHelper = new HXYLChartHelper();
                    //chartHelper.ChartReportHelper(model.exportpath,DataTableHelper.ProcessDataTableRowsCount(model.dt,"cyc"),"周期");
                    chartHelper.ChartListReportHelper(model.exportpath, processTotalStationReportModel.cce);
                }
                else
                {

                }


                mssg = "";
                return false;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("雨量报表输出出错，错误信息:" + ex.Message);
                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }
        ///// <summary>
        ///// 多点多单周期报表打印
        ///// </summary>
        ///// <returns></returns>
        //public bool ProcessTotalStationMPSCReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        //{

        //    ReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
        //    var processTotalStationReportModel = new ProcessGnssReportPrintBLL.ProcessTotalStationReportModel(string.Format("第{0}周期", model.stCyc), string.Format("{0}", model.etCyc), model.xmname, "点名, N(m), E(m), Z(m), ΔN(m), ΔE(m), ΔZ(m), ΣΔN(m), ΣΔE(m), ΣΔZ(m), 时间", "{0}", model.dt, "point_name", model.exportpath);
        //    if (gnssReportPrintBLL.ProcessTotalStationReportPrintMPSC(processTotalStationReportModel, out mssg))
        //    {
        //        string mappath = model.xlspath;
        //        //SeriesCreate(context);
        //        ReportHelper.WriteToFile(model.exportpath);
        //        //model.exportpath = model.exportpath; //Path.GetDirectoryName(mappath) + string.Format("\\{0}_表面位移_第" + model.stCyc + "周期-第" + model.etCyc + "周期数据.xls", model.xmname);
        //        //ChartHelper chartHelper = new ChartHelper();
        //        //chartHelper.ChartReportHelper(model.exportpath, DataTableHelper.ProcessDataTableRowsCount(model.dt, "point_name"), "点名");
        //        ChartHelper chartHelper = new ChartHelper();
        //        //chartHelper.ChartReportHelper(model.exportpath,DataTableHelper.ProcessDataTableRowsCount(model.dt,"cyc"),"周期");
        //        chartHelper.ChartListReportHelper(model.exportpath, processTotalStationReportModel.cce);
        //    }
        //    else
        //    {

        //    }


        //    mssg = "";
        //    return false;
        //}

        ///// <summary>
        ///// 多点多单周期报表打印
        ///// </summary>
        ///// <returns></returns>
        //public bool ProcessTotalStationMPMCReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        //{

        //    ReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
        //    var processTotalStationReportModel = new ProcessGnssReportPrintBLL.ProcessTotalStationMPMCReportModel(string.Format("第{0}周期", model.stCyc), string.Format("第{0}周期", model.etCyc), model.xmname, "点名,周期,N(m), E(m), Z(m), ΔN(m), ΔE(m), ΔZ(m), ΣΔN(m), ΣΔE(m), ΣΔZ(m), 时间", "{0}", model.dt, "point_name", model.exportpath);
        //    //List<string> pname = Tool.DataTableHelper.ProcessDataTableFilter(model.dt,"point_name");
        //    if (gnssReportPrintBLL.ProcessTotalStationReportPrintMPMC(processTotalStationReportModel, out mssg))
        //    {
        //        string mappath = model.xlspath;
        //        //SeriesCreate(context);
        //        ReportHelper.WriteToFile(model.exportpath);
        //        //model.exportpath = model.exportpath; //Path.GetDirectoryName(mappath) + string.Format("\\{0}_表面位移_第" + model.stCyc + "周期-第" + model.etCyc + "周期数据.xls", model.xmname);
        //        //ChartHelper chartHelper = new ChartHelper();
        //        //chartHelper.ChartReportHelper(model.exportpath,DataTableHelper.ProcessDataTableRowsCount(model.dt,"cyc"),"周期");
        //        //chartHelper.ChartSingleReportHelper(model.exportpath, processTotalStationReportModel.cce);
        //    }
        //    else
        //    {

        //    }


        //    mssg = "";
        //    return false;
        //}

        public class ProcessTotalStationSPMCReportModel
        {
            public string stCyc { get; set; }
            public string etCyc { get; set; }
            public string xmname { get; set; }
            public string xlspath { get; set; }
            public DataTable dt { get; set; }
            public string exportpath { get; set; }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
            }

        }





        /// <summary>
        /// 全站仪数据表打印类
        /// </summary>
        public class ProcessTotalStationReportModel : PrintCondition
        {
            public List<ChartCreateEnviroment> cce { get; set; }
            public ProcessTotalStationReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }

        public class ProcessTotalStationMPMCReportModel : PrintCondition
        {
            public ChartCreateEnviroment cce { get; set; }
            public List<string> pnames { get; set; }
            public ProcessTotalStationMPMCReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }



    }
}