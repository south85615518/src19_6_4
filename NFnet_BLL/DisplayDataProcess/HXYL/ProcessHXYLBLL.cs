﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.HXYL
{
    public class ProcessHXYLBLL
    {
        public static MDBDATA.BLL.HXYL hxylbll = new MDBDATA.BLL.HXYL();
        public bool HXYLPointLoad(HXYLPointLoadModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (hxylbll.HXYLPointLoad(model.xmno, out ls, out mssg))
            {
                model.pointnamelist = ls;
                return true;
            }
            return false;
        }
        public class HXYLPointLoadModel
        {
            public int xmno { get; set; }
            public List<string> pointnamelist { get; set; }
            public HXYLPointLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool HXYLID(HXYLIDmodel model, out string mssg)
        {
            string id = "";
            if (hxylbll.HXYLID(model.xmno, model.pointname, out id, out mssg))
            {
                model.id = id;
                return true;
            }
            return false;
        }
        public class HXYLIDmodel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public string id { get; set; }
            public HXYLIDmodel(int xmno, string pointname)
            {
                this.xmno = xmno;
                this.pointname = pointname;
            }
        }
        public bool HXYLPointname(HXYLPointmodel model, out string mssg)
        {
            string pointname = "";
            if (hxylbll.HXYLPointName(model.xmno, model.id, out pointname, out mssg))
            {
                model.pointname = pointname;
                return true;
            }
            return false;
        }
        public class HXYLPointmodel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public string id { get; set; }
            public HXYLPointmodel(int xmno, string id)
            {
                this.xmno = xmno;
                this.id = id;
            }
        }
        public bool ProcessHXYLMaxTime(ProcessHXYLMaxTimeModel model, out string mssg)
        {
            DateTime dt = new DateTime();
            if (hxylbll.HXYLMaxTime(model.pointnostr, out dt, out mssg))
            {
                model.maxTime = dt;
                return true;
            }
            return false;
        }
        public class ProcessHXYLMaxTimeModel
        {
            public string pointnostr { get; set; }
            public DateTime maxTime { get; set; }
            public ProcessHXYLMaxTimeModel(string pointnostr)
            {
                this.pointnostr = pointnostr;
            }
        }

        public bool ProcessHXYLAdd(MDBDATA.Model.hxyl model, out string mssg)
        {
            return hxylbll.Add(model, out mssg);
        }
        public bool ProcessHXYLAddMDB(MDBDATA.Model.hxyl model, out string mssg)
        {
            return hxylbll.AddMDB(model, out mssg);
        }
        public bool ProcessHXYLUpdate(MDBDATA.Model.hxyl model, out string mssg)
        {
            return hxylbll.update(model, out mssg);
        }

        public bool ProcessHXYLDATALoad(out List<MDBDATA.Model.hxyl> ls, out string mssg)
        {
            return hxylbll.HXYLDATALoad(out ls, out mssg);

        }
        public bool ProcessHXYLDATALoad(ProcessHXYLDATALoadModel model, out string mssg)
        {
            List<MDBDATA.Model.hxyl> ls = new List<MDBDATA.Model.hxyl>();
            if (hxylbll.HXYLDATALoad(model.maxTime, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;

        }
        public class ProcessHXYLDATALoadModel
        {
            public List<MDBDATA.Model.hxyl> ls { get; set; }
            public DateTime maxTime { get; set; }
            public ProcessHXYLDATALoadModel(DateTime maxTime)
            {
                this.maxTime = maxTime;
            }
        }
        public bool ProcessMaxTime(out DateTime maxTime, out string mssg)
        {
            return hxylbll.maxTime(out maxTime, out mssg);
        }

        public bool ProcessHXYLDataTime(HXYLDataTimeCondition model, out string mssg)
        {

            MDBDATA.Model.hxyl hxylDataModel = new MDBDATA.Model.hxyl();
            if (hxylbll.GetModel(model.pointid, model.dt, out hxylDataModel, out mssg))
            {
                model.model = hxylDataModel;
                return true;

            }
            return false;
        }
        public class HXYLDataTimeCondition
        {
            public int pointid { get; set; }
            public DateTime dt { get; set; }
            public MDBDATA.Model.hxyl model { get; set; }
            public HXYLDataTimeCondition(int pointid, DateTime dt)
            {
                this.pointid = pointid;
                this.dt = dt;
            }
        }
        public bool ProcessPointNewestDateTimeGet(ProcessPointNewestDateTimeGetModel model, out string mssg)
        {

            DateTime dt = new DateTime();
            if (hxylbll.PointNewestDateTimeGet(model.pointid, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessPointNewestDateTimeGetModel
        {
            public int pointid { get; set; }
            public DateTime dt { get; set; }
            public ProcessPointNewestDateTimeGetModel(int pointid)
            {
                this.pointid = pointid;
            }
        }
    }
}