﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;
using System.Data.Odbc;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.SS.UserModel;
using NFnet_DAL;
using NFnet_MODAL;
using NFnet_BLL;
namespace NFnet.项目设置.clss
{
    /// <summary>
    /// 根据数据文件的类型转库支持.txt\.dat\.xls
    /// </summary>
    public class TranstByType
    {
        private TranstInfo tif = null;
        private string tailstr = "";//为转换完成尾巴
        private HSSFWorkbook hssfWorkBook = null;
        public static database db = new database();
        public HSSFWorkbook HssfWorkBook
        {
            get { return hssfWorkBook; }
            set { hssfWorkBook = value; }
        }
        public string Tailstr
        {
            get { return tailstr; }
            set { tailstr = value; }
        }
        public TranstInfo Tif
        {
            get { return tif; }
            set { tif = value; }
        }
        private string filePot;

        public string FilePot
        {
            get { return filePot; }
            set { filePot = value; }
        }
        public TranstByType(TranstInfo tif, string filePot)
        {
            this.tif = tif;
            this.filePot = filePot;
        }
        public TranstByType(TranstInfo tif)
        {
            this.tif = tif;
        }
        public TranstByType()
        {

        }
        //private 
        /// <summary>
        /// 读取100条记录的函数记录以回车符分隔
        /// </summary>
        /// <returns></returns>
        public void GetCachTab(int index, int tabcount, string yq)
        {
            //获取文件名称的后缀名
            //string filename = tif.Fs.
            //if (tif.Filename.IndexOf(".txt") != -1)
            //{
            //GetTxtCachTab(index, tabcount);
            OdbcConnection conn = db.GetStanderConn(tif.Xmname);
            //将文件拷贝到指定目录下
            MultiFileUpload mfupload = new MultiFileUpload(tif.Fs, tif.Filename, "E:\\数据库文件");
            string filename = mfupload.UploadFile();
            //获取文件数据剧的总条数
            int lines = GetLinesFromDataFile(filename);
            bool isSuccess = InsertIntoTabDestrimilly(filename, tif.Tabname, conn, lines);
            LoadLine ld = null;
            //string tabJclx = ; 
            if (!isSuccess)
            {
                Err.ErrMessgeList.Add(yq + "数据导入失败");
                ld = new LoadLine(index, tabcount, tif.Tabname, 1, 0);
            }
            else
            {
                ld = new LoadLine(index, tabcount, tif.Tabname, 1, 1);
            }

            InsertOrUpdateLoadLine(tif.Tabname, conn, ld);
        }
        /// <summary>
        /// 从文件数据中获取到文件数据中的总条数
        /// </summary>
        /// <returns></returns>
        public int GetLinesFromDataFile(string path)
        {
            FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read);
            byte[] byteN = System.Text.Encoding.UTF8.GetBytes("\n");
            byte[] buff = new byte[byteN.Length];
            int i = 0;
            string str = "";
            int cont = 0;
            while ((i = fs.Read(buff, 0, buff.Length)) > 0)
            {
                str = System.Text.Encoding.UTF8.GetString(buff);
                buff = new byte[byteN.Length];
                if (str == "\n")
                {
                    cont++;
                    str = "";
                }
            }
            fs.Close();
            return cont;

        }
        /// <summary>
        /// 读取excel数据模板到内存表中
        /// </summary>
        public void GetExcelCachTab1()
        {
            string sheetName = "";//工作薄名称
            int rowStart = 0;

            InitializeWorkbook(tif.Fs);
            //DataTable dt = SetRecordInsertOrUpdateStandDB(lls, tif);
            //应该由前台传递定位到那个工作薄
            ISheet sheet = hssfWorkBook.GetSheet("Sheet1");
            //创建内存表
            DataTable dt = CreateCachDB(tif.Tabname, tif.Xmname);
            List<string> tabnamelist = new List<string>();
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                tabnamelist.Add(dt.Columns[j].ColumnName);
            }
            int tablength = tabnamelist.Count;
            dt = CreateStringTab(tabnamelist);
            int startRow = rowStart;
            while (true)
            {
                int endsign = 0;
                IRow row = sheet.GetRow(startRow);
                startRow++;
                DataRow dr = dt.NewRow();
                TranstCase tce = tif.Tc;
                int i = 0;
                foreach (Dictionary<string, string> dss in tce.Mapcase)
                {
                    if (row == null)
                    {
                        endsign = 1;
                        break;
                    }
                    if (dss[tabnamelist[i]] == null || dss[tabnamelist[i]] == "")
                    {
                        i++; continue;
                    }
                    string v = row.GetCell(Int32.Parse(dss[tabnamelist[i]])).CellType.ToString();
                    if (v == CellType.NUMERIC.ToString())
                    {
                        Double numcstr = row.GetCell(Int32.Parse(dss[tabnamelist[i]])).NumericCellValue;
                        if (numcstr <= 0)
                        {
                            dr[tabnamelist[i]] = row.GetCell(Int32.Parse(dss[tabnamelist[i]])).NumericCellValue.ToString();
                        }
                        else
                        {
                            string datestr = row.GetCell(Int32.Parse(dss[tabnamelist[i]])).DateCellValue.ToString();
                            DateTime time = Convert.ToDateTime(datestr);
                            if (1990 < time.Year && time.Year < 2050)//此处的区分方法很不严谨
                            {
                                dr[tabnamelist[i]] = row.GetCell(Int32.Parse(dss[tabnamelist[i]])).DateCellValue.ToString();
                            }
                            else
                            {
                                dr[tabnamelist[i]] = row.GetCell(Int32.Parse(dss[tabnamelist[i]])).NumericCellValue.ToString();
                            }
                        }

                    }
                    else
                    {
                        dr[tabnamelist[i]] = row.GetCell(Int32.Parse(dss[tabnamelist[i]])).StringCellValue.ToString();

                    }
                    i++;
                }
                dt.Rows.Add(dr);
                if (endsign == 1) { dt.Rows.RemoveAt(dt.Rows.Count - 1); break; }
                //if()
                if (startRow % 100 == 0 && startRow > 0)
                {
                    TranstCachTab(dt);
                    dt = CreateCachDB(tif.Tabname, tif.Xmname);
                }
            }
            TranstCachTab(dt);

        }
        /// <summary>
        /// 将excel文件上传到
        /// </summary>
        public void GetExcelCachTab(int index, int tabCount)
        {
            //将文件拷贝到指定目录下
            MultiFileUpload mfupload = new MultiFileUpload(tif.Fs, tif.Filename, this.filePot);
            OdbcConnection conn = db.GetStanderConn(tif.Xmname);
            string filename = mfupload.UploadFile();
            //测出的文件名就是excel文件的文件路径
            ConntectExcel cel = new ConntectExcel(filename);
            int allCount = cel.GetExcelLen();
            int offset = 0;
            while (true)
            {
                DataTable StanderDt = CreateCachDB(tif.Tabname, tif.Xmname);
                DataTable dt = cel.ExcelImport(offset, 20, StanderDt, tif);
                TranstCachTab(dt);
                int cont = dt.Rows.Count;
                offset += dt.Rows.Count;
                LoadLine ld = new LoadLine(index, tabCount, tif.Tabname, allCount, offset);
                InsertOrUpdateLoadLine(tif.Tabname, conn, ld);
                if (cont < 18)
                {
                    break;
                }
                //生成进度条

            }


        }
        /// <summary>
        /// 插入或者更新进度表
        /// </summary>
        public void InsertOrUpdateLoadLine(string tabname, OdbcConnection conn, LoadLine ld)
        {
            int sort = ld.Sort;
            int tabCount = ld.Tabs;
            double percent = (double)((float)ld.NowCount / (float)ld.AllCount);
            //判断进度表中是否有该表转库的进度的记录
            string selectSql = @"select * from LoadLine where tabName = '" + tabname + "' and sort = " + sort;
            //插入进度表进度条信息
            string insertSql = @"insert into LoadLine(tabname,loadline,sort,tabCount,lasttime) value(
'" + tabname + "','" + percent.ToString() + "'," + sort + "," + tabCount + ",'" + DateTime.Now + "')";
            string updateSql = @"update LoadLine set loadline = '" + percent.ToString() + "',lastTime = '" + DateTime.Now + "' where tabName='" + tabname + "' and sort=" + sort;
            string cont = querysql.querystanderstr(selectSql, conn);
            updatedb udb = new updatedb();
            if (cont != null && cont != "")
            {
                //更新进度
                udb.UpdateStanderDB(updateSql, conn);
            }
            else
            {
                udb.UpdateStanderDB(insertSql, conn);
            }
        }
        public DataTable CreateStringTab(List<string> tabList)
        {
            DataTable dt = new DataTable();
            foreach (string tabcolumn in tabList)
            {
                dt.Columns.Add(tabcolumn, typeof(string));
            }
            return dt;
        }
        /// <summary>
        /// 从文件中获取到文件中记录的总数
        /// </summary>
        /// <returns></returns>
        public long GetAllCountFromTxt(Stream fs)
        {
            //此处的技术方法需要调整,应换成读取每个回车键,数回车符
            int len = 0;
            string str = "";
            while (true)
            {
                byte[] buff = new byte[1];
                if (fs.Read(buff, 0, 1) > 0)
                {
                    str += System.Text.Encoding.UTF8.GetString(buff);
                    len++;
                    if (str.IndexOf("\n") != -1)
                    {
                        //第一条记录读完了,仅支持以回车结尾
                        break;
                    }
                    buff = new byte[1];

                }
                else
                {
                    return 0;

                }
            }
            long allCount = fs.Length / len;
            return allCount;
        }
        /// <summary>
        /// 将标准文件数据直接导入到数据库中
        /// </summary>
        public bool InsertIntoTabDestrimilly(string filePath/*文件路径*/ , string tabName/*表名*/ , OdbcConnection conn, int lines)
        {
         
            string sql = "load data infile \"" + filePath + "\" IGNORE  INTO   table  " + tabName + " character set utf8  fields terminated by ',' lines terminated by \"\n\"";
            updatedb udb = new updatedb();

            bool isSuccess = udb.UpdateTranstStanderDB(sql, conn, lines);

            return isSuccess;

        }

        /// <summary>
        /// 将数据表中的数据直接导出到文件中
        /// </summary>
        public string OutputDataIntoFile(string filePath/*文件路径*/ , OdbcConnection conn, int lines,string sql)
        {
            string centerPart = @"INTO OUTFILE '" + filePath + "' FIELDS TERMINATED BY ','  LINES TERMINATED BY '\n'";
            sql = sql.Replace("#",centerPart);
            updatedb udb = new updatedb();

            bool isSuccess = udb.UpdateTranstStanderDB(sql, conn, lines);
            if (isSuccess)
            {
                //返回文件的路径
                return filePath;
            }
            return "";

        }






        /// <summary>
        /// 获取txt文件数据格式的内存表
        /// </summary>
        public void GetTxtCachTab(int index, int tabCount)
        {
            OdbcConnection conn = db.GetStanderConn(tif.Xmname);
            long allCount = GetAllCountFromTxt(tif.Fs);
            int offset = 0;
            Stream fs = tif.Fs;
            fs.Seek(0, 0);
            int i = 0;
            byte[] buff = new byte[100];
            string str = "";
            List<List<string>> lls = null;
            int len = 0;
            //string strtemp = "";
            while ((len = fs.Read(buff, 0, buff.Length)) > 0)
            {

                str += System.Text.Encoding.UTF8.GetString(buff);
                if (i % 20 == 0 && i > 0)
                {
                    lls = GetRecordsBySplit(str, false);//此方法读取需要提剪掉一行，因为最后一行读到的数据很可能不完整                            
                    lls.RemoveAt(lls.Count - 1);
                    DataTable dt = SetRecordInsertOrUpdateStandDB(lls, tif);
                    TranstCachTab(dt);
                    str = null;
                    str += tailstr;
                    i = i % 20;
                    offset += lls.Count;//当前读取的记录数
                    //更新进度条表
                    LoadLine ld = new LoadLine(index, tabCount, tif.Tabname, (int)allCount, offset);
                    InsertOrUpdateLoadLine(tif.Tabname, conn, ld);
                }
                i++;
                buff = new byte[100];
            }
            //当记录数少于100条时
            if (i <= 20 && i > 0)
            {
                lls = GetRecordsBySplit(str, true);

                DataTable dt = SetRecordInsertOrUpdateStandDB(lls, tif);
                TranstCachTab(dt);
                offset += lls.Count;//当前读取的记录数
                //更新进度条表
                LoadLine ld = new LoadLine(index, tabCount, tif.Tabname, (int)allCount, (int)allCount);
                InsertOrUpdateLoadLine(tif.Tabname, conn, ld);
            }
            fs.Close();
            //最后清除掉session

        }
        public void TranstCachTab(DataTable dt)
        {



            //根据要转的标准表名确定主键
            string primary = GetCondicateMapInit(tif.Tabname);

            string dbname = querysql.queryaccessdbstring("select dbase from xmconnect where xmname ='" + tif.Xmname + "'");
            //querysql.querystanderdb("");
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);

            TranstTab(dt, conn, tif.Tabname, primary);

        }
        /// <summary>
        /// 根据分隔符获取到记录中的数值存到list中目前只支持‘,’
        /// </summary>
        /// <returns></returns>
        public List<List<string>> GetRecordsBySplit(string strsrc, bool fileFinish)
        {
            strsrc = strsrc.Replace("\r", "");
            List<string> recordList = strsrc.Split('\n').ToList();//行与行之间以回车符分隔
            //判断是否要剪去尾串
            if (!fileFinish)
                tailstr = recordList[recordList.Count - 1];
            List<List<string>> recordsList = new List<List<string>>();
            foreach (string rcd in recordList)
            {
                if (rcd == "\t" || rcd == "")
                {
                    continue;
                }

                List<string> record = rcd.Split(',').ToList();//记录与记录之间以逗号分隔

                recordsList.Add(record);
            }

            return recordsList;
        }
        public void InitializeWorkbook(Stream st)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added. 

            FileStream file = st as FileStream;
            FileStream fs = File.Open("C:\\Users\\pc\\Desktop\\红山龙光_m3task01_cycangnet.xls", FileMode.Open, FileAccess.Read);
            hssfWorkBook = new HSSFWorkbook(st);

            //create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfWorkBook.DocumentSummaryInformation = dsi;

            //create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfWorkBook.SummaryInformation = si;
        }
        /// <summary>
        /// 转库函数
        /// </summary>
        public void TranstTab(DataTable dt, OdbcConnection conn, string tabname, string primary)
        {
            List<string> primaryKey = primary.ToLowerInvariant().Split(',').ToList();
            //查询标准样表
            DataTable typeTab = new DataTable();
            string sqlType = "select * from " + tabname + " where  0 = 1";
            typeTab = querysql.querystanderdb(sqlType, conn);
            //更新字段
            List<string> updateSon = new List<string>();
            //插入字段
            List<string> zdSon = new List<string>();
            //插入数值
            List<string> valSon = new List<string>();
            //获取表的字段名建立where查询模板
            List<string> whereSon = new List<string>();//where查询条件
            int i = 0; if (!(primaryKey.Contains("ID") || primaryKey.Contains("id")))
                i = 1;
            for (; i < typeTab.Columns.Count; i++)
            {
                string typestr = typeTab.Columns[i].DataType.ToString();
                //判断类型凡是varchar都需要加上''
                if (primaryKey.Contains(typeTab.Columns[i].ColumnName.ToLowerInvariant()))
                {

                    if (typestr == "System.String" || typestr == "System.DateTime")
                    {
                        whereSon.Add("  " + typeTab.Columns[i].ColumnName + "='#_" + typeTab.Columns[i].ColumnName + "' ");
                        zdSon.Add(typeTab.Columns[i].ColumnName);
                        valSon.Add("'#_" + typeTab.Columns[i].ColumnName + "'");

                    }
                    else
                    {
                        whereSon.Add("  " + typeTab.Columns[i].ColumnName + "=#_" + typeTab.Columns[i].ColumnName + "  ");
                        zdSon.Add(typeTab.Columns[i].ColumnName);
                        valSon.Add("#_" + typeTab.Columns[i].ColumnName);
                    }

                }
                else if (typestr == "System.String" || typestr == "System.DateTime")
                {

                    zdSon.Add(typeTab.Columns[i].ColumnName);
                    valSon.Add("'#_" + typeTab.Columns[i].ColumnName + "'");
                    updateSon.Add("  " + typeTab.Columns[i].ColumnName + "='#_" + typeTab.Columns[i].ColumnName + "' ");
                }
                else
                {

                    zdSon.Add(typeTab.Columns[i].ColumnName);
                    valSon.Add("#_" + typeTab.Columns[i].ColumnName);
                    updateSon.Add("  " + typeTab.Columns[i].ColumnName + "=#_" + typeTab.Columns[i].ColumnName + "  ");
                }

            }
            string whereSonStr = string.Join("and", whereSon);
            string zdSonStr = string.Join(",", zdSon);
            string valSonStr = string.Join(",", valSon);
            string updateSonStr = string.Join(",", updateSon);

            DataView dv = new DataView(dt);
            int brk = 0;
            foreach (DataRowView drv in dv)
            {
                string whereTmp = whereSonStr;
                string valTmp = valSonStr;
                string updateTmp = updateSonStr;
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    string a = drv[dt.Columns[j].ColumnName].ToString();
                    if (drv[dt.Columns[j].ColumnName].ToString() == null || drv[dt.Columns[j].ColumnName].ToString() == "")
                    {
                        whereTmp = whereTmp.Replace("#_" + dt.Columns[j].ColumnName, "null");
                        valTmp = valTmp.Replace("#_" + dt.Columns[j].ColumnName, "null");
                        updateTmp = updateTmp.Replace("#_" + dt.Columns[j].ColumnName, "null");
                    }
                    else
                    {
                        whereTmp = whereTmp.Replace("#_" + dt.Columns[j].ColumnName, drv[dt.Columns[j].ColumnName].ToString());
                        valTmp = valTmp.Replace("#_" + dt.Columns[j].ColumnName, drv[dt.Columns[j].ColumnName].ToString());
                        updateTmp = updateTmp.Replace("#_" + dt.Columns[j].ColumnName, drv[dt.Columns[j].ColumnName].ToString());
                    }
                }
                string sql = "select * from " + tabname + "  where  ";
                sql += whereTmp;
                DataTable oneRecord = new DataTable();
                oneRecord = querysql.querystanderdb(sql, conn);
                string sqlInsert = "insert into " + tabname;
                sqlInsert += " (" + zdSonStr + ") values (" + valTmp + ")";
                if (oneRecord.Rows.Count <= 0)//如果不存在则插入
                {

                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(sqlInsert, conn);

                }
                else if (oneRecord.Columns.Count > primaryKey.Count)//如果存在则更新
                {
                    string updateSql = "update " + tabname + " set " + updateTmp + " where" + whereTmp;
                    updatedb udb = new updatedb();
                    udb.UpdateStanderDB(updateSql, conn);
                }
                //如果存在则更新__________________________
                //brk++;
                //if (brk > 4) break;
            }
        }
        /// <summary>
        /// 根据之前的专库设置将记录插入到标准库中去
        /// </summary>
        /// <param name="lls"></param>
        /// <param name="tif"></param>
        public DataTable SetRecordInsertOrUpdateStandDB(List<List<string>> lls, TranstInfo tif)
        {
            //创建内存表
            DataTable dtSatander = CreateCachDB(tif.Tabname, tif.Xmname);
            DataTable dt = new DataTable();
            for (int j = 0; j < dtSatander.Columns.Count; j++)
            {
                dt.Columns.Add(dtSatander.Columns[j].ColumnName);
            }
            List<string> tabnamelist = new List<string>();
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                tabnamelist.Add(dt.Columns[j].ColumnName);
            }
            int tablength = tabnamelist.Count;
            foreach (List<string> ls in lls)
            {
                //判断这是不是个无效的字符
                if (ls[0].IndexOf('\t') != -1 || ls[0].IndexOf('\0') != -1 || ls[0].IndexOf('\r') != -1) continue;
                DataRow dr = dt.NewRow();
                int i = 0;
                for (i = 0; i < ls.Count; i++)
                {
                    dr[i] = ls[i];//一一对应
                }
                dt.Rows.Add(dr);
            }
            return dt;

        }
        /// <summary>
        /// 根据transtInfo对象创建内存映射表
        /// </summary>
        /// <returns></returns>
        public DataTable CreateCachDB(string tabname, string xmname)
        {
            string dbname = querysql.queryaccessdbstring("select dbase from xmconnect where xmname ='" + xmname + "'");
            //querysql.querystanderdb("");
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            //tabname = IsTabExist(tabname,xmname);
            if (!IsTabExist(tabname, xmname))
            {
                string[] projectNameAndTaskName = tabname.Split('_');
                //创建表
                DataTableCreate.TableCreateByProjectAndTask(conn, projectNameAndTaskName[0], projectNameAndTaskName[1]);
                //在task中插入该信息
                string sqlInsert = "insert into task (projectName,taskName)values('" + projectNameAndTaskName[0] + "','" + projectNameAndTaskName[1] + "')"; updatedb udb = new updatedb();
                udb.UpdateStanderDB(sqlInsert, conn);
            }
            string sqlCreate = "select * from  " + tabname + " where 0 =1";
            DataTable dt = querysql.querystanderdb(sqlCreate, conn);
            return dt;
        }
        /// <summary>
        /// 判断表是否存在
        /// </summary>
        /// <returns></returns>
        public bool IsTabExist(string tabname, string xmname)
        {
            string dbname = querysql.queryaccessdbstring("select dbase from xmconnect where xmname ='" + xmname + "'");
            //querysql.querystanderdb("");
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            List<string> ls = new List<string>();
            //判断表名是否存在
            string sqltab = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" + dbname + "'";
            List<string> tabNameList = querysql.querystanderlist(sqltab, conn);
            if (!tabNameList.Contains(tabname.ToLowerInvariant())) return false;
            return true;
        }

        /// <summary>
        /// 各表的候选码初始化
        /// </summary>
        public string GetCondicateMapInit(string tabname)
        {
            List<Dictionary<string, string>> ldss = new List<Dictionary<string, string>>();
            Dictionary<string, string> tabmap = new Dictionary<string, string>();
            tabmap.Add("sensor", "MKH,SJBH");
            tabmap.Add("data", "MKH,SJ");
            tabmap.Add("GPSBaseStation", "Smos_Name");
            tabmap.Add("GPSBaseStationResult_Vector", "GPSBaseName,DateTime");
            tabmap.Add("CxGroup", "Name");
            tabmap.Add("CxPotName", "Name");
            tabmap.Add("CxResultData", "Name,MonitorTime");
            tabmap.Add("YlPotName", "Name");
            tabmap.Add("YlResultData", "RID,MonitorTime");
            tabmap.Add("studypoint", "POINT_NAME");
            tabmap.Add("task", "projectName,taskName");
            tabmap.Add("cycangnet", "POINT_NAME,Time");
            if (tabname.IndexOf("cycangnet") != -1)
            {
                return tabmap["cycangnet"];
            }
            return tabmap[tabname];

        }

    }



}