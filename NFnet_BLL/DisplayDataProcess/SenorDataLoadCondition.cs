﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.Inclinometer
{
    public class SenorDataLoadCondition
    {
        public int xmno { get; set; }
        public string pointname { get; set; }
        public DateTime starttime { get; set; }
        public DateTime endtime { get; set; }
        public int startPageIndex { get; set; }
        public int endPageIndex { get; set; }
        public string sord { get; set; }
        public DataTable dt { get; set; }
        public SenorDataLoadCondition(int xmno, string pointname, DateTime starttime, DateTime endtime, int startPageIndex, int endPageIndex, string sord)
        {
            this.xmno = xmno;
            this.pointname = pointname;
            this.starttime = starttime;
            this.endtime = endtime;
            this.startPageIndex = startPageIndex;
            this.endPageIndex = endPageIndex;
            this.sord = sord;
        }
    }
}