﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class BKGMcuPointLoadCondition
    {
        public string xmname { get; set; }
        public List<string> pointnamelist { get; set; }
        public BKGMcuPointLoadCondition(string xmname)
        {
            this.xmname = xmname;
        }
    }
}