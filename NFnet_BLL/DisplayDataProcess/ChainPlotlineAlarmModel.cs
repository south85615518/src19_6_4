﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer
{
    public class ChainPlotlineAlarmModel
    {
        public string pointname { get; set; }
        public NGN.Model.fixed_inclinometer_chainalarmvalue firstalarm { get; set; }
        public NGN.Model.fixed_inclinometer_chainalarmvalue secalarm { get; set; }
        public NGN.Model.fixed_inclinometer_chainalarmvalue thirdalarm { get; set; }

    }
}