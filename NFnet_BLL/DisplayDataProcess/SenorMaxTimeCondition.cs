﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SenorMaxTimeCondition
    {
        public int xmno { get; set; }
        public DateTime dt { get; set; }
        public data.Model.gtsensortype datatype { get; set; }
        public SenorMaxTimeCondition(int xmno, data.Model.gtsensortype datatype)
        {
            this.xmno = xmno;
            this.datatype = datatype;
        }
        public SenorMaxTimeCondition(int xmno)
        {
            this.xmno = xmno;
        }
        public SenorMaxTimeCondition()
        {
            
        }
    }
}