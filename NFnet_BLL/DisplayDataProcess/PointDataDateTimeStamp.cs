﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class PointDataDateTimeStamp
    {
        public string point_name { get; set; }
        public string surveystarttime { get; set; }
        public string surveyendtime { get; set; }
        public string cgstarttime { get; set; }
        public string cgendtime { get; set; }
        public int intervalhour { get; set; }
        public List<string> surveylackdatetime { get; set; }
        public List<string> cglackdatetime { get; set; }

       


    }
}