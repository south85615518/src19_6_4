﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class StrainPlotlineAlarmModel
    {
        public string pointname { get; set; }
        public global::Strain.Model.strainalarmvalue firstalarm { get; set; }
        public global::Strain.Model.strainalarmvalue secalarm { get; set; }
        public global::Strain.Model.strainalarmvalue thirdalarm { get; set; }

    }
}