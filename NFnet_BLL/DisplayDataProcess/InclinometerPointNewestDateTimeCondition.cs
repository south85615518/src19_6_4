﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class InclinometerPointNewestDateTimeCondition
    {
        public int xmno { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public InclinometerPointNewestDateTimeCondition(int xmno, string pointname)
        {
            this.xmno = xmno;
            this.pointname = pointname;
        }
    }
}