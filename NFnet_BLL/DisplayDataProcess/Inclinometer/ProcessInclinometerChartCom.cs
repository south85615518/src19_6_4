﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.Settlement;

namespace NFnet_BLL.DisplayDataProcess.Inclinometer
{
    public class ProcessInclinometerChartCom
    {
        public ProcessInclinometerChartBLL inclinometerChartBLL = new ProcessInclinometerChartBLL();
        public ProcessInclinometerChartCgBLL inclinometerChartCgBLL = new ProcessInclinometerChartCgBLL();
        public bool ProcessSerializestrSBWY(ProcessSerializestrSBWYModel model,out string mssg)
        {
            if (model.role == Role.superviseModel || model.tmpRole)
            {
                return inclinometerChartCgBLL.ProcessSerializestrSBWY(model.model,out mssg);
            }
                return inclinometerChartBLL.ProcessSerializestrSBWY(model.model,out mssg);
        }
        public class ProcessSerializestrSBWYModel
        {
            public SerializestrSBWYCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole { get; set; }
            public ProcessSerializestrSBWYModel(SerializestrSBWYCondition model, Role role, bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }
    }
}