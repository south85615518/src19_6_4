﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.BLL;
using NFnet_BLL.DataProcess;
using System.Data;
using TotalStation.Model.fmos_obj;
using Tool;
using NFnet_BLL.XmInfo;
using NFnet_BLL.AuthorityAlarmProcess;
using InclimeterDAL.Model;

namespace NFnet_BLL.DisplayDataProcess.Inclinometer
{
    /// <summary>
    /// 点号预警业务逻辑处理类
    /// </summary>
    public class ProcessPointAlarmBLL
    {
        
        public PointAttribute.BLL.senor_pointalarmvalue bll = new PointAttribute.BLL.senor_pointalarmvalue();
        public static ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public static ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public static ProcessalarmsplitondateBLL splitOnDateBLL = new ProcessalarmsplitondateBLL();
        public static int sec = 0;
        /// <summary>
        /// 点号预警表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointAlarmRecordsCount(ProcessAlarmRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (bll.PointAlarmValueTableRowsCount(model.xmname, model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点号预警记录数获取类
        /// </summary>
        public class ProcessAlarmRecordsCountModel : SearchCondition
        {
            public string totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessAlarmRecordsCountModel(string xmname, int xmno, string searchString)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        /// <summary>
        /// 深部位移点号表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.PointTableLoad(model.searchString, model.pageIndex, model.rows, model.xmno, model.xmname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点号预警表获取类
        /// </summary>
        public class ProcessAlarmLoadModel : SearchCondition
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 搜索条件
            /// </summary>
            public string searchString { get; set; }
            /// <summary>
            /// 点号预警表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessAlarmLoadModel(string xmname, int xmno, string searchString, string colName, int pageIndex, int rows, string sord)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.searchString = searchString;
                this.xmno = xmno;

            }
        }
        /// <summary>
        /// 点号是否存在
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmExist(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return bll.Exist(model, out mssg);
        }
        /// <summary>
        /// 点号预警编辑
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmEdit(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return bll.Update(model, out mssg);
        }
        /// <summary>
        /// 点号预警添加
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmAdd(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return bll.Add(model, out mssg);
        }

        public bool  ProcessAlarm(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            mssg = "";
            if (!ProcessAlarmExist(model, out mssg))
            {
               return  ProcessAlarmAdd(model, out mssg);
            }
            else
            {
              return  ProcessAlarmEdit(model, out mssg);
            }
        }
        /// <summary>
        /// 点号预警批量更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmMultiUpdate(ProcessAlarmMultiUpdateModel model, out string mssg)
        {
            return bll.MultiUpdate(model.pointstr, model.model, out mssg);
        }
        /// <summary>
        /// 点号预警删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmDel(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return bll.Delete(model, out mssg);
        }
        public class ProcessAlarmDelModel
        {
            public string xmname { get; set; }
            public string name { get; set; }
            public ProcessAlarmDelModel(string xmname,string name)
            {
                this.xmname = xmname;
                this.name = name;
            }
        }
        /// <summary>
        /// 点号预警批量更新类
        /// </summary>
        public class ProcessAlarmMultiUpdateModel
        {
            /// <summary>
            /// 点组
            /// </summary>
            public string pointstr { get; set; }
            /// <summary>
            /// 点号预警类
            /// </summary>
            public PointAttribute.Model.fmos_pointalarmvalue model { get; set; }
            public ProcessAlarmMultiUpdateModel(string pointstr, PointAttribute.Model.fmos_pointalarmvalue model)
            {
                this.pointstr = pointstr;
                this.model = model;
            }
        }

        /// <summary>
        /// 结果数据对象预警处理
        /// </summary>
        /// <param name="alarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmIntoInformation(InclimeterDAL.Model.inclinometer_alarmvalue alarmvalue, senor_data resultModel, out List<string> lscontext)
        {
            lscontext = new List<string>();
            List<string>  ls = new List<string>();
            bool result = false;
            lscontext.Add(string.Format("{0},{1}", resultModel.point_name, resultModel.time.ToString()));
            ls.Add(DataProcessHelper.Compare("", "本次", resultModel.this_disp, alarmvalue.this_disp, out result, result));
            ls.Add(DataProcessHelper.Compare("", "累计", resultModel.ac_disp, alarmvalue.ac_disp, out result, result));
            ls.Add(DataProcessHelper.Compare("", "速率", resultModel.this_rap, alarmvalue.this_rap, out result, result));
            lscontext.Add(string.Join(" ",ls));
            return result;
        }
       
        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            List<string> ls = new List<string>();
            string mssg = "";
            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.point_name,
                time = model.resultModel.time,
                type = "深部位移",
                atime = DateTime.Now,
                pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processCgAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, "深部位移", model.resultModel.point_name, model.resultModel.time);
            splitOnDateBLL.ProcessalarmsplitondateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);

            AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = "深部位移",
                pointName = model.resultModel.point_name,
                xmno = model.xmno,
                time = model.resultModel.time,
                adate = DateTime.Now

            };
            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add("三级预警");
                    pointCheck.alarm = 3;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck,out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno,"深部位移","",model.resultModel.point_name,3,out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add("二级预警");
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "深部位移", "", model.resultModel.point_name, 2, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add("一级预警");
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                    cgalarm.alarmContext = string.Join(",", ls);
                    splitOnDateBLL.ProcessalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "深部位移", "", model.resultModel.point_name, 1, out mssg);
                    return true;
                }


            }
                ls.Add("========预警解除==========");
                pointCheck.alarm = 0;
                pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
                ProcessHotPotColorUpdate(model.xmno, "深部位移", "", model.resultModel.point_name, 0, out mssg);
                return false;

        }
        public class ProcessPointAlarmfilterInformationModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public List<InclimeterDAL.Model.inclinometer_alarmvalue> levelalarmvalue { get; set; }
            public senor_data resultModel { get; set; }
            public List<string> ls { get; set; }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<InclimeterDAL.Model.inclinometer_alarmvalue> levelalarmvalue, senor_data resultModel,int xmno)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
                this.xmno = xmno;
            }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<InclimeterDAL.Model.inclinometer_alarmvalue> levelalarmvalue, senor_data resultModel)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
            }
            public ProcessPointAlarmfilterInformationModel()
            {
               
            }
        }
        public class AlarmRecord
        {
            public string pointName { get; set; }
            public string Time { get; set; }
            public string alarm { get; set; }
            public string RecordTime { get; set; }

        }

        public bool ProcessPointAlarmModelGet(ProcessPointAlarmModelGetModel model, out string mssg)
        {
            PointAttribute.Model.fmos_pointalarmvalue pointalarm = new PointAttribute.Model.fmos_pointalarmvalue();
            if (bll.GetModel(model.xmno, model.pointName, out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }
        public class ProcessPointAlarmModelGetModel
        {
            public int xmno { get; set; }
            public string pointName { get; set; }
            public PointAttribute.Model.fmos_pointalarmvalue model { get; set; }
            public ProcessPointAlarmModelGetModel(int xmno, string pointName)
            {
                this.xmno = xmno;
                this.pointName = pointName;
            }
        }
        
        public bool ProcessHotPotColorUpdate(int xmno,string jclx,string jcoption,string pointName,int color,out string mssg)
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(xmno, jclx, jcoption, pointName, color);
            return layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);
            
        }

      

    }
}