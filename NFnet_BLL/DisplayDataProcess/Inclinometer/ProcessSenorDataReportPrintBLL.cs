﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;
using System.IO;
using Tool.Inclinometer;

namespace NFnet_BLL.DisplayDataProcess.Inclinometer
{

    /// <summary>
    /// 全站仪数据报表打印业务逻辑处理类
    /// </summary>
    public class ProcessSenorDataReportPrintBLL
    {
        public static SenorReportHelper reportHelper = new SenorReportHelper();
        public static ProcessSenorDataAlarmBLL senorDataAlarmBLL = new ProcessSenorDataAlarmBLL();
        
        /// <summary>
        /// 单点多周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessSenorDataSPMTReport(ProcessSenorDataSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                
                SenorReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                List<string> ls = Tool.DataTableHelper.ProcessDataTableFilter(model.dt, "point_name");
                List<Tool.Inclinometer.SenorReportHelper.inclinometeralarm> ltsi = new List<Tool.Inclinometer.SenorReportHelper.inclinometeralarm>();
                senorDataAlarmBLL.xmno = model.xmno;
                senorDataAlarmBLL.xmname = model.xmname;
                foreach (string pointname in ls)
                {
                    Tool.Inclinometer.SenorReportHelper.inclinometeralarm model_ = new SenorReportHelper.inclinometeralarm();//senorDataAlarmBLL.ProcessPointAlarmValue(pointname, model.xmno);
                    ltsi.Add(model_);
                }

                List<Tool.Inclinometer.SenorChartHelper.SenorChartCreateEnvironment> lce = new List<Tool.Inclinometer.SenorChartHelper.SenorChartCreateEnvironment>();
                Tool.Inclinometer.SenorReportHelper.SetTabValSPMT(model.senorReport, model.dt, model.xlspath, ltsi, out lce);
                string mappath = model.xlspath;
                Tool.Inclinometer.SenorReportHelper.WriteToFile(model.exportpath);
                Tool.Inclinometer.SenorChartHelper chartHelper = new Tool.Inclinometer.SenorChartHelper();
                chartHelper.ChartListReportHelper(model.exportpath, lce);


                mssg = "";
                return true;
            }
            catch (Exception ex)
            {
                Tool.ExceptionLog.ExceptionWrite("测斜报表输出出错,错误信息:"+ex.Message);
                string a = "";
                return false;
            }
        }
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public class ProcessSenorDataSPMCReportModel
        {
            public Tool.Inclinometer.SenorReport senorReport { get; set; }
            public string xlspath { get; set; }
            public DataTable dt { get; set; }
            public string exportpath { get; set; }
            public int xmno { get; set; }
            public string xmname { get; set; }
            public ProcessSenorDataSPMCReportModel(Tool.Inclinometer.SenorReport senorReport, int xmno,string xmname,string xlspath, DataTable dt, string exportpath)
            {
                this.senorReport = senorReport;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
                this.xmno = xmno;
                this.xmname = xmname;
            }
           
        }





        /// <summary>
        /// 全站仪数据表打印类
        /// </summary>
        public class ProcessSenorDataReportModel : PrintCondition
        {
            public List<Tool.ChartCreateEnviroment> cce { get; set; }
            public ProcessSenorDataReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname,string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }

        public class ProcessSenorDataMPMCReportModel : PrintCondition
        {
            public Tool.ChartCreateEnviroment cce { get; set; }
            public List<string> pnames { get; set; }
            public ProcessSenorDataMPMCReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }



    }
}