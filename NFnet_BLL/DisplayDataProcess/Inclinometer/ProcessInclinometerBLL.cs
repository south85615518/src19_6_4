﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;
using System.Data;
using NFnet_BLL.Other;

namespace NFnet_BLL.DisplayDataProcess.Inclinometer
{

    /// <summary>
    /// 测斜数据展示业务逻辑类
    /// </summary>
    public class ProcessInclinometerBLL
    {
        public InclimeterDAL.BLL.senor_data senorBLL = new InclimeterDAL.BLL.senor_data();
        public static sensorDAL.BLL.InclinometerBLL inclinometerBLL = new sensorDAL.BLL.InclinometerBLL();
        /// <summary>
        /// 获取测斜点号
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessInclinometerPointLoad(ProcessInclinometerPointLoadModel model, out string mssg)
        {
            List<string> ls = null;
            if (inclinometerBLL.InclinometerPointLoadBLL(model.xmno, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 测斜点获取参数实体
        /// </summary>
        public class ProcessInclinometerPointLoadModel
        {
           
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 点名列表
            /// </summary>
            public List<string> ls { get; set; }
            public ProcessInclinometerPointLoadModel(int xmno)
            {
                //this.xmname = xmname;
                this.xmno = xmno;
            }

        }
        public bool ProcessSenorDataInsert(InclimeterDAL.Model.senor_data model, out string mssg)
        {
            return senorBLL.Add(model, out mssg);

        }
        public bool ProcessInclinometerDataAlarmModelList(ProcessInclinometerDataAlarmModelListModel model, out string mssg)
        {
            List<InclimeterDAL.Model.senor_data> ls = new List<InclimeterDAL.Model.senor_data>();
            if (senorBLL.GetList(model.xmno, out ls, out mssg))
            {
                model.model = ls;
                return true;
            }
            return false;
        }

        public class ProcessInclinometerDataAlarmModelListModel
        {
            public int xmno { get; set; }
            public List<InclimeterDAL.Model.senor_data> model { set; get; }
            public ProcessInclinometerDataAlarmModelListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        public bool ProcessDeleteTmp(int xmno, out string mssg)
        {
            return senorBLL.DeleteSenorTmp(xmno, out mssg);
        }

        public bool ProcessPointNewestDateTimeGet(InclinometerPointNewestDateTimeCondition model, out string mssg)
        {

            DateTime dt = new DateTime();
            if (senorBLL.PointNewestDateTimeGet(model.xmno, model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;


        }

        public bool ProcessSenorDataTime(SenorDataTimeCondition model, out string mssg)
        {

            InclimeterDAL.Model.senor_data senorDataModel = new InclimeterDAL.Model.senor_data();
            if (senorBLL.GetModel(model.xmno, model.pointname, model.dt, out senorDataModel, out mssg))
            {
                model.model = senorDataModel;
                return true;

            }
            return false;
        }
        public bool ProcessSenorMaxTime(SenorMaxTimeCondition model, out string mssg)
        {
            DateTime dt = new DateTime();
            if (senorBLL.MaxTime(model.xmno, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;

        }


        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessInclinometerDbFill(FillInclinometerDbFillCondition model)
        {
            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "Time");
            model.rqConditionStr = model.rqConditionStr.Replace("#_cyc", "deep");
            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "POINT_NAME");
            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" POINT_NAME ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = "select region,point_name,deep,previous_disp,ac_disp,this_disp,this_rap,mtimes,previous_time,time  from  senor_data where  xmno = '" + model.xmno + "' and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
                sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            var processquerystanderdbModel = new QuerystanderdbIntModel(sql, model.xmno);
            if (ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
            {
                model.dt = processquerystanderdbModel.dt;
                return true;
            }
            else
            {
                return false;
            }


        }

        /// <summary>
        /// 结果数据报表数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataReportTableCreate(SenorDataReportTableCreateCondition model, out string mssg)
        {
            DataTable dt = null;
            if (senorBLL.ResultDataReportPrint(model.sql, model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessSenorDataLoad(SenorDataLoadCondition model, out string mssg)
        {
            mssg = "";
            DataTable dt = new DataTable();
            if (senorBLL.SenordataTableLoad(model.starttime, model.endtime, model.startPageIndex, model.endPageIndex, model.xmno, model.pointname, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }

        public bool ProcessSenorPointNameCycListLoad(SenorPointNameCycListCondition model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (senorBLL.PointNameCycListGet(model.xmno, model.pointname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSenorDataRecordsCount(SenorDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (senorBLL.SenorTableRowsCount(model.starttime, model.endtime, model.xmno, model.pointname, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
       



        /// <summary>
        /// 根据表的分页单位获取当前记录在表中的页数
        /// </summary>
        /// <param name="pointname">点名</param>
        /// <param name="date">时间</param>
        /// <param name="deep">深度</param>
        /// <param name="dt">数据表</param>
        /// <param name="pointnameStr">点名字段名</param>
        /// <param name="deepstr">深度字段名</param>
        /// <param name="dateStr">日期字段名</param>
        /// <param name="pageSize">页数</param>
        /// <returns></returns>
        public int PageIndexFromTabCYC(string pointname, string date, string deep, DataTable dt, string pointnameStr, string deepstr, string dateStr, int pageSize)
        {

            DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr] + "|" + deep + "=" + drv[deepstr];
                if (drv[pointnameStr].ToString() == pointname && date.Trim() == drv[dateStr].ToString() && drv[deepstr].ToString() == deep)
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }

    }
}