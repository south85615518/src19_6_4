﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.BLL;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_DAL.MODEL;

namespace NFnet_BLL.DisplayDataProcess.Inclinometer
{
    /// <summary>
    /// 全站仪预警业务逻辑处理类
    /// </summary>
    public class ProcessAlarmBLL
    {
        //public static AlarmBLL alarmBLL = new AlarmBLL();
        public static InclimeterDAL.BLL.inclinometer_alarmvalue alarmBLL = new InclimeterDAL.BLL.inclinometer_alarmvalue();
        /// <summary>
        /// 预警参数表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmRecordsCount(ProcessAlarmRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (alarmBLL.TableRowsCount( model.xmname, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 预警参数表记录数获取实体
        /// </summary>
        public class ProcessAlarmRecordsCountModel : SearchCondition
        {
            /// <summary>
            /// 记录数
            /// </summary>
            public string totalCont { get; set; }
            public ProcessAlarmRecordsCountModel(string xmname)
            {
                this.xmname = xmname;
               

            }
        }
        /// <summary>
        /// 预警参数表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (alarmBLL.TableLoad(model.pageIndex, model.rows, model.xmname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessAlarmLoadModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessAlarmLoadModel(string xmname, string colName, int pageIndex, int rows, string sord)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        /// <summary>
        /// 预警参数添加处理
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmAdd(InclimeterDAL.Model.inclinometer_alarmvalue model, out string mssg)
        {
            return alarmBLL.Add(model, out mssg);
        }


       
        /// <summary>
        /// 预警参数编辑
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmEdit(InclimeterDAL.Model.inclinometer_alarmvalue model, out string mssg)
        {
            return alarmBLL.Update(model, out mssg);
        }
        /// <summary>
        /// 预警参数删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmDel(string xmname,string name, out string mssg)
        {
            return alarmBLL.Delete(xmname,name, out mssg);
        }
        /// <summary>
        /// 预警参数删除实体类
        /// </summary>
        public class ProcessAlarmDelModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 预警参数类
            /// </summary>
            public string name { get; set; }
            public ProcessAlarmDelModel(string xmname, string name)
            {
                this.xmname = xmname;
                this.name = name;
            }
        }
        /// <summary>
        /// 预警名称获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmValueName(ProcessAlarmValueNameModel model,out string mssg)
        {
             string alarmValueNameStr ="";
             mssg = "";
             if (alarmBLL.AlarmValueNameGet(model.xmname, out alarmValueNameStr, out mssg))
             {
                 model.alarmValueNameStr = alarmValueNameStr;
                 return true;
             }
             else
             {
                 return false;
             }
        }
        /// <summary>
        ///预警名称获取类
        /// </summary>
        public class ProcessAlarmValueNameModel
        {
            public string xmname { get; set; }
            public string alarmValueNameStr { get; set; }
            public ProcessAlarmValueNameModel(string xmname, string alarmValueNameStr)
            {
                this.xmname = xmname;
                this.alarmValueNameStr = alarmValueNameStr;
            }
        }


        public bool ProcessAlarmModelGetByName(ProcessAlarmModelGetByNameModel model,out string mssg)
        {
            InclimeterDAL.Model.inclinometer_alarmvalue alarm = new InclimeterDAL.Model.inclinometer_alarmvalue();
            if (alarmBLL.GetModel(model.name, model.xmname, out alarm, out mssg))
            {
                model.model = alarm;
                return true;
            }
            return false;
        }
        public class ProcessAlarmModelGetByNameModel
        {
            public string xmname { get; set; }
            public  InclimeterDAL.Model.inclinometer_alarmvalue  model { get; set; }
            public string name { get; set; }
            public ProcessAlarmModelGetByNameModel(string xmname,string name)
            {
                this.xmname = xmname;
                this.name = name;
            }
        }


    }
}