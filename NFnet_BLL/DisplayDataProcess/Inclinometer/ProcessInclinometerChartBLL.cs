﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using NFnet_MODAL;

namespace NFnet_BLL.DisplayDataProcess.Settlement
{
    /// <summary>
    /// 测斜曲线显示处理业务逻辑
    /// </summary>
    public class ProcessInclinometerChartBLL
    {
        

        #region 深部位移
        /// <summary>
        /// 表面位移曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public  bool ProcessSerializestrSBWY(SerializestrSBWYCondition model, out string mssg)
        {
            mssg = "";
            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            string sql = model.sql;
            string pointname = model.pointname;
            zuxyz[] zus = model.zus;
            string xmname = model.xmname;
            model.sr = new List<seriesarrow>();
            if (pointname != "" && pointname != null && zus != null)
            {

                var processquerystanderdbModel = new QuerystanderdbModel(sql, xmname);
                if (!ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
                {
                    //错误信息反馈
                }
                dt = processquerystanderdbModel.dt;
                
                //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[pointnamezu.Length];
                
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
                for (int k = 0; k < pointnamezu.Length; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(pointnamezu[k], dvzu[k]);
                }
                List<serie_point> lst = new List<serie_point>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < pointnamezu.Length; d++)
                    {
                        string filter = "";
                        if(pointnamezu[d].IndexOf("'") == -1)
                        dvzu[d].RowFilter = "POINT_NAME= '" + pointnamezu[d]+"'";
                        else
                        dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d] ;
                        seriesarrow sr = new seriesarrow();//创建曲线
                        sr.ls = new List<serie_point>();
                        sr.arrowname = pointnamezu[d] + "_" + zus[z].Name;
                        int cont = dvzu[d].Count;
                        //if (cont != 0)
                        //{
                        //DataView dvdefault = new DataView(dt);
                        DataTable dtdistinct = dvzu[d].ToTable(true, "time");
                        //从表中筛选出周期
                        List<string> lstime = TimeListGet(dtdistinct);
                        DataView[] dvtm = new DataView[lstime.Count];
                        for (int k = 0; k < lstime.Count; k++)
                        {
                            dvtm[k] = new DataView(dvzu[d].ToTable());
                            
                        }
                        foreach (string time in lstime)
                        {
                            dvtm[lstime.IndexOf(time)].RowFilter = "time = '"+time+"'";
                            for (int u = 0; u < zus[z].Bls.Length; u++)
                            {


                                serie_point st = new serie_point();
                                st.Stype = zus[z].Name;//曲线类别
                                if (zus[z].Bls[u] == "previous_disp")
                                {
                                    st.Name = pointnamezu[d].Replace("'", "") + "_" + dvtm[lstime.IndexOf(time)][0]["previous_time"] + "_" + rplname(zus[z].Bls[u]);
                                    switchnez(dvtm[lstime.IndexOf(time)], st, zus[z].Bls[u]);
                                }
                                else
                                {
                                    st.Name = pointnamezu[d].Replace("'", "") + "_" + time + "_" + rplname(zus[z].Bls[u]);
                                    switchnez(dvtm[lstime.IndexOf(time)], st, zus[z].Bls[u]);
                                }
                                
                                sr.ls.Add(st);

                            }
                        }

                        model.sr.Add(sr);
                        //}

                    }



                }
                
                return true;
            }
            return false;
        }
        /// <summary>
        /// 周期列表初始化
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<string> TimeListGet(DataTable dt)
        {
            DataView dv = new DataView(dt);
            List<string> ls = new List<string>();
            foreach (DataRowView drv in dv)
            {
                ls.Add(drv["time"].ToString());
            }
            return ls;
        }
        /// <summary>
        /// 重命名分量标签
        /// </summary>
        /// <param name="blm"></param>
        /// <returns></returns>
        public static string rplname(string blm)
        {
            if (blm.IndexOf("this_disp") != -1)
            {
                return "△L";
            }

            else if (blm.IndexOf("ac_disp") != -1 || blm.IndexOf("previous_disp") != -1)
            {
                return "∑△L";
            }
            
            else if (blm.IndexOf("this_rap") != -1)
            {
                return "△V";
            }
            return blm;

        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dv">结果数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量名称</param>
        public static void switchnez(DataView dv, serie_point st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_point[len];
            if (len != 0)
            {

                if (xyzes == "this_disp")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = wgformat(drv, "this_disp");
                        i++;
                    }
                }
                else if (xyzes == "ac_disp")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = wgformat(drv, "ac_disp");
                        i++;
                    }
                }
                else if (xyzes == "this_rap")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = wgformat(drv, "this_rap");
                        i++;
                    }
                }
                else if (xyzes == "previous_disp")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = wgformat(drv, "previous_disp");
                        i++;
                    }
                }


            }

        }
        /// <summary>
        /// 数据行生成曲线数据点
        /// </summary>
        /// <param name="drv"></param>
        /// <param name="sxtj"></param>
        /// <returns></returns>
        public static pt_point wgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string dtime = drv["time"].ToString();
            DateTime d = Convert.ToDateTime(dtime);
            int year = d.Year;
            int mon = d.Month;
            int day = d.Day;
            int hour = d.Hour;
            int minute = d.Minute;
            int second = d.Second;
            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt_point { PointName=drv["deep"].ToString() , Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }





        #endregion

    }
    
}