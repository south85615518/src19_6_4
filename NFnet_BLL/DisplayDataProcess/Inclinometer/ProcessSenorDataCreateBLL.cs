﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using Tool;

namespace NFnet_BLL
{
   public  class ProcessSenorDataCreateBLL
    {
        public ProcessInclinometerBLL processInclinometerBLL = new ProcessInclinometerBLL();
        public static string mssg = "";
        public bool SenorDataCreate(int xmno,string point_name,string region,double init_disp,string[] maxdisp,double[]deeps,DateTime startTime,DateTime endTime,int timeinteval )
        {

            string[] points = point_name.Split(',');
            int i = 0;
            //int k = 0;
            Random rand = new Random();
            //double disptmp = init_disp;
            //double thisdisptmp = init_disp;
            int dx = 0;
            while (startTime.AddMinutes(i * timeinteval) < endTime)
            {
                
                
                
                foreach (string pointName in points)
                {
                    List<InclimeterDAL.Model.senor_data> tmp = new List<InclimeterDAL.Model.senor_data>();
                    List<InclimeterDAL.Model.senor_data> lim = new List<InclimeterDAL.Model.senor_data>();
                    foreach (double deep in deeps)
                    {
                        dx = rand.Next(maxdisp.Length);
                        var this_dis = Convert.ToDouble(Convert.ToDouble(maxdisp[dx]).ToString("0.00"));

                        double _ac_disp = tmp.Count == 0 ? 0 : Convert.ToDouble((from m in tmp where m.deep == deep select m.ac_disp).ToList()[0]);



                        InclimeterDAL.Model.senor_data model = new InclimeterDAL.Model.senor_data
                        {
                            point_name = pointName,
                            time = startTime.AddMinutes(i * timeinteval),
                            region = region,
                            xmno = xmno,
                            ac_disp = _ac_disp + this_dis,
                            this_disp = this_dis,
                            deep = deep,
                            this_rap = this_dis/((timeinteval/(60*24)))
                            
                        };
                        if (!processInclinometerBLL.ProcessSenorDataInsert(model, out mssg))
                        {
                            ExceptionLog.ExceptionWrite(mssg);
                        }
                        lim.Add(model);
                    }
                   tmp = lim;
                   lim = new List<InclimeterDAL.Model.senor_data>();
                }
                i++;
            }

            return false;
        }
     
    }
}
