﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Tool;
using System.IO;
using InclimeterDAL.Model;

namespace NFnet_BLL.DisplayDataProcess.Inclinometer
{
    public class ProcessSenorDataAlarmBLL
    {
        public  string xmname {get;set; }
        public int xmno { get; set; }
        //public string dateTime { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessSenorDataAlarmBLL() { 
        }
        public ProcessSenorDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = xmname;
            this.xmno = xmno;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public static string mssg = "";
       // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
        public static ProcessInclinometerBLL senorBLL = new ProcessInclinometerBLL();
        public bool main()
        {
            
           return TestSenorModelList();
            
        }
        public bool TestSenorModelList()
        {
            //if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processSenorDataAlarmModelListModel = new ProcessInclinometerBLL.ProcessInclinometerDataAlarmModelListModel(xmno);
            //object obj;
            if (senorBLL.ProcessInclinometerDataAlarmModelList(processSenorDataAlarmModelListModel, out mssg))
            {
                List<senor_data> lc = (List<senor_data>)processSenorDataAlarmModelListModel.model;
                //ProcessPrintMssg.Print(mssg);
                SenorPointAlarm(lc);
                return true;
            }
            return false;

            //ProcessPrintMssg.Print(mssg);

        }
        public PointAttribute.Model.fmos_pointalarmvalue TestPointAlarmValue(string pointName)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointName);
            PointAttribute.Model.fmos_pointalarmvalue model = null;
            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print(mssg);
                //TestAlarmValueList(processPointAlarmModelGetModel.model);
                return processPointAlarmModelGetModel.model;

            }
            return null;
        }

        public Tool.SenorReportHelper.inclinometeralarm ProcessPointAlarmValue(string pointname, int xmno)
        {

            PointAttribute.Model.fmos_pointalarmvalue pointvalue = TestPointAlarmValue(pointname);
            List<InclimeterDAL.Model.inclinometer_alarmvalue> alarmList = TestAlarmValueList(pointvalue);
            var model = new Tool.SenorReportHelper.inclinometeralarm();
            model.pointname = pointname;
            if (alarmList[2] != null)
            {
                model.third_acdisp = alarmList[2].ac_disp;
                model.third_thisdisp = alarmList[2].this_disp;
                model.third_rap = alarmList[2].this_rap;
            }
            if (alarmList[1] != null)
            {

                model.sec_acdisp = alarmList[2].ac_disp;
                model.sec_thisdisp = alarmList[2].this_disp;
                model.sec_rap = alarmList[2].this_rap;
            }
            if (alarmList[0] != null)
            {

                model.first_acdisp = alarmList[2].ac_disp;
                model.first_thisdisp = alarmList[2].this_disp;
                model.first_rap = alarmList[2].this_rap;


            }


            return model;
        }


        public List<InclimeterDAL.Model.inclinometer_alarmvalue> TestAlarmValueList(PointAttribute.Model.fmos_pointalarmvalue pointalarm)
        {
            List<InclimeterDAL.Model.inclinometer_alarmvalue> alarmvalueList = new List<InclimeterDAL.Model.inclinometer_alarmvalue>();
            //一级
            var processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmname, pointalarm.FirstAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("一级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmname, pointalarm.SecondAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("二级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmname, pointalarm.ThirdAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                //ProcessPrintMssg.Print("三级：" + mssg);
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<InclimeterDAL.Model.inclinometer_alarmvalue> levelalarmvalue, senor_data resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel,xmno);
            if (pointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));

                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public bool SenorPointAlarm(List<senor_data> lc)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format("===================================={0}=================================", DateTime.Now));
            ls.Add(string.Format("===================================={0}=================================", xmname));
            ls.Add(string.Format("===================================={0}=================================", "深部位移--测斜仪--超限自检"));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            ExceptionLog.ExceptionWriteCheck(ls);
            foreach (senor_data cl in lc)
            {
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname))) return false;
                PointAttribute.Model.fmos_pointalarmvalue pointvalue = TestPointAlarmValue(cl.point_name);
                List<InclimeterDAL.Model.inclinometer_alarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            return true;
            //Console.ReadLine();
        }
        public List<string> SenorDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            main();
            return this.alarmInfoList;
        }
        public void SenorPointAlarm(senor_data data)
        {

            PointAttribute.Model.fmos_pointalarmvalue pointvalue = TestPointAlarmValue(data.point_name);
            if (pointvalue == null) return;
            List<InclimeterDAL.Model.inclinometer_alarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestPointAlarmfilterInformation(alarmList, data);
        }




    }
}