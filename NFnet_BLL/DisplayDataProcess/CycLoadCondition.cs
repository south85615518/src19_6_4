﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    /// <summary>
    /// 周期获取类
    /// </summary>
    public class CycLoadCondition
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 点名
        /// </summary>
        public string pointname { get; set; }
        /// <summary>
        /// 周期数据表
        /// </summary>
        public DataTable dt { get; set; }
        public CycLoadCondition(string xmname,string pointname)
        {
            this.xmname = xmname;
            this.pointname = pointname;
            
        }
    }
}