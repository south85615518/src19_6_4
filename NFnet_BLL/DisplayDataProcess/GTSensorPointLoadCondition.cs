﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class GTSensorPointLoadCondition
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 学习点名列表
        /// </summary>
        public List<string> ls { get; set; }
        public data.Model.gtsensortype datatype{ get; set; }
        public GTSensorPointLoadCondition(int xmno, data.Model.gtsensortype datatype)
        {
            this.xmno = xmno;
            this.datatype = datatype;
        }
    }
}