﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SerializestrBMWYC_SINGLE_CYCondition
    {
        /// <summary>
        /// 端点周期
        /// </summary>
        public string xmname { get; set; }
        public string pointname { get; set; }
        public int startcyc { get; set; }
        public int endcyc { get; set; }
        public zuxyz[] zus { get; set; }
        /// <summary>
        /// 周期曲线
        /// </summary>
        public List<serie_cyc_null> serie_cycs { get; set; }
        public SerializestrBMWYC_SINGLE_CYCondition(object xmname, object pointname, object startcyc,object endcyc,object zus)
        {
         
            this.xmname = xmname == null ? "" : xmname.ToString();
            this.pointname = pointname == null ? "" : pointname.ToString();
            this.zus = zus == null ? null : (zuxyz[])zus;
            this.startcyc = startcyc == null ? -1 : Convert.ToInt32(startcyc);
            this.endcyc = endcyc == null ? -1 : Convert.ToInt32(endcyc);
        }
    }
}