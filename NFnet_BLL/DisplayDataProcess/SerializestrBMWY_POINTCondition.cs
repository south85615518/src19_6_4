﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SerializestrBMWY_POINTCondition : SerializestrBMWYCondition
    {
        public List<serie_point> serie_points { get; set; }
        public SerializestrBMWY_POINTCondition(object sql, object xmname, object pointname, object zus)
        {
            this.sql = sql == null ? "" : sql.ToString();
            this.xmname = xmname == null ? "" : xmname.ToString();
            this.pointname = pointname == null ? "" : pointname.ToString();
            this.zus = zus == null ? null : (zuxyz[])zus;
            this.rows = 1000;
            this.pageIndex = 1;
        }
        public SerializestrBMWY_POINTCondition(object sql, object xmname, object pointname, object zus,int rows,int pageIndex)
        {
            this.sql = sql == null ? "" : sql.ToString();
            this.xmname = xmname == null ? "" : xmname.ToString();
            this.pointname = pointname == null ? "" : pointname.ToString();
            this.zus = zus == null ? null : (zuxyz[])zus;
            this.rows = rows;
            this.pageIndex = pageIndex;
        }
    }
}