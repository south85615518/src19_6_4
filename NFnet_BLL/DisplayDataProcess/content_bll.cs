﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;
using System.Web.Security;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;
using System.Diagnostics;
using System.Web.Script.Serialization;
using NFnet_MODAL;
using NFnet_DAL;

namespace NFnet_BLL
{
    public class content_bll : System.Web.UI.Page
    {
        public static DateTime stdtmer = new DateTime();
        public static accessdbse adb = new accessdbse();
        public static int _i = 0;
        public static database db = new database();
        /// <summary>
        /// 日期查询条件生成
        /// </summary>
        /// <param name="sttmTextBox">起始日期文本控件</param>
        /// <param name="edtmTextBox">结束日期文本控件</param>
        /// <param name="type">查询类型</param>
        /// <param name="unit">日期单位</param>
        /// <returns></returns>
        public string RqcxConditionCreate(TextBox sttmTextBox,TextBox edtmTextBox,string type, string unit)
        {
            //this.rqcxType.Text = type;
            string xmname = Session["xmname"].ToString();
            if (type == "0")
            {
                string sttm = sttmTextBox.Text;//从
                string edtm = edtmTextBox.Text;
                
                string sqlsttm = querystring.querynvl("#_date", sttm, ">=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                string sqledtm = querystring.querynvl("#_date", edtm, "<=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                string rqConditionStr = sqlsttm + " and " + sqledtm  + " order by #_point,cyc,#_date asc ";
                CycTeriminalSet(sttm,edtm,xmname);
                return rqConditionStr;
                //日期查询

            }
            else if (type == "1")
            {
                string rqConditionStr = "#_cyc >= "+sttmTextBox.Text+"   and  #_cyc <= " + edtmTextBox.Text  +  " order by #_point,cyc,#_date asc ";
                Session["minCyc"] = sttmTextBox.Text;
                Session["maxCyc"] = edtmTextBox.Text;
                return rqConditionStr;
                
            }
            else if (type == "2")
            {
                //快捷查询
                string rqConditionStr = dateswdl(sttmTextBox,edtmTextBox,"#_date", unit,xmname);
                return rqConditionStr;
            }

            return null;
        }
        /// <summary>
        /// 日期查询监测数据
        /// </summary>
        /// <param name="xmname">项目名称</param>
        /// <param name="jcxmAndjcd">监测项目和监测点</param>
        /// <param name="gv_sw">水位的表格控件</param>
        /// <param name="gv_jy">雨量的表格控件</param>
        /// <param name="gv_bmwy">表面位移的表格控件</param>
        /// <param name="gv_sbwy">深部位移的表格控件</param>
        /// <param name="gv_cj">沉降的表格控件</param>
        /// <param name="gv_yl">应力的表格控件</param>
        /// <param name="sttmTextBox">起始日期文本框</param>
        /// <param name="edtmTextBox">结束日期文本框</param>
        public void Buttonk_Click_bll(string xmname,string jcxmAndjcd,GridView gv_sw,GridView gv_jy,GridView gv_bmwy,GridView gv_sbwy,GridView gv_cj,GridView gv_yl,TextBox sttmTextBox,TextBox edtmTextBox,TextBox startCyc,TextBox endCyc,string queryType)
        {
            xmname = Session["xmname"].ToString();
            OdbcConnection conn = db.GetStanderConn(xmname);
            string rqConditionStr = "";
            //生成日期查询条件
            if(queryType == "日期查询")
            rqConditionStr = RqcxConditionCreate(sttmTextBox,edtmTextBox,"0", "");
            else
            rqConditionStr = RqcxConditionCreate(startCyc, endCyc, "1", "");
            //统一查询选择的所有点
            //解析点号暂存表
            SessionClearAll();
            string jcxmAndJcdPauseTab = jcxmAndjcd;
            string[] jcdMap = jcxmAndJcdPauseTab.Split('|');
            DataTable dt = null;
            int i = 0;
            try
            {
                for (i = 0; i < jcdMap.Length; i++)
                {
                    if (jcdMap[i].IndexOf("监测") == -1 && jcdMap[i].IndexOf("Surface displacement") == -1)
                    {
                        switch (jcdMap[i - 1].Trim())
                        {
                            case "水位监测":
                                dt = filltdb(jcdMap[i], rqConditionStr, conn);
                                //this.g
                                gv_sw.DataSource = dt;
                                gv_sw.DataBind();
                                break;
                            case "雨量监测":
                                dt = fillraindb(jcdMap[i], rqConditionStr, conn);
                                gv_jy.DataSource = dt;
                                gv_jy.DataBind();
                                break;
                            case "表位监测":
                            case "Surface displacement":
                                dt = fillfmosdb(jcdMap[i], rqConditionStr, conn);
                                gv_bmwy.DataSource = dt;
                                gv_bmwy.DataBind();
                                CaptionDisplay(dt.Rows.Count, gv_bmwy);
                                break;
                            case "深位监测":
                                dt = fillcxdb(jcdMap[i], rqConditionStr, conn);
                                gv_sbwy.DataSource = dt;
                                gv_sbwy.DataBind();
                                break;
                            case "沉降监测":
                                dt = fillsettlementdb(jcdMap[i], rqConditionStr, conn);
                                gv_cj.DataSource = dt;
                                gv_cj.DataBind();
                                break;

                            case "应力监测":
                                dt = fillstraindb(jcdMap[i], rqConditionStr, conn);
                                gv_yl.DataSource = dt;
                                gv_yl.DataBind();
                                break;


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("监测项目生成查询条件出错:");
            }
        }
        /// <summary>
        /// 快捷查询
        /// </summary>
        /// <param name="btn">被点击的按钮</param>
        /// <param name="xmname">项目名称</param>
        /// <param name="jcxmAndjcd">监测项目和监测点</param>
        /// <param name="gv_sw">水位的表格控件</param>
        /// <param name="gv_jy">雨量的表格控件</param>
        /// <param name="gv_bmwy">表面位移的表格控件</param>
        /// <param name="gv_sbwy">深部位移的表格控件</param>
        /// <param name="gv_cj">沉降的表格控件</param>
        /// <param name="gv_yl">应力的表格控件</param>
        /// <param name="sttmTextBox">起始日期文本框</param>
        /// <param name="edtmTextBox">结束日期文本框</param>
        public void ButtonConvenient_Click_bll(object sender,string jcxmAndjcd, TextBox sttmTextBox, TextBox edtmTextBox, GridView gv_sw, GridView gv_jy, GridView gv_bmwy, GridView gv_sbwy, GridView gv_cj, GridView gv_yl)
        {
            try
            {
                string xmname = Session["xmname"].ToString();
                Button btn = sender as Button;
                string rqConditionStr = "";
                switch (btn.Text)
                {
                    case "1天":
                    case "One day":
                        rqConditionStr = RqcxConditionCreate(sttmTextBox, edtmTextBox, "2", "day");
                        break;
                    case "1周":
                    case "One week":
                        rqConditionStr = RqcxConditionCreate(sttmTextBox, edtmTextBox, "2", "week");
                        break;
                    case "1月":
                    case "One month":
                        rqConditionStr = RqcxConditionCreate(sttmTextBox, edtmTextBox, "2", "mon");
                        break;
                    case "3月":
                    case "Three month":
                        rqConditionStr = RqcxConditionCreate(sttmTextBox, edtmTextBox, "2", "threemon");
                        break;
                }
                OdbcConnection conn = db.GetStanderConn(xmname);
                SessionClearAll();
                Buttonk_Click_bll(xmname, jcxmAndjcd, gv_sw, gv_jy, gv_bmwy, gv_sbwy, gv_cj, gv_yl, sttmTextBox, edtmTextBox, null, null, "日期查询");
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("监测项目生成快捷查询条件出错:");
            }
        }
        #region
        /// <summary>
        /// 填充水位表
        /// </summary>
        public DataTable filltdb(string pointname, string rqConditionStr, OdbcConnection conn)
        {
            rqConditionStr = rqConditionStr.Replace("#_date", "sj");
            rqConditionStr = rqConditionStr.Replace("#_point", "A.sjbh");
            pointname = "'" + pointname.Replace(",", "','") + "'";//getmkhs(xmname);
            string sql2 = querystring.querynvl("A.sjbh ", pointname, "in", "(", ")");
            string sql = @"select id,mkh,sjbh,swz,wdz,sj from data A where  " + sql2 + "  ";
            sql += " and " + rqConditionStr;
            Session["htusql"] = sql;
            Session["pageSwsql"] = sql;
            Session["points"] = pointname;
            DataTable dt = new DataTable();
            dt = querysql.querystanderdb(sql, conn);
            return dt;

        }
        /// <summary>
        /// 处理快捷查询的值，生成sql语句
        /// </summary>
        /// <returns></returns>
        public string dateswdl(TextBox sttmTextBox,TextBox edtmTextBox ,string dateword/*表中的时间字段*/, string unit,string xmname)
        {

            switch (unit)
            {
                case "week":
                    {
                        sttmTextBox.Text = DateTime.Now.AddDays(-7).ToString().Replace("/", "-");
                        edtmTextBox.Text = DateTime.Now.ToString().Replace("/", "-");
                        return " date_sub(SYSDATE(),INTERVAL 1 week) <=" + dateword + " and sysdate()>= " + dateword + "   order by #_point,cyc,#_date asc";

                    }
                case "day":
                    {
                        sttmTextBox.Text = DateTime.Now.AddDays(-1).ToString().Replace("/", "-");
                        edtmTextBox.Text = DateTime.Now.ToString().Replace("/", "-");
                        return "  date_sub(SYSDATE(),INTERVAL 1 day) <=" + dateword + " and sysdate()>= " + dateword + "  order by #_point,cyc,#_date asc";
                    }
                case "mon":
                    {
                        sttmTextBox.Text = DateTime.Now.AddMonths(-1).ToString().Replace("/", "-");
                        edtmTextBox.Text = DateTime.Now.ToString().Replace("/", "-");
                        return "  DATE_SUB(SYSDATE(),INTERVAL 1 month) <=" + dateword + " and sysdate()>= " + dateword + " order by #_point,cyc,#_date asc";
                    }
                case "threemon":
                    {
                        sttmTextBox.Text = DateTime.Now.AddMonths(-3).ToString().Replace("/", "-");
                        edtmTextBox.Text = DateTime.Now.ToString().Replace("/", "-");
                        return " DATE_SUB(SYSDATE(),INTERVAL 3 month) <=" + dateword + " and sysdate()>= " + dateword + " order by #_point,cyc,#_date asc";
                    }
                case "year":
                    {
                        sttmTextBox.Text = DateTime.Now.AddYears(-1).ToString().Replace("/", "-");
                        edtmTextBox.Text = DateTime.Now.ToString().Replace("/", "-");
                        return "YEAR(SYSDATE()) - yEAR(" + dateword + ")<=1 and sysdate() >= " + dateword + " order by #_point,cyc,#_date asc ";
                    }
            }
            CycTeriminalSet(sttmTextBox.Text, edtmTextBox.Text, xmname);
            return "";
        }
        public string changedata(string datetype, string txt, string dateword/*表中表示时间的字段*/)
        {
            //是否含有逗号
            if (txt.IndexOf(",") != -1)//是点
            {
                if (txt.IndexOf("~") != -1)//如果是多个范围并成的点
                {
                    string[] rangs = txt.Split(',');
                    string sql = "";
                    for (int i = 0; i < rangs.Length; i++)
                    {
                        rangs[i].Replace("~", " " + "<=" + "" + datetype + "(" + dateword + ")  and  " + " " + datetype + "(" + dateword + ")<=");
                        if (i == 0)
                        {
                            sql += rangs[i];
                        }
                        else
                        {
                            sql += "  and  " + rangs[i];
                        }

                    }
                    return sql;

                }
                //是点但不是范围
                return "  " + datetype + "(" + dateword + ") in (" + txt + ") ";



            }//不是点是范围
            else if (txt.IndexOf("~") != -1)
            {
                return txt.Replace("~", " " + "<=" + "" + datetype + "(" + dateword + ")  and  " + " " + datetype + "(" + dateword + ")<=");

            }
            return datetype + "(" + dateword + ") =" + txt;//是单点
        }
        public string addstring(string intsrt, string intdes)
        {
            int a = int.Parse(intsrt);
            int b = int.Parse(intdes);
            return (a + b).ToString();
        }
        #endregion
        #region
        public DataTable fillraindb(string pointname, string rqConditionStr, OdbcConnection conn)
        {
            rqConditionStr = rqConditionStr.Replace("#_date", "monitorTime");
            rqConditionStr = rqConditionStr.Replace("#_point", "ylresultdata.rname");
            pointname = "'" + pointname.Replace(",", "','") + "'";
            string sql = @"select rname,rainfall,initrainfall,monitortime from ylresultdata where ylresultdata.rname in (" + pointname + ") and  " + rqConditionStr;
            DataTable dt = querysql.querystanderdb(sql, conn);
            Session["rainsql"] = sql;
            Session["pageRainsql"] = sql;
            Session["raines"] = pointname;
            return dt;

        }

        //侧斜数据
        public DataTable fillcxdb(string pointname, string rqConditionStr, OdbcConnection conn)
        {
            rqConditionStr = rqConditionStr.Replace("#_date", "monitorTime");
            rqConditionStr = rqConditionStr.Replace("#_point", "cxresultdata.name");
            pointname = "'" + pointname.Replace(",", "','") + "'";
            string sql = @"select name,s,monitorTime from cxresultdata where cxresultdata.name in (" + pointname + ") and " + rqConditionStr;
            Session["cxsql"] = sql;
            Session["pageCxsql"] = sql;
            Session["cxes"] = pointname;
            DataTable dt = querysql.querystanderdb(sql, conn);
            return dt;
        }
        //沉降数据
        public DataTable fillsettlementdb(string pointname, string rqConditionStr, OdbcConnection conn)
        {
            rqConditionStr = rqConditionStr.Replace("#_date", "monitorTime");
            rqConditionStr = rqConditionStr.Replace("#_point", "settlementresultdata.pointname");
            pointname = "'" + pointname.Replace(",", "','") + "'";
            string sql = @"select pointname,initelevation,elevation,this_val,ac_val,MonitorTime from settlementresultdata where settlementresultdata.pointname in (" + pointname + ") and " + rqConditionStr;
            Session["settlementsql"] = sql;
            Session["pagesettlementsql"] = sql;
            Session["settlementes"] = pointname;
            DataTable dt = querysql.querystanderdb(sql, conn);
            return dt;
        }
        //应力数据
        public DataTable fillstraindb(string pointname, string rqConditionStr, OdbcConnection conn)
        {
            rqConditionStr = rqConditionStr.Replace("#_date", "monitorTime");
            rqConditionStr = rqConditionStr.Replace("#_point", "strainresultdata.pointname");
            pointname = "'" + pointname.Replace(",", "','") + "'";
            string sql = @"select pointname,initstrengthVal,strengthVal,this_val,ac_val,MonitorTime from strainresultdata where strainresultdata.pointname in (" + pointname + ") and " + rqConditionStr;
            Session["strainsql"] = sql;
            Session["pagestrainsql"] = sql;
            Session["straines"] = pointname;
            DataTable dt = querysql.querystanderdb(sql, conn);
            return dt;
        }
        public string dlval(DropDownList dl)
        {
            if (dl.SelectedValue == "0")
            {
                return "";
            }
            return dl.SelectedValue;
        }
        /// <summary>
        /// 获取gps组号
        /// </summary>
        /// <returns></returns>
        public DataTable createtable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("GPSBaseName");
            dt.Columns.Add("This_X");
            dt.Columns.Add("This_Y");
            dt.Columns.Add("This_Z");
            dt.Columns.Add("Ac_X");
            dt.Columns.Add("Ac_Y");
            dt.Columns.Add("Ac_Z");
            dt.Columns.Add("This_Py");
            dt.Columns.Add("Ac_Py");
            dt.Columns.Add("jd");
            dt.Columns.Add("DateTime");
            return dt;
        }
        /// 时间查询数据处理
        /// </summary>
        /// <param name="datetype"></param>
        /// <param name="txt"></param>
        /// <returns></returns>
        public string changegpsdata(string datetype, string txt, string dateword)
        {
            //是否含有逗号
            if (txt.IndexOf(",") != -1)//是点
            {
                if (txt.IndexOf("~") != -1)//如果是多个范围并成的点
                {
                    string[] rangs = txt.Split(',');
                    string sql = "";
                    for (int i = 0; i < rangs.Length; i++)
                    {
                        rangs[i].Replace("~", " " + "<=" + "" + "DatePart(" + datetype + "," + dateword + ")  and  " + " DatePart(" + datetype + "," + dateword + ")<=");
                        if (i == 0)
                        {
                            sql += rangs[i];
                        }
                        else
                        {
                            sql += "  and  " + rangs[i];
                        }

                    }
                    return sql;

                }
                //是点但不是范围
                return "  DatePart(" + datetype + "," + dateword + ") in (" + txt + ") ";



            }//不是点是范围
            else if (txt.IndexOf("~") != -1)
            {
                return txt.Replace("~", " " + "<=" + "DatePart(" + datetype + "," + dateword + ")  and  " + "DatePart(" + datetype + "," + dateword + ")<=");

            }
            return "DatePart(" + datetype + "," + dateword + ") =" + txt;//是单点
        }
        #endregion
        #region
        /// <summary>
        /// 填充全站仪数据//
        /// </summary>
        public DataTable fillfmosdb(string pointname, string rqConditionStr, OdbcConnection conn)
        {
            string tabname = "fmos_cycdirnet";
            if (Session["userID"].ToString() == "jmsb")
                tabname = "fmos_cycdirnet_gain";
            rqConditionStr = rqConditionStr.Replace("#_date", "Time");
            rqConditionStr = rqConditionStr.Replace("#_point", "POINT_NAME");
            rqConditionStr = rqConditionStr.Replace("#_cyc", "cyc");
            //if (conn.State != ConnectionState.Open)
            //    conn.Open();
            //获取点名
            //rqConditionStr = rqConditionStr.Replace("#_date", "sj");
            pointname = "'" + pointname.Replace(",", "','") + "'";//getmkhs(xmname);
            string sql2 = querystring.querynvl(" POINT_NAME ", pointname, "in", "(", ")");
            string sql = "select  point_name,cyc,n,e,z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time  from  "+tabname+" where  taskName = '"+Session["xmname"]+"' and  " + sql2 + "  ";//表名由项目任务决定
            sql += " and " + rqConditionStr;
            Session["fmossql"] = sql;
            Session["pageBMWYsql"] = sql;
            Session["pointname"] = pointname;
            Session["zus"] = getfmoszu();
            DataTable dt = new DataTable();
            dt = querysql.querystanderdb(sql, conn);
            return dt;

        }
        /// <summary>
        /// 获取fmos组号
        /// </summary>
        /// <returns></returns>
        public zuxyz[] getfmoszu()
        {

            string[] XzBlAry = { "This_dN", "This_dE", "This_dZ", "Ac_dN", "Ac_dE", "Ac_dZ" };
            List<zuxyz> zusls = new List<zuxyz>();
            List<string> bc = new List<string>();
            List<string> lj = new List<string>();
            List<string> pc = new List<string>();

            for (int i = 0; i < XzBlAry.Length; i++)
            {
                if ((XzBlAry[i] == "This_dN" || XzBlAry[i] == "This_dE" || XzBlAry[i] == "This_dZ"))
                {
                    bc.Add(XzBlAry[i]);
                }

                if ((XzBlAry[i] == "Ac_dN" || XzBlAry[i] == "Ac_dE" || XzBlAry[i] == "Ac_dZ"))
                {
                    lj.Add(XzBlAry[i]);
                }
                if ((XzBlAry[i] == "Avg_N" || XzBlAry[i] == "Avg_E" || XzBlAry[i] == "Avg_Z"))
                {
                    pc.Add(XzBlAry[i]);
                }
            }
            if (bc.Count != 0)
            {
                zuxyz zu = new zuxyz { Bls = bc.ToArray<string>(), Name = "本次变化量" };
                zusls.Add(zu);
            }
            if (lj.Count != 0)
            {
                zuxyz zu = new zuxyz { Bls = lj.ToArray<string>(), Name = "累计变化" };
                zusls.Add(zu);
            }
            if (pc.Count != 0)
            {
                zuxyz zu = new zuxyz { Bls = pc.ToArray<string>(), Name = "平差" };
                zusls.Add(zu);
            }
            return zusls.ToArray<zuxyz>();


        }
        //public string 
        #endregion
        /// <summary>
        /// 为表格条件鼠标读数事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void GridView1_RowDataBound_bll(object sender, GridViewRowEventArgs e)
        {
            //int _i = 0;
            GridView gridView = sender as GridView;
            
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("id", _i.ToString());
                e.Row.Attributes.Add("onKeyDown", "SelectRow();");
                e.Row.Attributes.Add("onMouseOver", "MarkRow(" + _i.ToString() + ");");
                e.Row.Attributes.Add("onMouseLeave", "Markdel(" + _i.ToString() + ");");
                if (_i % 2 == 0)
                    e.Row.Style.Add("background-color", "#86e791");
                _i++;
            }
            
            //if (gridView.Rows.Count != 0)
            //{
            //    Control table = gridView.Controls[0];
            //    int count = table.Controls.Count;
            //    table.Controls[count - 1].Visible = true;
            //} 
            //gridView.Rows.Count
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
        /// <summary>
        /// 接收选择的项目id 此处要求所写的项目名称不能重复
        /// </summary>
        //-------------
        /// <summary>
        /// 判断字段是否有效
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public bool isempty(string txt)
        {
            if (txt == "" || txt == null)
            {
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// 获取快捷查询的值
        /// </summary>
        /// <returns></returns>
        public void GridView_RowCommand_bll(object sender, GridViewCommandEventArgs e)
        {
            GridView gv = sender as GridView;
            TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
            tb.Text = (Int32.Parse(tb.Text) - 1).ToString();
            if (e.CommandName == "Page") 
            {
                //首页
                if (e.CommandArgument.ToString() == "First")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    tb.Text = "1";
                    gv.PageIndex = 0;

                }
                else if (e.CommandArgument.ToString() == "Next")
                {
                    
                    
                    gv.PageIndex = (Int32.Parse(tb.Text) + 1) > gv.PageCount ? gv.PageCount : Int32.Parse(tb.Text) + 1;
                    tb.Text = gv.PageIndex.ToString();

                }
                else if (e.CommandArgument.ToString() == "-2")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = Int32.Parse(tb.Text) > gv.PageCount ? gv.PageCount : Int32.Parse(tb.Text) < 0 ? 0 : Int32.Parse(tb.Text);
                    tb.Text = gv.PageIndex.ToString();


                }
                else if (e.CommandArgument.ToString() == "Last")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = gv.PageCount;
                    tb.Text = gv.PageCount.ToString();

                }
                else if (e.CommandArgument.ToString() == "Prev")
                {
                    //TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = (Int32.Parse(tb.Text) - 1) < 0 ? 0 : Int32.Parse(tb.Text) - 1;
                    tb.Text = gv.PageIndex.ToString();

                }
                //获取GV的ID
                string id = gv.ClientID;
                string sql = "";
                OdbcConnection conn = db.GetStanderConn(Session["xmname"].ToString());
                DataTable dt = new DataTable();
                //gv.Style.Add("display", "block");
                //ShowGridView(gv);
                switch (id)
                {
                    case "GridView1": //filltdb();
                        sql = Session["pageSwsql"].ToString();
                        dt = querysql.querystanderdb(sql, conn);
                        gv.DataSource = dt;
                        gv.DataBind();
                        break;
                    case "GridView2": //cross();
                        sql = Session["pageRainsql"].ToString();
                        dt = querysql.querystanderdb(sql, conn);
                        gv.DataSource = dt;
                        gv.DataBind();
                        break;
                    case "GridView3": //cxcross();
                        sql = Session["pageCxsql"].ToString();
                        dt = querysql.querystanderdb(sql, conn);
                        gv.DataSource = dt;
                        gv.DataBind();
                        break;
                    case "GridView4":// fillfmosdb();
                        sql = Session["pageBMWYsql"].ToString();
                        dt = querysql.querystanderdb(sql, conn);
                        gv.DataSource = dt;
                        gv.DataBind();
                        break;
                    case "GridView5":// raincross();
                        sql = Session["pageRainsql"].ToString();
                        dt = querysql.querystanderdb(sql, conn);
                        gv.DataSource = dt;
                        gv.DataBind();
                        break;
                    case "GridView9":// raincross();
                        sql = Session["pagesettlementsql"].ToString();
                        dt = querysql.querystanderdb(sql, conn);
                        gv.DataSource = dt;
                        gv.DataBind();
                        break;
                    case "GridView7":// raincross();
                        sql = Session["pagestrainsql"].ToString();
                        dt = querysql.querystanderdb(sql, conn);
                        gv.DataSource = dt;
                        gv.DataBind();
                        break;

                }

            }
        }
        //填写测试用的session
        public void FillSession()
        {
            Session["jcxmname"] = Session["xmname"]; //"白云边坡监测";
        }
        //在执行一次新的查询之前清除掉所有的session
        public void SessionClearAll()
        {
            string sessionStr = "htusql,points,fmossql,pointname,zus,rainsql,raines,cxsql,cxes,settlementsql,settlementes,strainsql,straines";
            string[] sessionNames = sessionStr.Split(',');
            foreach (string sessionName in sessionNames)
            {
                if (Session[sessionName] != null && Session[sessionName] != "")
                {
                    Session.Remove(sessionName);
                }
            }

        }
        /// <summary>
        /// 翻页查找
        /// </summary>
        /// <param name="sender">事件的发送者</param>
        /// <param name="e">事件参数</param>
        /// <param name="pointName">点名</param>
        /// <param name="timeShot">时间点</param>
        /// <param name="targetGV">目标GridView</param>
        /// <param name="pagesize">每页的行数</param>
        public void pageIndex(object sender, EventArgs e,string pointName,string timeShot,GridView gv,int pagesize)
        {
            string pointname = pointName;
            string time = timeShot;
            //time = time.Substring(0, time.IndexOf("GMT"));
            pointname = pointname.Replace("'", "");
            if (pointname.IndexOf("_") != -1)
            {
                pointname = pointname.Substring(0, pointname.IndexOf("_"));
            }

            string id = gv.ClientID;
            string sql = "";
            OdbcConnection conn = db.GetStanderConn(Session["xmname"].ToString());
            DataTable dt = new DataTable();
            switch (id)
            {
                case "GridView1":
                    sql = Session["pageSwsql"].ToString();
                    dt = querysql.querystanderdb(sql, conn);
                    gv.PageIndex = PageIndexFromTab(pointname, time, dt, "sjbh", "sj",pagesize);
                    gv.DataSource = dt;
                    gv.DataBind();
                    break;
                case "GridView2":
                    sql = Session["pageRainsql"].ToString();
                    dt = querysql.querystanderdb(sql, conn);
                    gv.PageIndex = PageIndexFromTab(pointname, time, dt, "rname", "monitortime",pagesize);
                    gv.DataSource = dt;
                    gv.DataBind();
                    break;
                case "GridView3":
                    sql = Session["pageCxsql"].ToString();
                    dt = querysql.querystanderdb(sql, conn);
                    gv.PageIndex = PageIndexFromTab(pointname, time, dt, "name", "monitortime", pagesize);
                    gv.DataSource = dt;
                    gv.DataBind();
                    break;
                case "GridView4":
                    sql = Session["pageBMWYsql"].ToString();
                    dt = querysql.querystanderdb(sql, conn);
                    gv.PageIndex = PageIndexFromTabCYC(pointname, time, dt, "point_name", "cyc", pagesize);
                    gv.DataSource = dt;
                    gv.DataBind();
                    CaptionDisplay(dt.Rows.Count,gv);
                    break;
                case "GridView5":
                    sql = Session["pageRainsql"].ToString();
                    dt = querysql.querystanderdb(sql, conn);
                    gv.PageIndex = PageIndexFromTab(pointname, time, dt, "rname", "monitortime", pagesize);
                    gv.DataSource = dt;
                    gv.DataBind();
                    break;
                case "GridView9":
                    sql = Session["pagesettlementsql"].ToString();
                    dt = querysql.querystanderdb(sql, conn);
                    gv.PageIndex = PageIndexFromTab(pointname, time, dt, "pointname", "monitortime", pagesize);
                    gv.DataSource = dt;
                    gv.DataBind();
                    break;
                case "GridView7":
                    sql = Session["pagestrainsql"].ToString();
                    dt = querysql.querystanderdb(sql, conn);
                    gv.PageIndex = PageIndexFromTab(pointname, time, dt, "pointname", "monitortime", pagesize);
                    gv.DataSource = dt;
                    gv.DataBind();
                    break;

            }


            //获取GV的ID

        }
        //根据表的分页单位获取当前记录在表中的页数
        public int PageIndexFromTab(string pointname, string date, DataTable dt, string pointnameStr, string dateStr, int pageSize)
        {
            try
            {
                DataView dv = new DataView(dt);
                int i = 0;
                DateTime dat = new DateTime();
                string datUTC = "";
                foreach (DataRowView drv in dv)
                {
                    dat = Convert.ToDateTime(drv[dateStr]);
                    datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                    datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                    if (drv[pointnameStr].ToString() == pointname && datUTC == date)
                    {
                        return i / pageSize;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("翻页查找出错：");
            }
            return -1;
        }
        //根据表的分页单位获取当前记录在表中的页数
        public int PageIndexFromTabCYC(string pointname, string date, DataTable dt, string pointnameStr, string dateStr, int pageSize)
        {
            try
            {
                DataView dv = new DataView(dt);
                int i = 0;
                //DateTime dat = new DateTime();
                string datUTC = "";
                foreach (DataRowView drv in dv)
                {
                    
                    //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                    //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                    string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr];
                    if (drv[pointnameStr].ToString() == pointname && date.Trim() == drv[dateStr].ToString())
                    {
                        return i / pageSize;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("翻页查找出错：");
            }
            return 0;
        }
        //根据项目名称获取最小周期
        public void ExtremelyCycGet(string xmname, string aggregateFunction,TextBox tb)
        {
            try
            {
                string sql = "select " + aggregateFunction + "(cyc) from fmos_cycdirnet where taskName='"+xmname+"'";
                OdbcConnection conn = db.GetStanderConn(xmname);
                string ExtremelyCyc = querysql.querystanderstr(sql, conn);
                tb.Text = ExtremelyCyc;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("获取端点周期出错");
            }
        }
        //根据日期范围获取周期范围
        public void CycTeriminalSet( string sttm, string edtm,string xmname)
        {
            string sqlmin = "select min(cyc) as mincyc from fmos_cycdirnet where taskName = '"+xmname+"'";
            string sqlmax = "select max(cyc) as maxcyc from fmos_cycdirnet where taskName = '" + xmname + "'";
            OdbcConnection conn = db.GetStanderConn(xmname);
            string cycmin = querysql.querystanderstr(sqlmin,conn);
            string cycmax = querysql.querystanderstr(sqlmax,conn);
            Session["minCyc"] = cycmin;
            Session["maxCyc"] = cycmax;

        }
        //隐藏显示GridView表头
        public void CaptionDisplay(int rowcont,GridView gv)
        {
            if (rowcont < 5)
            gv.Caption = "<input type='button' onclick='Close();' class='close_btn' id='close' value='关闭表格' />";
            else
            gv.Caption = "";
       

        }
    }
}