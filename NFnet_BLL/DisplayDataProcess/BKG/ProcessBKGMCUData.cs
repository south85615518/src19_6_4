﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.Other;
using Tool;
using System.Data.OleDb;

namespace NFnet_BLL.DisplayDataProcess.BKG
{
    public class ProcessBKGMCUData
    {
        public static string mssg = "";
        public static MDBDATA.BLL.mcudata mcubll = new MDBDATA.BLL.mcudata();
        /// <summary>
        /// 数据展示日期查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {
            if (model.type == QueryType.QT)
            {
                var processdatemdbModel = new ProcessComBLL.ProcessdatemdbModel(model.unit, model.maxTime);
                ProcessComBLL.Processdatemdb(processdatemdbModel, out mssg);
                model.startTime = processdatemdbModel.sttm;
                model.endTime = processdatemdbModel.edtm;
            }

            ExceptionLog.ExceptionWrite("起始日期:"+model.startTime+"结束日期:"+model.endTime);
            DateTime dtstart = Convert.ToDateTime(model.startTime);

            DateTime dtend = Convert.ToDateTime(model.endTime);
            
            model.sql = "select sname,r1*0.09875 as r_1, r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + model.xmname + "' and st1='应力' and dt>=format('" + Convert.ToDateTime(model.startTime).AddMinutes(-1) + "','yyyy/mm/dd HH:mm:ss') and dt <= format('" + Convert.ToDateTime(model.endTime).AddMinutes(1) + "','yyyy/mm/dd HH:mm:ss')  and acc=1   ";
            ExceptionLog.ExceptionWrite(model.sql);
            ExceptionLog.ExceptionWrite("浸润线查询语句:" + model.sql);
            

        }
        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessInclinometerDbFill(FillBKGDbFillCondition model)
        {

            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" sname ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = model.rqConditionStr + " and  " + Processquerynvlmodel.str + " order by sname,st1,dt asc ";//表名由项目任务决定
                //sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            
            //dt = new DataTable();
            //dt.Columns.Add("sname", Type.GetType("System.String"));
            //dt.Columns.Add("r_1", Type.GetType("System.Double"));
            //dt.Columns.Add("r2", Type.GetType("System.Double"));
            //dt.Columns.Add("dt", Type.GetType("System.DateTime"));
            
            var queryReadBKGModel = new QueryReadBKGModel { sql = sql, lsname = "sname,r_1,r2,dt".Split(',').ToList() };
            if (ProcessComBLL.Processquerystanderdb(queryReadBKGModel, out mssg))
            {

                model.dt = queryReadBKGModel.dt;
                return true;
            }
            else
            {
                return false;
            }


        }
        public bool ProcessMCUAlarmModelListGet(ProcessMCUAlarmModelListGetModel model, out string mssg)
        {
            List<MDBDATA.Model.mcudata> mcumodel = null;
            if (mcubll.GetList(model.xmname, model.pointnamelist, out mcumodel, out mssg))
            {
                model.model = mcumodel;
                return true;
            }
            return false;

        }
        public class ProcessMCUAlarmModelListGetModel
        {
            public string xmname { get; set; }
            public List<MDBDATA.Model.mcudata> model { get; set; }
            public List<string> pointnamelist { get; set; }
            public ProcessMCUAlarmModelListGetModel(string xmname, List<string> pointnamelist)
            {
                this.xmname = xmname;
                this.pointnamelist = pointnamelist;
            }
        }
        public bool GetAlarmTableCont(GetAlarmTableContModel model, out string mssg)
        {
            int cont = 0;
            if (mcubll.GetAlarmTableCont(model.xmname, model.pointnamelist, out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            return false;
        }
        public class GetAlarmTableContModel
        {
            public List<string> pointnamelist { get; set; }
            public int cont { get; set; }
            public string xmname { get; set; }
            public GetAlarmTableContModel(string xmname, List<string> pointnamelist)
            {
                this.pointnamelist = pointnamelist;
                this.xmname = xmname;
            }
        }
        public bool ProcessMCUDataDelete(DataDeleteCondition model, out string mssg)
        {
            return mcubll.Delete(model.xmname, model.point_name, model.dt, out mssg);
        }
        public bool ProcessMCUDataAcc(ProcessMCUDataAccModel model, out string mssg)
        {
            return mcubll.MCUDataAcc(model.pid, model.time, model.R2, out mssg);
        }
        public class ProcessMCUDataAccModel
        {
            public int pid { get; set; }
            public DateTime time { get; set; }
            public double R2 { get; set; }
            public ProcessMCUDataAccModel(int pid, DateTime time, double R2)
            {
                this.pid = pid;
                this.time = time;
                this.R2 = R2;
            }
        }


    }
    
}