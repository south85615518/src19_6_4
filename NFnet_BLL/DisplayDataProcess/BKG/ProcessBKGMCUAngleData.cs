﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.Other;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.BKG
{
    public class ProcessBKGMCUAngleData
    {
        public static MDBDATA.BLL.mcuangledata mcuanglebll = new MDBDATA.BLL.mcuangledata();
        public string mssg = "";
        /// <summary>
        /// 数据展示日期查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {
            if (model.type == QueryType.QT)
            {
                var processdatemdbModel = new ProcessComBLL.ProcessdatemdbModel(model.unit, model.maxTime);
                ProcessComBLL.Processdatemdb(processdatemdbModel, out mssg);
                model.startTime = processdatemdbModel.sttm;
                model.endTime = processdatemdbModel.edtm;

            }


            DateTime dtstart = Convert.ToDateTime(model.startTime);

            DateTime dtend = Convert.ToDateTime(model.endTime);
            model.sql = "select sname,r1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + model.xmname + "' and st1='位移/角度' and dt>=format('" + Convert.ToDateTime(model.startTime).AddMinutes(-1) + "','yyyy/mm/dd HH:mm:ss') and dt <= format('" + Convert.ToDateTime(model.endTime).AddMinutes(1) + "','yyyy/mm/dd HH:mm:ss')  and  acc=1   ";


            
        }
        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessInclinometerDbFill(FillBKGDbFillCondition model)
        {

            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" sname ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = model.rqConditionStr + " and  " + Processquerynvlmodel.str + " order by sname,st1,dt asc ";//表名由项目任务决定
                //sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            var queryBKGModel = new QueryBKGModel(sql);
            if (ProcessComBLL.Processquerystanderdb(queryBKGModel, out mssg))
            {

                model.dt = queryBKGModel.dt;
                return true;
            }
            else
            {
                return false;
            }


        }
        public bool ProcessMCUAngleAlarmModelListGet(ProcessMCUAngleAlarmModelListGetModel model, out string mssg)
        {
            List<MDBDATA.Model.mcuangledata> mcumodel = null;
            if (mcuanglebll.GetList(model.xmname, model.pointnamelist, out mcumodel, out mssg))
            {
                model.model = mcumodel;
                return true;
            }
            return false;

        }
        public class ProcessMCUAngleAlarmModelListGetModel
        {
            public string xmname { get; set; }
            public List<MDBDATA.Model.mcuangledata> model { get; set; }
            public List<string> pointnamelist { get; set; }
            public ProcessMCUAngleAlarmModelListGetModel(string xmname, List<string> pointnamelist)
            {
                this.xmname = xmname;
                this.pointnamelist = pointnamelist;
            }
        }
        public bool GetAlarmTableCont(GetAlarmTableContModel model, out string mssg)
        {
            int cont = 0;
            if (mcuanglebll.GetAlarmTableCont(model.xmname, model.idlist, out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            return false;
        }
        public class GetAlarmTableContModel
        {
            public List<string> idlist { get; set; }
            public int cont { get; set; }
            public string xmname { get; set; }
            public GetAlarmTableContModel(string xmname, List<string> idlist)
            {
                this.idlist = idlist;
                this.xmname = xmname;
            }
        }
        public bool ProcessMCUAngleDataDelete(DataDeleteCondition model,out string mssg)
        {
            return mcuanglebll.Delete(model.xmname,model.point_name,model.dt,out mssg);
        }

        public bool ProcessMCUAngleDataAcc(ProcessMCUAngleDataAccModel model,out string mssg)
        {
            return mcuanglebll.MCUAngleDataAcc(model.pid,model.time,model.R2,out mssg);
        }
        public class ProcessMCUAngleDataAccModel
        {
            public int pid {get;set;}
            public DateTime time { get; set; }
            public double R2 { get; set; }
            public ProcessMCUAngleDataAccModel(int pid, DateTime time, double R2)
            {
                this.pid = pid;
                this.time = time;
                this.R2 = R2;
            }
        }
        
    }
}