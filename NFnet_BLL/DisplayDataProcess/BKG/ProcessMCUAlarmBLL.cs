﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.BLL;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_DAL.MODEL;

namespace NFnet_BLL.DisplayDataProcess.MCUBKG
{
    /// <summary>
    /// 全站仪预警业务逻辑处理类
    /// </summary>
    public class ProcessAlarmBLL
    {
        //public static AlarmBLL alarmBLL = new AlarmBLL();
        public static MDBDATA.BLL.mcualarmvalue alarmBLL = new MDBDATA.BLL.mcualarmvalue();
        /// <summary>
        /// 预警参数表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmRecordsCount(ProcessAlarmRecordsCountModel model, out string mssg)
        {
            int totalCont = 0;
            if (alarmBLL.TableRowsCount( model.seachstring,model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 预警参数表记录数获取实体
        /// </summary>
        public class ProcessAlarmRecordsCountModel : SearchCondition
        {
            /// <summary>
            /// 记录数
            /// </summary>
            public int totalCont { get; set; }
            public string seachstring { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessAlarmRecordsCountModel(int xmno)
            {
                this.xmno = xmno;
               

            }
        }
        /// <summary>
        /// 预警参数表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (alarmBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessAlarmLoadModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessAlarmLoadModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        /// <summary>
        /// 预警参数添加处理
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmAdd(MDBDATA.Model.mcualarmvalue model, out string mssg)
        {
            return alarmBLL.Add(model, out mssg);
        }


       
        /// <summary>
        /// 预警参数编辑
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmEdit(MDBDATA.Model.mcualarmvalue model, out string mssg)
        {
            return alarmBLL.Update(model, out mssg);
        }
        /// <summary>
        /// 预警参数删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmDel(int xmno,string name, out string mssg)
        {
            return alarmBLL.Delete(xmno,name, out mssg);
        }
        /// <summary>
        /// 预警参数删除实体类
        /// </summary>
        public class ProcessAlarmDelModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 预警参数类
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessAlarmDelModel(int xmno, string name)
            {
                this.xmno = xmno;
                this.name = name;
            }
        }
        /// <summary>
        /// 预警名称获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmValueName(ProcessAlarmValueNameModel model,out string mssg)
        {
             string alarmValueNameStr ="";
             mssg = "";
             if (alarmBLL.AlarmValueNameGet(model.xmno, out alarmValueNameStr, out mssg))
             {
                 model.alarmValueNameStr = alarmValueNameStr;
                 return true;
             }
             else
             {
                 return false;
             }
        }
        /// <summary>
        ///预警名称获取类
        /// </summary>
        public class ProcessAlarmValueNameModel
        {
            public int xmno { get; set; }
            public string alarmValueNameStr { get; set; }
            public ProcessAlarmValueNameModel(int xmno)
            {
                this.xmno = xmno;
                
            }
        }


        public bool ProcessAlarmModelGetByName(ProcessAlarmModelGetByNameModel model,out string mssg)
        {
            MDBDATA.Model.mcualarmvalue alarm = new MDBDATA.Model.mcualarmvalue();
            if (alarmBLL.GetModel(model.name, model.xmno, out alarm, out mssg))
            {
                model.model = alarm;
                return true;
            }
            return false;
        }
        public class ProcessAlarmModelGetByNameModel
        {
            public int xmno { get; set; }
            public  MDBDATA.Model.mcualarmvalue  model { get; set; }
            public string name { get; set; }
            public ProcessAlarmModelGetByNameModel(int xmno,string name)
            {
                this.xmno = xmno;
                this.name = name;
            }
        }


    }
}