﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.Data;
using NFnet_BLL.DataProcess;
using System.IO;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.BKG
{
    /// <summary>
    /// 全站仪数据报表打印业务逻辑处理类
    /// </summary>
    public class ProcessMCUReportPrintBLL
    {
        public static ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public static ProcessMCUReportPrintBLL MCUReportPrintBLL = new ProcessMCUReportPrintBLL();
        /// <summary>
        /// 单点多周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationSPMCReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                McuReportHelper mcuReportHelper = new McuReportHelper();
                mcuReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成");
                //var processTotalStationReportModel = new ProcessMCUReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,高度,温度", "{0}", model.dt, "point_name", model.exportpath);
                var processTotalStationReportModel = new ProcessMCUReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,高度,温度", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");
                //mcuReportHelper.Main(
                //    model.xmname,
                //    "时间,高度,温度",
                //    model.dt,
                //    model.exportpath
                //    );
                mcuReportHelper.Main(
                    model.xmname,
                    "时间,高度,温度",
                    model.dt,
                    model.exportpath
                    );
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("MCU报表输出出错，错误信息:" + ex.Message);
                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }

        public class ProcessTotalStationSPMCReportModel
        {
            public string stCyc { get; set; }
            public string etCyc { get; set; }
            public string xmname { get; set; }
            public string xlspath { get; set; }
            public DataTable dt { get; set; }
            public string exportpath { get; set; }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
            }

        }





        /// <summary>
        /// 全站仪数据表打印类
        /// </summary>
        public class ProcessTotalStationReportModel : PrintCondition
        {
            public List<ChartCreateEnviroment> cce { get; set; }
            public ProcessTotalStationReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }

        public class ProcessTotalStationMPMCReportModel : PrintCondition
        {
            public ChartCreateEnviroment cce { get; set; }
            public List<string> pnames { get; set; }
            public ProcessTotalStationMPMCReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }



    }
}