﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MDBDATA.BLL;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.BKG
{
    public class ProcessMCUAngleBLL
    {
        public static BKGSenor bkgsenor = new BKGSenor();
        public static MDBDATA.BLL.mcuangledata mcubll = new mcuangledata();
        public static MDBDATA.BLL.mcuangletineinterval mcuangletineintervalBLL = new mcuangletineinterval();
        public bool ProcessBKGMCUAnglePointLoad(BKGMcuPointLoadCondition model,out string mssg)
        {
            List<string> pointnamelist = new List<string>();
            if (bkgsenor.BKGInclinometerSenorPointLoad(model.xmname, out pointnamelist, out mssg))
            {
                model.pointnamelist = pointnamelist;
                return true;
            }
            return false;
        }
        public bool ProcessBKGMCUAngleMaxTime(BKGMaxTimeCondition model,out string mssg)
        {
            DateTime dt = new DateTime();
            if (bkgsenor.BKGMcuAngleMaxTime(model.xmname, out dt, out mssg))
            {
                model.maxTime = dt;
                return true;
            }
            return false;

        }
        public bool ProcessAngleMcuDataTime(McuAngleDataTimeCondition model, out string mssg)
        {

             MDBDATA.Model.mcuangledata mcuDataModel = new  MDBDATA.Model.mcuangledata();
            if (mcubll.GetModel(model.xmname, model.pointname, model.dt, out mcuDataModel, out mssg))
            {
                model.model = mcuDataModel;
                return true;

            }
            return false;
        }
        public class McuAngleDataTimeCondition
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public DateTime dt { get; set; }
            public  MDBDATA.Model.mcuangledata model { get; set; }
            public McuAngleDataTimeCondition(string xmname, string pointname, DateTime dt)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.dt = dt;
            }
        }
        public bool ProcessPointNewestDateTimeGet(MdbdataPointNewestDateTimeCondition model, out string mssg)
        {

            DateTime dt = new DateTime();
            if (mcubll.PointNewestDateTimeGet(model.xmname, model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;


        }
        /// <summary>
        /// 时间间隔获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMCUAngleTimeInterval(mcuangletimeintervalModel model, out string mssg)
        {
            DataTable dt = null;
            if (mcuangletineintervalBLL.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 时间间隔表获取
        /// </summary>
        public class mcuangletimeintervalModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public mcuangletimeintervalModel(int xmno,int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessMcuAngleTimeIntevalAdd(MDBDATA.Model.bkgtimeinterval model, out string mssg)
        {
            return mcuangletineintervalBLL.Add(model, out mssg);
        }


    }
}