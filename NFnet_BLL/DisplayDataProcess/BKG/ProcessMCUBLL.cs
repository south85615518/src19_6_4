﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MDBDATA.BLL;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.BKG
{
    public class ProcessMCUBLL
    {
        public static BKGSenor bkgsenor = new BKGSenor();
        public static MDBDATA.BLL.mcudata mcubll = new mcudata();
        public static MDBDATA.BLL.mcutineinterval mcuintervalBLL = new mcutineinterval();
        public bool ProcessBKGMCUPointLoad(BKGMcuPointLoadCondition model, out string mssg)
        {
            List<string> pointnamelist = new List<string>();
            if (bkgsenor.BKGMcuSenorPointLoad(model.xmname, out pointnamelist, out mssg))
            {
                model.pointnamelist = pointnamelist;
                return true;
            }
            return false;
        }
        public bool ProcessBKGMCUMaxTime(BKGMaxTimeCondition model, out string mssg)
        {
            DateTime dt = new DateTime();
            if (bkgsenor.BKGMcuMaxTime(model.xmname, out dt, out mssg))
            {
                model.maxTime = dt;
                return true;
            }
            return false;

        }
        public bool ProcessMcuDataTime(McuDataTimeCondition model, out string mssg)
        {

            MDBDATA.Model.mcudata mcuDataModel = new MDBDATA.Model.mcudata();
            if (mcubll.GetModel(model.xmname, model.pointname, model.dt, out mcuDataModel, out mssg))
            {
                model.model = mcuDataModel;
                return true;

            }
            return false;
        }
        public class McuDataTimeCondition
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public DateTime dt { get; set; }
            public MDBDATA.Model.mcudata model { get; set; }
            public McuDataTimeCondition(string xmname, string pointname, DateTime dt)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.dt = dt;
            }
        }
        public bool ProcessPointNewestDateTimeGet(MdbdataPointNewestDateTimeCondition model, out string mssg)
        {

            DateTime dt = new DateTime();
            if (mcubll.PointNewestDateTimeGet(model.xmname, model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;


        }
        /// <summary>
        /// 时间间隔获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMCUTimeInterval(mcutimeintervalModel model, out string mssg)
        {
            DataTable dt = null;
            if (mcuintervalBLL.TableLoad(model.pageIndex, model.rows, model.xmno,model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 时间间隔表获取
        /// </summary>
        public class mcutimeintervalModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public mcutimeintervalModel(int xmno,  int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

        public bool ProcessMCUTimeIntevalAdd(MDBDATA.Model.bkgtimeinterval model,out string mssg)
        {
            return mcuintervalBLL.Add(model,out mssg);
        }



    }
}