﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    public class ResultDataReportTableCreateCondition
    {
        public string sql { get; set; }
        public string xmname { get; set; }
        public int xmno { get; set; }
        public DataTable dt { get; set; }
        public ResultDataReportTableCreateCondition(string sql,string xmname)
        {
            this.sql = sql;
            this.xmname = xmname;
        }
        public ResultDataReportTableCreateCondition(string sql, int xmno)
        {
            this.sql = sql;
            this.xmno = xmno;
        }
    }
}