﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class GTResultDataCountLoadCondition
    {
       
        public string sord { get; set; }
        public string xmname { get; set; }
        public int xmno { get; set; }
        public string pointname { get; set; }
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public int startcyc { get; set; }
        public int endcyc { get; set; }
        public string totalCont { get; set; }
        public data.Model.gtsensortype datatype { set; get; }
        public GTResultDataCountLoadCondition(string xmname, string pointname, data.Model.gtsensortype datatype, DateTime startTime, DateTime endTime)
        {
            this.xmname = xmname;
            this.pointname = pointname;
            this.startTime = startTime;
            this.endTime = endTime;
            this.datatype = datatype;
        }
        public GTResultDataCountLoadCondition(string xmname, string pointname,  DateTime startTime, DateTime endTime)
        {
            this.xmname = xmname;
            this.pointname = pointname;
            this.startTime = startTime;
            this.endTime = endTime;
        }
        public GTResultDataCountLoadCondition(int xmno, string pointname, DateTime startTime, DateTime endTime)
        {
            this.xmno = xmno;
            this.pointname = pointname;
            this.startTime = startTime;
            this.endTime = endTime;
        }
        public GTResultDataCountLoadCondition(int xmno, string pointname, int startcyc, int endcyc)
        {
            this.xmno = xmno;
            this.pointname = pointname;
            this.startcyc = startcyc;
            this.endcyc = endcyc;
            
        }
        public GTResultDataCountLoadCondition(string xmname, string pointname, data.Model.gtsensortype datatype, int startcyc, int endcyc)
        {
            this.xmname = xmname;
            this.pointname = pointname;
            this.startcyc = startcyc;
            this.endcyc = endcyc;
            this.datatype = datatype;
        }
    }
    
}