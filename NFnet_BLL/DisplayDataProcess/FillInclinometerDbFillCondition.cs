﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess
{
    public class FillInclinometerDbFillCondition
    {
            public string pointname { get; set; }
            public string rqConditionStr { get; set; }
            public int xmno { get; set; }
            public string sql { get; set; }
            public DataTable dt { get; set; }
            public FillInclinometerDbFillCondition(string pointname, string rqConditionStr, int xmno)
            {
                this.pointname = pointname;
                this.rqConditionStr = rqConditionStr;
                this.xmno = xmno;
            }
    }
}