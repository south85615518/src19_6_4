﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SettlementPlotlineAlarmModel
    {
        public string pointname { get; set; }
        public global::Settlement.Model.settlementalarmvalue firstalarm { get; set; }
        public global::Settlement.Model.settlementalarmvalue secalarm { get; set; }
        public global::Settlement.Model.settlementalarmvalue thirdalarm { get; set; }

    }
}