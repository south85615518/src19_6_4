﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;
using NFnet_MODAL;
using NFnet_DAL;
namespace NFnet_BLL
{
    public class ChartCreate
    {
        //SerializestrBMWY_point
        #region 水位
        /// <summary>
        /// 根据数据表生成曲线
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="mkhs"></param>
        /// <param name="sql"></param>
        public static List<serie> SerializestrSW(HttpContext context)
        {
            database db = new database();
            string sgno = context.Request.QueryString["num"];
            if (sgno == "1")
            {
                context.Response.Write("hello");
            }
            else
            {
                string xmname = "";
                if (context.Session["jcxmname"] != null)
                {
                    xmname = context.Session["jcxmname"].ToString();
                }
                if (context.Session["xmname"] != null)
                {
                    xmname = context.Session["xmname"].ToString();
                }
                OdbcConnection conn = db.GetStanderConn(xmname);
                DataTable dt = new DataTable();
                DataTable dtt = new DataTable();
                string mkhs = "";
                string sql = "";
                if (context.Session["htusql"] != "" && context.Session["htusql"] != null)
                {
                    sql = context.Session["htusql"].ToString();
                    context.Session.Remove("htusql");
                }
                if (context.Session["points"] != "" && context.Session["points"] != null)
                {
                    mkhs = context.Session["points"].ToString();
                    context.Session.Remove("points");
                }

                //分模块号显示
                if (sql == "") return null;
                //JavaScriptSerializer jss = new JavaScriptSerializer();
                OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
                int i = oda.Fill(dt);
                string[] mkharr = mkhs.Split(',');

                if (mkhs != "" && mkhs != null)
                {
                    List<serie> lst = CreateSeriesFromData(dt, mkhs, conn);
                    //string sult = jss.Serialize(lst);
                    //context.Response.Write(sult);
                    return lst;
                }//如果mkh为空默认只显示过程
                /*else
                {
                    //DataView dvt = new DataView(dtt);
                    //将日期和数值提取返回日期序列化对象
                    List<pt> dli = formatdat(dv);
                       
                    string result = jss.Serialize(dli);
                    //result += jss.Serialize(dsz1);
                    context.Response.Write(result);
                }*/
            }
            return null;
        }
        public static List<serie> CreateSeriesFromData(DataTable dt, string mkhs, OdbcConnection conn)
        {
            string[] mkharr = mkhs.Split(',');

            if (mkhs != "" && mkhs != null)
            {
                List<serie> lst = new List<serie>();

                omkhs[] omkharr = new omkhs[mkharr.Length];
                for (int t = 0; t < mkharr.Length; t++)
                {
                    serie sr = new serie();
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "sjbh=" + mkharr[t];
                    int nu = dv.Count;
                    string rri = mkharr[t];

                    sr.Stype = "温度";
                    sr.Name = rri.Replace("'", "");  //InstandMkhByPotName(mkharr[t], conn);
                    //加载点
                    formatdat(dv, sr, "wdz");
                    lst.Add(sr);
                    sr = new serie();
                    sr.Stype = "水位";
                    sr.Name = rri.Replace("'", ""); //InstandMkhByPotName(mkharr[t], conn);
                    //加载点
                    formatdat(dv, sr, "swz");
                    lst.Add(sr);
                }


                return lst;
            }
            return null;
        }
        //生成表面位移曲线
        //将模块号替换成水位的点号
        public string InstandMkhByPotName(string mkh, OdbcConnection conn)
        {

            string sql = "select sjbh from data where mkh =" + mkh + "";
            string potName = querysql.querystanderstr(sql, conn);
            return potName;
        }
        public static void formatdat(DataView dv, serie sr, string valstr)
        {

            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["sj"].ToString();
                string v = drv[valstr].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
        public string GetAllMkh(OdbcConnection conn)
        {
            string sql = "select distinct(pointname) from swpoint";
            List<string> mkhs = querysql.querystanderlist(sql, conn);
            return "'" + string.Join("','", mkhs) + "'";
        }
        //public 
        #endregion
        #region 表面位移
        public static List<serie> SerializestrBMWY(HttpContext context)
        {

            //  conn = db.getsqldbconn();
            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            database db = new database();
            string sql = "";
            string pointname = "";
            zuxyz[] zus = { };
            //JavaScriptSerializer jss = new JavaScriptSerializer();
            //用sql语句测试连接是否成功
            if (context.Session["fmossql"] != null && context.Session["fmossql"] != "")
            {
                sql = context.Session["fmossql"].ToString();
                context.Session.Remove("fmossql");
            }
            if (context.Session["pointname"] != null && context.Session["pointname"] != "")
            {
                pointname = context.Session["pointname"].ToString();
                context.Session.Remove("pointname");
            }
            if (context.Session["zus"] != null && context.Session["zus"] != "")
            {
                zus = (zuxyz[])context.Session["zus"];
                //context.Session.Remove("zus");
                //string rlt = jss.Serialize(zus);
            }
            if (pointname != "" && pointname != null && zus != null)
            {
                //dt = cross(sql);
                //string xmname = context.Session["xmanme"].ToString();
                string xmname = context.Session["jcxmname"].ToString();
                OdbcConnection conn = db.GetStanderConn(xmname);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                dt = getfmostb(sql, conn);
                //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[pointnamezu.Length];
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
                for (int k = 0; k < pointnamezu.Length; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(pointnamezu[k], dvzu[k]);
                }
                List<serie> lst = new List<serie>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < pointnamezu.Length; d++)
                    {
                        string filter =
                        dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d];
                        int cont = dvzu[d].Count;
                        //if (cont != 0)
                        //{

                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie st = new serie();//创建曲线
                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = pointnamezu[d].Replace("'", "") + "_" + rplname(zus[z].Bls[u]);
                            switchnez(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                        //}

                    }



                }
                return lst;
            }
            return null;
        }
        /// <summary>
        /// 根据数据表生成曲线
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="mkhs"></param>
        /// <param name="sql"></param>
        public static List<serie> CreateSeriesFromData(DataTable dt, string pointname, zuxyz[] zus, OdbcConnection conn)
        {
            string[] pointnamezu = pointname.Split(',');
            DataView[] dvzu = new DataView[pointnamezu.Length];
            Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
            for (int k = 0; k < pointnamezu.Length; k++)
            {
                dvzu[k] = new DataView(dt);
                ddzu.Add(pointnamezu[k], dvzu[k]);
            }
            List<serie> lst = new List<serie>();
            for (int z = 0; z < zus.Length; z++)
            {

                for (int d = 0; d < pointnamezu.Length; d++)
                {
                    string filter =
                    dvzu[d].RowFilter = "POINT_NAME= '" + pointnamezu[d] + "'";
                    int cont = dvzu[d].Count;
                    if (cont != 0)
                    {

                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie st = new serie();//创建曲线

                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = pointnamezu[d] + "_" + rplname(zus[z].Bls[u]);
                            switchnez(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                    }

                }



            }
            return lst;
        }
        /// <summary>
        /// 获取所有属于监测分项的监测点
        /// </summary>
        /// <param name="jcOption"></param>
        /// <returns></returns>
        public string GetAllPointName(string jcOption, OdbcConnection conn)
        {
            string sql = "select distinct(pointname) from studypoint where jcOption ='" + jcOption + "'";
            List<string> pointname = querysql.querystanderlist(sql, conn);
            return string.Join(",", pointname);
        }

        /// <summary>
        /// 用sql语句查询获得gps数据表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable getfmostb(string sql, OdbcConnection conn)
        {
            DataTable dt = new DataTable();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;
        }
        /// <summary>
        /// 替换不同类别曲线名
        /// </summary>
        public static string rplname(string blm)
        {
            if (blm.IndexOf("This_dE") != -1)
            {
                return "△E";
            }
            else if (blm.IndexOf("This_dN") != -1)
            {
                return "△N";
            }
            else if (blm.IndexOf("This_dZ") != -1)
            {
                return "△Z";
            }
            else if (blm.IndexOf("Ac_dE") != -1)
            {
                return "∑△E";
            }
            else if (blm.IndexOf("Ac_dN") != -1)
            {
                return "∑△N";
            }
            else if (blm.IndexOf("Ac_dZ") != -1)
            {
                return "∑△Z";
            }
            return blm;

        }
        public void switchxyz(DataView dv, serie st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            if (len != 0)
            {

                if (xyzes == "WGS84_X")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "WGS84_X");
                        i++;
                    }
                }
                else if (xyzes == "WGS84_Y")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "WGS84_Y");
                        i++;
                    }
                }
                else if (xyzes == "WGS84_Z")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "WGS84_Z");
                        i++;
                    }
                }
                else if (xyzes == "s_X")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "s_X");
                        i++;
                    }
                }
                else if (xyzes == "s_Y")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "s_Y");
                        i++;
                    }
                }
                else if (xyzes == "s_Z")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "s_Z");
                        i++;
                    }
                }
                else if (xyzes == "l_X")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_X");
                        i++;
                    }
                }
                else if (xyzes == "l_Y")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_Y");
                        i++;
                    }
                }
                else if (xyzes == "l_Z")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_Z");
                        i++;
                    }
                }
                else if (xyzes == "bcpy")
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "bcpy");
                        i++;
                    }
                }
                else if (xyzes == "ljpy")
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "ljpy");
                        i++;
                    }
                }

            }

        }
        /// <summary>
        /// 区分出NEZ
        /// </summary>
        /// <param name="dv"></param>
        /// <param name="st"></param>
        /// <param name="xyzes"></param>
        public static void switchnez(DataView dv, serie st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_N");
                        i++;
                    }
                }
                else if (xyzes == "Avg_E")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_E");
                        i++;
                    }
                }
                else if (xyzes == "Avg_Z")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_Z");
                        i++;
                    }
                }


            }

        }
        public static pt wgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string dtime = drv["time"].ToString();
            DateTime d = Convert.ToDateTime(dtime);
            int year = d.Year;
            int mon = d.Month;
            int day = d.Day;
            int hour = d.Hour;
            int minute = d.Minute;
            int second = d.Second;
            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt { Dt = d, Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        //周期格式化时间
        public static pt_cyc cycwgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = drv["cyc"].ToString();
            
            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt_cyc { Pt_x = pt_x , Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        //周期格式化时间允许为空
        public static pt_cyc_null cycwgformat_null(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = drv["cyc"].ToString();
            return new pt_cyc_null { Pt_x = pt_x, Yvalue1 = drv[sxtj].ToString() };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }

        ////周期格式化时间
        //public static pt_cyc cycwgformat(DataRowView drv, string sxtj/*筛选条件*/,string lackCyc)
        //{
        //    /*此处可验证list的值是否改变
        //     * flag绘制的曲线类型的标志，分为本值变化，累计变化量
        //     */
        //    //dateserlize[] dls = new dateserlize[3];
            
        //    string pt_x = drv["cyc"].ToString();
        //    lackCyc.Add(pt_x);
        //    lackCyc.Sort();
        //    if (lackCyc.IndexOf(pt_x) == 0)
        //    {
        //        float val = (float)Convert.ToDouble(drv[sxtj].ToString());
        //        return new pt_cyc { Pt_x = pt_x, Yvalue1 = (float)(val) };
        //    }
        //    else
        //    {
        //        int i = 0;
        //            //float val = (float)Convert.ToDouble(drv[sxtj].ToString());
        //            return new pt_cyc { Pt_x = lackCyc[0], Yvalue1 = 0 };
                
        //    }
        //    //若是要在客户端对数据进行国际化还需要对日期进行调整

        //}
        public zuxyz[] SetAllZus()
        {
            List<zuxyz> zusls = new List<zuxyz>();
            string[] bc = { "This_dN", "This_dE", "This_dZ" };
            string[] lj = { "Ac_dN", "Ac_dE", "Ac_dZ" };
            //for (int i = 0; i < XzBlAry.Length; i++)
            //{
            //    if ((XzBlAry[i] == "This_dN" || XzBlAry[i] == "This_dE" || XzBlAry[i] == "This_dZ"))
            //    {
            //        bc.Add(XzBlAry[i]);
            //    }

            //    if ((XzBlAry[i] == "Ac_dN" || XzBlAry[i] == "Ac_dE" || XzBlAry[i] == "Ac_dZ"))
            //    {
            //        lj.Add(XzBlAry[i]);
            //    }
            //    if ((XzBlAry[i] == "Avg_N" || XzBlAry[i] == "Avg_E" || XzBlAry[i] == "Avg_Z"))
            //    {
            //        pc.Add(XzBlAry[i]);
            //    }
            //}

            zuxyz zu = new zuxyz { Bls = bc, Name = "本次变化量" };
            zusls.Add(zu);

            zu = new zuxyz { Bls = lj, Name = "累计变化" };
            zusls.Add(zu);
            return zusls.ToArray<zuxyz>();
        }
        #endregion
        #region 表面位移周期
        public static List<serie_cyc_null> SerializestrBMWY_cyc(HttpContext context)
        {

            //  conn = db.getsqldbconn();
            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            database db = new database();
            string sql = "";
            string pointname = "";
            zuxyz[] zus = { };
            //JavaScriptSerializer jss = new JavaScriptSerializer();
            //用sql语句测试连接是否成功
            if (context.Session["fmossql"] != null && context.Session["fmossql"] != "")
            {
                sql = context.Session["fmossql"].ToString();
                context.Session.Remove("fmossql");
            }
            if (context.Session["pointname"] != null && context.Session["pointname"] != "")
            {
                pointname = context.Session["pointname"].ToString();
                context.Session.Remove("pointname");
            }
            if (context.Session["zus"] != null && context.Session["zus"] != "")
            {
                zus = (zuxyz[])context.Session["zus"];
                //context.Session.Remove("zus");
                //string rlt = jss.Serialize(zus);
            }
            if (pointname != "" && pointname != null && zus != null)
            {
                //dt = cross(sql);
                //string xmname = context.Session["xmanme"].ToString();
                string xmname = context.Session["jcxmname"].ToString();
                OdbcConnection conn = db.GetStanderConn(xmname);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                dt = getfmostb(sql, conn);
                List<string> lsCycTerminal = TerminalCycGet(context); 
                string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[pointnamezu.Length];
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
                for (int k = 0; k < pointnamezu.Length; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(pointnamezu[k], dvzu[k]);
                }
                List<serie_cyc_null> lst = new List<serie_cyc_null>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < pointnamezu.Length; d++)
                    {
                        string filter =
                        dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d];
                        int cont = dvzu[d].Count;
                        List<int> lackCyc = new List<int>();
                        //校验记录数是否等于周期数
                        if (cont != Math.Abs(Int32.Parse(lsCycTerminal[0]) - Int32.Parse(lsCycTerminal[1])) + 1)
                        { 
                            //查出少的周期进行补偿
                            int l = 0;
                            for (l = Int32.Parse(lsCycTerminal[0]); l <= Int32.Parse(lsCycTerminal[1]);l++ )
                            {
                                DataView temp = dvzu[d];
                                temp.RowFilter = "cyc='" + l + "' and  " + " POINT_NAME= " + pointnamezu[d];
                                if (temp == null || temp.Count == 0)
                                    lackCyc.Add(l);
                            }
                            dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d];
                        }
                        //if (cont != 0)
                        //{
                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie_cyc_null st = new serie_cyc_null();//创建曲线
                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = pointnamezu[d].Replace("'", "") + "_" + rplname(zus[z].Bls[u]);
                            if(lackCyc.Count != 0)
                            switchnez_cyc_null(dvzu[d], st, zus[z].Bls[u],lackCyc);
                            else
                            switchnez_cyc_null(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                        //}

                    }



                }
                return lst;
                //string result = jss.Serialize(lst);
                //long ln = result.Length;
                //return result;
            }
            return null;
        }
        /// <summary>
        /// 区分出NEZ
        /// </summary>
        /// <param name="dv"></param>
        /// <param name="st"></param>
        /// <param name="xyzes"></param>
        public static void switchnez_cyc(DataView dv, serie_cyc st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_cyc[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "Avg_N");
                        i++;
                    }
                }
                else if (xyzes == "Avg_E")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "Avg_E");
                        i++;
                    }
                }
                else if (xyzes == "Avg_Z")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc();
                        st.Pts[i] = cycwgformat(drv, "Avg_Z");
                        i++;
                    }
                }


            }

        }
        public static void switchnez_cyc_null(DataView dv, serie_cyc_null st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_cyc_null[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "Avg_N");
                        i++;
                    }
                }
                


            }

        }
        /// <summary>
        /// 区分出NEZL
        /// </summary>
        /// <param name="dv"></param>
        /// <param name="st"></param>
        /// <param name="xyzes"></param>
        public static void switchnez_cyc_null(DataView dv, serie_cyc_null st, string xyzes,List<int> lackCyc )
        {
            List<int> tempCyc = new List<int>(lackCyc.ToArray());
            int len = dv.Count+lackCyc.Count;
            int i = 0,k =0;
            st.Pts = new pt_cyc_null[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    serie_cyc_create(dv,len,tempCyc,"This_dE",st);
                }
                else if (xyzes == "This_dN")//Y
                {
                    serie_cyc_create(dv, len, tempCyc, "This_dN", st);
                }
                else if (xyzes == "This_dZ")//Z
                {
                    serie_cyc_create(dv, len, tempCyc, "This_dZ", st);
                }
                else if (xyzes == "Ac_dN")//X
                {
                    serie_cyc_create(dv, len, tempCyc, "Ac_dN", st);
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    serie_cyc_create(dv, len, tempCyc, "Ac_dE", st);
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    serie_cyc_create(dv, len, tempCyc, "Ac_dZ", st);
                }
               


            }

        }
        public static void serie_cyc_create( DataView dv ,int len,List<int>tempCyc ,string bls,serie_cyc_null st)
        {
            int i = 0, k = 0,t = 0;

            foreach (DataRowView drv in dv)
            {
                st.Pts[i] = new pt_cyc_null();
                //if(lackCyc.)
                int cycIndex = Int32.Parse(drv["cyc"].ToString());
                tempCyc.Add(cycIndex);
                tempCyc.Sort();
                if (tempCyc.IndexOf(cycIndex) == 0)
                {
                    st.Pts[i] = cycwgformat_null(drv, bls);
                    tempCyc.Remove(cycIndex);
                    i++;
                }
                else
                {
                    LackCycMakeUp(tempCyc, cycIndex, bls, st, ref i, drv);
                }
            }
            while (tempCyc.Count>0)
            {
                st.Pts[i] = new pt_cyc_null { Pt_x = tempCyc[0].ToString(), Yvalue1 = null };
                i++;
                tempCyc.RemoveAt(0);
            }

        }





        //周期格式化时间
        public static pt_cyc cycwgformat_cyc(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = drv["cyc"].ToString();

            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt_cyc { Pt_x = pt_x, Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        //获取端点周期
        public static List<string> TerminalCycGet(HttpContext context)
        {
         
               List<string> ls = new List<string>();
               ls.Add(context.Session["minCyc"].ToString());
               ls.Add(context.Session["maxCyc"].ToString());
               return ls;
        }
        #endregion
        #region 表面位移多点单周期
        public static List<serie_point> SerializestrBMWY_point(HttpContext context)
        {

            //  conn = db.getsqldbconn();
            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            database db = new database();
            string sql = "";
            string pointname = "";
            zuxyz[] zus = { };
            //JavaScriptSerializer jss = new JavaScriptSerializer();
            //用sql语句测试连接是否成功
            if (context.Session["fmossql"] != null && context.Session["fmossql"] != "")
            {
                sql = context.Session["fmossql"].ToString();
                context.Session.Remove("fmossql");
            }
            if (context.Session["pointname"] != null && context.Session["pointname"] != "")
            {
                pointname = context.Session["pointname"].ToString();
                context.Session.Remove("pointname");
            }
            if (context.Session["zus"] != null && context.Session["zus"] != "")
            {
                zus = (zuxyz[])context.Session["zus"];
                //context.Session.Remove("zus");
                //string rlt = jss.Serialize(zus);
            }
            if (pointname != "" && pointname != null && zus != null)
            {
                //dt = cross(sql);
                //string xmname = context.Session["xmanme"].ToString();
                string xmname = context.Session["jcxmname"].ToString();
                OdbcConnection conn = db.GetStanderConn(xmname);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                dt = getfmostb(sql, conn);
                DataView dvdefault = new DataView(dt);
                DataTable dtdistinct = dvdefault.ToTable(true,"cyc");
                //从表中筛选出周期
                List<string> lscyc = CycListGet(dtdistinct);
                
                //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                //string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[lscyc.Count];
                
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();

                for (int k = 0; k < lscyc.Count; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(lscyc[k], dvzu[k]);
                }
                List<serie_point> lst = new List<serie_point>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < lscyc.Count; d++)
                    {
                        string filter =
                        dvzu[d].RowFilter = "cyc= " + lscyc[d];
                        int cont = dvzu[d].Count;
                        //if (cont != 0)
                        //{

                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie_point st = new serie_point();//创建曲线
                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = lscyc[d].Replace("'", "") + "_" + rplname(zus[z].Bls[u]);
                            switchnez_point(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                        //}

                    }



                }
                return lst;
                //string result = jss.Serialize(lst);
                //long ln = result.Length;
                //return result;
            }
            return null;
        }
        public static List<string> CycListGet(DataTable dt)
        {
            DataView dv = new DataView(dt);
            List<string> ls = new List<string>();
            foreach (DataRowView drv in dv)
            {
                ls.Add(drv["cyc"].ToString());
            }
            return ls;
        }
        /// <summary>
        /// 区分出NEZ
        /// </summary>
        /// <param name="dv"></param>
        /// <param name="st"></param>
        /// <param name="xyzes"></param>
        public static void switchnez_point(DataView dv, serie_point st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_point[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Avg_N");
                        i++;
                    }
                }
                else if (xyzes == "Avg_E")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Avg_E");
                        i++;
                    }
                }
                else if (xyzes == "Avg_Z")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Avg_Z");
                        i++;
                    }
                }


            }

        }
        //周期格式化时间
        public static pt_point pointwgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = drv["cyc"].ToString();

            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt_point { PointName = drv["point_name"].ToString() , Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        #endregion
        #region 雨量
        public static List<serie> SerializestrRAIN(HttpContext context)
        {
            DataTable dt = new DataTable();
            string raines = "";
            string sqllist = "";
            if (context.Session["rainsql"] != null)
            {
                sqllist = context.Session["rainsql"].ToString();
                //context.Session.Remove("rainsql");
            }
            if (context.Session["raines"] != "" && context.Session["raines"] != null)
            {
                raines = context.Session["raines"].ToString();
                //context.Session.Remove("raines");
            }
            //分模块号显示
            raines = raines.Replace("(", "");
            raines = raines.Replace(")", "");
            string[] rainarr = raines.Split(',');
            if (raines != "" && raines != null)
            {
                dt = cross(sqllist, context);
                List<serie> lst = new List<serie>();
                for (int i = 0; i < rainarr.Length; i++)
                {
                    serie sr = new serie();
                    //sr.Stype = "雨量";
                    DataView filter = new DataView(dt);
                    string filterstr = "RName in (" + rainarr[i] + ")";
                    filter.RowFilter = filterstr;
                    sr.Name = rainarr[i].Replace("'", "");
                    //加载点
                    formatdat(filter, sr);
                    lst.Add(sr);


                }
                return lst;
            }
            return null;
        }
        /// <summary>
        /// 用sql语句查询获得gps数据表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable getraintb(string sql, OdbcConnection conn)
        {
            List<DataTable> ldt = new List<DataTable>();

            DataTable dt = new DataTable();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;
        }
        public static DataTable cross(string sql, HttpContext context)
        {
            database db = new database();
            string xmname = context.Session["xmname"].ToString();
            //根据项目名称获取fmos数据库的位置
            OdbcConnection conn = db.GetStanderConn(xmname);
            DataTable dt = getraintb(sql, conn);
            return dt;
        }
        public DataTable kisstable(DataTable dt_m, DataTable dt_w)//两个表的字段一模一样
        {
            //获取被衔接字段的长度
            int len = dt_w.Columns.Count;
            DataView dv = new DataView(dt_w);
            foreach (DataRowView drv in dv)
            {
                DataRow dr = dt_m.NewRow();
                for (int i = 0; i < len; i++)
                {
                    dr[i] = drv[i];
                }
                dt_m.Rows.Add(dr);
            }
            return dt_m;
        }
        public static void formatdat(DataView dv, serie sr)
        {
            sr.Stype = "雨量";
            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["MonitorTime"].ToString();
                string v = drv["Rainfall"].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
        #endregion
        #region 深部位移
        public static List<serie> SerializestrSBWY(HttpContext context)
        {
            DataTable dt = new DataTable();
            string cxes = "";
            string sql = "";
            if (context.Session["cxsql"] != "" && context.Session["cxsql"] != null)
            {
                sql = context.Session["cxsql"].ToString();
                //context.Session.Remove("cxsql");
            }
            if (context.Session["cxes"] != "" && context.Session["cxes"] != null)
            {
                cxes = context.Session["cxes"].ToString();
                //context.Session.Remove("cxes");
            }
            //分模块号显示
            cxes = cxes.Replace("(", "");
            cxes = cxes.Replace(")", "");
            string[] cxarr = cxes.Split(',');
            if (cxes != "" && cxes != null)
            {
                dt = crosscx(sql, context);
                List<serie> lst = new List<serie>();
                for (int i = 0; i < cxarr.Length; i++)
                {
                    serie sr = new serie();
                    DataView filter = new DataView(dt);
                    string filterstr = "Name in (" + cxarr[i] + ")";
                    filter.RowFilter = filterstr;
                    sr.Name = cxarr[i].Replace(",", "");
                    //加载点
                    formatdatcx(filter, sr);
                    lst.Add(sr);


                }
                return lst;
            }
            return null;
        }
        /// <summary>
        /// 用sql语句查询获得gps数据表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable getcxtb(string sql, OdbcConnection conn)
        {
            DataTable dt = new DataTable();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;

        }
        public static DataTable crosscx(string sql, HttpContext context)
        {
            database db = new database();
            string xmname = context.Session["xmname"].ToString();
            //根据项目名称获取fmos数据库的位置
            OdbcConnection conn = db.GetStanderConn(xmname);
            DataTable dt = getcxtb(sql, conn);
            return dt;
        }
        public static void formatdatcx(DataView dv, serie sr)
        {
            sr.Stype = "深部位移";
            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["MonitorTime"].ToString();
                string v = drv["s"].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
        #endregion
        #region 沉降
        public static List<serie> SerializestrCJ(HttpContext context)
        {
            DataTable dt = new DataTable();
            string settlementes = "";
            string sql = "";
            if (context.Session["settlementsql"] != "" && context.Session["settlementsql"] != null)
            {
                sql = context.Session["settlementsql"].ToString();
                //context.Session.Remove("settlementsql");
            }
            if (context.Session["settlementes"] != "" && context.Session["settlementes"] != null)
            {
                settlementes = context.Session["settlementes"].ToString();
                //context.Session.Remove("settlementes");
            }
            //分模块号显示
            settlementes = settlementes.Replace("(", "");
            settlementes = settlementes.Replace(")", "");
            string[] settlementarr = settlementes.Split(',');
            if (settlementes != "" && settlementes != "''" && settlementes != null)
            {
                dt = crosscx(sql, context);
                List<serie> lst = new List<serie>();
                for (int i = 0; i < settlementarr.Length; i++)
                {
                    serie sr = new serie();
                    serie srAc = new serie();
                    DataView filter = new DataView(dt);
                    string filterstr = "pointname in (" + settlementarr[i] + ")";
                    filter.RowFilter = filterstr;
                    sr.Name = settlementarr[i].Replace("'", "");
                    srAc.Name = settlementarr[i].Replace("'", "");
                    //加载点
                    formatdatsettlement(filter, sr, "沉降本次", "this_val");
                    formatdatsettlement(filter, srAc, "沉降累计", "ac_val");
                    lst.Add(sr);
                    lst.Add(srAc);


                }
                return lst;
            }
            return null;
        }
        /// <summary>
        /// 用sql语句查询获得gps数据表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable getsettlementtb(string sql, OdbcConnection conn)
        {
            DataTable dt = new DataTable();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;

        }
        public static DataTable crosssettlement(string sql, HttpContext context)
        {
            database db = new database();
            string xmname = context.Session["xmname"].ToString();
            //根据项目名称获取fmos数据库的位置
            OdbcConnection conn = db.GetStanderConn(xmname);
            DataTable dt = getsettlementtb(sql, conn);
            return dt;
        }
        public static void formatdatsettlement(DataView dv, serie sr, string typeTitle, string valword)
        {
            sr.Stype = typeTitle;
            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["MonitorTime"].ToString();
                string v = drv[valword].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
        #endregion
        #region 应力
        public static List<serie> SerializestrAxia(HttpContext context)
        {
            DataTable dt = new DataTable();
            string settlementes = "";
            string sql = "";
            if (context.Session["strainsql"] != "" && context.Session["strainsql"] != null)
            {
                sql = context.Session["strainsql"].ToString();
                //context.Session.Remove("strainsql");
            }
            if (context.Session["straines"] != "" && context.Session["straines"] != null)
            {
                settlementes = context.Session["straines"].ToString();
                //context.Session.Remove("straines");
            }
            //分模块号显示
            settlementes = settlementes.Replace("(", "");
            settlementes = settlementes.Replace(")", "");
            string[] settlementarr = settlementes.Split(',');
            if (settlementes != "''" && settlementes != "" && settlementes != null)
            {
                dt = crosscx(sql, context);
                List<serie> lst = new List<serie>();
                for (int i = 0; i < settlementarr.Length; i++)
                {
                    serie sr = new serie();
                    serie srAc = new serie();
                    DataView filter = new DataView(dt);
                    string filterstr = "pointname in (" + settlementarr[i] + ")";
                    filter.RowFilter = filterstr;
                    sr.Name = settlementarr[i].Replace("'", "");
                    srAc.Name = settlementarr[i].Replace("'", "");
                    //加载点
                    formatdatstrain(filter, sr, "应力本次", "this_val");
                    formatdatstrain(filter, srAc, "应力累计", "ac_val");
                    lst.Add(sr);
                    lst.Add(srAc);

                }
                return lst;
            }
            return null;
        }
        /// <summary>
        /// 用sql语句查询获得gps数据表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable getstraintb(string sql, OdbcConnection conn)
        {
            DataTable dt = new DataTable();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;

        }
        //public static DataTable crosssettlement(string sql, HttpContext context)
        //{
        //    database db = new database();
        //    string xmname = context.Session["xmname"].ToString();
        //    根据项目名称获取fmos数据库的位置
        //    OdbcConnection conn = db.GetStanderConn(xmname);
        //    DataTable dt = getstraintb(sql, conn);
        //    return dt;
        //}
        public static void formatdatstrain(DataView dv, serie sr, string typeTitle, string valword)
        {
            sr.Stype = typeTitle;
            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["MonitorTime"].ToString();
                string v = drv[valword].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
        #endregion
        public static List<serie> GetSeries(List<serie> seriesprototype, int times)
        {
            List<serie> series = new List<serie>();
            series =  seriesprototype;
            foreach (serie s in series)
            {
                List<pt> ptList = new List<pt>();
                if (s.Pts.Length <= times) continue; 
                double secInterval = 0;
                int j = 0, e = 0;
                //DateTime t1 = new DateTime();
                //DateTime t2 = new DateTime();
                //long m = t2. - t1;
                secInterval = (s.Pts[s.Pts.Length - 1].Dt - s.Pts[0].Dt).TotalSeconds/times;
                try
                {
                    for (j = 0; j < times; j++)
                    {//4
                        while (true)
                        {//3
                            //////alert("e=" + e);
                            //double cursec = s.Pts[e].Dt;
                            if (s.Pts[0].Dt.AddSeconds(j * secInterval) <= s.Pts[e].Dt && s.Pts[e].Dt <= s.Pts[0].Dt.AddSeconds((j + 1) * secInterval))/*当前查找的点在刻度范围内*/
                            {//1
                                ////alert("找到符合要求的点");
                                ptList.Add(s.Pts[e]);
                                // //alert("找到符合要求的点下标为" + e);
                                e++;
                                break;
                                /*找到最近的点转到下一个刻度*/
                            } //1
                            else if (s.Pts[e].Dt < s.Pts[0].Dt.AddSeconds(j * secInterval))
                            {//2
                                e++;
                            } //2
                            else
                                break;
                        } //3
                    }
                    s.Pts = ptList.ToArray();
                }
                catch (Exception ex)
                {
                    
                    ExceptionLog.ExceptionWrite("曲线数据处理出错:j="+j+";e="+e+"\n");
                    //return series;
                }
                
            }
            return series;
        }
        public static List<serie_cyc> GetSeries_cyc(List<serie_cyc> seriesprototype, int times)
        {
            List<serie_cyc> series = new List<serie_cyc>();
            series = seriesprototype;
            foreach (serie_cyc s in series)
            {
                List<pt_cyc> ptList = new List<pt_cyc>();
                if (s.Pts.Length <= times) continue;
                double secInterval = 0;
                int j = 0, e = 0;
                //DateTime t1 = new DateTime();
                //DateTime t2 = new DateTime();
                //long m = t2. - t1;
                secInterval = (Int32.Parse(s.Pts[s.Pts.Length - 1].Pt_x) - Int32.Parse(s.Pts[0].Pt_x)) / times;
                try
                {
                    for (j = 0; j < times; j++)
                    {//4
                        while (true)
                        {//3
                            //////alert("e=" + e);
                            //double cursec = s.Pts[e].Dt;
                            if (Int32.Parse(s.Pts[0].Pt_x) + secInterval <= Int32.Parse(s.Pts[e].Pt_x) && Int32.Parse(s.Pts[e].Pt_x) <= Int32.Parse(s.Pts[0].Pt_x)+(j + 1) * secInterval)/*当前查找的点在刻度范围内*/
                            {//1
                                ////alert("找到符合要求的点");
                                ptList.Add(s.Pts[e]);
                                // //alert("找到符合要求的点下标为" + e);
                                e++;
                                break;
                                /*找到最近的点转到下一个刻度*/
                            } //1
                            else if (Int32.Parse(s.Pts[e].Pt_x) < Int32.Parse(s.Pts[0].Pt_x) + j * secInterval)
                            {//2
                                e++;
                            } //2
                            else
                                break;
                        } //3
                    }
                    s.Pts = ptList.ToArray();
                }
                catch (Exception ex)
                {

                    ExceptionLog.ExceptionWrite("曲线数据处理出错:j=" + j + ";e=" + e + "\n");
                    //return series;
                }

            }
            return series;
        }
        public static List<serie_cyc_null> GetSeries_cyc(List<serie_cyc_null> seriesprototype, int times)
        {
            List<serie_cyc_null> series = new List<serie_cyc_null>();
            series = seriesprototype;
            foreach (serie_cyc_null s in series)
            {
                List<pt_cyc_null> ptList = new List<pt_cyc_null>();
                if (s.Pts.Length <= times) continue;
                double secInterval = 0;
                int j = 0, e = 0;
                //DateTime t1 = new DateTime();
                //DateTime t2 = new DateTime();
                //long m = t2. - t1;
                secInterval = (Int32.Parse(s.Pts[s.Pts.Length - 1].Pt_x) - Int32.Parse(s.Pts[0].Pt_x)) / times;
                try
                {
                    for (j = 0; j < times; j++)
                    {//4
                        while (true)
                        {//3
                            //////alert("e=" + e);
                            //double cursec = s.Pts[e].Dt;
                            if (Int32.Parse(s.Pts[0].Pt_x) + secInterval <= Int32.Parse(s.Pts[e].Pt_x) && Int32.Parse(s.Pts[e].Pt_x) <= Int32.Parse(s.Pts[0].Pt_x) + (j + 1) * secInterval)/*当前查找的点在刻度范围内*/
                            {//1
                                ////alert("找到符合要求的点");
                                ptList.Add(s.Pts[e]);
                                // //alert("找到符合要求的点下标为" + e);
                                e++;
                                break;
                                /*找到最近的点转到下一个刻度*/
                            } //1
                            else if (Int32.Parse(s.Pts[e].Pt_x) < Int32.Parse(s.Pts[0].Pt_x) + j * secInterval)
                            {//2
                                e++;
                            } //2
                            else
                                break;
                        } //3
                    }
                    s.Pts = ptList.ToArray();
                }
                catch (Exception ex)
                {

                    ExceptionLog.ExceptionWrite("曲线数据处理出错:j=" + j + ";e=" + e + "\n");
                    //return series;
                }

            }
            return series;
        }
        public static void LackCycMakeUp(List<int> tempCyc,int cycIndex,string bls,serie_cyc_null st,ref int i,DataRowView drv)
        {
             //tempCyc.Add(cycIndex);
                            int k =0,t = 0;
                            for (k = 0; k < tempCyc.IndexOf(cycIndex); k++)
                            {
                                st.Pts[i] = new pt_cyc_null { Pt_x = tempCyc[k].ToString(), Yvalue1 = null };
                                i++;
                                
                            }
                            st.Pts[i] = cycwgformat_null(drv, bls);
                            i++;
                            for (t = 0; t <=k; t++)
                            {
                                tempCyc.RemoveAt(0);

                            }
                            
                        
                        
        }
        //public static List<serie_point> GetSeries_cyc(List<serie_point> seriesprototype, int times)
        //{
        //    List<serie_point> series = new List<serie_point>();
        //    series = seriesprototype;
        //    foreach (serie_point s in series)
        //    {
        //        List<pt_point> ptList = new List<pt_point>();
        //        if (s.Pts.Length <= times) continue;
        //        double secInterval = 0;
        //        int j = 0, e = 0;
        //        //DateTime t1 = new DateTime();
        //        //DateTime t2 = new DateTime();
        //        //long m = t2. - t1;
        //        secInterval = (Int32.Parse(s.Pts[s.Pts.Length - 1].PointName) - Int32.Parse(s.Pts[0].PointName)) / times;
        //        try
        //        {
        //            for (j = 0; j < times; j++)
        //            {//4
        //                while (true)
        //                {//3
        //                    //////alert("e=" + e);
        //                    //double cursec = s.Pts[e].Dt;
        //                    if (Int32.Parse(s.Pts[0].PointName) + secInterval <= Int32.Parse(s.Pts[e].PointName) && Int32.Parse(s.Pts[e].Pt_x) <= Int32.Parse(s.Pts[0].Pt_x) + (j + 1) * secInterval)/*当前查找的点在刻度范围内*/
        //                    {//1
        //                        ////alert("找到符合要求的点");
        //                        ptList.Add(s.Pts[e]);
        //                        // //alert("找到符合要求的点下标为" + e);
        //                        e++;
        //                        break;
        //                        /*找到最近的点转到下一个刻度*/
        //                    } //1
        //                    else if (Int32.Parse(s.Pts[e].Pt_x) < Int32.Parse(s.Pts[0].Pt_x) + j * secInterval)
        //                    {//2
        //                        e++;
        //                    } //2
        //                    else
        //                        break;
        //                } //3
        //            }
        //            s.Pts = ptList.ToArray();
        //        }
        //        catch (Exception ex)
        //        {

        //            ExceptionLog.ExceptionWrite("曲线数据处理出错:j=" + j + ";e=" + e + "\n");
        //            //return series;
        //        }

        //    }
        //    return series;
        //}
    }
}