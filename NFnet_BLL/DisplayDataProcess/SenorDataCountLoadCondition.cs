﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class SenorDataCountLoadCondition
    {
       
        public string sord { get; set; }
        public int xmno { get; set; }
        public string pointname { get; set; }
        public DateTime starttime { get; set; }
        public DateTime endtime { get; set; }
        public string totalCont { get; set; }
        public SenorDataCountLoadCondition(int xmno, string pointname, DateTime starttime, DateTime endtime)
        {
            this.xmno = xmno;
            this.pointname = pointname;
            this.starttime = starttime;
            this.endtime = endtime;
        }
    }
}