﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DisplayDataProcess
{
    public class ResultDataExtremelyCondition
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 日期函数
        /// </summary>
        public string dateFunction { get; set; }
        /// <summary>
        /// 周期
        /// </summary>
        public string extremelyCyc { get; set; }
        public ResultDataExtremelyCondition(string xmname, string dateFunction)
        {
            this.xmname = ProcessAspectIndirectValue.GetXmnoFromXmnameStr(xmname);
            this.dateFunction = dateFunction;
        }
    }
}