﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.Data;
using NFnet_BLL.DataProcess;
using System.IO;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 全站仪数据报表打印业务逻辑处理类
    /// </summary>
    public partial class ProcessOrglDataReportPrintBLL
    {
      
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationMPMCReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        {

            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点多周期");
                //var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                ////model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");

                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}


                totalStationReportHelper.OrglDataMainMPMC(
                    model.xmname,
                    "点名,周期,水平角,竖直角,斜距,北坐标,东坐标,竖直坐标,时间",
                    model.dt,
                    model.exportpath
                    );
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点多周期报表输出出错，错误信息:" + ex.Message);
                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }



        public class ProcessTotalStationSPMCReportModel
        {
            public string stCyc { get; set; }
            public string etCyc { get; set; }
            public string xmname { get; set; }
            public string xlspath { get; set; }
            public DataTable dt { get; set; }
            public string exportpath { get; set; }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
            }

        }





        /// <summary>
        /// 全站仪数据表打印类
        /// </summary>
        public class ProcessTotalStationReportModel : PrintCondition
        {
            public List<ChartCreateEnviroment> cce { get; set; }
            public ProcessTotalStationReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }

        public class ProcessTotalStationMPMCReportModel : PrintCondition
        {
            public ChartCreateEnviroment cce { get; set; }
            public List<string> pnames { get; set; }
            public ProcessTotalStationMPMCReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }
      


    }
}