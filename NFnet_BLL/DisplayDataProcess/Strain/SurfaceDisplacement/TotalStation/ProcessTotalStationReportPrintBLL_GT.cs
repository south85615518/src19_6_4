﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.Data;
using NFnet_BLL.DataProcess;
using System.IO;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 全站仪数据报表打印业务逻辑处理类
    /// </summary>
    public partial class ProcessTotalStationReportPrintBLL
    {
        /// <summary>
        /// 单点多周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationSPMCReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        {

            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成单点");
                var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,N, E, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx",new Random().Next(999)+".xlsx");
                if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                {
                    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                }
                totalStationReportHelper.MainSPMC(
                    model.xmname,
                    "周期,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath
                    );
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移单点多周期报表输出出错，错误信息:" + ex.Message);
                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationMPSCReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                {
                    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                }


                totalStationReportHelper.MainMPSC(
                    model.xmname,
                    "点名,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath
                    );
                mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点单周期报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationMPMCReport(ProcessTotalStationSPMCReportModel model, out string mssg)
        {

            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点多周期");
                var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");

                if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                {
                    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                }


                totalStationReportHelper.MainMPMC(
                    model.xmname,
                    "点名,周期,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath
                    );
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点多周期报表输出出错，错误信息:" + ex.Message);
                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }



        public class ProcessTotalStationSPMCReportModel
        {
            public string stCyc { get; set; }
            public string etCyc { get; set; }
            public string xmname { get; set; }
            public string xlspath { get; set; }
            public DataTable dt { get; set; }
            public string exportpath { get; set; }
            public string timeunitname { get; set; }
            public Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams reportparams { get; set; }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
            }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath, string timeunitname)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
                this.timeunitname = timeunitname;
            }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath, string timeunitname, Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams reportparams)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
                this.timeunitname = timeunitname;
                this.reportparams = reportparams;
            }
            public ProcessTotalStationSPMCReportModel(string stCyc, string etCyc, string xmname, string xlspath, DataTable dt, string exportpath, Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams reportparams)
            {
                this.stCyc = stCyc;
                this.etCyc = etCyc;
                this.xmname = xmname;
                this.xlspath = xlspath;
                this.dt = dt;
                this.exportpath = exportpath;
                this.reportparams = reportparams;
            }
            public ProcessTotalStationSPMCReportModel()
            {
               
            }
        }





        /// <summary>
        /// 全站仪数据表打印类
        /// </summary>
        public class ProcessTotalStationReportModel : PrintCondition
        {
            public List<ChartCreateEnviroment> cce { get; set; }
            public ProcessTotalStationReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }

        public class ProcessTotalStationMPMCReportModel : PrintCondition
        {
            public ChartCreateEnviroment cce { get; set; }
            public List<string> pnames { get; set; }
            public ProcessTotalStationMPMCReportModel(string startTime, string endTime, string xmname, string tabHead, string sheetName, DataTable dt, string splitname, string xlsPath)
            {
                this.startTime = startTime;
                this.endTime = endTime;
                this.xmname = xmname;
                this.tabHead = tabHead;
                this.dt = dt;
                this.sheetName = sheetName;
                this.splitname = splitname;
                this.xlspath = xlsPath;
            }
        }
        #region GT
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationMPSCReport_GT(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}


                List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(model.dt, "cyc");

                model.reportparams.alarmmess = NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess.ProcessReportAlarmSplitModelList.ReportAlarmSplitMssgLoad(NFnet_BLL.DisplayDataProcess.pub.ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname), cyclist, data.Model.gtsensortype._HorizontalDisplacement, out mssg);



                totalStationReportHelper.MainMPSC_GT(
                    model.xmname,
                    "点名,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath
                    );
                mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点单周期报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationMPSCReport_GT_WhithoutSettlement(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}



                model.reportparams = new Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams { alarmmess = new List<string>(), n_firstalrmacdisp = "5/-5", e_firstalrmacdisp = "5/-5", z_firstalrmacdisp = "2/-2", n_secalrmacdisp = "10/-10", e_secalrmacdisp = "10/-10", z_secalrmacdisp = "4/-4", xmaddress = "工程地址" };
                //model.reportparams.alarmmess = new string[] { "SDYBQ687累计东方向4.5，级别:预警" }.ToList();
                List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(model.dt, "cyc");

                //model.reportparams.alarmmess = NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess.ProcessReportAlarmSplitModelList.ReportAlarmSplitMssgLoad(NFnet_BLL.DisplayDataProcess.pub.ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname), cyclist, data.Model.gtsensortype._HorizontalDisplacement, out mssg);
                //List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(model.dt, "cyc");

                //model.reportparams.alarmmess = NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess.ProcessReportAlarmSplitModelList.ReportAlarmSplitMssgLoad(NFnet_BLL.DisplayDataProcess.pub.ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname), cyclist, data.Model.gtsensortype._HorizontalDisplacement, out mssg);

                totalStationReportHelper.MainMPSC_GT_WithoutSettlement(
                    model.xmname,
                    "点名,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath,
                    model.reportparams
                    );
                mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点单周期报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }
        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationMPSCReport_GT_Report_WithoutSettlement(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}




                //model.reportparams = new Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams { alarmmess = new List<string>(), n_firstalrmacdisp = "5/-5", e_firstalrmacdisp = "5/-5", z_firstalrmacdisp = "2/-2", n_secalrmacdisp = "10/-10", e_secalrmacdisp = "10/-10", z_secalrmacdisp = "4/-4", xmaddress = "工程地址" };
                //model.reportparams.alarmmess = new string[] { "SDYBQ687累计东方向4.5，级别:预警" }.ToList();
                List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(model.dt, "cyc");

                model.reportparams.alarmmess = NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess.ProcessReportAlarmSplitModelList.ReportAlarmSplitMssgLoad(NFnet_BLL.DisplayDataProcess.pub.ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname), cyclist, data.Model.gtsensortype._HorizontalDisplacement, out mssg);
                totalStationReportHelper.MainMPSC_GT_DAYReport_WithoutSettlement(
                    model.xmname,
                    "点名,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath,
                    model.timeunitname,
                    model.reportparams
                    );
                mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点单周期报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// 多点多单周期报表打印
        /// </summary>
        /// <returns></returns>
        public bool ProcessTotalStationMPSCReport_GT_dimension_Report_WithoutSettlement(ProcessTotalStationSPMCReportModel model, out string mssg)
        {
            mssg = "";
            try
            {
                ExceptionLog.ExceptionWrite(model.xlspath);
                TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
                totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
                ExceptionLog.ExceptionWrite("模板初始化完成多点");
                var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
                ExceptionLog.ExceptionWrite("开始填充数据");
                //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


                //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
                //{
                //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
                //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
                //}



                //model.reportparams = new Tool.DATAREPORT.TotalStation.MPSC.GT_DAYRepprt_WhithoutSettlement.reportparams { alarmmess = new List<string>(), n_firstalrmacdisp = "5/-5", e_firstalrmacdisp = "5/-5", z_firstalrmacdisp = "2/-2", n_secalrmacdisp = "10/-10", e_secalrmacdisp = "10/-10", z_secalrmacdisp = "4/-4", xmaddress = "工程地址" };
                List<string> cyclist = Tool.DataTableHelper.ProcessDataTableFilter(model.dt, "cyc");

                model.reportparams.alarmmess = NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess.ProcessReportAlarmSplitModelList.ReportAlarmSplitMssgLoad(NFnet_BLL.DisplayDataProcess.pub.ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname), cyclist, data.Model.gtsensortype._HorizontalDisplacement, out mssg);

                totalStationReportHelper.MainMPSC_GT_dimension_Report_WithoutSettlement(
                    model.xmname,
                    "点名,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
                    model.dt,
                    model.exportpath,
                    model.reportparams
                    );
                mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("表面位移多点单周期报表输出出错，错误信息:" + ex.Message);

                ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
                return false;
            }
        }





        ///// <summary>
        ///// 多点多单周期报表打印
        ///// </summary>
        ///// <returns></returns>
        //public bool ProcessTotalStationMPSCReport_GT_Week_Report_WhithoutSettlement(ProcessTotalStationSPMCReportModel model, out string mssg)
        //{
        //    mssg = "";
        //    try
        //    {
        //        ExceptionLog.ExceptionWrite(model.xlspath);
        //        TotalStationReportHelper totalStationReportHelper = new TotalStationReportHelper();
        //        totalStationReportHelper.PublicCompatibleInitializeWorkbook(/*context.Server.MapPath("..\\数据报表\\ReportExcel.xls")*/model.xlspath);
        //        ExceptionLog.ExceptionWrite("模板初始化完成多点");
        //        var processTotalStationReportModel = new ProcessTotalStationReportPrintBLL.ProcessTotalStationReportModel(string.Format("{1}      {0}", model.stCyc, "{0}"), string.Format("{0}", model.etCyc), model.xmname, "时间,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ", "{0}", model.dt, "point_name", model.exportpath);
        //        ExceptionLog.ExceptionWrite("开始填充数据");
        //        //model.exportpath = model.exportpath.Replace(".xlsx", new Random().Next(999) + ".xlsx");


        //        //if (File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt"))
        //        //{
        //        //    ExceptionLog.ExceptionWrite(string.Format("项目{0}表面位移存在修正值,现在对数据做修正处理...", model.xmname));
        //        //    model.dt = Tool.com.OffsetHelper.ProcessTotalStationResultDataOffset(model.dt, System.AppDomain.CurrentDomain.BaseDirectory + "\\表面位移修正值\\" + model.xmname + "\\offset.txt");
        //        //}






        //        totalStationReportHelper.MainMPSC_GT_WeekReport_WithoutSettlement(
        //            model.xmname,
        //            "点名,X, Y, Z, ΔX, ΔY, ΔZ, ΣΔX, ΣΔY, ΣΔZ,时间",
        //            model.dt,
        //            model.exportpath
        //            );
        //        mssg = string.Format(DateTime.Now + " {0}表面位移{1}多点单周期报表生成成功", model.xmname, Path.GetFileNameWithoutExtension(model.exportpath));
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLog.ExceptionWrite("表面位移多点单周期报表输出出错，错误信息:" + ex.Message);

        //        ExceptionLog.ExceptionWrite("位置:" + ex.StackTrace);
        //        return false;
        //    }
        //}

        #endregion


    }
}