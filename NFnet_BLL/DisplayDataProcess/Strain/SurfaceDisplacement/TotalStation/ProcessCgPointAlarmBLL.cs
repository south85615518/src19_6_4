﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.BLL;
using NFnet_BLL.DataProcess;
using System.Data;
using TotalStation.Model.fmos_obj;
using Tool;
using NFnet_BLL.XmInfo;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 点号预警业务逻辑处理类
    /// </summary>
    public partial class ProcessPointAlarmBLL
    {

        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessCgPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            string mssg = "";
            List<string> ls = new List<string>();
            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.POINT_NAME,
                time = model.resultModel.Time,
                type = "表面位移",
                atime = DateTime.Now,
                pid = string.Format("a{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processCgAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, "表面位移", model.resultModel.POINT_NAME, model.resultModel.Time);
            splitOnDateBLL.ProcesscgalarmsplitondateDelete(processCgAlarmSplitOnDateDeleteModel, out mssg);

            AuthorityAlarm.Model.alarmsplitondate cgalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = "表面位移",
                pointName = model.resultModel.POINT_NAME,
                xmno = model.xmno,
                time = model.resultModel.Time,
                adate = DateTime.Now,
                cyc = model.resultModel.CYC

            };


            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add(string.Format("{0}三级预警",model.resultModel.POINT_NAME));
                    pointCheck.alarm = 3;
                    pointCheckBLL.ProcessCgPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;
                   
                    cgalarm.alarmContext = string.Join(",", ls);
                    cgalarm.alarm = 3;
                    splitOnDateBLL.ProcesscgalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "表面位移", "", model.resultModel.POINT_NAME, 3, out mssg);

                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add(string.Format("{0}二级预警", model.resultModel.POINT_NAME));
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessCgPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;

                    cgalarm.alarmContext = string.Join(",", ls);
                    cgalarm.alarm = 2;
                    splitOnDateBLL.ProcesscgalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "表面位移", "", model.resultModel.POINT_NAME, 2, out mssg);
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add(string.Format("{0}一级预警", model.resultModel.POINT_NAME));
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessCgPointCheckAdd(pointCheck, out mssg);
                    model.ls = ls;

                    cgalarm.alarmContext = string.Join(",", ls);
                    cgalarm.alarm = 1;
                    splitOnDateBLL.ProcesscgalarmsplitondateAdd(cgalarm, out mssg);
                    ProcessHotPotColorUpdate(model.xmno, "表面位移", "", model.resultModel.POINT_NAME, 1, out mssg);
                    return true;
                }
            }
            ls.Add("========预警解除==========");
            pointCheck.alarm = 0;
            pointCheckBLL.ProcessCgPointCheckDelete(pointCheck, out mssg);
            ProcessCgHotPotColorUpdate(model.xmno, "表面位移", "", model.resultModel.POINT_NAME, 0, out mssg);
            return false;

        }
        
       
        public bool ProcessCgPointAlarmModelGet(ProcessPointAlarmModelGetModel model, out string mssg)
        {
            PointAttribute.Model.fmos_pointalarmvalue pointalarm = new PointAttribute.Model.fmos_pointalarmvalue();
            if (bll.GetModel(model.xmno, model.pointName, out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }
  
       

        public bool ProcessCgHotPotColorUpdate(int xmno, string jclx, string jcoption, string pointName, int color, out string mssg)
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(xmno, jclx, jcoption, pointName, color);
            return layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);

        }

    }
}