﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;
using TotalStation.BLL.fmos_obj;
namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 时间任务业务处理逻辑
    /// </summary>
    public class ProcessTimeTaskBLL
    {
        public static timetask timetaskBLL = new timetask();
        /// <summary>
        /// 时间任务表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcesstimetaskRecordsCount(ProcesstimetaskRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";

            if (timetaskBLL.timetaskTableRowsCount(model.searchpost, model.xmname, out totalCont, out mssg))
            {
                model.tabCount = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 时间任务记录数获取类
        /// </summary>
        public class ProcesstimetaskRecordsCountModel : TabCountSearchCondition
        {
            /// <summary>
            /// 筛选条件
            /// </summary>
            public string searchpost { get; set; }
            public ProcesstimetaskRecordsCountModel(string xmname, int xmno, string searchpost)
            {
                this.xmname = xmname;
                this.searchpost = searchpost;

            }
        }
        /// <summary>
        /// 时间任务表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcesstimetaskLoad(ProcesstimetaskLoadModel model, out string mssg)
        {
            DataTable dt = null;

            if (timetaskBLL.timetaskTableLoad(model.pageIndex, model.rows, model.xmname, model.searchpost, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 时间任务表获取类
        /// </summary>
        public class ProcesstimetaskLoadModel : SearchCondition
        {
            /// <summary>
            /// 筛选条件
            /// </summary>
            public string searchpost { get; set; }
            /// <summary>
            /// 时间任务表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcesstimetaskLoadModel(string xmname, int pageIndex, int rows, string colName, string sord, string searchpost)
            {
                this.xmname = xmname;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.searchpost = searchpost;
                this.colName = colName;

            }
        }

     

    }
}