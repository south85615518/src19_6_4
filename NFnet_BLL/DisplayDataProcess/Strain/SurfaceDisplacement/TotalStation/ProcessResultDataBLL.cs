﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation;
using System.Data;
using TotalStation.BLL.fmos_obj;
using SqlHelpers;
using NFnet_BLL.Other;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using Tool;


namespace NFnet_BLL.DataProcess
{

    /// <summary>
    /// 结果数据业务逻辑处理类
    /// </summary>
    public partial class ProcessResultDataBLL
    {

        public static cycdirnet cycdirnetBLL = new cycdirnet();

        public bool ProcessResultDataAdd(global::TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            return cycdirnetBLL.Add(model, out mssg);
        }
        public bool ProcessSurveyResultDataAdd(global::TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            return cycdirnetBLL.AddSurveyData(model, out mssg);
        }


        public bool ProcessCgResultDataAdd(ProcessCgResultDataAddModel model, out string mssg)
        {
            return cycdirnetBLL.AddCgData(model.xmname, model.cyc, model.importCyc, out mssg);
        }
        public class ProcessCgResultDataAddModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public int importCyc { get; set; }
            public ProcessCgResultDataAddModel(string xmname, int cyc, int importCyc)
            {
                this.xmname = xmname;
                this.cyc = cyc;
                this.importCyc = importCyc;
            }
        }

        public bool ProcessSurveyDataUnion(ProcessSurveyDataUnionModel model, out string mssg)
        {
            return cycdirnetBLL.SurveyDataUnion(model.xmname, model.cyc, model.targetcyc, model.lastcyc, out mssg);
        }
        public class ProcessSurveyDataUnionModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public int targetcyc { get; set; }
            public int lastcyc { get; set; }
            public ProcessSurveyDataUnionModel(string xmname, int cyc, int targetcyc, int lastcyc)
            {
                this.xmname = xmname;
                this.cyc = cyc;
                this.targetcyc = targetcyc;
                this.lastcyc = lastcyc;
            }
        }
        public bool ProcessSurveyDataCouldUnion(ProcessSurveyDataCouldUnionModel model, out string mssg)
        {
            return cycdirnetBLL.SurveyDataCouldUnion(model.xmname, model.cyc, model.targetcyc, out mssg);
        }
        public class ProcessSurveyDataCouldUnionModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public int targetcyc { get; set; }
            public ProcessSurveyDataCouldUnionModel(string xmname, int cyc, int targetcyc)
            {
                this.xmname = xmname;
                this.cyc = cyc;
                this.targetcyc = targetcyc;
            }
        }

        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessCgResultDataLoad(ResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (cycdirnetBLL.CgResultDataTableLoad(model.startCyc, model.endCyc, model.pageIndex, model.rows, model.xmname, model.pointname, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        public bool ProcessResultDataUpdate(global::TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            return cycdirnetBLL.Update(model, out mssg);
        }

        public bool ProcessSurveyDataUpdata(global::TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            return cycdirnetBLL.UpdataSurveyData(model, out mssg);
        }
        public bool ProcessSurveyDataNextCYCThisUpdate(global::TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            return cycdirnetBLL.UpdataSurveyDataNextCYCThis(model, out mssg);
        }
        //public class ProcessSurveyDataUpdataModel
        //{
        //    public string xmname{ get;set;}
        //    public string vSet_name { get; set; }
        //    public string vLink_name { get; set; }
        //    public string pointname { get; set; }
        //    public int cyc { get; set; }
        //    public double srcval { get; set; }
        //    public ProcessSurveyDataUpdataModel(string xmname,string pointname,string vSet_name,string vLink_name,int cyc,double srcval)
        //    {
        //        this.xmname = xmname;
        //        this.pointname = pointname;
        //        this.cyc = cyc;
        //        this.srcval = srcval;
        //        this.vSet_name = vSet_name;
        //        this.vLink_name = vLink_name;
        //    }

        //}


        public bool ProcessResultDataAdd(ProcessResultDataAddModel model, out string mssg)
        {
            return cycdirnetBLL.AddData(model.xmname, model.cyc, out mssg);
        }
        public class ProcessResultDataAddModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public ProcessResultDataAddModel(string xmname, int cyc)
            {
                this.xmname = xmname;
                this.cyc = cyc;
            }
        }





        public bool ProcessSurveyPointCycDataAdd(ProcessSurveyPointCycDataAddModel model, out string mssg)
        {
            return cycdirnetBLL.SurveyPointCycDataAdd(model.xmname, model.pointname, model.cyc, model.importcyc, out mssg);
        }
        public class ProcessSurveyPointCycDataAddModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public int cyc { get; set; }
            public int importcyc { get; set; }
            public ProcessSurveyPointCycDataAddModel(string xmname, string pointname, int cyc, int importcyc)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.cyc = cyc;
                this.importcyc = importcyc;
            }
        }


        public bool ProcessLostPoints(ProcessLostPointsModel model, out string mssg)
        {
            mssg = "";
            string pointstr = "";
            if (cycdirnetBLL.LostPoints(model.xmno, model.xmname, model.cyc, out pointstr, out mssg))
            {
                model.pointstr = pointstr;
                return true;
            }
            return false;

        }
        public class ProcessLostPointsModel
        {
            public int xmno { get; set; }
            public string xmname { get; set; }
            public int cyc { get; set; }
            public string pointstr { get; set; }
            public ProcessLostPointsModel(string xmname, int xmno, int cyc)
            {
                this.cyc = cyc;
                this.xmno = xmno;
                this.xmname = xmname;
                this.pointstr = pointstr;
            }
        }


        /// <summary>
        /// 结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataRecordsCount(ResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (cycdirnetBLL.ResultTableRowsCount(model.startCyc, model.endCyc, model.xmname, model.pointname, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 结果数据编辑表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSurveyResultDataLoad(ResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (cycdirnetBLL.SurveyResultDataTableLoad(model.startCyc, model.endCyc, model.pageIndex, model.rows, model.xmname, model.pointname, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataLoad(ResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (cycdirnetBLL.ResultDataTableLoad(model.startCyc, model.endCyc, model.pageIndex, model.rows, model.xmname, model.pointname, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }


        public bool ProcessXmStateTableLoad(XmStateTableLoadCondition model, out string mssg)
        {
            DataTable dt = new DataTable();
            if (cycdirnetBLL.XmStateTable(model.pageIndex, model.rows, model.xmno, model.xmname, model.unitname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 结果数据报表数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataReportTableCreate(ResultDataReportTableCreateCondition model, out string mssg)
        {
            DataTable dt = null;
            if (cycdirnetBLL.ResultDataReportPrint(model.sql, model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 结果数据端点周期获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataExtremely(ResultDataExtremelyCondition model, out string mssg)
        {
            string extremelyCyc = "";
            if (cycdirnetBLL.ExtremelyCycGet(model.xmname, model.dateFunction, out mssg, out extremelyCyc))
            {
                model.extremelyCyc = extremelyCyc;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 数据展示日期查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {

            switch (model.type)
            {
                case QueryType.RQCX:
                    string sqlsttm = "";
                    string sqledtm = "";
                    string mssg = "";
                    string cycmin = "";
                    string cycmax = "";
                    var querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.startTime, ">=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqlsttm = querynvlmodel.str;
                    }
                    querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.endTime, "<=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqledtm = querynvlmodel.str;
                    }
                    string rqConditionStr = sqlsttm + " and " + sqledtm + " order by #_point,cyc,#_date asc ";
                    string sqlmin = "select min(cyc) as mincyc from fmos_cycdirnet where taskName = '" + model.xmname + "'";
                    string sqlmax = "select max(cyc) as maxcyc from fmos_cycdirnet where taskName = '" + model.xmname + "'";
                    var processquerystanderstrModel = new QuerystanderstrModel(sqlmin, model.xmname);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                        cycmin = processquerystanderstrModel.str;
                    }
                    processquerystanderstrModel = new QuerystanderstrModel(sqlmax, model.xmname);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                        cycmax = processquerystanderstrModel.str;
                    }
                    model.minCyc = cycmin;
                    model.maxCyc = cycmax;
                    model.sql = rqConditionStr;
                    break;
                case QueryType.ZQCX:
                    rqConditionStr = "#_cyc >= " + model.startTime + "   and  #_cyc <= " + model.endTime + " order by #_point,cyc,#_date asc ";
                    model.minCyc = model.startTime;
                    model.maxCyc = model.endTime;

                    model.sql = rqConditionStr;
                    break;
                case QueryType.QT:
                    var processdateswdlModel = new ProcessComBLL.ProcessdateswdlModel("#_date", model.unit, model.maxTime);
                    if (ProcessComBLL.Processdateswdl(processdateswdlModel, out mssg))
                    {
                        model.startTime = processdateswdlModel.sttm;
                        model.endTime = processdateswdlModel.edtm;
                        //model.sql = processdateswdlModel.sql;
                    }
                    break;


            }
        }



        public bool ProcessResultDataMaxTime(TotalStationResultDataMaxTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (cycdirnetBLL.MaxTime(model.xmname, out maxTime, out mssg))
            {
                model.maxTime = maxTime;
                return true;
            }
            return false;


        }


        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessfillTotalStationDbFill(FillTotalStationDbFillCondition model)
        {
            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "Time");
            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "POINT_NAME");
            model.rqConditionStr = model.rqConditionStr.Replace("#_cyc", "cyc");
            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" POINT_NAME ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = "select  point_name,cyc,n,e,z,round(this_dn,2) as this_dn,round(this_de,2) this_de,round(this_dz,2) as this_dz, round(ac_dn,2) as ac_dn,round(ac_de,2) as ac_de,round(ac_dz,2) as ac_dz,time,siblingpoint_name  from  fmos_cycdirnet where  taskName = '" + model.xmname + "' and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
                sql += " and " + model.rqConditionStr;

            }
            List<string> columnnames = "point_name,cyc,n,e,z,this_dn, this_de,this_dz,  ac_dn, ac_de, ac_dz,time,siblingpoint_name".Split(',').ToList();
            model.sql = sql;
            var processquerystanderdbModel = new QuerystanderdbModel(sql, model.xmname,columnnames);
            if (ProcessComBLL.ProcessquerystanderdbInreader(processquerystanderdbModel, out mssg))
            {
                ExceptionLog.ExceptionWrite(sql);
                ExceptionLog.ExceptionWrite(mssg);
                model.dt = processquerystanderdbModel.dt;
                return true;
            }
            else
            {
                return false;
            }
            


        }

        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessfillTotalStationDbFillNotExcute(FillTotalStationDbFillCondition model)
        {
            DateTime dtstart = DateTime.Now;
            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "Time");
            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "POINT_NAME");
            model.rqConditionStr = model.rqConditionStr.Replace("#_cyc", "cyc");
            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel(" POINT_NAME ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = "select  point_name,cyc,n,e,z,round(this_dn,2) as this_dn,round(this_de,2) this_de,round(this_dz,2) as this_dz, round(ac_dn,2) as ac_dn,round(ac_de,2) as ac_de,round(ac_dz,2) as ac_dz,time,siblingpoint_name  from  fmos_cycdirnet where  taskName = '" + model.xmname + "' and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
                sql += " and " + model.rqConditionStr;

            }

            model.sql = sql;
            ExceptionLog.ExceptionWrite("本次生成SQL语句用时："+(DateTime.Now - dtstart).TotalMilliseconds+"毫秒");
            List<string> columnnames = "point_name,cyc,n,e,z,this_dn, this_de,this_dz,  ac_dn, ac_de, ac_dz,time,siblingpoint_name".Split(',').ToList();
            model.sql = sql;
            var processquerystanderdbModel = new QuerystanderdbModel(sql, model.xmname,columnnames);
            if (ProcessComBLL.ProcessquerystanderdbInreader(processquerystanderdbModel, out mssg))
            {
                ExceptionLog.ExceptionWrite(sql);
                ExceptionLog.ExceptionWrite(mssg);
                model.dt = processquerystanderdbModel.dt;
                return true;
            }
            else
            {
                return false;
            }
            

        }
        //根据表的分页单位获取当前记录在表中的页数
        public int PageIndexFromTabCYC(string pointname, string date, DataView dv, string pointnameStr, string dateStr, int pageSize)
        {

            //DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr];
                if (drv[pointnameStr].ToString() == pointname && date.Trim() == drv[dateStr].ToString())
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }

        /// <summary>
        /// 获取fmos组号
        /// </summary>
        /// <returns></returns>
        public zuxyz[] getfmoszu()
        {

            string[] XzBlAry = { "This_dN", "This_dE", "This_dZ", "Ac_dN", "Ac_dE", "Ac_dZ" };
            List<zuxyz> zusls = new List<zuxyz>();
            List<string> bc = new List<string>();
            List<string> lj = new List<string>();
            List<string> pc = new List<string>();

            for (int i = 0; i < XzBlAry.Length; i++)
            {
                if ((XzBlAry[i] == "This_dN" || XzBlAry[i] == "This_dE" || XzBlAry[i] == "This_dZ"))
                {
                    bc.Add(XzBlAry[i]);
                }

                if ((XzBlAry[i] == "Ac_dN" || XzBlAry[i] == "Ac_dE" || XzBlAry[i] == "Ac_dZ"))
                {
                    lj.Add(XzBlAry[i]);
                }
                if ((XzBlAry[i] == "Avg_N" || XzBlAry[i] == "Avg_E" || XzBlAry[i] == "Avg_Z"))
                {
                    pc.Add(XzBlAry[i]);
                }
            }
            if (bc.Count != 0)
            {
                zuxyz zu = new zuxyz { Bls = bc.ToArray<string>(), Name = "本次变化量" };
                zusls.Add(zu);
            }
            if (lj.Count != 0)
            {
                zuxyz zu = new zuxyz { Bls = lj.ToArray<string>(), Name = "累计变化" };
                zusls.Add(zu);
            }
            if (pc.Count != 0)
            {
                zuxyz zu = new zuxyz { Bls = pc.ToArray<string>(), Name = "平差" };
                zusls.Add(zu);
            }
            return zusls.ToArray<zuxyz>();


        }
        public static bool ProcessResultDataCycLoad(CycLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";
            if (cycdirnetBLL.CycList(model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public  bool ProcessResultDataTimeCycLoad(ProcessResultDataTimeCycLoadModel model, out string mssg)
        {
            List<string> cyctimelist = null;
            mssg = "";
            if (cycdirnetBLL.CycTimeList(model.xmname, out cyctimelist, out mssg))
            {
                model.cyctimelist = cyctimelist;
                return true;
            }
            return false;
        }
        public class ProcessResultDataTimeCycLoadModel
        {
            public List<string> cyctimelist { get; set; }
            public string xmname { get; set; }
            public ProcessResultDataTimeCycLoadModel(string xmname)
            {
                this.xmname = xmname;
            }

        }
        public class ProcessResultDataCycLoadModel
        {
            public string xmname { get; set; }
            public DataTable dt { get; set; }

        }

        public bool ProcessDateTimeToCyc(DateTimeToCycCondition model, out string mssg)
        {
            List<string> cycList = null;
            if (cycdirnetBLL.GetCycFromDateTime(model.xmname, model.startTime, model.endTime, model.pointstr, out cycList, out mssg))
            {
                model.cycList = cycList;
                return true;
            }
            else
            {
                return false;
            }

        }

        public class ProcessDateTimeToCycModel
        {
            public string xmname { get; set; }
            public string startTime { get; set; }
            public string endTime { get; set; }
            public string pointstr { get; set; }
            public List<string> cycList { get; set; }
            public ProcessDateTimeToCycModel(string xmname, string startTime, string endTime, string pointstr)
            {
                this.xmname = xmname;
                this.startTime = startTime;
                this.endTime = endTime;
                this.pointstr = pointstr;
            }
        }

        public bool ProcessResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist, out string mssg)
        {
            modellist = null;
            if (cycdirnetBLL.GetModelList(model.xmname, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }

        public class ProcessResultDataAlarmModelListModel
        {
            public string xmname { get; set; }
            public List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist { set; get; }
            public ProcessResultDataAlarmModelListModel(string xmname)
            {
                this.xmname = xmname;
            }
        }


        public bool ProcessResultDataCycModelList(ProcessCycModelListModel model, out string mssg)
        {
            List<global::TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist = null;
            if (cycdirnetBLL.CycModelList(model.xmname, model.cyc, out cycdirnetlist, out mssg))
            {
                model.cycdirnetlist = cycdirnetlist;
                return true;
            }
            return false;
        }
        public class ProcessCycModelListModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public List<global::TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist { get; set; }
            public ProcessCycModelListModel(string xmname, int cyc)
            {
                this.xmname = xmname;
                this.cyc = cyc;
            }
        }


        public bool ProcessDataTableIntoModelList(ProcessDataTableIntoModelListModel model, out string mssg)
        {
            List<global::TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist = null;
            if (cycdirnetBLL.GetModelList(model.dt, out cycdirnetlist, out mssg))
            {
                model.cycdirnetlist = cycdirnetlist;
                return true;
            }
            return false;
        }

        public class ProcessDataTableIntoModelListModel
        {
            public DataTable dt { get; set; }
            public List<global::TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist { get; set; }
            public ProcessDataTableIntoModelListModel(DataTable dt)
            {
                this.dt = dt;
            }
        }

        public bool ProcessModelListIntoDataTable(ProcessModelListIntoDataTableModel model, out string mssg)
        {
            DataTable dt = null;
            if (cycdirnetBLL.GetTable(model.xmname, model.cycdirnetlist, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessModelListIntoDataTableModel
        {
            public string xmname { get; set; }
            public List<global::TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist { get; set; }
            public DataTable dt { get; set; }
            public ProcessModelListIntoDataTableModel(string xmname, List<global::TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist)
            {
                this.xmname = xmname;
                this.cycdirnetlist = cycdirnetlist;
            }
        }



        public bool ProcessSinglepointcycload(SinglepointcycloadCondition model, out string mssg)
        {
            mssg = "";
            DataTable dt = new DataTable();
            if (cycdirnetBLL.Singlepointcycload(model.xmname, model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }



        public bool ProcesssSinglecycdirnetdataload(SinglecycdirnetdataloadCondition model, out string mssg)
        {
            mssg = "";
            DataTable dt = new DataTable();
            if (cycdirnetBLL.Singlecycdirnetdataload(model.xmname, model.pointname, model.startcyc, model.endcyc, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 获取项目结果数据的最大日期
        /// </summary>
        /// <returns></returns>
        public bool ProcessXmLastDate(ProcessXmLastDateModel model, out string mssg)
        {
            string dateTime = "";
            if (cycdirnetBLL.XmLastDate(model.xmname, out dateTime, out mssg))
            {
                model.dateTime = dateTime;
                return true;
            }
            return false;
        }
        public class ProcessXmLastDateModel
        {
            public string xmname { get; set; }
            public string dateTime { get; set; }
            public ProcessXmLastDateModel(string xmname)
            {
                this.xmname = xmname;
            }
        }

        public bool ProcessResultDataSearch(ProcessResultDataTimeSearchModel model, out string mssg)
        {
            DataTable dt = null;
            if (cycdirnetBLL.ResultdataSearch(model.xmname, model.pointName, model.timestart, model.timeend, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }

        public class ProcessResultDataTimeSearchModel
        {

            public string xmname { get; set; }
            public string pointName { get; set; }
            public DateTime timestart { get; set; }
            public DataTable dt { get; set; }
            public DateTime timeend { get; set; }
            public ProcessResultDataTimeSearchModel(string xmname, string pointName, DateTime timestart, DateTime timeend)
            {
                this.xmname = xmname;
                this.pointName = pointName;
                this.timestart = timestart;
                this.timeend = timeend;
            }
        }
        /// <summary>
        /// 点名列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessTotalStationPointLoad(TotalStationPointLoadCondition model, out string mssg)
        {
            List<string> ls = null;
            if (cycdirnetBLL.TotalStationPointLoadBLL(model.xmno, out ls, out mssg))
            {


                List<string> lstmp = null;
                
                model.ls =  Tool.com.StringHelper.SameExSort(ls,out lstmp)?lstmp: ls;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessPointNameCycListLoad(PointNameCycListCondition model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (cycdirnetBLL.PointNameCycListGet(model.xmname, model.pointname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessPointNewestDateTimeGet(PointNewestDateTimeCondition model, out string mssg)
        {
            DateTime dt = new DateTime();
            if (cycdirnetBLL.PointNewestDateTimeGet(model.xmname, model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        //public class ProcessPointNewestDateTimeGet
        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(IsTypeDataModel model, out string mssg)
        {
            return cycdirnetBLL.IsInsertData(model.xmname, model.dt, out mssg);

        }
        public class IsInsertDataModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public IsInsertDataModel(string xmname, DateTime dt)
            {
                this.xmname = xmname;
                this.dt = dt;
            }
        }
        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(InsertDataCycGetModel model, out string mssg)
        {
            int cyc = -1;
            if (cycdirnetBLL.InsertDataCycGet(model.xmname, model.dt, out cyc, out mssg))
            {
                model.cyc = cyc;
                return true;
            }
            return false;
        }
        public class InsertDataCycGetModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public int cyc { get; set; }
            public InsertDataCycGetModel(string xmname, DateTime dt)
            {
                this.xmname = xmname;
                this.dt = dt;
            }
        }

        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(IsInsertCycExistModel model, out string mssg)
        {
            return cycdirnetBLL.IsInsertCycExist(model.xmname, model.cyc, out mssg);
        }
        public class IsInsertCycExistModel
        {
            public string xmname { get; set; }
            public int cyc { get; set; }
            public IsInsertCycExistModel(string xmname, int cyc)
            {
                this.xmname = xmname;
                this.cyc = cyc;
            }
        }


        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool InsertCycStep(InsertCycStepModel model, out string mssg)
        {
            return cycdirnetBLL.InsertCycStep(model.xmname, model.startcyc, out mssg);
        }

        public class InsertCycStepModel
        {
            public string xmname { get; set; }
            public int startcyc { get; set; }
            public InsertCycStepModel(string xmname, int startcyc)
            {
                this.xmname = xmname;
                this.startcyc = startcyc;
            }
        }

        public bool DeleteCgDataCycOnChain(InsertCycStepModel model, out string mssg)
        {
            return cycdirnetBLL.DeleteCgDataCycOnChain(model.xmname, model.startcyc, out mssg);
        }




        /// <summary>
        /// 判断记录是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool IsCycdirnetDataExist(IsCycdirnetDataExistModel model, out string mssg)
        {
            int cyc = -1;
            if (cycdirnetBLL.IsCycdirnetDataExist(model.xmname, model.pointname, model.dt, out cyc, out mssg))
            {
                model.cyc = cyc;
                return true;
            }
            return false;


        }

        public class IsCycdirnetDataExistModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public DateTime dt { get; set; }
            public int cyc { get; set; } 
            public IsCycdirnetDataExistModel(string xmname, string pointname, DateTime dt)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.dt = dt;
            }
        }

        public bool CgCycList(CgCycListModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (cycdirnetBLL.CgCycList(model.xmname, out ls, out mssg))
            {
                model.cyclist = ls;
                return true;
            }
            return false;
        }

        public class CgCycListModel
        {
            public string xmname { get; set; }
            public List<string> cyclist { get; set; }
            public CgCycListModel(string xmname)
            {
                this.xmname = xmname;
            }
        }
        public bool SurveyCycList(SurveyCycListModel model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (cycdirnetBLL.SurveyCycList(model.xmname, out ls, out mssg))
            {
                model.cyclist = ls;
                return true;
            }
            return false;
        }
        public class SurveyCycListModel
        {
            public string xmname { get; set; }
            public List<string> cyclist { get; set; }
            public SurveyCycListModel(string xmname)
            {
                this.xmname = xmname;
            }
        }
        public bool ProcessDateTimeCycGet(ProcessDateTimeCycGetModel model, out string mssg)
        {
            int cyc = 0;
            if (cycdirnetBLL.CycdirnetDateTimeCycGet(model.xmname, model.dt, out cyc, out mssg))
            {
                model.cyc = cyc;
                return true;
            }
            return false;
        }
        public class ProcessDateTimeCycGetModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public int cyc { get; set; }
            public ProcessDateTimeCycGetModel(string xmname, DateTime dt)
            {
                this.xmname = xmname;
                this.dt = dt;
            }
        }



        public class IsTypeDataModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public IsTypeDataModel(string xmname, DateTime dt)
            {
                this.xmname = xmname;
                this.dt = dt;
            }
        }

        public bool SiblingDataCycGet(DataCycGetModel model, out string mssg)
        {
            int cyc = -1;
            if (cycdirnetBLL.SiblingDataCycGet(model.xmname, model.dt, model.points, model.mobileStationInteval, out cyc,0 ,out mssg))
            {
                model.cyc = cyc;
                return true;
            }
            return false;
        }
        public class DataCycGetModel
        {
            public string xmname { get; set; }
            public DateTime dt { get; set; }
            public List<string> points { get; set; }
            public double mobileStationInteval { get; set; }
            public int cyc { get; set; }
            public DataCycGetModel(string xmname, DateTime dt, List<string> points, double mobileStationInteval)
            {
                this.xmname = xmname;
                this.dt = dt;
                this.points = points;
                this.mobileStationInteval = mobileStationInteval;
            }
        }

        public bool IsPointsDataExistInThisCyc(IsPointsDataExistInThisCycModel model, out string mssg)
        {
            return cycdirnetBLL.IsPointsDataExistInThisCyc(model.xmname, model.points, model.cyc, out mssg);
        }
        public class IsPointsDataExistInThisCycModel
        {
            public string xmname { get; set; }
            public List<string> points { get; set; }
            public int cyc { get; set; }
            public IsPointsDataExistInThisCycModel(string xmname, List<string> points, int cyc)
            {
                this.xmname = xmname;
                this.points = points;
                this.cyc = cyc;
            }
        }
        public bool ProcessPointOnLineCont(PointOnLineContCondition model, out string mssg)
        {
            string contpercent = "";
            if (cycdirnetBLL.OnLinePointCont(model.unitname,model.finishedxmnolist ,out contpercent, out mssg))
            {
                model.contpercent = contpercent;
                return true;
            }
            return false;
        }

        public bool ProcessReportDataView(ProcessReportDataViewModel model, out string mssg)
        {
            DataTable dt = null;
            if (cycdirnetBLL.ReportDataView(model.cyclist,model.pageIndex,model.rows, model.xmname,model.sord ,out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessReportDataViewModel : ResultDataLoadCondition
        {
            public List<string> cyclist { get; set; }
            public ProcessReportDataViewModel(List<string> cyclist, int pageIndex, int pageSize, string xmname, string sord)
            {
                this.cyclist = cyclist;
                this.pageIndex = pageIndex;
                this.rows = pageSize;
                this.xmname = xmname;
                this.sord = sord;
            }
        }


        public bool GetSurveyDataLackPointsModellistFromCgData(GetSurveyDataLackPointsModellistFromCgDataModel model, out string mssg)
        {
            List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist = null;
            if (cycdirnetBLL.GetSurveyDataLackPointsModellistFromCgData(model.xmno, model.pointnamelist, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;

        }
        public class GetSurveyDataLackPointsModellistFromCgDataModel
        {
            public int xmno { get; set; }
            public List<string> pointnamelist { get; set; }
            public List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist { get; set; }
            public GetSurveyDataLackPointsModellistFromCgDataModel(int xmno, List<string> pointnamelist)
            {
                this.xmno = xmno;
                this.pointnamelist = pointnamelist;
            }
            

        }

        public bool GetCgTimeSortModellist(GetCgTimeSortModellistModel model, out string mssg)
        {
            List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist = null;
            if (cycdirnetBLL.GetCgTimeSortModellist(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;

        }
        public class GetCgTimeSortModellistModel
        {
            public int xmno { get; set; }
            public List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist { get; set; }
            public GetCgTimeSortModellistModel(int xmno)
            {
                this.xmno = xmno;
            }


        }

        public bool GetSurveyCYCModellist(GetSurveyCYCModellistModel model, out string mssg)
        {
            List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist = null;
            if (cycdirnetBLL.GetSurveyCYCModellist(model.xmno,model.cyc ,out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;

        }
        public class GetSurveyCYCModellistModel
        {
            public int xmno { get; set; }
            public int cyc { get; set; }
            public List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist { get; set; }
            public GetSurveyCYCModellistModel(int xmno, int cyc)
            {
                this.xmno = xmno;
                this.cyc = cyc;
            }


        }


        public bool ProcessSurveyDataGetTimeFromCyc(ProcessSurveyDataGetTimeFromCycModel model,out string mssg)
        {
            DateTime time = new DateTime();
            if (cycdirnetBLL.SurveyDataCycTimeGet(model.xmno,model.cyc ,out time, out mssg))
            {
                model.time = time;
                return true;
            }
            return false;
        }
        public class ProcessSurveyDataGetTimeFromCycModel
        {
            public int xmno { get; set; }
            public int cyc { get; set; }
            public DateTime time { get; set; }
            public ProcessSurveyDataGetTimeFromCycModel(int xmno,int cyc)
            {
                this.xmno = xmno;
                this.cyc = cyc;
            }
        }

        




    }
}