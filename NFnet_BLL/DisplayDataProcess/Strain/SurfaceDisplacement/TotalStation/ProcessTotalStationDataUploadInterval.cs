﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.Strain.SurfaceDisplacement.totalstation
{
    public class ProcessTotalStationDataUploadInterval
    {
        public TotalStation.BLL.fmos_obj.totalstationdatauploadinterval bll = new TotalStation.BLL.fmos_obj.totalstationdatauploadinterval();
        public bool Add(TotalStation.Model.fmos_obj.totalstationdatauploadinterval model,out string mssg)
        {
            return bll.Add(model,out mssg);
        }

        public bool TableLoad(TableLoadModel model,out string mssg )
        {
            DataTable dt = null;
            if (bll.TableLoad(model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class TableLoadModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public DataTable dt { get; set; }
            public TableLoadModel(string xmname,int xmno)
            {
                this.xmname = xmname;
                this.xmno = xmno;
            }
        }
    }
}