﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;
using System.Data;
using NFnet_BLL.Other;
using NFnet_BLL.DataProcess;
using Tool;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 全站仪曲线表业务逻辑处理类
    /// </summary>
    public class ProcessTotalStationChartBLL
    {
        #region 表面位移
        /// <summary>
        /// 表面位移曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrBMWY(SerializestrBMWYCondition model, out string mssg)
        {
            mssg = "";
            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            string sql = model.sql;
            string pointname = model.pointname;
            zuxyz[] zus = model.zus;
            string xmname = model.xmname;
            if (pointname != "" && pointname != null && zus != null)
            {

                var processquerystanderdbModel = new QuerystanderdbModel(sql, xmname);
                if (!ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
                {
                    //错误信息反馈
                }
                dt = processquerystanderdbModel.dt;
                //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[pointnamezu.Length];
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
                for (int k = 0; k < pointnamezu.Length; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(pointnamezu[k], dvzu[k]);
                }
                List<serie> lst = new List<serie>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < pointnamezu.Length; d++)
                    {
                        string filter =
                        dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d];
                        int cont = dvzu[d].Count;
                        //if (cont != 0)
                        //{

                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie st = new serie();//创建曲线
                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = pointnamezu[d].Replace("'", "") + "_" + rplname(zus[z].Bls[u]);
                            switchnez(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                        //}

                    }



                }
                model.series = lst;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 重命名分量标签
        /// </summary>
        /// <param name="blm"></param>
        /// <returns></returns>
        public static string rplname(string blm)
        {
            if (blm.IndexOf("This_dE") != -1)
            {
                return "△E";
            }
            else if (blm.IndexOf("This_dN") != -1)
            {
                return "△N";
            }
            else if (blm.IndexOf("This_dZ") != -1)
            {
                return "△Z";
            }
            else if (blm.IndexOf("Ac_dE") != -1)
            {
                return "∑△E";
            }
            else if (blm.IndexOf("Ac_dN") != -1)
            {
                return "∑△N";
            }
            else if (blm.IndexOf("Ac_dZ") != -1)
            {
                return "∑△Z";
            }
            return blm;

        }
        /// <summary>
        /// 由数据表生成曲线组
        /// </summary>
        /// <param name="dv">结果数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量名称</param>
        public static void switchnez(DataView dv, serie st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_N");
                        i++;
                    }
                }
                else if (xyzes == "Avg_E")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_E");
                        i++;
                    }
                }
                else if (xyzes == "Avg_Z")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_Z");
                        i++;
                    }
                }


            }

        }
        /// <summary>
        /// 数据行生成曲线数据点
        /// </summary>
        /// <param name="drv"></param>
        /// <param name="sxtj"></param>
        /// <returns></returns>
        public static pt wgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string dtime = drv["time"].ToString();
            DateTime d = Convert.ToDateTime(dtime);
            int year = d.Year;
            int mon = d.Month;
            int day = d.Day;
            int hour = d.Hour;
            int minute = d.Minute;
            int second = d.Second;
            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt { Dt = d, Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        #endregion
        #region 表面位移周期
        /// <summary>
        /// 以周期作为横轴的表面位移曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrBMWY_cyc(SerializestrBMWYC_CYCondition model, out string mssg)
        {

            //  conn = db.getsqldbconn();
            mssg = "";
            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            string sql = model.sql;
            string pointname = model.pointname;
            zuxyz[] zus = model.zus;
            string xmname = model.xmname;
            if (pointname != "" && pointname != null && zus != null)
            {

                var processquerystanderdbModel = new QuerystanderdbModel(sql, xmname);
                if (!ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
                {
                    //错误信息反馈
                }
                dt = processquerystanderdbModel.dt;
                List<string> lsCycTerminal = model.terminalCyc;
                string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[pointnamezu.Length];
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
                for (int k = 0; k < pointnamezu.Length; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(pointnamezu[k], dvzu[k]);
                }
                List<serie_cyc_null> lst = new List<serie_cyc_null>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < pointnamezu.Length; d++)
                    {
                        string filter =
                        dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d];
                        int cont = dvzu[d].Count;
                        List<int> lackCyc = new List<int>();
                        //校验记录数是否等于周期数
                        if (cont != Math.Abs(Int32.Parse(lsCycTerminal[0]) - Int32.Parse(lsCycTerminal[1])) + 1)
                        {
                            //查出少的周期进行补偿
                            int l = 0;
                            for (l = Int32.Parse(lsCycTerminal[0]); l <= Int32.Parse(lsCycTerminal[1]); l++)
                            {
                                DataView temp = dvzu[d];
                                temp.RowFilter = "cyc='" + l + "' and  " + " POINT_NAME= " + pointnamezu[d];
                                if (temp == null || temp.Count == 0)
                                    lackCyc.Add(l);
                            }
                            dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d];
                        }
                        //if (cont != 0)
                        //{
                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie_cyc_null st = new serie_cyc_null();//创建曲线
                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = pointnamezu[d].Replace("'", "") + "_" + rplname(zus[z].Bls[u]);
                            if (lackCyc.Count != 0)
                                switchnez_cyc_null(dvzu[d], st, zus[z].Bls[u], lackCyc);
                            else
                                switchnez_cyc_null(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                        //}

                    }



                }
                model.serie_cycs = lst;
                //string result = jss.Serialize(lst);
                //long ln = result.Length;
                //return result;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 由数据表视图生成曲线组
        /// </summary>
        /// <param name="dv">数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量</param>
        public static void switchnez_cyc_null(DataView dv, serie_cyc_null st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_cyc_null[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_null(drv, "Avg_N");
                        i++;
                    }
                }



            }

        }
        /// <summary>
        /// 获取最大和最小周期
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<string> TerminalCycGet(HttpContext context)
        {

            List<string> ls = new List<string>();
            ls.Add(context.Session["minCyc"].ToString());
            ls.Add(context.Session["maxCyc"].ToString());
            return ls;
        }
        /// <summary>
        ///  由数据表视图生成曲线组
        /// </summary>
        /// <param name="dv">数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量</param>
        /// <param name="lackCyc">补偿的周期</param>
        public static void switchnez_cyc_null(DataView dv, serie_cyc_null st, string xyzes, List<int> lackCyc)
        {
            List<int> tempCyc = new List<int>(lackCyc.ToArray());
            int len = dv.Count + lackCyc.Count;
            int i = 0, k = 0;
            st.Pts = new pt_cyc_null[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    serie_cyc_create(dv, len, tempCyc, "This_dE", st);
                }
                else if (xyzes == "This_dN")//Y
                {
                    serie_cyc_create(dv, len, tempCyc, "This_dN", st);
                }
                else if (xyzes == "This_dZ")//Z
                {
                    serie_cyc_create(dv, len, tempCyc, "This_dZ", st);
                }
                else if (xyzes == "Ac_dN")//X
                {
                    serie_cyc_create(dv, len, tempCyc, "Ac_dN", st);
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    serie_cyc_create(dv, len, tempCyc, "Ac_dE", st);
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    serie_cyc_create(dv, len, tempCyc, "Ac_dZ", st);
                }



            }

        }
        /// <summary>
        /// 曲线创建
        /// </summary>
        /// <param name="dv"></param>
        /// <param name="len"></param>
        /// <param name="tempCyc"></param>
        /// <param name="bls"></param>
        /// <param name="st"></param>
        public static void serie_cyc_create(DataView dv, int len, List<int> tempCyc, string bls, serie_cyc_null st)
        {
            int i = 0, k = 0, t = 0;

            foreach (DataRowView drv in dv)
            {
                st.Pts[i] = new pt_cyc_null();
                //if(lackCyc.)
                int cycIndex = Int32.Parse(drv["cyc"].ToString());
                tempCyc.Add(cycIndex);
                tempCyc.Sort();
                if (tempCyc.IndexOf(cycIndex) == 0)
                {
                    st.Pts[i] = cycwgformat_null(drv, bls);
                    tempCyc.Remove(cycIndex);
                    i++;
                }
                else
                {
                    LackCycMakeUp(tempCyc, cycIndex, bls, st, ref i, drv);
                }
            }
            while (tempCyc.Count > 0)
            {
                st.Pts[i] = new pt_cyc_null { Pt_x = tempCyc[0].ToString(), Yvalue1 = null };
                i++;
                tempCyc.RemoveAt(0);
            }

        }
        /// <summary>
        /// 缺失周期补偿
        /// </summary>
        /// <param name="tempCyc"></param>
        /// <param name="cycIndex"></param>
        /// <param name="bls"></param>
        /// <param name="st"></param>
        /// <param name="i"></param>
        /// <param name="drv"></param>
        public static void LackCycMakeUp(List<int> tempCyc, int cycIndex, string bls, serie_cyc_null st, ref int i, DataRowView drv)
        {
            //tempCyc.Add(cycIndex);
            int k = 0, t = 0;
            for (k = 0; k < tempCyc.IndexOf(cycIndex); k++)
            {
                st.Pts[i] = new pt_cyc_null { Pt_x = tempCyc[k].ToString(), Yvalue1 = null };
                i++;

            }
            st.Pts[i] = cycwgformat_null(drv, bls);
            i++;
            for (t = 0; t <= k; t++)
            {
                tempCyc.RemoveAt(0);

            }



        }

        //周期格式化时间允许为空
        public static pt_cyc_null cycwgformat_null(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = drv["cyc"].ToString();
            return new pt_cyc_null { Pt_x = string.Format("第{0}周期 {1}", pt_x, drv["time"].ToString()), Yvalue1 = drv[sxtj].ToString() };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }

        #endregion
        #region 表面位移多点单周期
        /// <summary>
        /// 以点名作为横轴的曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrBMWY_point(SerializestrBMWY_POINTCondition model, out string mssg)
        {

            int seriescont = 0;

            mssg = "";
            try
            {

                DataTable dt = new DataTable();
                DataTable dtt = new DataTable();
                string sql = model.sql;
                string pointname = model.pointname;
                zuxyz[] zus = model.zus;
                string xmname = model.xmname;
                if (pointname != "" && pointname != null && zus != null)
                {
                    List<string> columnnames = "point_name,cyc,n,e,z,this_dn, this_de,this_dz,  ac_dn, ac_de, ac_dz,time,siblingpoint_name".Split(',').ToList();
                    model.sql = sql;
                    var processquerystanderdbModel = new QuerystanderdbModel(sql, model.xmname, columnnames);

                    if (!ProcessComBLL.ProcessquerystanderdbInreader(processquerystanderdbModel, out mssg))
                    {
                        //错误信息反馈
                    }
                    dt = processquerystanderdbModel.dt;
                    DataView dvdefault = new DataView(dt);
                    DataTable dtdistinct = dvdefault.ToTable(true, "cyc");
                    //从表中筛选出周期
                    List<string> lscyc = CycListGet(dtdistinct);

                    //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                    //string[] pointnamezu = pointname.Split(',');
                    DataView[] dvzu = new DataView[lscyc.Count];

                    Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();

                    for (int k = 0; k < lscyc.Count; k++)
                    {
                        dvzu[k] = new DataView(dt);
                        ddzu.Add(lscyc[k], dvzu[k]);
                    }
                    List<serie_point> lst = new List<serie_point>();
                    for (int z = 0; z < zus.Length; z++)
                    {
                        if (seriescont > model.rows * model.pageIndex - 1) { ExceptionLog.ExceptionWrite(seriescont + ">" + model.rows * model.pageIndex); continue; }
                        for (int d = 0; d < lscyc.Count; d++)
                        {

                            string filter =
                            dvzu[d].RowFilter = "cyc= " + lscyc[d];
                            int cont = dvzu[d].Count;
                            //if (cont != 0)
                            //{
                            ExceptionLog.ExceptionWrite(seriescont + "<" + model.rows * (model.pageIndex - 1));
                            if (seriescont > model.rows * model.pageIndex - 1) { ExceptionLog.ExceptionWrite(seriescont + ">" + model.rows * model.pageIndex); break; }
                            else if (seriescont < model.rows * (model.pageIndex - 1)) { seriescont++; continue; }
                            for (int u = 0; u < zus[z].Bls.Length; u++)
                            {
                                serie_point st = new serie_point();//创建曲线
                                st.Stype = zus[z].Name;//曲线类别
                                st.Name = lscyc[d].Replace("'", "") + "_" + rplname(zus[z].Bls[u]);
                                switchnez_point(dvzu[d], st, zus[z].Bls[u]);
                                lst.Add(st);
                            }


                            seriescont++;
                        }
                        seriescont = 0;
                    }




                    model.serie_points = lst;

                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("曲线绘制出错，错误信息：" + ex.Message + "位置:" + ex.StackTrace);
                return false;
            }

        }





        /// <summary>
        /// 周期列表初始化
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<string> CycListGet(DataTable dt)
        {
            DataView dv = new DataView(dt);
            dv.Sort = " cyc asc ";
            List<int> ls = new List<int>();
            
            foreach (DataRowView drv in dv)
            {
                if (drv["cyc"] != null&& drv["cyc"].ToString() != "")
                ls.Add(Convert.ToInt32(drv["cyc"].ToString()));
            }
            
            ExceptionLog.ExceptionWrite(string.Join(",", ls));
            ls.Sort();
            return ls.Select(m => m.ToString()).ToList();
        }
        /// <summary>
        /// 有数据表生成曲线组
        /// </summary>
        /// <param name="dv">数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量名称</param>
        public static void switchnez_point(DataView dv, serie_point st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_point[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Avg_N");
                        i++;
                    }
                }
                else if (xyzes == "Avg_E")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Avg_E");
                        i++;
                    }
                }
                else if (xyzes == "Avg_Z")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_point();
                        st.Pts[i] = pointwgformat(drv, "Avg_Z");
                        i++;
                    }
                }


            }

        }
        /// <summary>
        /// 数据行生成
        /// </summary>
        /// <param name="drv"></param>
        /// <param name="sxtj"></param>
        /// <returns></returns>
        public static pt_point pointwgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = drv["cyc"].ToString();

            float val =drv[sxtj] == null||drv[sxtj].ToString() == ""? 0:(float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt_point { PointName = string.Format("{0} {1}", drv["point_name"].ToString(), drv["time"].ToString()), Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        #endregion
        #region 表面位移周期-横轴显示周期和日期
        public static ProcessResultDataBLL resultdataBLL = new ProcessResultDataBLL();
        /// <summary>
        /// 以周期作为横轴的表面位移曲线序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrBMWY_single_cyc(SerializestrBMWYC_SINGLE_CYCondition model, out string mssg)
        {

            //  conn = db.getsqldbconn();
            mssg = "";
            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            string pointname = model.pointname;
            zuxyz[] zus = model.zus;
            string xmname = model.xmname;
            if (pointname != "" && pointname != null && zus != null)
            {


                var singlecycdirnetdataloadCondition = new SinglecycdirnetdataloadCondition(xmname, pointname, model.startcyc, model.endcyc);
                if (!resultdataBLL.ProcesssSinglecycdirnetdataload(singlecycdirnetdataloadCondition, out mssg))
                {

                }
                dt = singlecycdirnetdataloadCondition.dt;
                List<string> lsCycTerminal = new List<string>();
                lsCycTerminal.Add(model.startcyc.ToString());
                lsCycTerminal.Add(model.endcyc.ToString());
                string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[pointnamezu.Length];
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
                for (int k = 0; k < pointnamezu.Length; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(pointnamezu[k], dvzu[k]);
                }
                List<serie_cyc_null> lst = new List<serie_cyc_null>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < pointnamezu.Length; d++)
                    {
                        string filter =
                        dvzu[d].RowFilter = "POINT_NAME= '" + pointnamezu[d] + "'";
                        int cont = dvzu[d].Count;
                        List<int> lackCyc = new List<int>();
                        //校验记录数是否等于周期数
                        if (cont != Math.Abs(Int32.Parse(lsCycTerminal[0]) - Int32.Parse(lsCycTerminal[1])) + 1)
                        {
                            //查出少的周期进行补偿
                            int l = 0;
                            for (l = Int32.Parse(lsCycTerminal[0]); l <= Int32.Parse(lsCycTerminal[1]); l++)
                            {
                                DataView temp = dvzu[d];
                                temp.RowFilter = "cyc='" + l + "' and  " + " POINT_NAME= '" + pointnamezu[d] + "'";
                                if (temp == null || temp.Count == 0)
                                    lackCyc.Add(l);
                            }
                            dvzu[d].RowFilter = "POINT_NAME= '" + pointnamezu[d] + "'";
                        }
                        //if (cont != 0)
                        //{
                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie_cyc_null st = new serie_cyc_null();//创建曲线
                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = pointnamezu[d].Replace("'", "") + "_" + rplname(zus[z].Bls[u]);
                            if (lackCyc.Count != 0)
                                switchnez_single_cyc_null(dvzu[d], st, zus[z].Bls[u], lackCyc);
                            else
                                switchnez_single_cyc_null(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                        //}

                    }



                }
                model.serie_cycs = lst;
                //string result = jss.Serialize(lst);
                //long ln = result.Length;
                //return result;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 由数据表视图生成曲线组
        /// </summary>
        /// <param name="dv">数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量</param>
        public static void switchnez_single_cyc_null(DataView dv, serie_cyc_null st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            st.Pts = new pt_cyc_null[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_single_null(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_single_null(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_single_null(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_single_null(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_single_null(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_single_null(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;

                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt_cyc_null();
                        st.Pts[i] = cycwgformat_single_null(drv, "Avg_N");
                        i++;
                    }
                }



            }

        }
        /// <summary>
        /// 获取最大和最小周期
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<string> TerminalCycGet_single(HttpContext context)
        {

            List<string> ls = new List<string>();
            ls.Add(context.Session["minCyc"].ToString());
            ls.Add(context.Session["maxCyc"].ToString());
            return ls;
        }
        /// <summary>
        ///  由数据表视图生成曲线组
        /// </summary>
        /// <param name="dv">数据表</param>
        /// <param name="st">曲线</param>
        /// <param name="xyzes">分量</param>
        /// <param name="lackCyc">补偿的周期</param>
        public static void switchnez_single_cyc_null(DataView dv, serie_cyc_null st, string xyzes, List<int> lackCyc)
        {
            List<int> tempCyc = new List<int>(lackCyc.ToArray());
            int len = dv.Count + lackCyc.Count;
            int i = 0, k = 0;
            st.Pts = new pt_cyc_null[len];
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    serie_single_cyc_create(dv, len, tempCyc, "This_dE", st);
                }
                else if (xyzes == "This_dN")//Y
                {
                    serie_single_cyc_create(dv, len, tempCyc, "This_dN", st);
                }
                else if (xyzes == "This_dZ")//Z
                {
                    serie_single_cyc_create(dv, len, tempCyc, "This_dZ", st);
                }
                else if (xyzes == "Ac_dN")//X
                {
                    serie_single_cyc_create(dv, len, tempCyc, "Ac_dN", st);
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    serie_single_cyc_create(dv, len, tempCyc, "Ac_dE", st);
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    serie_single_cyc_create(dv, len, tempCyc, "Ac_dZ", st);
                }



            }

        }
        /// <summary>
        /// 曲线创建
        /// </summary>
        /// <param name="dv"></param>
        /// <param name="len"></param>
        /// <param name="tempCyc"></param>
        /// <param name="bls"></param>
        /// <param name="st"></param>
        public static void serie_single_cyc_create(DataView dv, int len, List<int> tempCyc, string bls, serie_cyc_null st)
        {
            int i = 0, k = 0, t = 0;

            foreach (DataRowView drv in dv)
            {
                st.Pts[i] = new pt_cyc_null();
                //if(lackCyc.)
                int cycIndex = Int32.Parse(drv["cyc"].ToString());
                tempCyc.Add(cycIndex);
                tempCyc.Sort();
                if (tempCyc.IndexOf(cycIndex) == 0)
                {
                    st.Pts[i] = cycwgformat_single_null(drv, bls);
                    tempCyc.Remove(cycIndex);
                    i++;
                }
                else
                {
                    LackCycMakeUp_single(tempCyc, cycIndex, bls, st, ref i, drv);
                }
            }
            while (tempCyc.Count > 0)
            {
                st.Pts[i] = new pt_cyc_null { Pt_x = tempCyc[0].ToString(), Yvalue1 = null };
                i++;
                tempCyc.RemoveAt(0);
            }

        }
        /// <summary>
        /// 缺失周期补偿
        /// </summary>
        /// <param name="tempCyc"></param>
        /// <param name="cycIndex"></param>
        /// <param name="bls"></param>
        /// <param name="st"></param>
        /// <param name="i"></param>
        /// <param name="drv"></param>
        public static void LackCycMakeUp_single(List<int> tempCyc, int cycIndex, string bls, serie_cyc_null st, ref int i, DataRowView drv)
        {
            //tempCyc.Add(cycIndex);
            int k = 0, t = 0;
            for (k = 0; k < tempCyc.IndexOf(cycIndex); k++)
            {
                st.Pts[i] = new pt_cyc_null { Pt_x = tempCyc[k].ToString(), Yvalue1 = null };
                i++;

            }
            st.Pts[i] = cycwgformat_single_null(drv, bls);
            i++;
            for (t = 0; t <= k; t++)
            {
                tempCyc.RemoveAt(0);

            }



        }

        //周期格式化时间允许为空
        public static pt_cyc_null cycwgformat_single_null(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string pt_x = "第" + drv["cyc"].ToString() + "周期[" + drv["time"].ToString() + "]";
            return new pt_cyc_null { Pt_x = pt_x, Yvalue1 = drv[sxtj].ToString() };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }

        #endregion

        
    }


}