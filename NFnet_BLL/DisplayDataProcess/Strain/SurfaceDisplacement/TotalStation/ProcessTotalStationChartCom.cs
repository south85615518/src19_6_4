﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessTotalStationChartCom
    {
        public static ProcessCgTotalStationChartBLL cgTotalStationChartBLL = new ProcessCgTotalStationChartBLL();
        public static ProcessTotalStationChartBLL totalStationChartBLL = new ProcessTotalStationChartBLL();

        public static bool ProcessSerializestrBMWY(ProcessSerializestrBMWYModel model, out string mssg)
        {
            
                return ProcessTotalStationChartBLL.ProcessSerializestrBMWY(model.model, out mssg);



        }
        public class ProcessSerializestrBMWYModel
        {
            public SerializestrBMWYCondition model { get; set; }
            public Role role { get; set; }
            public bool tempRole { get; set; }
            public ProcessSerializestrBMWYModel(SerializestrBMWYCondition model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }
        }

        public static bool ProcessSerializestrCYCBMWY(ProcessSerializestrCYCBMWYModel model, out string mssg)
        {
            
                return ProcessTotalStationChartBLL.ProcessSerializestrBMWY_cyc(model.model, out mssg);



        }
        public class ProcessSerializestrCYCBMWYModel
        {
            public SerializestrBMWYC_CYCondition model { get; set; }
            public Role role { get; set; }
            public bool tempRole { get; set; }
            public ProcessSerializestrCYCBMWYModel(SerializestrBMWYC_CYCondition model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }
        }


        public static bool ProcessSerializestrSINGLECYCBMWY(ProcessSerializestrSINGLECYCBMWYModel model, out string mssg)
        {
            
                return ProcessTotalStationChartBLL.ProcessSerializestrBMWY_single_cyc(model.model, out mssg);



        }
        public class ProcessSerializestrSINGLECYCBMWYModel
        {
            public SerializestrBMWYC_SINGLE_CYCondition model { get; set; }
            public Role role { get; set; }
            public ProcessSerializestrSINGLECYCBMWYModel(SerializestrBMWYC_SINGLE_CYCondition model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }



        public static bool ProcessSerializestrPOINTBMWY(ProcessSerializestrPOINTBMWYModel model, out string mssg)
        {
            
                return ProcessTotalStationChartBLL.ProcessSerializestrBMWY_point(model.model, out mssg);



        }
        public class ProcessSerializestrPOINTBMWYModel
        {
            public SerializestrBMWY_POINTCondition model { get; set; }
            public Role role { get; set; }
            public bool tempRole { get; set; }
            public ProcessSerializestrPOINTBMWYModel(SerializestrBMWY_POINTCondition model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }
        }

       



    }
}