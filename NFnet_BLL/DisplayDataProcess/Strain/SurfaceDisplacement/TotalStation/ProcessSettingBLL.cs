﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.BLL.fmos_obj;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 设置参数业务逻辑处理类
    /// </summary>
    public class ProcessSettingBLL
    {
        public static setting settingBLL = new setting();
        /// <summary>
        /// 设置参数记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcesssettingRecordsCount(ProcesssettingRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (settingBLL.settingTableRowsCount(model.searchpost, model.xmname, out totalCont, out mssg))
            {
                model.tabCount = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 设置参数记录数获取类
        /// </summary>
        public class ProcesssettingRecordsCountModel : TabCountSearchCondition
        {
            /// <summary>
            /// 筛选条件
            /// </summary>
            public string searchpost { get; set; }
            public ProcesssettingRecordsCountModel(string xmname, string searchpost)
            {
                this.xmname = xmname;
                this.searchpost = searchpost;

            }
        }
        /// <summary>
        /// 设置参数表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcesssettingLoad(ProcesssettingLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (settingBLL.settingTableLoad(model.pageIndex, model.rows, model.xmname, model.searchpost, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 设置参数表获取类
        /// </summary>
        public class ProcesssettingLoadModel : SearchCondition
        {
            /// <summary>
            /// 筛选条件
            /// </summary>
            public string searchpost { get; set; }
            /// <summary>
            /// 设置参数表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcesssettingLoadModel(string xmname, string colName, int pageIndex, int rows, string sord, string searchpost)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.searchpost = searchpost;

            }
        }
    }
}