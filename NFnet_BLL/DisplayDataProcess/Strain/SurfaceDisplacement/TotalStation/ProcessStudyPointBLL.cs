﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.BLL;
using TotalStation.BLL.fmos_obj;
using System.Data;
using PointAttribute.BLL;

namespace NFnet_BLL.DataProcess.TotalStation
{
    /// <summary>
    /// 学习点业务逻辑处理类
    /// </summary>
    public class ProcessStudyPointBLL
    {
        public static studypoint studypointBLL = new studypoint();
        public static fmos_pointalarmvalue pointalarmvalueBLL = new fmos_pointalarmvalue();
        /// <summary>
        /// 学习点记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessStudyPointRecordsCount(ProcessStudyPointRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (studypointBLL.StudyPointTableRowsCount(model.xmname, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 学习点记录数获取类
        /// </summary>
        public class ProcessStudyPointRecordsCountModel : SearchCondition
        {
            /// <summary>
            /// 记录数
            /// </summary>
            public string totalCont { get; set; }
            public ProcessStudyPointRecordsCountModel(string xmname, string colName, int pageIndex, int rows, string sord)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }
        /// <summary>
        /// 学习点表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessStudyPointLoad(ProcessStudyPointLoadModel model, out string mssg)
        {
            ///学习点表
            DataTable dt = null;
            if (studypointBLL.StudyPointTableLoad(model.pageIndex, model.rows, model.xmname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 学习点表获取类
        /// </summary>
        public class ProcessStudyPointLoadModel : SearchCondition
        {
            /// <summary>
            /// 学习点表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessStudyPointLoadModel(string xmname, string colName, int pageIndex, int rows, string sord)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
               
            }
        }
      


    }
}