﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation;
using System.Data;
using SqlHelpers;
using NFnet_BLL.Other;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.LoginProcess;


namespace NFnet_BLL.DataProcess
{
   
    /// <summary>
    /// 结果数据业务逻辑处理类
    /// </summary>
    public partial class ProcessResultDataBLL 
    {
        public bool ProcessCgResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out List<global::TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist, out string mssg)
        {

            if (cycdirnetBLL.GetCgModelList(model.xmname, out cycdirnetlist, out mssg))
            {
                model.modellist = cycdirnetlist;
                return true;
            }
            return false;
        }
        public bool ProcessCgDateTimeCycGet(ProcessDateTimeCycGetModel model, out string mssg)
        {
            int cyc = 0;
            if (cycdirnetBLL.CgCycdirnetDateTimeCycGet(model.xmname, model.dt, out cyc, out mssg))
            {
                model.cyc = cyc;
                return true;
            }
            return false;
        }
       
       
  

    }
}