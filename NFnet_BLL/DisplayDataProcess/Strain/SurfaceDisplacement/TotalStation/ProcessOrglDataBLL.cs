﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DataProcess;
using System.Data;
using TotalStation.BLL.fmos_obj;
namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 全站仪原始数据业务逻辑处理类
    /// </summary>
    public class ProcessOrglDataBLL
    {
        public static orgldata orglBLL = new orgldata();
        /// <summary>
        /// 原始数据表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessOrglDataRecordsCount(ProcessOrglDataRecordsCountModel model,out string mssg)
        {
            string totalCont = "0";
            if (orglBLL.OrgldataTableRowsCount(model.startCyc, model.endCyc, model.xmname, model.pointname, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {
                
                return false;
            }

        }
        /// <summary>
        /// 原始数据表记录数获取类
        /// </summary>
        public class ProcessOrglDataRecordsCountModel:SearchCondition
        {
            /// <summary>
            /// 起始周期
            /// </summary>
            public int startCyc { get; set; }
            /// <summary>
            /// 结束周期
            /// </summary>
            public int endCyc { get; set; }
            /// <summary>
            /// 总记录数
            /// </summary>
            public string totalCont { get; set; }
            public ProcessOrglDataRecordsCountModel(string xmname,int pageIndex,int rows,string pointname,int startCyc,int endCyc )
            {
                this.xmname = xmname;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.pointname = pointname;
                this.startCyc = startCyc;
                this.endCyc = endCyc;
            }
        }
        /// <summary>
        /// 原始数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessOrglDataLoad(ProcessOrglDataLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (orglBLL.OrgldataTableLoad(model.startCyc, model.endCyc, model.pageIndex, model.rows, model.xmname, model.pointname,out dt,out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 原始数据表获取类
        /// </summary>
        public class ProcessOrglDataLoadModel : SearchCondition
        {
            /// <summary>
            /// 点名
            /// </summary>
            public string pointname { get; set; }
            /// <summary>
            /// 起始周期
            /// </summary>
            public int startCyc { get; set; }
            /// <summary>
            /// 结束周期
            /// </summary>
            public int endCyc { get; set; }
            /// <summary>
            /// 原始数据表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessOrglDataLoadModel(string xmname,  int pageIndex, int rows, string pointname, int startCyc, int endCyc)
            {
                this.xmname = xmname;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.pointname = pointname;
                this.startCyc = startCyc;
                this.endCyc = endCyc;
            }
        }
        /// <summary>
        /// 原始数据查询周期条件初始化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessOrglDataCycLoad(CycLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";
            if (orglBLL.CycList(model.xmname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }



        public bool ProcessPointNameCycListLoad(PointNameCycListCondition model, out string mssg)
        {
            List<string> ls = new List<string>();
            if (orglBLL.PointNameCycListGet(model.xmname, model.pointname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }

        public bool ProcessPointNameCycListLoad(string xmname, string pointname, out List<string> ls, out string mssg)
        {
            ls = new List<string>();
            var pointNameCycListCondition = new PointNameCycListCondition(xmname, pointname);
          
                if (ProcessPointNameCycListLoad(pointNameCycListCondition, out mssg))
                {
                    ls = pointNameCycListCondition.ls;
                    return true;
                }
                return false;
            
        }



        public static TotalStationCgDAL.BLL.cycdirnet cycdirnetBLL = new TotalStationCgDAL.BLL.cycdirnet();
        /// <summary>
        /// 点名列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessTotalStationPointLoad(TotalStationPointLoadCondition model, out string mssg)
        {
            List<string> ls = null;
            if (cycdirnetBLL.TotalStationPointLoadDAL(model.xmno, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
      


    }
}