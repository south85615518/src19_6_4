﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.BLL.fmos_obj;
using NFnet_BLL.DataProcess;
using System.Data;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 全站仪仪器业务逻辑处理类
    /// </summary>
    public class ProcessInstrumentparamBLL
    {
        public static fmos_instrumentparam instrumentparamBLL = new fmos_instrumentparam();
        /// <summary>
        /// 仪器表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessinstrumentparamRecordsCount(ProcessinstrumentparamRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (instrumentparamBLL.InstrumentParamTableRowsCount(model.searchpost,model.xmno,model.xmname, out totalCont, out mssg))
            {
                model.tabCount = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 仪器表记录数获取类
        /// </summary>
        public class ProcessinstrumentparamRecordsCountModel : TabCountSearchCondition
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 搜索参数
            /// </summary>
            public string searchpost { get; set; }
            public ProcessinstrumentparamRecordsCountModel(string xmname,int xmno,string searchpost)
            {
                this.xmname = xmname;
                this.searchpost = searchpost;
                this.xmno = xmno;
            }
        }
        /// <summary>
        /// 仪器参数表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessinstrumentparamLoad(ProcessinstrumentparamLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (instrumentparamBLL.InstrumentParamTableLoad(model.pageIndex, model.rows,model.xmno ,model.xmname,model.searchpost ,model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 仪器参数表获取类
        /// </summary>
        public class ProcessinstrumentparamLoadModel : SearchCondition
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 搜索条件
            /// </summary>
            public string searchpost { get; set; }
            /// <summary>
            /// 仪器表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessinstrumentparamLoadModel(string xmname, string colName, int pageIndex, int rows, string sord,int xmno,string searchpost)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.xmno = xmno;
                this.searchpost = searchpost;

            }
        }

        public bool ProcessInstrumentparamModel(ProcessInstrumentparamModelGetModel model,out string mssg)
        {
            object obj = new object();
            if (instrumentparamBLL.GetModel(model.xmno,model.name, out obj, out mssg))
            {
                model.obj = obj;
            }
            return false;
        }
        public class ProcessInstrumentparamModelGetModel
        {
            public int xmno { get; set; }
            public string name { get; set; }
            public object obj { get; set; }
            public ProcessInstrumentparamModelGetModel(int xmno,string name)
            {
                this.xmno = xmno;
                this.name = name;
            }
        }

      

 
    }
}