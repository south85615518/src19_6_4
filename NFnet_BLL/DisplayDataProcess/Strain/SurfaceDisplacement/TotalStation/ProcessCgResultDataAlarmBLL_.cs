﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public class ProcessCgResultDataAlarmBLL
    {
        
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
        public static ProcessCgResultDataBLL resultBLL = new ProcessCgResultDataBLL();
       

        public bool ProcessPointAlarmValue(ProcessPointAlarmValueModel model, out string mssg)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(model.xmno, model.pointName);

            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                model.model = processPointAlarmModelGetModel.model;
                return true;
            }
            return false;
        }

        public class ProcessPointAlarmValueModel
        {
            public int xmno { get; set; }
            public string pointName { get; set; }
            public PointAttribute.Model.fmos_pointalarmvalue model { get; set; }
            public ProcessPointAlarmValueModel(int xmno, string pointName)
            {
                this.xmno = xmno;
                this.pointName = pointName;

            }
        }

        public bool ProcessAlarmValueList(ProcessAlarmValueListModel model, out string mssg)
        {
            List<NFnet_DAL.MODEL.alarmvalue> alarmvalueList = new List<NFnet_DAL.MODEL.alarmvalue>();
            //一级
            var processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmname, model.pointalarm.FirstAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmname, model.pointalarm.SecondAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmname, model.pointalarm.ThirdAlarmName);
            if (alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                alarmvalueList.Add(processAlarmModelGetByNameModel.model);
            }
            if (alarmvalueList.Count == 0) return false;
            model.alarmvalueList = alarmvalueList;
            return true;
        }
        public class ProcessAlarmValueListModel
        {
            public PointAttribute.Model.fmos_pointalarmvalue pointalarm { get; set; }
            public List<NFnet_DAL.MODEL.alarmvalue> alarmvalueList { get; set; }
            public string xmname { get; set; }
            public ProcessAlarmValueListModel(string xmname, PointAttribute.Model.fmos_pointalarmvalue pointalarm)
            {
                this.xmname = xmname;
                this.pointalarm = pointalarm;
            }
        }

        public void ProcessPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(model.resultModel.TaskName, model.levelalarmvalue, model.resultModel, model.xmno);
            pointAlarmBLL.ProcessCgPointAlarmfilterInformation(processPointAlarmfilterInformationModel);
           
        }

        public class ProcessPointAlarmfilterInformationModel
        {
            public int xmno { get; set; }
            public List<NFnet_DAL.MODEL.alarmvalue> levelalarmvalue { get; set; }
            public cycdirnet resultModel { get; set; }
            public ProcessPointAlarmfilterInformationModel(int xmno, List<NFnet_DAL.MODEL.alarmvalue> levelalarmvalue, cycdirnet resultModel)
            {
                this.xmno = xmno;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
            }
        }

        public bool ProcessCycdirnetPointAlarm(ProcessCycdirnetPointAlarmModel model, out string mssg)
        {
            
            var processPointAlarmValueModel = new ProcessPointAlarmValueModel(model.xmno, model.model.POINT_NAME);
            if (!ProcessPointAlarmValue(processPointAlarmValueModel, out mssg))
            {
                mssg = string.Format("项目编号{0}表面位移{1}点未设置预警值", processPointAlarmValueModel.xmno, processPointAlarmValueModel.pointName);
                return false;
            }
            var processAlarmValueListModel = new ProcessAlarmValueListModel(model.model.TaskName, processPointAlarmValueModel.model);
            if (!ProcessAlarmValueList(processAlarmValueListModel, out mssg))
            {
                mssg = string.Format("项目编号{0}表面位移{1}点预警名对应的参数值出错", processPointAlarmValueModel.xmno, processPointAlarmValueModel.pointName);
                return false;
            }
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmfilterInformationModel(processPointAlarmValueModel.xmno, processAlarmValueListModel.alarmvalueList,model.model);
            ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel);
            mssg = string.Format("对项目编号{0}表面位移{1}点的成果预警处理成功", model.xmno, model.model.POINT_NAME);
            return true;

        }
        public class ProcessCycdirnetPointAlarmModel
        {
            public int xmno { get; set; }
            public cycdirnet model { get; set; }
            public ProcessCycdirnetPointAlarmModel(int xmno, cycdirnet model)
            {
                this.xmno = xmno;
                this.model = model;
            }
        }
        public bool ProcessCycdirnetPointAlarmBLL(int xmno, cycdirnet model)
        {
            var processCycdirnetPointAlarmModel = new ProcessCgResultDataAlarmBLL.ProcessCycdirnetPointAlarmModel(xmno, model);
            return ProcessCycdirnetPointAlarm(processCycdirnetPointAlarmModel, out mssg);
        }
    }
}