﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL.BLL;
using NFnet_BLL.DataProcess;
using System.Data;
using TotalStation.Model.fmos_obj;
using Tool;
using NFnet_BLL.XmInfo;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 点号预警业务逻辑处理类
    /// </summary>
    public partial class ProcessPointAlarmBLL
    {
        public static PointAlarmBLL alarmBLL = new PointAlarmBLL();
        public PointAttribute.BLL.fmos_pointalarmvalue bll = new PointAttribute.BLL.fmos_pointalarmvalue();
        public static ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public static ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public static ProcessalarmsplitondateBLL splitOnDateBLL = new ProcessalarmsplitondateBLL();
        public PointAttribute.BLL.fmos_settlementdisprelationship settlementdispbll = new PointAttribute.BLL.fmos_settlementdisprelationship();
        public static int sec = 0;
        /// <summary>
        /// 点号预警表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointAlarmRecordsCount(ProcessAlarmRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (alarmBLL.PointAlarmValueTableRowsCount(model.xmname, model.searchString, model.xmno, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点号预警记录数获取类
        /// </summary>
        public class ProcessAlarmRecordsCountModel : SearchCondition
        {
            public string totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessAlarmRecordsCountModel(string xmname, int xmno, string searchString, int pageIndex, int rows, string sord)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        /// <summary>
        /// 点号预警表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointAlarmLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (alarmBLL.PointAlarmValueTableLoad(model.searchString, model.pageIndex, model.rows, model.xmno, model.xmname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 表面位移点号表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (alarmBLL.PointTableLoad(model.searchString, model.pageIndex, model.rows, model.xmno, model.xmname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点号预警表获取类
        /// </summary>
        public class ProcessAlarmLoadModel : SearchCondition
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 搜索条件
            /// </summary>
            public string searchString { get; set; }
            /// <summary>
            /// 点号预警表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessAlarmLoadModel(string xmname, int xmno, string searchString, string colName, int pageIndex, int rows, string sord)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.searchString = searchString;
                this.xmno = xmno;

            }
        }
        /// <summary>
        /// 点号是否存在
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmExist(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return bll.Exist(model, out mssg);
        }
        /// <summary>
        /// 点号预警编辑
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmEdit(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return bll.Update(model, out mssg);
        }
        /// <summary>
        /// 点号预警添加
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmAdd(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            return bll.Add(model, out mssg);
        }

        public bool  ProcessAlarm(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            mssg = "";
            if (!ProcessAlarmExist(model, out mssg))
            {
               return  ProcessAlarmAdd(model, out mssg);
            }
            else
            {
              return  ProcessAlarmEdit(model, out mssg);
            }
        }
        /// <summary>
        /// 点号预警批量更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmMultiUpdate(ProcessAlarmMultiUpdateModel model, out string mssg)
        {
            return bll.MultiUpdate(model.pointstr, model.model, out mssg);
        }
        /// <summary>
        /// 点号预警删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmDel(NFnet_DAL.MODEL.pointalarmvalue model, out string mssg)
        {
            return alarmBLL.PointAlarmValueDel(model, out mssg);
        }

        /// <summary>
        /// 点号预警批量更新类
        /// </summary>
        public class ProcessAlarmMultiUpdateModel
        {
            /// <summary>
            /// 点组
            /// </summary>
            public string pointstr { get; set; }
            /// <summary>
            /// 点号预警类
            /// </summary>
            public PointAttribute.Model.fmos_pointalarmvalue model { get; set; }
            public ProcessAlarmMultiUpdateModel(string pointstr, PointAttribute.Model.fmos_pointalarmvalue model)
            {
                this.pointstr = pointstr;
                this.model = model;
            }
        }

        public bool ProcessSettlementDispPointReferenceAdd(PointAttribute.Model.fmos_settlementdisprelationship model,out string mssg)
        {
            return settlementdispbll.Add(model,out mssg);
        }
        public bool ProcessSettlementDispPointReferenceDelete(ProcessSettlementDispPointReferenceDeleteModel model, out string mssg)
        {
            return settlementdispbll.Delete(model.xmno,model.pointname,out mssg);
        }
        public class ProcessSettlementDispPointReferenceDeleteModel
        {
            public int xmno { get; set; }
            public string pointname { get; set; }
            public ProcessSettlementDispPointReferenceDeleteModel(int xmno,string pointname)
            {
                this.xmno = xmno;
                this.pointname = pointname;
            }
        }
        public bool ProcessSettlementDispPointReferenceTableLoad(ProcessSettlementDispPointReferenceTableLoadModel model, out string mssg)
        {
            DataTable dt = new DataTable();
            if (settlementdispbll.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt,out  mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessSettlementDispPointReferenceTableLoadModel:SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessSettlementDispPointReferenceTableLoadModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }
        public bool ProcessSettlementDispPointReferenceTableCountLoad(ProcessSettlementDispPointReferenceTableCountLoadModel model, out string mssg)
        {
            int count = 0;
            if (settlementdispbll.TableRowsCount(model.searchString, model.xmno, out count, out mssg))
            {
                model.totalCont = count.ToString();
                return true;
            }
            return false;
        }

        public bool ProcessSettlementDispPointReferenceModelLoad(ProcessSettlementDispPointReferenceModelLoadModel model,out string mssg)
        {
            PointAttribute.Model.fmos_settlementdisprelationship settlementdispmodel = new PointAttribute.Model.fmos_settlementdisprelationship();
            if (settlementdispbll.GetModel(model.xmno, model.point_name, out settlementdispmodel,out mssg))
            {
                model.model = settlementdispmodel;
                return true;
            }
            return false;
        }
        public class ProcessSettlementDispPointReferenceModelLoadModel
        {
            public int xmno { get; set; }
            public string point_name { get; set; }
            public PointAttribute.Model.fmos_settlementdisprelationship model { get; set; }
            public ProcessSettlementDispPointReferenceModelLoadModel(int xmno,string point_name)
            {
                this.xmno = xmno;
                this.point_name = point_name;
            }
        }

        /// <summary>
        /// 点号预警记录数获取类
        /// </summary>
        public class ProcessSettlementDispPointReferenceTableCountLoadModel
        {
            public string totalCont { get; set; }
            public int xmno { get; set; }
            public string searchString { get; set; }
            public ProcessSettlementDispPointReferenceTableCountLoadModel( int xmno, string searchString)
            {
                this.xmno = xmno;
                this.searchString = searchString;
            }
        }
        

        /// <summary>
        /// 结果数据对象预警处理
        /// </summary>
        /// <param name="alarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmIntoInformation(NFnet_DAL.MODEL.alarmvalue alarmvalue, cycdirnet resultModel, out List<string> lscontext)
        {
            List<string>  ls = new List<string>();
            lscontext = new List<string>();
            bool result = false;
            lscontext.Add(string.Format("{0},第{1}期  {2}", resultModel.POINT_NAME, resultModel.CYC, resultModel.Time.ToString()));
            //ExceptionLog.DTUPortInspectionWrite(string.Format("点名{0},周期{1},{2},{3},{4},{5},{6},{7}", resultModel.POINT_NAME,resultModel.CYC,resultModel.This_dN,resultModel.This_dE,resultModel.This_dZ,resultModel.Ac_dN,resultModel.Ac_dE,resultModel.Ac_dZ));
            bool hasalarm = false;

            DataProcessHelper.Compare("北", "本次", resultModel.This_dN, alarmvalue.This_rN, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if(result)
            ls.Add(string.Format("本次北方向{0}", resultModel.This_dN));

            DataProcessHelper.Compare("东", "本次", resultModel.This_dE, alarmvalue.This_rE, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
            ls.Add(string.Format("本次东方向{0}", resultModel.This_dE));

            DataProcessHelper.Compare("竖直", "本次", resultModel.This_dZ, alarmvalue.This_rZ, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
            ls.Add(string.Format("本次竖直方向{0}", resultModel.This_dZ));

            DataProcessHelper.RangeCompareU("北", "累计上限", resultModel.Ac_dN, alarmvalue.Ac_rN_U, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
            ls.Add(string.Format("累计北方向{0}", resultModel.Ac_dN));

            DataProcessHelper.RangeCompareL("北", "累计下限", resultModel.Ac_dN, alarmvalue.Ac_rN_L, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
            ls.Add(string.Format("累计北方向{0}", resultModel.Ac_dN));

            DataProcessHelper.RangeCompareU("东", "累计上限", resultModel.Ac_dE, alarmvalue.Ac_rE_U, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
            ls.Add(string.Format("累计东方向{0}", resultModel.Ac_dE));

            DataProcessHelper.RangeCompareL("东", "累计下限", resultModel.Ac_dE, alarmvalue.Ac_rE_L, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
            ls.Add(string.Format("累计东方向{0}", resultModel.Ac_dE));

            DataProcessHelper.RangeCompareU("竖直", "累计上限", resultModel.Ac_dZ, alarmvalue.Ac_rZ_U, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
            ls.Add(string.Format("累计竖直方向{0}", resultModel.Ac_dZ));

            DataProcessHelper.RangeCompareL("竖直", "累计下限", resultModel.Ac_dZ, alarmvalue.Ac_rZ_L, out result, false);
            hasalarm = hasalarm == false ? hasalarm = result : hasalarm;
            if (result)
            ls.Add(string.Format("累计竖直方向{0}", resultModel.Ac_dZ));
            
            
           
            
            
            
            lscontext.Add(string.Join("  ",ls));

            return hasalarm;
        }
       
        /// <summary>
        /// 结果数据多级预警处理
        /// </summary>
        /// <param name="levelalarmvalue"></param>
        /// <param name="resultModel"></param>
        /// <param name="ls"></param>
        /// <returns>true/false 超限信息</returns>
        public bool ProcessPointAlarmfilterInformation(ProcessPointAlarmfilterInformationModel model)
        {
            List<string> ls = new List<string>();
            string mssg = "";
            ExceptionLog.TotalSationPointCheckVedioWrite("*****************************************");
            //点号自检记录对象生成
            AuthorityAlarm.Model.pointcheck pointCheck = new AuthorityAlarm.Model.pointcheck
            {
                xmno = model.xmno,
                point_name = model.resultModel.POINT_NAME,
                time = model.resultModel.Time,
                type = "表面位移",
                atime = DateTime.Now,
                pid = string.Format("P{0}", DateHelper.DateTimeToString(DateTime.Now.AddMilliseconds(++sec))),
                readed = false
            };
            var processAlarmSplitOnDateDeleteModel = new ProcessalarmsplitondateBLL.ProcessalarmsplitondateDeleteModel(model.xmno, "表面位移", model.resultModel.POINT_NAME, model.resultModel.Time);
            //删除测量时间小于当前测量时间的对应项目的对应点号的数据[主键:项目编号,类型,点名,测量时间]
            if(splitOnDateBLL.ProcessalarmsplitondateDelete(processAlarmSplitOnDateDeleteModel, out mssg))
            ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
            //自建分包预警详情生成
            AuthorityAlarm.Model.alarmsplitondate splitalarm = new AuthorityAlarm.Model.alarmsplitondate
            {
                dno = string.Format("D{0}", DateHelper.DateTimeToString(DateTime.Now)),
                jclx = "表面位移",
                pointName = model.resultModel.POINT_NAME,
                xmno = model.xmno,
                time = model.resultModel.Time,
                adate = DateTime.Now,
                cyc = model.resultModel.CYC

            };
            if (model.levelalarmvalue.Count == 0) return false;
            if (model.levelalarmvalue[2] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[2], model.resultModel, out ls))
                {
                    ls.Add("三级预警");
                    pointCheck.alarm = 3;
                    //如果发生三级预警，覆盖进点号自检记录表[主键:项目编号,类型,点名]
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck,out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
                    model.ls = ls;
                    splitalarm.alarmContext = string.Join(",", ls);
                    splitalarm.alarm = 3;
                    splitOnDateBLL.ProcessalarmsplitondateAdd(splitalarm, out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
                    ProcessHotPotColorUpdate(model.xmno,"表面位移","",model.resultModel.POINT_NAME,3,out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite("*****************************************");
                    return true;
                }
            }
            if (model.levelalarmvalue[1] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[1], model.resultModel, out ls))
                {
                    ls.Add("二级预警");
                    pointCheck.alarm = 2;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
                    model.ls = ls;
                    splitalarm.alarmContext = string.Join(",", ls);
                    splitalarm.alarm = 2;
                    splitOnDateBLL.ProcessalarmsplitondateAdd(splitalarm, out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
                    ProcessHotPotColorUpdate(model.xmno, "表面位移", "", model.resultModel.POINT_NAME, 2, out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite("*****************************************");
                    return true;
                }
            }
            if (model.levelalarmvalue[0] != null)
            {

                if (ProcessPointAlarmIntoInformation(model.levelalarmvalue[0], model.resultModel, out ls))
                {
                    ls.Add("一级预警");
                    pointCheck.alarm = 1;
                    pointCheckBLL.ProcessPointCheckAdd(pointCheck, out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
                    model.ls = ls;
                    splitalarm.alarmContext = string.Join(",", ls);
                    splitalarm.alarm = 1;
                    splitOnDateBLL.ProcessalarmsplitondateAdd(splitalarm, out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
                    ProcessHotPotColorUpdate(model.xmno, "表面位移", "", model.resultModel.POINT_NAME, 1, out mssg);
                    ExceptionLog.TotalSationPointCheckVedioWrite("*****************************************");
                    return true;
                }


            }
                ls.Add("========预警解除==========");
                ExceptionLog.TotalSationPointCheckVedioWrite(string.Format("{0}表面位移-全站仪{1}未触发预警解除预警状态",model.xmname,model.resultModel.POINT_NAME));
                pointCheck.alarm = 0;
                pointCheckBLL.ProcessPointCheckDelete(pointCheck, out mssg);
                ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
                ProcessHotPotColorUpdate(model.xmno, "表面位移", "", model.resultModel.POINT_NAME, 0, out mssg);
                ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
                ExceptionLog.TotalSationPointCheckVedioWrite("*****************************************");
                return false;

        }
        public class ProcessPointAlarmfilterInformationModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public List<NFnet_DAL.MODEL.alarmvalue> levelalarmvalue { get; set; }
            public cycdirnet resultModel { get; set; }
            public List<string> ls { get; set; }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<NFnet_DAL.MODEL.alarmvalue> levelalarmvalue, cycdirnet resultModel,int xmno)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
                this.xmno = xmno;
            }
            public ProcessPointAlarmfilterInformationModel(string xmname, List<NFnet_DAL.MODEL.alarmvalue> levelalarmvalue, cycdirnet resultModel)
            {
                this.xmname = xmname;
                this.levelalarmvalue = levelalarmvalue;
                this.resultModel = resultModel;
            }
            public ProcessPointAlarmfilterInformationModel()
            {
               
            }
        }
        public class AlarmRecord
        {
            public string pointName { get; set; }
            public string Time { get; set; }
            public string alarm { get; set; }
            public string RecordTime { get; set; }

        }

        public bool ProcessPointAlarmModelGet(ProcessPointAlarmModelGetModel model, out string mssg)
        {
            PointAttribute.Model.fmos_pointalarmvalue pointalarm = new PointAttribute.Model.fmos_pointalarmvalue();
            if (bll.GetModel(model.xmno, model.pointName, out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }
        public class ProcessPointAlarmModelGetModel
        {
            public int xmno { get; set; }
            public string pointName { get; set; }
            public PointAttribute.Model.fmos_pointalarmvalue model { get; set; }
            public ProcessPointAlarmModelGetModel(int xmno, string pointName)
            {
                this.xmno = xmno;
                this.pointName = pointName;
            }
            
        }
        
        public bool ProcessHotPotColorUpdate(int xmno,string jclx,string jcoption,string pointName,int color,out string mssg)
        {
            var processMonitorPointLayoutColorUpdateModel = new ProcessLayoutBLL.ProcessMonitorPointLayoutColorUpdateModel(xmno, jclx, jcoption, pointName, color);
            return layoutBLL.ProcessMonitorPointLayoutColorUpdate(processMonitorPointLayoutColorUpdateModel, out mssg);
            
        }

        public bool ProcessXmAlarmModelGet(ProcessXmAlarmModelGetModel model, out string mssg)
        {
            PointAttribute.Model.fmos_pointalarmvalue pointalarm = new PointAttribute.Model.fmos_pointalarmvalue();
            if (bll.GetModel(model.xmno, out pointalarm, out mssg))
            {
                model.model = pointalarm;
                return true;
            }
            return false;
        }
        public class ProcessXmAlarmModelGetModel
        {
            public int xmno { get; set; }
            public PointAttribute.Model.fmos_pointalarmvalue model { get; set; }
            public ProcessXmAlarmModelGetModel(int xmno)
            {
                this.xmno = xmno;
            }

        }

    }
}