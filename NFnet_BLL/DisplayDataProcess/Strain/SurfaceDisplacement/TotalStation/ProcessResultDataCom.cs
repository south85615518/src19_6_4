﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DataProcess;
using System.Data;
using NFnet_BLL.Other;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{

    /// <summary>
    /// 根据角色选择测量库处理还是成果库处理
    /// </summary>
    public class ProcessResultDataCom
    {
        public ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        public void ProcessfillTotalStationDbFill(ProcessfillTotalStationDbFillModel model)
        {
            if (model.role == Role.superviseModel || model.tempRole)
            {
                resultDataBLL.ProcessfillTotalStationDbFill(model.model);
            }
            else
                resultDataBLL.ProcessfillTotalStationDbFill(model.model);


        }
        public void ProcessfillTotalStationDbFillNotExcute(ProcessfillTotalStationDbFillModel model)
        {
            if (model.role == Role.superviseModel || model.tempRole)
            {
                resultDataBLL.ProcessfillTotalStationDbFillNotExcute(model.model);
            }
            else
                resultDataBLL.ProcessfillTotalStationDbFillNotExcute(model.model);


        }
        public class ProcessfillTotalStationDbFillModel
        {
            public FillTotalStationDbFillCondition model { get; set; }
            public Role role { get; set; }
            public bool tempRole { get; set; }
            public ProcessfillTotalStationDbFillModel(FillTotalStationDbFillCondition model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }
        }


        public bool ProcessResultPageIndexChange(ProcessResultPageIndexChangeModel model, out string mssg)
        {
            if (model.role == Role.superviseModel || model.tempRole)
            {
                return ProcessComBLL.Processquerystanderdb(model.model, out mssg);
            }
            else
                return ProcessComBLL.Processquerystanderdb(model.model, out mssg);

        }

        public class ProcessResultPageIndexChangeModel
        {
            public QuerystanderdbModel model { get; set; }

            public Role role { get; set; }

            public bool tempRole { get; set; }
            public ProcessResultPageIndexChangeModel(QuerystanderdbModel model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }
        }


        

        public bool ProcessResultDataExtremely(ProcessResultDataExtremelyModel model, out string mssg)
        {
            if (model.role == Role.superviseModel || model.tempRole)
                return resultDataBLL.ProcessResultDataExtremely(model.model, out mssg);
            else
                return resultDataBLL.ProcessResultDataExtremely(model.model, out mssg);
        }
        public class ProcessResultDataExtremelyModel
        {
            public ResultDataExtremelyCondition model { get; set; }
            public Role role { get; set; }
            public bool tempRole { get; set; }
            public ProcessResultDataExtremelyModel(ResultDataExtremelyCondition model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }
        }

        public bool ProcessDateTimeToCyc(ProcessDateTimeToCycModel model, out string mssg)
        {

            if (model.role == Role.superviseModel || model.tempRole)
            {
                return resultDataBLL.ProcessDateTimeToCyc(model.model, out mssg);
            }
            return resultDataBLL.ProcessDateTimeToCyc(model.model, out mssg);
        }
        public class ProcessDateTimeToCycModel
        {
            public DateTimeToCycCondition model { get; set; }
            public Role role { get; set; }
            public bool tempRole { get; set; }
            public ProcessDateTimeToCycModel(DateTimeToCycCondition model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }
        }


        public bool ProcessTotalStationPointLoad(ProcessTotalStationPointLoadModel model, out string mssg)
        {

            return resultDataBLL.ProcessTotalStationPointLoad(model.model, out mssg);



        }

        public class ProcessTotalStationPointLoadModel
        {
            public TotalStationPointLoadCondition model { get; set; }
            public Role role { get; set; }
            public ProcessTotalStationPointLoadModel(TotalStationPointLoadCondition model, Role role)
            {
                this.model = model;
                this.role = role;

            }
        }

        public bool ProcessSinglepointcycload(ProcessSinglepointcycloadModel model, out string mssg)
        {
            if (model.role == Role.superviseModel)
            {
                return resultDataBLL.ProcessSinglepointcycload(model.model, out mssg);
            }
            else
                return resultDataBLL.ProcessSinglepointcycload(model.model, out mssg);
        }
        public class ProcessSinglepointcycloadModel
        {
            public SinglepointcycloadCondition model { get; set; }
            public Role role { get; set; }
            public ProcessSinglepointcycloadModel(SinglepointcycloadCondition model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }

        public bool ProcesssSinglecycdirnetdataload(ProcesssSinglecycdirnetdataloadModel model, out string mssg)
        {
            if (model.role == Role.superviseModel)
            {
                return resultDataBLL.ProcesssSinglecycdirnetdataload(model.model, out mssg);
            }
            else
                return resultDataBLL.ProcesssSinglecycdirnetdataload(model.model, out mssg);
        }

        public class ProcesssSinglecycdirnetdataloadModel
        {
            public SinglecycdirnetdataloadCondition model { get; set; }
            public Role role { get; set; }
            public ProcesssSinglecycdirnetdataloadModel(SinglecycdirnetdataloadCondition model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }
        public DataTable ProcesssSinglecycdirnetdataloadBLL(string xmname, string pointname, int startcyc, int endcyc, Role role, out string mssg)
        {
            var singlecycdirnetdataloadCondition = new SinglecycdirnetdataloadCondition(xmname, pointname, startcyc, endcyc);
            var processSinglepointcycloadModel = new ProcessResultDataCom.ProcesssSinglecycdirnetdataloadModel(singlecycdirnetdataloadCondition, role);
            ProcesssSinglecycdirnetdataload(processSinglepointcycloadModel, out mssg);
            return processSinglepointcycloadModel.model.dt;
        }
        public bool ProcessResultDataTableLoad(ProcessResultDataTableLoadModel model, out string mssg)
        {
            if (model.role == Role.superviseModel || model.tempRole)
            {
                return resultDataBLL.ProcessResultDataLoad(model.model, out mssg);
            }
            return resultDataBLL.ProcessResultDataLoad(model.model, out mssg);
        }
        public class ProcessResultDataTableLoadModel
        {
            public ResultDataLoadCondition model { get; set; }
            public Role role { get; set; }
            public bool tempRole { get; set; }
            public ProcessResultDataTableLoadModel(ResultDataLoadCondition model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }

        }
        public DataTable ProcessResultDataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname,string sord ,Role role, bool tmpRole, out string mssg)
        {
            var resultDataLoadCondition = new ResultDataLoadCondition(xmname, pointname, startPageIndex, pageSize, startCyc, endCyc,sord);
            var processResultDataTableLoadModel = new ProcessResultDataTableLoadModel(resultDataLoadCondition, role, tmpRole);
            if (ProcessResultDataTableLoad(processResultDataTableLoadModel, out mssg))
            {
                return processResultDataTableLoadModel.model.dt;
            }
            return new DataTable();
        }

        public bool ProcessResultDataTableCountLoad(ProcessResultDataTableCountLoadModel model, out string mssg)
        {
            if (model.role == Role.superviseModel || model.tempRole)
            {
                return resultDataBLL.ProcessResultDataRecordsCount(model.model, out mssg);
            }
            return resultDataBLL.ProcessResultDataRecordsCount(model.model, out mssg);
        }
        public class ProcessResultDataTableCountLoadModel
        {
            public ResultDataCountLoadCondition model { get; set; }
            public Role role { get; set; }
            public bool tempRole { get; set; }
            public ProcessResultDataTableCountLoadModel(ResultDataCountLoadCondition model, Role role, bool tempRole)
            {
                this.model = model;
                this.role = role;
                this.tempRole = tempRole;
            }

        }
        public string ProcessResultDataTableCountLoad(int startCyc, int endCyc, string xmname, string pointname, Role role, bool tmpRole, out string mssg)
        {
            var resultDataLoadCondition = new ResultDataCountLoadCondition(xmname, pointname, startCyc, endCyc);
            var processResultDataTableCountLoadModel = new ProcessResultDataTableCountLoadModel(resultDataLoadCondition, role, tmpRole);
            if (ProcessResultDataTableCountLoad(processResultDataTableCountLoadModel, out mssg))
            {
                return processResultDataTableCountLoadModel.model.totalCont;
            }
            return "0";
        }

        public bool ProcessPointNameCycListLoad(string xmname, string pointname, Role role, bool tmpRole, out List<string> ls ,out string mssg)
        {
            ls = new List<string>();
            var pointNameCycListCondition = new PointNameCycListCondition(xmname, pointname);
            if (role == Role.superviseModel || tmpRole)
            {

                if (resultDataBLL.ProcessPointNameCycListLoad(pointNameCycListCondition, out mssg))
                {
                    ls = pointNameCycListCondition.ls;
                    return true;
                }
                return false;
            }

            if (resultDataBLL.ProcessPointNameCycListLoad(pointNameCycListCondition, out mssg))
            {
                ls = pointNameCycListCondition.ls;
                return true;
            }
            return false;
        }

        public bool ProcessPointNewestDateTimeGet(ProcessPointNewestDateTimeGetModel model,out string mssg)
        {
            if (model.role == Role.superviseModel)
            {
                return resultDataBLL.ProcessPointNewestDateTimeGet(model.model, out mssg);
                 
            }
            return resultDataBLL.ProcessPointNewestDateTimeGet(model.model, out mssg);
          
            
        }
        public class ProcessPointNewestDateTimeGetModel
        {
            public PointNewestDateTimeCondition model { get; set; }
            public Role role { get; set; }
            public ProcessPointNewestDateTimeGetModel(PointNewestDateTimeCondition model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }
        public bool ProcessPointNewestDateTimeGet(string xmname,string pointname,Role role,out DateTime dt,out string mssg)
        {
            var pointNewestDateTimeCondition = new PointNewestDateTimeCondition(xmname,pointname,data.Model.gtsensortype._HorizontalDisplacement);
            var processPointNewestDateTimeGetModel = new ProcessPointNewestDateTimeGetModel(pointNewestDateTimeCondition,role);
            if (ProcessPointNewestDateTimeGet(processPointNewestDateTimeGetModel, out mssg))
            {
                dt = processPointNewestDateTimeGetModel.model.dt;
                return true;
            }
            dt = new DateTime();
            return false;
            
        }
        public bool ProcessResultDataReportTableCreate(processResultDataReportTableCreateModel model,out string  mssg)
        {
            if (model.role == Role.superviseModel || model.tmpRole)
            {
                return resultDataBLL.ProcessResultDataReportTableCreate(model.model, out mssg);
                 
            }
            return resultDataBLL.ProcessResultDataReportTableCreate(model.model, out mssg);
          
        }
        public class processResultDataReportTableCreateModel
        {
            public ResultDataReportTableCreateCondition model{ get; set;}
            public Role role { get; set; }
            public bool tmpRole{ get; set; }
            public processResultDataReportTableCreateModel(ResultDataReportTableCreateCondition model,Role role,bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }
        public bool ProcessResultDataMaxTime(ProcessResultDataMaxTimeModel model,out string mssg)
        {
            if (model.role == Role.superviseModel || model.tmpRole)
            {
                return resultDataBLL.ProcessResultDataMaxTime(model.model, out mssg);
            }
            return resultDataBLL.ProcessResultDataMaxTime(model.model, out mssg);
            
        }
        public class ProcessResultDataMaxTimeModel
        {
            public TotalStationResultDataMaxTimeCondition model { get; set; }
            public Role role { get; set; }
            public bool tmpRole{ get;set;}
            public ProcessResultDataMaxTimeModel(TotalStationResultDataMaxTimeCondition model,Role role,bool tmpRole)
            {
                this.model = model;
                this.role = role;
                this.tmpRole = tmpRole;
            }
        }

    }
}