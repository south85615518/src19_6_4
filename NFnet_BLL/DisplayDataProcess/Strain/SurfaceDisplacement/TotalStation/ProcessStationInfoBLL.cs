﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.BLL.fmos_obj;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 测站业务逻辑处理类
    /// </summary>
    public class ProcessStationInfoBLL
    {
        public static stationinfo stationinfoBLL = new stationinfo();
        /// <summary>
        /// 测站记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessstationinfoRecordsCount(ProcessstationinfoRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (stationinfoBLL.stationInfoTableRowsCount(model.searchpost, model.xmname, out totalCont, out mssg))
            {
                model.tabCount = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 测站记录数获取类
        /// </summary>
        public class ProcessstationinfoRecordsCountModel : TabCountSearchCondition
        {
            /// <summary>
            /// 筛选条件
            /// </summary>
            public string searchpost { get; set; }
            public ProcessstationinfoRecordsCountModel(string xmname, string searchpost)
            {
                this.xmname = xmname;
                this.searchpost = searchpost;

            }
        }
        /// <summary>
        /// 测站表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessstationinfoLoad(ProcessstationinfoLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (stationinfoBLL.stationInfoTableLoad(model.pageIndex, model.rows, model.xmname, model.searchpost, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 测站表获取类
        /// </summary>
        public class ProcessstationinfoLoadModel : SearchCondition
        {
            /// <summary>
            /// 筛选条件
            /// </summary>
            public string searchpost { get; set; }
            /// <summary>
            /// 测站表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessstationinfoLoadModel(string xmname, string colName, int pageIndex, int rows, string sord, string searchpost)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.searchpost = searchpost;

            }
        }
    }
}