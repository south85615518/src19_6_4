﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.BLL.fmos_obj;
using System.Data;
using NFnet_BLL.DataProcess;


namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    /// <summary>
    /// 点组业务逻辑处理类
    /// </summary>
    public class ProcessPointsinteamBLL
    {
        public static fmos_pointsinteam PointsinteamBLL = new fmos_pointsinteam();
        /// <summary>
        /// 点组表记录数获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointsinteamRecordsCount(ProcessPointsinteamRecordsCountModel model, out string mssg)
        {
            string totalCont = "0";
            if (PointsinteamBLL.PointsInTeamTableRowsCount(model.searchpost, model.xmno,model.xmname, out totalCont, out mssg))
            {
                model.tabCount = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点组记录数获取类
        /// </summary>
        public class ProcessPointsinteamRecordsCountModel : TabCountSearchCondition
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 搜索条件
            /// </summary>
            public string searchpost { get; set; }
            public ProcessPointsinteamRecordsCountModel(string xmname, int xmno, string searchpost)
            {
                this.xmname = xmname;
                this.searchpost = searchpost;

            }
        }
        /// <summary>
        /// 点组表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessPointsinteamLoad(ProcessPointsinteamLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (PointsinteamBLL.PointsInTeamTableLoad(model.pageIndex, model.rows, model.xmno, model.xmname, model.searchpost, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 点组表获取类
        /// </summary>
        public class ProcessPointsinteamLoadModel : SearchCondition
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 搜索条件
            /// </summary>
            public string searchpost { get; set; }
            /// <summary>
            /// 点组表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessPointsinteamLoadModel(string xmname, string colName, int pageIndex, int rows, string sord, int xmno, string searchpost)
            {
                this.xmname = xmname;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;
                this.xmno = xmno;
                this.searchpost = searchpost;

            }
        }

        public bool ProcessInstrumentparamNameLoad(ProcessInstrumentparamNameLoadModel model, out string mssg)
        {
            List<string> teamName = null;
            if (PointsinteamBLL.PointsInTeamNameListLoad(model.xmno, out teamName, out mssg))
            {
                model.teamName = teamName;
                return true;
            }
            return false;
        }
        public class ProcessInstrumentparamNameLoadModel
        {
            public int xmno { get; set; }
            public List<string> teamName { get; set; }
            public ProcessInstrumentparamNameLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

       


    }
}