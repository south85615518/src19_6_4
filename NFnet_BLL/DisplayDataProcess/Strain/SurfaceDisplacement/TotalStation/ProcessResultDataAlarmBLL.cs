﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using NFnet_BLL.DataProcess;
using TotalStation.Model.fmos_obj;
using Tool;
using System.IO;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation
{
    public partial class ProcessResultDataAlarmBLL
    {
        public string xmname { get; set; }
        public int xmno { get; set; }
        public string dateTime { get; set; }
        public int alarmcont { get; set; }
        //保存预警信息
        public List<string> alarmInfoList { get; set; }
        public ProcessResultDataAlarmBLL()
        {
        }
        public ProcessResultDataAlarmBLL(string xmname, int xmno)
        {
            this.xmname = xmno.ToString();
            this.xmno = xmno;
        }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public static string mssg = "";
        // public string xmname = "华南水电四川成都大坝监测A";
        public static string timeJson = "";
        public static ProcessResultDataBLL resultBLL = new ProcessResultDataBLL();
        public bool main()
        {
            alarmcont = 0;
            return TestCycdirnetModelList();

        }
        public bool TestCycdirnetModelList()
        {
            if (dateTime == "") dateTime = DateTime.Now.ToString();
            var processResultDataAlarmModelListModel = new ProcessResultDataBLL.ProcessResultDataAlarmModelListModel(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(xmname));
            List<global::TotalStation.Model.fmos_obj.cycdirnet> modellist;
            if (resultBLL.ProcessResultDataAlarmModelList(processResultDataAlarmModelListModel, out modellist, out mssg))
            {

                CycdirnetPointAlarm(modellist, "表面位移--全站仪--超限自检");
                return true;
            }
            return false;

            //ProcessPrintMssg.Print(mssg);

        }
        public PointAttribute.Model.fmos_pointalarmvalue TestPointAlarmValue(string pointName)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(xmno, pointName);
            PointAttribute.Model.fmos_pointalarmvalue model = null;
            if (pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                //ProcessPrintMssg.Print(mssg);
                //TestAlarmValueList(processPointAlarmModelGetModel.model);
                return processPointAlarmModelGetModel.model;

            }
            return null;
        }
        public List<NFnet_DAL.MODEL.alarmvalue> TestAlarmValueList(PointAttribute.Model.fmos_pointalarmvalue pointalarm)
        {
            List<NFnet_DAL.MODEL.alarmvalue> alarmvalueList = new List<NFnet_DAL.MODEL.alarmvalue>();
            //一级
            var processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.FirstAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);

            //ProcessPrintMssg.Print("一级：" + mssg);
            alarmvalueList.Add(processAlarmModelGetByNameModel.model);


            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.SecondAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);

            //ProcessPrintMssg.Print("二级：" + mssg);
            alarmvalueList.Add(processAlarmModelGetByNameModel.model);


            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(xmno, pointalarm.ThirdAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);

            //ProcessPrintMssg.Print("三级：" + mssg);
            alarmvalueList.Add(processAlarmModelGetByNameModel.model);


            return alarmvalueList;
        }
        public void TestPointAlarmfilterInformation(List<NFnet_DAL.MODEL.alarmvalue> levelalarmvalue, cycdirnet resultModel)
        {
            var processPointAlarmfilterInformationModel = new ProcessPointAlarmBLL.ProcessPointAlarmfilterInformationModel(xmname, levelalarmvalue, resultModel, xmno);
            if (pointAlarmBLL.ProcessPointAlarmfilterInformation(processPointAlarmfilterInformationModel))
            {
                //Console.WriteLine("\n"+string.Join("\n", processPointAlarmfilterInformationModel.ls));
                alarmcont++;
                ExceptionLog.ExceptionWriteCheck(processPointAlarmfilterInformationModel.ls);
                alarmInfoList.AddRange(processPointAlarmfilterInformationModel.ls);
            }
        }
        public bool CycdirnetPointAlarm(List<cycdirnet> lc, string title)
        {
            alarmInfoList = new List<string>();
            List<string> ls = new List<string>();
            ls.Add("\n");
            ls.Add(string.Format("==========={0}===========", DateTime.Now));
            ls.Add(string.Format("==========={0}===========", xmname));
            ls.Add(string.Format("==========={0}===========", title));
            ls.Add("\n");
            alarmInfoList.AddRange(ls);
            //ExceptionLog.ExceptionWriteCheck(ls);
            ExceptionLog.TotalSationPointCheckVedioWrite("获取到项目" + xmname + "自检临时表中记录数" + lc.Count + "条");

            foreach (cycdirnet cl in lc)
            {
                //string threadname = Thread.CurrentThread.Name;
                //if (!Tool.ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", xmname)) && !Tool.ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", xmname))) return false;
                PointAttribute.Model.fmos_pointalarmvalue pointvalue = TestPointAlarmValue(cl.POINT_NAME);
                List<NFnet_DAL.MODEL.alarmvalue> alarmList = TestAlarmValueList(pointvalue);
                TestPointAlarmfilterInformation(alarmList, cl);

            }
            ExceptionLog.TotalSationPointCheckVedioWrite("项目" + xmname + "本次预警共产生新预警记录" + alarmcont + "条");
            ExceptionLog.TotalSationPointCheckVedioWrite("************预警结束**********");
            return true;
            //Console.ReadLine();
        }
        public List<string> ResultDataAlarm(string xmname, int xmno)
        {
            //ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL(xmname, xmno);
            this.xmname = xmname;
            this.xmno = xmno;
            ExceptionLog.TotalSationPointCheckVedioWrite("************开始预警**********");
            main();
            return this.alarmInfoList;
        }
        public void CycdirnetPointAlarm(cycdirnet dirnet)
        {
            alarmInfoList = new List<string>();
            PointAttribute.Model.fmos_pointalarmvalue pointvalue = TestPointAlarmValue(dirnet.POINT_NAME);
            if (pointvalue == null) return;
            List<NFnet_DAL.MODEL.alarmvalue> alarmList = TestAlarmValueList(pointvalue);
            TestPointAlarmfilterInformation(alarmList, dirnet);

        }




    }
}