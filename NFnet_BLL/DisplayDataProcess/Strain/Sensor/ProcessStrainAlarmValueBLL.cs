﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.DisplayDataProcess.Strain.Sensor
{
    public class ProcessAlarmValueBLL
    {
        public static global::Strain.BLL.strainalarmvalue bll = new  global::Strain.BLL.strainalarmvalue();
            
        #region  BasicMethod
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool ProcessstrainalarmvalueAdd(global::Strain.Model.strainalarmvalue model,out string mssg)
        {
           return bll.Add(model,out mssg);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool ProcessstrainalarmvalueUpdate(global::Strain.Model.strainalarmvalue model,out string mssg)
        {
            return bll.Update(model,out mssg);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool ProcessstrainalarmvalueDelete(ProcessstrainalarmvalueDeleteModel model,out string mssg)
        {

           return bll.Delete(model.xmno,model.alarmname,out mssg);
        }

        public class ProcessstrainalarmvalueDeleteModel
        {
            public int xmno{get;set;}
            public string alarmname{get;set;}
            public ProcessstrainalarmvalueDeleteModel(int xmno,string alarmname)
            {
            this.xmno = xmno;
            this.alarmname = alarmname;
            }
        }


        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool ProcessstrainpointalarmvalueDelCasc(ProcessstrainpointalarmvalueDelCascModel model,out string mssg)
        {
             return bll.PointAlarmValueDelCasc(model.alarmname, model.xmno,out mssg);
        }

        public class ProcessstrainpointalarmvalueDelCascModel
        {
            public string alarmname{get;set;}
            public int xmno{get;set;}
            ProcessstrainpointalarmvalueDelCascModel()
            {
            this.alarmname = alarmname;
            this.xmno = xmno;
            }
        }

         public bool ProcessAlarmModelGetByName(ProcessAlarmModelGetByNameModel model,out string mssg)
        {
            global::Strain.Model.strainalarmvalue alarm = new global::Strain.Model.strainalarmvalue();
            if (bll.GetModel(model.name, model.xmno, out alarm, out mssg))
            {
                model.model = alarm;
                return true;
            }
            return false;
        }
        public class ProcessAlarmModelGetByNameModel
        {
            public int xmno { get; set; }
            public  global::Strain.Model.strainalarmvalue  model { get; set; }
            public string name { get; set; }
            public ProcessAlarmModelGetByNameModel(int xmno,string name)
            {
                this.xmno = xmno;
                this.name = name;
            }
        }


        /// <summary>
        /// 预警名称获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmValueName(ProcessAlarmValueNameModel model, out string mssg)
        {
            string alarmValueNameStr = "";
            mssg = "";
            if (bll.AlarmValueNameGet(model.xmno, out alarmValueNameStr, out mssg))
            {
                model.alarmValueNameStr = alarmValueNameStr;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        ///预警名称获取类
        /// </summary>
        public class ProcessAlarmValueNameModel
        {
            public int xmno { get; set; }
            public string alarmValueNameStr { get; set; }
            public ProcessAlarmValueNameModel(int xmno, string alarmValueNameStr)
            {
                this.xmno = xmno;
                this.alarmValueNameStr = alarmValueNameStr;
            }
        }


        /// <summary>
        /// 预警参数表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmLoad(ProcessAlarmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (bll.TableLoad(model.pageIndex, model.rows, model.xmno, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 预警参数表获取实体
        /// </summary>
        public class ProcessAlarmLoadModel : SearchCondition
        {
            /// <summary>
            /// 预警参数表
            /// </summary>
            public DataTable dt { get; set; }
            public int xmno { get; set; }
            public ProcessAlarmLoadModel(int xmno, string colName, int pageIndex, int rows, string sord)
            {
                this.xmno = xmno;
                this.colName = colName;
                this.pageIndex = pageIndex;
                this.rows = rows;
                this.sord = sord;

            }
        }

  
        #endregion  BasicMethod
        
    }
}
