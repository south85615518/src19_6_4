﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Strain;
using System.Data;
using SqlHelpers;
using NFnet_BLL.Other;
using NFnet_MODAL;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using Tool;


namespace NFnet_BLL.DataProcess.Strain.Sensor
{

    /// <summary>
    /// 结果数据业务逻辑处理类
    /// </summary>
    public class ProcessResultDataBLL
    {

        public static global::Strain.BLL.strainresultdata bll = new global::Strain.BLL.strainresultdata();

        public bool ProcessResultDataUpdate(global::Strain.Model.strainresultdata model,out string mssg)
        {
            return bll.Update(model,out mssg);
        }

        public bool ProcessResultDataAdd(global::Strain.Model.strainresultdata model, out string mssg)
        {
            return bll.Add(model, out mssg);
        }

        /// <summary>
        /// 结果数据表记录获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataRecordsCount(ResultDataCountLoadCondition model, out string mssg)
        {
            string totalCont = "0";
            if (bll.ResultTableRowsCount(model.xmno,model.pointname ,out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            else
            {

                return false;
            }

        }
       
        /// <summary>
        /// 结果数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataLoad(ResultDataLoadCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ResultDataTableLoad(model.pageIndex, model.rows, model.xmno, model.pointname,model.sord ,out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {

                return false;
            }

        }

        public bool ProcessXmStateTableLoad(XmStateTableLoadCondition model,out string mssg)
        {
            DataTable dt = new DataTable();
            if (bll.XmStateTable(model.pageIndex, model.rows, model.xmno,model.xmname ,model.unitname, model.colName, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 结果数据报表数据表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataReportTableCreate(ResultDataReportTableCreateCondition model, out string mssg)
        {
            DataTable dt = null;
            if (bll.ResultDataReportPrint(model.sql, model.xmno, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        
      
        /// <summary>
        /// 数据展示日期查询条件生成
        /// </summary>
        /// <param name="model"></param>
        public void ProcessResultDataRqcxConditionCreate(ResultDataRqcxConditionCreateCondition model)
        {

            switch (model.type)
            {
                case QueryType.RQCX:
                    string sqlsttm = "";
                    string sqledtm = "";
                    string mssg = "";
                    string cycmin = "";
                    string cycmax = "";
                    var querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.startTime, ">=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqlsttm = querynvlmodel.str;
                    }
                    querynvlmodel = new ProcessComBLL.Processquerynvlmodel("#_date", model.endTime, "<=", "date_format('", "','%y-%m-%d %H:%i:%s')");
                    if (ProcessComBLL.Processquerynvl(querynvlmodel, out mssg))
                    {
                        sqledtm = querynvlmodel.str;
                    }
                    string rqConditionStr = sqlsttm + " and " + sqledtm + " order by #_point,#_date asc ";
                    string sqlmin = "select min(monitorTime) as mincyc from strainresultdata where xmno = '" + model.xmno + "'";
                    string sqlmax = "select max(monitorTime) as maxcyc from strainresultdata where xmno = '" + model.xmno + "'";
                    var processquerystanderstrModel = new QuerystanderstrModel(sqlmin, model.xmno);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                       model.startTime = processquerystanderstrModel.str;
                    }
                    processquerystanderstrModel = new QuerystanderstrModel(sqlmax, model.xmno);
                    if (ProcessComBLL.Processquerystanderstr(processquerystanderstrModel, out mssg))
                    {
                       model.endTime = processquerystanderstrModel.str;
                    }
                    //model.minCyc = cycmin;
                    //model.maxCyc = cycmax;
                    model.sql = rqConditionStr;
                    break;
                case QueryType.ZQCX:
                    rqConditionStr = "#_cyc >= " + model.startTime + "   and  #_cyc <= " + model.endTime + " order by #_point,cyc,#_date asc ";
                    model.minCyc = model.startTime;
                    model.maxCyc = model.endTime;

                    model.sql = rqConditionStr;
                    break;
                case QueryType.QT:
                    var processdateswdlModel = new ProcessComBLL.ProcessdateswdlModel("#_date", model.unit,model.maxTime);
                    if (ProcessComBLL.Processdateswdl(processdateswdlModel, out mssg))
                    {
                        model.startTime = processdateswdlModel.sttm;
                        model.endTime = processdateswdlModel.edtm;
                        //model.sql = processdateswdlModel.sql;
                    }
                    break;


            }
        }



        public bool ProcessResultDataMaxTime(SenorMaxTimeCondition model, out string mssg)
        {
            DateTime maxTime = new DateTime();
            if (bll.MaxTime(model.xmno, out maxTime,out mssg))
            {
                model.dt = maxTime;
                return true;
            }
            return false;

                
        }
        

        /// <summary>
        /// 填充全站仪结果数据表生成
        /// </summary>
        public bool ProcessfillStrainDbFill(FillSensorDbFillCondition model)
        {
            model.rqConditionStr = model.rqConditionStr.Replace("#_date", "monitorTime");
            model.rqConditionStr = model.rqConditionStr.Replace("#_point", "   pointname  ");
            model.pointname = "'" + model.pointname.Replace(",", "','") + "'";
            string mssg = "";
            DataTable dt = null;
            var Processquerynvlmodel = new ProcessComBLL.Processquerynvlmodel("  pointname  ", model.pointname, "in", "(", ")");
            string sql = "";
            if (ProcessComBLL.Processquerynvl(Processquerynvlmodel, out mssg))
            {
                sql = "select  pointname,initstrengthVal,strengthVal,this_val,ac_val,monitorTime    from  strainresultdata where  xmno = '" + model.xmno + "' and  " + Processquerynvlmodel.str + "  ";//表名由项目任务决定
                sql += " and " + model.rqConditionStr;
            }
            model.sql = sql;
            var processquerystanderdbModel = new QuerystanderdbIntModel(sql, model.xmno);
            if (ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
            {
                ExceptionLog.ExceptionWrite(sql);
                ExceptionLog.ExceptionWrite(mssg);
                model.dt = processquerystanderdbModel.dt;
                return true;
            }
            else
            {
                return false;
            }


        }


        public bool GetResultDataTable(string sql, int xmno, out DataTable dt, out string mssg)
        {
            var processquerystanderdbModel = new QuerystanderdbIntModel(sql, xmno);
            dt = null;
            mssg = "";
            if (ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
            {
                dt = processquerystanderdbModel.dt;
                return true;
            }
            return false;
        }



        //根据表的分页单位获取当前记录在表中的页数
        public int PageIndexFromTab(string pointname, string date, DataView dv, string pointnameStr, string dateStr, int pageSize)
        {

            //DataView dv = new DataView(dt);
            int i = 0;
            //DateTime dat = new DateTime();
            string datUTC = "";
            foreach (DataRowView drv in dv)
            {

                //datUTC = dat.GetDateTimeFormats('r')[0].ToString();
                //datUTC = datUTC.Substring(0, datUTC.IndexOf("GMT"));
                string a = drv[pointnameStr].ToString() + "=" + pointname + "|" + date + "=" + drv[dateStr];
                if (drv[pointnameStr].ToString() == pointname && date.Trim() == drv[dateStr].ToString())
                {
                    return i / pageSize;
                }
                i++;
            }

            return 0;
        }

        public bool ProcessResultDataAlarmModelList(ProcessResultDataAlarmModelListModel model, out List<global::Strain.Model.strainresultdata> modellist, out string mssg)
        {
            modellist = new List<global::Strain.Model.strainresultdata>();
            if (bll.GetModelList(model.xmno, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }

        public class ProcessResultDataAlarmModelListModel
        {
            public int xmno { get; set; }
            public List<global::Strain.Model.strainresultdata> modellist { set; get; }
            public ProcessResultDataAlarmModelListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }


       

       
        /// <summary>
        /// 点名列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessStrainPointLoad(SensorPointLoadCondition model, out string mssg)
        {
            List<string> ls = null;
            if (bll.StrainPointLoadBLL(model.xmno, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessPointNewestDateTimeGet(PointNewestDateTimeCondition model,out string mssg)
        {
            DateTime dt = new DateTime();
            if (bll.PointNewestDateTimeGet(model.xmno, model.pointname, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
      
    }
}