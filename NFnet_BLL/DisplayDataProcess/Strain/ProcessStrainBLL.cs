﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess.Strain
{
    /// <summary>
    /// 应力数据业务逻辑处理类
    /// </summary>
    public class ProcessStrainBLL
    {
        public static sensorDAL.BLL.StrainBLL ylBLL = new sensorDAL.BLL.StrainBLL();
        /// <summary>
        /// 应力点号获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessStrainPointLoad(ProcessStrainPointLoadModel model, out string mssg)
        {
            List<string> ls = null;
            if (ylBLL.StrainPointLoadBLL(model.xmname, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 应力点号获取类
        /// </summary>
        public class ProcessStrainPointLoadModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 点号列表
            /// </summary>
            public List<string> ls { get; set; }
            public ProcessStrainPointLoadModel(string xmname, List<string> ls)
            {
                this.xmname = xmname;
                this.ls = ls;
            }
        }
    }
}