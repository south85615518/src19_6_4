﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_MODAL;
using NFnet_BLL.Other;

namespace NFnet_BLL.DisplayDataProcess.Strain
{
    /// <summary>
    /// 应力曲线图业务逻辑处理类
    /// </summary>
    public class ProcessStrainChartBLL
    {
        /// <summary>
        /// 应力曲线图序列化
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSerializestrAxia(ProcessChartCondition model,out string mssg)
        {
            DataTable dt = new DataTable();
            mssg = "";
            string settlementes = model.pointname;
            string sql = model.sql;
            //分模块号显示
            settlementes = settlementes.Replace("(", "");
            settlementes = settlementes.Replace(")", "");
            string[] settlementarr = settlementes.Split(',');
            if (settlementes != "''" && settlementes != "" && settlementes != null)
            {
                var processquerystanderdbModel = new QuerystanderdbModel(sql, model.xmname);
                if (!ProcessComBLL.Processquerystanderdb(processquerystanderdbModel, out mssg))
                {
                    //错误信息反馈
                }
                dt = processquerystanderdbModel.dt;
                List<serie> lst = new List<serie>();
                for (int i = 0; i < settlementarr.Length; i++)
                {
                    serie sr = new serie();
                    serie srAc = new serie();
                    DataView filter = new DataView(dt);
                    string filterstr = "pointname in (" + settlementarr[i] + ")";
                    filter.RowFilter = filterstr;
                    sr.Name = settlementarr[i].Replace("'", "");
                    srAc.Name = settlementarr[i].Replace("'", "");
                    //加载点
                    formatdatstrain(filter, sr, "应力本次", "this_val");
                    formatdatstrain(filter, srAc, "应力累计", "ac_val");
                    lst.Add(sr);
                    lst.Add(srAc);

                }
                model.series = lst;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 应力数据表生成曲线图
        /// </summary>
        /// <param name="dv">应力数据表视图</param>
        /// <param name="sr">曲线</param>
        /// <param name="typeTitle">曲线类型</param>
        /// <param name="valword">绑定的数据表字段</param>
        public static void formatdatstrain(DataView dv, serie sr, string typeTitle, string valword)
        {
            sr.Stype = typeTitle;
            sr.Pts = new pt[dv.Count];
            int i = 0;
            foreach (DataRowView drv in dv)
            {
                string sj = drv["MonitorTime"].ToString();
                string v = drv[valword].ToString();
                DateTime d = Convert.ToDateTime(sj);
                int year = d.Year;
                int mon = d.Month;
                int day = d.Day;
                int hour = d.Hour;
                int minute = d.Minute;
                int second = d.Second;
                sr.Pts[i] = new pt { Dt = d, Yvalue1 = (float)Convert.ToDouble(v) };
                i++;
            }

        }
    }
}