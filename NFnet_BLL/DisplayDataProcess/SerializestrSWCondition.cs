﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;
using Tool;

namespace NFnet_BLL.DisplayDataProcess
{
    public class SerializestrSWCondition : ProcessChartCondition
    {
        //public int xmno { get; set; }
        public List<serie> sr { get; set; } 
        
        public SerializestrSWCondition(object sql, object xmname, object pointname)  {
            this.sql = sql == null ? "" : sql.ToString();
            this.xmname = xmname == null ? "0" : xmname.ToString();
            this.pointname = pointname == null ? "" : pointname.ToString();
        }
        public SerializestrSWCondition(object sql, object xmno, object pointname, int xm)  {
            ExceptionLog.ExceptionWrite("项目编号:"+xmno);
            this.sql = sql == null ? "" : sql.ToString();
            this.xmno = xmno == null ? -1 : Convert.ToInt32(xmno);
            this.pointname = pointname == null ? "" : pointname.ToString();
        }
        
    }
}