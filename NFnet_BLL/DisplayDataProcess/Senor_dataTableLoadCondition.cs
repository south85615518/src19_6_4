﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class Senor_dataTableLoadCondition
    {
        public int xmno { get; set; }
        public string pointstr { get; set; }
        public DateTime st { get; set; }
        public DateTime ed { get; set; }
        public Senor_dataTableLoadCondition(int xmno,string pointstr,DateTime st,DateTime ed)
        {
            this.xmno = xmno;
            this.pointstr = pointstr;
            this.st = st;
            this.ed = ed;
        }
    }
}