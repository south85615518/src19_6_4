﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DisplayDataProcess
{
    public class BKGMaxTimeCondition
    {
        public string xmname { get; set; }
        public DateTime maxTime { get; set; }
        public BKGMaxTimeCondition(string xmname)
        {
            this.xmname = xmname;
        }


    }
}