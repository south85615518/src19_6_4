﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.UserProcess;

namespace NFnet_BLL.ProjectMap
{
    public class ProcessXmMapBLL
    {
        public bool ProcessXmMap(ProcessXmMapModel model, out string mssg)
        {
            switch (model.role)
            {
                case Role.superAdministratrorModel:

                case Role.administratrorModel:
                    if (!ProcessAdministratrorBLL.ProcessAdministratorXmnameDb(processUserXmDbCondition, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        dt = processUserXmDbCondition.dt;
                    }
                    break;
                case Role.monitorModel:
                    if (!ProcessMonitorBLL.ProcessMonitorXmnameDb(processUserXmDbCondition, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        dt = processUserXmDbCondition.dt;
                    }
                    break;
                case Role.superviseModel:
                    if (!ProcessSuperviseBLL.ProcessSupercviseXmnameDb(processUserXmDbCondition, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        dt = processUserXmDbCondition.dt;
                    }
                    break;
            }
        }
        public class ProcessXmMapModel 
        {
             public Role role { get; set; }
             public ProcessXmFilterCondition model { get; set; }
            public ProcessXmMapModel(ProcessXmFilterCondition model,Role role)
            {
               this.model = model;
                this.role = role;
            }
        }
    }
}