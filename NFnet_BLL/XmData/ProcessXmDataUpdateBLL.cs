﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.XmData
{
    public class ProcessXmDataUpdateBLL
    {
        public static SystemData.BLL.xmdataupdatetime xmdataupdatetimebll = new SystemData.BLL.xmdataupdatetime();
        public bool ProcessXmDataUpdateInfoLoad(ProcessXmDataUpdateInfoLoadModel model,out string mssg)
        {
            List<SystemData.Model.xmdataupdatetime> modelist = null;
            if (xmdataupdatetimebll.XmDataUpdateModelListLoad(model.unitname, out modelist, out mssg))
            {
                model.modelist = modelist;
                return true;
            }
            return false;
        }
        public class ProcessXmDataUpdateInfoLoadModel
        {
            public string unitname { get; set; }
            public List<SystemData.Model.xmdataupdatetime> modelist { get; set; }
            public ProcessXmDataUpdateInfoLoadModel(string unitname)
            {
                this.unitname = unitname;
            }
        }


    }
}