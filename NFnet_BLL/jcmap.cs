﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class jcmap
    {
        private string id, pointName, taskName, jclx, absX, absY;

        public string Jclx
        {
            get { return jclx; }
            set { jclx = value; }
        }

        public string AbsY
        {
            get { return absY; }
            set { absY = value; }
        }

        public string AbsX
        {
            get { return absX; }
            set { absX = value; }
        }

        public string TaskName
        {
            get { return taskName; }
            set { taskName = value; }
        }

        public string PointName
        {
            get { return pointName; }
            set { pointName = value; }
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}