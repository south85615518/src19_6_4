﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;
using NFnet.项目设置.clss;
using System.Data.Odbc;
using NFnet_DAL;
using NFnet_BLL;

namespace NFnet_BLL
{
    public class JclxServer
    {
        private string xmname;
        //获取监测类型
        //根据监测点判断所有的监测项目的点号是否为空决定该监测项目是否显示
        public static List<string> JclxGet(string xmname)
        {
            List<string> jclxList = new List<string>();
            database db = new database();
            OdbcConnection conn = db.GetStanderConn(xmname);

            string[] jclxArry = { "水位", "雨量", "深部位移", "表面位移", "沉降", "应力" };
            foreach (string jclx in jclxArry)
            {
                string tabName = JclxTabMap.GetPointTabName(jclx);
                string sql = "select count(*) from " + tabName;
                string cont = querysql.querystanderstr(sql, conn);
                if (cont != "0")
                {
                    jclxList.Add(jclx);
                }
            }

            return jclxList;
        }
    }
}