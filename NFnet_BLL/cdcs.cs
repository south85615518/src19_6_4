﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class cdcs
    {
        private string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        private string gname;

        public string Gname
        {
            get { return gname; }
            set { gname = value; }
        }

        private string cdname;//测点名

        public string Cdname
        {
            get { return cdname; }
            set { cdname = value; }
        }
        private string initval;//初始值

        public string Initval
        {
            get { return initval; }
            set { initval = value; }
        }
        private string dmname;//断面名称


        public string Dmname
        {
            get { return dmname; }
            set { dmname = value; }
        }
        private string bjjb;//预警级别

        public string Bjjb
        {
            get { return bjjb; }
            set { bjjb = value; }
        }
        private string xmname;//项目名称

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        private string clfs;

        public string Clfs
        {
            get { return clfs; }
            set { clfs = value; }
        }
        public cdcs(string id, string cdname, string initval, string dmname, string yjjb, string clfs, string xmname)
        {
            this.id = id;
            this.xmname = xmname;
            this.initval = initval;
            this.dmname = dmname;
            this.bjjb = yjjb;
            this.cdname = cdname;
            this.clfs = clfs;
        }
        public cdcs(string id, string cdname, int initval, string yjjb, string xmname)
        {
            this.id = id;
            this.xmname = xmname;
            this.initval = Convert.ToString(initval);
            this.bjjb = yjjb;
            this.cdname = cdname;
        }
        public cdcs(string id, string cdname, string yjjb, string xmname,string gname)
        {
            this.id = id;
            this.xmname = xmname;
            this.bjjb = yjjb;
            this.cdname = cdname;
            this.gname = gname;
        }
        public cdcs()
        {

        }
    }
}