﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using NFnet_DAL;
namespace NFnet_BLL
{
    /// <summary>
    /// 监测点设置实例类
    /// </summary>
    public class JcpmMapSet
    {
        private string xmname;//项目名称
        private string imgUrl;//监测图标路径
        private string yjInfo;//预警信息

        public string YjInfo
        {
            get { return yjInfo; }
            set { yjInfo = value; }
        }
        public string ImgUrl
        {
            get { return imgUrl; }
            set { imgUrl = value; }
        }
        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        private int id;//编号

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string jcdName;//监测点名

        public string JcdName
        {
            get { return jcdName; }
            set { jcdName = value; }
        }
        private string jclx;//监测类型

        public string Jclx
        {
            get { return jclx; }
            set { jclx = value; }
        }
        private string yq;//仪器

        public string Yq
        {
            get { return yq; }
            set { yq = value; }
        }
        private string groupName;//测斜组名

        public string GroupName
        {
            get { return groupName; }
            set { groupName = value; }
        }
        private string projectName;//全站仪工程名

        public string ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }
        private string taskName;

        public string TaskName
        {
            get { return taskName; }
            set { taskName = value; }
        }private int jcdMpID;//监测点平面图ID

        public int JcdMpID
        {
            get { return jcdMpID; }
            set { jcdMpID = value; }
        }

        private string absX;//在监测点平面图上的X坐标

        public string AbsX
        {
            get { return absX; }
            set { absX = value; }
        }
        private string absY;//在监测点平面图上的Y坐标

        public string AbsY
        {
            get { return absY; }
            set { absY = value; }
        }
        private double zoom;//放大倍数

        public double Zoom
        {
            get { return zoom; }
            set { zoom = value; }
        }
        public JcpmMapSet(string xmname, int id, string jcdName, string jclx, string yq, string groupName,
            string projectName, string taskName, int jcdMapID, string absX, string absY, double zoom)
        {
            this.xmname = xmname; this.id = id; this.jcdName = jcdName;
            this.jclx = jclx; this.yq = yq; this.groupName = groupName; this.projectName = projectName;
            this.taskName = taskName;
            this.jcdMpID = jcdMapID; this.AbsX = absX; this.absY = absY; this.zoom = zoom;
        }
        public JcpmMapSet(string xmname)
        {
            this.xmname = xmname;
        }
        public string InsertJcpmMapSet()
        {
            string sql = @"insert into jcmapset (id,JcdName,Jclx,Yq,GroupName,ProjectName,taskName,JcdMapID,AbsX,AbsY,Zoom)
values(" + this.id + ",'" + this.jcdName + "','" + this.jclx + "','" + this.yq + "','" + this.groupName + "','" + this.projectName + "','" + this.taskName + "','"
        + this.jcdMpID + "','" + this.absX + "','" + this.absY + "'," + this.zoom + ")";
            database db = new database();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);
            //返回插入的id值
            sql = "select id from jcmapset where absX='"+this.absX+"' and absY='"+this.AbsY+"'";
            string idret = querysql.querystanderstr(sql,conn);
            return idret;
        }
        public List<JcpmMapSet> GetEntrl(string sql)
        {
            database db = new database();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            DataTable dt = querysql.querystanderdb(sql,conn);
            DataView dv = new DataView(dt);
            List<JcpmMapSet> ljms = new List<JcpmMapSet>();
            foreach (DataRowView drv in dv)
            {
                JcpmMapSet jms = new JcpmMapSet(this.xmname,Int32.Parse(drv["id"].ToString()), drv["jcdname"].ToString(), drv["jclx"].ToString(), drv["yq"].ToString(), drv["GroupName"].ToString()
                     , drv["projectName"].ToString(), drv["taskName"].ToString(), Int32.Parse(drv["JcdMapID"].ToString()), drv["absX"].ToString(), drv["absY"].ToString(), double.Parse(drv["zoom"].ToString()));
                ljms.Add(jms);
            }
            return ljms;
        }
        public JcpmMapSet(string xmname,int jcdMpID)
        {
            this.xmname = xmname;
            this.jcdMpID = jcdMpID;
        }
    }
}