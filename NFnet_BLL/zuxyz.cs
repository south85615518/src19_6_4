﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    /// <summary>
    /// 保存显示的族的xyz的变量类型
    /// </summary>
    public class zuxyz
    {
        private string name;//默认是测量、本次、累计、平面偏移、沉降

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string[] bls;//默认是“X”，“Y”，“Z”

        public string[] Bls
        {
            get { return bls; }
            set { bls = value; }
        }
    }
}