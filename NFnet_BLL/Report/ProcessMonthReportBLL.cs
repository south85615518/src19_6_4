﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.Report
{
    public class ProcessMonthReportBLL
    {

        public AutoReport.BLL.automonthreport bll = new AutoReport.BLL.automonthreport();
        public bool Add(AutoReport.Model.automonthreport model, out string mssg)
        {
            return bll.Add(model, out mssg);
        }
        public bool ProcessmonthReportModelGet(ProcessmonthReportModelGetModel model, out string mssg)
        {
            AutoReport.Model.automonthreport monthreportmodel = null;
            if (bll.GetModel(model.xmno, model.month, model.hour, out monthreportmodel, out mssg))
            {
                model.model = monthreportmodel;
                return true;
            }
            return false;

        }
        public class ProcessmonthReportModelGetModel
        {
            public int xmno { get; set; }
            public int month { get; set; }
            public int hour { get; set; }
            public string jclx { get; set; }
            public AutoReport.Model.automonthreport model { get; set; }
            public ProcessmonthReportModelGetModel(int xmno, int month, int hour)
            {
                this.xmno = xmno;
                this.month = month;
                this.hour = hour;
            }

        }

        public bool ProcessXmmonthReportModelGet(ProcessXmmonthReportModelGetModel model, out string mssg)
        {
            AutoReport.Model.automonthreport monthreportmodel = null;
            if (bll.GetModel(model.xmno, out monthreportmodel, out mssg))
            {
                model.model = monthreportmodel;
                return true;
            }
            return false;

        }
        public class ProcessXmmonthReportModelGetModel
        {
            public int xmno { get; set; }
            public AutoReport.Model.automonthreport model { get; set; }
            public ProcessXmmonthReportModelGetModel(int xmno)
            {
                this.xmno = xmno;
            }

        }

    }
}