﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.Report
{
    public class ProcessWeekReportBLL
    {
        public AutoReport.BLL.autoweekreport bll = new AutoReport.BLL.autoweekreport();
        public bool Add(AutoReport.Model.autoweekreport model,out string mssg)
        {
            return bll.Add(model,out mssg);
        }
        public bool ProcessWeekReportModelGet(ProcessWeekReportModelGetModel model,out string mssg)
        {
            AutoReport.Model.autoweekreport weekreportmodel = null;
            if(bll.GetModel(model.xmno,model.week,model.hour,out weekreportmodel,out mssg))
            {
                model.model = weekreportmodel;
                return true;
            }
            return false;
            
        }
        public class ProcessWeekReportModelGetModel
        {
            public int xmno { get; set; }
            public int week { get; set; }
            public int hour { get; set; }
            public string jclx { get; set; }
            public AutoReport.Model.autoweekreport model { get; set; }
            public ProcessWeekReportModelGetModel(int xmno, int week, int hour)
            {
                this.xmno = xmno;
                this.week = week;
                this.hour = hour;
            }

        }
        public bool ProcessXmWeekReportModelGet(ProcessXmWeekReportModelGetModel model, out string mssg)
        {
            AutoReport.Model.autoweekreport weekreportmodel = null;
            if (bll.GetModel(model.xmno, out weekreportmodel, out mssg))
            {
                model.model = weekreportmodel;
                return true;
            }
            return false;

        }
        public class ProcessXmWeekReportModelGetModel
        {
            public int xmno { get; set; }
            public AutoReport.Model.autoweekreport model { get; set; }
            public ProcessXmWeekReportModelGetModel(int xmno)
            {
                this.xmno = xmno;
            }

        }      


     

    }
}