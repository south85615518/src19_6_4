﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuthorityAlarm.BLL;
using System.Data;
using Tool;

namespace NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration
{
    public class ProcessSmsSendRecordBLL
    {
        public static smssendrecord smssendrecordbll = new smssendrecord();
        public bool ProcessSmsSendRecordAdd(AuthorityAlarm.Model.smssendrecord model,out string mssg)
        {
           return smssendrecordbll.Add(model,out mssg);
        }
        
        /// <summary>
        /// 更新一条数据
        /// </summary>
        //public bool Update(AuthorityAlarm.Model.smssendrecord model, out string mssg)
        //{
        //    return smssendrecordbll.Update(model,out mssg);
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(SmsSendRecordModelGetModel model, out string mssg)
        {
            AuthorityAlarm.Model.smssendrecord smssendrecordmodel = new AuthorityAlarm.Model.smssendrecord();
            if (smssendrecordbll.GetModel(model.id, model.unitname, out smssendrecordmodel, out mssg))
            {
                model.model = smssendrecordmodel;
                return true;
            }
            return false;
        }
        public class SmsSendRecordModelGetModel
        {
            public int id { get; set; }
            public string unitname { get; set; }
            public AuthorityAlarm.Model.smssendrecord model { get; set; }
            public SmsSendRecordModelGetModel(int id,string unitname)
            {
                this.id = id;
                this.unitname = unitname;
            }
        }



        public bool smssendrecordTableLoad(smssendrecordTableLoadModel model,out string mssg)
        {
            DataTable dt = new DataTable();
            ExceptionLog.ExceptionWrite("起始页:" + model.startPageIndex + "行:" + model.pageSize);
            if (smssendrecordbll.smssendrecordTableLoad(model.unitname, model.xmno, model.starttime, model.endtime, model.keyword, model.startPageIndex, model.pageSize, model.sordname, model.sord, out dt, out mssg)) {
                model.dt = dt;
                return true;
               }
            return false;
        }
        public class smssendrecordTableLoadModel
        {
                public string unitname{get; set;}
                public int xmno{get; set;}
                public DateTime starttime{get; set;}
                public DateTime endtime{get; set;} 
                public string keyword{get; set;}
                public int startPageIndex{get; set;}
                public int pageSize{get; set;} 
                public string sordname{get; set;}
                public string sord{get; set;}
                public DataTable dt { get; set; }
                public smssendrecordTableLoadModel(string unitname,int xmno, DateTime starttime, DateTime endtime, string keyword,int startPageIndex,int pagesize,string sordname,string sord)
                {
                    this.unitname = unitname;
                    this.xmno = xmno;
                    this.starttime = starttime;
                    this.endtime = endtime;
                    this.keyword = keyword;
                    this.startPageIndex = startPageIndex;
                    this.pageSize = pagesize;
                    this.sordname = sordname;
                    this.sord = sord;
                }

        }

        public bool smssendrecordTableCountLoad(smssendrecordTableCountLoadModel model, out string mssg)
        {
            int cont = 0;
            if (smssendrecordbll.smssendrecordTableLoadCount(model.unitname,  model.xmno,  model.starttime, model.endtime, model.keyword ,out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            return false;
        }
        public class smssendrecordTableCountLoadModel
        {
            public int cont { get; set; }
            public string unitname { get; set; }
            public int xmno { get; set; }
            public DateTime starttime { get; set; }
            public DateTime endtime { get; set; }
            public string keyword { get; set; }
            public smssendrecordTableCountLoadModel(string unitname, int xmno,  DateTime starttime, DateTime endtime, string keyword)
            {
                this.unitname = unitname;
                this.xmno = xmno;
                this.starttime = starttime;
                this.endtime = endtime;
                this.keyword = keyword;
               
            }

        }
    }
}