﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public partial class ProcessalarmsplitondateBLL
    {
       
        public bool ProcesscgalarmsplitondateAdd(AuthorityAlarm.Model.alarmsplitondate model, out string mssg)
        {
            return cgalarmBLL.CgAdd(model, out mssg);
        }
        public bool ProcesscgalarmsplitondateDelete(ProcessalarmsplitondateDeleteModel model, out string mssg)
        {

            return cgalarmBLL.CgDelete(model.xmno, model.jclx, model.pointName, model.time, out mssg);
        }
     
        public bool Processcgalarmcgsplitondatereaded(ProcessAlarmSplitOnDateReadedModel model, out string mssg)
        {

            return cgalarmBLL.cgalarmcgsplitondatereaded(model.xmno, model.dt, out mssg);
        }

        /// <summary>
        /// 获取项目某一天的预警详情
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcesscgalarmsplitondateModelList(ProcessDateAlarmSplitOnDateModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.alarmsplitondate> ls = null;
            mssg = "";
            if (cgalarmBLL.CgGetModelList(model.xmno, model.dt, model.keyWord, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
        public bool ProcessCgAlarmSplitOnDateModelList(ProcessAlarmSplitOnDateModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.alarmsplitondate> ls = null;
            mssg = "";
            if (cgalarmBLL.CgGetModelList(model.xmno, model.startTime, model.endTime, model.keyWord, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
        
        public bool ProcessCgAlarmOnDateSplit(ProcessAlarmOnDateSplitModel model, out string mssg)
        {
            mssg = "";
            int cnt = 0;
            List<string> ls = new List<string>();
            if (model.ls.Count == 0) return false;
            model.modells = new List<_alarmsplitondateModel>();
            var readSplitOnDate = new List<AuthorityAlarm.Model.alarmsplitondate>();
            AuthorityAlarm.Model.alarmsplitondate tmp = model.ls[0];
            foreach (AuthorityAlarm.Model.alarmsplitondate cgalarm in model.ls)
            {
                if (tmp.adate != cgalarm.adate)
                {
                    readSplitOnDate = (from m in model.ls where (m.adate == tmp.adate && m.readed == false) select m).ToList();
                    model.modells.Add(new _alarmsplitondateModel(tmp.adate.ToString(), readSplitOnDate.Count, cnt, string.Join("\n", ls), tmp.cont));

                    ls = new List<string>();
                    cnt = 0;

                }
                cnt++;
                ls.Add(cgalarm.alarmContext);
                tmp = cgalarm;
            }
            readSplitOnDate = (from m in model.ls where (m.adate == tmp.adate && m.readed == false) select m).ToList();
            model.modells.Add(new _alarmsplitondateModel(tmp.adate.ToString(), readSplitOnDate.Count, cnt, string.Join("\n", ls), tmp.cont));
            return true;
        }
        
        public bool ProcessCgAlarmOnDateSplitUnionSend(ProcessCgAlarmOnDateSplitUnionSendModel model, out string mssg)
        {
            int sec = 0;
            model.messlist = new List<string>();
            mssg = "";
            foreach (string dt in model.dtSelected)
            {
                string txtContext = string.Format("======{0}==={1}===<p>", dt, DateTime.Now);

                var modellistloadmodel = new ProcessalarmsplitondateBLL.ProcessDateAlarmSplitOnDateModelListModel(model.xmno, Convert.ToDateTime(dt), "");
                //List<AuthorityAlarm.Model.alarmsplitondate> ls = model.ls.Where(s => (s.adate == dt)).ToList();

                modellistloadmodel.dt = Convert.ToDateTime(dt);

                ProcessalarmsplitondateModelList(modellistloadmodel, out mssg);
                ExceptionLog.ExceptionWrite(string.Join(",", modellistloadmodel.ls.Count) + "|" + mssg);
                var lis = (from m in modellistloadmodel.ls where m.adate == Convert.ToDateTime(dt) select m.alarmContext).ToList();
                model.messlist.AddRange(lis);

            }
            return true;
            //    lis.Insert(0,string.Format("{0}预警信息通知", model.xmname));
            //    //txtContext += string.Join("\n", lis);
            //    ExceptionLog.ExceptionWrite(string.Join(",",lis));
            //    int t = 0;
            //    while (t < 3)
            //    {
            //        //重发三次
            //        if (!processSmsSendBLL.ProcessSmsSend(lis,  model.xmno,model.monitorList, out mssg))
            //        {
            //            t++;
            //        }
            //        else
            //        {
            //            //更新发送数量
            //            AuthorityAlarm.Model.alarmdatemailsendrecord mailsendrecord = new AuthorityAlarm.Model.alarmdatemailsendrecord
            //            {
            //                did = string.Format("D{0}", DateHelper.DateTimeToString(Convert.ToDateTime(dt).AddSeconds(++sec))),
            //                sdate = Convert.ToDateTime(dt),
            //                xmno = model.xmno
            //            };
            //            if (!mailSendRecordBLL.ProcessAlarmDateMailSendRecordAdd(mailsendrecord, out mssg))
            //            {
            //                //更新分包发送记录失败
            //            }
            //            break;

            //        }



            //    }
            //}
            ////string.Join(",", resultDataBLL.alarmInfoList.Where(s => (s != "")).ToList())
            //mssg = string.Format("发送{0}{1}预警信息邮件成功!", model.xmname, string.Join(",", model.dtSelected));
            //return true;


        }

       
        


    }
}