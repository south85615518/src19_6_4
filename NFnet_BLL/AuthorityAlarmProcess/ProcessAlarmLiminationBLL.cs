﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuthorityAlarm.BLL;
using System.Data;
using Tool;

namespace NFnet_BLL.AuthorityAlarmProcess.AchievementGeneration
{
    public class ProcessAlarmLiminationBLL
    {
        public static alarmelimination alarmeliminationbll = new alarmelimination();
        public bool ProcessAlarmLiminationAdd(AuthorityAlarm.Model.alarmelimination model,out string mssg)
        {
           return alarmeliminationbll.Add(model,out mssg);
        }
        
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(AuthorityAlarm.Model.alarmelimination model, out string mssg)
        {
            return alarmeliminationbll.Update(model,out mssg);
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(AlarmLiminationModelGetModel model, out string mssg)
        {
            AuthorityAlarm.Model.alarmelimination alarmeliminationmodel = new AuthorityAlarm.Model.alarmelimination();
            if (alarmeliminationbll.GetModel(model.id, model.unitname, out alarmeliminationmodel, out mssg))
            {
                model.model = alarmeliminationmodel;
                return true;
            }
            return false;
        }
        public class AlarmLiminationModelGetModel
        {
            public int id { get; set; }
            public string unitname { get; set; }
            public AuthorityAlarm.Model.alarmelimination model { get; set; }
            public AlarmLiminationModelGetModel(int id,string unitname)
            {
                this.id = id;
                this.unitname = unitname;
            }
        }



        public bool AlarmEliminationTableLoad(AlarmEliminationTableLoadModel model,out string mssg)
        {
            DataTable dt = new DataTable();
            ExceptionLog.ExceptionWrite("起始页:" + model.startPageIndex + "行:" + model.pageSize);
            if (alarmeliminationbll.AlarmEliminationTableLoad(model.unitname, model.monitorID, model.xmno, model.jclx, model.pointname, model.starttime, model.endtime, model.keyword, model.startPageIndex, model.pageSize, model.sordname, model.sord, out dt, out mssg)) {
                model.dt = dt;
                return true;
               }
            return false;
        }
        public class AlarmEliminationTableLoadModel
        {
                public string unitname{get; set;}
                public string monitorID{get; set;} 
                public int xmno{get; set;}
                public string jclx{get; set;}
                public string pointname{get; set;}
                public DateTime starttime{get; set;}
                public DateTime endtime{get; set;} 
                public string keyword{get; set;}
                public int startPageIndex{get; set;}
                public int pageSize{get; set;} 
                public string sordname{get; set;}
                public string sord{get; set;}
                public DataTable dt { get; set; }
                public AlarmEliminationTableLoadModel(string unitname,string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword,int startPageIndex,int pagesize,string sordname,string sord)
                {
                    this.unitname = unitname;
                    this.monitorID = monitorID;
                    this.xmno = xmno;
                    this.jclx = jclx;
                    this.pointname = pointname;
                    this.starttime = starttime;
                    this.endtime = endtime;
                    this.keyword = keyword;
                    this.startPageIndex = startPageIndex;
                    this.pageSize = pagesize;
                    this.sordname = sordname;
                    this.sord = sord;
                }

        }

        public bool AlarmEliminationTableCountLoad(AlarmEliminationTableCountLoadModel model, out string mssg)
        {
            int cont = 0;
            if (alarmeliminationbll.AlarmEliminationTableLoadCount(model.unitname, model.monitorID, model.xmno, model.jclx, model.pointname, model.starttime, model.endtime, model.keyword ,out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            return false;
        }
        public class AlarmEliminationTableCountLoadModel
        {
            public int cont { get; set; }
            public string unitname { get; set; }
            public string monitorID { get; set; }
            public int xmno { get; set; }
            public string jclx { get; set; }
            public string pointname { get; set; }
            public DateTime starttime { get; set; }
            public DateTime endtime { get; set; }
            public string keyword { get; set; }
            public AlarmEliminationTableCountLoadModel(string unitname, string monitorID, int xmno, string jclx, string pointname, DateTime starttime, DateTime endtime, string keyword)
            {
                this.unitname = unitname;
                this.monitorID = monitorID;
                this.xmno = xmno;
                this.jclx = jclx;
                this.pointname = pointname;
                this.starttime = starttime;
                this.endtime = endtime;
                this.keyword = keyword;
               
            }

        }
    }
}