﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public class RolePointAlarmCondition
    {
        public int xmno { get; set; }
        public string jclx { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
        public int alarm { get; set; }
        public RolePointAlarmCondition(int xmno,string jclx,string pointname)
        {
            this.xmno = xmno;
            this.jclx = jclx;
            this.pointname = pointname;
        }
    }
}