﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public class XmHightestAlarmCondition
    {
        public int xmno { get; set; }
        public int alarm { get; set; }
        public XmHightestAlarmCondition(int xmno)
        {
            this.xmno = xmno;
        }
    }
}