﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public class RolePointAlarmCom
    {
        public ProcessCgAlarmBLL cgAlarmBLL = new ProcessCgAlarmBLL();
        public ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public bool ProcessPointAlarm(ProcessPointAlarmModel model, out string mssg)
        {
            if (model.role == Role.superviseModel)
            {

                return cgAlarmBLL.ProcessPointAlarmModelGet(model.condition, out mssg);

            }
            return pointCheckBLL.ProcessPointCheckModelGet(model.condition, out mssg);
        }

        public class ProcessPointAlarmModel
        {
            public RolePointAlarmCondition condition { get; set; }
            public Role role { get; set; }
            public ProcessPointAlarmModel(RolePointAlarmCondition condition, Role role)
            {
                this.condition = condition;
                this.role = role;
            }
        }
    }

}