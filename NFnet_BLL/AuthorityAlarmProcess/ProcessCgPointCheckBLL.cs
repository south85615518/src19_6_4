﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using NFnet_BLL.LoginProcess;

namespace NFnet_BLL.AuthorityAlarmProcess
{

    public partial class ProcessPointCheckBLL
    {
       

        public bool ProcessCgPointCheckAdd(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            return pointCheck.CgAdd(model, out mssg);
        }

        public bool ProcessCgPointCheckUpdate(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            return pointCheck.CgUpdate(model, out mssg);
        }

        public bool ProcessCgPointCheckDelete(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            return pointCheck.CgDelete(model,out mssg);
        }


        public bool ProcessCgXmHightestAlarm(XmHightestAlarmCondition model, out string mssg)
        {
            int alarm = 0;
            if (pointCheck.CgXmHighestAlarm(model.xmno, out alarm, out mssg))
            {
                model.alarm = alarm;
                return true;
            }
            else
            {
                return false;
            }

        }


        public string ProcessCgXmAlarmColor(int xmno, out string mssg)
        {

            var processXmHightestAlarmModel = new XmHightestAlarmCondition(xmno);
            ProcessCgXmHightestAlarm(processXmHightestAlarmModel, out mssg);
            return DataProcessHelper.ColorDispaly(processXmHightestAlarmModel.alarm);


        }

        public bool ProcessCgPointCheckModelList(ProcessPointCheckModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.pointcheck> modelList = null;

            if (pointCheck.CgGetModelList(model.xmno, out modelList, out mssg))
            {
                model.modelList = modelList;
                return true;
            }
            return false;

        }
       
        public bool ProcessCgXmCheckCont(XmAlarmContCondition model, out string mssg)
        {
            int cont = 0;
            if (pointCheck.CgXmAlarmCount(model.xmno, out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessCgPointCheckModelGet(RolePointAlarmCondition model, out string mssg)
        {
            AuthorityAlarm.Model.pointcheck checkModel = null;
            if (pointCheck.CgGetModel(model.xmno,model.pointname,model.jclx,out checkModel,out mssg))
            {
                model.dt = checkModel.time;
                model.alarm = checkModel.alarm;
                return true;
            }
            return false;
        }


        public bool ProcessCgXmAlarmCount(ProcessXmAlarmCountModel model, out string mssg)
        {
           int cont = 0;
           if (pointCheck.CgXmAlarmCount(model.xmno, model.type, out cont, out mssg))
           {
               model.cont = cont;
               return true;
           }
           return false;

        }
      


        public bool ProcessCgMaxAlarm(ProcessMaxAlarmModel model, out string mssg)
        {

            int alarm = 0;
            if (pointCheck.CgMaxAlarm(model.xmno, model.type, out alarm, out mssg))
            {
                model.alarm = alarm;
                return true;
            }
            return false;
          
        }

    }
}