﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using NFnet_BLL.LoginProcess;
using System.Data;

namespace NFnet_BLL.AuthorityAlarmProcess
{

    public partial class ProcessPointCheckBLL
    {
        public AuthorityAlarm.BLL.pointcheck pointCheck = new AuthorityAlarm.BLL.pointcheck();

        public bool ProcessPointCheckAdd(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            return pointCheck.Add(model, out mssg);
        }

        public bool ProcessPointCheckUpdate(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            return pointCheck.Update(model, out mssg);
        }

        public bool ProcessPointCheckDelete(AuthorityAlarm.Model.pointcheck model, out string mssg)
        {
            return pointCheck.Delete(model,out mssg);
        }


        public bool ProcessXmHightestAlarm(XmHightestAlarmCondition model, out string mssg)
        {
            int alarm = 0;
            if (pointCheck.XmHighestAlarm(model.xmno, out alarm, out mssg))
            {
                model.alarm = alarm;
                return true;
            }
            else
            {
                return false;
            }

        }


        public string ProcessXmAlarmColor(int xmno, out string mssg)
        {

            var processXmHightestAlarmModel = new XmHightestAlarmCondition(xmno);
            ProcessXmHightestAlarm(processXmHightestAlarmModel, out mssg);
            return DataProcessHelper.ColorDispaly(processXmHightestAlarmModel.alarm);


        }

        public bool ProcessPointCheckModelList(ProcessPointCheckModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.pointcheck> modelList = null;

            if (pointCheck.GetModelList(model.xmno, out modelList, out mssg))
            {
                model.modelList = modelList;
                return true;
            }
            return false;

        }
        public class ProcessPointCheckModelListModel
        {
            public int xmno { get; set; }
            public List<AuthorityAlarm.Model.pointcheck> modelList { get; set; }
            public ProcessPointCheckModelListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        public bool ProcessXmCheckCont(XmAlarmContCondition model, out string mssg)
        {
            int cont = 0;
            if (pointCheck.XmAlarmCount(model.xmno, out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessPointCheckModelGet(RolePointAlarmCondition model, out string mssg)
        {
            AuthorityAlarm.Model.pointcheck checkModel = null;
            if (pointCheck.GetModel(model.xmno,model.pointname,model.jclx,out checkModel,out mssg))
            {
                model.dt = checkModel.time;
                model.alarm = checkModel.alarm;
                return true;
            }
            return false;
        }


        public bool ProcessXmAlarmCount(ProcessXmAlarmCountModel model, out string mssg)
        {
           int cont = 0;
           if (pointCheck.XmAlarmCount(model.xmno, model.type, out cont, out mssg))
           {
               model.cont = cont;
               return true;
           }
           return false;

        }
        public class ProcessXmAlarmCountModel
        {
            public int xmno { get; set; }
            public string type { get; set; }
            public int cont { get; set; }
            public ProcessXmAlarmCountModel(int xmno,string type)
            {
                this.xmno = xmno;
                this.type = type;
            }
        }


        public bool ProcessMaxAlarm(ProcessMaxAlarmModel model, out string mssg)
        {

            int alarm = 0;
            if (pointCheck.MaxAlarm(model.xmno, model.type, out alarm, out mssg))
            {
                model.alarm = alarm;
                return true;
            }
            return false;
          
        }
        public class ProcessMaxAlarmModel
        {
            public int xmno { get; set; }
            public string type { get; set; }
            public int alarm { get; set; }
            public ProcessMaxAlarmModel(int xmno,string type)
            {
                this.xmno = xmno;
                this.type = type;
            }
        }

        public bool ProcessAlarmContLayout(ProcessAlarmContLayoutModel model,out string mssg)
        {
            string alarmcontlayout = "0/0/0/0";
            if(pointCheck.AlarmContLayout(model.unitname,out alarmcontlayout,out mssg))
            {
                model.alarmcontlayout = alarmcontlayout;
                return true;
            }
            return false;
        }
        public bool ProcessXmAlarmContLayout(ProcessXmAlarmContLayoutModel model, out string mssg)
        {
            string alarmcontlayout = "0/0/0/0";
            if (pointCheck.AlarmContLayout(model.xmno, out alarmcontlayout, out mssg))
            {
                model.alarmcontlayout = alarmcontlayout;
                return true;
            }
            return false;
        }
        public class ProcessAlarmContLayoutModel
        {
            public string unitname { get; set; }
            public string alarmcontlayout { get; set; }
            public ProcessAlarmContLayoutModel(string unitname)
            {
                this.unitname = unitname;
            }
        }
        public class ProcessXmAlarmContLayoutModel
        {
            public int xmno { get; set; }
            public string alarmcontlayout { get; set; }
            public ProcessXmAlarmContLayoutModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool ProcessAlarmPointLoad(ProcessAlarmPointLoadModel model,out string mssg)
        {
            DataTable dt = null;
            if (pointCheck.AlarmPointLoad(model.xmno, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessAlarmPointLoadModel
        {
            public int xmno { get; set; }
            public DataTable dt { get; set; }
            public ProcessAlarmPointLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool ProcessUnitAlarmXmnoLoad(ProcessUnitAlarmXmnoLoadModel model, out string mssg)
        {
            List<string> xmnolist = null;
            if (pointCheck.AlarmXmnoGet(model.unitname, out xmnolist, out mssg))
            {
                model.xmnolist = xmnolist;
                return true;
            }
            return false;
        }
        public class ProcessUnitAlarmXmnoLoadModel
        {
            public string unitname { get; set; }
            public List<string> xmnolist { get; set; }
            public ProcessUnitAlarmXmnoLoadModel(string unitname)
            {
                this.unitname = unitname;
            }
        }


    }
}