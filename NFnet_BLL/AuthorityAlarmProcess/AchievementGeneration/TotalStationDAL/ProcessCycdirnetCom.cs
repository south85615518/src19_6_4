﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;

namespace NFnet_BLL.AchievementGeneration.TotalStationDAL
{
    
    public class ProcessCycdirnetCom
    {
        public ProcessCycdirnetBLL cycdirnetBLL = new ProcessCycdirnetBLL();
        public ProcessCycdirnetCgBLL cycdirnetCgBLL = new ProcessCycdirnetCgBLL();

        public bool ProcessCycdirnetModel(ProcessCycdirnetModelofModel model, out string mssg)
        {
            if (model.role == Role.superviseModel)
            {
                return cycdirnetCgBLL.ProcessCycdirnetModel(model.model,out mssg);
            }
            else
            {
                return cycdirnetBLL.ProcessCycdirnetModel(model.model,out mssg);
            }
        }
         public class ProcessCycdirnetModelofModel
         {
             public CycdirnetTimeModelGetCondition model { get; set; }
             public Role role { get; set; }
             public ProcessCycdirnetModelofModel(CycdirnetTimeModelGetCondition model, Role role)
             {
                 this.model = model;
                 this.role = role;
             }
         }
    }
}