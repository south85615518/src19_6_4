﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.Model.fmos_obj;
using System.Data;



namespace NFnet_BLL.AchievementGeneration.TotalStationDAL
{
    public class ProcessCycdirnetCgBLL
    {
        public TotalStationCgDAL.BLL.cycdirnet cycdirnetBLL = new TotalStationCgDAL.BLL.cycdirnet();

        /// <summary>
        /// 计算重新计算后一个周期的本次变化量
        /// </summary>
        /// <param name="modelca">前一个周期</param>
        /// <param name="modelcb">后一个周期</param>
        /// <returns></returns>
        public void cycdirnetThisCalculation(cycdirnet modelca, cycdirnet modelcb)
        {
            if (modelca != null)
            {
                modelcb.This_dN = double.Parse(((modelcb.N - modelca.N) * 1000).ToString("0.00"));
                modelcb.This_dE = double.Parse(((modelcb.E - modelca.E) * 1000).ToString("0.00"));
                modelcb.This_dZ = double.Parse(((modelcb.Z - modelca.Z) * 1000).ToString("0.00"));
            }

        }
        /// <summary>
        /// 计算重新计算后的一个周期的累计变化量
        /// </summary>
        /// <param name="modelca">后一个周期</param>
        /// <param name="modelcb">基准周期</param>
        public void cycdirnetAcCalculation(cycdirnet modelca, cycdirnet modelcb)
        {
            modelcb.Ac_dN = modelcb.N - modelca.N;
            modelcb.Ac_dE = modelcb.E - modelca.E;
            modelcb.Ac_dZ = modelcb.Z - modelca.Z;
        }

        /// <summary>
        /// 成果数据添加日期合法检查
        /// </summary>
        /// <returns></returns>
        public bool ProcessCgCycdirnetExist(ProcessCgCycdirnetExistModel model, out string mssg)
        {
            return cycdirnetBLL.IsPexCycdirnetExist(model.xmname, model.pointname, model.cyc, model.dt, out mssg);
        }
        public class ProcessCgCycdirnetExistModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public int cyc { get; set; }
            public string dt { get; set; }
            public ProcessCgCycdirnetExistModel(string xmname, string pointname, int cyc, string dt)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.cyc = cyc;
                this.dt = dt;
            }
        }


        /// <summary>
        /// 成果表中项目名称和点名周期的记录是否存在
        /// </summary>
        /// <returns></returns>
        public bool ProcessCgPointnameCycdirnetExist(ProcessCgPointnameCycdirnetExistModel model, out string mssg)
        {
            return cycdirnetBLL.IsCycdirnetExist(model.xmname, model.pointname, model.cyc, out mssg);
        }
        public class ProcessCgPointnameCycdirnetExistModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public int cyc { get; set; }
            public ProcessCgPointnameCycdirnetExistModel(string xmname, string pointname, int cyc)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.cyc = cyc;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ProcessCgCycdirnetAdd(cycdirnet model, out string mssg)
        {
            
            return cycdirnetBLL.Add(model, out mssg);
        }
        /// <summary>
        /// 删除成果库中的某条记录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessCgCycdirnetDel(ProcessCgCycdirnetDelModel model, out string mssg)
        {
            return cycdirnetBLL.Delete(model.taskName,model.cyc,model.pointname, out mssg);
        }

        public class ProcessCgCycdirnetDelModel
        {
            public string taskName { get; set; }
            public int cyc { get; set; }
            public string pointname { get; set; }
            public ProcessCgCycdirnetDelModel(string taskName, int cyc, string pointname)
            {
                this.taskName = taskName;
                this.cyc = cyc;
                this.pointname = pointname;
            }
        }

        public bool ProcessCgPointNameResultDataLoad(CgPointNameResultDataLoadModel model,out string mssg)
        {

            DataTable dt = null;
            if (cycdirnetBLL.CgPointNameResultDataLoad(1, 9999, model.xmname, model.pointName, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }

            return false;
        }
        public class CgPointNameResultDataLoadModel {

            public string xmname { get; set; }
            public string pointName { get; set; }
            public DataTable dt { get; set; }
            public CgPointNameResultDataLoadModel(string xmname,string pointName)
            {
                this.xmname = xmname;
                this.pointName = pointName;
            }

        }

        /// <summary>
        /// 获取成果库中的数据记录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessCycdirnetModel(ProcessCycdirnetModelGetModel model, out string mssg)
        {
            TotalStation.Model.fmos_obj.cycdirnet cycdirnet = null;
            mssg = "";
            if (cycdirnetBLL.GetModel(model.xmname, model.pointname, model.cyc, out cycdirnet, out mssg))
            {
                model.model = cycdirnet;
                return true;
            }
            return false;
        }
        public class ProcessCycdirnetModelGetModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public int cyc { get; set; }
            public TotalStation.Model.fmos_obj.cycdirnet model { get; set; }
            public ProcessCycdirnetModelGetModel(string xmname, string pointname, int cyc)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.cyc = cyc;
            }
        }

        public bool ProcessCycdirnetModel(CycdirnetTimeModelGetCondition model, out string mssg)
        {
            TotalStation.Model.fmos_obj.cycdirnet cycdirnet = null;
            mssg = "";
            if (cycdirnetBLL.GetModel(model.xmname, model.pointname, model.dt, out cycdirnet, out mssg))
            {
                model.model = cycdirnet;
                return true;
            }
            return false;
        }


       


    }
}