﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TotalStation.BLL.fmos_obj;
using System.Data;
using NFnet_BLL.DataProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.XmInfo;


namespace NFnet_BLL.AchievementGeneration.TotalStationDAL
{
    public class ProcessCycdirnetBLL
    {

        public cycdirnet cycdirnetBLL = new cycdirnet();

        public bool ProcessCycdirnetModel(ProcessCycdirnetModelGetModel model,out string mssg)
        {
            TotalStation.Model.fmos_obj.cycdirnet cycdirnet = null;
            mssg = "";
            if (cycdirnetBLL.GetModel(model.xmname, model.pointname, model.cyc, out cycdirnet, out mssg))
            {
                model.model = cycdirnet;
                return true;
            }
            return false;
        }




        public class ProcessCycdirnetModelGetModel
        {
            public string xmname { get; set; }
            public string pointname { get; set; }
            public int cyc { get; set; }
            public TotalStation.Model.fmos_obj.cycdirnet model { get; set; }
            public ProcessCycdirnetModelGetModel(string xmname, string pointname, int cyc)
            {
                this.xmname = xmname;
                this.pointname = pointname;
                this.cyc = cyc;
            }
        }

        public bool ProcessCycdirnetModel(CycdirnetTimeModelGetCondition model, out string mssg)
        {
            TotalStation.Model.fmos_obj.cycdirnet cycdirnet = null;
            mssg = "";
            if (cycdirnetBLL.GetModel(model.xmname, model.pointname, model.dt, out cycdirnet, out mssg))
            {
                model.model = cycdirnet;
                return true;
            }
            return false;
        }



        /// <summary>
        /// 成果数据更新日期合法检查
        /// </summary>
        /// <returns></returns>
        public bool ProcessCgCycdirnetUpdateDateValidate()
        {
            return false;
        }
        public static ProcessResultDataBLL resultDataBLL = new ProcessResultDataBLL();
        //获取时间范围内的测量库中的数据表
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <param name="cyc">0/最大周期</param>
        /// <returns></returns>
        public DataTable ProcessCycdirnetDataTableLoad(string xmname, string pointname,int cyc)
        {
            
            string mssg = "";
            DateTime dtEx = new DateTime();
            DateTime dtNext = new DateTime();
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModelEx = new TotalStation.Model.fmos_obj.cycdirnet();
            //成果库选定周期的后一个周期对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModelNext = new TotalStation.Model.fmos_obj.cycdirnet();
            List<string> cgCycList = CgCycdirnetDataList(xmname, pointname);
            var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("0001-1-1"));
            int cont = 0;
            DataTable dt = null;
            int index = cgCycList.IndexOf(cyc.ToString());
            //如果选定周期等于0则列出所有数据并选择选择一条数据加入成果库
            if (cyc != 0 && index == -1)
            {
                dt = CycdirnetDataTable(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("0001-1-1"), out mssg);
            }
            else
            {
                cycdirnetModelEx = CgCycdirnet(xmname, pointname, cyc - 1);
                cycdirnetModelNext = CgCycdirnet(xmname, pointname, cyc + 1);
                dtEx = cycdirnetModelEx == null ? Convert.ToDateTime("0001-1-1") : cycdirnetModelEx.Time;
                dtNext = cycdirnetModelNext == null ? Convert.ToDateTime("9999-1-1") : cycdirnetModelNext.Time;
                dt = CycdirnetDataTable(xmname, pointname, dtEx, dtNext, out mssg);
            }
                return dt;
           


        }

        public List<string> CgCycdirnetDataList(string xmname, string pointname)
        {
            //string mssg = "";
            //var processCgResultDataCycLoadModel = new ProcessCgResultDataBLL.ProcessCgResultDataCycLoadModel(xmname, pointname);
            //if (!ProcessCgResultDataBLL.ProcessCgResultDataCycLoad(processCgResultDataCycLoadModel, out mssg))
            //{

            //    //return;
            //}
            //return processCgResultDataCycLoadModel.ls;
            return null;
        }


        public static ProcessCycdirnetCgBLL cycdirnetCgBLL = new ProcessCycdirnetCgBLL();
        //获取成果库指定周期的数据对象
        public TotalStation.Model.fmos_obj.cycdirnet CgCycdirnet(string xmname, string pointname, int cyc)
        {
            string mssg = "";
            var processCycdirnetModelGetModel = new ProcessCycdirnetCgBLL.ProcessCycdirnetModelGetModel(xmname, pointname, cyc);
            if (!cycdirnetCgBLL.ProcessCycdirnetModel(processCycdirnetModelGetModel, out mssg))
            {


                //return;
            }
            return processCycdirnetModelGetModel.model;
        }

        //获取时间范围内的测量库中的数据表
        public DataTable CycdirnetDataTable(string xmname, string pointname, DateTime startTime, DateTime endTime,out string mssg)
        {
            
            var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointname, Convert.ToDateTime(startTime), Convert.ToDateTime(endTime));
            if (!resultDataBLL.ProcessResultDataSearch(processResultData, out mssg))
            {
                //Console.WriteLine("获取{0}测量库中的数据失败", pointname);

            }
        
            
            return processResultData.dt;
        }
        public ProcessResultDataAlarmBLL cgResultDataAlarmBLL = new ProcessResultDataAlarmBLL();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xname"></param>
        /// <param name="pointname"></param>
        /// <param name="cgCyc">成果库选定的周期</param>
        /// <param name="cyc"></param>
        public bool ProcessCgCycdirnetAdd(string xmname,int xmno,string pointname, int cycdirnetCyc)
        {
            return false;
            /*//成果周期列表
            List<string> cgCycList = new List<string>();
            //测量周期列表
            List<string> cycList = new List<string>();
            //成果库选定周期前周期的数据对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModelEx = new TotalStation.Model.fmos_obj.cycdirnet();
            //成果库选定周期的数据对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModel = new TotalStation.Model.fmos_obj.cycdirnet();
            //成果库选定周期的后一个周期对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModelNext = new TotalStation.Model.fmos_obj.cycdirnet();
            int cyc = 0;
            //测量库数据表
            DataTable dt = null;
            //测量库中的数据周期列表
            List<string> ls = new List<string>();
            //测量库中选中要加入成果库的数据周期
            //int cycdirnetCyc = 0;
            //测量库中选中的数据对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetTarget = new TotalStation.Model.fmos_obj.cycdirnet();
            //成果周期选定的序号
            int index = 0;
              
               
                cgCycList = CgCycdirnetDataList(xmname, pointname);
               
                 if (cgCycList.Count == 0) cyc = 1; else cyc = Convert.ToInt32(cgCycList[cgCycList.Count - 1]) + 1;

                //获取成果库中该周期的数据
                cycdirnetModel = CgCycdirnet(xmname, pointname, cyc);


                var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("0001-1-1"));
                int cont = 0;

                //如果选定周期等于0则列出所有数据并选择选择一条数据加入成果库
                if (cyc == 1)
                {
                   

                    
                    //插入第1周期的数据
                    cycdirnetTarget = Cycdirnet(xmname, pointname, cycdirnetCyc);
                    cycdirnetTarget.CYC = 1;
                    if (CgCycdirnetAdd(null, cycdirnetTarget))
                    {
                        cgResultDataAlarmBLL.ProcessCycdirnetPointAlarmBLL(xmno,cycdirnetTarget);
                        return true;
                    }
                    return false;





                }

                //如果选择定的周期不是最后一个周期则按照它的周期的时间和它下一个周期的时间筛选可用的周期
                else
                {

                    cycdirnetModelEx = CgCycdirnet(xmname, pointname, cyc - 1);
                    //重新计算本周期的变化量覆盖成果库中原有的周期,重新计算下一个周期的变化量更新
                    cycdirnetTarget = Cycdirnet(xmname, pointname, cycdirnetCyc);
                    //获取上一周期的对象
                    cycdirnetTarget.CYC = cyc;
                    if (CgCycdirnetAdd(cycdirnetModelEx, cycdirnetTarget))
                    {
                        cgResultDataAlarmBLL.ProcessCycdirnetPointAlarmBLL(xmno, cycdirnetTarget);
                        return true;
                    }
                    return false;

                }
              

            

        }
        public static ProcessCycdirnetBLL cycBLL = new ProcessCycdirnetBLL();
        //获取测量库指定周期的数据对象
        public TotalStation.Model.fmos_obj.cycdirnet Cycdirnet(string xmname, string pointname, int cyc)
        {
            string mssg = "";
            var processCycdirnetModelGetModel = new ProcessCycdirnetBLL.ProcessCycdirnetModelGetModel(xmname, pointname, cyc);
            if (!cycBLL.ProcessCycdirnetModel(processCycdirnetModelGetModel, out mssg))
            {
                //return;
            }
            return processCycdirnetModelGetModel.model;
        }
        public bool CgCycdirnetAdd(TotalStation.Model.fmos_obj.cycdirnet basecyc, TotalStation.Model.fmos_obj.cycdirnet targetcyc)
        {
            string mssg = "";
            cycdirnetCgBLL.cycdirnetThisCalculation(basecyc, targetcyc);
            return cycdirnetCgBLL.ProcessCgCycdirnetAdd(targetcyc, out mssg);
           
        }
        //成果数据更新
        public bool ProcessCgCycdirnetUpdate(string xmname, int xmno,string pointname, int cyc, int cycdirnetCyc)
        {

            //成果周期列表
            List<string> cgCycList = new List<string>();
            //测量周期列表
            List<string> cycList = new List<string>();
            //成果库选定周期前周期的数据对象
            //成果库选定周期前周期的数据对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModelEx = new TotalStation.Model.fmos_obj.cycdirnet();
            //成果库选定周期的数据对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModel = new TotalStation.Model.fmos_obj.cycdirnet();
            //成果库选定周期的后一个周期对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModelNext = new TotalStation.Model.fmos_obj.cycdirnet();
            //测量库数据表
            DataTable dt = null;
            //测量库中的数据周期列表
            List<string> ls = new List<string>();
            //测量库中选中要加入成果库的数据周期
            //int cycdirnetCyc = 0;
            //测量库中选中的数据对象
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetTarget = new TotalStation.Model.fmos_obj.cycdirnet();
            int index = 0;
            while (true)
            {
                
                cgCycList = CgCycdirnetDataList(xmname, pointname);
                index = cgCycList.IndexOf(cyc.ToString());
                //获取成果库中该周期的数据
                cycdirnetModel = CgCycdirnet(xmname, pointname, cyc);
                var processResultData = new ProcessResultDataBLL.ProcessResultDataTimeSearchModel(xmname, pointname, Convert.ToDateTime("0001-1-1"), Convert.ToDateTime("0001-1-1"));
                cycdirnetModelEx = CgCycdirnet(xmname, pointname, cyc - 1);
                cycdirnetModelNext = CgCycdirnet(xmname, pointname, cyc + 1);


                //重新计算本周期的变化量覆盖成果库中原有的周期,重新计算下一个周期的变化量更新

                cycdirnetTarget = Cycdirnet(xmname, pointname, cycdirnetCyc);
                //获取上一周期的对象

                if (index < cgCycList.Count - 1)
                {
                    cycdirnetTarget.CYC = cyc;
                    CgCycdirnetAdd(cycdirnetModelEx, cycdirnetTarget);
                    //cgResultDataAlarmBLL.ProcessCycdirnetPointAlarmBLL(xmno, cycdirnetTarget);
                    if (CgCycdirnetAdd(cycdirnetTarget, cycdirnetModelNext))
                    {
                        //cgResultDataAlarmBLL.ProcessCycdirnetPointAlarmBLL(xmno, cycdirnetTarget);
                        return true;
                    }
                    return false;
                }
                else
                {
                    cycdirnetTarget.CYC = cyc;
                    if (CgCycdirnetAdd(cycdirnetModelEx, cycdirnetTarget))
                    {
                        //cgResultDataAlarmBLL.ProcessCycdirnetPointAlarmBLL(xmno, cycdirnetTarget);
                        return true;
                    }
                    return false;
                }
               

            }
            */



        }

        /// <summary>
        /// 删除成果数据选定周期以及后面的所有数据
        /// </summary>
        public bool ProcessCycdirnetDataDelete(string xmname,string pointname,int cyc)
        {
            string mssg = "";


            //var processCgCycdirnetDelModel = new ProcessCycdirnetCgBLL.ProcessCgCycdirnetDelModel(xmname, cyc, pointname);
            //cycdirnetCgBLL.ProcessCgCycdirnetDel(processCgCycdirnetDelModel, out mssg);
            //ProcessPrintMssg.Print(mssg);
            //cgPointAlarmBLL.ProcessHotPotColorUpdate(Aspect.IndirectValue.GetXmnoFromXmname(xmname), "表面位移", "全站仪", pointname, 0, out mssg);
            return false;
          


        }
        /// <summary>
        /// 比较测量库和成果库中选择周期的时间
        /// </summary>
        /// <param name="cgcyc"></param>
        /// <param name="cyc"></param>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <returns></returns>
        public bool ProcessCycdirnetDateCompare(int cgcyc,int cyc,string xmname,string pointname)
        {
            TotalStation.Model.fmos_obj.cycdirnet cgCycdirnetModel = new TotalStation.Model.fmos_obj.cycdirnet();
            TotalStation.Model.fmos_obj.cycdirnet cycdirnetModel = new TotalStation.Model.fmos_obj.cycdirnet();
            DateTime dtCg = new DateTime();
            DateTime dt = new DateTime();
            
            cgCycdirnetModel = CgCycdirnet(xmname, pointname, cgcyc);
            cycdirnetModel = null;// Cycdirnet(xmname, pointname, cyc);
            dtCg = cgCycdirnetModel == null ? Convert.ToDateTime("0001-1-1") : cgCycdirnetModel.Time;
            dt = cycdirnetModel == null ? Convert.ToDateTime("9999-1-1") : cycdirnetModel.Time;
            if (cgCycdirnetModel.Time >= cycdirnetModel.Time) return false;

            return true;
        }

        public DataTable ProcessCgPointNameResultDataLoad(string xmname, string pointname,out string mssg)
        {
            var cgPointNameResultDataLoadModel = new ProcessCycdirnetCgBLL.CgPointNameResultDataLoadModel(xmname, pointname);
            cycdirnetCgBLL.ProcessCgPointNameResultDataLoad(cgPointNameResultDataLoadModel, out mssg);
            return cgPointNameResultDataLoadModel.dt;
        }


    }
}