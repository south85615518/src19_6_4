﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.AchievementGeneration.TotalStationDAL
{
    public class CycdirnetTimeModelGetCondition
    {
        public string xmname { get; set; }
        public string pointname { get; set; }
        public DateTime dt { get; set; }
      
        public TotalStation.Model.fmos_obj.cycdirnet model { get; set; }
        public CycdirnetTimeModelGetCondition(string xmname, string pointname, DateTime dt)
        {
            this.xmname = xmname;
            this.pointname = pointname;
            this.dt = dt;
        }
    }
}