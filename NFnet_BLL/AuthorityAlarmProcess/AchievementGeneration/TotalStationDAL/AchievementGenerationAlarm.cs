﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.UserProcess;
using Tool;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_BLL.AchievementGeneration.TotalStationDAL
{
    public class AchievementGenerationAlarm
    {
        public ProcessCgResultDataAlarmBLL dataAlarmBLL = new ProcessCgResultDataAlarmBLL();
        public ProcessAdministratorAlarmBLL administratorAlarmBLL = new ProcessAdministratorAlarmBLL();
        public MailHelper mail = new MailHelper();
        public bool ProcessAchievementGenerationAlarm(ProcessAchievementGenerationAlarmModel model, out string mssg)
        {

            try
            {
                
                string alarmContext = "";
                dataAlarmBLL = new ProcessCgResultDataAlarmBLL(model.xmname, 29, model.dt.ToString());
                dataAlarmBLL.main();
                model.alarmContext = dataAlarmBLL.alarmInfoList.Count > 5 ? string.Join("<br/>", dataAlarmBLL.alarmInfoList) : string.Format("{0}-{1}没有触发任何预警信息!", model.dt, DateTime.Now);
                mssg = string.Format("生成项目{0}预警信息成功!",model.xmname);
                return true;
            }
            catch (Exception ex)
            {
                mssg = string.Format("生成项目{0}预警信息出错!错误信息："+ex.Message, model.xmname);
                return false;
            }
            //Console.WriteLine("是否将预警信息发送给管理员Y/N");
            //if (Console.ReadKey().KeyChar == 'y')
            //{
            //    //将预警信息打包发送给管理员
            //    int sec = 0;
            //    //邮件群发
            //    if (dataAlarmBLL.alarmInfoList.Count > 5)
            //    {
            //        var processXmAdministatorModel = new ProcessAdministratrorBLL.ProcessXmAdministatorModel(dataAlarmBLL.xmname);
            //        ProcessAdministratrorBLL.ProcessXmAdministator(processXmAdministatorModel, out mssg);
            //        //管理员预警通知存库

            //        //监测员预警通知存库
            //        AuthorityAlarm.Model.adminalarm adminAlarm = new AuthorityAlarm.Model.adminalarm
            //        {
            //            xmno = Aspect.IndirectValue.GetXmnoFromXmname(dataAlarmBLL.xmname),
            //            context = string.Join(",", dataAlarmBLL.alarmInfoList.Where(s => (s != "")).ToList()),
            //            sendTime = DateTime.Now,
            //            confirm = false,
            //            mail = false,
            //            adminno = processXmAdministatorModel.model.userId,
            //            dataType = 1,
            //            ForwardTime = 0,
            //            mess = false,
            //            aid = string.Format("A{0}{1}", processXmAdministatorModel.model.userId, DateHelper.DateTimeToString(dt.AddSeconds(++sec)))
            //        };
            //        if (!administratorAlarmBLL.ProcessAdministrtorAlarmAdd(adminAlarm, out mssg))
            //        {

            //        }

            //        //监测员预警通知发送邮件
            //        int t = 0;
            //        while (t < 3)
            //        {
            //            //重发三次
            //            if (!mail.Small(string.Join("<p>", dataAlarmBLL.alarmInfoList.Where(s => (s != "")).ToList()), processXmAdministatorModel.model.email, string.Format("{0}预警信息通知", dataAlarmBLL.xmname), out mssg))
            //            {
            //                t++;
            //            }
            //            else
            //            {
            //                //更新管理员短信标志
            //                adminAlarm.mess = true;
            //                if (!administratorAlarmBLL.ProcessAdministrtorAlarmUpdate(adminAlarm, out mssg))
            //                {

            //                }
            //                break;
            //            }
            //        }

            //    }


            //}








        }


        public class ProcessAchievementGenerationAlarmModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string alarmContext { get; set; }
            public DateTime dt { get; set; }
            public ProcessAchievementGenerationAlarmModel(string xmname, int xmno, DateTime dt)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.dt = dt;
            }
        }




    }
}