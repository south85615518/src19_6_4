﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public class ProcessMonitorAlarmBLL
    {
        public AuthorityAlarm.BLL.monitoralarm monitorAlarm = new AuthorityAlarm.BLL.monitoralarm();

        /// <summary>
        /// 添加监测员预警通知
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMonitorAlarmAdd(AuthorityAlarm.Model.monitoralarm model,out string mssg)
        {
            return monitorAlarm.Add(model,out mssg);
        }

        public bool ProcessMonitorAlarmLastSendTime(ProcessMonitorAlarmLastSendTimeModel model,out string mssg)
        {
            DateTime dt = new DateTime();
            if(monitorAlarm.LastSendTime(model.xmno,out dt,out  mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;
        }
        public class ProcessMonitorAlarmLastSendTimeModel
        {
            public int xmno { get; set; }
            public DateTime dt { get; set; }
            public ProcessMonitorAlarmLastSendTimeModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        /// <summary>
        /// 更新监测员预警通知
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMonitorAlarmUpdate(AuthorityAlarm.Model.monitoralarm model, out string mssg)
        {
            return monitorAlarm.Update(model, out mssg);
        }
    }
}