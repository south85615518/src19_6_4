﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public class XmAlarmContCondition
    {
        public int xmno { get; set; }
        public int cont { get; set; }
        public XmAlarmContCondition(int xmno)
        {
            this.xmno = xmno;
        }
    }
}