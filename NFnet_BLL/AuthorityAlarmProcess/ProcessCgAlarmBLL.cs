﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public class ProcessCgAlarmBLL
    {
        public readonly AuthorityAlarm.BLL.pointalarm pointalarm = new AuthorityAlarm.BLL.pointalarm();


        public bool ProcessPointAlarmAdd(AuthorityAlarm.Model.pointalarm model, out string mssg)
        {
            return pointalarm.Add(model, out mssg);
        }

        public bool ProcessPointAlarmUpdate(AuthorityAlarm.Model.pointalarm model, out string mssg)
        {
            return pointalarm.Update(model, out mssg);
        }

        public bool ProcessXmHightestAlarm(XmHightestAlarmCondition model, out string mssg)
        {
            int alarm = 0;
            if (pointalarm.XmHighestAlarm(model.xmno, out alarm, out mssg))
            {
                model.alarm = alarm;
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ProcessPointCheckModelList(ProcessPointCheckModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.pointalarm> modelList = null;

            if (pointalarm.GetModelList(model.xmno, out modelList, out mssg))
            {
                model.modelList = modelList;
                return true;
            }
            return false;

        }
        public class ProcessPointCheckModelListModel
        {
            public int xmno { get; set; }
            public List<AuthorityAlarm.Model.pointalarm> modelList { get; set; }
            public ProcessPointCheckModelListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public string ProcessXmAlarmColor(int xmno, out string mssg)
        {

            var processXmHightestAlarmModel = new XmHightestAlarmCondition(xmno);
            ProcessXmHightestAlarm(processXmHightestAlarmModel, out mssg);
            return DataProcessHelper.ColorDispaly(processXmHightestAlarmModel.alarm);


        }

        public bool ProcessPointAlarmDelete(AuthorityAlarm.Model.pointalarm model, out string mssg)
        {
            return pointalarm.Delete(model, out mssg);
        }


        public bool ProcessXmAlarmCont(XmAlarmContCondition model, out string mssg)
        {
            int cont = 0;
            if (pointalarm.XmAlarmCount(model.xmno, out cont, out mssg))
            {
                model.cont = cont;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessPointAlarmModelGet(RolePointAlarmCondition model, out string mssg)
        {
            AuthorityAlarm.Model.pointalarm checkModel = null;
            if (pointalarm.GetModel(model.xmno, model.pointname, model.jclx, out checkModel, out mssg))
            {

                model.dt = checkModel.time;
                model.alarm = checkModel.alarm;
                return true;
            }
            return false;
        }

       
          

    }
}