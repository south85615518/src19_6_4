﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.AuthorityAlarmProcess
{

    public class ProcessAdministratorAlarmBLL
    {

        public AuthorityAlarm.BLL.adminalarm administrtorAlarm = new AuthorityAlarm.BLL.adminalarm();

        /// <summary>
        /// 添加管理员预警通知
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAdministrtorAlarmAdd(AuthorityAlarm.Model.adminalarm model, out string mssg)
        {
            return administrtorAlarm.Add(model, out mssg);
        }

        /// <summary>
        /// 更新管理员预警通知
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAdministrtorAlarmUpdate(AuthorityAlarm.Model.adminalarm model, out string mssg)
        {
            return administrtorAlarm.Update(model, out mssg);
        }

    }
}