﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.LoginProcess;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public class ProcessRoleAlarmBLL
    {
        public ProcessPointCheckBLL pointCheckBLL = new ProcessPointCheckBLL();
        public ProcessCgAlarmBLL cgAlarmBLL = new ProcessCgAlarmBLL();
        public bool ProcessXmHightestAlarm(ProcessXmHightestAlarmModel model, out string mssg)
        {
            mssg = "";
            if (model.role == Role.superviseModel)
            {
                cgAlarmBLL.ProcessXmHightestAlarm(model.model, out mssg);

            }
            else
            {
                pointCheckBLL.ProcessXmHightestAlarm(model.model, out mssg);
            }

            return false;

        }

        public class ProcessXmHightestAlarmModel
        {
            public XmHightestAlarmCondition model { get; set; }
            public Role role { get; set; }
            public ProcessXmHightestAlarmModel(XmHightestAlarmCondition model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }

        public bool ProcessXmAlarmCont(ProcessXmAlarmContModel model, out string mssg)
        {
            mssg = "";
            if (model.role == Role.superviseModel)
            {
                cgAlarmBLL.ProcessXmAlarmCont(model.model, out mssg);

            }
            else
            {
                pointCheckBLL.ProcessXmCheckCont(model.model, out mssg);
            }

            return false;
        }

        public class ProcessXmAlarmContModel
        {
            public XmAlarmContCondition model { get; set; }
            public Role role { get; set; }
            public ProcessXmAlarmContModel(XmAlarmContCondition model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }

        public string ProcessXmRoleAlarmColor(ProcessXmRoleAlarmColorModel model, out string mssg)
        {
            mssg = "";
            if (model.role == Role.superviseModel)
            {
                return cgAlarmBLL.ProcessXmAlarmColor(model.xmno, out mssg);

            }
            else
            {
                return pointCheckBLL.ProcessXmAlarmColor(model.xmno, out mssg);
            }

            
        }
        public class ProcessXmRoleAlarmColorModel
        {
            public int xmno { get; set; }
            public Role role { get; set; }
            public ProcessXmRoleAlarmColorModel(int xmno, Role role)
            {
                this.xmno = xmno;
                this.role = role;
            }
        }
        
        public bool ProcessXmRoleAlarmPointLoad(ProcessXmRoleAlarmPointLoadModel model,out string mssg)
        {
            mssg = "";
            if (model.role == Role.superviseModel)
            {
                var processPointCheckModelListModel = new ProcessCgAlarmBLL.ProcessPointCheckModelListModel(model.xmno);
                cgAlarmBLL.ProcessPointCheckModelList(processPointCheckModelListModel,out mssg);
                if (processPointCheckModelListModel.modelList == null) return false;
                var alarmpoints = from m in processPointCheckModelListModel.modelList select string.Format("{0},{1},{2}", m.type,m.point_name, m.alarm);
                var alarmpointsstr = string.Join("#", alarmpoints);
                model.alarmpoint = alarmpointsstr;
                return true;

            }
            else
            {
                var processPointCheckModelListModel = new ProcessPointCheckBLL.ProcessPointCheckModelListModel(model.xmno);
                pointCheckBLL.ProcessPointCheckModelList(processPointCheckModelListModel, out mssg);
                var alarmpoints = from m in processPointCheckModelListModel.modelList select string.Format("{0},{1},{2}", m.type,m.point_name, m.alarm);
                var alarmpointsstr = string.Join("#", alarmpoints);
                model.alarmpoint = alarmpointsstr;
                return true;
            }
        }
        public class ProcessXmRoleAlarmPointLoadModel
        {
            public int xmno { get; set; }
            public string alarmpoint { get;set; }
            public Role role { get; set; }
            public ProcessXmRoleAlarmPointLoadModel(int xmno, Role role)
            {
                this.xmno = xmno;
                this.role = role;
            }

        }



    }
}