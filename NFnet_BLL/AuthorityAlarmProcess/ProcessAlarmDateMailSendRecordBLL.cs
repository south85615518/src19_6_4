﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    
    public class ProcessAlarmDateMailSendRecordBLL
    {
        public static AuthorityAlarm.BLL.alarmdatemailsendrecord sendrecordBLL = new AuthorityAlarm.BLL.alarmdatemailsendrecord();

        public bool ProcessAlarmDateMailSendRecordAdd(AuthorityAlarm.Model.alarmdatemailsendrecord model,out string mssg)
        {
            return sendrecordBLL.Add(model,out mssg);
        }

        public bool ProcessAlarmDateMailSendRecordUpdate(AuthorityAlarm.Model.alarmdatemailsendrecord model, out string mssg)
        {
            return sendrecordBLL.Update(model, out mssg);
        }

    }
}