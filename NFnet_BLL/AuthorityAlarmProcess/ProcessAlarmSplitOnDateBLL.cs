﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.AuthorityAlarmProcess
{
    public partial class ProcessalarmsplitondateBLL
    {
        public static AuthorityAlarm.BLL.alarmsplitondate cgalarmBLL = new AuthorityAlarm.BLL.alarmsplitondate();
        public bool ProcessalarmsplitondateAdd(AuthorityAlarm.Model.alarmsplitondate model, out string mssg)
        {
            return cgalarmBLL.Add(model, out mssg);
        }
        public bool ProcessalarmsplitondateDelete(ProcessalarmsplitondateDeleteModel model, out string mssg)
        {

            return cgalarmBLL.Delete(model.xmno, model.jclx, model.pointName, model.time, out mssg);
        }
        public class ProcessalarmsplitondateDeleteModel
        {
            public int xmno { get; set; }
            public string jclx { get; set; }
            public string pointName { get; set; }
            public DateTime time { get; set; }
            public ProcessalarmsplitondateDeleteModel(int xmno, string jclx, string pointName, DateTime time)
            {
                this.xmno = xmno;
                this.jclx = jclx;
                this.pointName = pointName;
                this.time = time;
            }
        }

        //public bool ProcessalarmsplitondateModelList(ProcessalarmsplitondateModelListModel model, out string mssg)
        //{
        //    List<AuthorityAlarm.Model.alarmsplitondate> ls = null;
        //    mssg = "";
        //    if (cgalarmBLL.GetModelList(model.xmno, out ls, out mssg))
        //    {
        //        model.ls = ls;
        //        return true;
        //    }
        //    return false;
        //}
        public bool Processalarmcgsplitondatereaded(ProcessAlarmSplitOnDateReadedModel model, out string mssg)
        {

            return cgalarmBLL.alarmcgsplitondatereaded(model.xmno, model.dt, out mssg);
        }
        public class ProcessAlarmSplitOnDateReadedModel
        {
            public int xmno { get; set; }
            public DateTime dt { get; set; }
            public ProcessAlarmSplitOnDateReadedModel(int xmno, DateTime dt)
            {
                this.xmno = xmno;
                this.dt = dt;
            }
        }
        public bool ProcessalarmcgsplitondateModelGet(ProcessalarmcgsplitondateModelGetModel model, out string mssg)
        {
            AuthorityAlarm.Model.alarmsplitondate alarmsplitondatemodel = null;
            if (cgalarmBLL.GetModel(model.xmno, model.dno, out alarmsplitondatemodel, out mssg))
            {
                model.model = alarmsplitondatemodel;
                return true;
            }
            return false;
        }
        public class ProcessalarmcgsplitondateModelGetModel
        {
            public int xmno { get; set; }
            public string dno { get; set; }
            public AuthorityAlarm.Model.alarmsplitondate model { get; set; }
            public ProcessalarmcgsplitondateModelGetModel(int xmno, string dno)
            {
                this.xmno = xmno;
                this.dno = dno;
            }
        }

        public bool Processalarmcgsplitondateconfirm(ProcessalarmcgsplitondateconfirmModel model, out string mssg)
        {

            return cgalarmBLL.alarmcgsplitondateconfirm(model.xmno, model.alarmno, out mssg);
        }
        public class ProcessalarmcgsplitondateconfirmModel
        {
            public int xmno { get; set; }
            public string alarmno { get; set; }
            public ProcessalarmcgsplitondateconfirmModel(int xmno, string alarmno)
            {
                this.xmno = xmno;
                this.alarmno = alarmno;
            }
        }

        public bool Processalarmcgsplitondatedeletebyalarmid(ProcessalarmcgsplitondatedeletebyalarmidModel model, out string mssg)
        {
            return cgalarmBLL.alarmcgsplitondatedelete(model.xmno, model.alarmno, out mssg);
        }
        public class ProcessalarmcgsplitondatedeletebyalarmidModel
        {
            public int xmno { get; set; }
            public string alarmno { get; set; }
            public ProcessalarmcgsplitondatedeletebyalarmidModel(int xmno, string alarmno)
            {
                this.xmno = xmno;
                this.alarmno = alarmno;
            }
        }

        public bool Processalarmcgsplitondatedeletebydate(ProcessalarmcgsplitondatedeletebydateModel model, out string mssg)
        {
            return cgalarmBLL.alarmcgsplitondatedelete(model.xmno, model.date, out mssg);
        }
        public class ProcessalarmcgsplitondatedeletebydateModel
        {
            public int xmno { get; set; }
            public DateTime date { get; set; }
            public ProcessalarmcgsplitondatedeletebydateModel(int xmno, DateTime date)
            {
                this.xmno = xmno;
                this.date = date;
            }
        }

        /// <summary>
        /// 获取项目某一天的预警详情1111111111
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessalarmsplitondateModelList(ProcessDateAlarmSplitOnDateModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.alarmsplitondate> ls = null;
            mssg = "";
            if (cgalarmBLL.GetModelList(model.xmno, model.dt, model.keyWord, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
        public bool ProcessUnitAlarmSplitOnDateModelList(ProcessUnitAlarmSplitOnDateModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.alarmsplitondate> ls = null;
            mssg = "";
            if (cgalarmBLL.UnitDayAlarmListLoad(model.unitname, model.lasttime, model.timeunit, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
        public class ProcessUnitAlarmSplitOnDateModelListModel
        {
            public string unitname { get; set; }
            public DateTime lasttime { get; set; }
            public int timeunit { get; set; }
            public List<AuthorityAlarm.Model.alarmsplitondate> ls { get; set; }
            public ProcessUnitAlarmSplitOnDateModelListModel(string unitname, DateTime lasttime, int timeunit)
            {
                this.unitname = unitname;
                this.lasttime = lasttime;
                this.timeunit = timeunit;
            }
        }
        public bool ProcessXmAlarmSplitOnDateModelList(ProcessXmAlarmSplitOnDateModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.alarmsplitondate> ls = null;
            mssg = "";
            if (cgalarmBLL.XmDayAlarmListLoad(model.xmno, model.lasttime, model.timeunit, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
        public class ProcessXmAlarmSplitOnDateModelListModel
        {
            public int xmno { get; set; }
            public DateTime lasttime { get; set; }
            public int timeunit { get; set; }
            public List<AuthorityAlarm.Model.alarmsplitondate> ls { get; set; }
            public ProcessXmAlarmSplitOnDateModelListModel(int xmno, DateTime lasttime, int timeunit)
            {
                this.xmno = xmno;
                this.lasttime = lasttime;
                this.timeunit = timeunit;
            }
        }


        public bool ProcessalarmsplitondateModelListBYCYCList(ProcessalarmsplitondateModelListBYCYCListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.alarmsplitondate> ls = null;
            mssg = "";
            if (cgalarmBLL.AlarmListGetByCycList(model.xmno,model.datatype ,model.cyclist,  out ls, out mssg))
            {
                model.alarmsplitondatelist = ls;
                return true;
            }
            return false;
        }

        public class ProcessalarmsplitondateModelListBYCYCListModel
        {
            public int xmno { get; set; }
            public List<string> cyclist { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public List<AuthorityAlarm.Model.alarmsplitondate> alarmsplitondatelist { get; set; }
            public ProcessalarmsplitondateModelListBYCYCListModel(int xmno, List<string> cyclist, data.Model.gtsensortype datatype)
            {
                this.xmno = xmno;
                this.cyclist = cyclist;
                this.datatype = datatype;
            }
        }



        /// <summary>
        /// 获取项目某一天的预警详情
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAlarmSplitOnDateModelList(ProcessAlarmSplitOnDateModelListModel model, out string mssg)
        {
            List<AuthorityAlarm.Model.alarmsplitondate> ls = null;
            mssg = "";
            if (cgalarmBLL.GetModelList(model.xmno, model.startTime, model.endTime, model.keyWord, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;
        }
        public class ProcessAlarmSplitOnDateModelListModel
        {
            public int xmno { get; set; }
            public DateTime startTime { get; set; }
            public DateTime endTime { get; set; }
            public string keyWord { get; set; }
            public List<AuthorityAlarm.Model.alarmsplitondate> ls { get; set; }
            public ProcessAlarmSplitOnDateModelListModel(int xmno, object startTime, object endTime, string keyWord)
            {
                this.xmno = xmno;
                this.startTime = startTime == null || startTime == "" ? Convert.ToDateTime("1970-1-1") : Convert.ToDateTime(startTime);
                this.endTime = endTime == null || endTime == "" ? Convert.ToDateTime("9999-1-1") : Convert.ToDateTime(endTime);
                this.keyWord = keyWord;
            }
        }
        public class alarmsplitondateModel : AuthorityAlarm.Model.alarmsplitondate
        {
            public string itimestr { get; set; }
            public alarmsplitondateModel(AuthorityAlarm.Model.alarmsplitondate model)
            {
                this.adate = model.adate;
                this.alarmContext = model.alarmContext;
                this.cont = model.cont;
                this.dno = model.dno;
                this.itimestr = model.itime.ToString();
                this.pointName = model.pointName;
                this.xmno = model.xmno;
                this.jclx = model.jclx;
                this.confirm = model.confirm;
            }
        }

        public class ProcessDateAlarmSplitOnDateModelListModel
        {
            public int xmno { get; set; }
            public List<AuthorityAlarm.Model.alarmsplitondate> ls { get; set; }
            public DateTime dt { get; set; }
            public string keyWord { get; set; }
            public ProcessDateAlarmSplitOnDateModelListModel(int xmno, DateTime dt, string keyWord)
            {
                this.xmno = xmno;
                this.dt = dt;
                this.keyWord = keyWord;
            }
        }


        public class _alarmsplitondateModel
        {
            public string dt { get; set; }
            public int read { get; set; }
            public int acont { get; set; }
            public int scont { get; set; }
            public string alarmContext { get; set; }
            public _alarmsplitondateModel(string dt, int read, int acont, string alarmContext, int scont)
            {
                this.dt = dt;
                this.read = read;
                this.acont = acont;
                this.alarmContext = alarmContext;
                this.scont = scont;

            }

        }

        public bool ProcessAlarmOnDateSplit(ProcessAlarmOnDateSplitModel model, out string mssg)
        {
            mssg = "";
            int cnt = 0;
            List<string> ls = new List<string>();
            if (model.ls.Count == 0) return false;
            model.modells = new List<_alarmsplitondateModel>();
            var readSplitOnDate = new List<AuthorityAlarm.Model.alarmsplitondate>();
            AuthorityAlarm.Model.alarmsplitondate tmp = model.ls[0];
            foreach (AuthorityAlarm.Model.alarmsplitondate cgalarm in model.ls)
            {
                if (tmp.adate != cgalarm.adate)
                {
                    readSplitOnDate = (from m in model.ls where (m.adate == tmp.adate && m.readed == false) select m).ToList();
                    model.modells.Add(new _alarmsplitondateModel(tmp.adate.ToString(), readSplitOnDate.Count, cnt, string.Join("\n", ls), tmp.cont));

                    ls = new List<string>();
                    cnt = 0;

                }
                cnt++;
                ls.Add(cgalarm.alarmContext);
                tmp = cgalarm;
            }
            readSplitOnDate = (from m in model.ls where (m.adate == tmp.adate && m.readed == false) select m).ToList();
            model.modells.Add(new _alarmsplitondateModel(tmp.adate.ToString(), readSplitOnDate.Count, cnt, string.Join("\n", ls), tmp.cont));
            return true;
        }
        public bool ProcessConfirmAlarmOnDateSplit(ProcessAlarmOnDateSplitModel model, out string mssg)
        {
            mssg = "";
            int cnt = 0;
            List<string> ls = new List<string>();
            if (model.ls.Count == 0) return false;
            model.modells = new List<_alarmsplitondateModel>();
            var readSplitOnDate = new List<AuthorityAlarm.Model.alarmsplitondate>();
            AuthorityAlarm.Model.alarmsplitondate tmp = model.ls[0];
            foreach (AuthorityAlarm.Model.alarmsplitondate cgalarm in model.ls)
            {

                if (tmp.adate != cgalarm.adate)
                {
                    readSplitOnDate = (from m in model.ls where (m.adate == tmp.adate && m.readed == false && m.confirm == true) select m).ToList();
                    model.modells.Add(new _alarmsplitondateModel(tmp.adate.ToString(), readSplitOnDate.Count, cnt, string.Join("\n", ls), tmp.cont));

                    ls = new List<string>();
                    cnt = 0;

                }
                if (cgalarm.confirm)
                    cnt++;
                ls.Add(cgalarm.alarmContext);
                tmp = cgalarm;
            }
            readSplitOnDate = (from m in model.ls where (m.adate == tmp.adate && m.readed == false && m.confirm == true) select m).ToList();
            model.modells.Add(new _alarmsplitondateModel(tmp.adate.ToString(), readSplitOnDate.Count, cnt, string.Join("\n", ls), tmp.cont));
            return true;
        }
        public class ProcessAlarmOnDateSplitModel
        {
            public List<_alarmsplitondateModel> modells { get; set; }
            public List<AuthorityAlarm.Model.alarmsplitondate> ls { get; set; }
            public ProcessAlarmOnDateSplitModel(List<AuthorityAlarm.Model.alarmsplitondate> ls)
            {
                this.ls = ls;
            }
        }

        public bool ProcessAlarmOnDateSplitUnionSend(ProcessCgAlarmOnDateSplitUnionSendModel model, out string mssg)
        {
            MailHelper mail = new MailHelper();
            ProcessSmsSendBLL processSmsSendBLL = new ProcessSmsSendBLL();
            ProcessAlarmDateMailSendRecordBLL mailSendRecordBLL = new ProcessAlarmDateMailSendRecordBLL();
            int sec = 0;
            var modellistloadmodel = new ProcessalarmsplitondateBLL.ProcessDateAlarmSplitOnDateModelListModel(model.xmno, new DateTime(), "");
            foreach (string dt in model.dtSelected)
            {
                string txtContext = string.Format("======{0}==={1}===<p>", dt, DateTime.Now);


                modellistloadmodel.dt = Convert.ToDateTime(dt);

                ProcessalarmsplitondateModelList(modellistloadmodel, out mssg);

                var lis = (from m in modellistloadmodel.ls where m.adate == Convert.ToDateTime(dt) select m.alarmContext).ToList();
                lis.Insert(0, string.Format("{0}自检信息通知", model.xmname));

                //txtContext += string.Join("\n", lis);

                int t = 0;
                while (t < 3)
                {
                    //重发三次
                    if (!processSmsSendBLL.ProcessSmsSend(lis, model.xmno, model.monitorList, out mssg))
                    {
                        t++;
                    }
                    else
                    {
                        //更新发送数量
                        AuthorityAlarm.Model.alarmdatemailsendrecord mailsendrecord = new AuthorityAlarm.Model.alarmdatemailsendrecord
                        {
                            did = string.Format("D{0}", DateHelper.DateTimeToString(Convert.ToDateTime(dt).AddSeconds(++sec))),
                            sdate = Convert.ToDateTime(dt),
                            xmno = model.xmno
                        };
                        if (!mailSendRecordBLL.ProcessAlarmDateMailSendRecordAdd(mailsendrecord, out mssg))
                        {
                            //更新分包发送记录失败
                        }
                        //monitorAlarm.mess = true;
                        //if (!monitorAlarmBLL.ProcessMonitorAlarmUpdate(monitorAlarm, out mssg))
                        //{
                        //    return false;
                        //}
                        break;

                    }



                }
            }
            //string.Join(",", resultDataBLL.alarmInfoList.Where(s => (s != "")).ToList())
            mssg = string.Format("发送{0}{1}预警短信成功!", model.xmname, string.Join(",", model.dtSelected));
            return true;


        }

        public class ProcessCgAlarmOnDateSplitUnionSendModel
        {
            public int xmno { get; set; }
            public string xmname { get; set; }
            public List<Authority.Model.MonitorMember> monitorList { get; set; }
            public List<string> dtSelected { get; set; }
            public List<string> messlist { get; set; }
            public ProcessCgAlarmOnDateSplitUnionSendModel(int xmno, List<Authority.Model.MonitorMember> monitorList, string xmname, List<string> dtSelected)
            {
                this.xmno = xmno;
                this.xmname = xmname;
                this.monitorList = monitorList;
                this.dtSelected = dtSelected;

            }
        }





    }
}