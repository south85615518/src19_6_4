﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NFnet_BLL
{
    interface Sql_Bll_I
    {
        //插入机构信息
         string GetUnitSql_i();
        //更新机构信息
         string GetUnitSql_u();
         //获取机构信息表
         string GetUnitSql_t();
        //查询机构信息
         string GetUnitSql_s();
        //获取机构列表
         string GetUnitSql_ls();
        //插入人员信息
         string GetMemberSql_i();
        //更新人员信息
         string GetMemberSql_u();
        //查询人员信息
         string GetMemberSql_s();
        //获取人员信息表
         string GetMemberSql_t();
        //获取人员列表
         string GetMemberSql_ls();
        //人员登陆验证
         string MemberLoginSql_s();
         //插入设备信息
         string GetInstrumentSql_i();
         //更新设备信息
         string GetInstrumentSql_u();
         //查询设备信息
         string GetInstrumentSql_s();
         //获取设备信息表
         string GetInstrumentSql_t();
         //获取设备列表
         string GetInstrumentSql_ls();
         //设备登陆验证
         string InstrumentLoginSql_s();
        //数据库中所有的库名
         string dbNameSql_s();
        //获取全站仪任务表中的信息
         string taskSql_s();
        //在项目表中填充名称和监测单位
         string projectSql_u(); 
        //获取表面位移（全站仪）的点号
         string studyPointSql_s();
        //获取表面位移（全站仪）表
         DataTable FmosTableSql_s();
        //获取工程信息
         string XminfoSql_s();
        //获取管理员最新监测项目的名称
         string NearestSql_s();
        //获取超管最新监测项目名称
         string CGNearest_s();
        //获取管理员的项目名称
         string GMXmnamesql_s(); 
        
    }
}
