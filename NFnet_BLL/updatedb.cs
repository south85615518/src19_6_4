﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using NFnet_BLL.全局类;
namespace NFnet_BLL
{
    public class updatedb
    {
        public static accessdbse accessdb = new accessdbse();
        public static database db = new database();
        /// <summary>
        /// 更新和删除数据库中的记录
        /// </summary>
        /// <param name="tbName">要更新的表名</param>
        /// <param name="attrs">设置的属性名</param>
        /// <param name="setvals">设置的属性值</param>
        /// <param name="args">条件参数</param>
        /// <param name="vals">条件参数值</param>
        /// <returns></returns>
        public static int update(string tbName, string[] attrs, string[] setvals, string[] args, string[] vals)
        {
            int line;//更新的行数
            string set = "";
            string afterw = "";
            for (int j = 0; j < attrs.Length; j++)
            {
                set += attrs[j] + " = '" + setvals[j] + "'" + " , ";

            }
            set = set.Substring(0, set.LastIndexOf(","));
            for (int i = 0; i < args.Length; i++)
            {
                afterw += querystring.querynvl(args[i], vals[i], "=", null, null)
                + " and ";
            }
            afterw = afterw.Substring(0, afterw.LastIndexOf("and"));
            string sql = " update " + tbName + "  set " + set + "  where " + afterw;
            OdbcConnection conn = db.getdbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;
            try
            {
                line = ocmd.ExecuteNonQuery();
                //提交事务
                ot.Commit();
            }
            catch (Exception e)
            {
                ExceptionLog.ExceptionWrite(e, sql);
                ot.Rollback();//如操作失败，则事务回滚
                return -1;
            }
            finally
            {
                ot.Dispose();
                conn.Close();
            }

            return 0;
        }
        public static int delete(string tbName, string[] args, string[] vals)
        {
            int line;//删除的行数
            string afterw = "";
            for (int i = 0; i < args.Length; i++)
            {
                afterw += querystring.querynvl(args[i], vals[i], "=", null, null)
                + " and ";
            }
            afterw = afterw.Substring(0, afterw.LastIndexOf("and"));
            string sql = "delete from " + tbName + " where " + afterw;
            OdbcConnection conn = db.getdbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                ot.Rollback();//如操作失败，则事务回滚
                return -1;
            }
            finally
            {
                conn.Close();
            }
            //提交事务
            ot.Commit();
            ot.Dispose();
            return 0;
        }
        /// <summary>
        /// access数据库插入
        /// </summary>
        /// <param name="sql"></param>
        public void insert(string sql)
        {
            int line;
            OleDbConnection conn = accessdb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbTransaction ot = conn.BeginTransaction();//开始事务
            OleDbCommand ocmd = new OleDbCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                string i = "";
                ot.Commit();
                ot.Dispose();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                ot.Rollback();//如操作失败，则事务回滚
                return;
            }
            finally
            {
                conn.Close();
            }
            //提交事务
            //ot.Rollback();
        }
        /// <summary>
        /// access数据库删除
        /// </summary>
        /// <param name="sql"></param>
        public void del(string sql)
        {
            int line;
            OleDbConnection conn = accessdb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbTransaction ot = conn.BeginTransaction();//开始事务
            OleDbCommand ocmd = new OleDbCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                string i = "";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                ot.Rollback();//如操作失败，则事务回滚
                return;
            }
            finally
            {
                conn.Close();
                ot.Dispose();
            }
            //提交事务
            //ot.Rollback();
            ot.Commit();



        }
        /// <summary>
        /// mysql数据库插入
        /// </summary>
        /// <param name="sql"></param>
        public void insertdb(string sql)
        {
            int line;
            OdbcConnection conn = db.getdbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                string i = "";
                ot.Commit();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                ot.Rollback();//如操作失败，则事务回滚
                return;
            }
            finally
            {
                ot.Dispose();
                conn.Close();
            }
            //提交事务
            //ot.Rollback();
        }
        /// <summary>
        /// mysql数据库插入
        /// </summary>
        /// <param name="sql"></param>
        public int UpdateStanderDB(string sql, OdbcConnection conn)
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                int line = ocmd.ExecuteNonQuery();
                string i = "";
                ot.Commit();
                return line;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                ot.Rollback();//如操作失败，则事务回滚
            }
            //提交事务
            //ot.Rollback();
            finally
            {
                ot.Dispose();
                conn.Close();
            }
            return 0;
        }
        /// <summary>
        /// mysql数据库插入
        /// </summary>
        /// <param name="sql"></param>
        public int UpdateStanderDB(string sql, List<MySqlParameter> lMP, MySqlConnection conn)
        {
            int i = 0;
            int line = 0;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            for (i = 0; i < lMP.Count; i++)
            {
                cmd.Parameters.Add(lMP[i]);
            }
            MySqlTransaction ot = conn.BeginTransaction();
            try
            {
                line = cmd.ExecuteNonQuery();
                ot.Commit();
                return line;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                ot.Rollback();//如操作失败，则事务回滚
            }
            //提交事务
            //ot.Rollback();
            finally
            {
                ot.Dispose();
                conn.Close();
            }
            return 0;
        }
        /// <summary>
        /// 标准数据库转库正确性验证
        /// </summary>
        /// <param name="sql"></param>
        public bool UpdateTranstStanderDB(string sql, OdbcConnection conn, int lines)
        {
            bool result = false;
            int line = 0;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                ot.Dispose();
                conn.Close();
                return result;
                //string i = "";
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                ot.Rollback();//如操作失败，则事务回滚
                ot.Dispose();
                conn.Close();
                return false;
            }
          
           
            

        }
        /// <summary>
        /// 更新access数据库
        /// </summary>
        public void updateaccess(string sql)
        {
            int line = 0;
            OleDbConnection conn = accessdb.getconn();
            if (conn.State != ConnectionState.Open) conn.Open();
            OleDbCommand ocmd = new OleDbCommand(sql, conn);
            OleDbTransaction ot = conn.BeginTransaction();//开始事务
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                ot.Commit();
            }
            catch (Exception e)
            {
                ExceptionLog.ExceptionWrite(e, sql);
                ot.Rollback();
                return;
            }
            finally
            {
                ot.Dispose();
                conn.Close();
            }



        }
        //使用odbc执行创建表
        public static void ExcuteSql(OdbcConnection conn, string sql)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            ocmd.Transaction = ot;//绑定事务
            try
            {
                ocmd.ExecuteNonQuery();
                //提交事务
                ot.Commit();
                ot.Dispose();
            }
            catch (Exception e)
            {
                ExceptionLog.ExceptionWrite(e, sql);
                ot.Rollback();//如操作失败，则事务回滚
                return;
            }
            finally
            {
                conn.Close();
            }




        }
        public int del(string tbName, string[] args, string[] vals)
        {
            int line;//删除的行数
            string afterw = "";
            for (int i = 0; i < args.Length; i++)
            {
                afterw += querystring.querynvl(args[i], vals[i], "=", null, null)
                + " and ";
            }
            afterw = afterw.Substring(0, afterw.LastIndexOf("and"));
            string sql = "delete from " + tbName + " where " + afterw;
            OdbcConnection conn = db.getdbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcTransaction ot = conn.BeginTransaction();//开始事务
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                ot.Commit();
                ot.Dispose();
            }
            catch (Exception e)
            {
                ExceptionLog.ExceptionWrite(e, sql);
                ot.Rollback();//如操作失败，则事务回滚
                return -1;
            }
            //提交事务
            finally
            {
                conn.Close();
            }

            return 0;
        }
        /// <summary>
        /// sql数据库更新
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        public void updatexmdb(string sql, string xmname)
        {
            int line;
            SqlConnection conn = db.getconnbydbase("SMOSDB");
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlTransaction ot = conn.BeginTransaction();//开始事务
            SqlCommand ocmd = new SqlCommand(sql, conn);
            ocmd.Transaction = ot;//绑定事务
            try
            {
                line = ocmd.ExecuteNonQuery();
                string i = "";
            }
            catch
            {
                ot.Rollback();//如操作失败，则事务回滚
            }
            //提交事务
            //ot.Rollback();
            ot.Commit();
            ot.Dispose();
            conn.Close();

        }

    }
}