﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Odbc;
using NFnet_MODAL;

namespace NFnet_BLL
{
    public class Unit
    {
        private string unitName, pro, city, country, addr, tel, email, natrue, linkman, aptitude, police, taxproof, state, dbName;

        public string DbName
        {
            get { return dbName; }
            set { dbName = value; }
        }

        public string State
        {
            get { return state; }
            set { state = value; }
        }
        
        
        public string Taxproof
        {
            get { return taxproof; }
            set { taxproof = value; }
        }

        public string Police
        {
            get { return police; }
            set { police = value; }
        }

        public string Aptitude
        {
            get { return aptitude; }
            set { aptitude = value; }
        }

        public string Linkman
        {
            get { return linkman; }
            set { linkman = value; }
        }

        public string Natrue
        {
            get { return natrue; }
            set { natrue = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public string Addr
        {
            get { return addr; }
            set { addr = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Pro
        {
            get { return pro; }
            set { pro = value; }
        }
        
        public string UnitName
        {
            get { return unitName; }
            set { unitName = value; }
        }
        /// <summary>
        /// 保存机构信息
        /// </summary>
        public string SaveUnitInfo()
        {
            
            Sql_DAL_E dal = new Sql_DAL_E();
            string mess = dal.UnitInsertExcute(this);
            return mess;
        }
        /// <summary>
        /// 更新机构信息
        /// </summary>
        /// <returns></returns>
       public string UpdateUnitInfo()
       {
           
           Sql_DAL_E dal = new Sql_DAL_E();
           string mess = dal.UnitUpdateExcute(this);
           return mess;
       }
        /// <summary>
        /// 获取机构信息
        /// </summary>
        /// <returns></returns>
       public Unit GetUnit()
       {
           Sql_DAL_E dal = new Sql_DAL_E();
           Unit unit = dal.UnitSelectExcute(this);
           return unit;
       }
       /// <summary>
       /// 获取机构信息列表
       /// </summary>
       /// <returns></returns>
       public List<Unit> GetUnitList()
       {
           Sql_DAL_E dal = new Sql_DAL_E();
           List<Unit> ls = dal.UnitSelectListExcute(this);
           return ls;
       }
        /// <summary>
        /// 上传文件选择框中选择的文件
        /// </summary>
        /// <param name="fu"></param>
       public string ImageUploadInsert(FileUpload fu,string postName)
       {
        string extend = Path.GetExtension(fu.FileName);
        string path = postName + "\\" + this.unitName+extend;
       
        ImageFileDel(path);
        try
        {
            fu.SaveAs(path);
            return FileImagePath(path);
        }
        catch (Exception ex)
        {
            ExceptionLog.ExceptionWrite(ex,"文件保存出错");
            return null;
        }

       }
        /// <summary>
        /// 文件删除
        /// </summary>
        /// <param name="path"></param>
       public void ImageFileDel(string path)
       {
           string filepath = path;//此处应用绝对路径
           try
           {
               if (File.Exists(filepath))
               {
                   File.Delete(filepath);
               }
           }
           catch(Exception ex)
           {
               ExceptionLog.ExceptionWrite(ex,"文件删除出错");
           }
       }
        /// <summary>
        /// 绝对路径转化成相对路径
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
       public string FileImagePath(string fullPath)
       {
           try
           {
               return fullPath.Substring(fullPath.LastIndexOf("\\") + 1);
           }
           catch(Exception ex)
           {
               ExceptionLog.ExceptionWrite(ex,"文件名截取出错");
               return null;
           }
       }

       public DataTable GetUnitTable()
       {
           Sql_DAL_E dal = new Sql_DAL_E();
           DataTable dt = dal.UnitTableExcute(this);
           return dt;
       }
        //获取全站仪的工程名
       public List<string> GetProjectNameList()
       {
           Sql_DAL_E dal = new Sql_DAL_E();
           List<string> ls = dal.ProjectNameExcute(this);
           return ls;
       }

       //更新监测项目表
       public void ProjectInitSet(List<string> projectNameList)
       {
           Sql_DAL_E dal = new Sql_DAL_E();
           
            foreach(string projectName in projectNameList)
            {
                xm xminfo = new xm { Xmname = projectName, Jcdw = this.UnitName, Dbase = this.DbName};
                dal.projectInfoInsertExcute(xminfo);
            }
       }
        //创建新的机构数据库
       public string UnitDbCreate(string dbname,string path)
       {
           Sql_DAL_E dal = new Sql_DAL_E();
           return dal.UnitDbCreate(dbname,path);
            
       }
        
    }
}