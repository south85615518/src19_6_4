﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.IO;
using NFnet_DAL;
using System.Data;
using NFnet_MODAL;
using NFnet_Model;
namespace NFnet_BLL
{
    public class FileDecode
    {
        public static string TxtFileReader(string path, OdbcConnection conn)
        {

            path = path.Substring(0, path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(path);
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string insertCmd = "";
                string sqlCmd = "select * from cycangnet where POINT_NAME =@POINT_NAME and Time =@Time and TaskName = @TaskName";
                char delimChar = ',';
                string[] split = null;
                int i = 0;
                string strTemp = sr.ReadLine();
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    if (i < 4) { lshead.Add(strTemp); strTemp = sr.ReadLine(); i++; continue; }
                    split = strTemp.Split(delimChar);

                    insertCmd = @"insert into fmos_cycdirnet(CYC, POINT_NAME,N,E,Z,This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Time,TaskName) values (@CYC, @POINT_NAME,@N,@E,@Z,@This_dN,@This_dE,@This_dZ,@Ac_dN,@Ac_dE,@Ac_dZ,@Time,@TaskName)";

                    sqlCmd = string.Format("select * from fmos_cycdirnet where POINT_NAME ='{0}' and Time ='{1}' and taskName = '{2}'", split[0], split[12].Replace("/", "-"), database.SubString(lshead[0]));
                    if (conn.State != ConnectionState.Open) conn.Open();
                    try
                    {

                        List<string> ls = querysql.querystanderlist(sqlCmd,conn);
                        if (ls.Count > 0)
                        {
                            strTemp = sr.ReadLine();
                            continue;
                        }
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        ExceptionLog.ExceptionWrite("数据写入SQL数据库出错!");
                    }
                    insertCmd = insertCmd.Replace("@POINT_NAME","'"+split[0]+"'");
                    insertCmd = insertCmd.Replace("@CYC","'"+split[1]+"'");
                    insertCmd = insertCmd.Replace("@N","'"+split[2]+"'");
                    insertCmd = insertCmd.Replace("@E","'"+split[3]+"'");
                    insertCmd = insertCmd.Replace("@Z","'"+split[4]+"'");
                    insertCmd = insertCmd.Replace("@This_dN","'"+ split[5]+"'");
                    insertCmd = insertCmd.Replace("@This_dE","'"+split[6]+"'");
                    insertCmd = insertCmd.Replace("@This_dZ","'"+ split[7]+"'");
                    insertCmd = insertCmd.Replace("@Ac_dN","'"+split[8]+"'");
                    insertCmd = insertCmd.Replace("@Ac_dE","'"+ split[9]+"'");
                    insertCmd = insertCmd.Replace("@Ac_dZ", "'"+ split[10]+"'");
                    insertCmd = insertCmd.Replace("@Time", "'"+ split[12]+"'");
                    insertCmd = insertCmd.Replace("@TaskName", "'" + database.SubString(lshead[0]) + "'");
                    OdbcCommand cmd = new OdbcCommand(insertCmd, conn);
                    if (conn.State != ConnectionState.Open) conn.Open();
                    try
                    {

                        cmd.ExecuteNonQuery();
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    conn.Close();
                    strTemp = sr.ReadLine();
                }
                sr.Close();
                fs.Close();
                FileReName(path, "周期数据文件\\" + database.SubString(lshead[2]) + ".txt");
                return "周期数据文件\\" + database.SubString(lshead[2]) + ".txt";
            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite(ex, "文件读取出错!");
            }
            return null;

        }
        public static string TxtFileOrglReader(string path, OdbcConnection conn)
        {

            path = path.Substring(0, path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(path);
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string insertCmd = "";
                string sqlCmd = "select * from fmos_orgldata where POINT_NAME =@POINT_NAME and DateTime =@Time and projectName = @projectName";
                char delimChar = ',';
                string[] split = null;
                int i = 0;
                string strTemp = sr.ReadLine();
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    if (i < 4) { lshead.Add(strTemp); strTemp = sr.ReadLine(); i++; continue; }
                    split = strTemp.Split(delimChar);

                    sqlCmd = string.Format("select * from fmos_orgldata where POINT_NAME ='{0}' and search_time ='{1}' and taskName = '{2}'", split[0], split[13].Replace("/", "-"), database.SubString(lshead[0]));
                    
                    if (conn.State != ConnectionState.Open) conn.Open();
                    try
                    {
                        List<string> ls = querysql.querystanderlist(sqlCmd,conn);
                        if (ls.Count > 0)
                        {
                            strTemp = sr.ReadLine();
                            continue;
                        }
                    }
                    catch (System.Data.Odbc.OdbcException e)
                    {
                        ExceptionLog.ExceptionWrite("数据写入SQL数据库出错!");
                    }
                    insertCmd = @"insert into fmos_orgldata(POINT_NAME,CYC,CH,POINT_HAR,POINT_VAR,HAR,VAR,SD,HarAcc,VarAcc,SdAcc,CrossIncl,LenIncl,search_time,StationMark,StationName,TARGETHIGHT,dN,dE,dZ,N,E,Z,taskName) 
values ('" + split[0] + "'," + split[1] + "," + split[2] + ",'" + split[3] + "','" + split[4] + "'," + split[5] + "," + split[6] + "," + split[7] + "," + split[8] + "," + split[9] + "," + split[10] + "," + split[11] + "," + split[12] + ",'" + split[13] + "','" + split[14] + "','" + split[15] + "'," + split[16] + "," + split[17] + "," + split[18] + "," + split[19] + "," + split[20] + "," + split[21] + "," + split[22] + ",'" + database.SubString(lshead[0]) + "')";
                    OdbcCommand cmd = new OdbcCommand(insertCmd, conn);


                    if (conn.State != ConnectionState.Open) conn.Open();
                    try
                    {

                        cmd.ExecuteNonQuery();
                    }
                    catch (System.Data.Odbc.OdbcException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    conn.Close();
                    strTemp = sr.ReadLine();
                }
                sr.Close();
                fs.Close();
                FileReName(path, "周期数据文件\\" + database.SubString(lshead[2]) + ".txt");
                return "周期数据文件\\" + database.SubString(lshead[2]) + ".txt";
            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite(ex, "文件读取出错!");
            }
            return null;

        }
        public static string FileDecodeTxt(string path, OdbcConnection conn,string xmname)
        {
            string dataType = "";
            string typepath = path.Substring(0, path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(typepath);
            FileStream fsTxt = new FileStream(typepath, FileMode.Open, FileAccess.Read);
            
                ExceptionLog.ExceptionWrite("锁定文件" + path);
                StreamReader sr = new StreamReader(fsTxt, TxtEncoding.Encoding);
                try
                {
                    int i = 0;
                    string lineText = "";
                    for (i = 0; i < 4; i++)
                    {
                        lineText = sr.ReadLine();
                        if (i == 0)
                        {
                            if (database.SubString(lineText).TrimEnd() != xmname)
                            {
                                //fs.Unlock(0, fs.Length);
                                if (sr != null)
                                    sr.Close();
                                if (fsTxt != null)
                                    fsTxt.Close();
                                //ExceptionLog.ExceptionWrite("解锁文件之后删除" + path);
                                File.Delete(typepath);
                                return "-3";
                            }
                        }
                        if (i == 3)
                        {
                            dataType = lineText;
                            //fs.Unlock(0, fs.Length);
                            if (sr != null)
                                sr.Close();
                            if (fsTxt != null)
                                fsTxt.Close();

                            //ExceptionLog.ExceptionWrite("解锁文件之后解析" + path);
                        }
                    }
                    switch (database.SubString(dataType).TrimEnd())
                    {
                        case "结果数据":
                            return TxtFileReader(path, conn);
                        //break;
                        case "原始数据":
                            return TxtFileOrglReader(path, conn);
                        //break;
                        case "学习点":
                            return TotalStation.BLL.fmos_obj.studypoint.TxtFileReader(path,conn);
                           
                            //return TxtFileReader(path, conn);
                        //break;
                        case "测站":
                            return TotalStation.BLL.fmos_obj.stationinfo.TxtFileReader(path, conn);
                        //break;
                        case "点组":
                            return TotalStation.BLL.fmos_obj.stationinfo.TxtFileReader(path, conn);
                        //break;
                        case "设置":
                            return TotalStation.BLL.fmos_obj.setting.TxtFileReader(path, conn);
                        //break;
                        case "时间":
                            return TotalStation.BLL.fmos_obj.timetask.TxtFileReader(path, conn);
                        //break;
                    }


                }
                catch (Exception ex)
                {
                    //fs.Unlock(0, fs.Length);
                    if (sr != null)
                        sr.Close();
                    if (fsTxt != null)
                        fsTxt.Close();

                    //ExceptionLog.ExceptionWrite("出错之后解锁文件" + path);
                    ExceptionLog.ExceptionWrite(ex.Message);
                }
               
            //}
                //fsTxt = new FileStream(typepath, FileMode.Truncate, FileAccess.Write, FileShare.Write);
                //if (fsTxt != null)
                //    fsTxt.Close();
            return null;
        }
        public static List<string> DbParamLoad(string type)
        {
            List<string> oracleTxt = new List<string>();
            List<string> sqlserverTxt = new List<string>();
            StreamReader sr = null;
            try
            {
                sr = File.OpenText("数据库配置文件\\dbproperty.txt");
                string linetxt = "";
                while ((linetxt = sr.ReadLine().Trim()) != "分隔处")
                {
                    if (linetxt.Trim() != "sqlserver:")
                        sqlserverTxt.Add(linetxt);
                }
                while ((linetxt = sr.ReadLine()) != null)
                {
                    if (linetxt == "") continue;
                    if (linetxt.Trim() != "oracle:")
                        oracleTxt.Add(linetxt);
                }
                if (type == "sqlserver")
                {
                    return sqlserverTxt;
                }
                else
                {
                    return oracleTxt;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, "数据库属性文件读取失败");
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
            return null;
        }
        public static string SubString(string str)
        {
            return str.Substring(str.IndexOf(":") + 1);
        }
        public static void FileReName(string path, string newName)
        {
            try
            {
                //FileStream fs = File(path);
                //fs.Close();

                //fs.CanWrite
                File.Copy(path, newName);


            }
            catch (Exception ex)
            {

            }

        }
        public static void ReciveLog(string path, string lineTxt)
        {
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamWriter sw = new StreamWriter(fs, TxtEncoding.Encoding);
            fs.Seek(0, SeekOrigin.End);
            try
            {
                sw.WriteLine(lineTxt);

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();

            }
        }
    }
}