﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Data;
using NFnet_DAL;
using NFnet_MODAL;

namespace NFnet_BLL
{
    public class Instrument
    {
        private string instrumentName, vender, instrumentNo, instrumentType, verificationDate, validityPeriod, verificationProof, unitName;

        public string UnitName
        {
            get { return unitName; }
            set { unitName = value; }
        }

        public string VerificationProof
        {
            get { return verificationProof; }
            set { verificationProof = value; }
        }

        public string ValidityPeriod
        {
            get { return validityPeriod; }
            set { validityPeriod = value; }
        }

        public string VerificationDate
        {
            get { return verificationDate; }
            set { verificationDate = value; }
        }

        public string InstrumentType
        {
            get { return instrumentType; }
            set { instrumentType = value; }
        }

        public string InstrumentNo
        {
            get { return instrumentNo; }
            set { instrumentNo = value; }
        }

        public string Vender
        {
            get { return vender; }
            set { vender = value; }
        }

        public string InstrumentName
        {
            get { return instrumentName; }
            set { instrumentName = value; }
        }
        /// <summary>
        /// 保存设备信息
        /// </summary>
        public  string SaveInstrumentInfo()
        {

            Sql_DAL_E dal = new Sql_DAL_E();
            string mess = dal.InstrumentInsertExcute(this);
            return mess;
        }

        public  DataTable GetInstrumentTable()
        {
            Sql_DAL_E dal = new Sql_DAL_E();
            DataTable dt = dal.InstrumentTableExcute(this);
            return dt;
        }

    }
}