﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using NFnet_MODAL;
using NFnet_DAL;

namespace NFnet_BLL
{
    public class xm
    {

        private string jcCreateDate;//项目建立日期
        private string province, city, area, lng, lat, jcdw, wtdw, jcry, respone, telphone, xmlx, jcrq, dbase;

        public string Dbase
        {
            get { return dbase; }
            set { dbase = value; }
        }

        public string Jcrq
        {
            get { return jcrq; }
            set { jcrq = value; }
        }

        public string Xmlx
        {
            get { return xmlx; }
            set { xmlx = value; }
        }

        public string Telphone
        {
            get { return telphone; }
            set { telphone = value; }
        }

        public string Respone
        {
            get { return respone; }
            set { respone = value; }
        }

        public string Jcry
        {
            get { return jcry; }
            set { jcry = value; }
        }

        public string Wtdw
        {
            get { return wtdw; }
            set { wtdw = value; }
        }

        public string Jcdw
        {
            get { return jcdw; }
            set { jcdw = value; }
        }

        public string Lat
        {
            get { return lat; }
            set { lat = value; }
        }

        public string Lng
        {
            get { return lng; }
            set { lng = value; }
        }

        public string Area
        {
            get { return area; }
            set { area = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Province
        {
            get { return province; }
            set { province = value; }
        }
        private string id;//项目编号

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        private string xmname;//名称

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        private string xmaddress;//项目地址

        public string Xmaddress
        {
            get { return xmaddress; }
            set { xmaddress = value; }
        }
        private string xmbz;//备注

        public string Xmbz
        {
            get { return xmbz; }
            set { xmbz = value; }
        }
        private string creatMan;//创立者

        public string CreatMan
        {
            get { return creatMan; }
            set { creatMan = value; }
        }

        private string createTime;//创建时间

        public string CreateTime
        {
            get { return createTime; }
            set { createTime = value; }
        }
        private List<jclx> jcls = null;

        public List<jclx> Jcls
        {
            get { return jcls; }
            set { jcls = value; }
        }
        /// <summary>
        /// 由项目名称获取到项目的监测类型
        /// </summary>
        /// <returns></returns>
        public xm GetJclxByXmname(string xmname)
        {
            string dbname = querysql.queryaccessdbstring("select dbase from xmconnect where xmname ='" + xmname + "'");
            //querysql.querystanderdb("");
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            //声明项目实例
            xm xmdomain = new xm();
            xmdomain.xmname = xmname;

            DataTable dt = new DataTable();
            string sqlxm = "select * from xmconnect where xmname ='"+xmname+"'";
            querysql query = new querysql();
            dt = query.querytaccesstdb(sqlxm);
            DataView dv = new DataView(dt);
            foreach(DataRowView drv in dv)
            {
                xmdomain = new xm
                {
                    Xmname =xmname,
                    CreatMan = drv["createMan"].ToString(),
                    Xmaddress = drv["xmadress"].ToString(),
                    Xmbz = drv["xmbz"].ToString(),
                    Province = drv["provincial"].ToString(),
                    City = drv["cities"].ToString(),
                    Area = drv["countries"].ToString(),
                    Lng = drv["wd"].ToString(),
                    Lat = drv["jd"].ToString(),
                    Jcdw = drv["jcdw"].ToString(),
                    Wtdw = drv["delegateDw"].ToString(),
                    Jcry = drv["jcxmgm"].ToString(),
                    Respone = drv["jcAppealPerson"].ToString(),
                    Telphone = drv["telphone"].ToString(),
                    Xmlx = drv["xmType"].ToString(),
                    Jcrq = drv["jcCreateDate"].ToString()
                };
                break;
            }
            
            List<jclx> jcls = new List<jclx>();
            string sql = "select type from xmconnect where xmname='"+xmname+"'";
            List<string> ls = querysql.queryaccessdbstring(sql).Split(',').ToList();
            foreach (string str in ls)
            {
                jclx jcl = new jclx();
                jcl.Jclname = str;
                GetYqByJclx(jcl,conn);
                jcls.Add(jcl);
            }
            xmdomain.Jcls = jcls;
            xmdomain.Xmname = xmname;
            return xmdomain;
        }
        /// <summary>
        /// 由项目的监测类型获得仪器
        /// </summary>
        /// <returns></returns>
        public void GetYqByJclx(jclx jcl, OdbcConnection conn)
        {
            List<string> ls = new List<string>();
            List<yq> yqs = new List<yq>();
            //此处将查询仪器参数表改成查询多个表来判断该监测类型是否已经导入数据
            //if(is)
            
            //string yqs = 
            //string sql = "select Yq from YQParamTab where jclx='" + jcl.Jclname + "'";
            //ls = querysql.querystanderlist(sql, conn);
            ls = DssInit(jcl.Jclname,conn);
            //if (ls == null) return;
            foreach (string yqname in ls)
            {
                yq yqdomain = new yq();
                yqdomain.Yqname = yqname;
                GetCdsByYqlx(yqdomain, conn);
                yqs.Add(yqdomain);
            }
            jcl.Yqs = yqs;
            
        }
        public bool IsYqExist(string jclx,OdbcConnection conn)
        {
            string sql = "";
            switch(jclx)
            {
                case "水位计": sql = "select count(*) from sensor "; break;
                case "雨量计": sql = "select count(*) from ylpotName "; break;
                case "测斜仪": sql = "select count(*) from cxpotname "; break;
                case "GPS": sql = "select count(*) from gpsbasestation "; break;
                case "全站仪": sql = "select count(*) from studypoint"; break;
            }
            string cont = querysql.querystanderstr(sql,conn);
            if (Int32.Parse(cont) > 0)
            {
                return true;
            }
            return false;
        }
        public List<string> DssInit(string jclx,OdbcConnection conn)
        {
            List<string> ls = new List<string>();
            Dictionary<string, string> dss = new Dictionary<string, string>();
            dss.Add("水位", "水位计");
            dss.Add("雨量", "雨量计");
            dss.Add("测斜", "测斜仪");
            dss.Add("位移", "GPS,全站仪");
            if (!dss.ContainsKey(jclx)) return ls;
            string yqstr = dss[jclx];
            List<string> yqs = yqstr.Split(',').ToList();
            foreach(string yq in yqs)
            {
                if (IsYqExist(yq,conn))
                    ls.Add(yq);
            }
            return ls;
        }
        /// <summary>
        /// 由仪器类型获得测点
        /// </summary>
        /// <returns></returns>
        public void GetCdsByYqlx(yq yqdomain, OdbcConnection conn)
        {
            string sql = GetCdsSqlByYq(yqdomain.Yqname);
            List<cdcs> cds = new List<cdcs>();
            List<string> ls = querysql.querystanderlist(sql,conn);
            foreach (string str in ls)
            {
                cdcs cd = new cdcs();
                cd.Cdname = str;
                cds.Add(cd);
            }
            yqdomain.Cds = cds;
            
        }
        public string GetCdsSqlByYq(string yqname)
        {
            string sql = "";
            switch (yqname)
            {
                case "水位计": sql = "select sjbh from sensor "; break;
                case "雨量计": sql = "select Name from ylpotname "; break;
                case "测斜仪": sql = "select Name from cxpotname "; break;
                case "GPS": sql = "select smos_Name from gpsbasestation "; break;
                case "全站仪": sql = "select POINT_NAME from studypoint "; break;
            }
            return sql;
        }
    }
}