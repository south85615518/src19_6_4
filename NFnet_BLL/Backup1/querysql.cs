﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using NFnet_MODAL;
namespace NFnet_DAL
{
    /// <summary>
    /// 根据sql语句查询并返回值
    /// </summary>
    public class querysql
    {
        public static database db = new database();
        public static accessdbse adb = new accessdbse();
        //使用MySQL查询字符串
        public static string querystring(string sql)
        {
            string seg = "";
            OdbcConnection conn = db.getdbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OdbcCommand ocmd = new OdbcCommand(sql, conn);
                OdbcDataReader reader = ocmd.ExecuteReader();
                if (reader.Read())
                {
                    seg = reader[0].ToString();
                }
                reader.Read();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }
            return seg;
        }
        //根据项目名称和检测类型查询list
        //使用sqlserver查询查询list
        //根据sql语句查找并返回字符数组
        public static List<string> querysqllist(string sql, string xmname)
        {
            List<string> strl = new List<string>();
            OdbcConnection conn = (OdbcConnection)db.getdbconn();//连接mysql数据库
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OdbcCommand ocmd = new OdbcCommand(sql, conn);
                OdbcDataReader reader = ocmd.ExecuteReader();
                if (reader.Read())
                {
                    string[] st = reader[0].ToString().Split(',');
                    strl = st.ToList<string>();

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }
            return strl;
        }
        //使用sqlserver查询字符串
        public static string querysqlstring(string sql)
        {
            string seg = "";
            OdbcConnection conn = db.getsqldbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            OdbcDataReader reader = ocmd.ExecuteReader();
            if (reader.Read())
            {
                seg = reader[0].ToString();
            }
            reader.Read();
            conn.Close();
            return seg;
        }
        //使用sqlserver查询字符串
        public static string queryxmsqlstring(string sql)
        {
            string seg = "";
            SqlConnection conn = db.getconnbydbase("SMOSDB");
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand ocmd = new SqlCommand(sql, conn);
            SqlDataReader reader = ocmd.ExecuteReader();
            if (reader.Read())
            {
                seg = reader[0].ToString();
            }
            reader.Read();
            conn.Close();
            return seg;

        }
        /// <summary>
        /// 根据sql语句查找并返回字符数组
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static List<string> queryxmsqllist(string sql)
        {
            List<string> strl = new List<string>();
            SqlConnection conn = db.getconnbydbase("SMOSDB");
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand ocmd = new SqlCommand(sql, conn);
            SqlDataReader reader = ocmd.ExecuteReader();
            while (reader.Read())
            {
                string strh = "";
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    strh += reader[i].ToString() + ",";
                }
                strh = strh.Substring(0, strh.LastIndexOf(","));
                strl.Add(strh);
            }
            conn.Close();
            return strl;
        }
        //使用access查询字符串
        public static string queryaccessdbstring(string sql)
        {
            string seg = "";
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OleDbCommand ocmd = new OleDbCommand(sql, conn);
                OleDbDataReader reader = ocmd.ExecuteReader();
                if (reader.Read())
                {
                    seg = reader[0].ToString();
                }
                reader.Read();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }
            return seg;
        }
        //使用access查询excel字符串
        public static string queryaccessdbstring(string sql, OleDbConnection conn)
        {
            string seg = "";
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OleDbCommand ocmd = new OleDbCommand(sql, conn);
            OleDbDataReader reader = ocmd.ExecuteReader();
            if (reader.Read())
            {
                seg = reader[0].ToString();
            }
            reader.Read();
            conn.Close();
            return seg;
        }
        //使用sqlserver查询查询list
        //根据sql语句查找并返回字符数组
        public static List<string> querysqllist(string sql)
        {
            List<string> strl = new List<string>();
            SqlConnection conn = db.getsqlconnstr();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand ocmd = new SqlCommand(sql, conn);
            SqlDataReader reader = ocmd.ExecuteReader();
            while (reader.Read())
            {
                string strh = "";
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    strh += reader[i].ToString() + ",";
                }
                strh = strh.Substring(0, strh.LastIndexOf(","));
                strl.Add(strh);
            }
            conn.Close();
            return strl;
        }
        //使用access查询list
        public static List<string> queryaccesslist(string sql)
        {
            List<string> strl = new List<string>();
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OleDbCommand ocmd = new OleDbCommand(sql, conn);
                OleDbDataReader reader = ocmd.ExecuteReader();
                while (reader.Read())
                {
                    string strh = "";
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        strh += reader[i].ToString() + ",";
                    }
                    strh = strh.Substring(0, strh.LastIndexOf(","));
                    strl.Add(strh);
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }

            return strl;
        }
        //使用mysql查询list
        public static List<string> querylist(string sql)
        {
            List<string> strl = new List<string>();
            OdbcConnection conn = db.getdbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            OdbcDataReader reader = ocmd.ExecuteReader();
            while (reader.Read())
            {
                string strh = "";
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    strh += reader[i].ToString() + ",";
                }
                strh = strh.Substring(0, strh.LastIndexOf(","));
                strl.Add(strh);
            }
            conn.Close();
            return strl;
        }
        /// <summary>
        /// 使用mysql查询shema数据库
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static List<string> queryschemalist(string sql)
        {
            List<string> strl = new List<string>();
            OdbcConnection conn = db.getshemadbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcCommand ocmd = new OdbcCommand(sql, conn);
            OdbcDataReader reader = ocmd.ExecuteReader();
            while (reader.Read())
            {
                string strh = "";
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    strh += reader[i].ToString() + ",";
                }
                strh = strh.Substring(0, strh.LastIndexOf(","));
                strl.Add(strh);
            }
            conn.Close();
            return strl;
        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static DataTable querystanderdb(string sql, OdbcConnection conn)
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            try
            {
                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
                return null;
            }
            finally
            {
                conn.Close();
            }

            return dt;

        }
        //使用mysql查询转换标准表/目标表的list
        public static List<string> querystanderlist(string sql, OdbcConnection conn)
        {
            List<string> strl = new List<string>();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {
                OdbcCommand ocmd = new OdbcCommand(sql, conn);
                OdbcDataReader reader = ocmd.ExecuteReader();
                while (reader.Read())
                {
                    string strh = "";
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        strh += reader[i].ToString() + ",";
                    }
                    strh = strh.Substring(0, strh.LastIndexOf(","));
                    strl.Add(strh);
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }

            return strl;
        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static string querystanderstr(string sql, OdbcConnection conn)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            // sql = "select * from select * from 红山龙光_m2cs_cycangent";
            try
            {
                OdbcCommand cmd = new OdbcCommand(sql, conn);

                OdbcDataReader read = cmd.ExecuteReader();

                if (read.Read())
                {
                    return read[0].ToString();
                }

            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }
            return null;
        }
        /// <summary>
        /// 使用mysql查询转换标准表/目标表获取表的字段
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static List<string> QueryStandDBColumns(string tabname, OdbcConnection conn)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            List<string> ls = new List<string>();
            string sql = "select * from " + tabname + " limit 1,4 ";
            DataTable dt = querystanderdb(sql, conn);
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ls.Add(dt.Columns[i].ToString());
            }
            return ls;
        }
        /// <summary>
        /// 使用sqlserver查询目标表
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static DataTable QuerySSTargetDB(string sql, SqlConnection conn)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            DataTable dt = new DataTable();
            try
            {

                SqlDataAdapter oda = new SqlDataAdapter(sql, conn);

                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }
        /// <summary>
        /// 使用sqlserver查询字符串
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public static string QuerySSTargetDBStr(string sql, SqlConnection conn)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            try
            {

                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader read = cmd.ExecuteReader();
                if (read.Read())
                {
                    return read[0].ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }
            return null;

        }
        //使用mysql查询表
        public DataTable querytdb(string sql)
        {
            OdbcConnection conn = db.getdbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            conn.Close();
            return dt;

        }
        /// <summary>
        /// 使用sqlserver查询表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable queryxmtdb(string sql)
        {
            SqlConnection conn = db.getconnbydbase("SMOSDB");
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlDataAdapter oda = new SqlDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            conn.Close();
            return dt;

        }
        //使用sqlserver查询查询表
        public DataTable querytsqldb(string sql)
        {
            OdbcConnection conn = db.getsqldbconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            conn.Close();
            return dt;

        }
        //使用sqlserver根据连接器查询表
        public DataTable QuerySqlLjqDb(string sql, SqlConnection conn)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlDataAdapter oda = new SqlDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            conn.Close();
            return dt;

        }
        //使用access查询表
        public DataTable querytaccesstdb(string sql)
        {
            OleDbConnection conn = adb.getconn();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            DataTable dt = new DataTable();
            try
            {
                OleDbDataAdapter oda = new OleDbDataAdapter(sql, conn);

                oda.Fill(dt);
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex, sql);
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }
        
    }
}