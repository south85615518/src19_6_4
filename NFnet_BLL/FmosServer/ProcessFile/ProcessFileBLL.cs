﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Tool;
using NFnet_BLL.XmInfo.pub;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.ProjectInfo;
using NFnet_MODAL;
using NFnet_BLL.UserProcess;
using System.Text;
using NFnet_BLL.DisplayDataProcess.pub;
namespace NFnet_BLL.DataImport.ProcessFile
{
    /// <summary>
    /// 文件业务逻辑处理类
    /// </summary>
    public class ProcessFileBLL
    {

        public static ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public static ProcessLayoutBLL processLayoutBLL = new ProcessLayoutBLL();
        /// <summary>
        /// 文件上传处理
        /// </summary>
        /// <param name="stream">文件流</param>
        /// <param name="path">上传路径</param>
        /// <returns></returns>
        public bool FileUploadProcess(Stream stream, string path)//工具函数
        {
            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }
           
            byte[] buff = new byte[1024];
            FileStream fs = null;
            try
            {
                fs = File.Open(path, FileMode.Create, FileAccess.Write);
                while (stream.Read(buff, 0, buff.Length) > 0)
                {
                    fs.Write(buff, 0, buff.Length);
                    buff = new byte[1024];
                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("文件写入出错,错误信息" + ex.Message);
                return false;
            }
            finally
            {
                if (fs != null)
                    fs.Close();

            }

        }

        /// <summary>
        /// Json写入文件
        /// </summary>
        /// <param name="stream">文件流</param>
        /// <param name="path">上传路径</param>
        /// <returns></returns>
        public bool JsonUploadProcess(string json, string path)//工具函数
        {


               
                ExceptionLog.ExceptionWrite("源数据"+json);
                if (!Directory.Exists(Path.GetDirectoryName(path))) Directory.CreateDirectory(Path.GetDirectoryName(path));
                FileStream fs = File.Open(path, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                try
                {
                    sw.Write(json);
                    return true;
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("json数据写入文件出错,错误信息:" + ex.Message);
                    return false;
                }
                finally
                {
                    if (sw != null)
                        sw.Close();
                    if (fs != null)
                        fs.Close();
                }
            
        }


        /// <summary>
        /// 文件上传处理
        /// </summary>
        /// <param name="context"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<string> FilesUploadProcess(ProcessFileUploadModel model)
        {
            List<string> lspath = new List<string>();
            string pathTemp = model.path;

            int i = 0;

            string ext = Path.GetExtension(model.f.name);
            ImageFileDel(model.path + ext);
            string timeshot = "_Time_" + DateTime.Now.Minute + "_" + DateTime.Now.Second;
            string fileNowPath = model.path + timeshot;
            FileUploadProcess(model.f.st, fileNowPath + ext);
            lspath.Add(model.pathOut + timeshot + ext);
            return lspath;


        }
        public static string mssg = "";
        //保存文件并保存文件名
        public List<string> FilesUploadProcessWhiteFileName(ProcessFileUploadModelWhithFileNameModel model, string path/*绝对路径*/)
        {
            ExceptionLog.ExceptionWrite("上传文件路径" + path + "文件名" + model.f.name);
            List<string> pathList = new List<string>();
            List<string> lspath = new List<string>();
            string pathTemp = path;
            int i = 0;
            string fileName = "";
            string fileNowPath = "";
            string ext = Path.GetExtension(model.f.name);
            ExceptionLog.ExceptionWrite("上传文件路径" + path);
            if (path.IndexOf("/监测平面图/") != -1 || path.IndexOf("\\监测平面图\\") != -1)
            {
                string sizeinfo = path.Substring(path.LastIndexOf("!") + 1);
                string[] sizevals = sizeinfo.Split('×');
                path = path.Substring(0, path.LastIndexOf("!"));

                fileName = ReNameFileName(path + model.f.name);
                lspath.Add(fileName.Replace("\\", "/"));
                fileNowPath = fileName;
                FileUploadProcess(model.f.st, fileNowPath);
                string savepath = path + "ImageSize\\" + Path.GetFileName(fileName).Replace(".", "_{0}.");
                ExceptionLog.ExceptionWrite("图片源路径" + fileNowPath + "保存路径" + savepath);
                savepath = ImageHelper.ImageSize(fileNowPath, sizevals[0], sizevals[1], savepath);
                fileNowPath = savepath;
                ProcessLayoutBLL processLayoutBLL = new ProcessLayoutBLL();

                //FolderClear(context.Server.MapPath(path));
                Layout.BLL.basemap bll = new Layout.BLL.basemap();
                Layout.Model.basemap baseMapModel = new Layout.Model.basemap { folderUrl = fileNowPath.Replace("\\", "/"), name = Path.GetFileNameWithoutExtension(fileNowPath), remark = "", xmno = model.xmno };
                var processLayoutAddModel = new ProcessLayoutBLL.ProcessLayoutAddModel(model.xmname, baseMapModel);
                if (ProcessLayoutBaseMap(processLayoutAddModel))
                {
                    var processLayoutMaxIdModel = new ProcessLayoutBLL.ProcessLayoutMaxIdModel(model.xmno);
                    if (layoutBLL.ProcessLayoutMaxId(processLayoutMaxIdModel, out mssg))
                    {
                        int maxId = processLayoutMaxIdModel.maxId;
                        Layout.Model.basemapIdx basemapIdx = new Layout.Model.basemapIdx(model.xmno, fileName.Replace("\\", "/"), baseMapModel.folderUrl);
                        if (ProcessLayoutIdxCreate(basemapIdx, out mssg))
                        {

                        }

                    }

                }




            }
            else
            {
                fileName = ReNameFileName(path + "\\" + model.f.name);
                fileNowPath = fileName;
                FileUploadProcess(model.f.st, fileNowPath);
                lspath.Add(fileNowPath.Replace("\\", "/"));
            }

            return lspath;


        }
        public ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();

        public bool ProcessBaseMapDelete(string xmname, string path, out string mssg)
        {

            var processFolderUrlGetModel = new ProcessLayoutBLL.ProcessFolderUrlGetModel(ProcessAspectIndirectValue.GetXmnoFromXmname(xmname), path);
            layoutBLL.ProcessFolderUrlGet(processFolderUrlGetModel, out mssg);
            ExceptionLog.ExceptionWrite(mssg);
            var processLayoutBaseMapDeleteModel = new ProcessLayoutBLL.ProcessLayoutBaseMapDeleteModel(xmname, path);
            layoutBLL.ProcessLayoutBaseMapDelete(processLayoutBaseMapDeleteModel, out mssg);
            ExceptionLog.ExceptionWrite(mssg);
            ImageFileDel(path, out mssg);
            ImageFileDel(processFolderUrlGetModel.folderUrl, out mssg);
            return true;

        }
        public class ProcessBaseMapDeleteModel
        {
            public string xmname { get; set; }
            public string path { get; set; }
            public ProcessBaseMapDeleteModel(string xmname, string path)
            {
                this.xmname = xmname;
                this.path = path;
            }
        }
        public Layout.BLL.basemapIdx basemapBllIdx = new Layout.BLL.basemapIdx();
        public bool ProcessLayoutIdxCreate(Layout.Model.basemapIdx model, out string mssg)
        {
            return basemapBllIdx.Add(model, out mssg);
        }

        /// <summary>
        /// 监测平面图上传存库处理
        /// </summary>
        /// <param name="processModel"></param>
        /// <returns></returns>
        public bool ProcessLayoutBaseMap(NFnet_BLL.XmInfo.pub.ProcessLayoutBLL.ProcessLayoutAddModel processModel)
        {
            Layout.BLL.basemap bll = new Layout.BLL.basemap();
            string mssg = "";
            if (bll.Add(processModel.model, out mssg) && bll.CgAdd(processModel.model, out mssg))
            {
                mssg = "成功";
                return true;
            }
            else
            {
                mssg = "失败";
                return false;
            }

        }
        /// <summary>
        /// 监测平面图存库处理类
        /// </summary>
        public class ProcessLayoutBaseMapModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 监测平面类
            /// </summary>
            public Layout.Model.basemap model { get; set; }

            public ProcessLayoutBaseMapModel(string xmname, Layout.Model.basemap model)
            {
                this.xmname = xmname;
                this.model = model;
            }
        }



        /// <summary>
        /// 如果出现重复的文件名则自动重命名
        /// </summary>
        /// <param name="path">文件所在文件夹</param>
        /// <returns></returns>
        public string ReNameFileName(string path)
        {
            string dirPath = Path.GetDirectoryName(path);
            if (!Directory.Exists(dirPath)) { Directory.CreateDirectory(dirPath); return path; }
            DirectoryInfo folder = new DirectoryInfo(dirPath);
            string fileName = Path.GetFileNameWithoutExtension(path);
            int i = 0;
            string filePath = "";
            do
            {
                if (i == 0)
                    filePath = path;
                else
                    filePath = path.Replace(Path.GetExtension(path), "(" + i + ")" + Path.GetExtension(path));
                i++;
            }
            while (File.Exists(filePath));

            return filePath;
        }




        /// <summary>
        /// 文件实体类
        /// </summary>
        public class FileModel
        {
            /// <summary>
            /// 文件名
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 文件流
            /// </summary>
            public Stream st { get; set; }
            public FileModel(string name, Stream st)
            {
                this.name = name;
                this.st = st;
            }
        }
        /// <summary>
        /// 文件上传处理类
        /// </summary>
        public class ProcessFileUploadModel
        {
            /// <summary>
            /// 文件类实体
            /// </summary>
            public FileModel f { get; set; }
            /// <summary>
            /// 文件上传路径
            /// </summary>
            public string path { get; set; }
            public string pathOut { get; set; }
            public ProcessFileUploadModel(FileModel f, string path, string pathOut)
            {
                this.f = f;

                this.path = path;
                this.pathOut = pathOut;
            }
            public ProcessFileUploadModel() { }


        }
        /// <summary>
        /// 带有文件名的文件上传处理类
        /// </summary>
        public class ProcessFileUploadModelWhithFileNameModel : ProcessFileUploadModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessFileUploadModelWhithFileNameModel()
            {

            }
            public ProcessFileUploadModelWhithFileNameModel(string xmname, int xmno, FileModel f, string path)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.f = f;
                this.path = path;
            }


        }

        /// <summary>
        /// 文件删除
        /// </summary>
        /// <param name="path"></param>
        public void ImageFileDel(string path)
        {
            string filepath = path;//此处应用绝对路径
            try
            {
                //删除文件名中包含机构名，人物名的图片文件
                string dirPath = Path.GetDirectoryName(path);
                if (!Directory.Exists(dirPath)) { Directory.CreateDirectory(dirPath); return; }
                DirectoryInfo folder = new DirectoryInfo(dirPath);
                string fileName = Path.GetFileNameWithoutExtension(path);
                string fileNameKey = fileName.IndexOf("_Time_") != -1 ? fileName.Substring(0, fileName.IndexOf("_Time_")) : fileName;
                foreach (FileInfo fi in folder.GetFiles())
                {
                    if (fi.FullName.IndexOf("_Time_") == -1) continue;
                    //查找与机构,用户名相符的所有文件 
                    string fiFileName = Path.GetFileNameWithoutExtension(fi.FullName);

                    string nameKey = fiFileName.Substring(0, fiFileName.IndexOf("_Time_"));
                    if (fileNameKey == nameKey) fi.Delete();


                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("文件删除出错,错误信息:" + ex.Message);
            }
        }
        public bool ImageFileDel(string path, out string mssg)
        {
            try
            {
                File.Delete(path);
                mssg = string.Format("删除文件{0}成功", path);
                return true;
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除文件{0}出错,错误信息:" + ex.Message, path);
                return false;
            }
        }


        /// <summary>
        /// 文件上传前的登录验证
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileUploadLogin(ProcessFileUploadLoginModel model, out string mssg)
        {
            mssg = "";
            var processLoginModel = new ProcessLoginBLL.ProcessLoginNGNModel(model.userName, model.pass);
            if (ProcessLoginBLL.ProcessLogin(processLoginModel, out mssg))
            {
                model.role = processLoginModel.Role;
                model.currentObj = processLoginModel.currentUseInfo;
                return true;

            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 文件上传登陆验证请求参数类
        /// </summary>
        public class ProcessFileUploadLoginModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string userName { get; set; }
            /// <summary>
            /// 密码
            /// </summary>
            public string pass { get; set; }
            /// <summary>
            /// 当前用户信息
            /// </summary>
            public object currentObj { get; set; }
            /// <summary>
            /// 角色
            /// </summary>
            public Role role { get; set; }
            public ProcessFileUploadLoginModel(string userName, string pass)
            {
                this.userName = userName;
                this.pass = pass;
                this.role = Role.unknown;
            }
        }
        public static ProcessXmReference processXmReference = new ProcessXmReference();
        /// <summary>
        /// 数据文件上传处理
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessDataFileUpload(ProcessDataFileUploadModel model, out string mssg)
        {
            mssg = "";
            var processXmExistModel = new ProcessXmBLL.ProcessXmExistModel(model.userId, model.xmname);
            var processclient_xmanmeExistModel = new ProcessXmBLL.ExistsClient_XmnameModel(model.userId, model.xmname);
            if (!processXmBLL.ProcessXmExist(processXmExistModel, out mssg))
            {
                if (!processXmBLL.ExistsClient_Xmname(processclient_xmanmeExistModel, out mssg))
                {
                    //错误信息反馈
                    mssg = "-2";
                    return false;
                }
                var processClient_xmnameXmnoGetModel = new ProcessXmReference.ProcessClient_xmnameXmnoGetModel(model.xmname);
                if (processXmReference.ProcessClient_xmnameXmnoGet(processClient_xmnameXmnoGetModel, out mssg))
                {
                    var xmIntInfoLoadModel = new ProcessXmBLL.ProcessXmIntInfoLoadModel(processClient_xmnameXmnoGetModel.xmno);
                    if (processXmBLL.ProcessXmInfoLoad(xmIntInfoLoadModel, out mssg))
                        model.xmname = xmIntInfoLoadModel.model.xmname;
                }

                //if(processXmReference.)
                //var xmIntInfoLoadModel = new ProcessXmBLL.ProcessXmIntInfoLoadModel();
            }

           

            return true;

        }
        /// <summary>
        /// 数据文件上传处理类
        /// </summary>
        public class ProcessDataFileUploadModel
        {
            public string userId { get; set; }
            /// <summary>
            /// 上传目录
            /// </summary>
            public string path { get; set; }
            /// <summary>
            /// 单位名称
            /// </summary>
            public string unitName { get; set; }
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            public ProcessDataFileUploadModel(string userId,string path, string unitName, string xmname)
            {
                this.userId = userId;
                this.path = path;
                this.unitName = unitName;
                this.xmname = xmname;
            }
            public ProcessDataFileUploadModel()
            {
                
            }
        }
        public static int filecont = 0;
        /// <summary>
        /// 目录文件显示
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool ProcessDirectoryFilesDispaly(ProcessDirectoryFilesDispalyModel model)
        {
            filecont = 0;
            ExceptionLog.ExceptionWrite(filecont + ">" + model.rowscont * model.pageIndex);
            DirectoryInfo folder = new DirectoryInfo(model.dirPath);
            List<file> lf = new List<file>();
            DirectoryInfo[] dif = folder.GetDirectories();
            bool inunit = false;
             List<string> projectnamelist = null;
             if (model.dirPath.IndexOf(model.unitName) != -1 && Path.GetDirectoryName(model.dirPath).IndexOf(model.unitName) == -1)
            {
                projectnamelist = ProjectListLoad(model.userID, model.role);
                inunit = true;
            }
            
            foreach (DirectoryInfo dir in dif)
            {
                if (model.unitName == dir.Name || "公共资源" == dir.Name || (model.dirPath.IndexOf("全部文件\\") != -1 && model.xmnameList.Contains(dir.Name)) || dir.Name.IndexOf("数据报表") != -1 || dir.Name.IndexOf("测量库") != -1 || dir.Name.IndexOf("成果库") != -1 || dir.Name.IndexOf("GPS") != -1 || dir.Name.IndexOf("全站仪") != -1 || dir.Name.IndexOf("水位") != -1 || dir.Name.IndexOf("客户端数据") != -1)
                    if (model.role == Role.superviseModel && dir.Name.IndexOf("成果库") != -1)
                    {
                        
                        model.dirPath =model.dirPath+"\\" +dir.Name;
                        return ProcessDirectoryFilesDispaly(model);
                    }
                if (filecont > model.rowscont * model.pageIndex-1) { ExceptionLog.ExceptionWrite(filecont +">"+ model.rowscont * model.pageIndex); break; }
                else if (filecont < model.rowscont * (model.pageIndex-1)) { filecont++; continue; }
                if (!inunit)
                {
                    
                    lf.Add(new file { FileName = dir.Name, FileType = "文件夹", LoadAddress = model.dirPath, Dt = dir.LastWriteTime.ToString() } );
                    filecont++;
                }
                else if (projectnamelist.Contains(dir.Name))
                {
                    
                    lf.Add(new file { FileName = dir.Name, FileType = "文件夹", LoadAddress = model.dirPath, Dt = dir.LastWriteTime.ToString() });
                    filecont++;
                }
                

            }

            foreach (FileInfo fi in folder.GetFiles().OrderByDescending(m=>m.CreationTime))
            {
                if (filecont > model.rowscont * model.pageIndex-1) break;
                else if (filecont < model.rowscont * (model.pageIndex-1)) { filecont++; continue; }

                string ext = Path.GetExtension(fi.FullName);
                string fileType = "";
                switch (ext)
                {
                    case ".txt": fileType = "txt"; break;
                    case ".xls":
                    case ".xlsx": fileType = "excel"; break;
                    case ".jpg":
                    case ".png":
                    case ".jpeg":
                    case ".gif": fileType = "图片"; break;
                    case ".zip": fileType = "zip"; break;
                    case ".doc":
                    case ".docx": fileType = "word"; break;
                    case ".psc":
                    case ".sql": fileType = "数据库"; break;
                    default: fileType = "其他"; break;
                }

                lf.Add(new file { FileName = fi.Name, FileType = fileType, LoadAddress = fi.FullName, Size = fi.Length.ToString(), Dt = fi.LastWriteTime.ToString() });
                filecont++;
            }
            lf.Sort();
            lf.Reverse();
            model.ls = lf;
            return true;

        }
        public static ProcessAdministratrorBLL administratorBLL = new ProcessAdministratrorBLL();
        public static ProcessMonitorBLL monitorBLL = new ProcessMonitorBLL();
        public static ProcessSuperviseBLL superviseBLL = new ProcessSuperviseBLL();
        public  static List<string> ProjectListLoad(string userID, Role role)
        {

            //string userID = context.Session["userID"].ToString();
            //Role role = (Role)context.Session["role"];
            StringBuilder strSql = new StringBuilder();
            List<string> ls = new List<string>();
            var model = new ProcessPeopleXmnameListCondition(userID);
            switch (role)
            {

                case Role.administratrorModel:


                    if (!administratorBLL.ProcessAdministratorXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        ls = model.xmnameList;
                    }
                    break;
                case Role.monitorModel:
                    if (!monitorBLL.ProcessMonitorXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        ls = model.xmnameList;
                    }
                    break;
                case Role.superviseModel:
                    if (!superviseBLL.ProcessSuperviseXmnameList(model, out mssg))
                    {
                        //出错处理
                    }
                    else
                    {
                        ls = model.xmnameList;
                    }
                    break;
            }
            return ls;
        }
        public static string GetFileDate(string filename)
        {
            filename = Path.GetFileNameWithoutExtension(filename);
            string datestr = "";
            try
            {
                string datefirststr = filename.Substring(filename.IndexOf("_") + 1);
                string datesecstr = datefirststr.Substring(datefirststr.IndexOf("_") + 1);
                datestr = 2018 + "/" + datesecstr.Replace("月_", "/").Replace("日_", " ").Replace("时_", ":").Replace("分_", ":").Replace("秒", "");
                //ProcessPrintMssg.Print(Convert.ToDateTime(datestr).ToString());

                return Convert.ToDateTime(datestr).ToString();
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("从" + datestr + "获取文件日期出错，错误信息:" + ex.Message);
                return "";
            }
        }



        /// <summary>
        /// 目录文件显示实体
        /// </summary>
        public class ProcessDirectoryFilesDispalyModel
        {
            /// <summary>
            /// 单位名称
            /// </summary>
            public string unitName { get; set; }
            /// <summary>
            /// 文件目录
            /// </summary>
            public string dirPath { get; set; }
            /// <summary>
            /// 文件列表
            /// </summary>
            public List<file> ls { get; set; }

            public Role role { get; set; }

            public string userID { get; set; }

            public List<string> xmnameList { get; set; }

            public int rowscont { get; set; }

            public int pageIndex { get; set; }
            public ProcessDirectoryFilesDispalyModel(string userID, string unitName, string dirPath, int rowscont, int pageIndex, List<string> xmnameList, Role role)
            {
                this.unitName = unitName;
                this.dirPath = dirPath;
                this.xmnameList = xmnameList;
                this.role = role;
                this.userID = userID;
                this.rowscont = rowscont;
                this.pageIndex = pageIndex;
            }

           


        }

       




    }




}