﻿using System;
using System.Collections.Generic;
using System.IO;
using NFnet_MODAL;
using System.Web.UI.WebControls;

namespace NFnet_BLL
{
    public class FileManage : FileManage_i
    {
        public void FileMenuCreate(string filepath)
        {
            try
            {
                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
            }
        }

        public List<string> FileTravel(string directoryPath)
        {
            DirectoryInfo folder = new DirectoryInfo(directoryPath);
            Dictionary<string, string> dss = new Dictionary<string, string>();
            DirectoryTravel(folder,dss);
            //DirectoryInfo[] dif = folder.GetDirectories();
            //foreach (DirectoryInfo dir in dif)
            //{
                
            //}
            //FileInfo[] fis = folder.GetFiles();
            
            //FileInfo fi = new FileInfo(directoryPath); 
            //foreach()
            //{

            //}

            return null;
        }
        public void DirectoryTravel(DirectoryInfo folder,Dictionary<string,string> dss)
        {
            
            dss.Add("-------------"+folder.FullName+"--------------","-----------------"+folder.FullName+"----------------");
            DirectoryInfo[] dif = folder.GetDirectories();
            foreach (DirectoryInfo dir in dif)
            {
                DirectoryTravel(dir,dss);
            }
            foreach(FileInfo fi in folder.GetFiles())
            {
                dss.Add(fi.FullName,fi.Name);
            }
            
        }
        public void FileDownLoad(List<string> filePathList)
        {
            throw new NotImplementedException();
        }
        //需要权限验证
        public List<file> DirectoryFilesDispaly(string dirPath,string userId,string unitName)
        {
            DirectoryInfo folder = new DirectoryInfo(dirPath);
            List<file> lf = new List<file>();
            DirectoryInfo[] dif = folder.GetDirectories();
            //string unitName = Aspect.PageHeader.CurrentUserUnitNameGet(userId);
            foreach (DirectoryInfo dir in dif)
            {
                if (unitName == dir.Name || "公共资源" == dir.Name)
                lf.Add(new file{ FileName = dir.Name, FileType = "文件夹", LoadAddress = dirPath });
            }
            foreach (FileInfo fi in folder.GetFiles())
            {
                string ext = Path.GetExtension(fi.FullName);
                string fileType = "";
                switch (ext)
                {
                    case ".txt": fileType = "txt"; break;
                    case ".xls": 
                    case ".xlsx": fileType = "excel"; break;
                    case ".jpg":
                    case ".png": 
                    case ".jpeg": 
                    case ".gif": fileType = "图片"; break;
                    case ".zip": fileType = "zip"; break;
                    case ".doc": 
                    case ".docx": fileType = "word"; break;
                    case ".psc":
                    case ".sql": fileType = "数据库"; break;
                    default: fileType = "其他"; break;
                }

                lf.Add(new file { FileName = fi.Name, FileType = fileType, LoadAddress = fi.FullName, Size = fi.Length.ToString() });
            }
            
            return lf;
        }
        //公共资源
        public List<file> DirectoryFilesDispaly(string dirPath)
        {
            DirectoryInfo folder = new DirectoryInfo(dirPath);
            List<file> lf = new List<file>();
            DirectoryInfo[] dif = folder.GetDirectories();
            foreach (DirectoryInfo dir in dif)
            {

                lf.Add(new file { FileName = dir.Name, FileType = "文件夹", LoadAddress = dirPath });
            }
            foreach (FileInfo fi in folder.GetFiles())
            {
                string ext = Path.GetExtension(fi.FullName);
                string fileType = "";
                switch (ext)
                {
                    case ".txt": fileType = "txt"; break;
                    case ".xls":
                    case ".xlsx": fileType = "excel"; break;
                    case ".jpg":
                    case ".png":
                    case ".jpeg":
                    case ".gif": fileType = "图片"; break;
                    case ".zip": fileType = "zip"; break;
                    case ".doc":
                    case ".docx": fileType = "word"; break;
                    case ".psc":
                    case ".sql": fileType = "数据库"; break;
                    default: fileType = "其他"; break;
                }

                lf.Add(new file { FileName = fi.Name, FileType = fileType, LoadAddress = fi.FullName, Size = fi.Length.ToString() });
            }

            return lf;
        }
        public string FileRoteFilter(string userId)
        {
            member member = new member { UserId = userId };
            member = member.GetMember();
            //string sql = "";
            return member.UnitName;
        }
    }
}