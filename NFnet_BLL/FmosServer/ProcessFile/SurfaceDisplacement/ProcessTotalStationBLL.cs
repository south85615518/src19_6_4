﻿using System;
using System.Collections.Generic;
using Tool;
using System.IO;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.UserProcess;
using System.Linq;
using NFnet_BLL.DataProcess;
using NFnet_BLL.AuthorityAlarmProcess;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement;
using NFnet_BLL.ProjectInfo;
namespace NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement
{
    /// <summary>
    /// 全站仪数据导入处理类
    /// </summary>
    public class ProcessTotalStationBLL
    {
        public TotalStation.BLL.fmos_obj.cycdirnet cycdirnetBLL = new TotalStation.BLL.fmos_obj.cycdirnet();
        public TotalStation.BLL.fmos_obj.orgldata orgldataBLL = new TotalStation.BLL.fmos_obj.orgldata();
        public TotalStation.BLL.fmos_obj.fmos_instrumentparam fmos_instrumentparamBLL = new TotalStation.BLL.fmos_obj.fmos_instrumentparam();
        public TotalStation.BLL.fmos_obj.fmos_pointsinteam fmos_pointsinteamBLL = new TotalStation.BLL.fmos_obj.fmos_pointsinteam();
        public TotalStation.BLL.fmos_obj.setting settingBLL = new TotalStation.BLL.fmos_obj.setting();
        public TotalStation.BLL.fmos_obj.stationinfo stationinfoBLL = new TotalStation.BLL.fmos_obj.stationinfo();
        public TotalStation.BLL.fmos_obj.studypoint studypointBLL = new TotalStation.BLL.fmos_obj.studypoint();
        public TotalStation.BLL.fmos_obj.team teamBLL = new TotalStation.BLL.fmos_obj.team();
        public TotalStation.BLL.fmos_obj.timetask timetaskBLL = new TotalStation.BLL.fmos_obj.timetask();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public ProcessTotalStationBLL_GT processTotalStationBLL_GT = new ProcessTotalStationBLL_GT();
        public ProcessSiblingPointBLL processSiblingPointBLL = new ProcessSiblingPointBLL();
        public System.Windows.Forms.ListBox listbox { get; set; }
        public int rows { get; set; }
        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataBLL(TotalStationFileUploadConditionModel model, out string mssg)
        {


            ExceptionLog.XmRecordWrite(string.Format("现在导入项目{0}数据文件解析任务{1}", model.xmname, model.path), model.xmname);
            ProcessDeleteTmp(model.xmname, out mssg);
            ExceptionLog.XmRecordWrite(mssg, model.xmname);
            int i = 0, t = 0;
            bool hassiblingpointname = false;
            NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement.ProcessSiblingPointBLL.SiblingpointnameGetModel siblingpointnameGetModel = new NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement.ProcessSiblingPointBLL.SiblingpointnameGetModel("", model.xmname);
            if (processSiblingPointBLL.SiblingpointnameGet(siblingpointnameGetModel, out mssg))
            {
                hassiblingpointname = true;
            }
            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = "";
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                //记录下当前项目正在入库操作的周期
                int currentcyc = -1;
                string currenttaskname = "";
                List<TotalStation.Model.fmos_obj.cycdirnet> tmplistcycdirnet = new List<TotalStation.Model.fmos_obj.cycdirnet>();
                while (/*ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", model.xmname)) &&*/ !sr.EndOfStream)
                {
                    try
                    {
                        strTemp = sr.ReadLine();
                        //ExceptionLog.XmRecordWrite(strTemp+"----------");
                        TotalStation.Model.fmos_obj.cycdirnet cycdirnet = (TotalStation.Model.fmos_obj.cycdirnet)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.cycdirnet));
                        //ExceptionLog.XmRecordWrite("反序列化成功...");
                        cycdirnet.TaskName = model.xmno.ToString();
                        //DateTime dt = cycdirnet.Time;
                        cycdirnet.Time = cycdirnet.Time.AddHours(8);

                       

                        //ExceptionLog.XmRecordWrite(currentcyc +"=?"+ -1);
                        //ExceptionLog.XmRecordWrite(currentcyc +"=?"+ cycdirnet.CYC+"&&"+ currenttaskname +"=?"+ cycdirnet.TaskName);
                        //ExceptionLog.XmRecordWrite(currentcyc +"!="+ cycdirnet.CYC +"&&"+ currenttaskname +"=?"+ cycdirnet.TaskName);
                        if (currentcyc == -1)
                        {
                            currenttaskname = cycdirnet.TaskName;
                            currentcyc = cycdirnet.CYC;
                            if (hassiblingpointname&&processSiblingPointBLL.SiblingpointnameGet(siblingpointnameGetModel, out mssg))
                            {
                                //ExceptionLog.XmRecordWrite(mssg);
                                cycdirnet.siblingpointname = siblingpointnameGetModel.silblingpointname;
                            }
                            tmplistcycdirnet.Add(cycdirnet);
                        }
                        else if (currentcyc == cycdirnet.CYC && currenttaskname == cycdirnet.TaskName)
                        {
                            //ExceptionLog.XmRecordWrite(currentcyc + "==" + cycdirnet.CYC + "&&" + currenttaskname + "==" + cycdirnet.TaskName);
                            if (hassiblingpointname&&processSiblingPointBLL.SiblingpointnameGet(siblingpointnameGetModel, out mssg))
                            {
                                //ExceptionLog.XmRecordWrite(mssg);
                                cycdirnet.siblingpointname = siblingpointnameGetModel.silblingpointname;
                            }
                            //else ExceptionLog.XmRecordWrite("点名" + siblingpointnameGetModel.pointname+"不存在沉降点",cycdirnet.TaskName);
                            tmplistcycdirnet.Add(cycdirnet);
                        }
                        else if (currentcyc != cycdirnet.CYC && currenttaskname == cycdirnet.TaskName)
                        {

                            CycListImport(tmplistcycdirnet, model.xmname);
                            tmplistcycdirnet = new List<TotalStation.Model.fmos_obj.cycdirnet>();
                            tmplistcycdirnet.Add(cycdirnet);
                            currentcyc = cycdirnet.CYC;
                            currenttaskname = cycdirnet.TaskName;
                        }
                        //ExceptionLog.XmRecordWrite("没找到符合条件的判断不做处理...");

                        i++;
                    }
                    catch (Exception ex)
                    {
                        mssg += string.Format("{1}文件第{0}行存在错误", ++i, "#");
                        ExceptionLog.XmRecordWrite(mssg, model.xmname);
                    }

                }
                ExceptionLog.XmRecordWrite("现在开始导入项目:" + model.xmname + tmplistcycdirnet.Count + "", model.xmname);
                if (CycListImport(tmplistcycdirnet, model.xmname))
                {
                    ExceptionLog.XmRecordWrite(string.Format("项目{0}数据文件解析任务{1}导入完成现在清除任务", model.xmname, model.path), model.xmname);
                    ProcessDataFileDescodeTaskRemove.DataFileDescodeTaskRemove(model.xmname, model.path);
                }
                else
                    ExceptionLog.XmRecordWrite(string.Format("项目{0}数据文件解析任务{1}导入出现异常!", model.xmname, model.path), model.xmname);
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.XmRecordWrite("文件读取出错!错误信息" + ex.Message, model.xmname);
                return false;
            }
            return true;


        }

        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataDelete(TotalStationFileUploadConditionModel model, out string mssg)
        {

            int i = 0, t = 0;
            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = "";
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                //记录下当前项目正在入库操作的周期
                int currentcyc = -1;
                string currenttaskname = "";
                List<TotalStation.Model.fmos_obj.cycdirnet> tmplistcycdirnet = new List<TotalStation.Model.fmos_obj.cycdirnet>();
                while (!sr.EndOfStream)
                {
                    try
                    {
                        strTemp = sr.ReadLine();
                        TotalStation.Model.fmos_obj.cycdirnet cycdirnet = (TotalStation.Model.fmos_obj.cycdirnet)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.cycdirnet));
                        cycdirnet.TaskName = model.xmname;
                        //DateTime dt = cycdirnet.Time;
                        cycdirnet.Time = cycdirnet.Time.AddHours(8);

                        NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement.ProcessSiblingPointBLL.SiblingpointnameGetModel siblingpointnameGetModel = new NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement.ProcessSiblingPointBLL.SiblingpointnameGetModel(cycdirnet.POINT_NAME, cycdirnet.TaskName);



                        tmplistcycdirnet.Add(cycdirnet);


                        strTemp = sr.ReadLine();
                        i++;
                    }
                    catch (Exception ex)
                    {
                        mssg += string.Format("{1}文件第{0}行存在错误", ++i, "#");

                    }

                }
                CycListDelete(tmplistcycdirnet, model.xmname);
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }

        /// <summary>
        /// 导入临时数据列表里的数据对象
        /// </summary>
        //public void CycListImport(List<TotalStation.Model.fmos_obj.cycdirnet> tmplistcycdirnet,string xmname)
        //{
        //    ExceptionLog.XmRecordWrite(string.Format("现在导入项目{0}第{1}周期数据{2}条", xmname, tmplistcycdirnet[0].CYC, tmplistcycdirnet.Count));
        //    string mssg = "";
        //    List<DateTime> listdt = (from entry in tmplistcycdirnet select entry.Time).ToList();
        //    listdt.Sort();//时间排序
        //    int t = 0;
        //    //如果记录存在则取消插入操作
        //    var IsCycdirnetDataExistModel = new ProcessResultDataBLL.IsCycdirnetDataExistModel(xmname, tmplistcycdirnet[0].POINT_NAME, tmplistcycdirnet[0].Time);
        //    if (processResultDataBLL.IsCycdirnetDataExist(IsCycdirnetDataExistModel, out mssg)) { ExceptionLog.XmRecordWrite(mssg); return; }


        //    //接段周期操作
        //    var IsInsertDataModel = new ProcessResultDataBLL.IsInsertDataModel(xmname, listdt[listdt.Count - 1]);
        //    if (processResultDataBLL.IsInsertData(IsInsertDataModel, out mssg))
        //    {
        //        //获取接段周期
        //        var InsertDataCycGetModel = new ProcessResultDataBLL.InsertDataCycGetModel(xmname, listdt[listdt.Count - 1]);
        //        if (!processResultDataBLL.InsertDataCycGet(InsertDataCycGetModel, out mssg)) { ExceptionLog.XmRecordWrite(mssg); return; }
        //        //如果接段周期有空位则无需做周期移位操作
        //        var IsInsertCycExistModel = new ProcessResultDataBLL.IsInsertCycExistModel(xmname, InsertDataCycGetModel.cyc - 1);
        //        if (InsertDataCycGetModel.cyc == 1|| processResultDataBLL.IsInsertCycExist(IsInsertCycExistModel, out mssg))
        //        {
        //            //执行周期后移操作
        //            var InsertCycStepModel = new ProcessResultDataBLL.InsertCycStepModel(xmname, InsertDataCycGetModel.cyc);
        //            if (!processResultDataBLL.InsertCycStep(InsertCycStepModel, out mssg))
        //            { ExceptionLog.XmRecordWrite(mssg); return; }
        //        }
        //        else
        //        {
        //            InsertDataCycGetModel.cyc -= 1;
        //        }
        //        //执行周期数据操作
        //        foreach (var datamodel in tmplistcycdirnet)
        //        {
        //            datamodel.CYC = InsertDataCycGetModel.cyc;
        //            for ( t = 0; t < 2; t++)
        //            {

        //                if (cycdirnetBLL.Add(datamodel, out mssg)) break;
        //                ExceptionLog.DTUPortInspectionWrite(string.Format("导入项目{0}点名{1}测量时间{2}的表面位移—全站仪测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.TaskName, datamodel.POINT_NAME, datamodel.Time, t));
        //                Thread.Sleep(1000);

        //            }
        //        }
        //        //清空临时数据列表
        //    }
        //    else
        //    {
        //        //非接段数据则获取当前库中该项目的最大周期
        //        var ResultDataExtremelyCondition = new ResultDataExtremelyCondition(xmname, "max");
        //        if (!processResultDataBLL.ProcessResultDataExtremely(ResultDataExtremelyCondition, out mssg))
        //        { ExceptionLog.XmRecordWrite(mssg); return; }

        //        int maxcyc = Convert.ToInt32(ResultDataExtremelyCondition.extremelyCyc) + 1;
        //        //执行周期数据操作
        //        foreach (var datamodel in tmplistcycdirnet)
        //        {
        //            datamodel.CYC = maxcyc;
        //            for ( t = 0; t < 2; t++)
        //            {

        //                if (cycdirnetBLL.Add(datamodel, out mssg)) break;
        //                ExceptionLog.DTUPortInspectionWrite(string.Format("导入项目{0}点名{1}测量时间{2}的表面位移—全站仪测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.TaskName, datamodel.POINT_NAME, datamodel.Time, t));
        //                Thread.Sleep(1000);

        //            }
        //        }


        //    }
        //}

        /// <summary>
        /// 导入临时数据列表里的数据对象
        /// </summary>
        public bool CycListImport(List<TotalStation.Model.fmos_obj.cycdirnet> tmplistcycdirnet, string xmname)
        {
            ExceptionLog.XmRecordWrite("现在开始导入项目" + xmname + "的数据" + tmplistcycdirnet.Count + "条", xmname);
            Console.WriteLine("现在开始导入项目" + xmname + "的数据" + tmplistcycdirnet.Count + "条");
            string mssg = "";
            tmplistcycdirnet.OrderBy(m => m.POINT_NAME).OrderBy(m=>m.Time);
            List<string> lspointname = tmplistcycdirnet.Select(m => m.POINT_NAME).ToList();
            lspointname = lspointname.Distinct().ToList();
            if (tmplistcycdirnet.Count > lspointname.Count)
            {
                ExceptionLog.XmRecordWrite("项目" + xmname + "将要导入的数据文件存在单周期多点,现在分点",xmname);
                List<List<TotalStation.Model.fmos_obj.cycdirnet>> llt = CycdirnetDataDPSCSplit(tmplistcycdirnet);
                foreach(var model in llt)
                {
                    CycListImport(model,xmname);
                }
            }

            List<DateTime> listdt = (from entry in tmplistcycdirnet select entry.Time).ToList();
            tmplistcycdirnet = tmplistcycdirnet.OrderByDescending(m => m.Time).ToList();
            listdt.Sort();//时间排序
            int t = 0;
            int updatecyc = -1;
            try
            {
                //如果记录存在则取消插入操作
                var IsCycdirnetDataExistModel = new ProcessResultDataBLL.IsCycdirnetDataExistModel(xmname, tmplistcycdirnet[0].POINT_NAME, tmplistcycdirnet[0].Time);
                var xmmodel = GetXmconnectFromXmno(xmname);
                //ExceptionLog.XmRecordWrite("获取到项目编号" + xmname + "的项目对象是" + xmmodel.dbname);
                //接入周期操作
                var IsSiblingDataModel = new ProcessResultDataBLL.IsTypeDataModel(xmname, listdt[listdt.Count - 1]);
                //接段周期操作
                var IsInsertDataModel = new ProcessResultDataBLL.IsTypeDataModel(xmname, listdt[listdt.Count - 1]);
                //本周期是否已经存在本次要插入点号的数据
                var points = (from m in tmplistcycdirnet select m.POINT_NAME).ToList();
                //NFnet_BLL.DataProcess.ProcessResultDataBLL.IsPointsDataExistInThisCycModel IsPointsDataExistInThisCycModel = null;
                DateTime minTime = Tool.DateHelper.GetMinDateTimeFromTimeList(listdt, 1);
                ExceptionLog.XmRecordWrite("获取到测量起始时间为:"+minTime,xmname);
                var SiblingDataCycGetModelU = new ProcessResultDataBLL.DataCycGetModel(xmname, listdt[listdt.Count - 1], points, xmmodel.mobileStationInteval);

                var SiblingDataCycGetModelL = new ProcessResultDataBLL.DataCycGetModel(xmname, minTime, points, xmmodel.mobileStationInteval);
                int i = 0;
                for (i = 0; i < 2; i++)
                {

                    if (processResultDataBLL.IsCycdirnetDataExist(IsCycdirnetDataExistModel, out mssg))
                    {
                        Console.WriteLine(mssg);
                        this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg); }));                        ExceptionLog.XmRecordWrite(mssg, xmname);
                        foreach (var datamodel in tmplistcycdirnet)
                        {

                            //for (t = 0; t < 2; t++)
                            //{

                            cycdirnetBLL.Update(datamodel, out mssg);
                            //ExceptionLog.XmRecordWrite(mssg,xmname);
                            //ExceptionLog.XmRecordWrite(string.Format("更新项目{0}点名{1}测量时间{2}的表面位移—全站仪测量数据失败,现在尝试进行周期数据添加操作", datamodel.TaskName, datamodel.POINT_NAME, datamodel.Time, t),xmname);
                            //datamodel.CYC = IsCycdirnetDataExistModel.cyc;
                            //cycdirnetBLL.Add(datamodel, out mssg);
                            Console.WriteLine(mssg);
                            this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg); }));
                            ExceptionLog.XmRecordWrite(mssg, xmname);
                            //Thread.Sleep(10);

                            //}
                        }
                        return true;
                    }
                    if (mssg.IndexOf("出错") != -1) { if (i < 1)continue; return false; }
                    break;
                }

                if (xmmodel.mobileStationInteval > 0 && processResultDataBLL.SiblingDataCycGet(SiblingDataCycGetModelU, out mssg))
                {
                    Console.WriteLine(mssg);
                    this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg); }));
                    SiblingDataCycGetModelU.cyc = SiblingCycbespokeProcess(SiblingDataCycGetModelU.xmname, SiblingDataCycGetModelU.cyc, SiblingDataCycGetModelU);

                    ExceptionLog.XmRecordWrite(mssg, xmname);
                    //执行周期数据操作
                    foreach (var datamodel in tmplistcycdirnet)
                    {
                        datamodel.CYC = SiblingDataCycGetModelU.cyc;
                        //for (t = 0; t < 2; t++)
                        //{

                        cycdirnetBLL.Add(datamodel, out mssg);
                        ExceptionLog.XmRecordWrite(mssg, xmname);
                        //ExceptionLog.DTUPortInspectionWrite(string.Format("导入项目{0}点名{1}测量时间{2}的表面位移—全站仪测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.TaskName, datamodel.POINT_NAME, datamodel.Time, t));
                        //Thread.Sleep(10);

                        //}
                    }
                    return true;
                }
                else if (xmmodel.mobileStationInteval > 0 && processResultDataBLL.SiblingDataCycGet(SiblingDataCycGetModelL, out mssg))
                {
                    Console.WriteLine(mssg);
                    this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg); }));
                    SiblingDataCycGetModelL.cyc = SiblingCycbespokeProcess(SiblingDataCycGetModelL.xmname, SiblingDataCycGetModelL.cyc, SiblingDataCycGetModelL);

                    ExceptionLog.XmRecordWrite(mssg, xmname);
                    //执行周期数据操作
                    foreach (var datamodel in tmplistcycdirnet)
                    {
                        datamodel.CYC = SiblingDataCycGetModelL.cyc;
                        //for (t = 0; t < 2; t++)
                        //{

                        cycdirnetBLL.Add(datamodel, out mssg);
                        ExceptionLog.XmRecordWrite(mssg, xmname);
                        //ExceptionLog.DTUPortInspectionWrite(string.Format("导入项目{0}点名{1}测量时间{2}的表面位移—全站仪测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.TaskName, datamodel.POINT_NAME, datamodel.Time, t));
                        //Thread.Sleep(10);

                        //}
                    }
                    return true;
                }
                else if (processResultDataBLL.IsInsertData(IsInsertDataModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    this.listbox.Invoke(new Action(() => { if (this.listbox.Items.Count > rows)this.listbox.Items.Clear(); this.listbox.Items.Insert(0, mssg); }));
                    ExceptionLog.XmRecordWrite(mssg, xmname);
                    //获取接段周期
                    var InsertDataCycGetModel = new ProcessResultDataBLL.InsertDataCycGetModel(xmname, listdt[listdt.Count - 1]);
                    if (!processResultDataBLL.InsertDataCycGet(InsertDataCycGetModel, out mssg)) { ExceptionLog.XmRecordWrite(mssg, xmname); return false; }
                    //如果接段周期有空位则无需做周期移位操作
                    var IsInsertCycExistModel = new ProcessResultDataBLL.IsInsertCycExistModel(xmname, InsertDataCycGetModel.cyc - 1);
                    if (InsertDataCycGetModel.cyc == 1 || processResultDataBLL.IsInsertCycExist(IsInsertCycExistModel, out mssg))
                    {
                        //执行周期后移操作
                        var InsertCycStepModel = new ProcessResultDataBLL.InsertCycStepModel(xmname, InsertDataCycGetModel.cyc);
                        if (!processResultDataBLL.InsertCycStep(InsertCycStepModel, out mssg))
                        { ExceptionLog.XmRecordWrite(mssg, xmname); return false; }
                    }
                    else
                    {
                        InsertDataCycGetModel.cyc -= 1;
                    }
                    //执行周期数据操作
                    foreach (var datamodel in tmplistcycdirnet)
                    {
                        datamodel.CYC = InsertDataCycGetModel.cyc;
                        //for (t = 0; t < 2; t++)
                        //{

                        cycdirnetBLL.Add(datamodel, out mssg);
                        ExceptionLog.XmRecordWrite(mssg, xmname);
                        //ExceptionLog.XmRecordWrite(string.Format("导入项目{0}点名{1}测量时间{2}的表面位移—全站仪测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.TaskName, datamodel.POINT_NAME, datamodel.Time, t), xmname);
                        //Thread.Sleep(10);

                        //}
                    }
                    return true;
                    //清空临时数据列表
                }
                else
                {
                    ExceptionLog.XmRecordWrite("直接插入数据", xmname);
                    //非接段数据则获取当前库中该项目的最大周期
                    var ResultDataExtremelyCondition = new ResultDataExtremelyCondition(xmname, "max");
                    if (!processResultDataBLL.ProcessResultDataExtremely(ResultDataExtremelyCondition, out mssg))
                    { ExceptionLog.XmRecordWrite(mssg, xmname); return false; }

                    int maxcyc = Convert.ToInt32(ResultDataExtremelyCondition.extremelyCyc) + 1;
                    //执行周期数据操作
                    foreach (var datamodel in tmplistcycdirnet)
                    {
                        datamodel.CYC = maxcyc;
                        //for (t = 0; t < 2; t++)
                        //{
                        cycdirnetBLL.Add(datamodel, out mssg);
                        ExceptionLog.XmRecordWrite(mssg, xmname);
                        //ExceptionLog.XmRecordWrite(string.Format("导入项目{0}点名{1}测量时间{2}的表面位移—全站仪测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.TaskName, datamodel.POINT_NAME, datamodel.Time, t),xmname);
                        //Thread.Sleep(10);


                    }
                    return true;

                }
            }
            catch (Exception ex)
            {
                ExceptionLog.XmRecordWrite("数据导入出错,错误信息:" + ex.Message + "位置:" + ex.StackTrace, xmname);
                string a = "";
                return false;
            }
        }

        public  List<List<TotalStation.Model.fmos_obj.cycdirnet>> CycdirnetDataDPSCSplit(List<TotalStation.Model.fmos_obj.cycdirnet> tmplistcycdirnet)
        {
            
            List<List<TotalStation.Model.fmos_obj.cycdirnet>> twodimensionalcycdirnet = new List<List<TotalStation.Model.fmos_obj.cycdirnet>>();
            
            while (tmplistcycdirnet.Count > 0)
            {
                int i = 0;
                int len = tmplistcycdirnet.Count;
                List<TotalStation.Model.fmos_obj.cycdirnet> cycdirnetlist = new List<TotalStation.Model.fmos_obj.cycdirnet>();
                List<string> pointnamelist = new List<string>();
                while (i < len)
                {

                    if (!pointnamelist.Contains(tmplistcycdirnet[0].POINT_NAME))
                    {
                        pointnamelist.Add(tmplistcycdirnet[0].POINT_NAME);
                        cycdirnetlist.Add(tmplistcycdirnet[0]);
                        tmplistcycdirnet.Remove(tmplistcycdirnet[0]);
                    }
                    i++;
                }
                twodimensionalcycdirnet.Add(cycdirnetlist);
            }

            return twodimensionalcycdirnet;
        }



        public static Authority.Model.xmconnect GetXmconnectFromXmno(string xmname)
        {
            ProcessXmBLL processXmBLL = new ProcessXmBLL();
            string mssg = "";
            var processXmInfoLoadModel = new ProcessXmBLL.ProcessXmIntInfoLoadModel(Convert.ToInt32(xmname));
            List<Authority.Model.xmconnect> lmx = new List<Authority.Model.xmconnect>();
            if (processXmBLL.ProcessXmInfoLoad(processXmInfoLoadModel, out mssg))
            {
                return processXmInfoLoadModel.model;
            }
            return new Authority.Model.xmconnect();
        }

        /// <summary>
        /// 导入临时数据列表里的数据对象
        /// </summary>
        public void CycListDelete(List<TotalStation.Model.fmos_obj.cycdirnet> tmplistcycdirnet, string xmname)
        {
            string mssg = "";
            foreach (var model in tmplistcycdirnet)
            {
                ProcessDelete(model, out mssg);
            }
        }






        /// <summary>
        /// 原始数据导入
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessOrglDataBLL(TotalStationFileUploadConditionModel model, out string mssg)
        {

            ExceptionLog.XmRecordWrite(string.Format("现在导入项目{0}数据文件解析任务{1}", model.xmname, model.path), model.xmname);
            mssg = "";
            int i = 0;
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                while (!sr.EndOfStream)
                {

                    try
                    {
                        TotalStation.Model.fmos_obj.orgldata orgldata = (TotalStation.Model.fmos_obj.orgldata)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.orgldata));
                        orgldata.TaskName = model.xmname;
                        orgldata.SEARCH_TIME = orgldata.SEARCH_TIME.AddHours(8);
                        if (!orgldataBLL.Add(orgldata, out mssg))
                        {
                            //添加出错
                        }
                        strTemp = sr.ReadLine();
                        i++;
                    }
                    catch (Exception ex)
                    {
                        mssg += string.Format("{1}文件第{0}行存在错误", i, "#");
                        strTemp = sr.ReadLine();
                        i++;
                    }



                }

                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }
        /// <summary>
        /// 仪器数据导入
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessInstrumentparamBLL(TotalStationFileUploadConditionXmnoModel model, out string mssg)
        {


            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    TotalStation.Model.fmos_obj.fmos_instrumentparam fmos_instrumentparam = (TotalStation.Model.fmos_obj.fmos_instrumentparam)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.fmos_instrumentparam));
                    fmos_instrumentparam.Xmno = model.xmno;
                    if (!fmos_instrumentparamBLL.Add(fmos_instrumentparam, out mssg))
                    {
                        //添加出错
                    }
                    strTemp = sr.ReadLine();
                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }
        /// <summary>
        /// 目标表中的项目标记是项目编号的表的文件上传条件类
        /// </summary>
        public class TotalStationFileUploadConditionXmnoModel : TotalStationFileUploadConditionModel
        {
            public int xmno { get; set; }
            public TotalStationFileUploadConditionXmnoModel(string xmname, string path, int xmno)
            {

                this.path = path;
                this.xmname = xmname;
                this.xmno = xmno;
            }
        }
        /// <summary>
        /// 点组数据导入
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFmos_pointsinteamBLL(TotalStationFileUploadConditionXmnoModel model, out string mssg)
        {


            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    TotalStation.Model.fmos_obj.fmos_pointsinteam fmos_pointsinteam = (TotalStation.Model.fmos_obj.fmos_pointsinteam)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.fmos_pointsinteam));
                    fmos_pointsinteam.Xmno = model.xmno;
                    if (!fmos_pointsinteamBLL.Add(fmos_pointsinteam, out mssg))
                    {
                        //添加出错
                    }
                    strTemp = sr.ReadLine();
                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }
        /// <summary>
        /// 设置参数导入
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSettingBLL(TotalStationFileUploadConditionModel model, out string mssg)
        {


            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    TotalStation.Model.fmos_obj.setting setting = (TotalStation.Model.fmos_obj.setting)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.setting));
                    setting.TaskName = model.xmname;
                    setting.Name = "自定义参数";
                    if (!settingBLL.Add(setting, out mssg))
                    {
                        //添加出错
                    }
                    strTemp = sr.ReadLine();
                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }
        /// <summary>
        /// 测站信息导入
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessStationInfoBLL(TotalStationFileUploadConditionModel model, out string mssg)
        {


            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    TotalStation.Model.fmos_obj.stationinfo stationinfo = (TotalStation.Model.fmos_obj.stationinfo)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.stationinfo));
                    stationinfo.Taskname = model.xmname;
                    if (!stationinfoBLL.Add(stationinfo, out mssg))
                    {
                        //添加出错
                    }
                    strTemp = sr.ReadLine();
                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }
        /// <summary>
        /// 学习点信息导入
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessStudyPointInfoBLL(TotalStationFileUploadConditionModel model, out string mssg)
        {


            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    TotalStation.Model.fmos_obj.studypoint studypoint = (TotalStation.Model.fmos_obj.studypoint)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.studypoint));
                    studypoint.taskName = model.xmname;
                    if (!studypointBLL.Add(studypoint, out mssg))
                    {
                        //添加出错
                    }
                    strTemp = sr.ReadLine();
                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }
        /// <summary>
        /// 时间任务信息导入
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessTimeTaskBLL(TotalStationFileUploadConditionModel model, out string mssg)
        {


            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    TotalStation.Model.fmos_obj.timetask timetask = (TotalStation.Model.fmos_obj.timetask)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.timetask));
                    timetask.taskName = model.xmname;
                    if (!timetaskBLL.Add(timetask, out mssg))
                    {
                        //添加出错
                    }
                    strTemp = sr.ReadLine();
                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }
        public ProcessExcelDataImport processExcelDataImport = new ProcessExcelDataImport();
        /// <summary>
        /// 全站仪数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileDecodeTxt(ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            switch (model.fileType)
            {
                case "结果数据":
                    bool ispass = true;//Tool.ObjectHelper.ProcessTextFileCheck<TotalStation.Model.fmos_obj.cycdirnet>(model.path, out mssg);
                    var cycdirnetDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);
                    //if (true)
                    //{
                    //CycdirnetDataImport(cycdirnetDataImportModel);
                    //try
                    //{
                    //    ThreadPool.QueueUserWorkItem(CycdirnetDataImport, cycdirnetDataImportModel);
                    //}
                    //catch (Exception ex)
                    //{
                    //    ExceptionLog.ExceptionWrite(ex.Message);
                    //}
                    //t = new Thread(new ParameterizedThreadStart(CycdirnetDataImport));
                    //t.Name = string.Format("{0}resultdataimport", model.xmname);
                    //t.Start(cycdirnetDataImportModel);
                    //model.thread = t;
                    //}
                    CycdirnetDataImport(cycdirnetDataImportModel);
                    //List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
                    //Tool.TcpServer.SendMessToServer(string.Format("data descode request?{0}", jss.Serialize(model)), ls[0], ls[1]);
                    return ispass;

                case "结果数据删除":
                    cycdirnetDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);

                    //CycdirnetDataDelete(cycdirnetDataImportModel);
                    CycdirnetDataDelete(cycdirnetDataImportModel);
                    return true;
                case "原始数据":
                    ispass = true;//Tool.ObjectHelper.ProcessTextFileCheck<TotalStation.Model.fmos_obj.orgldata>(model.path, out mssg);
                    cycdirnetDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);
                    //if (/*!ThreadProcess.ThreadExist(string.Format("{0}orgldata", model.xmname))*/true)
                    //{
                    //Thread t = new Thread(new ParameterizedThreadStart(OrglDataImport));
                    //t.Name = string.Format("{0}orgldataimport", model.xmname);
                    //t.Start(cycdirnetDataImportModel);
                    //model.thread = t;
                    //OrglDataImport(cycdirnetDataImportModel);
                    //}
                    OrglDataImport(cycdirnetDataImportModel);
                    //ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
                    //Tool.TcpServer.SendMessToServer(string.Format("data descode request?{0}", jss.Serialize(model)), ls[0], ls[1]);
                    return ispass;

                case "学习点":
                    var totalStationFileUploadConditionModel = new TotalStationFileUploadConditionModel(model.xmname, model.path);
                    if (!ProcessStudyPointInfoBLL(totalStationFileUploadConditionModel, out mssg))
                    {
                        return false;
                    }
                    return true;

                case "测站":
                    totalStationFileUploadConditionModel = new TotalStationFileUploadConditionModel(model.xmname, model.path);
                    if (!ProcessStationInfoBLL(totalStationFileUploadConditionModel, out mssg))
                    {
                        return false;
                    }
                    return true;

                case "设置":
                    var totalStationFileUploadConditionXmnoModel = new TotalStationFileUploadConditionXmnoModel(model.xmname, model.path, model.xmno);
                    if (!ProcessSettingBLL(totalStationFileUploadConditionXmnoModel, out mssg))
                    {
                        return false;
                    }
                    return true;

                case "时间":
                    totalStationFileUploadConditionXmnoModel = new TotalStationFileUploadConditionXmnoModel(model.xmname, model.path, model.xmno);
                    if (!ProcessTimeTaskBLL(totalStationFileUploadConditionXmnoModel, out mssg))
                    {
                        return false;
                    }
                    return true;
                case "仪器":
                    totalStationFileUploadConditionXmnoModel = new TotalStationFileUploadConditionXmnoModel(model.xmname, model.path, model.xmno);
                    if (!ProcessInstrumentparamBLL(totalStationFileUploadConditionXmnoModel, out mssg))
                    {
                        return false;
                    }
                    return true;
                case "点组":
                    totalStationFileUploadConditionXmnoModel = new TotalStationFileUploadConditionXmnoModel(model.xmname, model.path, model.xmno);
                    if (!ProcessFmos_pointsinteamBLL(totalStationFileUploadConditionXmnoModel, out mssg))
                    {
                        return false;
                    }
                    return true;
                case "成果数据":

                    processTotalStationBLL_GT.ProcessFileDecodeTxt(model, out mssg);
                    return true;

                default: return false;

            }

        }


        /// 全站仪数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public void ProcessFileDecodeTxtDescode(ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            switch (model.fileType)
            {
                case "结果数据":
                    //bool ispass = Tool.ObjectHelper.ProcessTextFileCheck<TotalStation.Model.fmos_obj.cycdirnet>(model.path, out mssg);
                    var cycdirnetDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);
                    //if (true)
                    //{
                    //CycdirnetDataImport(cycdirnetDataImportModel);
                    //try
                    //{
                    //    ThreadPool.QueueUserWorkItem(CycdirnetDataImport, cycdirnetDataImportModel);
                    //}
                    //catch (Exception ex)
                    //{
                    //    ExceptionLog.ExceptionWrite(ex.Message);
                    //}
                    //t = new Thread(new ParameterizedThreadStart(CycdirnetDataImport));
                    //t.Name = string.Format("{0}resultdataimport", model.xmname);
                    //t.Start(cycdirnetDataImportModel);
                    //model.thread = t;
                    //}
                    CycdirnetDataImport(cycdirnetDataImportModel);
                    //List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
                    return;


                case "原始数据":
                    //ispass = Tool.ObjectHelper.ProcessTextFileCheck<TotalStation.Model.fmos_obj.orgldata>(model.path, out mssg);
                    cycdirnetDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);
                    //if (/*!ThreadProcess.ThreadExist(string.Format("{0}orgldata", model.xmname))*/true)
                    //{
                    //Thread t = new Thread(new ParameterizedThreadStart(OrglDataImport));
                    //t.Name = string.Format("{0}orgldataimport", model.xmname);
                    //t.Start(cycdirnetDataImportModel);
                    //model.thread = t;
                    OrglDataImport(cycdirnetDataImportModel);
                    //}
                    //OrglDataImport(cycdirnetDataImportModel);
                    return;



            }

        }



        public void OrglDataImport(object obj)
        {


            DataImportModel model = obj as DataImportModel;
            //Tool.ThreadProcess.Threads.Add(string.Format("{0}orgldata", model.xmname));
            string mssg = "";
            var totalStationFileUploadConditionModel = new TotalStationFileUploadConditionModel(model.xmname, model.path);
            if (!ProcessOrglDataBLL(totalStationFileUploadConditionModel, out mssg))
            {
                return;
            }
            ExceptionLog.XmRecordWrite("现在清除原始数据文件" + model.path, ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname));
            ProcessDataFileDescodeTaskRemove.DataFileDescodeTaskRemove(model.xmname, model.path);
            // ProcessFileRemove.FileInfoListRemove(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname) + "\\.txt", model.path + "|原始数据");
            //Tool.ThreadProcess.Threads.Remove(string.Format("{0}orgldata", model.xmname));
        }

        //public ProcessResultDataAlarm resultDataAlarm = new ProcessResultDataAlarm();


        public void CycdirnetDataImport(object obj)
        {
            ExceptionLog.ExceptionWrite("----------------------------");
            ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
            ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL();
            DataImportModel model = obj as DataImportModel;
            //Tool.ThreadProcess.Threads.Add(string.Format("{0}cycdirnet", model.xmname));
            string mssg = "";
            var totalStationFileUploadConditionModel = new TotalStationFileUploadConditionModel(model.xmname, model.path);
            if (!ProcessResultDataBLL(totalStationFileUploadConditionModel, out mssg))
            {
                return;
            }
            else
            {
                return;
                //CYCSort(totalStationFileUploadConditionModel.xmname);
                // ProcessFileRemove.FileInfoListRemove(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + ProcessAspectIndirectValue.GetXmnoFromXmname(model.xmname) + "\\.txt", model.path + "|结果数据");
                ExceptionLog.XmRecordWrite("现在进行预警检查...", totalStationFileUploadConditionModel.xmname);

                List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析任务表\\" + totalStationFileUploadConditionModel.xmname + ".txt");
                ExceptionLog.XmRecordWrite(string.Format("现在解析队列中一共有{0}个任务", ls.Count), totalStationFileUploadConditionModel.xmname);
                int cont = ls.Select(m => m.IndexOf("结果") != -1) == null ? 0 : ls.Select(m => m.IndexOf("结果") != -1).ToList().Count;
                if ( cont < 1)
                {
                    ExceptionLog.XmRecordWrite(string.Format("现在解析任务列表中结果数据文件个数为{0},开启数据预警检查...", cont), totalStationFileUploadConditionModel.xmname);
                    List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);

                    emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
                }
                else
                    ExceptionLog.XmRecordWrite(string.Format("现在解析任务列表中结果数据文件个数为{0}取消本次预警", cont), totalStationFileUploadConditionModel.xmname);
            }
            //Tool.ThreadProcess.Threads.Remove(string.Format("{0}cycdirnet", model.xmname));


        }

        public void CycdirnetDataDelete(object obj)
        {
            ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
            ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL();
            DataImportModel model = obj as DataImportModel;
            string mssg = "";
            var totalStationFileUploadConditionModel = new TotalStationFileUploadConditionModel(model.xmname, model.path);
            ProcessResultDataDelete(totalStationFileUploadConditionModel, out mssg);


        }

        public class DataImportModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string path { get; set; }
            public DataImportModel(string xmname, int xmno, string path)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
            }
        }

        public bool ProcessDelete(TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            return cycdirnetBLL.Delete(model, out mssg);
        }


        public bool ProcessDeleteTmp(string xmname, out string mssg)
        {
            return cycdirnetBLL.DeleteTmp(xmname, out mssg);
        }

        public bool ProcessDeleteCgTmp(string xmname, out string mssg)
        {
            return cycdirnetBLL.DeleteCgTmp(xmname, out mssg);
        }

        /// <summary>
        /// 全站仪数据导入处理主程序处理参数实体类
        /// </summary>
        public class ProcessFileDecodeTxtModel : TotalStationFileUploadConditionModel
        {
            /// <summary>
            /// 文件类型
            /// </summary>
            public string fileType { get; set; }
            public Thread thread { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessFileDecodeTxtModel(string xmname, int xmno, string path, string fileType)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
                this.fileType = fileType;
            }
            public ProcessFileDecodeTxtModel()
            {

            }

        }

        public bool ProcessCycdirnetCYCDataDelete(ProcessCycdirnetCYCDataDeleteModel model, out string mssg)
        {
            return cycdirnetBLL.Delete(model.startcyc, model.endcyc, model.xmname, model.pointname, out mssg);
        }
        public bool ProcessSurveyCYCDataDelete(ProcessCycdirnetCYCDataDeleteModel model, out string mssg)
        {
            return cycdirnetBLL.DeleteSurveyData(model.startcyc, model.endcyc, model.xmname, model.pointname, out mssg);
        }
        public bool ProcessCgCycdirnetCYCDataDelete(ProcessCycdirnetCYCDataDeleteModel model, out string mssg)
        {
            return cycdirnetBLL.DeleteCg(model.startcyc, model.endcyc, model.xmname, model.pointname, out mssg);
        }
        public class ProcessCycdirnetCYCDataDeleteModel
        {
            public string xmname { get; set; }
            public int startcyc { get; set; }
            public int endcyc { get; set; }
            public string pointname { get; set; }
            public ProcessCycdirnetCYCDataDeleteModel(int startcyc, int endcyc, string xmname, string pointname)
            {
                this.xmname = xmname;
                this.startcyc = startcyc;
                this.endcyc = endcyc;
                this.pointname = pointname;
            }
        }

        public int SiblingCycbespokeProcess(string xmno, int siblingCyc, ProcessResultDataBLL.DataCycGetModel siblingcycgetmodel)
        {
            string modelstr = "";
            string jssstr = jss.Serialize(siblingcycgetmodel);
            try
            {
                modelstr = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "项目周期预约\\" + xmno + "\\cycbespoke.txt");
                if (modelstr == "") return siblingCyc;
                var model = (ProcessResultDataBLL.DataCycGetModel)jss.Deserialize<ProcessResultDataBLL.DataCycGetModel>(modelstr);
                ExceptionLog.ExceptionWrite(string.Format("获取到有项目{0}的预约周期{1}" + modelstr, xmno, model.cyc));


                if (siblingCyc == model.cyc && ListHasCommElement(siblingcycgetmodel.points, model.points)) { return siblingCyc + 1; }
                return siblingCyc;


            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite(string.Format("获取到项目{0}已预约周期序列对象:{1}当前预约周期序列对象{2}", modelstr, xmno, jssstr));
                return siblingCyc;
            }
            finally
            {

                Tool.FileHelper.FileStringWrite(jssstr, System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "项目周期预约\\" + xmno + "\\cycbespoke.txt");

            }


        }

        public bool ListHasCommElement(List<string> src, List<string> des)
        {
            List<string> lstmp = new List<string>();
            lstmp.AddRange(src);
            lstmp.AddRange(des);
            lstmp.Distinct();
            if (lstmp.Count == src.Count + des.Count)
                return false;
            return true;
        }

        //public void CYCSort(string xmname)
        //{
        //    string sortxmnostr = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "周期重排项目序列\\setting.txt");
        //    ExceptionLog.ExceptionWrite("需要重排周期的项目有:"+sortxmnostr+"当前项目:"+xmname);
        //    if (!sortxmnostr.Split(',').ToList().Contains(xmname)) return;
        //    ExceptionLog.ExceptionWrite("现在进行项目"+xmname+"周期重排");
        //    SqlHelpers.database db = new SqlHelpers.database();
        //    string sql = "SELECT * FROM `fmos_cycdirnet` where taskname ="+xmname+" group by DATE_FORMAT(time,'%y%m%d %h') order by time;";
        //    System.Data.Odbc.OdbcConnection conn = db.GetStanderConn(xmname);
        //    System.Data.DataTable dt = SqlHelpers.querysql.querystanderdb(sql, conn);
        //    System.Data.DataView dv = new System.Data.DataView(dt);
        //    int i = 1;
        //    foreach (System.Data.DataRowView drv in dv)
        //    {
        //        sql = string.Format("update fmos_cycdirnet set cyc={0} where taskname="+xmname+" and  (DATE_FORMAT(time,'%y%m%d %h') = DATE_FORMAT('{1}','%y%m%d %h') or  DATE_FORMAT(time,'%y%m%d %h') = DATE_FORMAT('{2}','%y%m%d %h'))", i, drv["time"], Convert.ToDateTime(drv["time"]).AddHours(1));
        //        i++;
        //        SqlHelpers.updatedb udb = new SqlHelpers.updatedb();
        //        udb.UpdateStanderDB(sql, conn);
        //    }



        //}


    }

}