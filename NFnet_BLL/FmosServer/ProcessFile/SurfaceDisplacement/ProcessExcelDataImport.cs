﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;

namespace NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement
{
    public class ProcessExcelDataImport
    {
        public ExcelHelperHandle ExcelHelper = new ExcelHelperHandle();
        public TotalStation.BLL.fmos_obj.cycdirnet cycdirnetBLL = new TotalStation.BLL.fmos_obj.cycdirnet();
        public  List<string> mssglist = new  List<string>() ;
        public string mssg = "";
        public string woorkpath {get;set; }
        public void main()
        {

            ExcelHelper.workbookpath = woorkpath;
            ExcelHelper.ExcelInit();
            DataImport();
            //TotalStationDataImport();
        }
        public void DataImport()
        {
            int i = 0;
            for (i = 1; i < ExcelHelper.xBook.Sheets.Count+1; i++)
            {
                try
                {
                    ExcelHelper.xSheet = (Microsoft.Office.Interop.Excel.Worksheet)ExcelHelper.xBook.Sheets[i];
                    ExcelHelper.xSheet.Activate();
                    TotalStationDataImport();
                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite("广铁成果数据文件导入出错");
                }

            }
            ExcelHelper.xApp.Quit();
        }
        public void TotalStationDataImport()
        {
            //ExcelHelper.xSheet.Cells.Value
            string xmname = ExcelHelper.xSheet.Cells[4, 2].Value;
            int cyc = Convert.ToInt32(ExcelHelper.xSheet.Cells[3,1].Value);
            DateTime dt= ExcelHelper.xSheet.Cells[5,6].Value;
            int i = 8;
            TotalStation.Model.fmos_obj.cycdirnet model = null;
            while (ExcelHelper.xSheet.Cells[i, 1].Value.IndexOf("监测小结") == -1)
            {
                model = new TotalStation.Model.fmos_obj.cycdirnet { 
                     POINT_NAME = ExcelHelper.xSheet.Cells[i,1].Value,
                      TaskName = xmname,
                     Time = dt,
                     This_dN = ExcelHelper.xSheet.Cells[i, 2].Value,
                     This_dE = ExcelHelper.xSheet.Cells[i, 3].Value,
                     This_dZ = ExcelHelper.xSheet.Cells[i, 4].Value,
                     Ac_dN = ExcelHelper.xSheet.Cells[i, 5].Value,
                     Ac_dE = ExcelHelper.xSheet.Cells[i, 6].Value,
                     Ac_dZ = ExcelHelper.xSheet.Cells[i, 7].Value,
                      CYC = cyc,
                     siblingpointname = SiblingPointNameGet(ExcelHelper.xSheet.Cells[i,1].Value)
                };
                if (!cycdirnetBLL.Add(model, out mssg))
                {
                    mssglist.Add(mssg);
                }
                i++;
                
                //Console.WriteLine(mssg);
            }
           
        }

        public string SiblingPointNameGet(string pointname)
        {
            int i = 8;
            while (ExcelHelper.xSheet.Cells[i, 8].Value != "" && ExcelHelper.xSheet.Cells[i, 8].Value != null)
            {
                string siblingpointstr = ExcelHelper.xSheet.Cells[i, 8].Value;
                if (siblingpointstr.IndexOf(string.Format("{0}：",pointname)) != -1)
                {
                    return ExcelHelper.xSheet.Cells[i, 8].Value.Split('：')[1];
                }
                i++;
            }
            return "";
        }



        //public void ExcelClose()
        //{

        //     //保存
        //public  void save()
        //{
        //    ExcelHelper.xBook.Save();
        //    ExcelHelper.xSheet = null;
        //    ExcelHelper.xBook = null;
        //    ExcelHelper.xApp.Quit(); //这一句是非常重要的，否则Excel对象不能从内存中退出 
        //    ExcelHelper.xApp = null;
        //}
        //}

    }
}
