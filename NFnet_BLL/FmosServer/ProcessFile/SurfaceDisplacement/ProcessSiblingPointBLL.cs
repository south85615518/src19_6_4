﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement
{
    public class ProcessSiblingPointBLL
    {
        public static Authority.BLL.xmconnect bll = new Authority.BLL.xmconnect();
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public bool SiblingpointnameGet(SiblingpointnameGetModel siblingpointnameGetModel, out string mssg)
        {
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();

            if (!bll.GetModel(ProcessAspectIndirectValue.GetXmnoFromXmname(siblingpointnameGetModel.xmname), out model, out mssg))

                return false;
            var processSettlementDispPointReferenceModelLoadModel = new ProcessPointAlarmBLL.ProcessSettlementDispPointReferenceModelLoadModel(model.xmno, siblingpointnameGetModel.pointname);
            if (processPointAlarmBLL.ProcessSettlementDispPointReferenceModelLoad(processSettlementDispPointReferenceModelLoadModel, out mssg))
            {
                siblingpointnameGetModel.silblingpointname = processSettlementDispPointReferenceModelLoadModel.model.referencepoint_name;
                return true;
            }
            return false;

        }
        public class SiblingpointnameGetModel
        {
            public string pointname { get; set; }
            public string xmname { get; set; }
            public string silblingpointname { get; set; }
            public SiblingpointnameGetModel(string pointname, string xmname)
            {
                this.pointname = pointname;
                this.xmname = xmname;
            }
        }
    }
}