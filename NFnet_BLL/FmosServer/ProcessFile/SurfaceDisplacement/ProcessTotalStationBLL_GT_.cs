﻿using System;
using System.Collections.Generic;
using Tool;
using System.IO;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.DataImport.ProcessFile.SenmosServer;
using System.Threading;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.ProjectInfo;
namespace NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement
{
    /// <summary>
    /// 全站仪数据导入处理类
    /// </summary>
    public class ProcessTotalStationBLL_GT
    {
        public ProcessExcelDataImport processExcelDataImport = new ProcessExcelDataImport();
        public  ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL();
        public bool ProcessFileDecodeTxt(NFnet_BLL.DataImport.ProcessFile.Inclinometer.ProcessInclinometerBLL.ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            
            switch (model.fileType)
            {
                case "广铁成果数据":
                    //processTotalStationBLL.ProcessDeleteTmp(model.xmname, out mssg);
                    //processExcelDataImport.woorkpath = model.path;
                    //processExcelDataImport.main();
                    //mssg = string.Join(";",processExcelDataImport.mssglist);
                    //List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
                    //emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
                    return true;

                default: return false;

            }

        }


 
        public class DataImportModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string path { get; set; }
            public DataImportModel(string xmname, int xmno, string path)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
            }
        }

        /// <summary>
        /// 测斜仪数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileDecodeTxt(ProcessTotalStationBLL.ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            switch (model.fileType)
            {
                case "成果数据":
                    if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));

                        bool ipass = Tool.GTXLSHelper.ProcessXlsDataImportCheck(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno, model.xmname);
                        if (!ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", model.xmname)))
                        {

                            Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsDataDirImport));
                            t.Name = string.Format("GTresultdataimport", model.xmname);
                            t.Start(processXlsDataDirImportModel);
                            model.thread = t;
                        }
                        return ipass;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx")
                    {

                        bool ipass = Tool.GTXLSHelper.XlsFileDataImportCheck(model.path, out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(model.path,model.xmno,model.xmname);
                        if (!ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", model.xmname)))
                        {

                            Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsFileDataDirImport));
                            t.Name = string.Format("GTresultdataimport", model.xmname);
                            t.Start(processXlsDataDirImportModel);
                            model.thread = t;
                        }
                        return ipass;
                    }

                    return false;



                case "":

                    return true;

                case "时间":

                    return true;
                case "仪器":

                    return true;

                default: return false;

            }

        }
        /// <summary>
        /// 全站仪数据导入处理主程序处理参数实体类
        /// </summary>
        public class ProcessFileDecodeTxtModel : TotalStationFileUploadConditionModel
        {
            /// <summary>
            /// 文件类型
            /// </summary>
            public string fileType { get; set; }
            public Thread thread { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessFileDecodeTxtModel(string xmname, int xmno, string path, string fileType)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
                this.fileType = fileType;
            }

        }
        public ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public TotalStation.BLL.fmos_obj.cycdirnet cycdirnetBLL = new TotalStation.BLL.fmos_obj.cycdirnet();
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public static string mssg = "";
        public bool ProcessXlsDataImport(string xlspath,out string mssg,out string outmssgimport)
        {
            
         
            outmssgimport = "";
            string mssgimport = "";
            GTXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);
            DataTable dt = new DataTable();
            GTXLSHelper.ProcessXlsDataTableImport(xlspath,out dt,out mssg);
            if (dt.Rows.Count == 0) return false;
            SiblingpointnameGetModel siblingpointnameGetModel = null;
            foreach (DataRow dr in dt.Rows)
            {

                
                global::TotalStation.Model.fmos_obj.cycdirnet model = new global::TotalStation.Model.fmos_obj.cycdirnet
                {
                    TaskName = dr["xmname"].ToString(),
                    POINT_NAME = dr["point_name"].ToString(),
                    Time = Convert.ToDateTime(dr["time"]),
                    This_dN = Convert.ToDouble(dr["This_dN"]),
                    This_dE = Convert.ToDouble(dr["This_dE"]),
                    This_dZ = Convert.ToDouble(dr["This_dZ"]),
                    Ac_dN = Convert.ToDouble(dr["Ac_dN"]),
                    Ac_dE = Convert.ToDouble(dr["Ac_dE"]),
                    Ac_dZ = Convert.ToDouble(dr["Ac_dZ"]),
                    CYC = Convert.ToInt32(dr["cyc"]),
                    siblingpointname = dr["siblingpointname"].ToString()

                };
                siblingpointnameGetModel = new SiblingpointnameGetModel(model.TaskName,model.POINT_NAME);
                if (SiblingpointnameGet(siblingpointnameGetModel, out mssg))
                {
                    model.siblingpointname = siblingpointnameGetModel.silblingpointname;
                }
                if (!cycdirnetBLL.Add(model, out mssgimport))
                {
                    if (mssgimport!= "")
                    outmssgimport += "\r\n" + mssgimport;
                    ExceptionLog.ExceptionWrite(mssg);
                }
            }
            return true;
        }




        public class ProcessXlsDataDirImportModel
        {
            public string dirpath { get; set; }
            public int xmno { get; set; }
            public string xmname { get; set; }
            public ProcessXlsDataDirImportModel(string dirpath, int xmno, string xmname)
            {
                this.dirpath = dirpath;
                this.xmno = xmno;
                this.xmname = xmname;
            }
        }
        public static Authority.BLL.xmconnect bll = new Authority.BLL.xmconnect();
        public bool SiblingpointnameGet(SiblingpointnameGetModel siblingpointnameGetModel, out string mssg)
        {
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();
            
            if (!bll.GetModel(siblingpointnameGetModel.xmname, out model, out mssg))

                return false;
            var processSettlementDispPointReferenceModelLoadModel = new ProcessPointAlarmBLL.ProcessSettlementDispPointReferenceModelLoadModel(model.xmno, siblingpointnameGetModel.pointname);
            if (processPointAlarmBLL.ProcessSettlementDispPointReferenceModelLoad(processSettlementDispPointReferenceModelLoadModel, out mssg))
            {
                siblingpointnameGetModel.silblingpointname = processSettlementDispPointReferenceModelLoadModel.model.referencepoint_name;
                return true;
            }
            return false;
            
        }
        public class SiblingpointnameGetModel
        {
            public string pointname { get; set; }
            public string xmname { get; set; }
            public string silblingpointname { get; set; }
            public SiblingpointnameGetModel(string pointname, string xmname)
            {
                this.pointname = pointname;
                this.xmname = xmname;
            }
        }


        public void ProcessXlsFileDataDirImport(object obj)
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            Tool.ThreadProcess.Threads.Add(string.Format("{0}GT_cycdirnet", model.xmname));
            string filename = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            string importmssg = "";
            if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
            {
                ProcessXlsDataImport(filename, out mssg, out importmssg);

                filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg);
                filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg);
                ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg));
                ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg));
            }

            
           ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
            emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);

            Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_cycdirnet", model.xmname));
            //return result;
        }



        public void ProcessXlsDataDirImport(object obj)
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            string dirpath = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            string importmssg = "";
            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
                {
                    ProcessXlsDataImport(filename, out mssg, out importmssg);

                    filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg);
                    filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg);
                    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg));
                    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg));
                }

            }
            ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
            emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);

            Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_cycdirnet", model.xmname));
            //return result;
        }


      
   




    }

}