﻿using System;
using System.Collections.Generic;
using Tool;
using System.IO;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.DataImport.ProcessFile.SenmosServer;
using System.Threading;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.ProjectInfo;
using NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement;
namespace NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement
{
    /// <summary>
    /// 全站仪数据导入处理类
    /// </summary>
    public class ProcessTotalStationBLL_GT
    {
        public ProcessExcelDataImport processExcelDataImport = new ProcessExcelDataImport();
        public  ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL();
        //public bool ProcessFileDecodeTxt(NFnet_BLL.DataImport.ProcessFile.Inclinometer.ProcessInclinometerBLL.ProcessFileDecodeTxtModel model, out string mssg)
        //{
        //    mssg = "";
            
        //    switch (model.fileType)
        //    {
        //        case "广铁成果数据":
        //            //processTotalStationBLL.ProcessDeleteTmp(model.xmname, out mssg);
        //            //processExcelDataImport.woorkpath = model.path;
        //            //processExcelDataImport.main();
        //            //mssg = string.Join(";",processExcelDataImport.mssglist);
        //            //List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
        //            //emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
        //            return true;

        //        default: return false;

        //    }

        //}


 
        public class DataImportModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string path { get; set; }
            public DataImportModel(string xmname, int xmno, string path)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
            }
        }

        /// <summary>
        /// 数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileDecodeTxt(ProcessTotalStationBLL.ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            switch (model.fileType)
            {
                case "成果数据":
                    if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));

                        bool ipass = Tool.GTXLSHelper.ProcessXlsDataImportCheck(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno, model.xmname);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", model.xmname)))
                        //{

                        //    Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsDataDirImport));
                        //    t.Name = string.Format("GTresultdataimport", model.xmname);
                            ProcessXlsDataDirImport(processXlsDataDirImportModel);
                        //    model.thread = t;
                        //}
                        //return ipass;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx")
                    {

                        bool ipass = Tool.GTXLSHelper.XlsFileDataImportCheck(model.path, out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(model.path,model.xmno,model.xmname);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", model.xmname)))
                        //{

                        //    Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsFileDataDirImport));
                        //    t.Name = string.Format("GTresultdataimport", model.xmname);
                            ProcessXlsFileDataDirImport(processXlsDataDirImportModel);
                        //    model.thread = t;
                        //}
                        return ipass;
                    }

                    return false;



                case "成果数据删除":

                      if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno, model.xmname);
                        ProcessXlsDataDirDescode(processXlsDataDirImportModel);
                           return true;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx")
                    {

                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(model.path,model.xmno,model.xmname);
                        ProcessXlsFileDataDirDescode(processXlsDataDirImportModel);
                        return true;
                    }

                    return false;

                case "时间":

                    return true;
                case "仪器":

                    return true;

                default: return false;

            }

        }
        /// <summary>
        /// 全站仪数据导入处理主程序处理参数实体类
        /// </summary>
        public class ProcessFileDecodeTxtModel : TotalStationFileUploadConditionModel
        {
            /// <summary>
            /// 文件类型
            /// </summary>
            public string fileType { get; set; }
            public Thread thread { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessFileDecodeTxtModel(string xmname, int xmno, string path, string fileType)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
                this.fileType = fileType;
            }

        }
        public ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public TotalStation.BLL.fmos_obj.cycdirnet cycdirnetBLL = new TotalStation.BLL.fmos_obj.cycdirnet();
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public static string mssg = "";
        public ProcessSiblingPointBLL processSiblingPointBLL = new ProcessSiblingPointBLL();
        public bool ProcessXlsDataImport(string xlspath,out int cont,out string mssg,out string outmssgimport)
        {
            string xmname = "";
            cont = 0;
            outmssgimport = "";
            string mssgimport = "";
            GTXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);
            DataTable dt = new DataTable();
            GTXLSHelper.ProcessXlsDataTableImport(xlspath,out dt,out mssg);
            if (dt.Rows.Count == 0) return false;
            NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement.ProcessSiblingPointBLL.SiblingpointnameGetModel siblingpointnameGetModel = null;
            if (dt.Rows.Count > 0) xmname = dt.Rows[0]["xmname"].ToString();
            int xmno = GetXmnoFromXmname(xmname);
            foreach (DataRow dr in dt.Rows)
            {
                
                
                global::TotalStation.Model.fmos_obj.cycdirnet model = new global::TotalStation.Model.fmos_obj.cycdirnet
                {

                    TaskName =  xmno.ToString(),
                    POINT_NAME = dr["point_name"].ToString(),
                    Time = Convert.ToDateTime(dr["time"]),
                    This_dN = Convert.ToDouble(dr["This_dN"]),
                    This_dE = Convert.ToDouble(dr["This_dE"]),
                    This_dZ = Convert.ToDouble(dr["This_dZ"]),
                    Ac_dN = Convert.ToDouble(dr["Ac_dN"]),
                    Ac_dE = Convert.ToDouble(dr["Ac_dE"]),
                    Ac_dZ = Convert.ToDouble(dr["Ac_dZ"]),
                    CYC = Convert.ToInt32(dr["cyc"]),
                    siblingpointname = dr["siblingpointname"].ToString()
                    

                };
                siblingpointnameGetModel = new NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement.ProcessSiblingPointBLL.SiblingpointnameGetModel(model.POINT_NAME, model.TaskName);

               

                if (processSiblingPointBLL.SiblingpointnameGet(siblingpointnameGetModel, out mssg))
                {
                    model.siblingpointname = siblingpointnameGetModel.silblingpointname;
                    
                }
                if (!cycdirnetBLL.Add(model, out mssgimport))
                {
                    if (mssgimport != "")
                        outmssgimport += "\r\n" + mssgimport;
                    ExceptionLog.ExceptionWrite(mssg);
                }
                else
                {
                    cont++;
                }
            }
            //ExceptionLog.TotalSationPointCheckVedioWrite("项目"+xmname+"共导入全站仪结果数据数据"+cont+"条");
            return true;
        }

        public bool ProcessXlsDataDelete(string xlspath, out string mssg, out string outmssgimport)
        {


            outmssgimport = "";
            string mssgimport = "";
            GTXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);
            NFnet_BLL.DataProcess.ProcessResultDataBLL processResultDataBLL = new DataProcess.ProcessResultDataBLL();
            List<Tool.GTXLSHelper.DeleteDataFromXlsOutInfoModel> modellist = null;
            GTXLSHelper.ProcessXlsCycDataDelete(xlspath, out modellist, out mssg);
            
            foreach (var model in modellist)
            {

                //测量库
                var processDateTimeCycGetModel = new NFnet_BLL.DataProcess.ProcessResultDataBLL.ProcessDateTimeCycGetModel
( ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname),model.dt);
                processResultDataBLL.ProcessDateTimeCycGet(processDateTimeCycGetModel,out mssg);
                var cycdirnetCYCDataDeleteModel = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessCycdirnetCYCDataDeleteModel(processDateTimeCycGetModel.cyc, processDateTimeCycGetModel.cyc, model.xmname, "");
                ProcessSurveyCycdirnetCYCDataDelete(cycdirnetCYCDataDeleteModel, out mssg);
                ExceptionLog.ExceptionWrite(mssg);
                //成果库
                processResultDataBLL.ProcessCgDateTimeCycGet(processDateTimeCycGetModel, out mssg);
                cycdirnetCYCDataDeleteModel = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessCycdirnetCYCDataDeleteModel(processDateTimeCycGetModel.cyc, processDateTimeCycGetModel.cyc, model.xmname, "");
                ProcessCgCycdirnetCYCDataDelete(cycdirnetCYCDataDeleteModel, out mssg);
                ExceptionLog.ExceptionWrite(mssg);
              
            }
            return true;
        }

       




        public class ProcessXlsDataDirImportModel
        {
            public string dirpath { get; set; }
            public int xmno { get; set; }
            public string xmname { get; set; }
            public ProcessXlsDataDirImportModel(string dirpath, int xmno, string xmname)
            {
                this.dirpath = dirpath;
                this.xmno = xmno;
                this.xmname = xmname;
            }
        }


        public bool ProcessSurveyCycdirnetCYCDataDelete(NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessCycdirnetCYCDataDeleteModel model, out string mssg)
        {
            return cycdirnetBLL.DeleteSurveyData(model.startcyc, model.endcyc, GetXmnoFromXmname(model.xmname).ToString(), model.pointname, out mssg);
        }
        public bool ProcessCgCycdirnetCYCDataDelete(NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessCycdirnetCYCDataDeleteModel model, out string mssg)
        {
            return cycdirnetBLL.DeleteCg(model.startcyc, model.endcyc, GetXmnoFromXmname(model.xmname).ToString(), model.pointname, out mssg);
        }
        public static Authority.BLL.xmconnect bll = new Authority.BLL.xmconnect();
      


        public void ProcessXlsFileDataDirImport(object obj)
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            Tool.ThreadProcess.Threads.Add(string.Format("{0}GT_cycdirnet", model.xmname));
            string filename = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            ProcessDeleteTmp(model.xmname, out mssg);
            ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
            mssg = "";
            int cont = 0;
            int totalcount = 0;
            string importmssg = "";
            if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
            {
                ProcessXlsDataImport(filename, out cont,out mssg, out importmssg);
                totalcount += cont;
                ExceptionLog.TotalSationPointCheckVedioWrite("项目" + model.xmname + "共导入全站仪结果数据数据" + totalcount + "条");
                filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg);
                filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg);
                ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg));
                ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg));
            }

            
           ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
            emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);

            Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_cycdirnet", model.xmname));
            //return result;
        }
        public void ProcessXlsFileDataDirDescode(object obj)
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            string filename = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            string importmssg = "";
            if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
            {
                ProcessXlsDataDelete(filename, out mssg, out importmssg);

                filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除完成!" : mssg);
                filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除完成!" : importmssg);
                ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除完成!" : mssg));
                ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除完成!" : importmssg));
            }


            ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
        }





       
        public void ProcessXlsDataDirImport(object obj)
        {

            
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            string dirpath = model.dirpath;
            int xmno = model.xmno;
            string mssg = "";
            bool result = true;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            string filemssg = "";
            string filemiportmssg = "";
            string importmssg = "";
            ProcessDeleteTmp(model.xmname, out mssg);
            ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
            mssg = "";
            int cont = 0;
            int totalcount = 0;
            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
                {
                    ProcessXlsDataImport(filename, out cont,out mssg, out importmssg);
                    totalcount += cont;
                    ExceptionLog.TotalSationPointCheckVedioWrite("项目" + model.xmname + "共导入全站仪结果数据数据" + totalcount + "条");
                    filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg);
                    filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg);
                    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg));
                    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg));
                }

            }
            ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
            emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);

            Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_cycdirnet", model.xmname));
            //return result;
        }
        public void ProcessXlsDataDirDescode(object obj)
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            string dirpath = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            string importmssg = "";
            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
                {
                    ProcessXlsDataDelete(filename, out mssg, out importmssg);

                    filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除成功!" : mssg);
                    filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除成功!" : importmssg);
                    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除成功!" : mssg));
                    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除成功!" : importmssg));
                }

            }
            ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_cycdirnet", model.xmname));
            //return result;
        }
        public bool ProcessDeleteTmp(string xmname, out string mssg)
        {
            return cycdirnetBLL.DeleteTmp(xmname, out mssg);
        }

        public static int GetXmnoFromXmname(string xmname)
        {
            string mssg = "";
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();
            if (bll.GetModel(xmname, out model, out mssg))
            {
                return model.xmno;
            }
            else
            {
                mssg = "不存在该项目名称的项目编号";
                return -1;
            }
        }

    }

}