﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.pub;

namespace NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement
{
    /// <summary>
    /// 全站仪数据导入请求参数类
    /// </summary>
    public class TotalStationFileUploadConditionModel
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string path{get;set;}
        
        public TotalStationFileUploadConditionModel(string xmname,string path)
        {
            this.path = path;
            this.xmname =ProcessAspectIndirectValue.GetXmnoFromXmnameStr(xmname);
            this.xmno = ProcessAspectIndirectValue.GetXmnoFromXmname(xmname);
            
        }
        
        public TotalStationFileUploadConditionModel()
        {
          
        }
       
      
    }
}