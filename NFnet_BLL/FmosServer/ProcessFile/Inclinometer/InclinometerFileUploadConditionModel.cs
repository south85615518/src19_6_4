﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DataImport.ProcessFile.Inclinometer
{
    /// <summary>
    /// 全站仪数据导入请求参数类
    /// </summary>
    public class InclinometerFileUploadConditionModel
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string xmname { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string path{get;set;}
        /// <summary>
        /// 项目编号
        /// </summary>
        public int xmno { get; set; }
        public InclinometerFileUploadConditionModel(string xmname,int xmno,string path)
        {
            this.path = path;
            this.xmno = xmno;
            this.xmname = xmname;
        }
        public InclinometerFileUploadConditionModel()
        {
          
        }
    }
}