﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement;
using Tool;
using System.Web.Script.Serialization;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.Inclinometer;
using NFnet_BLL.DisplayDataProcess.pub;
using System.Data;

namespace NFnet_BLL.DataImport.ProcessFile.Inclinometer
{
    public class ProcessInclinometerBLL
    {
        public NFnet_BLL.DisplayDataProcess.Inclinometer.ProcessInclinometerBLL processInclinometerBLL = new NFnet_BLL.DisplayDataProcess.Inclinometer.ProcessInclinometerBLL();
        public JavaScriptSerializer jss = new JavaScriptSerializer();
        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">测斜文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataBLL(InclinometerFileUploadConditionModel model, out string mssg)
        {



            if (!processInclinometerBLL.ProcessDeleteTmp(model.xmno, out mssg)) return false;
            int i = 0;
            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = sr.ReadLine();
                //InclimeterDAL.DAL.senor_data dal = new TotalStation.DAL.fmos_obj.setting();
                while (ThreadProcess.ThreadExist(string.Format("{0}inclinometer", model.xmname)) && strTemp != null && strTemp != "" && strTemp.Trim() != "" && strTemp != "ThisIsFileEnd")
                {
                    try
                    {
                        InclimeterDAL.Model.senor_data senor_data = (InclimeterDAL.Model.senor_data)jss.Deserialize(strTemp, typeof(InclimeterDAL.Model.senor_data));
                        senor_data.xmno = model.xmno;
                        //DateTime dt = inclinometer.Time;



                        ////dt = Convert.ToDateTime(string.Format("{0}-{1}-{2} {3}:{4}:{5}", dt.Year,dt.Month,dt.Day,dt.Hour,dt.Minute,dt.Second));


                        //inclinometer.Time = dt;
                        if (!processInclinometerBLL.ProcessSenorDataInsert(senor_data, out mssg))
                        {
                            //添加出错
                        }
                        strTemp = sr.ReadLine();
                        i++;
                    }
                    catch (Exception ex)
                    {
                        mssg += string.Format("{1}文件第{0}行存在错误", i, "#");
                        strTemp = sr.ReadLine();
                        i++;
                    }

                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }
        public class DataImportModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string path { get; set; }
            public DataImportModel(string xmname, int xmno, string path)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
            }
        }
        /// <summary>
        /// 测斜仪数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileDecodeTxt(ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            switch (model.fileType)
            {
                case "结果数据":
                    if (Path.GetFileNameWithoutExtension(model.path) == ".txt")
                    {
                        bool ispass = Tool.ObjectHelper.ProcessTextFileCheck<InclimeterDAL.Model.senor_data>(model.path, out mssg);
                        var inclinometerDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);
                        if (!ThreadProcess.ThreadExist(string.Format("{0}inclinometer", model.xmname)))
                        {

                            Thread t = new Thread(new ParameterizedThreadStart(InclinometerDataImport));
                            t.Name = string.Format("{0}resultdataimport", model.xmname);
                            t.Start(inclinometerDataImportModel);
                            model.thread = t;
                        }
                        return ispass;
                    }
                    else
                    {

                        string dirpath = Path.GetDirectoryName(model.path) + "\\" + Path.GetFileNameWithoutExtension(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath);

                        bool ipass = Tool.SenorXLSHelper.ProcessXlsDataImportCheck(dirpath,model.xmno,out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(dirpath, model.xmno,model.xmname);
                        if (!ThreadProcess.ThreadExist(string.Format("{0}inclinometer", model.xmname)))
                        {

                            Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsDataDirImport));
                            t.Name = string.Format("{0}inclinometerdataimport", model.xmname);
                            t.Start(processXlsDataDirImportModel);
                            model.thread = t;
                        }
                        return ipass;
                    }



                case "设置":
                  
                    return true;

                case "时间":
                  
                    return true;
                case "仪器":
                  
                    return true;
             
                default: return false;

            }

        }
        /// <summary>
        /// 全站仪数据导入处理主程序处理参数实体类
        /// </summary>
        public class ProcessFileDecodeTxtModel : TotalStationFileUploadConditionModel
        {
            /// <summary>
            /// 文件类型
            /// </summary>
            public string fileType { get; set; }
            public Thread thread { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessFileDecodeTxtModel(string xmname, int xmno, string path, string fileType)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
                this.fileType = fileType;
            }

        }
        public ProcessSenorDataAlarmBLL senorDataAlarmBLL = new ProcessSenorDataAlarmBLL();
        public ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public void InclinometerDataImport(object obj)
        {

            DataImportModel model = obj as DataImportModel;

            Tool.ThreadProcess.Threads.Add(string.Format("{0}inclinometer", model.xmname));
            string mssg = "";
            var inclinometerFileUploadConditionModel = new InclinometerFileUploadConditionModel(model.xmname,model.xmno ,model.path);
            if (!ProcessResultDataBLL(inclinometerFileUploadConditionModel, out mssg))
            {
                return;
            }
            else
            {

                List<string> alarmInfoList = senorDataAlarmBLL.SenorDataAlarm(model.xmname, model.xmno);
                emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
            }
            Tool.ThreadProcess.Threads.Remove(string.Format("{0}inclinometer", model.xmname));


        }

        public NFnet_BLL.DisplayDataProcess.Inclinometer.ProcessInclinometerBLL inclinometerBLL = new NFnet_BLL.DisplayDataProcess.Inclinometer.ProcessInclinometerBLL();
        public static string mssg = "";
        public bool ProcessXlsDataImport(string xlspath,int xmno)
        {
            SenorXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);
            DataTable dt = SenorXLSHelper.CetDataFromXls();
            if (dt.Rows.Count == 0) return false; 
            foreach (DataRow dr in dt.Rows)
            {
                InclimeterDAL.Model.senor_data model = new InclimeterDAL.Model.senor_data
                {
                    point_name = dr["point_name"].ToString(),
                    time = Convert.ToDateTime(dr["time"]),
                    region = "1",
                    xmno = xmno,
                    ac_disp = Convert.ToDouble(Convert.ToDouble(dr["ac_disp"]).ToString("0.00")) ,
                    this_disp = Convert.ToDouble(Convert.ToDouble(dr["this_disp"]).ToString("0.00")),
                    deep = Convert.ToDouble(dr["deep"]),
                    this_rap = Convert.ToDouble(Convert.ToDouble(dr["this_rap"]).ToString("0.00")),
                    previous_disp = Convert.ToDouble(Convert.ToDouble(dr["previous_disp"]).ToString("0.00")),
                    previous_time = Convert.ToDateTime(dr["previous_time"]),
                    mtimes = Convert.ToInt32(dr["mtimes"])

                };
                if (!inclinometerBLL.ProcessSenorDataInsert(model, out mssg))
                {
                    ExceptionLog.ExceptionWrite(mssg);
                }
            }
            return true;
        }
       



        public class ProcessXlsDataDirImportModel
        {
            public string dirpath { get; set; }
            public int xmno { get; set; }
            public string xmname{get;set;}
            public ProcessXlsDataDirImportModel(string dirpath,int xmno,string xmname)
            {
                this.dirpath = dirpath;
                this.xmno = xmno;
                this.xmname = xmname;
            }
        }

        public void ProcessXlsDataDirImport(object obj  )
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            string dirpath = Path.GetDirectoryName(model.dirpath);
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
                    if (!ProcessXlsDataImport(filename, xmno))
                    {
                        mssg = string.Format("{0}测斜数据导入失败",filename+"、{0}");
                        result = false;
                    }
                
            }
            mssg = mssg.Replace("{0}","");
            List<string> alarmInfoList = senorDataAlarmBLL.SenorDataAlarm(model.xmname, model.xmno);
            emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
            
            Tool.ThreadProcess.Threads.Remove(string.Format("{0}inclinometer", model.xmname));
            //return result;
        }

    }
}