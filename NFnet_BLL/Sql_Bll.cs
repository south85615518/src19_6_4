﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace NFnet_BLL
{
    public class Sql_Bll : Sql_Bll_I
    {


        public string GetUnitSql_i()
        {
            return @"insert into unit(
            unitName, 
            pro, 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof,
            state,
            createTime,
            dbname
            )values(
            @unitName, 
            @pro, 
            @city, 
            @country, 
            @addr, 
            @tel, 
            @email, 
            @natrue, 
            @linkman, 
            @aptitude, 
            @police, 
            @taxproof,
            @state,
            @createTime,
            @dbname
            )";
        }


        public string GetUnitSql_u()
        {
            return @"update  unit(
            pro = @pro , 
            city = @city, 
            country = @country, 
            addr=@addr, 
            tel=@tel, 
            email=@email, 
            natrue=@natrue, 
            linkman=@linkman, 
            aptitude=@aptitude, 
            police=@police, 
            taxproof=@taxproof
            ) where
            uintName = @unitName
            ";
        }


        public string GetUnitSql_s()
        {
            return @"select pro , 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof,state,dbname from unit where unitName = @unitName";
            
        }


        public string GetUnitSql_ls()
        {
            return @"select pro , 
            city, 
            country, 
            addr, 
            tel, 
            email, 
            natrue, 
            linkman, 
            aptitude, 
            police, 
            taxproof from unit ";
        }


        public string GetMemberSql_i()
        {
            return @"insert into member(
            [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel],
            [email], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],
            [createTime]
            )values(
            @userId,
            @pass,
            @role, 
            @userGroup, 
            @userName, 
            @workNo, 
            @position, 
            @tel,
            @email, 
            @zczsmc, 
            @zczsbh, 
            @zczs, 
            @sgzsmc,
            @sgzsbh,
            @sgzs,
            @unitName,
            @createTime)";
        }
        /*
         */
        /*
         */
        public string GetMemberSql_u()
        {
            return @"update  member set
            [pass] = @pass , 
            [role] = @role, 
            [userGroup] = @userGroup, 
            [userName] = @userName, 
            [workNo] = @workNo, 
            [position] = @position, 
            [tel] = @tel, 
            [zczsmc] = @zczsmc , 
            [zczsbh] = @zczsbh, 
            [zczs] = @zczs, 
            [sgzsmc] = @sgzsmc,
            [sgzsbh] = @sgzsbh,
            [sgzs] = @sgzs,
            [email] = @email
             where
            [userId] = @userId
            ";
        }

        public string GetMemberSql_s()
        {
            return @"select [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],[email] from member where userId = @userId";
        }

        public string GetMemberSql_ls()
        {
            return @"select  [userId], 
            [pass], 
            [role], 
            [userGroup], 
            [userName], 
            [workNo], 
            [position], 
            [tel], 
            [zczsmc], 
            [zczsbh], 
            [zczs], 
            [sgzsmc],
            [sgzsbh],
            [sgzs],
            [unitName],[email] from member ";
        }


        public string GetMemberSql_t()
        {
            return "select * from member where unitName = @unitName";
        }


        public string MemberLoginSql_s()
        {
            return "select * from member where userId = @userId and [pass]= @pass";
        }


        public string GetUnitSql_t()
        {
            return "select * from unit ";
        }


        public string GetInstrumentSql_i()
        {
            return @"insert into Instrument(
            [instrumentName],
            [vender],
            [instrumentNo],
            [instrumentType],
            [verificationDate],
            [validityPeriod],
            [verificationProof],
            [unitName]
            )values(
            @instrumentName,
            @vender,
            @instrumentNo,
            @instrumentType,
            @verificationDate,
            @validityPeriod,
            @verificationProof,
            @unitName
            )";
        }

        public string GetInstrumentSql_u()
        {
            throw new NotImplementedException();
        }

        public string GetInstrumentSql_s()
        {
            throw new NotImplementedException();
        }

        public string GetInstrumentSql_t()
        {
            return "select * from instrument where unitName=@unitName";
        }

        public string GetInstrumentSql_ls()
        {
            throw new NotImplementedException();
        }

        public string InstrumentLoginSql_s()
        {
            throw new NotImplementedException();
        }


        public string dbNameSql_s()
        {
            return @"SELECT SCHEMA_NAME 
FROM information_schema.SCHEMATA";
        }

        
        public string taskSql_s()
        {
            return @"select taskname from fmos_task";
        }


        public string projectInfoSql_u()
        {
            return @"insert into xmconnect( xmname,jcdw,dbase) values (@xmname,@jcdw,@dbase)";
        }


        public string projectSql_u()
        {
            throw new NotImplementedException();
        }


        public string studyPointSql_s()
        {
            return @"select point_name from fmos_studypoint";
        }


        public System.Data.DataTable FmosTableSql_s()
        {
            return null;
        }


        public string XminfoSql_s()
        {
            return "select * from xmconnect where xmname = @xmname ";
        }


        public string NearestSql_s()
        {
            return "select xmname from xmconnect,member where xmconnect.jcdw = member.unitName and member.userId= @userId order by  jcpgstarttime desc";
        }


        public string CGNearest_s()
        {
            return "select xmname from xmconnect  order by  jcpgstarttime desc";
        }


        public string GMXmnamesql_s()
        {
            return "select xmname from xmconnect where jcdw = @jcdw";
        }
    }
}