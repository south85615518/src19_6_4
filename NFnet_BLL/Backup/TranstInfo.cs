﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
namespace NFnet_BLL
{
    public class TranstInfo
    {
        private string xmname;//项目名称

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        private string tabname;//监测类型

        public string Tabname
        {
          get { return tabname; }
          set { tabname = value; }
        }

        private string filename;//文件名

        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }

        private Stream fs;

        public Stream Fs
        {
            get { return fs; }
            set { fs = value; }
            
        }
        private TranstCase tc = null;

        public TranstCase Tc
        {
            get { return tc; }
            set { tc = value; }
        }
        public TranstInfo(string xmname,string tabname,Stream fs,string filename)
        {
            this.filename = filename;
            this.xmname = xmname;
            this.tabname = tabname;
            this.fs = fs;
            
        }
    }
}