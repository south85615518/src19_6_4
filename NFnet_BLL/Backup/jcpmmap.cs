﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using NFnet_DAL;
namespace NFnet_BLL
{
    /// <summary>
    /// 监测平面图实例类
    /// </summary>
    public class jcpmmap
    {
        private string xmname;//项目名称

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        private int id;//编号

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string jcpmName;//监测平面名称

        public string JcpmName
        {
            get { return jcpmName; }
            set { jcpmName = value; }
        }
        private string jcpmImgUrl;//监测平面底图url

        public string JcpmImgUrl
        {
            get { return jcpmImgUrl; }
            set { jcpmImgUrl = value; }
        }
        private int groupId;//所属组ID

        public int GroupId
        {
            get { return groupId; }
            set { groupId = value; }
        }
        private string remark;//备注

        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        public jcpmmap( string jcpmName, string jcpmImgUrl, int groupId, string remark,string xmname)
        {
            this.jcpmName = jcpmName;
            this.jcpmImgUrl = jcpmImgUrl;
            this.groupId = groupId;
            this.remark = remark;
            this.xmname = xmname;
        }
        /// <summary>
        /// 想监测平面表中插入一条记录
        /// </summary>
        public void  InsertJcpmMap()
        {
            string sql = @"insert into jcmaptab (JcpmName,JcpmImgUrl,GroupId,remark)
        values('"+this.jcpmName+"','"+this.jcpmImgUrl+"',"+this.groupId+",'"+this.remark+"')";
            database db = new database();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql,conn);
        }
        public List<jcpmmap> GetJcpmMapEntrl(string sql)
        {
            database db = new database();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            DataTable dt = new DataTable();
            dt = querysql.querystanderdb(sql,conn);
            DataView dv = new DataView(dt);
            List<jcpmmap> ljp = new List<jcpmmap>();
            foreach (DataRowView drv in dv)
            {
                jcpmmap mp = new jcpmmap(drv["JcpmName"].ToString(), drv["JcpmImgUrl"].ToString(), Int32.Parse(drv["groupId"].ToString()), drv["remark"].ToString(),this.xmname);
                ljp.Add(mp);
            }
            return ljp;
        }
        public jcpmmap(string xmname)
        {
            this.xmname = xmname;
        }
    }
}