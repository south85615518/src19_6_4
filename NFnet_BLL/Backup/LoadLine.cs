﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class LoadLine
    {
        private int sort;//进度条序号

        public int Sort
        {
            get { return sort; }
            set { sort = value; }
        }
        private int tabs;//总的表数

        public int Tabs
        {
            get { return tabs; }
            set { tabs = value; }
        }
        private string tabName;//表名

        public string TabName
        {
            get { return tabName; }
            set { tabName = value; }
        }
        private int allCount;//总的进度条数

        public int AllCount
        {
            get { return allCount; }
            set { allCount = value; }
        }
        private int nowCount;//当前进度条数

        public int NowCount
        {
            get { return nowCount; }
            set { nowCount = value; }
        }
        public LoadLine(int sort,int tabs,string tabName,int allCount,int nowCount)
        {
            this.sort = sort;
            this.tabs = tabs;
            this.tabName = tabName;
            this.allCount = allCount;
            this.nowCount = nowCount;
        }
        /// <summary>
        /// 获取进度条的进度
        /// </summary>
        /// <returns></returns>
        public float GetDegree()
        {
           float percent = (nowCount / allCount)*100;
           return percent;
        }
    }
}