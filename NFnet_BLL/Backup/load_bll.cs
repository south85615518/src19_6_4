﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using NFnet_DAL;
using NFnet_MODAL;

namespace NFnet_BLL
{
    public class load_bll : System.Web.UI.Page
    {
        public static accessdbse adb = new accessdbse();
        public static int _i, _j = 0;
        public static database db = new database();
        public static load_bll bll = new load_bll();
        //读取项目信息
        public void filldb(TextBox author,TextBox unitName,Label identya )
        {
            string userID = Session["userID"].ToString();

            member member = new member { UserId = userID };
            member = member.GetMember();
            author.Text = member.Position;
            unitName.Text = member.UnitName;
            identya.Text = string.Format("{0}:{1}", member.Position, member.UserName);
        }

        //按照SQL语句查询表
        public int filldb(string sql,Panel pl)
        {

            OleDbConnection conn = adb.getconn();
            //根据用户名先查取范围
            //根据范围查询项目
            OleDbDataAdapter ods = new OleDbDataAdapter(sql, conn);
            conn.Close();
            DataTable dt = new DataTable();
            conn.Close();
            ods.Fill(dt);
            ProjectListLoad(dt,pl);
            return 0;
        }
        public void multiKeySearch_bll(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area, Panel projectList)
        {
            member member = new member { UserId = userID };
            member = member.GetMember();
            string unitName = member.UnitName;
            string sql = "";
            if (unitName == "")
            {
                try
                {
                    //管理员的工程查询
                    string safeLevel = gcjb.TrimEnd() == "不限" ? " 1=1 and " : " safeLevel='" + gcjb.TrimEnd() + "' and  ";
                    gclx = gclx.TrimEnd() == "不限" ? " 1=1 and " : " xmType ='" + gclx.TrimEnd() + "' and ";
                    string gcName = gcname.TrimEnd() == "" ? " 1=1 " : " xmname ='" + gcname.TrimEnd() + "'";
                    sql = "select '" + userID + "' as userID,'\"'+xmname+'\"' as xmno,* from xmconnect where  " + safeLevel + gclx + gcName;
                    filldb(sql,projectList);

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex.Message);

                }
            }
            else
            {
                try
                {
                    //管理员的工程查询
                    string safeLevel = gcjb.TrimEnd() == "不限" ? " 1=1 and " : " safeLevel='" + gcjb.TrimEnd() + "' and  ";
                    gclx = gclx.TrimEnd() == "不限" ? " 1=1 and " : " xmType ='" + gclx.TrimEnd() + "' and ";
                    string gcName = gcname.TrimEnd() == "" ? " 1=1 " : " xmname ='" + gcname.TrimEnd() + "'";
                    sql = "select '" + userID + "' as userID,'\"'+xmname+'\"' as xmno,* from xmconnect where jcdw='" + unitName + "' and  " + safeLevel + gclx + gcName;
                    filldb(sql, projectList);

                }
                catch (Exception ex)
                {
                    ExceptionLog.ExceptionWrite(ex.Message);

                }
            }

        }
        public void ProjectListLoad(DataTable dt,Panel projectList)
        {
            DataView dv = new DataView(dt);
            projectList.Controls.Clear();

            foreach (DataRowView drv in dv)
            {
                HyperLink hl = new HyperLink();
                hl.Text = drv["xmname"].ToString();
                hl.NavigateUrl = "javascript:TopPageRedirct('../框架页/project_infor.aspx?xmname=" + drv["xmname"].ToString() + "&xmid=" + drv["id"].ToString() + "')";
                projectList.Controls.Add(hl);
                hl.CssClass = "brClass1";
                _j++;
                HyperLink h2 = new HyperLink();
                h2.Text = "地址:" + drv["xmadress"].ToString();
                h2.NavigateUrl = "javascript:info(" + drv["jd"] + "," + drv["wd"] + ",'" + drv["xmname"].ToString() + "')";
                projectList.Controls.Add(h2);
                h2.CssClass = "brClass2";
                TextBox tb = new TextBox();
                tb.CssClass = "posAddress";
                tb.Text = drv["xmname"].ToString() + "," + drv["jd"].ToString() + "," + drv["wd"].ToString();
                tb.Style.Add("display", "none");
                projectList.Controls.Add(tb);
            }
        }

    }
}