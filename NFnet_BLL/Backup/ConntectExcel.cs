﻿using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;
using NFnet_DAL;
namespace NFnet_BLL
{
    public class ConntectExcel
    {
        private string path;
        private string sheetName;//工作表名，不指定默认为第一个工作表
        /// <summary>
        /// Excel 版本
        /// </summary>
        public enum ExcelType
        {
            Excel2003,
            Excel2007
        }
        /// <summary>
        /// IMEX 三种模式。
        /// IMEX是用来告诉驱动程序使用Excel文件的模式，其值有0、1、2三种，分别代表导出、导入、混合模式。
        /// </summary>
        public enum IMEXType
        {
            ExportMode = 0,
            ImportMode = 1,
            LinkedMode = 2
        } 

        public string Path
        {
            get { return path; }
            set { path = value; }
        }
        public ConntectExcel(string path)
        {
            this.path = path;
        }
        public DataTable ExcelImport(int offset,int rows,DataTable standerDt,TranstInfo tif)
        {
            ExcelType et = GetExcelPulish(this.path);
            string conn = GetExcelConnectstring(this.path,false,et,IMEXType.ImportMode);

            OleDbConnection oleCon = new OleDbConnection(conn);

            oleCon.Open();
            string sql = "select * from ["+this.sheetName+"]";
            string tabsql = "select top 1 * from [" + this.sheetName + "]";
            querysql query = new querysql();
            DataTable dt = new DataTable();
            dt = GetStanderTab.GetAccessTab(sql, offset, rows, oleCon, standerDt, tif);
            return dt;
        }
        public int GetExcelLen()//此处可以指定工作表名
        {
            ExcelType et = GetExcelPulish(this.path);
            string conn = GetExcelConnectstring(this.path, false, et, IMEXType.ImportMode);

            OleDbConnection oleCon = new OleDbConnection(conn);

            oleCon.Open();
            List<string> sheetNameList = GetSheetName(oleCon);
            string sql = "";
            int allCount = 0;
            foreach(string sheetName in sheetNameList)
            {
                DataTable dt = new DataTable();
                
                sql = "select top 1 * from ["+sheetName+"] ";
                OleDbDataAdapter oad = new OleDbDataAdapter(sql,oleCon);
                oad.Fill(dt);
                if (dt.Columns.Count > 1)
                {
                    string sqlCount = "select count(*) from [" + sheetName + "] ";
                    allCount = GetStanderTab.GetAccessTabCount(sqlCount,oleCon);
                    this.sheetName = sheetName;
                    break;
                }
            }
            
            return allCount;
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<string> GetSheetName(OleDbConnection oleCon)
        {
            
            DataTable dt = oleCon.GetSchema("Tables");
            DataView dv = new DataView(dt);
            List<string> tabnameList = new List<string>();
            foreach (DataRowView drv in dv)
            {
                tabnameList.Add(drv["TABLE_NAME"].ToString());
            }
            return tabnameList;
        }
        
        /// <summary>
        /// 获取excel版本
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public ExcelType GetExcelPulish(string path)
        {
            if (path.IndexOf(".xlsx") == -1)
            {
                return ExcelType.Excel2003;
            }
           
                return ExcelType.Excel2007;
            
        }

        /// <summary>
        /// 返回Excel 连接字符串
        /// </summary>
        /// <param name="excelPath">Excel文件 绝对路径</param>
        /// <param name="header">是否把第一行作为列名</param>
        /// <param name="eType">Excel 版本 </param>
        /// <param name="imex">IMEX模式</param>
        /// <returns>返回值</returns>
        public static string GetExcelConnectstring(string excelPath, bool header, ExcelType eType, IMEXType imex)
        {
            string connectstring;

            string hdr = "NO";
            if (header)
                hdr = "YES";

            if (eType == ExcelType.Excel2003)
                connectstring = "Provider=Microsoft.Jet.OleDb.4.0; data source=" + excelPath +
                                ";Extended Properties='Excel 8.0; HDR=" + hdr + "; IMEX=" + imex.GetHashCode() + "'";
            else
                connectstring = "Provider=Microsoft.ACE.OLEDB.12.0; data source=" + excelPath +
                                ";Extended Properties='Excel 12.0 Xml; HDR=" + hdr + "; IMEX=" + imex.GetHashCode() +
                                "'";

            return connectstring;
        }

    }
}