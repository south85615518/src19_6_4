﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using NFnet.项目设置.clss;
using NFnet_DAL;
namespace NFnet_BLL
{
    public class GetPotName
    {
        public static database db = new database();
        private string xmname;

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        private string jclx;

        public string Jclx
        {
            get { return jclx; }
            set { jclx = value; }
        }
        private string yq;

        public string Yq
        {
            get { return yq; }
            set { yq = value; }
        }
        private string cxgroup;

        public string Cxgroup
        {
            get { return cxgroup; }
            set { cxgroup = value; }
        }
        private string projectName;

        public string ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }
        private string task;

        public string Task
        {
            get { return task; }
            set { task = value; }
        }
        public GetPotName(string xmname,string jclx,string yq,string cxgroup,string projectName,string task)
        {
            this.xmname = xmname;
            this.jclx = jclx;
            this.yq = yq;
            this.cxgroup = cxgroup;
            this.projectName = projectName;
            this.task = task;
        }
        /// <summary>
        /// 查询条件语句初始化
        /// </summary>
        /// <returns></returns>
        public string QueryConditionSql()
        {
            string tabName = JclxTabMap.GetPointTabName(this.jclx);
            string sql ="select pointName from "+tabName;
            return sql;
        }
        public List<string> GetPotNames()
        {
            string sql = QueryConditionSql();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            List<string> ls = querysql.querystanderlist(sql,conn);
            return ls;
        }
        

    }
}