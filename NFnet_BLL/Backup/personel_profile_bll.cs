﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;

namespace NFnet_BLL
{
    public class personel_profile_bll : System.Web.UI.Page
    {
        public static int _i = 0;
        public void MemberLoad(GridView GridView1,string unitName)
        {
            member member = new member { UnitName = unitName };
            DataTable dt = member.GetMemberTable();
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        public void GridView_RowCommand_bll(object sender, GridViewCommandEventArgs e,string unitName)
        {
            GridView gv = sender as GridView;
            if (e.CommandName == "Page")
            {
                //首页
                if (e.CommandArgument.ToString() == "First")
                {
                    TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    tb.Text = "0";
                    gv.PageIndex = 0;

                }
                else if (e.CommandArgument.ToString() == "Next")
                {
                    TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = (Int32.Parse(tb.Text) + 1) > gv.PageCount ? gv.PageCount : Int32.Parse(tb.Text) + 1;
                    tb.Text = gv.PageIndex.ToString();

                }
                else if (e.CommandArgument.ToString() == "-2")
                {
                    TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = Int32.Parse(tb.Text) > gv.PageCount ? gv.PageCount : Int32.Parse(tb.Text) < 0 ? 0 : Int32.Parse(tb.Text);
                    tb.Text = gv.PageIndex.ToString();


                }
                else if (e.CommandArgument.ToString() == "Last")
                {
                    TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = gv.PageCount;
                    tb.Text = gv.PageCount.ToString();

                }
                else if (e.CommandArgument.ToString() == "Prev")
                {
                    TextBox tb = (TextBox)gv.BottomPagerRow.FindControl("txtNewPageIndex");
                    gv.PageIndex = (Int32.Parse(tb.Text) - 1) < 0 ? 0 : Int32.Parse(tb.Text) - 1;
                    tb.Text = gv.PageIndex.ToString();

                }
                MemberLoad(gv,unitName);

            }
        }
        /// <summary>
        /// 为表格条件鼠标读数事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void GridView1_RowDataBound_bll(object sender, GridViewRowEventArgs e)
        {
            //int _i = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("id", _i.ToString());
                e.Row.Attributes.Add("onKeyDown", "SelectRow();");
                e.Row.Attributes.Add("onMouseOver", "MarkRow(" + _i.ToString() + ");");
                e.Row.Attributes.Add("onMouseLeave", "Markdel(" + _i.ToString() + ");");
                if (_i % 2 == 0)
                    e.Row.Style.Add("background-color", "#86e791");
                _i++;
            }
        }
    }
}