﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using NFnet_DAL;

namespace NFnet_BLL
{
    public class DataTableCreate
    {
        /// <summary>
        /// 查询水位预警表
        /// </summary>
        public static void TableCreate(OdbcConnection conn)
        {
            string sqlListStr = @"create table   if not EXISTS sensor
(
ID					int(10)  auto_increment  PRIMARY KEY,			
MKH				int(10),		
SJBH				varchar(50),
baseVal         int(10),
dW              char(10)
)		#
create table   if not EXISTS PointNameNotInJclxOption
(
ID					int(10)  auto_increment  PRIMARY KEY,			
pointName			varchar(50),		
jclx                varchar(50),
jcOption            varchar(50)
)
#
create table   if not EXISTS data
(
ID					int				(10)  default 0 ,			
MKH				varchar				(50) ,				
SJBH				varchar		(50) ,				
SWZ				float			(10.4),
WDZ				int				(4),
SJ            datetime,
t               int(10),
PRIMARY key(sjbh,sj)
)		#
			create table   if not EXISTS GPSBaseStation
(
ID							int				(10)	auto_increment  PRIMARY key,			
Smos_Name				varchar		(50)	,			
Smos_Remarks			varchar		(50)	,			
Smos_CoordinateL		float			(20.8),
Smos_CoordinateB		float			(20.8),
Smos_CoordinateH		float			(20.8)

)	  #
			create table   if not EXISTS GPSBaseStationResult_Vector
(
ID							int				(10)	 default 0	,		
GPSBaseName			    varchar		(50)		,		
This_X					double,
This_Y					double,
This_Z					double,
Ac_X					double,
Ac_Y					double,
Ac_Z					double,
This_Py					double,
Ac_Py					double,
Degreen					double,
DateTime			  datetime,
primary key(GPSBaseName,DateTime)						
) #
			create table   if not EXISTS CxGroup
(
ID							int				(10) auto_increment  PRIMARY key,				
Name						varchar		(50),				
Remarks					varchar		(50)				

)#
			create table   if not EXISTS CxPotName
(
ID							int				(10) auto_increment  PRIMARY key,				
GID						int				(10)		,		
Name						varchar		(50)		,	
Remark					varchar		(50)				
			

)#
			create table   if not EXISTS CxResultData
(
ID							int				(10) default 0,			
GID						int				(10)	,
GName						varchar		(50)	,		
Name						varchar		(50)	,			
S							varchar		(50)				,
MonitorTime				datetime,
primary key(Name,MonitorTime)			
			
			

)#
			create table   if not EXISTS YlPotName
(
ID							int				(10) auto_increment  PRIMARY key,				
Name						varchar		(50)	,			
Remark					varchar		(50)				
				
			
			

)#
create table   if not EXISTS YlResultData
(
ID							int				(10)	default 0	,		
RID						int				(10)			,
RName						varchar				(50)	,
Rainfall					int				(10)		,	
MonitorTime				datetime	,
InitRainfall				int				(10),
t               int(10),
primary key(RName,MonitorTime)				
)#
DROP TABLE IF EXISTS studypoint#
CREATE TABLE studypoint (
   id int DEFAULT 0,
  orglHar varchar(30) DEFAULT NULL,
  orglVar varchar(30) DEFAULT NULL,
  orglSd double DEFAULT NULL,
  Team varchar(30) DEFAULT NULL,
  SEARCH_TIME datetime DEFAULT NULL,
	POINTNAME varchar(30) DEFAULT NULL,
  POINT_NAME varchar(30) DEFAULT NULL,
  N double DEFAULT NULL,
  E double DEFAULT NULL,
  Z double DEFAULT NULL,
  TARGET_HEIGHT double DEFAULT NULL,
  RefressType int(11) DEFAULT NULL,
  PrismType int(11) DEFAULT NULL,
  Prism_Constant double DEFAULT NULL,
  ReflectorName varchar(30) DEFAULT NULL,
  REMARK varchar(30) DEFAULT NULL,
  ProjectName varchar(30) DEFAULT NULL,
  StationName varchar(30) DEFAULT NULL,
   comefrom varchar(30) DEFAULT NULL,
   jcoption varchar(30)  DEFAULT NULL,
	sectionName varchar(30) DEFAULT NULL,
	instrument VARCHAR(30)	DEFAULT NULL
)
#
create table   if not EXISTS task
(
ID							int				(10) auto_increment  PRIMARY key,			
taskName					varchar		(50)						
				
)#create table   if not EXISTS YQParamTab
(
ID							int				(10) auto_increment  PRIMARY key,				
jclx        				varchar		(50),		
TranstFileUrl   			varchar		(500),
defaultSet                     varchar(10),
usi                           varchar(10)											
)#create table   if not EXISTS cycangnet(
ID							int				(10) default 0,
CYC                         int             (10),				
POINT_NAME     				varchar		(50),		
This_dN    					double		,
This_dE    					double		,
This_dZ    					double		,
Ac_dN    					double		,
Ac_dE    					double		,
Ac_dZ    					double		,
Avg_N    					double		,
Avg_E    					double		,
Avg_Z    					double		,
taskName                    varchar(50),
Time      				    datetime,
MeasureMethod               varchar(50),
t               int(10)						
)#create table   if not EXISTS LoadLine(
ID							int				(10) auto_increment  PRIMARY key,			
tabName     				varchar		(50)	,		
recorderCont				varchar		(10),
updateRecorders		        varchar		(50),			
sort                        int         (10),
tabCount                    int         (10),   
lastTime   					datetime							
)#create table   if not EXISTS JcMapTab(
ID							int				(10) auto_increment  PRIMARY key,			
JcpmName     				varchar		(50),		
JcpmImgUrl 					varchar		(100),
GroupId                     int         (10),
remark 					    varchar		(500)
)#create table   if not EXISTS JcMapGroup(
ID							int				(10) auto_increment  PRIMARY key,			
GroupName     				varchar		(50)	,		
Remark   					varchar		(50)						
)#create table   if not EXISTS JcMapSet(
ID							int				(10) auto_increment  PRIMARY key,			
JcdName    				varchar		(50)	,		
Jclx   					varchar		(50)	,
Yq      				varchar		(50)	,		
GroupName				varchar		(50)	,
taskName                varchar     (50),		
ProjectName				varchar		(50)	,
JcdMapID   				int		(10)	,		
AbsX						varchar 	(50),				
AbsY						varchar		(50),				
Zoom						double										
)#create table   if not EXISTS YJParamTab(
ID							int				(10) auto_increment  PRIMARY key,				
Bcyj						varchar		(50),				
Bcbj						varchar		(50),				
Bckz						varchar		(50),				
Ljyj						varchar		(50),				
Ljbj						varchar		(50),				
Ljkz						varchar		(50),				
Yjjb						varchar	(50),				
Jclx                        varchar (50),
speed                       varchar(50),
state                      varchar	(50)						
)#create table   if not EXISTS potYjjb(
ID							int				(10) auto_increment  PRIMARY key,				
Jclx						varchar		(50)				,
PotName					varchar		(50)				,
YjjbID						int				(10)										
)#create table   if not EXISTS StyleSheet(
ID							int				(10) auto_increment  PRIMARY key,			
StyleName					varchar		(50)				,
Width   					int						,
Height						int														
)#create table   if not EXISTS swPoint
(
ID					int(10)  auto_increment  PRIMARY KEY,
pointName           varchar(50),			
bdxs				double DEFAULT 0,		
initMoshu				double DEFAULT 0,
deep         double DEFAULT 0,
initVal      double DEFAULT 0,
yq						varchar(50),
jcOption varchar(50)
)#create table   if not EXISTS section
(
ID					int(10)  auto_increment  PRIMARY KEY,
sectionName             varchar(50),
setType					varchar(50),						
pointStart					varchar(50),					
pointEnd					varchar(50),					
X0							varchar(50),						
Y0							varchar(50),					
X1							varchar(50),						
Y1							varchar(50),						
Angle						varchar(50)						
)#create table   if not EXISTS displace
(
ID							int		auto_increment  PRIMARY KEY			,	
pointName						varchar(50)		,			
instrument							varchar(50)		,				
SectionName				varchar(50)		,				
warning					varchar(50),
jcOption				varchar(50)						
)#
create table   if not EXISTS SettlementBase
(
ID							int				auto_increment  PRIMARY KEY		,
basePointName			varchar(50)				,
height						varchar(50)										
)#create table   if not EXISTS Settlement
(
ID							int				auto_increment  PRIMARY KEY		,
PointName			varchar(50)				,
initheight						varchar(50)	,
jcOption						varchar(50)								
)#create table   if not EXISTS Inclinometer
(ID							int				auto_increment  PRIMARY KEY	,
pointName				varchar	(50),
Deep						varchar(50),
A0							varchar(50),				
A180						varchar(50),						
jcOption					varchar(50))
#
create table   if not EXISTS AxialForce
(ID							int				auto_increment  PRIMARY KEY,
pointName				varchar	(50),
SteelDiameter			varchar	(50),
ConcreteModulus		varchar	(50),
SupportingSectionArea varchar(50),
SteelSectionArea		varchar(50),
Steelmodulus			varchar	(50),
CalibrationConstant		varchar	(50),
initFrequency			varchar	(50),
initVal						varchar	(50),
jcOption varchar(50))
#create table   if not EXISTS Rain
(ID							int		auto_increment  PRIMARY KEY,
pointName				varchar(50),
initVal						varchar(50),
jcOption  varchar(50)      )
#
create table   if not EXISTS InstrumentTab
(ID							int				auto_increment  PRIMARY KEY,
InstrumentNo			varchar(50),
InstrumentType			varchar(50),
InstrumentName		varchar(50),
Made						varchar(50)     )
#
create table   if not EXISTS IPC
(ID							int				auto_increment  PRIMARY KEY,
IPCNo			varchar(50),
ConnState       varchar(50),
IPCRemark       varchar(50),
IPCGather       varchar(50)
)
#
create table   if not EXISTS IPCSync
(ID							int				auto_increment  PRIMARY KEY,
SyncName				varchar(50),
ListenPort					varchar(50),
IPCNoStr					varchar(50),
isNo						varchar(50)
    )#
create table   if not EXISTS IPCStudyPoint(
ID							int		default 0	,
E							varchar		(50),
N							varchar		(50),
Z							varchar		(50),
point_name					varchar		(50),
TARGET_HEIGHT			varchar		(50),
ReflectorName			varchar		(50),
orglHar					varchar		(50),
orglVar					varchar		(50),
orglSd						varchar	(50),
StationName				varchar	(50),
Remark					varchar	(50),
IPCID						int	,
DateTime					datetime,
primary key(point_name,ipcid)
)#
create table   if not EXISTS IPCOriginalData(
ID							int					auto_increment  PRIMARY KEY,
CYC						varchar(50),
CH						varchar(50),
POINT_NAME			    varchar(50),
POINT_HAR				varchar(50),
POINT_VAR				varchar(50),
HAR						varchar(50),
VAR						varchar(50),
SD						varchar(50),
HarAcc					varchar(50),
VarAcc					varchar(50),
SdAcc					varchar(50),
CrossIncl				varchar(50),
LenIncl					varchar(50),
DateTime					datetime,
StationMark				varchar(50),
StationName				varchar(50),
TARGET_HEIGHT			varchar(50),
dN						varchar(50),
dE						varchar(50),
dZ						varchar(50),
N 						varchar(50),
E						varchar(50),
Z 						varchar(50),
IPCNO					varchar(50)
)#
create table   if not EXISTS reflector(
reflectorName			varchar(50) primary key ,
reflectType				varchar(50),
prismType				varchar(50),
prismConst				varchar(50),
comefrom    int

)#
create table   if not EXISTS  CYCCalculationMethod(
POINT_NAME			varchar		(50),
Source					varchar	(50),
N							double,							
E							double,						
Z							double,							
Remark					varchar(50),
primary key (POINT_NAME,source)
)
#
create table   if not EXISTS  CYCCalculationBaseSource(
CYC						int	,
baseName				varchar(50))
#
create table   if not EXISTS  POINT_CYC_INIT(
POINT_NAME			varchar(50),
CYC						int	,
N							double,
E							double,
Z							double,
Source					varchar(50),
Remark					varchar(50)
)#
create TABLE  if not EXISTS linkAttr(
userName						varchar		(50),				
PASS				varchar		(50)	,
serverIP				VARCHAR    (50) ,
linkAttrName	VARCHAR(50) PRIMARY key,
PORT   VARCHAR(50),
engine VARCHAR(50),
dbname varchar(50)

)#
create TABLE  if not EXISTS TranstRecord(
ID							int					auto_increment  PRIMARY KEY,
transtCont          int (20),
timeshot	datetime,
isfinished  varchar(50)
)#
create TABLE  if not EXISTS TimeSet(
ID							int					auto_increment  PRIMARY KEY,
startTime          time,
CH	int,
remark  varchar(50),
comefrom varchar(50)
)#
create table  if not EXISTS station(
N double default 0,
E double default 0,
Z double default 0,
height double default 0,
stable  varchar(30),
comefrom varchar(30),
stationName varchar(30)
)#
create table  if not EXISTS Team
(
ID int,
TeamName varchar(50),
ipcno int,
remark varchar(99),
us	varchar(10)
)#
create table  if not EXISTS PointTeam
(
POINTNAME varchar(50),
POINTTYPE varchar(20),
TEAMID varchar(20)
)#
CREATE TABLE  if not EXISTS setting (
  Name varchar(120) DEFAULT NULL,
  remark varchar(120) default NULL, 
  CheckAngle varchar(11) DEFAULT 1,
  ReSearchInterval varchar(11) DEFAULT 10,
  ReSearchTime varchar(11) DEFAULT 3,
  dN varchar(11) DEFAULT 50,
  dE varchar(11) DEFAULT 50,
  dZ varchar(11) DEFAULT 50,
  _2C varchar(11) DEFAULT 20,
  HarReadingDiff varchar(11) DEFAULT 2,
  VarReadingDiff varchar(11) DEFAULT 2,
  SdReadingDiff varchar(11) DEFAULT 1,
  IndexError varchar(11) DEFAULT 40,
  HalfChRegZeroDiff varchar(11) DEFAULT 5,
  BetweenChRegZeroDiff varchar(11) DEFAULT 2,
  BetweenCh2CDiff varchar(11) DEFAULT 9,
  BetweenChIndexErrorDiff varchar(11) DEFAULT 9,
  BetweenChSdDiff varchar(11) DEFAULT 2,
  dSD varchar(11) DEFAULT 60,
  ChaoXianCheck varchar(11) DEFAULT 1,
  ChaoXianTime varchar(11) DEFAULT 3,
  SdTimes varchar(11) DEFAULT 3,
  ChaoXianPause varchar(11) DEFAULT 0,
  CloseInstrument varchar(11) DEFAULT 0,
  AutoPause varchar(11) DEFAULT 0,
  AlwaysCW varchar(11) DEFAULT 0,
  AutoUpdateStudyPoint varchar(11) DEFAULT 0,
  AutoReport varchar(11) DEFAULT 0,
  AutoReportPath varchar(120) DEFAULT null,
  TurningAngleRegis varchar(11) DEFAULT 2,
  PowerOffAllowInterval varchar(11) DEFAULT 30,
  RemoveDBNullRow varchar(10) DEFAULT 1,
  ipcno varchar (10),
  us varchar(10) default '否'
)
#create table strainResultData
(
pointname varchar(20),
initstrengthVal float(4),
strengthVal float(4),
this_val float(4),
ac_val float(4),
monitorTime datetime
)
#
create table  settlementResultData
(
pointname varchar(20),
initelevation float(4),
elevation float(4),
this_val float(4),
ac_val float(4),
monitorTime datetime
)
";

            List<string> sqlList = (List<string>)sqlListStr.Split('#').ToList();
            for (int i = 0; i < sqlList.Count; i++)
            {
                updatedb.ExcuteSql(conn,sqlList[i]);
            }
        }
        /// <summary>
        /// 全站仪数据根据工程名和任务名动态建库
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="projectName"></param>
        /// <param name="taskName"></param>
        public static void TableCreateByProjectAndTask(OdbcConnection conn, string projectName, string taskName)
        {
            string sql = @"create table   if not EXISTS " + projectName + "_" + taskName + "_cycangnet" + @"(
ID							int				(10) auto_increment  PRIMARY key,
CYC                         int             (10),				
POINT_NAME     				varchar		(50)	,		
This_dN    					double		,
This_dE    					double		,
This_dZ    					double		,
Ac_dN    					double		,
Ac_dE    					double		,
Ac_dZ    					double		,
Avg_N    					double		,
Avg_E    					double		,
Avg_Z    					double		,
taskName                    varchar(50),
Time      				    datetime								
)";
            updatedb.ExcuteSql(conn, sql);
        }
    }
}