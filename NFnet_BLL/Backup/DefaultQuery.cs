﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;
using NFnet.项目设置.clss;
using System.Data.Odbc;
using NFnet_DAL;

namespace NFnet_BLL
{
    /// <summary>
    /// 默认查询
    /// </summary>
    public class DefaultQuery
    {
        private string sql, sjcolumn, pointtabname, pointcolumn, datatabname, datapointcolumn, xmname;

        public string Datapointcolumn
        {
            get { return datapointcolumn; }
            set { datapointcolumn = value; }
        }

        public string Pointcolumn
        {
            get { return pointcolumn; }
            set { pointcolumn = value; }
        }
        public static database db = new database();
        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }

        //数据表
        public string Datatabname
        {
            get { return datatabname; }
            set { datatabname = value; }
        }
        //点名表
        public string Pointtabname
        {
            get { return pointtabname; }
            set { pointtabname = value; }
        }
        public string Sjcolumn
        {
            get { return sjcolumn; }
            set { sjcolumn = value; }
        }

        public string Sql
        {
            get { return sql; }
            set { sql = value; }
        }
        public DataTable LastDayQuerySqlCreate()
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            this.sql = "SELECT max(DATE_FORMAT("+this.Sjcolumn+",\"%Y-%m-%d\")) FROM "+datatabname+";";
            string lastDay = querysql.querystanderstr(this.sql,conn);
            string sqlquery = "select * from  "+this.Datatabname+","+this.Pointtabname+" where "+this.Pointtabname
                +"."+this.Pointcolumn+" = "+this.datatabname+"."+this.Datapointcolumn+" and DATE_FORMAT("+this.Sjcolumn+",'%Y-%m-%d') = '"+lastDay+"'";
            DataTable dt = querysql.querystanderdb(sqlquery,conn);
            return dt;
        }
        public List<string> GetPointFromPointNameTab(string jcoption)
        {
            string sql = "";
            if (jcoption != null && jcoption != "")
            {
                sql = "select distinct(" + this.Pointcolumn + ")  from " + this.Pointtabname + " where jcoption = '" + jcoption + "' ";
            }
            else
            {
                sql = "select distinct(" + this.Pointcolumn + ")  from " + this.Pointtabname;
            }
            OdbcConnection conn = db.GetStanderConn(this.Xmname);
            List<string> ls = querysql.querystanderlist(sql,conn);
            return ls;

        }

    }
}