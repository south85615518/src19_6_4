﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data;
using NFnet_DAL;
namespace NFnet_BLL
{
    /// <summary>
    /// 预警检查状态类型
    /// </summary>
    public class YJCheckState
    {
        public static database db = new database();
        private string jcpmPotID;//监测平面的点ID
        private string xmname;//项目名称
        private JcpmMapSet jms;

        public JcpmMapSet Jms
        {
            get { return jms; }
            set { jms = value; }
        }
        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        public string JcpmPotID
        {
            get { return jcpmPotID; }
            set { jcpmPotID = value; }
        }
        private List<string> checkStateVal = new List<string>();//用于检查的值

        public List<string> CheckStateVal
        {
            get { return checkStateVal; }
            set { checkStateVal = value; }
        }
        public YJCheckState(string jcpmPotID, string xmname)
        {
            this.xmname = xmname;
            this.jcpmPotID = jcpmPotID;
        }
        /// <summary>
        /// 获取预警检查状态
        /// </summary>
        /// <returns></returns>
        public YJCheckState GetYJCheckState()
        {
            //由ID获取到监测类型仪器类型
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            string sql = "select * from jcmapset where id=" + this.JcpmPotID;
            JcpmMapSet jms = new JcpmMapSet(this.xmname);
            List<JcpmMapSet> jmslist = jms.GetEntrl(sql);
            this.jms = jmslist[0];
            string sqlCheckState = GetStateSqlByYQ(this.jms);
            //获取到预警检查状态表
            DataTable dt = querysql.querystanderdb(sqlCheckState, conn);
            SetYJCheck(this.jms,dt);
            return this;
        }
        public void SetYJCheck(JcpmMapSet jms, DataTable dt)
        {
            DataView dv = new DataView(dt);
            if (dv.Count > 0)
            {
                switch (jms.Jclx)
                {

                    case "水位":
                        //本次
                        DataRowView drv_s0 = dv[0];
                        DataRowView drv_s1 = dv.Count>1?dv[1]:dv[0];
                        string thisVal = (Int32.Parse(drv_s0["SWZ"].ToString()) - Int32.Parse(drv_s1["SWZ"].ToString())).ToString();
                        //累计
                        drv_s0 = dv[0];
                        drv_s1 = dv.Count > 1 ? dv[dv.Count - 1] : dv[0];
                        string sumVal = (Int32.Parse(drv_s0["SWZ"].ToString()) - Int32.Parse(drv_s1["SWZ"].ToString())).ToString();
                        this.checkStateVal.Add("本次" + "," + thisVal);
                        this.checkStateVal.Add("累计" + "," + sumVal);
                        break;
                    case "雨量":
                        //本次
                        DataRowView drv_y0 = dv[0];
                        DataRowView drv_y1 = dv.Count > 1 ? dv[1] : dv[0];
                        thisVal = (Int32.Parse(drv_y0["Rainfall"].ToString()) - Int32.Parse(drv_y1["Rainfall"].ToString())).ToString();
                        //累计
                        drv_y0 = dv[0];
                        drv_y1 = dv.Count > 1 ? dv[dv.Count - 1] : dv[0];
                        sumVal = (Int32.Parse(drv_y0["InitRainfall"].ToString()) - Int32.Parse(drv_y1["InitRainfall"].ToString())).ToString();
                        this.checkStateVal.Add("本次" + "," + thisVal);
                        this.checkStateVal.Add("累计" + "," + sumVal);
                        break;
                    case "深部位移":
                        //本次
                        DataRowView drv_c0 = dv[0];
                        DataRowView drv_c1 = dv.Count > 1 ? dv[1] : dv[0];
                        thisVal = (double.Parse(drv_c0["s"].ToString()) - double.Parse(drv_c1["s"].ToString())).ToString();
                        //累计
                        drv_c0 = dv[0];
                        drv_c1 = dv.Count > 1 ? dv[dv.Count - 1] : dv[0];
                        sumVal = (double.Parse(drv_c0["s"].ToString()) - double.Parse(drv_c1["s"].ToString())).ToString();
                        this.checkStateVal.Add("本次" + "," + thisVal);
                        this.checkStateVal.Add("累计" + "," + sumVal);
                        break;
                    case "表面位移":
                        //拼凑出监测结果表名*测出要求添加监测点时必须定位到任务
                        //本次
                        string thisValX, thisValY, thisValZ, sumValX, sumValY, sumValZ;
                        DataRowView drv_q0 = dv[0];
                        //DataRowView drv_q1 = dv[1];
                        thisValX = (double.Parse(drv_q0["this_dN"].ToString())).ToString();
                        thisValY = (double.Parse(drv_q0["this_dE"].ToString())).ToString();
                        thisValZ = (double.Parse(drv_q0["this_dZ"].ToString())).ToString();
                        //累计
                        drv_q0 = dv[0];
                        sumValX = (double.Parse(drv_q0["Ac_dN"].ToString())).ToString();
                        sumValY = (double.Parse(drv_q0["Ac_dN"].ToString())).ToString();
                        sumValZ = (double.Parse(drv_q0["Ac_dN"].ToString())).ToString();
                        checkStateVal.Add("本次N" + "," + thisValX);
                        checkStateVal.Add("本次E" + "," + thisValY);
                        checkStateVal.Add("本次Z" + "," + thisValZ);
                        checkStateVal.Add("累计N" + "," + sumValX);
                        checkStateVal.Add("累计E" + "," + sumValY);
                        checkStateVal.Add("累计Z" + "," + sumValZ);
                        break;

                }
            }
            else
            {
                switch (jms.Jclx)
                {

                    case "水位":

                    case "雨量":

                    case "深部位移":
                        this.checkStateVal.Add("本次" + ",未知");
                        this.checkStateVal.Add("累计" + ",未知");
                        break;
                    case "表面位移":
                       
                        checkStateVal.Add("本次N" + ",未知");
                        checkStateVal.Add("本次E" + ",未知");
                        checkStateVal.Add("本次Z" + ",未知");
                        checkStateVal.Add("累计N" + ",未知");
                        checkStateVal.Add("累计E" + ",未知");
                        checkStateVal.Add("累计Z" + ",未知");
                        break;

                }
            }
            
        }
        /// <summary>
        /// 根据监测类型和仪器返回预警检查状态表
        /// </summary>
        /// <param name="jms"></param>
        /// <returns></returns>
        public string GetStateSqlByYQ(JcpmMapSet jms)
        {
            string sql = "";
            switch (jms.Jclx)
            {
                case "水位":
                    sql = "select * from  data  where data.SJBH='" + jms.JcdName + "' order by sj desc";
                    break;
                case "雨量":
                    sql = "select * from  rain, ylresultdata  where pointName ='" + jms.JcdName + "' ORDER BY ylresultdata.MonitorTime desc ";
                    break;
                case "深部位移":
                    sql = "select * from  cxresultdata where Name= '" + jms.JcdName + "' order by cxresultdata.MonitorTime desc ";
                    break;
                case "表面位移":
                    //拼凑出监测结果表名*测出要求添加监测点时必须定位到任务
                    sql = "select * from cycangnet  where POINT_NAME ='" + jms.JcdName + "' ";
                    break;
                default: return null;
            }
            return sql;
        }
    }
}