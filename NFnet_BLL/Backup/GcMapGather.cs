﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class GcMapGather
    {
        private List<JcpmMapSet> jcpmMapSet;

        public List<JcpmMapSet> JcpmMapSet
        {
            get { return jcpmMapSet; }
            set { jcpmMapSet = value; }
        }
        private int sfCont, yjCont, bjCont, kzCont, unknowCont;

        public int SfCont
        {
            get { return sfCont; }
            set { sfCont = value; }
        }

        public int UnknowCont
        {
            get { return unknowCont; }
            set { unknowCont = value; }
        }

        public int KzCont
        {
            get { return kzCont; }
            set { kzCont = value; }
        }

        public int BjCont
        {
            get { return bjCont; }
            set { bjCont = value; }
        }

        public int YjCont
        {
            get { return yjCont; }
            set { yjCont = value; }
        }

    }
}