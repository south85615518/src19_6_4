﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Script.Serialization;
using System.Data.Odbc;
using NFnet_DAL;

namespace NFnet_BLL
{
    public class jcxmAndjcd
    {
        public static database db = new database();
        private string xmname, jcxm;
        List<string> jcd;

        public List<string> Jcd
        {
            get { return jcd; }
            set { jcd = value; }
        }
        public string Jcxm
        {
            get { return jcxm; }
            set { jcxm = value; }
        }

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        //获取监测工程中的监测项目的监测点
        public List<jcxmAndjcd> JcxmAndJcdLoad()
        {
            List<jcxmAndjcd> ls = new List<jcxmAndjcd>();
            //水位
            string sql = "select distinct(pointname) from swpoint ";
            ls.Add(jcxmAndjcdLoad(sql ,"水位"));
            //雨量
            sql = "select distinct(pointname) from rain";
            ls.Add(jcxmAndjcdLoad(sql, "雨量"));
            //沉降
            sql = "select distinct(pointname) from settlement";
            ls.Add(jcxmAndjcdLoad(sql, "沉降"));
            //表面位移
            sql = "select distinct point_name from fmos_studypoint";
            ls.Add(jcxmAndjcdLoad(sql, "表面位移"));
            //深部位移
            sql = "select distinct(pointname) from inclinometer";
            ls.Add(jcxmAndjcdLoad(sql, "深部位移"));
            //应力
            sql = "select distinct(pointname) from axialforce";
            ls.Add(jcxmAndjcdLoad(sql, "应力"));
            return ls;
        }
        public jcxmAndjcd jcxmAndjcdLoad(string sql ,string type)
        {
            Sql_DAL_E dal = new Sql_DAL_E();
            jcxmAndjcd entrl = new jcxmAndjcd {  Xmname = this.Xmname };
            List<string> ls = dal.StudyPointExcute(entrl,sql);
            entrl.Jcxm = type;
            entrl.Jcd = ls;
            return entrl;
        }

    }
}