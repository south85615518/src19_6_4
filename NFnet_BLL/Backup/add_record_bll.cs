﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NFnet_BLL
{
    public class add_record_bll : System.Web.UI.Page
    {

        public void save_Click_bll(member member,Label mess)
        {
            
            string result = member.SaveMemberInfo();
            if (result.IndexOf("由于将在索引") != -1)
            {
                result = "用户名已经存在！";
            }
            else if (result == "success") result = "保存成功！";
            mess.Text = result;
        }
    }
}