﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL;

namespace NFnet_BLL
{
    /// <summary>
    /// 根据项目名称创建数据库
    /// </summary>
    public class dbcreate
    {
        //创建水位监测数据库
        public void createdb(string dbname/*数据库名称*/)
        {
            updatedb udb = new updatedb();
            string sql = @"drop database if Exists  "+dbname+"";
            udb.insertdb(sql);
            sql = @"Create Database If Not Exists "+dbname+" Character Set GBK";
            udb.insertdb(sql);

        }
        /// <summary>
        /// 获取id作为数据库的命名规则
        /// </summary>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public string getnamerules(string xmname,string jclx)
        {
            string dbex = querysql.queryaccessdbstring("select id from xmconnect where xmname='"+xmname+"'");
            switch(jclx)
            {
                case "水位": return "sw_"+dbex;
                default: return "";
            }
        }
        //创建
        //public void create
    }
}