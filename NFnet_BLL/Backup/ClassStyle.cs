﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using NFnet_DAL;
namespace NFnet_BLL
{
    /// <summary>
    /// 保存监测平面上图标的大小
    /// </summary>
    public class ClassStyle
    {
        public static database db = new database();
        private string styleClass;
        private string xmanme;
        public string Xmanme
        {
            get { return xmanme; }
            set { xmanme = value; }
        }
        public string StyleClass
        {
            get { return styleClass; }
            set { styleClass = value; }
        }
        private int width;

        public int Width
        {
            get { return width; }
            set { width = value; }
        }
        private int height;

        public int Height
        {
            get { return height; }
            set { height = value; }
        }
        public ClassStyle()
        {
 
        }
        public ClassStyle(string xmname)
        {
            this.xmanme = xmname;

        }
        public ClassStyle(string xmname,string styleClass,int width, int height)
        {
            this.xmanme = xmname;
            this.styleClass = styleClass;
            this.width = width;
            this.height = height;
 
        }
        /// <summary>
        /// 保存类样式
        /// </summary>
        public void InsertClassStyle()
        {
            
            OdbcConnection conn = db.GetStanderConn(this.xmanme);
            string sql ="insert into StyleSheet(StyleName,width,height)values('"+this.styleClass+"','"+this.width+"','"+this.height+"')";
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql,conn);
        }
        /// <summary>
        /// 更新类样式
        /// </summary>
        public void UpdateClassStyle()
        {

            OdbcConnection conn = db.GetStanderConn(this.xmanme);
            //判断该类型样式是否在数据库中已经存在
            string sqlExist = "select count(*) from StyleSheet  where  StyleName='" + this.styleClass + "'";
            string cont = querysql.querystanderstr(sqlExist,conn);
            if (cont == "0")
            {
                InsertClassStyle();
            }
            else
            {
                string sql = "update StyleSheet set width='" + this.width + "',height='" + this.height + "' where  StyleName='" + this.styleClass + "'";
                updatedb udb = new updatedb();
                udb.UpdateStanderDB(sql, conn);
            }
        }
        /// <summary>
        /// 查询样式
        /// </summary>
        /// <returns></returns>
        public List<ClassStyle> GetClassStyleByEntrl()
        {
            
            OdbcConnection conn = db.GetStanderConn(this.xmanme);
            string sql ="select * from StyleSheet" ;
            DataTable dt = new DataTable();
            dt = querysql.querystanderdb(sql,conn);
            DataView dv = new DataView(dt);
            List<ClassStyle> lcs = new List<ClassStyle>();
            foreach(DataRowView drv in dv)
            {
                ClassStyle cStyle = new ClassStyle();
                //cStyle = this;
                cStyle.width = Int32.Parse(drv["width"].ToString());
                cStyle.height = Int32.Parse(drv["height"].ToString());
                cStyle.styleClass = drv["styleName"].ToString();
                lcs.Add(cStyle);
            }
            return lcs;
            
        }
        
    }
}