﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_MODAL;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.Odbc;
using NFnet_DAL;
using System.Web.SessionState;

namespace NFnet_BLL
{
    public class MultipleChart_ashx_bll : IHttpHandler, IReadOnlySessionState
    {
        public static database db = new database();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {

            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            string txt = context.Request.QueryString["type"];
            if (txt == "script")
            {
                int i = 0;
                List<serie> ls = null;
                bool inRange = false;
                // List<serie> lsSw = Serializestr(context);
                ls = MultiChartCreate(context);
                string result = "";
                //Serialize
                if (ls.Count > 0)
                    result = Serialize(ls, 0, 50);
                context.Response.Write(result);

            }


        }
        public List<serie> Serializestr(HttpContext context)
        {

            DataTable dt = new DataTable();
            DataTable dtt = new DataTable();
            string sql = "";
            string pointname = "";
            zuxyz[] zus = { };
            JavaScriptSerializer jss = new JavaScriptSerializer();
            //用sql语句测试连接是否成功
            if (context.Session["fmossql"] != null && context.Session["fmossql"] != "")
            {
                sql = context.Session["fmossql"].ToString();

            }
            if (context.Session["pointname"] != null && context.Session["pointname"] != "")
            {
                pointname = context.Session["pointname"].ToString();
            }
            if (context.Session["zus"] != null && context.Session["zus"] != "")
            {
                zus = (zuxyz[])context.Session["zus"];
                string rlt = jss.Serialize(zus);
            }
            if (pointname != "" && pointname != null && zus != null)
            {
                //dt = cross(sql);
                //string xmname = context.Session["xmanme"].ToString();
                string xmname = context.Session["xmname"].ToString();
                OdbcConnection conn = db.GetStanderConn(xmname);
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                dt = getfmostb(sql, conn);
                //string[] czlx = { "测量值", "本次变化量", "累计变化量", "平面偏移", "沉降" };
                string[] pointnamezu = pointname.Split(',');
                DataView[] dvzu = new DataView[pointnamezu.Length];
                Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
                for (int k = 0; k < pointnamezu.Length; k++)
                {
                    dvzu[k] = new DataView(dt);
                    ddzu.Add(pointnamezu[k], dvzu[k]);
                }
                List<serie> lst = new List<serie>();
                for (int z = 0; z < zus.Length; z++)
                {

                    for (int d = 0; d < pointnamezu.Length; d++)
                    {
                        string filter = "";
                        dvzu[d].RowFilter = "POINT_NAME= " + pointnamezu[d];
                        int cont = dvzu[d].Count;
                        if (cont != 0)
                        {

                            for (int u = 0; u < zus[z].Bls.Length; u++)
                            {
                                serie st = new serie();//创建曲线
                                st.Stype = zus[z].Name;//曲线类别
                                st.Name = pointnamezu[d] + "_" + rplname(zus[z].Bls[u]);
                                switchnez(dvzu[d], st, zus[z].Bls[u]);
                                lst.Add(st);
                            }

                        }

                    }



                }
                return lst;
                //string result = jss.Serialize(lst);
                //long ln = result.Length;
                //return result;
            }
            return null;
        }


        /// <summary>
        /// 根据数据表生成曲线
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="mkhs"></param>
        /// <param name="sql"></param>
        public List<serie> CreateSeriesFromData(DataTable dt, string pointname, zuxyz[] zus, OdbcConnection conn)
        {
            string[] pointnamezu = pointname.Split(',');
            DataView[] dvzu = new DataView[pointnamezu.Length];
            Dictionary<string, DataView> ddzu = new Dictionary<string, DataView>();
            for (int k = 0; k < pointnamezu.Length; k++)
            {
                dvzu[k] = new DataView(dt);
                ddzu.Add(pointnamezu[k], dvzu[k]);
            }
            List<serie> lst = new List<serie>();
            for (int z = 0; z < zus.Length; z++)
            {

                for (int d = 0; d < pointnamezu.Length; d++)
                {
                    string filter =
                    dvzu[d].RowFilter = "POINT_NAME= '" + pointnamezu[d] + "'";
                    int cont = dvzu[d].Count;
                    if (cont != 0)
                    {

                        for (int u = 0; u < zus[z].Bls.Length; u++)
                        {
                            serie st = new serie();//创建曲线
                            st.Stype = zus[z].Name;//曲线类别
                            st.Name = pointnamezu[d] + "_" + rplname(zus[z].Bls[u]);
                            switchnez(dvzu[d], st, zus[z].Bls[u]);
                            lst.Add(st);
                        }

                    }

                }



            }
            return lst;
        }
        /// <summary>
        /// 获取所有属于监测分项的监测点
        /// </summary>
        /// <param name="jcOption"></param>
        /// <returns></returns>
        public string GetAllPointName(string jcOption, OdbcConnection conn)
        {
            string sql = "select distinct(pointname) from studypoint where jcOption ='" + jcOption + "'";
            List<string> pointname = querysql.querystanderlist(sql, conn);
            return string.Join(",", pointname);
        }

        /// <summary>
        /// 用sql语句查询获得gps数据表
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable getfmostb(string sql, OdbcConnection conn)
        {
            DataTable dt = new DataTable();
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            oda.Fill(dt);
            return dt;
        }
        /// <summary>
        /// 替换不同类别曲线名
        /// </summary>
        public string rplname(string blm)
        {
            if (blm.IndexOf("E") != -1)
            {
                return "E";
            }
            else if (blm.IndexOf("N") != -1)
            {
                return "N";
            }
            else if (blm.IndexOf("Z") != -1)
            {
                return "Z";
            }
            return blm;

        }
        public void switchxyz(DataView dv, serie st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            if (len != 0)
            {

                if (xyzes == "WGS84_X")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "WGS84_X");
                        i++;
                    }
                }
                else if (xyzes == "WGS84_Y")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "WGS84_Y");
                        i++;
                    }
                }
                else if (xyzes == "WGS84_Z")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "WGS84_Z");
                        i++;
                    }
                }
                else if (xyzes == "s_X")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "s_X");
                        i++;
                    }
                }
                else if (xyzes == "s_Y")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "s_Y");
                        i++;
                    }
                }
                else if (xyzes == "s_Z")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "s_Z");
                        i++;
                    }
                }
                else if (xyzes == "l_X")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_X");
                        i++;
                    }
                }
                else if (xyzes == "l_Y")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_Y");
                        i++;
                    }
                }
                else if (xyzes == "l_Z")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "l_Z");
                        i++;
                    }
                }
                else if (xyzes == "bcpy")
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "bcpy");
                        i++;
                    }
                }
                else if (xyzes == "ljpy")
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "ljpy");
                        i++;
                    }
                }

            }

        }
        /// <summary>
        /// 区分出NEZ
        /// </summary>
        /// <param name="dv"></param>
        /// <param name="st"></param>
        /// <param name="xyzes"></param>
        public void switchnez(DataView dv, serie st, string xyzes)
        {
            int len = dv.Count;
            int i = 0;
            if (len != 0)
            {

                if (xyzes == "This_dE")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dE");
                        i++;
                    }
                }
                else if (xyzes == "This_dN")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dN");
                        i++;
                    }
                }
                else if (xyzes == "This_dZ")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "This_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dN")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dN");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dE")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dE");
                        i++;
                    }
                }
                else if (xyzes == "Ac_dZ")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Ac_dZ");
                        i++;
                    }
                }
                else if (xyzes == "Avg_N")//X
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_N");
                        i++;
                    }
                }
                else if (xyzes == "Avg_E")//Y
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_E");
                        i++;
                    }
                }
                else if (xyzes == "Avg_Z")//Z
                {
                    i = 0;
                    st.Pts = new pt[len];
                    foreach (DataRowView drv in dv)
                    {
                        st.Pts[i] = new pt();
                        st.Pts[i] = wgformat(drv, "Avg_Z");
                        i++;
                    }
                }


            }

        }
        public pt wgformat(DataRowView drv, string sxtj/*筛选条件*/)
        {
            /*
             * flag绘制的曲线类型的标志，分为本值变化，累计变化量
             */
            //dateserlize[] dls = new dateserlize[3];
            string dtime = drv["time"].ToString();
            DateTime d = Convert.ToDateTime(dtime);
            int year = d.Year;
            int mon = d.Month;
            int day = d.Day;
            int hour = d.Hour;
            int minute = d.Minute;
            int second = d.Second;
            float val = (float)Convert.ToDouble(drv[sxtj].ToString());
            return new pt { Dt = d, Yvalue1 = (float)(val) };
            //若是要在客户端对数据进行国际化还需要对日期进行调整

        }
        public zuxyz[] SetAllZus()
        {
            List<zuxyz> zusls = new List<zuxyz>();
            string[] bc = { "This_dN", "This_dE", "This_dZ" };
            string[] lj = { "Ac_dN", "Ac_dE", "Ac_dZ" };
            //for (int i = 0; i < XzBlAry.Length; i++)
            //{
            //    if ((XzBlAry[i] == "This_dN" || XzBlAry[i] == "This_dE" || XzBlAry[i] == "This_dZ"))
            //    {
            //        bc.Add(XzBlAry[i]);
            //    }

            //    if ((XzBlAry[i] == "Ac_dN" || XzBlAry[i] == "Ac_dE" || XzBlAry[i] == "Ac_dZ"))
            //    {
            //        lj.Add(XzBlAry[i]);
            //    }
            //    if ((XzBlAry[i] == "Avg_N" || XzBlAry[i] == "Avg_E" || XzBlAry[i] == "Avg_Z"))
            //    {
            //        pc.Add(XzBlAry[i]);
            //    }
            //}

            zuxyz zu = new zuxyz { Bls = bc, Name = "本次变化量" };
            zusls.Add(zu);

            zu = new zuxyz { Bls = lj, Name = "累计变化" };
            zusls.Add(zu);
            return zusls.ToArray<zuxyz>();
        }
        //在同一个曲线表中同时显示多个量多个曲线
        public List<serie> MultiChartCreate(HttpContext context)
        {
            List<serie> ls = new List<serie>();
            //分拣水位曲线图组
            List<serie> lsSW = ChartCreate.SerializestrSW(context);
            List<serie> lsBMWY = ChartCreate.SerializestrBMWY(context);
            List<serie> lsRAIN = ChartCreate.SerializestrRAIN(context);
            List<serie> lsSBWY = ChartCreate.SerializestrSBWY(context);
            List<serie> lsCJ = ChartCreate.SerializestrCJ(context);
            List<serie> lsAXIA = ChartCreate.SerializestrAxia(context);
            //List<serie> ls = new List<serie>();
            if (lsSW != null && lsSW.Count > 0)
                ls.AddRange(lsSW);
            if (lsBMWY != null && lsBMWY.Count > 0)
                ls.AddRange(lsBMWY);
            if (lsRAIN != null && lsRAIN.Count > 0)
                ls.AddRange(lsRAIN);
            if (lsSBWY != null && lsSBWY.Count > 0)
                ls.AddRange(lsSBWY);
            if (lsCJ != null && lsCJ.Count > 0)
                ls.AddRange(lsCJ);
            if (lsAXIA != null && lsAXIA.Count > 0)
                ls.AddRange(lsAXIA);
            //lsSW = lsSW.AddRange(lsBMWY);
            //分拣表面位移曲线图组
            //ChartCreate.GetSeries(ls,);
            context.Session["series"] = ls;
            return ls;

        }
        /// <summary>
        /// 递归序列化曲线
        /// </summary>
        public string Serialize(List<serie> tempserie, int left, int right)
        {
            List<serie> ls = Clone(tempserie);
            if (left + 1 >= right)
            {
                ChartCreate.GetSeries(tempserie, (left + right) / 2);
                return jss.Serialize(tempserie);
            }
            try
            {
                ChartCreate.GetSeries(ls, (left + right) / 2);
                jss.Serialize(ls);
                return Serialize(tempserie, (left + right) / 2, right);
            }
            catch (Exception ex)
            {

                return Serialize(tempserie, left, (left + right) / 2);
            }
            return "";
        }
        //克隆曲线组
        public List<serie> Clone(List<serie> ls)
        {
            List<serie> lsClone = new List<serie>();
            foreach (serie s in ls)
            {
                lsClone.Add(s.Clone());
            }
            return lsClone;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}