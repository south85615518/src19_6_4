﻿using System.Collections.Generic;

namespace NFnet_BLL
{
    /// <summary>
    /// 预警状态类有预警检查状态对比参数设置返回预警状态
    /// </summary>
    public class YJState
    {
        private List<string> yjSateMap = null;

        public List<string> YjSateMap
        {
            get { return yjSateMap; }
            set { yjSateMap = value; }
        }
        private YJCheckState checkState;

        public YJCheckState CheckState
        {
            get { return checkState; }
            set { checkState = value; }
        }
        public YJState(YJCheckState checkState)
        {
            
            this.checkState = checkState;
        }
        public void GetYjSate()
        {
            yj yjState = new yj(this.checkState.Xmname);
            yj yjEntrl = yjState.GetEntrlByJclx(this.checkState.Jms.Jclx, this.checkState.Jms.JcdName);
            this.yjSateMap = yjEntrl.GetYJStateByYJCheckSate(this.checkState);

        }

    }
}