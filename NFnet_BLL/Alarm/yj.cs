﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using System.Data;
using NFnet_DAL;
namespace NFnet_BLL
{
    /// <summary>
    /// 预警实体
    /// </summary>
    public class yj
    {
        public static database db = new database();
        private string speed;

        public string Speed
        {
            get { return speed; }
            set { speed = value; }
        }
        private string id;//预警id
        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        private string jclx;

        public string Jclx
        {
            get { return jclx; }
            set { jclx = value; }
        }
        private string bcyj;//本次预警

        public string Bcyj
        {
            get { return bcyj; }
            set { bcyj = value; }
        }
        private string bcbj;//本次报警

        public string Bcbj
        {
            get { return bcbj; }
            set { bcbj = value; }
        }
        private string bckz;//本次控制

        public string Bckz
        {
            get { return bckz; }
            set { bckz = value; }
        }
        private string ljyj;//累计预警

        public string Ljyj
        {
            get { return ljyj; }
            set { ljyj = value; }
        }
        private string ljbj;//累计报警

        public string Ljbj
        {
            get { return ljbj; }
            set { ljbj = value; }
        }
        private string ljkz;//累计控制

        public string Ljkz
        {
            get { return ljkz; }
            set { ljkz = value; }
        }
        private string yjjb;//预警级别

        public string Yjjb
        {
            get { return yjjb; }
            set { yjjb = value; }
        }
        private string xmname;//项目名称

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        //构造函数用于填充对象
        public yj(string id, string bcyj, string bcbj, string bckz, string ljyj,
           string ljbj, string ljkz, string yjjb, string xmname)
        {
            this.id = id;
            this.bcyj = bcyj;
            this.bcbj = bcbj;
            this.bckz = bckz;
            this.ljyj = ljyj;
            this.ljbj = ljbj;
            this.ljkz = ljkz;
            this.yjjb = yjjb;
            this.xmname = xmname;
        }
        //构造函数用于填充对象
        public yj(string id, string bcyj, string bcbj, string bckz, string ljyj,
           string ljbj, string ljkz, string yjjb, string xmname,string jclx)
        {
            this.id = id;
            this.bcyj = bcyj;
            this.bcbj = bcbj;
            this.bckz = bckz;
            this.ljyj = ljyj;
            this.ljbj = ljbj;
            this.ljkz = ljkz;
            this.yjjb = yjjb;
            this.xmname = xmname;
            this.jclx = jclx;
            
        }
        //构造函数用于填充对象
        public yj(string id, string bcyj, string bcbj, string bckz, string ljyj,
           string ljbj, string ljkz, string yjjb, string xmname, string jclx,string speed)
        {
            this.id = id;
            this.bcyj = bcyj;
            this.bcbj = bcbj;
            this.bckz = bckz;
            this.ljyj = ljyj;
            this.ljbj = ljbj;
            this.ljkz = ljkz;
            this.yjjb = yjjb;
            this.xmname = xmname;
            this.jclx = jclx;
            this.speed = speed;

        }
        //默认构造函数
        public yj()
        {

        }
        //默认构造函数
        public yj(string xmname)
        {
            this.xmname = xmname;
        }
        //默认构造函数
        public yj(string xmname,string jclx)
        {
            this.xmname = xmname;
            this.jclx = jclx;
        }
        public yj GetEntrlByJclx(string jclx,string potName)
        {
            string sql = "select * from potYjjb,yjparamtab where potYjjb.YjjbID = yjparamtab.ID and yjparamtab.jclx ='" + jclx + "' and potYjjb.potName = '" + potName + "' ";
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            DataTable dt = querysql.querystanderdb(sql, conn);
            DataView dv = new DataView(dt);
            if (dv.Count > 0)
            {
                DataRowView drv = dv[0];
                yj yjentrl = new yj(drv["id"].ToString(), drv["bcyj"].ToString(), drv["bcbj"].ToString(),
                    drv["bckz"].ToString(), drv["ljyj"].ToString(), drv["ljbj"].ToString(), drv["ljkz"].ToString(), drv["yjjb"].ToString(), this.xmname);
                return yjentrl;
            }
            else
            {
                yj yjentrl = new yj("-1", "#", "#",
                                   "#", "#", "#", "#", "#", this.xmname);
                return yjentrl;
            }
        }
        public yj GetEntrlById(string id)
        {
            string sql = "select * from YJParamTab where id ="+id;
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            DataTable dt = querysql.querystanderdb(sql, conn);
            DataView dv = new DataView(dt);
            if (dv.Count > 0)
            {
                DataRowView drv = dv[0];
                yj yjentrl = new yj(drv["id"].ToString(), drv["bcyj"].ToString(), drv["bcbj"].ToString(),
                    drv["bckz"].ToString(), drv["ljyj"].ToString(), drv["ljbj"].ToString(), drv["ljkz"].ToString(), drv["yjjb"].ToString(), this.xmname, drv["jclx"].ToString(),drv["speed"].ToString());
                return yjentrl;
            }
            return null;
        }
        public List<yj> GetEntrlListByJclx(string jclx)
        {
            string sql = "select * from YJParamTab where jclx ='" + jclx +"'";
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            DataTable dt = querysql.querystanderdb(sql, conn);
            DataView dv = new DataView(dt);
            List<yj> lyj = new List<yj>();
            foreach(DataRowView drv in dv)
            {
                yj yjentrl = new yj(drv["id"].ToString(), drv["bcyj"].ToString(), drv["bcbj"].ToString(),
                   drv["bckz"].ToString(), drv["ljyj"].ToString(), drv["ljbj"].ToString(), drv["ljkz"].ToString(), drv["yjjb"].ToString(), this.xmname,this.jclx,drv["speed"].ToString());
                lyj.Add(yjentrl);
            }
            return lyj;
        }
        public List<string> GetYJStateByYJCheckSate(YJCheckState checkSate)
        {
            //处理一种特殊情况，并未给监测点关联预警信息
            List<string> dsds = new List<string>();
            //提取checkSate中的键集合
            foreach (string valKeyWord in checkSate.CheckStateVal)
            {

                string[] stateMap = valKeyWord.Split(',');
                string typeAndSate = "";
                if (stateMap[0].IndexOf("本次") != -1)
                {
                    
                    typeAndSate =stateMap[1] == "未知"? "未知" :GetSateByParamTab(double.Parse(stateMap[1]), "本次");
                    
                }
                else if (valKeyWord.IndexOf("累计") != -1)
                {

                    typeAndSate = stateMap[1] == "未知" ? "未知" : GetSateByParamTab(double.Parse(stateMap[1]), "累计");
                    
                }
                dsds.Add(valKeyWord + "," + typeAndSate);
            }
            return dsds;
        }
        public string GetSateByParamTab(double val/*比较值*/, string type/*比较类型本次/累计*/)
        {
            
            switch (type)
            {
                case "本次":
                    if (this.bcyj == "#" || this.bcbj == "#" || this.bckz == "#")
                    {
                        return "未知";
                    }
                    else if (val < double.Parse(this.bcyj))
                    {
                        return "安全";
                    }
                    else if (val > double.Parse(this.bcyj) && val < double.Parse(this.bcbj))
                    {
                        return "预警";
                    }
                    else if (val > double.Parse(this.bcbj) && val < double.Parse(this.bckz))
                    {
                        return "报警";
                    }
                    else
                    {
                        return "危险";
                    }
                   
                case "累计":
                    if (this.ljyj == "#" || this.ljbj == "#" || this.ljkz == "#")
                    {
                        return "未知";
                    }
                    else if (val < double.Parse(this.ljyj))
                    {
                        return "安全";
                    }
                    else if (val > double.Parse(this.ljyj) && val < double.Parse(this.ljbj))
                    {
                        return "预警";
                    }
                    else if (val > double.Parse(this.ljbj) && val < double.Parse(this.ljkz))
                    {
                        return "报警";
                    }
                    else
                    {
                        return "危险";
                    }
                    
            }
            return null;
        }
        public int InsertYJ()
        {
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            //判断是否存在相同的预警名称
            sqlhelper helper = new sqlhelper();
            string[] pointKeyWord = { "yjjb","jclx" };
            string[] pointName = { this.yjjb,this.jclx};
            if (!helper.PointNameExist("YJParamTab",this.xmname,pointKeyWord,pointName))
            {
                string sql = @"insert into YJParamTab (bcyj,bcbj,bckz,ljyj,ljbj,ljkz,yjjb,jclx,speed)
values('" + this.bcyj + "','" + this.bcbj + "','" + this.bckz + "','" + this.ljyj + "','" + this.ljbj + "','" + this.ljkz + "','" + this.yjjb + "','" + this.jclx + "','" + this.speed + "')";
                updatedb udb = new updatedb();
                udb.UpdateStanderDB(sql, conn);
                return 1;
            }
            return 0;
        }
        public void UpdateYJ()
        {
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            string sql = @"update  YJParamTab set
bcyj ='"+this.bcyj+"',bcbj='"+this.bcbj+"',bckz='"+this.bckz+"',ljyj='"+this.ljyj+"',ljbj='"+this.ljbj+"',ljkz='"+this.ljkz+"',yjjb='"+this.yjjb+"',jclx='"+this.jclx+"' where id="+this.id;
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);
            
        }
        public void DelYjById(string idstr)
        {
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            //删除预警参数
            string sql = @"delete from YjParamTab where id in (" + idstr + ")";
            //删除点号预警关联
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);
            sql = @"delete from potyjjb where yjjbID in("+idstr+")";
            udb.UpdateStanderDB(sql, conn);
        }
    }
}