﻿using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data;
using System.Web.Security;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Sql;
using System.IO;
using System.Diagnostics;
using System.Web.Script.Serialization;
using NFnet_DAL;

namespace NFnet_BLL
{
    /// <summary>
    /// 查询各项目名称个监测类型下的各店的值
    /// </summary>
    public class GetNowVal
    {
        protected string xmname;//项目名称
        protected string jclx;//监测类型
        protected string potName;//点号
        public static accessdbse adb = new accessdbse();
        public static int _i = 0;
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static database db = new database();
        /// <summary>
        /// 水位
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="jclx"></param>
        /// <param name="potName"></param>
        /// <returns></returns>
        public string GetSwNowVal(string xmname, string potName, int typeVal)
        {
           
                return null;

        }
        /// <summary>
        /// 雨量
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="jclx"></param>
        /// <param name="potName"></param>
        /// <returns></returns>
        public string GetYlNowVal(string xmname, string potName, int typeVal)
        {
            DataTable dt_2015 = new DataTable();
            DataTable dt_2014 = new DataTable();
            DataTable dt_cross = new DataTable();
            SqlConnection sqlconn_2015 = db.getsqlconnstr("2015");
            string sql = @"select distinct(ID) from MeteorologyRainBase where Name in ('" + potName + "')";
            List<string> ridlist = querysql.queryxmsqllist(sql);
            foreach (string rid in ridlist)
            {
                sql = @"select top 1 MonitorTime,(select SUM(Rainfall) from MeteorologyRaininfo b where b.MonitorTime < a.MonitorTime and b.RID in ('" + rid + "'))" +
"as Rainfall from MeteorologyRaininfo a where a.RID in ('" + rid + "') order by a.MonitorTime desc";
                SqlDataAdapter da = new SqlDataAdapter(sql, sqlconn_2015);
                da.Fill(dt_2015);
                DataView dv = new DataView(dt_2015);
                List<string> ls = new List<string>();
                string sjVal = "";
                foreach (DataRowView drv in dv)
                {
                    ls.Add("累计雨量:" + drv["Rainfall"]);
                    sjVal = drv["MonitorTime"].ToString();
                    break;
                }
                string result = string.Join(" ", ls);

                if (typeVal == 0)
                {

                    return result;
                }
                else if (typeVal == 1)
                {
                    return sjVal;
                }
                else
                    return null;
            }
            return null;

        }
        /// <summary>
        /// 测斜
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="jclx"></param>
        /// <param name="potName"></param>
        /// <returns></returns>
        public string GetCxNowVal(string xmname, string potName, int typeVal)
        {
            DataTable dt_2015 = new DataTable();
            DataTable dt_cross = new DataTable();
            SqlConnection sqlconn_2015 = db.getsqlconnstr("2015");
            string   sql = @"
SELECT top 1 *, round(a.s-round((select  top 1  b.s from McuEverydInclinometerinfo b where 
b.Name = a.Name and b.MonitorTime =( select max(c.MonitorTime) from McuEverydInclinometerinfo c where c.Name = a.Name 
and c.MonitorTime < a.MonitorTime)),4),4) as bcbh,
 round(a.s - round((select b.s from  McuEverydInclinometerinfo b
 where b.Name = a.Name and b.MonitorTime = (select min(c.MonitorTime) from McuEverydInclinometerinfo c where  c.Name=a.Name
  )),4),4) as ljbh
  FROM McuEverydInclinometerinfo a 
            where 1=1 and a.Name in ('"+potName+"') order by MonitorTime desc";
                SqlDataAdapter da = new SqlDataAdapter(sql, sqlconn_2015);
                da.Fill(dt_2015);
                DataView dv = new DataView(dt_2015);
                List<string> ls = new List<string>();
                string sjVal = "";
                foreach (DataRowView drv in dv)
                {
                    ls.Add("模数:" + drv["s"]);
                    sjVal = drv["MonitorTime"].ToString();
                    break;
                }
                string result = string.Join(" ", ls);

                if (typeVal == 0)
                {

                    return result;
                }
                else if (typeVal == 1)
                {
                    return sjVal;
                }
                else
                    return null;
        }
        /// <summary>
        /// GPS
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="jclx"></param>
        /// <param name="potName"></param>
        /// <returns></returns>
        public string GetGPSNowVal(string xmname, string potName, int typeVal)
        {
            DataTable dt_2015 = new DataTable();
            DataTable dt_2014 = new DataTable();
            DataTable dt_cross = new DataTable();
            SqlConnection sqlconn_2015 = db.getsqlconnstr("2015");
            string sql = "select top 1 * from GPSBaseStationResult_Vector where GPSBaseName in ('" + potName + "') order by DateTime desc";
            SqlDataAdapter da = new SqlDataAdapter(sql, sqlconn_2015);
            da.Fill(dt_2015);
            DataView dv = new DataView(dt_2015);
            List<string> ls = new List<string>();
            string sjVal = "";
            foreach (DataRowView drv in dv)
            {
                ls.Add("<table><tr><td>N方向:" + drv["WGS84_X"] + "</td></tr>");
                ls.Add("<tr><td>E方向:" + drv["WGS84_Y"] + "</td></tr>");
                ls.Add("<tr><td>高程:" + drv["WGS84_Z"] + "<td></tr></table>");
                sjVal = drv["DateTime"].ToString();
                break;
            }
            string result = string.Join(" ", ls);

            if (typeVal == 0)
            {

                return result;
            }
            else if (typeVal == 1)
            {
                return sjVal;
            }
            else
                return null;
        }
        /// <summary>
        /// 将两个表拼接成一个表
        /// </summary>
        /// <param name="dt_m"></param>
        /// <param name="dt_w"></param>
        /// <returns></returns>
        public DataTable kisstable(DataTable dt_m, DataTable dt_w)//两个表的字段一模一样
        {
            //获取被衔接字段的长度
            int len = dt_m.Columns.Count;
            DataView dv = new DataView(dt_w);
            foreach (DataRowView drv in dv)
            {
                DataRow dr = dt_m.NewRow();
                for (int i = 0; i < len; i++)
                {
                    dr[i] = drv[i];
                }
                dt_m.Rows.Add(dr);
            }
            return dt_m;
        }
        public string getmkhs(string xmname, string potName)
        {
            //是没选的时候视为全选还是全选才是全选


            string[] dots = { potName };
            int i = 0;
            string vi = dots.ToString();
            string t6str = "";
            for (int l = 0; l < dots.Length; l++)
            {
                dots[l] = "'" + dots[l] + "'";
                if (l < dots.Length - 1)
                {
                    dots[l] += ",";
                }
                t6str += dots[l];
            }
            t6str = "(" + t6str + ")";
            OdbcConnection conn = db.GetStanderConn(xmname);//根据项目名称获取相应的连接
            DataTable dt = new DataTable();
            string sql = "select distinct(mkh) from sensor where sjbh in " + t6str;
            OdbcDataAdapter oda = new OdbcDataAdapter(sql, conn);
            int j = oda.Fill(dt);
            DataView dv = new DataView(dt);
            string mkhs = "";
            foreach (DataRowView drv in dv)
            {

                mkhs += drv["mkh"];
                j--;
                if (j > 0)
                    mkhs += ",";

            }
            return mkhs;

        }
        public string GetVal(string xmname, string jclx, string potName, int typeVal)
        {
            switch (jclx)
            {
                case "水位":
                    return GetSwNowVal(xmname, potName, typeVal);
                case "雨量":
                    return GetYlNowVal(xmname, potName, typeVal);
                case "测斜":
                    return GetCxNowVal(xmname, potName, typeVal);
                case "位移":
                    return GetGPSNowVal(xmname, potName, typeVal);
            }
            return null;
        }

    }
}