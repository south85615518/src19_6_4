﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;

namespace NFnet_BLL.XmInfo.Inclinometer
{
    public class ProcessRoleLayoutBLL
    {
        public ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public bool Processalarmsummary(ProcessalarmsummaryModel model, out string mssg)
        {
            return layoutBLL.Processalarmsummary(model.model,out mssg);
        }
        public class ProcessalarmsummaryModel
        {
            public alarmsummarycondition model { get; set; }
            public Role role { get; set; }
            public ProcessalarmsummaryModel(alarmsummarycondition model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }
        public bool ProcessLayoutHotPots(ProcessLayoutHotPotsModel model, out string mssg)
        {
            return layoutBLL.ProcessLayoutMonitorPointLoadByBaseMapId(model.model, out mssg);
        }
        public class ProcessLayoutHotPotsModel
        {
            public LayoutMonitorPointLoadByBaseMapIdCondition model { get; set; }
            public Role role { get; set; }
            public ProcessLayoutHotPotsModel(LayoutMonitorPointLoadByBaseMapIdCondition model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }

        public bool basemapformat(List<Layout.Model.alarmsummary> llm, out List<basemapformatModel> lsformat)
        {

            lsformat = new List<basemapformatModel>();
            if (llm.Count == 0) return true;
            Layout.Model.alarmsummary tmp = llm[0];
            List<string> lis = new List<string>();
            List<string> pointary = new List<string>();
            foreach (Layout.Model.alarmsummary alarmsumary in llm)
            {
                if (tmp.id != alarmsumary.id)
                {

                    //获取表面位移的所有点的预警信息
                    lis = (from m in llm where (m.type == "表面位移" && m.id == tmp.id) select m.alarmContext).ToList();
                    pointary = (from m in llm where (m.type == "表面位移" && m.id == tmp.id) select m.point_name).ToList();
                    lsformat.Add(new basemapformatModel(tmp.id, tmp.name, tmp.remark, "表面位移", string.Join(",", pointary), string.Join("<p>", lis), lis.Count));
                    tmp = alarmsumary;

                }


            }
            lis = (from m in llm where (m.type == "表面位移" && m.id == tmp.id) select m.alarmContext).ToList();
            pointary = (from m in llm where (m.type == "表面位移" && m.id == tmp.id) select m.point_name).ToList();
            lsformat.Add(new basemapformatModel(tmp.id, tmp.name, tmp.remark, "表面位移", string.Join(",", pointary), string.Join("<p>", lis), lis.Count));
            return true;
        }
        public class basemapformatModel
        {
            public int id { get; set; }
            public string url { get; set; }
            public string remark { get; set; }
            public string context { get; set; }
            public int cont { get; set; }
            public string pointstr { get; set; }
            public string jclx { get; set; }
            public basemapformatModel(int id, string url, string remark, string jclx, string pointstr, string context, int cont)
            {
                this.id = id;
                this.url = url;
                this.remark = remark;
                this.context = context;
                this.cont = cont;
                this.pointstr = pointstr;
                this.jclx = jclx;
            }
        }

    }
}