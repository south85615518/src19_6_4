﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.XmInfo
{
    public class MonitorPointLayoutAlarmPointCont
    {
            public int xmno { get; set; }
            public int basemapid { get; set; }
            public int cont { get; set; }
            public MonitorPointLayoutAlarmPointCont(int xmno, int basemapid)
            {
                this.xmno = xmno;
                this.basemapid = basemapid;
            }
    }
}