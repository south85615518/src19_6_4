﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.XmInfo
{
    public class alarmsummarycondition
    {
        public int xmno { get; set; }
        public List<Layout.Model.alarmsummary> llm { get; set; }
        public alarmsummarycondition(int xmno)
        {
            this.xmno = xmno;
        }
    }
}