﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_BLL.XmInfo.mcuangle
{
    public class ProcessCgLayoutBLL
    {
        public Layout.BLL.basemap basemapBLL = new Layout.BLL.basemap();
        public ProcessCgAlarmSplitOnDateBLL splitOnDateBLL = new ProcessCgAlarmSplitOnDateBLL();
        public Layout.BLL.cgmonitoringpointlayout monitoringpointBLL = new Layout.BLL.cgmonitoringpointlayout();
        public AuthorityAlarm.BLL.cgalarmsplitondate splitondate = new AuthorityAlarm.BLL.cgalarmsplitondate();
        public Layout.BLL.cgalarmsummary alarmsummaryBLL = new Layout.BLL.cgalarmsummary();
        public bool ProcessMonitorPointLayoutColorUpdate(ProcessMonitorPointLayoutColorUpdateModel model, out string mssg)
        {

            
            //获取预警级别
            var processcgalarmgetModel = new ProcessCgAlarmSplitOnDateBLL.ProcesscgalarmgetModel(model.xmno, model.jclx, model.pointname);
            splitOnDateBLL.Processcgalarmget(processcgalarmgetModel,out mssg);
            model.color = processcgalarmgetModel.alarm;
            //splitondate.cgalarmget(model.xmno,model.jclx,model.pointname,out alarm,out mssg);
            return monitoringpointBLL.UpdateCgHotPotColor(model.xmno, model.jclx, model.jcoption, model.pointname, model.color, out mssg);
        }
        public class ProcessMonitorPointLayoutColorUpdateModel
        {
            public int xmno { get; set; }
            public string jclx { get; set; }
            public string jcoption { get; set; }
            public string pointname { get; set; }
            //0,1,2,3
            public int color { get; set; }
            public ProcessMonitorPointLayoutColorUpdateModel(int xmno, string jclx, string jcoption, string pointname, int color)
            {
                this.xmno = xmno;
                this.jclx = jclx;
                this.jcoption = jcoption;
                this.pointname = pointname;
                this.color = color;

            }
        }
        /// <summary>
        /// 根据监测平面的编号获取所有热点信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutMonitorPointLoadByBaseMapId(LayoutMonitorPointLoadByBaseMapIdCondition model, out string mssg)
        {
            List<Layout.Model.monitoringpointlayout> llm = null;
            if (monitoringpointBLL.GetModels(model.xmno, model.jclx, model.basemapid, out llm, out mssg))
            {
                model.llm = llm;
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool Processalarmsummary(alarmsummarycondition model, out string mssg)
        {
            List<Layout.Model.alarmsummary> llm = null;
            if (alarmsummaryBLL.GetModelList(model.xmno, out llm, out mssg))
            {
                model.llm = llm;
                return true;
            }
            return false;


        }
    }
}