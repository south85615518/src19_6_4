﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;
using Tool;

namespace NFnet_BLL.XmInfo
{
    public class ProcessRoleLayoutBLL
    {
        public ProcessLayoutBLL layoutBLL = new ProcessLayoutBLL();
        public bool ProcessLayoutHotPots(LayoutMonitorPointLoadByBaseMapIdCondition model, out string mssg)
        {
            return layoutBLL.ProcessLayoutMonitorPointLoadByBaseMapId(model, out mssg);
        }

        public bool Processalarmsummary(alarmsummarycondition model, out string mssg)
        {
            return layoutBLL.Processalarmsummary(model, out mssg);
        }
     


        public bool basemapformat(List<Layout.Model.alarmsummary> llm, out List<basemapformatModel> lsformatcom, string jclxstr)
        {
            lsformatcom = new List<basemapformatModel>();
            var lsformat = new List<basemapformatModel>();
            if (llm.Count == 0) return true;
            Layout.Model.alarmsummary tmp = llm[0];
            List<string> lis = new List<string>();
            List<string> pointary = new List<string>();
            foreach (Layout.Model.alarmsummary alarmsumary in llm)
            {
                if (tmp.id != alarmsumary.id)
                {

                    //获取表面位移的所有点的预警信息
                    basemapformatcom(llm,out lsformat,tmp,jclxstr);
                    lsformatcom.AddRange(lsformat);
                    tmp = alarmsumary;

                }


            }
            basemapformatcom(llm, out lsformat, tmp, jclxstr);
            lsformatcom.AddRange(lsformat);
            return true;
        }
        public class basemapformatModel
        {
            public int id { get; set; }
            public string url { get; set; }
            public string remark { get; set; }
            public string context { get; set; }
            public int cont { get; set; }
            public string pointstr { get; set; }
            public string jclx { get; set; }
            public basemapformatModel(int id, string url, string remark, string jclx, string pointstr, string context, int cont)
            {
                this.id = id;
                this.url = url;
                this.remark = remark;
                this.context = context;
                this.cont = cont;
                this.pointstr = pointstr;
                this.jclx = jclx;
            }
        }

        public void basemapformatcom(List<Layout.Model.alarmsummary> llm, out  List<basemapformatModel> lsformat, Layout.Model.alarmsummary tmp, string jclxstr)
        {
            lsformat = new List<basemapformatModel>();
            int i = 0;
            var jclxatom = jclxstr.Split(',');
            List<string> alarmContextList = new List<string>();
            List<string> pointNameList = new List<string>();
            for (i = 0; i < jclxatom.Length; i++)
            {
                alarmContextList.AddRange((from m in llm where (m.type == jclxatom[i] && m.id == tmp.id) select ListInsert(m.alarmContext,jclxatom[i]) ).ToList());
                pointNameList.AddRange((from m in llm where (m.type == jclxatom[i] && m.id == tmp.id) select  m.point_name).ToList());
            }
            lsformat.Add(new basemapformatModel(tmp.id, tmp.name, tmp.remark, "", string.Join(",", pointNameList), string.Join("<p>", alarmContextList), alarmContextList.Count));
        }

        public string ListInsert(string src,string jclx)
        {
            try{
            if (string.IsNullOrEmpty(src)) return null;
            List<string> ls  =src.Split(',').ToList();
            ls.Insert(1,jclx);
            string.Join(",", ls);
            return string.Join(",",ls);
            }
            catch(Exception ex)
            {
                ExceptionLog.ExceptionWrite(ex.Message);
                return null;
            }
        }

    }
}