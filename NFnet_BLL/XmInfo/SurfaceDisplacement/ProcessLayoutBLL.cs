﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.IO;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;


namespace NFnet_BLL.XmInfo
{
    /// <summary>
    /// 监测平面图业务逻辑处理类
    /// </summary>
    public class ProcessLayoutBLL
    {
        public Layout.BLL.basemap basemapBLL = new Layout.BLL.basemap();
        public Layout.BLL.monitoringpointlayout monitoringpointBLL = new Layout.BLL.monitoringpointlayout();
        public Layout.BLL.cgmonitoringpointlayout cgmonitoringpointBLL = new Layout.BLL.cgmonitoringpointlayout();
        public ProcessPointAlarmBLL pointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessAlarmBLL alarmBLL = new ProcessAlarmBLL();
        public Layout.BLL.alarmsummary alarmsummaryBLL = new Layout.BLL.alarmsummary();
        
       

        public bool Processalarmsummary(alarmsummarycondition model, out string mssg)
        {
            List<Layout.Model.alarmsummary> llm = null;
            if (alarmsummaryBLL.GetModelList(model.xmno, out llm, out mssg))
            {
                model.llm = llm;
                return true;
            }
            return false;


        }

        public bool ProcessPointCheckValue(ProcessPointCheckValueModel model, out string mssg)
        {

            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(model.xmno, model.cycdirnet.POINT_NAME);

            if (!pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {

                model.result = mssg;
                return false;

            }
            if (processPointAlarmModelGetModel.model == null) return false;
            ProcessAlarmBLL.ProcessAlarmModelGetByNameModel processAlarmModelGetByNameModel = null;
            switch (model.alarm)
            {
                case 0:
                case 1:
                    processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmno, processPointAlarmModelGetModel.model.FirstAlarmName);
                    break;
                case 2:
                    processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmno, processPointAlarmModelGetModel.model.SecondAlarmName);
                    break;
                case 3:
                    processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmno, processPointAlarmModelGetModel.model.ThirdAlarmName);
                    break;
            }
            if (!alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg))
            {
                model.result = mssg;
                return false;
            }
            List<string> ls = new List<string>();
            ls.Add(string.Format("{0},{1}", "表面位移,全站仪", model.cycdirnet.POINT_NAME));
            ls.Add(string.Format("{0},{1},{2}", model.cycdirnet.This_dN, model.cycdirnet.This_dE, model.cycdirnet.This_dZ));
            ls.Add(string.Format("{0},{1},{2}", model.cycdirnet.Ac_dN, model.cycdirnet.Ac_dE, model.cycdirnet.Ac_dZ));
            List<string> lsout = null;
            pointAlarmBLL.ProcessPointAlarmIntoInformation(processAlarmModelGetByNameModel.model, model.cycdirnet, out lsout);
            ls.AddRange(lsout);
            model.result = string.Join(",", ls);

            return true;

        }
        public class ProcessPointCheckValueModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public TotalStation.Model.fmos_obj.cycdirnet cycdirnet { get; set; }
            public string result { get; set; }
            public int alarm { get; set; }
            public ProcessPointCheckValueModel(string xmname, int xmno, int alarm, TotalStation.Model.fmos_obj.cycdirnet cycdirnet)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.cycdirnet = cycdirnet;
                this.alarm = alarm;
            }
        }
        public bool ProcessMonitorPointLayoutColorUpdate(ProcessMonitorPointLayoutColorUpdateModel model, out string mssg)
        {

            return monitoringpointBLL.UpdateHotPotColor(model.xmno, model.jclx, model.jcoption, model.pointname, model.color, out mssg);
        }
        public class ProcessMonitorPointLayoutColorUpdateModel
        {
            public int xmno { get; set; }
            public string jclx { get; set; }
            public string jcoption { get; set; }
            public string pointname { get; set; }
            //0,1,2,3
            public int color { get; set; }
            public ProcessMonitorPointLayoutColorUpdateModel(int xmno, string jclx, string jcoption, string pointname, int color)
            {
                this.xmno = xmno;
                this.jclx = jclx;
                this.jcoption = jcoption;
                this.pointname = pointname;
                this.color = color;

            }
        }
        /// <summary>
        /// 根据监测平面的编号获取所有热点信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutMonitorPointLoadByBaseMapId(LayoutMonitorPointLoadByBaseMapIdCondition model, out string mssg)
        {
            List<Layout.Model.monitoringpointlayout> llm = null;
            if (monitoringpointBLL.GetModels(model.xmno, model.jclx, model.basemapid, out llm, out mssg))
            {
                model.llm = llm;
                return true;
            }
            else
            {
                return false;
            }

        }
       

    }
}