﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.XmInfo
{
    public class ProcessSurfaceDisplacementPointAttributeBLL
    {
        public static PointAttribute.BLL.fmos_pointalarmvalue pointalarmBLL = new PointAttribute.BLL.fmos_pointalarmvalue();
        /// <summary>
        /// 表面位移点加载
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessTotalStationPointLoad(ProcessTotalStationPointLoadModel model, out string mssg)
        {
            List<string> ls = null;
            if (pointalarmBLL.TotalStationPointLoadBLL(model.xmno, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 学习点名获取类
        /// </summary>
        public class ProcessTotalStationPointLoadModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 学习点名列表
            /// </summary>
            public List<string> ls { get; set; }
            public ProcessTotalStationPointLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
    }
}