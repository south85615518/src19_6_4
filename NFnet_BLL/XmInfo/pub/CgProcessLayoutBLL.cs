﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.IO;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;


namespace NFnet_BLL.XmInfo.pub
{
    /// <summary>
    /// 监测平面图业务逻辑处理类
    /// </summary>
    public partial class ProcessLayoutBLL
    {
        
        /// <summary>
        /// 监测平面图添加
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutAdd(ProcessLayoutAddModel model, out string mssg)
        {
            if (basemapBLL.Add(model.model, out mssg) && basemapBLL.CgAdd(model.model, out mssg))
            {
                return true;
            }
            else
            {
                return false;
            }




        }
        public bool ProcessLayoutMaxId(ProcessLayoutMaxIdModel model, out string mssg)
        {

            int maxId = 0;
            if (basemapBLL.BaseMapMaxIdGet(model.xmno, out maxId, out mssg))
            {
                model.maxId = maxId;
                return true;
            }
            return false;
        }
        public class ProcessLayoutMaxIdModel
        {
            public int xmno { get; set; }
            public int maxId { get; set; }
            public ProcessLayoutMaxIdModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        /// <summary>
        /// 监测平面图添加类
        /// </summary>
        public class ProcessLayoutAddModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 监测平面图对象
            /// </summary>
            public Layout.Model.basemap model { get; set; }
            public ProcessLayoutAddModel(string xmname, Layout.Model.basemap model)
            {
                this.xmname = xmname;
                this.model = model;
            }
        }
        /// <summary>
        /// 项目的监测平面图列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutListLoad(ProcessLayoutListLoadModel model, out string mssg)
        {

            List<Layout.Model.basemap> lmb;
            if (basemapBLL.GetModelList(model.xmname, model.id, out lmb, out mssg))
            {
                model.lmb = lmb;
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// 项目的监测平面图列表获取类
        /// </summary>
        public class ProcessLayoutListLoadModel
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 监测平面列表
            /// </summary>
            public List<Layout.Model.basemap> lmb { get; set; }
            public ProcessLayoutListLoadModel(int id, string xmname)
            {
                this.id = id;
                this.xmname = xmname;
            }
        }
        /// <summary>
        /// 判断监测平面图是否已经存在
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutExist(ProcessLayoutModel model, out string mssg)
        {
            return monitoringpointBLL.Exists(model.pointName, model.jclx, model.xmno, out mssg);
        }
        /// <summary>
        /// 判断监测热点是否已经存在类
        /// </summary>
        public class ProcessLayoutModel
        {
            /// <summary>
            /// 点名
            /// </summary>
            public string pointName { get; set; }
            /// <summary>
            /// 监测类型
            /// </summary>
            public string jclx { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }

            public ProcessLayoutModel(string pointName, string jclx, int xmno)
            {
                this.pointName = pointName;
                this.jclx = jclx;
                this.xmno = xmno;
            }
        }

        /// <summary>
        /// 监测平面图上添加监测热点
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutAdd(Layout.Model.monitoringpointlayout model, out string mssg)
        {
            return monitoringpointBLL.Add(model, out mssg) && monitoringpointBLL.Add(model, out mssg);
        }
        /// <summary>
        /// 更新监测平面图上的热点信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutUpdate(Layout.Model.monitoringpointlayout model, out string mssg)
        {
            return monitoringpointBLL.Update(model, out mssg) && monitoringpointBLL.CgUpdate(model, out mssg);
        }
        /// <summary>
        /// 删除监测平面图上的热点
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutDel(ProcessLayoutModel model, out string mssg)
        {
            return monitoringpointBLL.Delete(model.pointName, model.jclx, model.xmno, out mssg) && monitoringpointBLL.CgDelete(model.pointName, model.jclx, model.xmno, out mssg);
        }
        /// <summary>
        /// 获取监测平面图上的所有热点信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutMonitorPointLoad(ProcessLayoutMonitorPointLoadModel model, out string mssg)
        {
            List<Layout.Model.monitoringpointlayout> llm = null;
            if (monitoringpointBLL.GetModelList(model.xmno, model.jclx, out llm, out mssg))
            {
                model.llm = llm;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获取监测平面图上的所有热点信息类
        /// </summary>
        public class ProcessLayoutMonitorPointLoadModel
        {
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 监测类型
            /// </summary>
            public string jclx { get; set; }
            /// <summary>
            /// 监测热点列表
            /// </summary>
            public List<Layout.Model.monitoringpointlayout> llm { get; set; }
            public ProcessLayoutMonitorPointLoadModel(int xmno, string jclx)
            {
                this.xmno = xmno;
                this.jclx = jclx;
            }
        }
        /// <summary>
        /// 根据监测平面的编号获取所有热点信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutMonitorPointLoadByBaseMapId(LayoutMonitorPointLoadByBaseMapIdCondition model, out string mssg)
        {
            List<Layout.Model.monitoringpointlayout> llm = null;
            if (monitoringpointBLL.GetModels(model.xmno, model.jclx, model.basemapid, out llm, out mssg))
            {
                model.llm = llm;
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ProcessFolderUrlGet(ProcessFolderUrlGetModel model, out string mssg)
        {
            Layout.BLL.basemap bll = new Layout.BLL.basemap();
            string folderUrl = "";
            if (bll.FolderUrlGet(model.xmname, model.path, out folderUrl, out mssg))
            {
                model.folderUrl = folderUrl;
                return true;
            }
            return false;
        }

        public class ProcessFolderUrlGetModel
        {
            public string xmname { get; set; }
            public string path { get; set; }
            public string folderUrl { get; set; }
            public ProcessFolderUrlGetModel(string xmname, string path)
            {
                this.xmname = xmname;
                string dirPath = Path.GetDirectoryName(path);
                string fileName = Path.GetFileNameWithoutExtension(path);
                dirPath = dirPath.Replace("\\", "/");
                this.path = string.Format("{0}/ImageSize/{1}_", dirPath, fileName);
            }
        }



        public bool ProcessLayoutBaseMapDelete(ProcessLayoutBaseMapDeleteModel model, out string mssg)
        {
            Layout.BLL.basemap bll = new Layout.BLL.basemap();
            return bll.Delete(model.path, model.xmname, out mssg) && bll.CgDelete(model.path, model.xmname, out mssg);
        }

        public class ProcessLayoutBaseMapDeleteModel
        {
            public string xmname { get; set; }
            public string path { get; set; }
            public ProcessLayoutBaseMapDeleteModel(string xmname, string path)
            {
                this.xmname = xmname;
                //string dirPath = Path.GetDirectoryName(path);
                string fileName = Path.GetFileNameWithoutExtension(path);
                this.path = path;
                //dirPath = dirPath.Replace("\\", "/");
                //this.path = string.Format("{0}/ImageSize/{1}_", dirPath, fileName);
            }
        }

        public bool ProcessBaseMapGet(ProcessBaseMapIdGetModel model, out string mssg)
        {

            Layout.Model.basemap basemapmodel = null;
            if (basemapBLL.BaseMapGet(model.xmno, model.jclx, model.pointname, out basemapmodel, out mssg))
            {
                model.basemapmodel = basemapmodel;
                return true;
            }
            return false;
        }

        public class ProcessBaseMapIdGetModel
        {
            public int xmno { get; set; }
            public string jclx { get; set; }
            public string pointname { get; set; }
            public Layout.Model.basemap basemapmodel { get; set; }
            public ProcessBaseMapIdGetModel(int xmno, string jclx, string pointname)
            {
                this.xmno = xmno;
                this.jclx = jclx;
                this.pointname = pointname;
            }
        }

        /// <summary>
        /// 监测平面图热点删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessLayoutMonitorPointDel(ProcessLayoutMonitorPointDelModel model, out string mssg)
        {
            return monitoringpointBLL.Delete(model.pointName, model.jclx, model.xmno, out mssg) && monitoringpointBLL.CgDelete(model.pointName, model.jclx, model.xmno, out mssg);
        }
        /// <summary>
        /// 监测平面图热点删除类
        /// </summary>
        public class ProcessLayoutMonitorPointDelModel
        {
            public string pointName { get; set; }
            public string jclx { get; set; }
            public int xmno { get; set; }
            public ProcessLayoutMonitorPointDelModel(string pointName, string jclx, int xmno)
            {
                this.pointName = pointName;
                this.jclx = jclx;
                this.xmno = xmno;
            }
        }
        /// <summary>
        /// 监测平面图获取展示
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessBaseMapLoad(ProcessBaseMapLoadModel model, out string mssg)
        {
            Layout.Model.basemap basemap = new Layout.Model.basemap();

            if (basemapBLL.GetModel(model.xmname, model.basemapid, out basemap, out mssg))
            {
                model.basemap = basemap;
                model.path = model.rootpath + "/" + model.xmname + "/ImageSize/" + Path.GetFileName(model.basemap.folderUrl);
                return true;
            }
            return false;





        }
        /// <summary>
        /// 监测平面图获取展示类
        /// </summary>
        public class ProcessBaseMapLoadModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 监测平面图编号
            /// </summary>
            public int basemapid { get; set; }
            /// <summary>
            /// 文件根目录
            /// </summary>
            public string rootpath { get; set; }
            /// <summary>
            /// 全部路径
            /// </summary>
            public string path { get; set; }
            /// <summary>
            /// 监测平面图对象
            /// </summary>
            public Layout.Model.basemap basemap { get; set; }
            public ProcessBaseMapLoadModel(string xmname, int basemapid, string rootpath)
            {
                this.xmname = xmname;
                this.basemapid = basemapid;
                this.rootpath = rootpath;

            }
        }

        public bool ProcessMonitorPointLayoutColorUpdate(ProcessMonitorPointLayoutColorUpdateModel model, out string mssg)
        {

            return monitoringpointBLL.UpdateHotPotColor(model.xmno, model.jclx, model.jcoption, model.pointname, model.color, out mssg);
        }
        public class ProcessMonitorPointLayoutColorUpdateModel
        {
            public int xmno { get; set; }
            public string jclx { get; set; }
            public string jcoption { get; set; }
            public string pointname { get; set; }
            //0,1,2,3
            public int color { get; set; }
            public ProcessMonitorPointLayoutColorUpdateModel(int xmno, string jclx, string jcoption, string pointname, int color)
            {
                this.xmno = xmno;
                this.jclx = jclx;
                this.jcoption = jcoption;
                this.pointname = pointname;
                this.color = color;

            }
        }

        public bool ProcessMonitorPointLayoutAlarmPointCont(RoleMonitorPointLayoutAlarmPointCont model, out string mssg)
        {
            int cont = 0;
           
                if (monitoringpointBLL.BasemapAlarmPointCount(model.model.xmno, model.model.basemapid, out cont, out mssg))
                {
                    model.model.cont = cont;
                    return true;
                }

                return false;
        }

        public class RoleMonitorPointLayoutAlarmPointCont
        {
            public MonitorPointLayoutAlarmPointCont model { get; set; }
            public Role role { get; set; }
            public RoleMonitorPointLayoutAlarmPointCont(MonitorPointLayoutAlarmPointCont model, Role role)
            {
                this.model = model;
                this.role = role;
            }
        }


        //public void TestProcessCycdirnetTimeBLL()
        //{
        //    var processCycdirnetTimeModelGetModel = new ProcessCycdirnetBLL.ProcessCycdirnetTimeModelGetModel(xmname, pointName, dt);
        //    cycdirnetBLL.ProcessCycdirnetModel(processCycdirnetTimeModelGetModel, out mssg);
        //    ProcessPrintMssg.Print(mssg);
        //    resultDataAlarmBLL = new ProcessResultDataAlarmBLL(xmname, Aspect.IndirectValue.GetXmnoFromXmname(xmname), "");
        //    resultDataAlarmBLL.alarmInfoList = new List<string>();
        //    resultDataAlarmBLL.CycdirnetPointAlarm(processCycdirnetTimeModelGetModel.model);
        //    ProcessPrintMssg.Print(string.Join("\n", resultDataAlarmBLL.alarmInfoList));
        //}

      
        public bool ProcessLayoutPointAlarmParamInfo(LayoutPointAlarmParamInfoModel model, out string mssg)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(model.xmno, model.pointname);

            if (!pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                model.ls = new List<alarmnameparam>();
                return true;
            }
            ProcessAlarmBLL.ProcessAlarmModelGetByNameModel processAlarmModelGetByNameModel = null;
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmname, processPointAlarmModelGetModel.model.FirstAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
            model.ls.Add(new alarmnameparam("一级预警", processAlarmModelGetByNameModel.model));
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmname, processPointAlarmModelGetModel.model.SecondAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
            model.ls.Add(new alarmnameparam("二级预警", processAlarmModelGetByNameModel.model));
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmname, processPointAlarmModelGetModel.model.ThirdAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
            model.ls.Add(new alarmnameparam("三级预警", processAlarmModelGetByNameModel.model));
            return true;
        }

        public class alarmnameparam
        {
            public string alarmname { get; set; }
            public NFnet_DAL.MODEL.alarmvalue model { get; set; }
            public alarmnameparam(string alarmname, NFnet_DAL.MODEL.alarmvalue model)
            {
                this.alarmname = alarmname;
                this.model = model;
            }
        }
        public class LayoutPointAlarmParamInfoModel
        {
            public int xmno { get; set; }
            public string xmname { get; set; }
            public string pointname { get; set; }
            public List<alarmnameparam> ls { get; set; }
            public LayoutPointAlarmParamInfoModel(int xmno, string xmname, string pointname)
            {
                this.xmno = xmno;
                this.xmname = xmname;
                this.pointname = pointname;
                this.ls = new List<alarmnameparam>();

            }
        }

        public bool ProcessBasemapJclxList(ProcessBasemapJclxListModel model,out string mssg)
        {
            List<string> jclxlist = new List<string>();
            if(monitoringpointBLL.BasemapJclxList(model.xmno,model.basemapid,out jclxlist,out  mssg))
            {
                model.jclxlist = jclxlist;
                return true;
            }
            return false;
        }
        public class ProcessBasemapJclxListModel
        {
            public int xmno { get; set; }
            public int basemapid{ get; set; }
            public List<string> jclxlist { get; set; }
            public ProcessBasemapJclxListModel(int xmno,int basemapid)
            {
                this.xmno = xmno;
                this.basemapid = basemapid;
            }
        }



    }
}