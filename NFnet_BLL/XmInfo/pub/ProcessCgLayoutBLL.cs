﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.IO;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;


namespace NFnet_BLL.XmInfo.pub
{
    /// <summary>
    /// 监测平面图业务逻辑处理类
    /// </summary>
    public partial class ProcessLayoutBLL
    {
       
        public bool ProcessCgLayoutMaxId(ProcessLayoutMaxIdModel model, out string mssg)
        {

            int maxId = 0;
            if (basemapBLL.CgBaseMapMaxIdGet(model.xmno, out maxId, out mssg))
            {
                model.maxId = maxId;
                return true;
            }
            return false;
        }
       
        /// <summary>
        /// 项目的监测平面图列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessCgLayoutListLoad(ProcessLayoutListLoadModel model, out string mssg)
        {

            List<Layout.Model.basemap> lmb;
            if (basemapBLL.CgGetModelList(model.xmname, model.id, out lmb, out mssg))
            {
                model.lmb = lmb;
                return true;
            }
            else
            {
                return false;
            }

        }
       
        /// <summary>
        /// 判断监测平面图是否已经存在
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessCgLayoutExist(ProcessLayoutModel model, out string mssg)
        {
            return monitoringpointBLL.CgExists(model.pointName, model.jclx, model.xmno, out mssg);
        }
        /// <summary>
        /// 获取监测平面图上的所有热点信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessCgLayoutMonitorPointLoad(ProcessLayoutMonitorPointLoadModel model, out string mssg)
        {
            List<Layout.Model.monitoringpointlayout> llm = null;
            if (monitoringpointBLL.CgGetModelList(model.xmno, model.jclx, out llm, out mssg))
            {
                model.llm = llm;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 根据监测平面的编号获取所有热点信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessCgLayoutMonitorPointLoadByBaseMapId(LayoutMonitorPointLoadByBaseMapIdCondition model, out string mssg)
        {
            List<Layout.Model.monitoringpointlayout> llm = null;
            if (monitoringpointBLL.CgGetModels(model.xmno, model.jclx, model.basemapid, out llm, out mssg))
            {
                model.llm = llm;
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ProcessCgFolderUrlGet(ProcessFolderUrlGetModel model, out string mssg)
        {
            Layout.BLL.basemap bll = new Layout.BLL.basemap();
            string folderUrl = "";
            if (bll.CgFolderUrlGet(model.xmname, model.path, out folderUrl, out mssg))
            {
                model.folderUrl = folderUrl;
                return true;
            }
            return false;
        }
        public bool ProcessCgBaseMapGet(ProcessBaseMapIdGetModel model, out string mssg)
        {

            Layout.Model.basemap basemapmodel = null;
            if (basemapBLL.CgBaseMapGet(model.xmno, model.jclx, model.pointname, out basemapmodel, out mssg))
            {
                model.basemapmodel = basemapmodel;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 监测平面图获取展示
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessCgBaseMapLoad(ProcessBaseMapLoadModel model, out string mssg)
        {
            Layout.Model.basemap basemap = new Layout.Model.basemap();

            if (basemapBLL.CgGetModel(model.xmname, model.basemapid, out basemap, out mssg))
            {
                model.basemap = basemap;
                model.path = model.rootpath + "/" + model.xmname + "/ImageSize/" + Path.GetFileName(model.basemap.folderUrl);
                return true;
            }
            return false;
        }
    

        public bool ProcessCgMonitorPointLayoutColorUpdate(ProcessMonitorPointLayoutColorUpdateModel model, out string mssg)
        {

            return monitoringpointBLL.CgUpdateHotPotColor(model.xmno, model.jclx, model.jcoption, model.pointname, model.color, out mssg);
        }
      

        public bool ProcessCgMonitorPointLayoutAlarmPointCont(RoleMonitorPointLayoutAlarmPointCont model, out string mssg)
        {
            int cont = 0;
           
                if (monitoringpointBLL.CgBasemapAlarmPointCount(model.model.xmno, model.model.basemapid, out cont, out mssg))
                {
                    model.model.cont = cont;
                    return true;
                }

                return false;
        }

        public bool ProcessCgLayoutPointAlarmParamInfo(LayoutPointAlarmParamInfoModel model, out string mssg)
        {
            var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(model.xmno, model.pointname);

            if (!pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {
                model.ls = new List<alarmnameparam>();
                return true;
            }
            ProcessAlarmBLL.ProcessAlarmModelGetByNameModel processAlarmModelGetByNameModel = null;
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmno, processPointAlarmModelGetModel.model.FirstAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
            model.ls.Add(new alarmnameparam("一级预警", processAlarmModelGetByNameModel.model));
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmno, processPointAlarmModelGetModel.model.SecondAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
            model.ls.Add(new alarmnameparam("二级预警", processAlarmModelGetByNameModel.model));
            processAlarmModelGetByNameModel = new ProcessAlarmBLL.ProcessAlarmModelGetByNameModel(model.xmno, processPointAlarmModelGetModel.model.ThirdAlarmName);
            alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
            model.ls.Add(new alarmnameparam("三级预警", processAlarmModelGetByNameModel.model));
            return true;
        }
        public bool ProcessCgBasemapJclxList(ProcessBasemapJclxListModel model,out string mssg)
        {
            List<string> jclxlist = new List<string>();
            if(monitoringpointBLL.CgBasemapJclxList(model.xmno,model.basemapid,out jclxlist,out  mssg))
            {
                model.jclxlist = jclxlist;
                return true;
            }
            return false;
        }

    }
}