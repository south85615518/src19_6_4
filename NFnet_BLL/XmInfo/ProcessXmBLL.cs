﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using NFnet_BLL.AuthorityAlarmProcess;

namespace NFnet_BLL.ProjectInfo
{
    /// <summary>
    /// 项目业务逻辑处理类
    /// </summary>
    public class ProcessXmBLL
    {
        public Authority.BLL.xmconnect xmBLL = new Authority.BLL.xmconnect();

        /// <summary>
        /// 项目信息保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmSave(Authority.Model.xmconnect model, out string mssg)
        {

            return xmBLL.Add(model, out mssg);

        }

        /// <summary>
        /// 项目信息保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXm_d_Add(int xmno, out string mssg)
        {

            return xmBLL.Add_xm_d(xmno, out mssg);

        }


        /// <summary>
        /// 项目删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmDelete(int xmno, out string mssg)
        {

            return xmBLL.Delete(xmno, out mssg);

        }
        /// <summary>
        /// 项目信息更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmUpdate(Authority.Model.xmconnect model, out string mssg)
        {

            return xmBLL.Update(model, out mssg);

        }



        public class ProcessXmSaveModel
        {

        }
        /// <summary>
        /// 判断项目是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmExist(string xmname, out string mssg)
        {
            return xmBLL.Exists(xmname, out mssg);
        }

        public bool ExistsClient_Xmname(ExistsClient_XmnameModel model, out string mssg)
        {
            return xmBLL.ExistsClient_Xmname(model.userId,model.client_xmname,out mssg);
        }
        public class ExistsClient_XmnameModel
        {
            public string userId { get; set; }
            public string client_xmname { get; set; }
            public ExistsClient_XmnameModel(string userId,string client_xmname)
            {
                this.userId = userId;
                this.client_xmname = client_xmname;
            }
        }
        public bool ExistsXmname(ExistsXmnameModel model, out string mssg)
        {
            return xmBLL.ExistsXmname(model.userId,model.xmname,out mssg);
        }
        public class ExistsXmnameModel
        {
             public string userId { get; set; }
            public string xmname { get; set; }
            public ExistsXmnameModel(string userId, string xmname)
            {
                this.userId = userId;
                this.xmname = xmname;
            }
        }

        /// <summary>
        /// 判断项目是否存在
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmExist(ProcessXmExistModel model, out string mssg)
        {
            return xmBLL.Exists(model.xmname, model.unitName, out mssg);
        }
        /// <summary>
        /// 判断项目是否存在类
        /// </summary>
        public class ProcessXmExistModel
        {
            public string xmname { get; set; }
            public string unitName { get; set; }
            public ProcessXmExistModel(string xmname, string unitName)
            {
                this.xmname = xmname;
                this.unitName = unitName;
            }
        }
        /// <summary>
        /// 项目信息获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmInfoLoad(ProcessXmInfoLoadModel model, out string mssg)
        {
            Authority.Model.xmconnect xmmodel = null;
            if (xmBLL.GetModel(model.xmname, out xmmodel, out mssg))
            {
                model.model = xmmodel;
                return true;
            }
            else
            {

                return false;
            }
        }
        /// <summary>
        /// 项目信息获取类
        /// </summary>
        public class ProcessXmInfoLoadModel
        {
            public string xmname { get; set; }
            public Authority.Model.xmconnect model { get; set; }
            public ProcessXmInfoLoadModel(string xmname)
            {
                this.xmname = xmname;
            }
        }


        /// <summary>
        /// 项目信息获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmInfoLoad(ProcessXmIntInfoLoadModel model, out string mssg)
        {
            Authority.Model.xmconnect xmmodel = null;
            if (xmBLL.GetModel(model.xmno, out xmmodel, out mssg))
            {
                model.model = xmmodel;
                return true;
            }
            else
            {

                return false;
            }
        }
        /// <summary>
        /// 项目信息获取类
        /// </summary>
        public class ProcessXmIntInfoLoadModel
        {
            public int xmno { get; set; }
            public Authority.Model.xmconnect model { get; set; }
            public ProcessXmIntInfoLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }


        /// <summary>
        /// 项目的建设单位信息生成
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessUnitCreate(ProcessUnitModel model, out string mssg)
        {
            string createUnitStr = "";
            if (xmBLL.XmUnitCreate(model.xmname, out createUnitStr, out mssg))
            {
                model.creatUnitStr = createUnitStr;
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// 项目的建设单位信息生成类
        /// </summary>
        public class ProcessUnitModel
        {
            public string xmname { get; set; }
            public string creatUnitStr { get; set; }
            public ProcessUnitModel(string xmname)
            {
                this.xmname = xmname;
            }
        }

        public bool ProcessXmList(out List<string> xmlist, out string mssg)
        {
            return xmBLL.GetXmList(out xmlist, out mssg);
        }
        public bool ProcessXmnameUpdate(ProcessXmnameUpdateModel model,out string mssg)
        {
            return xmBLL.XmnameUpdate(model.xmname,model.xmno,out mssg);
        }
        public class ProcessXmnameUpdateModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public ProcessXmnameUpdateModel(string xmname,int xmno)
            {
                this.xmname = xmname;
                this.xmno = xmno;
            }
        }

    }
}