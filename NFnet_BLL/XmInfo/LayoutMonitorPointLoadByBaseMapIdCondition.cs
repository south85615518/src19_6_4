﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.XmInfo
{
    public class LayoutMonitorPointLoadByBaseMapIdCondition
    {
        /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            /// <summary>
            /// 监测类型
            /// </summary>
            public string jclx { get; set; }
            /// <summary>
            /// 监测平面图编号
            /// </summary>
            public int basemapid { get; set; }
            /// <summary>
            /// 监测热点列表
            /// </summary>
            public List<Layout.Model.monitoringpointlayout> llm { get; set; }
            public LayoutMonitorPointLoadByBaseMapIdCondition(int xmno, string jclx, int basemapid)
            {
                this.xmno = xmno;
                this.jclx = jclx;
                this.basemapid = basemapid;
            }
    }
}