﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.IO;
using NFnet_BLL.LoginProcess;
using NFnet_BLL.DisplayDataProcess.WaterLevel;


namespace NFnet_BLL.XmInfo.waterLine
{
    /// <summary>
    /// 监测平面图业务逻辑处理类
    /// </summary>
    public class ProcessLayoutBLL
    {
        public Layout.BLL.basemap basemapBLL = new Layout.BLL.basemap();
        public Layout.BLL.monitoringpointlayout monitoringpointBLL = new Layout.BLL.monitoringpointlayout();
        public Layout.BLL.cgmonitoringpointlayout cgmonitoringpointBLL = new Layout.BLL.cgmonitoringpointlayout();
        public ProcessDTUPointAlarmBLL pointAlarmBLL = new ProcessDTUPointAlarmBLL();
        public ProcessDTUAlarmValueBLL alarmBLL = new ProcessDTUAlarmValueBLL();
        public Layout.BLL.alarmsummary alarmsummaryBLL = new Layout.BLL.alarmsummary();
       

        public bool ProcessPointCheckValue(ProcessPointCheckValueModel model, out string mssg)
        {

            var processPointAlarmModelGetModel = new ProcessDTUPointAlarmBLL.ProcessDTUPointAlarmModel(model.xmno, model.senordata.point_name);

            if (!pointAlarmBLL.ProcessDTUPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
            {

                model.result = mssg;
                return false;

            }
            if (processPointAlarmModelGetModel.model == null) return false;
            ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueModelGetModel processAlarmModelGetByNameModel = null;
            switch (model.alarm)
            {
                case 0:
                case 1:
                    processAlarmModelGetByNameModel = new ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueModelGetModel(model.xmno, processPointAlarmModelGetModel.model.firstAlarm);
                    break;
                case 2:
                    processAlarmModelGetByNameModel = new ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueModelGetModel(model.xmno, processPointAlarmModelGetModel.model.secalarm);
                    break;
                case 3:
                    processAlarmModelGetByNameModel = new ProcessDTUAlarmValueBLL.ProcessDTUAlarmValueModelGetModel(model.xmno, processPointAlarmModelGetModel.model.thirdalarm);
                    break;
            }
            if (!alarmBLL.ProcessDTUAlarmValueModelGet(processAlarmModelGetByNameModel, out mssg))
            {
                model.result = mssg;
                return false;
            }
            List<string> ls = new List<string>();
            ls.Add(string.Format("{0},{1}", "水位,水位计", model.senordata.point_name));
            ls.Add(string.Format("{0},{1}", model.senordata.deep, model.senordata.deep));
            List<string> lsout = null;
            pointAlarmBLL.ProcessPointAlarmIntoInformation(processAlarmModelGetByNameModel.model, model.senordata, out lsout);
            ls.AddRange(lsout);
            model.result = string.Join(",", ls);

            return true;

        }
        public class ProcessPointCheckValueModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public DTU.Model.dtudatareport senordata { get; set; }
            public string result { get; set; }
            public int alarm { get; set; }
            public ProcessPointCheckValueModel(string xmname, int xmno, int alarm, DTU.Model.dtudatareport senordata)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.senordata = senordata;
                this.alarm = alarm;
            }
        }

        //public bool ProcessLayoutPointAlarmParamInfo(LayoutPointAlarmParamInfoModel model, out string mssg)
        //{
        //    var processPointAlarmModelGetModel = new ProcessDTUPointAlarmBLL.ProcessPointAlarmModelGetModel(model.xmno, model.pointname);

        //    if (!pointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg))
        //    {
        //        model.ls = new List<alarmnameparam>();
        //        return true;
        //    }
        //    ProcessDTUAlarmValueBLL.ProcessAlarmModelGetByNameModel processAlarmModelGetByNameModel = null;
        //    processAlarmModelGetByNameModel = new ProcessDTUAlarmValueBLL.ProcessAlarmModelGetByNameModel(model.xmname, processPointAlarmModelGetModel.model.FirstAlarmName);
        //    alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
        //    model.ls.Add(new alarmnameparam("一级预警", processAlarmModelGetByNameModel.model));
        //    processAlarmModelGetByNameModel = new ProcessDTUAlarmValueBLL.ProcessAlarmModelGetByNameModel(model.xmname, processPointAlarmModelGetModel.model.SecondAlarmName);
        //    alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
        //    model.ls.Add(new alarmnameparam("二级预警", processAlarmModelGetByNameModel.model));
        //    processAlarmModelGetByNameModel = new ProcessDTUAlarmValueBLL.ProcessAlarmModelGetByNameModel(model.xmname, processPointAlarmModelGetModel.model.ThirdAlarmName);
        //    alarmBLL.ProcessAlarmModelGetByName(processAlarmModelGetByNameModel, out mssg);
        //    model.ls.Add(new alarmnameparam("三级预警", processAlarmModelGetByNameModel.model));
        //    return true;
        //}

        public class alarmnameparam
        {
            public string alarmname { get; set; }
            public NFnet_DAL.MODEL.alarmvalue model { get; set; }
            public alarmnameparam(string alarmname, NFnet_DAL.MODEL.alarmvalue model)
            {
                this.alarmname = alarmname;
                this.model = model;
            }
        }
        public class LayoutPointAlarmParamInfoModel
        {
            public int xmno { get; set; }
            public string xmname { get; set; }
            public string pointname { get; set; }
            public List<alarmnameparam> ls { get; set; }
            public LayoutPointAlarmParamInfoModel(int xmno, string xmname, string pointname)
            {
                this.xmno = xmno;
                this.xmname = xmname;
                this.pointname = pointname;
                this.ls = new List<alarmnameparam>();

            }
        }

    }
}