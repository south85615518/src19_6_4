﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_DAL;
using System.Data.Odbc;
using System.Data;

namespace NFnet_BLL
{
    public class studypoint
    {
         public static database db = new database();
         private string id, point_name, n, e, z, orglHar, orglVar, orglSd, stationName, search_time, onlyAngleUseful, mileage, iscontrolpoint, targethight, reflectorName, remark, taskName;

         public string TaskName
         {
             get { return taskName; }
             set { taskName = value; }
         }

         public string Remark
         {
             get { return remark; }
             set { remark = value; }
         }

         public string ReflectorName
         {
             get { return reflectorName; }
             set { reflectorName = value; }
         }

         public string Targethight
         {
             get { return targethight; }
             set { targethight = value; }
         }

         public string Iscontrolpoint
         {
             get { return iscontrolpoint; }
             set { iscontrolpoint = value; }
         }

         public string Mileage
         {
             get { return mileage; }
             set { mileage = value; }
         }

         public string OnlyAngleUseful
         {
             get { return onlyAngleUseful; }
             set { onlyAngleUseful = value; }
         }

         public string Search_time
         {
             get { return search_time; }
             set { search_time = value; }
         }

         public string StationName
         {
             get { return stationName; }
             set { stationName = value; }
         }

         public string OrglVar
         {
             get { return orglVar; }
             set { orglVar = value; }
         }

         public string OrglSd
         {
             get { return orglSd; }
             set { orglSd = value; }
         }

         public string OrglHar
         {
             get { return orglHar; }
             set { orglHar = value; }
         }

         public string Z
         {
             get { return z; }
             set { z = value; }
         }

         public string E
         {
             get { return e; }
             set { e = value; }
         }

         public string N
         {
             get { return n; }
             set { n = value; }
         }

         public string Point_name
         {
             get { return point_name; }
             set { point_name = value; }
         }

         public string Id
         {
             get { return id; }
             set { id = value; }
         }
        //学习点添加

       
    }
}