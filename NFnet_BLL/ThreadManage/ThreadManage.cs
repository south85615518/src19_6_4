﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.ThreadManage
{
    public class ThreadProcess
    {
         public static List<string> Threads = new List<string>();
         public static bool ThreadExist(string threadname)
         {
             return Threads.Contains(threadname);
         }
    }
}