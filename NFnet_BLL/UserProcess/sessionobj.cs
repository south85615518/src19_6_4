﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.LoginProcess;

namespace NFnet_BLL.UserProcess
{
    public   class  sessionobj
    {
        public static object obj { get; set; }
        public static string userID { get; set; }
        public static Role role { get; set; }
        static sessionobj()
        {
            obj = null;
            userID = "";
            role = Role.unknown;
        }
    }
}