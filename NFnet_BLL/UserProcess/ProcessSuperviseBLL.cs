﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.UserProcess
{
    /// <summary>
    /// 监督人员业务逻辑处理类
    /// </summary>
    public class ProcessSuperviseBLL
    {
        public static Authority.BLL.MonitorMember memberbll = new Authority.BLL.MonitorMember();
        public static Authority.BLL.xmconnect xmconnectbll = new Authority.BLL.xmconnect();
        /// <summary>
        /// 监督人员登录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg">返回有"数据库查询登录出错","成功"，“失败”</param>
        /// <returns></returns>
        public static bool SuperviseLoginProcess(ProcessSuperviseModel model, out string mssg)
        {
            Authority.Model.MonitorMember monitorMember;
            if (memberbll.Exists(model.username, model.pass, out monitorMember, out mssg))
            {
                model.member = monitorMember;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 监督人员登录类
        /// </summary>
        public class ProcessSuperviseModel
        {
            public string username { get; set; }
            public string pass { get; set; }
            public Authority.Model.MonitorMember member { get; set; }
            public ProcessSuperviseModel(string username, string pass)
            {
                this.username = username;
                this.pass = pass;
            }

        }
        /// <summary>
        /// 监督人员最新项目名称获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool GetSuperviseXmProcess(ProcessXmModel model, out string mssg)
        {
            string topxmname = "";
            if (xmconnectbll.GetTopMonitorMember(model.name, out topxmname, out mssg))
            {

                model.xmname = topxmname;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 监督人员最新项目名称获取类
        /// </summary>
        public class ProcessXmModel
        {
            public string name { get; set; }
            public string xmname { get; set; }
            public ProcessXmModel(string name)
            {
                this.name = name;
            }
        }
        /// <summary>
        /// 监督人员信息添加
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSuperviseAdd(Authority.Model.MonitorMember model, out string mssg)
        {
            mssg = "";
            return memberbll.Add(model, out mssg);


        }
        /// <summary>
        /// 监督人员信息更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSuperviseUpdate(Authority.Model.MonitorMember model, out string mssg)
        {
            mssg = "";
            return memberbll.Update(model, out mssg);


        }
        /// <summary>
        /// 与项目名称相关的监督人员获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmSuperviseLoad(ProcessXmSuperviseLoadModel model, out string mssg)
        {
            string SupervisePeople = "";
            if (memberbll.XmUnitMonitorLoad(model.xmname, out SupervisePeople, out mssg))
            {
                model.SupervisePeople = SupervisePeople;
                return true;
            }
            return false;

        }
        /// <summary>
        /// 与项目名称相关的监督人员获取类
        /// </summary>

        public class ProcessXmSuperviseLoadModel
        {
            public string xmname { get; set; }
            public string SupervisePeople { get; set; }
            public ProcessXmSuperviseLoadModel(string xmname)
            {
                this.xmname = xmname;
            }
        }
        /// <summary>
        /// 判断监督人员是否存在
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSuperviseNameExist(string userName, out string mssg)
        {
            mssg = "";
            if (memberbll.Exists(userName, out mssg))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 根据条件加载监督人员
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSuperviseLoad(ProcessSuperviseLoadModel model, out string mssg)
        {
            mssg = "";
            DataSet ds = null;
            if (memberbll.LoadList(model.userId, out ds, out mssg))
            {
                model.dt = ds.Tables[0];
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 根据条件加载监督人员类
        /// </summary>
        public class ProcessSuperviseLoadModel
        {

            public string userId { get; set; }
            public DataTable dt { get; set; }
            public ProcessSuperviseLoadModel(string userId)
            {
                this.userId = userId;
            }
            public ProcessSuperviseLoadModel()
            {

            }
        }
        /// <summary>
        /// 获取监督人员的项目名列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSuperviseXmnameList(ProcessPeopleXmnameListCondition model, out string mssg)
        {
            List<string> ls = null;
            if (memberbll.GetMonitorNameList(model.userName, out ls, out mssg))
            {
                model.xmnameList = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获取监督人员项目表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessSupercviseXmnameDb(ProcessUserXmDbCondition model, out string mssg)
        {
            DataTable dt = null;
            if (memberbll.GetMonitorXmDb(model.username, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 监督员多条件筛选项目表
        /// </summary>
        /// <param name="model">项目筛选条件</param>
        /// <param name="mssg">执行结果信息</param>
        /// <returns>返回项目结果表</returns>
        public static bool ProcessMutilConditionSuperviseXmTableFilter(ProcessXmFilterCondition model, out string mssg)
        {
            List<Authority.Model.xmconnect> xmconnectlist = null;
            if (memberbll.MutilConditionMonitorXmTableFilter(model.userID, model.gcjb, model.gclx, model.gcname, model.pro, model.city, model.area, out  xmconnectlist, out  mssg))
            {
                model.xmconnectlist = xmconnectlist;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessSuperviseInfoLoad(ProcessSuperviseInfoLoadModel model, out string mssg)
        {
            mssg = "";
            Authority.Model.MonitorMember monitorMember = null;
            if (memberbll.GetModel(model.userNo, out monitorMember, out mssg))
            {
                model.monitorMember = monitorMember;
                return true;
            }
            return false;
        }
        public class ProcessSuperviseInfoLoadModel
        {
            public int userNo { get; set; }
            public Authority.Model.MonitorMember monitorMember { get; set; }
            public ProcessSuperviseInfoLoadModel(int userNo)
            {
                this.userNo = userNo;
            }
        }

        public bool ProcessSuperviseXmUnit(ProcessSuperviseXmUnitModel model, out string mssg)
        {
            string unitName = "";
            if (memberbll.GetMonitorMemberXmUnit(model.userNo, out unitName, out mssg))
            {
                model.unitName = unitName;
                return true;
            }
            return false;
        }
        public class ProcessSuperviseXmUnitModel
        {
            public int userNo { get; set; }
            public string unitName { get; set; }
            public ProcessSuperviseXmUnitModel(int userNo)
            {
                this.userNo = userNo;
            }

        }

        public bool ProcessSupervisesmsBLL(int userno, out string mssg)
        {
            return memberbll.sms(userno, out  mssg);
        }

        public bool ProcessXmSupervisesLoad(ProcessXmSupervisesLoadModel model, out string mssg)
        {
            mssg = "";
            List<Authority.Model.MonitorMember> ls = new List<Authority.Model.MonitorMember>();
            if (memberbll.GetModelList(model.xmno, out ls, out mssg))
            {
                model.ls = ls;
                return true;
            }
            return false;

        }
        public class ProcessXmSupervisesLoadModel
        {
            public int xmno { get; set; }
            public List<Authority.Model.MonitorMember> ls { get; set; }
            public ProcessXmSupervisesLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

    }
}