﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.UserProcess
{
    /// <summary>
    /// 项目多条件查询条件类
    /// </summary>
    public class ProcessXmFilterCondition
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string userID { get; set; } 
        /// <summary>
        /// 工程级别
        /// </summary>
        public string gcjb { get; set; }
        /// <summary>
        /// 工程类型
        /// </summary>
        public string gclx { get; set; }
        /// <summary>
        /// 工程名
        /// </summary>
        public string gcname { get; set; } 
        /// <summary>
        /// 所在省
        /// </summary>
        public string pro { get; set; } 
        /// <summary>
        /// 所在市
        /// </summary>
        public string city { get; set; } 
        /// <summary>
        /// 所在区域
        /// </summary>
        public string area { get; set; }
        /// <summary>
        /// 查询结果表
        /// </summary>
        public List<Authority.Model.xmconnect> xmconnectlist { get; set; }
        public ProcessXmFilterCondition(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area)
        {
            this.userID = userID;
            this.gcjb = gcjb;
            this.gclx = gclx;
            this.gcname = gcname;
            this.pro = pro;
            this.city = city;
            this.area = area;

        }
    }
}