﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_DAL.MODEL;
using NFnet_DAL.BLL;


namespace NFnet_BLL.UserProcess
{
    /// <summary>
    /// 超管业务逻辑处理类
    /// </summary>
    public class ProcessSuperAdministratorBLL
    {
        private readonly NFnet_DAL.BLL.UnitBLL bll = new NFnet_DAL.BLL.UnitBLL();
        public static NFnet_DAL.BLL.UnitBLL unitBLL = new NFnet_DAL.BLL.UnitBLL();
        public static NFnet_DAL.BLL.administratorBLL administratorBLL = new NFnet_DAL.BLL.administratorBLL();
        Authority.BLL.unit unitbll = new Authority.BLL.unit();
        /// <summary>
        /// 获取所有机构列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessUnitLoad(out DataTable dt, out string mssg)
        {
            NFnet_DAL.MODEL.Unit unit = new NFnet_DAL.MODEL.Unit { UnitName = "" };
            dt = new DataTable();
            mssg = "";

            if (bll.UnitTableExcute(unit, out dt, out mssg))
            {

                return true;
            }
            else
            {

                return false;
            }
        }



        /// <summary>
        /// 数据库名称列表获取
        /// </summary>
        /// <param name="ls"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessDbListLoad(out List<string> ls, out string mssg)
        {
            return unitBLL.DbListExcute(out ls, out mssg);

        }
        /// <summary>
        /// 单位数据库建立
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessUnitCreate(ProcessUnitCreateModel model, out string mssg)
        {
            return (unitBLL.UnitDbCreate(model.unit.DbName, model.path, out mssg)&&(unitBLL.UnitDbCreate(model.unit.CgdbName, model.cgpath, out mssg)));


        }
        
        /// <summary>
        /// 单位名称是否存在
        /// </summary>
        /// <param name="unitName"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessUnitNameExist(string unitName, out string mssg)
        {
            return unitBLL.UnitNameExist(unitName, out mssg);
        }
        /// <summary>
        /// 机构创建
        /// </summary>
        public class ProcessUnitCreateModel
        {
            public NFnet_DAL.MODEL.Unit unit { get; set; }
            public string path { get; set; }
            public string cgpath { get; set; }
            public ProcessUnitCreateModel(NFnet_DAL.MODEL.Unit unit, string path,string cgpath)
            {
                this.unit = unit;
                this.path = path;
                this.cgpath = cgpath;
            }

        }
        /// <summary>
        /// 机构信息保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessUnitSave(NFnet_DAL.MODEL.Unit model, out string mssg)
        {
            return unitBLL.SaveUnitInfo(model, out mssg);

        }

        /// <summary>
        /// 机构信息更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public  bool ProcessUnitUpdate(NFnet_DAL.MODEL.Unit model, out string mssg)
        {
            return unitBLL.UpdateUnitInfo(model, out mssg);

        }
        /// <summary>
        /// 管理员信息添加
        /// </summary>
        /// <param name="member"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAdministratorAdd(member member, out string mssg)
        {
            return administratorBLL.SaveMemberInfo(member, out mssg);
        }
        /// <summary>
        /// 管理员信息更新
        /// </summary>
        /// <param name="member"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAdministratorUpdate(member member, out string mssg)
        {
            return administratorBLL.UpdateMemberInfo(member, out mssg);

        }

        /// <summary>
        /// 机构权限冻结
        /// </summary>
        /// <param name="unitName"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFreezeLogin(string unitName, out string mssg)
        {

            return unitbll.Delete(unitName, out mssg);

        }
        /// <summary>
        /// 机构权限激活
        /// </summary>
        /// <param name="unitName"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessActiveLogin(string unitName, out string mssg)
        {

            return unitbll.Active(unitName, out mssg);

        }
        public bool ProcessUnitInfoLoad(string unitName, out Unit unit, out string mssg)
        {
            unit = null;
            return unitBLL.GetUnit(unitName, out unit, out mssg);
        }
    }
}