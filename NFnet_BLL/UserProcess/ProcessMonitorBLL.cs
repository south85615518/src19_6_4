﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.UserProcess
{
    /// <summary>
    /// 监测员业务逻辑处理类
    /// </summary>
    public class ProcessMonitorBLL
    {
        public static Authority.BLL.UnitMember memberbll = new Authority.BLL.UnitMember();
        public static Authority.BLL.xmconnect xmconnectbll = new Authority.BLL.xmconnect();
        /// <summary>
        /// 管理员和超级管理员登录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg">返回有"数据库查询登录出错","成功"，“失败”</param>
        /// <returns></returns>
        public static bool MonitorLoginProcess(ProcessMonitorModel model, out string mssg)
        {
            Authority.Model.UnitMember unitmember;
            if (memberbll.Exists(model.username, model.pass, out unitmember, out mssg))
            {
                model.member = unitmember;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 监测员登录处理类
        /// </summary>
        public class ProcessMonitorModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string username { get; set; }
            /// <summary>
            /// 密码
            /// </summary>
            public string pass { get; set; }
            /// <summary>
            /// 监测员对象
            /// </summary>
            public Authority.Model.UnitMember member { get; set; }
            public ProcessMonitorModel(string username, string pass)
            {
                this.username = username;
                this.pass = pass;
            }

        }
        /// <summary>
        /// 获取监测员最新项目名称
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool GetMonitorXmProcess(ProcessXmModel model, out string mssg)
        {
            string topxmname = "";
            if (xmconnectbll.GetTopUnitMember(model.name, out topxmname, out mssg))
            {

                model.xmname = topxmname;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 获取项目类
        /// </summary>
        public class ProcessXmModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 项目名
            /// </summary>
            public string xmname { get; set; }
            public ProcessXmModel(string name)
            {
                this.name = name;
            }
        }


        /// <summary>
        /// 监测员信息添加
        /// </summary>
        /// <param name="member"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMonitorInfoSave(Authority.Model.UnitMember member, out string mssg)
        {
            var bll = new Authority.BLL.UnitMember();
            return bll.Add(member, out mssg);

        }
        /// <summary>
        /// 监测员项目获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmMonitorLoad(ProcessXmMonitorLoadModel model, out string mssg)
        {
            string monitorPeople = "";
            if (memberbll.XmUnitMemberLoad(model.xmname, out monitorPeople, out mssg))
            {
                model.monitorPeople = monitorPeople;
                return true;
            }
            return false;

        }

        /// <summary>
        /// 监测员项目获取类
        /// </summary>
        public class ProcessXmMonitorLoadModel
        {
            /// <summary>
            /// 项目名称
            /// </summary>
            public string xmname { get; set; }
            /// <summary>
            /// 监测人员
            /// </summary>
            public string monitorPeople { get; set; }
            public ProcessXmMonitorLoadModel(string xmname)
            {
                this.xmname = xmname;
            }
        }
        public bool ProcessMonitorInfoLoad(ProcessMonitorInfoLoadModel model,out string mssg)
        {
            Authority.Model.UnitMember member = new Authority.Model.UnitMember();
            if(memberbll.GetModel(model.memberno,out member,out mssg))
            {
                model.model = member;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ProcessMonitorInfoLoadByUserID(ProcessMonitorInfoLoadModel model, out string mssg)
        {
            Authority.Model.UnitMember member = new Authority.Model.UnitMember();
            if (memberbll.GetModel(model.userID, out member, out mssg))
            {
                model.model = member;
                return true;
            }
            else
            {
                return false;
            }
        }

        public class ProcessMonitorInfoLoadModel
        {
            public int memberno { get; set; }
            public string userID { get; set; }
            public Authority.Model.UnitMember model {get;set; }
            public ProcessMonitorInfoLoadModel(int memberno)
            {
                this.memberno = memberno;
            }
            public ProcessMonitorInfoLoadModel(string userID)
            {
                this.userID = userID;
            }
        }


        public bool ProcessMonitorDelete(int memberno,out string mssg)
        {
            return memberbll.Delete(memberno,out mssg);
        }



        /// <summary>
        /// 判断监测员的用户名是否已经存在
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMonitorNameExist(string userName, out string mssg)
        {
            mssg = "";
            if (memberbll.Exists(userName, out mssg))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 监测员信息更新
        /// </summary>
        /// <param name="member"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMonitorUpdate(Authority.Model.UnitMember member, out string mssg)
        {
            return memberbll.Update(member, out mssg);


        }
        /// <summary>
        /// 监测员列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMonitorLoad(ProcessMonitorLoadModelByUserId model, out string mssg)
        {
            mssg = "";
            DataSet ds = null;
            if (memberbll.LoadList(model.userId, out ds, out mssg))
            {
                model.ds = ds;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 监测员列表获取类
        /// </summary>
        public class ProcessMonitorLoadModel
        {

            public string strWhere { get; set; }
            public DataTable dt { get; set; }
            public ProcessMonitorLoadModel(string strWhere)
            {
                this.strWhere = strWhere;
            }

        }
        /// <summary>
        /// 获取管理员的监测员表
        /// </summary>
        public class ProcessMonitorLoadModelByUserId
        {

            public string userId { get; set; }
            public DataSet ds { get; set; }
            public ProcessMonitorLoadModelByUserId(string userId)
            {
                this.userId = userId;
            }

        }
        /// <summary>
        /// 获取监测员的项目名称列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMonitorXmnameList(ProcessPeopleXmnameListCondition model, out string mssg)
        {
            List<string> ls = null;
            if (memberbll.GetUnitMemberNameList(model.userName, out ls, out mssg))
            {
                model.xmnameList = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获取监测员的项目表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessMonitorXmnameDb(ProcessUserXmDbCondition model, out string mssg)
        {
            DataTable dt = null;
            if (memberbll.GetUnitMemberXmDb(model.username, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 管理员多条件筛选项目表
        /// </summary>
        /// <param name="model">项目筛选条件</param>
        /// <param name="mssg">执行结果信息</param>
        /// <returns>返回项目结果表</returns>
        public static bool ProcessMutilConditionMonitorXmTableFilter(ProcessXmFilterCondition model, out string mssg)
        {
            List<Authority.Model.xmconnect> xmconnectlist = null;
            if (memberbll.MutilConditionUnitMemberXmTableFilter(model.userID, model.gcjb, model.gclx, model.gcname, model.pro, model.city, model.area, out  xmconnectlist, out  mssg))
            {
                model.xmconnectlist = xmconnectlist;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获取项目的监测员列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXmMonitorList(ProcessXmMonitorListModel model,out string mssg)
        {
            List<Authority.Model.UnitMember> lum = null;
            if (memberbll.XmMonitorListGet(model.xmno,out lum,out mssg))
            {
                model.lum = lum;
                return true;
            }
            return false;
        }
        public class ProcessXmMonitorListModel
        {
            public int xmno { set; get; }
            public List<Authority.Model.UnitMember> lum { get; set; }
            public ProcessXmMonitorListModel(int xmno)
            {
                this.xmno = xmno;
            }
        }

        public bool ProcessMonitorsmsBLL(int memberno, out string mssg)
        {
            return memberbll.sms(memberno, out  mssg);
        }

        public bool ProcessMonitorSurveyBaseExchange(ProcessMonitorSurveyBaseExchangeModel model, out string mssg)
        {
            return memberbll.SurveyBaseExchange(model.userId, model.surveybase, out mssg);
        }
        public class ProcessMonitorSurveyBaseExchangeModel
        {
            public string userId { get; set; }
            public bool surveybase { get; set; }
            public ProcessMonitorSurveyBaseExchangeModel(string userId, bool surveybase)
            {
                this.userId = userId;
                this.surveybase = !surveybase;
            }
        }
    }
}