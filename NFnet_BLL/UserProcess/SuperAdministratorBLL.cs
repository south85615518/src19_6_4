﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.UserProcess
{
    public class SuperAdministratorBLL
    {
        public static Authority.BLL.xmconnect xmconnectbll = new Authority.BLL.xmconnect();
        public static string GetSuperAdminstratorXmProcess(ProcessXmModel model, out string mssg)
        {
            string topxmname = "";
            if (xmconnectbll.GetTopMember(model.name, out topxmname, out mssg))
            {

                return topxmname;
            }
            else
            {

                return "";
            }

        }
        public class ProcessXmModel
        {
            public string name { get; set; }
            public string xmname { get; set; }
            public ProcessXmModel(string name)
            {
                this.name = name;
            }
        }
    }
}