﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ;

namespace NFnet_BLL.UserProcess
{
    /// <summary>
    /// 获取登录人员的项目列表类
    /// </summary>
    public  class ProcessPeopleXmnameListCondition
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string userName { get; set; }
        /// <summary>
        /// 项目名称列表
        /// </summary>
        public List<string> xmnameList { get; set; }
        public ProcessPeopleXmnameListCondition(string userName)
        {
            this.userName = userName;
        }
       

         //根据设备编号获取到项目编号
        public static bool  ProcessGetXmnoBySno(string sno, out string mssg, out string xmno)
        {
             Authority.BLL.VibrationDeviceMap bll = new Authority.BLL.VibrationDeviceMap();
            if(bll.GetXmnoBySno(sno, out xmno, out mssg))
                return true;
            else
                return false;
                
        }
    }
}