﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using NFnet_DAL.BLL;
using System.Data;
using Authority.Model;


namespace NFnet_BLL.UserProcess
{
    /// <summary>
    /// 管理员业务逻辑处理类
    /// </summary>
    public class ProcessAdministratrorBLL
    {
        public static Authority.BLL.member memberbll = new Authority.BLL.member();
        public static Authority.BLL.xmconnect xmconnectbll = new Authority.BLL.xmconnect();
        public static administratorBLL adminBLL = new administratorBLL();
        public static Authority.BLL.UnitMember unitMemberBLL = new Authority.BLL.UnitMember();
        public static Authority.BLL.Xm_Member xm_MemberBLL = new Authority.BLL.Xm_Member();
        public static Authority.BLL.Xm_Monitor xm_MonitorBLL = new Authority.BLL.Xm_Monitor();
        /// <summary>
        /// 管理员和超级管理员登录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg">返回有"数据库查询登录出错","成功"，“失败”</param>
        /// <returns></returns>
        public static bool AdministratrorLoginProcess(ProcessAdministratorModel model, out string mssg)
        {
            Authority.Model.member member;
            if (memberbll.Exists(model.username, model.pass, out member, out mssg))
            {
                model.member = member;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 管理员登陆处理类
        /// </summary>
        public class ProcessAdministratorModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string username { get; set; }
            /// <summary>
            /// 密码
            /// </summary>
            public string pass { get; set; }
            /// <summary>
            /// 管理员信息
            /// </summary>
            public Authority.Model.member member { get; set; }
            public ProcessAdministratorModel(string username, string pass)
            {
                this.username = username;
                this.pass = pass;
            }

        }
        /// <summary>
        /// 管理员最新项目获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool GetAdminstratorXmProcess(ProcessXmModel model, out string mssg)
        {
            string topxmname = "";
            if (xmconnectbll.GetTopMember(model.name, out topxmname, out mssg))
            {

                model.xmname = topxmname;
                return true;
            }
            else
            {

                return false;
            }

        }
        /// <summary>
        /// 管理员项目获取类
        /// </summary>
        public class ProcessXmModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 项目名
            /// </summary>
            public string xmname { get; set; }
            public ProcessXmModel(string name)
            {
                this.name = name;
            }
        }
        /// <summary>
        /// 管理员列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAdministratorLoad(ProcessAdministratorLoadModel model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";
            if (adminBLL.GetMemberTable(model.userName, out dt, out mssg))
            {

                model.dt = dt;
                return true;
            }
            else
            {

                model.dt = dt;
                return false;
            }
        }
        /// <summary>
        /// 管理员列表获取类
        /// </summary>
        public class ProcessAdministratorLoadModel
        {
            /// <summary>
            /// 用户名
            /// </summary>
            public string userName { get; set; }
            /// <summary>
            /// 管理员信息表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessAdministratorLoadModel(string userName)
            {
                this.userName = userName;
            }
            public ProcessAdministratorLoadModel()
            {

            }
        }
        /// <summary>
        /// 管理员项目表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAdministrtorXmLoad(ProcessAdministrtorXmLoadModel model, out string mssg)
        {
            DataTable dt = null;
            mssg = "";
            if (memberbll.GetXmList(model.strWhere, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 管理员项目表获取类
        /// </summary>
        public class ProcessAdministrtorXmLoadModel
        {
            /// <summary>
            /// 查询条件
            /// </summary>
            public string strWhere { get; set; }
            /// <summary>
            /// 管理员项目表
            /// </summary>
            public DataTable dt { get; set; }
            public ProcessAdministrtorXmLoadModel(string strWhere)
            {
                this.strWhere = strWhere;
            }
        }
        /// <summary>
        /// 监测员信息更新
        /// </summary>
        /// <param name="member"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessMonitorUpdate(UnitMember member, out string mssg)
        {
            return unitMemberBLL.Update(member, out mssg);

        }

        /// <summary>
        /// 监测员与项目的关系处理
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ProcessXm_Member(ProcessXm_MemberModel model)
        {
            List<Authority.Model.Xm_Member> lxm = new List<Authority.Model.Xm_Member>();
            Authority.BLL.UnitMember um = new Authority.BLL.UnitMember();
            string[] mapstrs = model.xm_member_str.Split(',');
            foreach (string mapstr in mapstrs)
            {

                int id = um.GetMemberId(mapstr);
                lxm.Add(new Authority.Model.Xm_Member { xmno = model.xmno, memberno = id.ToString() });
            }
            lxm.Add(new Authority.Model.Xm_Member { xmno = model.xmno, memberno = model.userid.ToString() });
            model.Listmember = lxm;
            return true;
        }
        /// <summary>
        /// 监测员与项目的关系处理类
        /// </summary>
        public class ProcessXm_MemberModel
        {
            public string xmno { get; set; }
            public int userid { get; set; }
            public string xm_member_str { get; set; }
            public List<Xm_Member> Listmember { get; set; }
            public ProcessXm_MemberModel(string xmno, string xm_member_str,int memberno)
            {
                this.xmno = xmno;
                this.userid = memberno;
                this.xm_member_str = xm_member_str;

            }
        }
        /// <summary>
        /// 监督员与项目的关系处理
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ProcessXm_Monitor(ProcessXm_MonitorModel model)
        {
            List<Authority.Model.Xm_Monitor> lxm = new List<Authority.Model.Xm_Monitor>();
            Authority.BLL.MonitorMember um = new Authority.BLL.MonitorMember();
            string[] mapstrs = model.xm_Monitor_str.Split(',');
            foreach (string mapstr in mapstrs)
            {
                string name = mapstr.Substring(0, mapstr.IndexOf("["));
                string unitname = mapstr.Substring(mapstr.IndexOf("[") + 1, mapstr.Length - 2 - mapstr.IndexOf("["));
                int id = um.GetMonitorId(name, unitname);
                lxm.Add(new Authority.Model.Xm_Monitor { xmno = model.xmno, userNO = id.ToString() });
            }
            model.ListMonitor = lxm;
            return true;
        }
        /// <summary>
        /// 监督员与项目的关系处理类
        /// </summary>
        public class ProcessXm_MonitorModel
        {
            public string xmno { get; set; }
            public string xm_Monitor_str { get; set; }
            public List<Xm_Monitor> ListMonitor { get; set; }
            public ProcessXm_MonitorModel(string xmno, string xm_Monitor_str)
            {
                this.xmno = xmno;
                this.xm_Monitor_str = xm_Monitor_str;
            }
        }
        /// <summary>
        /// 监测员与项目的关系添加
        /// </summary>
        /// <param name="xm_member"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXm_MemberAdd(Xm_Member xm_member, out string mssg)
        {
            return xm_MemberBLL.Add(xm_member, out mssg);
        }
        /// <summary>
        /// 监督员与项目的关系添加
        /// </summary>
        /// <param name="xm_monitor"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXm_MonitorAdd(Xm_Monitor xm_monitor, out string mssg)
        {
            return xm_MonitorBLL.Add(xm_monitor, out mssg);
        }
        /// <summary>
        /// 监测员与项目的关系批量删除
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXm_MemberDelList(string xmname, out string mssg)
        {
            return xm_MemberBLL.DeleteList(xmname, out mssg);
        }
        /// <summary>
        /// 监督员与项目的关系批量删除
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessXm_MonitorDelList(string xmname, out string mssg)
        {
            return xm_MonitorBLL.DeleteList(xmname, out mssg);
        }
        /// <summary>
        /// 管理员项目名称列表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessAdministratorXmnameList(ProcessPeopleXmnameListCondition model, out string mssg)
        {
            List<string> ls = null;
            if (memberbll.GetMemberNameList(model.userName, out ls, out mssg))
            {
                model.xmnameList = ls;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ProcessAdministratorInfoLoad(ProcessAdministratorInfoLoadModel model, out string mssg)
        {
            NFnet_DAL.MODEL.member member = new NFnet_DAL.MODEL.member();
            if (adminBLL.GetMember(model.userId, out member, out mssg))
            {
                model.model = member;
                return true;
            }
            else
            {
                return false;
            }

        }
        public class ProcessAdministratorInfoLoadModel
        {
            public string userId { get; set; }
            public NFnet_DAL.MODEL.member model { get; set; }
            public ProcessAdministratorInfoLoadModel(string userId)
            {
                this.userId = userId;
            }
        }



        /// <summary>
        /// 管理员项目表获取
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public static bool ProcessAdministratorXmnameDb(ProcessUserXmDbCondition model, out string mssg)
        {
            DataTable dt = null;
            if (memberbll.GetMemberXmTable(model.username, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 管理员多条件筛选项目表
        /// </summary>
        /// <param name="model">项目筛选条件</param>
        /// <param name="mssg">执行结果信息</param>
        /// <returns>返回项目结果表</returns>
        public static bool ProcessMutilConditionAdministratorXmTableFilter(ProcessXmFilterCondition model, out string mssg)
        {
            List<Authority.Model.xmconnect> xmconnectlist = null;
            if (memberbll.MutilConditionMemberXmTableFilter(model.userID, model.gcjb, model.gclx, model.gcname, model.pro, model.city, model.area, out  xmconnectlist, out  mssg))
            {
                model.xmconnectlist = xmconnectlist;
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ProcessXmAdministator(ProcessXmAdministatorModel model, out string mssg)
        {

            member member = null;
            if (memberbll.XmMember(model.xmname, out member, out mssg))
            {
                model.model = member;
                return true;
            }
            return false;
        }


        public class ProcessXmAdministatorModel
        {
            public string xmname { get; set; }
            public member model { get; set; }
            public ProcessXmAdministatorModel(string xmname)
            {
                this.xmname = xmname;

            }

        }

        public bool ProcessAdministratorsmsBLL(string userid, out string mssg)
        {
            return memberbll.sms(userid,out  mssg);
        }

        public bool ProcessAdministratorSurveyBaseExchange(ProcessAdministratorSurveyBaseExchangeModel model,out string mssg)
        {
            return memberbll.SurveyBaseExchange(model.userId,model.surveybase,out mssg);
        }
        public class ProcessAdministratorSurveyBaseExchangeModel
        {
            public string userId { get; set; }
            public bool surveybase { get; set; }
            public ProcessAdministratorSurveyBaseExchangeModel(string userId, bool surveybase)
            {
                this.userId = userId;
                this.surveybase = !surveybase;
            }
        }

    }
}