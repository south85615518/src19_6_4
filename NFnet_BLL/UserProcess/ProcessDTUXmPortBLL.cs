﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.UserProcess
{
    public class ProcessDTUXmPortBLL
    {
        private Authority.BLL.DTUXmPort xmPortBLL = new Authority.BLL.DTUXmPort();
        public bool ProcessDTUPortXmBLL(ProcessDTUPortXmBLLModel model,out string mssg)
        {
            Authority.Model.DTUXmPort xmPortModel = new Authority.Model.DTUXmPort();
            if (xmPortBLL.GetDTUPortXm(model.port, out xmPortModel,out mssg))
            {
                model.model = xmPortModel;
                return true;
            }
            return false;

        }
        public class ProcessDTUPortXmBLLModel
        {
            public int port { get; set; }
            public Authority.Model.DTUXmPort model { get; set; }
            public ProcessDTUPortXmBLLModel(int port)
            {
                this.port = port;
            }
        }
        public bool ProcessDTUPortXmBLL(ProcessDTUPortXmnameBLLModel model, out string mssg)
        {
            string xmname = "";
            if (xmPortBLL.GetDTUPortXm(model.port, out xmname, out mssg))
            {
                model.xmname = xmname;
                return true;
            }
            return false;

        }
        public bool ProcessDTUXmList(out List<string>superadministrator,out string mssg)
        {
            superadministrator = null;
            mssg = "";
            List<string> xmList = new List<string>();
            if (xmPortBLL.DTUXmPortList(out xmList,out mssg))
            {
                superadministrator = xmList;
                return true;
            }
            return false;
        }

        public bool ProcessFixedInclinometerXmList(out List<string> superadministrator, out string mssg)
        {
            superadministrator = null;
            mssg = "";
            List<string> xmList = new List<string>();
            if (xmPortBLL.FixedInclinometerXmPortList(out xmList, out mssg))
            {
                superadministrator = xmList;
                return true;
            }
            return false;
        }

        public class ProcessDTUPortXmnameBLLModel
        {
            public int port { get; set; }
            public string xmname { get; set; }
            public ProcessDTUPortXmnameBLLModel(int port)
            {
                this.port = port;
            }
        }

        public bool ProcessDTUXmPortTableLoad(out System.Data.DataTable dt ,out string mssg )
        {
            if(xmPortBLL.GetDTUXmTable(out dt,out mssg))
            {
                return true;
            }
            return false;
        }
       


    }
}