﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace NFnet_BLL.UserProcess
{
    /// <summary>
    /// 用户项目关系查询条件类
    /// </summary>
    public class ProcessUserXmDbCondition
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// 项目表
        /// </summary>
        public DataTable dt { get; set; }
        public ProcessUserXmDbCondition(string username)
        {
            this.username = username;
        }
    }
}