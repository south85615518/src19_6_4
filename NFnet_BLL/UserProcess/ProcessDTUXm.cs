﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.UserProcess
{
    public class ProcessDTUXm
    {
        private Authority.BLL.DTU_Xm xmPortBLL = new Authority.BLL.DTU_Xm();
        public bool ProcessDTUXmBLL(ProcessDTUXmBLLModel model,out string mssg)
        {
            Authority.Model.DTU_Xm xmPortModel = new Authority.Model.DTU_Xm();
            if (xmPortBLL.GetDTUXm(model.dtu, out xmPortModel,out mssg))
            {
                model.model = xmPortModel;
                return true;
            }
            return false;

        }
        public class ProcessDTUXmBLLModel
        {
            public string dtu { get; set; }
            public Authority.Model.DTU_Xm model { get; set; }
            public ProcessDTUXmBLLModel(string dtu)
            {
                this.dtu = dtu;
            }
        }
        //public bool ProcessDTUXmBLL(NFnet_BLL.UserProcess.ProcessDTUXmPortBLL.ProcessDTUPortXmnameBLLModel model, out string mssg)
        //{
        //    string xmname = "";
        //    if (xmPortBLL.GetDTUXm(0, out xmname, out mssg))
        //    {
        //        model.xmname = xmname;
        //        return true;
        //    }
        //    return false;

        //}
       

    }
}