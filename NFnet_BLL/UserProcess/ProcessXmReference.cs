﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.UserProcess
{
    public class ProcessXmReference
    {
        public Authority.BLL.xmreference bll = new Authority.BLL.xmreference();
        public bool ProcessXmReferenceExist(Authority.Model.xmreference model,out string mssg )
        {
            return bll.Exists(model,out mssg);
        }
        public bool ProcessXmReferenceAdd(Authority.Model.xmreference model,out string mssg)
        {
            return bll.Add(model,out mssg);
        }
        public bool ProcessXmReferenceDelete(int  xmno,out string mssg)
        {
            return bll.Delete(xmno, out mssg);
        }
        public bool ProcessXmReferenceListLoad(ProcessXmReferenceListLoadModel model,out string mssg)
        {
            List<string> ls = null;
            if (bll.RefrenceListLoad(model.xmno, out ls, out mssg))
            {
                model.client_xmnamelist = ls;
                return true;
            }
            return false;
        }
        public class ProcessXmReferenceListLoadModel
        {
            public int xmno { get; set; }
            public List<string> client_xmnamelist { get; set; }
            public ProcessXmReferenceListLoadModel(int xmno)
            {
                this.xmno = xmno;
            }
        }
        public bool ProcessClient_xmnameXmnoGet(ProcessClient_xmnameXmnoGetModel model,out string mssg)
        {
            int xmno = 0;
            if (bll.XmnoGet(model.client_xmname, out xmno, out mssg))
            {
                model.xmno = xmno;
                return true;
            }
            return false;

        }
        public class ProcessClient_xmnameXmnoGetModel
        {
            public string client_xmname { get; set; }
            public int xmno { get; set; }
            public ProcessClient_xmnameXmnoGetModel(string client_xmname)
            {
                this.client_xmname = client_xmname;
            }
        }
    }
}