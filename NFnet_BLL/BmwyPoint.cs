﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using NFnet_DAL;
namespace NFnet.数据同步.clss
{
    public class BmwyPoint
    {
        public static database db = new database();
        private string xmname, id, pOINT_NAME,
                         n,
                         e,
                         z,
                         tARGET_HEIGHT,
                         reflectorName,
                         orglHar,
                         orglVar,
                         orglSd,
                         stationName,
                         rEMARK, ipcID, datetime;

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }

        public string Datetime
        {
            get { return datetime; }
            set { datetime = value; }
        }

        public string IpcID
        {
            get { return ipcID; }
            set { ipcID = value; }
        }

        public string REMARK
        {
            get { return rEMARK; }
            set { rEMARK = value; }
        }

        public string StationName
        {
            get { return stationName; }
            set { stationName = value; }
        }

        public string OrglSd
        {
            get { return orglSd; }
            set { orglSd = value; }
        }

        public string OrglVar
        {
            get { return orglVar; }
            set { orglVar = value; }
        }

        public string OrglHar
        {
            get { return orglHar; }
            set { orglHar = value; }
        }

        public string ReflectorName
        {
            get { return reflectorName; }
            set { reflectorName = value; }
        }

        public string TARGET_HEIGHT
        {
            get { return tARGET_HEIGHT; }
            set { tARGET_HEIGHT = value; }
        }

        public string E
        {
            get { return e; }
            set { e = value; }
        }

        public string Z
        {
            get { return z; }
            set { z = value; }
        }

        public string N
        {
            get { return n; }
            set { n = value; }
        }

        public string POINT_NAME
        {
            get { return pOINT_NAME; }
            set { pOINT_NAME = value; }
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        //学习点同步更新或插入
        public void BmwyPointSync()
        {
            sqlhelper helper = new sqlhelper();
            string[] pointKeyWord = { "comefrom", "POINTNAME" };
            string[] pointName = { helper.GetIPCID(this.IpcID, this.Xmname), this.POINT_NAME };
            if (!helper.PointNameExist("studyPoint", this.xmname, pointKeyWord, pointName))
            {
                BmwyPointInsert();
            }
            else
            {
                BmwyPointUpdate();
            }

        }
        //插入学习点
        public void BmwyPointInsert()
        {
            sqlhelper helper = new sqlhelper();
            database db = new database();
            OdbcConnection conn = db.GetStanderConn(this.xmname);

            string[] param = {
                                     "id",
                "E",
                "N",
                "Z","POINT_NAME","POINTNAME",
                "TARGET_HEIGHT",
                "ReflectorName",
                "orglHar",
                "orglVar",
                "orglSd",
                "StationName",
                "Remark",
                "comefrom",
                "search_time"};
            string[] condition = {
                                         helper.CreateIdOfTableName("studypoint",conn).ToString(),
                         n,this.
                         e,this.
                         z,this.
                         pOINT_NAME,this.POINT_NAME,this.
                         tARGET_HEIGHT,this.
                         reflectorName,this.
                         orglHar,this.
                         orglVar,this.
                         orglSd,this.
                         stationName,this.
                         rEMARK, GetIPCID(this.ipcID),Convert.ToString(DateTime.Now)};
            helper.InsertEntrl("studyPoint", param, condition, conn);

        }
        //更新学习点
        public void BmwyPointUpdate()
        {
            database db = new database();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            sqlhelper helper = new sqlhelper();
            string[] param = { "E",
                "N",
                "Z",
                "TARGET_HEIGHT",
                "ReflectorName",
                "orglHar",
                "orglVar",
                "orglSd",
                "StationName",
                "Remark",
                "search_time"};
            string[] paramVal = {
                                    this.e,
                                    this.n,
                                    this.z,
                                    this.TARGET_HEIGHT,
                                    this.ReflectorName,
                                    this.OrglHar,
                                    this.OrglVar,
                                    this.OrglSd,
                                    this.StationName,
                                    this.REMARK,
                                    Convert.ToString(DateTime.Now)
                                };
            string[] condition = { "comefrom", "POINTNAME" };
            string[] conditionVal = { helper.GetIPCID(this.ipcID, this.Xmname), this.POINT_NAME };
            helper.UpdateEntrl("studypoint", param, paramVal, condition, conditionVal, conn);
        }
        public BmwyPoint()
        {
        }
        public BmwyPoint(string id, string n, string e,
            string z, string pOINT_NAME, string tARGET_HEIGHT, string reflectorName,
            string orglHar, string orglVar, string orglSd, string stationName,
            string rEMARK, string ipcID, string datetime)
        {
            this.id = id;
            this.pOINT_NAME = pOINT_NAME;
            this.n = n;
            this.e = e;
            this.z = z;
            this.tARGET_HEIGHT = tARGET_HEIGHT;
            this.reflectorName = reflectorName;
            this.orglHar = orglHar;
            this.orglVar = orglVar;
            this.orglSd = orglSd;
            this.stationName = stationName;
            this.rEMARK = rEMARK;
            this.ipcID = ipcID;
            this.datetime = datetime;
        }

        //获取所有学习点
        public List<BmwyPoint> GetBmwyPoint()
        {
            sqlhelper helper = new sqlhelper();
            string sql = "select * from studypoint where comefrom='" + helper.GetIPCID(this.IpcID, this.Xmname) + "'";
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            DataTable dt = querysql.querystanderdb(sql, conn);
            string[] param = { "id","E",
                "N",
                "Z","point_name",
                "TARGET_HEIGHT",
                "ReflectorName",
                "orglHar",
                "orglVar",
                "orglSd",
                "StationName",
                "Remark",
                "IPCID",
                "DateTime" };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<BmwyPoint> lds = new List<BmwyPoint>();
            foreach (List<string> ls in lls)
            {
                BmwyPoint place = new BmwyPoint(ls[0], ls[1], ls[2], ls[3], ls[4], ls[5], ls[6], ls[7], ls[8], ls[9], ls[10], ls[11], GetIPCNo(ls[12]), ls[13]);
                lds.Add(place);
            }

            return lds;
        }
        //根据工控机的编号获取ID
        public string GetIPCID(string IPCNo)
        {
            string sql = "select id from ipc where ipcno = '" + IPCNo + "'";
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            string id = querysql.querystanderstr(sql, conn);
            return id;
        }
        //根据工控机的编号ID获取工控机编号
        public string GetIPCNo(string IPCID)
        {
            string sql = "select IPCNO from ipc where id = '" + IPCID + "'";
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            string id = querysql.querystanderstr(sql, conn);
            return id;
        }
        //删除学习点
        public void BmwyPointDel()
        {
            sqlhelper helper = new sqlhelper();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            string sql = "delete from studypoint where POINT_NAME='" + this.POINT_NAME + "' and comefrom ='" + helper.GetIPCID(this.IpcID, this.Xmname) + "'";
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);
        }
        //批量删除本地学习点
        public void BmwyPointDel(string pointStr)
        {
            pointStr = pointStr.Replace(",","','");
            sqlhelper helper = new sqlhelper();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            string sql = "delete from studypoint where POINT_NAME in ('" + pointStr + "')";
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);
        }
        //将工控机上的学习点卸下
        public void DebusStudyPoint(string pointstr)
        {
            sqlhelper helper = new sqlhelper();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            pointstr = pointstr.Replace(",", "','");
            pointstr = "('" + pointstr + "')";

            string sql = "update studypoint set comefrom='' where POINTNAME in " + pointstr + " and comefrom ='" + helper.GetIPCID(this.IpcID, this.Xmname) + "' ";
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);
        }
        //将本地的学习点装载到工控机
        public void LoadStudyPoint(string pointstr)
        {
            sqlhelper helper = new sqlhelper();
            OdbcConnection conn = db.GetStanderConn(this.xmname);
            pointstr = pointstr.Replace(",", "','");
            pointstr = "('" + pointstr + "')";

            string sql = "update studypoint set comefrom='" + helper.GetIPCID(this.IpcID, this.Xmname) + "' where POINTNAME in " + pointstr + " and comefrom ='' ";
            updatedb udb = new updatedb();
            udb.UpdateStanderDB(sql, conn);
        }

    }
}