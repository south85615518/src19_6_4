﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using NFnet_DAL;
using System.Data.Odbc;

namespace NFnet_BLL
{
    public class resultdata_bll : System.Web.UI.Page
    {
        public static database db = new database();
        public static int _i = 0;
        public void CycInit(DropDownList ddl,string xmname)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = "select distinct(cyc) from fmos_cycdirnet where taskName='" + xmname + "' order by cyc asc ";
            List<string> ls = querysql.querystanderlist(sql,conn);
            ddl.DataSource = ls;
            ddl.DataBind();
            if(ls != null)
            ddl.SelectedIndex = ls.Count - 1 >= 0 ? ls.Count - 1 : 0;
        }
        public void PointInit(DropDownList ddl, string xmname)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = "select distinct(point_name) from fmos_cycdirnet where taskName='" + xmname + "' order by point_name, cyc asc ";
            List<string> ls = new List<string>();
            ls.Add("全部");
            ls.AddRange(querysql.querystanderlist(sql, conn));
            ddl.DataSource = ls;
            ddl.DataBind();
            if (ls != null)
                ddl.SelectedIndex = 0;
        }
    }
}