﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class JclxTabMap
    {
        private static Dictionary<string,string> ds = new Dictionary<string,string>();
        //根据监测类型名称获取点好表名
        public static string GetPointTabName(string jclx)
        {
            switch (jclx)
            {
                case "水位": return " swPoint ";
                case "表面位移": return " studypoint ";
                case "深部位移": return " inclinometer ";
                case "雨量": return " rain ";
                case "应力": return " axialforce ";
                case "沉降": return " settlement ";
            }
            return null;
        }
    }
}