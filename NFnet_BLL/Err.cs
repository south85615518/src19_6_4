﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL
{
    public class Err
    {
        private static List<string> errMessgeList;

        public static List<string> ErrMessgeList
        {
            get { return Err.errMessgeList; }
            set { Err.errMessgeList = value; }
        }

       
        public static void ClearErrMess()
        {
            Err.errMessgeList = new List<string>();
        }
        //设置错误提示信息
        public void SetErrMess(string errMess)
        {
            ErrMessgeList.Add(errMess);

        }
    }
}