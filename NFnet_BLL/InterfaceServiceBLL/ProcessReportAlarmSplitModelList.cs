﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.AuthorityAlarmProcess;
using Tool;

namespace NFnet_BLL.InterfaceServiceBLL.AuthorityAlarmProcess
{
    public class ProcessReportAlarmSplitModelList
    {
       public static ProcessalarmsplitondateBLL processalarmsplitondateBLL = new ProcessalarmsplitondateBLL();
       public static List<string> ReportAlarmSplitMssgLoad(int xmno , List<string> cyclist ,data.Model.gtsensortype datatype ,out string mssg)
       {
           var processXmAlarmSplitOnDateModelListModel = new NFnet_BLL.AuthorityAlarmProcess.ProcessalarmsplitondateBLL.ProcessalarmsplitondateModelListBYCYCListModel(xmno,cyclist,datatype);
           processalarmsplitondateBLL.ProcessalarmsplitondateModelListBYCYCList(processXmAlarmSplitOnDateModelListModel, out mssg);
           if (processXmAlarmSplitOnDateModelListModel.alarmsplitondatelist == null||processXmAlarmSplitOnDateModelListModel.alarmsplitondatelist.Count == 0) return new List<string>();
           return ReportAlarmSplitMssgCreate(processXmAlarmSplitOnDateModelListModel.alarmsplitondatelist);
       }


       public static List<string> ReportAlarmSplitMssgCreate(List<AuthorityAlarm.Model.alarmsplitondate> alarmsplitondatelist )
       {
           var mssglist = (from m in alarmsplitondatelist select m.alarmContext).ToList();
           string mssgstr = string.Join(";",mssglist);
           ExceptionLog.ExceptionWrite("预警信息长度:"+mssgstr.Length);
           if (mssgstr.Length < 100) return mssglist;
           else
           {
               var pointnamelist = (from m in alarmsplitondatelist   select m.pointName+"/"+m.time).ToList();
               string pointnamestr = string.Join(";", pointnamelist);
               if (pointnamestr.Length < 100) return pointnamelist;
               else
                   return ListAppendLengthCount(pointnamelist);
           }

       }
       public static  List<string> ListAppendLengthCount(List<string> strlist)
       {
            List<string> lsappend = new List<string>();
            StringBuilder strappend = new StringBuilder();
            foreach(var str in strlist)
            {
                if (strappend.AppendFormat(";{0}", str).Length > 100) break;
                lsappend.Add(str);
            }
            if (strappend.Length > 41)
                lsappend.Add( string.Format("等{0}个", strlist.Count));
            else lsappend.Add(string.Format("{0}个", strlist.Count));
            return lsappend;
       }
       public static List<string> ListDistinct(List<string> ls)
       {
            List<string> lstmp = new List<string>();
            foreach(var str in ls)
            {
                if (!lstmp.Contains(str))
                {
                    ls.Add(str);
                }
            }
            return lstmp;
       }
    }
}
