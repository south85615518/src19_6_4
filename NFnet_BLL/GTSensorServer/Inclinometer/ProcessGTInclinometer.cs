﻿using System;
using System.Collections.Generic;
using Tool;
using System.IO;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.DataImport.ProcessFile.SenmosServer;
using System.Threading;
using System.Data;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.ProjectInfo;
using NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement;
using NFnet_BLL.DisplayDataProcess.GTInclinometer;
using NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.NGN_FixedInclinometer;
namespace NFnet_BLL.DataImport.ProcessFile.GTSensorServer
{
    /// <summary>
    /// 全站仪数据导入处理类
    /// </summary>
    public class ProcessGTInclinometer
    {
        //public ProcessExcelDataImport processExcelDataImport = new ProcessExcelDataImport();
        public ProcessOrglDataAlarmBLL orglDataAlarmBLL = new ProcessOrglDataAlarmBLL();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public class DataImportModel
        {
            public int xmno { get; set; }
            public string path { get; set; }
            public DataImportModel(int xmno, string path)
            {
                this.xmno = xmno;
                this.path = path;
            }
        }

        /// <summary>
        /// 数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileDecodeTxt(ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            switch (model.fileType)
            {
                case "测斜原始数据":
                    if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));

                        bool ipass =  Tool.GTInclinometer.GTXLSHelper.ProcessXlsDataImportCheck(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno);
                        string dataimportsetting = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
                        Tool.FileHelper.FileStringAppendWrite(string.Format("data descode request?{0}", jss.Serialize(model)), System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析列表\\datadescodelist.txt");
                        Tool.FileHelper.FileStringWrite(jss.Serialize(model), dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt");
                        ExceptionLog.ExceptionWrite("数据文件导入任务文件名【" + dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt" + "】");
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_inclinometer", model.xmno)))
                        //{
                        //ProcessXlsDataDirImport(processXlsDataDirImportModel);
                        //Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsDataDirImport));
                        //t.Name = string.Format("GTresultdataimport", model.xmno);
                        //t.Start(processXlsDataDirImportModel);
                        //model.thread = t;
                        //}
                        return ipass;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx" || Path.GetExtension(model.path) == ".csv")
                    {

                        bool ipass = Tool.GTInclinometer.GTXLSHelper.XlsFileDataImportCheck(model.path, out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(model.path, model.xmno);
                        string dataimportsetting = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
                        Tool.FileHelper.FileStringAppendWrite(string.Format("data descode request?{0}", jss.Serialize(model)), System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析列表\\datadescodelist.txt");
                        Tool.FileHelper.FileStringWrite(jss.Serialize(model), dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt");
                        ExceptionLog.ExceptionWrite("数据文件导入任务文件名【" + dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt" + "】");
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_inclinometer", model.xmno)))
                        //{
                        //ProcessXlsFileDataDirImport(processXlsDataDirImportModel);
                        //Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsFileDataDirImport));
                        //t.Name = string.Format("GTresultdataimport", model.xmno);
                        //t.Start(processXlsDataDirImportModel);
                        //model.thread = t;
                        //}
                        return ipass;
                    }

                    return false;



                case "测斜数据删除":

                    if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno);
                        ProcessXlsDataDirDescode(processXlsDataDirImportModel);
                        return true;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx")
                    {

                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(model.path, model.xmno);
                        ProcessXlsFileDataDirDescode(processXlsDataDirImportModel);
                        return true;
                    }

                    return false;

                case "时间":

                    return true;
                case "仪器":

                    return true;

                default: return false;

            }

        }

        /// <summary>
        /// 数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileDecodeTxtDescode(ProcessFileDecodeTxtModel model, out string mssg)
        {
            mssg = "";
            switch (model.fileType)
            {
                case "测斜原始数据":
                    if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));

                        bool ipass = Tool.GTInclinometer.GTXLSHelper.ProcessXlsDataImportCheck(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_inclinometer", model.xmno)))
                        //{
                        ProcessXlsDataDirImport(processXlsDataDirImportModel);
                        //Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsDataDirImport));
                        //t.Name = string.Format("GTresultdataimport", model.xmno);
                        //t.Start(processXlsDataDirImportModel);
                        //model.thread = t;
                        //}
                        List<string> alarmInfoList = new List<string>(); /*resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);*/
                        //emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
                        alarmInfoList.AddRange(orglDataAlarmBLL.ResultDataAlarm(model.xmname, model.xmno));
                        emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
                        return ipass;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx" || Path.GetExtension(model.path) == ".csv")
                    {

                        bool ipass = Tool.GTInclinometer.GTXLSHelper.XlsFileDataImportCheck(model.path, out mssg);
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(model.path, model.xmno);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_inclinometer", model.xmno)))
                        //{
                        ProcessXlsFileDataDirImport(processXlsDataDirImportModel);
                        //Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsFileDataDirImport));
                        //t.Name = string.Format("GTresultdataimport", model.xmno);
                        //t.Start(processXlsDataDirImportModel);
                        //model.thread = t;
                        //}
                        List<string> alarmInfoList = new List<string>(); /*resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);*/
                        //emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
                        alarmInfoList.AddRange(orglDataAlarmBLL.ResultDataAlarm(model.xmname, model.xmno));
                        emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
                        return ipass;
                    }
                    
                    return false;



                case "测斜数据删除":

                    if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));
                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno);
                        ProcessXlsDataDirDescode(processXlsDataDirImportModel);
                        return true;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx")
                    {

                        var processXlsDataDirImportModel = new ProcessXlsDataDirImportModel(model.path, model.xmno);
                        ProcessXlsFileDataDirDescode(processXlsDataDirImportModel);
                        return true;
                    }

                    return false;

                case "时间":

                    return true;
                case "仪器":

                    return true;

                default: return false;

            }

        }





        /// <summary>
        /// 全站仪数据导入处理主程序处理参数实体类
        /// </summary>
        public class ProcessFileDecodeTxtModel : GTInclinometerFileUploadConditionModel
        {
            /// <summary>
            /// 文件类型
            /// </summary>
            public string fileType { get; set; }
            public Thread thread { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public string xmname { get; set; }
            public ProcessFileDecodeTxtModel(int xmno, string path, string fileType)
            {
                this.xmno = xmno;
                this.path = path;
                this.fileType = fileType;
            }
            public ProcessFileDecodeTxtModel()
            {
                this.xmno = xmno;
                this.path = path;
                this.fileType = fileType;
            }
        }
        public ProcessEmailSendBLL emailSendBLL = new ProcessEmailSendBLL();
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public static string mssg = "";
        public ProcessFixed_Inclinometer_orglDataBLL processFixed_Inclinometer_orglDataBLL = new ProcessFixed_Inclinometer_orglDataBLL();
        public bool ProcessXlsDataImport(int xmno, string xlspath, out int cont, out string mssg, out string outmssgimport)
        {
            string xmname = "";
            cont = 0;
            outmssgimport = "";
            string mssgimport = "";
            Tool.GTInclinometer.GTXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);
            DataTable dt = new DataTable();
            Tool.GTInclinometer.GTXLSHelper.ProcessXlsDataTableImport(xlspath, out dt, out mssg);
            List<DataTable> datatablelist = Tool.DataTableHelper.ProcessGTInclinometerDataTableSplit(dt);
            if (dt.Rows.Count == 0) return false;
            //DataView dv = new DataView(dt);
            //dv.Sort = " deepth asc ";
            //NFnet_BLL.DataProcess.ProcessSurfaceDataBLL.ProcessResultDataTimeListLoadModel processResultDataTimeListLoadModel = null;



            double a_ac_diff = 0;
            double b_ac_diff = 0;
            foreach (DataTable splittable in datatablelist)
            {
                DataView dv = new DataView(splittable);
                if (dv.Count == 0) continue;
                DateTime time = Convert.ToDateTime(dv[0]["time"].ToString());
                string holename = dv[0]["point_name"].ToString();
                var IsInsertDataModel = new ProcessFixed_Inclinometer_orglDataBLL.IsTypeDataModel(xmno, time, holename);
                var IsInclinometerDataExistModel = new ProcessFixed_Inclinometer_orglDataBLL.IsInclinometerDataExistModel(xmno,holename,time);

                int inclinometerimportcyc = 0;
                if (processFixed_Inclinometer_orglDataBLL.IsInclinometerDataExist(IsInclinometerDataExistModel,out mssg))
                {
                    Console.WriteLine(mssg);
                    continue;
                }
                Console.WriteLine(mssg);
                if (processFixed_Inclinometer_orglDataBLL.IsInsertData(IsInsertDataModel, out mssg))
                {
                    Console.WriteLine(mssg);
                    //获取接段周期
                    var InsertDataCycGetModel = new ProcessFixed_Inclinometer_orglDataBLL.InsertDataCycGetModel(xmno, time, holename);
                    if (!processFixed_Inclinometer_orglDataBLL.InsertDataCycGet(InsertDataCycGetModel, out mssg)) { Console.WriteLine(mssg); continue; }
                    //如果接段周期有空位则无需做周期移位操作
                    var IsInsertCycExistModel = new ProcessFixed_Inclinometer_orglDataBLL.IsInsertCycExistModel(xmno, InsertDataCycGetModel.cyc - 1, holename);
                    if (InsertDataCycGetModel.cyc == 1 || processFixed_Inclinometer_orglDataBLL.IsInsertCycExist(IsInsertCycExistModel, out mssg))
                    {
                        //执行周期后移操作
                        var InsertCycStepModel = new ProcessFixed_Inclinometer_orglDataBLL.InsertCycStepModel(xmno, InsertDataCycGetModel.cyc, holename);
                        if (!processFixed_Inclinometer_orglDataBLL.ProcessInsertCycStep(InsertCycStepModel, out mssg))
                        {
                            Console.WriteLine(mssg);  continue;
                        }
                    }
                    else
                    {
                        InsertDataCycGetModel.cyc -= 1;
                    }
                    //执行周期数据操作
                    //foreach (var datamodel in tmplistcycdirnet)
                    //{
                    inclinometerimportcyc = InsertDataCycGetModel.cyc;
                    //    for (t = 0; t < 2; t++)
                    //    {

                    //        if (processResultDataBLL.ProcessResultDataAdd(datamodel, out mssg)) { Console.WriteLine(mssg); break; }
                    //        ExceptionLog.DTUPortInspectionWrite(string.Format("导入项目{0}点名{1}测量时间{2}的" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.project_name, datamodel.point_name, datamodel.time, t));
                    //        Thread.Sleep(1000);

                    //    }
                    //}
                    //清空临时数据列表
                }
                else
                {
                    Console.WriteLine("直接插入数据");
                    //非接段数据则获取当前库中该项目的最大周期
                    var ResultDataExtremelyCondition = new NFnet_BLL.DisplayDataProcess.NGN_FixedInclinometer.ProcessFixed_Inclinometer_orglDataBLL.ResultDataExtremelyCondition(xmno,holename ,"max");
                    if (!processFixed_Inclinometer_orglDataBLL.ProcessResultDataExtremely(ResultDataExtremelyCondition, out mssg))
                    { Console.WriteLine(mssg); continue; }
                    Console.WriteLine(mssg);
                    int maxcyc = Convert.ToInt32(ResultDataExtremelyCondition.extremelyCyc) + 1;
                    //执行周期数据操作

                    inclinometerimportcyc = maxcyc;

                }

                foreach (DataRowView drv in dv)
                {
                    
                    global::NGN.Model.fixed_inclinometer_orgldata model = new global::NGN.Model.fixed_inclinometer_orgldata
                    {

                        xmno = xmno,
                        holename = drv["point_name"].ToString(),
                        deepth = Convert.ToDouble(drv["deepth"].ToString()),
                        time = Convert.ToDateTime(drv["time"]),
                        a_positive = Convert.ToDouble(drv["a_positive"]),
                        a_nagtive = Convert.ToDouble(drv["a_nagtive"]),
                        b_positive = Convert.ToDouble(drv["b_positive"]),
                        b_nagtive = Convert.ToDouble(drv["b_nagtive"]),
                        a_ac_diff = Convert.ToDouble(drv["a_ac"]),
                        b_ac_diff = Convert.ToDouble(drv["b_ac"]),
                        cyc = inclinometerimportcyc
                    };
                    if (drv["a_ac"] == "" && drv["b_ac"] == "")
                    {
                        model.a_planediff = (model.a_positive + model.a_nagtive) * 1000;
                        model.b_planediff = (model.b_positive + model.b_nagtive) * 1000;
                        model.a_avediff = (model.a_positive - model.a_nagtive) * 500;
                        model.b_avediff = (model.b_positive - model.b_nagtive) * 500;
                        a_ac_diff += model.a_planediff;
                        b_ac_diff += model.b_planediff;
                        model.a_ac_diff = a_ac_diff;
                        model.b_ac_diff = b_ac_diff;
                        GTInclinometerCalculate(model);
                    }


                    processFixed_Inclinometer_orglDataBLL.Add(model, out mssg);
                }
            }
            //ExceptionLog.TotalSationPointCheckVedioWrite("项目"+xmname+"共导入全站仪结果数据数据"+cont+"条");
            return true;
        }

        public int CycGetFromTimeList(List<string> timeatrlist, DateTime time)
        {
            
            List<DateTime> timelist = new List<DateTime>();
            foreach (var timestr in timeatrlist)
            {
                timelist.Add(Convert.ToDateTime(timestr.Substring(1, timestr.Length-2)));
            }
            if (timelist.Contains(time)) return -1;
            try
            {
                if (timelist.Count == 0) { return 1; }
                timelist.Sort();
                int i = 0;
                while (i < timelist.Count)
                {
                    if (time > timelist[timelist.Count-1 - i])
                        return timelist.Count-i+1;
                    i++;
                }
                return -1;
            }
            finally
            {
                timelist.Add(time);
                timelist.Sort();
            }
        }






        public bool ProcessgtinclinometerXlsDataImport(int xmno, string xlspath, out int cont, out string mssg, out string outmssgimport)
        {
            string xmname = "";
            cont = 0;
            outmssgimport = "";
            string mssgimport = "";
            GTXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);
            DataTable dt = new DataTable();
            Tool.GTInclinometer.GTXLSHelper.ProcessgtInclinometerXlsDataTableImport(xlspath, out dt, out mssg);

            if (dt.Rows.Count == 0) return false;
            DataView dv = new DataView(dt);
            dv.Sort = " deepth asc ";
            double a_ac_diff = 0;
            double b_ac_diff = 0;
            foreach (DataRowView drv in dv)
            {


                global::NGN.Model.fixed_inclinometer_orgldata model = new global::NGN.Model.fixed_inclinometer_orgldata
                {

                    xmno = xmno,
                    holename = drv["point_name"].ToString(),
                    deepth = Convert.ToDouble(drv["deepth"].ToString()),
                    time = Convert.ToDateTime(drv["time"]),
                    a_positive = Convert.ToDouble(drv["a_positive"]),
                    a_nagtive = Convert.ToDouble(drv["a_nagtive"]),
                    b_positive = Convert.ToDouble(drv["b_positive"]),
                    b_nagtive = Convert.ToDouble(drv["b_nagtive"]),
                    a_ac_diff = Convert.ToDouble(drv["a_ac"]),
                    b_ac_diff = Convert.ToDouble(drv["b_ac"])
                };

                processFixed_Inclinometer_orglDataBLL.Add(model, out mssg);
            }
            //ExceptionLog.TotalSationPointCheckVedioWrite("项目"+xmname+"共导入全站仪结果数据数据"+cont+"条");
            return true;
        }
        public void GTInclinometerCalculate(global::NGN.Model.fixed_inclinometer_orgldata model)
        {
            var fixed_inclinometer_orgldataModelGetModel = new ProcessFixed_Inclinometer_orglDataBLL.fixed_inclinometer_orgldataModelGetModel(model.xmno, model.holename, model.deepth, model.time);
            if (!processFixed_Inclinometer_orglDataBLL.GetLastTimeModel(fixed_inclinometer_orgldataModelGetModel, out mssg))
            {
                fixed_inclinometer_orgldataModelGetModel.model = model;
            }

            model.a_this_diff = model.a_ac_diff - fixed_inclinometer_orgldataModelGetModel.model.a_ac_diff;
            model.b_this_diff = model.b_ac_diff - fixed_inclinometer_orgldataModelGetModel.model.b_ac_diff;
        }



        public bool ProcessXlsDataDelete(string xlspath, out string mssg, out string outmssgimport)
        {


            outmssgimport = "";
            string mssgimport = "";
            GTXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);

            List<Tool.GTInclinometer.GTXLSHelper.DeleteDataFromXlsOutInfoModel> modellist = null;
            Tool.GTInclinometer.GTXLSHelper.ProcessXlsCycDataDelete(xlspath, out modellist, out mssg);
            NGN.Model.fixed_inclinometer_orgldata inclinometermodel = null;
            foreach (var model in modellist)
            {
                inclinometermodel = new NGN.Model.fixed_inclinometer_orgldata { xmno = model.xmno, holename = model.pointname, time = model.time };
                ProcessCycdirnetCYCDataDelete(inclinometermodel, out mssg);
                Console.WriteLine(mssg);

            }
            return true;
        }


        public class ProcessXlsDataDirImportModel
        {
            public string dirpath { get; set; }
            public int xmno { get; set; }
            public ProcessXlsDataDirImportModel(string dirpath, int xmno)
            {
                this.dirpath = dirpath;
                this.xmno = xmno;
            }
        }


        public bool ProcessCycdirnetCYCDataDelete(NGN.Model.fixed_inclinometer_orgldata model, out string mssg)
        {
            return processFixed_Inclinometer_orglDataBLL.Delete(model, out mssg);
        }
        public static Authority.BLL.xmconnect bll = new Authority.BLL.xmconnect();



        public void ProcessXlsFileDataDirImport(object obj)
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            //Tool.ThreadProcess.Threads.Add(string.Format("{0}GT_inclinometer", model.xmno));
            string filename = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            ProcessDeleteTmp(model.xmno, out mssg);
            ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
            mssg = "";
            int cont = 0;
            int totalcount = 0;
            string importmssg = "";
            string tmpname = "";
            tmpname = string.Format("{0}.xls", Path.GetFileNameWithoutExtension(filename));
            if (Path.GetExtension(tmpname) == ".xls" || Path.GetExtension(tmpname) == ".xlsx")
            {
                ProcessXlsDataImport(model.xmno, tmpname, out cont, out mssg, out importmssg);
                totalcount += cont;
                ExceptionLog.TotalSationPointCheckVedioWrite("项目" + model.xmno + "共导入全站仪结果数据数据" + totalcount + "条");
                filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", tmpname, mssg == "" ? "数据导入完成!" : mssg);
                filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", tmpname, importmssg == "" ? "数据导入完成!" : importmssg);
                Console.WriteLine(string.Format("\r\n文件{0}------------------\r\n{1}", tmpname, mssg == "" ? "数据导入完成!" : mssg));
                Console.WriteLine(string.Format("\r\n文件{0}------------------\r\n{1}", tmpname, importmssg == "" ? "数据导入完成!" : importmssg));
            }


            Console.WriteLine(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            // List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
            // emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);

            Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_inclinometer", model.xmno));
            //return result;
        }
        public void ProcessXlsFileDataDirDescode(object obj)
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            string filename = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            string importmssg = "";
            if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
            {
                ProcessXlsDataDelete(filename, out mssg, out importmssg);

                filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除完成!" : mssg);
                filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除完成!" : importmssg);
                Console.WriteLine(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除完成!" : mssg));
                Console.WriteLine(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除完成!" : importmssg));
            }


            Console.WriteLine(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
        }






        public void ProcessXlsDataDirImport(object obj)
        {


            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            string dirpath = model.dirpath;
            int xmno = model.xmno;
            string mssg = "";
            bool result = true;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            string filemssg = "";
            string filemiportmssg = "";
            string importmssg = "";
            ProcessDeleteTmp(model.xmno, out mssg);
            ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
            mssg = "";
            int cont = 0;
            int totalcount = 0;
            string tmpname = "";
            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".csv") continue;
                tmpname = filename;
                //tmpname = string.Format("{1}.xls", dirpath, Path.GetFileNameWithoutExtension(filename));
                if (Path.GetExtension(tmpname) == ".xls" || Path.GetExtension(tmpname) == ".xlsx")
                {

                    ProcessXlsDataImport(model.xmno, tmpname, out cont, out mssg, out importmssg);
                    totalcount += cont;
                    ExceptionLog.TotalSationPointCheckVedioWrite("项目" + model.xmno + "共导入全站仪结果数据数据" + totalcount + "条");
                    filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", tmpname, mssg == "" ? "数据导入完成!" : mssg);
                    filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", tmpname, importmssg == "" ? "数据导入完成!" : importmssg);
                    Console.WriteLine(string.Format("\r\n文件{0}------------------\r\n{1}", tmpname, mssg == "" ? "数据导入完成!" : mssg));
                    Console.WriteLine(string.Format("\r\n文件{0}------------------\r\n{1}", tmpname, importmssg == "" ? "数据导入完成!" : importmssg));
                }

            }
            Console.WriteLine(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            //List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
            //emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);

            //Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_inclinometer", model.xmno));
            //return result;
        }
        public void ProcessXlsDataDirDescode(object obj)
        {
            ProcessXlsDataDirImportModel model = obj as ProcessXlsDataDirImportModel;
            string dirpath = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            string importmssg = "";
            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
                {
                    ProcessXlsDataDelete(filename, out mssg, out importmssg);

                    filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除成功!" : mssg);
                    filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除成功!" : importmssg);
                    Console.WriteLine(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除成功!" : mssg));
                    Console.WriteLine(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除成功!" : importmssg));
                }

            }
            Console.WriteLine(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_inclinometer", model.xmno));
            //return result;
        }
        public bool ProcessDeleteTmp(int xmno, out string mssg)
        {
            var deletemodel = new ProcessFixed_Inclinometer_orglDataBLL.deletetmpmodel(xmno);
            return processFixed_Inclinometer_orglDataBLL.delete_tmp(deletemodel, out mssg);
        }

    }

}