﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.DataImport.ProcessFile.GTSensorServer
{
    /// <summary>
    /// 全站仪数据导入请求参数类
    /// </summary>
    public class GTInclinometerFileUploadConditionModel
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public int xmno { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string path{get;set;}
        public GTInclinometerFileUploadConditionModel(int xmno,string path)
        {
            this.path = path;
            this.xmno = xmno;
        }
        public GTInclinometerFileUploadConditionModel()
        {
          
        }
    }
}