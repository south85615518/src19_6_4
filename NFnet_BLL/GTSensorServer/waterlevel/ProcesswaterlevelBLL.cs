﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFnet_BLL.DisplayDataProcess.WaterLevel;
using NFnet_BLL.DataImport.ProcessFileSenmos;
using NFnet_BLL.GTSensorServer;

namespace NFnet_BLL.DataImport.ProcessFile
{
    public partial class ProcessGTSensorDataBLL
    {
        public ProcessDTUDATABLL processDTUDATABLL = new ProcessDTUDATABLL();

        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessWaterLevelResultDataBLL(data.Model.gtsensordata gtsensorData, out string mssg)
        {
            mssg = "";
            if (gtsensorData.datatype != "水位") return false;

            global::DTU.Model.dtudata dtudata = new global::DTU.Model.dtudata
            {
                xmno = GetXmnoFromXmname(gtsensorData.project_name),
                point_name = gtsensorData.point_name,
                deep = gtsensorData.single_oregion_scalarvalue,
                dtutime = gtsensorData.time,
                time = gtsensorData.time
            };
            return processDTUDATABLL.ProcessDTUDATAInsertBLL(dtudata, out mssg);
        }
    }
}