﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFnet_BLL.GTSensorServer
{
    public class GTSensorData
    {
        public string project_name { get; set; }//工程名称,
        public string point_name { get; set; }  //店名,
        public string senorno { get; set; }//仪器编号,
        public string datatype { get; set; }  //数据类型,// 水位、测斜、应力、倾角..
        public string valuetype { get; set; }  //值类型,//一个变化量【如水位】、两个变化量【待加注】、三个变化量【全站仪、GPS】
        public double oregion_N { get; set; }  //北方向原始测量值,
        public double oregion_E { get; set; }  //东方向原始测量值,
        public double oregion_Z { get; set; }  //竖直方向原始测量值,
        public double single_oregion_scalarvalue { get; set; }  //单量原始测量值,
        public double first_oregion_scalarvalue { get; set; }  //双量第一个原始测量值,
        public double sec_oregion_scalarvalue { get; set; }  //双量第二个原始测量值,
        public double this_dN { get; set; }  //本次北方向变化值,
        public double this_dE { get; set; }  //本次东方向变化值,
        public double this_dZ { get; set; }  //本次竖直变化值,
        public double ac_dN { get; set; }  //累计北方向变化值,
        public double ac_dE { get; set; }  //累计东方变化值,
        public double ac_dZ { get; set; }  //累计竖直方向变化值,
        public double single_this_scalarvalue { get; set; }  //本次单量变化值,
        public double single_ac_scalarvalue { get; set; }  //累计单量变化值,
        public double first_this_scalarvalue { get; set; }  //本次双量第一个量变化值,
        public double first_ac_scalarvalue { get; set; }  //累计双量第一个量变化值,
        public double sec_this_scalarvalue { get; set; }  //本次双量第二个量变化值,
        public double sec_ac_scalarvalue { get; set; }  //累计双量第二个量变化值,
        public int cyc { get; set; }  //周期数,
        public DateTime time { get; set; }  //测量时间精确到秒

    }
}