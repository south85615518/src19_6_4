﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Tool;
using NFnet_BLL.DisplayDataProcess.GTSettlement;
using NFnet_BLL.UserProcess;
using NFnet_BLL.DataProcess.GTSettlement;

namespace NFnet_BLL.GTSensorServer
{
    public class ProcessGTSettlementBLL
    {
        public ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
        public ProcessDTUXm processDTUXm = new ProcessDTUXm();
        public ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        public string mssg = "";
        public void ProcessDTUDataFileTravel(string dtusettingpath)
        {
            List<data.Model.gtsettlementresultdata> resultdatalist = new List<data.Model.gtsettlementresultdata>();
            List<string> datastrsplit = new List<string>();
            List<string> filenamelist = Tool.FileHelper.DirTravel(dtusettingpath);
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            string importmssg = "";
            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".txt" && Path.GetFileNameWithoutExtension(filename).IndexOf("DTU")!=-1)
                {

                    ProcessDTUFileDescode(filename);
                   
                }

            }
        }
        public bool ProcessSettleBasePoint()
        {
            return false;
        }
        public class ProcessSettleBasePointModel
        {
            
        }
        public data.Model.gtsettlementresultdata ProcessDataStringDescode(string datastr,  int xmno,DateTime dt)
        {
            
            try
            {

                if (datastr.IndexOf("#") == -1) { ExceptionLog.gtsettlemntdataRecordWrite(string.Format("{0}不是沉降有效的数据", datastr)); return null; };
                int deviceno = 0, addressno = 0;
                string[] str = datastr.Split(' ');
                deviceno = Convert.ToInt32(Tool.com.HexHelper.GetHexStrFromString(str[0], 3, 2), 16);
                addressno = Convert.ToInt32(Tool.com.HexHelper.GetHexStrFromString(str[0], 1, 2), 16);
                //string timestrsrc = Tool.com.HexHelper.GetHexStrFromString(str[1], 2, 3);
                //string timestr = Tool.com.HexHelper.GetHexDateTimeFromTimeStr(timestrsrc);
                double val = Convert.ToInt32("0x" + str[3], 16) / 5;
                double degree = Convert.ToInt32("0x" + str[4], 16) > 32768 ? Convert.ToInt32("0x" + str[4], 16) / 16 - 4096 : Convert.ToInt32("0x" + str[4], 16) / 16;
                ExceptionLog.gtsettlemntdataRecordWrite(string.Format("经解析{0}得到设备号{1}地址吗{2}采集时间{3}沉降值{4}温度值{5}", datastr, deviceno, addressno,dt , val, degree));

                double Rt = -0.00057 * degree + 1.08685;
                val = Math.Round( val / Rt,2,MidpointRounding.AwayFromZero);
                var processSettlementTOPointModuleGetModel = new ProcessPointAlarmBLL.ProcessSettlementTOPointModuleGetModel(xmno,deviceno.ToString(),addressno.ToString());
                if (!processPointAlarmBLL.ProcessSettlementTOPointModule(processSettlementTOPointModuleGetModel, out mssg))
                {
                    ExceptionLog.gtsettlemntdataRecordWrite(mssg);
                    return null ;
                }
                var model = new data.Model.gtsettlementresultdata { point_name = processSettlementTOPointModuleGetModel.devicemodel.point_name, xmno = xmno, settlementvalue = val, settlementdiff = 0, degree = degree, initsettlementval = processSettlementTOPointModuleGetModel.devicemodel.initsettlementval, time = dt};
                //1.判断是不是基准点，如果是取出未结算的
                //processResultDataBLL.ProcessResultDataAdd(model,out mssg);
                ExceptionLog.gtsettlemntdataRecordWrite(mssg);
                return model;
                
            }
            catch (Exception ex)
            {
                ExceptionLog.gtsettlemntdataRecordWrite(string.Format("解析{0}沉降数据出错，错误信息:" + ex.Message, datastr));
                return null;
            }
        }
        
        public void ProcessDTUFileDescode(string filename)
        {
            string markstr = "";
            mssg = "";
            List<string> ls = new List<string>();
            string logpath = string.Format("{0}\\{1}_log.txt", Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename));
            List<data.Model.gtsettlementresultdata> gtdatalist = new List<data.Model.gtsettlementresultdata>();
            if (File.Exists(logpath))
            {
                markstr = Tool.FileHelper.ProcessSettingString(logpath);
                
            }
            ls = Tool.FileHelper.LocationFileRead(filename, markstr);
            if (ls.Count == 0)  return; 
            int i = 0;
            for (i = ls.Count; i > 0;i-- )
            {
                if (ls[i - 1] != "" && ls[i - 1].Length > "#5090BE 633831:00 FFFF FFFF FFFF FFFF".Length -2)
                {
                    Tool.FileHelper.FileStringWrite(ls[i-1], logpath);
                    break;
                }
            }
            
            string dtufilename = Path.GetFileNameWithoutExtension(filename);
            string dtuno = dtufilename.Substring(dtufilename.IndexOf("DTU") + 3, dtufilename.IndexOf("-") - dtufilename.IndexOf("DTU") -3);
            //string date = PorcessFileDateGetFromStr(dtufilename.Substring(dtufilename.IndexOf("-")+1));
            var processDTUXmBLLModel = new NFnet_BLL.UserProcess.ProcessDTUXm.ProcessDTUXmBLLModel(dtuno);

            if (!processDTUXm.ProcessDTUXmBLL(processDTUXmBLLModel, out mssg)) { 
                ExceptionLog.gtsettlemntdataRecordWrite(mssg);
                return ;
                }
            data.Model.gtsettlementresultdata model = null;
            List<data.Model.gtsettlementresultdata> ldgt = new List<data.Model.gtsettlementresultdata>();
            ProcessPointAlarmBLL processPointAlarmBLL = new ProcessPointAlarmBLL();
            string basepointname = "";
            DateTime currentdt = new DateTime();
            DateTime currenttime = new DateTime();
            foreach(var str in ls)
            {
                if (str.IndexOf("/") != -1 && str.IndexOf(":") != -1)
                {
                    currentdt = Convert.ToDateTime(str);
                    continue;
                }
                if ((model = ProcessDataStringDescode(str, processDTUXmBLLModel.model.xmno, currentdt)) == null) continue;
                if (model.time != currenttime && ldgt.Count > 0)
                {
                    foreach (var gtsettlementmodel in ldgt)
                    {
                        var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(model.xmno, model.point_name);
                        if (!processPointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg)) { ExceptionLog.gtsettlemntdataRecordWrite(mssg); continue; }
                        if (processPointAlarmModelGetModel.model.basepointname == "")
                        {
                            basepointname = processPointAlarmModelGetModel.model.point_name;
                        }
                        ProcessCYCSettlementDataCalculate(ldgt, basepointname);
                        break;
                    }
                    ldgt = new List<data.Model.gtsettlementresultdata>();
                    currenttime=model.time ;
                }
                else
                {
                    ldgt.Add(model);
                    currenttime = model.time;
                }
            }
            foreach (var gtsettlementmodel in ldgt)
            {
                var processPointAlarmModelGetModel = new ProcessPointAlarmBLL.ProcessPointAlarmModelGetModel(model.xmno, model.point_name);
                if (!processPointAlarmBLL.ProcessPointAlarmModelGet(processPointAlarmModelGetModel, out mssg)) { ExceptionLog.gtsettlemntdataRecordWrite(mssg); continue; }
                if (processPointAlarmModelGetModel.model.pointtype == "基准点")
                {
                    basepointname = processPointAlarmModelGetModel.model.point_name;
                }
                else
                    basepointname = processPointAlarmModelGetModel.model.basepointname;
                ProcessCYCSettlementDataCalculate(ldgt, basepointname);
                break;
            }

            return ;

        }

        public void ProcessCYCSettlementDataCalculate(List<data.Model.gtsettlementresultdata> ldgt,string basepointname)
        {

            ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
            var basemodel = (from m in ldgt where m.point_name == basepointname select m).ToList().Count>0?(from m in ldgt where m.point_name == basepointname select m).ToList()[0]:null;
            if (basemodel == null) { ExceptionLog.gtsettlemntdataRecordWrite("没有基准点数据"); return; }
            var processBaseResultDataLastTimeModelGetModel = new ProcessResultDataBLL.ProcessResultDataLastTimeModelGetModel(basemodel.point_name,basemodel.xmno,basemodel.time);

            if (!processResultDataBLL.ProcessResultDataLastTimeModelGet(processBaseResultDataLastTimeModelGetModel, out mssg))
                processBaseResultDataLastTimeModelGetModel.model = basemodel;
            foreach (var model in ldgt)
            {
                if (model.point_name == basemodel.point_name) { processResultDataBLL.ProcessResultDataAdd(model, out mssg); continue; }
                var processResultDataLastTimeModelGetModel = new ProcessResultDataBLL.ProcessResultDataLastTimeModelGetModel(model.point_name,model.xmno,model.time);
                if (!processResultDataBLL.ProcessResultDataLastTimeModelGet(processResultDataLastTimeModelGetModel, out mssg))
                    processResultDataLastTimeModelGetModel.model = model;
                model.ac_val = (model.settlementvalue - processResultDataLastTimeModelGetModel.model.settlementvalue) - (basemodel.settlementvalue - processBaseResultDataLastTimeModelGetModel.model.settlementvalue);
                model.this_val = model.ac_val - processResultDataLastTimeModelGetModel.model.ac_val;
                processResultDataBLL.ProcessResultDataAdd(model, out mssg);
                ExceptionLog.gtsettlemntdataRecordWrite(mssg);
            }
        }



        public DateTime PorcessFileDateGetFromStr(string datestr)
        {
            if (datestr.IndexOf("/") != -1 && datestr.IndexOf(":") != -1)
            {
                return Convert.ToDateTime(datestr);
            }
            return new DateTime();
            //string year = datestr.Substring(0,4);
            //string month = datestr.Substring(4,2);
            //string day = datestr.Substring(6,2);
            //return string.Format("{0}/{1}/{2}",year,month,day);
        }
        
    }
}