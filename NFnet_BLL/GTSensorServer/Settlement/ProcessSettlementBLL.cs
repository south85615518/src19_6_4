﻿using System;
using System.Collections.Generic;
using Tool;
using System.IO;
using System.Web.Script.Serialization;
using NFnet_BLL.DisplayDataProcess.SurfaceDisplacement.TotalStation;
using NFnet_BLL.UserProcess;
using System.Linq;
using NFnet_BLL.DataProcess;
using NFnet_BLL.AuthorityAlarmProcess;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.DisplayDataProcess;
using NFnet_BLL.DataImport.ProcessFileSenmos;
using NFnet_BLL.DisplayDataProcess.Settlement.Senor;
using NFnet_BLL.GTSensorServer;
namespace NFnet_BLL.DataImport.ProcessFile
{
    /// <summary>
    /// 全站仪数据导入处理类
    /// </summary>
    public partial class ProcessGTSensorDataBLL
    {
        public ProcessSettlementDataBLL processSettlementDataBLL = new ProcessSettlementDataBLL();

        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessSettlementResultDataBLL(data.Model.gtsensordata gtsensordata, out string mssg)
        {
            mssg = "";
            if (gtsensordata.datatype != "沉降") return false;

            global::Settlement.Model.Settlementresultdata Settlementresultdata = new global::Settlement.Model.Settlementresultdata
            {
                xmno = GetXmnoFromXmname(gtsensordata.project_name),
                this_val = gtsensordata.single_this_scalarvalue,
                ac_val = gtsensordata.single_ac_scalarvalue,
                elevation  = gtsensordata.single_oregion_scalarvalue,
                monitorTime = gtsensordata.time,
                cyc = gtsensordata.cyc,
                pointname = gtsensordata.point_name

            };


           return  processSettlementDataBLL.Add(Settlementresultdata,out mssg);
        }






    }

}