﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;
using System.IO;
using NFnet_BLL.DataImport.ProcessFileSenmos;
using System.Web.Script.Serialization;
using System.Threading;
using NFnet_BLL.ProjectInfo;
using NFnet_BLL.GTSensorServer;
using NFnet_BLL.DisplayDataProcess.GT;
using System.Data;

namespace NFnet_BLL.DataImport.ProcessFile
{
    public partial class ProcessGTSensorDataBLL
    {

        public int rows { get; set; }
        public System.Windows.Forms.ListBox listbox { get; set; }
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static ProcessXmBLL processXmBLL = new ProcessXmBLL();
        public static data.BLL.gtsensordata gtsensordatabll = new data.BLL.gtsensordata();
        public static Authority.BLL.xmconnect bll = new Authority.BLL.xmconnect();
        public static string mssg = "";
        public static ProcessResultDataBLL processResultDataBLL = new ProcessResultDataBLL();
        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">传感器文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        //public bool ProcessResultDataBLL(SensorFileUploadConditionModel model, out string mssg)
        //{
        //    int i = 0, t = 0;
        //    mssg = "";
        //    model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
        //    TxtEncoding.GetType(model.path);
        //    FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        //    StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
        //    try
        //    {

        //        List<string> lshead = new List<string>();
        //        string strTemp = ""; 
        //        //记录下当前项目正在入库操作的周期
        //        while (!sr.EndOfStream)
        //        {
        //            strTemp = sr.ReadLine();
        //            ExceptionLog.ExceptionWrite("数据"+strTemp);
        //            try
        //            {
        //                strTemp = HttpUtility.UrlDecode(strTemp);
        //                ExceptionLog.ExceptionWrite("Url解析后" + strTemp);
        //                data.Model.gtsensordata sensordata = (data.Model.gtsensordata)jss.Deserialize(strTemp, typeof(data.Model.gtsensordata));
        //                sensordata.project_name = model.xmno.ToString();
        //                ExceptionLog.ExceptionWrite("项目名" + sensordata.project_name);
        //                i++;
        //                //Console.WriteLine(mssg = sensordata.senorno +"=152217?");
        //                //if (sensordata.senorno.IndexOf("152217") != -1) { string a = ""; };
        //                processResultDataBLL.ProcessResultDataCalculate(sensordata, out mssg);
        //                ProcesGtSensorDataAdd(sensordata,out mssg);
        //                ExceptionLog.ExceptionWrite(mssg);
        //                //ProcessWaterLevelResultDataBLL(sensordata, out mssg);
        //                //ProcessStrainResultDataBLL(sensordata, out mssg);
        //                //ProcessSettlementResultDataBLL(sensordata, out mssg);

        //            }
        //            catch (Exception ex)
        //            {
        //                mssg += string.Format("{1}文件第{0}行存在错误", i, "#");
        //                ExceptionLog.ExceptionWrite(mssg);
        //                strTemp = sr.ReadLine();
        //                i++;
        //            }

        //        }
        //        if (sr != null)
        //            sr.Close();
        //        if (fs != null)
        //            fs.Close();






        //    }
        //    catch (Exception ex)
        //    {
        //        if (sr != null)
        //            sr.Close();
        //        if (fs != null)
        //            fs.Close();
        //        ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
        //        return false;
        //    }
        //    return true;


        //}

        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessResultDataBLL(SensorFileUploadConditionModel model, out string mssg)
        {


            ExceptionLog.ExceptionWrite("现在导入传感器数据...");
            int i = 0, t = 0;
            mssg = "";
            model.path = model.path.Substring(0, model.path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(model.path);
            FileStream fs = new FileStream(model.path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string strTemp = "";
                TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
                //记录下当前项目正在入库操作的周期
                DateTime currenttime = new DateTime();
                string currenttaskname = "";
                string currrentdatatype = "";
                List<data.Model.gtsensordata> tmplistcycdirnet = new List<data.Model.gtsensordata>();
                while (/*ThreadProcess.ThreadExist(string.Format("{0}cycdirnet", model.xmname)) &&*/ !sr.EndOfStream)
                {
                    try
                    {
                        strTemp = sr.ReadLine();
                        ExceptionLog.ExceptionWrite(strTemp + "----------");
                        data.Model.gtsensordata cycdirnet = (data.Model.gtsensordata)jss.Deserialize(strTemp, typeof(data.Model.gtsensordata));
                        ExceptionLog.ExceptionWrite("反序列化成功...");
                        cycdirnet.project_name = model.xmno.ToString();
                        //if (strTemp.IndexOf("2018") == -1) cycdirnet.time = cycdirnet.time.AddHours(8);

                        //cycdirnet.datatype = data.DAL.gtsensortype.GTSensorTypeToString(data.DAL.gtsensortype.GTStringToSensorType(cycdirnet.datatype));
                        //Console.WriteLine(mssg = "获取到类型为:" + cycdirnet.datatype);

                        processResultDataBLL.ProcessResultDataCalculate(cycdirnet, out mssg);
                        //ProcesGtSensorDataAdd(sensordata, out mssg);
                        ExceptionLog.ExceptionWrite(mssg);
                        //DateTime dt = Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time);
                        //Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time) = Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time).AddHours(8);

                        //NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement.ProcessSiblingPointBLL.SiblingpointnameGetModel siblingpointnameGetModel = new NFnet_BLL.FmosServer.ProcessFile.SurfaceDisplacement.ProcessSiblingPointBLL.SiblingpointnameGetModel(cycdirnet.POINT_NAME, cycdirnet.project_name);

                        ExceptionLog.ExceptionWrite(currenttime + "=?" + -1);
                        ExceptionLog.ExceptionWrite(currenttime + "=?" + Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time) + "&&" + currenttaskname + "=?" + cycdirnet.project_name);
                        ExceptionLog.ExceptionWrite(currenttime + "!=" + Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time) + "&&" + currenttaskname + "=?" + cycdirnet.project_name);
                        if (currenttime == new DateTime())
                        {
                            currenttaskname = cycdirnet.project_name;
                            currenttime = Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time);
                            currrentdatatype = cycdirnet.datatype;
                            tmplistcycdirnet.Add(cycdirnet);
                        }
                        else if (currenttime == Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time) && currenttaskname == cycdirnet.project_name && currrentdatatype == cycdirnet.datatype)
                        {
                            ExceptionLog.ExceptionWrite(currenttime + "==" + Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time) + "&&" + currenttaskname + "==" + cycdirnet.project_name + "&&" + currrentdatatype + "==" + cycdirnet.datatype);

                            tmplistcycdirnet.Add(cycdirnet);
                        }
                        else if ((currrentdatatype != cycdirnet.datatype) || (currenttime != Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time) && currenttaskname == cycdirnet.project_name && currrentdatatype == cycdirnet.datatype))
                        {
                            Console.WriteLine(mssg = currenttime + "=?" + Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time));
                            CycListImport(tmplistcycdirnet, model.xmno.ToString());
                            tmplistcycdirnet = new List<data.Model.gtsensordata>();
                            tmplistcycdirnet.Add(cycdirnet);
                            currenttime = Tool.DateHelper.DateTimePreciseToMinute(cycdirnet.time);
                            currenttaskname = cycdirnet.project_name;
                            currrentdatatype = cycdirnet.datatype;
                        }
                        ExceptionLog.ExceptionWrite("没找到符合条件的判断不做处理...");

                        i++;
                    }
                    catch (Exception ex)
                    {
                        mssg += string.Format("{1}文件第{0}行存在错误", ++i, "#");
                        ExceptionLog.ExceptionWrite(mssg);
                    }

                }
                ExceptionLog.ExceptionWrite("现在开始导入项目:" + model.xmname + tmplistcycdirnet.Count + "");
                CycListImport(tmplistcycdirnet, model.xmno.ToString());
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();

            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                ExceptionLog.ExceptionWrite("文件读取出错!错误信息" + ex.Message);
                return false;
            }
            return true;


        }





        /// <summary>
        /// 导入临时数据列表里的数据对象
        /// </summary>
        public void CycListImport(List<data.Model.gtsensordata> tmplistcycdirnet, string xmname)
        {
            string mssg = "";
            data.Model.gtsensortype datatype = data.DAL.gtsensortype.GTStringToSensorType(tmplistcycdirnet[0].datatype);
            if (datatype == data.Model.gtsensortype._staticLevel)
                processResultDataBLL.StaticLevelCalculate(Convert.ToInt32(xmname), tmplistcycdirnet);
            ProcessDeleteTmp(xmname, data.DAL.gtsensortype.GTStringToSensorType(tmplistcycdirnet[0].datatype), out mssg);

            ExceptionLog.ExceptionWrite("现在开始导入项目" + xmname + "的数据" + tmplistcycdirnet.Count + "条");

            List<DateTime> listdt = (from entry in tmplistcycdirnet select entry.time).ToList();
            listdt.Sort();//时间排序
            int t = 0;
            try
            {
                //如果记录存在则取消插入操作
                var IsCycdirnetDataExistModel = new ProcessResultDataBLL.IsCycdirnetDataExistModel(xmname, tmplistcycdirnet[0].point_name, tmplistcycdirnet[0].time, datatype);
                var xmmodel = GetXmconnectFromXmno(xmname);
                ExceptionLog.ExceptionWrite("获取到项目编号" + xmname + "的项目对象是" + xmmodel.dbname);
                //接入周期操作
                var IsSiblingDataModel = new ProcessResultDataBLL.IsTypeDataModel(xmname, listdt[listdt.Count - 1], datatype);
                //接段周期操作
                var IsInsertDataModel = new ProcessResultDataBLL.IsTypeDataModel(xmname, listdt[listdt.Count - 1], datatype);
                //本周期是否已经存在本次要插入点号的数据
                var points = (from m in tmplistcycdirnet select m.point_name).ToList();
                //NFnet_BLL.DataProcess.ProcessResultDataBLL.IsPointsDataExistInThisCycModel IsPointsDataExistInThisCycModel = null;
                var SiblingDataCycGetModel = new ProcessResultDataBLL.DataCycGetModel(xmname, listdt[listdt.Count - 1], datatype, points, 1);
                if (processResultDataBLL.IsCycdirnetDataExist(IsCycdirnetDataExistModel, out mssg))
                {
                    ExceptionLog.ExceptionWrite(mssg);
                    //foreach (var datamodel in tmplistcycdirnet)
                    //{

                    //    for (t = 0; t < 2; t++)
                    //    {

                    //        if (processResultDataBLL.(datamodel, out mssg)) { ExceptionLog.ExceptionWrite(mssg); break; }
                    //        ExceptionLog.ExceptionWrite(mssg);
                    //        ExceptionLog.DTUPortInspectionWrite(string.Format("更新项目{0}点名{1}测量时间{2}的表面位移—全站仪测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.project_name, datamodel.POINT_NAME, datamodel.Time, t));
                    //        Thread.Sleep(1000);

                    //    }
                    //}
                    return;
                }
                if (processResultDataBLL.SiblingDataCycGet(SiblingDataCycGetModel, out mssg))
                {
                    ExceptionLog.ExceptionWrite(mssg);
                    //执行周期数据操作
                    foreach (var datamodel in tmplistcycdirnet)
                    {
                        datamodel.cyc = SiblingDataCycGetModel.cyc;
                        for (t = 0; t < 2; t++)
                        {

                            if (processResultDataBLL.ProcessResultDataAdd(datamodel, out mssg)) { ExceptionLog.ExceptionWrite(mssg); break; };
                            ExceptionLog.DTUPortInspectionWrite(string.Format("导入项目{0}点名{1}测量时间{2}的" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.project_name, datamodel.point_name, datamodel.time, t));
                            Thread.Sleep(1000);

                        }
                    }
                }
                else if (processResultDataBLL.IsInsertData(IsInsertDataModel, out mssg))
                {
                    ExceptionLog.ExceptionWrite(mssg);
                    //获取接段周期
                    var InsertDataCycGetModel = new ProcessResultDataBLL.InsertDataCycGetModel(xmname, listdt[listdt.Count - 1], datatype);
                    if (!processResultDataBLL.InsertDataCycGet(InsertDataCycGetModel, out mssg)) { ExceptionLog.ExceptionWrite(mssg); return; }
                    //如果接段周期有空位则无需做周期移位操作
                    var IsInsertCycExistModel = new ProcessResultDataBLL.IsInsertCycExistModel(xmname, InsertDataCycGetModel.cyc - 1, datatype);
                    if (InsertDataCycGetModel.cyc == 1 || processResultDataBLL.IsInsertCycExist(IsInsertCycExistModel, out mssg))
                    {
                        //执行周期后移操作
                        var InsertCycStepModel = new ProcessResultDataBLL.InsertCycStepModel(xmname, InsertDataCycGetModel.cyc, datatype);
                        if (!processResultDataBLL.InsertCycStep(InsertCycStepModel, out mssg))
                        {
                            ExceptionLog.ExceptionWrite(mssg); return;
                        }
                    }
                    else
                    {
                        InsertDataCycGetModel.cyc -= 1;
                    }
                    //执行周期数据操作
                    foreach (var datamodel in tmplistcycdirnet)
                    {
                        datamodel.cyc = InsertDataCycGetModel.cyc;
                        for (t = 0; t < 2; t++)
                        {

                            if (processResultDataBLL.ProcessResultDataAdd(datamodel, out mssg)) { ExceptionLog.ExceptionWrite(mssg); break; }
                            ExceptionLog.DTUPortInspectionWrite(string.Format("导入项目{0}点名{1}测量时间{2}的" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.project_name, datamodel.point_name, datamodel.time, t));
                            Thread.Sleep(1000);

                        }
                    }
                    //清空临时数据列表
                }
                else
                {
                    ExceptionLog.ExceptionWrite("直接插入数据");
                    //非接段数据则获取当前库中该项目的最大周期
                    var ResultDataExtremelyCondition = new NFnet_BLL.DisplayDataProcess.GT.ProcessResultDataBLL.ResultDataExtremelyCondition(xmname, "max", datatype);
                    if (!processResultDataBLL.ProcessResultDataExtremely(ResultDataExtremelyCondition, out mssg))
                    { ExceptionLog.ExceptionWrite(mssg); return; }

                    int maxcyc = Convert.ToInt32(ResultDataExtremelyCondition.extremelyCyc) + 1;
                    //执行周期数据操作
                    foreach (var datamodel in tmplistcycdirnet)
                    {
                        datamodel.cyc = maxcyc;
                        for (t = 0; t < 2; t++)
                        {
                            if (processResultDataBLL.ProcessResultDataAdd(datamodel, out mssg)) break;
                            ExceptionLog.DTUPortInspectionWrite(string.Format("导入项目{0}点名{1}测量时间{2}的" + data.DAL.gtsensortype.GTSensorTypeToString(datatype) + "测量数据失败,现在排队等待1秒进行第{3}次添加操作", datamodel.project_name, datamodel.point_name, datamodel.time, t));
                            Thread.Sleep(1000);

                        }
                    }


                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("数据导入出错,错误信息:" + ex.Message + "位置:" + ex.StackTrace);
                string a = "";
            }
        }


        public static int GetXmnoFromXmname(string xmname)
        {
            string mssg = "";
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();
            if (bll.GetModel(xmname, out model, out mssg))
            {
                return model.xmno;
            }
            else
            {
                mssg = "不存在该项目名称的项目编号";
                return -1;
            }
        }





        public bool ProcesGtSensorDataAdd(data.Model.gtsensordata model, out string mssg)
        {
            return gtsensordatabll.Add(model, out mssg);
        }
        public void threadtest()
        {
            ExceptionLog.ExceptionWrite("我是线程...");
            //alter = "我是线程...";
        }
        string alter = "";
        /// <summary>
        /// 传感器数据导入处理主程序
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessFileDecodeTxt(ProcessFileDecodeTxtModel model, out string mssg)
        {
            //ThreadPool.UnsafeQueueUserWorkItem(state => { ExceptionLog.ExceptionWrite("我是线程..."); });
            //Thread thead = new Thread(threadtest);
            //thead.Start();
            //Thread.Sleep(5000);
            //mssg = "";
            //return true;
            List<string> ls = Tool.FileHelper.ProcessFileInfoList(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");

            mssg = "";
            ExceptionLog.ExceptionWrite("开始检验数据...");
            switch (model.fileType)
            {
                case "金马设备数据":
                    if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));

                        bool ipass = Tool.GTXLSHelper.GTSensorDataFileDataImportCheck(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), out mssg);
                        var processGTSensorXlsDataDirImportModel = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL_GT.ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno, model.xmname);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", model.xmname)))
                        //{
                        string dataimportsetting = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
                        Tool.FileHelper.FileStringAppendWrite(string.Format("data descode request?{0}", jss.Serialize(model)), System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析列表\\datadescodelist.txt");
                        Tool.FileHelper.FileStringWrite(jss.Serialize(model), dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt");
                        ExceptionLog.ExceptionWrite("数据文件导入任务文件名【" + dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt" + "】");
                        //Thread t = new Thread(new ParameterizedThreadStart(ProcessGTSensorXlsDataDirImport));
                        //    t.Name = string.Format("GTresultdataimport", model.xmname);
                        //t.Start(processGTSensorXlsDataDirImportModel);
                        //Tool.TcpServer.SendMessToServer(string.Format("data descode request?{0}",jss.Serialize(model)),ls[0],ls[1]);
                        //ProcessGTSensorXlsDataDirImport(processGTSensorXlsDataDirImportModel);

                        //    model.thread = t;
                        //}
                        return ipass;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx")
                    {

                        bool ipass = Tool.GTXLSHelper.GTSensorDataFileDataImportCheck(model.path, out mssg);
                        var processGTSensorXlsDataDirImportModel = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL_GT.ProcessXlsDataDirImportModel(model.path, model.xmno, model.xmname);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", model.xmname)))
                        //{
                        //Tool.TcpServer.SendMessToServer(string.Format("data descode request?{0}" ,jss.Serialize(model)), ls[0], ls[1]);
                        //获取数据导入任务列表
                        string dataimportsetting = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
                        Tool.FileHelper.FileStringAppendWrite(string.Format("data descode request?{0}", jss.Serialize(model)), System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析列表\\datadescodelist.txt");
                        Tool.FileHelper.FileStringWrite(jss.Serialize(model), dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt");
                        ExceptionLog.ExceptionWrite("数据文件导入任务文件名【" + dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt" + "】");
                        //Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsFileDataDirImport));
                        //t.Start(processGTSensorXlsDataDirImportModel);
                        //t.Name = string.Format("GTresultdataimport", model.xmname);
                        //ProcessXlsFileDataDirImport(processGTSensorXlsDataDirImportModel);
                        //    model.thread = t;
                        //}
                        return ipass;
                    }
                    else
                    {


                        bool ispass = Tool.ObjectHelper.ProcessTextFileCheck<data.Model.gtsensordata>(model.path, out mssg);
                        var sensorDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);
                        //SenosorDataImport(sensorDataImportModel);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}sensor", model.xmname)))
                        //{
                        //ExceptionLog.ExceptionWrite("开始使用线程导入数据...");
                        //try
                        //{
                        //Thread t = new Thread(new ParameterizedThreadStart(SenosorDataImport));
                        //t.Name = string.Format("{0}resultdataimport", model.xmname);
                        //t.Start(sensorDataImportModel);
                        //model.thread = t;
                        //}
                        //catch (Exception ex)
                        //{
                        //    ExceptionLog.ExceptionWrite("线程执行出错，错误信息：" + ex.Message);
                        //}
                        //}
                        //SenosorDataImport():
                        //SenosorDataImport(sensorDataImportModel);
                        string dataimportsetting = Tool.FileHelper.ProcessSettingString(System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "Setting\\delegatedescodesetting.txt");
                        ExceptionLog.ExceptionWrite("现在写入解析列表...");
                        Tool.FileHelper.FileStringAppendWrite(string.Format("data descode request?{0}", jss.Serialize(model)), System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "数据文件解析列表\\datadescodelist.txt");
                        Tool.FileHelper.FileStringWrite(jss.Serialize(model), dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt");
                        ExceptionLog.ExceptionWrite("数据文件导入任务文件名【" + dataimportsetting + "\\采集端数据文件任务表\\" + System.IO.Path.GetFileNameWithoutExtension(model.path) + ".txt" + "】");
                        //Tool.TcpServer.SendMessToServer(string.Format("data descode request?{0}",jss.Serialize(model)), ls[0], ls[1]);
                        return ispass;
                    }

                default: return false;

            }

        }


        public void ProcessFileDecodeTxtDescode(ProcessFileDecodeTxtModel model, out string mssg)
        {
            //ThreadPool.UnsafeQueueUserWorkItem(state => { ExceptionLog.ExceptionWrite("我是线程..."); });
            //Thread thead = new Thread(threadtest);
            //thead.Start();
            //Thread.Sleep(5000);
            //mssg = "";
            //return true;
            mssg = "";
            ExceptionLog.ExceptionWrite("开始解析数据...");
            switch (model.fileType)
            {
                case "金马设备数据":
                    if (Path.GetExtension(model.path) == ".zip" || Path.GetExtension(model.path) == ".rar")
                    {
                        string dirpath = Path.GetDirectoryName(model.path);
                        Tool.FileHelper.unZipFile(model.path, dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path));

                        //bool ipass = Tool.GTXLSHelper.GTSensorDataFileDataImportCheck(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), out mssg);
                        var processGTSensorXlsDataDirImportModel = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL_GT.ProcessXlsDataDirImportModel(dirpath + "\\" + Path.GetFileNameWithoutExtension(model.path), model.xmno, model.xmname);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", model.xmname)))
                        //{

                        //Thread t = new Thread(new ParameterizedThreadStart(ProcessGTSensorXlsDataDirImport));
                        //    t.Name = string.Format("GTresultdataimport", model.xmname);
                        //t.Start(processGTSensorXlsDataDirImportModel);
                        //Tool.TcpServer.SendMessToServer();
                        List<string> filenamelist = Tool.FileHelper.DirTravel(processGTSensorXlsDataDirImportModel.dirpath);
                        if (Path.GetExtension(filenamelist[0]) == ".txt")
                        {
                            foreach (string filename in filenamelist)
                            {
                                if (Path.GetExtension(filename) != ".txt") continue;

                                var sensorDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);

                                SenosorDataImport(sensorDataImportModel);
                            }
                        }

                        else
                            ProcessGTSensorXlsDataDirImport(processGTSensorXlsDataDirImportModel);

                        //    model.thread = t;
                        //}
                        //return ipass;
                    }
                    else if (Path.GetExtension(model.path) == ".xls" || Path.GetExtension(model.path) == ".xlsx")
                    {

                        bool ipass = Tool.GTXLSHelper.GTSensorDataFileDataImportCheck(model.path, out mssg);
                        var processGTSensorXlsDataDirImportModel = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL_GT.ProcessXlsDataDirImportModel(model.path, model.xmno, model.xmname);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}GT_cycdirnet", model.xmname)))
                        //{

                        //Thread t = new Thread(new ParameterizedThreadStart(ProcessXlsFileDataDirImport));
                        //t.Start(processGTSensorXlsDataDirImportModel);
                        //t.Name = string.Format("GTresultdataimport", model.xmname);
                        ProcessXlsFileDataDirImport(processGTSensorXlsDataDirImportModel);
                        //    model.thread = t;
                        //}
                        //return ipass;
                    }
                    else
                    {


                        //bool ispass = Tool.ObjectHelper.ProcessTextFileCheck<data.Model.gtsensordata>(model.path, out mssg);
                        var sensorDataImportModel = new DataImportModel(model.xmname, model.xmno, model.path);
                        //SenosorDataImport(sensorDataImportModel);
                        //if (!ThreadProcess.ThreadExist(string.Format("{0}sensor", model.xmname)))
                        //{
                        //ExceptionLog.ExceptionWrite("开始使用线程导入数据...");
                        //try
                        //{
                        //Thread t = new Thread(new ParameterizedThreadStart(SenosorDataImport));
                        //t.Name = string.Format("{0}resultdataimport", model.xmname);
                        //t.Start(sensorDataImportModel);
                        //model.thread = t;
                        //}
                        //catch (Exception ex)
                        //{
                        //    ExceptionLog.ExceptionWrite("线程执行出错，错误信息：" + ex.Message);
                        //}
                        //}
                        //SenosorDataImport():
                        SenosorDataImport(sensorDataImportModel);

                        //return ispass;
                    }
                    return;
                default: return;

            }

        }

        public ProcesSurfaceDataAlarmBLL procesSurfaceDataAlarmBLL = new ProcesSurfaceDataAlarmBLL();
        public void SenosorDataImport(object obj)
        {
            NFnet_BLL.DisplayDataProcess.pub.ProcessEmailSendBLL emailSendBLL = new NFnet_BLL.DisplayDataProcess.pub.ProcessEmailSendBLL();
            ProcessResultDataAlarmBLL resultDataBLL = new ProcessResultDataAlarmBLL();
            try
            {
                ExceptionLog.ExceptionWrite("正在导入数据...");
                DataImportModel model = obj as DataImportModel;
                //Tool.ThreadProcess.Threads.Add(string.Format("{0}sensor", model.xmname));
                string mssg = "";
                var sensorFileUploadConditionModel = new SensorFileUploadConditionModel(model.xmname, model.xmno, model.path);

                if (!ProcessResultDataBLL(sensorFileUploadConditionModel, out mssg))
                {
                    return;
                }
                else
                {

                    List<string> alarmInfoList = resultDataBLL.ResultDataAlarm(model.xmname, model.xmno);
                    //emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
                    alarmInfoList.AddRange(procesSurfaceDataAlarmBLL.ResultDataAlarm(model.xmname, model.xmno));
                    emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);
                }
                //Tool.ThreadProcess.Threads.Remove(string.Format("{0}sensor", model.xmname));
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("导入数据出错,错误信息:" + ex.Message);
            }


        }
        public class DataImportModel
        {
            public string xmname { get; set; }
            public int xmno { get; set; }
            public string path { get; set; }
            public DataImportModel(string xmname, int xmno, string path)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
            }
        }



        /// <summary>
        /// 传感器数据导入处理主程序处理参数实体类
        /// </summary>
        public class ProcessFileDecodeTxtModel : SensorFileUploadConditionModel
        {
            /// <summary>
            /// 文件类型
            /// </summary>
            public string fileType { get; set; }
            public Thread thread { get; set; }
            /// <summary>
            /// 项目编号
            /// </summary>
            public int xmno { get; set; }
            public ProcessFileDecodeTxtModel(string xmname, int xmno, string path, string fileType)
            {
                this.xmname = xmname;
                this.xmno = xmno;
                this.path = path;
                this.fileType = fileType;
            }
            public ProcessFileDecodeTxtModel()
            {

            }
        }

        public bool ProcessGTSensorXlsDataImport(string xlspath, int xmno, out int cont, out string mssg, out string outmssgimport)
        {

            cont = 0;
            outmssgimport = "";
            string mssgimport = "";
            GTXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);
            DataTable dt = new DataTable();
            GTXLSHelper.ProcessGTSensorDataDataTableImport(xlspath, out dt, out mssg);
            DataView dv = new DataView(dt);
            if (dt.Rows.Count == 0) return false;
            List<string> datatypelist = Tool.DataTableHelper.ProcessDataTableFilter(dt, "datatype");
            List<DataTable> datatablelist = Tool.DataTableHelper.ProcessGTSensorDataDataTableSplit(dt);
            List<string> lshead = new List<string>();
            string strTemp = "";
            TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
            //记录下当前项目正在入库操作的周期
            DateTime currenttime = new DateTime();
            string currenttaskname = "";
            string currrentdatatype = "";
            List<data.Model.gtsensordata> tmplistsensordata = new List<data.Model.gtsensordata>();
            foreach (DataTable datatable in datatablelist)
            {
                ProcessDeleteTmp(xmno, data.DAL.gtsensortype.GTStringToSensorType(data.DAL.gtsensortype.ImportStringToGTString(datatable.Rows[0].ItemArray[1].ToString())), out mssg);

                int i = 0;
                foreach (DataRow dr in datatable.Rows)
                {

                    try
                    {
                        data.Model.gtsensordata sensordata = new global::data.Model.gtsensordata
                        {

                            project_name = xmno.ToString(),
                            senorno = dr["point_name"].ToString(),
                            time = Convert.ToDateTime(dr["time"]),
                            single_oregion_scalarvalue = Convert.ToDouble(dr["single_region_scalarvalue"]),
                            first_oregion_scalarvalue = Convert.ToDouble(dr["first_region_scalarvalue"]),
                            sec_oregion_scalarvalue = dr["sec_region_scalarvalue"] == null || dr["sec_region_scalarvalue"].ToString() == "" ? 0 : Convert.ToDouble(dr["sec_region_scalarvalue"]),
                            datatype = data.DAL.gtsensortype.ImportStringToGTString(dr["datatype"].ToString())


                        };
                        i++;
                        processResultDataBLL.ProcessResultDataCalculate(sensordata, out mssg);

                        ExceptionLog.ExceptionWrite(currenttime + "=?" + -1);
                        ExceptionLog.ExceptionWrite(currenttime + "=?" + Tool.DateHelper.DateTimePreciseToMinute(sensordata.time) + "&&" + currenttaskname + "=?" + sensordata.project_name);
                        ExceptionLog.ExceptionWrite(currenttime + "!=" + Tool.DateHelper.DateTimePreciseToMinute(sensordata.time) + "&&" + currenttaskname + "=?" + sensordata.project_name);
                        if (currenttime == new DateTime())
                        {
                            currenttaskname = sensordata.project_name;
                            currenttime = Tool.DateHelper.DateTimePreciseToMinute(sensordata.time);
                            currrentdatatype = sensordata.datatype;
                            tmplistsensordata.Add(sensordata);
                        }
                        else if (currenttime == Tool.DateHelper.DateTimePreciseToMinute(sensordata.time) && currenttaskname == sensordata.project_name && currrentdatatype == sensordata.datatype)
                        {
                            ExceptionLog.ExceptionWrite(currenttime + "==" + Tool.DateHelper.DateTimePreciseToMinute(sensordata.time) + "&&" + currenttaskname + "==" + sensordata.project_name + "&&" + currrentdatatype + "==" + sensordata.datatype);

                            tmplistsensordata.Add(sensordata);
                        }
                        else if (currenttime != Tool.DateHelper.DateTimePreciseToMinute(sensordata.time) && currenttaskname == sensordata.project_name && currrentdatatype == sensordata.datatype)
                        {
                            Console.WriteLine(mssg = currenttime + "=?" + Tool.DateHelper.DateTimePreciseToMinute(sensordata.time));
                            CycListImport(tmplistsensordata, xmno.ToString());
                            tmplistsensordata = new List<data.Model.gtsensordata>();
                            tmplistsensordata.Add(sensordata);
                            currenttime = Tool.DateHelper.DateTimePreciseToMinute(sensordata.time);
                            currenttaskname = sensordata.project_name;
                        }
                        ExceptionLog.ExceptionWrite("没找到符合条件的判断不做处理...");

                        i++;
                    }
                    catch (Exception ex)
                    {
                        mssg += string.Format("{1}文件第{0}行存在错误", ++i, "#");
                        ExceptionLog.ExceptionWrite(mssg);
                    }

                }

                //ExceptionLog.ExceptionWrite("现在开始导入项目:" + process + tmplistcycdirnet.Count + "");
                CycListImport(tmplistsensordata, xmno.ToString());


            }
            //ExceptionLog.TotalSationPointCheckVedioWrite("项目"+xmname+"共导入全站仪结果数据数据"+cont+"条");
            return true;
        }

        public bool ProcessGTSensorXlsDataDelete(string xlspath, out string mssg, out string outmssgimport)
        {


            outmssgimport = "";
            mssg = "";
            //            string mssgimport = "";
            //            GTXLSHelper.PublicCompatibleInitializeWorkbook(xlspath);
            //            NFnet_BLL.DataProcess.ProcessResultDataBLL processResultDataBLL = new DataProcess.ProcessResultDataBLL();
            //            List<Tool.GTXLSHelper.DeleteDataFromXlsOutInfoModel> modellist = null;
            //            GTXLSHelper.ProcessXlsCycDataDelete(xlspath, out modellist, out mssg);

            //            foreach (var model in modellist)
            //            {

            //                //测量库
            //                var processDateTimeCycGetModel = new NFnet_BLL.DataProcess.ProcessResultDataBLL.ProcessDateTimeCycGetModel
            //(ProcessAspectIndirectValue.GetXmnoFromXmnameStr(model.xmname), model.dt);
            //                processResultDataBLL.ProcessDateTimeCycGet(processDateTimeCycGetModel, out mssg);
            //                var cycdirnetCYCDataDeleteModel = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessCycdirnetCYCDataDeleteModel(processDateTimeCycGetModel.cyc, processDateTimeCycGetModel.cyc, model.xmname, "");
            //                ProcessSurveyCycdirnetCYCDataDelete(cycdirnetCYCDataDeleteModel, out mssg);
            //                ExceptionLog.ExceptionWrite(mssg);
            //                //成果库
            //                processResultDataBLL.ProcessCgDateTimeCycGet(processDateTimeCycGetModel, out mssg);
            //                cycdirnetCYCDataDeleteModel = new NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL.ProcessCycdirnetCYCDataDeleteModel(processDateTimeCycGetModel.cyc, processDateTimeCycGetModel.cyc, model.xmname, "");
            //                ProcessCgCycdirnetCYCDataDelete(cycdirnetCYCDataDeleteModel, out mssg);
            //                ExceptionLog.ExceptionWrite(mssg);

            //            }
            return true;
        }



        public void ProcessXlsFileDataDirImport(object obj)
        {
            ExceptionLog.ExceptionWrite("用线程导入数据...");
            NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL_GT.ProcessXlsDataDirImportModel model = obj as NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL_GT.ProcessXlsDataDirImportModel;
            //Tool.ThreadProcess.Threads.Add(string.Format("{0}GT_cycdirnet", model.xmname));
            string filename = model.dirpath;
            int xmno = model.xmno;
            //mssg = "";
            bool result = true;
            string filemssg = "";
            string filemiportmssg = "";
            string mssg = "";
            ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
            mssg = "";
            int cont = 0;
            int totalcount = 0;
            string importmssg = "";
            if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
            {
                ProcessGTSensorXlsDataImport(filename, xmno, out cont, out mssg, out importmssg);
                totalcount += cont;
                ExceptionLog.TotalSationPointCheckVedioWrite("项目" + model.xmname + "共导入全站仪结果数据数据" + totalcount + "条");
                filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg);
                filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg);
                ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg));
                ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg));
            }


            ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            ProcessResultDataAlarmBLL processResultDataAlarmBLL = new ProcessResultDataAlarmBLL();
            List<string> alarmInfoList = processResultDataAlarmBLL.ResultDataAlarm(model.xmname, model.xmno);
            NFnet_BLL.DisplayDataProcess.pub.ProcessEmailSendBLL emailSendBLL = new NFnet_BLL.DisplayDataProcess.pub.ProcessEmailSendBLL();
            emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);

            //Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_cycdirnet", model.xmname));
            //return result;
        }
        public void ProcessXlsFileDataDirDescode(object obj)
        {
            //ProcessGTSensorXlsDataDirImportModel model = obj as ProcessGTSensorXlsDataDirImportModel;
            //string filename = model.dirpath;
            //int xmno = model.xmno;
            ////mssg = "";
            //bool result = true;
            //string filemssg = "";
            //string filemiportmssg = "";
            //string mssg = "";
            //string importmssg = "";
            //if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
            //{
            //    ProcessGTSensorXlsDataDelete(filename, out mssg, out importmssg);

            //    filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除完成!" : mssg);
            //    filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除完成!" : importmssg);
            //    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除完成!" : mssg));
            //    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除完成!" : importmssg));
            //}


            //ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            //mssg = mssg.Replace("{0}", "");
        }


        public class ProcessXlsDataDirImportModel
        {
            public string dirpath { get; set; }
            public int xmno { get; set; }
            public string xmname { get; set; }
            public data.Model.gtsensortype datatype { get; set; }
            public ProcessXlsDataDirImportModel(string dirpath, data.Model.gtsensortype datatype, int xmno, string xmname)
            {
                this.dirpath = dirpath;
                this.xmno = xmno;
                this.xmname = xmname;
            }
        }


        public static Authority.Model.xmconnect GetXmconnectFromXmno(string xmname)
        {
            ProcessXmBLL processXmBLL = new ProcessXmBLL();
            string mssg = "";
            var processXmInfoLoadModel = new ProcessXmBLL.ProcessXmIntInfoLoadModel(Convert.ToInt32(xmname));
            List<Authority.Model.xmconnect> lmx = new List<Authority.Model.xmconnect>();
            if (processXmBLL.ProcessXmInfoLoad(processXmInfoLoadModel, out mssg))
            {
                return processXmInfoLoadModel.model;
            }
            return new Authority.Model.xmconnect();
        }

        public void ProcessGTSensorXlsDataDirImport(object obj)
        {


            NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL_GT.ProcessXlsDataDirImportModel model = obj as NFnet_BLL.DataImport.ProcessFile.SurfaceDisplacement.ProcessTotalStationBLL_GT.ProcessXlsDataDirImportModel;
            string dirpath = model.dirpath;
            int xmno = model.xmno;
            string mssg = "";
            bool result = true;
            //string[] filenames = Directory.GetFiles(dirpath);
            List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            string filemssg = "";
            string filemiportmssg = "";
            string importmssg = "";
            ExceptionLog.TotalSationPointCheckVedioWrite(mssg);
            mssg = "";
            int cont = 0;
            int totalcount = 0;
            foreach (string filename in filenamelist)
            {
                if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
                {
                    ProcessGTSensorXlsDataImport(filename, xmno, out cont, out mssg, out importmssg);
                    totalcount += cont;
                    ExceptionLog.TotalSationPointCheckVedioWrite("项目" + model.xmname + "共导入全站仪结果数据数据" + totalcount + "条");
                    filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg);
                    filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg);
                    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据导入完成!" : mssg));
                    ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据导入完成!" : importmssg));
                }

            }
            ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            mssg = mssg.Replace("{0}", "");
            ProcessResultDataAlarmBLL processResultDataAlarmBLL = new ProcessResultDataAlarmBLL();
            List<string> alarmInfoList = processResultDataAlarmBLL.ResultDataAlarm(model.xmname, model.xmno);
            NFnet_BLL.DisplayDataProcess.pub.ProcessEmailSendBLL emailSendBLL = new NFnet_BLL.DisplayDataProcess.pub.ProcessEmailSendBLL();
            emailSendBLL.ProcessEmailSend(alarmInfoList, model.xmname, model.xmno, out mssg);

            //Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_cycdirnet", model.xmname));
            //return result;
        }
        public void ProcessGTSensorXlsDataDirDescode(object obj)
        {
            //ProcessGTSensorXlsDataDirImportModel model = obj as ProcessGTSensorXlsDataDirImportModel;
            //string dirpath = model.dirpath;
            //int xmno = model.xmno;
            ////mssg = "";
            //bool result = true;
            ////string[] filenames = Directory.GetFiles(dirpath);
            //List<string> filenamelist = Tool.FileHelper.DirTravel(dirpath);
            //string filemssg = "";
            //string filemiportmssg = "";
            //string mssg = "";
            //string importmssg = "";
            //foreach (string filename in filenamelist)
            //{
            //    if (Path.GetExtension(filename) == ".xls" || Path.GetExtension(filename) == ".xlsx")
            //    {
            //        ProcessGTSensorXlsDataDelete(filename, out mssg, out importmssg);

            //        filemssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除成功!" : mssg);
            //        filemiportmssg += string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除成功!" : importmssg);
            //        ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, mssg == "" ? "数据删除成功!" : mssg));
            //        ExceptionLog.ExceptionWrite(string.Format("\r\n文件{0}------------------\r\n{1}", filename, importmssg == "" ? "数据删除成功!" : importmssg));
            //    }

            //}
            //ExceptionLog.ExceptionWrite(filemssg + "==============================" + filemiportmssg);
            //mssg = mssg.Replace("{0}", "");
            //Tool.ThreadProcess.Threads.Remove(string.Format("{0}GT_cycdirnet", model.xmname));
            //return result;
        }

        public bool ProcessDeleteTmp(int xmno, data.Model.gtsensortype datatype, out string mssg)
        {
            var deletetempmodel = new NFnet_BLL.DisplayDataProcess.GT.ProcessResultDataBLL.DeleteTmpModel(xmno.ToString(), datatype);
            return processResultDataBLL.DeleteTmp(deletetempmodel, out mssg);
        }
        public bool ProcessDeleteTmp(string xmname, data.Model.gtsensortype datatype, out string mssg)
        {
            var deletetempmodel = new NFnet_BLL.DisplayDataProcess.GT.ProcessResultDataBLL.DeleteTmpModel(xmname, datatype);
            return processResultDataBLL.DeleteTmp(deletetempmodel, out mssg);
        }
    }
}