﻿using System;
using System.Collections.Generic;
using Tool;
using System.IO;
using System.Web.Script.Serialization;
using NFnet_BLL.UserProcess;
using System.Linq;
using NFnet_BLL.AuthorityAlarmProcess;
using System.Threading;
using NFnet_BLL.DisplayDataProcess.pub;
using NFnet_BLL.DataImport.ProcessFileSenmos;
using NFnet_BLL.DisplayDataProcess.Strain.Sensor;
using NFnet_BLL.DataProcess.Strain.Sensor;
using NFnet_BLL.GTSensorServer;
namespace NFnet_BLL.DataImport.ProcessFile
{
    /// <summary>
    /// 全站仪数据导入处理类
    /// </summary>
    public partial class ProcessGTSensorDataBLL
    {
        public ProcessResultDataBLL processStrainDataBLL = new ProcessResultDataBLL();

        /// <summary>
        /// 结果数据导入
        /// </summary>
        /// <param name="model">全站仪文件上传条件</param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ProcessStrainResultDataBLL(data.Model.gtsensordata gtsensordata, out string mssg)
        {
            mssg = "";
            if (gtsensordata.datatype != "应力") return false;

            global::Strain.Model.strainresultdata strainresultdata = new global::Strain.Model.strainresultdata
            {
                xmno = GetXmnoFromXmname(gtsensordata.project_name),
                this_val = gtsensordata.single_this_scalarvalue,
                ac_val = gtsensordata.single_ac_scalarvalue,
                strengthVal = gtsensordata.single_oregion_scalarvalue,
                monitorTime = gtsensordata.time,
                cyc = gtsensordata.cyc,
                pointname = gtsensordata.point_name

            };


           return  processStrainDataBLL.ProcessResultDataAdd(strainresultdata,out mssg);
        }






    }

}