﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
namespace NFnet_BLL
{
    /// <summary>
    /// 连接数据库
    /// </summary>D:\工具\安装环境\数据备份\gps发布版\DAY02\css\
    public class database
    {

        public static string constr = "Dsn=ngnconn;description=ngn;server=localhost;uid=root;database=ngn;port=3308";
        public static string sqlconlstr = "Dsn=gpsdata;description=gps;trusted_connection=Yes;app=Microsoft® Visual Studio® 2010;wsid=PC-PC;database=2014db";
        public static string sqlconstr = "Dsn=db2015;description=gps;trusted_connection=Yes;app=Microsoft® Visual Studio® 2010;wsid=PC-PC;database=SMOSDB2015";
        public static accessdbse adb = new accessdbse();

        /// <summary>
        /// 根据项目名称获取到该项目所属数据库的连接语句
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetXmConn(string dataBaseName)
        {
            return TargetBaseLink.GetOdbcConnect(dataBaseName);
        }

        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public  OdbcConnection GetStanderConn(string xmname)
        {
            string dbname = querysql.queryaccessdbstring("select dbase from xmconnect where xmname ='" + xmname + "'");
            //querysql.querystanderdb("");
            OdbcConnection conn = TargetBaseLink.GetOdbcConnect(dbname);
            return conn;
        }

        /// <summary>
        /// 根据项目名称获取标准数据库表的连接器
        /// </summary>
        /// <returns></returns>
        public MySqlConnection GetMySqlStanderConn(string dbname)
        {
          
            MySqlConnection conn = TargetBaseLink.GetMysqlConnect(dbname);
            return conn;
        }







        public OdbcConnection getdbconn()
        {
            return TargetBaseLink.GetOdbcConnect("fmosjc");

        }
        /// <summary>
        /// 获取数据库最基本数据库的连接语句
        /// </summary>
        /// <returns></returns>
        public OdbcConnection getshemadbconn()
        {
            constr = "Driver={MySQL ODBC 5.3 ANSI Driver};database=performance_schema;uid=root;password=root;server=localhost;port=3308";//发布开发版
            //constr = "Dsn=ngn;server=localhost;uid=root;password=root;database=ngn;port=3308";//非发布开发版
            //生成数据库连接字符串
            /*OdbcConnectionStringBuilder scsb = new OdbcConnectionStringBuilder();
            scsb["Server"] = @"localhost";
            scsb. = "root";
            scsb.Password = "";
            scsb.InitialCatalog = "";//不设定要打开那个数据库
            scsb["database"] = dbName;
            if (scsb.UserID == "")
            {
                scsb.IntegratedSecurity = true;
            }
            else
            {
                scsb.IntegratedSecurity = false;
            }
            return scsb.ConnectionString;*/
            //使用odbc连接数据库
            OdbcConnection conn = new OdbcConnection(constr);
            System.Console.WriteLine("数据库连接成功!\n");
            return conn;

        }
        //mysql数据库连接功能函数
        public OdbcConnection getdbconn(string constr)
        {
            constr = "Driver={MySQL ODBC 5.3 ANSI Driver};database=ngn;uid=root;password=root;description=ngn;server=localhost;port=3308";//发布开发版
            OdbcConnection conn = new OdbcConnection(constr);
            System.Console.WriteLine("数据库连接成功!\n");
            return conn;

        }
        ////连接sqlserver数据库
        public OdbcConnection getsqldbconn()
        {
            sqlconstr = "Driver={SQL Server};Server=pc-pc;Database=SMOSDB2015;user id=sa;Password=gnss;Trusted_Connection=No";
            OdbcConnection conn = new OdbcConnection(sqlconstr);
            SqlConnection connsql = getsqlconnstr();
            connsql.Open();
            return conn;
        }
        /// <summary>
        /// 跨年查询连接,根据查询的时间是否跨年进行数据库连接
        /// </summary>
        /// <returns></returns>
        public SqlConnection getsqlconnstr(string year/*2014、2015*/)
        {
            SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            scsb.DataSource = @"pc-PC\SQLEXPRESS";
            scsb.IntegratedSecurity = true;
            scsb.InitialCatalog = "";
            scsb.UserID = "sa";
            scsb.Password = "gnss";
            if (year == "2014")
                scsb["database"] = "SMOSDB2014";
            if (year == "2015")
                scsb["database"] = "SMOSDB2015";
            if (scsb.UserID == "")
            {
                scsb.IntegratedSecurity = true;
            }
            else
            {
                scsb.IntegratedSecurity = false;
            }
            SqlConnection myConnection = new SqlConnection(scsb.ConnectionString);
            return myConnection;

        }
        /// <summary>
        /// 默认查询2015年的数据
        /// </summary>
        /// <returns></returns>
        public SqlConnection getsqlconnstr()
        {
            //SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            //scsb.DataSource = @"pc-pc\SQLExpress";
            //scsb.IntegratedSecurity = true;
            //scsb.InitialCatalog = "";
            //scsb.UserID = "sa";
            //scsb.Password = "gnss";
            //scsb["database"] = "SMOSDB2015";
            //if (scsb.UserID == "")
            //{
            //    scsb.IntegratedSecurity = true;
            //}
            //else
            //{
            //    scsb.IntegratedSecurity = false;
            //}
            //SqlConnection myConnection = new SqlConnection(scsb.ConnectionString);
            //return myConnection;
            SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            //scsb["Server"] = "61.140.60.19";
            scsb.DataSource = @"pc-PC\SQLEXPRESS";
            scsb.IntegratedSecurity = true;
            scsb.InitialCatalog = "SMOSDB2015";
            scsb.UserID = "sa";
            scsb.Password = "gnss";
            scsb["database"] = "SMOSDB2015";
            if (scsb.UserID == "")
            {
                scsb.IntegratedSecurity = true;
            }
            else
            {
                scsb.IntegratedSecurity = false;
            }
            SqlConnection myConnection = new SqlConnection(scsb.ConnectionString);
            return myConnection;
            //SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();//非发布版
            //scsb["Server"] = "192.168.191.2";
            //scsb.DataSource = @"WIN-OU6587RDTC6";
            //scsb.IntegratedSecurity = true;
            //scsb.InitialCatalog = "";
            //scsb.UserID = "sa";
            //scsb.Password = "gnss";
            //scsb["database"] = "SMOSDB2015";
            //if (scsb.UserID == "")
            //{
            //    scsb.IntegratedSecurity = true;
            //}
            //else
            //{
            //    scsb.IntegratedSecurity = false;
            //}
            //SqlConnection myConnection = new SqlConnection("Data Source=WIN-OU6587RDTC6;Initial Catalog=SMOSDB2015;Integrated Security=True");
            //return myConnection;

        }
        /// <summary>
        ///sqlserver数据库连接功能函数
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// 根据项目名称和监测量类型获取数据库连接语句
        /// </summary>
        /// <returns></returns>
        public SqlConnection getconnbyfs()
        {

            return getconnbydbase("SMOSDB");

        }
        /// <summary>
        /// 根据数据库名称连接sqlserver数据库
        /// </summary>
        /// <returns></returns>
        public SqlConnection getconnbydbase(string dbname)
        {
            SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            //scsb["Server"] = "61.140.60.19";
            scsb.DataSource = @"pc-PC\SQLEXPRESS";
            scsb.IntegratedSecurity = true;
            scsb.InitialCatalog = dbname;
            scsb.UserID = "sa";
            scsb.Password = "gnss";
            scsb["database"] = dbname;
            if (scsb.UserID == "")
            {
                scsb.IntegratedSecurity = true;
            }
            else
            {
                scsb.IntegratedSecurity = false;
            }
            SqlConnection myConnection = new SqlConnection(scsb.ConnectionString);
            return myConnection;
        }
        public object getconnstr(string xmname, string jcl)
        {
            DataTable dt = new DataTable();
            querysql query = new querysql();
            string sql = "select * from xmconnect where xmname='#_1' and type ='#_2'";
            sql = sql.Replace("#_1", xmname);
            sql = sql.Replace("#_2", jcl);
            dt = query.querytaccesstdb(sql);
            string dbtype = "";//数据库类型
            string connecttype = "";//连接方式
            string connectstr = "";//数据库连接
            DataView dv = new DataView(dt);
            foreach (DataRowView drv in dv)
            {
                dbtype = drv["dbtype"].ToString();
                connecttype = drv["connecttype"].ToString();
                connectstr = drv["connectstr"].ToString();
            }
            switch (dbtype)
            {
                case "mysql": return mysqlconnect(connectstr, connecttype);

                case "sqlserver": return sqlserverconnect(connectstr, connecttype);

                default: return null;
            }

        }
        /// <summary>
        /// mysql 按连接方式连接接口函数
        /// </summary>
        /// <param name="connectstr"></param>
        /// <param name="connecttype"></param>
        public object mysqlconnect(string connectstr/*数据库连接语句*/, string connecttype/*数据库连接方式*/ )
        {
            switch (connecttype)
            {
                case "odbc": return getdbconn(connectstr);

                default: return null;
            }
        }
        /// <summary>
        /// mysql 按连接方式连接接口函数
        /// </summary>
        /// <param name="connectstr"></param>
        /// <param name="connecttype"></param>
        public object sqlserverconnect(string connectstr/*数据库连接语句*/, string connecttype/*数据库连接方式*/ )
        {
            switch (connecttype)
            {
                case "sqlclient": return getsqlconnstr(connectstr);

                default: return null;
            }
        }
    }
}