﻿using System.Collections.Generic;
using System.Data;
using NFnet_DAL;
namespace NFnet_BLL
{
    public class xminfo
    {
        private string xmname, lng, lat, xmdress,  jcStartDate, safeMonitor, safeLevel, jcEndDate, monitorMeasure, monitor, instrustmentstr, province, city, area, jcdw, wtdw, respone, xmlx, xmbz;
        public string Xmbz
        {
            get { return xmbz; }
            set { xmbz = value; }
        }

        public string Xmlx
        {
            get { return xmlx; }
            set { xmlx = value; }
        }


        public string Respone
        {
            get { return respone; }
            set { respone = value; }
        }

        public string Wtdw
        {
            get { return wtdw; }
            set { wtdw = value; }
        }

        public string Jcdw
        {
            get { return jcdw; }
            set { jcdw = value; }
        }

        public string Lat
        {
            get { return lat; }
            set { lat = value; }
        }

        public string Lng
        {
            get { return lng; }
            set { lng = value; }
        }

        public string Area
        {
            get { return area; }
            set { area = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Province
        {
            get { return province; }
            set { province = value; }
        }

        public string Instrustmentstr
        {
            get { return instrustmentstr; }
            set { instrustmentstr = value; }
        }

        public string Monitor
        {
            get { return monitor; }
            set { monitor = value; }
        }

        public string MonitorMeasure
        {
            get { return monitorMeasure; }
            set { monitorMeasure = value; }
        }

        public string JcEndDate
        {
            get { return jcEndDate; }
            set { jcEndDate = value; }
        }

        public string SafeLevel
        {
            get { return safeLevel; }
            set { safeLevel = value; }
        }

        public string SafeMonitor
        {
            get { return safeMonitor; }
            set { safeMonitor = value; }
        }

        public string JcStartDate
        {
            get { return jcStartDate; }
            set { jcStartDate = value; }
        }


        public string Xmdress
        {
            get { return xmdress; }
            set { xmdress = value; }
        }

        public string Xmname
        {
            get { return xmname; }
            set { xmname = value; }
        }
        public xminfo()
        {
 
        }
        public xminfo(string xmname, string lng, string lat, string xmdress,
            string jcStartDate, string safeMonitor, string safeLevel,
            string jcEndDate, string monitorMeasure, string monitor,
            string instrustmentstr, string province, string city, string area,
            string jcdw, string wtdw, string respone, string xmlx, string xmbz)
        {

            this.xmname = xmname;
            this.lng = lng;
            this.lat = lat;
            this.xmdress = xmdress;
            this.jcStartDate = jcStartDate;
            this.safeMonitor = safeMonitor;
            this.safeLevel = safeLevel;
            this.jcEndDate = jcEndDate;
            this.monitorMeasure = monitorMeasure;
            this.monitor = monitor;
            this.instrustmentstr = instrustmentstr;
            this.province = province;
            this.city = city;
            this.area = area;
            this.jcdw = jcdw;
            this.wtdw = wtdw;
            this.respone = respone;
            this.xmlx = xmlx;
            this.xmbz = xmbz;
        }
        //获取项目信息
        public List<xminfo> GetXmInfoById(string xmid)
        {
            querysql query = new querysql();
            string sql = "select * from xmconnect where xmname ='" + xmid+"'";
            DataTable dt = query.querytaccesstdb(sql);
            sqlhelper helper = new sqlhelper();
            string[] param = { "xmname", "jd", "wd", "xmadress", "jcpgStartTime", "monitorInstrusment", "safeLevel", "jcpgEndTime", "monitorMeasureMan", "monitorMan", "buildInstrutment", "provincial", "cities", "countries", "jcDw", "delegateDw", "ResponseMan", "xmType", "xmBz" };
            List<List<string>> lls = helper.SetEntrlFromData(param, dt);
            List<xminfo> lds = new List<xminfo>();
            foreach (List<string> ls in lls)
            {
                xminfo xf = new xminfo(ls[0], ls[1], ls[2], ls[3], ls[4], ls[5], ls[6], ls[7], ls[8], ls[9]
                    , ls[10], ls[11], ls[12], ls[13], ls[14], ls[15], ls[16], ls[17], ls[18]);
                lds.Add(xf);
            }
            return lds;

        }
        //更新项目信息
        public void UpdateXminfoById(string idstr)
        {
            sqlhelper helper = new sqlhelper();
            string[] param = { "xmname", "jd", "wd", "xmadress", "jcpgStartTime", "monitorInstrusment", "safeLevel", "jcpgEndTime", "monitorMeasureMan", "monitorMan", "buildInstrutment", "provincial", "cities", "countries", "jcDw", "delegateDw", "ResponseMan", "xmType", "xmBz" };
            string[] paramVal = { 
this.xmname ,
            this.lng,
            this.lat,
            this.xmdress ,
            this.jcStartDate,
            this.safeMonitor,
            this.safeLevel,
            this.jcEndDate,
            this.monitorMeasure,
            this.monitor,
            this.instrustmentstr,
            this.province,
            this.city,
            this.area,
            this.jcdw,
            this.wtdw,
            this.respone,
            this.xmlx,
            this.xmbz 
                                };
            string[] condition = { "id" };
            string[] conditionVal = { idstr };
            helper.UpdateAccessEntrlById("xmconnect", param, paramVal, condition, conditionVal);
        }
        //更新项目名称
        public void UpdateXminfoByName(string idstr)
        {
            sqlhelper helper = new sqlhelper();
            string[] param = { "xmname", "jd", "wd", "xmadress", "jcpgStartTime", "monitorInstrusment", "safeLevel", "jcpgEndTime", "monitorMeasureMan", "monitorMan", "buildInstrutment", "provincial", "cities", "countries", "jcDw", "delegateDw", "ResponseMan", "xmType", "xmBz" };
            string[] paramVal = { 
this.xmname ,
            this.lng,
            this.lat,
            this.xmdress ,
            this.jcStartDate,
            this.safeMonitor,
            this.safeLevel,
            this.jcEndDate,
            this.monitorMeasure,
            this.monitor,
            this.instrustmentstr,
            this.province,
            this.city,
            this.area,
            this.jcdw,
            this.wtdw,
            this.respone,
            this.xmlx,
            this.xmbz 
                                };
            string[] condition = { "xmname" };
            string[] conditionVal = { "'"+idstr+"'" };
            helper.UpdateAccessEntrlById("xmconnect", param, paramVal, condition, conditionVal);
        }
    }
}