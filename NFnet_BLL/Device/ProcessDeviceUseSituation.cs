﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.deviceusesituation
{
    public class ProcessdeviceusesituationUseSituation
    {
        public global::device.BLL.deviceusesituation deviceusesituationbll = new device.BLL.deviceusesituation();
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool ProcessdeviceusesituationAdd(global::device.Model.deviceusesituation model, out string mssg)
        {
            return deviceusesituationbll.Add(model, out mssg);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool ProcessdeviceusesituationUpdate(global::device.Model.deviceusesituation model, out string mssg)
        {
            return deviceusesituationbll.Update(model, out mssg);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool ProcessdeviceusesituationDelete(ProcessdeviceusesituationDeleteModel model, out string mssg)
        {
            return deviceusesituationbll.Delete(model.unitname, model.id, out mssg);
        }

        public class ProcessdeviceusesituationDeleteModel
        {
            public string unitname { get; set; }
            public int id { get; set; }
            public ProcessdeviceusesituationDeleteModel(string unitname, int id)
            {
                this.unitname = unitname;
                this.id = id;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool ProcessdeviceusesituationModelGet(ProcessdeviceusesituationModelGetModel model, out string mssg)
        {
            global::device.Model.deviceusesituation xmdevicemodel = null;
            if (deviceusesituationbll.GetModel(model.unitname, model.id, out xmdevicemodel, out mssg))
            {
                model.xmdevicemodel = xmdevicemodel;
                return true;
            }
            return false;
        }
        public class ProcessdeviceusesituationModelGetModel
        {
            public string unitname { get; set; }
            public int id { get; set; }
            public global::device.Model.deviceusesituation xmdevicemodel { get; set; }
            public ProcessdeviceusesituationModelGetModel(string unitname, int id)
            {
                this.unitname = unitname;
                this.id = id;
            }
        }

       

        //获取设备表
        public bool ProcessXmdeviceusesituationTableLoad(ProcessXmdeviceusesituationTableLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (deviceusesituationbll.DeviceUseSituationTableLoad(model.unitname, model.pageIndex, model.rows, model.searchList, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;

        }

        public class ProcessXmdeviceusesituationTableLoadModel : SearchCondition
        {
            public DataTable dt { get; set; }
            public string unitname { get; set; }
            public List<String> searchList { get; set; }
            public ProcessXmdeviceusesituationTableLoadModel(int startPageIndex, int pageSize, string unitname, List<string> searchList, string sord)
            {
                this.pageIndex = startPageIndex;
                this.rows = pageSize;
                this.unitname = unitname;
                this.searchList = searchList;
                this.sord = sord;

            }

        }


        public bool ProcessXmdeviceusesituationTableCountLoad(ProcessXmdeviceusesituationTableCountLoadModel model, out string mssg)
        {
            string totalCont = "";
            if (deviceusesituationbll.DeviceUseSituationTableCountLoad(model.unitname, model.searchList, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessXmdeviceusesituationTableCountLoadModel
        {
            public string unitname { get; set; }
            public List<string> searchList { get; set; }
            public string totalCont { get; set; }
            public ProcessXmdeviceusesituationTableCountLoadModel(string unitname, List<string> searchList)
            {
                this.unitname = unitname;
                this.searchList = searchList;
            }
        }



       
    }
}