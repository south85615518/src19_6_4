﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFnet_BLL.Device;
using NFnet_BLL.UserProcess;
using System.Data;
using NFnet_BLL.DisplayDataProcess.pub;
using Tool;

namespace NFnet_BLL.Device
{
    public class ProcessXmDeviceAlarm
    {
        
        public List<string> alarmlist{get;set;}
        public string unitname { get; set; }
        public string mssg = "";
        public ProcessDeviceBLL processDeviceBLL = new ProcessDeviceBLL();
        public ProcessAdministratrorBLL processAdministratrorBLL = new ProcessAdministratrorBLL();
        public ProcessSmsSendBLL processSmsSendBLL = new ProcessSmsSendBLL();
        public void main()
        {
            var ProcessAdministratorBLL = new ProcessAdministratrorBLL();
            var processAdministratorLoadModel = new ProcessAdministratrorBLL.ProcessAdministratorLoadModel(unitname);
            List<Authority.Model.smssendmember> smssendmemberlist = new List<Authority.Model.smssendmember>();
            if (ProcessAdministratorBLL.ProcessAdministratorLoad(processAdministratorLoadModel, out mssg))
            {
                DataView dv = new DataView(processAdministratorLoadModel.dt);
                foreach(DataRowView drv in dv)
                {
                    smssendmemberlist.Add(new Authority.Model.smssendmember { name = drv["userName"].ToString(), tel = drv["tel"].ToString() });
                }

            }
            List<global::device.Model.xmdevice> xmdevicelist = XmDeviceAlarm(unitname,out  mssg);
            foreach (var xmdevice in xmdevicelist)
            {
                alarmlist.Add(string.Format("仪器{0}检定预警!最后检定时间{1},检定周期{2}",xmdevice.senorno,xmdevice.lastinspectiontime,xmdevice.inspectionalarmvalue));
            }
            List<string> mssglist = null;
            processSmsSendBLL.ProcessSmsSend(smssendmemberlist, processSmsSendBLL.smsalarmsplit(string.Join("\r\t",alarmlist)),out mssglist);
            ExceptionLog.ExceptionWrite(string.Join("\r\n",mssglist));
           
        }
        public List<global::device.Model.xmdevice> XmDeviceAlarm(string unitname,out string mssg)
        {
            var model = new ProcessDeviceBLL.ProcessXmDeviceAlarmModel(unitname);
            if (processDeviceBLL.ProcessXmDeviceAlarm(model, out mssg))
                return model.modellist;
            return null ;
        }
    }
}
