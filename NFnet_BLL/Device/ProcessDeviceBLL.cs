﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.Device
{
    public class ProcessDeviceBLL
    {
        public global::device.BLL.xmdevice xmdevicebll = new device.BLL.xmdevice(); 
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool ProcessDeviceAdd(global::device.Model.xmdevice model, out string mssg)
        {
            return xmdevicebll.Add(model,out mssg);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool ProcessDeviceUpdate(global::device.Model.xmdevice model, out string mssg)
        {
            return xmdevicebll.Update(model,out mssg);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool ProcessDeviceDelete(ProcessDeviceDeleteModel model, out string mssg)
        {
            return xmdevicebll.Delete(model.unitname,model.id,out mssg);
        }

        public class ProcessDeviceDeleteModel
        {
            public string unitname { get; set; }
            public int id { get; set; }
            public ProcessDeviceDeleteModel(string unitname, int id)
            {
                this.unitname = unitname;
                this.id = id;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool ProcessDeviceModelGet(ProcessDeviceModelGetModel model, out string mssg)
        {
            global::device.Model.xmdevice xmdevicemodel = null;
            if (xmdevicebll.GetModel(model.unitname, model.id, out xmdevicemodel, out mssg))
            {
                model.xmdevicemodel = xmdevicemodel;
                return true;
            }
            return false;
        }
        public class ProcessDeviceModelGetModel
        {
            public string unitname { get; set; }
            public int id { get; set; }
            public global::device.Model.xmdevice xmdevicemodel { get; set; }
            public ProcessDeviceModelGetModel(string unitname,int id)
            {
                this.unitname = unitname;
                this.id = id;
            }
        }

        //设置设备的当前是否可用的状态
        public bool ProcessDeviceStateSet(ProcessDeviceStateSetModel model, out string mssg)
        {
            return xmdevicebll.SetState(model.unitname,model.id,model.state,out mssg);

        }
        public class ProcessDeviceStateSetModel
        {
            public string unitname { get; set; }
            public int id { get; set; }
            public string state { get; set; }
            public ProcessDeviceStateSetModel(string unitname,int id,string state)
            {
                this.unitname = unitname;
                this.id = id;
                this.state = state;
            }
        }

        //设置设备的当前是否在用的状态
        public bool ProcessDeviceUseStateSet(ProcessDeviceUseStateSetModel model, out string mssg)
        {
            return xmdevicebll.SetUseState(model.unitname,model.id,model.usestate,out mssg);
        }
        public class ProcessDeviceUseStateSetModel
        {
            public string unitname { get; set; }
            public int id { get; set; }
            public bool usestate { get; set; }
            public ProcessDeviceUseStateSetModel(string unitname, int id, bool usestate)
            {
                this.unitname = unitname;
                this.id = id;
                this.usestate = usestate;
            }
        }

        //获取设备表
        public bool ProcessUnitDeviceTableLoad(ProcessUnitDeviceTableLoadModel model, out string mssg)
        {
           DataTable dt = null;
           if (xmdevicebll.XmDeviceTableLoad(model.unitname, model.pageIndex, model.rows, model.searchList, model.sord, out dt, out mssg))
           {
               model.dt = dt;
               return true;
           }
           return false;

        }

        public class ProcessUnitDeviceTableLoadModel : SearchCondition
        {
            public DataTable dt { get; set; }
            public string unitname { get; set; }
            public List<String> searchList { get; set; }
            public ProcessUnitDeviceTableLoadModel(int startPageIndex, int pageSize, string unitname, List<string> searchList, string sord)
            {
                this.pageIndex = startPageIndex;
                this.rows = pageSize;
                this.unitname = unitname;
                this.searchList = searchList;
                this.sord = sord;

            }

        }

        //获取设备表
        public bool ProcessXmDeviceModellistLoad(ProcessXmDeviceModellistLoadModel model, out string mssg)
        {
            List<device.Model.xmdevice> xmdevicelist = null;
            if (xmdevicebll.XmDeviceModellistLoad(model.xmno, out xmdevicelist, out mssg))
            {
                model.xmdevicelist = xmdevicelist;
                return true;
            }
            return false;

        }

        public class ProcessXmDeviceModellistLoadModel 
        {
            public List<device.Model.xmdevice> xmdevicelist { get; set; }
            public int xmno { get; set; }
            public ProcessXmDeviceModellistLoadModel(int xmno)
            {
                this.xmno = xmno;

            }

        }



        public bool ProcessXmDeviceTableCountLoad(ProcessXmDeviceTableCountLoadModel model, out string mssg)
        {
            string totalCont = "";
            if (xmdevicebll.XmDeviceTableCountLoad(model.unitname, model.searchList, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessXmDeviceTableCountLoadModel
        {
            public string unitname { get; set; }
            public List<string> searchList { get; set; }
            public string totalCont{ get; set; }
            public ProcessXmDeviceTableCountLoadModel(string unitname, List<string> searchList)
            {
                this.unitname = unitname;
                this.searchList = searchList;
            }
        }



        //设备预警
        public bool ProcessXmDeviceAlarm(ProcessXmDeviceAlarmModel model, out string mssg)
        {

            List<global::device.Model.xmdevice> modellist = null;
            if (xmdevicebll.XmDeviceAlarm(model.unitname, out modellist, out mssg))
            {
                model.modellist = modellist;
                return true;
            }
            return false;
        }
        public class ProcessXmDeviceAlarmModel
        {
            public string unitname { get; set; }
            public List<global::device.Model.xmdevice> modellist { get; set; }
            public ProcessXmDeviceAlarmModel(string unitname)
            {
                this.unitname = unitname;
            }
        }
    }
}