﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NFnet_BLL.DataProcess;

namespace NFnet_BLL.Device
{
    public class ProcessDeviceApplyBLL
    {
        public global::device.BLL.deviceuseapply deviceuseapplybll = new device.BLL.deviceuseapply(); 
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool ProcessDeviceAdd(global::device.Model.deviceuseapply model, out string mssg)
        {
            return deviceuseapplybll.Add(model,out mssg);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool ProcessDeviceUpdate(global::device.Model.deviceuseapply model, out string mssg)
        {
            return deviceuseapplybll.Update(model,out mssg);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool ProcessDeviceDelete(ProcessDeviceDeleteModel model, out string mssg)
        {
            return deviceuseapplybll.Delete(model.unitname,model.id,out mssg);
        }

        public class ProcessDeviceDeleteModel
        {
            public string unitname { get; set; }
            public int id { get; set; }
            public ProcessDeviceDeleteModel(string unitname, int id)
            {
                this.unitname = unitname;
                this.id = id;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool ProcessDeviceModelGet(ProcessDeviceModelGetModel model, out string mssg)
        {
            global::device.Model.deviceuseapply deviceuseapplymodel = null;
            if (deviceuseapplybll.GetModel(model.unitname, model.id, out deviceuseapplymodel, out mssg))
            {
                model.deviceuseapplymodel = deviceuseapplymodel;
                return true;
            }
            return false;
        }
        public class ProcessDeviceModelGetModel
        {
            public string unitname { get; set; }
            public int id { get; set; }
            public global::device.Model.deviceuseapply deviceuseapplymodel { get; set; }
            public ProcessDeviceModelGetModel(string unitname,int id)
            {
                this.unitname = unitname;
                this.id = id;
            }
        }

        //设置设备的当前是否可用的状态
        //public bool ProcessDeviceStateSet(ProcessDeviceStateSetModel model, out string mssg)
        //{
        //    return deviceuseapplybll.SetState(model.unitname,model.id,model.state,out mssg);

        //}
        //public class ProcessDeviceStateSetModel
        //{
        //    public string unitname { get; set; }
        //    public int id { get; set; }
        //    public string state { get; set; }
        //    public ProcessDeviceStateSetModel(string unitname,int id,string state)
        //    {
        //        this.unitname = unitname;
        //        this.id = id;
        //        this.state = state;
        //    }
        //}

        ////设置设备的当前是否在用的状态
        //public bool ProcessDeviceUseStateSet(ProcessDeviceUseStateSetModel model, out string mssg)
        //{
        //    return deviceuseapplybll.SetUseState(model.unitname,model.id,model.usestate,out mssg);
        //}
        //public class ProcessDeviceUseStateSetModel
        //{
        //    public string unitname { get; set; }
        //    public int id { get; set; }
        //    public bool usestate { get; set; }
        //    public ProcessDeviceUseStateSetModel(string unitname, int id, bool usestate)
        //    {
        //        this.unitname = unitname;
        //        this.id = id;
        //        this.usestate = usestate;
        //    }
        //}

        //获取设备申请表
        public bool ProcessdeviceuseapplyTableLoad(ProcessdeviceuseapplyTableLoadModel model, out string mssg)
        {
           DataTable dt = null;
           if (deviceuseapplybll.UnitDeviceApplyTableLoad(model.unitname, model.pageIndex, model.rows,  model.sord, out dt, out mssg))
           {
               model.dt = dt;
               return true;
           }
           return false;

        }

        public class ProcessdeviceuseapplyTableLoadModel : SearchCondition
        {
            public DataTable dt { get; set; }
            public string unitname { get; set; }
            public ProcessdeviceuseapplyTableLoadModel(int startPageIndex, int pageSize, string unitname, string sord)
            {
                this.pageIndex = startPageIndex;
                this.rows = pageSize;
                this.unitname = unitname;
                this.sord = sord;

            }

        }


        public bool ProcessdeviceuseapplyTableCountLoad(ProcessdeviceuseapplyTableCountLoadModel model, out string mssg)
        {
            string totalCont = "";
            if (deviceuseapplybll.UnitDeviceApplyTableCountLoad(model.unitname, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessdeviceuseapplyTableCountLoadModel
        {
            public string unitname { get; set; }
            public string totalCont{ get; set; }
            public ProcessdeviceuseapplyTableCountLoadModel(string unitname)
            {
                this.unitname = unitname;
            }
        }

        public bool ProcessMonitordeviceuseapplyTableLoad(ProcessMonitordeviceuseapplyTableLoadModel model, out string mssg)
        {
            DataTable dt = null;
            if (deviceuseapplybll.UnitDeviceApplyTableLoad(model.unitname, model.pageIndex, model.rows, model.sord, out dt, out mssg))
            {
                model.dt = dt;
                return true;
            }
            return false;

        }

        public class ProcessMonitordeviceuseapplyTableLoadModel : SearchCondition
        {
            public DataTable dt { get; set; }
            public string monitorID { get; set; }
            public string unitname { get; set; }
            public ProcessMonitordeviceuseapplyTableLoadModel(string unitname, string monitorID,int startPageIndex, int pageSize, string sord)
            {
                this.pageIndex = startPageIndex;
                this.rows = pageSize;
                this.monitorID = monitorID;
                this.unitname = unitname;
                this.sord = sord;

            }

        }


        public bool ProcessMonitordeviceuseapplyTableCountLoad(ProcessMonitordeviceuseapplyTableCountLoadModel model, out string mssg)
        {
            string totalCont = "";
            if (deviceuseapplybll.UnitDeviceApplyTableCountLoad(model.unitname, out totalCont, out mssg))
            {
                model.totalCont = totalCont;
                return true;
            }
            return false;
        }

        public class ProcessMonitordeviceuseapplyTableCountLoadModel
        {
            public string unitname { get; set; }
            public string monitorID { get; set; }
            public string totalCont { get; set; }
            public ProcessMonitordeviceuseapplyTableCountLoadModel(string unitname, string monitorID)
            {
                this.unitname = unitname;
                this.monitorID = monitorID;
            }
        }

       
    }
}