﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;
using NFnet_DAL;

namespace TotalStation.DAL
{
    public class ResultDataDAL
    {
        public database db = new database();
        public bool List(
            string ColName,
            string  Sord,
            string Xmname,
            int StartCyc,
            int EndCyc,
            int PageIndex,
            int Rows,
            out string jsondata)
        {
            string order = ColName == "id" ? "" : "order by " + ColName + "  " + Sord;
            OdbcConnection conn = db.GetStanderConn(Xmname);
            string sql = string.Format("select count(*) from fmos_orgldata where taskName='{3}' and CYC between {0} and {1}  {2}", StartCyc, EndCyc, order, Xmname);
            string totalCont = querysql.querystanderstr(sql, conn);
            sql = string.Format("select  point_name,CYC,point_har,point_var,sd,N,E,Z,dN,dE,dZ,search_Time from fmos_orgldata where taskName='{5}' and CYC between {0} and {1}  {4} limit {2},{3}", StartCyc, EndCyc, (PageIndex - 1) * Rows, PageIndex * Rows, order, Xmname);
            DataTable dt = new DataTable();
            dt = querysql.querystanderdb(sql, conn);
            DataView dv = new DataView(dt);
            List<string> rows = new List<string>();
            int i = 0;
            foreach (DataRowView drv in dv)
            {

                List<string> row = new List<string>();
                for (i = 0; i < dt.Columns.Count; i++)
                {
                    row.Add("\"" + drv[i].ToString() + "\"");
                }
                string temp = string.Join(",", row);
                rows.Add("{\"cell\":[" + temp + "]}");
            }
            int totalPageNum = Int32.Parse(totalCont) / Rows == 0 ? Int32.Parse(totalCont) / Rows : Int32.Parse(totalCont) / Rows + 1;
            string rowstr = string.Format("[{0}]", string.Join(", ", rows));
            string template = "{\"page\":" + PageIndex + ",\"total\":" + totalPageNum + ",\"records\":" + totalCont + ",\"rows\":" + rowstr + ",\"userdata\":{\"amount\":3330,\"tax\":342,\"total\":3564,\"name\":\"Totals:\"}}";


            jsondata = template;
            return true;
        }
    }
}