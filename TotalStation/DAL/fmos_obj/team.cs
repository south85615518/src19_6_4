﻿/**  版本信息模板在安装目录下，可自行修改。
* team.cs
*
* 功 能： N/A
* 类 名： team
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
namespace TotalStation.DAL.fmos_obj
{
	/// <summary>
	/// 数据访问类:team
	/// </summary>
	public partial class team
	{
		public team()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TotalStation.Model.fmos_obj.team model)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("insert into fmos_team(");
            strSql.Append("TEAM_NAME,REMARK,taskName)");
            strSql.Append(" values (");
            strSql.Append("@TEAM_NAME,@REMARK,@taskName)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("TEAM_NAME=@TEAM_NAME");
			OdbcParameter[] parameters = {
					new OdbcParameter("@TEAM_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@REMARK", OdbcType.VarChar,120),
					new OdbcParameter("@taskName", OdbcType.VarChar,120)};
			parameters[0].Value = model.TEAM_NAME;
			parameters[1].Value = model.REMARK;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStation.Model.fmos_obj.team model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update fmos_team set ");
			strSql.Append("TEAM_NAME=@TEAM_NAME,");
			strSql.Append("REMARK=@REMARK");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@TEAM_NAME", OdbcType.Text),
					new OdbcParameter("@REMARK", OdbcType.Text)};
			parameters[0].Value = model.TEAM_NAME;
			parameters[1].Value = model.REMARK;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from fmos_team ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.team GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TEAM_NAME,REMARK from fmos_team ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			TotalStation.Model.fmos_obj.team model=new TotalStation.Model.fmos_obj.team();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "fmos_team", parameters);
			if(ds!=null&&ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.team DataRowToModel(DataRow row)
		{
			TotalStation.Model.fmos_obj.team model=new TotalStation.Model.fmos_obj.team();
			if (row != null)
			{
				if(row["TEAM_NAME"]!=null)
				{
					model.TEAM_NAME=row["TEAM_NAME"].ToString();
				}
				if(row["REMARK"]!=null)
				{
					model.REMARK=row["REMARK"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TEAM_NAME,REMARK ");
			strSql.Append(" FROM fmos_team ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
            return OdbcSQLHelper.Query(strSql.ToString(), "fmos_team");
		}

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM fmos_team ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = //OdbcSQLHelper.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.ID desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from fmos_team T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return OdbcSQLHelper.Query(strSql.ToString());
        //}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int32),
					new OdbcParameter("@PageIndex", OdbcType.Int32),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "fmos_team";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

