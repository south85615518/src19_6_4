﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TotalStation.DAL.fmos_obj
{
    public class reportformattype
    {

        public static string GTreportformattypeToString(Model.fmos_obj.reportformattype datatype)
        {
            switch (datatype)
            {
                case Model.fmos_obj.reportformattype.serise: return "曲线报表";
                case Model.fmos_obj.reportformattype.settlement: return "沉降报表";

              
                default: return "";
            }
        }


        public static  Model.fmos_obj.reportformattype GTStringToreportformattype(string datatype)
        {
            switch (datatype)
            {
                case "曲线报表": return Model.fmos_obj.reportformattype.serise;
                case "沉降报表": return Model.fmos_obj.reportformattype.settlement;
                

                default: return Model.fmos_obj.reportformattype.serise;
            }
        }
    }
}
