﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_pointsinteam.cs
*
* 功 能： N/A
* 类 名： fmos_pointsinteam
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/21 14:45:52   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
//using Odbc.Data.OdbcClient;
//using TotalStation.DBUtility;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;//Please add references
namespace TotalStation.DAL.fmos_obj
{
	/// <summary>
	/// 数据访问类:fmos_pointsinteam
	/// </summary>
	public partial class fmos_pointsinteam
	{
        public database db = new database();
		public fmos_pointsinteam()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TotalStation.Model.fmos_obj.fmos_pointsinteam model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.Xmno);
            OdbcSQLHelper.Conn = conn;
			strSql.Append("insert into fmos_pointsinteam(");
			strSql.Append("ID,Idex,TEAM_NAME,POINT_NAME,xmno)");
			strSql.Append(" values (");
			strSql.Append("@ID,@Idex,@TEAM_NAME,@POINT_NAME,@xmno)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("  ID=@ID, ");
            strSql.Append("Idex=@Idex,");
            strSql.Append("TEAM_NAME=@TEAM_NAME,");
            strSql.Append("POINT_NAME=@POINT_NAME,");
            strSql.Append("xmno=@xmno");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@Idex", OdbcType.Int,11),
					new OdbcParameter("@TEAM_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
                    new OdbcParameter("@xmno", OdbcType.Int),
                    new OdbcParameter("@_ID", OdbcType.Int,11),
					new OdbcParameter("@_Idex", OdbcType.Int,11),
					new OdbcParameter("@_TEAM_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@_POINT_NAME", OdbcType.VarChar,120),
                    new OdbcParameter("@_xmno", OdbcType.Int)
                                         };
			parameters[0].Value = model.ID;
			parameters[1].Value = model.Idex;
			parameters[2].Value = model.TEAM_NAME;
			parameters[3].Value = model.POINT_NAME;
            parameters[4].Value = model.Xmno;
            parameters[5].Value = model.ID;
            parameters[6].Value = model.Idex;
            parameters[7].Value = model.TEAM_NAME;
            parameters[8].Value = model.POINT_NAME;
            parameters[9].Value = model.Xmno;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStation.Model.fmos_obj.fmos_pointsinteam model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.Xmno);
            OdbcSQLHelper.Conn = conn;
			strSql.Append("update fmos_pointsinteam set ");
			strSql.Append("ID=@ID,");
			strSql.Append("Idex=@Idex,");
			strSql.Append("TEAM_NAME=@TEAM_NAME,");
			strSql.Append("POINT_NAME=@POINT_NAME");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@Idex", OdbcType.Int,11),
					new OdbcParameter("@TEAM_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.Idex;
			parameters[2].Value = model.TEAM_NAME;
			parameters[3].Value = model.POINT_NAME;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from fmos_pointsinteam ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool GetModel(int xmno,string name,out TotalStation.Model.fmos_obj.fmos_pointsinteam model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,Idex,TEAM_NAME,POINT_NAME from fmos_pointsinteam ");
			strSql.Append(" where     name = @name     and    xmno = @xmno   ");
			OdbcParameter[] parameters = {
                    new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,10)
			};
            parameters[0].Value = name;
            parameters[1].Value = xmno;
			model=new TotalStation.Model.fmos_obj.fmos_pointsinteam();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds!=null&&ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.fmos_pointsinteam DataRowToModel(DataRow row)
		{
			TotalStation.Model.fmos_obj.fmos_pointsinteam model=new TotalStation.Model.fmos_obj.fmos_pointsinteam();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["Idex"]!=null && row["Idex"].ToString()!="")
				{
					model.Idex=int.Parse(row["Idex"].ToString());
				}
				if(row["TEAM_NAME"]!=null)
				{
					model.TEAM_NAME=row["TEAM_NAME"].ToString();
				}
				if(row["POINT_NAME"]!=null)
				{
					model.POINT_NAME=row["POINT_NAME"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,Idex,TEAM_NAME,POINT_NAME ");
			strSql.Append(" FROM fmos_pointsinteam ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}


        public bool PointsInTeamTableLoad(int startPageIndex, int pageSize, int xmno, string xmname, string searchpost, string colName, string sord, out DataTable dt)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " xmno= " + xmno);
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select id,idex,point_name from fmos_pointsinteam where {3}  {2}  limit  {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, searchstr);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointsInTeamTableRowsCount(string searchpost, int xmno, string xmname, out string totalCont)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " xmno= " + xmno);
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select count(*) from fmos_pointsinteam where {0}   ", searchstr);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }


        public bool PointsInTeamNameListLoad(int xmno, out List<string> teamName)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select distinct(team_name) from fmos_pointsinteam where xmno = {0} ", xmno);
            teamName = querysql.querystanderlist(sql, conn);
            if (teamName != null) return true;
            return false;
        }
        

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

