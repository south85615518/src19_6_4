﻿/**  版本信息模板在安装目录下，可自行修改。
* cycdirnet.cs
*
* 功 能： N/A
* 类 名： cycdirnet
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
namespace TotalStation.DAL.fmos_obj
{
    /// <summary>
    /// 数据访问类:cycdirnet
    /// </summary>
    public partial class cycdirnet
    {
        public database db = new database();
        public cycdirnet()
        { }
        #region  BasicMethod



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.cycdirnet model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.TaskName);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into fmos_cycdirnet(");
            strSql.Append("This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod,siblingpoint_name)");
            strSql.Append(" values (");
            strSql.Append("@This_dN,@This_dE,@This_dZ,@Ac_dN,@Ac_dE,@Ac_dZ,@Ac_dNspd,@Ac_dEspd,@Ac_dZspd,@This_dNspd,@This_dEspd,@This_dZspd,@This_dAvgHar,@This_dAvgVar,@This_dAvgSd,@Ac_dAvgHar,@Ac_dAvgVar,@Ac_dAvgSd,@CYC,@TaskName,@AvgHar,@CorrectAzimuth,@AvgVar,@Time,@POINT_NAME,@dAvgSD,@N,@E,@Z,@dAvgHar,@dAvgVar,@dAvgZeroAng,@IsControlPoint,@dCorrectAzimuth,@calculationMethod,@siblingpoint_name )");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("This_dN=@This_dN,");
            strSql.Append("This_dE=@This_dE,");
            strSql.Append("This_dZ=@This_dZ,");
            strSql.Append("Ac_dN=@Ac_dN,");
            strSql.Append("Ac_dE=@Ac_dE,");
            strSql.Append("Ac_dZ=@Ac_dZ,");
            strSql.Append("Ac_dNspd=@Ac_dNspd,");
            strSql.Append("Ac_dEspd=@Ac_dEspd,");
            strSql.Append("Ac_dZspd=@Ac_dZspd,");
            strSql.Append("This_dNspd=@This_dNspd,");
            strSql.Append("This_dEspd=@This_dEspd,");
            strSql.Append("This_dZspd=@This_dZspd,");
            strSql.Append("This_dAvgHar=@This_dAvgHar,");
            strSql.Append("This_dAvgVar=@This_dAvgVar,");
            strSql.Append("This_dAvgSd=@This_dAvgSd,");
            strSql.Append("Ac_dAvgHar=@Ac_dAvgHar,");
            strSql.Append("Ac_dAvgVar=@Ac_dAvgVar,");
            strSql.Append("Ac_dAvgSd=@Ac_dAvgSd,");
            strSql.Append("CYC=@CYC,");
            strSql.Append("AvgHar=@AvgHar,");
            strSql.Append("CorrectAzimuth=@CorrectAzimuth,");
            strSql.Append("AvgVar=@AvgVar,");
            strSql.Append("dAvgSD=@dAvgSD,");
            strSql.Append("N=@N,");
            strSql.Append("E=@E,");
            strSql.Append("Z=@Z,");
            strSql.Append("dAvgHar=@dAvgHar,");
            strSql.Append("dAvgVar=@dAvgVar,");
            strSql.Append("dAvgZeroAng=@dAvgZeroAng,");
            strSql.Append("IsControlPoint=@IsControlPoint,");
            strSql.Append("dCorrectAzimuth=@dCorrectAzimuth,");
            strSql.Append("calculationMethod=@calculationMethod,");
            strSql.Append("siblingpoint_name=@_siblingpoint_name");
            OdbcParameter[] parameters = {
					new OdbcParameter("@This_dN", OdbcType.Double),
					new OdbcParameter("@This_dE", OdbcType.Double),
					new OdbcParameter("@This_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dN", OdbcType.Double),
					new OdbcParameter("@Ac_dE", OdbcType.Double),
					new OdbcParameter("@Ac_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dNspd", OdbcType.Double),
					new OdbcParameter("@This_dEspd", OdbcType.Double),
					new OdbcParameter("@This_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@CYC", OdbcType.Int,11),
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@Time", OdbcType.DateTime),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@dAvgSD", OdbcType.Double),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@dAvgHar", OdbcType.Double),
					new OdbcParameter("@dAvgVar", OdbcType.Double),
					new OdbcParameter("@dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@IsControlPoint", OdbcType.TinyInt,11),
					new OdbcParameter("@dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@calculationMethod", OdbcType.VarChar,120),
                    new OdbcParameter("@siblingpoint_name", OdbcType.VarChar,120),

                    new OdbcParameter("@_This_dN", OdbcType.Double),
					new OdbcParameter("@_This_dE", OdbcType.Double),
					new OdbcParameter("@_This_dZ", OdbcType.Double),
					new OdbcParameter("@_Ac_dN", OdbcType.Double),
					new OdbcParameter("@_Ac_dE", OdbcType.Double),
					new OdbcParameter("@_Ac_dZ", OdbcType.Double),
					new OdbcParameter("@_Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@_Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@_Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@_This_dNspd", OdbcType.Double),
					new OdbcParameter("@_This_dEspd", OdbcType.Double),
					new OdbcParameter("@_This_dZspd", OdbcType.Double),
					new OdbcParameter("@_This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@_CYC", OdbcType.Int,11),
					new OdbcParameter("@_AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@_CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@_AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@_dAvgSD", OdbcType.Double),
					new OdbcParameter("@_N", OdbcType.Double),
					new OdbcParameter("@_E", OdbcType.Double),
					new OdbcParameter("@_Z", OdbcType.Double),
					new OdbcParameter("@_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@_IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("@_dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@_calculationMethod", OdbcType.VarChar,120),
                    new OdbcParameter("@_siblingpoint_name", OdbcType.VarChar,120)

                                         };
            parameters[0].Value = model.This_dN;
            parameters[1].Value = model.This_dE;
            parameters[2].Value = model.This_dZ;
            parameters[3].Value = model.Ac_dN;
            parameters[4].Value = model.Ac_dE;
            parameters[5].Value = model.Ac_dZ;
            parameters[6].Value = model.Ac_dNspd;
            parameters[7].Value = model.Ac_dEspd;
            parameters[8].Value = model.Ac_dZspd;
            parameters[9].Value = model.This_dNspd;
            parameters[10].Value = model.This_dEspd;
            parameters[11].Value = model.This_dZspd;
            parameters[12].Value = model.This_dAvgHar;
            parameters[13].Value = model.This_dAvgVar;
            parameters[14].Value = model.This_dAvgSd;
            parameters[15].Value = model.Ac_dAvgHar;
            parameters[16].Value = model.Ac_dAvgVar;
            parameters[17].Value = model.Ac_dAvgSd;
            parameters[18].Value = model.CYC;
            parameters[19].Value = model.TaskName;
            parameters[20].Value = model.AvgHar;
            parameters[21].Value = model.CorrectAzimuth;
            parameters[22].Value = model.AvgVar;
            parameters[23].Value = model.Time;
            parameters[24].Value = model.POINT_NAME;
            parameters[25].Value = model.dAvgSD;
            parameters[26].Value = model.N;
            parameters[27].Value = model.E;
            parameters[28].Value = model.Z;
            parameters[29].Value = model.dAvgHar;
            parameters[30].Value = model.dAvgVar;
            parameters[31].Value = model.dAvgZeroAng;
            parameters[32].Value = model.IsControlPoint;
            parameters[33].Value = model.dCorrectAzimuth;
            parameters[34].Value = model.calculationMethod;
            parameters[35].Value = model.siblingpointname;

            parameters[36].Value = model.This_dN;
            parameters[37].Value = model.This_dE;
            parameters[38].Value = model.This_dZ;
            parameters[39].Value = model.Ac_dN;
            parameters[40].Value = model.Ac_dE;
            parameters[41].Value = model.Ac_dZ;
            parameters[42].Value = model.Ac_dNspd;
            parameters[43].Value = model.Ac_dEspd;
            parameters[44].Value = model.Ac_dZspd;
            parameters[45].Value = model.This_dNspd;
            parameters[46].Value = model.This_dEspd;
            parameters[47].Value = model.This_dZspd;
            parameters[48].Value = model.This_dAvgHar;
            parameters[49].Value = model.This_dAvgVar;
            parameters[50].Value = model.This_dAvgSd;
            parameters[51].Value = model.Ac_dAvgHar;
            parameters[52].Value = model.Ac_dAvgVar;
            parameters[53].Value = model.Ac_dAvgSd;
            parameters[54].Value = model.CYC;
            parameters[55].Value = model.AvgHar;
            parameters[56].Value = model.CorrectAzimuth;
            parameters[57].Value = model.AvgVar;
            parameters[58].Value = model.dAvgSD;
            parameters[59].Value = model.N;
            parameters[60].Value = model.E;
            parameters[61].Value = model.Z;
            parameters[62].Value = model.dAvgHar;
            parameters[63].Value = model.dAvgVar;
            parameters[64].Value = model.dAvgZeroAng;
            parameters[65].Value = model.IsControlPoint;
            parameters[66].Value = model.dCorrectAzimuth;
            parameters[67].Value = model.calculationMethod;
            parameters[68].Value = model.siblingpointname;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool AddSurveyData(TotalStation.Model.fmos_obj.cycdirnet model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.TaskName);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into fmos_cycdirnet_data(");
            strSql.Append("This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod,siblingpoint_name)");
            strSql.Append(" values (");
            strSql.Append("@This_dN,@This_dE,@This_dZ,@Ac_dN,@Ac_dE,@Ac_dZ,@Ac_dNspd,@Ac_dEspd,@Ac_dZspd,@This_dNspd,@This_dEspd,@This_dZspd,@This_dAvgHar,@This_dAvgVar,@This_dAvgSd,@Ac_dAvgHar,@Ac_dAvgVar,@Ac_dAvgSd,@CYC,@TaskName,@AvgHar,@CorrectAzimuth,@AvgVar,@Time,@POINT_NAME,@dAvgSD,@N,@E,@Z,@dAvgHar,@dAvgVar,@dAvgZeroAng,@IsControlPoint,@dCorrectAzimuth,@calculationMethod,@siblingpoint_name )");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("This_dN=@This_dN,");
            strSql.Append("This_dE=@This_dE,");
            strSql.Append("This_dZ=@This_dZ,");
            strSql.Append("Ac_dN=@Ac_dN,");
            strSql.Append("Ac_dE=@Ac_dE,");
            strSql.Append("Ac_dZ=@Ac_dZ,");
            strSql.Append("Ac_dNspd=@Ac_dNspd,");
            strSql.Append("Ac_dEspd=@Ac_dEspd,");
            strSql.Append("Ac_dZspd=@Ac_dZspd,");
            strSql.Append("This_dNspd=@This_dNspd,");
            strSql.Append("This_dEspd=@This_dEspd,");
            strSql.Append("This_dZspd=@This_dZspd,");
            strSql.Append("This_dAvgHar=@This_dAvgHar,");
            strSql.Append("This_dAvgVar=@This_dAvgVar,");
            strSql.Append("This_dAvgSd=@This_dAvgSd,");
            strSql.Append("Ac_dAvgHar=@Ac_dAvgHar,");
            strSql.Append("Ac_dAvgVar=@Ac_dAvgVar,");
            strSql.Append("Ac_dAvgSd=@Ac_dAvgSd,");
            strSql.Append("CYC=@CYC,");
            strSql.Append("AvgHar=@AvgHar,");
            strSql.Append("CorrectAzimuth=@CorrectAzimuth,");
            strSql.Append("AvgVar=@AvgVar,");
            strSql.Append("dAvgSD=@dAvgSD,");
            strSql.Append("N=@N,");
            strSql.Append("E=@E,");
            strSql.Append("Z=@Z,");
            strSql.Append("dAvgHar=@dAvgHar,");
            strSql.Append("dAvgVar=@dAvgVar,");
            strSql.Append("dAvgZeroAng=@dAvgZeroAng,");
            strSql.Append("IsControlPoint=@IsControlPoint,");
            strSql.Append("dCorrectAzimuth=@dCorrectAzimuth,");
            strSql.Append("calculationMethod=@calculationMethod,");
            strSql.Append("siblingpoint_name=@_siblingpoint_name");
            OdbcParameter[] parameters = {
					new OdbcParameter("@This_dN", OdbcType.Double),
					new OdbcParameter("@This_dE", OdbcType.Double),
					new OdbcParameter("@This_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dN", OdbcType.Double),
					new OdbcParameter("@Ac_dE", OdbcType.Double),
					new OdbcParameter("@Ac_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dNspd", OdbcType.Double),
					new OdbcParameter("@This_dEspd", OdbcType.Double),
					new OdbcParameter("@This_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@CYC", OdbcType.Int,11),
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@Time", OdbcType.DateTime),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@dAvgSD", OdbcType.Double),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@dAvgHar", OdbcType.Double),
					new OdbcParameter("@dAvgVar", OdbcType.Double),
					new OdbcParameter("@dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@IsControlPoint", OdbcType.TinyInt,11),
					new OdbcParameter("@dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@calculationMethod", OdbcType.VarChar,120),
                    new OdbcParameter("@siblingpoint_name", OdbcType.VarChar,120),

                    new OdbcParameter("@_This_dN", OdbcType.Double),
					new OdbcParameter("@_This_dE", OdbcType.Double),
					new OdbcParameter("@_This_dZ", OdbcType.Double),
					new OdbcParameter("@_Ac_dN", OdbcType.Double),
					new OdbcParameter("@_Ac_dE", OdbcType.Double),
					new OdbcParameter("@_Ac_dZ", OdbcType.Double),
					new OdbcParameter("@_Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@_Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@_Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@_This_dNspd", OdbcType.Double),
					new OdbcParameter("@_This_dEspd", OdbcType.Double),
					new OdbcParameter("@_This_dZspd", OdbcType.Double),
					new OdbcParameter("@_This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@_CYC", OdbcType.Int,11),
					new OdbcParameter("@_AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@_CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@_AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@_dAvgSD", OdbcType.Double),
					new OdbcParameter("@_N", OdbcType.Double),
					new OdbcParameter("@_E", OdbcType.Double),
					new OdbcParameter("@_Z", OdbcType.Double),
					new OdbcParameter("@_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@_IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("@_dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@_calculationMethod", OdbcType.VarChar,120),
                    new OdbcParameter("@_siblingpoint_name", OdbcType.VarChar,120)

                                         };
            parameters[0].Value = model.This_dN;
            parameters[1].Value = model.This_dE;
            parameters[2].Value = model.This_dZ;
            parameters[3].Value = model.Ac_dN;
            parameters[4].Value = model.Ac_dE;
            parameters[5].Value = model.Ac_dZ;
            parameters[6].Value = model.Ac_dNspd;
            parameters[7].Value = model.Ac_dEspd;
            parameters[8].Value = model.Ac_dZspd;
            parameters[9].Value = model.This_dNspd;
            parameters[10].Value = model.This_dEspd;
            parameters[11].Value = model.This_dZspd;
            parameters[12].Value = model.This_dAvgHar;
            parameters[13].Value = model.This_dAvgVar;
            parameters[14].Value = model.This_dAvgSd;
            parameters[15].Value = model.Ac_dAvgHar;
            parameters[16].Value = model.Ac_dAvgVar;
            parameters[17].Value = model.Ac_dAvgSd;
            parameters[18].Value = model.CYC;
            parameters[19].Value = model.TaskName;
            parameters[20].Value = model.AvgHar;
            parameters[21].Value = model.CorrectAzimuth;
            parameters[22].Value = model.AvgVar;
            parameters[23].Value = model.Time;
            parameters[24].Value = model.POINT_NAME;
            parameters[25].Value = model.dAvgSD;
            parameters[26].Value = model.N;
            parameters[27].Value = model.E;
            parameters[28].Value = model.Z;
            parameters[29].Value = model.dAvgHar;
            parameters[30].Value = model.dAvgVar;
            parameters[31].Value = model.dAvgZeroAng;
            parameters[32].Value = model.IsControlPoint;
            parameters[33].Value = model.dCorrectAzimuth;
            parameters[34].Value = model.calculationMethod;
            parameters[35].Value = model.siblingpointname;

            parameters[36].Value = model.This_dN;
            parameters[37].Value = model.This_dE;
            parameters[38].Value = model.This_dZ;
            parameters[39].Value = model.Ac_dN;
            parameters[40].Value = model.Ac_dE;
            parameters[41].Value = model.Ac_dZ;
            parameters[42].Value = model.Ac_dNspd;
            parameters[43].Value = model.Ac_dEspd;
            parameters[44].Value = model.Ac_dZspd;
            parameters[45].Value = model.This_dNspd;
            parameters[46].Value = model.This_dEspd;
            parameters[47].Value = model.This_dZspd;
            parameters[48].Value = model.This_dAvgHar;
            parameters[49].Value = model.This_dAvgVar;
            parameters[50].Value = model.This_dAvgSd;
            parameters[51].Value = model.Ac_dAvgHar;
            parameters[52].Value = model.Ac_dAvgVar;
            parameters[53].Value = model.Ac_dAvgSd;
            parameters[54].Value = model.CYC;
            parameters[55].Value = model.AvgHar;
            parameters[56].Value = model.CorrectAzimuth;
            parameters[57].Value = model.AvgVar;
            parameters[58].Value = model.dAvgSD;
            parameters[59].Value = model.N;
            parameters[60].Value = model.E;
            parameters[61].Value = model.Z;
            parameters[62].Value = model.dAvgHar;
            parameters[63].Value = model.dAvgVar;
            parameters[64].Value = model.dAvgZeroAng;
            parameters[65].Value = model.IsControlPoint;
            parameters[66].Value = model.dCorrectAzimuth;
            parameters[67].Value = model.calculationMethod;
            parameters[68].Value = model.siblingpointname;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddCg(TotalStation.Model.fmos_obj.cycdirnet model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetCgStanderConn(model.TaskName);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into fmos_cycdirnet(");
            strSql.Append("This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod,siblingpoint_name)");
            strSql.Append(" values (");
            strSql.Append("@This_dN,@This_dE,@This_dZ,@Ac_dN,@Ac_dE,@Ac_dZ,@Ac_dNspd,@Ac_dEspd,@Ac_dZspd,@This_dNspd,@This_dEspd,@This_dZspd,@This_dAvgHar,@This_dAvgVar,@This_dAvgSd,@Ac_dAvgHar,@Ac_dAvgVar,@Ac_dAvgSd,@CYC,@TaskName,@AvgHar,@CorrectAzimuth,@AvgVar,@Time,@POINT_NAME,@dAvgSD,@N,@E,@Z,@dAvgHar,@dAvgVar,@dAvgZeroAng,@IsControlPoint,@dCorrectAzimuth,@calculationMethod,@siblingpoint_name )");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("This_dN=@This_dN,");
            strSql.Append("This_dE=@This_dE,");
            strSql.Append("This_dZ=@This_dZ,");
            strSql.Append("Ac_dN=@Ac_dN,");
            strSql.Append("Ac_dE=@Ac_dE,");
            strSql.Append("Ac_dZ=@Ac_dZ,");
            strSql.Append("Ac_dNspd=@Ac_dNspd,");
            strSql.Append("Ac_dEspd=@Ac_dEspd,");
            strSql.Append("Ac_dZspd=@Ac_dZspd,");
            strSql.Append("This_dNspd=@This_dNspd,");
            strSql.Append("This_dEspd=@This_dEspd,");
            strSql.Append("This_dZspd=@This_dZspd,");
            strSql.Append("This_dAvgHar=@This_dAvgHar,");
            strSql.Append("This_dAvgVar=@This_dAvgVar,");
            strSql.Append("This_dAvgSd=@This_dAvgSd,");
            strSql.Append("Ac_dAvgHar=@Ac_dAvgHar,");
            strSql.Append("Ac_dAvgVar=@Ac_dAvgVar,");
            strSql.Append("Ac_dAvgSd=@Ac_dAvgSd,");
            strSql.Append("CYC=@CYC,");
            strSql.Append("AvgHar=@AvgHar,");
            strSql.Append("CorrectAzimuth=@CorrectAzimuth,");
            strSql.Append("AvgVar=@AvgVar,");
            strSql.Append("dAvgSD=@dAvgSD,");
            strSql.Append("N=@N,");
            strSql.Append("E=@E,");
            strSql.Append("Z=@Z,");
            strSql.Append("dAvgHar=@dAvgHar,");
            strSql.Append("dAvgVar=@dAvgVar,");
            strSql.Append("dAvgZeroAng=@dAvgZeroAng,");
            strSql.Append("IsControlPoint=@IsControlPoint,");
            strSql.Append("dCorrectAzimuth=@dCorrectAzimuth,");
            strSql.Append("calculationMethod=@calculationMethod,");
            strSql.Append("siblingpoint_name=@_siblingpoint_name");
            OdbcParameter[] parameters = {
					new OdbcParameter("@This_dN", OdbcType.Double),
					new OdbcParameter("@This_dE", OdbcType.Double),
					new OdbcParameter("@This_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dN", OdbcType.Double),
					new OdbcParameter("@Ac_dE", OdbcType.Double),
					new OdbcParameter("@Ac_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dNspd", OdbcType.Double),
					new OdbcParameter("@This_dEspd", OdbcType.Double),
					new OdbcParameter("@This_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@CYC", OdbcType.Int,11),
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@Time", OdbcType.DateTime),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@dAvgSD", OdbcType.Double),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@dAvgHar", OdbcType.Double),
					new OdbcParameter("@dAvgVar", OdbcType.Double),
					new OdbcParameter("@dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@IsControlPoint", OdbcType.TinyInt,11),
					new OdbcParameter("@dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@calculationMethod", OdbcType.VarChar,120),
                    new OdbcParameter("@siblingpoint_name", OdbcType.VarChar,120),

                    new OdbcParameter("@_This_dN", OdbcType.Double),
					new OdbcParameter("@_This_dE", OdbcType.Double),
					new OdbcParameter("@_This_dZ", OdbcType.Double),
					new OdbcParameter("@_Ac_dN", OdbcType.Double),
					new OdbcParameter("@_Ac_dE", OdbcType.Double),
					new OdbcParameter("@_Ac_dZ", OdbcType.Double),
					new OdbcParameter("@_Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@_Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@_Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@_This_dNspd", OdbcType.Double),
					new OdbcParameter("@_This_dEspd", OdbcType.Double),
					new OdbcParameter("@_This_dZspd", OdbcType.Double),
					new OdbcParameter("@_This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@_CYC", OdbcType.Int,11),
					new OdbcParameter("@_AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@_CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@_AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@_dAvgSD", OdbcType.Double),
					new OdbcParameter("@_N", OdbcType.Double),
					new OdbcParameter("@_E", OdbcType.Double),
					new OdbcParameter("@_Z", OdbcType.Double),
					new OdbcParameter("@_dAvgHar", OdbcType.Double),
					new OdbcParameter("@_dAvgVar", OdbcType.Double),
					new OdbcParameter("@_dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@_IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("@_dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@_calculationMethod", OdbcType.VarChar,120),
                    new OdbcParameter("@_siblingpoint_name", OdbcType.VarChar,120)

                                         };
            parameters[0].Value = model.This_dN;
            parameters[1].Value = model.This_dE;
            parameters[2].Value = model.This_dZ;
            parameters[3].Value = model.Ac_dN;
            parameters[4].Value = model.Ac_dE;
            parameters[5].Value = model.Ac_dZ;
            parameters[6].Value = model.Ac_dNspd;
            parameters[7].Value = model.Ac_dEspd;
            parameters[8].Value = model.Ac_dZspd;
            parameters[9].Value = model.This_dNspd;
            parameters[10].Value = model.This_dEspd;
            parameters[11].Value = model.This_dZspd;
            parameters[12].Value = model.This_dAvgHar;
            parameters[13].Value = model.This_dAvgVar;
            parameters[14].Value = model.This_dAvgSd;
            parameters[15].Value = model.Ac_dAvgHar;
            parameters[16].Value = model.Ac_dAvgVar;
            parameters[17].Value = model.Ac_dAvgSd;
            parameters[18].Value = model.CYC;
            parameters[19].Value = model.TaskName;
            parameters[20].Value = model.AvgHar;
            parameters[21].Value = model.CorrectAzimuth;
            parameters[22].Value = model.AvgVar;
            parameters[23].Value = model.Time;
            parameters[24].Value = model.POINT_NAME;
            parameters[25].Value = model.dAvgSD;
            parameters[26].Value = model.N;
            parameters[27].Value = model.E;
            parameters[28].Value = model.Z;
            parameters[29].Value = model.dAvgHar;
            parameters[30].Value = model.dAvgVar;
            parameters[31].Value = model.dAvgZeroAng;
            parameters[32].Value = model.IsControlPoint;
            parameters[33].Value = model.dCorrectAzimuth;
            parameters[34].Value = model.calculationMethod;
            parameters[35].Value = model.siblingpointname;

            parameters[36].Value = model.This_dN;
            parameters[37].Value = model.This_dE;
            parameters[38].Value = model.This_dZ;
            parameters[39].Value = model.Ac_dN;
            parameters[40].Value = model.Ac_dE;
            parameters[41].Value = model.Ac_dZ;
            parameters[42].Value = model.Ac_dNspd;
            parameters[43].Value = model.Ac_dEspd;
            parameters[44].Value = model.Ac_dZspd;
            parameters[45].Value = model.This_dNspd;
            parameters[46].Value = model.This_dEspd;
            parameters[47].Value = model.This_dZspd;
            parameters[48].Value = model.This_dAvgHar;
            parameters[49].Value = model.This_dAvgVar;
            parameters[50].Value = model.This_dAvgSd;
            parameters[51].Value = model.Ac_dAvgHar;
            parameters[52].Value = model.Ac_dAvgVar;
            parameters[53].Value = model.Ac_dAvgSd;
            parameters[54].Value = model.CYC;
            parameters[55].Value = model.AvgHar;
            parameters[56].Value = model.CorrectAzimuth;
            parameters[57].Value = model.AvgVar;
            parameters[58].Value = model.dAvgSD;
            parameters[59].Value = model.N;
            parameters[60].Value = model.E;
            parameters[61].Value = model.Z;
            parameters[62].Value = model.dAvgHar;
            parameters[63].Value = model.dAvgVar;
            parameters[64].Value = model.dAvgZeroAng;
            parameters[65].Value = model.IsControlPoint;
            parameters[66].Value = model.dCorrectAzimuth;
            parameters[67].Value = model.calculationMethod;
            parameters[68].Value = model.siblingpointname;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SurveyDataUnion(string xmname, int cyc, int targetcyc, int lastcyc)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            strSql.Append("replace into `fmos_cycdirnet_data`  select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd," + lastcyc + @",TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod,siblingpoint_name from fmos_cycdirnet_data  where  taskname='" + xmname + "'  and    cyc in (" + targetcyc + "," + cyc + ")");


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool SurveyDataCouldUnion(string xmname, int cyc, int targetcyc)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            strSql.AppendFormat(" select * from fmos_cycdirnet_data where cyc = {0} and taskname = {1} and POINT_NAME in (select POINT_NAME from fmos_cycdirnet_data where cyc = {2} and taskname = {1})", cyc, xmname, targetcyc);
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool AddData(string taskname, int cyc)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(taskname);
            strSql.Append("REPLACE into fmos_cycdirnet_data (select * from fmos_cycdirnet   where   taskname=@taskname     and    cyc = @cyc  and    point_name in ( select distinct(point_name) from  fmos_pointalarmvalue where xmno = " + taskname + "  ) )");

            OdbcParameter[] parameters = { new OdbcParameter("@taskname", OdbcType.VarChar), new OdbcParameter("@cyc", OdbcType.Int) };
            parameters[0].Value = taskname;
            parameters[1].Value = cyc;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddCgData(string xmname, int cyc, int importcyc, out int rows)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection Conn = db.GetSurveyStanderConn(xmname);
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            OdbcConnection CgConn = db.GetStanderCgConn(xmname);
            strSql.AppendFormat("REPLACE into {0}.fmos_cycdirnet (point_name,cyc,taskname,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,siblingpoint_name,time) select point_name," + importcyc + " as    cyc ,taskname,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,siblingpoint_name,time from {1}.fmos_cycdirnet_data   where   taskname=@taskname     and    cyc = @cyc  and   point_name in ( select distinct(point_name) from  {1}.fmos_pointalarmvalue where xmno = " + xmname + " ) ", CgConn.Database, Conn.Database);
            OdbcParameter[] parameters = { new OdbcParameter("@taskname", OdbcType.VarChar), new OdbcParameter("@cyc", OdbcType.Int) };
            parameters[0].Value = xmname;
            parameters[1].Value = cyc;

            rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CgFindSurveyOriginCyc(string taskname, out int cyc)
        {
            cyc = 0;
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(taskname);
            string sql = string.Format("select * from cggt.fmos_cycdirnet a  join gt.fmos_cycdirnet_data b  on    a.taskname = b.taskName and a.POINT_NAME = b.POINT_NAME and a.time = b.time   where  a.TaskName = 103   and a.cyc = (select max(c.cyc) from cggt.fmos_cycdirnet c where c.taskname = {0} ) group by a.cyc", taskname);
            Object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, sql);
            if (obj == null) return false;
            cyc = Convert.ToInt32(obj);
            return true;
        }

        public bool ReportDataView(List<string> cyclist, int startPageIndex, int pageSize, string xmname, string sord, out DataTable dt)
        {
            dt = new DataTable();

            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcConnection surveyconn = db.GetSurveyStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select point_name,CYC,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time from fmos_cycdirnet where      taskName=@taskName         and    point_name in (select distinct(point_name) from " + surveyconn.Database + ".fmos_pointalarmvalue where xmno =" + xmname + " )  and   CYC    in ({0})  order by {1}  asc  limit    @startPageIndex,   @endPageIndex", string.Join(",", cyclist), sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = (startPageIndex - 1) * pageSize;
            parameters[2].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }


        public bool LostPoints(int xmno, string xmname, int cyc, out string pointstr)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            strSql.AppendFormat("select distinct(POINT_NAME) from fmos_pointalarmvalue where  xmno={0}  and fmos_pointalarmvalue.POINT_NAME not in ( select POINT_NAME from fmos_cycdirnet_data where taskname= '{1}' and cyc = {2} )", xmno, xmname, cyc);
            List<string> ls = querysql.querystanderlist(strSql.ToString(), conn);
            pointstr = string.Join(",", ls);
            return true;
        }


        public bool UpdataSurveyData(TotalStation.Model.fmos_obj.cycdirnet model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.TaskName);
            strSql.AppendFormat(@"update fmos_cycdirnet_data set this_dn = this_dn - ac_dn +{0}
                ,this_de = this_de - ac_de +{1}
,this_dz = this_dz - ac_dz +{2},ac_dn = {0},ac_de = {1},ac_dz={2}  where    taskname= '{3}'    and point_name  =   '{4}'   and  cyc =   {5}   ", model.Ac_dN, model.Ac_dE, model.Ac_dZ, model.TaskName, model.POINT_NAME, model.CYC);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdataSurveyDataNextCYCThis(TotalStation.Model.fmos_obj.cycdirnet model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.TaskName);
            strSql.AppendFormat("update fmos_cycdirnet_data set   where this_dn = ac_dn -{0},this_de = ac_de - {1},this_dz = ac_dz - {2}   taskname= '{3}'    and point_name  =   '{4}'   and  cyc =   {5}   ", model.Ac_dN, model.Ac_dE, model.Ac_dZ, model.TaskName, model.POINT_NAME, model.CYC + 1);
            ExceptionLog.ExceptionWrite(strSql.ToString());
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public bool SurveyPointCycDataAdd(string xmname, string pointname, int cyc, int importcyc)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            strSql.AppendFormat(@"insert into fmos_cycdirnet_data (point_name,cyc,taskname,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time) select point_name," + importcyc + " as cyc,taskname,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,(select min(time) from fmos_cycdirnet_data where taskname='" + xmname + "' and  cyc = " + importcyc + "  ) as time from  fmos_cycdirnet where taskname = '" + xmname + "'  and   point_name   =   '" + pointname + "'   and     cyc=" + cyc);

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.cycdirnet model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(model.TaskName);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update fmos_cycdirnet set ");
            strSql.Append("This_dN=@This_dN,");
            strSql.Append("This_dE=@This_dE,");
            strSql.Append("This_dZ=@This_dZ,");
            strSql.Append("Ac_dN=@Ac_dN,");
            strSql.Append("Ac_dE=@Ac_dE,");
            strSql.Append("Ac_dZ=@Ac_dZ,");
            strSql.Append("Ac_dNspd=@Ac_dNspd,");
            strSql.Append("Ac_dEspd=@Ac_dEspd,");
            strSql.Append("Ac_dZspd=@Ac_dZspd,");
            strSql.Append("This_dNspd=@This_dNspd,");
            strSql.Append("This_dEspd=@This_dEspd,");
            strSql.Append("This_dZspd=@This_dZspd,");
            strSql.Append("This_dAvgHar=@This_dAvgHar,");
            strSql.Append("This_dAvgVar=@This_dAvgVar,");
            strSql.Append("This_dAvgSd=@This_dAvgSd,");
            strSql.Append("Ac_dAvgHar=@Ac_dAvgHar,");
            strSql.Append("Ac_dAvgVar=@Ac_dAvgVar,");
            strSql.Append("Ac_dAvgSd=@Ac_dAvgSd,");
            strSql.Append("AvgHar=@AvgHar,");
            strSql.Append("CorrectAzimuth=@CorrectAzimuth,");
            strSql.Append("AvgVar=@AvgVar,");
            strSql.Append("dAvgSD=@dAvgSD,");
            strSql.Append("N=@N,");
            strSql.Append("E=@E,");
            strSql.Append("Z=@Z,");
            strSql.Append("dAvgHar=@dAvgHar,");
            strSql.Append("dAvgVar=@dAvgVar,");
            strSql.Append("dAvgZeroAng=@dAvgZeroAng,");
            strSql.Append("IsControlPoint=@IsControlPoint,");
            strSql.Append("dCorrectAzimuth=@dCorrectAzimuth,");
            strSql.Append("calculationMethod=@calculationMethod");
            strSql.Append("    where     TaskName=@TaskName     and     Time=@Time     and      POINT_NAME=@POINT_NAME ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@This_dN", OdbcType.Double),
					new OdbcParameter("@This_dE", OdbcType.Double),
					new OdbcParameter("@This_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dN", OdbcType.Double),
					new OdbcParameter("@Ac_dE", OdbcType.Double),
					new OdbcParameter("@Ac_dZ", OdbcType.Double),
					new OdbcParameter("@Ac_dNspd", OdbcType.Double),
					new OdbcParameter("@Ac_dEspd", OdbcType.Double),
					new OdbcParameter("@Ac_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dNspd", OdbcType.Double),
					new OdbcParameter("@This_dEspd", OdbcType.Double),
					new OdbcParameter("@This_dZspd", OdbcType.Double),
					new OdbcParameter("@This_dAvgHar", OdbcType.Double),
					new OdbcParameter("@This_dAvgVar", OdbcType.Double),
					new OdbcParameter("@This_dAvgSd", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgHar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgVar", OdbcType.Double),
					new OdbcParameter("@Ac_dAvgSd", OdbcType.Double),
					new OdbcParameter("@AvgHar", OdbcType.VarChar,120),
					new OdbcParameter("@CorrectAzimuth", OdbcType.VarChar,120),
					new OdbcParameter("@AvgVar", OdbcType.VarChar,120),
					new OdbcParameter("@dAvgSD", OdbcType.Double),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@dAvgHar", OdbcType.Double),
					new OdbcParameter("@dAvgVar", OdbcType.Double),
					new OdbcParameter("@dAvgZeroAng", OdbcType.Double),
					new OdbcParameter("@IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("@dCorrectAzimuth", OdbcType.Double),
					new OdbcParameter("@calculationMethod", OdbcType.VarChar,120),
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@Time", OdbcType.DateTime),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120)};
            parameters[0].Value = model.This_dN;
            parameters[1].Value = model.This_dE;
            parameters[2].Value = model.This_dZ;
            parameters[3].Value = model.Ac_dN;
            parameters[4].Value = model.Ac_dE;
            parameters[5].Value = model.Ac_dZ;
            parameters[6].Value = model.Ac_dNspd;
            parameters[7].Value = model.Ac_dEspd;
            parameters[8].Value = model.Ac_dZspd;
            parameters[9].Value = model.This_dNspd;
            parameters[10].Value = model.This_dEspd;
            parameters[11].Value = model.This_dZspd;
            parameters[12].Value = model.This_dAvgHar;
            parameters[13].Value = model.This_dAvgVar;
            parameters[14].Value = model.This_dAvgSd;
            parameters[15].Value = model.Ac_dAvgHar;
            parameters[16].Value = model.Ac_dAvgVar;
            parameters[17].Value = model.Ac_dAvgSd;
            parameters[18].Value = model.AvgHar;
            parameters[19].Value = model.CorrectAzimuth;
            parameters[20].Value = model.AvgVar;
            parameters[21].Value = model.dAvgSD;
            parameters[22].Value = model.N;
            parameters[23].Value = model.E;
            parameters[24].Value = model.Z;
            parameters[25].Value = model.dAvgHar;
            parameters[26].Value = model.dAvgVar;
            parameters[27].Value = model.dAvgZeroAng;
            parameters[28].Value = model.IsControlPoint;
            parameters[29].Value = model.dCorrectAzimuth;
            parameters[30].Value = model.calculationMethod;
            parameters[31].Value = model.TaskName;
            parameters[32].Value = model.Time;
            parameters[33].Value = model.POINT_NAME;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(Model.fmos_obj.cycdirnet model)
        {
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.TaskName);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from fmos_cycdirnet ");
            strSql.Append(" where    TaskName=@TaskName     and     Time=@Time      and      POINT_NAME=@POINT_NAME ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@Time", OdbcType.DateTime),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120)			};
            parameters[0].Value = model.TaskName;
            parameters[1].Value = model.Time;
            parameters[2].Value = model.POINT_NAME;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int startCyc, int endCyc, string xmname, string pointname)
        {
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from fmos_cycdirnet where      taskName=@taskName      and    {0}    and     CYC    between    @startCyc      and    @endCyc  ", searchstr);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@startCyc", OdbcType.Int),
					new OdbcParameter("@endCyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = startCyc;
            parameters[2].Value = endCyc;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除成果数据
        /// </summary>
        public bool DeleteCg(int startCyc, int endCyc, string xmname, string pointname)
        {
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from fmos_cycdirnet where      taskName=@taskName      and    {0}    and     CYC    between    @startCyc      and    @endCyc  ", searchstr);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@startCyc", OdbcType.Int),
					new OdbcParameter("@endCyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = startCyc;
            parameters[2].Value = endCyc;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteSurveyData(int startCyc, int endCyc, string xmname, string pointname)
        {
            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("delete  from fmos_cycdirnet_data where      taskName=@taskName      and    {0}    and     CYC    between    @startCyc      and    @endCyc  ", searchstr);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@startCyc", OdbcType.Int),
					new OdbcParameter("@endCyc", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = startCyc;
            parameters[2].Value = endCyc;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteTmp(string TaskName)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(TaskName);
            strSql.Append("delete from fmos_cycdirnet_tmp ");
            strSql.Append(" where TaskName=@TaskName ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120)
							};
            parameters[0].Value = TaskName;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteCgTmp(string TaskName)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderCgConn(TaskName);
            strSql.Append("delete from fmos_cycdirnet_tmp ");
            strSql.Append(" where TaskName=@TaskName ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120)
							};
            parameters[0].Value = TaskName;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SurveyResultdataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname, string sord, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select point_name,CYC,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time from fmos_cycdirnet_data   where      taskName=@taskName      and    {0}    and     CYC    between    @startCyc      and    @endCyc  order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@startCyc", OdbcType.Int),
					new OdbcParameter("@endCyc", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = startCyc;
            parameters[2].Value = endCyc;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }


        public bool ResultdataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname, string sord, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select point_name,CYC,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time from fmos_cycdirnet where      taskName=@taskName      and    {0}    and     CYC    between    @startCyc      and    @endCyc  order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@startCyc", OdbcType.Int),
					new OdbcParameter("@endCyc", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = startCyc;
            parameters[2].Value = endCyc;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool CgResultdataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname, string sord, out DataTable dt)
        {
            dt = new DataTable();

            string searchstr = pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? "  1=1  " : string.Format("point_name='{0}'", pointname);
            OdbcConnection conn = db.GetCgStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select point_name,CYC,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time from fmos_cycdirnet where      taskName=@taskName      and    {0}    and     CYC    between    @startCyc      and    @endCyc  order by {1}  asc  limit    @startPageIndex,   @endPageIndex", searchstr, sord);
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@startCyc", OdbcType.Int),
					new OdbcParameter("@endCyc", OdbcType.Int),
                    new OdbcParameter("@startPageIndex", OdbcType.Int),
                    new OdbcParameter("@endPageIndex", OdbcType.Int)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = startCyc;
            parameters[2].Value = endCyc;
            parameters[3].Value = (startPageIndex - 1) * pageSize;
            parameters[4].Value = pageSize * startPageIndex;

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0]; return true;

        }

        public bool ResultTableRowsCount(int startCyc, int endCyc, string xmname, string pointname, out string totalCont)
        {
            string searchstr = pointname == null || pointname == "" || (pointname.IndexOf("全部") != -1) ? "   1=1   " : string.Format("  point_name = '{0}'  ", pointname);
            string sql = string.Format("select count(1) from fmos_cycdirnet where taskName='{2}' and {3}  and CYC between {0} and {1}", startCyc, endCyc, xmname, searchstr);
            OdbcConnection conn = db.GetStanderConn(xmname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }

        public bool ResultDataReportPrint(string sql, string xmname, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            dt = querysql.querystanderdb(sql, conn);
            return true;
        }

        //根据项目名称获取端点周期
        public bool ExtremelyCycGet(string xmname, string DateFunction, out string ExtremelyCyc)
        {

            string sql = "select  " + DateFunction + "(cyc)  from fmos_cycdirnet where taskName='" + xmname + "'";
            OdbcConnection conn = db.GetStanderConn(xmname);
            ExtremelyCyc = querysql.querystanderstr(sql, conn);
            if (ExtremelyCyc == "") ExtremelyCyc = "0";
            return ExtremelyCyc != null ? true : false;

        }
        //根据项目名获取所以的周期
        public bool CycList(string xmname, out DataTable dt)
        {

            string sql = "select  distinct(cyc)  from fmos_cycdirnet where taskName='" + xmname + "'";
            OdbcConnection conn = db.GetStanderConn(xmname);
            dt = querysql.querystanderdb(sql, conn);
            return dt != null ? true : false;
        }
        public bool CycTimeList(string xmname, out List<string> cycTimeList)
        {

            string sql = "SELECT distinct(cyc),time ,taskname FROM `fmos_cycdirnet` where taskname = " + xmname + "    group by cyc order by cyc asc ;";
            OdbcConnection conn = db.GetStanderConn(xmname);
            DataTable dt = querysql.querystanderdb(sql, conn);
            cycTimeList = new List<string>();
            if (dt.Rows.Count == 0) return false;
            int i = 0;
            for (i = 0; i < dt.Rows.Count; i++)
            {
                cycTimeList.Add(string.Format("{0}[{1}]", dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1]));
            }
            return true;
        }
        //根据项目名获取所以的周期
        /// <summary>
        /// 从起始和结束时间中获取周期
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="pointstr"></param>
        /// <param name="CycList"></param>
        /// <returns></returns>
        public bool GetCycFromDateTime(string xmname, string startTime, string endTime, string pointstr, out List<string> CycList)
        {
            CycList = new List<string>();
            string sql = @"select min(cyc) from fmos_cycdirnet where fmos_cycdirnet.Time >= DATE_FORMAT('" + startTime + "','%y-%m-%d %H:%i:%s') and fmos_cycdirnet.Time <= DATE_FORMAT('" + endTime + "','%y-%m-%d %H:%i:%s') and point_name in (" + pointstr + ") and  taskname='" + xmname + "'  ";
            OdbcConnection conn = db.GetStanderConn(xmname);
            string maxCyc = querysql.querystanderstr(sql, conn);
            CycList.Add(maxCyc);
            sql = @"select max(cyc) from fmos_cycdirnet where fmos_cycdirnet.Time >= DATE_FORMAT('" + startTime + "','%y-%m-%d %H:%i:%s') and fmos_cycdirnet.Time <= DATE_FORMAT('" + endTime + "','%y-%m-%d %H:%i:%s') and point_name in (" + pointstr + ")  and  taskname='" + xmname + "'  ";
            string minCyc = querysql.querystanderstr(sql, conn);
            CycList.Add(minCyc);
            return true;
        }

        //public bool GetCycdirnetAlarmModelList(string xmname,string DataTime , out object obj)
        //{
        //    obj = null;
        //    string sql = "select *  ";
        //}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(string TaskName, out List<TotalStation.Model.fmos_obj.cycdirnet> lt)
        {
            OdbcConnection conn = db.GetStanderConn(TaskName);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod from fmos_cycdirnet_tmp ");
            strSql.Append(" where    TaskName=@TaskName   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120)
							};
            parameters[0].Value = TaskName;

            lt = new List<Model.fmos_obj.cycdirnet>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetCgModelList(string TaskName, out List<TotalStation.Model.fmos_obj.cycdirnet> lt)
        {
            OdbcConnection conn = db.GetCgStanderConn(TaskName);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod from fmos_cycdirnet_tmp ");
            strSql.Append(" where    TaskName=@TaskName   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120)
							};
            parameters[0].Value = TaskName;

            lt = new List<Model.fmos_obj.cycdirnet>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(DataTable dt, out List<TotalStation.Model.fmos_obj.cycdirnet> lt)
        {
            lt = new List<Model.fmos_obj.cycdirnet>();
            int i = 0;
            while (i < dt.Rows.Count)
            {
                lt.Add(DataReportRowToModel(dt.Rows[i]));
                i++;
            }
            return true;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetTable(string xmname, List<TotalStation.Model.fmos_obj.cycdirnet> lt, out DataTable dt)
        {
            dt = new DataTable();
            dt.Columns.Add("point_name");
            dt.Columns.Add("cyc");
            dt.Columns.Add("N");
            dt.Columns.Add("E");
            dt.Columns.Add("Z");
            dt.Columns.Add("this_dn");
            dt.Columns.Add("this_de");
            dt.Columns.Add("this_dz");
            dt.Columns.Add("ac_dn");
            dt.Columns.Add("ac_de");
            dt.Columns.Add("ac_dz");
            dt.Columns.Add("time");
            //string sql = "select point_name, cyc, N, E, Z, this_dn, this_de, this_dz, ac_dn, ac_de, ac_dz,time from fmos_cycdirnet  where 1=0 ";
            //OdbcConnection conn = db.GetStanderConn(xmname);
            //dt = querysql.querystanderdb(sql,conn);
            int i = 0;
            while (i < lt.Count)
            {
                dt.Rows.Add(ModelToDataRow(lt[i], dt.NewRow()));
                i++;
            }
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.cycdirnet DataRowToModel(DataRow row)
        {
            TotalStation.Model.fmos_obj.cycdirnet model = new TotalStation.Model.fmos_obj.cycdirnet();
            if (row != null)
            {
                if (row["This_dN"] != null && row["This_dN"].ToString() != "")
                {
                    model.This_dN = double.Parse(row["This_dN"].ToString());
                }
                if (row["This_dE"] != null && row["This_dE"].ToString() != "")
                {
                    model.This_dE = double.Parse(row["This_dE"].ToString());
                }
                if (row["This_dZ"] != null && row["This_dZ"].ToString() != "")
                {
                    model.This_dZ = double.Parse(row["This_dZ"].ToString());
                }
                if (row["N"] != null && row["N"].ToString() != "")
                {
                    model.N = double.Parse(row["N"].ToString());
                }
                if (row["E"] != null && row["E"].ToString() != "")
                {
                    model.E = double.Parse(row["E"].ToString());
                }
                if (row["Z"] != null && row["Z"].ToString() != "")
                {
                    model.Z = double.Parse(row["Z"].ToString());
                }
                if (row["Ac_dN"] != null && row["Ac_dN"].ToString() != "")
                {
                    model.Ac_dN = double.Parse(row["Ac_dN"].ToString());
                }
                if (row["Ac_dE"] != null && row["Ac_dE"].ToString() != "")
                {
                    model.Ac_dE = double.Parse(row["Ac_dE"].ToString());
                }
                if (row["Ac_dZ"] != null && row["Ac_dZ"].ToString() != "")
                {
                    model.Ac_dZ = double.Parse(row["Ac_dZ"].ToString());
                }
                if (row["CYC"] != null && row["CYC"].ToString() != "")
                {
                    model.CYC = int.Parse(row["CYC"].ToString());
                }
                if (row["TaskName"] != null)
                {
                    model.TaskName = row["TaskName"].ToString();
                }
                if (row["Time"] != null && row["Time"].ToString() != "")
                {
                    model.Time = Convert.ToDateTime(row["Time"].ToString());
                }
                if (row["POINT_NAME"] != null)
                {
                    model.POINT_NAME = row["POINT_NAME"].ToString();
                }

            }
            return model;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.cycdirnet DataReportRowToModel(DataRow row)
        {
            TotalStation.Model.fmos_obj.cycdirnet model = new TotalStation.Model.fmos_obj.cycdirnet();
            if (row != null)
            {
                if (row["This_dN"] != null && row["This_dN"].ToString() != "")
                {
                    model.This_dN = double.Parse(row["This_dN"].ToString());
                }
                if (row["This_dE"] != null && row["This_dE"].ToString() != "")
                {
                    model.This_dE = double.Parse(row["This_dE"].ToString());
                }
                if (row["This_dZ"] != null && row["This_dZ"].ToString() != "")
                {
                    model.This_dZ = double.Parse(row["This_dZ"].ToString());
                }
                if (row["N"] != null && row["N"].ToString() != "")
                {
                    model.N = double.Parse(row["N"].ToString());
                }
                if (row["E"] != null && row["E"].ToString() != "")
                {
                    model.E = double.Parse(row["E"].ToString());
                }
                if (row["Z"] != null && row["Z"].ToString() != "")
                {
                    model.Z = double.Parse(row["Z"].ToString());
                }
                if (row["Ac_dN"] != null && row["Ac_dN"].ToString() != "")
                {
                    model.Ac_dN = double.Parse(row["Ac_dN"].ToString());
                }
                if (row["Ac_dE"] != null && row["Ac_dE"].ToString() != "")
                {
                    model.Ac_dE = double.Parse(row["Ac_dE"].ToString());
                }
                if (row["Ac_dZ"] != null && row["Ac_dZ"].ToString() != "")
                {
                    model.Ac_dZ = double.Parse(row["Ac_dZ"].ToString());
                }
                if (row["CYC"] != null && row["CYC"].ToString() != "")
                {
                    model.CYC = int.Parse(row["CYC"].ToString());
                }
                if (row["Time"] != null && row["Time"].ToString() != "")
                {
                    model.Time = Convert.ToDateTime(row["Time"].ToString());
                }
                if (row["POINT_NAME"] != null)
                {
                    model.POINT_NAME = row["POINT_NAME"].ToString();
                }

            }
            return model;
        }



        public DataRow ModelToDataRow(TotalStation.Model.fmos_obj.cycdirnet model, DataRow dr)
        {
            if (model != null)
            {
                if (model.This_dN != null && model.This_dN.ToString() != "")
                {
                    dr["This_dN"] = double.Parse(model.This_dN.ToString());
                }
                if (model.This_dE != null && model.This_dE.ToString() != "")
                {
                    dr["This_dE"] = double.Parse(model.This_dE.ToString());
                }
                if (model.This_dZ != null && model.This_dZ.ToString() != "")
                {
                    dr["This_dZ"] = double.Parse(model.This_dZ.ToString());
                }
                if (model.N != null && model.N.ToString() != "")
                {
                    dr["N"] = double.Parse(model.N.ToString());
                }
                if (model.E != null && model.E.ToString() != "")
                {
                    dr["E"] = double.Parse(model.E.ToString());
                }
                if (model.Z != null && model.Z.ToString() != "")
                {
                    dr["Z"] = double.Parse(model.Z.ToString());
                }
                if (model.Ac_dN != null && model.Ac_dN.ToString() != "")
                {
                    dr["Ac_dN"] = double.Parse(model.Ac_dN.ToString());
                }
                if (model.Ac_dE != null && model.Ac_dE.ToString() != "")
                {
                    dr["Ac_dE"] = double.Parse(model.Ac_dE.ToString());
                }
                if (model.Ac_dZ != null && model.Ac_dZ.ToString() != "")
                {
                    dr["Ac_dZ"] = double.Parse(model.Ac_dZ.ToString());
                }
                if (model.CYC != null && model.CYC.ToString() != "")
                {
                    dr["CYC"] = int.Parse(model.CYC.ToString());
                }
                if (model.TaskName != null)
                {
                    dr["TaskName"] = model.TaskName.ToString();
                }
                if (model.Time != null && model.Time.ToString() != "")
                {
                    dr["Time"] = Convert.ToDateTime(model.Time.ToString());
                }
                if (model.POINT_NAME != null)
                {
                    dr["POINT_NAME"] = model.POINT_NAME.ToString();
                }

            }
            return dr;

        }




        public bool XmLastDate(string xmname, out string dateTime)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            string sql = "select max(time) from fmos_cycdirnet where taskName='" + xmname + "'";
            dateTime = querysql.querystanderstr(sql, conn);
            return dateTime == null ? false : true;

        }


        public bool ResultdataSearch(string xmname, string pointName, DateTime time, out DataTable dt)
        {

            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select point_name,CYC,time,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz from fmos_cycdirnet where taskName='{0}'   and point_name='{1}' and {2} ", xmname, pointName, time == Convert.ToDateTime("2017-1-1") ? " 1=1 " : " time > '" + time + "'");
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool ResultdataSearch(string xmname, string pointName, DateTime timestart, DateTime timeend, out DataTable dt)
        {

            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select point_name,CYC,time,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz from fmos_cycdirnet where    taskName= @xmname     and       point_name=  @point_name     and     time   >  @timestart     and      time   <    @timeend ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmname", OdbcType.VarChar,100),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@timestart", OdbcType.DateTime),
                    new OdbcParameter("@timeend", OdbcType.DateTime)
                                         };
            parameters[0].Value = xmname;
            parameters[1].Value = pointName;
            parameters[2].Value = timestart;
            parameters[3].Value = timeend;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) { dt = null; return false; }
            dt = ds.Tables[0];
            return true;
            //string sql = string.Format("select point_name,CYC,time,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz from fmos_cycdirnet where taskName='{0}'   and point_name='{1}' and {2} ", xmname, pointName, time == Convert.ToDateTime("2017-1-1") ? " 1=1 " : " time > " + Convert.ToDateTime(time));
            //dt = querysql.querystanderdb(sql, conn);
            //if (dt != null) return true;
            //return false;

        }

        public bool Singlepointcycload(string xmname, string pointname, out DataTable dt)
        {
            dt = null;
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select distinct(cyc),concat('第',cyc,'周期[',DATE_FORMAT(time,   '%y-%m-%d %H:%i:%S'),']') as timecyc  from fmos_cycdirnet where     taskName=@taskName    and      point_name=@point_name    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120)
                    
							};
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0];
            return true;
        }

        public bool Singlecycdirnetdataload(string xmname, string pointname, int startcyc, int endcyc, out DataTable dt)
        {
            dt = null;
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select point_name,CYC,N,E,Z,this_dn,this_de,this_dz,ac_dn,ac_de,ac_dz,time from fmos_cycdirnet where     taskName=@taskName    and       point_name=@point_name     and    cyc>=@startcyc    and    cyc <= @endcyc   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@startcyc", OdbcType.Int),
                    new OdbcParameter("@endcyc", OdbcType.Int)
                    
							};
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            parameters[2].Value = startcyc;
            parameters[3].Value = endcyc;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            dt = ds.Tables[0];
            return true;
        }

        public bool GetModel(string TaskName, string pointname, int cyc, out TotalStation.Model.fmos_obj.cycdirnet model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(TaskName);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod from fmos_cycdirnet ");
            strSql.Append(" where    TaskName=@TaskName   and   point_name=@point_name    and    cyc = @cyc   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@cyc", OdbcType.Int,4)
							};
            parameters[0].Value = TaskName;
            parameters[1].Value = pointname;
            parameters[2].Value = cyc;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }
        public bool GetModel(string TaskName, string pointname, DateTime dt, out TotalStation.Model.fmos_obj.cycdirnet model)
        {
            model = null;
            OdbcConnection conn = db.GetStanderConn(TaskName);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod from fmos_cycdirnet ");
            strSql.Append(" where    TaskName=@TaskName   and   point_name=@point_name    and    time = @time   ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
					new OdbcParameter("@point_name", OdbcType.VarChar,120),
                    new OdbcParameter("@time", OdbcType.DateTime)
							};
            parameters[0].Value = TaskName;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }

        public bool MaxTime(string xmname, out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from fmos_cycdirnet  where   taskName = @xmname");
            OdbcParameter[] paramters = { 
                new OdbcParameter("@xmname",OdbcType.VarChar,100)
            };
            paramters[0].Value = xmname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), paramters);
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        public bool TotalStationPointLoadDAL(int xmno, out List<string> ls)
        {

            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = "select DISTINCT(POINT_NAME) from fmos_pointalarmvalue where xmno=" + xmno + " order by point_name asc ";
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }

        public bool PointNameCycListGet(string xmname, string pointname, out List<string> ls)
        {

            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select distinct(cyc) from fmos_cycdirnet    where         taskName=@taskName     and      {0}   ", pointname == "" || pointname == null || (pointname.IndexOf("全部") != -1) ? " 1=1 " : string.Format("  point_name=  '{0}'", pointname));
            ls = new List<string>();
            OdbcParameter[] parameters = {
					new OdbcParameter("@taskName", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmname;
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds == null) return false;
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(ds.Tables[0].Rows[i].ItemArray[0].ToString());
                i++;
            }
            return true;

        }

        public bool PointNewestDateTimeGet(string xmname, string pointname, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from fmos_cycdirnet where    taskName=@taskName   and point_name = @point_name  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@taskName", OdbcType.VarChar,120),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) { dt = Convert.ToDateTime("0001-1-1"); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }

        //public bool XmState(int xmno,string xmname,DateTime)
        //{
        //    StringBuilder strSql = new StringBuilder(256);
        //    strSql.AppendFormat(@"SELECT iF((SELECT IFNULL(MAX(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from fmos_cycdirnet where taskname ='"+xmname+@"' ) > DATE_ADD(SYSDATE(),INTERVAL -(select  IFNULL(hour*60+minute,24*60) from totalstationdatauploadinterval  where  xmname='"+xmname+@"') minute),'true','false' ) as state ,(select count(1) from pointcheck  where xmno="+xmno+" and type='表面位移') as alarmcount from fmos_cycdirnet where taskname='"+xmname+@"'");
        //}

        public bool XmStateTable(int startPageIndex, int pageSize, int xmno, string xmname, string unitname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            StringBuilder strSql = new StringBuilder(256);
            strSql.AppendFormat(@"SELECT distinct(taskname) as xmname,'" + unitname + "' as unitname ,max(time) as lastrectime,iF((SELECT IFNULL(MAX(time),DATE_ADD(SYSDATE(),INTERVAL -2 day)) from fmos_cycdirnet where taskname ='" + xmname + @"' ) > DATE_ADD(SYSDATE(),INTERVAL -IFNULL((select  hour*60+minute from totalstationdatauploadinterval  where  xmname='" + xmname + @"'),24*60) minute),'true','false' ) as state ,(select count(1) from pointcheck  where xmno=" + xmno + " and type='表面位移') as alarmcount from fmos_cycdirnet where taskname='" + xmname + @"'");
            //Tool.ExceptionLog.ExceptionWrite(strSql);
            dt = querysql.querystanderdb(strSql.ToString(), conn);
            if (dt != null) return true;
            return false;
        }

        //public bool CycQueueCreate(string taskname,string pointname,DateTime dt)
        //{
        //    string sql = "select cyc from fmos_cycdirnet where point_name ='"+pointname+"' and  taskname='"+taskname+"'  "; 
        //}
        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(string xmname, DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select count(1) from fmos_cycdirnet where    time > @time        and    taskname=@taskname  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@time",OdbcType.DateTime),
                 new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = dt;
            parameters[1].Value = xmname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);

            return Convert.ToInt32(obj) > 0 ? true : false;
        }
        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(string xmname, DateTime dt, out  int cyc)
        {
            cyc = -1;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select    min(cyc)    from    fmos_cycdirnet    where     time>@time    and     taskname=@taskname ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@time",OdbcType.DateTime),
                  new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = dt;
            parameters[1].Value = xmname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            if (obj == null) return false;
            cyc = Convert.ToInt32(obj);
            return true;
        }
        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool InsertCycStep(string xmname, int startcyc)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("update    fmos_cycdirnet    set     cyc = cyc+1     where      cyc>@cyc       and       taskname=@taskname ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@cyc",OdbcType.Int),
                  new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = startcyc - 1;
            parameters[1].Value = xmname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strsql.ToString(), parameters);
            return rows > 0 ? true : false;
        }

        public bool DeleteCgDataCycOnChain(string xmname, int startcyc)
        {
            OdbcSQLHelper.Conn = db.GetCgStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("update    fmos_cycdirnet    set     cyc = cyc-1     where      cyc>@cyc       and       taskname=@taskname ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@cyc",OdbcType.Int),
                  new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = startcyc;
            parameters[1].Value = xmname;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strsql.ToString(), parameters);
            return rows > 0 ? true : false;
        }

        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(string xmname, int cyc)
        {
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select count(1) from fmos_cycdirnet where    cyc =@cyc        and    taskname=@taskname  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@cyc",OdbcType.Int),
                 new OdbcParameter("@taskname",OdbcType.VarChar,100)
             };
            parameters[0].Value = cyc;
            parameters[1].Value = xmname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            return Convert.ToInt32(obj) > 0 ? true : false;
        }

        public bool IsCycdirnetDataExist(string xmname, string pointname, DateTime dt,out int cyc)
        {
            cyc = -1;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select    cyc     from     fmos_cycdirnet    where     taskname=@taskname     and     point_name =@point_name    and        time=@time  ");
            OdbcParameter[] parameters ={ 
                 new OdbcParameter("@taskname",OdbcType.VarChar,100),
                 new OdbcParameter("@point_name",OdbcType.VarChar,100),
                 new OdbcParameter("@time",OdbcType.DateTime)
             };
            parameters[0].Value = xmname;
            parameters[1].Value = pointname;
            parameters[2].Value = dt;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString(), parameters);
            if (obj == null) return false;
            cyc = Convert.ToInt32(obj);
            return true;
        }

        public bool CycModelList(string xmname, int cyc, out List<TotalStation.Model.fmos_obj.cycdirnet> cycdirnetModelList)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select This_dN,This_dE,This_dZ,Ac_dN,Ac_dE,Ac_dZ,Ac_dNspd,Ac_dEspd,Ac_dZspd,This_dNspd,This_dEspd,This_dZspd,This_dAvgHar,This_dAvgVar,This_dAvgSd,Ac_dAvgHar,Ac_dAvgVar,Ac_dAvgSd,CYC,TaskName,AvgHar,CorrectAzimuth,AvgVar,Time,POINT_NAME,dAvgSD,N,E,Z,dAvgHar,dAvgVar,dAvgZeroAng,IsControlPoint,dCorrectAzimuth,calculationMethod from fmos_cycdirnet ");
            strSql.Append(" where    TaskName=@TaskName  and cyc = @cyc  ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@TaskName", OdbcType.VarChar,120),
                    new OdbcParameter("@cyc", OdbcType.VarChar,120)
							};
            parameters[0].Value = xmname;
            parameters[1].Value = cyc;
            cycdirnetModelList = new List<Model.fmos_obj.cycdirnet>();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                cycdirnetModelList.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        public bool CgCycList(string xmname, out List<string> ls)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderCgConn(xmname);
            strSql.AppendFormat("select distinct(cyc)  from  fmos_cycdirnet where taskname='{0}' ", xmname);
            ls = querysql.querystanderlist(strSql.ToString(), conn);
            return true;

        }

        public bool SurveyCycList(string xmname, out List<string> ls)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            strSql.AppendFormat("select distinct(cyc)  from  fmos_cycdirnet_data where taskname='{0}' ", xmname);
            ls = querysql.querystanderlist(strSql.ToString(), conn);
            return true;

        }

        public bool CycdirnetDateTimeCycGet(string xmname, DateTime dt, out int cyc)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetSurveyStanderConn(xmname);
            strSql.AppendFormat("select distinct(cyc)  from  fmos_cycdirnet where taskname='{0}' and time='{1}' ", xmname, dt);
            string cycstr = querysql.querystanderstr(strSql.ToString(), conn);
            cyc = Convert.ToInt32(cycstr);
            return true;
        }
        public bool CgCycdirnetDateTimeCycGet(string xmname, DateTime dt, out int cyc)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetCgStanderConn(xmname);
            strSql.AppendFormat("select distinct(cyc)  from  fmos_cycdirnet where taskname='{0}' and time='{1}' ", xmname, dt);
            string cycstr = querysql.querystanderstr(strSql.ToString(), conn);
            cyc = Convert.ToInt32(cycstr);
            return true;
        }
        public bool IsPointsDataExistInThisCyc(string xmname, List<string> points, int cyc)
        {
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmname);
            string sql = string.Format("select count(1) from fmos_cycdirnet where taskname='{0}' and point_name in ('{1}') and cyc ={2} ", xmname, string.Join("','", points), cyc);
            string cont = querysql.querystanderstr(sql, OdbcSQLHelper.Conn);
            return Convert.ToInt32(cont) > 0 ? true : false;
        }
        /// <summary>
        /// 接入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool SiblingDataCycGet(string xmname, DateTime dt, List<string> points, double mobileStationInteval, out  int cyc)
        {
            cyc = -1;
            OdbcSQLHelper.Conn = db.GetStanderConn(xmname);
            StringBuilder strsql = new StringBuilder();
            strsql.AppendFormat("select  distinct(cyc)  from fmos_cycdirnet   where     time   between   '{0}'    and     '{1}'    and     taskname='{2}' and cyc not in (select  cyc  from    fmos_cycdirnet    where     time   between   '{0}'    and     '{1}'    and     taskname='{2}'  and  point_name  in ('{3}')  group by cyc  )  order by cyc asc ", dt.AddHours(-mobileStationInteval / 2), dt.AddHours(mobileStationInteval / 2), xmname, string.Join("','", points));
            string sql = strsql.ToString();
             
            List<string> ls = querysql.querystanderlist(strsql.ToString(), OdbcSQLHelper.Conn);
            if (ls == null || ls.Count == 0) return false;
            cyc = Convert.ToInt32(ls[0]);
            return true;
        }
        public bool OnLinePointCont(string unitname, List<string> finishedxmnolist, out string contpercent)
        {
            OdbcSQLHelper.Conn = db.GetUnitSurveyStanderConn(unitname);
            OdbcConnection SurveyConn = db.GetUnitStanderConn(unitname);
            contpercent = "0/0";
            string sql = string.Format("select count(distinct(fmos_pointalarmvalue.point_name)),(select count(1) from  fmos_pointalarmvalue )   from fmos_pointalarmvalue left join fmos_cycdirnet on fmos_cycdirnet.TaskName = fmos_pointalarmvalue.xmno  and  fmos_pointalarmvalue.POINT_NAME = fmos_cycdirnet.POINT_NAME  where  {0}    and    time between DATE_ADD(SYSDATE(),INTERVAL -1 day) and SYSDATE()", finishedxmnolist.Count == 0 ? " 1=1 " : " fmos_cycdirnet.TaskName not in (" + string.Join(",", finishedxmnolist) + ")");
            ExceptionLog.ExceptionWrite(sql);
            DataSet ds = OdbcSQLHelper.Query(sql);
            if (ds == null) return false;
            DataTable dt = ds.Tables[0];
            contpercent = string.Format("{0}/{1}", dt.Rows[0].ItemArray[0], dt.Rows[0].ItemArray[1]);//querysql.querystanderstr(sql, 
            return true;
            //if (obj == null) return false;
            //contpercent = obj.ToString() ;

        }
        public bool GetSurveyDataLackPointsModellistFromCgData(int xmno, List<string> pointlist, out List<Model.fmos_obj.cycdirnet> modellist)
        {
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            modellist = new List<Model.fmos_obj.cycdirnet>();
            StringBuilder strsql = new StringBuilder(255);
            strsql.AppendFormat("select * from fmos_cycdirnet where taskname={0} and point_name in ('{1}')  GROUP BY  cyc,POINT_NAME order by cyc desc", xmno, string.Join("','", pointlist));
            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
            int i = 0;
            List<string> tmpls = new List<string>();
            tmpls.AddRange(pointlist);
            TotalStation.Model.fmos_obj.cycdirnet model = new Model.fmos_obj.cycdirnet();
            while (i < ds.Tables[0].Rows.Count)
            {
                if (tmpls.Count == 0) return true;
                model = DataRowToModel(ds.Tables[0].Rows[i]);

                if (tmpls.Contains(model.POINT_NAME))
                {
                    modellist.Add(model);

                    tmpls.Remove(model.POINT_NAME);
                }
                i++;
            }
            return true;
        }

        public bool GetCgTimeSortModellist(int xmno, out List<Model.fmos_obj.cycdirnet> modellist)
        {
            OdbcSQLHelper.Conn = db.GetStanderCgConn(xmno);
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            modellist = new List<Model.fmos_obj.cycdirnet>();
            StringBuilder strsql = new StringBuilder(255);
            strsql.AppendFormat(@"SELECT * FROM fmos_cycdirnet where taskname ={0}   and  cyc = (select  cyc  from fmos_cycdirnet where taskname={0}  GROUP BY cyc HAVING count(1) = (select count(*) from {1}.fmos_pointalarmvalue where xmno ={0}) order by cyc desc LIMIT 0,1)   order by time,point_name  ", xmno, conn.Database);
            ExceptionLog.ExceptionWrite(strsql.ToString());
            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
            int i = 0;
            TotalStation.Model.fmos_obj.cycdirnet model = new Model.fmos_obj.cycdirnet();
            while (i < ds.Tables[0].Rows.Count)
            {
                model = DataRowToModel(ds.Tables[0].Rows[i]);
                modellist.Add(model);


                i++;
            }
            return true;
        }

        public bool GetSurveyCYCModellist(int xmno,int cyc ,out List<Model.fmos_obj.cycdirnet> modellist)
        {
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            modellist = new List<Model.fmos_obj.cycdirnet>();
            StringBuilder strsql = new StringBuilder(255);
            strsql.AppendFormat("SELECT * FROM fmos_cycdirnet_data where taskname ={0} and cyc ={1}  order by time,point_name ", xmno,cyc);
            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
            int i = 0;
            TotalStation.Model.fmos_obj.cycdirnet model = new Model.fmos_obj.cycdirnet();
            while (i < ds.Tables[0].Rows.Count)
            {
                model = DataRowToModel(ds.Tables[0].Rows[i]);
                modellist.Add(model);


                i++;
            }
            return true;
        }


        //SELECT point_name,cyc,time FROM cggt.`fmos_cycdirnet` where taskname = 96 and cyc = 773 order by time,point_name ;
        public bool SurveyDataCycTimeGet(int xmno, int cyc, out DateTime time)
        {
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            StringBuilder strsql = new StringBuilder(255);
            strsql.AppendFormat("select min(time) from fmos_cycdirnet where taskname={0} and cyc={1} ", xmno, cyc);
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strsql.ToString());
            time = obj == null ? new DateTime() : Convert.ToDateTime(obj);
            return true;
        }

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

