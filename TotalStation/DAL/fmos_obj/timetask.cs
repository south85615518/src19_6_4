﻿/**  版本信息模板在安装目录下，可自行修改。
* timetask.cs
*
* 功 能： N/A
* 类 名： timetask
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//using TotalStation.DBUtility;//Please add references
namespace TotalStation.DAL.fmos_obj
{
    /// <summary>
    /// 数据访问类:timetask
    /// </summary>
    public partial class timetask
    {
        public timetask()
        { }
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.timetask model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.taskName);
            strSql.Append("insert into fmos_timetask(");
            strSql.Append("CH,DateTaskName,Indx,TimingHour,TimingMinute,StartTimeType,mInterval,Taskname)");
            strSql.Append(" values (");
            strSql.Append("@CH,@DateTaskName,@Indx,@TimingHour,@TimingMinute,@StartTimeType,@mInterval,@Taskname)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("CH=@CH,");
            strSql.Append("Indx=@Indx,");
            strSql.Append("TimingHour=@TimingHour,");
            strSql.Append("TimingMinute=@TimingMinute,");
            strSql.Append("StartTimeType=@StartTimeType,");
            strSql.Append("mInterval=@mInterval");
            OdbcParameter[] parameters = {
					new OdbcParameter("@CH", OdbcType.Int,11),
					new OdbcParameter("@DateTaskName", OdbcType.VarChar,120),
					new OdbcParameter("@Indx", OdbcType.Int,11),
					new OdbcParameter("@TimingHour", OdbcType.Int,11),
					new OdbcParameter("@TimingMinute", OdbcType.Int,11),
					new OdbcParameter("@StartTimeType", OdbcType.Int,11),
					new OdbcParameter("@mInterval", OdbcType.Int,11),
					new OdbcParameter("@Taskname", OdbcType.VarChar,100),
                    new OdbcParameter("@CH", OdbcType.Int,11),
					new OdbcParameter("@Indx", OdbcType.Int,11),
					new OdbcParameter("@TimingHour", OdbcType.Int,11),
					new OdbcParameter("@TimingMinute", OdbcType.Int,11),
					new OdbcParameter("@StartTimeType", OdbcType.Int,11),
					new OdbcParameter("@mInterval", OdbcType.Int,11)
                                         };
            parameters[0].Value = model.CH;
            parameters[1].Value = model.DateTaskName;
            parameters[2].Value = model.Indx;
            parameters[3].Value = model.TimingHour;
            parameters[4].Value = model.TimingMinute;
            parameters[5].Value = model.StartTimeType;
            parameters[6].Value = model.mInterval;
            parameters[7].Value = model.taskName;

            parameters[8].Value = model.CH;
            parameters[9].Value = model.Indx;
            parameters[10].Value = model.TimingHour;
            parameters[11].Value = model.TimingMinute;
            parameters[12].Value = model.StartTimeType;
            parameters[13].Value = model.mInterval;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.timetask model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.taskName);
            strSql.Append("update fmos_timetask set ");
            strSql.Append("CH=@CH,");
            strSql.Append("Indx=@Indx,");
            strSql.Append("TimingHour=@TimingHour,");
            strSql.Append("TimingMinute=@TimingMinute,");
            strSql.Append("StartTimeType=@StartTimeType,");
            strSql.Append("mInterval=@mInterval");
            strSql.Append(" where DateTaskName=@DateTaskName and Taskname=@Taskname ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@CH", OdbcType.Int,11),
					new OdbcParameter("@Indx", OdbcType.Int,11),
					new OdbcParameter("@TimingHour", OdbcType.Int,11),
					new OdbcParameter("@TimingMinute", OdbcType.Int,11),
					new OdbcParameter("@StartTimeType", OdbcType.Int,11),
					new OdbcParameter("@mInterval", OdbcType.Int,11),
					new OdbcParameter("@DateTaskName", OdbcType.VarChar,220),
					new OdbcParameter("@Taskname", OdbcType.VarChar,200)};
            parameters[0].Value = model.CH;
            parameters[1].Value = model.Indx;
            parameters[2].Value = model.TimingHour;
            parameters[3].Value = model.TimingMinute;
            parameters[4].Value = model.StartTimeType;
            parameters[5].Value = model.mInterval;
            parameters[6].Value = model.DateTaskName;
            parameters[7].Value = model.taskName;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string DateTaskName, string Taskname)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(Taskname);
            strSql.Append("delete from fmos_timetask ");
            strSql.Append(" where     DateTaskName=@DateTaskName     and    Taskname=@Taskname    ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@DateTaskName", OdbcType.VarChar,220),
					new OdbcParameter("@Taskname", OdbcType.VarChar,200)			};
            parameters[0].Value = DateTaskName;
            parameters[1].Value = Taskname;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TimetaskTableLoad(int startPageIndex, int pageSize, string xmname, string searchpost, string colName, string sord, out DataTable dt)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " taskName= '" + xmname + "'");
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select datetaskname,ch,timinghour,timingMinute from fmos_timetask where {2} order by Indx asc  limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, searchstr);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public database db = new database();
        public bool TimetaskTableRowsCount(string searchpost, string xmname, out string totalCont)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " taskName= '" + xmname + "'");
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select count(*) from fmos_timetask where {0}   ", searchstr);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }
        public bool GetModelList(string xmname, List<timetask> lt)
        {


            return false;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.timetask GetModel(string DateTaskName, string taskName)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CH,DateTaskName,Indx,TimingHour,TimingMinute,StartTimeType,mInterval,taskName from fmos_timetask ");
            strSql.Append(" where DateTaskName=@DateTaskName and taskName=@taskName ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@DateTaskName", OdbcType.VarChar,120),
					new OdbcParameter("@taskName", OdbcType.VarChar,100)			};
            parameters[0].Value = DateTaskName;
            parameters[1].Value = taskName;

            TotalStation.Model.fmos_obj.timetask model = new TotalStation.Model.fmos_obj.timetask();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个表中的所有对象实体
        /// </summary>
        public bool GetModelList(string taskName, out List<TotalStation.Model.fmos_obj.timetask> lt)
        {


            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CH,DateTaskName,Indx,TimingHour,TimingMinute,StartTimeType,mInterval,taskName from fmos_timetask ");
            strSql.Append(" where  taskName=@taskName  order by indx");
            OdbcParameter[] parameters = {
					new OdbcParameter("@taskName", OdbcType.VarChar,100)			};
            parameters[0].Value = taskName;
            OdbcConnection conn = db.GetStanderConn(taskName);
            OdbcSQLHelper.Conn = conn;
            TotalStation.Model.fmos_obj.timetask model = new TotalStation.Model.fmos_obj.timetask();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            lt = new List<Model.fmos_obj.timetask>();
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                lt.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;

            }
            return true;

        }

        



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.timetask DataRowToModel(DataRow row)
        {
            TotalStation.Model.fmos_obj.timetask model = new TotalStation.Model.fmos_obj.timetask();
            if (row != null)
            {
                if (row["CH"] != null && row["CH"].ToString() != "")
                {
                    model.CH = int.Parse(row["CH"].ToString());
                }
                if (row["DateTaskName"] != null)
                {
                    model.DateTaskName = row["DateTaskName"].ToString();
                }
                if (row["Indx"] != null && row["Indx"].ToString() != "")
                {
                    model.Indx = int.Parse(row["Indx"].ToString());
                }
                if (row["TimingHour"] != null && row["TimingHour"].ToString() != "")
                {
                    model.TimingHour = int.Parse(row["TimingHour"].ToString());
                }
                if (row["TimingMinute"] != null && row["TimingMinute"].ToString() != "")
                {
                    model.TimingMinute = int.Parse(row["TimingMinute"].ToString());
                }
                if (row["StartTimeType"] != null && row["StartTimeType"].ToString() != "")
                {
                    model.StartTimeType = int.Parse(row["StartTimeType"].ToString());
                }
                if (row["mInterval"] != null && row["mInterval"].ToString() != "")
                {
                    model.mInterval = int.Parse(row["mInterval"].ToString());
                }
                if (row["taskName"] != null)
                {
                    model.taskName = row["taskName"].ToString();
                }
            }
            return model;
        }




        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

