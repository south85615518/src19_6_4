﻿/**  版本信息模板在安装目录下，可自行修改。
* studypoint.cs
*
* 功 能： N/A
* 类 名： studypoint
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:12   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//using TotalStation.DBUtility;//Please add references
namespace TotalStation.DAL.fmos_obj
{
	/// <summary>
	/// 数据访问类:studypoint
	/// </summary>
	public partial class studypoint
	{
        public static database db = new database();
		public studypoint()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TotalStation.Model.fmos_obj.studypoint model)
		{
            OdbcConnection conn = db.GetStanderConn(model.taskName);
            OdbcSQLHelper.Conn = conn;
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into fmos_studypoint(");
			strSql.Append("orglHar,orglVar,orglSd,StationName,SEARCH_TIME,OnlyAngleUseful,Mileage,IsControlPoint,POINT_NAME,N,E,Z,TargetHight,ReflectorName,REMARK,taskName)");
			strSql.Append(" values (");
			strSql.Append("@orglHar,@orglVar,@orglSd,@StationName,@SEARCH_TIME,@OnlyAngleUseful,@Mileage,@IsControlPoint,@POINT_NAME,@N,@E,@Z,@TargetHight,@ReflectorName,@REMARK,@taskName)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("orglHar=@orglHar,");
            strSql.Append("orglVar=@orglVar,");
            strSql.Append("orglSd=@orglSd,");
            strSql.Append("StationName=@StationName,");
            strSql.Append("SEARCH_TIME=@SEARCH_TIME,");
            strSql.Append("OnlyAngleUseful=@OnlyAngleUseful,");
            strSql.Append("Mileage=@Mileage,");
            strSql.Append("IsControlPoint=@IsControlPoint,");
            strSql.Append("N=@N,");
            strSql.Append("E=@E,");
            strSql.Append("Z=@Z,");
            strSql.Append("TargetHight=@TargetHight,");
            strSql.Append("ReflectorName=@ReflectorName,");
            strSql.Append("REMARK=@REMARK");
			OdbcParameter[] parameters = {
					new OdbcParameter("@orglHar", OdbcType.VarChar,120),
					new OdbcParameter("@orglVar", OdbcType.VarChar,120),
					new OdbcParameter("@orglSd", OdbcType.Double),
					new OdbcParameter("@StationName", OdbcType.VarChar,120),
					new OdbcParameter("@SEARCH_TIME", OdbcType.DateTime),
					new OdbcParameter("@OnlyAngleUseful", OdbcType.Int,11),
					new OdbcParameter("@Mileage", OdbcType.VarChar,120),
					new OdbcParameter("@IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@TargetHight", OdbcType.Double),
					new OdbcParameter("@ReflectorName", OdbcType.VarChar,120),
					new OdbcParameter("@REMARK", OdbcType.VarChar,120),
					new OdbcParameter("@taskName", OdbcType.VarChar,120),
                    new OdbcParameter("_@orglHar", OdbcType.VarChar,120),
					new OdbcParameter("_@orglVar", OdbcType.VarChar,120),
					new OdbcParameter("_@orglSd", OdbcType.Double),
					new OdbcParameter("_@StationName", OdbcType.VarChar,120),
					new OdbcParameter("_@SEARCH_TIME", OdbcType.DateTime),
					new OdbcParameter("_@OnlyAngleUseful", OdbcType.Int,11),
					new OdbcParameter("_@Mileage", OdbcType.VarChar,120),
					new OdbcParameter("_@IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("_@N", OdbcType.Double),
					new OdbcParameter("_@E", OdbcType.Double),
					new OdbcParameter("_@Z", OdbcType.Double),
					new OdbcParameter("_@TargetHight", OdbcType.Double),
					new OdbcParameter("_@ReflectorName", OdbcType.VarChar,120),
					new OdbcParameter("_@REMARK", OdbcType.VarChar,120)
                    };
			parameters[0].Value = model.orglHar;
			parameters[1].Value = model.orglVar;
			parameters[2].Value = model.orglSd;
			parameters[3].Value = model.StationName;
			parameters[4].Value = model.SEARCH_TIME;
			parameters[5].Value = model.OnlyAngleUseful;
			parameters[6].Value = model.Mileage;
			parameters[7].Value = model.IsControlPoint;
			parameters[8].Value = model.POINT_NAME;
			parameters[9].Value = model.N;
			parameters[10].Value = model.E;
			parameters[11].Value = model.Z;
			parameters[12].Value = model.TargetHight;
			parameters[13].Value = model.ReflectorName;
			parameters[14].Value = model.REMARK;
			parameters[15].Value = model.taskName;

            parameters[16].Value = model.orglHar;
            parameters[17].Value = model.orglVar;
            parameters[18].Value = model.orglSd;
            parameters[19].Value = model.StationName;
            parameters[20].Value = model.SEARCH_TIME;
            parameters[21].Value = model.OnlyAngleUseful;
            parameters[22].Value = model.Mileage;
            parameters[23].Value = model.IsControlPoint;
            parameters[24].Value = model.N;
            parameters[25].Value = model.E;
            parameters[26].Value = model.Z;
            parameters[27].Value = model.TargetHight;
            parameters[28].Value = model.ReflectorName;
            parameters[29].Value = model.REMARK;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStation.Model.fmos_obj.studypoint model)
		{
            OdbcConnection conn = db.GetStanderConn(model.taskName);
            OdbcSQLHelper.Conn = conn;
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update fmos_studypoint set ");
			strSql.Append("orglHar=@orglHar,");
			strSql.Append("orglVar=@orglVar,");
			strSql.Append("orglSd=@orglSd,");
			strSql.Append("StationName=@StationName,");
			strSql.Append("SEARCH_TIME=@SEARCH_TIME,");
			strSql.Append("OnlyAngleUseful=@OnlyAngleUseful,");
			strSql.Append("Mileage=@Mileage,");
			strSql.Append("IsControlPoint=@IsControlPoint,");
			strSql.Append("POINT_NAME=@POINT_NAME,");
			strSql.Append("N=@N,");
			strSql.Append("E=@E,");
			strSql.Append("Z=@Z,");
			strSql.Append("TargetHight=@TargetHight,");
			strSql.Append("ReflectorName=@ReflectorName,");
			strSql.Append("REMARK=@REMARK,");
			strSql.Append("taskName=@taskName");
			strSql.Append(" where ID=@ID");
			OdbcParameter[] parameters = {
					new OdbcParameter("@orglHar", OdbcType.VarChar,120),
					new OdbcParameter("@orglVar", OdbcType.VarChar,120),
					new OdbcParameter("@orglSd", OdbcType.Double),
					new OdbcParameter("@StationName", OdbcType.VarChar,120),
					new OdbcParameter("@SEARCH_TIME", OdbcType.DateTime),
					new OdbcParameter("@OnlyAngleUseful", OdbcType.Int,11),
					new OdbcParameter("@Mileage", OdbcType.VarChar,120),
					new OdbcParameter("@IsControlPoint", OdbcType.Int,11),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@N", OdbcType.Double),
					new OdbcParameter("@E", OdbcType.Double),
					new OdbcParameter("@Z", OdbcType.Double),
					new OdbcParameter("@TargetHight", OdbcType.Double),
					new OdbcParameter("@ReflectorName", OdbcType.VarChar,120),
					new OdbcParameter("@REMARK", OdbcType.VarChar,120),
					new OdbcParameter("@taskName", OdbcType.VarChar,120),
					new OdbcParameter("@ID", OdbcType.Int,11)};
			parameters[0].Value = model.orglHar;
			parameters[1].Value = model.orglVar;
			parameters[2].Value = model.orglSd;
			parameters[3].Value = model.StationName;
			parameters[4].Value = model.SEARCH_TIME;
			parameters[5].Value = model.OnlyAngleUseful;
			parameters[6].Value = model.Mileage;
			parameters[7].Value = model.IsControlPoint;
			parameters[8].Value = model.POINT_NAME;
			parameters[9].Value = model.N;
			parameters[10].Value = model.E;
			parameters[11].Value = model.Z;
			parameters[12].Value = model.TargetHight;
			parameters[13].Value = model.ReflectorName;
			parameters[14].Value = model.REMARK;
			parameters[15].Value = model.taskName;
			parameters[16].Value = model.ID;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from fmos_studypoint ");
			strSql.Append(" where ID=@ID");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int)
			};
			parameters[0].Value = ID;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from fmos_studypoint ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
            int rows = 0; //OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.studypoint GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select orglHar,orglVar,orglSd,StationName,SEARCH_TIME,OnlyAngleUseful,Mileage,IsControlPoint,ID,POINT_NAME,N,E,Z,TargetHight,ReflectorName,REMARK,taskName from fmos_studypoint ");
			strSql.Append(" where ID=@ID");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int)
			};
			parameters[0].Value = ID;

			TotalStation.Model.fmos_obj.studypoint model=new TotalStation.Model.fmos_obj.studypoint();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "fmos_studypoint", parameters);
			if(ds!=null&&ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.studypoint DataRowToModel(DataRow row)
		{
			TotalStation.Model.fmos_obj.studypoint model=new TotalStation.Model.fmos_obj.studypoint();
			if (row != null)
			{
				if(row["orglHar"]!=null)
				{
					model.orglHar=row["orglHar"].ToString();
				}
				if(row["orglVar"]!=null)
				{
					model.orglVar=row["orglVar"].ToString();
				}
					//model.orglSd=row["orglSd"].ToString();
				if(row["StationName"]!=null)
				{
					model.StationName=row["StationName"].ToString();
				}
				if(row["SEARCH_TIME"]!=null && row["SEARCH_TIME"].ToString()!="")
				{
					model.SEARCH_TIME=DateTime.Parse(row["SEARCH_TIME"].ToString());
				}
				if(row["OnlyAngleUseful"]!=null && row["OnlyAngleUseful"].ToString()!="")
				{
					model.OnlyAngleUseful=Convert.ToBoolean(row["OnlyAngleUseful"].ToString());
				}
				if(row["Mileage"]!=null)
				{
					model.Mileage=row["Mileage"].ToString();
				}
				if(row["IsControlPoint"]!=null && row["IsControlPoint"].ToString()!="")
				{
                    model.IsControlPoint = Convert.ToBoolean(row["IsControlPoint"].ToString());
				}
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["POINT_NAME"]!=null)
				{
					model.POINT_NAME=row["POINT_NAME"].ToString();
				}
					//model.N=row["N"].ToString();
					//model.E=row["E"].ToString();
					//model.Z=row["Z"].ToString();
					//model.TargetHight=row["TargetHight"].ToString();
				if(row["ReflectorName"]!=null)
				{
					model.ReflectorName=row["ReflectorName"].ToString();
				}
				if(row["REMARK"]!=null)
				{
					model.REMARK=row["REMARK"].ToString();
				}
				if(row["taskName"]!=null)
				{
					model.taskName=row["taskName"].ToString();
				}
			}
			return model;
		}

        public bool StudyPointTableLoad( int startPageIndex, int pageSize, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select id,point_name ,n,e,z,targethight,reflectorName,orglHar,orglSd,orglVar,stationName,mileage,if(onlyAngleUseful=1,'是','否'),if(iscontrolpoint=1,'是','否'),remark from fmos_studypoint where taskName='{3}'  {2}  limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmname);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool StudyPointTableRowsCount(string xmname,out string totalCont)
        {
            string sql = "select count(*) from fmos_studypoint where taskName='" + xmname + "'";
            OdbcConnection conn = db.GetStanderConn(xmname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }

   
    

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

