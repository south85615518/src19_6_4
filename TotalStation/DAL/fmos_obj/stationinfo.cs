﻿/**  版本信息模板在安装目录下，可自行修改。
* stationinfo.cs
*
* 功 能： N/A
* 类 名： stationinfo
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
namespace TotalStation.DAL.fmos_obj
{
    /// <summary>
    /// 数据访问类:stationinfo
    /// </summary>
    public partial class stationinfo
    {
        public stationinfo()
        { }
        #region  BasicMethod
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.stationinfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into fmos_stationinfo(");
            strSql.Append("InstrumentID,ID,STATION_NAME,DirMethod,STATION_N,STATION_E,STATION_Z,STATION_HIGHT,BACKSIGNT_NAME,BACKSIGNT_ANGLE,SEARCH_TIME,Instrument_Name,REMARK,taskName)");
            strSql.Append(" values (");
            strSql.Append("@InstrumentID,@ID,@STATION_NAME,@DirMethod,@STATION_N,@STATION_E,@STATION_Z,@STATION_HIGHT,@BACKSIGNT_NAME,@BACKSIGNT_ANGLE,@SEARCH_TIME,@Instrument_Name,@REMARK,@taskName)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");

            strSql.Append("InstrumentID=@InstrumentID,");
            strSql.Append("ID=@ID,");
            strSql.Append("DirMethod=@DirMethod,");
            strSql.Append("STATION_N=@STATION_N,");
            strSql.Append("STATION_E=@STATION_E,");
            strSql.Append("STATION_Z=@STATION_Z,");
            strSql.Append("STATION_HIGHT=@STATION_HIGHT,");
            strSql.Append("BACKSIGNT_NAME=@BACKSIGNT_NAME,");
            strSql.Append("BACKSIGNT_ANGLE=@BACKSIGNT_ANGLE,");
            strSql.Append("SEARCH_TIME=@SEARCH_TIME,");
            strSql.Append("Instrument_Name=@Instrument_Name,");
            strSql.Append("REMARK=@REMARK");
            OdbcParameter[] parameters = {
					new OdbcParameter("@InstrumentID", OdbcType.Int,11),
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@STATION_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@DirMethod", OdbcType.Int,11),
					new OdbcParameter("@STATION_N", OdbcType.Double),
					new OdbcParameter("@STATION_E", OdbcType.Double),
					new OdbcParameter("@STATION_Z", OdbcType.Double),
					new OdbcParameter("@STATION_HIGHT", OdbcType.Double),
					new OdbcParameter("@BACKSIGNT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@BACKSIGNT_ANGLE", OdbcType.VarChar,120),
					new OdbcParameter("@SEARCH_TIME", OdbcType.DateTime),
					new OdbcParameter("@Instrument_Name", OdbcType.VarChar,120),
					new OdbcParameter("@REMARK", OdbcType.VarChar,120),
					new OdbcParameter("@taskName", OdbcType.VarChar,120),

                    new OdbcParameter("@InstrumentID", OdbcType.Int,11),
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@DirMethod", OdbcType.Int,11),
					new OdbcParameter("@STATION_N", OdbcType.Double),
					new OdbcParameter("@STATION_E", OdbcType.Double),
					new OdbcParameter("@STATION_Z", OdbcType.Double),
					new OdbcParameter("@STATION_HIGHT", OdbcType.Double),
					new OdbcParameter("@BACKSIGNT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@BACKSIGNT_ANGLE", OdbcType.VarChar,120),
					new OdbcParameter("@SEARCH_TIME", OdbcType.DateTime),
					new OdbcParameter("@Instrument_Name", OdbcType.VarChar,120),
					new OdbcParameter("@REMARK", OdbcType.VarChar,120)
                                         };

            parameters[0].Value = model.InstrumentID;
            parameters[1].Value = model.ID;
            parameters[2].Value = model.STATION_NAME;
            parameters[3].Value = model.DirMethod;
            parameters[4].Value = model.STATION_N;
            parameters[5].Value = model.STATION_E;
            parameters[6].Value = model.STATION_Z;
            parameters[7].Value = model.STATION_HIGHT;
            parameters[8].Value = model.BACKSIGNT_NAME;
            parameters[9].Value = model.BACKSIGNT_ANGLE;
            parameters[10].Value = model.SEARCH_TIME;
            parameters[11].Value = model.Instrument_Name;
            parameters[12].Value = model.REMARK;
            parameters[13].Value = model.Taskname;

            parameters[14].Value = model.InstrumentID;
            parameters[15].Value = model.ID;
            parameters[16].Value = model.DirMethod;
            parameters[17].Value = model.STATION_N;
            parameters[18].Value = model.STATION_E;
            parameters[19].Value = model.STATION_Z;
            parameters[20].Value = model.STATION_HIGHT;
            parameters[21].Value = model.BACKSIGNT_NAME;
            parameters[22].Value = model.BACKSIGNT_ANGLE;
            parameters[23].Value = model.SEARCH_TIME;
            parameters[24].Value = model.Instrument_Name;
            parameters[25].Value = model.REMARK;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.stationinfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update fmos_stationinfo set ");
            strSql.Append("InstrumentID=@InstrumentID,");
            strSql.Append("STATION_NAME=@STATION_NAME,");
            strSql.Append("DirMethod=@DirMethod,");
            strSql.Append("STATION_N=@STATION_N,");
            strSql.Append("STATION_E=@STATION_E,");
            strSql.Append("STATION_Z=@STATION_Z,");
            strSql.Append("STATION_HIGHT=@STATION_HIGHT,");
            strSql.Append("BACKSIGNT_NAME=@BACKSIGNT_NAME,");
            strSql.Append("BACKSIGNT_ANGLE=@BACKSIGNT_ANGLE,");
            strSql.Append("SEARCH_TIME=@SEARCH_TIME,");
            strSql.Append("Instrument_Name=@Instrument_Name,");
            strSql.Append("REMARK=@REMARK");
            strSql.Append(" where ID=@ID");
            OdbcParameter[] parameters = {
					new OdbcParameter("@InstrumentID", OdbcType.Int,11),
					new OdbcParameter("@STATION_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@DirMethod", OdbcType.Int,11),
					new OdbcParameter("@STATION_N", OdbcType.Double),
					new OdbcParameter("@STATION_E", OdbcType.Double),
					new OdbcParameter("@STATION_Z", OdbcType.Double),
					new OdbcParameter("@STATION_HIGHT", OdbcType.Double),
					new OdbcParameter("@BACKSIGNT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@BACKSIGNT_ANGLE", OdbcType.VarChar,120),
					new OdbcParameter("@SEARCH_TIME", OdbcType.DateTime),
					new OdbcParameter("@Instrument_Name", OdbcType.VarChar,120),
					new OdbcParameter("@REMARK", OdbcType.VarChar,120),
					new OdbcParameter("@ID", OdbcType.Int,11)};
            parameters[0].Value = model.InstrumentID;
            parameters[1].Value = model.STATION_NAME;
            parameters[2].Value = model.DirMethod;
            parameters[3].Value = model.STATION_N;
            parameters[4].Value = model.STATION_E;
            parameters[5].Value = model.STATION_Z;
            parameters[6].Value = model.STATION_HIGHT;
            parameters[7].Value = model.BACKSIGNT_NAME;
            parameters[8].Value = model.BACKSIGNT_ANGLE;
            parameters[9].Value = model.SEARCH_TIME;
            parameters[10].Value = model.Instrument_Name;
            parameters[11].Value = model.REMARK;
            parameters[12].Value = model.ID;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from fmos_stationinfo ");
            strSql.Append(" where ID=@ID");
            OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int)
			};
            parameters[0].Value = ID;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from fmos_stationinfo ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = 0;//OdbcSQLHelper.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.stationinfo GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select InstrumentID,ID,STATION_NAME,DirMethod,STATION_N,STATION_E,STATION_Z,STATION_HIGHT,BACKSIGNT_NAME,BACKSIGNT_ANGLE,SEARCH_TIME,Instrument_Name,REMARK from fmos_stationinfo ");
            strSql.Append(" where ID=@ID");
            OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int)
			};
            parameters[0].Value = ID;

            TotalStation.Model.fmos_obj.stationinfo model = new TotalStation.Model.fmos_obj.stationinfo();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "fmos_stationinfo", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.stationinfo DataRowToModel(DataRow row)
        {
            TotalStation.Model.fmos_obj.stationinfo model = new TotalStation.Model.fmos_obj.stationinfo();
            if (row != null)
            {
                if (row["InstrumentID"] != null && row["InstrumentID"].ToString() != "")
                {
                    model.InstrumentID = int.Parse(row["InstrumentID"].ToString());
                }
                if (row["ID"] != null && row["ID"].ToString() != "")
                {
                    model.ID = int.Parse(row["ID"].ToString());
                }
                if (row["STATION_NAME"] != null)
                {
                    model.STATION_NAME = row["STATION_NAME"].ToString();
                }
                if (row["DirMethod"] != null && row["DirMethod"].ToString() != "")
                {
                    model.DirMethod = int.Parse(row["DirMethod"].ToString());
                }
                //model.STATION_N=row["STATION_N"].ToString();
                //model.STATION_E=row["STATION_E"].ToString();
                //model.STATION_Z=row["STATION_Z"].ToString();
                //model.STATION_HIGHT=row["STATION_HIGHT"].ToString();
                if (row["BACKSIGNT_NAME"] != null)
                {
                    model.BACKSIGNT_NAME = row["BACKSIGNT_NAME"].ToString();
                }
                if (row["BACKSIGNT_ANGLE"] != null)
                {
                    model.BACKSIGNT_ANGLE = row["BACKSIGNT_ANGLE"].ToString();
                }
                if (row["SEARCH_TIME"] != null && row["SEARCH_TIME"].ToString() != "")
                {
                    model.SEARCH_TIME = DateTime.Parse(row["SEARCH_TIME"].ToString());
                }
                if (row["Instrument_Name"] != null)
                {
                    model.Instrument_Name = row["Instrument_Name"].ToString();
                }
                if (row["REMARK"] != null)
                {
                    model.REMARK = row["REMARK"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select InstrumentID,ID,STATION_NAME,DirMethod,STATION_N,STATION_E,STATION_Z,STATION_HIGHT,BACKSIGNT_NAME,BACKSIGNT_ANGLE,SEARCH_TIME,Instrument_Name,REMARK ");
            strSql.Append(" FROM fmos_stationinfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return OdbcSQLHelper.Query(strSql.ToString(),"fmos_stationinfo");
        }

        public bool StationInfoTableLoad(int startPageIndex, int pageSize,  string xmname, string searchpost, string colName, string sord, out DataTable dt)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " taskName=  '" + xmname+"' ");
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select * from fmos_stationInfo where {3}  {2}  limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, searchstr);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public database db = new database();
        public bool StationInfoTableRowsCount(string searchpost,  string xmname, out string totalCont)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " taskName= '" + xmname+"'");
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select count(*) from fmos_stationInfo where {0}   ", searchstr);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }



        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM fmos_stationinfo ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.ID desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from fmos_stationinfo T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return OdbcSQLHelper.Query(strSql.ToString());
        //}

        ///*
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@tblName", OdbcType.VarChar, 255),
        //            new OdbcParameter("@fldName", OdbcType.VarChar, 255),
        //            new OdbcParameter("@PageSize", OdbcType.Int),
        //            new OdbcParameter("@PageIndex", OdbcType.Int),
        //            new OdbcParameter("@IsReCount", OdbcType.Bit),
        //            new OdbcParameter("@OrderType", OdbcType.Bit),
        //            new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
        //            };
        //    parameters[0].Value = "fmos_stationinfo";
        //    parameters[1].Value = "ID";
        //    parameters[2].Value = PageSize;
        //    parameters[3].Value = PageIndex;
        //    parameters[4].Value = 0;
        //    parameters[5].Value = 0;
        //    parameters[6].Value = strWhere;	
        //    return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        //}*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

