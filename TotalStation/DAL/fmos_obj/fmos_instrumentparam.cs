﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_instrumentparam.cs
*
* 功 能： N/A
* 类 名： fmos_instrumentparam
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/21 14:45:51   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//using Odbc.Data.OdbcClient;
//using TotalStation.DBUtility;//Please add references
namespace TotalStation.DAL.fmos_obj
{
	/// <summary>
	/// 数据访问类:fmos_instrumentparam
	/// </summary>
	public partial class fmos_instrumentparam
	{
        public database db = new database();
		public fmos_instrumentparam()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TotalStation.Model.fmos_obj.fmos_instrumentparam model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.Xmno);
            OdbcSQLHelper.Conn = conn;
			strSql.Append("insert into fmos_instrumentparam(");
			strSql.Append("ID,Name,InstrumentType,AutoConnect,Remark,PowerModName,PONTimesEach,PONInterval,PONTimes,PowerControl,ReceiveTimeOut,HarSRange,VarSRange,COM,Connect_METHOD,BaudRate,DataBits,StopBits,Parity,Localport,LocalIP,xmno)");
			strSql.Append(" values (");
			strSql.Append("@ID,@Name,@InstrumentType,@AutoConnect,@Remark,@PowerModName,@PONTimesEach,@PONInterval,@PONTimes,@PowerControl,@ReceiveTimeOut,@HarSRange,@VarSRange,@COM,@Connect_METHOD,@BaudRate,@DataBits,@StopBits,@Parity,@Localport,@LocalIP,@xmno)");
            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("ID=@ID,");
            strSql.Append("Name=@Name,");
            strSql.Append("InstrumentType=@InstrumentType,");
            strSql.Append("AutoConnect=@AutoConnect,");
            strSql.Append("Remark=@Remark,");
            strSql.Append("PowerModName=@PowerModName,");
            strSql.Append("PONTimesEach=@PONTimesEach,");
            strSql.Append("PONInterval=@PONInterval,");
            strSql.Append("PONTimes=@PONTimes,");
            strSql.Append("PowerControl=@PowerControl,");
            strSql.Append("ReceiveTimeOut=@ReceiveTimeOut,");
            strSql.Append("HarSRange=@HarSRange,");
            strSql.Append("VarSRange=@VarSRange,");
            strSql.Append("COM=@COM,");
            strSql.Append("Connect_METHOD=@Connect_METHOD,");
            strSql.Append("BaudRate=@BaudRate,");
            strSql.Append("DataBits=@DataBits,");
            strSql.Append("StopBits=@StopBits,");
            strSql.Append("Parity=@Parity,");
            strSql.Append("Localport=@Localport,");
            strSql.Append("LocalIP=@LocalIP");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@Name", OdbcType.VarChar,120),
					new OdbcParameter("@InstrumentType", OdbcType.Int,11),
					new OdbcParameter("@AutoConnect", OdbcType.Int,11),
					new OdbcParameter("@Remark", OdbcType.VarChar,120),
					new OdbcParameter("@PowerModName", OdbcType.VarChar,120),
					new OdbcParameter("@PONTimesEach", OdbcType.Int,11),
					new OdbcParameter("@PONInterval", OdbcType.Int,11),
					new OdbcParameter("@PONTimes", OdbcType.Int,11),
					new OdbcParameter("@PowerControl", OdbcType.Int,11),
					new OdbcParameter("@ReceiveTimeOut", OdbcType.Int,11),
					new OdbcParameter("@HarSRange", OdbcType.Double),
					new OdbcParameter("@VarSRange", OdbcType.Double),
					new OdbcParameter("@COM", OdbcType.VarChar,120),
					new OdbcParameter("@Connect_METHOD", OdbcType.Int,11),
					new OdbcParameter("@BaudRate", OdbcType.Int,11),
					new OdbcParameter("@DataBits", OdbcType.Int,11),
					new OdbcParameter("@StopBits", OdbcType.Int,11),
					new OdbcParameter("@Parity", OdbcType.Int,11),
					new OdbcParameter("@Localport", OdbcType.Int,11),
					new OdbcParameter("@LocalIP", OdbcType.VarChar,120),
                    new OdbcParameter("@xmno", OdbcType.Int),

                    new OdbcParameter("@_ID", OdbcType.Int,11),
					new OdbcParameter("@_Name", OdbcType.VarChar,120),
					new OdbcParameter("@_InstrumentType", OdbcType.Int,11),
					new OdbcParameter("@_AutoConnect", OdbcType.Int,11),
					new OdbcParameter("@_Remark", OdbcType.VarChar,120),
					new OdbcParameter("@_PowerModName", OdbcType.VarChar,120),
					new OdbcParameter("@_PONTimesEach", OdbcType.Int,11),
					new OdbcParameter("@_PONInterval", OdbcType.Int,11),
					new OdbcParameter("@_PONTimes", OdbcType.Int,11),
					new OdbcParameter("@_PowerControl", OdbcType.Int,11),
					new OdbcParameter("@_ReceiveTimeOut", OdbcType.Int,11),
					new OdbcParameter("@_HarSRange", OdbcType.Double),
					new OdbcParameter("@_VarSRange", OdbcType.Double),
					new OdbcParameter("@_COM", OdbcType.VarChar,120),
					new OdbcParameter("@_Connect_METHOD", OdbcType.Int,11),
					new OdbcParameter("@_BaudRate", OdbcType.Int,11),
					new OdbcParameter("@_DataBits", OdbcType.Int,11),
					new OdbcParameter("@_StopBits", OdbcType.Int,11),
					new OdbcParameter("@_Parity", OdbcType.Int,11),
					new OdbcParameter("@_Localport", OdbcType.Int,11),
					new OdbcParameter("@_LocalIP", OdbcType.VarChar,120)
                                         };
			parameters[0].Value = model.ID;
			parameters[1].Value = model.Name;
			parameters[2].Value = model.InstrumentType;
			parameters[3].Value = model.Autoconnect;
			parameters[4].Value = model.Remark;
			parameters[5].Value = model.PowerModName;
			parameters[6].Value = model.PONTimesEach;
			parameters[7].Value = model.PONInterval;
			parameters[8].Value = model.PONTimes;
			parameters[9].Value = model.Powercontrol;
			parameters[10].Value = model.ReceiveTimeOut;
			parameters[11].Value = model.HarSRange;
			parameters[12].Value = model.VarSRange;
			parameters[13].Value = model.COM;
			parameters[14].Value = model.Connect_METHOD;
			parameters[15].Value = model.BaudRate;
			parameters[16].Value = model.DataBits;
			parameters[17].Value = model.StopBits;
			parameters[18].Value = model.Parity;
			parameters[19].Value = model.Localport;
			parameters[20].Value = model.LocalIP;
            parameters[21].Value = model.Xmno;

            parameters[22].Value = model.ID;
            parameters[23].Value = model.Name;
            parameters[24].Value = model.InstrumentType;
            parameters[25].Value = model.Autoconnect;
            parameters[26].Value = model.Remark;
            parameters[27].Value = model.PowerModName;
            parameters[28].Value = model.PONTimesEach;
            parameters[29].Value = model.PONInterval;
            parameters[30].Value = model.PONTimes;
            parameters[31].Value = model.Powercontrol;
            parameters[32].Value = model.ReceiveTimeOut;
            parameters[33].Value = model.HarSRange;
            parameters[34].Value = model.VarSRange;
            parameters[35].Value = model.COM;
            parameters[36].Value = model.Connect_METHOD;
            parameters[37].Value = model.BaudRate;
            parameters[38].Value = model.DataBits;
            parameters[39].Value = model.StopBits;
            parameters[40].Value = model.Parity;
            parameters[41].Value = model.Localport;
            parameters[42].Value = model.LocalIP;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStation.Model.fmos_obj.fmos_instrumentparam model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.Xmno);
            OdbcSQLHelper.Conn = conn;
			strSql.Append("update fmos_instrumentparam set ");
			strSql.Append("ID=@ID,");
			strSql.Append("Name=@Name,");
			strSql.Append("InstrumentType=@InstrumentType,");
			strSql.Append("AutoConnect=@AutoConnect,");
			strSql.Append("Remark=@Remark,");
			strSql.Append("PowerModName=@PowerModName,");
			strSql.Append("PONTimesEach=@PONTimesEach,");
			strSql.Append("PONInterval=@PONInterval,");
			strSql.Append("PONTimes=@PONTimes,");
			strSql.Append("PowerControl=@PowerControl,");
			strSql.Append("ReceiveTimeOut=@ReceiveTimeOut,");
			strSql.Append("HarSRange=@HarSRange,");
			strSql.Append("VarSRange=@VarSRange,");
			strSql.Append("COM=@COM,");
			strSql.Append("Connect_METHOD=@Connect_METHOD,");
			strSql.Append("BaudRate=@BaudRate,");
			strSql.Append("DataBits=@DataBits,");
			strSql.Append("StopBits=@StopBits,");
			strSql.Append("Parity=@Parity,");
			strSql.Append("Localport=@Localport,");
			strSql.Append("LocalIP=@LocalIP");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@Name", OdbcType.VarChar,120),
					new OdbcParameter("@InstrumentType", OdbcType.Int,11),
					new OdbcParameter("@AutoConnect", OdbcType.Int,11),
					new OdbcParameter("@Remark", OdbcType.VarChar,120),
					new OdbcParameter("@PowerModName", OdbcType.VarChar,120),
					new OdbcParameter("@PONTimesEach", OdbcType.Int,11),
					new OdbcParameter("@PONInterval", OdbcType.Int,11),
					new OdbcParameter("@PONTimes", OdbcType.Int,11),
					new OdbcParameter("@PowerControl", OdbcType.Int,11),
					new OdbcParameter("@ReceiveTimeOut", OdbcType.Int,11),
					new OdbcParameter("@HarSRange", OdbcType.Double),
					new OdbcParameter("@VarSRange", OdbcType.Double),
					new OdbcParameter("@COM", OdbcType.VarChar,120),
					new OdbcParameter("@Connect_METHOD", OdbcType.Int,11),
					new OdbcParameter("@BaudRate", OdbcType.Int,11),
					new OdbcParameter("@DataBits", OdbcType.Int,11),
					new OdbcParameter("@StopBits", OdbcType.Int,11),
					new OdbcParameter("@Parity", OdbcType.Int,11),
					new OdbcParameter("@Localport", OdbcType.Int,11),
					new OdbcParameter("@LocalIP", OdbcType.VarChar,120)};
			parameters[0].Value = model.ID;
			parameters[1].Value = model.Name;
			parameters[2].Value = model.InstrumentType;
			parameters[3].Value = model.Autoconnect;
			parameters[4].Value = model.Remark;
			parameters[5].Value = model.PowerModName;
			parameters[6].Value = model.PONTimesEach;
			parameters[7].Value = model.PONInterval;
			parameters[8].Value = model.PONTimes;
			parameters[9].Value = model.Powercontrol;
			parameters[10].Value = model.ReceiveTimeOut;
			parameters[11].Value = model.HarSRange;
			parameters[12].Value = model.VarSRange;
			parameters[13].Value = model.COM;
			parameters[14].Value = model.Connect_METHOD;
			parameters[15].Value = model.BaudRate;
			parameters[16].Value = model.DataBits;
			parameters[17].Value = model.StopBits;
			parameters[18].Value = model.Parity;
			parameters[19].Value = model.Localport;
			parameters[20].Value = model.LocalIP;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from fmos_instrumentparam ");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
			};

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        public bool GetModel(int xmno, string name, out TotalStation.Model.fmos_obj.fmos_instrumentparam model)
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			strSql.Append("select ID,Name,InstrumentType,AutoConnect,Remark,PowerModName,PONTimesEach,PONInterval,PONTimes,PowerControl,ReceiveTimeOut,HarSRange,VarSRange,COM,Connect_METHOD,BaudRate,DataBits,StopBits,Parity,Localport,LocalIP from fmos_instrumentparam ");
            strSql.Append("   where     name = @name     and    xmno = @xmno   ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,10)
			};
            parameters[0].Value = name;
            parameters[1].Value = xmno;
            model = new TotalStation.Model.fmos_obj.fmos_instrumentparam();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.fmos_instrumentparam DataRowToModel(DataRow row)
		{
			TotalStation.Model.fmos_obj.fmos_instrumentparam model=new TotalStation.Model.fmos_obj.fmos_instrumentparam();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["Name"]!=null)
				{
					model.Name=row["Name"].ToString();
				}
				if(row["InstrumentType"]!=null && row["InstrumentType"].ToString()!="")
				{
					model.InstrumentType=int.Parse(row["InstrumentType"].ToString());
				}
				if(row["AutoConnect"]!=null && row["AutoConnect"].ToString()!="")
				{
					model.Autoconnect=row["AutoConnect"].ToString() == "1" ? true : false;
				}
				if(row["Remark"]!=null)
				{
					model.Remark=row["Remark"].ToString();
				}
				if(row["PowerModName"]!=null)
				{
					model.PowerModName=row["PowerModName"].ToString();
				}
				if(row["PONTimesEach"]!=null && row["PONTimesEach"].ToString()!="")
				{
					model.PONTimesEach=int.Parse(row["PONTimesEach"].ToString());
				}
				if(row["PONInterval"]!=null && row["PONInterval"].ToString()!="")
				{
					model.PONInterval=int.Parse(row["PONInterval"].ToString());
				}
				if(row["PONTimes"]!=null && row["PONTimes"].ToString()!="")
				{
					model.PONTimes=int.Parse(row["PONTimes"].ToString());
				}
				if(row["PowerControl"]!=null && row["PowerControl"].ToString()!="")
				{
                    model.Powercontrol = row["PowerControl"].ToString() == "1" ? true:false;
				}
				if(row["ReceiveTimeOut"]!=null && row["ReceiveTimeOut"].ToString()!="")
				{
					model.ReceiveTimeOut=int.Parse(row["ReceiveTimeOut"].ToString());
				}
					//model.HarSRange=row["HarSRange"].ToString();
					//model.VarSRange=row["VarSRange"].ToString();
				if(row["COM"]!=null)
				{
					model.COM=row["COM"].ToString();
				}
				if(row["Connect_METHOD"]!=null && row["Connect_METHOD"].ToString()!="")
				{
					model.Connect_METHOD=int.Parse(row["Connect_METHOD"].ToString());
				}
				if(row["BaudRate"]!=null && row["BaudRate"].ToString()!="")
				{
					model.BaudRate=int.Parse(row["BaudRate"].ToString());
				}
				if(row["DataBits"]!=null && row["DataBits"].ToString()!="")
				{
					model.DataBits=int.Parse(row["DataBits"].ToString());
				}
				if(row["StopBits"]!=null && row["StopBits"].ToString()!="")
				{
					model.StopBits=int.Parse(row["StopBits"].ToString());
				}
				if(row["Parity"]!=null && row["Parity"].ToString()!="")
				{
					model.Parity=int.Parse(row["Parity"].ToString());
				}
				if(row["Localport"]!=null && row["Localport"].ToString()!="")
				{
					model.Localport=int.Parse(row["Localport"].ToString());
				}
				if(row["LocalIP"]!=null)
				{
					model.LocalIP=row["LocalIP"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,Name,InstrumentType,AutoConnect,Remark,PowerModName,PONTimesEach,PONInterval,PONTimes,PowerControl,ReceiveTimeOut,HarSRange,VarSRange,COM,Connect_METHOD,BaudRate,DataBits,StopBits,Parity,Localport,LocalIP ");
			strSql.Append(" FROM fmos_instrumentparam ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return OdbcSQLHelper.Query(strSql.ToString());
		}

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM fmos_instrumentparam ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = OdbcSQLHelper.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T. desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from fmos_instrumentparam T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return OdbcSQLHelper.Query(strSql.ToString());
        //}
        public bool InstrumentParamTableLoad(int startPageIndex, int pageSize, int xmno, string xmname, string searchpost, string colName, string sord, out DataTable dt)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " xmno= " + xmno);
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select * from fmos_instrumentparam where {3} {2}  limit  {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, searchstr);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool InstrumentParamTableRowsCount(string searchpost, int xmno,string xmname, out string totalCont)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " xmno= " + xmno);
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select count(*) from fmos_instrumentparam where {3}   ",  searchstr);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

