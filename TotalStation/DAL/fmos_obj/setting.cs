﻿/**  版本信息模板在安装目录下，可自行修改。
* setting.cs
*
* 功 能： N/A
* 类 名： setting
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
namespace TotalStation.DAL.fmos_obj
{
    /// <summary>
    /// 数据访问类:setting
    /// </summary>
    public partial class setting
    {
        public setting()
        { }
        #region  BasicMethod



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.setting model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.TaskName);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into fmos_setting(");
            strSql.Append("Name,CheckAngle,ReSearchInterval,ReSearchTime,dN,dE,dZ,_2C,HarReadingDiff,VarReadingDiff,SdReadingDiff,IndexError,HalfChRegZeroDiff,BetweenChRegZeroDiff,BetweenCh2CDiff,BetweenChIndexErrorDiff,BetweenChSdDiff,dSD,ChaoXianCheck,ChaoXianTime,GetSdTimes,ChaoXianPause,CloseInstrument,AutoPause,AlwaysCW,AutoUpdateStudyPoint,AutoReport,AutoReportPath,TurningAngleRegis,PowerOffAllowInterval,RemoveDBNullRow,taskName)");
            strSql.Append(" values (");
            strSql.Append("@Name,@CheckAngle,@ReSearchInterval,@ReSearchTime,@dN,@dE,@dZ,@_2C,@HarReadingDiff,@VarReadingDiff,@SdReadingDiff,@IndexError,@HalfChRegZeroDiff,@BetweenChRegZeroDiff,@BetweenCh2CDiff,@BetweenChIndexErrorDiff,@BetweenChSdDiff,@dSD,@ChaoXianCheck,@ChaoXianTime,@GetSdTimes,@ChaoXianPause,@CloseInstrument,@AutoPause,@AlwaysCW,@AutoUpdateStudyPoint,@AutoReport,@AutoReportPath,@TurningAngleRegis,@PowerOffAllowInterval,@RemoveDBNullRow,@taskName)");

            strSql.Append(" ON DUPLICATE KEY UPDATE ");
            strSql.Append("CheckAngle=@CheckAngle,");
            strSql.Append("ReSearchInterval=@ReSearchInterval,");
            strSql.Append("ReSearchTime=@ReSearchTime,");
            strSql.Append("dN=@dN,");
            strSql.Append("dE=@dE,");
            strSql.Append("dZ=@dZ,");
            strSql.Append("_2C=@_2C,");
            strSql.Append("HarReadingDiff=@HarReadingDiff,");
            strSql.Append("VarReadingDiff=@VarReadingDiff,");
            strSql.Append("SdReadingDiff=@SdReadingDiff,");
            strSql.Append("IndexError=@IndexError,");
            strSql.Append("HalfChRegZeroDiff=@HalfChRegZeroDiff,");
            strSql.Append("BetweenChRegZeroDiff=@BetweenChRegZeroDiff,");
            strSql.Append("BetweenCh2CDiff=@BetweenCh2CDiff,");
            strSql.Append("BetweenChIndexErrorDiff=@BetweenChIndexErrorDiff,");
            strSql.Append("BetweenChSdDiff=@BetweenChSdDiff,");
            strSql.Append("dSD=@dSD,");
            strSql.Append("ChaoXianCheck=@ChaoXianCheck,");
            strSql.Append("ChaoXianTime=@ChaoXianTime,");
            strSql.Append("GetSdTimes=@GetSdTimes,");
            strSql.Append("ChaoXianPause=@ChaoXianPause,");
            strSql.Append("CloseInstrument=@CloseInstrument,");
            strSql.Append("AutoPause=@AutoPause,");
            strSql.Append("AlwaysCW=@AlwaysCW,");
            strSql.Append("AutoUpdateStudyPoint=@AutoUpdateStudyPoint,");
            strSql.Append("AutoReport=@AutoReport,");
            strSql.Append("AutoReportPath=@AutoReportPath,");
            strSql.Append("TurningAngleRegis=@TurningAngleRegis,");
            strSql.Append("PowerOffAllowInterval=@PowerOffAllowInterval,");
            strSql.Append("RemoveDBNullRow=@RemoveDBNullRow,");
            strSql.Append("name=@name");
            OdbcParameter[] parameters = {
					new OdbcParameter("@Name", OdbcType.VarChar,120),
					new OdbcParameter("@CheckAngle", OdbcType.TinyInt,2),
					new OdbcParameter("@ReSearchInterval", OdbcType.Int,11),
					new OdbcParameter("@ReSearchTime", OdbcType.Int,11),
					new OdbcParameter("@dN", OdbcType.Int,11),
					new OdbcParameter("@dE", OdbcType.Int,11),
					new OdbcParameter("@dZ", OdbcType.Int,11),
					new OdbcParameter("@_2C", OdbcType.Int,11),
					new OdbcParameter("@HarReadingDiff", OdbcType.Int,11),
					new OdbcParameter("@VarReadingDiff", OdbcType.Int,11),
					new OdbcParameter("@SdReadingDiff", OdbcType.Int,11),
					new OdbcParameter("@IndexError", OdbcType.Int,11),
					new OdbcParameter("@HalfChRegZeroDiff", OdbcType.Int,11),
					new OdbcParameter("@BetweenChRegZeroDiff", OdbcType.Int,11),
					new OdbcParameter("@BetweenCh2CDiff", OdbcType.Int,11),
					new OdbcParameter("@BetweenChIndexErrorDiff", OdbcType.Int,11),
					new OdbcParameter("@BetweenChSdDiff", OdbcType.Int,11),
					new OdbcParameter("@dSD", OdbcType.Int,11),
					new OdbcParameter("@ChaoXianCheck", OdbcType.TinyInt,2),
					new OdbcParameter("@ChaoXianTime", OdbcType.Int,11),
					new OdbcParameter("@GetSdTimes", OdbcType.Int,11),
					new OdbcParameter("@ChaoXianPause", OdbcType.TinyInt,2),
					new OdbcParameter("@CloseInstrument", OdbcType.TinyInt,2),
					new OdbcParameter("@AutoPause", OdbcType.TinyInt,2),
					new OdbcParameter("@AlwaysCW", OdbcType.TinyInt,2),
					new OdbcParameter("@AutoUpdateStudyPoint", OdbcType.TinyInt,2),
					new OdbcParameter("@AutoReport", OdbcType.TinyInt,2),
					new OdbcParameter("@AutoReportPath", OdbcType.VarChar,120),
					new OdbcParameter("@TurningAngleRegis", OdbcType.Int,11),
					new OdbcParameter("@PowerOffAllowInterval", OdbcType.Int,11),
					new OdbcParameter("@RemoveDBNullRow", OdbcType.TinyInt,2),
                    new OdbcParameter("@taskName", OdbcType.VarChar,120),

                    new OdbcParameter("_@CheckAngle", OdbcType.TinyInt,2),
					new OdbcParameter("_@ReSearchInterval", OdbcType.Int,11),
					new OdbcParameter("_@ReSearchTime", OdbcType.Int,11),
					new OdbcParameter("_@dN", OdbcType.Int,11),
					new OdbcParameter("_@dE", OdbcType.Int,11),
					new OdbcParameter("_@dZ", OdbcType.Int,11),
					new OdbcParameter("_@_2C", OdbcType.Int,11),
					new OdbcParameter("_@HarReadingDiff", OdbcType.Int,11),
					new OdbcParameter("_@VarReadingDiff", OdbcType.Int,11),
					new OdbcParameter("_@SdReadingDiff", OdbcType.Int,11),
					new OdbcParameter("_@IndexError", OdbcType.Int,11),
					new OdbcParameter("_@HalfChRegZeroDiff", OdbcType.Int,11),
					new OdbcParameter("_@BetweenChRegZeroDiff", OdbcType.Int,11),
					new OdbcParameter("_@BetweenCh2CDiff", OdbcType.Int,11),
					new OdbcParameter("_@BetweenChIndexErrorDiff", OdbcType.Int,11),
					new OdbcParameter("_@BetweenChSdDiff", OdbcType.Int,11),
					new OdbcParameter("_@dSD", OdbcType.Int,11),
					new OdbcParameter("_@ChaoXianCheck", OdbcType.TinyInt,2),
					new OdbcParameter("_@ChaoXianTime", OdbcType.Int,11),
					new OdbcParameter("_@GetSdTimes", OdbcType.Int,11),
					new OdbcParameter("_@ChaoXianPause", OdbcType.TinyInt,2),
					new OdbcParameter("_@CloseInstrument", OdbcType.TinyInt,2),
					new OdbcParameter("_@AutoPause", OdbcType.TinyInt,2),
					new OdbcParameter("_@AlwaysCW", OdbcType.TinyInt,2),
					new OdbcParameter("_@AutoUpdateStudyPoint", OdbcType.TinyInt,2),
					new OdbcParameter("_@AutoReport", OdbcType.TinyInt,2),
					new OdbcParameter("_@AutoReportPath", OdbcType.VarChar,120),
					new OdbcParameter("_@TurningAngleRegis", OdbcType.Int,11),
					new OdbcParameter("_@PowerOffAllowInterval", OdbcType.Int,11),
					new OdbcParameter("_@RemoveDBNullRow", OdbcType.TinyInt,2),
                    new OdbcParameter("_@Name", OdbcType.VarChar,120)
                                         };
            parameters[0].Value = model.Name;
            parameters[1].Value = model.CheckAngle;
            parameters[2].Value = model.ReSearchInterval;
            parameters[3].Value = model.ReSearchTime;
            parameters[4].Value = model.dN;
            parameters[5].Value = model.dE;
            parameters[6].Value = model.dZ;
            parameters[7].Value = model._2C;
            parameters[8].Value = model.HarReadingDiff;
            parameters[9].Value = model.VarReadingDiff;
            parameters[10].Value = model.SdReadingDiff;
            parameters[11].Value = model.IndexError;
            parameters[12].Value = model.HalfChRegZeroDiff;
            parameters[13].Value = model.BetweenChRegZeroDiff;
            parameters[14].Value = model.BetweenCh2CDiff;
            parameters[15].Value = model.BetweenChIndexErrorDiff;
            parameters[16].Value = model.BetweenChSdDiff;
            parameters[17].Value = model.dSD;
            parameters[18].Value = model.ChaoXianCheck;
            parameters[19].Value = model.ChaoXianTime;
            parameters[20].Value = model.GetSdTimes;
            parameters[21].Value = model.ChaoXianPause;
            parameters[22].Value = model.CloseInstrument;
            parameters[23].Value = model.AutoPause;
            parameters[24].Value = model.AlwaysCW;
            parameters[25].Value = model.AutoUpdateStudyPoint;
            parameters[26].Value = model.AutoReport;
            parameters[27].Value = model.AutoReportPath;
            parameters[28].Value = model.TurningAngleRegis;
            parameters[29].Value = model.PowerOffAllowInterval;
            parameters[30].Value = model.RemoveDBNullRow;
            parameters[31].Value = model.TaskName;

            parameters[32].Value = model.CheckAngle;
            parameters[33].Value = model.ReSearchInterval;
            parameters[34].Value = model.ReSearchTime;
            parameters[35].Value = model.dN;
            parameters[36].Value = model.dE;
            parameters[37].Value = model.dZ;
            parameters[38].Value = model._2C;
            parameters[39].Value = model.HarReadingDiff;
            parameters[40].Value = model.VarReadingDiff;
            parameters[41].Value = model.SdReadingDiff;
            parameters[42].Value = model.IndexError;
            parameters[43].Value = model.HalfChRegZeroDiff;
            parameters[44].Value = model.BetweenChRegZeroDiff;
            parameters[45].Value = model.BetweenCh2CDiff;
            parameters[46].Value = model.BetweenChIndexErrorDiff;
            parameters[47].Value = model.BetweenChSdDiff;
            parameters[48].Value = model.dSD;
            parameters[49].Value = model.ChaoXianCheck;
            parameters[50].Value = model.ChaoXianTime;
            parameters[51].Value = model.GetSdTimes;
            parameters[52].Value = model.ChaoXianPause;
            parameters[53].Value = model.CloseInstrument;
            parameters[54].Value = model.AutoPause;
            parameters[55].Value = model.AlwaysCW;
            parameters[56].Value = model.AutoUpdateStudyPoint;
            parameters[57].Value = model.AutoReport;
            parameters[58].Value = model.AutoReportPath;
            parameters[59].Value = model.TurningAngleRegis;
            parameters[60].Value = model.PowerOffAllowInterval;
            parameters[61].Value = model.RemoveDBNullRow;
            parameters[62].Value = model.Name;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.setting model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update fmos_setting set ");
            strSql.Append("Name=@Name,");
            strSql.Append("CheckAngle=@CheckAngle,");
            strSql.Append("ReSearchInterval=@ReSearchInterval,");
            strSql.Append("ReSearchTime=@ReSearchTime,");
            strSql.Append("dN=@dN,");
            strSql.Append("dE=@dE,");
            strSql.Append("dZ=@dZ,");
            strSql.Append("_2C=@_2C,");
            strSql.Append("HarReadingDiff=@HarReadingDiff,");
            strSql.Append("VarReadingDiff=@VarReadingDiff,");
            strSql.Append("SdReadingDiff=@SdReadingDiff,");
            strSql.Append("IndexError=@IndexError,");
            strSql.Append("HalfChRegZeroDiff=@HalfChRegZeroDiff,");
            strSql.Append("BetweenChRegZeroDiff=@BetweenChRegZeroDiff,");
            strSql.Append("BetweenCh2CDiff=@BetweenCh2CDiff,");
            strSql.Append("BetweenChIndexErrorDiff=@BetweenChIndexErrorDiff,");
            strSql.Append("BetweenChSdDiff=@BetweenChSdDiff,");
            strSql.Append("dSD=@dSD,");
            strSql.Append("ChaoXianCheck=@ChaoXianCheck,");
            strSql.Append("ChaoXianTime=@ChaoXianTime,");
            strSql.Append("GetSdTimes=@GetSdTimes,");
            strSql.Append("ChaoXianPause=@ChaoXianPause,");
            strSql.Append("CloseInstrument=@CloseInstrument,");
            strSql.Append("AutoPause=@AutoPause,");
            strSql.Append("AlwaysCW=@AlwaysCW,");
            strSql.Append("AutoUpdateStudyPoint=@AutoUpdateStudyPoint,");
            strSql.Append("AutoReport=@AutoReport,");
            strSql.Append("AutoReportPath=@AutoReportPath,");
            strSql.Append("TurningAngleRegis=@TurningAngleRegis,");
            strSql.Append("PowerOffAllowInterval=@PowerOffAllowInterval,");
            strSql.Append("RemoveDBNullRow=@RemoveDBNullRow");
            strSql.Append(" where ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@Name", OdbcType.VarChar,120),
					new OdbcParameter("@CheckAngle", OdbcType.Int,11),
					new OdbcParameter("@ReSearchInterval", OdbcType.Int,11),
					new OdbcParameter("@ReSearchTime", OdbcType.Int,11),
					new OdbcParameter("@dN", OdbcType.Int,11),
					new OdbcParameter("@dE", OdbcType.Int,11),
					new OdbcParameter("@dZ", OdbcType.Int,11),
					new OdbcParameter("@_2C", OdbcType.Int,11),
					new OdbcParameter("@HarReadingDiff", OdbcType.Int,11),
					new OdbcParameter("@VarReadingDiff", OdbcType.Int,11),
					new OdbcParameter("@SdReadingDiff", OdbcType.Int,11),
					new OdbcParameter("@IndexError", OdbcType.Int,11),
					new OdbcParameter("@HalfChRegZeroDiff", OdbcType.Int,11),
					new OdbcParameter("@BetweenChRegZeroDiff", OdbcType.Int,11),
					new OdbcParameter("@BetweenCh2CDiff", OdbcType.Int,11),
					new OdbcParameter("@BetweenChIndexErrorDiff", OdbcType.Int,11),
					new OdbcParameter("@BetweenChSdDiff", OdbcType.Int,11),
					new OdbcParameter("@dSD", OdbcType.Int,11),
					new OdbcParameter("@ChaoXianCheck", OdbcType.Int,11),
					new OdbcParameter("@ChaoXianTime", OdbcType.Int,11),
					new OdbcParameter("@GetSdTimes", OdbcType.Int,11),
					new OdbcParameter("@ChaoXianPause", OdbcType.Int,11),
					new OdbcParameter("@CloseInstrument", OdbcType.Int,11),
					new OdbcParameter("@AutoPause", OdbcType.Int,11),
					new OdbcParameter("@AlwaysCW", OdbcType.Int,11),
					new OdbcParameter("@AutoUpdateStudyPoint", OdbcType.Int,11),
					new OdbcParameter("@AutoReport", OdbcType.Int,11),
					new OdbcParameter("@AutoReportPath", OdbcType.VarChar,120),
					new OdbcParameter("@TurningAngleRegis", OdbcType.Int,11),
					new OdbcParameter("@PowerOffAllowInterval", OdbcType.Int,11),
					new OdbcParameter("@RemoveDBNullRow", OdbcType.Int,1)};
            parameters[0].Value = model.Name;
            parameters[1].Value = model.CheckAngle;
            parameters[2].Value = model.ReSearchInterval;
            parameters[3].Value = model.ReSearchTime;
            parameters[4].Value = model.dN;
            parameters[5].Value = model.dE;
            parameters[6].Value = model.dZ;
            parameters[7].Value = model._2C;
            parameters[8].Value = model.HarReadingDiff;
            parameters[9].Value = model.VarReadingDiff;
            parameters[10].Value = model.SdReadingDiff;
            parameters[11].Value = model.IndexError;
            parameters[12].Value = model.HalfChRegZeroDiff;
            parameters[13].Value = model.BetweenChRegZeroDiff;
            parameters[14].Value = model.BetweenCh2CDiff;
            parameters[15].Value = model.BetweenChIndexErrorDiff;
            parameters[16].Value = model.BetweenChSdDiff;
            parameters[17].Value = model.dSD;
            parameters[18].Value = model.ChaoXianCheck;
            parameters[19].Value = model.ChaoXianTime;
            parameters[20].Value = model.GetSdTimes;
            parameters[21].Value = model.ChaoXianPause;
            parameters[22].Value = model.CloseInstrument;
            parameters[23].Value = model.AutoPause;
            parameters[24].Value = model.AlwaysCW;
            parameters[25].Value = model.AutoUpdateStudyPoint;
            parameters[26].Value = model.AutoReport;
            parameters[27].Value = model.AutoReportPath;
            parameters[28].Value = model.TurningAngleRegis;
            parameters[29].Value = model.PowerOffAllowInterval;
            parameters[30].Value = model.RemoveDBNullRow;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete()
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from fmos_setting ");
            strSql.Append(" where ");
            OdbcParameter[] parameters = {
			};

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string xmname, out TotalStation.Model.fmos_obj.setting setting)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            OdbcSQLHelper.Conn = conn;
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Name,CheckAngle,ReSearchInterval,ReSearchTime,dN,dE,dZ,_2C,HarReadingDiff,VarReadingDiff,SdReadingDiff,IndexError,HalfChRegZeroDiff,BetweenChRegZeroDiff,BetweenCh2CDiff,BetweenChIndexErrorDiff,BetweenChSdDiff,dSD,ChaoXianCheck,ChaoXianTime,GetSdTimes,ChaoXianPause,CloseInstrument,AutoPause,AlwaysCW,AutoUpdateStudyPoint,AutoReport,AutoReportPath,TurningAngleRegis,PowerOffAllowInterval,RemoveDBNullRow from fmos_setting ");
            strSql.Append(" where taskName=@taskName ");
            OdbcParameter[] parameters = {
                new OdbcParameter("@taskName", OdbcType.VarChar,120)
			};
            parameters[0].Value = xmname;
            setting = new Model.fmos_obj.setting();
            TotalStation.Model.fmos_obj.setting model = new TotalStation.Model.fmos_obj.setting();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), "fmos_setting", parameters);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                setting = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.setting DataRowToModel(DataRow row)
        {
            TotalStation.Model.fmos_obj.setting model = new TotalStation.Model.fmos_obj.setting();
            if (row != null)
            {
                if (row["Name"] != null)
                {
                    model.Name = row["Name"].ToString();
                }
                if (row["CheckAngle"] != null && row["CheckAngle"].ToString() != "")
                {
                    model.CheckAngle = row["CheckAngle"].ToString().ToString() == "1" ? true : false;
                }
                if (row["ReSearchInterval"] != null && row["ReSearchInterval"].ToString() != "")
                {
                    model.ReSearchInterval = int.Parse(row["ReSearchInterval"].ToString());
                }
                if (row["ReSearchTime"] != null && row["ReSearchTime"].ToString() != "")
                {
                    model.ReSearchTime = int.Parse(row["ReSearchTime"].ToString());
                }
                if (row["dN"] != null && row["dN"].ToString() != "")
                {
                    model.dN = int.Parse(row["dN"].ToString());
                }
                if (row["dE"] != null && row["dE"].ToString() != "")
                {
                    model.dE = int.Parse(row["dE"].ToString());
                }
                if (row["dZ"] != null && row["dZ"].ToString() != "")
                {
                    model.dZ = int.Parse(row["dZ"].ToString());
                }
                if (row["_2C"] != null && row["_2C"].ToString() != "")
                {
                    model._2C = int.Parse(row["_2C"].ToString());
                }
                if (row["HarReadingDiff"] != null && row["HarReadingDiff"].ToString() != "")
                {
                    model.HarReadingDiff = int.Parse(row["HarReadingDiff"].ToString());
                }
                if (row["VarReadingDiff"] != null && row["VarReadingDiff"].ToString() != "")
                {
                    model.VarReadingDiff = int.Parse(row["VarReadingDiff"].ToString());
                }
                if (row["SdReadingDiff"] != null && row["SdReadingDiff"].ToString() != "")
                {
                    model.SdReadingDiff = int.Parse(row["SdReadingDiff"].ToString());
                }
                if (row["IndexError"] != null && row["IndexError"].ToString() != "")
                {
                    model.IndexError = int.Parse(row["IndexError"].ToString());
                }
                if (row["HalfChRegZeroDiff"] != null && row["HalfChRegZeroDiff"].ToString() != "")
                {
                    model.HalfChRegZeroDiff = int.Parse(row["HalfChRegZeroDiff"].ToString());
                }
                if (row["BetweenChRegZeroDiff"] != null && row["BetweenChRegZeroDiff"].ToString() != "")
                {
                    model.BetweenChRegZeroDiff = int.Parse(row["BetweenChRegZeroDiff"].ToString());
                }
                if (row["BetweenCh2CDiff"] != null && row["BetweenCh2CDiff"].ToString() != "")
                {
                    model.BetweenCh2CDiff = int.Parse(row["BetweenCh2CDiff"].ToString());
                }
                if (row["BetweenChIndexErrorDiff"] != null && row["BetweenChIndexErrorDiff"].ToString() != "")
                {
                    model.BetweenChIndexErrorDiff = int.Parse(row["BetweenChIndexErrorDiff"].ToString());
                }
                if (row["BetweenChSdDiff"] != null && row["BetweenChSdDiff"].ToString() != "")
                {
                    model.BetweenChSdDiff = int.Parse(row["BetweenChSdDiff"].ToString());
                }
                if (row["dSD"] != null && row["dSD"].ToString() != "")
                {
                    model.dSD = int.Parse(row["dSD"].ToString());
                }
                if (row["ChaoXianCheck"] != null && row["ChaoXianCheck"].ToString() != "")
                {
                    model.ChaoXianCheck = row["ChaoXianCheck"].ToString().ToString() == "1" ? true : false;
                }
                if (row["ChaoXianTime"] != null && row["ChaoXianTime"].ToString() != "")
                {
                    model.ChaoXianTime = int.Parse(row["ChaoXianTime"].ToString());
                }
                if (row["GetSdTimes"] != null && row["GetSdTimes"].ToString() != "")
                {
                    model.GetSdTimes = int.Parse(row["GetSdTimes"].ToString());
                }
                if (row["ChaoXianPause"] != null && row["ChaoXianPause"].ToString() != "")
                {
                    model.ChaoXianPause = row["ChaoXianPause"].ToString().ToString() == "1" ? true : false;
                }
                if (row["CloseInstrument"] != null && row["CloseInstrument"].ToString() != "")
                {
                    model.CloseInstrument = row["CloseInstrument"].ToString().ToString() == "1" ? true : false;
                }
                if (row["AutoPause"] != null && row["AutoPause"].ToString() != "")
                {
                    model.AutoPause = row["AutoPause"].ToString().ToString() == "1" ? true : false;
                }
                if (row["AlwaysCW"] != null && row["AlwaysCW"].ToString() != "")
                {
                    model.AlwaysCW = row["AlwaysCW"].ToString().ToString() == "1" ? true : false;
                }
                if (row["AutoUpdateStudyPoint"] != null && row["AutoUpdateStudyPoint"].ToString() != "")
                {
                    model.AutoUpdateStudyPoint = row["AutoUpdateStudyPoint"].ToString().ToString() == "1" ? true : false;
                }
                if (row["AutoReport"] != null && row["AutoReport"].ToString() != "")
                {
                    model.AutoReport = row["AutoReport"].ToString() == "1" ? true : false;
                }
                if (row["AutoReportPath"] != null)
                {
                    model.AutoReportPath = row["AutoReportPath"].ToString();
                }
                if (row["TurningAngleRegis"] != null && row["TurningAngleRegis"].ToString() != "")
                {
                    model.TurningAngleRegis = int.Parse(row["TurningAngleRegis"].ToString());
                }
                if (row["PowerOffAllowInterval"] != null && row["PowerOffAllowInterval"].ToString() != "")
                {
                    model.PowerOffAllowInterval = int.Parse(row["PowerOffAllowInterval"].ToString());
                }
                if (row["RemoveDBNullRow"] != null && row["RemoveDBNullRow"].ToString() != "")
                {
                    model.RemoveDBNullRow = row["RemoveDBNullRow"].ToString() == "1" ? true : false;
                }
            }
            return model;
        }

        public bool SettingTableLoad(int startPageIndex, int pageSize, string xmname, string searchpost, string colName, string sord, out DataTable dt)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " taskName= " + xmname);
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select * from fmos_setting where {3}   {2}  limit {0},{1} ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, searchstr);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        public database db = new database();
        public bool SettingTableRowsCount(string searchpost, string xmname, out string totalCont)
        {
            string searchstr = string.Format(" {1} and {0} ", searchpost, " taskName= " + xmname);
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select count(*) from fmos_setting where {3}   ", searchstr);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }
        public bool SettingNameLoad(string xmname, out List<string> settingname)
        {
            string sql = "select name from fmos_setting where taskName='" + xmname + "'";
            OdbcConnection conn = db.GetStanderConn(xmname);
            List<string> settingNames = querysql.querystanderlist(sql, conn);
            settingname = settingNames;
            return true;
        }
       



        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

