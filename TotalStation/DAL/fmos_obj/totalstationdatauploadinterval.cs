﻿/**  版本信息模板在安装目录下，可自行修改。
* totalstationdatauploadinterval.cs
*
* 功 能： N/A
* 类 名： totalstationdatauploadinterval
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/4/11 10:38:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//using Odbc.Data.OdbcClient;
//using Maticsoft.DBUtility;//Please add references
namespace TotalStation.DAL.fmos_obj
{
	/// <summary>
	/// 数据访问类:totalstationdatauploadinterval
	/// </summary>
	public partial class totalstationdatauploadinterval
	{

        public static database db = new database();

		public totalstationdatauploadinterval()
		{}
		

		



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TotalStation.Model.fmos_obj.totalstationdatauploadinterval model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("replace into totalstationdatauploadinterval(");
			strSql.Append("xmno,hour,minute,xmname)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@hour,@minute,@xmname)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@minute", OdbcType.Int,11),
                    new OdbcParameter("@xmname", OdbcType.VarChar,100)
                                         };
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.hour;
			parameters[2].Value = model.minute;
            parameters[3].Value = model.xmname;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStation.Model.fmos_obj.totalstationdatauploadinterval model)
		{
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update totalstationdatauploadinterval set ");
			strSql.Append("hour=@hour,");
			strSql.Append("minute=@minute");
			strSql.Append(" where xmno=@xmno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@minute", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.hour;
			parameters[1].Value = model.minute;
			parameters[2].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from totalstationdatauploadinterval ");
			strSql.Append(" where xmno=@xmno ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
			parameters[0].Value = xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        public bool TableLoad(string xmname, out DataTable dt)
        {
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = "SELECT distinct(taskname),hour,minute  FROM fmos_cycdirnet left join totalstationdatauploadinterval on TaskName = xmname where taskname='"+xmname+"'";
            dt = querysql.querystanderdb(sql,conn);
            return true;
        }

		


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.totalstationdatauploadinterval DataRowToModel(DataRow row)
		{
			TotalStation.Model.fmos_obj.totalstationdatauploadinterval model=new TotalStation.Model.fmos_obj.totalstationdatauploadinterval();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["hour"]!=null && row["hour"].ToString()!="")
				{
					model.hour=int.Parse(row["hour"].ToString());
				}
				if(row["minute"]!=null && row["minute"].ToString()!="")
				{
					model.minute=int.Parse(row["minute"].ToString());
				}
			}
			return model;
		}

		
	}
}

