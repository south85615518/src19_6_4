﻿/**  版本信息模板在安装目录下，可自行修改。
* stationinfo.cs
*
* 功 能： N/A
* 类 名： stationinfo
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStation.Model.fmos_obj
{
	/// <summary>
	/// stationinfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class stationinfo
	{
		public stationinfo()
		{}
		#region Model
		private int? _instrumentid;
		private int _id;
		private string _station_name;
		private int? _dirmethod=0;
		private double? _station_n=0;
		private double? _station_e=0;
		private double? _station_z=0;
		private double? _station_hight;
		private string _backsignt_name;
		private string _backsignt_angle= "0";
		private DateTime? _search_time;
		private string _instrument_name;
		private string _remark;
        private string _taskname;

        public string Taskname
        {
            get { return _taskname; }
            set { _taskname = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int? InstrumentID
		{
			set{ _instrumentid=value;}
			get{return _instrumentid;}
		}
		/// <summary>
		/// auto_increment
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string STATION_NAME
		{
			set{ _station_name=value;}
			get{return _station_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? DirMethod
		{
			set{ _dirmethod=value;}
			get{return _dirmethod;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? STATION_N
		{
			set{ _station_n=value;}
			get{return _station_n;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? STATION_E
		{
			set{ _station_e=value;}
			get{return _station_e;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? STATION_Z
		{
			set{ _station_z=value;}
			get{return _station_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? STATION_HIGHT
		{
			set{ _station_hight=value;}
			get{return _station_hight;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BACKSIGNT_NAME
		{
			set{ _backsignt_name=value;}
			get{return _backsignt_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BACKSIGNT_ANGLE
		{
			set{ _backsignt_angle=value;}
			get{return _backsignt_angle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SEARCH_TIME
		{
			set{ _search_time=value;}
			get{return _search_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Instrument_Name
		{
			set{ _instrument_name=value;}
			get{return _instrument_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string REMARK
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		#endregion Model

	}
}

