﻿/**  版本信息模板在安装目录下，可自行修改。
* totalstationdatauploadinterval.cs
*
* 功 能： N/A
* 类 名： totalstationdatauploadinterval
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/4/11 10:34:13   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStation.Model.fmos_obj
{
	/// <summary>
	/// totalstationdatauploadinterval:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class totalstationdatauploadinterval
	{
		public totalstationdatauploadinterval()
		{}
		#region Model
		private int _xmno;
		private int _hour;
		private int _minute;
        private string _xmname;

        public string xmname
        {
            get { return _xmname; }
            set { _xmname = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int hour
		{
			set{ _hour=value;}
			get{return _hour;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int minute
		{
			set{ _minute=value;}
			get{return _minute;}
		}
		#endregion Model

	}
}

