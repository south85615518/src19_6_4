﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_pointsinteam.cs
*
* 功 能： N/A
* 类 名： fmos_pointsinteam
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/21 14:45:52   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStation.Model.fmos_obj
{
	/// <summary>
	/// fmos_pointsinteam:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fmos_pointsinteam
	{
		public fmos_pointsinteam()
		{}
		#region Model
		private int _id=0;
		private int? _idex;
        private int _xmno;

        public int Xmno
        {
            get { return _xmno; }
            set { _xmno = value; }
        }
		private string _team_name;
		private string _point_name;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Idex
		{
			set{ _idex=value;}
			get{return _idex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TEAM_NAME
		{
			set{ _team_name=value;}
			get{return _team_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POINT_NAME
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		#endregion Model

	}
}

