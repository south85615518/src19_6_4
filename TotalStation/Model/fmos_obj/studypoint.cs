﻿/**  版本信息模板在安装目录下，可自行修改。
* studypoint.cs
*
* 功 能： N/A
* 类 名： studypoint
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:12   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStation.Model.fmos_obj
{
	/// <summary>
	/// studypoint:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class studypoint
	{
		public studypoint()
		{}
		#region Model
		private string _orglhar;
		private string _orglvar;
		private double? _orglsd;
		private string _stationname;
		private DateTime? _search_time;
		private bool? _onlyangleuseful= false;
		private string _mileage= "K0+0";
		private bool? _iscontrolpoint=false;
		private int _id;
		private string _point_name;
		private double? _n=0;
		private double? _e=0;
		private double? _z=0;
		private double? _targethight=0;
		private string _reflectorname;
		private string _remark;
		private string _taskname;
		/// <summary>
		/// 
		/// </summary>
		public string orglHar
		{
			set{ _orglhar=value;}
			get{return _orglhar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string orglVar
		{
			set{ _orglvar=value;}
			get{return _orglvar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? orglSd
		{
			set{ _orglsd=value;}
			get{return _orglsd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string StationName
		{
			set{ _stationname=value;}
			get{return _stationname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SEARCH_TIME
		{
			set{ _search_time=value;}
			get{return _search_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool? OnlyAngleUseful
		{
			set{ _onlyangleuseful=value;}
			get{return _onlyangleuseful;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Mileage
		{
			set{ _mileage=value;}
			get{return _mileage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool? IsControlPoint
		{
			set{ _iscontrolpoint=value;}
			get{return _iscontrolpoint;}
		}
		/// <summary>
		/// auto_increment
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POINT_NAME
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? N
		{
			set{ _n=value;}
			get{return _n;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? E
		{
			set{ _e=value;}
			get{return _e;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? Z
		{
			set{ _z=value;}
			get{return _z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? TargetHight
		{
			set{ _targethight=value;}
			get{return _targethight;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ReflectorName
		{
			set{ _reflectorname=value;}
			get{return _reflectorname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string REMARK
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string taskName
		{
			set{ _taskname=value;}
			get{return _taskname;}
		}
		#endregion Model

	}
}

