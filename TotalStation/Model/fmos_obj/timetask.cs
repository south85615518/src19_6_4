﻿/**  版本信息模板在安装目录下，可自行修改。
* timetask.cs
*
* 功 能： N/A
* 类 名： timetask
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStation.Model.fmos_obj
{
	/// <summary>
	/// timetask:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class timetask
	{
		public timetask()
		{}
		#region Model
		private int? _ch;
		private string _datetaskname;
		private int? _indx;
		private int? _timinghour;
		private int? _timingminute;
		private int? _starttimetype;
		private int? _minterval;
		private string _taskname;
		/// <summary>
		/// 
		/// </summary>
		public int? CH
		{
			set{ _ch=value;}
			get{return _ch;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DateTaskName
		{
			set{ _datetaskname=value;}
			get{return _datetaskname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Indx
		{
			set{ _indx=value;}
			get{return _indx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TimingHour
		{
			set{ _timinghour=value;}
			get{return _timinghour;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TimingMinute
		{
			set{ _timingminute=value;}
			get{return _timingminute;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? StartTimeType
		{
			set{ _starttimetype=value;}
			get{return _starttimetype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mInterval
		{
			set{ _minterval=value;}
			get{return _minterval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string taskName
		{
			set{ _taskname=value;}
			get{return _taskname;}
		}
		#endregion Model


	}
}

