﻿/**  版本信息模板在安装目录下，可自行修改。
* setting.cs
*
* 功 能： N/A
* 类 名： setting
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStation.Model.fmos_obj
{
	/// <summary>
	/// setting:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class setting
	{
		public setting()
		{}
		#region Model
		private string _name;
		private bool _checkangle=true;
		private int? _researchinterval=10;
		private int? _researchtime=3;
		private int? _dn=50;
		private int? _de=50;
		private int? _dz=50;
		private int? __2c=20;
		private int? _harreadingdiff=2;
		private int? _varreadingdiff=2;
		private int? _sdreadingdiff=1;
		private int? _indexerror=40;
		private int? _halfchregzerodiff=5;
		private int? _betweenchregzerodiff=2;
		private int? _betweench2cdiff=9;
		private int? _betweenchindexerrordiff=9;
		private int? _betweenchsddiff=2;
		private int? _dsd=60;
		private bool _chaoxiancheck=true;
		private int? _chaoxiantime=3;
		private int? _getsdtimes=3;
		private bool _chaoxianpause=false;
		private bool _closeinstrument=false;
		private bool _autopause=false;
		private bool _alwayscw=false;
		private bool _autoupdatestudypoint=false;
		private bool _autoreport=false;
		private string _autoreportpath= "C:UsersAdministratorDesktop自动生成报表";
		private int? _turningangleregis=2;
		private int? _poweroffallowinterval=30;
		private bool _removedbnullrow=true;
        private string _taskName;

        public string TaskName
        {
            get { return _taskName; }
            set { _taskName = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool CheckAngle
		{
			set{ _checkangle=value;}
			get{return _checkangle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ReSearchInterval
		{
			set{ _researchinterval=value;}
			get{return _researchinterval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ReSearchTime
		{
			set{ _researchtime=value;}
			get{return _researchtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? dN
		{
			set{ _dn=value;}
			get{return _dn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? dE
		{
			set{ _de=value;}
			get{return _de;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? dZ
		{
			set{ _dz=value;}
			get{return _dz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? _2C
		{
			set{ __2c=value;}
			get{return __2c;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? HarReadingDiff
		{
			set{ _harreadingdiff=value;}
			get{return _harreadingdiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? VarReadingDiff
		{
			set{ _varreadingdiff=value;}
			get{return _varreadingdiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SdReadingDiff
		{
			set{ _sdreadingdiff=value;}
			get{return _sdreadingdiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IndexError
		{
			set{ _indexerror=value;}
			get{return _indexerror;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? HalfChRegZeroDiff
		{
			set{ _halfchregzerodiff=value;}
			get{return _halfchregzerodiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? BetweenChRegZeroDiff
		{
			set{ _betweenchregzerodiff=value;}
			get{return _betweenchregzerodiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? BetweenCh2CDiff
		{
			set{ _betweench2cdiff=value;}
			get{return _betweench2cdiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? BetweenChIndexErrorDiff
		{
			set{ _betweenchindexerrordiff=value;}
			get{return _betweenchindexerrordiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? BetweenChSdDiff
		{
			set{ _betweenchsddiff=value;}
			get{return _betweenchsddiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? dSD
		{
			set{ _dsd=value;}
			get{return _dsd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool ChaoXianCheck
		{
			set{ _chaoxiancheck=value;}
			get{return _chaoxiancheck;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ChaoXianTime
		{
			set{ _chaoxiantime=value;}
			get{return _chaoxiantime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? GetSdTimes
		{
			set{ _getsdtimes=value;}
			get{return _getsdtimes;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool ChaoXianPause
		{
			set{ _chaoxianpause=value;}
			get{return _chaoxianpause;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool CloseInstrument
		{
			set{ _closeinstrument=value;}
			get{return _closeinstrument;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool AutoPause
		{
			set{ _autopause=value;}
			get{return _autopause;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool AlwaysCW
		{
			set{ _alwayscw=value;}
			get{return _alwayscw;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool AutoUpdateStudyPoint
		{
			set{ _autoupdatestudypoint=value;}
			get{return _autoupdatestudypoint;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool AutoReport
		{
			set{ _autoreport=value;}
			get{return _autoreport;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AutoReportPath
		{
			set{ _autoreportpath=value;}
			get{return _autoreportpath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TurningAngleRegis
		{
			set{ _turningangleregis=value;}
			get{return _turningangleregis;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PowerOffAllowInterval
		{
			set{ _poweroffallowinterval=value;}
			get{return _poweroffallowinterval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool RemoveDBNullRow
		{
			set{ _removedbnullrow=value;}
			get{return _removedbnullrow;}
		}
		#endregion Model

	}
}

