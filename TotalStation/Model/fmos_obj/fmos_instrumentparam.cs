﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_instrumentparam.cs
*
* 功 能： N/A
* 类 名： fmos_instrumentparam
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/21 14:45:50   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace TotalStation.Model.fmos_obj
{
	/// <summary>
	/// fmos_instrumentparam:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fmos_instrumentparam
	{
		public fmos_instrumentparam()
		{}
		#region Model
		private int _id=0;
		private string _name;
		private int? _instrumenttype=0;
        private bool? _autoconnect = false;

        public bool? Autoconnect
        {
            get { return _autoconnect; }
            set { _autoconnect = value; }
        }
		private string _remark;
		private string _powermodname;
		private int? _pontimeseach=3;
		private int? _poninterval=300;
		private int? _pontimes=1;
        private bool? _powercontrol = false;

        public bool? Powercontrol
        {
            get { return _powercontrol; }
            set { _powercontrol = value; }
        }
		private int? _receivetimeout=30;
		private double? _harsrange=0;
		private double? _varsrange=0;
		private string _com= "COM1";
		private int? _connect_method=0;
		private int? _baudrate=9600;
		private int? _databits=8;
		private int? _stopbits=1;
		private int? _parity=0;
		private int? _localport=0;
		private string _localip= "127.0.0.1";
        private int _xmno;

        public int Xmno
        {
            get { return _xmno; }
            set { _xmno = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InstrumentType
		{
			set{ _instrumenttype=value;}
			get{return _instrumenttype;}
		}
	
		/// <summary>
		/// 
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PowerModName
		{
			set{ _powermodname=value;}
			get{return _powermodname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PONTimesEach
		{
			set{ _pontimeseach=value;}
			get{return _pontimeseach;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PONInterval
		{
			set{ _poninterval=value;}
			get{return _poninterval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PONTimes
		{
			set{ _pontimes=value;}
			get{return _pontimes;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int? ReceiveTimeOut
		{
			set{ _receivetimeout=value;}
			get{return _receivetimeout;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? HarSRange
		{
			set{ _harsrange=value;}
			get{return _harsrange;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? VarSRange
		{
			set{ _varsrange=value;}
			get{return _varsrange;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string COM
		{
			set{ _com=value;}
			get{return _com;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Connect_METHOD
		{
			set{ _connect_method=value;}
			get{return _connect_method;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? BaudRate
		{
			set{ _baudrate=value;}
			get{return _baudrate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? DataBits
		{
			set{ _databits=value;}
			get{return _databits;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? StopBits
		{
			set{ _stopbits=value;}
			get{return _stopbits;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Parity
		{
			set{ _parity=value;}
			get{return _parity;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Localport
		{
			set{ _localport=value;}
			get{return _localport;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LocalIP
		{
			set{ _localip=value;}
			get{return _localip;}
		}
		#endregion Model

	}
}

