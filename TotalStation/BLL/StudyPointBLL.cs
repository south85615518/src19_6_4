﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using NFnet_DAL;
using System.Data;
using NFnet_MODAL;
using NFnet_Model;
using System.Web.Script.Serialization;
using TotalStation.Model.fmos_obj;

namespace TotalStation.BLL
{
    public class StudyPointBLL
    {
       private readonly studypoint dal = new studypoint();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public StudyPointBLL()
        {

        }
        #region  BasicMethod
        //读取数据文件建立对象模型
        public static string TxtFileReader(string path, OdbcConnection conn,string xmname)
        {

            path = path.Substring(0, path.IndexOf(".txt") + 4);
            TxtEncoding.GetType(path);
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs, TxtEncoding.Encoding);
            try
            {
                List<string> lshead = new List<string>();
                string insertCmd = "";
                //string sqlCmd = "select * from fmos_studypoint where POINT_NAME =@POINT_NAME and TaskName = @TaskName";
                char delimChar = ',';
                string[] split = null;
                int i = 0;
                string strTemp = sr.ReadLine();
                studypoint dal = new studypoint();
                OdbcSQLHelper.Conn = conn;
                while (strTemp != null && strTemp != "" && strTemp != "ThisIsFileEnd")
                {
                    TotalStation.Model.fmos_obj.studypoint model = (TotalStation.Model.fmos_obj.studypoint)jss.Deserialize(strTemp, typeof(TotalStation.Model.fmos_obj.studypoint));
                    model.taskName = xmname;
                    dal.Add(model);
                    strTemp = sr.ReadLine();
                }
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }
            catch (Exception ex)
            {
                if (sr != null)
                    sr.Close();
                if (fs != null)
                    fs.Close();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                ExceptionLog.ExceptionWrite(ex, "文件读取出错!");
            }
            return null;

        }





        ///// <summary>
        ///// 得到最大ID
        ///// </summary>
        //public int GetMaxId()
        //{
        //    return dal.GetMaxId();
        //}

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        //public bool Exists(int ID)
        //{
        //    return dal.Exists(ID);
        //}

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.studypoint model)
        {
            try
            {
                return dal.Add(model);
            }
            catch (Exception ex)
            {
                //
                string error = ex.StackTrace;
                ExceptionLog.ExceptionWrite(ex,  error);
                //log
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.studypoint model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.studypoint GetModel(int ID)
        {

            return dal.GetModel(ID);
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TotalStation.Model.fmos_obj.studypoint> DataTableToList(DataTable dt)
        {
            List<TotalStation.Model.fmos_obj.studypoint> modelList = new List<TotalStation.Model.fmos_obj.studypoint>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TotalStation.Model.fmos_obj.studypoint model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}