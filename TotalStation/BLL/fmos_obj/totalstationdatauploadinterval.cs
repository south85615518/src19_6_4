﻿/**  版本信息模板在安装目录下，可自行修改。
* totalstationdatauploadinterval.cs
*
* 功 能： N/A
* 类 名： totalstationdatauploadinterval
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/4/11 10:38:31   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//using Odbc.Data.OdbcClient;
//using Maticsoft.DBUtility;//Please add references
namespace TotalStation.BLL.fmos_obj
{
	/// <summary>
	/// 数据访问类:totalstationdatauploadinterval
	/// </summary>
	public partial class totalstationdatauploadinterval
	{

        public static database db = new database();
        public static TotalStation.DAL.fmos_obj.totalstationdatauploadinterval dal = new DAL.fmos_obj.totalstationdatauploadinterval(); 
		public totalstationdatauploadinterval()
		{}
		

		



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TotalStation.Model.fmos_obj.totalstationdatauploadinterval model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目{0}的数据检查间隔成功", model.xmname);
                    return true;

                }
                else
                {
                    mssg = string.Format("添加项目{0}的数据检查间隔失败", model.xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目{0}的数据检查间隔出错，错误信息："+ex.Message, model.xmname);
                return false;
            }
		}
        ///// <summary>
        ///// 更新一条数据
        ///// </summary>
        //public bool Update(TotalStation.Model.fmos_obj.totalstationdatauploadinterval model,out string mssg)
        //{
        //    mssg = "";
        //    return false;
        //}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,out string mssg)
		{
            mssg = "";
            return false;
			
		}

        public bool TableLoad(string xmname, out DataTable dt,out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.TableLoad(xmname, out dt))
                {
                    mssg = string.Format("获取项目{0}全站仪项目的数据检查时间间隔表记录数{1}成功", xmname, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}全站仪项目的数据检查时间间隔表失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}全站仪项目的数据检查时间间隔表出错,错误信息："+ex.Message, xmname, dt.Rows.Count);
                return false;
            }

            //return false;
        }

		


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.totalstationdatauploadinterval DataRowToModel(DataRow row)
		{
			TotalStation.Model.fmos_obj.totalstationdatauploadinterval model=new TotalStation.Model.fmos_obj.totalstationdatauploadinterval();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["hour"]!=null && row["hour"].ToString()!="")
				{
					model.hour=int.Parse(row["hour"].ToString());
				}
				if(row["minute"]!=null && row["minute"].ToString()!="")
				{
					model.minute=int.Parse(row["minute"].ToString());
				}
			}
			return model;
		}

		
	}
}

