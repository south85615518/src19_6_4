﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_instrumentparam.cs
*
* 功 能： N/A
* 类 名： fmos_instrumentparam
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/21 14:45:51   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using System.Web.Script.Serialization;
//using TotalStation.Common;
//using TotalStation.Model;
namespace TotalStation.BLL.fmos_obj
{
	/// <summary>
	/// fmos_instrumentparam
	/// </summary>
	public partial class fmos_instrumentparam
	{
		private readonly TotalStation.DAL.fmos_obj.fmos_instrumentparam dal=new TotalStation.DAL.fmos_obj.fmos_instrumentparam();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
		public fmos_instrumentparam()
		{}
		#region  BasicMethod
        
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.fmos_instrumentparam model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = "仪器参数数据添加成功";
                    return true;
                }
                else
                {
                    mssg = "仪器参数数据添加失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "仪器参数数据添加出错，错误信息：" + ex.Message;
                return false;
            }
        }
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStation.Model.fmos_obj.fmos_instrumentparam model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete();
		}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string name, out object obj, out string mssg)
        {
            //该表无主键信息，请自定义主键/条件字段
            //return dal.GetModel();
            obj = null;
            TotalStation.Model.fmos_obj.fmos_instrumentparam model = new TotalStation.Model.fmos_obj.fmos_instrumentparam();
            try
            {
                if (dal.GetModel(xmno, name, out model))
                {
                    mssg = string.Format("获取项目编号{0}仪器名为{1}的参数成功", xmno, name);
                    obj = model;
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}仪器名为{1}的参数失败", xmno, name);
                    return false;
                }



            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}仪器名为{1}的参数出错,错误信息:" + ex.Message, xmno, name);
                return false;
            }
        }

        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public TotalStation.Model.fmos_obj.fmos_instrumentparam GetModelByCache()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    string CacheKey = "fmos_instrumentparamModel-" ;
        //    object objModel = TotalStation.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel();
        //            if (objModel != null)
        //            {
        //                int ModelCache = TotalStation.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                TotalStation.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (TotalStation.Model.fmos_obj.fmos_instrumentparam)objModel;
        //}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TotalStation.Model.fmos_obj.fmos_instrumentparam> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TotalStation.Model.fmos_obj.fmos_instrumentparam> DataTableToList(DataTable dt)
		{
			List<TotalStation.Model.fmos_obj.fmos_instrumentparam> modelList = new List<TotalStation.Model.fmos_obj.fmos_instrumentparam>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TotalStation.Model.fmos_obj.fmos_instrumentparam model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

        public bool InstrumentParamTableLoad(int startPageIndex, int pageSize, int xmno, string xmname, string searchpost, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.InstrumentParamTableLoad(startPageIndex, pageSize, xmno, xmname, searchpost, colName, sord, out  dt))
                {
                    mssg = "加载仪器表成功";
                    return true;
                }
                else
                {
                    mssg = "加载仪器表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载仪器表出错，错误信息："+ex.Message;
                return false;
            }
        }

        public bool InstrumentParamTableRowsCount(string searchpost,int xmno, string xmname, out string totalCont,out string mssg)
        {
            totalCont = "-1";
            try
            {
                if (dal.InstrumentParamTableRowsCount(searchpost, xmno, xmname,  out  totalCont))
                {
                    mssg = "加载仪器表记录数成功";
                    return true;
                }
                else
                {
                    mssg = "加载仪器表记录数失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载仪器表记录数出错，错误信息：" + ex.Message;
                return false;
            }
        }

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

