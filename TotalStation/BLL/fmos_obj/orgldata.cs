﻿/**  版本信息模板在安装目录下，可自行修改。
* cycdirnet.cs
*
* 功 能： N/A
* 类 名： cycdirnet
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Web.Script.Serialization;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
namespace TotalStation.BLL.fmos_obj
{
    /// <summary>
    /// orgldata
    /// </summary>
    public partial class orgldata
    {
        private readonly TotalStation.DAL.fmos_obj.orgldata dal = new TotalStation.DAL.fmos_obj.orgldata();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public static database db = new database();
        public orgldata()
        { }
        #region  BasicMethod
        
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.orgldata model, out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("{0}{1}周期{2}原始数据添加成功", model.POINT_NAME, model.SEARCH_TIME,model.CYC);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}{1}周期{2}原始数据添加失败", model.POINT_NAME, model.SEARCH_TIME,model.CYC);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("{0}{1}周期{2}原始数据添加出错,错误信息:" + ex.Message, model.POINT_NAME, model.SEARCH_TIME,model.CYC); ;
                return false;
            }
            finally
            {
                Console.WriteLine(mssg);
                ExceptionLog.XmRecordWrite(mssg,model.TaskName);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.orgldata model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string TaskName, string POINT_NAME, DateTime SEARCH_TIME)
        {

            return dal.Delete(TaskName, POINT_NAME, SEARCH_TIME);
        }

        public bool OrgldataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.OrgldataTableLoad(startCyc, endCyc, startPageIndex, pageSize, xmname, pointname, out  dt))
                {
                    mssg = "原始数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "原始数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool OrgldataTableRowsCount(int startCyc, int endCyc,  string xmname, string pointname,out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.OrgldataTableRowsCount(startCyc, endCyc, xmname,pointname,out totalCont))
                {
                    mssg = "原始数据表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "原始数据表记录数加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        //根据项目名获取所以的周期
        public bool CycList(string xmname, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {

                if (dal.CycList(xmname, out dt))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }
        public bool PointNameCycListGet(string xmname, string pointname, out List<string> ls, out string mssg)
        {
            ls = new List<string>();
            try
            {
                if (dal.PointNameCycListGet(xmname, pointname, out ls))
                {
                    mssg = string.Format("获取项目{0}点名{1}的周期列表数量{2}成功", xmname, pointname, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}点名{1}的周期列表失败", xmname, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}点名{1}的周期列表出错,错误信息:" + ex.Message, xmname, pointname);
                return false;
            }

        }
        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

