﻿/**  版本信息模板在安装目录下，可自行修改。
* stationinfo.cs
*
* 功 能： N/A
* 类 名： stationinfo
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using System.Web.Script.Serialization;
using Tool;
namespace TotalStation.BLL.fmos_obj
{
	/// <summary>
	/// stationinfo
	/// </summary>
	public partial class stationinfo
	{
		private readonly TotalStation.DAL.fmos_obj.stationinfo dal=new TotalStation.DAL.fmos_obj.stationinfo();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
		public stationinfo()
		{}
		#region  BasicMethod
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.stationinfo model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = "测站添加成功";
                    return true;
                }
                else
                {
                    mssg = "测站添加失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "测站添加出错，错误信息：" + ex.Message;
                return false;
            }
        }
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStation.Model.fmos_obj.stationinfo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TotalStation.Model.fmos_obj.stationinfo GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}


        public bool stationInfoTableLoad(int startPageIndex, int pageSize, string xmname, string searchpost, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.StationInfoTableLoad(startPageIndex, pageSize, xmname, searchpost, colName, sord, out  dt))
                {
                    mssg = "加载测站表成功";
                    return true;
                }
                else
                {
                    mssg = "加载测站表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载测站表出错，错误信息：" + ex.Message;
                return false;
            }
        }

        public bool stationInfoTableRowsCount(string searchpost, string xmname, out string totalCont, out string mssg)
        {
            totalCont = "-1";
            try
            {
                if (dal.StationInfoTableRowsCount(searchpost, xmname, out  totalCont))
                {
                    mssg = "加载测站表记录数成功";
                    return true;
                }
                else
                {
                    mssg = "加载测站表记录数失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载测站表记录数出错，错误信息：" + ex.Message;
                return false;
            }
        }


        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public TotalStation.Model.fmos_obj.stationinfo GetModelByCache(int ID)
        //{
			
        //    string CacheKey = "stationinfoModel-" + ID;
        //    object objModel = TotalStation.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(ID);
        //            if (objModel != null)
        //            {
        //                int ModelCache = TotalStation.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                TotalStation.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (TotalStation.Model.fmos_obj.stationinfo)objModel;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    return dal.GetList(strWhere);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<TotalStation.Model.fmos_obj.stationinfo> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<TotalStation.Model.fmos_obj.stationinfo> DataTableToList(DataTable dt)
        //{
        //    List<TotalStation.Model.fmos_obj.stationinfo> modelList = new List<TotalStation.Model.fmos_obj.stationinfo>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        TotalStation.Model.fmos_obj.stationinfo model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

