﻿/**  版本信息模板在安装目录下，可自行修改。
* studypoint.cs
*
* 功 能： N/A
* 类 名： studypoint
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:12   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
namespace TotalStation.BLL.fmos_obj
{
    /// <summary>
    /// studypoint
    /// </summary>
    public partial class studypoint
    {
        private readonly TotalStation.DAL.fmos_obj.studypoint dal = new TotalStation.DAL.fmos_obj.studypoint();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public studypoint()
        { }
        #region  BasicMethod


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.studypoint model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = "学习点添加成功";
                    return true;
                }
                else
                {
                    mssg = "学习点添加失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "学习点添加出错，错误信息：" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.studypoint model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TotalStation.Model.fmos_obj.studypoint GetModel(int ID)
        {

            return dal.GetModel(ID);
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TotalStation.Model.fmos_obj.studypoint> DataTableToList(DataTable dt)
        {
            List<TotalStation.Model.fmos_obj.studypoint> modelList = new List<TotalStation.Model.fmos_obj.studypoint>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TotalStation.Model.fmos_obj.studypoint model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }


        public bool StudyPointTableLoad(int startPageIndex, int pageSize, string xmname, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.StudyPointTableLoad(startPageIndex, pageSize, xmname, colName, sord, out  dt))
                {
                    mssg = "学习点表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "学习点表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "学习点表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool StudyPointTableRowsCount( string xmname, out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.StudyPointTableRowsCount( xmname, out totalCont))
                {
                    mssg = "学习点表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "学习点表记录数加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "学习点表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

    

        /// <summary>
        /// 分页获取数据列表
        /// </summary>

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

