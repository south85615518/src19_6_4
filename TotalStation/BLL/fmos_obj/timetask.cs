﻿/**  版本信息模板在安装目录下，可自行修改。
* timetask.cs
*
* 功 能： N/A
* 类 名： timetask
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using System.Web.Script.Serialization;
using Tool;
using System.Text;
using SqlHelpers;
namespace TotalStation.BLL.fmos_obj
{
    /// <summary>
    /// timetask
    /// </summary>
    public partial class timetask
    {
        private readonly TotalStation.DAL.fmos_obj.timetask dal = new TotalStation.DAL.fmos_obj.timetask();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public timetask()
        { }
        #region  BasicMethod


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.timetask model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = "时间设置添加成功";
                    return true;
                }
                else
                {
                    mssg = "时间设置添加失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "时间设置添加出错，错误信息：" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.timetask model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = "时间设置更新成功";
                    return true;
                }
                else
                {
                    mssg = "时间设置更新失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "时间设置更新出错，错误信息：" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string DateTaskName, string Taskname, out string mssg)
        {
            try
            {
                if (dal.Delete(DateTaskName, Taskname))
                {
                    mssg = "时间设置删除成功";
                    return true;
                }
                else
                {
                    mssg = "时间设置删除失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "时间设置删除出错，错误信息：" + ex.Message;
                return false;
            }

        }

        public bool timetaskTableLoad(int startPageIndex, int pageSize, string xmname, string searchpost, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TimetaskTableLoad(startPageIndex, pageSize, xmname, searchpost, colName, sord, out  dt))
                {
                    mssg = "加载时间参数表成功";
                    return true;
                }
                else
                {
                    mssg = "加载时间参数表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载时间参数表出错，错误信息：" + ex.Message;
                return false;
            }
        }

        public bool timetaskTableRowsCount(string searchpost, string xmname, out string totalCont, out string mssg)
        {
            totalCont = "-1";
            try
            {
                if (dal.TimetaskTableRowsCount(searchpost, xmname, out  totalCont))
                {
                    mssg = "加载时间参数表记录数成功";
                    return true;
                }
                else
                {
                    mssg = "加载时间参数表记录数失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载时间参数表记录数出错，错误信息：" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 得到一个表中的所有对象实体
        /// </summary>
        public bool GetModelList(string taskName, out List<TotalStation.Model.fmos_obj.timetask> lt, out string mssg)
        {
            mssg = "";
            lt = null;
            try
            {
                if (dal.GetModelList(taskName, out lt))
                {
                    mssg = string.Format("获取{0}时间对象列表成功", taskName);
                    return true;

                }
                else
                {
                    mssg = string.Format("获取{0}时间对象列表失败", taskName);
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}时间对象列表出错,错误信息:" + ex.Message);
                return false;
            }

        }
        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

