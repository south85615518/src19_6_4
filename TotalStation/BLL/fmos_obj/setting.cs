﻿/**  版本信息模板在安装目录下，可自行修改。
* setting.cs
*
* 功 能： N/A
* 类 名： setting
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/7 9:44:13   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using System.Web.Script.Serialization;
using Tool;
namespace TotalStation.BLL.fmos_obj
{
    /// <summary>
    /// setting
    /// </summary>
    public partial class setting
    {
        private readonly TotalStation.DAL.fmos_obj.setting dal = new TotalStation.DAL.fmos_obj.setting();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
        public setting()
        { }
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TotalStation.Model.fmos_obj.setting model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = "设置参数添加成功";
                    return true;
                }
                else
                {
                    mssg = "设置参数添加失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "设置参数添加出错，错误信息：" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TotalStation.Model.fmos_obj.setting model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete()
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.Delete();
        }

        public bool settingTableLoad(int startPageIndex, int pageSize, string xmname, string searchpost, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SettingTableLoad(startPageIndex, pageSize, xmname, searchpost, colName, sord, out  dt))
                {
                    mssg = "加载设置参数表成功";
                    return true;
                }
                else
                {
                    mssg = "加载设置参数表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载设置参数表出错，错误信息：" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string xmname, out TotalStation.Model.fmos_obj.setting setting, out string mssg)
        {
            setting = null;
            try
            {
                if (dal.GetModel(xmname, out setting))
                {
                    mssg = string.Format("获取{0}的设置参数成功", xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}的设置参数失败", xmname);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的设置参数出错，出错信息：{1}", xmname, ex.Message);
                return false;
            }
        }



        public bool settingTableRowsCount(string searchpost, string xmname, out string totalCont, out string mssg)
        {
            totalCont = "-1";
            try
            {
                if (dal.SettingTableRowsCount(searchpost, xmname, out  totalCont))
                {
                    mssg = "加载设置参数表记录数成功";
                    return true;
                }
                else
                {
                    mssg = "加载设置参数表记录数失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载设置参数表记录数出错，错误信息：" + ex.Message;
                return false;
            }
        }

        public bool SettingNameLoad(string xmname, out List<string> settingname, out string mssg)
        {
            settingname = null;
            try
            {
                dal.SettingNameLoad(xmname, out settingname);
                if (settingname != null)
                {
                    mssg = "设置名称加载成功";
                    return true;
                }
                else
                {
                    mssg = "设置名称加载失败";
                    return false;

                }
            }
            catch (Exception ex)
            {
                mssg = "设置名称加载出错,错误信息:"+ex.Message;
                return false;
            }
        }



        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public TotalStation.Model.fmos_obj.setting GetModel()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    return dal.GetModel();
        //}

        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public TotalStation.Model.fmos_obj.setting GetModelByCache()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    string CacheKey = "settingModel-" ;
        //    object objModel = TotalStation.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel();
        //            if (objModel != null)
        //            {
        //                int ModelCache = TotalStation.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                TotalStation.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (TotalStation.Model.fmos_obj.setting)objModel;
        //}

        /////// <summary>
        /////// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    return dal.GetList(strWhere);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<TotalStation.Model.fmos_obj.setting> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<TotalStation.Model.fmos_obj.setting> DataTableToList(DataTable dt)
        //{
        //    List<TotalStation.Model.fmos_obj.setting> modelList = new List<TotalStation.Model.fmos_obj.setting>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        TotalStation.Model.fmos_obj.setting model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

