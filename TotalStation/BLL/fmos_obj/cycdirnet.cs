﻿/**  版本信息模板在安装目录下，可自行修改。
* cycdirnet.cs
*
* 功 能： N/A
* 类 名： cycdirnet
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/8 14:51:06   N/A    初版
*
* Copyright (c) 2012 TotalStation Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Data;
using System.Web.Script.Serialization;
using Tool;
using SqlHelpers;
namespace TotalStation.BLL.fmos_obj
{
	/// <summary>
	/// cycdirnet
	/// </summary>
	public partial class cycdirnet
	{
        
		private readonly TotalStation.DAL.fmos_obj.cycdirnet dal=new TotalStation.DAL.fmos_obj.cycdirnet();
        public static JavaScriptSerializer jss = new JavaScriptSerializer();
		public cycdirnet()
		{}
		#region  BasicMethod
        

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TotalStation.Model.fmos_obj.cycdirnet model,out string mssg)
		{
            mssg = "";
            try
            {
                if (dal.Add(model))
                { 
                    mssg = string.Format("项目{0}点{1} {2}周期{3}表面位移结果数据添加成功", model.TaskName, model.POINT_NAME,model.Time ,model.CYC);

                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}点{1} {2}周期{3}表面位移结果数据添加失败", model.TaskName, model.POINT_NAME,model.Time ,model.CYC);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}点{1} {2}周期{3}表面位移结果数据添加出错,错误信息：" + ex.Message, model.TaskName, model.POINT_NAME,model.Time ,model.CYC);
                return false;
            }
            finally
            {
                Console.WriteLine(mssg);
            }
		}

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool AddSurveyData(TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            try
            {
                if (dal.AddSurveyData(model))
                {
                    mssg = string.Format("项目{0}点{1}周期{2}表面位移结果数据添加进编辑库成功", model.TaskName, model.POINT_NAME, model.CYC);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}点{1}周期{2}表面位移结果数据添加进编辑库失败", model.TaskName, model.POINT_NAME, model.CYC);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}点{1}周期{2}表面位移结果数据添加进编辑库出错,错误信息：" + ex.Message, model.TaskName, model.POINT_NAME, model.CYC);
                return false;
            }
        }


        public bool AddCg(TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            try
            {
                if (dal.AddCg(model))
                {
                    mssg = string.Format("项目{0}点{1}周期{2}表面位移成果数据添加成功", model.TaskName, model.POINT_NAME, model.CYC);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}点{1}周期{2}表面位移成果数据添加失败", model.TaskName, model.POINT_NAME, model.CYC);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}点{1}周期{2}表面位移成果数据添加出错,错误信息：" + ex.Message, model.TaskName, model.POINT_NAME, model.CYC);
                return false;
            }
        }

        public bool SurveyDataUnion(string xmname, int cyc, int targetcyc, int lastcyc,out string mssg)
        {
            try
            {
                if (dal.SurveyDataUnion(xmname, cyc, targetcyc, lastcyc))
                {
                    mssg = string.Format("将编辑库中项目{0}第{1}周期和第{2}周期的数据合并成第{3}成功", xmname, cyc, targetcyc, lastcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("将编辑库中项目{0}第{1}周期和第{2}周期的数据合并成第{3}失败", xmname, cyc, targetcyc, lastcyc);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("将编辑库中项目{0}第{1}周期和第{2}周期的数据合并成第{3}出错,错误信息:"+ex.Message, xmname, cyc, targetcyc, lastcyc);
                return false;
            }
        }
        public bool SurveyDataCouldUnion(string xmname, int cyc, int targetcyc,out string mssg)
        {
            try
            {
                if (dal.SurveyDataCouldUnion(xmname, cyc, targetcyc))
                {
                    mssg = string.Format("编辑库中项目{0}第{1}周期和第{2}周期存在相同点名，不符合合并条件", xmname, cyc, targetcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("编辑库中项目{0}第{1}周期和第{2}周期不存在相同点名，可以合并", xmname, cyc, targetcyc);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("检查编辑库中项目{0}第{1}周期和第{2}周期相同点名存在情况出错，错误信息:"+ex.Message, xmname, cyc, targetcyc);
                return false;
            }
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool AddData(string taskname, int cyc,out string mssg)
        {
            try
            {
                if (dal.AddData(taskname, cyc))
                {
                    mssg = string.Format("添加项目{0}周期{1}的数据进数据编辑表中成功",taskname,cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目{0}周期{1}的数据进数据编辑表中失败",taskname,cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目{0}周期{1}的数据进数据编辑表中出错,错误信息:"+ex.Message,taskname,cyc );
                return false;
            }
        }


        public bool CgFindSurveyOriginCyc(string taskname, out int cyc,out string mssg)
        {
            cyc = 0;
            try
            {
                if (dal.CgFindSurveyOriginCyc(taskname, out cyc))
                {
                    mssg = string.Format("获取到成果库中最大周期来自于编辑库中的{0}", cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("溯源成果库最大周期的编辑库周期失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("溯源成果库最大周期的编辑库周期出错,错误信息："+ex.Message);
                return false;
            }
        }


        public bool AddCgData(string xmname,  int cyc, int importcyc,out string mssg)
        {
            int cont = 0;
            try
            {
                if (dal.AddCgData(xmname, cyc, importcyc,out cont))
                {
                    mssg = string.Format("将项目{0}编辑库中第{1}周期的数据{3}条添加到成果库的第{2}周期成功", xmname, cyc, importcyc,cont);
                    return true;
                }
                else {
                    mssg = string.Format("将项目{0}编辑库中第{1}周期的数据添加到成果库的第{2}周期失败", xmname, cyc, importcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("将项目{0}编辑库中第{1}周期的数据添加到成果库的第{2}周期出错,错误信息:" + ex.Message, xmname, cyc, importcyc);
                return false;
            }
        }

        public bool LostPoints(int xmno, string xmname, int cyc, out string pointstr,out string mssg)
        {
            mssg = "";
            pointstr = "";
            try {
                if (dal.LostPoints(xmno, xmname, cyc, out pointstr))
                {
                   mssg = string.Format("项目{0}第{1}周期有{2}点缺失", xmname, cyc, pointstr);
                    return true;
                }
                else
                {
                   mssg = string.Format("获取项目{0}第{1}周期缺失点失败", xmname, cyc);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("获取项目{0}第{1}周期缺失点出错，错误信息:"+ex.Message, xmname, cyc);
                 return false;
                
            }
        }


		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TotalStation.Model.fmos_obj.cycdirnet model,out string mssg)
		{

            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("项目{0}点{1}周期{2}测量时间{3}结果数据更新成功",model.TaskName,model.POINT_NAME,model.CYC,model.Time);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}点{1}周期{2}测量时间{3}结果数据更新失败", model.TaskName, model.POINT_NAME, model.CYC, model.Time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}点{1}周期{2}测量时间{3}结果数据更新出错,错误信息:"+ex.Message, model.TaskName, model.POINT_NAME, model.CYC, model.Time);
                return false;
            }



			return dal.Update(model);
		}

        public bool UpdataSurveyData(TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            try
            {
                if (dal.UpdataSurveyData(model))
                {
                    mssg = string.Format("更新项目{0}第{1}周期数据成功", model.TaskName,model.CYC);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目{0}第{1}周期数据失败", model.TaskName, model.CYC);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目{0}第{1}周期数据出错,错误信息:"+ex.Message, model.TaskName, model.CYC);
                return false;
            }
        }
        public bool UpdataSurveyDataNextCYCThis(TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {

            try
            {
                if (dal.UpdataSurveyDataNextCYCThis(model))
                {
                    mssg = string.Format("更新项目{0}第{1}周期数据成功", model.TaskName, model.CYC+1);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目{0}第{1}周期数据失败", model.TaskName, model.CYC+1);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目{0}第{1}周期数据出错,错误信息:" + ex.Message, model.TaskName, model.CYC+1);
                return false;
            }
        }


        public bool SurveyPointCycDataAdd(string xmname, string pointname, int cyc, int importcyc,out string mssg)
        {
            try
            {
                if (dal.SurveyPointCycDataAdd(xmname, pointname, cyc, importcyc))
                {
                    mssg = string.Format("将测量库项目{0}点{1}周期{2}的数据添加为数据编辑表中第{3}周期的数据成功", xmname, pointname, cyc, importcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("将测量库项目{0}点{1}周期{2}的数据添加为数据编辑表中第{3}周期的数据失败", xmname, pointname, cyc, importcyc);
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                mssg = string.Format("将测量库项目{0}点{1}周期{2}的数据添加为数据编辑表中第{3}周期的数据出错,错误信息:"+ex.Message, xmname, pointname, cyc, importcyc);
                return false;
            }
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Model.fmos_obj.cycdirnet model,out string mssg)
		{
            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("删除项目{0}点{1}采集时间{2}的数据成功", model.TaskName, model.Time, model.POINT_NAME);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}点{1}采集时间{2}的数据失败", model.TaskName, model.Time, model.POINT_NAME);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}点{1}采集时间{2}的数据出错,错误信息:" + ex.Message, model.TaskName, model.Time, model.POINT_NAME);
                return false;
            }
		}
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteTmp(string TaskName,out string mssg)
        {
            try
            {
                if (dal.DeleteTmp(TaskName))
                {
                    mssg = string.Format("删除项目{0}临时表的数据成功", TaskName);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}临时表的数据失败", TaskName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}临时表的数据出错,错误信息："+ex.Message, TaskName);
                return false;
            }

        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteCgTmp(string TaskName, out string mssg)
        {
            try
            {
                if (dal.DeleteCgTmp(TaskName))
                {
                    mssg = string.Format("删除项目{0}成果临时表的数据成功", TaskName);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}成果临时表的数据失败", TaskName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}成果临时表的数据出错,错误信息：" + ex.Message, TaskName);
                return false;
            }

        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int startCyc, int endCyc, string xmname,string pointname, out string mssg)
        {
            try
            {
                if (dal.Delete(startCyc, endCyc, xmname, pointname))
                {
                    mssg = string.Format("删除项目{0}点名{1}第{2}周期-第{3}周期的数据成功", xmname,pointname ,startCyc,endCyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}点名{1}第{2}周期-第{3}周期的数据失败", xmname, pointname, startCyc, endCyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}第{1}第{2}周期-第{3}周期的数据出错，错误信息:" + ex.Message, xmname, pointname, startCyc, endCyc);
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteCg(int startCyc, int endCyc, string xmname, string pointname, out string mssg)
        {
            try
            {
                if (dal.DeleteCg(startCyc, endCyc, xmname, pointname))
                {
                    mssg = string.Format("删除项目{0}点名{1}第{2}周期-第{3}周期的成果数据成功", xmname, pointname, startCyc, endCyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}点名{1}第{2}周期-第{3}周期的成果数据失败", xmname, pointname, startCyc, endCyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}第{1}第{2}周期-第{3}周期的成果数据出错，错误信息:" + ex.Message, xmname, pointname, startCyc, endCyc);
                return false;
            }
        }


        /// <summary>
        /// 删除编辑库数据
        /// </summary>
        public bool DeleteSurveyData(int startCyc, int endCyc, string xmname, string pointname, out string mssg)
        {
            try
            {
                if (dal.DeleteSurveyData(startCyc, endCyc, xmname, pointname))
                {
                    mssg = string.Format("删除项目{0}编辑库中点名{1}第{2}周期-第{3}周期的数据成功", xmname, pointname, startCyc, endCyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目{0}编辑库中点名{1}第{2}周期-第{3}周期的数据失败", xmname, pointname, startCyc, endCyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目{0}编辑库中第{1}第{2}周期-第{3}周期的数据出错，错误信息:" + ex.Message, xmname, pointname, startCyc, endCyc);
                return false;
            }
        }

        public bool SurveyResultDataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.SurveyResultdataTableLoad(startCyc, endCyc, startPageIndex, pageSize, xmname, pointname, sord, out  dt))
                {
                    mssg = "结果数据编辑表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "结果数据编辑表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "结果数据编辑表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        

        public bool ResultDataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname, string sord,out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultdataTableLoad(startCyc, endCyc, startPageIndex, pageSize, xmname, pointname, sord,out  dt))
                {
                    mssg = "结果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "结果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool CgResultDataTableLoad(int startCyc, int endCyc, int startPageIndex, int pageSize, string xmname, string pointname, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.CgResultdataTableLoad(startCyc, endCyc, startPageIndex, pageSize, xmname, pointname, sord, out  dt))
                {
                    mssg = "成果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "成果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "成果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool ResultTableRowsCount(int startCyc, int endCyc, string xmname, string pointname,out string totalCont, out string mssg)
        {
            totalCont = "0";
            try
            {
                if (dal.ResultTableRowsCount(startCyc, endCyc, xmname,  pointname,out totalCont))
                {
                    mssg = "结果数据表记录数加载成功!";
                    return true;

                }
                else
                {
                    mssg = "结果数据表记录数加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "原始数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool ResultDataReportPrint(string sql, string xmname, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultDataReportPrint(sql, xmname, out dt))
                {
                    mssg = "结果数据报表数据表生成成功";
                    return true;
                }
                else
                {
                    mssg = "结果数据报表数据表生成失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                
                    mssg = "结果数据报表数据表生成出错，错误信息"+ex.Message;
                    return false;
                
            }
        }
        //根据项目名称获取端点周期
        public bool ExtremelyCycGet(string xmname, string DateFunction, out string mssg  ,out string ExtremelyCyc)
        {
            ExtremelyCyc = null;
            try
            {

                if (dal.ExtremelyCycGet(xmname, DateFunction, out ExtremelyCyc))
                {
                    mssg = "端点周期获取成功";
                    return true;
                }
                else 
                {
                    mssg = "端点周期获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "端点周期获取出错，错误信息："+ex.Message;
                return false;
            }

        }
        //根据项目名获取的周期
        public bool CycList(string xmname, out DataTable dt, out string mssg)
        {

            dt = null;
            try
            {

                if (dal.CycList(xmname, out dt))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }

        //根据项目名获取的周期
        public bool CycTimeList(string xmname, out List<string> cyctimelist, out string mssg)
        {

            cyctimelist = null;
            try
            {

                if (dal.CycTimeList(xmname, out cyctimelist))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }

        //根据编辑库中项目名获取的周期
        public bool SurveyCycList(string xmname, out List<string> ls, out string mssg)
        {

            ls = null;
            try
            {

                if (dal.SurveyCycList(xmname, out ls))
                {
                    mssg = "周期列表获取成功";
                    return true;
                }
                else
                {
                    mssg = "周期列表获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "周期列表获取出错，错误信息：" + ex.Message;
                return false;
            }
        }

        public bool MaxTime(string xmname, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.MaxTime(xmname, out maxTime))
                {
                    mssg = string.Format("获取项目{0}表面位移测量数据的最大日期{1}成功", xmname, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}表面位移测量数据的最大日期失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移测量数据的最大日期出错，错误信息:" + ex.Message, xmname);
                return false;
            }
        }

        /// <summary>
        /// 从起始和结束时间中获取周期
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="pointstr"></param>
        /// <param name="CycList"></param>
        /// <returns></returns>
        public bool GetCycFromDateTime(string xmname, string startTime, string endTime, string pointstr, out List<string> CycList,out string mssg)
        {
            CycList = null;
            mssg = "";
            try
            {
                if (dal.GetCycFromDateTime(xmname, startTime, endTime, pointstr, out CycList))
                {
                    mssg = "从日期中获取端点周期成功";
                    return true;
                }
                else
                {
                    mssg = "从日期中获取端点周期失败";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "从日期中获取端点周期出错，错误信息"+ex.Message;
                return false;
            }
           
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(string TaskName,out List<TotalStation.Model.fmos_obj.cycdirnet> modellist ,out string mssg)
        {
            List<TotalStation.Model.fmos_obj.cycdirnet> lt = null;
            modellist = null;
            try
            {
                if (dal.GetModelList(TaskName,out lt))
                {
                    mssg = string.Format("获取项目{0}临时表中的结果数据成功!", TaskName);
                    modellist = lt;
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}临时表中的结果数据失败!", TaskName);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}临时表中的结果数据出错!错误信息:" + ex.Message, TaskName);
                return false;
            }
           
        }

      

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetCgModelList(string TaskName, out List<TotalStation.Model.fmos_obj.cycdirnet> lt,out string mssg)
        {
            lt = null;
            try
            {
                if (dal.GetCgModelList(TaskName, out lt))
                {
                    mssg = string.Format("获取项目{0}临时表中的成果数据成功!", TaskName);
                    
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}临时表中的成果数据失败!", TaskName);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}临时表中的成果数据出错!错误信息:" + ex.Message, TaskName);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModelList(DataTable dt, out List<TotalStation.Model.fmos_obj.cycdirnet> lt, out string mssg)
        {
            lt = null;
            try
            {
                if (dal.GetModelList(dt, out lt))
                {
                    mssg = string.Format("将数据表{0}的{1}条记录转换为数据对象{2}条成功",dt.TableName,dt.Rows.Count,lt.Count);
                    
                    return true;
                }
                else
                {
                    mssg = string.Format("将数据表{0}的{1}条记录转换为数据对象失败", dt.TableName, dt.Rows.Count);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("将数据表{0}的{1}条记录转换为数据对象出错，错误信息:"+ex.Message, dt.TableName, dt.Rows.Count);
                return false;
            }

        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetTable(string xmname,List<TotalStation.Model.fmos_obj.cycdirnet> lt, out DataTable dt, out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.GetTable(xmname,lt,out dt))
                {
                    mssg = string.Format("将数据对象{0}条转换为数据表{1}条记录成功", lt.Count, dt.Rows.Count);

                    return true;
                }
                else
                {
                    mssg = string.Format("将数据对象{0}条记录转换为数据表失败", lt.Count);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("将数据对象的{0}条记录转换为数据对象出错，错误信息:" + ex.Message, lt.Count);
                return false;
            }

        }

        public bool XmLastDate(string xmname, out string dateTime,out string mssg)
        {
            dateTime = "";
            try
            {
                if (dal.XmLastDate(xmname, out dateTime))
                {
                    mssg = string.Format("获取项目{0}最大时间成功", xmname);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目{0}最大时间失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}最大时间出错，错误信息:"+ex.Message, xmname);
                return false;
            }

        }

        public bool GetModel(string TaskName, string pointname, int cyc, out TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(TaskName, pointname, cyc, out model))
                {
                    mssg = string.Format("获取{0}{1}点第{2}周期的测量数据成功", TaskName, pointname, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}点第{2}周期的测量数据失败", TaskName, pointname, cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}{1}点第{2}周期的测量数据出错,错误信息:" + ex.Message, TaskName, pointname, cyc);
                return false;
            }
        }

        public bool GetModel(string TaskName, string pointname, DateTime dt, out TotalStation.Model.fmos_obj.cycdirnet model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(TaskName, pointname, dt, out model))
                {
                    mssg = string.Format("获取{0}{1}点{2}的测量数据成功", TaskName, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}{1}点{2}的测量数据失败", TaskName, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}{1}点{2}的测量数据出错,错误信息:" + ex.Message, TaskName, pointname, dt);
                return false;
            }
        }

        /// <summary>
        /// 条件搜索测量数据
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointName"></param>
        /// <param name="cyc"></param>
        /// <param name="dt"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool ResultdataSearch(string xmname, string pointName,DateTime time, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultdataSearch(xmname, pointName,time,out  dt))
                {
                    mssg = "测量结果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "测量结果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "测量结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }


        /// <summary>
        /// 条件搜索测量数据
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointName"></param>
        /// <param name="timestart"></param>
        /// <param name="timeend"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool ResultdataSearch(string xmname, string pointName, DateTime timestart, DateTime timeend, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.ResultdataSearch(xmname, pointName, timestart,timeend, out  dt))
                {
                    mssg = "测量结果数据表加载成功!";
                    return true;

                }
                else
                {
                    mssg = "测量结果数据表加载失败!";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "测量结果数据表加载出错，出错信息" + ex.Message;
                return false;
            }
        }

        public bool Singlepointcycload(string xmname,string pointname,out DataTable dt,out string mssg)
        {

            dt = null;
            try
            {
                if (dal.Singlepointcycload(xmname, pointname, out dt))
                {
                    mssg = string.Format("获取项目{0}表面位移{1}的周期数{2}成功", xmname, pointname, dt.Rows.Count);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目{0}表面位移{1}的周期数{2}失败", xmname, pointname, dt.Rows.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移{1}的周期列表出错,错误信息:"+ex.Message, xmname, pointname);
                return false;
            }

        }

        public bool Singlecycdirnetdataload(string xmname, string pointname, int startcyc, int endcyc, out DataTable dt,out string mssg)
        {
            dt = null;
            try
            {
                if (dal.Singlecycdirnetdataload(xmname, pointname, startcyc, endcyc, out dt))
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点{2}-{3}周期的数据{4}条成功", xmname, pointname, startcyc, endcyc, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点{2}-{3}周期的数据失败", xmname, pointname, startcyc, endcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移{1}点{2}-{3}周期的数据出错,错误信息:"+ex.Message, xmname, pointname, startcyc, endcyc);
                return false;
            }
            
        }

        public bool TotalStationPointLoadBLL(int xmno, out List<string> ls, out string mssg)
        {
            ls = null;
            try
            {
                if (dal.TotalStationPointLoadDAL(xmno, out ls))
                {
                    mssg = "全站仪点号加载成功";
                    return true;

                }
                else
                {
                    mssg = "全站仪点号加载失败";
                    return true;
                }

            }
            catch (Exception ex)
            {
                mssg = "全站仪点号加载出错，错误信息" + ex.Message;
                return true;
            }

        }

        public bool PointNameCycListGet(string xmname, string pointname, out List<string> ls,out string mssg)
        {
            ls = new List<string>();
            try
            {
                if (dal.PointNameCycListGet(xmname, pointname, out ls))
                {
                    mssg = string.Format("获取项目{0}点名{1}的周期列表数量{2}成功", xmname, pointname, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}点名{1}的周期列表失败", xmname, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}点名{1}的周期列表出错,错误信息:"+ex.Message, xmname, pointname);
                return false;
            }

        }

        public bool PointNewestDateTimeGet(string xmname, string pointname, out DateTime dt,out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(xmname, pointname, out dt))
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点的最新数据的测量时间{2}成功", xmname, pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}表面位移{1}点的最新数据的测量时间失败", xmname, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}表面位移{1}点的最新数据的测量时间出错，错误信息:"+ex.Message, xmname, pointname);
                return false;
            }

        }

        public bool XmStateTable(int startPageIndex, int pageSize, int xmno, string xmname, string unitname, string colName, string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.XmStateTable(startPageIndex, pageSize, xmno, xmname, unitname, colName, sord, out  dt))
                {
                    mssg = string.Format("获取编号为{0}项目名{1}的全站仪项目状态表记录数{2}条成功", xmno, xmname, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取编号为{0}项目名{1}的全站仪项目状态表失败", xmno, xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取编号为{0}项目名{1}的全站仪项目状态表出错,错误信息:"+ex.Message, xmno, xmname);
                return false;
            }
        }


        /// <summary>
        /// 是否插入数据
        /// </summary>
        /// <returns></returns>
        public bool IsInsertData(string xmname, DateTime dt,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.IsInsertData(xmname, dt))
                {
                    mssg = string.Format("经查库发现项目{0}{1}采集的数据是接段数据", xmname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("经查库发现项目{0}{1}采集的数据是最新数据", xmname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}{1}采集的数据的性质出错，错误信息："+ex.Message, xmname, dt);
                return false;
            }
        }
        /// <summary>
        /// 插入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool InsertDataCycGet(string xmname, DateTime dt,out int cyc,out string mssg)
        {
            mssg = "";
            cyc = -1;
            try
            {
                if (dal.InsertDataCycGet(xmname, dt,out cyc))
                {
                    mssg = string.Format("获取项目编号{0}的段接周期为{1}", xmname, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的段接周期失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的段接周期出错,错误信息:"+ex.Message, xmname);
                return false;
            }
        }
        /// <summary>
        /// 插入数据周期后移
        /// </summary>
        /// <returns></returns>
        public bool InsertCycStep(string xmname, int startcyc,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.InsertCycStep(xmname, startcyc))
                {
                    mssg = string.Format("项目{0}周期{1}后的所有周期后移成功", xmname, startcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}周期{1}后的所有周期后移失败", xmname, startcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}周期{1}后的所有周期后移出错,错误信息:"+ex.Message, xmname, startcyc);
                return false;
            }
        }

        /// <summary>
        /// 删除周期前移
        /// </summary>
        /// <returns></returns>
        public bool DeleteCgDataCycOnChain(string xmname, int startcyc, out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.DeleteCgDataCycOnChain(xmname, startcyc))
                {
                    mssg = string.Format("项目{0}周期{1}后的所有周期前移成功", xmname, startcyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}周期{1}后的所有周期前移失败", xmname, startcyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}周期{1}后的所有周期前移出错,错误信息:" + ex.Message, xmname, startcyc);
                return false;
            }
        }



        /// <summary>
        /// 插入的周期是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="cyc"></param>
        /// <returns></returns>
        public bool IsInsertCycExist(string xmname, int cyc,out string mssg)
        {
            try
            {
                if (dal.IsInsertCycExist(xmname, cyc))
                {
                    mssg = string.Format("项目{0}已经存在周期{1}", xmname, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}不存在周期{1}", xmname, cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}周期{1}的存在与否出错,错误信息:"+ex.Message, xmname, cyc);
                return false;
            }
        }
        /// <summary>
        /// 判断记录是否存在
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="pointname"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool IsCycdirnetDataExist(string xmname, string pointname, DateTime dt,out int cyc,out string mssg)
        {
            cyc = -1;
            try
            {
                if (dal.IsCycdirnetDataExist(xmname, pointname, dt,out cyc))
                {
                    mssg = string.Format("获取到项目{0}点名{1}采集时间{2}的周期为{3}", xmname, pointname, dt,cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("不存在项目{0}点名{1}采集时间{2}的记录", xmname, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}点名{1}采集时间{2}的记录出错,错误信息:"+ex.Message, xmname, pointname, dt);
                return false;
            } 
            
        }

        public bool CycModelList(string xmname, int cyc, out List<TotalStation.Model.fmos_obj.cycdirnet> cycdirnetModelList,out string mssg)
        {
            mssg = "";
            cycdirnetModelList = null;
            try
            {
                if (dal.CycModelList(xmname, cyc, out cycdirnetModelList))
                {
                    mssg = string.Format("获取项目{0}第{1}周期数据{2}条成功", xmname, cyc, cycdirnetModelList.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}第{1}周期数据失败", xmname, cyc, cycdirnetModelList.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}第{1}周期数据出错，错误信息:"+ex.Message, xmname, cyc);
                return false;
            }
        }
        public bool CgCycList(string xmname, out List<string> ls,out string mssg)
        {
            mssg = "";
            ls = null;
            try
            {
                if (dal.CgCycList(xmname, out ls))
                {
                    mssg = string.Format("获取到项目{0}成果数据表中周期列表为{1}", xmname, string.Join(",", ls));
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目{0}成果数据表中周期列表失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目{0}成果数据表中周期列表出错,错误信息："+ex.Message, xmname);
                return false;
            }

        }

        public bool CycdirnetDateTimeCycGet(string xmname, DateTime dt, out int cyc,out string mssg)
        {
            cyc = -1;
            try
            {
                if (dal.CycdirnetDateTimeCycGet(xmname,dt, out cyc))
                {
                    mssg = string.Format("成功获取测量库项目{0}时间{1}的周期为{2}",xmname,dt,cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取测量库项目{0}时间{1}的周期失败", xmname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取测量库项目{0}时间{1}的周期出错,错误信息：" + ex.Message, xmname, dt);
                return false;
            }
        }
        public bool CgCycdirnetDateTimeCycGet(string xmname,  DateTime dt, out int cyc, out string mssg)
        {
            cyc = -1;
            try
            {
                if (dal.CgCycdirnetDateTimeCycGet(xmname,  dt, out cyc))
                {
                    mssg = string.Format("成功获取成果库项目{0}时间{1}的周期为{2}", xmname, dt, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取成果库项目{0}时间{1}的周期失败", xmname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取成果库项目{0}时间{1}的周期出错,错误信息：" + ex.Message, xmname, dt);
                return false;
            }
        }

        public bool IsPointsDataExistInThisCyc(string xmname, List<string> points, int cyc, out string mssg)
        {
            try
            {
                if (dal.IsPointsDataExistInThisCyc(xmname, points, cyc))
                {
                    mssg = string.Format("存在项目{0}点名{1}第周期{2}的数据记录", xmname, points, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("不存在项目{0}点名{1}第周期{2}的数据记录", xmname, points, cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}点名{1}第周期{2}的数据记录出错,错误信息：" + ex.Message, xmname, points, cyc);
                return false;
            }
        }
        /// <summary>
        /// 接入数据的周期生成
        /// </summary>
        /// <returns></returns>
        public bool SiblingDataCycGet(string xmname, DateTime dt,List<string> points,double mobileStationInteval ,out  int cyc,int n ,out string mssg)
        {
            mssg = "";
            cyc = -1;
            try
            {
                if (dal.SiblingDataCycGet(xmname, dt, points, mobileStationInteval, out cyc))
                {
                    mssg = string.Format("获取项目编号{0}的接入周期为{1}", xmname, cyc);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的接入周期失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                
                mssg = string.Format("获取项目编号{0}的接入周期出错,错误信息:" + ex.Message, xmname);
                ExceptionLog.ExceptionWrite("发现查询资源共享");
                //System.Threading.Thread.Sleep(1000);
                return n > 0 ? false : SiblingDataCycGet(xmname, dt, points, mobileStationInteval, out cyc, 1, out mssg);
                
            }
        }
        public bool OnLinePointCont(string unitname, List<string> finishedxmnolist, out string contpercent, out string mssg)
        {
            mssg = "";
            contpercent = "";
            try
            {
                if (dal.OnLinePointCont(unitname, finishedxmnolist, out contpercent))
                {
                    mssg = string.Format("成功获取表面位移在线点情况为{0}", contpercent);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取表面位移在线点情况失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取表面位移在线点情况出错,错误信息:"+ex.Message);
                return false;
            }
        }

        public bool ReportDataView(List<string> cyclist, int startPageIndex, int pageSize, string xmname,  string sord, out DataTable dt,out string mssg)
        {
            dt = null;
            mssg = "";
            try
            {
                if (dal.ReportDataView(cyclist, startPageIndex, pageSize, xmname,  sord, out  dt))
                {
                    mssg = string.Format("获取项目{0}以{1}生成的周期报表成功", xmname, string.Join("、",cyclist));
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}以{1}生成的周期报表失败", xmname, string.Join("、", cyclist));
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}以{1}生成的周期报表出错，错误信息:" + ex.Message, xmname, string.Join("、", cyclist));
                return false;
            }
        }

        public bool GetSurveyDataLackPointsModellistFromCgData(int xmno, List<string> pointlist, out List<Model.fmos_obj.cycdirnet> modellist, out string mssg)
        {
            modellist = null;
            try
            {
                if (dal.GetSurveyDataLackPointsModellistFromCgData(xmno, pointlist, out modellist))
                {
                    mssg = string.Format("从成果库中获取项目编号{0}编辑库缺失点{1}的数据参考对象数量{2}成功", xmno, string.Join("','", pointlist), modellist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("从成果库中获取项目编号{0}编辑库缺失点{1}的数据参考对象数量{2}失败", xmno, string.Join("','", pointlist), modellist.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("从成果库中获取项目编号{0}编辑库缺失点{1}的数据参考对象出错,错误信息：" + ex.Message, xmno, string.Join("','", pointlist));
                return false;
            }

        }


        public bool GetCgTimeSortModellist(int xmno, out List<Model.fmos_obj.cycdirnet> modellist, out string mssg)
        {
            modellist = null;
            try
            {
                ExceptionLog.ExceptionWrite("现在"+string.Format("从成果库中获取项目编号{0}点名时间列表", xmno));
                if (dal.GetCgTimeSortModellist(xmno, out modellist))
                {
                    mssg = string.Format("从1成果库中获取项目编号{0}点名时间列表数量{1}", xmno, modellist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("从1成果库中获取项目编号{0}点名时间列表失败", xmno, modellist.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("从1成果库中获取项目编号{0}点名时间列表出错,错误信息:"+ex.Message, xmno);
                return false;
            }

        }

        public bool GetSurveyCYCModellist(int xmno, int cyc,out List<Model.fmos_obj.cycdirnet> modellist, out string mssg)
        {
            modellist = null;
            try
            {
                if (dal.GetSurveyCYCModellist(xmno,cyc ,out modellist))
                {
                    mssg = string.Format("从编辑库中获取项目编号{0}周期{1}数据列表数量{2}", xmno,cyc, modellist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("从编辑库中获取项目编号{0}周期{1}数据列表失败", xmno, cyc);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("从编辑库中获取项目编号{0}周期{1}数据列表出错,错误信息:" + ex.Message, xmno,cyc);
                return false;
            }

        }

        


        public bool SurveyDataCycTimeGet(int xmno, int cyc, out DateTime time,out string mssg)
        {
            time = new DateTime();
            try
            {
                if (dal.SurveyDataCycTimeGet(xmno, cyc, out time))
                {
                    mssg = string.Format("获取到项目编号{0}周期{1}的最早测量时间{2}成功", xmno, cyc, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}周期{1}的最早测量时间{2}失败", xmno, cyc, time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}周期{1}的最早测量时间{2}出错,错误信息:"+ex.Message, xmno, cyc, time);
                return false;
            }
        }




		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

