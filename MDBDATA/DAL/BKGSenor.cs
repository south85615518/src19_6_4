﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SqlHelpers;

namespace MDBDATA.DAL
{
    public class BKGSenor
    {
        
        public bool BKGSenorLoad(string xmname,out DataTable dt)
        {
            dt = null;
            return false;
        }
        #region 应力
        //应力点号加载
        public bool  BKGMcuSenorPointLoad(string xmname,out List<string> pointnameList)
        {
            pointnameList = new List<string>();

            string sql = "select sname from sensorset,mcuset where sensorset.mid = mcuset.id  and mcuset.mname = '"+xmname+"' and st1='应力' ";
            pointnameList = querybkgsql.queryaccesslist(sql);


            return true;
        }
        public bool BKGMcuMaxTime(string xmname, out DateTime maxTime)
        {
            maxTime = new DateTime();
            StringBuilder strSql = new StringBuilder();
            string sql = "select max(dt) from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '"+xmname+"' and st1='应力' ";
            string maxtime = querybkgsql.queryaccessdbstring(sql);
            if (maxtime == "") return false;
            maxTime = Convert.ToDateTime(maxtime);
            return true;
        }

        #endregion
        #region 测斜仪
        //测斜仪
        public bool BKGInclinometerSenorPointLoad(string xmname, out List<string> pointnameList)
        {
            pointnameList = new List<string>();

            string sql = "select sname from sensorset,mcuset where sensorset.mid = mcuset.id  and mcuset.mname = '" + xmname + "' and st1='位移/角度' ";
            pointnameList = querybkgsql.queryaccesslist(sql);


            return true;
        }

        public bool BKGMcuAngleMaxTime(string xmname, out DateTime maxTime)
        {
            maxTime = new DateTime();
            StringBuilder strSql = new StringBuilder();
            string sql = "select max(dt) from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='位移/角度' ";
            string maxtime = querybkgsql.queryaccessdbstring(sql);
            if (maxtime == "") return false;
            maxTime = Convert.ToDateTime(maxtime);
            return true;
        }


        #endregion
    }
}
