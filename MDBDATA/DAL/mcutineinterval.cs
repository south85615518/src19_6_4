﻿/**  版本信息模板在安装目录下，可自行修改。
* bkgtimeinterval.cs
*
* 功 能： N/A
* 类 名： bkgtimeinterval
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/3/14 15:26:26   N/A    初版
*
* Copyright (c) 2012 MDBDATA Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.OleDb;
using System.Data.Odbc;
using SqlHelpers;
//Please add references
namespace MDBDATA.DAL
{
	/// <summary>
	/// 数据访问类:bkgtimeinterval
	/// </summary>
	public partial class mcutineinterval
	{
		public mcutineinterval()
		{}
		#region  BasicMethod
        public static database db = new database();


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(MDBDATA.Model.bkgtimeinterval model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("replace into bkgtimeinterval(");
			strSql.Append("pointname,hour,minute,xmno)");
			strSql.Append(" values (");
			strSql.Append("@pointname,@hour,@minute,@xmno)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@pointname", OdbcType.VarChar,100),
					new OdbcParameter("@hour", OdbcType.Int,11),
					new OdbcParameter("@minute", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.Int,11)};
			parameters[0].Value = model.pointname;
			parameters[1].Value = model.hour;
			parameters[2].Value = model.minute;
			parameters[3].Value = model.xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        //public bool Update(MDBDATA.Model.bkgtimeinterval model)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("update bkgtimeinterval set ");
        //    strSql.Append("pointname=@pointname,");
        //    strSql.Append("hour=@hour,");
        //    strSql.Append("minute=@minute,");
        //    strSql.Append("xmno=@xmno");
        //    strSql.Append(" where ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@pointname", OdbcType.VarChar,100),
        //            new OdbcParameter("@hour", OdbcType.Int,11),
        //            new OdbcParameter("@minute", OdbcType.Int,11),
        //            new OdbcParameter("@xmno", OdbcType.Int,11)};
        //    parameters[0].Value = model.pointname;
        //    parameters[1].Value = model.hour;
        //    parameters[2].Value = model.minute;
        //    parameters[3].Value = model.xmno;

        //    int rows=DbHelperOleDb.ExecuteSql(strSql.ToString(),parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        //public bool Delete()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from bkgtimeinterval ");
        //    strSql.Append(" where ");
        //    OdbcParameter[] parameters = {
        //    };

        //    int rows=DbHelperOleDb.ExecuteSql(strSql.ToString(),parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        //public MDBDATA.Model.bkgtimeinterval GetModel()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select pointname,hour,minute,xmno from bkgtimeinterval ");
        //    strSql.Append(" where ");
        //    OdbcParameter[] parameters = {
        //    };

        //    MDBDATA.Model.bkgtimeinterval model=new MDBDATA.Model.bkgtimeinterval();
        //    DataSet ds=DbHelperOleDb.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        return DataRowToModel(ds.Tables[0].Rows[0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public MDBDATA.Model.bkgtimeinterval DataRowToModel(DataRow row)
		{
			MDBDATA.Model.bkgtimeinterval model=new MDBDATA.Model.bkgtimeinterval();
			if (row != null)
			{
				if(row["pointname"]!=null)
				{
					model.pointname=row["pointname"].ToString();
				}
				if(row["hour"]!=null && row["hour"].ToString()!="")
				{
					model.hour=int.Parse(row["hour"].ToString());
				}
				if(row["minute"]!=null && row["minute"].ToString()!="")
				{
					model.minute=int.Parse(row["minute"].ToString());
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select pointname,hour,minute,xmno ");
        //    strSql.Append(" FROM bkgtimeinterval ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    return DbHelperOleDb.Query(strSql.ToString());
        //}

		/// <summary>
		/// 获取记录总数
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM bkgtimeinterval ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T. desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from bkgtimeinterval T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return DbHelperOleDb.Query(strSql.ToString());
        //}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "bkgtimeinterval";
			parameters[1].Value = "";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperOleDb.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string sord, out DataTable dt)
        {
            //string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select mcupointalarmvalue.point_name,bkgtimeinterval.applicated,bkgtimeinterval.`hour`,bkgtimeinterval.`minute` from mcupointalarmvalue left join bkgtimeinterval on mcupointalarmvalue.point_name = bkgtimeinterval.pointname and mcupointalarmvalue.xmno = bkgtimeinterval.xmno where mcupointalarmvalue.xmno={2} order by point_name limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, xmno);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

