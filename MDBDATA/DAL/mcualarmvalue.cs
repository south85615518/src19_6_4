﻿/**  版本信息模板在安装目录下，可自行修改。
* mcualarmvalue.cs
*
* 功 能： N/A
* 类 名： mcualarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:27   N/A    初版
*
* Copyright (c) 2012 MDBDATA Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;
//Please add references
namespace MDBDATA.DAL
{
	/// <summary>
	/// 数据访问类:mcualarmvalue
	/// </summary>
	public partial class mcualarmvalue
	{
       public static database db = new database();
	   public mcualarmvalue()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
        //public int GetMaxId()
        //{
        //return OdbcSQLHelper.GetMaxID("xmno", "mcualarmvalue"); 
        //}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        //public bool Exists(string alarmname,int xmno)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from mcualarmvalue");
        //    strSql.Append(" where alarmname=@alarmname and xmno=@xmno ");
        //    OdbcParameter[] parameters = {
        //            new OdbcParameter("@alarmname", OdbcType.VarChar,100),
        //            new OdbcParameter("@xmno", OdbcType.Int,11)			};
        //    parameters[0].Value = alarmname;
        //    parameters[1].Value = xmno;
           
        //    return OdbcSQLHelper.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(MDBDATA.Model.mcualarmvalue model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("insert into mcualarmvalue(");
            strSql.Append("name,deep,deepL,temperlate,temperlateL,xmno)");
			strSql.Append(" values (");
            strSql.Append("@name,@deep,@deepL,@temperlate,@temperlateL,@xmno)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@deep", OdbcType.Double),
                    new OdbcParameter("@deepL", OdbcType.Double),
                    new OdbcParameter("@temperlate", OdbcType.Double),
                    new OdbcParameter("@temperlateL", OdbcType.Double),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };
			parameters[0].Value = model.name;
			parameters[1].Value = model.deep;
            parameters[2].Value = model.deepL;
            parameters[3].Value = model.temperlate;
            parameters[4].Value = model.temperlateL;
            parameters[5].Value = model.xmno;
			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(MDBDATA.Model.mcualarmvalue model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(model.xmno);
			strSql.Append("update mcualarmvalue set ");
            strSql.Append("deep=@deep,");
            strSql.Append("temperlate=@temperlate,");
            strSql.Append("deepL=@deepL,");
            strSql.Append("temperlateL=@temperlateL");
			strSql.Append("     where     name=@name     and      xmno=@xmno   ");
			OdbcParameter[] parameters = {
                    
					new OdbcParameter("@deep", OdbcType.Double),
                    new OdbcParameter("@temperlate", OdbcType.VarChar,100),
                    new OdbcParameter("@deepL", OdbcType.Double),
                    new OdbcParameter("@temperlateL", OdbcType.VarChar,100),
                    new OdbcParameter("@name", OdbcType.VarChar,100),
                    new OdbcParameter("@xmno", OdbcType.Int)
					};
			parameters[0].Value = model.deep;
            parameters[1].Value = model.temperlate;
            parameters[2].Value = model.deepL;
            parameters[3].Value = model.temperlateL;
            parameters[4].Value = model.name;
            parameters[5].Value = model.xmno;
           

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string alarmname,int xmno)
		{

            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from mcualarmvalue ");
			strSql.Append("     where     name=@name    and    xmno=@xmno  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
			parameters[0].Value = alarmname;
			parameters[1].Value = xmno;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
			if (rows > 0)
			{
                PointAlarmValueDelCasc(alarmname,xmno);
				return true;
			}
			else
			{
				return false;
			}
		}
        /// <summary>
        /// 级联删除点名的预警参数
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="xmname"></param>
        /// <returns></returns>
        public bool PointAlarmValueDelCasc(string alarmname, int xmno)
        {
            List<string> ls = new List<string>();
            //?用存储过程是否会更加简便
            //一级预警
            ls.Add("update mcupointalarmvalue set firstAlarm='' where xmno=@xmno and firstAlarm=@firstAlarm");
            //二级预警
            ls.Add("update mcupointalarmvalue set secAlarm='' where xmno=@xmno and secAlarm=@secAlarm");
            //三级预警
            ls.Add("update mcupointalarmvalue set thirdAlarm='' where xmno=@xmno and thirdAlarm=@thirdAlarm");
            List<string> cascSql = ls;
            OdbcConnection conn = db.GetStanderConn(xmno);
            foreach (string sql in cascSql)
            {

                string temp = sql.Replace("@xmno", "'" + xmno + "'");
                temp = temp.Replace("@firstAlarm", "'" + alarmname + "'");
                temp = temp.Replace("@secAlarm", "'" + alarmname + "'");
                temp = temp.Replace("@thirdAlarm", "'" + alarmname + "'");

                updatedb udb = new updatedb();
                udb.UpdateStanderDB(temp, conn);


            }
            return true;
        }




        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = string.Format("select name,deep,deepL,temperlate,temperlateL,xmno  from  mcualarmvalue    where      xmno =  '" + xmno + "'    {2} limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(1) from mcualarmvalue where xmno = '" + xmno + "'";
            if (searchstring != null&& searchstring.Trim() != "1=1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;
            
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public MDBDATA.Model.mcualarmvalue GetModel(string name,int xmno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select name,deep,temperlate,xmno from mcualarmvalue ");
			strSql.Append(" where        name=@name       and         xmno=@xmno  ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@name", OdbcType.VarChar,100),
					new OdbcParameter("@xmno", OdbcType.Int,11)			};
			parameters[0].Value = name;
			parameters[1].Value = xmno;

			MDBDATA.Model.mcualarmvalue model=new MDBDATA.Model.mcualarmvalue();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

        /// <summary>
        /// 获取预警参数名
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="alarmValueNameStr"></param>
        /// <returns></returns>
        public bool AlarmValueNameGet(int xmno, out string alarmValueNameStr)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select distinct(name) from mcualarmvalue where xmno = "+xmno+"";
            List<string> ls = querysql.querystanderlist(sql, conn);
            List<string> lsFormat = new List<string>();
            foreach (string name in ls)
            {
                lsFormat.Add(name + ":" + name);
            }
            alarmValueNameStr = string.Join(";", lsFormat);
            return true;
        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public MDBDATA.Model.mcualarmvalue DataRowToModel(DataRow row)
		{
			MDBDATA.Model.mcualarmvalue model=new MDBDATA.Model.mcualarmvalue();
			if (row != null)
			{
				if(row["name"]!=null)
				{
					model.name=row["name"].ToString();
				}
                if (row["deep"] != null && row["deep"].ToString() != "")
                {
                    model.deep = Convert.ToDouble(Convert.ToDouble(row["deep"]).ToString("0.000"));
                }
                if (row["temperlate"] != null && row["temperlate"].ToString() != "")
                {
                    model.temperlate = Convert.ToDouble(Convert.ToDouble(row["temperlate"]).ToString("0.000"));
                }
                if (row["deepL"] != null && row["deepL"].ToString() != "")
                {
                    model.deepL = Convert.ToDouble(Convert.ToDouble(row["deepL"]).ToString("0.000"));
                }
                if (row["temperlateL"] != null && row["temperlateL"].ToString() != "")
                {
                    model.temperlateL = Convert.ToDouble(Convert.ToDouble(row["temperlateL"]).ToString("0.000"));
                }
					//model.deep=row["deep"].ToString();
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
                
                
			}
			return model;
		}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string name, int xmno, out MDBDATA.Model.mcualarmvalue model)
        {
            model = new Model.mcualarmvalue();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            strSql.Append("select name,deep,deepL,temperlate,temperlateL,xmno from mcualarmvalue ");
            strSql.Append(" where        name=@name       and         xmno=@xmno  "); 
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@name", OdbcType.VarChar,100),
			        new OdbcParameter("@xmno", OdbcType.Int)
                                         };
            parameters[0].Value = name;
            parameters[1].Value = xmno;
            //InclimeterDAL.Model.inclinometer_alarmvalue model=new InclimeterDAL.Model.inclinometer_alarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


		/// <summary>
		/// 获得数据列表
		/// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select alarmname,deep,xmno,id ");
        //    strSql.Append(" FROM mcualarmvalue ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    return OdbcSQLHelper.Query(strSql.ToString());
        //}

		/// <summary>
		/// 获取记录总数
		/// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM mcualarmvalue ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.xmno desc");
			}
			strSql.Append(")AS Row, T.*  from mcualarmvalue T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return OdbcSQLHelper.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OdbcParameter[] parameters = {
					new OdbcParameter("@tblName", OdbcType.VarChar, 255),
					new OdbcParameter("@fldName", OdbcType.VarChar, 255),
					new OdbcParameter("@PageSize", OdbcType.Int),
					new OdbcParameter("@PageIndex", OdbcType.Int),
					new OdbcParameter("@IsReCount", OdbcType.Bit),
					new OdbcParameter("@OrderType", OdbcType.Bit),
					new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
					};
			parameters[0].Value = "mcualarmvalue";
			parameters[1].Value = "xmno";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return OdbcSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

