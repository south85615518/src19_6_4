﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
using System.Data.OleDb;

namespace MDBDATA.DAL
{
    public class mcuangledata
    {

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(string xmname, List<string> pointnamelist, out List<MDBDATA.Model.mcuangledata> li)
        {
            li = new List<MDBDATA.Model.mcuangledata>();

            //StringBuilder strSql = new StringBuilder();
            //OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            //strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
            //strSql.Append(" FROM dtudata_tmp where xmno = @xmno ");
            string sql = "select sname,r1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='位移/角度'   and acc =1  and  sname in ('" + string.Join("','", pointnamelist) + "')  order by dt desc ,sname desc";
            //OdbcParameter[] parameters = {

            //        new OdbcParameter("@xmno", OdbcType.Int,11)};

            //parameters[0].Value = xmno;
            DataSet ds = OleDbSQLBKGHelper.Query(sql, "T_DATAMEAS");
            //DataTable dt  = querybkgsql.
            if (ds == null) return false;
            int i = 0;

            List<string> pointhashtable = new List<string>();
            while (i < ds.Tables[0].Rows.Count && i < pointnamelist.Count)
            {
                if (!pointhashtable.Contains(ds.Tables[0].Rows[i]["sname"]))
                    pointhashtable.Add(ds.Tables[0].Rows[i]["sname"].ToString());
                else
                    break;
                li.Add(DataRowToReportModel(ds.Tables[0].Rows[i]));
                i++;

            }
            return true;
        }
        public bool GetAlarmTableCont(string xmname, List<string> pointnamelist, out int cont)
        {
            cont = 0;

            //StringBuilder strSql = new StringBuilder();
            //OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            //strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
            //strSql.Append(" FROM dtudata_tmp where xmno = @xmno ");
            string sql = "select count(1) from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='位移/角度' and sname in ('" + string.Join("','", pointnamelist) + "')  ";
            //OdbcParameter[] parameters = {

            //        new OdbcParameter("@xmno", OdbcType.Int,11)};

            //parameters[0].Value = xmno;
            string contstr = querybkgsql.queryaccessdbstring(sql);
            //DataTable dt  = querybkgsql.
            cont = int.Parse(contstr);
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public MDBDATA.Model.mcuangledata DataRowToReportModel(DataRow row)
        {
            MDBDATA.Model.mcuangledata model = new MDBDATA.Model.mcuangledata();
            if (row != null)
            {
                if (row["sname"] != null)
                {
                    model.point_name = row["sname"].ToString();
                }
                //model.holedepth=row["holedepth"].ToString();
                //model.deep=row["deep"].ToString();
                //model.predeep=row["predeep"].ToString();
                if (row["r1"] != null && row["r1"].ToString() != "")
                {
                    model.disp = double.Parse(row["r1"].ToString());
                }
                if (row["r2"] != null && row["r2"].ToString() != "")
                {
                    model.scalevalue = double.Parse(row["r2"].ToString());
                }
                if (row["dt"] != null && row["dt"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["dt"].ToString());
                }

            }
            return model;
        }
        public bool Delete(string xmname, string pointname, DateTime dt)
        {

            string sql = "delete   from t_datameas where t_datameas.pointid  in (select  sensorset.id  from sensorset,mcuset where  mcuset.id =  sensorset.mid and mcuset.mname = '" + xmname + "' and st1='位移/角度' and sname ='" + pointname + "' )  and format(t_datameas.dt,'yyyy/mm/dd HH:mm') = format('" + dt + "','yyyy/mm/dd HH:mm')";
            ExceptionLog.DTUPortInspectionWrite(sql);
            int rows = OleDbSQLBKGHelper.ExecuteNonQuery(CommandType.Text, sql);
            if (rows > 0) return true;
            return false;
        }

        public bool MCUAngleDataAcc(int pid, DateTime time, double R2)
        {
            if (time.Second == 59) time = time.AddSeconds(1);
            StringBuilder strsql = new StringBuilder(256);
            strsql.AppendFormat("update t_datameas set r1 = {0},acc = 1 where pointid ={1} and format(dt,'yyyy/mm/dd HH:mm')=format('{2}','yyyy/mm/dd HH:mm') ", R2, pid, time);
            int rows = OleDbSQLBKGHelper.ExecuteNonQuery(CommandType.Text, strsql.ToString());
            if (rows > 0) return true;
            return false;

        }
        public bool PointNewestDateTimeGet(string xmname, string pointname, out DateTime dt)
        {
            //OdbcSQLHelper.Conn = db.GetStanderConn(xmno);

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select max(dt) from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='位移/角度' and sname ='" + pointname + "'  ");
            object obj = OleDbSQLBKGHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }
        public bool GetModel(string xmname, string pointname, DateTime dt, out    MDBDATA.Model.mcuangledata model)
        {
            model = null;
            //OdbcConnection conn = db.GetStanderConn(xmno);
            //bcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select sname,r1,r2,dt from mcuset,sensorset,t_datameas where mcuset.id = sensorset.mid and sensorset.id = t_datameas.pointid  and mcuset.mname = '" + xmname + "' and st1='位移/角度' and sname ='" + pointname + "' and format(t_datameas.dt,'yyyy/mm/dd HH:mm') = format('" + dt + "','yyyy/mm/dd HH:mm')  order by dt desc ,sname desc ");
            //strSql.Append(" FROM   senor_data   where xmno =   @xmno    and     point_name =  @point_name    and   time = @time   ");

            DataSet ds = OleDbSQLBKGHelper.Query(strSql.ToString(), "t_datameas");
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToReportModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }
    }
}
