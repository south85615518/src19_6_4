﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlHelpers;
using System.Data.OleDb;
using Tool;
using System.Data;
using MDBDATA.Model;
using System.Data.Odbc;

namespace MDBDATA.DAL
{
    public class mcupointset
    {
        public bool Add(MDBDATA.Model.mcupointalarmvalue model)
        {
            string sql = "insert into mcupointset (pointname,holedepth) values('"+model.point_name+"','"+model.holedepth+"')";
            int rows = OleDbSQLBKGHelper.ExecuteNonQuery(CommandType.Text,sql);
            return rows >= 0 ? true : false;
        }
        public bool Update(MDBDATA.Model.mcupointalarmvalue model)
        {
            string sql = "update mcupointset set holedepth='" + model.holedepth + "' where pointname='"+model.point_name+"'";
            int rows = OleDbSQLBKGHelper.ExecuteNonQuery(CommandType.Text, sql);
            return rows >= 0 ? true : false;
        }
        public bool Delete(MDBDATA.Model.mcupointalarmvalue model)
        {
            string sql = "delete from mcupointset  where pointname='" + model.point_name + "'";
            int rows = OleDbSQLBKGHelper.ExecuteNonQuery(CommandType.Text, sql);
            return rows >= 0 ? true : false;
        }
    }
}
