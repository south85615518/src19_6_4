﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MDBDATA.BLL
{
    public class mcupointset
    {
        public static MDBDATA.DAL.mcupointset dal = new DAL.mcupointset();
        public bool Add(MDBDATA.Model.mcupointalarmvalue model,out string mssg)
        {
            
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加浸润线点{0}的参数成功", model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加浸润线点{0}的参数失败", model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加浸润线点{0}的参数出错，错误信息:"+ex.Message, model.point_name);
                return false;
            }
        }

        public bool Update(MDBDATA.Model.mcupointalarmvalue model, out string mssg)
        {

            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("更新浸润线点{0}的参数成功", model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新浸润线点{0}的参数失败", model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新浸润线点{0}的参数出错，错误信息:" + ex.Message, model.point_name);
                return false;
            }
        }
        public bool Delete(MDBDATA.Model.mcupointalarmvalue model, out string mssg)
        {
            try
            {
                if (dal.Delete(model))
                {
                    mssg = string.Format("删除浸润线点{0}的参数成功", model.point_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除浸润线点{0}的参数失败", model.point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除浸润线点{0}的参数出错，错误信息:" + ex.Message, model.point_name);
                return false;
            }
        }
    }
}
