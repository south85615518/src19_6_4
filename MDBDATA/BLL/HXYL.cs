﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tool;
using MDBDATA.Model;

namespace MDBDATA.BLL
{
    public class HXYL
    {
        public MDBDATA.DAL.HXYL dal = new MDBDATA.DAL.HXYL();
        public bool HXYLPointLoad(int xmno, out List<string> pointnamelist, out string mssg)
        {
            pointnamelist = null;
            try
            {
                if (dal.HXYLPointLoad(xmno, out pointnamelist))
                {
                    mssg = string.Format("成功获取项目编号{0}点名数量{1}", xmno, pointnamelist.Count);
                    ExceptionLog.ExceptionWrite(mssg);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点名失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点名出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        public bool HXYLID(int xmno, string pointname, out string id, out string mssg)
        {
            id = "";
            try
            {
                if (dal.HXYLID(xmno, pointname, out id))
                {
                    mssg = string.Format("获取项目编号{0}点名为{1}的设备编号为{2}", xmno, pointname, id);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点名为{1}的设备编号失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点名为{1}的设备编号出错，错误信息：" + ex.Message, xmno, pointname);
                return false;
            }
        }
        public bool HXYLPointName(int xmno, string id, out string pointname, out string mssg)
        {
            pointname = "";
            try
            {
                if (dal.HXYLPointname(xmno, id, out pointname))
                {
                    mssg = string.Format("获取项目编号{0}设备号为{1}的点名为{2}", xmno, id,pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}设备号为{1}的点名失败", xmno,id);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}设备号为{1}的点名出错，错误信息：" + ex.Message, xmno, id);
                return false;
            }
        }
        public bool HXYLMaxTime(string pointnamenostr, out DateTime maxTime,out string mssg)
        {
            maxTime = new DateTime();
            mssg = "";
            try
            {
                if (dal.HXYLMaxTime(pointnamenostr, out maxTime))
                {
                    mssg = string.Format("获取设备号{0}雨量的最大日期为{1}", pointnamenostr, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取设备号{0}雨量的最大日期失败", pointnamenostr);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取设备号{0}雨量的最大日期出错，错误信息:"+ex.Message, pointnamenostr);
                return false;
            }
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(MDBDATA.Model.hxyl model,out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加雨量计设备编号{0}时间{1}数据成功",model.id,model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加雨量计设备编号{0}时间{1}数据失败", model.id, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加雨量计设备编号{0}时间{1}数据出错,错误信息:"+ex.Message, model.id, model.time);
                return false;
            }
        }
      
        /// <summary>
        /// 更新已传送标志
        /// </summary>
        public bool update(MDBDATA.Model.hxyl model,out string mssg)
        {
            try
            {
                if (dal.update(model))
                {
                    mssg = string.Format("更新雨量计设备编号{0}时间{1}数据成功", model.id, model.time);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新雨量计设备编号{0}时间{1}数据失败", model.id, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新雨量计设备编号{0}时间{1}数据出错,错误信息:" + ex.Message, model.id, model.time);
                return false;
            }
        }
        //获取当前采集数据的时间起点
        public bool maxTime(out DateTime maxTime,out string mssg)
        {
            maxTime = new DateTime();
            mssg = "";
            try
            {
                if (dal.maxTime(out maxTime))
                {
                    mssg = string.Format("获取雨量采集的时间起点为{0}", maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取雨量采集的时间起点失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取雨量采集的时间起点出错，错误信息:" + ex.Message);
                return false;
            }
           
        }

        public bool HXYLDATALoad(out List<hxyl> hxyllist,out string mssg)
        {
            hxyllist = new List<hxyl>();
            try
            {
                if (dal.HXYLDATALoad(out hxyllist))
                {
                    mssg = string.Format("获取新采集的雨量数据{0}条成功", hxyllist.Count);
                    return true;
                }
                else {
                    mssg = string.Format("获取新采集的雨量数据失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取新采集的雨量数据出错，错误信息:"+ex.Message);
                return false;
            }
        }



        public bool HXYLDATALoad(DateTime maxTime, out List<hxyl> hxyllist,out string mssg)
        {
            hxyllist = new List<hxyl>();
            try
            {
                if (/*dal.HXYLDATALoad(maxTime, out hxyllist)*/true)
                {
                    mssg = string.Format("从采集软件获取新采集的雨量数据{0}条成功", hxyllist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("从采集软件获取新采集的雨量数据失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("从采集软件获取新采集的雨量数据出错，错误信息:" + ex.Message);
                return false;
            }
        }
        /// <summary>
        /// 向云平台的数据中增加一条记录
        /// </summary>
        public bool AddMDB(MDBDATA.Model.hxyl model,out string mssg)
        {

            
                try
                {
                    if (dal.AddMDB(model))
                    {
                        mssg = string.Format("添加雨量计云数据设备编号{0}时间{1}数据成功", model.id, model.time);
                        return true;
                    }
                    else
                    {
                        mssg = string.Format("添加雨量计云数据设备编号{0}时间{1}数据失败", model.id, model.time);
                        return false;
                    }
                }
            
                catch (Exception ex)
                {
                    mssg = string.Format("添加雨量计云数据设备编号{0}时间{1}数据出错,错误信息:" + ex.Message, model.id, model.time);
                    return false;
                }
            

        }
        public bool PreAcVal(MDBDATA.Model.hxyl model, out MDBDATA.Model.hxyl pretimemodel,out string mssg)
        {
            pretimemodel = new hxyl();
            try
            {
                if (dal.PreAcVal(model, out pretimemodel))
                {
                    mssg = string.Format("获取设备号{0}{1}之前的累计雨量数据为{2}成功", model.id,model.time ,pretimemodel.ac_val);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取设备号{0}{1}之前的累计雨量数据失败", model.id, model.time);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取设备号{0}{1}之前的累计雨量数据出错，错误信息:" + ex.Message, model.id, model.time);
                return false;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList( List<string> pointnamelist, out List<MDBDATA.Model.hxyl> li, out string mssg)
        {
            li = new List<Model.hxyl>();
            try
            {
                if (dal.GetList(pointnamelist, out li))
                {
                    mssg = string.Format("获取雨量点名{0}的最新记录{1}条成功",  string.Join(",", pointnamelist), li.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取雨量点名{0}的最新记录失败",  string.Join(",", pointnamelist));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取雨量点名{0}的最新记录出错，错误信息：" + ex.Message,  string.Join(",", pointnamelist));
                return false;
            }
        }

        public bool GetAlarmTableCont(List<string> idlist, out int cont, out string mssg)
        {
            cont = 0;
            try
            {
                if (dal.GetAlarmTableCont(idlist, out cont))
                {
                    mssg = string.Format("当前雨量表中点{0}的记录共有{1}条", string.Join(",", idlist), cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取当前雨量表中点{0}的记录失败", string.Join(",", idlist));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取当前雨量表中点{0}的记录出错，错误信息:" + ex.Message, string.Join(",", idlist));
                return false;
            }
        }
        public bool Delete(string id, DateTime dt, out string mssg)
        {
            try
            {
                if (dal.Delete(id,  dt))
                {
                    mssg = string.Format("剔除设备编号{0}时间{1}雨量的数据点成功", id, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("剔除设备编号{0}时间{1}雨量的数据点失败", id, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("剔除设备编号{0}时间{1}雨量的数据点出错，错误信息:" + ex.Message, id, dt);
                return false;
            }

        }
        public bool PointNewestDateTimeGet(int pointid, out DateTime dt, out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(pointid, out dt))
                {
                    mssg = string.Format("获取雨量点{0}数据的最新日期为{1}", pointid, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取雨量点{0}数据的最新日期失败", pointid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取雨量点{0}数据的最新日期出错，错误信息:" + ex.Message, pointid);
                return false;
            }

        }
        public bool GetModel(int pointid, DateTime dt, out   MDBDATA.Model.hxyl model, out string mssg)
        {
            model = new Model.hxyl();
            try
            {
                if (dal.GetModel(pointid, dt, out model))
                {
                    mssg = string.Format("获取雨量点{0}测量时间{1}的记录成功",pointid, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取雨量点{0}测量时间{1}的记录失败",pointid, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取雨量点{0}测量时间{1}的记录出错,错误信息:" + ex.Message, pointid, dt);
                return false;
            }
        }

    }
}
