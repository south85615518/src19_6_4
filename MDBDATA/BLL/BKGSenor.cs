﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SqlHelpers;

namespace MDBDATA.BLL
{
    public class BKGSenor
    {
        public MDBDATA.DAL.BKGSenor dal = new DAL.BKGSenor();
        public bool BKGSenorLoad(string xmname,string sql,out DataTable dt)
        {
            dt = null;
            return false;
        }
        //测斜点号加载
        #region 测斜
        public bool  BKGMcuSenorPointLoad(string xmname,out List<string> pointnameList,out string mssg)
        {
            pointnameList = null;
            try
            {
                if (dal.BKGMcuSenorPointLoad(xmname, out pointnameList))
                {
                    mssg = string.Format("成功获取项目{0}的应力仪点名数量{1}", xmname, pointnameList.Count);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目{0}的应力仪点名列表失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的应力仪点名列表出错，错误信息:"+ex.Message, xmname);
                return false;
            }
        }

        public bool BKGMcuMaxTime(string xmname, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.BKGMcuMaxTime(xmname, out maxTime))
                {
                    mssg = string.Format("获取项目{0}应力最大日期为{1}", xmname, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}应力最大日期失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}应力最大日期出错，错误信息:" + ex.Message, xmname);
                return false;
            }
        }



       
        #endregion
        #region 应力
        public bool BKGInclinometerSenorPointLoad(string xmname, out List<string> pointnameList,out string mssg)
        {
            pointnameList = null;
            try
            {
                if (dal.BKGInclinometerSenorPointLoad(xmname, out pointnameList))
                {
                    mssg = string.Format("成功获取项目{0}的固定式测斜点名数量{1}", xmname, pointnameList.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的固定式测斜点名列表失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}的固定式测斜点名列表出错，错误信息:" + ex.Message, xmname);
                return false;
            }
        }

        public bool BKGMcuAngleMaxTime(string xmname, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.BKGMcuMaxTime(xmname, out maxTime))
                {
                    mssg = string.Format("获取项目{0}测斜最大日期为{1}", xmname, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}测斜最大日期失败", xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}测斜最大日期出错，错误信息:" + ex.Message, xmname);
                return false;
            }
        }
#endregion
    }
}
