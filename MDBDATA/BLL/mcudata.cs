﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SqlHelpers;

namespace MDBDATA.BLL
{
    public class mcudata
    {
        public MDBDATA.DAL.mcudata dal = new DAL.mcudata();
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(string xmname, List<string> pointnamelist, out List<MDBDATA.Model.mcudata> li, out string mssg)
        {
            li = new List<Model.mcudata>();
            try
            {
                if (dal.GetList(xmname, pointnamelist, out li))
                {
                    mssg = string.Format("获取项目{0}浸润线点名{1}的最新记录{2}条成功", xmname, string.Join(",", pointnamelist), li.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}浸润线点名{1}的最新记录失败", xmname, string.Join(",", pointnamelist));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}浸润线点名{1}的最新记录出错，错误信息：" + ex.Message, xmname, string.Join(",", pointnamelist));
                return false;
            }
        }
        public bool GetAlarmTableCont(string xmname,List<string> pointnamelist, out int cont, out string mssg)
        {
            cont = 0;
            try
            {
                if (dal.GetAlarmTableCont(xmname,pointnamelist, out cont))
                {
                    mssg = string.Format("当前浸润线表中点{0}的记录共有{1}条", string.Join(",", pointnamelist), cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取当前浸润线表中点{0}的记录失败", string.Join(",", pointnamelist));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取当前浸润线表中点{0}的记录出错，错误信息:" + ex.Message, string.Join(",", pointnamelist));
                return false;
            }
        }
        public bool Delete(string xmname, string pointname, DateTime dt,out string mssg)
        {
            try
            {
                if (dal.Delete(xmname,pointname, dt))
                {
                    mssg = string.Format("剔除点{0}时间{1}浸润线的数据点成功", pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("剔除点{0}时间{1}浸润线的数据点失败", pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("剔除点{0}时间{1}浸润线的数据点出错，错误信息:" + ex.Message, pointname, dt);
                return false;
            }
            
        }

        public bool MCUDataAcc(int pid, DateTime time, double R2, out string mssg)
        {
            try
            {
                if (dal.MCUDataAcc(pid, time, R2))
                {
                    mssg = string.Format("浸润线点名编号{0}{1}的数据重算成功", pid, time);
                    return true;
                }
                else
                {
                    mssg = string.Format("浸润线点名编号{0}{1}的数据重算失败", pid, time);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("浸润线点名编号{0}{1}的数据重算出错，错误信息:" + ex.Message, pid, time);
                return false;
            }

        }

        public bool PointNewestDateTimeGet(string xmname, string pointname, out DateTime dt,out string mssg)
        {
            mssg = "";
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(xmname, pointname, out dt))
                {
                    mssg = string.Format("获取项目{0}浸润线点{1}数据的最新日期为{2}", xmname, pointname, dt);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目{0}浸润线点{1}数据的最新日期失败", xmname, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}浸润线点{1}数据的最新日期出错，错误信息:"+ex.Message, xmname, pointname); 
                return false;
            }

        }
        public bool GetModel(string xmname, string pointname, DateTime dt, out   MDBDATA.Model.mcudata model,out string mssg)
        {
            model = new Model.mcudata();
            try
            {
                if (dal.GetModel(xmname, pointname, dt, out model))
                {
                    mssg = string.Format("获取项目{0}浸润线点{1}测量时间{2}的记录成功", xmname, pointname, dt);
                    return true;
                }
                else {
                    mssg = string.Format("获取项目{0}浸润线点{1}测量时间{2}的记录失败", xmname, pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目{0}浸润线点{1}测量时间{2}的记录出错,错误信息:"+ex.Message, xmname, pointname, dt);
                return false;
            }
        }


    }
}
