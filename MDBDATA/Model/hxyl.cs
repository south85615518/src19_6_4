﻿/**  版本信息模板在安装目录下，可自行修改。
* hxyl.cs
*
* 功 能： N/A
* 类 名： hxyl
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/24 13:47:06   N/A    初版
*
* Copyright (c) 2012 MDBDATA Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace MDBDATA.Model
{
	/// <summary>
	/// hxyl:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class hxyl
	{
		public hxyl()
		{}
		#region Model
		private string _id;
        private DateTime _time;
        private double _ac_val;
        private string _point_name;

        public string point_name
        {
            get { return _point_name; }
            set { _point_name = value; }
        }
        public double ac_val
        {
            get { return _ac_val; }
            set { _ac_val = value; }
        }
        
        private int _xmno = 0;

		private double _val;
        private bool _send = false;

        public bool send
        {
            get { return _send; }
            set { _send = value; }
        }
        public DateTime time
        {
            get { return _time; }
            set { _time = value; }
        }

        public int xmno
        {
            get { return _xmno; }
            set { _xmno = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string id
		{
			set{ _id=value;}
			get{return _id;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public double val
		{
			set{ _val=value;}
			get{return _val;}
		}
		#endregion Model

	}
}

