﻿/**  版本信息模板在安装目录下，可自行修改。
* mcuanglepointalarm.cs
*
* 功 能： N/A
* 类 名： mcuanglepointalarm
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:27   N/A    初版
*
* Copyright (c) 2012 MDBDATA Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace MDBDATA.Model
{
	/// <summary>
	/// mcuanglepointalarm:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class mcuanglepointalarm
	{
		public mcuanglepointalarm()
		{}
        #region Model
        private string _point_name;
        private string _firstalarm;
        private string _secalarm;
        private string _thirdalarm;
        private int _xmno;
        private string _remark;
        private string _pointtype;
        private int _id;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string pointtype
        {
            get { return _pointtype; }
            set { _pointtype = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string point_name
        {
            set { _point_name = value; }
            get { return _point_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string firstalarm
        {
            set { _firstalarm = value; }
            get { return _firstalarm; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string secalarm
        {
            set { _secalarm = value; }
            get { return _secalarm; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string thirdalarm
        {
            set { _thirdalarm = value; }
            get { return _thirdalarm; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int xmno
        {
            set { _xmno = value; }
            get { return _xmno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string remark
        {
            set { _remark = value; }
            get { return _remark; }
        }
        #endregion Model

	}
}

