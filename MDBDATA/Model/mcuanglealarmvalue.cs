﻿/**  版本信息模板在安装目录下，可自行修改。
* mcuanglealarmvalue.cs
*
* 功 能： N/A
* 类 名： mcuanglealarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:27   N/A    初版
*
* Copyright (c) 2012 MDBDATA Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace MDBDATA.Model
{
	/// <summary>
	/// mcuanglealarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class mcuanglealarmvalue
	{
		public mcuanglealarmvalue()
		{}
		#region Model
		private string _name;
		private double _disp;
		private double _scalevalue;
		private int _xmno;
        private double _dispL;

        public double dispL
        {
            get { return _dispL; }
            set { _dispL = value; }
        }
        private double _scalevalueL;

        public double scalevalueL
        {
            get { return _scalevalueL; }
            set { _scalevalueL = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
        public double disp
		{
            set { _disp = value; }
            get { return _disp; }
		}
		/// <summary>
		/// 
		/// </summary>
        public double scalevalue
		{
            set { _scalevalue = value; }
            get { return _scalevalue; }
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

