﻿/**  版本信息模板在安装目录下，可自行修改。
* bkgtimeinterval.cs
*
* 功 能： N/A
* 类 名： bkgtimeinterval
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/3/14 15:26:26   N/A    初版
*
* Copyright (c) 2012 MDBDATA Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace MDBDATA.Model
{
	/// <summary>
	/// bkgtimeinterval:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class bkgtimeinterval
	{
		public bkgtimeinterval()
		{}
		#region Model
		private string _pointname;
		private int _hour;
		private int _minute;
		private int _xmno;
		/// <summary>
		/// 
		/// </summary>
		public string pointname
		{
			set{ _pointname=value;}
			get{return _pointname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int hour
		{
			set{ _hour=value;}
			get{return _hour;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int minute
		{
			set{ _minute=value;}
			get{return _minute;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

