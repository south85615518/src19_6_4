﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PointAttribute.BLL;

namespace PointAttribute
{
    public class ProcessSurfaceDisplacementBLL
    {
        public static SurfaceDisplacementPointBLL bll = new SurfaceDisplacementPointBLL();
        public static bool ProcessPointCont(ProcessPointContModel model,out string mssg)
        {
            string contText = "";
            mssg = "";
            if (bll.ProcessPointCont(model.xmname, model.xmno, out contText,out mssg))
            {
                model.contText = contText;
                return true;
            }
            else
            {
                model.contText = "0";
                return false;
            }
        }

        public class ProcessPointContModel
        {
            public string xmno { get; set; }
            public string xmname { get; set; }
            public string contText { get; set; }
            public ProcessPointContModel(string xmno, string xmname)
            {
                this.xmno = xmno;
                this.xmname = xmname;
            }
        }
    }
}
