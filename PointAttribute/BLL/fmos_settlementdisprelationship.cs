﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_settlementdisprelationship.cs
*
* 功 能： N/A
* 类 名： fmos_settlementdisprelationship
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/30 11:51:23   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace PointAttribute.BLL
{
	/// <summary>
	/// fmos_settlementdisprelationship
	/// </summary>
	public partial class fmos_settlementdisprelationship
	{
		private readonly PointAttribute.DAL.fmos_settlementdisprelationship dal=new PointAttribute.DAL.fmos_settlementdisprelationship();
		public fmos_settlementdisprelationship()
		{}
		#region  BasicMethod

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(PointAttribute.Model.fmos_settlementdisprelationship model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("设置项目编号{0}点名{1}的沉降差参考点为{2}成功", model.xmno, model.point_name, model.referencepoint_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("设置项目编号{0}点名{1}的沉降差参考点为{2}失败", model.xmno, model.point_name, model.referencepoint_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("设置项目编号{0}点名{1}的沉降差参考点为{2}出错,错误信息："+ex.Message, model.xmno, model.point_name, model.referencepoint_name);
                return false;
            }
			
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(PointAttribute.Model.fmos_settlementdisprelationship model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = string.Format("更新项目编号{0}点名{1}的沉降差参考点为{2}成功", model.xmno, model.point_name, model.referencepoint_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("更新项目编号{0}点名{1}的沉降差参考点为{2}失败", model.xmno, model.point_name, model.referencepoint_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("更新项目编号{0}点名{1}的沉降差参考点为{2}出错,错误信息：" + ex.Message, model.xmno, model.point_name, model.referencepoint_name);
                return false;
            }
			
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string pointname,out string mssg)
		{
            try
            {
                if (dal.Delete(xmno, pointname))
                {
                    mssg = string.Format("删除项目编号{0}点名{1}的沉降差参考关联成功", xmno, pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}点名{1}的沉降差参考关联失败", xmno, pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}点名{1}的沉降差参考关联出错,错误信息:"+ex.Message, xmno, pointname);
                return false;
            }
		}

        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, colName, sord, out  dt))
                {
                    mssg = string.Format("加载项目编号{0}的沉降差点名参考关系记录{1}条成功", xmno, dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("加载项目编号{0}的沉降差点名参考关系记录失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目编号{0}的沉降差点名参考关系记录出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont, out string mssg)
        {
            totalCont = 0;
            try
            {
                if (dal.TableRowsCount(searchstring, xmno, out totalCont))
                {
                    mssg = string.Format("加载项目编号{0}的沉降差点名参考关系记录数为{1}条", xmno, totalCont);
                    return true;

                }
                else
                {
                    mssg = string.Format("加载项目{0}的沉降差点名参考关系记录数失败!", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "加载项目{0}的沉降差点名参考关系记录数加载出错，出错信息" + ex.Message;
                return false;
            }

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string point_name, out PointAttribute.Model.fmos_settlementdisprelationship model,out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(xmno, point_name, out model))
                {
                    mssg = string.Format("获取到项目编号{0}点{1}的沉降差参考点{2}", xmno, point_name,model.referencepoint_name);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}点{1}的沉降差参考点失败", xmno, point_name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}点{1}的沉降差参考点出错,错误信息:"+ex.Message, xmno, point_name);
                return false;
            }
        }


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

