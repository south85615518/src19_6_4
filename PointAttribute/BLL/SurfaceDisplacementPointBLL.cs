﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PointAttribute.DAL;
using Tool;

namespace PointAttribute.BLL
{

   public class SurfaceDisplacementPointBLL
    {
        public SurfaceDisplacementPointDAL dal = new SurfaceDisplacementPointDAL();
        public bool ProcessPointCont(string xmname,string xmno,out string contText,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.pointcont( xmname,xmno,out contText))
                {
                    mssg = "获取成功";
                    return true;
                }
                else
                {
                    mssg = "获取失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("测点数获取失败");
                contText = "0";
                mssg = "表面位移测点获取出错,错误信息:"+ex.Message;
                return false;
            }
        }
       
    }
}
