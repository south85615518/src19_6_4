﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_pointalarmvalue.cs
*
* 功 能： N/A
* 类 名： fmos_pointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 15:36:27   N/A    初版
*
* Copyright (c) 2012 PointAttribute Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using PointAttribute.Common;
using PointAttribute.Model;
namespace PointAttribute.BLL
{
	/// <summary>
	/// fmos_pointalarmvalue
	/// </summary>
	public partial class fmos_pointalarmvalue
	{
		private readonly PointAttribute.DAL.fmos_pointalarmvalue dal=new PointAttribute.DAL.fmos_pointalarmvalue();
		public fmos_pointalarmvalue()
		{}
		#region  BasicMethod
        public bool Exist(PointAttribute.Model.fmos_pointalarmvalue model,out string mssg)
        {

            try
            {
                if (dal.Exist(model))
                {
                    mssg = string.Format("项目编号{0}{1}{2}点已经存在",model.xmno,model.pointtype,model.POINT_NAME);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目编号{0}{1}{2}点不存在", model.xmno, model.pointtype, model.POINT_NAME);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目编号{0}{1}{2}点查询出错,错误信息:" + ex.Message;
                return true;
            }


        }
		/// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = "表面位移预警点新增成功";
                    return true;
                }
                else
                {
                    mssg = "表面位移预警点新增失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "表面位移预警点新增出错，错误信息" + ex.Message;
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(PointAttribute.Model.fmos_pointalarmvalue model,out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = "表面位移预警点更新成功";
                    return true;
                }
                else
                {
                    mssg = "表面位移预警点更新失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "表面位移预警点更新出错，错误信息"+ex.Message;
                return false;
            }
		}
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointnamestr,PointAttribute.Model.fmos_pointalarmvalue model,out string mssg)
        {
             //dal.MultiUpdate(pointnamestr,model);
            try
            {
                if (dal.MultiUpdate(pointnamestr,model))
                {
                    mssg = "表面位移预警点批量更新成功";
                    return true;
                }
                else
                {
                    mssg = "表面位移预警点批量更新失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "表面位移预警点批量更新出错，错误信息" + ex.Message;
                return false;
            }
        }
		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
		{
			//该表无主键信息，请自定义主键/条件字段
            try
            {
                if (dal.Delete(model))
                {
                    mssg = "表面位移预警点删除成功";
                    return true;
                }
                else
                {
                    mssg = "表面位移预警点删除失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "表面位移预警点删除出错，错误信息" + ex.Message;
                return false;
            }
		
        }

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public PointAttribute.Model.fmos_pointalarmvalue GetModel()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    return dal.GetModel();
        //}

        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public PointAttribute.Model.fmos_pointalarmvalue GetModelByCache()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    string CacheKey = "fmos_pointalarmvalueModel-" ;
        //    object objModel = PointAttribute.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel();
        //            if (objModel != null)
        //            {
        //                int ModelCache = PointAttribute.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                PointAttribute.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (PointAttribute.Model.fmos_pointalarmvalue)objModel;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    return dal.GetList(strWhere);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<PointAttribute.Model.fmos_pointalarmvalue> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<PointAttribute.Model.fmos_pointalarmvalue> DataTableToList(DataTable dt)
		{
			List<PointAttribute.Model.fmos_pointalarmvalue> modelList = new List<PointAttribute.Model.fmos_pointalarmvalue>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				PointAttribute.Model.fmos_pointalarmvalue model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}
        public bool TotalStationPointLoadBLL(int xmno, out List<string> ls, out string mssg)
        {
            ls = null;
            try
            {
                if (dal.TotalStationPointLoadDAL(xmno, out ls))
                {
                    mssg = "全站仪点号加载成功";
                    return true;

                }
                else
                {
                    mssg = "全站仪点号加载失败";
                    return true;
                }

            }
            catch (Exception ex)
            {
                mssg = "全站仪点号加载出错，错误信息" + ex.Message;
                return true;
            }

        }
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, out PointAttribute.Model.fmos_pointalarmvalue model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, pointname, out model))
                {
                    mssg = string.Format("获取{0}的预警参数成功!",pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取{0}的预警参数失败!", pointname);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}的预警参数出错!错误信息:"+ex.Message, pointname);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, out PointAttribute.Model.fmos_pointalarmvalue model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(xmno, out model))
                {
                    mssg = string.Format("获取项目编号{0}的预警参数成功!", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的预警参数成功!", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的预警参数出错!错误信息:" + ex.Message, xmno);
                return false;
            }
        }
      



		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

