﻿/**  版本信息模板在安装目录下，可自行修改。
* inclinometer_pointalarmvalue.cs
*
* 功 能： N/A
* 类 名： inclinometer_pointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 15:36:27   N/A    初版
*
* Copyright (c) 2012 PointAttribute Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.Odbc;

using SqlHelpers;
using System.Collections.Generic;
//using Odbc.Data.OdbcClient;
//using PointAttribute.DBUtility;//Please add references
namespace PointAttribute.DAL
{

    /// <summary>
    /// 数据访问类:inclinometer_pointalarmvalue
    /// </summary>
    public class senor_pointalarmvalue
    {
        public senor_pointalarmvalue()
        { }
        public static database db = new database();
        #region  BasicMethod
        //判断点名对象是否存在
        public bool Exist(PointAttribute.Model.fmos_pointalarmvalue model)
        {

            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("select count(1) from inclinometer_pointalarmvalue   where     xmno=@xmno     and     pointtype=@pointtype    ");
            strSql.Append("   and       point_name=@point_name       ");
            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
					new OdbcParameter("@pointtype", OdbcType.VarChar,100),
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
                                         };

            parameters[0].Value = model.xmno;
            parameters[1].Value = model.pointtype;
            parameters[2].Value = model.POINT_NAME;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);

            return obj == null ? true : int.Parse(obj.ToString()) > 0 ? true : false;
            
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(PointAttribute.Model.fmos_pointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("insert into inclinometer_pointalarmvalue(");
            strSql.Append("ID,xmno,POINT_NAME,FirstAlarmName,SecondAlarmName,ThirdAlarmName,remark,pointtype)");
            strSql.Append(" values (");
            strSql.Append("@ID,@xmno,@POINT_NAME,@FirstAlarmName,@SecondAlarmName,@ThirdAlarmName,@remark,@pointtype)");
            OdbcParameter[] parameters = {
					new OdbcParameter("@ID", OdbcType.Int,11),
					new OdbcParameter("@xmno", OdbcType.VarChar,120),
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@pointtype", OdbcType.VarChar,100)};
            parameters[0].Value = model.ID;
            parameters[1].Value = model.xmno;
            parameters[2].Value = model.POINT_NAME;
            parameters[3].Value = model.FirstAlarmName;
            parameters[4].Value = model.SecondAlarmName;
            parameters[5].Value = model.ThirdAlarmName;
            parameters[6].Value = model.remark;
            parameters[7].Value = model.pointtype;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(PointAttribute.Model.fmos_pointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update inclinometer_pointalarmvalue set ");
            strSql.Append("POINT_NAME=@POINT_NAME,");
            strSql.Append("FirstAlarmName=@FirstAlarmName,");
            strSql.Append("SecondAlarmName=@SecondAlarmName,");
            strSql.Append("ThirdAlarmName=@ThirdAlarmName,");
            strSql.Append("remark=@remark,");
            strSql.Append("pointtype=@pointtype");
            strSql.Append("   where   ");
            strSql.Append("ID=@ID");
            OdbcParameter[] parameters = {
					
					new OdbcParameter("@POINT_NAME", OdbcType.VarChar,120),
					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
					new OdbcParameter("@pointtype", OdbcType.VarChar,100),
					new OdbcParameter("@ID", OdbcType.Int,10)
                    
                                         };

            parameters[0].Value = model.POINT_NAME;
            parameters[1].Value = model.FirstAlarmName;
            parameters[2].Value = model.SecondAlarmName;
            parameters[3].Value = model.ThirdAlarmName;
            parameters[4].Value = model.remark;
            parameters[5].Value = model.pointtype;

            parameters[6].Value = model.ID;

            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新多条数据
        /// </summary>
        public bool MultiUpdate(string pointNameStr, PointAttribute.Model.fmos_pointalarmvalue model)
        {
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("update inclinometer_pointalarmvalue set ");
            strSql.Append("FirstAlarmName=@FirstAlarmName,");
            strSql.Append("SecondAlarmName=@SecondAlarmName,");
            strSql.Append("ThirdAlarmName=@ThirdAlarmName,");
            strSql.Append(" remark=@remark ,");
            strSql.Append("pointtype=@pointtype");
            strSql.Append("   where   ");
            strSql.Append(" point_name  in ('" + pointNameStr + "')   and xmno = @xmno ");
            OdbcParameter[] parameters = {

					new OdbcParameter("@FirstAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@SecondAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@ThirdAlarmName", OdbcType.VarChar,120),
					new OdbcParameter("@remark", OdbcType.VarChar,500),
                    new OdbcParameter("@pointtype",OdbcType.VarChar,120),
                    new OdbcParameter("@xmno", OdbcType.Int)
                                         };



            parameters[0].Value = model.FirstAlarmName;
            parameters[1].Value = model.SecondAlarmName;
            parameters[2].Value = model.ThirdAlarmName;
            parameters[3].Value = model.remark;
            parameters[4].Value = model.pointtype;
            parameters[5].Value = model.xmno;


            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(PointAttribute.Model.fmos_pointalarmvalue model)
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            OdbcConnection conn = db.GetStanderConn(model.xmno);
            OdbcSQLHelper.Conn = conn;
            strSql.Append("delete from  inclinometer_pointalarmvalue  where point_name in (@point_name) and  xmno = @xmno  ");
            OdbcParameter[] parameters = {
                new OdbcParameter("@point_name", OdbcType.VarChar,120),
                new OdbcParameter("@xmno", OdbcType.Int,4)
			};
            parameters[0].Value = model.POINT_NAME;
            parameters[1].Value = model.xmno;
            int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 点名加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="ls"></param>
        /// <returns></returns>
        public bool TotalStationPointLoadDAL(int xmno, out List<string> ls)
        {

            string sql = "select distinct(point_name) from inclinometer_pointalarmvalue where xmno='" + xmno + "' order by point_name asc";

            OdbcConnection conn = db.GetStanderConn(xmno);
            ls = querysql.querystanderlist(sql, conn);
            return true;

        }

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public PointAttribute.Model.fmos_pointalarmvalue GetModel()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select ID,xmno,POINT_NAME,FirstAlarmName,SecondAlarmName,ThirdAlarmName,remark,pointtype from inclinometer_pointalarmvalue ");
        //    strSql.Append(" where ");
        //    OdbcParameter[] parameters = {
        //    };

        //    PointAttribute.Model.fmos_pointalarmvalue model=new PointAttribute.Model.fmos_pointalarmvalue();
        //    DataSet ds=DbHelperOdbc.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        return DataRowToModel(ds.Tables[0].Rows[0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public PointAttribute.Model.fmos_pointalarmvalue DataRowToModel(DataRow row)
        {
            PointAttribute.Model.fmos_pointalarmvalue model = new PointAttribute.Model.fmos_pointalarmvalue();
            if (row != null)
            {
                if (row["ID"] != null && row["ID"].ToString() != "")
                {
                    model.ID = int.Parse(row["ID"].ToString());
                }
                if (row["xmno"] != null)
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["POINT_NAME"] != null)
                {
                    model.POINT_NAME = row["POINT_NAME"].ToString();
                }
                if (row["FirstAlarmName"] != null)
                {
                    model.FirstAlarmName = row["FirstAlarmName"].ToString();
                }
                if (row["SecondAlarmName"] != null)
                {
                    model.SecondAlarmName = row["SecondAlarmName"].ToString();
                }
                if (row["ThirdAlarmName"] != null)
                {
                    model.ThirdAlarmName = row["ThirdAlarmName"].ToString();
                }
                if (row["remark"] != null)
                {
                    model.remark = row["remark"].ToString();
                }
                if (row["pointtype"] != null)
                {
                    model.pointtype = row["pointtype"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, string pointname, out PointAttribute.Model.fmos_pointalarmvalue model)
        {
            OdbcConnection conn = db.GetStanderConn(xmno);
            OdbcSQLHelper.Conn = conn;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,POINT_NAME,FirstAlarmName,SecondAlarmName,ThirdAlarmName,remark,pointtype,id from inclinometer_pointalarmvalue where       xmno=@xmno   and   point_name=@point_name   ");

            OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,10),
                    new OdbcParameter("@point_name", OdbcType.VarChar,200)
			};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;
            model = new PointAttribute.Model.fmos_pointalarmvalue();
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select ID,xmno,POINT_NAME,FirstAlarmName,SecondAlarmName,ThirdAlarmName,remark,pointtype ");
        //    strSql.Append(" FROM inclinometer_pointalarmvalue ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    return DbHelperOdbc.Query(strSql.ToString());
        //}

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM inclinometer_pointalarmvalue ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.nu desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from inclinometer_pointalarmvalue T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return DbHelperOdbc.Query(strSql.ToString());
        //}

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OdbcParameter[] parameters = {
                    new OdbcParameter("@tblName", OdbcType.VarChar, 255),
                    new OdbcParameter("@fldName", OdbcType.VarChar, 255),
                    new OdbcParameter("@PageSize", OdbcType.Int),
                    new OdbcParameter("@PageIndex", OdbcType.Int),
                    new OdbcParameter("@IsReCount", OdbcType.Bit),
                    new OdbcParameter("@OrderType", OdbcType.Bit),
                    new OdbcParameter("@strWhere", OdbcType.VarChar,1000),
                    };
            parameters[0].Value = "inclinometer_pointalarmvalue";
            parameters[1].Value = "nu";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperOdbc.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        public bool PointAlarmValueTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select id,point_name,pointtype,firstAlarmName,secondAlarmName,thirdAlarmName,remark from inclinometer_pointalarmvalue where xmno='{3}' and {4} {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointTableLoad(string searchstring, int startPageIndex, int pageSize, int xmno, string xmname, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id" ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetStanderConn(xmname);
            string sql = string.Format("select id,point_name,pointtype,firstAlarmName,secondAlarmName,thirdAlarmName,remark from inclinometer_pointalarmvalue where xmno='{3}' and {4}  {2} limit {0},{1}   ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order, xmno, searchstring);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }

        public bool PointAlarmValueTableRowsCount(string xmname, string searchstring, int xmno, out string totalCont)
        {
            string sql = "select count(*) from inclinometer_pointalarmvalue where xmno = '" + xmno + "'";
            if (searchstring != "1 = 1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetStanderConn(xmname);
            totalCont = querysql.querystanderstr(sql, conn);
            return true;
            return false;
        }



        public bool PointAlarmValueMultilUpdate( PointAttribute.Model.fmos_pointalarmvalue alarm, string xmname)
        {
            string sql = "update inclinometer_pointalarmvalue set firstAlarmName=@firstAlarmName,secondAlarmName=@secondAlarmName,thirdAlarmName=@thirdAlarmName,remark=@remark where   point_name in (@pointname) and xmno =@xmno or id = @id ";
            OdbcConnection conn = db.GetStanderConn(xmname);
            sql = sql.Replace("@id", "'" + alarm.ID + "'");
            sql = sql.Replace("@pointname", "'" + alarm.POINT_NAME + "'");
            sql = sql.Replace("@xmno", "'" + alarm.xmno + "'");
            sql = sql.Replace("@firstAlarmName", "'" + alarm.FirstAlarmName + "'");
            sql = sql.Replace("@secondAlarmName", "'" + alarm.SecondAlarmName + "'");
            sql = sql.Replace("@thirdAlarmName", "'" + alarm.ThirdAlarmName + "'");
            sql = sql.Replace("@remark", "'" + alarm.remark + "'");


            updatedb udb = new updatedb();
            int cont = udb.UpdateStanderDB(sql, conn);
            return cont > 0 ? true : false;

        }




        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

