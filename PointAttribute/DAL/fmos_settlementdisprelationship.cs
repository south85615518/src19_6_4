﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_settlementdisprelationship.cs
*
* 功 能： N/A
* 类 名： fmos_settlementdisprelationship
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/30 11:51:23   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
//Please add references
namespace PointAttribute.DAL
{
	/// <summary>
	/// 数据访问类:fmos_settlementdisprelationship
	/// </summary>
	public partial class fmos_settlementdisprelationship
	{
        public database db = new database();


		public fmos_settlementdisprelationship()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(PointAttribute.Model.fmos_settlementdisprelationship model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
			strSql.Append("replace into fmos_settlementdisprelationship(");
			strSql.Append("xmno,point_name,referencepoint_name)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@point_name,@referencepoint_name)");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@referencepoint_name", OdbcType.VarChar,100)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.referencepoint_name;

			int rows = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(PointAttribute.Model.fmos_settlementdisprelationship model)
		{
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(model.xmno);
			strSql.Append("update fmos_settlementdisprelationship set ");
			strSql.Append("xmno=@xmno,");
			strSql.Append("point_name=@point_name,");
			strSql.Append("referencepoint_name=@referencepoint_name");
			strSql.Append(" where ");
			OdbcParameter[] parameters = {
					new OdbcParameter("@xmno", OdbcType.Int,11),
					new OdbcParameter("@point_name", OdbcType.VarChar,100),
					new OdbcParameter("@referencepoint_name", OdbcType.VarChar,100)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.point_name;
			parameters[2].Value = model.referencepoint_name;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int xmno,string pointname)
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
			strSql.Append("delete from fmos_settlementdisprelationship ");
			strSql.Append("  where  ");
            strSql.Append(" xmno = @xmno  and  point_name = @point_name");
            
			OdbcParameter[] parameters = {
                new OdbcParameter("@xmno",OdbcType.Int),
                new OdbcParameter("@point_name",OdbcType.VarChar,100)

			};
            parameters[0].Value = xmno;
            parameters[1].Value = pointname;

			int rows=OdbcSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 预警参数表加载
        /// </summary>
        /// <param name="startPageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="xmname"></param>
        /// <param name="colName"></param>
        /// <param name="sord"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool TableLoad(int startPageIndex, int pageSize, int xmno, string colName, string sord, out DataTable dt)
        {
            string order = colName == "id " ? "" : "order by " + colName + "  " + sord;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string sql = string.Format("select fmos_pointalarmvalue.point_name,fmos_settlementdisprelationship.referencepoint_name,fmos_pointalarmvalue.xmno from  fmos_pointalarmvalue  left  join  fmos_settlementdisprelationship  on  fmos_pointalarmvalue.xmno = fmos_settlementdisprelationship.xmno  and  fmos_pointalarmvalue.point_name = fmos_settlementdisprelationship.point_name   where     fmos_pointalarmvalue.xmno =  '" + xmno + "'    {2}  limit {0},{1}  ", (startPageIndex - 1) * pageSize, pageSize * startPageIndex, order);
            dt = querysql.querystanderdb(sql, conn);
            if (dt != null) return true;
            return false;
        }
        /// <summary>
        /// 预警参数表记录数加载
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="totalCont"></param>
        /// <returns></returns>
        public bool TableRowsCount(string searchstring, int xmno, out int totalCont)
        {
            string sql = "select count(1) from fmos_pointalarmvalue where xmno = '" + xmno + "'";
            if (searchstring != null && searchstring.Trim() != "1=1") sql = sql + "  and  " + searchstring;
            OdbcConnection conn = db.GetSurveyStanderConn(xmno);
            string cont = querysql.querystanderstr(sql, conn);
            totalCont = cont == "" ? 0 : Convert.ToInt32(cont);
            return true;

        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public bool  GetModel( int xmno,string point_name,out PointAttribute.Model.fmos_settlementdisprelationship model)
		{
			//该表无主键信息，请自定义主键/条件字段
			StringBuilder strSql=new StringBuilder();
            OdbcSQLHelper.Conn = db.GetSurveyStanderConn(xmno);
            string pointnamecondition = point_name == "" ? " 1 = 1  " : "point_name = '" + point_name + "'";
			strSql.Append("select xmno,point_name,referencepoint_name from fmos_settlementdisprelationship ");
			strSql.Append(" where  xmno = @xmno  and  "+pointnamecondition);
			OdbcParameter[] parameters = {
                                             new OdbcParameter("@xmno",OdbcType.Int)
			};
            parameters[0].Value = xmno;
			model=new PointAttribute.Model.fmos_settlementdisprelationship();
			DataSet ds=OdbcSQLHelper.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
			}
            return false;
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public PointAttribute.Model.fmos_settlementdisprelationship DataRowToModel(DataRow row)
		{
			PointAttribute.Model.fmos_settlementdisprelationship model=new PointAttribute.Model.fmos_settlementdisprelationship();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["point_name"]!=null)
				{
					model.point_name=row["point_name"].ToString();
				}
				if(row["referencepoint_name"]!=null)
				{
					model.referencepoint_name=row["referencepoint_name"].ToString();
				}
			}
			return model;
		}

		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

