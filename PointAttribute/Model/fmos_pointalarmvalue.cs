﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_pointalarmvalue.cs
*
* 功 能： N/A
* 类 名： fmos_pointalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/19 15:36:27   N/A    初版
*
* Copyright (c) 2012 PointAttribute Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace PointAttribute.Model
{
	/// <summary>
	/// fmos_pointalarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fmos_pointalarmvalue
	{
		public fmos_pointalarmvalue()
		{}
		#region Model
		private int _id=0;
		private int _xmno;
		private string _point_name;
		private string _firstalarmname;
		private string _secondalarmname;
		private string _thirdalarmname;
		private string _remark;
		private string _pointtype;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POINT_NAME
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FirstAlarmName
		{
			set{ _firstalarmname=value;}
			get{return _firstalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SecondAlarmName
		{
			set{ _secondalarmname=value;}
			get{return _secondalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ThirdAlarmName
		{
			set{ _thirdalarmname=value;}
			get{return _thirdalarmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pointtype
		{
			set{ _pointtype=value;}
			get{return _pointtype;}
		}
		#endregion Model

	}
}

