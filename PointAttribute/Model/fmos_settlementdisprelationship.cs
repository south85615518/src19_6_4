﻿/**  版本信息模板在安装目录下，可自行修改。
* fmos_settlementdisprelationship.cs
*
* 功 能： N/A
* 类 名： fmos_settlementdisprelationship
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/30 11:51:22   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace PointAttribute.Model
{
	/// <summary>
	/// fmos_settlementdisprelationship:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class fmos_settlementdisprelationship
	{
		public fmos_settlementdisprelationship()
		{}
		#region Model
		private int _xmno;
		private string _point_name;
		private string _referencepoint_name;
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string referencepoint_name
		{
			set{ _referencepoint_name=value;}
			get{return _referencepoint_name;}
		}
		#endregion Model

	}
}

