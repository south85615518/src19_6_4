﻿/**  版本信息模板在安装目录下，可自行修改。
* xmdataupdatetime.cs
*
* 功 能： N/A
* 类 名： xmdataupdatetime
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2019/2/27 11:34:38   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using SqlHelpers;
using System.Collections.Generic;//Please add references
namespace SystemData.DAL
{

	/// <summary>
	/// 数据访问类:xmdataupdatetime
	/// </summary>
	public partial class xmdataupdatetime
	{
        public static database db = new database();
		public xmdataupdatetime()
		{}
		#region  BasicMethod

        public bool XmDataUpdateModelListLoad(string unitname,out List<Model.xmdataupdatetime> modelist)
        {
            OdbcSQLHelper.Conn = db.GetUnitStanderConn(unitname);
            StringBuilder strsql = new StringBuilder();
            strsql.Append("select xmno,time from xmdataupdatetime  order  by time  ");
            DataSet ds = OdbcSQLHelper.Query(strsql.ToString());
            modelist = new List<Model.xmdataupdatetime>();
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                modelist.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public SystemData.Model.xmdataupdatetime DataRowToModel(DataRow row)
        {
            SystemData.Model.xmdataupdatetime model = new SystemData.Model.xmdataupdatetime();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
            }
            return model;
        }
		
		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

