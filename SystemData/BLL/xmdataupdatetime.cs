﻿/**  版本信息模板在安装目录下，可自行修改。
* xmdataupdatetime.cs
*
* 功 能： N/A
* 类 名： xmdataupdatetime
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2019/2/27 11:34:38   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using SystemData.Model;
namespace SystemData.BLL
{
	/// <summary>
	/// xmdataupdatetime
	/// </summary>
	public partial class xmdataupdatetime
	{
		private readonly SystemData.DAL.xmdataupdatetime dal=new SystemData.DAL.xmdataupdatetime();
		public xmdataupdatetime()
		{}
		#region  BasicMethod
        public bool XmDataUpdateModelListLoad(string unitname, out List<Model.xmdataupdatetime> modelist, out string mssg)
        {
            mssg = "";
            modelist = null;
            try
            {
                if (dal.XmDataUpdateModelListLoad(unitname, out modelist))
                {
                    mssg = string.Format("成功获取到项目数据更新查询表记录数{0}条", modelist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目数据更新查询表记录失败", modelist.Count);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目数据更新查询表记录出错,错误信息:"+ex.Message);
                return false;
            }
           
        }
		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

