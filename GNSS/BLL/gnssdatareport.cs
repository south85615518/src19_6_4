﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SqlHelpers;

namespace GPS.BLL
{
    public class gnssdatareport
    {
        public GPS.DAL.gnssdatareport dal = new GPS.DAL.gnssdatareport();
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList( List<string> pointnamelist, out List<GPS.Model.gnssdatareport> li, out string mssg)
        {
            li = new List<GPS.Model.gnssdatareport>();
            try
            {
                if (dal.GetList(pointnamelist, out li))
                {
                    mssg = string.Format("获取GNSS点名{0}的最新记录{1}条成功", string.Join(",", pointnamelist), li.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取GNSS点名{0}的最新记录失败", string.Join(",", pointnamelist));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取GNSS点名{0}的最新记录出错，错误信息：" + ex.Message,  string.Join(",", pointnamelist));
                return false;
            }
        }
        public bool GetAlarmTableCont(List<string> pointnamelist, out int cont, out string mssg)
        {
            cont = 0;
            try
            {
                if (dal.GetAlarmTableCont(pointnamelist, out cont))
                {
                    mssg = string.Format("当前GNSS表中点{0}的记录共有{1}条", string.Join(",", pointnamelist), cont);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取当前GNSS表中点{0}的记录失败", string.Join(",", pointnamelist));
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取当前GNSS表中点{0}的记录出错，错误信息:" + ex.Message, string.Join(",", pointnamelist));
                return false;
            }
        }
        public bool Delete(string pointname, DateTime dt, out string mssg)
        {
            try
            {
                if (dal.Delete(pointname, dt))
                {
                    mssg = string.Format("剔除点{0}时间{1}GNSS的数据点成功", pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("剔除点{0}时间{1}GNSS的数据点失败", pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("剔除点{0}时间{1}GNSS的数据点出错，错误信息:" + ex.Message, pointname, dt);
                return false;
            }

        }
        public bool PointNewestDateTimeGet(string pointname, out DateTime dt, out string mssg)
        {
            dt = new DateTime();
            try
            {
                if (dal.PointNewestDateTimeGet(pointname, out dt))
                {
                    mssg = string.Format("获取点{0}GNSS的最新采集时间成功", pointname);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取点{0}GNSS的最新采集时间失败", pointname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取点{0}GNSS的最新采集时间出错,错误信息:" + ex.Message, pointname);
                return false;
            }



        }

        public bool GetModel(string pointname, DateTime dt, out GPS.Model.gnssdatareport model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(pointname, dt, out model))
                {
                    mssg = string.Format("获取点{0}测量时间{1}的GNSS数据记录成功", pointname, dt);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取点{0}测量时间{1}的GNSS数据记录失败", pointname, dt);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取点{0}测量时间{1}的GNSS数据记录出错,错误信息:" + ex.Message, pointname, dt);
                return false;
            }

        }
    }
}
