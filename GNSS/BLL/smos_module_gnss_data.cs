﻿/**  版本信息模板在安装目录下，可自行修改。
* smos_module_gnss_data.cs
*
* 功 能： N/A
* 类 名： smos_module_gnss_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/22 10:46:08   N/A    初版
*
* Copyright (c) 2012 GNSS Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace GPS.BLL
{
	/// <summary>
	/// smos_module_gnss_data
	/// </summary>
	public partial class smos_module_gnss_data
	{
        public GPS.DAL.smos_module_gnss_data dal = new DAL.smos_module_gnss_data(); 
        public bool MaxTime(string pointnamestr, out DateTime maxTime, out string mssg)
        {
            maxTime = new DateTime();
            try
            {
                if (dal.MaxTime(pointnamestr, out maxTime))
                {
                    mssg = string.Format("获取点列{0}GPS测量数据的最大日期{1}成功", pointnamestr, maxTime);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取点列{0}GPS测量数据的最大日期失败", pointnamestr);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取点列{0}GPS测量数据的最大日期出错，错误信息:" + ex.Message, pointnamestr);
                return false;
            }
        }
	}
}

