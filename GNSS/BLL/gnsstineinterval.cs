﻿/**  版本信息模板在安装目录下，可自行修改。
* gnsstimeinterval.cs
*
* 功 能： N/A
* 类 名： gnsstimeinterval
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/3/14 15:26:28   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace GPS.BLL
{
	/// <summary>
	/// gnsstimeinterval
	/// </summary>
	public partial class gnsstimeinterval
	{
		private readonly GPS.DAL.gnsstimeinterval dal=new GPS.DAL.gnsstimeinterval();
		public gnsstimeinterval()
		{}
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(GPS.Model.gnsstimeinterval model, out string mssg)
        {
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加GNSS{0}时间间隔{1}时{2}分成功", model.pointname, model.hour, model.minute);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加GNSS{0}时间间隔{1}时{2}分失败", model.pointname, model.hour, model.minute);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加GNSS{0}时间间隔{1}时{2}分出错，错误信息:" + ex.Message, model.pointname, model.hour, model.minute);
                return false;
            }
        }

        ///// <summary>
        ///// 更新一条数据
        ///// </summary>
        //public bool Update(GPS.Model.gnsstimeinterval model)
        //{
        //    return dal.Update(model);
        //}

        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool Delete()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    return dal.Delete();
        //}

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public GPS.Model.gnsstimeinterval GetModel()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    return dal.GetModel();
        //}

        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public GPS.Model.gnsstimeinterval GetModelByCache()
        //{
        //    //该表无主键信息，请自定义主键/条件字段
        //    string CacheKey = "bkgtimeintervalModel-" ;
        //    object objModel = MDBDATA.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel();
        //            if (objModel != null)
        //            {
        //                int ModelCache = MDBDATA.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                MDBDATA.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (GPS.Model.gnsstimeinterval)objModel;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    return dal.GetList(strWhere);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<GPS.Model.gnsstimeinterval> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<GPS.Model.gnsstimeinterval> DataTableToList(DataTable dt)
        //{
        //    List<GPS.Model.gnsstimeinterval> modelList = new List<GPS.Model.gnsstimeinterval>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        GPS.Model.gnsstimeinterval model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}
        public bool TableLoad(int startPageIndex, int pageSize, int xmno,string sord, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.TableLoad(startPageIndex, pageSize, xmno, sord, out  dt))
                {
                    mssg = string.Format("获取项目编号{0}GNSS的时间间隔列表成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}GNSS的时间间隔列表失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}GNSS的时间间隔列表出错，错误信息:" + ex.Message, xmno);
                return false;
            }
        }
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

