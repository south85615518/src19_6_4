﻿/**  版本信息模板在安装目录下，可自行修改。
* smos_module_gnss_device.cs
*
* 功 能： N/A
* 类 名： smos_module_gnss_device
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/22 10:46:08   N/A    初版
*
* Copyright (c) 2012 GNSS Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Collections.Generic;
using System;
namespace GPS.BLL
{
	/// <summary>
	/// smos_module_gnss_device
	/// </summary>
	public partial class smos_module_gnss_device
	{
        public GPS.DAL.smos_module_gnss_device dal = new GPS.DAL.smos_module_gnss_device();
        public bool gnsspointnameload(int xmno, out List<string> pointlist,out string mssg)
        {
            mssg = "";
            pointlist = new List<string>();
            try
            {
                if (dal.gnsspointnameload(xmno, out pointlist))
                {
                    mssg = string.Format("加载项目编号{0}的GNSS点名数量为{1}", xmno,pointlist.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("加载项目编号{0}的GNSS点名失败", xmno);
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目编号{0}的GNSS点名出错，错误信息："+ex.Message, xmno);
                return false;
            }
        }
	}
}

