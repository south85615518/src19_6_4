﻿/**  版本信息模板在安装目录下，可自行修改。
* gnssdatareport.cs
*
* 功 能： N/A
* 类 名： gnssdatareport
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/31 11:41:00   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace GPS.Model
{
	/// <summary>
	/// gnssdatareport:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gnssdatareport
	{
		public gnssdatareport()
		{}
		#region Model
		private string _point_name;
		private double _this_x;
		private double _this_y;
		private double _this_z;
		private double _l_x;
		private double _l_y;
		private double _l_z;
		private double _x;
		private double _y;
		private double _z;
		private DateTime _time= new DateTime();
		private int _platform_id;
		/// <summary>
		/// 
		/// </summary>
		public string point_name
		{
			set{ _point_name=value;}
			get{return _point_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_x
		{
			set{ _this_x=value;}
			get{return _this_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_y
		{
			set{ _this_y=value;}
			get{return _this_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_z
		{
			set{ _this_z=value;}
			get{return _this_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double l_x
		{
			set{ _l_x=value;}
			get{return _l_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double l_y
		{
			set{ _l_y=value;}
			get{return _l_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double l_z
		{
			set{ _l_z=value;}
			get{return _l_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double x
		{
			set{ _x=value;}
			get{return _x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double y
		{
			set{ _y=value;}
			get{return _y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double z
		{
			set{ _z=value;}
			get{return _z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int platform_id
		{
			set{ _platform_id=value;}
			get{return _platform_id;}
		}
		#endregion Model

	}
}

