﻿/**  版本信息模板在安装目录下，可自行修改。
* smos_module_gnss_data.cs
*
* 功 能： N/A
* 类 名： smos_module_gnss_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/22 10:46:07   N/A    初版
*
* Copyright (c) 2012 GNSS Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace GPS.Model
{
	/// <summary>
	/// smos_module_gnss_data:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class smos_module_gnss_data
	{
		public smos_module_gnss_data()
		{}
		#region Model
		private int _id;
		private string _platform_id;
		private DateTime? _time;
		private string _name;
		private double? _s_b;
		private double? _s_l;
		private double? _s_h;
		private double? _s_x;
		private double? _s_y;
		private double? _s_z;
		private double? _s_variation_x;
		private double? _s_variation_y;
		private double? _s_variation_z;
		private double? _d_b;
		private double? _d_l;
		private double? _d_h;
		private double? _d_plane_x;
		private double? _d_plane_y;
		private double? _d_plane_h;
		private double? _d_variation_plane_x;
		private double? _d_variation_plane_y;
		private double? _d_variation_plane_h;
		private double? _rms_fix;
		private double? _fix_ratio;
		private string _flag;
		private double? _baseline_x;
		private double? _baseline_y;
		private double? _baseline_z;
		private int _calculating_type;
		private int _statistics_type;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string platform_id
		{
			set{ _platform_id=value;}
			get{return _platform_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? time
		{
			set{ _time=value;}
			get{return _time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_b
		{
			set{ _s_b=value;}
			get{return _s_b;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_l
		{
			set{ _s_l=value;}
			get{return _s_l;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_h
		{
			set{ _s_h=value;}
			get{return _s_h;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_x
		{
			set{ _s_x=value;}
			get{return _s_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_y
		{
			set{ _s_y=value;}
			get{return _s_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_z
		{
			set{ _s_z=value;}
			get{return _s_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_variation_x
		{
			set{ _s_variation_x=value;}
			get{return _s_variation_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_variation_y
		{
			set{ _s_variation_y=value;}
			get{return _s_variation_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? s_variation_z
		{
			set{ _s_variation_z=value;}
			get{return _s_variation_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_b
		{
			set{ _d_b=value;}
			get{return _d_b;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_l
		{
			set{ _d_l=value;}
			get{return _d_l;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_h
		{
			set{ _d_h=value;}
			get{return _d_h;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_plane_x
		{
			set{ _d_plane_x=value;}
			get{return _d_plane_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_plane_y
		{
			set{ _d_plane_y=value;}
			get{return _d_plane_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_plane_h
		{
			set{ _d_plane_h=value;}
			get{return _d_plane_h;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_variation_plane_x
		{
			set{ _d_variation_plane_x=value;}
			get{return _d_variation_plane_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_variation_plane_y
		{
			set{ _d_variation_plane_y=value;}
			get{return _d_variation_plane_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? d_variation_plane_h
		{
			set{ _d_variation_plane_h=value;}
			get{return _d_variation_plane_h;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? rms_fix
		{
			set{ _rms_fix=value;}
			get{return _rms_fix;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? fix_ratio
		{
			set{ _fix_ratio=value;}
			get{return _fix_ratio;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string flag
		{
			set{ _flag=value;}
			get{return _flag;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? baseline_x
		{
			set{ _baseline_x=value;}
			get{return _baseline_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? baseline_y
		{
			set{ _baseline_y=value;}
			get{return _baseline_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? baseline_z
		{
			set{ _baseline_z=value;}
			get{return _baseline_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int calculating_type
		{
			set{ _calculating_type=value;}
			get{return _calculating_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int statistics_type
		{
			set{ _statistics_type=value;}
			get{return _statistics_type;}
		}
		#endregion Model

	}
}

