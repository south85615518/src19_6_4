﻿/**  版本信息模板在安装目录下，可自行修改。
* smos_module_gnss_device.cs
*
* 功 能： N/A
* 类 名： smos_module_gnss_device
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/22 10:46:08   N/A    初版
*
* Copyright (c) 2012 GNSS Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace GPS.Model
{
	/// <summary>
	/// smos_module_gnss_device:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class smos_module_gnss_device
	{
		public smos_module_gnss_device()
		{}
		#region Model
		private int _id;
		private string _platform_id;
		private string _name;
		private string _device_type;
		private int _type;
		private string _notation;
		private string _relating_base;
		private double? _lon;
		private double? _lat;
		private double? _alt;
		private double? _init_s_x;
		private double? _init_s_y;
		private double? _init_s_z;
		private double? _init_d_x;
		private double? _init_d_y;
		private double? _init_d_h;
		private int _communicate_type;
		private string _ip_addr;
		private int _tcp_port;
		private string _serl_port;
		private int _baud;
		//private mediumblob _image;
		private string _flag;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string platform_id
		{
			set{ _platform_id=value;}
			get{return _platform_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string device_type
		{
			set{ _device_type=value;}
			get{return _device_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string notation
		{
			set{ _notation=value;}
			get{return _notation;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string relating_base
		{
			set{ _relating_base=value;}
			get{return _relating_base;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? lon
		{
			set{ _lon=value;}
			get{return _lon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? lat
		{
			set{ _lat=value;}
			get{return _lat;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? alt
		{
			set{ _alt=value;}
			get{return _alt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? init_s_x
		{
			set{ _init_s_x=value;}
			get{return _init_s_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? init_s_y
		{
			set{ _init_s_y=value;}
			get{return _init_s_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? init_s_z
		{
			set{ _init_s_z=value;}
			get{return _init_s_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? init_d_x
		{
			set{ _init_d_x=value;}
			get{return _init_d_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? init_d_y
		{
			set{ _init_d_y=value;}
			get{return _init_d_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double? init_d_h
		{
			set{ _init_d_h=value;}
			get{return _init_d_h;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int communicate_type
		{
			set{ _communicate_type=value;}
			get{return _communicate_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ip_addr
		{
			set{ _ip_addr=value;}
			get{return _ip_addr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int tcp_port
		{
			set{ _tcp_port=value;}
			get{return _tcp_port;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string serl_port
		{
			set{ _serl_port=value;}
			get{return _serl_port;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int baud
		{
			set{ _baud=value;}
			get{return _baud;}
		}
        ///// <summary>
        ///// 
        ///// </summary>
        //public mediumblob image
        //{
        //    set{ _image=value;}
        //    get{return _image;}
        //}
		/// <summary>
		/// 
		/// </summary>
		public string flag
		{
			set{ _flag=value;}
			get{return _flag;}
		}
		#endregion Model

	}
}

