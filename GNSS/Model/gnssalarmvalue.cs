﻿/**  版本信息模板在安装目录下，可自行修改。
* gnssalarmvalue.cs
*
* 功 能： N/A
* 类 名： gnssalarmvalue
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/30 14:08:26   N/A    初版
*
* Copyright (c) 2012 GPS Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace GPS.Model
{
	/// <summary>
	/// gnssalarmvalue:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gnssalarmvalue
	{
		public gnssalarmvalue()
		{}
		#region Model
		private string _name;
		private double _this_x;
		private double _this_y;
		private double _this_z;
		private double _ac_x;
		private double _ac_y;
		private double _ac_z;
		private int _xmno;
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_x
		{
			set{ _this_x=value;}
			get{return _this_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_y
		{
			set{ _this_y=value;}
			get{return _this_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double this_z
		{
			set{ _this_z=value;}
			get{return _this_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_x
		{
			set{ _ac_x=value;}
			get{return _ac_x;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_y
		{
			set{ _ac_y=value;}
			get{return _ac_y;}
		}
		/// <summary>
		/// 
		/// </summary>
		public double ac_z
		{
			set{ _ac_z=value;}
			get{return _ac_z;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

