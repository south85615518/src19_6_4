﻿/**  版本信息模板在安装目录下，可自行修改。
* smos_module_gnss_device.cs
*
* 功 能： N/A
* 类 名： smos_module_gnss_device
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/22 10:46:08   N/A    初版
*
* Copyright (c) 2012 GNSS Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.Odbc;
using System.Collections.Generic;
using SqlHelpers;
//Please add references
namespace GPS.DAL
{
	/// <summary>
	/// 数据访问类:smos_module_gnss_device
	/// </summary>
	public partial class smos_module_gnss_device
	{
        public database db = new database();
        public bool gnsspointnameload(int xmno, out List<string> pointlist)
        {
            //string dbname = querysql.queryaccessdbstring("select dbname from xmconnect where xmno =" + xmno);
            OdbcConnection conn = db.GetStanderConn(xmno);
            string sql = "select DISTINCT(point_name) from gnsspoint where  xmno = "+xmno+"";
            pointlist = querysql.querystanderlist(sql,conn);
            return true;
        }
	}
}

