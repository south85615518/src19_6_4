﻿/**  版本信息模板在安装目录下，可自行修改。
* gnssdatareport.cs
*
* 功 能： N/A
* 类 名： gnssdatareport
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/31 11:41:00   N/A    初版
*
* Copyright (c) 2012 Gauge Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
using System.Data;
using Tool;
namespace GPS.DAL
{
	/// <summary>
	/// gnssdatareport:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class gnssdatareport
	{
        public static database db = new database();
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList( List<string>pointnamelist,out List<GPS.Model.gnssdatareport> li)
        {
            li = new List<GPS.Model.gnssdatareport>();
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            strSql.AppendFormat(@"select point_name,this_x,this_y,this_z,l_x,l_y,l_z,x,y,z,time,platform_id  from gnssdatareport
where point_name in ('"+string.Join("','",pointnamelist)+@"')
order by time desc,point_name desc  limit 0,{0}",pointnamelist.Count);
            
            
            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            if (ds == null) return false;
            int i = 0;

            List<string> pointhashtable = new List<string>();
            while (i < ds.Tables[0].Rows.Count && i < pointnamelist.Count)
            {
                if (!pointhashtable.Contains(ds.Tables[0].Rows[i]["point_name"].ToString()))
                    pointhashtable.Add(ds.Tables[0].Rows[i]["point_name"].ToString());
                else
                    break;
                li.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
                
            }
            return true;
        }

        public bool GetAlarmTableCont( List<string> pointnamelist, out int cont)
        {
            cont = 0;

            //StringBuilder strSql = new StringBuilder();
            //OdbcSQLHelper.Conn = db.GetStanderConn(xmno);
            //strSql.Append("select point_name,holedepth,deep,predeep,thisdeep,acdeep,rap,xmno,remark,times,time ");
            //strSql.Append(" FROM dtudata_tmp where xmno = @xmno ");
            StringBuilder strSql = new StringBuilder();
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            strSql.AppendFormat(@"select count(1)  from gnssdatareport
where point_name in ('" + string.Join("','", pointnamelist) + @"')
order by time desc,point_name desc  limit 0,{0}", pointnamelist.Count);
            //OdbcParameter[] parameters = {

            //        new OdbcParameter("@xmno", OdbcType.Int,11)};

            //parameters[0].Value = xmno;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString());
            //DataTable dt  = querybkgsql.
            if (obj == null) return false;
            cont = int.Parse(obj.ToString());
            return true;
        }
      
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public GPS.Model.gnssdatareport DataRowToModel(DataRow row)
        {
            GPS.Model.gnssdatareport model = new GPS.Model.gnssdatareport();
            if (row != null)
            {
                if (row["point_name"] != null)
                {
                    model.point_name = row["point_name"].ToString();
                }
                model.this_x =(double)( Math.Round(decimal.Parse(row["this_x"].ToString()), 4,MidpointRounding.AwayFromZero));
                model.this_y = (double)(Math.Round(decimal.Parse(row["this_y"].ToString()), 4, MidpointRounding.AwayFromZero));
                model.this_z = (double)(Math.Round(decimal.Parse(row["this_z"].ToString()), 4, MidpointRounding.AwayFromZero));
                model.l_x = (double)(Math.Round(decimal.Parse(row["l_x"].ToString()), 4, MidpointRounding.AwayFromZero));
                model.l_y = (double)(Math.Round(decimal.Parse(row["l_y"].ToString()), 4, MidpointRounding.AwayFromZero));
                model.l_z = (double)(Math.Round(decimal.Parse(row["l_z"].ToString()), 4, MidpointRounding.AwayFromZero));
                model.x = (double)(Math.Round(decimal.Parse(row["x"].ToString()), 4, MidpointRounding.AwayFromZero));
                model.y = (double)(Math.Round(decimal.Parse(row["y"].ToString()), 4, MidpointRounding.AwayFromZero));
                model.z = (double)(Math.Round(decimal.Parse(row["z"].ToString()), 4, MidpointRounding.AwayFromZero));
                if (row["time"] != null && row["time"].ToString() != "")
                {
                    model.time = DateTime.Parse(row["time"].ToString());
                }
                if (row["platform_id"] != null && row["platform_id"].ToString() != "")
                {
                    model.platform_id = int.Parse(row["platform_id"].ToString());
                }
            }
            return model;
        }
        public bool Delete(string pointname,DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            string sql = "delete a ,b from smos_module_gnss_data a ,gnssdatareport b  where  a.name =b.point_name and a.time = b.time and a.name = '"+pointname+"' and a.time = '"+dt+"'  ";
            ExceptionLog.DTUPortInspectionWrite(sql);
            int cont = OdbcSQLHelper.ExecuteNonQuery(CommandType.Text,sql);
            return cont > 0 ? true : false;
        }

        public bool PointNewestDateTimeGet(string pointname, out DateTime dt)
        {
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  max(time)  from gnssdatareport  where  point_name = @point_name  ");
            OdbcParameter[] parameters = {
                    new OdbcParameter("@point_name", OdbcType.VarChar,120)
							};
            parameters[0].Value = pointname;
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) { dt = new DateTime(); return false; }
            dt = Convert.ToDateTime(obj); return true;

        }

        public bool GetModel(string pointname, DateTime dt, out GPS.Model.gnssdatareport model)
        {
            model = null;
            StringBuilder strSql = new StringBuilder(256);
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            strSql.AppendFormat(@"select point_name,this_x,this_y,this_z,l_x,l_y,l_z,x,y,z,time,platform_id  from gnssdatareport
where point_name ='"+pointname+"'  and  time ='"+dt+"'   ");

            DataSet ds = OdbcSQLHelper.Query(strSql.ToString());
            while (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            return false;
        }
	}
}

