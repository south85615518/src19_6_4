﻿/**  版本信息模板在安装目录下，可自行修改。
* smos_module_gnss_data.cs
*
* 功 能： N/A
* 类 名： smos_module_gnss_data
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/1/22 10:46:07   N/A    初版
*
* Copyright (c) 2012 GNSS Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using SqlHelpers;
using System.Data.Odbc;
//Please add references
namespace GPS.DAL
{
	/// <summary>
	/// 数据访问类:smos_module_gnss_data
	/// </summary>
	public partial class smos_module_gnss_data
	{
        public static database db = new database(); 
        public bool MaxTime(string pointnamestr,out DateTime maxTime)
        {
            maxTime = new DateTime();
            OdbcSQLHelper.Conn = db.GetRemoteConn("smos_client");
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select max(time) from  gnssdatareport  where  point_name in "+ string.Format("('{0}')", pointnamestr.Replace(",","','")));

            //OdbcParameter[] parameters = { new OdbcParameter("@pointnamestr",OdbcType.VarChar,200) };

            //parameters[0].Value = string.Format("{0}'", pointnamestr.Replace(",","','"));
            object obj = OdbcSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            if (obj == null) return false;
            maxTime = Convert.ToDateTime(obj);
            return true;
        }
        
	}
}

