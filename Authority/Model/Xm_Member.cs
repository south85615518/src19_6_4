﻿/**  版本信息模板在安装目录下，可自行修改。
* Xm_Member.cs
*
* 功 能： N/A
* 类 名： Xm_Member
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Authority.Model
{
	/// <summary>
	/// Xm_Member:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Xm_Member
	{
		public Xm_Member()
		{}
		#region Model
		private string _xmno;
		private string _memberno;
		/// <summary>
		/// 
		/// </summary>
		public string xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string memberno
		{
			set{ _memberno=value;}
			get{return _memberno;}
		}
		#endregion Model

	}
}

