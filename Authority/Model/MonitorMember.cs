﻿/**  版本信息模板在安装目录下，可自行修改。
* MonitorMember.cs
*
* 功 能： N/A
* 类 名： MonitorMember
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Authority.Model
{
	/// <summary>
	/// MonitorMember:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class MonitorMember
	{
		public MonitorMember()
		{}
		#region Model
		private string _user;
		private string _pass;
		private string _role;
		private string _position;
		private string _tel;
		private string _email;
		private string _unit_call;
		private string _name;
		private int _userno;
		private string _id;
        private bool _surveybase;

        public bool surveybase
        {
            get { return _surveybase; }
            set { _surveybase = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string user
		{
			set{ _user=value;}
			get{return _user;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pass
		{
			set{ _pass=value;}
			get{return _pass;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string role
		{
			set{ _role=value;}
			get{return _role;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string position
		{
			set{ _position=value;}
			get{return _position;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string tel
		{
			set{ _tel=value;}
			get{return _tel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string unit_call
		{
			set{ _unit_call=value;}
			get{return _unit_call;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int userNO
		{
			set{ _userno=value;}
			get{return _userno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		#endregion Model

	}
}

