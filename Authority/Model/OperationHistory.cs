﻿/**  版本信息模板在安装目录下，可自行修改。
* OperationHistory.cs
*
* 功 能： N/A
* 类 名： OperationHistory
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/22 14:17:15   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Authority.Model
{
	/// <summary>
	/// OperationHistory:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class OperationHistory
	{
		public OperationHistory()
		{}
		#region Model
		private int _id;
		private string _uintname;
		private string _dbname;
		private int _xmno;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string uintName
		{
			set{ _uintname=value;}
			get{return _uintname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dbname
		{
			set{ _dbname=value;}
			get{return _dbname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		#endregion Model

	}
}

