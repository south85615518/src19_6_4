﻿/**  版本信息模板在安装目录下，可自行修改。
* xmconnect.cs
*
* 功 能： N/A
* 类 名： xmconnect
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/14 11:18:32   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Authority.Model
{
	/// <summary>
	/// xmconnect:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class xmconnect
	{
		public xmconnect()
		{}
		#region Model
		private int _xmno;
		private string _xmname;
		private string _jd;
		private string _wd;
		private string _dbname;
		private string _type;
		private string _jcpmurl;
		private string _createman;
		private string _company;
		private string _xmaddress;
		private string _pro;
		private string _city;
		private string _country;
		private string _jcdw;
		private string _delegatedw;
		private string _telphone;
		private string _xmbz;
		private string _jcpgstartdate;
		private string _jcpgenddate;
		private string _safelevel;
		private string _responseman;
		private string _buildinstrutment;
        private string _cgdbname;
        private string _xmreference;
        private int _mobileStationInteval;

        public int mobileStationInteval
        {
            get { return _mobileStationInteval; }
            set { _mobileStationInteval = value; }
        }
        public string xmreference
        {
            get { return _xmreference; }
            set { _xmreference = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xmname
		{
			set{ _xmname=value;}
			get{return _xmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jd
		{
			set{ _jd=value;}
			get{return _jd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string wd
		{
			set{ _wd=value;}
			get{return _wd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dbname
		{
			set{ _dbname=value;}
			get{return _dbname;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string cgdbname
        {
            set { _cgdbname = value; }
            get { return _cgdbname; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jcpmurl
		{
			set{ _jcpmurl=value;}
			get{return _jcpmurl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string createman
		{
			set{ _createman=value;}
			get{return _createman;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string company
		{
			set{ _company=value;}
			get{return _company;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xmaddress
		{
			set{ _xmaddress=value;}
			get{return _xmaddress;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro
		{
			set{ _pro=value;}
			get{return _pro;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string city
		{
			set{ _city=value;}
			get{return _city;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string country
		{
			set{ _country=value;}
			get{return _country;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jcdw
		{
			set{ _jcdw=value;}
			get{return _jcdw;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string delegateDw
		{
			set{ _delegatedw=value;}
			get{return _delegatedw;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string telphone
		{
			set{ _telphone=value;}
			get{return _telphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string xmBz
		{
			set{ _xmbz=value;}
			get{return _xmbz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jcpgStartDate
		{
			set{ _jcpgstartdate=value;}
			get{return _jcpgstartdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jcpgEndDate
		{
			set{ _jcpgenddate=value;}
			get{return _jcpgenddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string safeLevel
		{
			set{ _safelevel=value;}
			get{return _safelevel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ResponseMan
		{
			set{ _responseman=value;}
			get{return _responseman;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string buildInstrutment
		{
			set{ _buildinstrutment=value;}
			get{return _buildinstrutment;}
		}
		#endregion Model

	}
}

