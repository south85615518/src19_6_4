﻿/**  版本信息模板在安装目录下，可自行修改。
* UnitMember.cs
*
* 功 能： N/A
* 类 名： UnitMember
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Authority.Model
{
	/// <summary>
	/// UnitMember:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class UnitMember
	{
		public UnitMember()
		{}
		#region Model
		private string _username;
		private string _pass;
		private string _role;
		private string _position;
		private string _tel;
		private string _email;
		private string _zczsmc;
		private string _zczsbh;
		private string _zczs;
		private string _sgzsmc;
		private string _sgzsbh;
		private string _sgzs;
		private int _memberno;
		private string _id;
		private string _unitno;
		private string __call;
        private string _datamodify;
        private bool _surveybase;

        public bool surveybase
        {
            get { return _surveybase; }
            set { _surveybase = value; }
        }
        public string datamodify
        {
            get { return _datamodify; }
            set { _datamodify = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string userName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pass
		{
			set{ _pass=value;}
			get{return _pass;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string role
		{
			set{ _role=value;}
			get{return _role;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string position
		{
			set{ _position=value;}
			get{return _position;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string tel
		{
			set{ _tel=value;}
			get{return _tel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zczsmc
		{
			set{ _zczsmc=value;}
			get{return _zczsmc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zczsbh
		{
			set{ _zczsbh=value;}
			get{return _zczsbh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string zczs
		{
			set{ _zczs=value;}
			get{return _zczs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sgzsmc
		{
			set{ _sgzsmc=value;}
			get{return _sgzsmc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sgzsbh
		{
			set{ _sgzsbh=value;}
			get{return _sgzsbh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string sgzs
		{
			set{ _sgzs=value;}
			get{return _sgzs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int memberno
		{
			set{ _memberno=value;}
			get{return _memberno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string unitno
		{
			set{ _unitno=value;}
			get{return _unitno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string _call
		{
			set{ __call=value;}
			get{return __call;}
		}
		#endregion Model

	}
}

