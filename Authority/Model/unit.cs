﻿/**  版本信息模板在安装目录下，可自行修改。
* unit.cs
*
* 功 能： N/A
* 类 名： unit
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/12 10:07:59   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Authority.Model
{
	/// <summary>
	/// unit:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class unit
	{
		public unit()
		{}
		#region Model
		private string _unitname;
		private string _pro;
		private string _city;
		private string _country;
		private string _addr;
		private string _tel;
		private string _email;
		private string _natrue;
		private string _linkman;
		private string _aptitude;
		private string _police;
		private string _taxproof;
		private string _state;
		private DateTime? _createtime;
		private string _dbname;
        private string _cgdbname;
		/// <summary>
		/// 
		/// </summary>
		public string unitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro
		{
			set{ _pro=value;}
			get{return _pro;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string city
		{
			set{ _city=value;}
			get{return _city;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string country
		{
			set{ _country=value;}
			get{return _country;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string addr
		{
			set{ _addr=value;}
			get{return _addr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string tel
		{
			set{ _tel=value;}
			get{return _tel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string natrue
		{
			set{ _natrue=value;}
			get{return _natrue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string linkman
		{
			set{ _linkman=value;}
			get{return _linkman;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string aptitude
		{
			set{ _aptitude=value;}
			get{return _aptitude;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string police
		{
			set{ _police=value;}
			get{return _police;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string taxproof
		{
			set{ _taxproof=value;}
			get{return _taxproof;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string state
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? createTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dbname
		{
			set{ _dbname=value;}
			get{return _dbname;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string cgdbname
        {
            set { _cgdbname = value; }
            get { return _cgdbname; }
        }
		#endregion Model

	}
}

