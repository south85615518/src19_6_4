﻿/**  版本信息模板在安装目录下，可自行修改。
* xmreference.cs
*
* 功 能： N/A
* 类 名： xmreference
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/30 16:20:45   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Authority.Model
{
	/// <summary>
	/// xmreference:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class xmreference
	{
		public xmreference()
		{}
		#region Model
		private int _xmno;
		private string _client_xmname;

		/// <summary>
		/// 
		/// </summary>
		public int xmno
		{
			set{ _xmno=value;}
			get{return _xmno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string client_xmname
		{
			set{ _client_xmname=value;}
			get{return _client_xmname;}
		}
		#endregion Model

	}
}

