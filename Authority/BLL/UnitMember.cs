﻿/**  版本信息模板在安装目录下，可自行修改。
* UnitMember.cs
*
* 功 能： N/A
* 类 名： UnitMember
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using Authority.Common;
using Authority.Model;
using System.Web.UI.WebControls;
using Tool;
namespace Authority.BLL
{
    /// <summary>
    /// UnitMember
    /// </summary>
    public partial class UnitMember
    {
        private readonly Authority.DAL.UnitMember dal = new Authority.DAL.UnitMember();
        public Authority.BLL.member memberbll = new Authority.BLL.member();
        public UnitMember()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string userId, string pass, out Authority.Model.UnitMember model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.Exists(userId, pass, out model))
                {
                    mssg = "监测员成功";
                    return true;
                }
                mssg = "监测员失败";
            }
            catch (Exception ex)
            {
                mssg = "数据库查询登录出错";
            }

            return false;
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMemberId(string name)
        {
            return dal.GetMemberId(name);
        }

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        public bool Exists(string userName, out string mssg)
        {
            try
            {
                if (dal.Exists(userName))
                {
                    mssg = "监测员" + userName + "存在";
                    return true;
                }
                else
                {
                    mssg = "监测员" + userName + "不存在";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "监测人员记录查询出错，错误信息：" + ex.Message;
                return false;
            }

        }
        public bool Exist(string name, string userId,out string mssg)
        {
            try
            {
                if (dal.Exist(name,userId))
                {
                    mssg = string.Format("名叫{0}的监测员已经存在",name);
                    return true;
                }
                else
                {
                    mssg = string.Format("名叫{0}的监测员不存在", name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取名叫{0}的监测员的信息出错，出错信息："+ex.Message, name);
                return false;
            }
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Authority.Model.UnitMember model, out string mssg)
        {
            mssg = "";
            //更具管理员的ID获取单位ID
            try
            {
                Authority.Model.member member = null;
                if (!memberbll.GetModel(model.ID, out member, out mssg))
                {

                    return false;
                }
                model.unitno = member.unitName;

                if (dal.Add(model))
                {
                    mssg = "保存成功";
                    return true;
                }
                else
                {
                    mssg = "保存失败";
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = "监测员信息添加出错" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Authority.Model.UnitMember model, out string mess)
        {
            try
            {
                if (dal.Update(model))
                {
                    mess = "成功";
                    return true;
                }
                else
                {
                    mess = "失败";
                    return false;
                }
                //if (result) mess.Text = "保存成功!"; else mess.Text = "保存失败!";
                //return result;
            }
            catch (Exception ex)
            {
                mess = "监测人员更新出错，错误信息：" + ex.Message;
                return false;
            }

        }

        public bool SurveyBaseExchange(string userId, bool surveybase, out string mssg)
        {
            try
            {
                if (dal.SurveyBaseExchange(userId, surveybase))
                {
                    mssg = string.Format("切换监测员{0}的项目数据源成功", userId);
                    return true;
                }
                else
                {
                    mssg = string.Format("切换监测员{0}的项目数据源失败", userId);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("切换监测员{0}的项目数据源出错,错误信息:" + ex.Message, userId);
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int memberno,out string mssg)
        {
            mssg = "";
            try {
                if (dal.Delete(memberno))
                {
                    mssg = string.Format("删除编号为{0}的监测员信息成功", memberno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除编号为{0}的监测员信息失败", memberno);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("删除编号为{0}的监测员信息出错,错误信息："+ex.Message, memberno);
                return false;
            }
        }
        public bool XmUnitMemberLoad(string xmname, out string monitorPeople, out string mssg)
        {
            monitorPeople = "";
            mssg = "";
            try
            {
                if (dal.XmUnitMemberLoad(xmname, out monitorPeople))
                {
                    mssg = "成功";
                    return true;
                }
                else
                {
                    mssg = "失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目的监测人员获取出错,出错信息" + ex.Message;

                return false;
            }
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool LoadList(string userID, out DataSet ds, out string mssg)
        {
            ds = null;
            try
            {
                if (dal.GetList(userID, out ds))
                {
                    mssg = "获取管理员" + userID + "的监测员列表成功";
                    return true;
                }
                else
                {
                    mssg = "获取管理员" + userID + "的监测员列表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("获取管理员" + userID + "的监测员列表出错！错误信息" + ex.Message);
                mssg = "监测员列表加载出错！错误信息" + ex.Message;
                return false;
            }


        }



        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool DeleteList(string membernolist )
        //{
        //    return dal.DeleteList(membernolist );
        //}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int memberno,out Authority.Model.UnitMember model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(memberno, out model))
                {
                    mssg = string.Format("获取监测员{0}的个人信息成功", memberno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取监测员{0}的个人信息失败", memberno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取监测员{0}的个人信息出错，错误信息{1}", memberno,ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string userID, out Authority.Model.UnitMember model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(userID, out model))
                {
                    mssg = string.Format("获取监测员{0}的个人信息成功", userID);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取监测员{0}的个人信息失败", userID);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取监测员{0}的个人信息出错，错误信息{1}", userID, ex.Message);
                return false;
            }
        }


        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public Authority.Model.UnitMember GetModelByCache(int memberno)
        //{

        //    string CacheKey = "UnitMemberModel-" + memberno;
        //    object objModel = Authority.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(memberno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = Authority.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                Authority.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch { }
        //    }
        //    return (Authority.Model.UnitMember)objModel;
        //}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(string strWhere, out DataSet ds, out string mssg)
        {
            ds = null;
            try
            {
                if (dal.GetList(strWhere, out ds))
                {
                    mssg = "获取监测人员表成功";
                    return true;
                }
                else
                {
                    mssg = "获取监测人员表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取监测人员表出错，出错信息：" + ex.Message;
                return false;
                throw (ex);
            }


        }

        /// <summary>
        /// 获得监测员所有项目
        /// </summary>
        public bool GetUnitMemberNameList(string userName, out List<string> xmnameList, out string mssg)
        {
            xmnameList = null;
            try
            {
                if (dal.GetUnitMemberNameList(userName, out xmnameList))
                {
                    mssg = "获取监测员项目列表成功";
                    return true;
                }
                else
                {
                    mssg = "获取监测员项目列表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取监测员项目列表出错，错误信息" + ex.Message;
                return false;
            }

        }

        /// <summary>
        /// 获得监测员所有项目
        /// </summary>
        public bool GetUnitMemberXmDb(string userName, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.GetUnitMemberXmDb(userName, out dt))
                {
                    mssg = "获取监测员项目表成功";
                    return true;
                }
                else
                {
                    mssg = "获取监测员项目表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取监测员项目表出错，错误信息" + ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 多条件查询获得监测员项目表
        /// </summary>
        public bool MutilConditionUnitMemberXmTableFilter(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area, out List<Authority.Model.xmconnect> xmconnectlist, out string mssg)
        {

            xmconnectlist = null;
            try
            {
                if (dal.MutilConditionUnitMemberXmTableFilter(userID, gcjb, gclx, gcname, pro, city, area, out  xmconnectlist))
                {
                    mssg = "多条件查询获取监测员项目表成功";
                    return true;
                }
                else
                {
                    mssg = "多条件查询获取监测员项目表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "多条件查询获取监测员项目表出错，错误信息" + ex.Message;
                return false;
            }

        }

        public void recordNumSet(Label currentRecord, Label totalRecord, GridView gv, int totalCont)
        {
            int pageSize = gv.PageSize;
            int pageIndex = gv.PageIndex;
            totalRecord.Text = totalCont.ToString();
            //如果是最后一页
            currentRecord.Text = pageIndex >= gv.PageCount - 1 ? pageSize * pageIndex + 1 + "-" + totalCont.ToString() : pageSize * pageIndex + 1 + "-" + (pageSize * (pageIndex + 1)).ToString();
        }

        public bool XmMonitorListGet(int xmno,out List<Authority.Model.UnitMember> lmm,out string mssg)
        {
            lmm = new List<Model.UnitMember>();
            mssg = "";
            try
            {
                if (dal.XmMonitorListGet(xmno, out lmm))
                {
                    mssg = string.Format("获取项目编号{0}的监测人员信息成功",xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的监测人员信息失败",xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的监测人员信息出错,错误信息:"+ex.Message,xmno);
                return false;
            }
        }


        public bool sms(int memberno, out string mssg)
        {
            try
            {
                if (dal.sms(memberno))
                {
                    mssg = string.Format("监测员{0}短信次数处理成功", memberno);
                    return true;
                }
                else
                {
                    mssg = string.Format("监测员{0}短信次数处理失败", memberno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("监测员{0}短信次数处理出错，错误信息：" + ex.Message, memberno);
                return false;
            }
        }





        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.UnitMember> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.UnitMember> DataTableToList(DataTable dt)
        //{
        //    List<Authority.Model.UnitMember> modelList = new List<Authority.Model.UnitMember>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        Authority.Model.UnitMember model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

