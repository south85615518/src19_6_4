﻿/**  版本信息模板在安装目录下，可自行修改。
* Xm_Monitor.cs
*
* 功 能： N/A
* 类 名： Xm_Monitor
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using Authority.Common;
using Authority.Model;
namespace Authority.BLL
{
	/// <summary>
	/// Xm_Monitor
	/// </summary>
	public partial class Xm_Monitor
	{
		private readonly Authority.DAL.Xm_Monitor dal=new Authority.DAL.Xm_Monitor();
		public Xm_Monitor()
		{}
		#region  BasicMethod
		

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Authority.Model.Xm_Monitor model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = "项目和监督员关系记录添加成功";
                    return true;
                }
                else
                {
                    mssg = "项目和监督员关系记录添加失败";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目和监督员关系记录添加出错，错误信息" + ex.Message;
                return false;
            }
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Authority.Model.Xm_Monitor model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string xmno)
		{
			
			return dal.Delete(xmno);
		}
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string xmname,out string mssg)
        {
            try
            {
                if (dal.DeleteList(xmname))
                {
                    mssg = "删除与" + xmname + "关联的监督员信息成功";
                    return true;
                }
                else
                {
                    mssg = "删除与" + xmname + "关联的监督员信息失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "删除与" + xmname + "关联的监督员信息出错，错误信息" + ex.Message;
                return false;
            }
        }

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public Authority.Model.Xm_Monitor GetModel(string xmno)
        //{
			
        //    return dal.GetModel(xmno);
        //}

        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public Authority.Model.Xm_Monitor GetModelByCache(string xmno)
        //{
			
        //    string CacheKey = "Xm_MonitorModel-" + xmno;
        //    object objModel = Authority.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(xmno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = Authority.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                Authority.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (Authority.Model.Xm_Monitor)objModel;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    return dal.GetList(strWhere);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.Xm_Monitor> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.Xm_Monitor> DataTableToList(DataTable dt)
        //{
        //    List<Authority.Model.Xm_Monitor> modelList = new List<Authority.Model.Xm_Monitor>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        Authority.Model.Xm_Monitor model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

