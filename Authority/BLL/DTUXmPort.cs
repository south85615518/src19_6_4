﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using SqlHelpers;

namespace Authority.BLL
{
    public class DTUXmPort
    {
        private Authority.DAL.DTUXmPort dal = new Authority.DAL.DTUXmPort();
        public bool GetDTUPortXm(int port,out Authority.Model.DTUXmPort model,out string mssg )
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetDTUPortXm(port, out model))
                {
                    mssg = string.Format("成功获取到端口{0}的项目编号为{1}", port, model.xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("成功获取到端口{0}的项目编号失败", port);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("成功获取到端口{0}的项目编号出错,错误信息:"+ex.Message, port);
                return false;
            }
        }
        public bool GetDTUPortXm(int port, out string xmname, out string mssg)
        {
            xmname = null;
            mssg = "";
            try
            {
                if (dal.GetDTUPortXm(port, out xmname))
                {
                    mssg = string.Format("成功获取到端口{0}的项目名称为{1}", port, xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("成功获取到端口{0}的项目名称失败", port);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("成功获取到端口{0}的项目名称出错,错误信息:" + ex.Message, port);
                return false;
            }
        }
        public bool DTUXmPortList(out List<string> superadministratorxmnameList,out string mssg)
        {
            superadministratorxmnameList = new List<string>();
            try
            {
                if (dal.DTUXmPortList(out superadministratorxmnameList))
                {
                    mssg = string.Format("获取DTU项目数量为{0}", superadministratorxmnameList.Count);
                    return true;
                }
                else
                {
                    mssg = "获取DTU项目失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取DTU项目出错，错误信息:"+ex.Message);
                return false;
            }
        }
        public bool FixedInclinometerXmPortList(out List<string> superadministratorxmnameList, out string mssg)
        {
            superadministratorxmnameList = new List<string>();
            try
            {
                if (dal.FixedInclinometerXmPortList(out superadministratorxmnameList))
                {
                    mssg = string.Format("获取固定测斜项目数量为{0}", superadministratorxmnameList.Count);
                    return true;
                }
                else
                {
                    mssg = "获取固定测斜项目失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取固定测斜项目出错，错误信息:" + ex.Message);
                return false;
            }
        }
        /// <summary>
        /// 获得管理员项目表
        /// </summary>
        public bool GetDTUXmTable( out DataTable dt,out string mssg)
        {
            mssg = "";
            dt = null;
            try
            {
                if (dal.GetDTUXmTable(out dt))
                {
                    mssg = string.Format("获取到项目关联端口列表记录数{0}条", dt.Rows.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目关联端口列表记录数失败");
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到项目关联端口列表记录数出错,错误信息:"+ex.Message);
                return false;
            }

        }
    }
}
