﻿/**  版本信息模板在安装目录下，可自行修改。
* MonitorMember.cs
*
* 功 能： N/A
* 类 名： MonitorMember
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using Authority.Common;
using Authority.Model;
using System.Web.UI.WebControls;
using Tool;
namespace Authority.BLL
{
	/// <summary>
	/// MonitorMember
	/// </summary>
	public partial class MonitorMember
	{
		private readonly Authority.DAL.MonitorMember dal=new Authority.DAL.MonitorMember();
        public Authority.BLL.member memberbll = new Authority.BLL.member();
		public MonitorMember()
		{}
		#region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string userId, string pass, out Authority.Model.MonitorMember model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.Exists(userId, pass, out model))
                {
                    mssg = "监督员登录成功";
                    return true;
                }
                mssg = "监督员登录失败";
            }
            catch (Exception ex)
            {
                mssg = "数据库查询登录出错,错误信息:"+ex.Message;
            }

            return false;
        }
        public bool Exist(string name, string userId, out string mssg)
        {
            try
            {
                if (dal.Exist(name, userId))
                {
                    mssg = string.Format("名叫{0}的监督员已经存在", name);
                    return true;
                }
                else
                {
                    mssg = string.Format("名叫{0}的监督员不存在", name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取名叫{0}的监督员的信息出错，出错信息：" + ex.Message, name);
                return false;
            }
        }
        /// <summary>
        /// 得到监督人员ID
        /// </summary>
        public int GetMonitorId(string name,string unitname )
        {
            return dal.GetMonitorId(name,unitname);
        }

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        public bool Exists(string userName, out string mssg)
        {
            try
            {
                if (dal.Exists(userName))
                {
                    mssg = "监督员"+userName+"存在";
                    return true;
                }
                else
                {
                    mssg = "监督员" + userName + "不存在"; ;
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "监督人员记录查询出错，错误信息：" + ex.Message;
                return false;
            }

        }
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Authority.Model.MonitorMember model,out string mssg)
		{

            try
            {
                if (dal.Add(model))
                {
                    mssg = "保存成功!";
                    return true;
                }
                else
                {
                    mssg = "保存失败";
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = "保存出错，错误信息:"+ex.Message;
                return false;
            }
            
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Authority.Model.MonitorMember model, out string mssg)
		{
            try
            {
                if (dal.Update(model))
                {
                    mssg = "保存成功!";
                    return true;
                }
                else
                {
                    mssg = "保存失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "更新出错，错误信息:" + ex.Message;
                return false;
            }
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int userNO)
		{
			
			return dal.Delete(userNO);
		}
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string xmname)
        {
            return dal.DeleteList(xmname);
        }

        /// <summary>
        /// 得到项目的监督员列表
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="ls"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool GetModelList(int xmno, out List<Authority.Model.MonitorMember> ls, out string mssg)
        {
            mssg = "";
            ls = null;
            try
            {
                if (dal.GetModelList(xmno, out ls))
                {
                    mssg = string.Format("获取项目编号{0}的{1}监督员信息成功",xmno,ls.Count );
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}监督员信息失败", xmno);
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}监督员信息出错，错误信息{1}", xmno, ex.Message);
                return false;
            }

            return false;
 
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int userNO,out  Authority.Model.MonitorMember model,out string mssg )
        {
            model = null;
            try{
                if (dal.GetModel(userNO, out model))
                {
                    mssg = string.Format("获取监督员{0}信息成功", userNO);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取监督员{0}信息失败", userNO);
                    return true;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("获取监督员{0}信息出错，错误信息{1}", userNO,ex.Message);
                return false;
            }
        }

        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public Authority.Model.MonitorMember GetModelByCache(int userNO)
        //{
			
        //    string CacheKey = "MonitorMemberModel-" + userNO;
        //    object objModel = Authority.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(userNO);
        //            if (objModel != null)
        //            {
        //                int ModelCache = Authority.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                Authority.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (Authority.Model.MonitorMember)objModel;
        //}
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool LoadList(string userId, out DataSet ds, out string mssg)
        {
            ds = null;
            try
            {
                if (dal.GetList(userId, out ds))
                {
                    mssg = "监督员列表加载成功";
                    return true;
                }
                else
                {
                    mssg = "监督员列表加载失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("监督员列表加载出错！错误信息" + ex.Message);
                mssg = "监督员列表加载出错！错误信息" + ex.Message;
                return false;
            }


            //DataSet ds = dal.GetList(strWhere);
            //ddl.DataSource = ds;
            //ddl.DataTextField = "pos";
            ////ddl.DataMember = "pos";
            //ddl.DataValueField = "pos";
            //ddl.DataBind();

        }

        /// <summary>
        /// 项目监督人员
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="monitorPeople"></param>
        /// <param name="mssg"></param>
        /// <returns></returns>
        public bool XmUnitMonitorLoad(string xmname, out string supervisorPeoples, out string mssg)
        {
            supervisorPeoples = "";
            mssg = "";
            try
            {
                if (dal.XmMonitorMemberLoad(xmname, out supervisorPeoples))
                {
                    mssg = "成功";
                    return true;
                }
                else
                {
                    mssg = "失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目的监督人员获取出错,出错信息" + ex.Message;

                return false;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(string strWhere, out DataSet ds, out string mssg)
        {
            ds = null;
            try
            {
                if (dal.GetList(strWhere, out ds))
                {
                    mssg = "成功";
                    return true;
                }
                else
                {
                    mssg = "失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLog.ExceptionWrite("监督员列表加载出错！错误信息" + ex.Message);
                mssg = "监督员列表加载出错！错误信息" + ex.Message;
                return false;
            }

        }
        public void recordNumSet(Label currentRecord, Label totalRecord, GridView gv, int totalCont)
        {
            int pageSize = gv.PageSize;
            int pageIndex = gv.PageIndex;
            totalRecord.Text = totalCont.ToString();
            //如果是最后一页
            currentRecord.Text = pageIndex >= gv.PageCount - 1 ? pageSize * pageIndex + 1 + "-" + totalCont.ToString() : pageSize * pageIndex + 1 + "-" + (pageSize * (pageIndex + 1)).ToString();
        }
        /// <summary>
        /// 获得监督员所有项目
        /// </summary>
        public bool GetMonitorNameList(string userName, out List<string> xmnameList, out string mssg)
        {
            xmnameList = null;
            try
            {
                if (dal.GetMonitorNameList(userName, out xmnameList))
                {
                    mssg = "获取监督员项目列表成功";
                    return true;
                }
                else
                {
                    mssg = "获取监督员项目列表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取监督员项目列表出错，错误信息" + ex.Message;
                return false;
            }

        }

        /// <summary>
        /// 获得监督员项目表
        /// </summary>
        public bool GetMonitorXmDb(string userName, out DataTable dt,out string mssg)
        {

            dt = null;
            try
            {
                if (dal.GetMonitorXmDb(userName, out dt))
                {
                    mssg = "获取监督员项目表成功";
                    return true;
                }
                else
                {
                    mssg = "获取监督员项目表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取监督员项目表出错，错误信息" + ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 多条件查询获得监督员项目表
        /// </summary>
        public bool MutilConditionMonitorXmTableFilter(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area, out List<Authority.Model.xmconnect> xmconnectlist, out string mssg)
        {

            xmconnectlist = null;
            try
            {
                if (dal.MutilConditionMonitorMemberXmTableFilter(userID, gcjb, gclx, gcname, pro, city, area, out  xmconnectlist))
                {
                    mssg = "多条件查询获取监督员项目表成功";
                    return true;
                }
                else
                {
                    mssg = "多条件查询获取监督员项目表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "多条件查询获取监督员项目表出错，错误信息" + ex.Message;
                return false;
            }

        }

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.MonitorMember> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    DataTable dt = ds;
        //    //return //DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.MonitorMember> DataTableToList(DataTable dt)
        //{
        //    List<Authority.Model.MonitorMember> modelList = new List<Authority.Model.MonitorMember>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        Authority.Model.MonitorMember model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}
        public bool GetMonitorMemberXmUnit(int userno, out string unitName,out string mssg)
        {
            unitName = "";
            try
            {
                if (dal.GetMonitorMemberXmUnit(userno, out unitName))
                {
                    mssg = string.Format("监督员编号{0}的项目监测单位名是{1}", userno, unitName);
                    return true;
                }
                else
                {
                    mssg = string.Format("监督员编号{0}的项目监测单位名是{1}", userno, unitName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取监督员编号{0}的项目监测单位名出错，错误信息:"+ex.Message, userno);
                return false;
            }
        }

        public bool sms(int userno, out string mssg)
        {
            try
            {
                if (dal.sms(userno))
                {
                    mssg = string.Format("监督员{0}短信次数处理成功", userno);
                    return true;
                }
                else
                {
                    mssg = string.Format("监督员{0}短信次数处理失败", userno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("监督员{0}短信次数处理出错，错误信息：" + ex.Message, userno);
                return false;
            }
        }


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

