﻿/**  版本信息模板在安装目录下，可自行修改。
* xmconnect.cs
*
* 功 能： N/A
* 类 名： xmconnect
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/14 11:18:32   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using Authority.Common;
using Authority.Model;
using System.Web.UI.WebControls;
using Tool;
namespace Authority.BLL
{
    /// <summary>
    /// xmconnect
    /// </summary>
    public partial class xmconnect
    {
        private readonly Authority.DAL.xmconnect dal = new Authority.DAL.xmconnect();
        public xmconnect()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId(string xmname)
        {
            return dal.GetMaxId(xmname);
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string xmname, out string mssg)
        {
            try
            {
                if (dal.Exists(xmname))
                {
                    mssg = "该项目名已经存在";
                    return true;
                }
                else
                {
                    mssg = "不存在该项目名";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询该项目名出错，错误信息" + ex.Message;
                return true;
            }
        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string xmname, string company, out string mssg)
        {
            try
            {
                if (dal.Exists(xmname, company))
                {
                    mssg = "存在该项目名";
                    return true;
                }
                else
                {
                    mssg = "不存在该项目名";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "查询该项目名出错，错误信息" + ex.Message;
                return true;
            }
        }


        public bool ExistsClient_Xmname(string userId, string client_xmname,out string mssg)
        {
            try
            {
                if (dal.ExistsClient_Xmname(userId, client_xmname))
                {
                    mssg = string.Format("存在{0}监测员的客户端项目{1}映射关系", userId, client_xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("不存在{0}监测员的客户端项目{1}映射关系", userId, client_xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}监测员的客户端项目{1}映射关系出错，错误信息："+ex.Message, userId, client_xmname);
                return false;
            }
        }

        public bool ExistsXmname(string userId, string xmname,out string mssg)
        {
            try
            {
                if (dal.ExistsClient_Xmname(userId, xmname))
                {
                    mssg = string.Format("{0}监测员的项目列表中存在项目名{1}", userId, xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("{0}监测员的项目列表中不存在项目名{1}", userId, xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取{0}监测员的项目名{1}的记录出错，错误信息：" + ex.Message, userId, xmname);
                return false;
            }
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add_xm_d(int xmno, out string mssg)
        {
            mssg = "";
            try
            {
                //根据管理员ID获取公司名

                if (dal.Add_xm_d(xmno))
                {
                    mssg =string.Format("项目{0}信息备份成功",xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("项目{0}信息备份失败",xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("项目{0}信息备份出错，错误信息" + ex.Message,xmno);
                return false;
            }




        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Authority.Model.xmconnect model, out string mssg)
        {
            mssg = "";
            try
            {
                //根据管理员ID获取公司名
                
                if (dal.Add(model))
                {
                    mssg = "项目信息保存成功";
                    return true;
                }
                else
                {
                    mssg = "项目信息保存失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目信息保存出错，错误信息" + ex.Message;
                return false;
            }




        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Delete(int xmno,out string mssg)
        {
            try
            {
                if (dal.Delete(xmno))
                {
                    mssg = string.Format("删除项目编号{0}的项目信息成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除项目编号{0}的项目信息失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("删除项目编号{0}的项目信息出错,错误信息:"+ex.Message, xmno);
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Authority.Model.xmconnect model, out string mssg)
        {
            try
            {
                if (dal.Update(model))
                {
                    mssg = "项目信息更新成功";
                    return true;
                }
                else
                {
                    mssg = "项目信息更新失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目信息更新出错，错误信息" + ex.Message;
                return true;
            }
        }
        /// <summary>
        /// 获得监测员数据列表
        /// </summary>
        public void GetUnitMemberList(string strWhere, Label currentRecord, Label totalRecord, GridView gv)
        {
            DataSet ds = dal.GetUnitMemberList(strWhere);
            gv.DataSource = ds;
            gv.DataBind();
            recordNumSet(currentRecord, totalRecord, gv, ds.Tables[0].Rows.Count);

        }

        /// <summary>
        /// 获得监督员数据列表
        /// </summary>
        public void GetMonitorMemberList(string strWhere, Label currentRecord, Label totalRecord, GridView gv)
        {
            DataSet ds = dal.GetMonitorMemberList(strWhere);
            gv.DataSource = ds;
            gv.DataBind();
            recordNumSet(currentRecord, totalRecord, gv, ds.Tables[0].Rows.Count);

        }

        public bool XmUnitCreate(string xmname, out string createUnitStr, out string mssg)
        {
            createUnitStr = "";
            mssg = "";
            try
            {
                if (dal.XmUnitCreate(xmname, out createUnitStr))
                {
                    mssg = "获取项目的建设单位信息成功";
                    return true;
                }
                else
                {
                    mssg = "获取项目的建设单位信息失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取项目的建设单位信息出错,错误信息" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 获得监测员数据列表
        /// </summary>
        public bool GetTopUnitMember(string userName, out string topXmname, out string mssg)
        {
            topXmname = "";
            try
            {
                topXmname = dal.GetTopUnitMember(userName);
                mssg = "成功";
                return true;
            }
            catch (Exception ex)
            {

                mssg = "获取最新项目失败!";
                return false;
            }

        }

        public bool XmnameUpdate(string xmname, int xmno, out string mssg)
        {

            try
            {
                if (dal.XmnameUpdate(xmname, xmno))
                {
                    mssg = string.Format("修改项目{0}的名字为{1}成功", xmname, xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("修改项目{0}的名字为{1}失败", xmname, xmno);
                    return false;
                }


            }
            catch (Exception ex)
            {
                mssg = string.Format("修改项目{0}的名字为{1}出错,错误信息:" + ex.Message, xmname, xmno);
                return false;
            }

        }



        /// <summary>
        /// 获得监测员数据列表
        /// </summary>
        public bool GetTopMember(string userName, out string topXmname, out string mssg)
        {
            topXmname = "";
            try
            {
                topXmname = dal.GetTopMember(userName);
                mssg = "成功";
                return true;
            }
            catch (Exception ex)
            {

                mssg = "获取最新项目失败!";
                return false;
            }

        }
        /// <summary>
        /// 获得监测员数据列表
        /// </summary>
        public bool GetTopMonitorMember(string userName, out string topXmname, out string mssg)
        {
            topXmname = "";
            try
            {
                topXmname = dal.GetTopMonitorMember(userName);
                mssg = "成功";
                return true;
            }
            catch (Exception ex)
            {

                mssg = "获取最新项目失败!";
                return false;
            }

        }
        /// <summary>
        /// 获得监测员数据列表
        /// </summary>
        public string GetTop(string userName)
        {
            return dal.GetTop(userName);

        }

        /// <summary>
        /// 获得监督员数据列表
        /// </summary>
        public string GetMonitorMemberList(string strWhere)
        {
            return "";
        }

        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool Delete(int xmno)
        //{

        //    return dal.Delete(xmno);
        //}
        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool DeleteList(string xmnolist )
        //{
        //    return dal.DeleteList(xmnolist );
        //}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int xmno, out Authority.Model.xmconnect model, out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(xmno, out model))
                {
                    mssg = string.Format("获取项目编号{0}信息成功",xmno);

                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}信息失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}信息出错，错误信息",xmno) + ex.Message;
                return false;
            }
            finally
            {
                ExceptionLog.ExceptionWrite(mssg);
            }

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string xmname, out Authority.Model.xmconnect model, out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetModel(xmname, out model))
                {
                    mssg = "获取项目信息成功";

                    return true;
                }
                else
                {
                    mssg = "获取项目信息失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取项目信息出错，错误信息" + ex.Message;
                return false;
            }
            finally
            {
                ExceptionLog.ExceptionWrite(mssg);
            }

        }

        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public Authority.Model.xmconnect GetModelByCache(int xmno)
        //{

        //    string CacheKey = "xmconnectModel-" + xmno;
        //    object objModel = Authority.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(xmno);
        //            if (objModel != null)
        //            {
        //                int ModelCache = Authority.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                Authority.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (Authority.Model.xmconnect)objModel;
        //}


        public void recordNumSet(Label currentRecord, Label totalRecord, GridView gv, int totalCont)
        {
            int pageSize = gv.PageSize;
            int pageIndex = gv.PageIndex;
            totalRecord.Text = totalCont.ToString();
            //如果是最后一页
            currentRecord.Text = pageIndex >= gv.PageCount - 1 ? pageSize * pageIndex + 1 + "-" + totalCont.ToString() : pageSize * pageIndex + 1 + "-" + (pageSize * (pageIndex + 1)).ToString();
        }
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.xmconnect> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.xmconnect> DataTableToList(DataTable dt)
        //{
        //    List<Authority.Model.xmconnect> modelList = new List<Authority.Model.xmconnect>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        Authority.Model.xmconnect model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}


        public bool GetXmList(out List<string> xmlist, out string mssg)
        {
            xmlist = null;
            mssg = "";
            try
            {
                dal.GetXmList(out xmlist);
                mssg = string.Format("成功获取项目数{0}个", xmlist.Count);
                return true;


            }
            catch (Exception ex)
            {
                string.Format("获取项目数出错错误信息:" + ex.Message, ex.Message);
                return false;
            }

        }


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

