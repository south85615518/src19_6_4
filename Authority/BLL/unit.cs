﻿/**  版本信息模板在安装目录下，可自行修改。
* unit.cs
*
* 功 能： N/A
* 类 名： unit
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/12 10:07:59   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
//using Authority.Common;
using Authority.Model;
namespace Authority.BLL
{
	/// <summary>
	/// unit
	/// </summary>
	public partial class unit
	{
		private readonly Authority.DAL.unit dal=new Authority.DAL.unit();
		public unit()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string unitName)
		{
			return dal.Exists(unitName);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Authority.Model.unit model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Authority.Model.unit model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string unitName,out string mssg )
		{
            try
            {
                if (dal.Delete(unitName))
                {
                    mssg = unitName+"权限冻结成功";
                    return true;
                }
                else
                {
                    mssg = unitName+"权限冻结失败";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "权限冻结出错，错误信息" + ex.Message;
                return false;
            }
		}
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Active(string unitName, out string mssg)
        {

            try
            {
                if (dal.Active(unitName))
                {
                    mssg = unitName+"权限激活成功";
                    return true;
                }
                else
                {
                    mssg = unitName + "权限激活失败";
                    return true;
                }
            }
            catch (Exception ex)
            {
                mssg = "解除限制出错，错误信息" + ex.Message;
                return false;
            }
        }
        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool DeleteList(string unitNamelist )
        //{
        //    return dal.DeleteList(unitNamelist );
        //}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Authority.Model.unit GetModel(string unitName)
		{
			
			return dal.GetModel(unitName);
		}

        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public Authority.Model.unit GetModelByCache(string unitName)
        //{
			
        //    string CacheKey = "unitModel-" + unitName;
        //    object objModel = Authority.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(unitName);
        //            if (objModel != null)
        //            {
        //                int ModelCache = Authority.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                Authority.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (Authority.Model.unit)objModel;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    return dal.GetList(strWhere);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.unit> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.unit> DataTableToList(DataTable dt)
        //{
        //    List<Authority.Model.unit> modelList = new List<Authority.Model.unit>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        Authority.Model.unit model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

