﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Authority.BLL
{
    public class VibrationDeviceMap
    {
        private Authority.DAL.VibrationDeviceMap dal = new DAL.VibrationDeviceMap();

        //根据设备编号获取到项目编号
        public bool GetXmnoBySno(string sno, out string mssg, out string xmno)
        {
            xmno = null;
            mssg = null;
            try
            {
                if (dal.GetXmnoBySno(sno, out xmno))
                {
                    mssg = string.Format("查询到设备编号为{0}的项目编号为{1}", sno, xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("查询设备编号为{0}的项目编号失败", sno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("查询设备编号为{0}的项目编号出错， 错误信息为{1}", sno, ex.Message);
                return false;
            }
        }
    }
}
