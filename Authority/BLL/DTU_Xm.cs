﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using SqlHelpers;

namespace Authority.BLL
{
    public class DTU_Xm
    {
        private Authority.DAL.DTU_Xm dal = new Authority.DAL.DTU_Xm();
        public bool GetDTUXm(string dtu,out Authority.Model.DTU_Xm model,out string mssg )
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetDTUXm(dtu, out model))
                {
                    mssg = string.Format("成功获取到DTU{0}的项目编号为{1}", dtu, model.xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到DTU{0}的项目编号失败", dtu);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取到DTU{0}的项目编号出错,错误信息:"+ex.Message, dtu);
                return false;
            }
        }
        public bool GetDTUXm(string dtu, out string xmname, out string mssg)
        {
            xmname = null;
            mssg = "";
            try
            {
                if (dal.GetDTUXm(dtu, out xmname))
                {
                    mssg = string.Format("成功获取到DTU{0}的项目名称为{1}", dtu, xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("成功获取到DTU{0}的项目名称失败", dtu);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("成功获取到DTU{0}的项目名称出错,错误信息:" + ex.Message, dtu);
                return false;
            }
        }

        public bool ExistDTUNo(string dtuno,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.ExistDTUNo(dtuno))
                {
                    mssg = string.Format("dtu编号{0}已经存在", dtuno);
                    return true;
                }
                else
                {
                    mssg = string.Format("不存在dtu编号{0}的记录", dtuno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取dtu编号{0}的记录出错,错误信息:"+ex.Message, dtuno);
                return false;
            }

        }
        public bool AddDTU(Model.DTU_Xm model,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.AddDTU(model))
                {
                    mssg = string.Format("添加DTU编号{0}成功", model.DTU);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加DTU编号{0}失败", model.DTU);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("添加DTU编号{0}出错,错误信息:" + ex.Message, model.DTU);
                return false;
            }
        }

        public bool DeleteDTU(Model.DTU_Xm model,out string mssg)
        {
            mssg = "";
            try
            {
                if (dal.DeleteDTU(model))
                {
                    mssg = string.Format("删除DTU编号{0}成功", model.DTU);
                    return true;
                }
                else
                {
                    mssg = string.Format("删除DTU编号{0}失败", model.DTU);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("删除DTU编号{0}出错,错误信息:" + ex.Message, model.DTU);
                return false;
            }
        }
        public bool DTUListLoad(int xmno, out List<string> ls,out string mssg)
        {

            mssg = "";
            ls = null;
            try
            {
                if (dal.DTUListLoad(xmno,out ls))
                {
                    mssg = string.Format("加载项目编号{0}DTU列表成功", xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("加载项目编号{0}DTU列表失败", xmno);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("加载项目编号{0}DTU列表出错,错误信息:" + ex.Message, xmno);
                return false;
            }
        }
       
        
    }
}
