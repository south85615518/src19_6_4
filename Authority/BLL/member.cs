﻿/**  版本信息模板在安装目录下，可自行修改。
* member.cs
*
* 功 能： N/A
* 类 名： member
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/12 10:16:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using Tool;
using System.Text;
namespace Authority.BLL
{
    /// <summary>
    /// member
    /// </summary>
    public partial class member
    {
        private readonly Authority.DAL.member dal = new Authority.DAL.member();
        public member()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string userId, string pass, out Authority.Model.member model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.Exists(userId, pass))
                {
                    mssg = "管理员登录成功";
                    if (!GetModel(userId, out model, out mssg))
                    {
                        return false;
                    }
                    return true;
                }
                mssg = "管理员登录失败";
            }
            catch (Exception ex)
            {
                mssg = "数据库查询登录出错";
            }

            return false;
        }

        ///// <summary>
        ///// 增加一条数据
        ///// </summary>
        public bool Add(Authority.Model.member model)
        {
            return dal.Add(model);
        }

        public bool SurveyBaseExchange(string userId, bool surveybase,out string mssg)
        {
            try
            {
                if (dal.SurveyBaseExchange(userId, surveybase))
                {
                    mssg = string.Format("切换管理员{0}的项目数据源成功", userId);
                    return true;
                }
                else
                {
                    mssg = string.Format("切换管理员{0}的项目数据源失败", userId);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("切换管理员{0}的项目数据源出错,错误信息:"+ex.Message, userId);
                return false;
            }
        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Authority.Model.member model)
        {
            return dal.Update(model);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetXmList(string strWhere, out DataTable dt, out string mssg)
        {
            dt = null;
            try
            {
                if (dal.GetXmList(strWhere, out dt))
                {
                    mssg = "项目列表加载成功";
                    return true;
                }
                else
                {
                    mssg = "项目列表加载失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "项目列表加载出错，错误信息" + ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string userId)
        {

            return dal.Delete(userId);
        }

     
        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool DeleteList(string userIdlist )
        //{
        //    return dal.DeleteList(userIdlist );
        //}

        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        public bool GetModel(string userId, out Authority.Model.member model, out string mssg)
        {
            model = null;
            try
            {
                if (dal.GetModel(userId, out model))
                {
                    mssg = "获取管理员信息成功";
                    return true;
                }
                else
                {
                    mssg = "获取管理员信息失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取管理员信息出错";
                return false;
            }
        }
        //public Authority.Model.xmconnect GetXm
        ///// <summary>
        ///// 得到一个对象实体，从缓存中
        ///// </summary>
        //public Authority.Model.member GetModelByCache(string userId)
        //{

        //    string CacheKey = "memberModel-" + userId;
        //    object objModel = Authority.Common.DataCache.GetCache(CacheKey);
        //    if (objModel == null)
        //    {
        //        try
        //        {
        //            objModel = dal.GetModel(userId);
        //            if (objModel != null)
        //            {
        //                int ModelCache = Authority.Common.ConfigHelper.GetConfigInt("ModelCache");
        //                Authority.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
        //            }
        //        }
        //        catch{}
        //    }
        //    return (Authority.Model.member)objModel;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    return dal.GetList(strWhere);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.member> GetModelList(string strWhere)
        //{
        //    DataSet ds = dal.GetList(strWhere);
        //    return DataTableToList(ds.Tables[0]);
        //}
        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public List<Authority.Model.member> DataTableToList(DataTable dt)
        //{
        //    List<Authority.Model.member> modelList = new List<Authority.Model.member>();
        //    int rowsCount = dt.Rows.Count;
        //    if (rowsCount > 0)
        //    {
        //        Authority.Model.member model;
        //        for (int n = 0; n < rowsCount; n++)
        //        {
        //            model = dal.DataRowToModel(dt.Rows[n]);
        //            if (model != null)
        //            {
        //                modelList.Add(model);
        //            }
        //        }
        //    }
        //    return modelList;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetAllList()
        //{
        //    return GetList("");
        //}

        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    return dal.GetRecordCount(strWhere);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        ////public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        ////{
        //    //return dal.GetList(PageSize,PageIndex,strWhere);
        ////}
        /// <summary>
        /// 获得管理员所有项目
        /// </summary>
        public bool GetMemberNameList(string userName, out List<string> xmnameList, out string mssg)
        {
            xmnameList = null;
            try
            {
                if (dal.GetMemberNameList(userName, out xmnameList))
                {
                    mssg = "获取管理员项目列表成功";
                    return true;
                }
                else
                {
                    mssg = "获取管理员项目列表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取管理员项目列表出错，错误信息" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 获得管理员项目表
        /// </summary>
        public bool GetMemberXmTable(string userName, out DataTable dt, out string mssg)
        {

            dt = null;
            try
            {
                if (dal.GetMemberXmTable(userName, out dt))
                {
                    mssg = "获取管理员项目表成功";
                    return true;
                }
                else
                {
                    mssg = "获取管理员项目表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "获取管理员项目表出错，错误信息" + ex.Message;
                return false;
            }

        }
        /// <summary>
        /// 多条件查询获得管理员项目表
        /// </summary>
        public bool MutilConditionMemberXmTableFilter(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area, out List<Authority.Model.xmconnect> xmconnectlist, out string mssg)
        {

            xmconnectlist = null;
            try
            {
                if (dal.MutilConditionMemberXmTableFilter(userID, gcjb, gclx, gcname, pro, city, area, out  xmconnectlist))
                {
                    mssg = "多条件查询获取管理员项目表成功";
                    return true;
                }
                else
                {
                    mssg = "多条件查询获取管理员项目表失败";
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = "多条件查询获取管理员项目表出错，错误信息" + ex.Message+" 位置："+ex.StackTrace;
                return false;
            }

        }

        /// <summary>
        /// 获取项目的负责人信息
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool XmMember(string xmname, out Authority.Model.member model,out string mssg)
        {
            model = null;
            try
            {
                if (dal.XmMember(xmname, out model))
                {
                    mssg = string.Format("获取项目{0}的创建者{1}信息成功", xmname, model.userId);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目{0}的创建者{1}信息失败", xmname, model.userId);
                    return true;
                }
            }
            catch (Exception ex)
            {

                mssg = string.Format("获取项目{0}的创建者信息出错，错误信息:"+ex.Message, xmname);
                return false;
            }

        }
        public bool sms(string userid,out string mssg)
        {
            try
            {
                if (dal.sms(userid))
                {
                    mssg = string.Format("管理员{0}短信次数处理成功",userid);
                    return true;
                }
                else
                {
                    mssg = string.Format("管理员{0}短信次数处理失败", userid);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("管理员{0}短信次数处理出错，错误信息："+ex.Message, userid);
                return false;
            }
        }
        /// <summary>
        /// 获取项目管理员信息
        /// </summary>
        /// <returns></returns>
        public bool GetXmMemberModel(int xmno, out Authority.Model.member model,out string mssg)
        {
            model = null;
            mssg = "";
            try
            {
                if (dal.GetXmMemberModel(xmno, out model))
                {
                    mssg = string.Format("获取项目编号{0}的管理员信息成功",xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取项目编号{0}的管理员信息失败", xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}的管理员信息出错，错误信息:"+ex.Message, xmno);
                return false;
            }
           
        }



        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

