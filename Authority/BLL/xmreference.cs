﻿/**  版本信息模板在安装目录下，可自行修改。
* xmreference.cs
*
* 功 能： N/A
* 类 名： xmreference
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/30 16:20:45   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
namespace Authority.BLL
{
	/// <summary>
	/// xmreference
	/// </summary>
	public partial class xmreference
	{
		private readonly Authority.DAL.xmreference dal=new Authority.DAL.xmreference();
		public xmreference()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(Authority.Model.xmreference model, out string mssg)
		{
            try
            {
                if (dal.Exists(model))
                {
                    mssg = string.Format("已经存在项目编号{0}-客户端项目名称{1}映射关系", model.xmno, model.client_xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("不存在项目编号{0}-客户端项目名称{1}映射关系", model.xmno, model.client_xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("获取项目编号{0}-客户端项目名称{1}映射关系出错,错误信息：" + ex.Message, model.xmno, model.client_xmname);
                return false;
            }
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Authority.Model.xmreference model,out string mssg)
		{
            try
            {
                if (dal.Add(model))
                {
                    mssg = string.Format("添加项目编号{0} - 客户端项目名称{1}映射关系成功", model.xmno, model.client_xmname);
                    return true;
                }
                else
                {
                    mssg = string.Format("添加项目编号{0} - 客户端项目名称{1}映射关系失败", model.xmno, model.client_xmname);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("添加项目编号{0} - 客户端项目名称{1}映射关系出错,错误信息："+ex.Message, model.xmno, model.client_xmname);
                return false;
            }

		}


		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(int xmno, out string mssg)
		{
            try
            {
                if (dal.Delete(xmno))
                {
                    mssg = string.Format("清除项目编号{0}的客户端项目名称映射关系成功",xmno);
                    return true;
                }
                else
                {
                    mssg = string.Format("清除项目编号{0}的客户端项目名称映射关系失败",xmno);
                    return false;
                }
            }
            catch (Exception ex)
            {
                mssg = string.Format("清除项目编号{0}的客户端项目名称映射关系出错,错误信息:" + ex.Message,xmno);
                return true;
            }
		}

        public bool RefrenceListLoad(int xmno, out List<string> ls,out string mssg)
        {
            mssg = "";
            ls = null;
            try {
                if (dal.RefrenceListLoad(xmno, out ls))
                {
                    mssg = string.Format("获取到项目编号{0}的客户端项目列表数{1}个", xmno, ls.Count);
                    return true;
                }
                else
                {
                    mssg = string.Format("获取到项目编号{0}的客户端项目列表失败", xmno);
                    return false;
                }
            }
            catch(Exception ex)
            {
                mssg = string.Format("获取到项目编号{0}的客户端项目列表出错,错误信息："+ex.Message, xmno);
                return false;
            }
        }

        public bool XmnoGet(string client_xmname, out int xmno,out string mssg)
        {
            mssg = "";
            xmno = -1;
            try
            {
                if (dal.XmnoGet(client_xmname, out xmno))
                {
                    mssg = string.Format("获取客户端项目{0}的项目编号为{1}", client_xmname, xmno);
                    return true;
                }
                else {
                    mssg = string.Format("获取客户端项目{0}的项目编号失败", client_xmname);
                    return false;
                }

            }
            catch (Exception ex)
            {
                mssg = string.Format("获取客户端项目{0}的项目编号出错，错误信息："+ex.Message, client_xmname);
                return false;
            }
        }


		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

