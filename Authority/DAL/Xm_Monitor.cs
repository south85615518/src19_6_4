﻿/**  版本信息模板在安装目录下，可自行修改。
* Xm_Monitor.cs
*
* 功 能： N/A
* 类 名： Xm_Monitor
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.OleDb;
using SqlHelpers;
//using Authority.DBUtility;//Please add references
namespace Authority.DAL
{
    /// <summary>
    /// 数据访问类:Xm_Monitor
    /// </summary>
    public partial class Xm_Monitor
    {
        public Xm_Monitor()
        { }
        #region  BasicMethod

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        //public bool Exists(string xmno)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from Xm_Monitor");
        //    strSql.Append(" where xmno=@xmno ");
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@xmno", OleDbType.VarChar,255)			};
        //    parameters[0].Value = xmno;

        //    return DbHelperOleDb.Exists(strSql.ToString(),parameters);
        //}


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Authority.Model.Xm_Monitor model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Xm_Monitor(");
            strSql.Append("xmno,userNO)");
            strSql.Append(" values (");
            strSql.Append("@xmno,@userNO)");
            OleDbParameter[] parameters = {
					new OleDbParameter("@xmno", OleDbType.VarChar,255),
					new OleDbParameter("@userNO", OleDbType.VarChar,255),
                   
                                          };
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.userNO;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Authority.Model.Xm_Monitor model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Xm_Monitor set ");
            strSql.Append("userNO=@userNO");
            strSql.Append(" where xmno=@xmno ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userNO", OleDbType.VarChar,255),
					new OleDbParameter("@xmno", OleDbType.VarChar,255)};
            parameters[0].Value = model.userNO;
            parameters[1].Value = model.xmno;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string xmno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Xm_Monitor ");
            strSql.Append(" where xmno=@xmno ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@xmno", OleDbType.VarChar,255)			};
            parameters[0].Value = xmno;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string xmname)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete  from Xm_Monitor ");
            strSql.Append(" where Xm_Monitor.xmno = (select xmconnect.xmno from xmconnect where xmconnect.xmname='" + xmname + "') ");
            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public Authority.Model.Xm_Monitor GetModel(string xmno)
        //{

        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select xmno,userNO from Xm_Monitor ");
        //    strSql.Append(" where xmno=@xmno ");
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@xmno", OleDbType.VarChar,255)			};
        //    parameters[0].Value = xmno;

        //    Authority.Model.Xm_Monitor model=new Authority.Model.Xm_Monitor();
        //    DataSet ds=DbHelperOleDb.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        return DataRowToModel(ds.Tables[0].Rows[0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public Authority.Model.Xm_Monitor DataRowToModel(DataRow row)
        //{
        //    Authority.Model.Xm_Monitor model=new Authority.Model.Xm_Monitor();
        //    if (row != null)
        //    {
        //        if(row["xmno"]!=null)
        //        {
        //            model.xmno=row["xmno"].ToString();
        //        }
        //        if(row["userNO"]!=null)
        //        {
        //            model.userNO=row["userNO"].ToString();
        //        }
        //    }
        //    return model;
        //}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select xmno,userNO ");
        //    strSql.Append(" FROM Xm_Monitor ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    return DbHelperOleDb.Query(strSql.ToString());
        //}

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM Xm_Monitor ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.xmno desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from Xm_Monitor T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return DbHelperOleDb.Query(strSql.ToString());
        //}

        ///*
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@tblName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@fldName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@PageSize", OleDbType.Integer),
        //            new OleDbParameter("@PageIndex", OleDbType.Integer),
        //            new OleDbParameter("@IsReCount", OleDbType.Boolean),
        //            new OleDbParameter("@OrderType", OleDbType.Boolean),
        //            new OleDbParameter("@strWhere", OleDbType.VarChar,1000),
        //            };
        //    parameters[0].Value = "Xm_Monitor";
        //    parameters[1].Value = "xmno";
        //    parameters[2].Value = PageSize;
        //    parameters[3].Value = PageIndex;
        //    parameters[4].Value = 0;
        //    parameters[5].Value = 0;
        //    parameters[6].Value = strWhere;	
        //    return DbHelperOleDb.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        //}*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

