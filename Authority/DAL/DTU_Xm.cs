﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using SqlHelpers;
using System.Data.Odbc;

namespace Authority.DAL
{
    public class DTU_Xm
    {


        public bool GetDTUXm(string DTU, out Authority.Model.DTU_Xm model)
        {
            StringBuilder strSql = new StringBuilder();
            model = new Model.DTU_Xm();
            strSql.Append("select * from DTU_Xm where DTU =@DTU  ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@DTU", OleDbType.VarChar,100)
                   
                                          };
            parameters[0].Value = DTU;
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "DTU_Xm", parameters);
            if (ds.Tables[0].Rows.Count == 0) return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;
        }
        public bool GetDTUXm(string DTU, out string xmname)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select xmname from DTU_Xm,xmconnect where DTU_Xm.xmno =xmconnect.xmno and  DTU =@DTU ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@DTU", OleDbType.VarChar,50)
                   
                                          };
            parameters[0].Value = DTU;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            xmname = obj == null ? "" : obj.ToString();
            return true;
        }
        public bool ExistDTUNo(string dtuno)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select count(1) from DTU_Xm where  DTU =@DTU ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@DTU", OleDbType.VarChar,50)
                   
                                          };
            parameters[0].Value = dtuno;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return Convert.ToInt32(obj)>0?true:false;
            
        }
        public bool AddDTU(Model.DTU_Xm model)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("insert into DTU_Xm (xmno,dtu) values(@xmno,@dtu) ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@xmno", OleDbType.Integer),
                    new OleDbParameter("@dtu", OleDbType.VarChar,50)
                                          };
            parameters[0].Value = model.xmno;
            parameters[1].Value = model.DTU;
            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
            return rows>0? true:false;
        }

        public bool DeleteDTU(Model.DTU_Xm model)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("delete from  DTU_Xm where dtu=@dtu ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@dtu", OleDbType.VarChar,100)
                                          };
            parameters[0].Value = model.DTU;
            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            return rows > 0 ? true : false;
        }
        public bool DTUListLoad(int xmno,out List<string> ls)
        {
            StringBuilder strSql = new StringBuilder();

            string sql = "select dtu from DTU_Xm where xmno = @xmno";
            ls = querysql.queryaccesslist(sql);
            return true;
           
        }


        //public bool DTU_XmList(out List<string> superadministratorxmnameList)
        //{
        //    string sql = "select distinct(xmname) from DTU_Xm,xmconnect where  DTU_Xm.xmno =xmconnect.xmno and DTU_Xm.type='水位' ";
        //    superadministratorxmnameList = querysql.queryaccesslist(sql);
        //    return true;
        //}
        //public bool FixedInclinometerXmPortList(out List<string> superadministratorxmnameList)
        //{
        //    string sql = "select distinct(xmname) from DTU_Xm,xmconnect where  DTU_Xm.xmno =xmconnect.xmno and DTU_Xm.type='固定测斜' ";
        //    superadministratorxmnameList = querysql.queryaccesslist(sql);
        //    return true;
        //}

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Authority.Model.DTU_Xm DataRowToModel(DataRow row)
        {
            Authority.Model.DTU_Xm model = new Authority.Model.DTU_Xm();
            if (row != null)
            {

                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["id"] != null && row["id"].ToString() != "")
                {
                    model.ID = int.Parse(row["id"].ToString());
                }
                if (row["DTU"] != null && row["DTU"].ToString() != "")
                {
                    model.DTU = row["DTU"].ToString();
                }
            }
            return model;
        }
    }
}
