﻿/**  版本信息模板在安装目录下，可自行修改。
* unit.cs
*
* 功 能： N/A
* 类 名： unit
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/12 10:07:59   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.OleDb;
using SqlHelpers;
//using Authority.DBUtility;//Please add references
namespace Authority.DAL
{
    /// <summary>
    /// 数据访问类:unit
    /// </summary>
    public partial class unit
    {
        public unit()
        { }
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string unitName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from unit");
            strSql.Append(" where unitName=@unitName ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@unitName", OleDbType.VarChar,200)			};
            parameters[0].Value = unitName;

            return false;//0;/OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Authority.Model.unit model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into unit(");
            strSql.Append("unitName,pro,city,country,addr,tel,email,natrue,linkman,aptitude,police,taxproof,state,createTime,dbname)");
            strSql.Append(" values (");
            strSql.Append("@unitName,@pro,@city,@country,@addr,@tel,@email,@natrue,@linkman,@aptitude,@police,@taxproof,@state,@createTime,@dbname)");
            OleDbParameter[] parameters = {
					new OleDbParameter("@unitName", OleDbType.VarChar,200),
					new OleDbParameter("@pro", OleDbType.VarChar,50),
					new OleDbParameter("@city", OleDbType.VarChar,50),
					new OleDbParameter("@country", OleDbType.VarChar,200),
					new OleDbParameter("@addr", OleDbType.VarChar,200),
					new OleDbParameter("@tel", OleDbType.VarChar,50),
					new OleDbParameter("@email", OleDbType.VarChar,50),
					new OleDbParameter("@natrue", OleDbType.VarChar,50),
					new OleDbParameter("@linkman", OleDbType.VarChar,50),
					new OleDbParameter("@aptitude", OleDbType.VarChar,50),
					new OleDbParameter("@police", OleDbType.VarChar,50),
					new OleDbParameter("@taxproof", OleDbType.VarChar,50),
					new OleDbParameter("@state", OleDbType.VarChar,255),
					new OleDbParameter("@createTime", OleDbType.Date),
					new OleDbParameter("@dbname", OleDbType.VarChar,255)};
            parameters[0].Value = model.unitName;
            parameters[1].Value = model.pro;
            parameters[2].Value = model.city;
            parameters[3].Value = model.country;
            parameters[4].Value = model.addr;
            parameters[5].Value = model.tel;
            parameters[6].Value = model.email;
            parameters[7].Value = model.natrue;
            parameters[8].Value = model.linkman;
            parameters[9].Value = model.aptitude;
            parameters[10].Value = model.police;
            parameters[11].Value = model.taxproof;
            parameters[12].Value = model.state;
            parameters[13].Value = model.createTime;
            parameters[14].Value = model.dbname;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Authority.Model.unit model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update unit set ");
            strSql.Append("pro=@pro,");
            strSql.Append("city=@city,");
            strSql.Append("country=@country,");
            strSql.Append("addr=@addr,");
            strSql.Append("tel=@tel,");
            strSql.Append("email=@email,");
            strSql.Append("natrue=@natrue,");
            strSql.Append("linkman=@linkman,");
            strSql.Append("aptitude=@aptitude,");
            strSql.Append("police=@police,");
            strSql.Append("taxproof=@taxproof,");
            strSql.Append("state=@state,");
            strSql.Append("createTime=@createTime,");
            strSql.Append("dbname=@dbname");
            strSql.Append(" where unitName=@unitName ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@pro", OleDbType.VarChar,50),
					new OleDbParameter("@city", OleDbType.VarChar,50),
					new OleDbParameter("@country", OleDbType.VarChar,200),
					new OleDbParameter("@addr", OleDbType.VarChar,200),
					new OleDbParameter("@tel", OleDbType.VarChar,50),
					new OleDbParameter("@email", OleDbType.VarChar,50),
					new OleDbParameter("@natrue", OleDbType.VarChar,50),
					new OleDbParameter("@linkman", OleDbType.VarChar,50),
					new OleDbParameter("@aptitude", OleDbType.VarChar,50),
					new OleDbParameter("@police", OleDbType.VarChar,50),
					new OleDbParameter("@taxproof", OleDbType.VarChar,50),
					new OleDbParameter("@state", OleDbType.VarChar,255),
					new OleDbParameter("@createTime", OleDbType.Date),
					new OleDbParameter("@dbname", OleDbType.VarChar,255),
					new OleDbParameter("@unitName", OleDbType.VarChar,200)};
            parameters[0].Value = model.pro;
            parameters[1].Value = model.city;
            parameters[2].Value = model.country;
            parameters[3].Value = model.addr;
            parameters[4].Value = model.tel;
            parameters[5].Value = model.email;
            parameters[6].Value = model.natrue;
            parameters[7].Value = model.linkman;
            parameters[8].Value = model.aptitude;
            parameters[9].Value = model.police;
            parameters[10].Value = model.taxproof;
            parameters[11].Value = model.state;
            parameters[12].Value = model.createTime;
            parameters[13].Value = model.dbname;
            parameters[14].Value = model.unitName;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string unitName)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("update  unit ");
            strSql.Append(" set isActive=false where unitName=@unitName ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@unitName", OleDbType.VarChar,200)			};
            parameters[0].Value = unitName;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
            //strSql = new StringBuilder();

            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 解除机构权限
        /// </summary>
        public bool Active(string unitName)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("update  unit ");
            strSql.Append(" set isActive=true where unitName=@unitName ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@unitName", OleDbType.VarChar,200)			};
            parameters[0].Value = unitName;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            //strSql = new StringBuilder();

            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        ///// <summary>
        ///// 批量删除数据
        ///// </summary>
        //public bool DeleteList(string unitNamelist)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("delete from unit ");
        //    strSql.Append(" where unitName in (" + unitNamelist + ")  ");
        //    int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Authority.Model.unit GetModel(string unitName)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select unitName,pro,city,country,addr,tel,email,natrue,linkman,aptitude,police,taxproof,state,createTime,dbname,cgdbname from unit ");
            strSql.Append(" where unitName=@unitName ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@unitName", OleDbType.VarChar,200)			};
            parameters[0].Value = unitName;

            Authority.Model.unit model = new Authority.Model.unit();
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(),"unit",parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Authority.Model.unit DataRowToModel(DataRow row)
        {
            Authority.Model.unit model = new Authority.Model.unit();
            if (row != null)
            {
                if (row["unitName"] != null)
                {
                    model.unitName = row["unitName"].ToString();
                }
                if (row["pro"] != null)
                {
                    model.pro = row["pro"].ToString();
                }
                if (row["city"] != null)
                {
                    model.city = row["city"].ToString();
                }
                if (row["country"] != null)
                {
                    model.country = row["country"].ToString();
                }
                if (row["addr"] != null)
                {
                    model.addr = row["addr"].ToString();
                }
                if (row["tel"] != null)
                {
                    model.tel = row["tel"].ToString();
                }
                if (row["email"] != null)
                {
                    model.email = row["email"].ToString();
                }
                if (row["natrue"] != null)
                {
                    model.natrue = row["natrue"].ToString();
                }
                if (row["linkman"] != null)
                {
                    model.linkman = row["linkman"].ToString();
                }
                if (row["aptitude"] != null)
                {
                    model.aptitude = row["aptitude"].ToString();
                }
                if (row["police"] != null)
                {
                    model.police = row["police"].ToString();
                }
                if (row["taxproof"] != null)
                {
                    model.taxproof = row["taxproof"].ToString();
                }
                if (row["state"] != null)
                {
                    model.state = row["state"].ToString();
                }
                if (row["createTime"] != null && row["createTime"].ToString() != "")
                {
                    model.createTime = DateTime.Parse(row["createTime"].ToString());
                }
                if (row["dbname"] != null)
                {
                    model.dbname = row["dbname"].ToString();
                }
                if (row["cgdbname"] != null)
                {
                    model.cgdbname = row["cgdbname"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select unitName,pro,city,country,addr,tel,email,natrue,linkman,aptitude,police,taxproof,state,createTime,dbname ");
            strSql.Append(" FROM unit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return null;//OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
        }

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM unit ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.unitName desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from unit T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return0;/OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
        //}

        ///*
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@tblName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@fldName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@PageSize", OleDbType.Integer),
        //            new OleDbParameter("@PageIndex", OleDbType.Integer),
        //            new OleDbParameter("@IsReCount", OleDbType.Boolean),
        //            new OleDbParameter("@OrderType", OleDbType.Boolean),
        //            new OleDbParameter("@strWhere", OleDbType.VarChar,1000),
        //            };
        //    parameters[0].Value = "unit";
        //    parameters[1].Value = "unitName";
        //    parameters[2].Value = PageSize;
        //    parameters[3].Value = PageIndex;
        //    parameters[4].Value = 0;
        //    parameters[5].Value = 0;
        //    parameters[6].Value = strWhere;	
        //    return DbHelperOleDb.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        //}*/
        
        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

