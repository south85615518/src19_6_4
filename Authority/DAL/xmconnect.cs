﻿/**  版本信息模板在安装目录下，可自行修改。
* xmconnect.cs
*
* 功 能： N/A
* 类 名： xmconnect
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/14 11:18:32   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.OleDb;
using SqlHelpers;
using System.Collections.Generic;
using System;
//using Authority.DBUtility;//Please add references
namespace Authority.DAL
{
	/// <summary>
	/// 数据访问类:xmconnect
	/// </summary>
	public partial class xmconnect
	{
		public xmconnect()
		{}
		#region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId(string xmname)
        {
        
            string sql = "select xmno from xmconnect where xmname=@xmname";
            OleDbParameter[] parameters = {
					new OleDbParameter("@xmconnect", OleDbType.VarChar,255)
			};
            parameters[0].Value = xmname;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, sql, parameters);
            return (int)obj;
           
        
        }

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
		public bool Exists(string xmname)
		{
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from xmconnect");
            strSql.Append(" where xmname=@xmname");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255)
			};
            parameters[0].Value = xmname;

            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return (int)obj == 0 ? false : true;
		}
        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        public bool Exists(string xmname,string unitName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from xmconnect");
            strSql.Append(" where xmname=@xmname and company=@company");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255),
                    new OleDbParameter("@company", OleDbType.VarChar,255)
			};
            parameters[0].Value = xmname;
            parameters[1].Value = unitName;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return (int)obj == 0 ? false : true;
        }


        public bool ExistsClient_Xmname(string userId,string client_xmname)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from xm_member,xmconnect,xmreference where  xm_member.xmno = xmconnect.xmno and xmconnect.xmno = xmreference.xmno and xmreference.client_xmname='" + client_xmname + "'");
            
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            return (int)obj == 0 ? false : true;
        }

        public bool ExistsXmname(string userId, string xmname)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from xm_member,xmconnect where  xm_member.xmno = xmconnect.xmno and xmconnect.xmname='"+xmname+"'");
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            return (int)obj == 0 ? false : true;
        }

        public bool XmnameUpdate(string xmname, int xmno)
        {
            
            string sql = string.Format("update xmconnect set xmname='{0}' where xmno='{1}' ", xmname, xmno);
            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, sql);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Authority.Model.xmconnect model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into xmconnect(");
            strSql.Append("xmname,jd,wd,dbname,type,jcpmurl,createman,company,xmaddress,pro,city,country,jcdw,delegateDw,telphone,xmBz,jcpgStartDate,jcpgEndDate,safeLevel,ResponseMan,buildInstrutment,cgdbname,MobileStationInteval)");
			strSql.Append(" values (");
            strSql.Append("@xmname,@jd,@wd,@dbname,@type,@jcpmurl,@createman,@company,@xmaddress,@pro,@city,@country,@jcdw,@delegateDw,@telphone,@xmBz,@jcpgStartDate,@jcpgEndDate,@safeLevel,@ResponseMan,@buildInstrutment,@cgdbname,@MobileStationInteval)");
			OleDbParameter[] parameters = {
					new OleDbParameter("@xmname", OleDbType.VarChar,255),
					new OleDbParameter("@jd", OleDbType.VarChar,255),
					new OleDbParameter("@wd", OleDbType.VarChar,255),
					new OleDbParameter("@dbname", OleDbType.VarChar,255),
                    
					new OleDbParameter("@type", OleDbType.VarChar,255),
					new OleDbParameter("@jcpmurl", OleDbType.VarChar,255),
					new OleDbParameter("@createman", OleDbType.VarChar,255),
					new OleDbParameter("@company", OleDbType.VarChar,255),
					new OleDbParameter("@xmaddress", OleDbType.VarChar,255),
					new OleDbParameter("@pro", OleDbType.VarChar,255),
					new OleDbParameter("@city", OleDbType.VarChar,255),
					new OleDbParameter("@country", OleDbType.VarChar,255),
					new OleDbParameter("@jcdw", OleDbType.VarChar,255),
					new OleDbParameter("@delegateDw", OleDbType.VarChar,255),
					new OleDbParameter("@telphone", OleDbType.VarChar,255),
					new OleDbParameter("@xmBz", OleDbType.VarChar,255),
					new OleDbParameter("@jcpgStartDate", OleDbType.VarChar,255),
					new OleDbParameter("@jcpgEndDate", OleDbType.VarChar,255),
					new OleDbParameter("@safeLevel", OleDbType.VarChar,255),
					new OleDbParameter("@ResponseMan", OleDbType.VarChar,255),
					new OleDbParameter("@buildInstrutment", OleDbType.VarChar,255),
                    new OleDbParameter("@cgdbname", OleDbType.VarChar,255),
                    new OleDbParameter("@mobileStationInteval", OleDbType.Integer)
                                          };
			parameters[0].Value = model.xmname;
			parameters[1].Value = model.jd;
			parameters[2].Value = model.wd;
			parameters[3].Value = model.dbname;
			parameters[4].Value = model.type;
            parameters[5].Value = model.jcpmurl == null ? "" : model.jcpmurl;
			parameters[6].Value = model.createman;
			parameters[7].Value = model.company;
			parameters[8].Value = model.xmaddress;
			parameters[9].Value = model.pro;
			parameters[10].Value = model.city;
			parameters[11].Value = model.country;
			parameters[12].Value = model.jcdw;
			parameters[13].Value = model.delegateDw;
            parameters[14].Value = model.telphone == null ? "" : model.telphone;
			parameters[15].Value = model.xmBz;
			parameters[16].Value = model.jcpgStartDate;
			parameters[17].Value = model.jcpgEndDate;
			parameters[18].Value = model.safeLevel;
			parameters[19].Value = model.ResponseMan;
			parameters[20].Value = model.buildInstrutment;
            parameters[21].Value = model.cgdbname;
            parameters[22].Value = model.mobileStationInteval;
			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{

				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add_xm_d(int  xmno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("insert into xmconnect_d select * from xmconnect where xmno={0}",xmno);
           
            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Delete(int xmno)
        {
            string sql =string.Format("delete from xmconnect where xmno = {0} ",xmno);
            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,sql);
            return rows > 0 ? true : false;
        }
       
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Authority.Model.xmconnect model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update xmconnect  set  ");
			strSql.Append("jd=@jd,");
			strSql.Append("wd=@wd,");
			strSql.Append("type=@type,");
			strSql.Append("xmaddress=@xmaddress,");
			strSql.Append("pro=@pro,");
			strSql.Append("city=@city,");
			strSql.Append("country=@country,");
			strSql.Append("jcdw=@jcdw,");
			strSql.Append("delegateDw=@delegateDw,");
			strSql.Append("xmBz=@xmBz,");
			strSql.Append("jcpgStartDate=@jcpgStartDate,");
			strSql.Append("jcpgEndDate=@jcpgEndDate,");
			strSql.Append("safeLevel=@safeLevel,");
			strSql.Append("buildInstrutment=@buildInstrutment,");
            strSql.Append(" xmname=@xmname, ");
            strSql.Append(" mobileStationInteval=@mobileStationInteval  ");
			strSql.Append(" where xmno=@xmno");
			OleDbParameter[] parameters = {
                    new OleDbParameter("@xmname", OleDbType.VarChar,255),
					new OleDbParameter("@jd", OleDbType.VarChar,255),
					new OleDbParameter("@wd", OleDbType.VarChar,255),
					new OleDbParameter("@type", OleDbType.VarChar,255),
					new OleDbParameter("@xmaddress", OleDbType.VarChar,255),
					new OleDbParameter("@pro", OleDbType.VarChar,255),
					new OleDbParameter("@city", OleDbType.VarChar,255),
					new OleDbParameter("@country", OleDbType.VarChar,255),
					new OleDbParameter("@jcdw", OleDbType.VarChar,255),
					new OleDbParameter("@delegateDw", OleDbType.VarChar,255),
					new OleDbParameter("@xmBz", OleDbType.VarChar,255),
					new OleDbParameter("@jcpgStartDate", OleDbType.VarChar,255),
					new OleDbParameter("@jcpgEndDate", OleDbType.VarChar,255),
					new OleDbParameter("@safeLevel", OleDbType.VarChar,255),
					new OleDbParameter("@buildInstrutment", OleDbType.VarChar,255),
                    new OleDbParameter("@xmname", OleDbType.VarChar,255),
                    new OleDbParameter("@mobileStationInteval", OleDbType.Integer),
					new OleDbParameter("@xmno", OleDbType.Integer)};
			parameters[0].Value = model.jd;
			parameters[1].Value = model.wd;
			parameters[2].Value = model.type;
			parameters[3].Value = model.xmaddress;
			parameters[4].Value = model.pro;
			parameters[5].Value = model.city;
			parameters[6].Value = model.country;
			parameters[7].Value = model.jcdw;
			parameters[8].Value = model.delegateDw;
			parameters[9].Value = model.xmBz;
			parameters[10].Value = model.jcpgStartDate;
			parameters[11].Value = model.jcpgEndDate;
			parameters[12].Value = model.safeLevel;
			parameters[13].Value = model.buildInstrutment;
            parameters[14].Value = model.xmname;
            parameters[15].Value = model.mobileStationInteval;
            parameters[16].Value = model.xmno;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool Delete(int xmno)
        //{
			
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from xmconnect ");
        //    strSql.Append(" where xmno=@xmno");
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@xmno", OleDbType.Integer,4)
        //    };
        //    parameters[0].Value = xmno;

        //    int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        ///// <summary>
        ///// 批量删除数据
        ///// </summary>
        //public bool DeleteList(string xmnolist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from xmconnect ");
        //    strSql.Append(" where xmno in ("+xmnolist + ")  ");
        //    int rows=OleDbSQLHelper.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        public bool XmUnitCreate(string xmname,out string createUnitStr)
        {
            string sql = "select buildInstrutment from xmconnect where xmname='" + xmname + "'";
            createUnitStr = querysql.queryaccessdbstring(sql);
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int  xmno, out Authority.Model.xmconnect model)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,xmname,jd,wd,dbname,type,jcpmurl,createman,company,xmaddress,pro,city,country,jcdw,delegateDw,telphone,xmBz,jcpgStartDate,jcpgEndDate,safeLevel,ResponseMan,buildInstrutment,MobileStationInteval from xmconnect ");
            strSql.Append(" where xmno=@xmno");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@xmno", OleDbType.Integer,100)
            };
            parameters[0].Value = xmno;
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "xmconnect", parameters);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                model = null;
                return false;
            }
        }
    
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool  GetModel(string xmname,out Authority.Model.xmconnect model)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,xmname,jd,wd,dbname,type,jcpmurl,createman,company,xmaddress,pro,city,country,jcdw,delegateDw,telphone,xmBz,jcpgStartDate,jcpgEndDate,safeLevel,ResponseMan,buildInstrutment,MobileStationInteval   from xmconnect ");
            strSql.Append(" where xmname=@xmname");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@xmname", OleDbType.VarChar,100)
            };
            parameters[0].Value = xmname;
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "xmconnect", parameters);
            if (ds != null&&ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                model = null;
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Authority.Model.xmconnect DataRowToModel(DataRow row)
        {
            
            Authority.Model.xmconnect model = new Authority.Model.xmconnect();
            if (row != null)
            {
                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["xmname"] != null)
                {
                    model.xmname = row["xmname"].ToString();
                }
                if (row["jd"] != null)
                {
                    model.jd = row["jd"].ToString();
                }
                if (row["wd"] != null)
                {
                    model.wd = row["wd"].ToString();
                }
                if (row["dbname"] != null)
                {
                    model.dbname = row["dbname"].ToString();
                }
                if (row["type"] != null)
                {
                    model.type = row["type"].ToString();
                }
                if (row["jcpmurl"] != null)
                {
                    model.jcpmurl = row["jcpmurl"].ToString();
                }
                if (row["createman"] != null)
                {
                    model.createman = row["createman"].ToString();
                }
                if (row["company"] != null)
                {
                    model.company = row["company"].ToString();
                }
                if (row["xmaddress"] != null)
                {
                    model.xmaddress = row["xmaddress"].ToString();
                }
                if (row["pro"] != null)
                {
                    model.pro = row["pro"].ToString();
                }
                if (row["city"] != null)
                {
                    model.city = row["city"].ToString();
                }
                if (row["country"] != null)
                {
                    model.country = row["country"].ToString();
                }
                if (row["jcdw"] != null)
                {
                    model.jcdw = row["jcdw"].ToString();
                }
                if (row["delegateDw"] != null)
                {
                    model.delegateDw = row["delegateDw"].ToString();
                }
                if (row["telphone"] != null)
                {
                    model.telphone = row["telphone"].ToString();
                }
                if (row["xmBz"] != null)
                {
                    model.xmBz = row["xmBz"].ToString();
                }
                if (row["jcpgStartDate"] != null)
                {
                    model.jcpgStartDate = row["jcpgStartDate"].ToString();
                }
                if (row["jcpgEndDate"] != null)
                {
                    model.jcpgEndDate = row["jcpgEndDate"].ToString();
                }
                if (row["safeLevel"] != null)
                {
                    model.safeLevel = row["safeLevel"].ToString();
                }
                if (row["ResponseMan"] != null)
                {
                    model.ResponseMan = row["ResponseMan"].ToString();
                }
                if (row["buildInstrutment"] != null)
                {
                    model.buildInstrutment = row["buildInstrutment"].ToString();
                }
                if (row["MobileStationInteval"] != null && row["MobileStationInteval"].ToString() != "")
                {
                    model.mobileStationInteval = Convert.ToInt32(row["MobileStationInteval"].ToString());
                }
            }
            return model;
        }

        




        /// <summary>
        /// 获得监测员数据列表
        /// </summary>
        public DataSet GetUnitMemberList(string userName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,xmname,jd,wd,dbname,type,jcpmurl,createman,company,xmaddress,pro,city,country,jcdw,delegateDw,telphone,xmBz,jcpgStartDate,jcpgEndDate,safeLevel,ResponseMan,buildInstrutment from xmconnect,xm_member,unitmember where xmconnect.xmno =xm_member.xmno and xm_member.memberno = unitmember.memberno ");
            strSql.Append(" and unitmember.userName =@userName  order by xmconnect.xmno ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255)
			};
            parameters[0].Value = userName;
            return OleDbSQLHelper.Query(strSql.ToString(), "xmconnect");
            //return null;
        }
        /// <summary>
        /// 获得监督员数据列表
        /// </summary>
        public DataSet GetMonitorMemberList(string userName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,xmname,jd,wd,dbname,type,jcpmurl,createman,company,xmaddress,pro,city,country,jcdw,delegateDw,telphone,xmBz,jcpgStartDate,jcpgEndDate,safeLevel,ResponseMan,buildInstrutment from xmconnect,xm_monitor,monitormember where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = monitormember.userno ");
            strSql.Append(" and monitormember.user =@userName  order by xmconnect.xmno ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255)
			};
            parameters[0].Value = userName;
            return OleDbSQLHelper.Query(strSql.ToString(), "xmconnect");
            //return null;
        }
        

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM xmconnect ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.xmno desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from xmconnect T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return OleDbSQLHelper.Query(strSql.ToString());
        //}

        ///*
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@tblName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@fldName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@PageSize", OleDbType.Integer),
        //            new OleDbParameter("@PageIndex", OleDbType.Integer),
        //            new OleDbParameter("@IsReCount", OleDbType.Boolean),
        //            new OleDbParameter("@OrderType", OleDbType.Boolean),
        //            new OleDbParameter("@strWhere", OleDbType.VarChar,1000),
        //            };
        //    parameters[0].Value = "xmconnect";
        //    parameters[1].Value = "xmno";
        //    parameters[2].Value = PageSize;
        //    parameters[3].Value = PageIndex;
        //    parameters[4].Value = 0;
        //    parameters[5].Value = 0;
        //    parameters[6].Value = strWhere;	
        //    return OleDbSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        //}*/
        /// <summary>
        /// 获得监测员数据列表
        /// </summary>
        public string GetTopUnitMember(string userName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top 1 xmname from xmconnect,xm_member,unitmember where xmconnect.xmno =xm_member.xmno and xm_member.memberno = unitmember.memberno ");
            strSql.Append(" and unitmember.userName =@userName  order by  xmconnect.xmno");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255)
			};
            parameters[0].Value = userName;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if(obj == null ) obj = "";return obj.ToString();

        }
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@tblName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@fldName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@PageSize", OleDbType.Integer),
        //            new OleDbParameter("@PageIndex", OleDbType.Integer),
        //            new OleDbParameter("@IsReCount", OleDbType.Boolean),
        //            new OleDbParameter("@OrderType", OleDbType.Boolean),
        //            new OleDbParameter("@strWhere", OleDbType.VarChar,1000),
        //            };
        //    parameters[0].Value = "xmconnect";
        //    parameters[1].Value = "xmno";
        //    parameters[2].Value = PageSize;
        //    parameters[3].Value = PageIndex;
        //    parameters[4].Value = 0;
        //    parameters[5].Value = 0;
        //    parameters[6].Value = strWhere;	
        //    return OleDbSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        //}*/
        /// <summary>
        /// 获得管理员最新项目
        /// </summary>
        public string GetTopMember(string userName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top 1 xmname from xmconnect where createman=@createman  ");
            strSql.Append("   order by xmconnect.xmno");
            OleDbParameter[] parameters = {
					new OleDbParameter("@createman", OleDbType.VarChar,255)
			};
            parameters[0].Value = userName;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if(obj == null ) obj = "";return obj.ToString();

        }

        /// <summary>
        /// 获得管理员最新项目
        /// </summary>
        public string GetTop(string userName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top 1 xmname from xmconnect ");
            strSql.Append("   order by xmconnect.xmno  ");
           
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString());
            if(obj == null ) obj = "";return obj.ToString();

        }
        /// <summary>
        /// 获得监督员数据列表
        /// </summary>
        public string GetTopMonitorMember(string userName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top 1 xmname from xmconnect,xm_monitor,monitormember where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = monitormember.userno ");
            strSql.Append(" and monitormember.user =@userName  order by xmconnect.xmno "); OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255)
			};
            parameters[0].Value = userName;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if(obj == null ) obj = "";return obj.ToString();
        }

        public bool GetXmList(out List<string> xmlist)
        {
            
            string sql = "select distinct(xmname) from xmconnect ";
            xmlist = querysql.queryaccesslist(sql);
            return true;
        }

        //public bool GetCompleteXmCont(string unitname,out cont)
        //{
        //    string sql =string.Format("select count(1) from xmconnect,unitmember where xmconnect.createman = unitmember.username and  unitmember.ID  and  ID = '"+unitname+"' and  jcpgenddate<date()  ");
        //    object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, sql);
        //    if (obj == null) obj = ""; return obj.ToString();
        //}

        


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

