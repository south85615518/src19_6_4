﻿/**  版本信息模板在安装目录下，可自行修改。
* member.cs
*
* 功 能： N/A
* 类 名： member
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/12 10:16:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.OleDb;
using SqlHelpers;
using System.Collections.Generic;
using Tool;
//using Authority.DBUtility;//Please add references
namespace Authority.DAL
{
    /// <summary>
    /// 数据访问类:member
    /// </summary>
    public partial class member
    {
        public member()
        { }
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string userId, string pass)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select userid from member,unit");
            strSql.Append(" where  member.unitName = unit.unitName and  member.userId = @userId  and  member.pass = @pass  and    unit.isActive = true");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@userId", OleDbType.VarChar,50),
                    new OleDbParameter("@pass", OleDbType.VarChar,50)
                                          };
            parameters[0].Value = userId;
            parameters[1].Value = pass;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return obj == null ? false : true;

        }
        public string GetRole(string userId, string pass)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select role from member");
            strSql.Append(" where userId = @userId and pass = @pass ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@userId", OleDbType.VarChar,50),
                    new OleDbParameter("@pass", OleDbType.VarChar,50)
                                          };
            parameters[0].Value = userId;
            parameters[1].Value = pass;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null) obj = ""; return obj.ToString();
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Authority.Model.member model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into member(");
            strSql.Append("userId,pass,role,userGroup,userName,workNo,position,tel,email,zczsmc,zczsbh,zczs,sgzsmc,sgzsbh,sgzs,unitName,createTime)");
            strSql.Append(" values (");
            strSql.Append("@userId,@pass,@role,@userGroup,@userName,@workNo,@position,@tel,@email,@zczsmc,@zczsbh,@zczs,@sgzsmc,@sgzsbh,@sgzs,@unitName,@createTime)");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userId", OleDbType.VarChar,50),
					new OleDbParameter("@pass", OleDbType.VarChar,50),
					new OleDbParameter("@role", OleDbType.VarChar,50),
					new OleDbParameter("@userGroup", OleDbType.VarChar,200),
					new OleDbParameter("@userName", OleDbType.VarChar,200),
					new OleDbParameter("@workNo", OleDbType.VarChar,50),
					new OleDbParameter("@position", OleDbType.VarChar,50),
					new OleDbParameter("@tel", OleDbType.VarChar,50),
					new OleDbParameter("@email", OleDbType.VarChar,255),
					new OleDbParameter("@zczsmc", OleDbType.VarChar,200),
					new OleDbParameter("@zczsbh", OleDbType.VarChar,100),
					new OleDbParameter("@zczs", OleDbType.VarChar,200),
					new OleDbParameter("@sgzsmc", OleDbType.VarChar,200),
					new OleDbParameter("@sgzsbh", OleDbType.VarChar,200),
					new OleDbParameter("@sgzs", OleDbType.VarChar,100),
					new OleDbParameter("@unitName", OleDbType.VarChar,100),
					new OleDbParameter("@createTime", OleDbType.Date)};
            parameters[0].Value = model.userId;
            parameters[1].Value = model.pass;
            parameters[2].Value = model.role;
            parameters[3].Value = model.userGroup;
            parameters[4].Value = model.userName;
            parameters[5].Value = model.workNo;
            parameters[6].Value = model.position;
            parameters[7].Value = model.tel;
            parameters[8].Value = model.email;
            parameters[9].Value = model.zczsmc;
            parameters[10].Value = model.zczsbh;
            parameters[11].Value = model.zczs;
            parameters[12].Value = model.sgzsmc;
            parameters[13].Value = model.sgzsbh;
            parameters[14].Value = model.sgzs;
            parameters[15].Value = model.unitName;
            parameters[16].Value = model.createTime;

            int rows = 0;//OleDbSQLHelper.ExecuteSql(strSql.ToString(),parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Authority.Model.member model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update member set ");
            strSql.Append("pass=@pass,");
            strSql.Append("role=@role,");
            strSql.Append("userGroup=@userGroup,");
            strSql.Append("userName=@userName,");
            strSql.Append("workNo=@workNo,");
            strSql.Append("position=@position,");
            strSql.Append("tel=@tel,");
            strSql.Append("email=@email,");
            strSql.Append("zczsmc=@zczsmc,");
            strSql.Append("zczsbh=@zczsbh,");
            strSql.Append("zczs=@zczs,");
            strSql.Append("sgzsmc=@sgzsmc,");
            strSql.Append("sgzsbh=@sgzsbh,");
            strSql.Append("sgzs=@sgzs,");
            strSql.Append("unitName=@unitName,");
            strSql.Append("createTime=@createTime");
            strSql.Append(" where userId=@userId ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@pass", OleDbType.VarChar,50),
					new OleDbParameter("@role", OleDbType.VarChar,50),
					new OleDbParameter("@userGroup", OleDbType.VarChar,200),
					new OleDbParameter("@userName", OleDbType.VarChar,200),
					new OleDbParameter("@workNo", OleDbType.VarChar,50),
					new OleDbParameter("@position", OleDbType.VarChar,50),
					new OleDbParameter("@tel", OleDbType.VarChar,50),
					new OleDbParameter("@email", OleDbType.VarChar,255),
					new OleDbParameter("@zczsmc", OleDbType.VarChar,200),
					new OleDbParameter("@zczsbh", OleDbType.VarChar,100),
					new OleDbParameter("@zczs", OleDbType.VarChar,200),
					new OleDbParameter("@sgzsmc", OleDbType.VarChar,200),
					new OleDbParameter("@sgzsbh", OleDbType.VarChar,200),
					new OleDbParameter("@sgzs", OleDbType.VarChar,100),
					new OleDbParameter("@unitName", OleDbType.VarChar,100),
					new OleDbParameter("@createTime", OleDbType.Date),
					new OleDbParameter("@userId", OleDbType.VarChar,50)};
            parameters[0].Value = model.pass;
            parameters[1].Value = model.role;
            parameters[2].Value = model.userGroup;
            parameters[3].Value = model.userName;
            parameters[4].Value = model.workNo;
            parameters[5].Value = model.position;
            parameters[6].Value = model.tel;
            parameters[7].Value = model.email;
            parameters[8].Value = model.zczsmc;
            parameters[9].Value = model.zczsbh;
            parameters[10].Value = model.zczs;
            parameters[11].Value = model.sgzsmc;
            parameters[12].Value = model.sgzsbh;
            parameters[13].Value = model.sgzs;
            parameters[14].Value = model.unitName;
            parameters[15].Value = model.createTime;
            parameters[16].Value = model.userId;

            int rows = 0;//OleDbSQLHelper.ExecuteSql(strSql.ToString(),parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

       


        public bool SurveyBaseExchange(string userId,bool surveybase)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" update member set surveybase = @surveybase where userId=@userId  ");
            OleDbParameter[] parameters = { 
                                              new OleDbParameter("@surveybase", OleDbType.Boolean,2),
                                              new OleDbParameter("@userId", OleDbType.VarChar,50)

                                          };
            parameters[0].Value = surveybase;
            parameters[1].Value = userId;
            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }



        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string userId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from member ");
            strSql.Append(" where userId=@userId ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userId", OleDbType.VarChar,50)			};
            parameters[0].Value = userId;

            int rows = 0;//OleDbSQLHelper.ExecuteSql(strSql.ToString(),parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        ///// <summary>
        ///// 批量删除数据
        ///// </summary>
        //public bool DeleteList(string userIdlist)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("delete from member ");
        //    strSql.Append(" where userId in (" + userIdlist + ")  ");
        //    int rows = OleDbSQLHelper.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetXmList(string userID, out DataTable dt)
        {
            dt = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmno,xmname,jd,wd,dbname,type,jcpmurl,createman,company,xmaddress,pro,city,country,jcdw,delegateDw,telphone,xmBz,jcpgStartDate,jcpgEndDate,safeLevel,ResponseMan,buildInstrutment  ");
            strSql.Append(" FROM xmconnect,unitmember where xmconnect.createman = unitmember.username and  unitmember.id = '"+userID+"'    ");
            //if (strWhere.Trim() != "")
            //{
            //    strSql.Append(" where " + strWhere + "  order by xmno ");
            //}
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "xmconnect");

            if (ds != null)
            {
                dt = ds.Tables[0];
                return true;
            }
            else return false;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string userId, out Authority.Model.member model)
        {
            model = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select userId,pass,role,userGroup,userName,workNo,[position],tel,email,zczsmc,zczsbh,zczs,sgzsmc,sgzsbh,sgzs,unitName,createTime,surveybase from member ");
            strSql.Append(" where userId=@userId ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@userId", OleDbType.VarChar,50)};
            parameters[0].Value = userId;
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "member", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获取项目管理员信息
        /// </summary>
        /// <returns></returns>
        public bool GetXmMemberModel(int xmno, out Authority.Model.member model)
        {
            model = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select userId,pass,role,userGroup,userName,workNo,[position],tel,email,zczsmc,zczsbh,zczs,sgzsmc,sgzsbh,sgzs,unitName,createTime,surveybase from member join xmconnect on  member.userId = xmconnect.createMan    ");
            strSql.Append(" where xmconnect.xmno=@xmno ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@userId", OleDbType.VarChar,50)};
            parameters[0].Value = xmno;
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "member", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Authority.Model.member DataRowToModel(DataRow row)
        {
            Authority.Model.member model = new Authority.Model.member();
            if (row != null)
            {
                if (row["userId"] != null)
                {
                    model.userId = row["userId"].ToString();
                }
                if (row["pass"] != null)
                {
                    model.pass = row["pass"].ToString();
                }
                if (row["role"] != null)
                {
                    model.role = row["role"].ToString();
                }
                if (row["userGroup"] != null)
                {
                    model.userGroup = row["userGroup"].ToString();
                }
                if (row["userName"] != null)
                {
                    model.userName = row["userName"].ToString();
                }
                if (row["workNo"] != null)
                {
                    model.workNo = row["workNo"].ToString();
                }
                if (row["position"] != null)
                {
                    model.position = row["position"].ToString();
                }
                if (row["tel"] != null)
                {
                    model.tel = row["tel"].ToString();
                }
                if (row["email"] != null)
                {
                    model.email = row["email"].ToString();
                }
                if (row["zczsmc"] != null)
                {
                    model.zczsmc = row["zczsmc"].ToString();
                }
                if (row["zczsbh"] != null)
                {
                    model.zczsbh = row["zczsbh"].ToString();
                }
                if (row["zczs"] != null)
                {
                    model.zczs = row["zczs"].ToString();
                }
                if (row["sgzsmc"] != null)
                {
                    model.sgzsmc = row["sgzsmc"].ToString();
                }
                if (row["sgzsbh"] != null)
                {
                    model.sgzsbh = row["sgzsbh"].ToString();
                }
                if (row["sgzs"] != null)
                {
                    model.sgzs = row["sgzs"].ToString();
                }
                if (row["unitName"] != null)
                {
                    model.unitName = row["unitName"].ToString();
                }
                if (row["createTime"] != null && row["createTime"].ToString() != "")
                {
                    model.createTime = DateTime.Parse(row["createTime"].ToString());
                }
                if (row["surveybase"] != null && row["surveybase"].ToString() != "")
                {
                    model.surveybase = Convert.ToBoolean(row["surveybase"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得管理员所有项目
        /// </summary>
        public bool GetMemberNameList(string userName, out List<string> ls)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  xmname from xmconnect,unitmember where  xmconnect.createman = unitmember.username  and   id='" + userName + "'  ");
            strSql.Append("   order by xmconnect.xmno");
            ls = querysql.queryaccesslist(strSql.ToString());
            return ls != null ? true : false;

        }
        /// <summary>
        /// 获得管理员项目表
        /// </summary>
        public bool GetMemberXmTable(string userName, out DataTable dt)
        {
            dt = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select *,iif(format(jcpgEndDate,'yyyy/mm/dd HH:mm:ss') > format(date(),'yyyy/mm/dd HH:mm:ss'),'监测中','已完工') as state from xmconnect,unitmember where  xmconnect.createman = unitmember.username and  unitmember.id='" + userName + "' ");
            ExceptionLog.ExceptionWrite(strSql.ToString());
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "xmconnect");
            if (ds != null && ds.Tables.Count > 0) { dt = ds.Tables[0]; return true; }
            return false;

        }
        /// <summary>
        /// 多条件查询获得管理员项目表
        /// </summary>
        public bool MutilConditionMemberXmTableFilter(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area, out List<Authority.Model.xmconnect> xmconnectlist)
        {
            xmconnectlist = new List<Model.xmconnect>();
            string safeLevel = gcjb.TrimEnd() == "不限" ? " 1=1 and " : " safeLevel= '" + gcjb.TrimEnd() + "' and  ";
            gclx = gclx.TrimEnd() == "不限" ? " 1=1 and " : " type ='" + gclx.TrimEnd() + "' and ";
            string gcName = gcname.TrimEnd() == "" ? " 1=1 and" : " xmname like '%" + gcname.TrimEnd() + "%' and ";
            pro = pro.TrimEnd() == "=省份=" || pro.TrimEnd() == "" ? " 1=1 and" : " pro= '" + pro + "'  and  ";
            city = city.TrimEnd() == "=城市=" || city.TrimEnd() == "" ? " 1=1 and" : " city= '" + city + "' and ";
            area = area.TrimEnd() == "=地区=" || area.TrimEnd() == "" ? " 1=1 " : " country= '" + area + "'";
            //string sql = "select '" + userID + "' as userID,'\"'+xmname+'\"' as xmno,* from xmconnect,unitmember where  xmconnect.createman=unitmember.username and  unitmember.id='"+userID+"'   and " + safeLevel + gclx + gcName + pro + city + area;
            string sql = "select * from xmconnect,unitmember where  xmconnect.createman=unitmember.username and  unitmember.id='" + userID + "'   and " + safeLevel + gclx + gcName + pro + city + area;
            DataSet ds = OleDbSQLHelper.Query(sql, "xmconnect");
            if (ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0) return false;
            Authority.DAL.xmconnect xmconnect = new Authority.DAL.xmconnect();
            int i = 0;
            while(i < ds.Tables[0].Rows.Count )
            {
                xmconnectlist.Add(xmconnect.DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }

            //if (ds != null && ds.Tables.Count > 0) { dt = ds.Tables[0]; return true; }
            return true;
        }

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select userId,pass,role,userGroup,userName,workNo,position,tel,email,zczsmc,zczsbh,zczs,sgzsmc,sgzsbh,sgzs,unitName,createTime ");
        //    strSql.Append(" FROM member ");
        //    if (strWhere.Trim() != "")
        //    {
        //        strSql.Append(" where " + strWhere);
        //    }
        //    return OleDbSQLHelper.Query(strSql.ToString());
        //}

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select count(1) FROM member ");
        //    if (strWhere.Trim() != "")
        //    {
        //        strSql.Append(" where " + strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby);
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.userId desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from member T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return OleDbSQLHelper.Query(strSql.ToString());
        //}

        ///*
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@tblName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@fldName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@PageSize", OleDbType.Integer),
        //            new OleDbParameter("@PageIndex", OleDbType.Integer),
        //            new OleDbParameter("@IsReCount", OleDbType.Boolean),
        //            new OleDbParameter("@OrderType", OleDbType.Boolean),
        //            new OleDbParameter("@strWhere", OleDbType.VarChar,1000),
        //            };
        //    parameters[0].Value = "member";
        //    parameters[1].Value = "userId";
        //    parameters[2].Value = PageSize;
        //    parameters[3].Value = PageIndex;
        //    parameters[4].Value = 0;
        //    parameters[5].Value = 0;
        //    parameters[6].Value = strWhere;	
        //    return OleDbSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        //}*/
        /// <summary>
        /// 获取项目的负责人信息
        /// </summary>
        /// <param name="xmname"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool XmMember(string xmname, out Authority.Model.member model)
        {
            model = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select userId,pass,role,userGroup,userName,workNo,[position],tel,email,zczsmc,zczsbh,zczs,sgzsmc,sgzsbh,sgzs,unitName,createTime,surveybase from member ,xmconnect where xmconnect.createMan = member.userId and xmconnect.xmname=@xmname  ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@xmname", OleDbType.VarChar,100)};
            parameters[0].Value = xmname;
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "member", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool sms(string userid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update member set sms = iif(sms>0,sms-1,0) where userid='" + userid + "'");
            updatedb udb = new updatedb();
            int i = udb.updateaccess(strSql.ToString());
            strSql = new StringBuilder();
            strSql.Append("select sms from member where userid='" + userid+"'");
            int cont = int.Parse(querysql.queryaccessdbstring(strSql.ToString()));
            if (i > 0 && cont > 0) return true;
            return false;
        }

        


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

