﻿/**  版本信息模板在安装目录下，可自行修改。
* MonitorMember.cs
*
* 功 能： N/A
* 类 名： MonitorMember
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.OleDb;
using SqlHelpers;
using System.Collections.Generic;
using System;
using Tool;
//using Authority.DBUtility;//Please add references
namespace Authority.DAL
{
    /// <summary>
    /// 数据访问类:MonitorMember
    /// </summary>
    public partial class MonitorMember
    {
        public MonitorMember()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string userId, string pass, out Authority.Model.MonitorMember model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"  select userno from MonitorMember,unitmember,unit 
            where          MonitorMember.user= @user   and MonitorMember.pass = @pass      ");
            
            OleDbParameter[] parameters = {
                    new OleDbParameter("@user", OleDbType.VarChar,50),
                    new OleDbParameter("@pass", OleDbType.VarChar,50)
                                          };
            parameters[0].Value = userId;
            parameters[1].Value = pass;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null)
            {
                
                if (GetModel((int)obj, out model))
                return true;
            }
            model = null;
            return false;
        }
        public bool Exist(string name, string userId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from monitorMember,member ");
            strSql.Append(" where monitorMember.id = unitmember.userid   and [name] = @name   and    member.userid = @userId");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@name", OleDbType.VarChar,50),
                    new OleDbParameter("@userId", OleDbType.VarChar,50)
                                          };
            parameters[0].Value = name;
            parameters[1].Value = userId;
            ExceptionLog.ExceptionWrite(strSql.ToString());
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return obj == null ? false : Convert.ToInt32(obj) > 0 ? true : false;
        }
        /// <summary>
        /// 得到监督人员ID
        /// </summary>
        public int GetMonitorId(string name, string unitname)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select userNo from MonitorMember");
            strSql.Append(" where name = @name   and   unitcall = @unitcall");
            OleDbParameter[] parameters = {
					new OleDbParameter("@_call", OleDbType.VarChar,255),
                    new OleDbParameter("@_unitcall", OleDbType.VarChar,255)
			};
            parameters[0].Value = name;
            parameters[1].Value = unitname;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return (int)obj;
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string memberno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from MonitorMember");
            strSql.Append(" where user=@userName");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255)
			};
            parameters[0].Value = memberno;

            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return (int)obj == 0 ? false : true;
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Authority.Model.MonitorMember model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MonitorMember(");
            strSql.Append("[user],[pass],[position],[tel],[email],[unitcall],[name],[ID])");
            strSql.Append(" values (");
            strSql.Append("@user,@pass,@position,@tel,@email,@unitcall,@name,@ID)");
            OleDbParameter[] parameters = {
					new OleDbParameter("@user", OleDbType.VarChar,255),
					new OleDbParameter("@pass", OleDbType.VarChar,255),
					new OleDbParameter("@position", OleDbType.VarChar,255),
					new OleDbParameter("@tel", OleDbType.VarChar,255),
					new OleDbParameter("@email", OleDbType.VarChar,255),
					new OleDbParameter("@unitcall", OleDbType.VarChar,255),
					new OleDbParameter("@name", OleDbType.VarChar,255),
					new OleDbParameter("@ID", OleDbType.VarChar,255)};
            parameters[0].Value = model.user;
            parameters[1].Value = model.pass;
            parameters[2].Value = model.position == null ? "" : model.position;
            parameters[3].Value = model.tel == null ? "" : model.tel;
            parameters[4].Value = model.email;
            parameters[5].Value = model.unit_call;
            parameters[6].Value = model.name;
            parameters[7].Value = model.ID;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Authority.Model.MonitorMember model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update MonitorMember set ");
            strSql.Append("pass=@pass,");
            strSql.Append("tel=@tel,");
            strSql.Append("email=@email,");
            strSql.Append("unitcall=@unit_call,");
            strSql.Append("name=@name,");
            strSql.Append(" ID=@ID ");
            strSql.Append("  where userNO=@userNO");
            OleDbParameter[] parameters = {
					new OleDbParameter("@pass", OleDbType.VarChar,255),
					new OleDbParameter("@tel", OleDbType.VarChar,255),
					new OleDbParameter("@email", OleDbType.VarChar,255),
					new OleDbParameter("@unit_call", OleDbType.VarChar,255),
					new OleDbParameter("@name", OleDbType.VarChar,255),
					new OleDbParameter("@ID", OleDbType.VarChar,255),
					new OleDbParameter("@userNO", OleDbType.Integer,4)};
            parameters[0].Value = model.pass;
            parameters[1].Value = model.tel;
            parameters[2].Value = model.email;
            parameters[3].Value = model.unit_call;
            parameters[4].Value = model.name;
            parameters[5].Value = model.ID;
            parameters[6].Value = model.userNO;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int userNO)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from MonitorMember ");
            strSql.Append(" where userNO=@userNO");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userNO", OleDbType.Integer,4)
			};
            parameters[0].Value = userNO;

            int rows = 0; //OleDbSQLHelper.ExecuteNonQuery(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string xmname)
        {
            //StringBuilder strSql = new StringBuilder();
            //strSql.Append("delete MonitorMember from MonitorMember,  ");
            //strSql.Append(" where userNO in (" + userNOlist + ")  ");
            //int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
            //if (rows > 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }


        public bool GetModelList(int xmno,out List<Authority.Model.MonitorMember> ls )
        {
            
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT MonitorMember.* from xmconnect ,Xm_monitor ,MonitorMember where xmconnect.xmno = Xm_monitor.xmno and  Xm_monitor.userNo  = MonitorMember.userNo and xmconnect.xmno = @xmno");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@xmno", OleDbType.Integer,4)
            };
            parameters[0].Value = xmno;

            ls = new List<Authority.Model.MonitorMember>();
            int i = 0;
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "MonitorMember", parameters);
            if (ds == null) return false;
            while (i < ds.Tables[0].Rows.Count)
            {
                ls.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool  GetModel(int userNO , out Authority.Model.MonitorMember model)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from MonitorMember ");
            strSql.Append(" where userNO=@userNO");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@userNO", OleDbType.Integer,4)
            };
            parameters[0].Value = userNO;

             model = new Authority.Model.MonitorMember();
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "MonitorMember", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model =  DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Authority.Model.MonitorMember DataRowToModel(DataRow row)
        {
            Authority.Model.MonitorMember model = new Authority.Model.MonitorMember();
            if (row != null)
            {
                if (row["user"] != null)
                {
                    model.user = row["user"].ToString();
                }
                if (row["pass"] != null)
                {
                    model.pass = row["pass"].ToString();
                }
                if (row["role"] != null)
                {
                    model.role = row["role"].ToString();
                }
                if (row["position"] != null)
                {
                    model.position = row["position"].ToString();
                }
                if (row["tel"] != null)
                {
                    model.tel = row["tel"].ToString();
                }
                if (row["email"] != null)
                {
                    model.email = row["email"].ToString();
                }
                if (row["unitcall"] != null)
                {
                    model.unit_call = row["unitcall"].ToString();
                }
                if (row["name"] != null)
                {
                    model.name = row["name"].ToString();
                }
                if (row["userNO"] != null && row["userNO"].ToString() != "")
                {
                    model.userNO = int.Parse(row["userNO"].ToString());
                }
                if (row["ID"] != null)
                {
                    model.ID = row["ID"].ToString();
                }
                if (row["surveybase"] != null && row["surveybase"].ToString() != "")
                {
                    model.surveybase = Convert.ToBoolean(row["surveybase"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public bool GetList(string userId, out DataSet ds)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select user,pass,role,[position],tel,email,unitcall,name,userNO,ID,name+'['+unitcall+']' as pos ");
            strSql.Append(" FROM MonitorMember ");
            if (userId.Trim() != "")
            {
                strSql.Append(" where  id  in ('" + userId.Replace(",","','")+"') ");
            }
            ds = OleDbSQLHelper.Query(strSql.ToString(), "MonitorMember", null);
            if (ds != null) return true;
            else
                return false;
        }

   

        ///// <summary>
        ///// 获取监督人员列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql = new StringBuilder();
        //    strSql.Append("select user,pass,role,[position],tel,email,unitcall,name,userNO,ID ");
        //    strSql.Append(" FROM MonitorMember ");
        //    if (strWhere.Trim() != "")
        //    {
        //        strSql.Append(" where " + strWhere);
        //    }
        //    return OleDbSQLHelper.Query(strSql.ToString(), "MonitorMember", null);
        //}
        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM MonitorMember ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.userNO desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from MonitorMember T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return DbHelperOleDb.Query(strSql.ToString());
        //}
        public bool XmMonitorMemberLoad(string xmname, out string supervisorPeoples)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select monitormember.name+'['+monitormember.unitcall+']' from xmconnect,xm_monitor,monitormember where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = monitormember.userno and xmconnect.xmname='" + xmname + "' ");
            List<string> ls = querysql.queryaccesslist(strSql.ToString());
            if (ls != null)
            {
                supervisorPeoples = string.Join(",", ls);
                return true;
            }
            else
            {
                supervisorPeoples = "";
                return false;
            }
        }
        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            OleDbParameter[] parameters = {
                    new OleDbParameter("@tblName", OleDbType.VarChar, 255),
                    new OleDbParameter("@fldName", OleDbType.VarChar, 255),
                    new OleDbParameter("@PageSize", OleDbType.Integer),
                    new OleDbParameter("@PageIndex", OleDbType.Integer),
                    new OleDbParameter("@IsReCount", OleDbType.Boolean),
                    new OleDbParameter("@OrderType", OleDbType.Boolean),
                    new OleDbParameter("@strWhere", OleDbType.VarChar,1000),
                    };
            parameters[0].Value = "MonitorMember";
            parameters[1].Value = "userNO";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperOleDb.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/
        /// <summary>
        /// 获得监督员所有项目
        /// </summary>
        public bool GetMonitorNameList(string userName, out List<string> ls)
        {
            
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  xmname from xmconnect,xm_monitor,monitormember where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = monitormember.userno ");
            strSql.Append(" and monitormember.user ='" + userName + "'  order by xmconnect.xmno ");
            ls = querysql.queryaccesslist(strSql.ToString());
            return ls != null ? true : false;

        }
        /// <summary>
        /// 获得监督员项目表
        /// </summary>
        public bool GetMonitorXmDb(string userName, out DataTable dt)
        {
            dt = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmconnect.* from xmconnect,xm_monitor where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = " + userName + " order by xmconnect.xmno ");
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "xmconnect");
            if (ds != null && ds.Tables.Count > 0) { dt = ds.Tables[0]; return true; }
            return false;

        }
        /// <summary>
        /// 多条件查询获得监测员项目表
        /// </summary>
        public bool MutilConditionMonitorMemberXmTableFilter(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area, out List<Authority.Model.xmconnect> xmconnectlist)
        {
            xmconnectlist = new List<Model.xmconnect>();
            StringBuilder strSql = new StringBuilder();
            //strSql.Append("select '" + userID + "' as userID,'\"'+xmname+'\"' as xmno,* from xmconnect,xm_monitor,monitormember where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = monitormember.userno ");
            //strSql.Append(" and monitormember.user ='" + userID + "'" );
            strSql.Append("select xmconnect.xmno as xmno,* from xmconnect,xm_monitor,monitormember where xmconnect.xmno =xm_monitor.xmno and xm_monitor.userno = monitormember.userno ");
            strSql.Append(" and monitormember.user ='" + userID + "'");
            string safeLevel = gcjb.TrimEnd() == "不限" ? " 1=1 and " : " safeLevel= '" + gcjb.TrimEnd() + "' and  ";
            gclx = gclx.TrimEnd() == "不限" ? " 1=1 and " : " type ='" + gclx.TrimEnd() + "' and ";
            string gcName = gcname.TrimEnd() == "" ? " 1=1 and" : " xmname like '%" + gcname.TrimEnd() + "%' and ";
            pro = pro.TrimEnd() == "=省份=" || pro.TrimEnd() == "" ? " 1=1 and" : " pro= '" + pro + "'  and  ";
            city = city.TrimEnd() == "=城市=" || city.TrimEnd() == "" ? " 1=1 and" : " city= '" + city + "' and ";
            area = area.TrimEnd() == "=地区=" || area.TrimEnd() == "" ? " 1=1 " : " country= '" + area + "'";

            strSql.Append( "  and  " + safeLevel + gclx + gcName + pro + city + area+"  ");

            strSql.Append(" order by xmconnect.xmno ");
            string sql = strSql.ToString();
            ExceptionLog.ExceptionWrite(sql);
            DataSet ds = OleDbSQLHelper.Query(sql, "xmconnect");
            if (ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0) return false;
            Authority.DAL.xmconnect xmconnect = new Authority.DAL.xmconnect();
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                xmconnectlist.Add(xmconnect.DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        public bool GetMonitorMemberXmUnit(int userno,out string unitName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select unitno from monitormember ,unitmember where monitormember.id = unitmember.username and monitormember.userno="+userno);
            ExceptionLog.ExceptionWrite(strSql.ToString());
             unitName = querysql.queryaccessdbstring(strSql.ToString());
             return unitName == "" ? false : true;
        }

        public bool sms(int userno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update monitormember set sms = iif(sms>0,sms-1,0) where userno=" + userno);
            updatedb udb = new updatedb();
            int i = udb.updateaccess(strSql.ToString());
            strSql = new StringBuilder();
            strSql.Append("select sms from monitormember where userno=" + userno );
            int cont =int.Parse(querysql.queryaccessdbstring(strSql.ToString()));
            if (i > 0&&cont>0) return true;
            return false;
        }

     


        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

