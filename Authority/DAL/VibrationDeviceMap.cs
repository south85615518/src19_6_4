﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.OleDb;
using SqlHelpers;

namespace Authority.DAL
{
    
    public class VibrationDeviceMap
    {
        //根据设备编号获取到项目编号
        public bool GetXmnoBySno(string sno, out string xmno)
        {
            xmno = null;
           string sql = string.Format("select xmno from VibrationDeviceMap where sno = {0}", sno);
           DataSet ds = OleDbSQLHelper.Query(sql, "VibrationDeviceMap", null);
           DataTable dt = ds.Tables[0];
           if (dt.Rows.Count > 0)
           {
               xmno = dt.Rows[0]["xmno"].ToString();
               return true;
           }  
           else
               return false;
        }
    }
}
