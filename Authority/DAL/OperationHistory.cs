﻿/**  版本信息模板在安装目录下，可自行修改。
* OperationHistory.cs
*
* 功 能： N/A
* 类 名： OperationHistory
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/22 14:17:15   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.OleDb;
using SqlHelpers;
//using Authority.DBUtility;//Please add references
namespace Authority.DAL
{
	/// <summary>
	/// 数据访问类:OperationHistory
	/// </summary>
	public partial class OperationHistory
	{
		public OperationHistory()
		{}
		#region  BasicMethod

        ///// <summary>
        ///// 得到最大ID
        ///// </summary>
        //public int GetMaxId()
        //{
        //return DbHelperOleDb.GetMaxID("id", "OperationHistory"); 
        //}

        ///// <summary>
        ///// 是否存在该记录
        ///// </summary>
        //public bool Exists(int id)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) from OperationHistory");
        //    strSql.Append(" where id=@id");
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@id", OleDbType.Integer,4)
        //    };
        //    parameters[0].Value = id;

        //    return DbHelperOleDb.Exists(strSql.ToString(),parameters);
        //}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Authority.Model.OperationHistory model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into OperationHistory(");
			strSql.Append("uintName,dbname)");
			strSql.Append(" values (");
			strSql.Append("@uintName,@dbname)");
			OleDbParameter[] parameters = {
					new OleDbParameter("@uintName", OleDbType.VarChar,200),
					new OleDbParameter("@dbname", OleDbType.VarChar,200)};
			parameters[0].Value = model.uintName;
			parameters[1].Value = model.dbname;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Authority.Model.OperationHistory model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update OperationHistory set ");
			strSql.Append("uintName=@uintName,");
			strSql.Append("dbname=@dbname");
			strSql.Append(" where id=@id");
			OleDbParameter[] parameters = {
					new OleDbParameter("@uintName", OleDbType.VarChar,200),
					new OleDbParameter("@dbname", OleDbType.VarChar,200),
					new OleDbParameter("@id", OleDbType.Integer,4),
					new OleDbParameter("@xmno", OleDbType.Integer,4)};
			parameters[0].Value = model.uintName;
			parameters[1].Value = model.dbname;
			parameters[2].Value = model.id;
			parameters[3].Value = model.xmno;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OperationHistory ");
			strSql.Append(" where id=@id");
			OleDbParameter[] parameters = {
					new OleDbParameter("@id", OleDbType.Integer,4)
			};
			parameters[0].Value = id;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        ///// <summary>
        ///// 批量删除数据
        ///// </summary>
        //public bool DeleteList(string idlist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from OperationHistory ");
        //    strSql.Append(" where id in ("+idlist + ")  ");
        //    int rows=DbHelperOleDb.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        ///// <summary>
        ///// 得到一个对象实体
        ///// </summary>
        //public Authority.Model.OperationHistory GetModel(int id)
        //{
			
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select id,uintName,dbname,xmno from OperationHistory ");
        //    strSql.Append(" where id=@id");
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@id", OleDbType.Integer,4)
        //    };
        //    parameters[0].Value = id;

        //    Authority.Model.OperationHistory model=new Authority.Model.OperationHistory();
        //    DataSet ds=DbHelperOleDb.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        return DataRowToModel(ds.Tables[0].Rows[0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Authority.Model.OperationHistory DataRowToModel(DataRow row)
		{
			Authority.Model.OperationHistory model=new Authority.Model.OperationHistory();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["uintName"]!=null)
				{
					model.uintName=row["uintName"].ToString();
				}
				if(row["dbname"]!=null)
				{
					model.dbname=row["dbname"].ToString();
				}
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
			}
			return model;
		}

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        //public DataSet GetList(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select id,uintName,dbname,xmno ");
        //    strSql.Append(" FROM OperationHistory ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    return DbHelperOleDb.Query(strSql.ToString());
        //}

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM OperationHistory ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.id desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from OperationHistory T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return DbHelperOleDb.Query(strSql.ToString());
        //}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			OleDbParameter[] parameters = {
					new OleDbParameter("@tblName", OleDbType.VarChar, 255),
					new OleDbParameter("@fldName", OleDbType.VarChar, 255),
					new OleDbParameter("@PageSize", OleDbType.Integer),
					new OleDbParameter("@PageIndex", OleDbType.Integer),
					new OleDbParameter("@IsReCount", OleDbType.Boolean),
					new OleDbParameter("@OrderType", OleDbType.Boolean),
					new OleDbParameter("@strWhere", OleDbType.VarChar,1000),
					};
			parameters[0].Value = "OperationHistory";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperOleDb.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

