﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using SqlHelpers;
using System.Data.Odbc;
using Tool;

namespace Authority.DAL
{
    public class DTUXmPort
    {


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(Authority.Model.DTUXmPort model)
        {
            //StringBuilder strSql = new StringBuilder();
            //strSql.Append("insert into DTUXmPort(");
            //strSql.Append("xmno,port,type)");
            //strSql.Append(" values (");
            //strSql.Append("@xmno,@port,@type)");
            //OleDbParameter[] parameters = {
            //        new OleDbParameter("@xmno", OleDbType.Integer,4),
            //        new OleDbParameter("@port", OleDbType.VarChar,255),
            //        new OleDbParameter("@type", OleDbType.VarChar,255)};
            //parameters[0].Value = model.xmno;
            //parameters[1].Value = model.port;
            //parameters[2].Value = model.type;

            //int rows = OleDbSQLHelper.ExecuteSql(strSql.ToString(), parameters);
            //if (rows > 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }


        public bool GetDTUPortXm(int port, out Authority.Model.DTUXmPort model)
        {
            StringBuilder strSql = new StringBuilder();
            model = new Model.DTUXmPort();
            strSql.Append("select * from DTUXmPort where port =@port  ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@port", OleDbType.VarChar,50)
                   
                                          };
            parameters[0].Value = port;
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "DTUXmPort", parameters);
            if (ds.Tables[0].Rows.Count == 0) return false;
            model = DataRowToModel(ds.Tables[0].Rows[0]);
            return true;
        }
        public bool GetDTUPortXm(int port, out string xmname)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("select xmname from DTUXmPort,xmconnect where DTUXmPort.xmno =xmconnect.xmno and  port =@port ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@port", OleDbType.VarChar,50)
                   
                                          };
            parameters[0].Value = port;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            xmname = obj == null ? "" : obj.ToString();
            return true;
        }

        public bool DTUXmPortList(out List<string> superadministratorxmnameList)
        {
            string sql = "select distinct(xmname) from DTUXmPort,xmconnect where  DTUXmPort.xmno =xmconnect.xmno and DTUXmPort.type='水位' ";
            superadministratorxmnameList = querysql.queryaccesslist(sql);
            return true;
        }
        public bool FixedInclinometerXmPortList(out List<string> superadministratorxmnameList)
        {
            string sql = "select distinct(xmname) from DTUXmPort,xmconnect where  DTUXmPort.xmno =xmconnect.xmno and DTUXmPort.type='固定测斜' ";
            superadministratorxmnameList = querysql.queryaccesslist(sql);
            return true;
        }

        /// <summary>
        /// 获得管理员项目表
        /// </summary>
        public bool GetDTUXmTable(out DataTable dt)
        {
            dt = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmname,dtuxmport.xmno as xmno , port,dtuxmport.type as type  from  dtuxmport left join xmconnect on dtuxmport.xmno = xmconnect.xmno   ");
            ExceptionLog.ExceptionWrite(strSql.ToString());
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "xmconnect");
            if (ds != null && ds.Tables.Count > 0) { dt = ds.Tables[0]; return true; }
            return false;

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Authority.Model.DTUXmPort DataRowToModel(DataRow row)
        {
            Authority.Model.DTUXmPort model = new Authority.Model.DTUXmPort();
            if (row != null)
            {

                if (row["xmno"] != null && row["xmno"].ToString() != "")
                {
                    model.xmno = int.Parse(row["xmno"].ToString());
                }
                if (row["type"] != null && row["type"].ToString() != "")
                {
                    model.type = row["type"].ToString();
                }
                if (row["port"] != null && row["port"].ToString() != "")
                {
                    model.port = Int32.Parse(row["port"].ToString());
                }
            }
            return model;
        }
    }
}
