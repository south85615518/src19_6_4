﻿/**  版本信息模板在安装目录下，可自行修改。
* xmreference.cs
*
* 功 能： N/A
* 类 名： xmreference
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2018/8/30 16:20:45   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.OleDb;
using SqlHelpers;
using System.Collections.Generic;
//Please add references
namespace Authority.DAL
{
	/// <summary>
	/// 数据访问类:xmreference
	/// </summary>
	public partial class xmreference
	{
		public xmreference()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(Authority.Model.xmreference model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from xmreference");
			strSql.Append(" where  xmno = @xmno  and  client_xmname=@client_xmname ");
			OleDbParameter[] parameters = {
                    new OleDbParameter("@xmno", OleDbType.Integer),	
					new OleDbParameter("@client_xmname", OleDbType.VarChar,255)			
                                          };
			parameters[0].Value = model.xmno;
            parameters[1].Value = model.client_xmname;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
            if(obj == null) return false;
            return Convert.ToInt32(obj) > 0;
            
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Authority.Model.xmreference model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into xmreference(");
			strSql.Append("xmno,client_xmname)");
			strSql.Append(" values (");
			strSql.Append("@xmno,@client_xmname)");
			OleDbParameter[] parameters = {
					new OleDbParameter("@xmno", OleDbType.Integer,4),
					new OleDbParameter("@client_xmname", OleDbType.VarChar,255)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.client_xmname;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(Authority.Model.xmreference model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update xmreference set ");
			strSql.Append("xmno=@xmno");
			strSql.Append(" where client_xmname=@client_xmname ");
			OleDbParameter[] parameters = {
					new OleDbParameter("@xmno", OleDbType.Integer,4),
					new OleDbParameter("@client_xmname", OleDbType.VarChar,255)};
			parameters[0].Value = model.xmno;
			parameters[1].Value = model.client_xmname;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(int xmno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from xmreference ");
			strSql.Append(" where  xmno = @xmno   ");
			OleDbParameter[] parameters = {
                    new OleDbParameter("@xmno", OleDbType.Integer)		
                                          };
            parameters[0].Value = xmno;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows >= 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        public bool RefrenceListLoad(int xmno,out List<string> ls)
        {
            string sql = "select client_xmname from xmreference where xmno = "+xmno;
            ls = querysql.queryaccesslist(sql);
            return true;
        }

        public bool XmnoGet(string client_xmname,out int xmno)
        {
            string sql = "select xmno from xmreference where client_xmname = '" + client_xmname + "'";
            xmno = Convert.ToInt32(querysql.queryaccessdbstring(sql));
            return true;
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
        //public Authority.Model.xmreference GetModel(string client_xmname)
        //{
			
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select xmno,client_xmname from xmreference ");
        //    strSql.Append(" where client_xmname=@client_xmname ");
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@client_xmname", OleDbType.VarChar,255)			};
        //    parameters[0].Value = client_xmname;

        //    Authority.Model.xmreference model=new Authority.Model.xmreference();
        //    DataSet ds=OleDbSQLHelper.Query(strSql.ToString(),parameters);
        //    if(ds.Tables[0].Rows.Count>0)
        //    {
        //        return DataRowToModel(ds.Tables[0].Rows[0]);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public Authority.Model.xmreference DataRowToModel(DataRow row)
		{
			Authority.Model.xmreference model=new Authority.Model.xmreference();
			if (row != null)
			{
				if(row["xmno"]!=null && row["xmno"].ToString()!="")
				{
					model.xmno=int.Parse(row["xmno"].ToString());
				}
				if(row["client_xmname"]!=null)
				{
					model.client_xmname=row["client_xmname"].ToString();
				}
			}
			return model;
		}

		

		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

