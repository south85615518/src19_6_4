﻿/**  版本信息模板在安装目录下，可自行修改。
* UnitMember.cs
*
* 功 能： N/A
* 类 名： UnitMember
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/6/9 16:18:46   N/A    初版
*
* Copyright (c) 2012 Authority Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System.Data;
using System.Text;
using System.Data.OleDb;
//using Authority.DBUtility;
using SqlHelpers;
using System.Collections.Generic;
using System;
using Tool;//Please add references
namespace Authority.DAL
{
	/// <summary>
	/// 数据访问类:UnitMember
	/// </summary>
    public partial class UnitMember : System.Web.UI.Page
	{
		public UnitMember()
		{}
		#region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string userId, string pass, out Authority.Model.UnitMember member )
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select memberno from UnitMember,unit ");
            strSql.Append(" where UnitMember.unitno = unit.unitName   and UnitMember.userName = @userName   and    UnitMember.pass = @pass   and    unit.isActive = true   ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@userName", OleDbType.VarChar,50),
                    new OleDbParameter("@pass", OleDbType.VarChar,50)
                                          };
            parameters[0].Value = userId;
            parameters[1].Value = pass;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj != null)
            {
                string mssg = "";
                if(GetModel((int)obj,out member))
                return true;
            }
            member = null;
            return false;
        }
        public bool Exist(string name,string userId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from UnitMember,member ");
            strSql.Append(" where UnitMember.id = member.userid   and [_call] = @name   and    member.userid = @userId ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@name", OleDbType.VarChar,50),
                    new OleDbParameter("@userId", OleDbType.VarChar,50)
                                          };
            parameters[0].Value = name;
            parameters[1].Value = userId;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return obj == null ? false : Convert.ToInt32(obj) > 0 ? true : false;
        }
        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMemberId(string name)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select memberno from UnitMember " );
            strSql.Append("  where  [_call] = @_call ");
            OleDbParameter[] parameters = {
					new OleDbParameter("@_call", OleDbType.VarChar,255)
			};
            parameters[0].Value = name;
            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            return (int)obj;
        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string memberno)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from UnitMember");
            strSql.Append(" where userName=@userName");
            OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255)
			};
            parameters[0].Value = memberno;

            object obj = OleDbSQLHelper.ExecuteScalar(CommandType.Text,strSql.ToString(),parameters);
            return  (int)obj == 0 ? false : true;
        }
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(Authority.Model.UnitMember model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into UnitMember(");
			strSql.Append("[userName],[pass],[role],[position],[tel],[email],[zczsmc],[zczsbh],[zczs],[sgzsmc],[sgzsbh],[sgzs],[ID],[unitno],[_call])");
			strSql.Append(" values (");
			strSql.Append("@userName,@pass,@role,@position,@tel,@email,@zczsmc,@zczsbh,@zczs,@sgzsmc,@sgzsbh,@sgzs,@ID,@unitno,@_call)");
			OleDbParameter[] parameters = {
					new OleDbParameter("@userName", OleDbType.VarChar,255),
					new OleDbParameter("@pass", OleDbType.VarChar,255),
					new OleDbParameter("@role", OleDbType.VarChar,255),
					new OleDbParameter("@position", OleDbType.VarChar,255),
					new OleDbParameter("@tel", OleDbType.VarChar,255),
					new OleDbParameter("@email", OleDbType.VarChar,255),
					new OleDbParameter("@zczsmc", OleDbType.VarChar,255),
					new OleDbParameter("@zczsbh", OleDbType.VarChar,255),
					new OleDbParameter("@zczs", OleDbType.VarChar,255),
					new OleDbParameter("@sgzsmc", OleDbType.VarChar,255),
					new OleDbParameter("@sgzsbh", OleDbType.VarChar,255),
					new OleDbParameter("@sgzs", OleDbType.VarChar,255),
					new OleDbParameter("@ID", OleDbType.VarChar,255),
					new OleDbParameter("@unitno", OleDbType.VarChar,255),
					new OleDbParameter("@_call", OleDbType.VarChar,255)};
			parameters[0].Value = model.userName;
			parameters[1].Value = model.pass;
			parameters[2].Value = model.role;
			parameters[3].Value = model.position;
			parameters[4].Value = model.tel;
			parameters[5].Value = model.email;
			parameters[6].Value = model.zczsmc;
			parameters[7].Value = model.zczsbh;
            parameters[8].Value = model.zczs == null ? "" : model.zczs;
			parameters[9].Value = model.sgzsmc;
			parameters[10].Value = model.sgzsbh;
            parameters[11].Value = model.sgzs == null ? "" : model.sgzs;
			parameters[12].Value = model.ID;
			parameters[13].Value = model.unitno;
			parameters[14].Value = model._call;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(Authority.Model.UnitMember model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update UnitMember set ");
            strSql.Append("[pass]=@pass,");
            strSql.Append("[position]=@position,");
            strSql.Append("tel=@tel,");
            strSql.Append("email=@email,");
            strSql.Append("zczsmc=@zczsmc,");
            strSql.Append("zczsbh=@zczsbh,");
            strSql.Append("zczs=@zczs,");
            strSql.Append("sgzsmc=@sgzsmc,");
            strSql.Append("sgzsbh=@sgzsbh,");
            strSql.Append("sgzs=@sgzs,");
            strSql.Append("[_call]=@_call ");
            strSql.Append(" where memberno=@memberno");
            OleDbParameter[] parameters = {
					new OleDbParameter("@pass", OleDbType.VarChar,255),
					new OleDbParameter("@position", OleDbType.VarChar,255),
					new OleDbParameter("@tel", OleDbType.VarChar,255),
					new OleDbParameter("@email", OleDbType.VarChar,255),
					new OleDbParameter("@zczsmc", OleDbType.VarChar,255),
					new OleDbParameter("@zczsbh", OleDbType.VarChar,255),
					new OleDbParameter("@zczs", OleDbType.VarChar,255),
					new OleDbParameter("@sgzsmc", OleDbType.VarChar,255),
					new OleDbParameter("@sgzsbh", OleDbType.VarChar,255),
					new OleDbParameter("@sgzs", OleDbType.VarChar,255),
					new OleDbParameter("@_call", OleDbType.VarChar,255),
					new OleDbParameter("@memberno", OleDbType.Integer,4)};
            parameters[0].Value = model.pass;
            parameters[1].Value = model.position;
            parameters[2].Value = model.tel;
            parameters[3].Value = model.email;
            parameters[4].Value = model.zczsmc;
            parameters[5].Value = model.zczsbh;
            parameters[6].Value = model.zczs == null ? "" : model.zczs;
            parameters[7].Value = model.sgzsmc;
            parameters[8].Value = model.sgzsbh;
            parameters[9].Value = model.sgzs == null ? "" : model.sgzs;
            parameters[10].Value = model._call;
            parameters[11].Value = model.memberno;

            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SurveyBaseExchange(string userId, bool surveybase)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" update unitmember set surveybase = @surveybase where userName=@userName  ");
            OleDbParameter[] parameters = { 
                                              new OleDbParameter("@surveybase", OleDbType.Boolean,2),
                                              new OleDbParameter("@userName", OleDbType.VarChar,50)

                                          };
            parameters[0].Value = surveybase;
            parameters[1].Value = userId;
            int rows = OleDbSQLHelper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int memberno)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from UnitMember ");
			strSql.Append(" where memberno=@memberno");
			OleDbParameter[] parameters = {
					new OleDbParameter("@memberno", OleDbType.Integer,4)
			};
			parameters[0].Value = memberno;

			int rows=OleDbSQLHelper.ExecuteNonQuery(CommandType.Text,strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        ///// <summary>
        ///// 批量删除数据
        ///// </summary>
        //public bool DeleteList(string membernolist )
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("delete from UnitMember ");
        //    strSql.Append(" where memberno in ("+membernolist + ")  ");
        //    int rows=OleDbSQLHelper.ExecuteSql(strSql.ToString());
        //    if (rows > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(int memberno,out Authority.Model.UnitMember model )
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from UnitMember ");
            strSql.Append(" where memberno=@memberno");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@memberno", OleDbType.Integer,4)
            };
            parameters[0].Value = memberno;

            model = new Authority.Model.UnitMember();
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(),"UnitMember" ,parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public bool GetModel(string userID, out Authority.Model.UnitMember model)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from UnitMember ");
            strSql.Append(" where userName=@userName");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@userName", OleDbType.VarChar)
            };
            parameters[0].Value = userID;

            model = new Authority.Model.UnitMember();
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "UnitMember", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                model = DataRowToModel(ds.Tables[0].Rows[0]);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Authority.Model.UnitMember DataRowToModel(DataRow row)
        {
            Authority.Model.UnitMember model = new Authority.Model.UnitMember();
            if (row != null)
            {
                if (row["userName"] != null)
                {
                    model.userName = row["userName"].ToString();
                }
                if (row["pass"] != null)
                {
                    model.pass = row["pass"].ToString();
                }
                if (row["role"] != null)
                {
                    model.role = row["role"].ToString();
                }
                if (row["position"] != null)
                {
                    model.position = row["position"].ToString();
                }
                if (row["tel"] != null)
                {
                    model.tel = row["tel"].ToString();
                }
                if (row["email"] != null)
                {
                    model.email = row["email"].ToString();
                }
                if (row["zczsmc"] != null)
                {
                    model.zczsmc = row["zczsmc"].ToString();
                }
                if (row["zczsbh"] != null)
                {
                    model.zczsbh = row["zczsbh"].ToString();
                }
                if (row["zczs"] != null)
                {
                    model.zczs = row["zczs"].ToString();
                }
                if (row["sgzsmc"] != null)
                {
                    model.sgzsmc = row["sgzsmc"].ToString();
                }
                if (row["sgzsbh"] != null)
                {
                    model.sgzsbh = row["sgzsbh"].ToString();
                }
                if (row["sgzs"] != null)
                {
                    model.sgzs = row["sgzs"].ToString();
                }
                if (row["memberno"] != null && row["memberno"].ToString() != "")
                {
                    model.memberno = int.Parse(row["memberno"].ToString());
                }
                if (row["ID"] != null)
                {
                    model.ID = row["ID"].ToString();
                }
                if (row["unitno"] != null)
                {
                    model.unitno = row["unitno"].ToString();
                }
                if (row["_call"] != null)
                {
                    model._call = row["_call"].ToString();
                }
                if (row["datamodify"] != null)
                {
                    model.datamodify = row["datamodify"].ToString();
                }
                if (row["surveybase"] != null && row["surveybase"].ToString() != "")
                {
                    model.surveybase =Convert.ToBoolean(row["surveybase"].ToString());
                }
            }
            return model;
        }

        ///// <summary>
        ///// 获得数据列表
        ///// </summary>
        public bool GetList(string userID, out DataSet ds)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select *   ");
			strSql.Append(" FROM UnitMember ");
            if (userID.Trim() != "")
			{
				strSql.Append(" where  id='"+userID+"'");
			}
            ds = OleDbSQLHelper.Query(strSql.ToString(),"UnitMember",null);
            if (ds != null) return true;
            else return false;
		}

        

        ///// <summary>
        ///// 获取记录总数
        ///// </summary>
        //public int GetRecordCount(string strWhere)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("select count(1) FROM UnitMember ");
        //    if(strWhere.Trim()!="")
        //    {
        //        strSql.Append(" where "+strWhere);
        //    }
        //    object obj = DbHelperSQL.GetSingle(strSql.ToString());
        //    if (obj == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(obj);
        //    }
        //}
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        //{
        //    StringBuilder strSql=new StringBuilder();
        //    strSql.Append("SELECT * FROM ( ");
        //    strSql.Append(" SELECT ROW_NUMBER() OVER (");
        //    if (!string.IsNullOrEmpty(orderby.Trim()))
        //    {
        //        strSql.Append("order by T." + orderby );
        //    }
        //    else
        //    {
        //        strSql.Append("order by T.memberno desc");
        //    }
        //    strSql.Append(")AS Row, T.*  from UnitMember T ");
        //    if (!string.IsNullOrEmpty(strWhere.Trim()))
        //    {
        //        strSql.Append(" WHERE " + strWhere);
        //    }
        //    strSql.Append(" ) TT");
        //    strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
        //    return OleDbSQLHelper.Query(strSql.ToString());
        //}
        public bool XmUnitMemberLoad(string xmname, out string monitorPeople)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select [_call]+'  电话: '+tel+' 邮箱: '+[email]  from xmconnect,xm_member,unitmember where xmconnect.xmno = xm_member.xmno and xm_member.memberno = unitmember.memberno and xmconnect.xmname='"+xmname+"'  ");
            ExceptionLog.ExceptionWrite(strSql.ToString());
            List<string> ls = querysql.queryaccesslist(strSql.ToString());
            if (ls != null)
            {
                monitorPeople = string.Join(",", ls);
                return true;
            }
            else
            {
                monitorPeople = "";
                return false;
            }
        }
        ///
        ///// <summary>
        ///// 分页获取数据列表
        ///// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //    OleDbParameter[] parameters = {
        //            new OleDbParameter("@tblName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@fldName", OleDbType.VarChar, 255),
        //            new OleDbParameter("@PageSize", OleDbType.Integer),
        //            new OleDbParameter("@PageIndex", OleDbType.Integer),
        //            new OleDbParameter("@IsReCount", OleDbType.Boolean),
        //            new OleDbParameter("@OrderType", OleDbType.Boolean),
        //            new OleDbParameter("@strWhere", OleDbType.VarChar,1000),
        //            };
        //    parameters[0].Value = "UnitMember";
        //    parameters[1].Value = "memberno";
        //    parameters[2].Value = PageSize;
        //    parameters[3].Value = PageIndex;
        //    parameters[4].Value = 0;
        //    parameters[5].Value = 0;
        //    parameters[6].Value = strWhere;	
        //    return OleDbSQLHelper.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        //}*/
        /// <summary>
        /// 获得监测员所有项目
        /// </summary>
        public bool GetUnitMemberNameList(string userName, out List<string> ls)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  xmname from xmconnect,xm_member,unitmember where xmconnect.xmno =xm_member.xmno and xm_member.memberno = unitmember.memberno ");
            strSql.Append(" and unitmember.userName ='" + userName + "'  order by  xmconnect.xmno");
            ls = querysql.queryaccesslist(strSql.ToString());
            return ls != null ? true : false;

        }

        /// <summary>
        /// 获得监测员所有项目
        /// </summary>
        public bool GetUnitMemberXmDb(string userName, out DataTable dt)
        {
            dt = null;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select xmconnect.*,iif(format(jcpgEndDate,'yyyy/mm/dd HH:mm:ss') > format(date(),'yyyy/mm/dd HH:mm:ss'),'监测中','已完工') as state from xmconnect,xm_member where xmconnect.xmno =xm_member.xmno and xm_member.memberno = " + userName + " order by xmconnect.xmno   ");
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "xmconnect");
            if (ds != null && ds.Tables.Count > 0) { dt = ds.Tables[0]; return true; }
            return false;

        }
        /// <summary>
        /// 多条件查询获得监测员项目表
        /// </summary>
        public bool MutilConditionUnitMemberXmTableFilter(string userID, string gcjb, string gclx, string gcname, string pro, string city, string area,  out List<Authority.Model.xmconnect> xmconnectlist)
        {
            xmconnectlist = new List<Model.xmconnect>();
            StringBuilder strSql = new StringBuilder();
            //strSql.Append("select '" + userID + "' as userID,'\"'+xmname+'\"' as xmno,*  from xmconnect,xm_member,unitmember where xm_member.memberno = unitmember.memberno   and  xmconnect.xmno =xm_member.xmno and unitmember.username = '" + userID+"'");
            strSql.Append("select  xmconnect.xmno as xmno,*  from xmconnect,xm_member,unitmember where xm_member.memberno = unitmember.memberno   and  xmconnect.xmno =xm_member.xmno and unitmember.username = '" + userID + "'");
            string safeLevel = gcjb.TrimEnd() == "不限" ? " 1=1 and " : " safeLevel= '" + gcjb.TrimEnd() + "' and  ";
            gclx = gclx.TrimEnd() == "不限" ? " 1=1 and " : " type ='" + gclx.TrimEnd() + "' and ";
            string gcName = gcname.TrimEnd() == "" ? " 1=1 and" : " xmname like '%" + gcname.TrimEnd() + "%' and ";
            pro = pro.TrimEnd() == "=省份=" || pro.TrimEnd() == "" ? " 1=1 and" : " pro= '" + pro + "'  and  ";
            city = city.TrimEnd() == "=城市=" || city.TrimEnd() == "" ? " 1=1 and" : " city= '" + city + "' and ";
            area = area.TrimEnd() == "=地区=" || area.TrimEnd() == "" ? " 1=1 " : " country= '" + area + "'";
            strSql.Append(" and  " + safeLevel + gclx + gcName + pro + city + area + "  ");

            strSql.Append(" order by xmconnect.xmno ");
            string sql = strSql.ToString();
            ExceptionLog.ExceptionWrite(sql);
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "xmconnect");
            if (ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0) return false;
            Authority.DAL.xmconnect xmconnect = new Authority.DAL.xmconnect();
            int i = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                xmconnectlist.Add(xmconnect.DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }

            //if (ds != null && ds.Tables.Count > 0) { dt = ds.Tables[0]; return true; }
            return true;
        }
        /// <summary>
        /// 获取项目的监测员
        /// </summary>
        /// <param name="xmno"></param>
        /// <param name="lmm"></param>
        /// <returns></returns>
        public bool XmMonitorListGet(int xmno,out List<Authority.Model.UnitMember> lmm)
        {
            
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select unitmember.memberno as memberno,* from xm_member ,unitmember where xm_member.memberno = unitmember.memberno and xmno = @xmno ");
            OleDbParameter[] parameters = {
                    new OleDbParameter("@xmno", OleDbType.Integer,10)
            };
            parameters[0].Value = xmno;

            lmm = new List<Authority.Model.UnitMember>();
            DataSet ds = OleDbSQLHelper.Query(strSql.ToString(), "unitMember", parameters);
            int i = 0;
            while (i<ds.Tables[0].Rows.Count)
            {
                lmm.Add(DataRowToModel(ds.Tables[0].Rows[i]));
                i++;
            }
            return true;
        }

        public bool sms(int memberno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update unitmember set sms = iif(sms>0,sms-1,0) where memberno=" + memberno);
            updatedb udb = new updatedb();
            int i = udb.updateaccess(strSql.ToString());
            strSql = new StringBuilder();
            strSql.Append("select sms from unitmember where memberno=" + memberno);
            int cont = int.Parse(querysql.queryaccessdbstring(strSql.ToString()));
            if (i > 0 && cont > 0) return true;
            return false;
        }


		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

