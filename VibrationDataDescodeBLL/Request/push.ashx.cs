﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tool;


namespace VibrationDataDescodeBLL
{
    /// <summary>
    /// push 的摘要说明
    /// </summary>
    public class push : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            ExceptionLog.ExceptionWrite("访问请求");

            var requestFormparameters = context.Request.Form;
            foreach (var model in requestFormparameters.Keys)
            {
                ExceptionLog.ExceptionWrite(string.Format("key:{0}-value:{1}", model, context.Request.Form[model.ToString()]));
            }
            var requestQueryStringparameters = context.Request.QueryString;
            foreach (var model in requestQueryStringparameters.Keys)
            {
                ExceptionLog.ExceptionWrite(string.Format("key:{0}-value:{1}", model, context.Request.QueryString[model.ToString()]));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}